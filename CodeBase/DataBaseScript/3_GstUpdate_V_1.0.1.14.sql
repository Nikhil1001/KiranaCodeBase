USE [Kirana0001]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.MItemTaxSetting ADD
    IGSTPercent numeric(18, 2)  NULL,
	CGSTPercent numeric(18, 2)  NULL,
	SGSTPercent numeric(18, 2)  NULL,
	UTGSTPercent numeric(18, 2) NULL,
	CessPercent numeric(18, 2)  NULL
GO
COMMIT

GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.MItemTaxInfo ADD
    IGSTPercent numeric(18, 2)   NULL,
	CGSTPercent numeric(18, 2)   NULL,
	SGSTPercent numeric(18, 2)  NULL,
	UTGSTPercent numeric(18, 2)  NULL,
	CessPercent numeric(18, 2)  NULL
GO
COMMIT

GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AddMItemTaxSetting]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AddMItemTaxSetting]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Create PROCEDURE [dbo].[AddMItemTaxSetting]
     @PkSrNo                              numeric(18),
     @TaxSettingName                      varchar(100),
     @TaxLedgerNo                         numeric(18),
     @SalesLedgerNo                       numeric(18),
     @CalculationMethod                   varchar(50),
     @Percentage                          numeric(18,2),
     @IsActive                            bit,
     @UserID                              numeric(18),
     @UserDate                            datetime,
     @TaxTypeNo                           numeric(18),
     @TransactionTypeNo                   numeric(18),
     @IGSTPercent						  numeric(18,2),
	 @CGSTPercent						  numeric(18,2),
	 @SGSTPercent						  numeric(18,2),
	 @UTGSTPercent						  numeric(18,2),
	 @CessPercent						  numeric(18,2)

AS
IF EXISTS(select PkSrNo from MItemTaxSetting
          where
          PkSrNo = @PkSrNo)
     BEGIN
       --Update existing row
       UPDATE MItemTaxSetting
       SET
          TaxSettingName = @TaxSettingName,
          TaxLedgerNo = @TaxLedgerNo,
          SalesLedgerNo = @SalesLedgerNo,
          CalculationMethod = @CalculationMethod,
          Percentage = @Percentage,
          IsActive = @IsActive,
          UserID = @UserID,
          UserDate = @UserDate,
          TaxTypeNo = @TaxTypeNo,
          TransactionTypeNo = @TransactionTypeNo,
		  IGSTPercent=@IGSTPercent,
		  CGSTPercent=@CGSTPercent,
	      SGSTPercent=@SGSTPercent,
	      UTGSTPercent=@UTGSTPercent,
	      CessPercent=@CessPercent
	
       WHERE
          PkSrNo = @PkSrNo

     END
ELSE
     BEGIN
       --Insert new row
       Declare @Id numeric
       SELECT @Id=IsNull(Max(PkSrNo),0) From MItemTaxSetting
       DBCC CHECKIDENT('MItemTaxSetting', RESEED, @Id)
       INSERT INTO MItemTaxSetting(
          TaxSettingName,
          TaxLedgerNo,
          SalesLedgerNo,
          CalculationMethod,
          Percentage,
          IsActive,
          UserID,
          UserDate,
          TaxTypeNo,
          TransactionTypeNo,
		  IGSTPercent,
		  CGSTPercent,
		  SGSTPercent,
		  UTGSTPercent,
		  CessPercent 

)
       VALUES(
          @TaxSettingName,
          @TaxLedgerNo,
          @SalesLedgerNo,
          @CalculationMethod,
          @Percentage,
          @IsActive,
          @UserID,
          @UserDate,
          @TaxTypeNo,
          @TransactionTypeNo,
		  @IGSTPercent,
		  @CGSTPercent,
		  @SGSTPercent,
		  @UTGSTPercent,
		  @CessPercent

)
END

GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AddMItemTaxInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AddMItemTaxInfo]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Create PROCEDURE [dbo].[AddMItemTaxInfo]
     @PkSrNo                              numeric(18),
     @ItemNo                              numeric(18),
     @TaxLedgerNo                         numeric(18),
	 @SalesLedgerNo						  numeric(18),
     @FromDate                            datetime,
     @CalculationMethod                   varchar(50),
     @Percentage                          numeric(18,2),
	 @FKTaxSettingNo					  numeric(18),
     @UserID                              numeric(18),
     @UserDate                            datetime,
     @TaxTypeNo                           numeric(18,0),
     @TransactionTypeNo                   numeric(18, 0),
     @IsActive                            bit,
     @HSNCode                             varchar(50),
	 @HSNNo                               numeric(18),
	 @IGSTPercent						  numeric(18,2),
	 @CGSTPercent						  numeric(18,2),
	 @SGSTPercent						  numeric(18,2),
	 @UTGSTPercent						  numeric(18,2),
	 @CessPercent						  numeric(18,2)

AS

IF ((@PkSrNo > 0) AND (EXISTS(select PkSrNo from MItemTaxInfo
          where
          PkSrNo = @PkSrNo)))
     BEGIN
       --Update existing row
       UPDATE MItemTaxInfo
       SET
          ItemNo = @ItemNo,
          TaxLedgerNo = @TaxLedgerNo,
		  SalesLedgerNo=@SalesLedgerNo,
          --FromDate = @FromDate,
          CalculationMethod = @CalculationMethod,
          Percentage = @Percentage,
		  FKTaxSettingNo = @FKTaxSettingNo,
          UserID = @UserID,
          UserDate = @UserDate,
          TaxTypeNo = @TaxTypeNo,
          TransactionTypeNo = @TransactionTypeNo,
          IsActive = @IsActive,
          HSNCode = @HSNCode,
          HSNNo = @HSNNo,
		  IGSTPercent=@IGSTPercent,
		  CGSTPercent=@CGSTPercent,
		  SGSTPercent=@SGSTPercent,
		  UTGSTPercent=@UTGSTPercent,
		  CessPercent=@CessPercent,		  
          ModifiedBy = isnull(ModifiedBy,'') + cast(@UserID as varchar)+'@'+ CONVERT(VARCHAR(10), GETDATE(), 105)
       WHERE
          PkSrNo = @PkSrNo

     END
ELSE 

	BEGIN
	   --Insert new row
	   --Declare @Id numeric
	   --SELECT @Id=IsNull(Max(PkSrNo),0) From MItemTaxInfo
	   --DBCC CHECKIDENT('MItemTaxInfo', RESEED, @Id)
	    Set @FromDate='1-July-2017'
		Update MItemTaxInfo Set IsActive='False' Where ItemNo = @ItemNo And TaxTypeNo = @TaxTypeNo
									      And TransactionTypeNo = @TransactionTypeNo

	   INSERT INTO MItemTaxInfo(
		  ItemNo,
		  TaxLedgerNo,
		  SalesLedgerNo,
		  FromDate,
		  CalculationMethod,
		  Percentage,
		  FKTaxSettingNo,
		  UserID,
		  UserDate,
          TaxTypeNo,
          TransactionTypeNo,
          IsActive,
          HSNCode,
          HSNNo,
		  IGSTPercent,
		  CGSTPercent,
		  SGSTPercent,
		  UTGSTPercent,
		  CessPercent 

       )
	   VALUES(
		  @ItemNo,
		  @TaxLedgerNo,
		  @SalesLedgerNo,
		  @FromDate,
		  @CalculationMethod,
		  @Percentage,
		  @FKTaxSettingNo,
		  @UserID,
		  @UserDate,
          @TaxTypeNo,
          @TransactionTypeNo,
          @IsActive,
          @HSNCode,
          @HSNNo,
		  @IGSTPercent,
	      @CGSTPercent,
		  @SGSTPercent,
		  @UTGSTPercent,
		  @CessPercent
	
       )

END
Go
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/ 
Update MItemTaxInfo 
	SET IGSTPercent = Percentage, 
	CGSTPercent = ROUND(Percentage/2,2), 
	SGSTPercent = ROUND(Percentage/2,2),
	UTGSTPercent = 0,
	CessPercent = 0
	WHERE 
	TaxTypeNo = 38

Go
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/ 
Update MItemTaxSetting 
SET IGSTPercent = Percentage, 
CGSTPercent = ROUND(Percentage/2,2), 
SGSTPercent = ROUND(Percentage/2,2),
UTGSTPercent = 0,
CessPercent = 0
WHERE 
TaxTypeNo = 38

Go
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/ 
Update MItemTaxSetting 
SET IGSTPercent = 0, 
CGSTPercent = 0, 
SGSTPercent = 0,
UTGSTPercent = 0,
CessPercent = 0
WHERE 
TaxTypeNo <> 38

Go
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/ 

Update MItemTaxInfo 
SET IGSTPercent = 0, 
CGSTPercent = 0, 
SGSTPercent = 0,
UTGSTPercent = 0,
CessPercent = 0
WHERE 
TaxTypeNo <> 38
Go
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Update MItemTaxInfo Set IsActive='False' Where TaxTypeNo<>38
Go
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Update MItemTaxInfo Set IsActive='False' Where IsActive='True' And TaxTypeNo=38 And TransactionTypeNo=11 And PkSrNo Not In
	(Select Max(PkSrNo) From MItemTaxInfo Where TaxTypeNo=38 And IsActive='True' and TransactionTypeNo=11 Group By ItemNo)
Go
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/ 
Update MItemTaxInfo Set IsActive='False' Where IsActive='True' And TaxTypeNo=38 And TransactionTypeNo=10 And PkSrNo Not In
	(Select Max(PkSrNo) From MItemTaxInfo Where TaxTypeNo=38 And IsActive='True' and TransactionTypeNo=10 Group By ItemNo)
Go
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
 CREATE NONCLUSTERED INDEX IX_MItemTaxInfo1 ON dbo.MItemTaxInfo
 (
 ItemNo,
 TaxTypeNo,
 IsActive
 ) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetItemTaxAll]') AND type in (N'TF'))
DROP Function [dbo].[GetItemTaxAll]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/ 
Create Function [dbo].[GetItemTaxAll]
(
@PItemNo     numeric(18),
@POnDate     datetime,
@PGroupNo     numeric(18),
@PTaxTypeNo     numeric(18),
@PItemGroupNo     numeric(18)
)

RETURNS @TTax TABLE (
 PkSrNo numeric(18, 0),
 ItemNo numeric(18, 0) ,
 TaxLedgerNo numeric(18, 0) ,
 SalesLedgerNo numeric(18, 0) ,
 FromDate datetime ,
 CalculationMethod varchar(50) ,
 Percentage numeric(18,2) ,
 CompanyNo numeric(18, 0),
 GroupNo numeric(18,0),
 TaxTypeNo numeric(18,0),
 FkTaxSettingNo numeric(18),
 IsActive bit,
 HSNNo numeric(18),
 HSNCode varchar(50),
 IGSTPercent numeric(18,2),
 CGSTPercent numeric(18,2),
 SGSTPercent numeric(18,2),
 UTGSTPercent numeric(18,2),
 CessPercent numeric(18,2)
)
AS
Begin

Declare @StrFilter varchar(max), @SqlQuery varchar(max), @VItemNo numeric(18, 0),@VGroupNo numeric(18,0)

Declare @PkSrNo numeric(18, 0),
 @ItemNo numeric(18, 0) ,
 @TaxLedgerNo numeric(18, 0) ,
 @SalesLedgerNo numeric(18, 0) ,
 @FromDate datetime ,
 @CalculationMethod varchar(50) ,
 @Percentage numeric(18,2) ,
 @CompanyNo numeric(18, 0),
    @GroupNo numeric(18,0), -- TransactionTypeNo
 @TaxTypeNo numeric(18,0),
 @VTaxTypeNo numeric(18,0),
 @FKTaxSettingNo numeric(18),
 @IsActive bit,
 @HSNNo numeric(18),
 @HSNCode varchar(50),
 @IGSTPercent numeric(18,2),
 @CGSTPercent numeric(18,2),
 @SGSTPercent numeric(18,2),
 @UTGSTPercent numeric(18,2),
 @CessPercent numeric(18,2)

SET @PkSrNo = 0
SET @ItemNo = 0
SET @TaxLedgerNo = 0
SET @SalesLedgerNo = 0
SET @FromDate = getdate()
SET @CalculationMethod = ''
SET @Percentage = 0.00
SET @CompanyNo = 0
SET @VItemNo = 0
SET @GroupNo = 0
SET @VGroupNo = 0
SET @TaxTypeNo = 0
SET @VTaxTypeNo = 0
SET @FKTaxSettingNo = 0
SET @IsActive = 'True'
SET @HSNNo = 0
SET @HSNCode = ''
SET @IGSTPercent = 0
SET @CGSTPercent = 0
SET @SGSTPercent = 0
SET @UTGSTPercent = 0
SET @CessPercent = 0

Select Top 1 @CompanyNo=CompanyNo FRom MCompany

if (@PItemGroupNo is null)
 Declare CurRate Cursor For Select PkSrNo, ItemNo, TaxLedgerNo, SalesLedgerNo, FromDate, CalculationMethod, 
    Percentage, @CompanyNo, MItemTaxInfo.TransactionTypeNo As GroupNo, MItemTaxInfo.TaxTypeNo,IsNull(FKTaxSettingNo,0) As FKTaxSettingNo,
    MItemTaxInfo.IsActive, MItemTaxInfo.HSNNo, MItemTaxInfo.HSNCode, MItemTaxInfo.IGSTPercent, MItemTaxInfo.CGSTPercent,
                MItemTaxInfo.SGSTPercent, MItemTaxInfo.UTGSTPercent, MItemTaxInfo.CessPercent
                From MItemTaxInfo 
    WHERE ItemNo=Case When @PItemNo is null then ItemNo else @PItemNo end 
    AND MItemTaxInfo.TransactionTypeNo=Case When @PGroupNo is null then MItemTaxInfo.TransactionTypeNo else @PGroupNo end 
             AND MItemTaxInfo.TaxTypeNo=Case When @PTaxTypeNo is null then MItemTaxInfo.TaxTypeNo else @PTaxTypeNo end
			 AND IsActive='True'
                Order by ItemNo,FromDate desc 
else
 Declare CurRate Cursor For Select MItemTaxInfo.PkSrNo, MItemTaxInfo.ItemNo, MItemTaxInfo.TaxLedgerNo, MItemTaxInfo.SalesLedgerNo, MItemTaxInfo.FromDate, MItemTaxInfo.CalculationMethod, 
    MItemTaxInfo.Percentage, @CompanyNo, MItemTaxInfo.TransactionTypeNo As GroupNo, MItemTaxInfo.TaxTypeNo,IsNull(MItemTaxInfo.FKTaxSettingNo,0) as FKTaxSettingNo,
    MItemTaxInfo.IsActive, MItemTaxInfo.HSNNo, MItemTaxInfo.HSNCode, MItemTaxInfo.IGSTPercent, MItemTaxInfo.CGSTPercent,
                MItemTaxInfo.SGSTPercent, MItemTaxInfo.UTGSTPercent, MItemTaxInfo.CessPercent
                From MStockItems 
    INNER JOIN MItemTaxInfo ON MStockItems.ItemNo = MItemTaxInfo.ItemNo
    WHERE MStockItems.ItemNo=Case When @PItemNo is null then MStockItems.ItemNo else @PItemNo end
    AND MStockItems.GroupNo=Case When @PItemGroupNo is null then MStockItems.GroupNo else @PItemGroupNo end  
    AND MItemTaxInfo.TransactionTypeNo=Case When @PGroupNo is null then MItemTaxInfo.TransactionTypeNo else @PGroupNo end 
             AND MItemTaxInfo.TaxTypeNo=Case When @PTaxTypeNo is null then MItemTaxInfo.TaxTypeNo else @PTaxTypeNo end
	AND MItemTaxInfo.IsActive='True'
    Order by ItemNo,FromDate desc 

Open CurRate 

Fetch CurRate into @PkSrNo, @ItemNo, @TaxLedgerNo, @SalesLedgerNo, @FromDate, @CalculationMethod, 
     @Percentage, @CompanyNo,@GroupNo, @TaxTypeNo, @FKTaxSettingNo,
    @IsActive, @HSNNo, @HSNCode, @IGSTPercent, @CGSTPercent, @SGSTPercent, @UTGSTPercent, @CessPercent

DECLARE @isRecOK int

while(@@Fetch_Status = 0)
Begin

    SET @isRecOK = 1

 if (@POnDate is not NULL AND @FromDate > @POnDate)
 Begin
   SET @isRecOK = 0
 End --if(isNUll(@POnDate))
 else if(getdate()<@FromDate)
 Begin
  SET @isRecOK = 0
 End

 if((@isRecOK = 1) AND (@VItemNo != @ItemNo OR @VGroupNo!=@GroupNo OR @VTaxTypeNo!=@TaxTypeNo))
 Begin
  set @VItemNo = @ItemNo  
  set @VGroupNo=@GroupNo
  set @VTaxTypeNo = @TaxTypeNo
  insert into @TTax values (@PkSrNo, @ItemNo, @TaxLedgerNo, @SalesLedgerNo,
                 @FromDate, @CalculationMethod, @Percentage, @CompanyNo, @GroupNo, @TaxTypeNo, @FKTaxSettingNo,
                 @IsActive, @HSNNo, @HSNCode, @IGSTPercent, @CGSTPercent, @SGSTPercent, @UTGSTPercent, @CessPercent)
 End --if(@VItemNo != @ItemNo 
 
 Fetch CurRate into @PkSrNo, @ItemNo, @TaxLedgerNo, @SalesLedgerNo, @FromDate,
     @CalculationMethod, @Percentage, @CompanyNo, @GroupNo, @TaxTypeNo, @FKTaxSettingNo,
     @IsActive, @HSNNo, @HSNCode, @IGSTPercent, @CGSTPercent, @SGSTPercent, @UTGSTPercent, @CessPercent
End --CurRate while(@@Fetch_Status = 0)

close CurRate deallocate CurRate 

Return
End
Go
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/ 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetBillGSTTaxDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetBillGSTTaxDetails]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Create Procedure [dbo].[GetBillGSTTaxDetails]
@PkVoucherNo	numeric(18)
AS 

SELECT     TaxPercentage, SUM(TaxAmount) AS TaxAmount, SUM(Amount) AS Amount,SUM( CGSTAmount) as CGSTAmount , Sum(SGSTAmount) As SGSTAmount,Sum( CessAmount) as CessAmount
FROM         TStock
WHERE     (FKVoucherNo = @PKVoucherNo)
GROUP BY TaxPercentage,IGSTPercent,CGSTPercent,SGSTPercent,UTGSTPercent,CessPercent
ORDER BY TaxPercentage
Go

/*---------------------------------------------------------------------------------------------------------------------------------------------------*/

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.MStockItems ADD
	ShowItemName varchar(500) NULL,
	ShowItemName_Lang varchar(500) NULL,
	IsQtyRead bit NULL
GO
COMMIT

Go
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/ 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AddMStockItems]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AddMStockItems]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Create PROCEDURE [dbo].[AddMStockItems]
     @ItemNo                             numeric(18),
     @ItemName                           varchar(50),
     @ItemShortCode                      varchar(50),
     @GroupNo                            numeric(18),
     @UOMPrimary                         numeric(18),
	 @UOMDefault                         numeric(18),
	 @CompanyNo							 numeric(18),
	 @GroupNo1                           numeric(18),
	 @FKStockDeptNo						 numeric(18),
	 @FkDepartmentNo                     numeric(18),
	 @FkCategoryNo						 numeric(18),
     @FKStockLocationNo					 numeric(18),
     @IsActive                           bit,
     @IsFixedBarcode                     bit,
	 --@ReOrderLevelQty					 numeric(18,2),
     @LangFullDesc                       varchar(max),
     @LangShortDesc                      varchar(max),
	 @MinLevel							 numeric(18),
     @MaxLevel							 numeric(18),
     @UserId                             numeric(18),
     @UserDate                           datetime,
	 @ShortCode							 varchar(50),
     @FkStockGroupTypeNo                 numeric(18),
     @ControlUnder                       numeric(18),
     @FactorVal                          numeric(18,3),
     @Margin							 numeric(18,2),
	 @MKTQty				             numeric(18,2),
	 @GodownNo				             numeric(18),
	 @DiscountType					     numeric(18),
	 @HamaliInKg						 numeric(18,2),
	 @IsQtyRead							 Bit,
	 @ReturnID                           int output
AS

Declare @ShowItemName varchar(500),@ShowItemName_Lang Varchar(500)
Select @ShowItemName=IsNull(StockGroupName,''),@ShowItemName_Lang=IsNull(LanguageName,'') From MStockGroup Where StockGroupNo=@GroupNo
Set @ShowItemName=@ShowItemName+' '+@ItemName
Set @ShowItemName_Lang=@ShowItemName_Lang+' '+@LangFullDesc

IF EXISTS(select ItemNo from MStockItems
          where
          ItemNo = @ItemNo)
     BEGIN
       --Update existing row
       UPDATE MStockItems
       SET
          ItemName = @ItemName,
          ItemShortCode = @ItemShortCode,
          GroupNo = @GroupNo,
          UOMPrimary = @UOMPrimary,
		  UOMDefault = @UOMDefault,
		  CompanyNo = @CompanyNo,
		  GroupNo1 = @GroupNo1,
		  FKStockDeptNo = @FKStockDeptNo,
		  FkDepartmentNo = @FkDepartmentNo,
		  FkCategoryNo = @FkCategoryNo,
		  FKStockLocationNo=@FKStockLocationNo,
          IsActive = @IsActive,
          IsFixedBarcode = @IsFixedBarcode,
		  --ReOrderLevelQty=@ReOrderLevelQty,
          LangFullDesc = @LangFullDesc,
          LangShortDesc = @LangShortDesc,
		  MinLevel=@MinLevel,
		  MaxLevel=@MaxLevel,
          UserId = @UserId,
          UserDate = @UserDate,
		  ShortCode=@ShortCode,
          FkStockGroupTypeNo=@FkStockGroupTypeNo,
          ControlUnder=@ControlUnder,
          FactorVal=@FactorVal,
		  ModifiedBy = isnull(ModifiedBy,'') + cast(@UserID as varchar)+'@'+ CONVERT(VARCHAR(10), GETDATE(), 105),
          StatusNo=2,
		  Margin=@Margin,
		  MKTQty=@MKTQty,
		  GodownNo=@GodownNo,
	      HamaliInKg=@HamaliInKg,
		  DiscountType=@DiscountType,
		  IsQtyRead=@IsQtyRead,
		  ShowItemName=@ShowItemName,
		  ShowItemName_Lang=@ShowItemName_Lang
       WHERE
          ItemNo = @ItemNo
set @ReturnID = @ItemNo

     END
ELSE
     BEGIN


       --Insert new row
       Declare @Id numeric
       SELECT @Id=IsNull(Max(ItemNo),0) From MStockItems
       DBCC CHECKIDENT('MStockItems', RESEED, @Id)
       INSERT INTO MStockItems(
          ItemName,
          ItemShortCode,
          GroupNo,
          UOMPrimary,
		  UOMDefault,
		  CompanyNo,
		  GroupNo1,
		  FKStockDeptNo,
		  FkDepartmentNo,
		  FkCategoryNo,
		  FKStockLocationNo,
          IsActive,
          IsFixedBarcode,
		  --ReOrderLevelQty,
	      MinLevel,
		  MaxLevel,
          LangFullDesc,
          LangShortDesc,
          UserId,
          UserDate,
		  ShortCode,
          StatusNo,
          FkStockGroupTypeNo,
          ControlUnder,
          FactorVal,
  		  Margin,
		  MKTQty,
		  GodownNo,
		  DiscountType,
		  HamaliInKg,
		  IsQtyRead,
		  ShowItemName,
		  ShowItemName_Lang
)
       VALUES(
          @ItemName,
          @ItemShortCode,
          @GroupNo,
          @UOMPrimary,
		  @UOMDefault,
		  @CompanyNo,
		  @GroupNo1,
		  @FKStockDeptNo,
		  @FkDepartmentNo,
		  @FkCategoryNo,
		  @FKStockLocationNo,
          @IsActive,
          @IsFixedBarcode,
		  --@ReOrderLevelQty,
		  @MinLevel,
		  @MaxLevel,
          @LangFullDesc,
          @LangShortDesc,
          @UserId,
          @UserDate,
		  @ShortCode,
          1,
          @FkStockGroupTypeNo,
          @ControlUnder,
          @FactorVal,
		  @Margin,  
		  @MKTQty,
		  @GodownNo,
		  @DiscountType,
		  @HamaliInKg,
		  @IsQtyRead,
		  @ShowItemName,
		  @ShowItemName_Lang
)
set @ReturnID = Scope_Identity()
END
Go
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/ 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AddMStockGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AddMStockGroup]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Create PROCEDURE [dbo].[AddMStockGroup]
     @StockGroupNo                        numeric(18),
     @StockGroupName                      varchar(50),
	 @LanguageName						  varchar(200),
     @ControlGroup                        numeric(18),
	 @ControlSubGroup					  numeric(18),
     @IsActive                            bit,
     @UserId                              numeric(18),
     @UserDate                            datetime,
	 @CompanyNo							  numeric(18),
     @MfgCompNo                           numeric(18),
	 @Margin						      numeric(18,2),
	 @IsApplyToAll						  bit
    
AS
IF EXISTS(select StockGroupNo from MStockGroup
          where
          StockGroupNo = @StockGroupNo)
     BEGIN
       --Update existing row
       UPDATE MStockGroup
       SET
          StockGroupName = @StockGroupName,
		  LanguageName = @LanguageName,
          ControlGroup = @ControlGroup,
		  ControlSubGroup = @ControlSubGroup,
          IsActive = @IsActive,
          UserId = @UserId,
          UserDate = @UserDate,
		  CompanyNo=@CompanyNo,
          MfgCompNo=@MfgCompNo, 
          ModifiedBy =isnull(ModifiedBy,'') + cast(@UserID as varchar)+'@'+ CONVERT(VARCHAR(10), GETDATE(), 105),
          StatusNo=2,
		  Margin=@Margin,
		  IsApplyToAll =@IsApplyToAll						 
       WHERE
          StockGroupNo = @StockGroupNo

Update MStockItems Set ShowItemName=@StockGroupName+' '+ ItemName,ShowItemName_Lang=@LanguageName+' '+  LangFullDesc  From 
MStockItems Inner Join MStockGroup On MStockItems.GroupNo=MStockGroup.StockGroupNo
Where MStockGroup.StockGroupNo = @StockGroupNo

     END
ELSE
     BEGIN
       --Insert new row
       Declare @Id numeric
       SELECT @Id=IsNull(Max(StockGroupNo),0) From MStockGroup
       DBCC CHECKIDENT('MStockGroup', RESEED, @Id)
       INSERT INTO MStockGroup(
          
          StockGroupName,
		  LanguageName,
          ControlGroup,
		  ControlSubGroup,
          IsActive,
          UserId,
          UserDate,
		  CompanyNo,
          StatusNo,
          MfgCompNo,
		  Margin,
		  IsApplyToAll
)
       VALUES(
         
          @StockGroupName,
		  @LanguageName,
          @ControlGroup,
		  @ControlSubGroup,
          @IsActive,
          @UserId,
          @UserDate,
		  @CompanyNo,
          1,
          @MfgCompNo,
		  @Margin,
		  @IsApplyToAll
)

END

Go
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/ 

Update MStockItems Set ShowItemName=MStockGroup.StockGroupName+' '+ ItemName,ShowItemName_Lang=MStockGroup.LanguageName+' '+  LangFullDesc  From 
MStockItems Inner Join MStockGroup On MStockItems.GroupNo=MStockGroup.StockGroupNo

Go
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/ 

Update MSetting Set AppVersion='0rmaDPz94O8Xjqks4VHmyg=='

Go

/*---------------------------------------------------------------------------------------------------------------------------------------------------*/ 