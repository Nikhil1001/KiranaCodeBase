USE [Kirana0001]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetLedgerBookDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetLedgerBookDetails]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Create Procedure [dbo].[GetLedgerBookDetails]
@LedgerNo Varchar(Max),
@FromDate datetime,
@ToDate datetime

AS 

SELECT     TVoucherEntry.VoucherDate, TVoucherEntry.VoucherUserNo, MLedger.LedgerName, TVoucherEntry.Reference
			,case when TVoucherChqCreditDetails.ChequeNo = '' then null else TVoucherChqCreditDetails.ChequeNo end ChequeNo
			, TVoucherEntry.Remark
			, TVoucherDetails.Debit, TVoucherDetails.Credit, 
            TVoucherDetails.LedgerNo, TVoucherEntry.VoucherTypeCode, MVoucherType.VoucherTypeName
			,(TVoucherDetails.Credit- TVoucherDetails.Debit) as Diff,TVoucherEntry.PkVoucherno
FROM         MLedger 
			INNER JOIN TVoucherDetails ON MLedger.LedgerNo = TVoucherDetails.LedgerNo 
			INNER JOIN TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo 
			INNER JOIN MVoucherType ON TVoucherEntry.VoucherTypeCode = MVoucherType.VoucherTypeCode
			left join TVoucherChqCreditDetails on TVoucherEntry.PkVoucherNo  = TVoucherChqCreditDetails.FKVoucherNo
WHERE     (TVoucherDetails.LedgerNo IN(Select value From fn_Split(@LedgerNo,',') ) ) 
			AND (TVoucherEntry.VoucherDate >= @FromDate) 
			AND (TVoucherEntry.VoucherDate <= @ToDate)
			And TVoucherEntry.IsCancel='False'


UNION All
SELECT     @FromDate AS VoucherDate, null AS VoucherUserNo, MLedger.LedgerName, '' AS Reference,null asChequeNo, ''  AS Remark, null AS Debit,
    null AS Credit, 
                      MLedger.LedgerNo AS LedgerNo, 0 AS VoucherTypeCode, 'Op Bal.' AS VoucherTypeName, SUM(TVoucherDetails.Credit) - SUM(TVoucherDetails.Debit) AS Diff,TVoucherEntry.PkVoucherno
FROM         
                      TVoucherDetails INNER JOIN
                      TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo AND (TVoucherEntry.VoucherDate < @FromDate) RIGHT OUTER JOIN
MLedger  ON MLedger.LedgerNo = TVoucherDetails.LedgerNo 
WHERE     (MLedger.LedgerNo in(Select value From fn_Split(@LedgerNo,',')))
And TVoucherEntry.IsCancel='False'
GROUP BY MLedger.LedgerNo, MLedger.LedgerName,TVoucherEntry.PkVoucherno
ORDER BY LedgerName, LedgerNo, VoucherDate,TVoucherEntry.PkVoucherno

GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetLedgerBookDetails_Monthly]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetLedgerBookDetails_Monthly]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Create Procedure [dbo].[GetLedgerBookDetails_Monthly]
@LedgerNo Varchar(Max),
@FromDate datetime,
@ToDate datetime

AS 

;with x as
(
SELECT     convert(varchar(3),datename(mm,TVoucherEntry.VoucherDate))+' - '+datename(year,TVoucherEntry.VoucherDate) as VoucherDate, TVoucherEntry.VoucherUserNo, MLedger.LedgerName, TVoucherEntry.Reference, TVoucherEntry.Remark, TVoucherDetails.Debit, TVoucherDetails.Credit, 
                       TVoucherDetails.LedgerNo, TVoucherEntry.VoucherTypeCode, MVoucherType.VoucherTypeName,(TVoucherDetails.Credit- TVoucherDetails.Debit) as Diff
FROM         MLedger INNER JOIN
                      TVoucherDetails ON MLedger.LedgerNo = TVoucherDetails.LedgerNo INNER JOIN
                      TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN
                      MVoucherType ON TVoucherEntry.VoucherTypeCode = MVoucherType.VoucherTypeCode
WHERE     (TVoucherDetails.LedgerNo IN(Select value From fn_Split(@LedgerNo,',') ) ) AND (TVoucherEntry.VoucherDate >= @FromDate) AND (TVoucherEntry.VoucherDate <= @ToDate)
And TVoucherEntry.IsCancel='False'
)

select x.VoucherDate, x.LedgerNo, x.LedgerName, null Remark,sum(x.Debit) as Debit,sum(x.Credit) as Credit, sum(Diff) Diff --,temp.opening
from x 
group by x.VoucherDate,x.LedgerNo, x.LedgerName
Union all
SELECT     NULL AS VoucherDate, MLedger.LedgerNo AS LedgerNo, MLedger.LedgerName, 'Op Bal. AS On - '  AS Remark, null AS Debit,
    null AS Credit, SUM(TVoucherDetails.Credit) - SUM(TVoucherDetails.Debit) AS Diff
FROM         
                      TVoucherDetails INNER JOIN
                      TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo AND (TVoucherEntry.VoucherDate < @FromDate) RIGHT OUTER JOIN
MLedger  ON MLedger.LedgerNo = TVoucherDetails.LedgerNo 
WHERE     (MLedger.LedgerNo in(Select value From fn_Split(@LedgerNo,',')))
And TVoucherEntry.IsCancel='False'
GROUP BY MLedger.LedgerNo, MLedger.LedgerName
ORDER BY LedgerName, LedgerNo, VoucherDate

GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetLedgerBookDetails_Summary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetLedgerBookDetails_Summary]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Create Procedure [dbo].[GetLedgerBookDetails_Summary]
@LedgerNo Varchar(Max),
@FromDate datetime,
@ToDate datetime

AS 

;with x as
(
SELECT     convert(varchar(3),datename(mm,TVoucherEntry.VoucherDate))+' - '+datename(year,TVoucherEntry.VoucherDate) as VoucherDate, TVoucherEntry.VoucherUserNo, MLedger.LedgerName, TVoucherEntry.Reference, TVoucherEntry.Remark, TVoucherDetails.Debit, TVoucherDetails.Credit, 
                       TVoucherDetails.LedgerNo, TVoucherEntry.VoucherTypeCode, MVoucherType.VoucherTypeName,(TVoucherDetails.Credit- TVoucherDetails.Debit) as Diff
FROM         MLedger INNER JOIN
                      TVoucherDetails ON MLedger.LedgerNo = TVoucherDetails.LedgerNo INNER JOIN
                      TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN
                      MVoucherType ON TVoucherEntry.VoucherTypeCode = MVoucherType.VoucherTypeCode
WHERE     (TVoucherDetails.LedgerNo IN(Select value From fn_Split(@LedgerNo,',') ) ) AND (TVoucherEntry.VoucherDate >= @FromDate) AND (TVoucherEntry.VoucherDate <= @ToDate)
And TVoucherEntry.IsCancel='False'
)

select  x.LedgerNo, x.LedgerName, null Remark,sum(x.Debit) as Debit,sum(x.Credit) as Credit
,sum(Diff) as Diff
--,case when sum(Diff)>0 then sum(Diff) else 0.0 end  as Cr_Diff
 --,temp.opening
from x 
group by  x.LedgerNo, x.LedgerName

GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LedgerBookBillWise]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LedgerBookBillWise]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/

Create PROCEDURE [dbo].[LedgerBookBillWise]
@FromDate          datetime,
@ToDate            datetime,
@VchType           int,
@CompanyNo		   varchar(50),
@PartyNo		   varchar(Max)
AS

Declare @StrQry varchar(max)
declare @TempTable Table(PayTypeNo numeric(18), VoucherDate datetime, BillNo varchar(250), VoucherTypeCode numeric(18), BillAmount numeric(18,2), PayType varchar(50), recvDate datetime, recvAmount numeric(18,2), cheaqueNo varchar(50), RefNo numeric(18), TypeofRef varchar(25), LedgerName varchar(250), LedgerNo numeric(18), OpenBal numeric(18,2), DiscAmt numeric(18,2))
declare @TempOpTable Table(OpBal numeric(18,2), Ledger numeric(18))
declare @VchDate datetime, @ledgNo numeric(18)
declare @RefNumber numeric(18), @opB numeric(18,2)
declare @strOp varchar(max),@VType varchar(10)



if(@VchType=15)
	set @VType='15,30,12'
else 
	set @VType='9,31'

set @StrQry = 'SELECT TVoucherEntry.PayTypeNo, TVoucherEntry.VoucherDate, TVoucherEntry.VoucherUserNo, TVoucherEntry.VoucherTypeCode, '+
                      ' (SELECT     CASE WHEN TVoucherDetails.Debit <> 0 THEN TVoucherDetails.Debit ELSE TVoucherDetails.Credit END AS Expr1) AS billamount, '+
                      ' MPayType.PayTypeName, TVoucherEntry_1.VoucherDate AS recvDate, '+
                      ' TVoucherRefDetails.Amount  AS recvamount, ' +
                      ' (Select Case When TCC.ChequeNo<>'''' Then TCC.ChequeNo Else TCC.CreditCardNo End From TVoucherChqCreditDetails TCC Where TCC.FkVoucherNo=TVoucherEntry.PKVoucherno) , ' + 
                      ' TVoucherRefDetails.RefNo, TVoucherRefDetails.TypeOfRef, MLedger.LedgerName,MLedger.LedgerNo, 0 as OpenBal, '+
					  ' 0 As DiscAmt ' +
				' FROM TVoucherEntry INNER JOIN '+
                      ' TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo INNER JOIN '+
                      ' MPayType ON TVoucherEntry.PayTypeNo = MPayType.PKPayTypeNo INNER JOIN '+
                      ' TVoucherRefDetails ON TVoucherRefDetails.FkVoucherTrnNo = TVoucherDetails.PkVoucherTrnNo LEFT OUTER JOIN '+
                      ' TVoucherDetails AS TVoucherDetails_1 ON TVoucherDetails_1.PkVoucherTrnNo = TVoucherRefDetails.FkVoucherTrnNo LEFT OUTER JOIN '+
                      ' TVoucherEntry AS TVoucherEntry_1 ON TVoucherDetails_1.FkVoucherNo = TVoucherEntry_1.PkVoucherNo '+
					  ' INNER JOIN MLedger on Mledger.LedgerNo = TVoucherDetails.LedgerNo'+
				' WHERE TVoucherEntry.IsCancel=0 And (TVoucherEntry.VoucherDate between'''+cast(@FromDate as varchar)+''' and '''+cast(@ToDate as varchar)+''') AND TVoucherEntry.VoucherTypeCode in('+ @VType +') and '+
				' (TVoucherDetails.VoucherSrNo = 1) AND (TVoucherDetails.LedgerNo IN ('+cast(@PartyNo as varchar(Max))+'))'+ 
				' ORDER BY TVoucherEntry.VoucherDate, TVoucherEntry.VoucherUserNo, TVoucherRefDetails.RefNo, TVoucherEntry_1.VoucherDate, TVoucherEntry_1.VoucherUserNo '

--select @StrQry
insert into @TempTable Exec(@StrQry)

declare cur cursor for select VoucherDate,RefNo from @TempTable where TypeofRef='3'
Open cur
Fetch Next From cur Into @VchDate,@RefNumber
While(@@Fetch_Status=0)
BEGIN
	update @TempTable set VoucherDate=@VchDate where RefNo=@RefNumber and TypeofRef='2'
	Fetch Next From cur Into @VchDate,@RefNumber
END
close cur
deallocate cur

update @TempTable set recvDate=NULL,recvAmount=0 where TypeofRef='3'
update @TempTable set cheaqueNo=NULL where PayTypeNo<>4 ANd cheaqueNo=''
update @TempTable set BillAmount=NULL where TypeofRef='2'
update @TempTable set cheaqueNo=BillNo, PayType='Sales Return' where TypeofRef='2' and VoucherTypeCode=12
update @TempTable set BillNo=NULL where TypeofRef='2' 
update @TempTable set BillNo='Old Bal.' where TypeofRef='5' 
select * from @TempTable order by VoucherDate,RefNo,recvDate,BillNo

GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/

Create FUNCTION [dbo].[fn_Split](@text varchar(MAX), @delimiter varchar(20) = ' ')
RETURNS @Strings TABLE
(   
  position int IDENTITY PRIMARY KEY,
  value varchar(8000)  
)
AS
BEGIN

DECLARE @index int
SET @index = -1

WHILE (LEN(@text) > 0)
  BEGIN 
    SET @index = CHARINDEX(@delimiter , @text) 
    IF (@index = 0) AND (LEN(@text) > 0) 
      BEGIN  
        INSERT INTO @Strings VALUES (@text)
          BREAK 
      END 
    IF (@index > 1) 
      BEGIN  
        INSERT INTO @Strings VALUES (LEFT(@text, @index - 1))  
        SET @text = RIGHT(@text, (LEN(@text) - @index)) 
      END 
    ELSE
      SET @text = RIGHT(@text, (LEN(@text) - @index))
    END
  RETURN
END

GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/

