Use [Kirana0001]
GO
/*-----------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetStockSummaryMRPwiseDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetStockSummaryMRPwiseDetails]
GO 
/*-----------------------------------------------------------------------------------------------------------------------------------------------------*/
Create PROCEDURE [dbo].[GetStockSummaryMRPwiseDetails] --1,'2017-01-01','2019-02-26','37,38,39,40,2479','ASaleRate',1,0
--Declare
@CompNo		           numeric(18),
@FromDate	           datetime,
@ToDate		           datetime,
@ItemNo				   varchar(max),
@RateType			   varchar(100),
@Type				   int,
@IsViewStockZero	   int,
@StockType			   int

AS


Declare @TempItem Table(ItemNo numeric(18,0),MRP numeric(18,4),Op numeric(18,2),Inward numeric(18,2),Outward numeric(18,2),FkStockBarcodeNo int
, PkSrNo int, TVoucherDate Datetime, TVoucherNo int, TVoucherType varchar(50))


if(@IsViewStockZero=1)
Begin
				
insert into @TempItem (ItemNo,MRP,Op,Inward, Outward,FkStockBarcodeNo, PkSrNo, TVoucherDate, TVoucherNo, TVoucherType)

Select ItemNo,MRP,Sum(Op)As Op, Inward, Outward, FkStockBarcodeNo, PkSrNo,VoucherDate,VoucherUserNo, VoucherTypeName
					From (

								--Op Stock Sale
								SELECT Distinct   TStock.ItemNo, MRateSetting.MRP
								, SUM( case when TStock.TrnCode=1 then TStock.BilledQuantity else TStock.BilledQuantity * -1 end) AS Op
								, 0 As Inward,0 AS Outward,TSTock.FkStockBarcodeNo,MRateSetting.PkSrNo, null as VoucherDate, null as VoucherUserNo,
 '' as VoucherTypeName
								FROM    TStock INNER JOIN
										TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN
										MRateSetting ON TStock.FkRateSettingNo = MRateSetting.PkSrNo INNER Join
								fn_Split(@ItemNo,',') as fn on TStock.ItemNo=cast(Fn.Value as numeric(18))
								Where (TVoucherEntry.VoucherDate < @FromDate) and TVoucherEntry.IsCancel='false'
								Group By TStock.ItemNo,MRateSetting.MRP,TStock.FkStockBarcodeNo,MRateSetting.PkSrNo

								Union All


								SELECT Distinct TStock.ItemNo, MRateSetting.MRP,  0 AS Op
								, case when TStock.TrnCode = 1 then TStock.BilledQuantity else 0 end AS Inward
								, case when TStock.TrnCode = 2 then TStock.BilledQuantity else 0 end AS Outward
								, TSTock.FkStockBarcodeNo,MRateSetting.PkSrNo, TVoucherEntry.VoucherTime as VoucherDate, TVoucherEntry.VoucherUserNo, MVoucherType.VoucherTypeName
								FROM         TStock INNER JOIN
										  TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN
										  MRateSetting ON TStock.FkRateSettingNo = MRateSetting.PkSrNo INNER Join
								fn_Split(@ItemNo,',') as fn on TStock.ItemNo=cast(Fn.Value as numeric(18))
								inner join  mVoucherTYpe on TVoucherEntry.VoucherTypeCode = MVoucherType.VoucherTypeCode
								Where (TVoucherEntry.VoucherDate >= @FromDate) AND 
									(TVoucherEntry.VoucherDate <= @ToDate) And TVoucherEntry.IsCancel='false'
										
					) As A 
					Group by A.ItemNo,A.MRP,A.FkStockBarcodeNo, Inward, Outward, A.PkSrNo, A.VoucherDate, A.VoucherUserNo, A.VoucherTypeName		
					having Sum(A.Op + abs(A.Inward) - abs(A.Outward))<>0
End
Else
Begin
insert into @TempItem (ItemNo,MRP,Op,Inward, Outward,FkStockBarcodeNo, PkSrNo, TVoucherDate, TVoucherNo, TVoucherType)

Select ItemNo,MRP,Sum(Op)As Op, Inward, Outward, FkStockBarcodeNo, PkSrNo, VoucherDate, VoucherUserNo, VoucherTypeName
					From (

							--Op Stock Sale
							SELECT   TStock.ItemNo, MRateSetting.MRP
							, SUM( case when TStock.TrnCode=1 then TStock.BilledQuantity else TStock.BilledQuantity * -1 end) AS Op
							, 0 As Inward,0 AS Outward,TSTock.FkStockBarcodeNo,MRateSetting.PkSrNo, null as VoucherDate, null as VoucherUserNo, '' as VoucherTypeName
							FROM         TStock INNER JOIN
										  TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN
										  MRateSetting ON TStock.FkRateSettingNo = MRateSetting.PkSrNo INNER Join
							fn_Split(@ItemNo,',') as fn on TStock.ItemNo=cast(Fn.Value as numeric(18))
							Where (TVoucherEntry.VoucherDate < @FromDate) And TVoucherEntry.IsCancel='false'
							Group By TStock.ItemNo,MRateSetting.MRP,TStock.FkStockBarcodeNo,MRateSetting.PkSrNo 

							Union All

							SELECT TStock.ItemNo, MRateSetting.MRP,   0 as Op
							, case when TStock.TrnCode = 1 then TStock.BilledQuantity else 0 end AS Inward
							, case when TStock.TrnCode = 2 then TStock.BilledQuantity else 0 end AS Outward
							, TSTock.FkStockBarcodeNo,MRateSetting.PkSrNo, TVoucherEntry.VoucherTime as VoucherDate, TVoucherEntry.VoucherUserNo, MVoucherType.VoucherTypeName
							FROM         TStock INNER JOIN
										  TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN
										  MRateSetting ON TStock.FkRateSettingNo = MRateSetting.PkSrNo INNER Join
							fn_Split(@ItemNo,',') as fn on TStock.ItemNo=cast(Fn.Value as numeric(18))
							inner join  mVoucherTYpe on TVoucherEntry.VoucherTypeCode = MVoucherType.VoucherTypeCode
							Where (TVoucherEntry.VoucherDate >= @FromDate) AND 
								(TVoucherEntry.VoucherDate <= @ToDate)  And TVoucherEntry.IsCancel='false'
							--Group By TStock.ItemNo,MRateSetting.MRP,TStock.MRP,TStock.StockType,TStock.GodownNo,TStock.FkStockBarcodeNo,MRateSetting.PkSrNo,datepart(month,TVoucherEntry.VoucherDate), datepart(year,TVoucherEntry.VoucherDate)

					) As A 
					Group by A.ItemNo,A.MRP,A.FkStockBarcodeNo, Inward, Outward, A.PkSrNo, A.VoucherDate, A.VoucherUserNo, A.VoucherTypeName			

End



if(@Type=1) --Only Qty
Begin

			Select 
			T.ItemNo,
			M.ShowItemName  as ItemName ,
			B.Barcode, T.TVoucherDate,
			(Case When(@RateType='ASaleRate') then  R.ASaleRate else Case When (@RateType='BSaleRate') Then R.BSaleRate Else Case When  (@RateType='MRP') Then R.MRP Else R.PurRate End End End) As Rate
			,T.MRP,T.Op,T.Inward,T.Outward,((T.OP+T.Inward)-T.Outward) AS ClsQty
			, T.TVoucherNo, T.TVoucherType
			FROM         @TempItem AS T INNER JOIN
                      MStockItems AS M ON M.ItemNo = T.ItemNo  INNER JOIN
                      MStockBarcode AS B ON B.PkStockBarcodeNo = T.FkStockBarcodeNo INNER JOIN
                      MRateSetting AS R ON R.PkSrNo = T.PkSrNo AND T.MRP = R.MRP AND R.IsActive = 'True'		
			Order By M.ShowItemName,T.TVoucherDate	

End
Else if(@Type=2) -- only Amount
Begin


		Select ItemNo,ItemName ,
			   Barcode, TVoucherDate,
				Rate,MRP,Op*Rate as Op, Inward*Rate as Inward,Outward*Rate as Outward,ClsQty * Rate as ClsQty  
			, TVoucherNo, TVoucherType
			From 
			   (Select  T.ItemNo,M.ShowItemName as ItemName ,B.Barcode,(Case When(@RateType='ASaleRate') then  R.ASaleRate else Case When (@RateType='BSaleRate') Then R.BSaleRate Else Case When  (@RateType='MRP') Then R.MRP Else R.PurRate End End End) As Rate
								,T.MRP,T.Op,T.Inward,T.Outward,((T.OP+T.Inward)-T.Outward) AS ClsQty, T.TVoucherDate, T.TVoucherNo, T.TVoucherType
						FROM         @TempItem AS T INNER JOIN
                      MStockItems AS M ON M.ItemNo = T.ItemNo  INNER JOIN
                      MStockBarcode AS B ON B.PkStockBarcodeNo = T.FkStockBarcodeNo INNER JOIN
                      MRateSetting AS R ON R.PkSrNo = T.PkSrNo AND T.MRP = R.MRP AND R.IsActive = 'True'
				)
				As G  Order By ItemName,TVoucherDate
End Else if(@Type=3) -- only Qty And Amount
Begin
Select ItemNo,ItemName ,
Barcode,TVoucherDate
,Rate,MRP,Op,Op*Rate as OpAmt, Inward,Inward*Rate as InwardAmt,Outward,Outward*Rate as OutwardAmt,ClsQty ,ClsQty * Rate as ClsQtyAmt 
, TVoucherNo, TVoucherType
From
 (	Select  T.ItemNo,M.ShowItemName  as ItemName ,B.Barcode,(Case When(@RateType='ASaleRate') then  R.ASaleRate else Case When (@RateType='BSaleRate') Then R.BSaleRate Else Case When  (@RateType='MRP') Then R.MRP Else R.PurRate End End End) As Rate
		,T.MRP,T.Op,T.Inward,T.Outward,((T.OP+T.Inward)-T.Outward) AS ClsQty
		,T.TVoucherDate, T.TVoucherNo, T.TVoucherType
		FROM         @TempItem AS T INNER JOIN
                      MStockItems AS M ON M.ItemNo = T.ItemNo  INNER JOIN
                      MStockBarcode AS B ON B.PkStockBarcodeNo = T.FkStockBarcodeNo INNER JOIN
                      MRateSetting AS R ON R.PkSrNo = T.PkSrNo AND T.MRP = R.MRP AND R.IsActive = 'True'
)As G Order By ItemName,TVoucherDate

End 


GO
/*-----------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetStockSummaryMRPwiseX]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetStockSummaryMRPwiseX]
GO 
/*-----------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[GetStockSummaryMRPwiseX] --1,'2018-02-01','2018-02-26','174367,177054,68280,175222','ASaleRate',1,0,1--,2
--Declare
@CompNo		           numeric(18),
@FromDate	           datetime,
@ToDate		           datetime,
@ItemNo				   varchar(max),
@RateType			   varchar(100),
@Type				   int,
@IsViewStockZero	   int,
@StockType			   int

AS


Declare @TempItem Table(ItemNo numeric(18,0),MRP numeric(18,4),Op numeric(18,2),Inward numeric(18,2),Outward numeric(18,2),FkStockBarcodeNo int, PkSrNo int, month_ int, year_ int)


if(@IsViewStockZero=1)
Begin
				
insert into @TempItem (ItemNo,MRP,Op,Inward, Outward,FkStockBarcodeNo, PkSrNo, month_, year_)

Select ItemNo,MRP,Sum(Op)As Op,Sum(Inward) As Inward,Sum(Outward) As Outward,FkStockBarcodeNo, PkSrNo,month,year 
					From (

							--Op Stock Sale
							SELECT Distinct   TStock.ItemNo, MRateSetting.MRP
							, SUM( case when TStock.TrnCode=1 then TStock.BilledQuantity else TStock.BilledQuantity * -1 end) AS Op
							,0 As Inward,0 AS Outward, TSTock.FkStockBarcodeNo,	MRateSetting.PkSrNo, null as month,null as year
							FROM         TStock INNER JOIN
												  TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN
												  MRateSetting ON TStock.FkRateSettingNo = MRateSetting.PkSrNo INNER Join
							fn_Split(@ItemNo,',') as fn on TStock.ItemNo=cast(Fn.Value as numeric(18))
							Where (TVoucherEntry.VoucherDate < @FromDate) and TVoucherEntry.IsCancel='false'
							Group By TStock.ItemNo,MRateSetting.MRP,TStock.FkStockBarcodeNo,MRateSetting.PkSrNo--,datepart(month,'1900-01-01'), datepart(year,'1900-01-01')

							Union All

							SELECT Distinct TStock.ItemNo, MRateSetting.MRP,  0 AS Op
							, sum(case when TStock.TrnCode = 1 then TStock.BilledQuantity else 0 end) AS Inward
							, sum(case when TStock.TrnCode = 2 then TStock.BilledQuantity else 0 end) AS Outward
							, TSTock.FkStockBarcodeNo,MRateSetting.PkSrNo
							, datepart(month,TVoucherEntry.VoucherDate) as month,datepart(year,TVoucherEntry.VoucherDate) as year
							FROM         TStock INNER JOIN
												  TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN
												  MRateSetting ON TStock.FkRateSettingNo = MRateSetting.PkSrNo INNER Join
							fn_Split(@ItemNo,',') as fn on TStock.ItemNo=cast(Fn.Value as numeric(18))
							Where (TVoucherEntry.VoucherDate >= @FromDate) AND 
								(TVoucherEntry.VoucherDate <= @ToDate) And TVoucherEntry.IsCancel='false'
							Group By TStock.ItemNo,MRateSetting.MRP,TStock.FkStockBarcodeNo,MRateSetting.PkSrNo,datepart(month,TVoucherEntry.VoucherDate), datepart(year,TVoucherEntry.VoucherDate)

					) As A 
					Group by A.ItemNo,A.MRP,A.FkStockBarcodeNo, A.PkSrNo, month, year
					having Sum(A.Op + abs(A.Inward) - abs(A.Outward))<>0
End
Else
Begin
insert into @TempItem (ItemNo,MRP,Op,Inward, Outward,FkStockBarcodeNo, PkSrNo, month_, year_)

Select ItemNo,MRP,Sum(Op)As Op,Sum(Inward) As Inward,Sum(Outward) As Outward,FkStockBarcodeNo, PkSrNo, month, year
					From (

							SELECT TStock.ItemNo, MRateSetting.MRP
							, SUM( case when TStock.TrnCode=1 then TStock.BilledQuantity else TStock.BilledQuantity * -1 end) AS Op
							, 0 As Inward,0 AS Outward,TSTock.FkStockBarcodeNo,MRateSetting.PkSrNo, null as month,null as year
							FROM         TStock INNER JOIN
												  TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN
												  MRateSetting ON TStock.FkRateSettingNo = MRateSetting.PkSrNo INNER Join
							fn_Split(@ItemNo,',') as fn on TStock.ItemNo=cast(Fn.Value as numeric(18))
							Where (TVoucherEntry.VoucherDate < @FromDate) And TVoucherEntry.IsCancel='false'
							Group By TStock.ItemNo,MRateSetting.MRP,TStock.FkStockBarcodeNo,MRateSetting.PkSrNo--,datepart(month,'1900-01-01'), datepart(year,'1900-01-01')

							Union All

							SELECT TStock.ItemNo, MRateSetting.MRP,   0 as Op
							,sum(case when TStock.TrnCode = 1 then TStock.BilledQuantity else 0 end) AS Inward
							,sum(case when TStock.TrnCode = 2 then TStock.BilledQuantity else 0 end) AS Outward
							,TSTock.FkStockBarcodeNo,MRateSetting.PkSrNo, datepart(month,TVoucherEntry.VoucherDate) as month,datepart(year,TVoucherEntry.VoucherDate) as year
							FROM         TStock INNER JOIN
												  TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN
												  MRateSetting ON TStock.FkRateSettingNo = MRateSetting.PkSrNo INNER Join
							fn_Split(@ItemNo,',') as fn on TStock.ItemNo=cast(Fn.Value as numeric(18))
							Where (TVoucherEntry.VoucherDate >= @FromDate) AND 
								(TVoucherEntry.VoucherDate <= @ToDate) And TVoucherEntry.IsCancel='false'
							Group By TStock.ItemNo,MRateSetting.MRP,TStock.FkStockBarcodeNo,MRateSetting.PkSrNo,datepart(month,TVoucherEntry.VoucherDate), datepart(year,TVoucherEntry.VoucherDate)

					) As A 
					Group by A.ItemNo,A.MRP,A.FkStockBarcodeNo, A.PkSrNo, month, year		

End



if(@Type=1) --Only Qty
Begin

			Select 
			T.ItemNo,
			M.ShowItemName  as ItemName ,
			B.Barcode
	,case when year_ is null then 'Op As On:' else  convert(varchar(3),DateName( month , DateAdd( month , month_ , 0 ) - 1 ) )+ ' - ' + right(convert(varchar,year_),2) end as Month,
			(Case When(@RateType='ASaleRate') then  R.ASaleRate else Case When (@RateType='BSaleRate') Then R.BSaleRate Else Case When  (@RateType='MRP') Then R.MRP Else R.PurRate End End End) As Rate
			,T.MRP
,T.Op,
T.Inward
--case when year_ <> '1900' then T.Inward else  T.Op end as Inward
,T.Outward,((T.OP+T.Inward)-T.Outward) AS ClsQty
			FROM         @TempItem AS T INNER JOIN
                      MStockItems AS M ON M.ItemNo = T.ItemNo  INNER JOIN
                      MStockBarcode AS B ON B.PkStockBarcodeNo = T.FkStockBarcodeNo INNER JOIN
                      MRateSetting AS R ON R.PkSrNo = T.PkSrNo AND T.MRP = R.MRP AND R.IsActive = 'True'					
			Order By M.ShowItemName,T.month_,T.year_			 

End
Else if(@Type=2) -- only Amount
Begin


		Select ItemNo,ItemName ,
			   Barcode	,case when year_ is null then 'Op As On:' else  convert(varchar(3),DateName( month , DateAdd( month , month_ , 0 ) - 1 ) )+ ' - ' + right(convert(varchar,year_),2) end as Month,
				Rate,MRP,Op*Rate as Op, Inward*Rate as Inward,Outward*Rate as Outward,ClsQty * Rate as ClsQty  From 
			   (Select  T.ItemNo,M.ShowItemName as ItemName ,B.Barcode,(Case When(@RateType='ASaleRate') then  R.ASaleRate else Case When (@RateType='BSaleRate') Then R.BSaleRate Else Case When  (@RateType='MRP') Then R.MRP Else R.PurRate End End End) As Rate
								,T.MRP,T.Op,T.Inward,T.Outward,((T.OP+T.Inward)-T.Outward) AS ClsQty
						,T.month_,T.year_
						FROM         @TempItem AS T INNER JOIN
                      MStockItems AS M ON M.ItemNo = T.ItemNo  INNER JOIN
                      MStockBarcode AS B ON B.PkStockBarcodeNo = T.FkStockBarcodeNo INNER JOIN
                      MRateSetting AS R ON R.PkSrNo = T.PkSrNo AND T.MRP = R.MRP AND R.IsActive = 'True'
					)
						As G  Order By ItemName,month_,year_
End Else if(@Type=3) -- only Qty And Amount
Begin
Select ItemNo,ItemName ,
Barcode	,case when year_ is null then 'Op As On:' else  convert(varchar(3),DateName( month , DateAdd( month , month_ , 0 ) - 1 ) )+ ' - ' + right(convert(varchar,year_),2) end as Month,
Rate,MRP,Op,Op*Rate as OpAmt, Inward,Inward*Rate as InwardAmt,Outward,Outward*Rate as OutwardAmt,ClsQty ,ClsQty * Rate as ClsQtyAmt From
 (	Select  T.ItemNo,M.ShowItemName as ItemName ,B.Barcode,(Case When(@RateType='ASaleRate') then  R.ASaleRate else Case When (@RateType='BSaleRate') Then R.BSaleRate Else Case When  (@RateType='MRP') Then R.MRP Else R.PurRate End End End) As Rate
		,T.MRP,T.Op,T.Inward,T.Outward,((T.OP+T.Inward)-T.Outward) AS ClsQty
		,T.month_,T.year_
		FROM         @TempItem AS T INNER JOIN
                      MStockItems AS M ON M.ItemNo = T.ItemNo  INNER JOIN
                      MStockBarcode AS B ON B.PkStockBarcodeNo = T.FkStockBarcodeNo INNER JOIN
                      MRateSetting AS R ON R.PkSrNo = T.PkSrNo AND T.MRP = R.MRP AND R.IsActive = 'True'
)As G Order By ItemName,month_,year_

End 

GO
/*-----------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetLedgerBookDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetLedgerBookDetails]
GO 
/*-----------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[GetLedgerBookDetails] 
@LedgerNo Varchar(Max),
@FromDate datetime,
@ToDate datetime
AS 
Declare  @StrQry  varchar(max)
set @StrQry ='SELECT     TVoucherEntry.VoucherDate, TVoucherEntry.VoucherUserNo, MLedger.LedgerName, TVoucherEntry.Reference
			,case when TVoucherChqCreditDetails.ChequeNo = '''' then null else TVoucherChqCreditDetails.ChequeNo end ChequeNo
			,TVoucherEntry.Remark  as Remark
			, TVoucherDetails.Debit, TVoucherDetails.Credit, 
            TVoucherDetails.LedgerNo, TVoucherEntry.VoucherTypeCode, MVoucherType.VoucherTypeName
			,(TVoucherDetails.Credit- TVoucherDetails.Debit) as Diff
FROM         MLedger 
			INNER JOIN TVoucherDetails ON MLedger.LedgerNo = TVoucherDetails.LedgerNo 
			INNER JOIN TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo 
			INNER JOIN MVoucherType ON TVoucherEntry.VoucherTypeCode = MVoucherType.VoucherTypeCode
			left join TVoucherChqCreditDetails on TVoucherEntry.PkVoucherNo  = TVoucherChqCreditDetails.FKVoucherNo
WHERE     (TVoucherDetails.LedgerNo IN('+@LedgerNo+') )  AND
			 (TVoucherEntry.VoucherDate >= '''+cast(@FromDate as varchar)+''') 
			AND (TVoucherEntry.VoucherDate <= '''+cast(@ToDate as varchar)+''')
			And TVoucherEntry.IsCancel=''False''
UNION All
SELECT     '''+cast(@FromDate as varchar)+''' AS VoucherDate, null AS VoucherUserNo, MLedger.LedgerName, '''' AS Reference,null asChequeNo, ''''  AS Remark, null AS Debit,
    null AS Credit, 
                      MLedger.LedgerNo AS LedgerNo, 0 AS VoucherTypeCode, ''Op Bal.'' AS VoucherTypeName, SUM(TVoucherDetails.Credit) - SUM(TVoucherDetails.Debit) AS Diff
FROM         
                    TVoucherDetails INNER JOIN
                    TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo AND (TVoucherEntry.VoucherDate < '''+cast(@FromDate as varchar)+''') 
					RIGHT OUTER JOIN MLedger  ON MLedger.LedgerNo = TVoucherDetails.LedgerNo 
WHERE     (MLedger.LedgerNo in('+@LedgerNo+')) And
 TVoucherEntry.IsCancel=''False''
GROUP BY MLedger.LedgerNo, MLedger.LedgerName
ORDER BY LedgerName, LedgerNo, VoucherDate'


exec(@StrQry)
return
GO
/*-----------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetStockSummaryMRPwise]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetStockSummaryMRPwise]
GO 
/*-----------------------------------------------------------------------------------------------------------------------------------------------------*/
Create Procedure [dbo].[GetStockSummaryMRPwise]-- 1,'2018-01-01','2018-02-02','180082,180083,180084','ASaleRate',2,0,1
--Declare
@CompNo		           numeric(18),
@FromDate	           datetime,
@ToDate		           datetime,
@ItemNo				   varchar(max),
@RateType			   varchar(100),
@Type				   int,
@IsViewStockZero	   int,
@StockType			   int

AS


Declare @TempItem Table(ItemNo numeric(18,0),MRP numeric(18,4),Op numeric(18,2),Inward numeric(18,2),Outward numeric(18,2),FkStockBarcodeNo int, PkSrNo int)

if(@IsViewStockZero=1)
Begin
				
insert into @TempItem (ItemNo,MRP,Op,Inward, Outward,FkStockBarcodeNo, PkSrNo)

Select ItemNo,MRP,Sum(Op)As Op,Sum(Inward) As Inward,Sum(Outward) As Outward,FkStockBarcodeNo, PkSrNo
					From (

										--Op Stock Sale
										SELECT Distinct   TStock.ItemNo, MRateSetting.MRP, SUM(TStock.BilledQuantity) AS Op,0 As Inward,0 AS Outward,TSTock.FkStockBarcodeNo,MRateSetting.PkSrNo
										FROM         TStock INNER JOIN
															  TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN
															  MRateSetting ON TStock.FkRateSettingNo = MRateSetting.PkSrNo INNER Join
										fn_Split(@ItemNo,',') as fn on TStock.ItemNo=cast(Fn.Value as numeric(18))
										Where (TVoucherEntry.VoucherDate < @FromDate) And TStock.TrnCode=1 and TVoucherEntry.IsCancel='false'
										Group By TStock.ItemNo,MRateSetting.MRP,TStock.FkStockBarcodeNo,MRateSetting.PkSrNo

										Union All

										--Op Stock Purchase

										SELECT Distinct TStock.ItemNo, MRateSetting.MRP, SUM(TStock.BilledQuantity) *-1 AS Op,0 As Inward,0 AS Outward,TSTock.FkStockBarcodeNo,MRateSetting.PkSrNo
										FROM         TStock INNER JOIN
															  TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN
															  MRateSetting ON TStock.FkRateSettingNo = MRateSetting.PkSrNo INNER Join
										fn_Split(@ItemNo,',') as fn on TStock.ItemNo=cast(Fn.Value as numeric(18))
										Where (TVoucherEntry.VoucherDate < @FromDate) And TStock.TrnCode=2 And TVoucherEntry.IsCancel='false'
										Group By TStock.ItemNo,MRateSetting.MRP,TStock.FkStockBarcodeNo,MRateSetting.PkSrNo

										Union All

										--In Stock Purchase

										SELECT Distinct TStock.ItemNo, MRateSetting.MRP,  0 AS Op,  SUM(TStock.BilledQuantity) AS Inward,0 As Outward,TSTock.FkStockBarcodeNo,MRateSetting.PkSrNo
										FROM         TStock INNER JOIN
															  TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN
															  MRateSetting ON TStock.FkRateSettingNo = MRateSetting.PkSrNo INNER Join
										fn_Split(@ItemNo,',') as fn on TStock.ItemNo=cast(Fn.Value as numeric(18))
										Where (TVoucherEntry.VoucherDate >= @FromDate) AND 
											(TVoucherEntry.VoucherDate <= @ToDate)  And TVoucherEntry.IsCancel='false'
										Group By TStock.ItemNo,MRateSetting.MRP,TStock.FkStockBarcodeNo,MRateSetting.PkSrNo


										Union All

										--Out Stock For  Sale

										SELECT Distinct TStock.ItemNo, MRateSetting.MRP,   0 as Op,0 AS Inward, SUM(TStock.BilledQuantity) AS Outward,TSTock.FkStockBarcodeNo,MRateSetting.PkSrNo
										FROM         TStock INNER JOIN
															  TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN
															  MRateSetting ON TStock.FkRateSettingNo = MRateSetting.PkSrNo INNER Join
										fn_Split(@ItemNo,',') as fn on TStock.ItemNo=cast(Fn.Value as numeric(18))
										Where (TVoucherEntry.VoucherDate >= @FromDate) AND 
											(TVoucherEntry.VoucherDate <= @ToDate)  And TStock.TrnCode=2 And TVoucherEntry.IsCancel='false'
										Group By TStock.ItemNo,MRateSetting.MRP,TStock.FkStockBarcodeNo,MRateSetting.PkSrNo
					) As A 
					Group by A.ItemNo,A.MRP,A.FkStockBarcodeNo, A.PkSrNo	
					having Sum(A.Op + abs(A.Inward) - abs(A.Outward))<>0
End
Else
Begin
insert into @TempItem (ItemNo,MRP,Op,Inward, Outward,FkStockBarcodeNo, PkSrNo)

Select ItemNo,MRP,Sum(Op)As Op,Sum(Inward) As Inward,Sum(Outward) As Outward,FkStockBarcodeNo, PkSrNo
					From (

										--Op Stock Sale
										SELECT   TStock.ItemNo, MRateSetting.MRP, SUM(TStock.BilledQuantity) AS Op,0 As Inward,0 AS Outward,TSTock.FkStockBarcodeNo,MRateSetting.PkSrNo
										FROM         TStock INNER JOIN
															  TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN
															  MRateSetting ON TStock.FkRateSettingNo = MRateSetting.PkSrNo INNER Join
										fn_Split(@ItemNo,',') as fn on TStock.ItemNo=cast(Fn.Value as numeric(18))
										Where (TVoucherEntry.VoucherDate < @FromDate) And TStock.TrnCode=1 And TVoucherEntry.IsCancel='false'
										Group By TStock.ItemNo,MRateSetting.MRP,TStock.FkStockBarcodeNo,MRateSetting.PkSrNo

										Union All

										--Op Stock Purchase

										SELECT TStock.ItemNo, MRateSetting.MRP, SUM(TStock.BilledQuantity) *-1 AS Op,0 As Inward,0 AS Outward,TSTock.FkStockBarcodeNo,MRateSetting.PkSrNo
										FROM         TStock INNER JOIN
															  TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN
															  MRateSetting ON TStock.FkRateSettingNo = MRateSetting.PkSrNo INNER Join
										fn_Split(@ItemNo,',') as fn on TStock.ItemNo=cast(Fn.Value as numeric(18))
										Where (TVoucherEntry.VoucherDate < @FromDate) And TStock.TrnCode=2 And TVoucherEntry.IsCancel='false'
										Group By TStock.ItemNo,MRateSetting.MRP,TStock.FkStockBarcodeNo,MRateSetting.PkSrNo

										Union All

										--In Stock Purchase

										SELECT TStock.ItemNo, MRateSetting.MRP,  0 AS Op,  SUM(TStock.BilledQuantity) AS Inward,0 As Outward,TSTock.FkStockBarcodeNo,MRateSetting.PkSrNo
										FROM         TStock INNER JOIN
															  TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN
															  MRateSetting ON TStock.FkRateSettingNo = MRateSetting.PkSrNo INNER Join
										fn_Split(@ItemNo,',') as fn on TStock.ItemNo=cast(Fn.Value as numeric(18))
										Where (TVoucherEntry.VoucherDate >= @FromDate) AND 
											(TVoucherEntry.VoucherDate <= @ToDate)  And TStock.TrnCode=1 And TVoucherEntry.IsCancel='false'
										Group By TStock.ItemNo,MRateSetting.MRP,TStock.FkStockBarcodeNo,MRateSetting.PkSrNo


										Union All

										--Out Stock For  Sale

										SELECT TStock.ItemNo, MRateSetting.MRP,   0 as Op,0 AS Inward, SUM(TStock.BilledQuantity) AS Outward,TSTock.FkStockBarcodeNo,MRateSetting.PkSrNo
										FROM         TStock INNER JOIN
															  TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN
															  MRateSetting ON TStock.FkRateSettingNo = MRateSetting.PkSrNo INNER Join
										fn_Split(@ItemNo,',') as fn on TStock.ItemNo=cast(Fn.Value as numeric(18))
										Where (TVoucherEntry.VoucherDate >= @FromDate) AND 
											(TVoucherEntry.VoucherDate <= @ToDate)  And TStock.TrnCode=2 And TVoucherEntry.IsCancel='false'
										Group By TStock.ItemNo,MRateSetting.MRP,TStock.FkStockBarcodeNo,MRateSetting.PkSrNo

					) As A 
					Group by A.ItemNo,A.MRP,A.FkStockBarcodeNo, A.PkSrNo					

End



if(@Type=1) --Only Qty
Begin

			Select  T.ItemNo,
			M.ShowItemName  as ItemName ,
			B.Barcode,
			(Case When(@RateType='ASaleRate') then  R.ASaleRate else Case When (@RateType='BSaleRate') Then R.BSaleRate Else Case When  (@RateType='MRP') Then R.MRP Else R.PurRate End End End) As Rate
			,T.MRP,T.Op,T.Inward,T.Outward,((T.OP+T.Inward)-T.Outward) AS ClsQty
			FROM         @TempItem AS T INNER JOIN
                      MStockItems AS M ON M.ItemNo = T.ItemNo  INNER JOIN
                      MStockBarcode AS B ON B.PkStockBarcodeNo = T.FkStockBarcodeNo INNER JOIN
                      MRateSetting AS R ON R.PkSrNo = T.PkSrNo AND T.MRP = R.MRP AND R.IsActive = 'True'
--			From @TempItem as T inner join  MStockItems as M on M.ItemNo= T.ItemNo
--	Inner Join MStockGroup As SG On SG.StockGroupNo=M.GroupNo
-- inner join 
--			MStockBarcode as B on B.ItemNo=T.ItemNo Inner Join MRateSetting as R on R.ItemNo=T.ItemNo And T.MRP=R.MRP And R.IsActive='True'
--		    Order By M.ItemName		 

End
Else if(@Type=2) -- only Amount
Begin


		Select ItemNo,ItemName ,
			   Barcode,Rate,MRP,Op*Rate as Op, Inward*Rate as Inward,Outward*Rate as Outward,ClsQty * Rate as ClsQty  From 
			   (Select  T.ItemNo,M.ShowItemName as ItemName ,B.Barcode,(Case When(@RateType='ASaleRate') then  R.ASaleRate else Case When (@RateType='BSaleRate') Then R.BSaleRate Else Case When  (@RateType='MRP') Then R.MRP Else R.PurRate End End End) As Rate
								,T.MRP,T.Op,T.Inward,T.Outward,((T.OP+T.Inward)-T.Outward) AS ClsQty
						FROM         @TempItem AS T INNER JOIN
                      MStockItems AS M ON M.ItemNo = T.ItemNo  INNER JOIN
                      MStockBarcode AS B ON B.PkStockBarcodeNo = T.FkStockBarcodeNo INNER JOIN
                      MRateSetting AS R ON R.PkSrNo = T.PkSrNo AND T.MRP = R.MRP AND R.IsActive = 'True'
						)

						As G  Order By ItemName
End Else if(@Type=3) -- only Qty And Amount
Begin
Select ItemNo,ItemName ,
Barcode,Rate,MRP,Op,Op*Rate as OpAmt, Inward,Inward*Rate as InwardAmt,Outward,Outward*Rate as OutwardAmt,ClsQty ,ClsQty * Rate as ClsQtyAmt From
 (	Select  T.ItemNo,M.ShowItemName as ItemName ,B.Barcode,(Case When(@RateType='ASaleRate') then  R.ASaleRate else Case When (@RateType='BSaleRate') Then R.BSaleRate Else Case When  (@RateType='MRP') Then R.MRP Else R.PurRate End End End) As Rate
		,T.MRP,T.Op,T.Inward,T.Outward,((T.OP+T.Inward)-T.Outward) AS ClsQty
		FROM         @TempItem AS T INNER JOIN
                      MStockItems AS M ON M.ItemNo = T.ItemNo  INNER JOIN
                      MStockBarcode AS B ON B.PkStockBarcodeNo = T.FkStockBarcodeNo INNER JOIN
                      MRateSetting AS R ON R.PkSrNo = T.PkSrNo AND T.MRP = R.MRP AND R.IsActive = 'True'

)
						As G Order By ItemName

End 

GO 
/*-----------------------------------------------------------------------------------------------------------------------------------------------------*/
INSERT INTO [MMenuMaster]
           ([SrNo]
           ,[MenuID]
           ,[MenuName]
           ,[ControlMenu]
           ,[NavigateURL]
           ,[IsChildNode]
           ,[IsAllow]
           ,[ShortCutKey]
           ,[ConstructorValue])
     VALUES
           (223
           ,10
           ,'Stock Summary Amount Wise'
           ,61
           ,'Display.StockSummaryMain'
           ,NULL
           ,1
           ,NULL
           ,NULL)

GO 
/*-----------------------------------------------------------------------------------------------------------------------------------------------------*/
