USE [Kirana0001]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Drop Table [dbo].[TParkingBill]
Go
CREATE TABLE [dbo].[TParkingBill](
	[ParkingBillNo] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[BillNo] [numeric](18, 0) NULL,
	[BillDate] [datetime] NULL,
	[BillTime] [datetime] NULL,
	[PersonName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LedgerNo] [numeric](18, 0) NULL,
	[IsBill] [bit] NULL,
	[FKVoucherNo] [numeric](18, 0) NULL,
	[CompanyNo] [numeric](18, 0) NULL,
	[IsCancel] [bit] NULL,
	[UserID] [numeric](18, 0) NULL,
	[UserDate] [datetime] NULL,
	[StatusNo] [int] NULL,
	[Discount] [numeric](18, 2) NULL,
	[Charges] [numeric](18, 2) NULL,
	[Remark] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RateTypeNo] [numeric](18, 0) NULL,
	[TaxTypeNo] [numeric](18, 0) NULL,
	[MobilePkSrNo] [numeric](18, 0) NULL,
	[MobileBillNo] [numeric](18, 0) NULL,
	[VoucherTypeCode] [numeric](18, 0) NULL,
	[InvNo] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_TParkingBill] PRIMARY KEY CLUSTERED 
(
	[ParkingBillNo] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AddTParkingBill_Mob]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AddTParkingBill_Mob]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Create PROCEDURE [dbo].[AddTParkingBill_Mob]
     @ParkingBillNo                       numeric(18),
     @BillNo                              numeric(18),
     @BillDate                            datetime,
     @BillTime                            datetime,
     @PersonName                          varchar(50),
	 @LedgerNo							  numeric(18),
     @IsBill                              bit,
     @CompanyNo                           numeric(18),
	 @IsCancel							  bit,
     @UserID                              numeric(18),
     @UserDate                            datetime,
	 @Discount							  numeric(18,2),
	 @Charges							  numeric(18,2),
	 @Remark							  varchar(500),
	 @RateTypeNo						  numeric(18),
	 @TaxTypeNo							  numeric(18),
	 @MobilePkSrNo						  numeric(18),
	 @MobileBillNo						  numeric(18),
	 @VoucherTypeCode					  numeric(18),
	 @InvNo								  Varchar(50),
     @ReturnID							  int output
AS
IF EXISTS(select ParkingBillNo from TParkingBill
          where
          ParkingBillNo = @ParkingBillNo)
     BEGIN
       --Update existing row
       UPDATE TParkingBill
       SET
          BillNo = @BillNo,
          BillDate = @BillDate,
          BillTime = @BillTime,
          PersonName = @PersonName,
		  LedgerNo = @LedgerNo,
          IsBill = @IsBill,
          CompanyNo = @CompanyNo,
		  IsCancel=@IsCancel,
          UserID = @UserID,
          UserDate = @UserDate,
		  Discount=@Discount,
		  Charges=@Charges,
		  Remark=@Remark,
		  RateTypeNo = @RateTypeNo,
		  TaxTypeNo = @TaxTypeNo,
          StatusNo=2,
		  MobilePkSrNo=@MobilePkSrNo, 
		  MobileBillNo=@MobileBillNo,
	      VoucherTypeCode=@VoucherTypeCode,
		  InvNo=@InvNo
       WHERE
          ParkingBillNo = @ParkingBillNo
		set @ReturnID=@ParkingBillNo
     END
ELSE
     BEGIN
       --Insert new row
		Select @BillNo=IsNull(Max(BillNo),0)+1 from TParkingBill 

       Declare @Id numeric
       SELECT @Id=IsNull(Max(ParkingBillNo),0) From TParkingBill
       DBCC CHECKIDENT('TParkingBill', RESEED, @Id)
       INSERT INTO TParkingBill(
          BillNo,
          BillDate,
          BillTime,
          PersonName,
		  LedgerNo,
          IsBill,
          CompanyNo,
		  IsCancel,
          UserID,
          UserDate,
          StatusNo,
		  Discount,
		  Charges,
		  Remark,
		  RateTypeNo,
		  TaxTypeNo,
		  MobilePkSrNo,
		  MobileBillNo,
		  VoucherTypeCode,
		  InvNo
)
       VALUES(
          @BillNo,
          @BillDate,
          @BillTime,
          @PersonName,
		  @LedgerNo,
          @IsBill,
          @CompanyNo,
		  @IsCancel,
          @UserID,
          @UserDate,
          1,
		  @Discount,
		  @Charges,
		  @Remark,
		  @RateTypeNo,
		  @TaxTypeNo,
		  @MobilePkSrNo,
		  @MobileBillNo,
	      @VoucherTypeCode,	
		  @InvNo
)
Set @ReturnID=Scope_Identity()
END

GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Update MSetting Set AppVersion='WQfxKU4Xoyve8zCKGyMrCw=='
Go
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/




