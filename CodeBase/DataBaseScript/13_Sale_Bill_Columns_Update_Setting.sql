USE [Kirana0001]
Go
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_MSettings
	(
	PkSettingNo numeric(18, 0) NOT NULL,
	SettingKeyCode varchar(100) NULL,
	SettingTypeNo numeric(18, 0) NULL,
	SettingValue varchar(MAX) NULL,
	SettingDescription varchar(MAX) NULL,
	StatusNo int NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
IF EXISTS(SELECT * FROM dbo.MSettings)
	 EXEC('INSERT INTO dbo.Tmp_MSettings (PkSettingNo, SettingKeyCode, SettingTypeNo, SettingValue, SettingDescription, StatusNo)
		SELECT PkSettingNo, SettingKeyCode, SettingTypeNo, SettingValue, SettingDescription, StatusNo FROM dbo.MSettings WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.MSettings
GO
EXECUTE sp_rename N'dbo.Tmp_MSettings', N'MSettings', 'OBJECT' 
GO
ALTER TABLE dbo.MSettings ADD CONSTRAINT
	PK_MSettings PRIMARY KEY CLUSTERED 
	(
	PkSettingNo
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = OFF) ON [PRIMARY]

GO
COMMIT


GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Update MSettings Set SettingDescription=0 Where PkSettingNo=67
Update MSettings Set SettingDescription=1 Where PkSettingNo=68
Update MSettings Set SettingDescription=2 Where PkSettingNo=69
Update MSettings Set SettingDescription=3 Where PkSettingNo=70
Update MSettings Set SettingDescription=5 Where PkSettingNo=71
Update MSettings Set SettingDescription=6 Where PkSettingNo=72
Update MSettings Set SettingDescription=7 Where PkSettingNo=73
Update MSettings Set SettingDescription=8 Where PkSettingNo=74
Update MSettings Set SettingDescription=9 Where PkSettingNo=75
Update MSettings Set SettingDescription=10 Where PkSettingNo=76
Update MSettings Set SettingDescription=11 Where PkSettingNo=77
Update MSettings Set SettingDescription=12 Where PkSettingNo=78
Update MSettings Set SettingDescription=13 Where PkSettingNo=79
Update MSettings Set SettingDescription=14 Where PkSettingNo=80
Update MSettings Set SettingDescription=15 Where PkSettingNo=81
Update MSettings Set SettingDescription=16 Where PkSettingNo=82
Update MSettings Set SettingDescription=17 Where PkSettingNo=83
Update MSettings Set SettingDescription=18 Where PkSettingNo=84
Update MSettings Set SettingDescription=19 Where PkSettingNo=85
Update MSettings Set SettingDescription=20 Where PkSettingNo=86
Update MSettings Set SettingDescription=21 Where PkSettingNo=87
Update MSettings Set SettingDescription=22 Where PkSettingNo=88
Update MSettings Set SettingDescription=23 Where PkSettingNo=89
Update MSettings Set SettingDescription=24 Where PkSettingNo=90
Update MSettings Set SettingDescription=25 Where PkSettingNo=91
Update MSettings Set SettingDescription=26 Where PkSettingNo=92
Update MSettings Set SettingDescription=27 Where PkSettingNo=93
Update MSettings Set SettingDescription=28 Where PkSettingNo=94
Update MSettings Set SettingDescription=29 Where PkSettingNo=157
Update MSettings Set SettingDescription=30 Where PkSettingNo=158
Update MSettings Set SettingDescription=31 Where PkSettingNo=159
Update MSettings Set SettingDescription=32 Where PkSettingNo=160
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Go
INSERT INTO [MSettings] ([PkSettingNo],[SettingKeyCode] ,[SettingTypeNo] ,[SettingValue] ,[SettingDescription]) VALUES ('309','SB_MRP',4,'FALSE',4)
Go
INSERT INTO [MSettings] ([PkSettingNo],[SettingKeyCode] ,[SettingTypeNo] ,[SettingValue] ,[SettingDescription]) VALUES ('310','SB_SchemeDetailsNo',4,'FALSE',33)
Go
INSERT INTO [MSettings] ([PkSettingNo],[SettingKeyCode] ,[SettingTypeNo] ,[SettingValue] ,[SettingDescription]) VALUES ('311','SB_SchemeFromNo',4,'FALSE',34)
Go
INSERT INTO [MSettings] ([PkSettingNo],[SettingKeyCode] ,[SettingTypeNo] ,[SettingValue] ,[SettingDescription]) VALUES ('312','SB_SchemeToNo ',4,'FALSE',35)
Go
INSERT INTO [MSettings] ([PkSettingNo],[SettingKeyCode] ,[SettingTypeNo] ,[SettingValue] ,[SettingDescription]) VALUES ('313','SB_RewardFromNo',4,'FALSE',36)
Go
INSERT INTO [MSettings] ([PkSettingNo],[SettingKeyCode] ,[SettingTypeNo] ,[SettingValue] ,[SettingDescription]) VALUES ('314','SB_RewardToNo ',4,'FALSE',37)
Go
INSERT INTO [MSettings] ([PkSettingNo],[SettingKeyCode] ,[SettingTypeNo] ,[SettingValue] ,[SettingDescription]) VALUES ('315','SB_ItemLevelDiscNo',4,'FALSE',38)
Go
INSERT INTO [MSettings] ([PkSettingNo],[SettingKeyCode] ,[SettingTypeNo] ,[SettingValue] ,[SettingDescription]) VALUES ('316','SB_FKItemLevelDiscNo',4,'FALSE',39)
Go
INSERT INTO [MSettings] ([PkSettingNo],[SettingKeyCode] ,[SettingTypeNo] ,[SettingValue] ,[SettingDescription]) VALUES ('317','SB_GodownNo',4,'FALSE',40)
Go
INSERT INTO [MSettings] ([PkSettingNo],[SettingKeyCode] ,[SettingTypeNo] ,[SettingValue] ,[SettingDescription]) VALUES ('318','SB_DiscountType',4,'FALSE',41)
Go
INSERT INTO [MSettings] ([PkSettingNo],[SettingKeyCode] ,[SettingTypeNo] ,[SettingValue] ,[SettingDescription]) VALUES ('319','SB_HamaliInKg',4,'FALSE',42)
Go
INSERT INTO [MSettings] ([PkSettingNo],[SettingKeyCode] ,[SettingTypeNo] ,[SettingValue] ,[SettingDescription]) VALUES ('320','SB_HSNCode',4,'FALSE',43)
Go
INSERT INTO [MSettings] ([PkSettingNo],[SettingKeyCode] ,[SettingTypeNo] ,[SettingValue] ,[SettingDescription]) VALUES ('321','SB_IGSTPercent',4,'FALSE',44)
Go
INSERT INTO [MSettings] ([PkSettingNo],[SettingKeyCode] ,[SettingTypeNo] ,[SettingValue] ,[SettingDescription]) VALUES ('322','SB_IGSTAmount',4,'FALSE',45)
Go
INSERT INTO [MSettings] ([PkSettingNo],[SettingKeyCode] ,[SettingTypeNo] ,[SettingValue] ,[SettingDescription]) VALUES ('323','SB_CGSTPercent',4,'FALSE',46)
Go
INSERT INTO [MSettings] ([PkSettingNo],[SettingKeyCode] ,[SettingTypeNo] ,[SettingValue] ,[SettingDescription]) VALUES ('324','SB_CGSTAmount',4,'FALSE',47)
Go
INSERT INTO [MSettings] ([PkSettingNo],[SettingKeyCode] ,[SettingTypeNo] ,[SettingValue] ,[SettingDescription]) VALUES ('325','SB_SGSTPercent',4,'FALSE',48)
Go
INSERT INTO [MSettings] ([PkSettingNo],[SettingKeyCode] ,[SettingTypeNo] ,[SettingValue] ,[SettingDescription]) VALUES ('326','SB_SGSTAmount',4,'FALSE',49)
Go
INSERT INTO [MSettings] ([PkSettingNo],[SettingKeyCode] ,[SettingTypeNo] ,[SettingValue] ,[SettingDescription]) VALUES ('327','SB_UTGSTPercent',4,'FALSE',50)
Go
INSERT INTO [MSettings] ([PkSettingNo],[SettingKeyCode] ,[SettingTypeNo] ,[SettingValue] ,[SettingDescription]) VALUES ('328','SB_UTGSTAmount',4,'FALSE',51)
Go
INSERT INTO [MSettings] ([PkSettingNo],[SettingKeyCode] ,[SettingTypeNo] ,[SettingValue] ,[SettingDescription]) VALUES ('329','SB_CessPercent',4,'FALSE',52)
Go
INSERT INTO [MSettings] ([PkSettingNo],[SettingKeyCode] ,[SettingTypeNo] ,[SettingValue] ,[SettingDescription]) VALUES ('330','SB_CessAmount',4,'FALSE',53)
Go
INSERT INTO [MSettings] ([PkSettingNo],[SettingKeyCode] ,[SettingTypeNo] ,[SettingValue] ,[SettingDescription]) VALUES ('331','SB_IsQtyRead',4,'FALSE',54)
Go
