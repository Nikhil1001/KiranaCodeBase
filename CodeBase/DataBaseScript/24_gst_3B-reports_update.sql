USE [Kirana0001]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTaxDetailsGST_3B]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetTaxDetailsGST_3B]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[GetTaxDetailsGST_3B] 
	@FromDate datetime,
	@ToDate datetime,
	@VchType numeric(18),
	@TaxTypeNo	numeric(18)

AS
 
if(@VchType=15)
Begin

		Select S.FkVoucherNo,IGSTPercent,
		Sum(Case When(TVoucherEntry.VoucherTypeCode=15) Then S.IGSTAmount When(TVoucherEntry.VoucherTypeCode=12) Then - S.IGSTAmount Else 0 End) As IGSTAmount,
		Sum(Case When(TVoucherEntry.VoucherTypeCode=15) Then S.CGSTAmount When(TVoucherEntry.VoucherTypeCode=12) Then - S.CGSTAmount Else 0 End) As CGSTAmount,
		Sum(Case When(TVoucherEntry.VoucherTypeCode=15) Then S.SGSTAmount When(TVoucherEntry.VoucherTypeCode=12) Then - S.SGSTAmount Else 0 End) As SGSTAmount,
	    Sum(Case When(TVoucherEntry.VoucherTypeCode=15) Then S.CessAmount When(TVoucherEntry.VoucherTypeCode=12) Then - S.CessAmount Else 0 End) As CessAmount,
		Sum(Case When(TVoucherEntry.VoucherTypeCode=15) Then S.NetAmount When(TVoucherEntry.VoucherTypeCode=12) Then - S.NetAmount Else 0 End) As NetAmount
--	    Sum( S.CGSTAmount ) As  CGSTAmount,
--		Sum( S.SGSTAmount ) AS SGSTAmount,
--		Sum( S.CessAmount ) As CessAmount,
--		Sum( S.NetAmount) As NetAmount
        From  TStock As S Inner Join  TVoucherEntry ON TVoucherEntry.PkVoucherNo=S.FkVoucherNo
		Where TVoucherEntry.VoucherTypeCode in(15,12) and TVoucherEntry.VoucherDate>=@FromDate and TVoucherEntry.VoucherDate<=@ToDate 
		 AND (TVoucherEntry.IsCancel = 'false')
					AND TVoucherEntry.TaxTypeNo= @TaxTypeNo
		Group By IGSTPercent,FkVoucherNo
		order by FkVoucherNo

End
Else 
Begin

		Select S.FkVoucherNo,IGSTPercent,
	    Sum(Case When(TVoucherEntry.VoucherTypeCode=9) Then S.IGSTAmount When(TVoucherEntry.VoucherTypeCode=13) Then - S.IGSTAmount Else 0 End) As IGSTAmount,
		Sum(Case When(TVoucherEntry.VoucherTypeCode=9) Then S.CGSTAmount When(TVoucherEntry.VoucherTypeCode=13) Then - S.CGSTAmount Else 0 End) As CGSTAmount,
		Sum(Case When(TVoucherEntry.VoucherTypeCode=9) Then S.SGSTAmount When(TVoucherEntry.VoucherTypeCode=13) Then - S.SGSTAmount Else 0 End) As SGSTAmount,
	    Sum(Case When(TVoucherEntry.VoucherTypeCode=9) Then S.CessAmount When(TVoucherEntry.VoucherTypeCode=13) Then - S.CessAmount Else 0 End) As CessAmount,
		Sum(Case When(TVoucherEntry.VoucherTypeCode=9) Then S.NetAmount When(TVoucherEntry.VoucherTypeCode=13) Then - S.NetAmount Else 0 End) As NetAmount
		From  TStock As S Inner Join  TVoucherEntry ON TVoucherEntry.PkVoucherNo=S.FkVoucherNo
		Where TVoucherEntry.VoucherTypeCode in(9,13) and TVoucherEntry.VoucherDate>=@FromDate and TVoucherEntry.VoucherDate<=@ToDate 
		 AND (TVoucherEntry.IsCancel = 'false')
					AND TVoucherEntry.TaxTypeNo= @TaxTypeNo
		Group By IGSTPercent,FkVoucherNo
		order by FkVoucherNo


End

Go
