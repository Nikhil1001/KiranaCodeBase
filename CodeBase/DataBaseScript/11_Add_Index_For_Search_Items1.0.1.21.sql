USE [Kirana0001]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE NONCLUSTERED INDEX [SearchItems] ON [dbo].[MStockItemBalance] 
(
	[ItemNo] ASC,
	[MRP] ASC
)WITH (PAD_INDEX  = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE NONCLUSTERED INDEX [SearchItems] ON [dbo].[MStockItems] 
(
	[ItemNo] ASC,
	[IsActive] ASC,
	[ShowItemName] ASC
)WITH (PAD_INDEX  = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
