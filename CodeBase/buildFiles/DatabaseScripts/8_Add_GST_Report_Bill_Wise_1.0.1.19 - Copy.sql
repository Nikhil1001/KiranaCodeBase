USE [Kirana0001]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTaxDetailsGST_Header]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetTaxDetailsGST_Header]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Create PROCEDURE [dbo].[GetTaxDetailsGST_Header] 
	@FromDate datetime,
	@ToDate datetime,
	@VchType numeric(18),
	@DiscLedg varchar(max),
	@ChargesLedg varchar(max),
	@RoundOffLedgNo numeric(18),
	@TaxTypeNo	numeric(18)

AS
/*
Set  @FromDate='1-Sep-2017'
Set @ToDate ='10-Sep-2017'
Set @VchType=15
Set @DiscLedg=0
Set @ChargesLedg=0
Set @RoundOffLedgNo=0
Set @TaxTypeNo=38 */


Declare @Month varchar(20),@MNo int ,@TDate datetime ,@Yr int,@FrDate datetime,@TempPk numeric(18),@TempColNo numeric(18),
			@TaxAmount numeric(18,2),@TempDocNo numeric(18),@Disc numeric(18,2),@Charges numeric(18,2),@RndOff numeric(18,2),@LedgerName varchar(max),
			@ChrgTaxAcc numeric(18),@ChrgTaxAmt numeric(18,2),@TempChrgAmt numeric(18,2),@TempChrgTaxAmt numeric(18,2),@IsChrgTax bit
	


if(@VchType=15)
Begin
	Select @IsChrgTax=	Cast(SettingValue as bit) From MSettings Where PkSettingNo=346
	Select @ChrgTaxAcc=	Cast(SettingValue as numeric) From MSettings Where PkSettingNo=347
End
else
Begin
	Select @IsChrgTax=	Cast(SettingValue as bit) From MSettings Where PkSettingNo=348 
	Select @ChrgTaxAcc=	Cast(SettingValue as numeric) From MSettings Where PkSettingNo=349
End


Declare @TDisc Table(LedgNo numeric(18))
Declare @TChrg Table(LedgNo numeric(18))

insert into @TDisc Exec('Select LedgerNo From MLedger Where LedgerNo in ('+ @DiscLedg +')')
insert into @TChrg Exec('Select LedgerNo From MLedger Where LedgerNo in ('+ @ChargesLedg +')')

DECLARE @CompStateNo numeric(18),@VarState varchar(500)
SELECT TOP 1 @CompStateNo = StateNo FROM MCompany
Select @VarState= ISNULL(StateCodeGST,'') + '-' + ISNULL(StateName,'')   From MState Where StateNo= @CompStateNo


 SELECT  TVoucherEntry_1.PkVoucherNo, TVoucherEntry_1.VoucherUserNo As BillNo, TVoucherEntry_1.VoucherDate As Date,
			MLedger.LedgerName,
			TVoucherEntry_1.BilledAmount,
						(SELECT     isNull(SUM(CASE WHEN (Debit <> 0) THEN Debit ELSE Credit END),0) 
						FROM          TVoucherEntry INNER JOIN
						TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo
						WHERE      (TVoucherDetails.LedgerNo IN
						(SELECT LedgNo FROM @TDisc )) AND (TVoucherDetails.FkVoucherNo = TVoucherEntry_1.PkVoucherNo)) AS Disc ,
						(SELECT     isNull(SUM(CASE WHEN (Debit <> 0) THEN Debit ELSE Credit END),0) 
						FROM          TVoucherEntry AS TVoucherEntry_3 INNER JOIN
						TVoucherDetails AS TVoucherDetails_3 ON TVoucherEntry_3.PkVoucherNo = TVoucherDetails_3.FkVoucherNo
						WHERE      (TVoucherDetails_3.LedgerNo IN
						(@RoundOffLedgNo)) AND (TVoucherDetails_3.FkVoucherNo = TVoucherEntry_1.PkVoucherNo)) As RndOff ,
						(SELECT     isNull(SUM(CASE WHEN (Debit <> 0) THEN Debit ELSE Credit END),0)
						FROM          TVoucherEntry AS TVoucherEntry_2 INNER JOIN
						TVoucherDetails AS TVoucherDetails_2 ON TVoucherEntry_2.PkVoucherNo = TVoucherDetails_2.FkVoucherNo
						WHERE      (TVoucherDetails_2.LedgerNo IN
						(SELECT LedgNo FROM @TChrg)) AND (TVoucherDetails_2.FkVoucherNo = TVoucherEntry_1.PkVoucherNo)) As Charges,
						
	IsNull(MState.StateName,@VarState) As StateName,IsNull(MLedgerDetails.CSTNo,'') as GSTNo,
		SUM(TStock.NetAmount) AS SAmt,SUM(TStock.TaxAmount)  AS TAmt
	FROM TVoucherEntry AS TVoucherEntry_1 INNER JOIN
			TStock ON TVoucherEntry_1.PkVoucherNo = TStock.FKVoucherNo   INNER JOIN
			TVoucherDetails ON TVoucherEntry_1.PkVoucherNo = TVoucherDetails.FkVoucherNo INNER JOIN
			MLedger ON TVoucherDetails.LedgerNo = MLedger.LedgerNo And TVoucherDetails.SrNo=501 LEFT OUTER JOIN
			MLedgerDetails ON MLedger.LedgerNo = MLedgerDetails.LedgerNo LEFT OUTER JOIN
			MState ON MLedgerDetails.StateNo = MState.StateNo

	WHERE TVoucherEntry_1.VoucherTypeCode=@VchType	 and TVoucherEntry_1.VoucherDate>=@FromDate and TVoucherEntry_1.VoucherDate<=@ToDate  AND (TVoucherEntry_1.IsCancel = 'false')
			AND TVoucherEntry_1.TaxTypeNo= @TaxTypeNo
			Group by TVoucherEntry_1.VoucherUserNo,TVoucherEntry_1.VoucherDate,TVoucherEntry_1.PkVoucherNo,BilledAmount,MLedger.LedgerName, MState.StateName
			,MLedgerDetails.CSTNo
			order by TVoucherEntry_1.VoucherUserNo,TVoucherEntry_1.PkVoucherNo


Select S.FkVoucherNo,IGSTPercent,
Sum(S.IGSTAmount) As IGSTAmount,
Sum( S.CGSTAmount ) As  CGSTAmount,
Sum( S.SGSTAmount ) AS SGSTAmount,
Sum( S.CessAmount ) As CessAmount
 From  TStock As S Inner Join  TVoucherEntry ON TVoucherEntry.PkVoucherNo=S.FkVoucherNo
Where TVoucherEntry.VoucherTypeCode=@VchType	 and TVoucherEntry.VoucherDate>=@FromDate and TVoucherEntry.VoucherDate<=@ToDate  AND (TVoucherEntry.IsCancel = 'false')
			AND TVoucherEntry.TaxTypeNo= @TaxTypeNo
Group By IGSTPercent,FkVoucherNo
order by FkVoucherNo




Go
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Update MMenuMaster Set MenuID=15,ControlMenu=68 Where SrNo=219
Go
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
INSERT INTO [MMenuMaster]
           ([SrNo]
           ,[MenuID]
           ,[MenuName]
           ,[ControlMenu]
           ,[NavigateURL]
           ,[IsChildNode]
           ,[IsAllow]
           ,[ShortCutKey]
           ,[ConstructorValue])
     VALUES
           (220
           ,11
           ,'GST Reports'
           ,76
           ,'Display.TaxDetailsGST'
           ,NULL
           ,1
           ,NULL
           ,9)
Go
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Update MSetting Set AppVersion='+lRShJR1dVowISbNd/OjSA=='






