USE [Kirana0001]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE [dbo].[TStock] ADD
	SchemeDetailsNo numeric(18, 0) NULL,
	SchemeFromNo numeric(18, 0) NULL,
	SchemeToNo numeric(18, 0) NULL
GO
COMMIT
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AddTStock]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AddTStock]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[AddTStock]
     @PkStockTrnNo                        numeric(18),
	 @FKVoucherNo						  numeric(18),
     @FkVoucherTrnNo                      numeric(18),
     @FkVoucherSrNo                       numeric(18),
     @GroupNo                             numeric(18),
     @ItemNo                              numeric(18),
     @TrnCode                             numeric(18),
     @Quantity                            numeric(18,3),
     @BilledQuantity                      numeric(18,2),
     @Rate                                numeric(18,2),
     @Amount                              numeric(18,2),
	 @NetRate							  numeric(18,2),
	 @NetAmount							  numeric(18,2),
     @TaxPercentage                       numeric(18,2),
     @TaxAmount                           numeric(18,2),
	 @DiscPercentage					  numeric(18,2),
	 @DiscAmount						  numeric(18,2),
	 @DiscRupees						  numeric(18,2),
	 @DiscPercentage2					  numeric(18,2),
	 @DiscAmount2						  numeric(18,2),
	 @DiscRupees2						  numeric(18,2),
	 @FkUomNo							  numeric(18),
	 @FkStockBarCodeNo				      numeric(18),
	 @FkRateSettingNo					  numeric(18),
	 @FkItemTaxInfo						  numeric(18),
	 @FreeQty                             numeric(18,2),
	 @FreeUOMNo							  numeric(18),   
     @UserID                              numeric(18),
     @UserDate                            datetime,
	 @CompanyNo							  numeric(18),
	 @LandedRate						  numeric(18,2),
	 @DiscountType						  numeric(18),
	 @HamaliInKg						  numeric(18,2),
	 @IGSTPercent						  numeric(18,2),
	 @IGSTAmount						  numeric(18,3),
	 @CGSTPercent						  numeric(18,2),
	 @CGSTAmount						  numeric(18,3),
	 @SGSTPercent						  numeric(18,2),
	 @SGSTAmount						  numeric(18,3),
	 @UTGSTPercent						  numeric(18,2),
	 @UTGSTAmount						  numeric(18,3),
	 @CessPercent						  numeric(18,2),
	 @CessAmount						  numeric(18,3),
	 @HSNCode							  varchar(50),
	 @SchemeDetailsNo				      numeric(18,2),
	 @SchemeFromNo                        numeric(18,2),
	 @SchemeToNo                          numeric(18,2),
     @ReturnID							  int output
     
AS
IF EXISTS(select PkStockTrnNo from TStock
          where
          PkStockTrnNo = @PkStockTrnNo)
     BEGIN
       --Update existing row
       UPDATE TStock
       SET
		  FKVoucherNo = @FKVoucherNo,
          FkVoucherTrnNo = @FkVoucherTrnNo,
          FkVoucherSrNo = @FkVoucherSrNo,
          GroupNo = @GroupNo,
          ItemNo = @ItemNo,
          TrnCode = @TrnCode,
          Quantity = @Quantity,
          BilledQuantity = @BilledQuantity,
          Rate = @Rate,
          Amount = @Amount,
		  NetRate = @NetRate,
		  NetAmount = @NetAmount,
          TaxPercentage = @TaxPercentage,
          TaxAmount = @TaxAmount,
		  DiscPercentage=@DiscPercentage,
		  DiscAmount=@DiscAmount,
		  DiscRupees = @DiscRupees,
		  DiscPercentage2 =@DiscPercentage2,
		  DiscAmount2 = @DiscAmount2,
		  DiscRupees2 = @DiscRupees2,
	      FkUomNo=@FkUomNo,
	      FkStockBarCodeNo=@FkStockBarCodeNo,
	      FkRateSettingNo=@FkRateSettingNo,
	      FkItemTaxInfo=@FkItemTaxInfo,
		  FreeQty=@FreeQty,
	      FreeUOMNo=@FreeUOMNo,
          UserID = @UserID,
          UserDate = @UserDate,
		  CompanyNo= @CompanyNo,
		  LandedRate = @LandedRate,
		  StatusNo = 2,
		  DiscountType=@DiscountType,
	      HamaliInKg=@HamaliInKg,
		  IGSTPercent=@IGSTPercent,
		  IGSTAmount=@IGSTAmount,
          CGSTPercent=@CGSTPercent,
		  CGSTAmount=@CGSTAmount,
		  SGSTPercent=@SGSTPercent,
		  SGSTAmount=@SGSTAmount,
		  UTGSTPercent=@UTGSTPercent,
		  UTGSTAmount=@UTGSTAmount,
		  CessPercent=@CessPercent,
		  CessAmount=@CessAmount,
		  HSNCode=@HSNCode,      
		  SchemeDetailsNo=@SchemeDetailsNo,
          SchemeFromNo=@SchemeFromNo,
          SchemeToNo=@SchemeToNo,    
          ModifiedBy = isnull(ModifiedBy,'') + cast(@UserID as varchar)+'@'+ CONVERT(VARCHAR(10), GETDATE(), 105)
       WHERE
          PkStockTrnNo = @PkStockTrnNo
		  set @ReturnID=@PkStockTrnNo    

     END
ELSE
     BEGIN
       --Insert new row
       Declare @Id numeric
       SELECT @Id=IsNull(Max(PkStockTrnNo),0) From TStock
       DBCC CHECKIDENT('TStock', RESEED, @Id)
       INSERT INTO TStock(
		  FKVoucherNo,
          FkVoucherTrnNo,
          FkVoucherSrNo,
          GroupNo,
          ItemNo,
          TrnCode,
          Quantity,
          BilledQuantity,
          Rate,
          Amount,
		  NetRate,
		  NetAmount,
          TaxPercentage,
          TaxAmount,
		  DiscPercentage,
		  DiscAmount,
		  DiscRupees,
		  DiscPercentage2,
		  DiscAmount2,
		  DiscRupees2,
		  FkUomNo,
		  FkStockBarCodeNo,
		  FkRateSettingNo,
		  FkItemTaxInfo,
		  IsVoucherLock,
	      FreeQty,
		  FreeUOMNo,
          UserID,
          UserDate,
		  CompanyNo,
		  LandedRate,
		  StatusNo,
		  DiscountType,
	      HamaliInKg,
		  IGSTPercent,
		  IGSTAmount,
	      CGSTPercent,
	      CGSTAmount,
	      SGSTPercent,
	      SGSTAmount,
		  UTGSTPercent,
		  UTGSTAmount,
		  CessPercent,
		  CessAmount,
		  HSNCode,
		  SchemeDetailsNo,
		  SchemeFromNo,
		  SchemeToNo        
          
)
       VALUES(
		  @FKVoucherNo,
          @FkVoucherTrnNo,
          @FkVoucherSrNo,
          @GroupNo,
          @ItemNo,
          @TrnCode,
          @Quantity,
          @BilledQuantity,
          @Rate,
		  @Amount,
		  @NetRate,
          @NetAmount,
          @TaxPercentage,
          @TaxAmount,
	      @DiscPercentage,
		  @DiscAmount,
		  @DiscRupees,
		  @DiscPercentage2,
		  @DiscAmount2,
		  @DiscRupees2,
		  @FkUomNo,
		  @FkStockBarCodeNo,
		  @FkRateSettingNo,
		  @FkItemTaxInfo,
		  'false',
	      @FreeQty,
		  @FreeUOMNo,
          @UserID,
          @UserDate,
		  @CompanyNo,
		  @LandedRate,
		  1,
		  @DiscountType,
	      @HamaliInKg,
		  @IGSTPercent,
		  @IGSTAmount,
		  @CGSTPercent,
		  @CGSTAmount,
		  @SGSTPercent,
		  @SGSTAmount,
		  @UTGSTPercent,
		  @UTGSTAmount,
		  @CessPercent,
		  @CessAmount,
		  @HSNCode,
		  @SchemeDetailsNo,
          @SchemeFromNo,
	      @SchemeToNo
          
)
Set @ReturnID=Scope_Identity()
END
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetLoyaltyInstantTSKU_C]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetLoyaltyInstantTSKU_C]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Create procedure [dbo].[GetLoyaltyInstantTSKU_C]
@PFromDate Datetime,
@PItemno varchar(max),
@LedgerNo numeric(18),
@VchNo    numeric(18)
As
Declare @PrevSchDPkSrNo numeric(18,0), @SqlString varchar(max)
Declare @SchemeTSKUC Table(rType numeric(18,0), SchemeNo numeric(18,0), SchemeName varchar(max),SchemeTypeNo numeric(18,0), SchemeUserNo varchar(max), SchemeDate datetime, SchemePeriodFrom datetime, SchemePeriodTo datetime, DiscAmount numeric(18,2), PDiscAmount numeric(18,2),SchemeDetailsNo numeric(18,0),Itemno numeric(18,0),Qty numeric(18,2),UomName varchar(50),Uomno numeric(18,0),ItemName varchar(500), SchemeFromNo numeric(18), MRP numeric(18,2))    
Declare @SchemeTSKUCOUT Table(rType numeric(18,0), SchemeNo numeric(18,0), SchemeName varchar(max),SchemeTypeNo numeric(18,0), SchemeUserNo varchar(max), SchemeDate datetime, SchemePeriodFrom datetime, SchemePeriodTo datetime, DiscAmount numeric(18,2), PDiscAmount numeric(18,2),SchemeDetailsNo numeric(18,0),Itemno numeric(18,0),Qty numeric(18,2),UomName varchar(50),Uomno numeric(18,0),ItemName varchar(500), SchemeFromNo numeric(18), MRP numeric(18,2))    
Declare @rType numeric(18,0), @SchemeNo numeric(18,0), @SchemeName varchar(max),@SchemeTypeNo numeric(18,0), @SchemeUserNo varchar(max), @SchemeDate datetime, @SchemePeriodFrom datetime, @SchemePeriodTo datetime, @DiscAmount numeric(18,2), @PDiscAmount numeric(18,2),@SchemeDetailsNo numeric(18,0),@Itemno numeric(18,0),@Qty numeric(18,2),@UomName varchar(50),@Uomno numeric(18,0),@ItemName varchar(500), @SchemeFromNo numeric(18), @MRP numeric(18,2)    

Begin
	set @PrevSchDPkSrNo=0
	if(@VchNo=0)
		set  @SqlString='SELECT  (Case When (MScheme.SchemeTypeNo = 3) then ''1'' else ''2'' end) As rtype, MScheme.SchemeNo, MScheme.SchemeName, MScheme.SchemeTypeNo, MScheme.SchemeUserNo, MScheme.SchemeDate, 
		MScheme.SchemePeriodFrom, MScheme.SchemePeriodTo, MSchemeDetails.DiscAmount,
		(SELECT     ISNULL(SUM(Amount * DiscPercentage / 100), 0) AS Expr1
		FROM    MSchemeToDetails
		WHERE  (SchemeDetailsNo IN (MSchemeDetails.PkSrNo))) AS PDiscAmount, MSchemeDetails.PkSrNo, MSchemeFromDetails.ItemNo, 
		MSchemeFromDetails.Quantity ,MUOM.UOMName, MSchemeFromDetails.UomNo,(Select ShowItemName From mStockItems Where ItemNo=MSchemeFromDetails.ItemNo) as ItemName,
		MSchemeFromDetails.PkSrNo As SchemeFromNo , MSchemeFromDetails.MRP 
		FROM    MScheme INNER JOIN
		MSchemeDetails ON MScheme.SchemeNo = MSchemeDetails.SchemeNo INNER JOIN
		MSchemeFromDetails ON MSchemeDetails.PkSrNo = MSchemeFromDetails.SchemeDetailsNo INNER JOIN
		MUOM ON MSchemeFromDetails.UomNo = MUOM.UOMNo 
				WHERE (MScheme.SchemePeriodFrom <= ''' + cast(@PFromDate as varchar)  + ''') AND (MScheme.SchemePeriodTo >= ''' + cast(@PFromDate as varchar) + ''') AND (MScheme.IsActive = 1) AND
		(MScheme.SchemeTypeNo = 4 OR 
			 (MScheme.SchemeTypeNo = 3 AND MSchemeDetails.PkSrNo IN (SELECT  SchemeDetailsNo FROM MSchemeToDetails))) 
		AND MSchemeDetails.PkSrNo IN
		(SELECT  SchemeDetailsNo FROM    MSchemeFromDetails WHERE 
		 ('+cast(@PItemno as varchar(max))+')  ) and (MScheme.IsIWScheme=0)
ORDER BY MScheme.IsIWScheme desc, MScheme.SchemeTypeNo desc,MScheme.SchemeUserNo , MSchemeDetails.PkSrNo'
	else 
			set  @SqlString='SELECT  (Case When (MScheme.SchemeTypeNo = 3) then ''1'' else ''2'' end) As rtype, MScheme.SchemeNo, MScheme.SchemeName, MScheme.SchemeTypeNo, MScheme.SchemeUserNo, MScheme.SchemeDate, 
		MScheme.SchemePeriodFrom, MScheme.SchemePeriodTo, MSchemeDetails.DiscAmount,
		(SELECT     ISNULL(SUM(Amount * DiscPercentage / 100), 0) AS Expr1
		FROM    MSchemeToDetails
		WHERE  (SchemeDetailsNo IN (MSchemeDetails.PkSrNo))) AS PDiscAmount, MSchemeDetails.PkSrNo, MSchemeFromDetails.ItemNo, 
		MSchemeFromDetails.Quantity ,MUOM.UOMName, MSchemeFromDetails.UomNo,(Select ShowItemName From mStockItems Where ItemNo=MSchemeFromDetails.ItemNo) as ItemName,
		MSchemeFromDetails.PkSrNo As SchemeFromNo , MSchemeFromDetails.MRP 
		FROM    MScheme INNER JOIN
		MSchemeDetails ON MScheme.SchemeNo = MSchemeDetails.SchemeNo INNER JOIN
		MSchemeFromDetails ON MSchemeDetails.PkSrNo = MSchemeFromDetails.SchemeDetailsNo INNER JOIN
		MUOM ON MSchemeFromDetails.UomNo = MUOM.UOMNo
		WHERE (MScheme.SchemeTypeNo = 4 OR 
				(MScheme.SchemeTypeNo = 3 AND MSchemeDetails.PkSrNo IN (SELECT  SchemeDetailsNo FROM MSchemeToDetails))) 
			  AND MSchemeDetails.PkSrNo IN
		        (SELECT  SchemeDetailsNo FROM    TRewardDetails INNER JOIN TReward ON TReward.RewardNo = TRewardDetails.RewardNo
                     WHERE TReward.FkVoucherNo =  ' + Cast(@VchNo as Varchar) + ')  and (MScheme.IsIWScheme=0)
       ORDER BY MScheme.IsIWScheme desc, MScheme.SchemeTypeNo desc,MScheme.SchemeUserNo , MSchemeDetails.PkSrNo'
--ItemNo IN

	Insert into @SchemeTSKUC exec(@SqlString)

	Declare CurScheme1 Cursor for SELECT rType , SchemeNo , SchemeName ,SchemeTypeNo , SchemeUserNo , 
	SchemeDate , SchemePeriodFrom , SchemePeriodTo , DiscAmount , PDiscAmount ,
	SchemeDetailsNo ,Itemno ,Qty ,UomName ,Uomno ,ItemName, SchemeFromNo, MRP FROM @SchemeTSKUC

	Open  CurScheme1

	Fetch next from CurScheme1 into @rType , @SchemeNo , @SchemeName ,@SchemeTypeNo , @SchemeUserNo , 
	@SchemeDate , @SchemePeriodFrom , @SchemePeriodTo , @DiscAmount , @PDiscAmount ,
	@SchemeDetailsNo ,@Itemno ,@Qty ,@UomName,@Uomno ,@ItemName, @SchemeFromNo, @MRP
    set @PrevSchDPkSrNo=@SchemeDetailsNo
	While(@@Fetch_Status=0)
	Begin
		if(@PrevSchDPkSrNo=@SchemeDetailsNo)
		Begin
		set @PrevSchDPkSrNo=@SchemeDetailsNo
		insert into @SchemeTSKUCOUT values(@rType , @SchemeNo , @SchemeName ,@SchemeTypeNo , @SchemeUserNo , 
					@SchemeDate , @SchemePeriodFrom , @SchemePeriodTo , @DiscAmount , @PDiscAmount ,
					@SchemeDetailsNo ,@Itemno ,@Qty ,@UomName,@Uomno ,@ItemName, @SchemeFromNo, @MRP)
		End
        Else
		Begin
		set @PrevSchDPkSrNo=@SchemeDetailsNo
		insert into @SchemeTSKUCOUT values(3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,Null, NULL,NULL)
		insert into @SchemeTSKUCOUT values(@rType , @SchemeNo , @SchemeName ,@SchemeTypeNo , @SchemeUserNo , 
					@SchemeDate , @SchemePeriodFrom , @SchemePeriodTo , @DiscAmount , @PDiscAmount ,
					@SchemeDetailsNo ,@Itemno ,@Qty ,@UomName,@Uomno ,@ItemName, @SchemeFromNo, @MRP)
		End

		Fetch next from CurScheme1 into @rType , @SchemeNo , @SchemeName ,@SchemeTypeNo , @SchemeUserNo , 
		@SchemeDate , @SchemePeriodFrom , @SchemePeriodTo , @DiscAmount , @PDiscAmount ,
		@SchemeDetailsNo ,@Itemno ,@Qty ,@UomName,@Uomno ,@ItemName, @SchemeFromNo, @MRP

	End
	Close CurScheme1 Deallocate CurScheme1
End
select rType , SchemeNo ,SchemeUserNo , SchemeName ,ItemName,SchemeTypeNo, SchemeDate , SchemePeriodFrom , SchemePeriodTo , DiscAmount , PDiscAmount ,SchemeDetailsNo ,Itemno ,Qty,UomName,Uomno, SchemeFromNo,MRP  from @SchemeTSKUCOUT
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/

