USE [Kirana0001]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTaxDetailsGST_DayWise]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetTaxDetailsGST_DayWise]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Create PROCEDURE [dbo].[GetTaxDetailsGST_DayWise] 
	@FromDate datetime,
	@ToDate datetime,
	@VchType numeric(18)
AS
BEGIN

SELECT     TVoucherEntry.VoucherDate, Sum( TStock.Amount) As Amount , TStock.TaxPercentage,Sum( TStock.NetAmount) as NetAmount,
		Sum( TStock.IGSTAmount) as IGSTAmount,Sum( TStock.CGSTAmount ) as CGSTAmount,Sum( TStock.SGSTAmount) As SGSTAmount, 
                     Sum( TStock.CessAmount) AS CessAmount, MState.StateName
FROM         TStock INNER JOIN
                      TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo AND TVoucherEntry.VoucherTypeCode = @VchType AND TVoucherEntry.TaxTypeNo = 38 AND 
                      TVoucherEntry.IsCancel = 'False' INNER JOIN
                      TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo AND TVoucherDetails.SrNo = 501 INNER JOIN
                      MLedger ON TVoucherDetails.LedgerNo = MLedger.LedgerNo LEFT OUTER JOIN
                      MLedgerDetails ON MLedger.LedgerNo = MLedgerDetails.LedgerNo LEFT OUTER JOIN
                      MState ON MLedgerDetails.StateNo = MState.StateNo
WHERE     (TVoucherEntry.VoucherDate >= @FromDate) AND (TVoucherEntry.VoucherDate <= @ToDate)
Group By TVoucherEntry.VoucherDate,TStock.TaxPercentage,MState.StateName
END
Go
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTaxDetailsGST_MonthWise]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetTaxDetailsGST_MonthWise]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Create PROCEDURE [dbo].[GetTaxDetailsGST_MonthWise] 
	@FromDate datetime,
	@ToDate datetime,
	@VchType numeric(18)
AS
BEGIN

SELECT    
DateName( month , DateAdd( month , Month(TVoucherEntry.VoucherDate) , 0 ) - 1 ) +' -- '+ 
        Cast(Year(TVoucherEntry.VoucherDate) % 100 as varchar) Party

, Sum( TStock.Amount) As Amount , TStock.TaxPercentage,Sum( TStock.NetAmount) as NetAmount,
		Sum( TStock.IGSTAmount) as IGSTAmount,Sum( TStock.CGSTAmount ) as CGSTAmount,Sum( TStock.SGSTAmount) As SGSTAmount, 
                     Sum( TStock.CessAmount) AS CessAmount, MState.StateName
FROM         TStock INNER JOIN
                      TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo AND TVoucherEntry.VoucherTypeCode = @VchType AND TVoucherEntry.TaxTypeNo = 38 AND 
                      TVoucherEntry.IsCancel = 'False' INNER JOIN
                      TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo AND TVoucherDetails.SrNo = 501 INNER JOIN
                      MLedger ON TVoucherDetails.LedgerNo = MLedger.LedgerNo LEFT OUTER JOIN
                      MLedgerDetails ON MLedger.LedgerNo = MLedgerDetails.LedgerNo LEFT OUTER JOIN
                      MState ON MLedgerDetails.StateNo = MState.StateNo
WHERE     (TVoucherEntry.VoucherDate >= @FromDate) AND (TVoucherEntry.VoucherDate <= @ToDate)
Group By Year(TVoucherEntry.VoucherDate),Month(TVoucherEntry.VoucherDate),TStock.TaxPercentage,MState.StateName
END

Go
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTaxDetailsGST_BillWise]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetTaxDetailsGST_BillWise]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Create PROCEDURE [dbo].[GetTaxDetailsGST_BillWise] 
	@FromDate datetime,
	@ToDate datetime,	
	@VchType numeric(18),
	@DiscLedg varchar(max),
	@ChargesLedg varchar(max),
	@RoundOffLedgNo numeric(18),
	@TaxTypeNo	numeric(18)
AS


/*	Set @FromDate='01-Aug-2017'
	Set @ToDate ='3-sep-2017'
	Set @VchType =15
	Set @DiscLedg ='9'
	Set @ChargesLedg ='4'
	Set @RoundOffLedgNo=5
	Set @TaxTypeNo	=38 */

	Declare @ChrgTaxAcc numeric(18),@IsChrgTax bit
	Declare @TDisc Table(LedgNo numeric(18))
	Declare @TChrg Table(LedgNo numeric(18))

if(@VchType=15)
Begin
	Select @IsChrgTax=	Cast(SettingValue as bit) From MSettings Where PkSettingNo=346
	Select @ChrgTaxAcc=	Cast(SettingValue as numeric) From MSettings Where PkSettingNo=347
End
else
Begin
	Select @IsChrgTax=	Cast(SettingValue as bit) From MSettings Where PkSettingNo=348 
	Select @ChrgTaxAcc=	Cast(SettingValue as numeric) From MSettings Where PkSettingNo=349
End
insert into @TDisc Exec('Select LedgerNo From MLedger Where LedgerNo in ('+ @DiscLedg +')')
insert into @TChrg Exec('Select LedgerNo From MLedger Where LedgerNo in ('+ @ChargesLedg +')')

	SELECT TVoucherEntry_1.VoucherUserNo As BillNo, TVoucherEntry_1.VoucherDate As Date,
			MLedger.LedgerName,
			TVoucherEntry_1.BilledAmount,
						(SELECT     isNull(SUM(CASE WHEN (Debit <> 0) THEN Debit ELSE Credit END),0) 
						FROM          TVoucherEntry INNER JOIN
						TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo
						WHERE      (TVoucherDetails.LedgerNo IN
						(SELECT LedgNo FROM @TDisc )) AND (TVoucherDetails.FkVoucherNo = TVoucherEntry_1.PkVoucherNo)) AS Disc ,
						(SELECT     isNull(SUM(CASE WHEN (Debit <> 0) THEN Debit ELSE Credit END),0) 
						FROM          TVoucherEntry AS TVoucherEntry_3 INNER JOIN
						TVoucherDetails AS TVoucherDetails_3 ON TVoucherEntry_3.PkVoucherNo = TVoucherDetails_3.FkVoucherNo
						WHERE      (TVoucherDetails_3.LedgerNo IN
						(@RoundOffLedgNo)) AND (TVoucherDetails_3.FkVoucherNo = TVoucherEntry_1.PkVoucherNo)) As RndOff ,
						(SELECT     isNull(SUM(CASE WHEN (Debit <> 0) THEN Debit ELSE Credit END),0)
						FROM          TVoucherEntry AS TVoucherEntry_2 INNER JOIN
						TVoucherDetails AS TVoucherDetails_2 ON TVoucherEntry_2.PkVoucherNo = TVoucherDetails_2.FkVoucherNo
						WHERE      (TVoucherDetails_2.LedgerNo IN
						(SELECT LedgNo FROM @TChrg)) AND (TVoucherDetails_2.FkVoucherNo = TVoucherEntry_1.PkVoucherNo)) As Charges,
						(SELECT     isNull(SUM(CASE WHEN (Debit <> 0) THEN Debit ELSE Credit END),0)
						FROM          TVoucherEntry AS TVoucherEntry_4 INNER JOIN
						TVoucherDetails AS TVoucherDetails_4 ON TVoucherEntry_4.PkVoucherNo = TVoucherDetails_4.FkVoucherNo
						WHERE      (TVoucherDetails_4.LedgerNo IN(@ChrgTaxAcc)) AND (TVoucherDetails_4.FkVoucherNo = TVoucherEntry_1.PkVoucherNo)) As ChrgTaxAmt,

SUM(TStock.NetAmount) AS SAmt,
						SUM(TStock.TaxAmount)  AS TAmt
			,TStock.TaxPercentage,
			Sum(TStock.IGSTAmount) as IGSTAmount,TStock.IGSTPercent ,Sum( TStock.CGSTAmount ) as CGSTAmount,TStock.CGSTPercent  
			,Sum( TStock.SGSTAmount) As SGSTAmount,TStock.SGSTPercent, 
			Sum( TStock.CessAmount) AS CessAmount,TStock.CessPercent, MState.StateName,MLedgerDetails.CSTNo as GSTNo





	FROM TVoucherEntry AS TVoucherEntry_1 INNER JOIN
			TStock ON TVoucherEntry_1.PkVoucherNo = TStock.FKVoucherNo   INNER JOIN
			TVoucherDetails ON TVoucherEntry_1.PkVoucherNo = TVoucherDetails.FkVoucherNo INNER JOIN
			MLedger ON TVoucherDetails.LedgerNo = MLedger.LedgerNo And TVoucherDetails.SrNo=501 LEFT OUTER JOIN
			MLedgerDetails ON MLedger.LedgerNo = MLedgerDetails.LedgerNo LEFT OUTER JOIN
			MState ON MLedgerDetails.StateNo = MState.StateNo

	WHERE TVoucherEntry_1.VoucherTypeCode=@VchType	 and TVoucherEntry_1.VoucherDate>=@FromDate and TVoucherEntry_1.VoucherDate<=@ToDate  AND (TVoucherEntry_1.IsCancel = 'false')
			AND TVoucherEntry_1.TaxTypeNo= @TaxTypeNo
			Group by TVoucherEntry_1.VoucherUserNo,TVoucherEntry_1.VoucherDate,TStock.TaxPercentage,TVoucherEntry_1.PkVoucherNo,BilledAmount,MLedger.LedgerName, MState.StateName
			,TStock.IGSTPercent,TStock.CGSTPercent,TStock.SGSTPercent,TStock.CESSPercent,MLedgerDetails.CSTNo
			order by TVoucherEntry_1.VoucherUserNo,TStock.TaxPercentage			
Go
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/







