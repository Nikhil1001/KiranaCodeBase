USE [Kirana0001]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTaxDetailsGST_b2b]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetTaxDetailsGST_b2b]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Create PROCEDURE [dbo].[GetTaxDetailsGST_b2b] 
	@FromDate datetime,
	@ToDate datetime,
	@VchType numeric(18)
AS
BEGIN

--Set @FromDate='1-July-2017'
--Set @ToDate='10-Aug-2017'
--Set @VchType=15

Declare @temptbl table (GSTIN varchar(50),VoucherUserNo numeric(18),VoucherDate datetime,Amount numeric(18,2),
PlaceOfSupply varchar(50),IGSTPercent numeric(18,2),IGSTAmount numeric(18,2),CGSTAmount numeric(18,2),SGSTAmount numeric(18,2),
NetAmount numeric(18,2),CessAmount  numeric(18,2))

Insert Into @temptbl
SELECT      MLedgerDetails.CSTNo AS GSTIN, TVoucherEntry.VoucherUserNo, TVoucherEntry.VoucherDate, 
(Select Sum( Amount) From TStock Where FkVoucherNo=TVoucherEntry.PkVoucherNo) AS Amount, 
MState.StateCodeGST+'-'+ MState.StateName As PlaceOfSupply,TStock.IGSTPercent,
SUM(TStock.IGSTAmount) AS IGSTAmount, SUM(TStock.CGSTAmount) AS CGSTAmount, SUM(TStock.SGSTAmount) AS SGSTAmount,
SUM(TStock.NetAmount) AS NetAmount, SUM(TStock.CessAmount) AS CessAmount
                      
FROM         TStock INNER JOIN
                      TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo AND TVoucherEntry.VoucherTypeCode = @VchType 
					  AND TVoucherEntry.TaxTypeNo = 38 AND 
                      TVoucherEntry.IsCancel = 'False' AND TStock.IGSTPercent <> 0 INNER JOIN
                      TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo AND TVoucherDetails.SrNo = 501 INNER JOIN
                      MLedger ON TVoucherDetails.LedgerNo = MLedger.LedgerNo INNER JOIN
                      MLedgerDetails ON MLedger.LedgerNo = MLedgerDetails.LedgerNo INNER JOIN
                      MState ON MLedgerDetails.StateNo = MState.StateNo
WHERE     (TVoucherEntry.VoucherDate >= @FromDate) AND (TVoucherEntry.VoucherDate < @ToDate) AND (ISNULL(MLedgerDetails.CSTNo, '') <> '')
GROUP BY  MLedgerDetails.CSTNo, TVoucherEntry.VoucherUserNo, TVoucherEntry.VoucherDate, MState.StateName, MState.StateCodeGST, TStock.IGSTPercent, 
                      TStock.CessPercent,TVoucherEntry.PkVoucherNo
ORDER BY  TVoucherEntry.VoucherUserNo

--Table 1 Header
Declare @TotalNoOfRe numeric(18), @TotalBill numeric(18),@TotalInvValue numeric(18,2),@TotalTaxValue numeric(18,2),@TotalCessValue numeric(18,2),
@TotalIGSAmt numeric(18,2),@CGSTAmount numeric(18,2),@SGSTAmount numeric(18,2)
Select @TotalNoOfRe=Count(Distinct(GSTIN)) From @temptbl 
Select @TotalBill=Count(Distinct(VoucherUserNo)) From @temptbl 
Select @TotalInvValue = ISNULL(Sum( Amount),0) From  ( Select Distinct VoucherUserNo ,Amount From @temptbl  ) As tbl 
Select @TotalTaxValue=ISNULL(Sum(NetAmount),0),@TotalCessValue=ISNULL(Sum(CessAmount),0),
@TotalIGSAmt=ISNULL(Sum(IGSTAmount),0),@CGSTAmount=ISNULL(Sum(CGSTAmount),0),@SGSTAmount=ISNULL(Sum(SGSTAmount),0) From @temptbl
Select @TotalNoOfRe , @TotalBill ,@TotalInvValue ,@TotalTaxValue ,@TotalCessValue,@TotalIGSAmt,@CGSTAmount,@SGSTAmount

--Table 2 Data

Select GSTIN ,VoucherUserNo ,VoucherDate ,Amount ,PlaceOfSupply ,
'N' AS ReverseCharge,'R' AS InvoiceType, '' AS ECommerce,IGSTPercent,IGSTAmount,CGSTAmount,SGSTAmount,NetAmount ,CessAmount 
From @temptbl
End

GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTaxDetailsGST_b2cl]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetTaxDetailsGST_b2cl]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Create PROCEDURE [dbo].[GetTaxDetailsGST_b2cl] 
	@FromDate datetime,
	@ToDate datetime,
	@VchType numeric(18)
AS
BEGIN

--Set @FromDate='1-July-2017'
--Set @ToDate='10-Aug-2017'
--Set @VchType=15

Declare @temptbl table (VoucherUserNo numeric(18),VoucherDate datetime,Amount numeric(18,2),PlaceOfSupply varchar(50),
IGSTPercent numeric(18,2),IGSTAmount numeric(18,2),CGSTAmount numeric(18,2),SGSTAmount numeric(18,2),NetAmount numeric(18,2),CessAmount  numeric(18,2))

DECLARE @CompStateNo numeric(18)

SELECT TOP 1 @CompStateNo = StateNo FROM MCompany

Insert Into @temptbl
SELECT      TVoucherEntry.VoucherUserNo, TVoucherEntry.VoucherDate, (Select Sum( Amount) From TStock Where FkVoucherNo=TVoucherEntry.PkVoucherNo) AS Amount, 
MState.StateCodeGST+'-'+ MState.StateName As PlaceOfSupply,TStock.IGSTPercent,
SUM(TStock.IGSTAmount) AS IGSTAmount, SUM(TStock.CGSTAmount) AS CGSTAmount, SUM(TStock.SGSTAmount) AS SGSTAmount,
SUM(TStock.NetAmount) AS NetAmount, SUM(TStock.CessAmount) AS CessAmount                      
FROM         TStock INNER JOIN
                      TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo AND TVoucherEntry.VoucherTypeCode = @VchType 
					  AND TVoucherEntry.TaxTypeNo = 38 AND 
                      TVoucherEntry.IsCancel = 'False' AND TVoucherEntry.BilledAmount > 250000 AND TStock.IGSTPercent <> 0 INNER JOIN
                      TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo AND TVoucherDetails.SrNo = 501 INNER JOIN
                      MLedger ON TVoucherDetails.LedgerNo = MLedger.LedgerNo INNER JOIN
                      MLedgerDetails ON MLedger.LedgerNo = MLedgerDetails.LedgerNo 
						AND MLedgerDetails.StateNo <> @CompStateNo AND ISNULL(MLedgerDetails.CSTNo, '') = '' INNER JOIN
                      MState ON MLedgerDetails.StateNo = MState.StateNo
WHERE     (TVoucherEntry.VoucherDate >= @FromDate) AND (TVoucherEntry.VoucherDate < @ToDate)
		  
GROUP BY  MLedgerDetails.CSTNo, TVoucherEntry.VoucherUserNo, TVoucherEntry.VoucherDate, MState.StateName, MState.StateCodeGST, TStock.IGSTPercent, 
                      TStock.CessPercent,TVoucherEntry.PkVoucherNo
ORDER BY  TVoucherEntry.VoucherUserNo

--Table 1 Header

Declare @TotalBill numeric(18),@TotalInvValue numeric(18,2),@TotalTaxValue numeric(18,2),@TotalCessValue numeric(18,2),
@TotalIGSAmt numeric(18,2),@CGSTAmount numeric(18,2),@SGSTAmount numeric(18,2)
Select @TotalBill=Count(Distinct(VoucherUserNo)) From @temptbl 
Select @TotalInvValue = ISNULL(Sum( Amount),0) From  ( Select Distinct VoucherUserNo ,Amount From @temptbl  ) As tbl 
Select @TotalTaxValue=ISNULL(Sum(NetAmount),0),@TotalCessValue=ISNULL(Sum(CessAmount),0),@TotalIGSAmt=ISNULL(Sum(IGSTAmount),0),@CGSTAmount=ISNULL(Sum(CGSTAmount),0),@SGSTAmount=ISNULL(Sum(SGSTAmount),0)  From @temptbl
Select @TotalBill ,@TotalInvValue ,@TotalTaxValue ,@TotalCessValue ,@TotalIGSAmt,@CGSTAmount,@SGSTAmount

--Table 2 Data

Select VoucherUserNo ,VoucherDate ,Amount ,PlaceOfSupply ,IGSTPercent,IGSTAmount,CGSTAmount,SGSTAmount,NetAmount ,CessAmount ,'' AS ECommerce

From @temptbl

End
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTaxDetailsGST_b2cs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetTaxDetailsGST_b2cs]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Create PROCEDURE [dbo].[GetTaxDetailsGST_b2cs] 
	@FromDate datetime,
	@ToDate datetime,
	@VchType numeric(18)
AS
BEGIN

--Set @FromDate='1-July-2017'
--Set @ToDate='10-Aug-2017'
--Set @VchType=15

DECLARE @CompStateNo numeric(18),@VarState varchar(500)

SELECT TOP 1 @CompStateNo = StateNo FROM MCompany
Select @VarState= ISNULL(StateCodeGST,'') + '-' + ISNULL(StateName,'')   From MState Where StateNo= @CompStateNo

SELECT  'OE' as Type ,(Case When(MState.StateName is NULL) then @VarState else ISNULL(MState.StateCodeGST,'') + '-' + ISNULL(MState.StateName,'')  End )As PlaceOfSupply,
TStock.IGSTPercent,
SUM(TStock.IGSTAmount) AS IGSTAmount, SUM(TStock.CGSTAmount) AS CGSTAmount, SUM(TStock.SGSTAmount) AS SGSTAmount,
          SUM(TStock.NetAmount) AS NetAmount, SUM(TStock.CessAmount) AS CessAmount, '' AS ECommerce
FROM         TStock INNER JOIN
                      TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo AND TVoucherEntry.VoucherTypeCode = @VchType 
					  AND TVoucherEntry.TaxTypeNo = 38 AND 
                      TVoucherEntry.IsCancel = 'False' AND TStock.IGSTPercent <> 0 INNER JOIN
                      TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo AND TVoucherDetails.SrNo = 501 INNER JOIN
                      MLedger ON TVoucherDetails.LedgerNo = MLedger.LedgerNo INNER JOIN
                      MLedgerDetails ON MLedger.LedgerNo = MLedgerDetails.LedgerNo LEFT OUTER JOIN
                      MState ON MLedgerDetails.StateNo = MState.StateNo
WHERE     (TVoucherEntry.VoucherDate >= @FromDate AND TVoucherEntry.VoucherDate < @ToDate AND ISNULL(MLedgerDetails.CSTNo, '') = '')
AND (MState.StateName IS NULL OR MState.StateNo = @CompStateNo OR (MState.StateNo <> @CompStateNo AND TVoucherEntry.BilledAmount <= 250000))
			
GROUP BY  MState.StateName, MState.StateCodeGST, TStock.IGSTPercent, 
                      TStock.CessPercent

End
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTaxDetailsGST_HSN]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetTaxDetailsGST_HSN]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Create PROCEDURE [dbo].[GetTaxDetailsGST_HSN] 
	@FromDate datetime,
	@ToDate datetime,
	@VchType numeric(18)
AS
BEGIN

SELECT     TStock.HSNCode, MStockGroup.StockGroupName + ' ' + MStockItems.ItemName AS ItemName, MUOM.UOMName, SUM(TStock.Quantity) AS TotalQuantity, SUM(TStock.Amount) AS TotalValue, 
                      SUM(TStock.NetAmount) AS TaxableAmount,
					  SUM(TStock.IGSTAmount) AS IGSTAmount, SUM(TStock.CGSTAmount) AS CGSTAmount, SUM(TStock.SGSTAmount) AS SGSTAmount, 
                      SUM(TStock.CessAmount) AS CessAmount
FROM         TStock INNER JOIN
                      TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo AND TVoucherEntry.VoucherTypeCode = @VchType 
					  AND TVoucherEntry.TaxTypeNo = 38 AND 
                      TVoucherEntry.IsCancel = 'False' INNER JOIN
                      MStockItems ON TStock.ItemNo = MStockItems.ItemNo  INNER JOIN
                      MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo INNER JOIN
                      MUOM ON TStock.FkUomNo = MUOM.UOMNo
WHERE     (TVoucherEntry.VoucherDate >= @FromDate) AND (TVoucherEntry.VoucherDate < @ToDate)
GROUP BY TStock.HSNCode, TStock.ItemNo, MStockGroup.StockGroupName, MStockItems.ItemName, MUOM.UOMName
END

GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTaxDetailsGST_Nil]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetTaxDetailsGST_Nil]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Create PROCEDURE [dbo].[GetTaxDetailsGST_Nil] 
	@FromDate datetime,
	@ToDate datetime,
	@VchType numeric(18)
AS
BEGIN

--Set @FromDate='1-July-2017'
--Set @ToDate='10-Aug-2017'
--Set @VchType=15
DECLARE @CompStateNo numeric(18),@VarState varchar(500)
SELECT TOP 1 @CompStateNo = StateNo FROM MCompany
Select @VarState= ISNULL(StateCodeGST,'') + '-' + ISNULL(StateName,'')   From MState Where StateNo= @CompStateNo

Declare @temptbl table (PlaceOfSupply varchar(50),Type varchar(50),NetAmount numeric(18,2))

--b2b
Insert Into @temptbl
SELECT     MState.StateCodeGST + '-' + MState.StateName AS PlaceOfSupply, 'b2b' AS Type, SUM(TStock.NetAmount) AS NetAmount
FROM         TStock INNER JOIN
                      TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo AND TVoucherEntry.VoucherTypeCode = @VchType AND TVoucherEntry.TaxTypeNo = 38 AND 
                      TVoucherEntry.IsCancel = 'False' AND TStock.IGSTPercent = 0 INNER JOIN
                      TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo AND TVoucherDetails.SrNo = 501 INNER JOIN
                      MLedger ON TVoucherDetails.LedgerNo = MLedger.LedgerNo INNER JOIN
                      MLedgerDetails ON MLedger.LedgerNo = MLedgerDetails.LedgerNo INNER JOIN
                      MState ON MLedgerDetails.StateNo = MState.StateNo
WHERE     (TVoucherEntry.VoucherDate >= @FromDate) AND (TVoucherEntry.VoucherDate < @ToDate)
GROUP BY MState.StateCodeGST + '-' + MState.StateName

--b2cl
Insert Into @temptbl
SELECT     MState.StateCodeGST + '-' + MState.StateName AS PlaceOfSupply, 'b2cl' AS type, SUM(TStock.NetAmount) AS NetAmount
FROM         TStock INNER JOIN
                      TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo  AND TVoucherEntry.VoucherTypeCode = @VchType AND TVoucherEntry.TaxTypeNo = 38 AND 
                      TVoucherEntry.IsCancel = 'False' AND TStock.IGSTPercent = 0 AND TVoucherEntry.BilledAmount > 250000 INNER JOIN
                      TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo AND TVoucherDetails.SrNo = 501 INNER JOIN
                      MLedger ON TVoucherDetails.LedgerNo = MLedger.LedgerNo INNER JOIN
                      MLedgerDetails ON MLedger.LedgerNo = MLedgerDetails.LedgerNo AND MLedgerDetails.StateNo <> @CompStateNo 
					  AND ISNULL(MLedgerDetails.CSTNo, '') = '' INNER JOIN
                      MState ON MLedgerDetails.StateNo = MState.StateNo
WHERE     (TVoucherEntry.VoucherDate >= @FromDate) AND (TVoucherEntry.VoucherDate < @ToDate)
GROUP BY MState.StateCodeGST + '-' + MState.StateName

--B2cs

Insert Into @temptbl
SELECT     (CASE WHEN (MState.StateName IS NULL) THEN @VarState ELSE ISNULL(MState.StateCodeGST, '') + '-' + ISNULL(MState.StateName, '') END) AS PlaceOfSupply, 'b2cs' AS Type, 
                      SUM(TStock.NetAmount) AS NetAmount
FROM         TStock INNER JOIN
                      TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo AND TVoucherEntry.VoucherTypeCode = @VchType AND TVoucherEntry.TaxTypeNo = 38 AND 
                      TVoucherEntry.IsCancel = 'False'  AND TStock.IGSTPercent = 0 INNER JOIN
                      TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo AND TVoucherDetails.SrNo = 501 INNER JOIN
                      MLedger ON TVoucherDetails.LedgerNo = MLedger.LedgerNo INNER JOIN
                      MLedgerDetails ON MLedger.LedgerNo = MLedgerDetails.LedgerNo LEFT OUTER JOIN
                      MState ON MLedgerDetails.StateNo = MState.StateNo
WHERE     (TVoucherEntry.VoucherDate >= @FromDate) AND (TVoucherEntry.VoucherDate < @ToDate) AND (ISNULL(MLedgerDetails.CSTNo, '') = '') AND (MState.StateName IS NULL) OR
                      (TVoucherEntry.VoucherDate >= @FromDate) AND (TVoucherEntry.VoucherDate < @ToDate) AND (ISNULL(MLedgerDetails.CSTNo, '') = '') AND (MState.StateNo = @CompStateNo) OR
                      (TVoucherEntry.VoucherDate >= @FromDate) AND (TVoucherEntry.VoucherDate < @ToDate) AND (ISNULL(MLedgerDetails.CSTNo, '') = '') AND (MState.StateNo <> @CompStateNo) AND 
                      (TVoucherEntry.BilledAmount <= 250000)
GROUP BY (CASE WHEN (MState.StateName IS NULL) THEN @VarState ELSE ISNULL(MState.StateCodeGST, '') + '-' + ISNULL(MState.StateName, '') END)
Select * From @temptbl

End
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/

INSERT INTO [MMenuMaster]
           ([SrNo]
           ,[MenuID]
           ,[MenuName]
           ,[ControlMenu]
           ,[NavigateURL]
           ,[IsChildNode]
           ,[IsAllow]
           ,[ShortCutKey]
           ,[ConstructorValue])
     VALUES
           (219
           ,13
           ,'GST Reports'
           ,6
           ,'Display.TaxDetailsGST'
           ,NULL
           ,1
           ,NULL
           ,15)
		   
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Update MSetting Set AppVersion='C7bJMfoHpqGT20picrlFCw=='
GO