USE [Kirana0001]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Create PROCEDURE [dbo].[GetTaxDetailsGST_3B] 
	@FromDate datetime,
	@ToDate datetime,
	@VchType numeric(18),
	@TaxTypeNo	numeric(18)

AS
Select S.FkVoucherNo,IGSTPercent,
Sum(S.IGSTAmount) As IGSTAmount,
Sum( S.CGSTAmount ) As  CGSTAmount,
Sum( S.SGSTAmount ) AS SGSTAmount,
Sum( S.CessAmount ) As CessAmount,
Sum( S.NetAmount) As NetAmount
 From  TStock As S Inner Join  TVoucherEntry ON TVoucherEntry.PkVoucherNo=S.FkVoucherNo
Where TVoucherEntry.VoucherTypeCode=@VchType and TVoucherEntry.VoucherDate>=@FromDate and TVoucherEntry.VoucherDate<=@ToDate 
 AND (TVoucherEntry.IsCancel = 'false')
			AND TVoucherEntry.TaxTypeNo= @TaxTypeNo
Group By IGSTPercent,FkVoucherNo
order by FkVoucherNo

GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Update mSetting Set AppVersion='+lRShJR1dVowISbNd/OjSA=='
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
INSERT INTO [MSettings]
           ([SettingKeyCode]
           ,[SettingTypeNo]
           ,[SettingValue]
           ,[SettingDescription])
     VALUES
           ('S_StopOnDisc'
           ,1
           ,'False'
           ,NULL)
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
INSERT INTO [MSettings]
           ([SettingKeyCode]
           ,[SettingTypeNo]
           ,[SettingValue]
           ,[SettingDescription])
     VALUES
           ('O_AddvanceSearch'
           ,1
           ,'False'
           ,NULL)
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/