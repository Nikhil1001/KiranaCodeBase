USE [Kirana0001]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
INSERT INTO [MMenuMaster]
           ([SrNo]
           ,[MenuID]
           ,[MenuName]
           ,[ControlMenu]
           ,[NavigateURL]
           ,[IsChildNode]
           ,[IsAllow]
           ,[ShortCutKey]
           ,[ConstructorValue])
     VALUES
           (221
           ,16
           ,'Group Wise Sale Reports'
           ,68
           ,'Display.GroupWiseSaleReports'
           ,NULL
           ,1
           ,NULL
           ,15)
Go
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
INSERT INTO [MMenuMaster]
           ([SrNo]
           ,[MenuID]
           ,[MenuName]
           ,[ControlMenu]
           ,[NavigateURL]
           ,[IsChildNode]
           ,[IsAllow]
           ,[ShortCutKey]
           ,[ConstructorValue])
     VALUES
           (222
           ,12
           ,'Group Wise Purchase Reports'
           ,76
           ,'Display.GroupWiseSaleReports'
           ,NULL
           ,1
           ,NULL
           ,9)
Go
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetGroupWiseSaleReports]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetGroupWiseSaleReports]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Create PROCEDURE [dbo].[GetGroupWiseSaleReports]
@FromDate     datetime,
@ToDate      datetime,
@VchType     Varchar,
@grStr      Varchar(max)

AS 

Declare @StrQry varchar(max)
set @StrQry=''

Set @StrQry='SELECT     MStockGroup.StockGroupNo, MStockGroup.StockGroupName, MStockItems.ItemNo, MStockItems.ShowItemName AS ItemName, SUM(TStock.BilledQuantity) AS Qty, SUM(TStock.Amount) AS Amount
FROM         TStock INNER JOIN
                      MStockItems ON TStock.ItemNo = MStockItems.ItemNo INNER JOIN
                      MStockGroup ON MStockItems.FkDepartmentNo = MStockGroup.StockGroupNo INNER JOIN
                      TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo
WHERE    (TVoucherEntry.VoucherDate >= '''+cast(@FromDate as varchar)+''') AND 
        (TVoucherEntry.VoucherDate <= '''+cast(@ToDate as varchar)+''')  and TVoucherEntry.IsCancel=''false''   And  
(MStockGroup.StockGroupNo in ('+@grStr+')) 

GROUP BY MStockGroup.StockGroupNo, MStockGroup.StockGroupName, MStockItems.ItemNo, MStockItems.ShowItemName '

Exec(@StrQry)

GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Update MSetting Set AppVersion='WQfxKU4Xoyve8zCKGyMrCw=='
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
