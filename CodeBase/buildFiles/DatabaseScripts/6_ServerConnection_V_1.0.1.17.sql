USE [Kirana0001]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_MFirmAccount
	(
	FirmNo numeric(18, 0) NOT NULL IDENTITY (1, 1),
	ComanyName varchar(500) NULL,
	DataBaseName varchar(50) NULL,
	ServerName varchar(50) NULL,
	IsActive bit NULL
	)  ON [PRIMARY]
GO
SET IDENTITY_INSERT dbo.Tmp_MFirmAccount ON
GO
IF EXISTS(SELECT * FROM dbo.MFirmAccount)
	 EXEC('INSERT INTO dbo.Tmp_MFirmAccount (FirmNo, ComanyName, DataBaseName, IsActive)
		SELECT FirmNo, ComanyName, DataBaseName, IsActive FROM dbo.MFirmAccount WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_MFirmAccount OFF
GO
DROP TABLE dbo.MFirmAccount
GO
EXECUTE sp_rename N'dbo.Tmp_MFirmAccount', N'MFirmAccount', 'OBJECT' 
GO
ALTER TABLE dbo.MFirmAccount ADD CONSTRAINT
	PK_MFirmAccount PRIMARY KEY CLUSTERED 
	(
	FirmNo
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
Go
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/ 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTaxDetailsGST_Nil]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetTaxDetailsGST_Nil]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[GetTaxDetailsGST_Nil] 
	@FromDate datetime,
	@ToDate datetime,
	@VchType numeric(18)
AS
BEGIN

--Set @FromDate='1-July-2017'
--Set @ToDate='10-Aug-2017'
--Set @VchType=15
DECLARE @CompStateNo numeric(18),@VarState varchar(500)
SELECT TOP 1 @CompStateNo = StateNo FROM MCompany
Select @VarState= ISNULL(StateCodeGST,'') + '-' + ISNULL(StateName,'')   From MState Where StateNo= @CompStateNo

Declare @temptbl table (PlaceOfSupply varchar(50),Type varchar(50),NetAmount numeric(18,2))

--b2b
Insert Into @temptbl
SELECT     MState.StateCodeGST + '-' + MState.StateName AS PlaceOfSupply, 'b2b' AS Type, SUM(TStock.NetAmount) AS NetAmount
FROM         TStock INNER JOIN
                      TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo AND TVoucherEntry.VoucherTypeCode = @VchType
					  AND TVoucherEntry.TaxTypeNo = 38 AND 
                      TVoucherEntry.IsCancel = 'False' AND TStock.IGSTPercent = 0 INNER JOIN
                      TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo AND TVoucherDetails.SrNo = 501 INNER JOIN
                      MLedger ON TVoucherDetails.LedgerNo = MLedger.LedgerNo INNER JOIN
                      MLedgerDetails ON MLedger.LedgerNo = MLedgerDetails.LedgerNo INNER JOIN
                      MState ON MLedgerDetails.StateNo = MState.StateNo
WHERE     (TVoucherEntry.VoucherDate >= @FromDate) AND (TVoucherEntry.VoucherDate < @ToDate) AND (ISNULL(MLedgerDetails.CSTNo, '') <> '')
GROUP BY MState.StateCodeGST + '-' + MState.StateName

--b2cl
Insert Into @temptbl
SELECT     MState.StateCodeGST + '-' + MState.StateName AS PlaceOfSupply, 'b2cl' AS type, SUM(TStock.NetAmount) AS NetAmount
FROM         TStock INNER JOIN
                      TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo  AND TVoucherEntry.VoucherTypeCode = @VchType AND TVoucherEntry.TaxTypeNo = 38 AND 
                      TVoucherEntry.IsCancel = 'False' AND TStock.IGSTPercent = 0 INNER JOIN
                      TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo AND TVoucherDetails.SrNo = 501 INNER JOIN
                      MLedger ON TVoucherDetails.LedgerNo = MLedger.LedgerNo INNER JOIN
                      MLedgerDetails ON MLedger.LedgerNo = MLedgerDetails.LedgerNo INNER JOIN
                      MState ON MLedgerDetails.StateNo = MState.StateNo
WHERE     (TVoucherEntry.VoucherDate >= @FromDate) AND (TVoucherEntry.VoucherDate < @ToDate)
           AND (TVoucherEntry.BilledAmount > 250000) AND MLedgerDetails.StateNo <> @CompStateNo 
	       AND (ISNULL(MLedgerDetails.CSTNo, '') = '')
GROUP BY MState.StateCodeGST + '-' + MState.StateName

--B2cs

Insert Into @temptbl
SELECT     (CASE WHEN (MState.StateName IS NULL) THEN @VarState ELSE ISNULL(MState.StateCodeGST, '') + '-' + ISNULL(MState.StateName, '') END) AS PlaceOfSupply, 'b2cs' AS Type, 
                      SUM(TStock.NetAmount) AS NetAmount
FROM         TStock INNER JOIN
                      TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo AND TVoucherEntry.VoucherTypeCode = @VchType AND TVoucherEntry.TaxTypeNo = 38 AND 
                      TVoucherEntry.IsCancel = 'False'  AND TStock.IGSTPercent = 0 INNER JOIN
                      TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo AND TVoucherDetails.SrNo = 501 INNER JOIN
                      MLedger ON TVoucherDetails.LedgerNo = MLedger.LedgerNo INNER JOIN
                      MLedgerDetails ON MLedger.LedgerNo = MLedgerDetails.LedgerNo LEFT OUTER JOIN
                      MState ON MLedgerDetails.StateNo = MState.StateNo
WHERE     (TVoucherEntry.VoucherDate >= @FromDate) AND (TVoucherEntry.VoucherDate < @ToDate) 
           AND (ISNULL(MLedgerDetails.CSTNo, '') = '') AND 
			(
             (MState.StateName IS NULL) 
			  OR 
             (MState.StateNo = @CompStateNo)
              OR
			 ((MState.StateNo <> @CompStateNo) AND (TVoucherEntry.BilledAmount <= 250000))
            )

GROUP BY (CASE WHEN (MState.StateName IS NULL) THEN @VarState ELSE ISNULL(MState.StateCodeGST, '') + '-' + ISNULL(MState.StateName, '') END)
Select * From @temptbl

End
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTaxDetailsGST_b2cs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetTaxDetailsGST_b2cs]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[GetTaxDetailsGST_b2cs] 
	@FromDate datetime,
	@ToDate datetime,
	@VchType numeric(18)
AS
BEGIN

--Set @FromDate='1-July-2017'
--Set @ToDate='10-Aug-2017'
--Set @VchType=15

DECLARE @CompStateNo numeric(18),@VarState varchar(500),@VarStateName varchar(500),@VarStateCode varchar(500)

SELECT TOP 1 @CompStateNo = StateNo FROM MCompany

Select @VarState= ISNULL(StateCodeGST,'') + '-' + ISNULL(StateName,''), @VarStateName= ISNULL(StateName,''),
		@VarStateCode= ISNULL(StateCodeGST,'')
 From MState Where StateNo= @CompStateNo

SELECT  'OE' as Type ,
(ISNULL(MState.StateCodeGST,@VarStateCode) + '-' + ISNULL(MState.StateName,@VarStateName)) As PlaceOfSupply,
TStock.IGSTPercent,
SUM(TStock.IGSTAmount) AS IGSTAmount, SUM(TStock.CGSTAmount) AS CGSTAmount, SUM(TStock.SGSTAmount) AS SGSTAmount,
          SUM(TStock.NetAmount) AS NetAmount, SUM(TStock.CessAmount) AS CessAmount, '' AS ECommerce
FROM         TStock INNER JOIN
                      TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo AND TVoucherEntry.VoucherTypeCode = @VchType 
					  AND TVoucherEntry.TaxTypeNo = 38 AND 
                      TVoucherEntry.IsCancel = 'False' AND TStock.IGSTPercent <> 0 INNER JOIN
                      TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo AND TVoucherDetails.SrNo = 501 INNER JOIN
                      MLedger ON TVoucherDetails.LedgerNo = MLedger.LedgerNo INNER JOIN
                      MLedgerDetails ON MLedger.LedgerNo = MLedgerDetails.LedgerNo LEFT OUTER JOIN
                      MState ON MLedgerDetails.StateNo = MState.StateNo
WHERE     (TVoucherEntry.VoucherDate >= @FromDate AND TVoucherEntry.VoucherDate < @ToDate AND ISNULL(MLedgerDetails.CSTNo, '') = '')
AND (MState.StateName IS NULL OR MState.StateNo = @CompStateNo OR (MState.StateNo <> @CompStateNo AND TVoucherEntry.BilledAmount <= 250000))
			
GROUP BY  ISNULL(MState.StateName,@VarStateName), ISNULL(MState.StateCodeGST,@VarStateCode), TStock.IGSTPercent, 
                      TStock.CessPercent

End
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/

Update MSetting Set AppVersion='4wCTBeRbVvYQlCdHcHmrlw=='

Go

/*---------------------------------------------------------------------------------------------------------------------------------------------------*/ 