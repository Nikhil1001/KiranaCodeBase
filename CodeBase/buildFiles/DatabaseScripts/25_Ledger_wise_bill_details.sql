USE [Kirana0001]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetLedgerWiseBillDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetLedgerWiseBillDetails]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[GetLedgerWiseBillDetails]
@LedgerNo				Varchar(max),
@FromDate				Datetime,
@ToDate					Datetime

AS

SELECT     TVoucherEntry.VoucherUserNo AS BillNo, TVoucherEntry.VoucherDate AS BillDate, TVoucherEntry.BilledAmount, MLedger.LedgerName, MLedger_1.LedgerName AS Party, 
                      TVoucherDetails.Debit + TVoucherDetails.Credit AS Amount, MVoucherType.VoucherTypeName
FROM         TVoucherDetails INNER JOIN
                      MLedger ON TVoucherDetails.LedgerNo = MLedger.LedgerNo INNER JOIN
                      TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN
                      TVoucherDetails AS TVoucherDetails_1 ON TVoucherEntry.PkVoucherNo = TVoucherDetails_1.FkVoucherNo INNER JOIN
                      MLedger AS MLedger_1 ON TVoucherDetails_1.LedgerNo = MLedger_1.LedgerNo AND TVoucherDetails_1.SrNo = 501 INNER JOIN
                      MVoucherType ON TVoucherEntry.VoucherTypeCode = MVoucherType.VoucherTypeCode
WHERE     (MLedger.LedgerNo IN
                          (SELECT     value
                            FROM          dbo.fn_Split(@LedgerNo, ',') AS fn_Split_1)) AND (TVoucherEntry.VoucherDate >= @FromDate) AND (TVoucherEntry.VoucherDate <= @ToDate) AND (TVoucherEntry.IsCancel = 'False')
ORDER BY Party,MVoucherType.VoucherTypeName, BillDate, BillNo

Go
