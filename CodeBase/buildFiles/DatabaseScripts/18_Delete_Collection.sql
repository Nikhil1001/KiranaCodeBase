USE [Kirana0001]
GO

/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteCollectionDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DeleteCollectionDetails]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Create procedure [dbo].[DeleteCollectionDetails]
@ID			numeric(18)
As 
Declare @PkVoucherNo numeric(18)
Declare CurTemp Cursor for
SELECT    TVoucherDetails.FkVoucherNo
FROM         TVoucherRefDetails INNER JOIN
                      TVoucherDetails ON TVoucherRefDetails.FkVoucherTrnNo = TVoucherDetails.PkVoucherTrnNo
WHERE     (TVoucherRefDetails.RefNo IN
                          (SELECT     RefNo
                            FROM          TVoucherRefDetails AS TVoucherRefDetails_1
                            WHERE      (FkVoucherTrnNo IN
                                                       (SELECT     PkVoucherTrnNo
                                                         FROM          TVoucherDetails AS TVoucherDetails_1
                                                         WHERE      (FkVoucherNo = @ID))))) And TVoucherRefDetails.TypeOfRef=2
order by TVoucherDetails.FkVoucherNo Desc
Open  CurTemp
Fetch next from  CurTemp into  @PkVoucherNo
While(@@Fetch_Status=0)
Begin

			Exec DeleteAllVoucherEntry @PkVoucherNo

Fetch next from CurTemp into  @PkVoucherNo
End
Close CurTemp Deallocate CurTemp



	
