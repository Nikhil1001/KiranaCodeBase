USE [Kirana0001]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE TABLE [dbo].[MUpdateScriptDetails](
	[ScriptNo] [numeric](18, 0) NOT NULL,
	[ScriptName] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ScriptSql] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ScriptPath] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsScriptExecuted] [bit] NULL,
	[ScriptResult] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ExecutedOn] [datetime] NULL,
	[AppVersionNo] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_MUpdateScriptDetails] PRIMARY KEY NONCLUSTERED 
(
	[ScriptNo] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF

GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/

INSERT INTO MUpdateScriptDetails 
(ScriptNo, ScriptName, ScriptSql, ScriptPath,  IsScriptExecuted, ScriptResult, ExecutedOn, AppVersionNo) 
 Values
 (18, 'Delete_Collection',  '', '',  'False', 'OK', getDate(), '1.0.1.20')

GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
