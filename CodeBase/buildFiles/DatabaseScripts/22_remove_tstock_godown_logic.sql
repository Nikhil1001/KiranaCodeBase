USE [Kirana0001]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE [dbo].[TStock] ADD
	MRP									numeric(18,2)	NULL,
	GodownNo							numeric(18,0)	NULL	

GO
COMMIT
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AddTStock]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AddTStock]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[AddTStock]
     @PkStockTrnNo                        numeric(18),
	 @FKVoucherNo						  numeric(18),
     @FkVoucherTrnNo                      numeric(18),
     @FkVoucherSrNo                       numeric(18),
     @GroupNo                             numeric(18),
     @ItemNo                              numeric(18),
     @TrnCode                             numeric(18),
     @Quantity                            numeric(18,3),
     @BilledQuantity                      numeric(18,2),
     @Rate                                numeric(18,2),
     @Amount                              numeric(18,2),
	 @NetRate							  numeric(18,2),
	 @NetAmount							  numeric(18,2),
     @TaxPercentage                       numeric(18,2),
     @TaxAmount                           numeric(18,2),
	 @DiscPercentage					  numeric(18,2),
	 @DiscAmount						  numeric(18,2),
	 @DiscRupees						  numeric(18,2),
	 @DiscPercentage2					  numeric(18,2),
	 @DiscAmount2						  numeric(18,2),
	 @DiscRupees2						  numeric(18,2),
	 @FkUomNo							  numeric(18),
	 @FkStockBarCodeNo				      numeric(18),
	 @FkRateSettingNo					  numeric(18),
	 @FkItemTaxInfo						  numeric(18),
	 @FreeQty                             numeric(18,2),
	 @FreeUOMNo							  numeric(18),   
     @UserID                              numeric(18),
     @UserDate                            datetime,
	 @CompanyNo							  numeric(18),
	 @LandedRate						  numeric(18,2),
	 @DiscountType						  numeric(18),
	 @HamaliInKg						  numeric(18,2),
	 @IGSTPercent						  numeric(18,2),
	 @IGSTAmount						  numeric(18,3),
	 @CGSTPercent						  numeric(18,2),
	 @CGSTAmount						  numeric(18,3),
	 @SGSTPercent						  numeric(18,2),
	 @SGSTAmount						  numeric(18,3),
	 @UTGSTPercent						  numeric(18,2),
	 @UTGSTAmount						  numeric(18,3),
	 @CessPercent						  numeric(18,2),
	 @CessAmount						  numeric(18,3),
	 @HSNCode							  varchar(50),
	 @SchemeDetailsNo				      numeric(18,2),
	 @SchemeFromNo                        numeric(18,2),
	 @SchemeToNo                          numeric(18,2),
	 @MRP					              numeric(18,2),
	 @GodownNo					          numeric(18,0),
     @ReturnID							  int output
     
AS
IF EXISTS(select PkStockTrnNo from TStock
          where
          PkStockTrnNo = @PkStockTrnNo)
     BEGIN
       --Update existing row
       UPDATE TStock
       SET
		  FKVoucherNo = @FKVoucherNo,
          FkVoucherTrnNo = @FkVoucherTrnNo,
          FkVoucherSrNo = @FkVoucherSrNo,
          GroupNo = @GroupNo,
          ItemNo = @ItemNo,
          TrnCode = @TrnCode,
          Quantity = @Quantity,
          BilledQuantity = @BilledQuantity,
          Rate = @Rate,
          Amount = @Amount,
		  NetRate = @NetRate,
		  NetAmount = @NetAmount,
          TaxPercentage = @TaxPercentage,
          TaxAmount = @TaxAmount,
		  DiscPercentage=@DiscPercentage,
		  DiscAmount=@DiscAmount,
		  DiscRupees = @DiscRupees,
		  DiscPercentage2 =@DiscPercentage2,
		  DiscAmount2 = @DiscAmount2,
		  DiscRupees2 = @DiscRupees2,
	      FkUomNo=@FkUomNo,
	      FkStockBarCodeNo=@FkStockBarCodeNo,
	      FkRateSettingNo=@FkRateSettingNo,
	      FkItemTaxInfo=@FkItemTaxInfo,
		  FreeQty=@FreeQty,
	      FreeUOMNo=@FreeUOMNo,
          UserID = @UserID,
          UserDate = @UserDate,
		  CompanyNo= @CompanyNo,
		  LandedRate = @LandedRate,
		  StatusNo = 2,
		  DiscountType=@DiscountType,
	      HamaliInKg=@HamaliInKg,
		  IGSTPercent=@IGSTPercent,
		  IGSTAmount=@IGSTAmount,
          CGSTPercent=@CGSTPercent,
		  CGSTAmount=@CGSTAmount,
		  SGSTPercent=@SGSTPercent,
		  SGSTAmount=@SGSTAmount,
		  UTGSTPercent=@UTGSTPercent,
		  UTGSTAmount=@UTGSTAmount,
		  CessPercent=@CessPercent,
		  CessAmount=@CessAmount,
		  HSNCode=@HSNCode,      
		  SchemeDetailsNo=@SchemeDetailsNo,
          SchemeFromNo=@SchemeFromNo,
          SchemeToNo=@SchemeToNo,
		  MRP=@MRP,
		  GodownNo=@GodownNo,
          ModifiedBy = isnull(ModifiedBy,'') + cast(@UserID as varchar)+'@'+ CONVERT(VARCHAR(10), GETDATE(), 105)
       WHERE
          PkStockTrnNo = @PkStockTrnNo
		  set @ReturnID=@PkStockTrnNo   
		  
		  IF EXISTS(select PKStockGodownNo from TStockGodown
          where
          FkStockTrnNo = @PkStockTrnNo)
			 BEGIN
			   --Update existing row
			   UPDATE TStockGodown
			   SET
				  FKStockTrnNo = @ReturnID,
				  ItemNo = @ItemNo,
				  GodownNo = @GodownNo,
				  Qty = @Quantity,
				  ActualQty = @BilledQuantity,
				  UserID = @UserID,
				  UserDate = @UserDate,
				  ModifiedBy = isnull(ModifiedBy,'') + cast(@UserID as varchar)+'@'+ CONVERT(VARCHAR(10), GETDATE(), 105)
			   WHERE
				  FkStockTrnNo = @PkStockTrnNo
			 END

     END
ELSE
     BEGIN
       --Insert new row
       Declare @Id numeric
       SELECT @Id=IsNull(Max(PkStockTrnNo),0) From TStock
       DBCC CHECKIDENT('TStock', RESEED, @Id)
       INSERT INTO TStock(
		  FKVoucherNo,
          FkVoucherTrnNo,
          FkVoucherSrNo,
          GroupNo,
          ItemNo,
          TrnCode,
          Quantity,
          BilledQuantity,
          Rate,
          Amount,
		  NetRate,
		  NetAmount,
          TaxPercentage,
          TaxAmount,
		  DiscPercentage,
		  DiscAmount,
		  DiscRupees,
		  DiscPercentage2,
		  DiscAmount2,
		  DiscRupees2,
		  FkUomNo,
		  FkStockBarCodeNo,
		  FkRateSettingNo,
		  FkItemTaxInfo,
		  IsVoucherLock,
	      FreeQty,
		  FreeUOMNo,
          UserID,
          UserDate,
		  CompanyNo,
		  LandedRate,
		  StatusNo,
		  DiscountType,
	      HamaliInKg,
		  IGSTPercent,
		  IGSTAmount,
	      CGSTPercent,
	      CGSTAmount,
	      SGSTPercent,
	      SGSTAmount,
		  UTGSTPercent,
		  UTGSTAmount,
		  CessPercent,
		  CessAmount,
		  HSNCode,
		  SchemeDetailsNo,
		  SchemeFromNo,
		  SchemeToNo,
		  MRP,
		  GodownNo
          
)
       VALUES(
		  @FKVoucherNo,
          @FkVoucherTrnNo,
          @FkVoucherSrNo,
          @GroupNo,
          @ItemNo,
          @TrnCode,
          @Quantity,
          @BilledQuantity,
          @Rate,
		  @Amount,
		  @NetRate,
          @NetAmount,
          @TaxPercentage,
          @TaxAmount,
	      @DiscPercentage,
		  @DiscAmount,
		  @DiscRupees,
		  @DiscPercentage2,
		  @DiscAmount2,
		  @DiscRupees2,
		  @FkUomNo,
		  @FkStockBarCodeNo,
		  @FkRateSettingNo,
		  @FkItemTaxInfo,
		  'false',
	      @FreeQty,
		  @FreeUOMNo,
          @UserID,
          @UserDate,
		  @CompanyNo,
		  @LandedRate,
		  1,
		  @DiscountType,
	      @HamaliInKg,
		  @IGSTPercent,
		  @IGSTAmount,
		  @CGSTPercent,
		  @CGSTAmount,
		  @SGSTPercent,
		  @SGSTAmount,
		  @UTGSTPercent,
		  @UTGSTAmount,
		  @CessPercent,
		  @CessAmount,
		  @HSNCode,
		  @SchemeDetailsNo,
          @SchemeFromNo,
	      @SchemeToNo,
		  @MRP,
		  @GodownNo
          
)
Set @ReturnID=Scope_Identity()

INSERT INTO TStockGodown(          
          FKStockTrnNo,
		  ItemNo,
          GodownNo,
          Qty,
          ActualQty,
          UserID,
          UserDate
		)
       VALUES(
          @ReturnID,
		  @ItemNo,
          @GodownNo,
          @Quantity,
          @BilledQuantity,
          @UserID,
          @UserDate
)

END
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
--For MRP Update
UPDATE B SET B.MRP= A.MRP from TStock B inner join MrateSetting A on A.PkSrNo=B.fkRateSettingNo 
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
--For GodownNo Update
Update B set B.GodownNo = A.GodownNo from TStock B INNER JOIN TStockGodown A ON B.ItemNo=A.ItemNo AND A.FkStockTrnNo=B.PkStockTrnNo 
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteTStock]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DeleteTStock]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE  [dbo].[DeleteTStock] 
@PkStockTrnNo                       numeric(18)


AS

DELETE FROM TStockGodown 
       WHERE
          FkStockTrnNo = @PkStockTrnNo


DELETE FROM TStock 
       WHERE
          PkStockTrnNo = @PkStockTrnNo

GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetLedgerBookDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetLedgerBookDetails]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Create Procedure [dbo].[GetLedgerBookDetails]
@LedgerNo Varchar(Max),
@FromDate datetime,
@ToDate datetime

AS 

SELECT     TVoucherEntry.VoucherDate, TVoucherEntry.VoucherUserNo, MLedger.LedgerName, TVoucherEntry.Reference
			,case when TVoucherChqCreditDetails.ChequeNo = '' then null else TVoucherChqCreditDetails.ChequeNo end ChequeNo
			, TVoucherEntry.Remark
			, TVoucherDetails.Debit, TVoucherDetails.Credit, 
            TVoucherDetails.LedgerNo, TVoucherEntry.VoucherTypeCode, MVoucherType.VoucherTypeName
			,(TVoucherDetails.Credit- TVoucherDetails.Debit) as Diff,TVoucherEntry.PkVoucherno
FROM         MLedger 
			INNER JOIN TVoucherDetails ON MLedger.LedgerNo = TVoucherDetails.LedgerNo 
			INNER JOIN TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo 
			INNER JOIN MVoucherType ON TVoucherEntry.VoucherTypeCode = MVoucherType.VoucherTypeCode
			left join TVoucherChqCreditDetails on TVoucherEntry.PkVoucherNo  = TVoucherChqCreditDetails.FKVoucherNo
WHERE     (TVoucherDetails.LedgerNo IN(Select value From fn_Split(@LedgerNo,',') ) ) 
			AND (TVoucherEntry.VoucherDate >= @FromDate) 
			AND (TVoucherEntry.VoucherDate <= @ToDate)
			And TVoucherEntry.IsCancel='False'


UNION All
SELECT     @FromDate AS VoucherDate, null AS VoucherUserNo, MLedger.LedgerName, '' AS Reference,null asChequeNo, ''  AS Remark, null AS Debit,
    null AS Credit, 
                      MLedger.LedgerNo AS LedgerNo, 0 AS VoucherTypeCode, 'Op Bal.' AS VoucherTypeName, SUM(TVoucherDetails.Credit) - SUM(TVoucherDetails.Debit) AS Diff,TVoucherEntry.PkVoucherno
FROM         
                      TVoucherDetails INNER JOIN
                      TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo AND (TVoucherEntry.VoucherDate < @FromDate) RIGHT OUTER JOIN
MLedger  ON MLedger.LedgerNo = TVoucherDetails.LedgerNo 
WHERE     (MLedger.LedgerNo in(Select value From fn_Split(@LedgerNo,',')))
And TVoucherEntry.IsCancel='False'
GROUP BY MLedger.LedgerNo, MLedger.LedgerName,TVoucherEntry.PkVoucherno
ORDER BY LedgerName, LedgerNo, VoucherDate,TVoucherEntry.PkVoucherno

GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/