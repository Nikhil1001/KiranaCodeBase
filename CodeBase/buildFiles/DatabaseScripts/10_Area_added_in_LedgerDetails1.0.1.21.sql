USE [Kirana0001]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.MLedgerDetails ADD
	AreaNo numeric(18, 0) NULL
GO
COMMIT

GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AddMLedgerDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AddMLedgerDetails]
GO
/*---------------------------------------------------------------------------------------------------------------------------------------------------*/
Create PROCEDURE [dbo].[AddMLedgerDetails]
     @LedgerDetailsNo                     numeric(18),
     @LedgerNo                            numeric(18),
     @CreditLimit                         numeric(18,2),
     @Address                             varchar(300),
     @StateNo                             numeric(18),
     @CityNo                              numeric(18),
     @PinCode                             varchar(50),
     @PhNo1                               varchar(50),
     @PhNo2                               varchar(50),
     @MobileNo1                           varchar(50),
     @MobileNo2                           varchar(50),
     @EmailID                             varchar(50),
     @DOB                                 datetime,
     @QualificationNo                     numeric(18,0),
     @OccupationNo                        numeric(18,0),
     @CustomerType                        numeric(18,0),
     @PANNo                               varchar(50),
     @VATNo                               varchar(50),
     @CSTNo                               varchar(50),
     @UserID                              numeric(18),
     @UserDate                            datetime,
	 @Gender							  numeric(18),
	 @CompanyNo						      numeric(18),
	 @LangAddress						  Varchar(Max),
	 @AreaNo							  numeric(18)				
    
AS
IF EXISTS(select LedgerDetailsNo from MLedgerDetails
          where
          LedgerDetailsNo = @LedgerDetailsNo)
     BEGIN
       --Update existing row
       UPDATE MLedgerDetails
       SET
          LedgerNo = @LedgerNo,
          CreditLimit=@CreditLimit,
          Address = @Address,
	      StateNo=@StateNo,
          CityNo = @CityNo,
          PinCode = @PinCode,
          PhNo1 = @PhNo1,
          PhNo2 = @PhNo2,
          MobileNo1 = @MobileNo1,
          MobileNo2 = @MobileNo2,
          EmailID = @EmailID,
          DOB = @DOB,
          QualificationNo = @QualificationNo,
          OccupationNo = @OccupationNo,
          CustomerType = @CustomerType,
          PANNo=     @PANNo,
          VATNo=     @VATNo,
          CSTNo=     @CSTNo,
          UserID = @UserID,
          UserDate = @UserDate,
		  Gender=@Gender,
		  CompanyNo=@CompanyNo,
          ModifiedBy =  isnull(ModifiedBy,'') + cast(@UserID as varchar)+'@'+ CONVERT(VARCHAR(10), GETDATE(), 105),
          StatusNo=2,
		  LangAddress=@LangAddress,
		  AreaNo=@AreaNo
       WHERE
          LedgerDetailsNo = @LedgerDetailsNo

     END
ELSE
     BEGIN
       --Insert new row
       Declare @Id numeric
       SELECT @Id=IsNull(Max(LedgerDetailsNo),0) From MLedgerDetails
       DBCC CHECKIDENT('MLedgerDetails', RESEED, @Id)
       INSERT INTO MLedgerDetails(
          LedgerNo,
          CreditLimit,
          Address,
		  StateNo,
          CityNo,
          PinCode,
          PhNo1,
          PhNo2,
          MobileNo1,
          MobileNo2,
          EmailID,
          DOB,
          QualificationNo,
          OccupationNo,
          CustomerType,
          PANNo,
          VATNo,
          CSTNo,
          UserID,
          UserDate,
		  Gender,
          StatusNo,
		  CompanyNo,
          ConsentDate,
	      LangAddress,
		  AreaNo
)
       VALUES(
          @LedgerNo,
          @CreditLimit,
          @Address,
		  @StateNo,
          @CityNo,
          @PinCode,
          @PhNo1,
          @PhNo2,
          @MobileNo1,
          @MobileNo2,
          @EmailID,
          @DOB,
          @QualificationNo,
          @OccupationNo,
          @CustomerType,
          @PANNo,
          @VATNo,
          @CSTNo,
          @UserID,
          @UserDate,
		  @Gender,
          1,
		  @CompanyNo,
          '1-Jan-1900',
		  @LangAddress,
		  @AreaNo
)

END





