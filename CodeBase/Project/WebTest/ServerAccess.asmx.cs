﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Runtime.Serialization;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Net;
using OM;
using JitClass;
using System.Text;


namespace WebTest
{
    /// <summary>
    /// Summary description for JSonService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
   // [WebService(Namespace ="www.XMLWebServiceSoapHeaderAuth.net")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    //[WebServiceBinding(Name = "TestService", ConformsTo = WsiProfiles.BasicProfile1_1)]
    //[ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    //[System.Web.Script.Services.ScriptService]

    public class ServerAccess : System.Web.Services.WebService
    {

        public ServerAccess()
        {
            // DBConnection.SetConnection();
            //  Security ps = new Security();
            // string str = "SneXTNx4Zi+nE7lKX4cQhm2aLrDanTVLWtSArHo73CUtff/PQlKdxyMKC0VtnYdQSD2CkAqImwL/3X96H7kf1Txx7xZ5rnVYVoxgS9DHaAc=";
        }
        

        [WebMethod]
        public string SaveBills(string StrBills)
        {
            UCException Error = new UCException();
            try
            {
                CommonFunctions cc = new CommonFunctions();
                cc.SendMail("Save Bill","Sale Bill Data", StrBills,2);
               

                DTOClasses.UCSaleBillsList list = new WebTest.DTOClasses.UCSaleBillsList();
                List<DTOClasses.UCSaleBills> saleBillList = new JavaScriptSerializer().Deserialize<List<DTOClasses.UCSaleBills>>(StrBills.Replace("~", "&"));

                list.salesBills = saleBillList;
                DBTVoucherEntry dbTvoucherEntry = new DBTVoucherEntry();
                dbTvoucherEntry.SaveBills(list);
                Error.errorCode = ErrorCode.OK;
                Error.errorMessage = "Customer Details Saved..";
            }
            catch (Exception ex)
            {
                CommonFunctions cc = new CommonFunctions();
                cc.SendMail("Error In Save Bill",ex.Message,ex.StackTrace,1);
               
                Error.errorCode = ErrorCode.InternalError;
                Error.errorMessage = ex.Message.ToString();
            }
            return new JavaScriptSerializer().Serialize(Error);
        }

        [WebMethod]
        public string GetBill(string StrBillDetails)
        {
            UCException Error = new UCException();
            try
            {
                DTOClasses.UCTVoucherEntry lst_BillDetails = new JavaScriptSerializer().Deserialize<DTOClasses.UCTVoucherEntry>(StrBillDetails);

                DBTVoucherEntry dbTvoucherEntry = new DBTVoucherEntry();

                List<WebTest.DTOClasses.UCSaleBills> lst_List = new List<WebTest.DTOClasses.UCSaleBills>();
                lst_List = dbTvoucherEntry.SyncBills(lst_BillDetails);
                string StrData = "";
                if (lst_List != null)
                {
                    StrData = new JavaScriptSerializer().Serialize(lst_List);
                }
                else
                {
                    throw new Exception("Sale Data Not Found");
                }
                return StrData.Replace("&", "-");
            }
            catch (Exception ex)
            {
                Error.errorCode = 99;
                Error.errorMessage = ex.Message.ToString();
                return new JavaScriptSerializer().Serialize(Error);
            }
        }

        [WebMethod]
        public string GetBillCollection(string LedgerNo)
        {
            UCException Error = new UCException();
            try
            {
                DBTVoucherEntry dbTvoucherEntry = new DBTVoucherEntry();
                List<WebTest.DTOClasses.UCBillCollection> lst_List;
                lst_List = dbTvoucherEntry.SyncBillCollection(Convert.ToInt64(LedgerNo));
                string StrData = "";
                if (lst_List != null)
                {
                    StrData = new JavaScriptSerializer().Serialize(lst_List);
                }
                else
                {
                    throw new Exception("Bill Collection Data Not Found");
                }
                return StrData;
            }
            catch (Exception ex)
            {
                Error.errorCode = 99;
                Error.errorMessage = ex.Message.ToString();
                return new JavaScriptSerializer().Serialize(Error);
            }
        }

        [WebMethod]
        public string GetOutStandingDetails()
        {
            UCException Error = new UCException();
            try
            {
                DBTVoucherEntry dbTvoucherEntry = new DBTVoucherEntry();
                List<WebTest.DTOClasses.UCOutStanding> lst_List;
                lst_List = dbTvoucherEntry.SyncOutStandingDetails();
                string StrData = "";
                if (lst_List != null)
                {
                    StrData = new JavaScriptSerializer().Serialize(lst_List);
                }
                else
                {
                    throw new Exception("Out Standing Details Collection Data Not Found");
                }
                return StrData;
            }
            catch (Exception ex)
            {
                Error.errorCode = 99;
                Error.errorMessage = ex.Message.ToString();
                return new JavaScriptSerializer().Serialize(Error);
            }
        }

        [WebMethod]
        public string SaveCollection(string strDetails)
        {
            UCException Error = new UCException();
            try
            {
                DBTVoucherEntry dbTvoucherEntry = new DBTVoucherEntry();
                List<DTOClasses.UCBillCollection> saleBillList = new JavaScriptSerializer().Deserialize<List<DTOClasses.UCBillCollection>>(strDetails);
                dbTvoucherEntry.Save_Collection(saleBillList);
                Error.errorCode = ErrorCode.OK;
                Error.errorMessage = "Bill Collection Saved..";
            }
            catch (Exception ex)
            {
                Error.errorCode = ErrorCode.InternalError;
                Error.errorMessage = ex.Message.ToString();
            }
            return new JavaScriptSerializer().Serialize(Error);
        }
              
    }

}
