using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using JitClass;
using System.Web.Script.Serialization;
using System.Reflection;
using System.Net.Mail;
using System.IO;

/// <summary>
/// Summary description for CommonFunctions
/// </summary>
/// 
namespace OM
{

    public class CommonFunctions : JitClass.OMFunctions
    {
        //public static string ConStr = "Data Source=Nikhil-PC\\SQLEXPRESS;Initial Catalog=Test;User ID=Logicall;Password=Logicall";
        public static string ConStr = @"Data Source=184.168.194.70; Initial Catalog=Kirana0003; User ID=OM96102; Password='sparsh!@345'";
        DataSet dset = new DataSet();
        int i;
        public static long CompanyNo;
        GetDataSet objTrans = new GetDataSet();
        QueryOutPut objrate = new QueryOutPut();

        public CommonFunctions()
            : base()
        {
        }

        public CommonFunctions(bool bg)
            : base(bg)
        {
        }

        public string[] GetSplitParameter(string val)
        {
            string[] strSplitChar = { "," };
            return val.Split(strSplitChar, StringSplitOptions.None);
        }

        public string GetString(DataTable dt, int Col_Number)
        {
            string strSplitChar = "|||";
            string strReply = "";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                strReply += (i == 0 ? "" : "\n");
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    strReply += (j == 0 ? "" : strSplitChar);

                    if (dt.Columns[j].DataType == typeof(DateTime))
                    {
                        strReply += Convert.ToDateTime(dt.Rows[i][j].ToString()).ToString("dd/MM/yyyy hh:mm:ss tt");
                    }
                    else
                    {
                        strReply += dt.Rows[i][j].ToString();
                    }
                }
            }
            return strReply;
        }

        public void FillCom(DropDownList Cb)
        {
            //Only Select value is add to Dropdowlist Control
            Cb.Items.Clear();
            Cb.Items.Add(" ------ Select ------ ");
            Cb.Items[0].Value = System.Convert.ToString(0);
        }
        public void FillCombo(DropDownList Cb, string Str)
        {
            //The Fillcombo is used for filling of dropdownlist the qurey code is filled in value propery and name in text property 
            // Dim text As String
            //long cnt = 0;
            dset = objTrans.FillDset("NewTable", Str, DBConnection.GetConnection());
            i = 0;
            //cnt = 0;
            Cb.Items.Clear();
            Cb.Items.Add(" ------ Select ------ ");
            Cb.Items[0].Value = System.Convert.ToString(0);
            while (i <= dset.Tables[0].Rows.Count - 1)
            {
                if (dset.Tables[0].Rows[i].IsNull(1) == true)
                {
                    i = i + 1;
                }
                else
                {
                    //Lst.Items.Add(dset.Tables(0).Rows(i).Item(0))

                    Cb.Items.Add(System.Convert.ToString(dset.Tables[0].Rows[i][1]));
                    Cb.Items[Cb.Items.Count - 1].Value = System.Convert.ToString(dset.Tables[0].Rows[i][0]);
                    Cb.Items[Cb.Items.Count - 1].Attributes.Add("style", "red");
                    //If CultureName <> "en-US" Then
                    //    Cb.Items.Item(Cb.Items.Count - 1).Value = Rm1.GetString(Cb.Items.Item(Cb.Items.Count - 1).Value, c)
                    //End If
                    i = i + 1;


                }

            }
            Cb.SelectedIndex = 0;
            //The Fillcombo is used for filling of dropdownlist the qurey code is filled in value propery and name in text property 

        }
        public void FillCombo(DropDownList Cb, string Str, string NewName)
        {
            //The Fillcombo is used for filling of dropdownlist the qurey code is filled in value propery and name in text property 
            // Dim text As String
            //long cnt = 0;
            dset = objTrans.FillDset("NewTable", Str, DBConnection.GetConnection());
            i = 0;
            //cnt = 0;
            Cb.Items.Clear();
            Cb.Items.Add(" ------ Select ------ ");
            Cb.Items[0].Value = System.Convert.ToString(0);
            Cb.Items.Add("New " + NewName + "------ ");
            Cb.Items[1].Value = System.Convert.ToString(-1);
            while (i <= dset.Tables[0].Rows.Count - 1)
            {
                if (dset.Tables[0].Rows[i].IsNull(1) == true)
                {
                    i = i + 1;
                }
                else
                {
                    //Lst.Items.Add(dset.Tables(0).Rows(i).Item(0))
                    Cb.Items.Add(System.Convert.ToString(dset.Tables[0].Rows[i][1]));
                    Cb.Items[Cb.Items.Count - 1].Value = System.Convert.ToString(dset.Tables[0].Rows[i][0]);
                    //If CultureName <> "en-US" Then
                    //    Cb.Items.Item(Cb.Items.Count - 1).Value = Rm1.GetString(Cb.Items.Item(Cb.Items.Count - 1).Value, c)
                    //End If
                    i = i + 1;
                }
            }
            //The Fillcombo is used for filling of dropdownlist the qurey code is filled in value propery and name in text property 

        }
        public void FillCombo(string NewName, DropDownList Cb, string Str)
        {
            //The Fillcombo is used for filling of dropdownlist the qurey code is filled in value propery and name in text property 
            // Dim text As String
            //long cnt = 0;
            dset = objTrans.FillDset("NewTable", Str, DBConnection.GetConnection());
            i = 0;
            //cnt = 0;
            Cb.Items.Clear();
            Cb.Items.Add("New " + NewName + "------ ");
            Cb.Items[0].Value = System.Convert.ToString(0);
            while (i <= dset.Tables[0].Rows.Count - 1)
            {
                if (dset.Tables[0].Rows[i].IsNull(1) == true)
                {
                    i = i + 1;
                }
                else
                {
                    //Lst.Items.Add(dset.Tables(0).Rows(i).Item(0))
                    Cb.Items.Add(System.Convert.ToString(dset.Tables[0].Rows[i][1]));
                    Cb.Items[Cb.Items.Count - 1].Value = System.Convert.ToString(dset.Tables[0].Rows[i][0]);
                    //If CultureName <> "en-US" Then
                    //    Cb.Items.Item(Cb.Items.Count - 1).Value = Rm1.GetString(Cb.Items.Item(Cb.Items.Count - 1).Value, c)
                    //End If
                    i = i + 1;
                }
            }
            //The Fillcombo is used for filling of dropdownlist the qurey code is filled in value propery and name in text property 

        }
        public void FillCombo(DropDownList Cb, long value)
        {
            //The Fillcombo is used for filling of dropdownlist the qurey code is filled in value propery and name in text property 
            // Dim text As String
            i = 0;

            Cb.Items.Clear();
            Cb.Items.Add(" ------ Select ------ ");
            Cb.Items[0].Value = System.Convert.ToString(0);
            while (i <= value)
            {
                Cb.Items.Add(System.Convert.ToString((i + 1) + "Chits"));
                Cb.Items[Cb.Items.Count - 1].Value = (i + 1).ToString();
                i = i + 1;
            }

            //The Fillcombo is used for filling of dropdownlist the qurey code is filled in value propery and name in text property 

        }
        public void FillCombo1(DropDownList Cb, string Str)
        {
            //The Fillcombo is used for filling of dropdownlist the qurey code is filled in value propery and name in text property 
            // Dim text As String
            //long cnt = 0;
            dset = objTrans.FillDset("NewTable", Str, DBConnection.GetConnection());
            i = 0;
            //cnt = 0;
            Cb.Items.Clear();
            //Cb.Items.Add(" ------ Select ------ ");
            //Cb.Items[0].Value = System.Convert.ToString(0);
            while (i <= dset.Tables[0].Rows.Count - 1)
            {
                if (dset.Tables[0].Rows[i].IsNull(1) == true)
                {
                    i = i + 1;
                }
                else
                {
                    //Lst.Items.Add(dset.Tables(0).Rows(i).Item(0))

                    Cb.Items.Add(System.Convert.ToString(dset.Tables[0].Rows[i][1]));
                    Cb.Items[Cb.Items.Count - 1].Value = System.Convert.ToString(dset.Tables[0].Rows[i][0]);
                    //If CultureName <> "en-US" Then
                    //    Cb.Items.Item(Cb.Items.Count - 1).Value = Rm1.GetString(Cb.Items.Item(Cb.Items.Count - 1).Value, c)
                    //End If
                    i = i + 1;


                }

            }
            //The Fillcombo is used for filling of dropdownlist the qurey code is filled in value propery and name in text property 

        }
        public void FillCombo2(DropDownList Cb, string Str)
        {
            //The Fillcombo is used for filling of dropdownlist the qurey code is filled in value propery and name in text property 
            // Dim text As String
            //long cnt = 0;
            dset = objTrans.FillDset("NewTable", Str, DBConnection.GetConnection());
            i = 0;
            //cnt = 0;
            //Cb.Items.Clear();
            //Cb.Items.Add(" ------ Select ------ ");
            //Cb.Items[0].Value = System.Convert.ToString(0);
            while (i <= dset.Tables[0].Rows.Count - 1)
            {
                if (dset.Tables[0].Rows[i].IsNull(1) == true)
                {
                    i = i + 1;
                }
                else
                {
                    //Lst.Items.Add(dset.Tables(0).Rows(i).Item(0))

                    Cb.Items.Add(System.Convert.ToString(dset.Tables[0].Rows[i][1]));
                    Cb.Items[Cb.Items.Count - 1].Value = System.Convert.ToString(dset.Tables[0].Rows[i][0]);
                    //If CultureName <> "en-US" Then
                    //    Cb.Items.Item(Cb.Items.Count - 1).Value = Rm1.GetString(Cb.Items.Item(Cb.Items.Count - 1).Value, c)
                    //End If
                    i = i + 1;


                }

            }
            //The Fillcombo is used for filling of dropdownlist the qurey code is filled in value propery and name in text property 

        }
        public void FillHTMLCombo(HtmlSelect Cb, string TBName, string Str)
        {

            //long cnt = 0;
            //The FillHtMlcombo is used for filling of htmlselect the qurey code is filled in value propery and name in text property 
            dset = objTrans.FillDset(TBName, Str, DBConnection.GetConnection());
            i = 0;
            //cnt = 0;
            //Lst.Items.Clear()
            Cb.Items.Clear();
            while (i <= dset.Tables[0].Rows.Count - 1)
            {
                if ((dset.Tables[0].Rows[i].IsNull(1)) == true)
                {
                    i = i + 1;
                }
                else
                {

                    Cb.Items.Add(System.Convert.ToString(dset.Tables[0].Rows[i][1]));
                    Cb.Items[Cb.Items.Count - 1].Value = System.Convert.ToString(dset.Tables[0].Rows[i][0]);
                    i = i + 1;
                }
            }
            //The FillHtMlcombo is used for filling of htmlselect the qurey code is filled in value propery and name in text property 
        }
        //public long GetData(DropDownList Cb)
        //{

        //    return Microsoft.CSharp.CSharpCodeProvider.CreateProvider(Cb.Items[Cb.SelectedIndex].Value);

        //}

        //public long GetIndex(ref DropDownList Lst, string Value)
        //{
        //Dim i As Long
        //Dim LngValue As Long
        //LngValue = 0
        //For i = 0 To Lst.Items.Count - 1
        //    If String.Compare(CStr(Lst.Items(i).Text), Value) = 0 Then
        //        LngValue = i
        //    End If
        //Next i
        //Return LngValue


        //long i = 0;
        //long LngValue = 0;
        //LngValue = 0;
        //for (i = 0; i < Lst.Items.Count; i++)
        //{
        //    if (Microsoft.VisualBasic.Conversion.Val(Lst.Items[i].Value) == Value)
        //    {
        //        LngValue = i;
        //        break;
        //    }
        //}
        //return LngValue;
        //}
        public void FillList(ListBox Lst, string Str)
        {
            //long cnt = 0;
            //The Filllist is used for filling of listbox the qurey code is filled in value propery and name in text property 
            dset = objTrans.FillDset("NewTable", Str, DBConnection.GetConnection());
            i = 0;
            //cnt = 0;

            //Lst.Items.Clear()
            while (i <= dset.Tables[0].Rows.Count - 1)
            {
                if ((dset.Tables[0].Rows[i].IsNull(1)) == true)
                {
                    i = i + 1;
                }
                else
                {
                    //Lst.Items.Add(dset.Tables(0).Rows(i).Item(0))
                    Lst.Items.Add(System.Convert.ToString(dset.Tables[0].Rows[i][1]));
                    Lst.Items[Lst.Items.Count - 1].Value = System.Convert.ToString(dset.Tables[0].Rows[i][0]);
                    i = i + 1;
                }
            }
            //The Filllist is used for filling of listbox the qurey code is filled in value propery and name in text property 
        }
        public void FillList1(HtmlSelect Lst, string Str)
        {
            //long cnt = 0;
            //The Filllist is used for filling of listbox the qurey code is filled in value propery and name in text property 
            dset = objTrans.FillDset("NewTable", Str, DBConnection.GetConnection());
            i = 0;
            //cnt = 0;

            Lst.Items.Clear();
            while (i <= dset.Tables[0].Rows.Count - 1)
            {
                if ((dset.Tables[0].Rows[i].IsNull(0)) == true)
                {
                    i = i + 1;
                }
                else
                {
                    //Lst.Items.Add(dset.Tables(0).Rows(i).Item(0))
                    Lst.Items.Add(System.Convert.ToString(dset.Tables[0].Rows[i][0]));
                    if ((dset.Tables[0].Rows[i].IsNull(1)) == false)
                    {
                        Lst.Items[Lst.Items.Count - 1].Value = System.Convert.ToString(dset.Tables[0].Rows[i][1]);
                    }
                    else
                    {
                        Lst.Items[Lst.Items.Count - 1].Value = System.Convert.ToString(0);
                    }
                    i = i + 1;
                }
            }
            //The Filllist is used for filling of listbox the qurey code is filled in value propery and name in text property 
        }
        public void FillGrid(GridView Grd, string Str)
        {
            //long cnt = 0;
            //the qurey record is filled in datagrid 
            dset = objTrans.FillDset("NewTable", Str, DBConnection.GetConnection());
            i = 0;
            // cnt = 0;

            //Grd.Columns.Clear();
            Grd.DataBind();
            Grd.DataSource = dset.Tables[0].DefaultView;
            Grd.DataBind();
            //the qurey record is filled in datagrid 
        }

        public void FillCheckBoxList(CheckBoxList ChkList, string Str)
        {
            //long cnt = 0;
            //the qurey record is filled in datagrid 
            dset = objTrans.FillDset("NewTable", Str, DBConnection.GetConnection());
            i = 0;
            //cnt = 0;

            //Grd.Columns.Clear();
            ChkList.DataBind();
            ChkList.DataSource = dset.Tables[0].DefaultView;
            ChkList.DataValueField = Convert.ToString(dset.Tables[0].Columns[0]);
            ChkList.DataTextField = Convert.ToString(dset.Tables[0].Columns[1]);
            ChkList.DataBind();
            //the qurey record is filled in datagrid 
        }
        public void FillRadioButtonList(RadioButtonList RBList, string Str)
        {
            //long cnt = 0;
            //the qurey record is filled in datagrid 
            dset = objTrans.FillDset("NewTable", Str, DBConnection.GetConnection());
            i = 0;
            //cnt = 0;

            //Grd.Columns.Clear();
            RBList.DataBind();
            RBList.DataSource = dset.Tables[0].DefaultView;
            RBList.DataValueField = Convert.ToString(dset.Tables[0].Columns[0]);
            RBList.DataTextField = Convert.ToString(dset.Tables[0].Columns[1]);
            RBList.DataBind();
            //the qurey record is filled in datagrid 
        }
        public string UppercaseWords(string value)
        {
            char[] array = value.ToCharArray();

            if (array.Length >= 1)
            {
                if (char.IsLower(array[0]))
                {
                    array[0] = char.ToUpper(array[0]);
                }
            }


            for (int i = 1; i < array.Length; i++)
            {
                if (array[i - 1] == ' ')
                {
                    if (char.IsLower(array[i]))
                    {
                        array[i] = char.ToUpper(array[i]);
                    }
                }
            }
            return new string(array);
        }
        //public long GetIndex1(HtmlSelect Cb, long Value)
        //{
        //    //this functions are not Used but it's use to find code of in dropdownlist i.e. value
        //    int  i = 0;
        //    long LngValue = 0;
        //    LngValue = 0;
        //    for (i = 0; i < Cb.Items.Count; i++)
        //    {
        //        if (Microsoft.VisualBasic.Conversion.Val(Cb.Items[i].Value) == Value)
        //        {
        //            LngValue = i;
        //            break;
        //        }
        //    }
        //    return LngValue;
        //    //this functions are not Used but it's use to find code of in dropdownlist i.e. value
        //}

        public DataView GetDataView(string sql)
        {
            System.Data.SqlClient.SqlConnection Con = new System.Data.SqlClient.SqlConnection(DBConnection.GetConnection_My());
            System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch
            {
                throw;
            }
            finally
            {
                Con.Close();
            }

            return ds.Tables[(0)].DefaultView;
        }


        public void SendMail(String Subject, string StrShortBody, string StrBodyData,int Type)
        {
            Attachment attach = null;
            try
            {

                  MailMessage message = new MailMessage();
                  message.From = new MailAddress("omSystem101@gmail.com");
                  message.To.Add(new MailAddress("omSystem101@gmail.com"));
                  message.Subject = Subject;
                  message.Body = StrBodyData;
                  SmtpClient client = new SmtpClient();
                  if (StrBodyData != null)
                  {
                      string myPath = "";
                      if(Type==1)
                          myPath = System.Web.HttpContext.Current.Server.MapPath("~/TestFile/Error.txt");
                      else if (Type == 2)
                          myPath = System.Web.HttpContext.Current.Server.MapPath("~/TestFile/Info.txt");
                      else
                          myPath = System.Web.HttpContext.Current.Server.MapPath("~/TestFile/Test.txt");

                      File.WriteAllText(myPath, StrBodyData);
                      attach = new Attachment(myPath);
                      message.Attachments.Add(attach);
                  }
                  client.Send(message); 


             /*   MailMessage Msg = new MailMessage();
                Msg.From = new MailAddress("omSystem101@gmail.com");
                Msg.To.Add("nik.shah101@gmail.com");
                Msg.Subject = Subject;
                Msg.Body = StrShortBody;

                if (StrBodyData != null)
                {
                    string myPath = System.Web.HttpContext.Current.Server.MapPath("~/Error.txt");
                    File.WriteAllText(myPath, StrBodyData);
                    attach = new Attachment(System.Web.HttpContext.Current.Server.MapPath("~/Error.txt"));
                    Msg.Attachments.Add(attach);
                }

                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.Credentials = new System.Net.NetworkCredential("omSystem101@gmail.com", "manali@112233");

                smtp.EnableSsl = true;
                smtp.Send(Msg);*/
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                if (attach != null)
                    attach.Dispose();
            }
        }


        public class NullPropertiesConverter : JavaScriptConverter
        {
            public override object Deserialize(IDictionary<string, object> dictionary, Type type, JavaScriptSerializer serializer)
            {
                throw new NotImplementedException();
            }

            public override IDictionary<string, object> Serialize(object obj, JavaScriptSerializer serializer)
            {
                var jsonExample = new Dictionary<string, object>();
                foreach (var prop in obj.GetType().GetProperties())
                {
                    //check if decorated with ScriptIgnore attribute
                    bool ignoreProp = prop.IsDefined(typeof(ScriptIgnoreAttribute), true);

                    var value = prop.GetValue(obj, BindingFlags.Public, null, null, null);
                    if (value != null && !ignoreProp)
                        jsonExample.Add(prop.Name, value);
                }

                return jsonExample;
            }

            public override IEnumerable<Type> SupportedTypes
            {
                get { return GetType().Assembly.GetTypes(); }
            }
        }
    }

    class IsoDateTimeConverterWithMicroSeconds : Newtonsoft.Json.Converters.IsoDateTimeConverter
    {
        public IsoDateTimeConverterWithMicroSeconds()
        {
            DateTimeFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.fff'Z'";
        }
    }

}