﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.UI.MobileControls;
using Jit;
using JitClass;
using System.Collections.Generic;

namespace OM
{
    public class DBMCustomer
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();


        public List<WebTest.DTOClasses.UCCustomer> GetLsitCustomre(string SearchExp, string SearchValue)
        {
            try
            {
                List<WebTest.DTOClasses.UCCustomer> Lst = new List<WebTest.DTOClasses.UCCustomer>();
                string sql = " SELECT CustomerName, Address, MobileNo, PinCode, Area, City, State, EmailID, AdharNo, " +
                             " Gender, BDate, StatusNo " +
                             " FROM MCustomer " +
                             " Where " + SearchExp + " Like '%" + SearchValue + "%'";

                DataTable dt = ObjFunction.GetDataView(sql).Table;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    WebTest.DTOClasses.UCCustomer UC = new WebTest.DTOClasses.UCCustomer();
                    UC.customername = dt.Rows[i]["CustomerName"].ToString();
                    UC.mobileno = dt.Rows[i]["MobileNo"].ToString();
                    UC.pincode = dt.Rows[i]["PinCode"].ToString();
                    UC.area = dt.Rows[i]["Area"].ToString();
                    UC.state = dt.Rows[i]["State"].ToString();
                    UC.emailid = dt.Rows[i]["EmailID"].ToString();
                    UC.adharno = dt.Rows[i]["AdharNo"].ToString();
                    UC.gender = Convert.ToInt64(dt.Rows[i]["Gender"].ToString());
                    UC.bdate = Convert.ToDateTime(dt.Rows[i]["BDate"].ToString());
                    UC.statusno = Convert.ToInt64(dt.Rows[i]["StatusNo"].ToString());
                    Lst.Add(UC);
                }
                return Lst;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
