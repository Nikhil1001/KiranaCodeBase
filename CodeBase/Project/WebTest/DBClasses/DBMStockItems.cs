﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JitClass;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace OM
{
    
    public class DBMStockItems
    {
        CommonFunctions obj =new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDSet = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        public string GetStockItems()
        {
            string Sql=" SELECT Top 500 MStockGroup.StockGroupName As BrandName, MStockItems.ItemName, MStockItems.LangFullDesc, GetItemRateAll_1.MRP, GetItemRateAll_1.ASaleRate As Rate, MUOM.UOMName, MStockBarcode.Barcode,6.00 As SaleVat,6.00 As PurVat "+
                       //" --(Select Percentage From GetItemTaxAll(MStockItems.Itemno,GetDate(),10,32,NULL)) As SaleVat, --(Select Percentage From GetItemTaxAll(MStockItems.Itemno,GetDate(),11,32,NULL)) As PurVat "+
                       " FROM         MUOM INNER JOIN "+
                       " MRateSetting AS GetItemRateAll_1 ON MUOM.UOMNo = GetItemRateAll_1.UOMNo INNER JOIN "+
                       " MStockItems ON GetItemRateAll_1.ItemNo = MStockItems.ItemNo INNER JOIN "+
                       " MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo INNER JOIN "+
                       " MStockBarcode ON MStockItems.ItemNo = MStockBarcode.ItemNo  "+
                       " And MStockItems.IsActive='True' ";

            DataTable dt= obj.GetDataView(Sql).Table;

            List<ItemMaster> lst = new List<ItemMaster>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ItemMaster item = new ItemMaster();
                item.brandname = dt.Rows[i].ItemArray[0].ToString();
                item.itemname = dt.Rows[i].ItemArray[1].ToString();
                item.langfulldesc = dt.Rows[i].ItemArray[2].ToString();
                item.mrp = Convert.ToDouble(dt.Rows[i].ItemArray[3].ToString());
                item.rate = Convert.ToDouble(dt.Rows[i].ItemArray[4].ToString());
                item.uom = dt.Rows[i].ItemArray[5].ToString();
                item.barcode = dt.Rows[i].ItemArray[6].ToString();
                item.salevat = Convert.ToDouble(dt.Rows[i].ItemArray[7].ToString());
                item.purvat = Convert.ToDouble(dt.Rows[i].ItemArray[8].ToString());
                lst.Add(item);                
            }
            if (lst != null)
            {
             return   JsonConvert.SerializeObject(lst, Newtonsoft.Json.Formatting.None,
                                new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore
                                });
            }
            else
            {
                return "";
            }

        }

        public class ItemMaster
        {
            public string brandname { get; set; }
            public string itemname { get; set; }
            public string langfulldesc { get; set; }
            public double mrp { get; set; }
            public double rate { get; set; }
            public string uom { get; set; }
            public string barcode { get; set; }
            public double salevat { get; set; }
            public double purvat { get; set; }

        }
    }
}
