﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using OM;
using JitClass;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace OM
{

    public class DBTVoucherEntry
    {
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDSet = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        public static string strerrormsg;
        public CommandCollection commandcollection = new CommandCollection();
        public static int count;

        private bool AddTVoucherEntry(WebTest.DTOClasses.UCTVoucherEntry tvoucherentry)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTVoucherEntry";

            cmd.Parameters.AddWithValue("@PkVoucherNo", tvoucherentry.pkvoucherno);

            cmd.Parameters.AddWithValue("@VoucherUserNo", tvoucherentry.voucheruserno);

            cmd.Parameters.AddWithValue("@VoucherTypeCode", tvoucherentry.vouchertypecode);

            cmd.Parameters.AddWithValue("@SaleBillNo", tvoucherentry.salebillno);

            cmd.Parameters.AddWithValue("@LedgerNo", tvoucherentry.ledgerno);

            cmd.Parameters.AddWithValue("@VoucherDate", tvoucherentry.voucherdate);

            cmd.Parameters.AddWithValue("@VoucherTime", tvoucherentry.vouchertime);

            cmd.Parameters.AddWithValue("@BillAmount", tvoucherentry.billamount);

            cmd.Parameters.AddWithValue("@Remark", tvoucherentry.remark);

            cmd.Parameters.AddWithValue("@IsCancel", tvoucherentry.iscancel);

            cmd.Parameters.AddWithValue("@RateType", tvoucherentry.ratetype);

            cmd.Parameters.AddWithValue("@PaymentType", tvoucherentry.paymenttype);

            cmd.Parameters.AddWithValue("@BillDiscAmount", tvoucherentry.billdiscamount);

            cmd.Parameters.AddWithValue("@ChargAmount", tvoucherentry.chargamount);

            SqlParameter outParameter = new SqlParameter();
            outParameter.ParameterName = "@ReturnID";
            outParameter.Direction = ParameterDirection.Output;
            outParameter.DbType = DbType.Int32;
            cmd.Parameters.Add(outParameter);

            commandcollection.Add(cmd);
            return true;
        }

        private bool AddTStock(WebTest.DTOClasses.UCTStock tstock)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTStock";

            cmd.Parameters.AddWithValue("@PkVoucherTrnNo", tstock.pkvouchertrnno);

            // cmd.Parameters.AddWithValue("@FkVoucherNo", tstock.fkvoucherno);

            cmd.Parameters.AddWithValue("@Barcode", tstock.barcode);

            cmd.Parameters.AddWithValue("@ItemName", tstock.itemname);

            cmd.Parameters.AddWithValue("@LangItemName", tstock.langitemname);

            cmd.Parameters.AddWithValue("@ItemNo", tstock.itemno);

            cmd.Parameters.AddWithValue("@Qty", tstock.qty);

            cmd.Parameters.AddWithValue("@Rate", tstock.rate);

            cmd.Parameters.AddWithValue("@MRP", tstock.mrp);

            cmd.Parameters.AddWithValue("@Amount", tstock.amount);

            cmd.Parameters.AddWithValue("@NetRate", tstock.netrate);

            cmd.Parameters.AddWithValue("@NetAmount", tstock.netamount);

            cmd.Parameters.AddWithValue("@TaxPercentage", tstock.taxpercentage);

            cmd.Parameters.AddWithValue("@TaxAmount", tstock.taxamount);

            cmd.Parameters.AddWithValue("@DiscAmount", tstock.discamount);

            cmd.Parameters.AddWithValue("@uomname", tstock.uomname);


            commandcollection.Add(cmd);
            return true;
        }

        private bool AddTVoucherRefDetails(WebTest.DTOClasses.UCTVoucherRefDetails tvoucherrefdetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTVoucherRefDetails";

            cmd.Parameters.AddWithValue("@PkRefTrnNo", tvoucherrefdetails.pkreftrnno);

            //  cmd.Parameters.AddWithValue("@FkVoucherNo", tvoucherrefdetails.fkvoucherno);

            cmd.Parameters.AddWithValue("@LedgerNo", tvoucherrefdetails.ledgerno);

            //  cmd.Parameters.AddWithValue("@RefNo", tvoucherrefdetails.refno);

            cmd.Parameters.AddWithValue("@TypeOfRef", tvoucherrefdetails.typeofref);

            cmd.Parameters.AddWithValue("@Amount", tvoucherrefdetails.amount);

            cmd.Parameters.AddWithValue("@DiscAmount", tvoucherrefdetails.discamount);

            cmd.Parameters.AddWithValue("@BalAmount", tvoucherrefdetails.balamount);

            cmd.Parameters.AddWithValue("@PayDate", tvoucherrefdetails.paydate);

            cmd.Parameters.AddWithValue("@PayType", tvoucherrefdetails.paytype);

            cmd.Parameters.AddWithValue("@CardNo", tvoucherrefdetails.cardno);

            cmd.Parameters.AddWithValue("@Remark", tvoucherrefdetails.remark);


            commandcollection.Add(cmd);
            return true;
        }

        private bool AddTVoucherRefDetails_Collection(WebTest.DTOClasses.UCTVoucherRefDetails tvoucherrefdetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTVoucherRefDetails";

            cmd.Parameters.AddWithValue("@PkRefTrnNo", tvoucherrefdetails.pkreftrnno);

            cmd.Parameters.AddWithValue("@FkVoucherNo", tvoucherrefdetails.fkvoucherno);

            cmd.Parameters.AddWithValue("@LedgerNo", tvoucherrefdetails.ledgerno);

            cmd.Parameters.AddWithValue("@RefNo", tvoucherrefdetails.refno);

            cmd.Parameters.AddWithValue("@TypeOfRef", tvoucherrefdetails.typeofref);

            cmd.Parameters.AddWithValue("@Amount", tvoucherrefdetails.amount);

            cmd.Parameters.AddWithValue("@DiscAmount", tvoucherrefdetails.discamount);

            cmd.Parameters.AddWithValue("@BalAmount", tvoucherrefdetails.balamount);

            cmd.Parameters.AddWithValue("@PayDate", tvoucherrefdetails.paydate);

            cmd.Parameters.AddWithValue("@PayType", tvoucherrefdetails.paytype);

            cmd.Parameters.AddWithValue("@CardNo", tvoucherrefdetails.cardno);

            cmd.Parameters.AddWithValue("@Remark", tvoucherrefdetails.remark);


            commandcollection.Add(cmd);
            return true;
        }

        private bool AddMLedger(WebTest.DTOClasses.UCMLedger mledger)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMLedger";

            cmd.Parameters.AddWithValue("@PkSrNo", 0);

            cmd.Parameters.AddWithValue("@LedgerNo", mledger.ledgerno);

            cmd.Parameters.AddWithValue("@LedgerName", mledger.ledgername);

            cmd.Parameters.AddWithValue("@Address", mledger.address);

            cmd.Parameters.AddWithValue("@MobileNo", mledger.mobileno);

            cmd.Parameters.AddWithValue("@IsActive", mledger.isactive);

            cmd.Parameters.AddWithValue("@UserID", mledger.userid);

            cmd.Parameters.AddWithValue("@UserDate", mledger.userdate);


            commandcollection.Add(cmd);
            return true;
        }

        public bool UpdateBalAmount(long PkRefTrnNo, double BalAmount)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Update TVoucherRefDetails Set BalAmount=BalAmount+@BalAmount Where PkRefTrnNo=@PkRefTrnNo";

            cmd.Parameters.AddWithValue("@BalAmount", BalAmount);
            cmd.Parameters.AddWithValue("@PkRefTrnNo", PkRefTrnNo);

            commandcollection.Add(cmd);
            return true;
        }

        private bool DeleteAllVoucherEntry(long ledgerno, DateTime voucherdate, string salebillno)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteAllVoucherEntry";

            cmd.Parameters.AddWithValue("@Ledgerno", ledgerno);

            cmd.Parameters.AddWithValue("@VoucherDate", voucherdate);

            cmd.Parameters.AddWithValue("@Salebillno", salebillno);

            SqlParameter outParameter = new SqlParameter();
            outParameter.ParameterName = "@ReturnID";
            outParameter.Direction = ParameterDirection.Output;
            outParameter.DbType = DbType.Int32;
            cmd.Parameters.Add(outParameter);

            commandcollection.Add(cmd);
            return true;
        }

        public void SaveBills(WebTest.DTOClasses.UCSaleBillsList SaleBillList)
        {
            DBTVoucherEntry dbTVoucherEntry = new DBTVoucherEntry();
            try
            {
                foreach (WebTest.DTOClasses.UCSaleBills item in SaleBillList.salesBills)
                {
                    dbTVoucherEntry = new DBTVoucherEntry();
                    if (item.tvoucherentry.paymenttype == "Cash")
                        continue;

                    // if (item.tvoucherentry.statusno == 2)
                    //{
                    dbTVoucherEntry.DeleteAllVoucherEntry(item.tvoucherentry.ledgerno, item.tvoucherentry.voucherdate, item.tvoucherentry.salebillno);
                    //}
                    if (item.tvoucherentry != null)
                        dbTVoucherEntry.AddTVoucherEntry(item.tvoucherentry);

                    if (item.tstock != null)
                    {
                        foreach (WebTest.DTOClasses.UCTStock tstock in item.tstock)
                        {
                            dbTVoucherEntry.AddTStock(tstock);
                        }
                    }
                    if (item.tVoucherrefdetails != null)
                    {
                        foreach (WebTest.DTOClasses.UCTVoucherRefDetails tRef in item.tVoucherrefdetails)
                        {
                            dbTVoucherEntry.AddTVoucherRefDetails(tRef);
                        }
                    }
                    if (item.mledger != null)
                    {
                        if (ObjQry.ReturnLong("Select IsNull(LedgerNo,0) From MLedger Where LedgerNo=" + item.mledger.ledgerno + "", DBConnection.GetConnection_My()) == 0)
                        {
                            dbTVoucherEntry.AddMLedger(item.mledger);
                        }
                    }

                    dbTVoucherEntry.ExecuteNonQueryStatements();
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ExecuteNonQueryStatements()
        {

            SqlConnection cn = null;
            cn = new SqlConnection(DBConnection.GetConnection_My() + "Connect Timeout=3000");
            cn.Open();
            int cntref = -1, cntDelete = -1;

            SqlTransaction myTrans;
            myTrans = cn.BeginTransaction();
            try
            {
                for (int i = 0; (i < this.commandcollection.Count); i = (i + 1))
                {
                    if ((this.commandcollection[i] != null))
                    {
                        commandcollection[i].Connection = cn;
                        commandcollection[i].Transaction = myTrans;
                        if (commandcollection[i].CommandText == "DeleteAllVoucherEntry")
                        {
                            cntDelete = i;
                        }
                        else if (commandcollection[i].CommandText == "AddTVoucherEntry")
                        {
                            cntref = i;
                            if (cntDelete != -1)
                            {
                                commandcollection[i].Parameters["@PkVoucherNo"].Value = commandcollection[cntDelete].Parameters["@ReturnID"].Value;
                            }
                        }
                        else if (commandcollection[i].CommandText == "AddTStock")
                        {
                            commandcollection[i].Parameters.AddWithValue("@FkVoucherNo", commandcollection[cntref].Parameters["@ReturnID"].Value);
                        }
                        else if (commandcollection[i].CommandText == "AddTVoucherRefDetails")
                        {
                            if (cntref != -1)
                            {
                                commandcollection[i].Parameters.AddWithValue("@FkVoucherNo", commandcollection[cntref].Parameters["@ReturnID"].Value);
                                commandcollection[i].Parameters.AddWithValue("@RefNo", commandcollection[cntref].Parameters["@ReturnID"].Value);
                            }

                        }
                        if (commandcollection[i] != null)
                            commandcollection[i].ExecuteNonQuery();
                    }
                }

                myTrans.Commit();
            }
            catch (Exception e)
            {

                myTrans.Rollback();
                throw new Exception(e.Message, e);
            }
            finally
            {
                cn.Close();
            }
            //________________________________________________________________________________________________________________________________________________________________________________________________________________________
        }

        public List<WebTest.DTOClasses.UCSaleBills> SyncBills(WebTest.DTOClasses.UCTVoucherEntry tvc)
        {
            try
            {
                string StrWhere = "";
                if (tvc.ledgerno != 0)
                {
                    StrWhere = " LedgerNo =" + tvc.ledgerno;
                }
                if (tvc.voucherdate.ToString("dd-MM-yy") != "01-01-01")
                {
                    if (StrWhere == "")
                        StrWhere = " VoucherDate ='" + tvc.voucherdate.ToString("dd-MMM-yyyy") + "'";
                    else
                        StrWhere = StrWhere + " And VoucherDate ='" + tvc.voucherdate.ToString("dd-MMM-yyyy") + "'";
                }
                if (tvc.salebillno != null && tvc.salebillno != "")
                {
                    if (StrWhere == "")
                        StrWhere = " SaleBillNo ='" + tvc.salebillno + "'";
                    else
                        StrWhere = StrWhere + " And SaleBillNo ='" + tvc.salebillno + "'";
                }
                string sql = "Select  PkVoucherNo From TVoucherEntry  " +
                    " Where  " + StrWhere;
                CommonFunctions cc = new CommonFunctions();
                DataTable dt = cc.GetDataView(sql).Table;
                List<WebTest.DTOClasses.UCSaleBills> lst_SaleBill = new List<WebTest.DTOClasses.UCSaleBills>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lst_SaleBill.Add(GetBillData(Convert.ToInt64(dt.Rows[i].ItemArray[0].ToString())));
                }
                return lst_SaleBill;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        private WebTest.DTOClasses.UCSaleBills GetBillData(long PkVoucherNo)
        {
            CommonFunctions ObjFunction = new CommonFunctions();
            WebTest.DTOClasses.UCSaleBills SB = new WebTest.DTOClasses.UCSaleBills();
            try
            {
                // ObjFunction.WriteMessage("Start To Get Bill Data", LogLevel.Information, null);
                string sql = "SELECT TVoucherEntry.VoucherTypeCode, TVoucherEntry.SaleBillNo, TVoucherEntry.LedgerNo, TVoucherEntry.VoucherDate, TVoucherEntry.VoucherTime, TVoucherEntry.BillAmount, TVoucherEntry.Remark," +
                             " TVoucherEntry.IsCancel, TVoucherEntry.RateType, TVoucherEntry.PaymentType, TVoucherEntry.BillDiscAmount, TVoucherEntry.ChargAmount,  MLedger.LedgerName, MLedger.Address, MLedger.MobileNo " +
                            " FROM         TVoucherEntry INNER JOIN MLedger ON TVoucherEntry.LedgerNo = MLedger.LedgerNo " +
                            " WHERE     (TVoucherEntry.PkVoucherNo = " + PkVoucherNo + ") ";

                DataTable dt = ObjFunction.GetDataView(sql).Table;
                if (dt.Rows.Count == 0)
                    throw new Exception("Bill Not Found");


                WebTest.DTOClasses.UCTVoucherEntry tvc = new WebTest.DTOClasses.UCTVoucherEntry();
                tvc.pkvoucherno = PkVoucherNo;
                tvc.vouchertypecode = Convert.ToInt64(dt.Rows[0]["VoucherTypeCode"].ToString());
                tvc.voucheruserno = 0;
                tvc.salebillno = dt.Rows[0]["SaleBillNo"].ToString();
                tvc.ledgerno = Convert.ToInt64(dt.Rows[0]["LedgerNo"].ToString());
                tvc.voucherdate = Convert.ToDateTime(dt.Rows[0]["VoucherTime"].ToString()).Date;
                tvc.vouchertime = Convert.ToDateTime(dt.Rows[0]["VoucherTime"].ToString());
                tvc.billamount = Convert.ToDouble(dt.Rows[0]["BillAmount"].ToString());
                tvc.remark = dt.Rows[0]["Remark"].ToString();
                tvc.iscancel = Convert.ToBoolean(dt.Rows[0]["IsCancel"].ToString());
                tvc.paymenttype = dt.Rows[0]["PaymentType"].ToString();
                tvc.ratetype = dt.Rows[0]["RateType"].ToString();
                tvc.billdiscamount = Convert.ToDouble(dt.Rows[0]["BillDiscAmount"].ToString());
                tvc.chargamount = Convert.ToDouble(dt.Rows[0]["ChargAmount"].ToString());
                tvc.statusno = 0;
                SB.tvoucherentry = tvc;

                WebTest.DTOClasses.UCMLedger ML = new WebTest.DTOClasses.UCMLedger();
                ML.ledgerno = Convert.ToInt64(dt.Rows[0]["LedgerNo"].ToString());
                ML.ledgername = dt.Rows[0]["LedgerName"].ToString();
                ML.mobileno = dt.Rows[0]["MobileNo"].ToString();
                ML.address = dt.Rows[0]["Address"].ToString();
                SB.mledger = ML;


                sql = " SELECT  Barcode, ItemName, LangItemName, ItemNo, Qty, Rate, MRP, Amount, NetRate, NetAmount, TaxPercentage, TaxAmount, " +
                        " DiscAmount, FkVoucherNo, PkVoucherTrnNo,UomName " +
                      " FROM TStock " +
                          " Where  FkVoucherNo=" + PkVoucherNo + "";
                DataTable dtBillItems = ObjFunction.GetDataView(sql).Table;

                for (int i = 0; i < dtBillItems.Rows.Count; i++)
                {
                    WebTest.DTOClasses.UCTStock billItem = new WebTest.DTOClasses.UCTStock();
                    billItem.pkvouchertrnno = 0;
                    billItem.fkvoucherno = 0;
                    billItem.barcode = dtBillItems.Rows[i]["Barcode"].ToString();
                    billItem.itemname = dtBillItems.Rows[i]["ItemName"].ToString();
                    billItem.langitemname = dtBillItems.Rows[i]["LangItemName"].ToString();
                    billItem.qty = Convert.ToDouble(dtBillItems.Rows[i]["Qty"].ToString());
                    billItem.amount = Convert.ToDouble(dtBillItems.Rows[i]["Amount"].ToString());
                    billItem.rate = Convert.ToDouble(dtBillItems.Rows[i]["Rate"].ToString());
                    billItem.mrp = Convert.ToDouble(dtBillItems.Rows[i]["MRP"].ToString()); ;
                    billItem.netamount = Convert.ToDouble(dtBillItems.Rows[i]["NetAmount"].ToString());
                    billItem.netrate = Convert.ToDouble(dtBillItems.Rows[i]["NetAmount"].ToString());
                    billItem.taxpercentage = Convert.ToDouble(dtBillItems.Rows[i]["TaxPercentage"].ToString());
                    billItem.taxamount = Convert.ToDouble(dtBillItems.Rows[i]["TaxAmount"].ToString());
                    billItem.discamount = Convert.ToDouble(dtBillItems.Rows[i]["DiscAmount"].ToString());
                    billItem.itemno = Convert.ToInt64(dtBillItems.Rows[i]["ItemNo"].ToString());
                    billItem.uomname = dtBillItems.Rows[i]["UomName"].ToString();
                    SB.tstock.Add(billItem);
                }
                return SB;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public List<WebTest.DTOClasses.UCBillCollection> SyncBillCollection(long LedgerNo)
        {
            try
            {

                string sql = "Select  PkRefTrnNo From TVoucherRefDetails  " +
                    " Where  LedgerNo=" + LedgerNo + " And TypeOFRef=3 And Amount<>BalAmount";
                CommonFunctions cc = new CommonFunctions();
                DataTable dt = cc.GetDataView(sql).Table;
                List<WebTest.DTOClasses.UCBillCollection> lst_BillCol = new List<WebTest.DTOClasses.UCBillCollection>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lst_BillCol.Add(GetBillCollection(Convert.ToInt64(dt.Rows[i].ItemArray[0].ToString())));
                }
                return lst_BillCol;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        private WebTest.DTOClasses.UCBillCollection GetBillCollection(long PkSrNo)
        {
            CommonFunctions ObjFunction = new CommonFunctions();
            WebTest.DTOClasses.UCBillCollection SB = new WebTest.DTOClasses.UCBillCollection();
            try
            {
                // ObjFunction.WriteMessage("Start To Get Bill Data", LogLevel.Information, null);


                string sql = "SELECT     TVoucherEntry.SaleBillNo, TVoucherEntry.VoucherDate, TVoucherEntry.BillAmount, TVoucherRefDetails.PkRefTrnNo, TVoucherRefDetails.FkVoucherNo, TVoucherRefDetails.LedgerNo,  " +
                             " TVoucherRefDetails.RefNo, TVoucherRefDetails.TypeOfRef, TVoucherRefDetails.Amount, TVoucherRefDetails.DiscAmount, TVoucherRefDetails.BalAmount, TVoucherRefDetails.PayDate,  " +
                             " TVoucherRefDetails.PayType, TVoucherRefDetails.CardNo, TVoucherRefDetails.Remark " +
                             " FROM TVoucherRefDetails INNER JOIN  TVoucherEntry ON TVoucherRefDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo " +
                            " WHERE     (PkRefTrnNo = " + PkSrNo + ")";

                DataTable dt = ObjFunction.GetDataView(sql).Table;

                SB.pkreftrnno = Convert.ToInt64(dt.Rows[0]["PkRefTrnNo"].ToString());
                SB.fkvoucherno = Convert.ToInt64(dt.Rows[0]["FkVoucherNo"].ToString());
                SB.ledgerno = Convert.ToInt64(dt.Rows[0]["LedgerNo"].ToString());
                SB.refno = Convert.ToInt64(dt.Rows[0]["RefNo"].ToString());
                SB.typeofref = Convert.ToInt64(dt.Rows[0]["TypeOfRef"].ToString());
                SB.amount = Convert.ToDouble(dt.Rows[0]["Amount"].ToString());
                SB.discamount = Convert.ToInt64(dt.Rows[0]["DiscAmount"].ToString());
                SB.balamount = Convert.ToDouble(dt.Rows[0]["BalAmount"].ToString());
                SB.paydate = Convert.ToDateTime(dt.Rows[0]["PayDate"].ToString());
                SB.paytype = dt.Rows[0]["PayType"].ToString();
                SB.cardno = dt.Rows[0]["CardNo"].ToString();
                SB.remark = dt.Rows[0]["Remark"].ToString();
                SB.salebillno = dt.Rows[0]["SaleBillNo"].ToString();
                SB.voucherdate = Convert.ToDateTime(dt.Rows[0]["VoucherDate"].ToString());
                SB.billamount = Convert.ToDouble(dt.Rows[0]["BillAmount"].ToString());

                return SB;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public void Save_Collection(List<WebTest.DTOClasses.UCBillCollection> ColDetails)
        {
            DBTVoucherEntry dbTVoucherEntry = new DBTVoucherEntry();
            try
            {
                foreach (WebTest.DTOClasses.UCBillCollection item in ColDetails)
                {
                    WebTest.DTOClasses.UCTVoucherRefDetails tref = new WebTest.DTOClasses.UCTVoucherRefDetails();
                    tref.pkreftrnno = 0;
                    tref.fkvoucherno = item.fkvoucherno;
                    tref.ledgerno = item.ledgerno;
                    tref.paydate = item.voucherdate;
                    tref.paytype = "Cash";
                    tref.refno = item.refno;
                    tref.typeofref = 2;
                    tref.amount = item.amount;
                    tref.discamount = 0;
                    tref.balamount = 0;
                    tref.cardno = "";
                    tref.remark = item.remark;
                    dbTVoucherEntry.AddTVoucherRefDetails_Collection(tref);
                    dbTVoucherEntry.UpdateBalAmount(item.pkreftrnno, item.amount);
                }
                dbTVoucherEntry.ExecuteNonQueryStatements();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<WebTest.DTOClasses.UCOutStanding> SyncOutStandingDetails()
        {
            try
            {

                string sql = " Select  MLedger.LedgerName,TVoucherEntry.VoucherUserNo,TVoucherRefDetails.Amount,  " +
                             " (Case When (TVoucherRefDetails.BalAmount=0) Then 0 Else (TVoucherRefDetails.Amount-TVoucherRefDetails.BalAmount) End ) AS RecAmt, " +
                             " (Case When (TVoucherRefDetails.BalAmount=0) Then TVoucherRefDetails.Amount Else  TVoucherRefDetails.BalAmount End ) AS NetBalAmt " +
                             " ,MLedger.LedgerNo,TVoucherRefDetails.FkVoucherNo,TVoucherEntry.VoucherDate " +
                             " From TVoucherRefDetails Inner Join  " +
                             " MLedger ON MLedger.LedgerNo=TVoucherRefDetails.LedgerNo Inner Join  " +
                             " TVoucherEntry ON TVoucherEntry.PkVoucherNo=TVoucherRefDetails.FkVoucherNo " +
                             " Where TVoucherRefDetails.Amount <>TVoucherRefDetails.BalAmount And TVoucherRefDetails.TypeOfRef=3 " +
                             " Order by MLedger.LedgerName,TVoucherRefDetails.PayDate ";
                CommonFunctions cc = new CommonFunctions();
                DataTable dt = cc.GetDataView(sql).Table;
                List<WebTest.DTOClasses.UCOutStanding> lst_BillCol = new List<WebTest.DTOClasses.UCOutStanding>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    WebTest.DTOClasses.UCOutStanding uc = new WebTest.DTOClasses.UCOutStanding();
                    uc.LedgerName = dt.Rows[i]["LedgerName"].ToString();
                    uc.BillNo = dt.Rows[i]["VoucherUserNo"].ToString();
                    uc.Amount = Convert.ToDouble(dt.Rows[i]["Amount"].ToString());
                    uc.RecAmt = Convert.ToDouble(dt.Rows[i]["RecAmt"].ToString());
                    uc.NetBalAmt = Convert.ToDouble(dt.Rows[i]["NetBalAmt"].ToString());
                    uc.LedgerNo = Convert.ToInt64(dt.Rows[i]["LedgerNo"].ToString());
                    uc.FkVoucherNo = Convert.ToInt64(dt.Rows[i]["FkVoucherNo"].ToString());
                    uc.VoucherDate = Convert.ToDateTime(dt.Rows[i]["VoucherDate"].ToString());
                    lst_BillCol.Add(uc);
                }
                return lst_BillCol;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

    }

}
