﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JitClass;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Data.SqlClient;


namespace OM
{
    public class DBMLedger
    {
        CommonFunctions obj = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDSet = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        private bool AddMLedger(WebTest.DTOClasses.UCMLedger mledger)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMLedger";

            cmd.Parameters.AddWithValue("@PkSrNo", 0);

            cmd.Parameters.AddWithValue("@LedgerNo", mledger.ledgerno);

            cmd.Parameters.AddWithValue("@LedgerName", mledger.ledgername);

            cmd.Parameters.AddWithValue("@Address", mledger.address);

            cmd.Parameters.AddWithValue("@MobileNo", mledger.mobileno);

            cmd.Parameters.AddWithValue("@IsActive", mledger.isactive);

            cmd.Parameters.AddWithValue("@UserID", mledger.userid);

            cmd.Parameters.AddWithValue("@UserDate", mledger.userdate);

            
            if (ObjTrans.ExecuteNonQuery(cmd) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool SaveCustomer(WebTest.DTOClasses.UCMLedger ml)
        {
            return AddMLedger(ml);
        }

    }
}
