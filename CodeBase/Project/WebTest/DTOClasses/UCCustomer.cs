﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace WebTest.DTOClasses
{
    public class UCCustomer
    {
        public string customername { get; set; }
        public string address { get; set; }
        public string mobileno { get; set; }
        public string pincode { get; set; }
        public string area { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string emailid { get; set; }
        public string adharno { get; set; }
        public long gender { get; set; }
        public DateTime bdate { get; set; }
        public long statusno { get; set; }
    }

    public class UCSearch
    {
        public string expression { get; set; }
        public string values { get; set; }
    }
}
