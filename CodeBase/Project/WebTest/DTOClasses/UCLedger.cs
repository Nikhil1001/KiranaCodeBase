﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace WebTest.DTOClasses
{
    public class UCMLedger
    {
        public long ledgerno { get; set; }
        public string ledgername { get; set; }
        public string address { get; set; }
        public string mobileno { get; set; }
        public bool isactive { get; set; }
        public long userid { get; set; }
        [Newtonsoft.Json.JsonConverter(typeof(OM.IsoDateTimeConverterWithMicroSeconds))]
        public DateTime userdate { get; set; }
    }
}
