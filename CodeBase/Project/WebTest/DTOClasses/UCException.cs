﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace OM
{
    public class UCException
    {
        public int errorCode { get; set; }
        public String errorMessage { get; set; }
    }

    public static class ErrorCode
    {
        public static int OK = 0;
        public static int Failed = 101;
        public static int InternalError = 501;
    }
}
