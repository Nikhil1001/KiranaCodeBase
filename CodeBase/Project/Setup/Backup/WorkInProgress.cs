﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security;
using System.Data.SqlClient;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using System.IO;
using System.Reflection;
using OM;
using JitControls;
using System.Diagnostics;
using System.ServiceProcess;
using System.Net.Mail;
using System.IO;


namespace Setup.Display
{
    public partial class WorkInProgress : Form
    {
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();
        Security secure = new Security();
        List<ListDataBase> lstDataBase = new List<ListDataBase>();

        public static string StrInstanceName = "SQLEXPRESS", ServerName = "", DatabaseName = "";
        public string ConStr = "", CompanyName = "";
        bool isServer = true, isServiceAvaliable = false;

        public object argument = null;
        public WorkInProgress()
        {
            InitializeComponent();
        }

        public string StrStatus { set { lblStatus.Text = value; } }

        private void WorkInProgress_Shown(object sender, EventArgs e)
        {
            Application.DoEvents();
            //Check The Installation 
            if (SetConnection() == false)
            {
                MessageBox.Show("System Not Register,Sorry to update");
                Application.Exit();
            }
            ConStr = "Data Source=" + ServerName + ";Initial Catalog=" + DatabaseName + ";User ID=OM96;Password=OM96";

            CompanyName = ObjQry.ReturnString("Select CompanyName From MCompany", ConStr);

            //Check The Client Or Server By Default Server 
            if (ServerName.ToUpper().Replace("\\SQLEXPRESS", "") == System.Net.Dns.GetHostName().ToUpper())
                isServer = true;
            else
                isServer = false;

            if (argument != null)
            {
                bgWorker.RunWorkerAsync(argument);
            }
            else
            {
                bgWorker.RunWorkerAsync();
            }
        }

        private string GetConnection()
        {
            string Constr = "Data Source=" + ServerName;
            Constr = Constr + ";Initial Catalog=master;Integrated Security=True;";

            return Constr;
        }

        private void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                System.Threading.Thread.Sleep(3000);
                MessageBox.Show(e.Error.Message, "Update Setup", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Application.DoEvents();
            this.Close();
        }

        private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {

                //1) Stop Service
                StopSerivce();

                //2) Kirana Application is Close
                KillProcess();

                //3) Check The DataBase Sacript Method
                if (isServer == true) DBScriptRun();

                //4) Is Service is Avaliable in System Then Start Service
                if (isServer == true && isServiceAvaliable == true)
                    StartSerivce();

                //5) Copy Setup File
                CopyExecFile();

                //Update Verion
                if (isServer == true)
                {
                    UpdateDataBaseVerion();

                    #region Check Muti Firm
                    Check_Muti_Firm_Connection();
                    foreach (ListDataBase item in lstDataBase)
                    {
                        ServerName = item.ServerName;
                        DatabaseName = item.DataBaseName;
                        ConStr = item.ConnectionString;
                        DBScriptRun();
                        UpdateDataBaseVerion();
                    }
                    #endregion
                }

                RunNewExecFile();
            }
            catch (Exception ex)
            {
                bgWorker.ReportProgress(3, ex.Message);
                try
                {
                    SendMail("Error In Updating Setup in Store:" + CompanyName + "", "", ex.StackTrace.ToString(), 0);
                }
                catch (Exception se)
                {
                }

                throw ex;
            }


        }

        private void bgWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 0)//Main Status
                StrStatus = e.UserState.ToString();
            else if (e.ProgressPercentage == 1)// Process Status
            {
                lblProcess.ForeColor = Color.Black;
                lblProcess.Text = e.UserState.ToString();
            }
            else if (e.ProgressPercentage == 3)// Error Status
            {
                lblProcess.ForeColor = Color.Red;
                lblProcess.Text = e.UserState.ToString();
            }
        }

        private void StopSerivce()
        {
            try
            {
                bgWorker.ReportProgress(0, "Stop OMService command runing");
                ServiceController service = new ServiceController("OMService");
                service.Stop();
                var timeout = new TimeSpan(0, 0, 5); // 5seconds
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
                bgWorker.ReportProgress(0, "OMService Stop successfully..");
                isServiceAvaliable = true;
            }
            catch (Exception ex)
            {
                isServiceAvaliable = false;
                bgWorker.ReportProgress(3, ex.Message);
            }
        }

        private void StartSerivce()
        {
            try
            {
                bgWorker.ReportProgress(0, "OMService Start please wait..");
                ServiceController service = new ServiceController("OMService");
                service.Start();
                var timeout = new TimeSpan(0, 0, 5); // 5seconds
                service.WaitForStatus(ServiceControllerStatus.Running, timeout);
                bgWorker.ReportProgress(0, "OMService Start successfully..");
            }
            catch (Exception ex)
            {
                bgWorker.ReportProgress(3, ex.Message);
            }
        }

        private void DBScriptRun()
        {
            try
            {
                #region Check The DataBase Sacript Method

                bgWorker.ReportProgress(0, "Check The DataBase Sacript...");
                DirectoryInfo di = new DirectoryInfo("DatabaseScripts");
                if (!di.Exists)
                {
                    throw new Exception("Database Scripts Folder Not Found");
                }

                string strAppversion = new DBAssemblyInfo().AssemblyVersion;


                string currentScriptNo = ExecuteQuery("USE [" + DatabaseName + "] " + Environment.NewLine +
                    " SELECT ISNULL(MAX(ScriptNo), 0) AS mp FROM MUpdateScriptDetails").Tables[0].Rows[0][0].ToString();

                int iScriptNo = Convert.ToInt32(currentScriptNo);
                if (iScriptNo != 0)
                {
                    string StrStatus = ExecuteQuery("USE [" + DatabaseName + "] " + Environment.NewLine +
                        " SELECT ISNull(ScriptResult,'OK') FROM MUpdateScriptDetails Where ScriptNo=" + iScriptNo + "").Tables[0].Rows[0][0].ToString();
                    if (StrStatus != "OK")
                    {
                        Exception ex = new Exception("Script No :" + iScriptNo + " is Failed.");
                        throw new Exception("Previous update was unsuccessful. Please contact Support team", ex);
                    }
                }

                while (true)
                {
                    iScriptNo++;
                    FileInfo[] fa = di.GetFiles(iScriptNo + "_*.sql", SearchOption.TopDirectoryOnly);
                    if (fa.Length < 1)
                    {
                        break;
                    }
                    else
                    {
                        try
                        {
                            string strScriptName = fa[0].Name.Replace(iScriptNo + "_", "").Replace(fa[0].Extension, "");
                            string strScript = File.ReadAllText(fa[0].FullName);
                            strScript = strScript.Replace("Kirana0001", DatabaseName);
                            bgWorker.ReportProgress(1, "Runing database Script name:" + strScriptName + " ");
                            string strQuery = "USE [" + DatabaseName + "] " + Environment.NewLine +
                                " INSERT INTO MUpdateScriptDetails (ScriptNo, ScriptName, ScriptSql, ScriptPath, " +
                                " IsScriptExecuted, ScriptResult, ExecutedOn, AppVersionNo) " + Environment.NewLine +
                                " Values (" + iScriptNo + ", '" + strScriptName.Replace("'", "''") + "', " +
                                " '" + strScript.Replace("'", "''") + "', '" + fa[0].FullName.Replace("'", "''") + "', " +
                                " 'False', '', getDate(), '" + strAppversion + "')";
                            ExecuteScript(strQuery);

                            ExecuteScript(strScript);

                            strQuery = "USE [" + DatabaseName + "] " + Environment.NewLine +
                                " UPDATE MUpdateScriptDetails SET IsScriptExecuted = 'True', ScriptResult='OK', " +
                                " ExecutedOn=getDate() WHERE ScriptNo=" + iScriptNo + "";
                            ExecuteScript(strQuery);
                            bgWorker.ReportProgress(1, "Update Database Scripts successfully..");

                        }
                        catch (Exception ex)
                        {
                            Exception exl;

                            if (ex.InnerException != null)
                            {
                                exl = ex.InnerException;
                            }
                            else
                            {
                                exl = ex;
                            }
                            ExecuteScript("USE [" + DatabaseName + "] " + Environment.NewLine +
                                        " UPDATE MUpdateScriptDetails SET IsScriptExecuted = 'True', ScriptResult='FAILED :: " +
                                        ex.ToString().Replace("'", "''") + "', ExecutedOn=getDate() WHERE ScriptNo=" + iScriptNo + "");


                            throw new Exception("Error while executing update scripts !!!" + Environment.NewLine + Environment.NewLine +
                                        "Desc :: ", exl);
                        }
                    }
                }
                #endregion
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void UpdateDataBaseVerion()
        {
            ObjTrans.Execute("Update MSetting Set AppVersion='" + secure.psEncrypt(new DBAssemblyInfo().AssemblyVersion) + "' ", ConStr);
            SendMail("Store: " + CompanyName + " Updating New Version: " + new DBAssemblyInfo().AssemblyVersion + " successfully.", "", ServerName, 0);
        }

        private void CopyExecFile()
        {
            try
            {

                #region Copy Setup File

                //check Asbily Version To DataBase

                Version CusrentRunVersion = new Version(secure.psDecrypt(ObjQry.ReturnString("Select AppVersion From MSetting ", ConStr)));
                Version NewVersion = new Version(new DBAssemblyInfo().AssemblyVersion);
                int verResult = NewVersion.CompareTo(CusrentRunVersion);
                if (verResult < 0)
                {
                    throw new Exception("You click the old Setup,Please use new setup");
                }


                DirectoryInfo di = new DirectoryInfo("Debug");
                if (!di.Exists)
                {
                    throw new Exception("Debug Folder Not Found!");
                }

                //Copy All Files
                string sourcePath = di.FullName;
                string targetPath = "D:\\JitComputer\\Debug";
                CloneDirectory(sourcePath, targetPath);
                bgWorker.ReportProgress(0, "Setup Updated successfully..");

                #endregion

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void RunNewExecFile()
        {
            System.Threading.Thread.Sleep(2000);
            Process p1 = new Process();
            p1.StartInfo.FileName = "D:\\JitComputer\\Debug\\Kirana.exe";
            p1.Start();
            Application.Exit();
        }

        private void KillProcess()
        {
            try
            {
                foreach (var proc in Process.GetProcessesByName("Kirana"))
                {
                    //DialogResult dr = MessageBox.Show("If you cancel the update, Application will exit now.\r\n\r\n" +
                    //   "Do you want to exit the application?", "Exit Application", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    //if (dr == DialogResult.Yes)
                    //{
                    proc.Kill();
                    // }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static void CloneDirectory(string root, string dest)
        {
            foreach (var directory in Directory.GetDirectories(root))
            {
                string dirName = Path.GetFileName(directory);
                if (!Directory.Exists(Path.Combine(dest, dirName)))
                {
                    Directory.CreateDirectory(Path.Combine(dest, dirName));
                }
                CloneDirectory(directory, Path.Combine(dest, dirName));
            }

            foreach (var file in Directory.GetFiles(root))
            {
                File.Copy(file, Path.Combine(dest, Path.GetFileName(file)), true);
            }
        }

        private int ExecuteScript(string strScript)
        {
            if (String.IsNullOrEmpty(strScript))
            {
                return 0;
            }

            using (SqlConnection sqlConnection = new SqlConnection(GetConnection()))
            {
                ServerConnection svrConnection = new ServerConnection(sqlConnection);
                svrConnection.StatementTimeout = 0;
                Server server = new Server(svrConnection);
                return server.ConnectionContext.ExecuteNonQuery(strScript);
            }
        }

        private DataSet ExecuteQuery(string strQry)
        {
            using (SqlConnection sqlConnection = new SqlConnection(GetConnection()))
            {
                ServerConnection svrConnection = new ServerConnection(sqlConnection);
                svrConnection.StatementTimeout = 0;
                Server server = new Server(svrConnection);
                return server.ConnectionContext.ExecuteWithResults(strQry);
            }
        }

        public bool SetConnection()
        {
            try
            {
                string sysDrive = System.IO.Path.GetPathRoot(Environment.SystemDirectory);
                OMCommonClass cc = new OMCommonClass();

                string fname = sysDrive + "Windows\\System32\\Kirana RegisteredSerial.dat";
                StreamReader objreader = new StreamReader(fname);

                string str = objreader.ReadLine();
                str = objreader.ReadLine();
                //MainPassword = secure.psDecrypt(str);
                str = objreader.ReadLine();
                ServerName = secure.psDecrypt(str);
                str = objreader.ReadLine();
                DatabaseName = secure.psDecrypt(str);
                str = objreader.ReadLine();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public void Check_Muti_Firm_Connection()
        {
            bgWorker.ReportProgress(0, "Check Muti Firm Connection");
            lstDataBase = new List<ListDataBase>();
            DataTable dt = GetDataView(" Select ComanyName,DataBaseName,ServerName " +
                                       " From MFirmAccount " +
                                       " Where DataBaseName Not In('" + DatabaseName + "') ").Table;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ListDataBase data = new ListDataBase();
                data.ComanyName = dt.Rows[i]["ComanyName"].ToString();
                data.DataBaseName = dt.Rows[i]["DataBaseName"].ToString();
                data.ServerName = (dt.Rows[i]["ServerName"].ToString() == "") ? ServerName : dt.Rows[i]["ServerName"].ToString();
                data.ConnectionString = "Data Source=" + data.ServerName + ";Initial Catalog=" + data.DataBaseName + ";User ID=OM96;Password=OM96";
                if (CheckConnection(data.ConnectionString) == true)
                    lstDataBase.Add(data);
            }
        }

        private bool CheckConnection(string StrConnection)
        {

            SqlConnection con = null;
            try
            {
                con = new SqlConnection(StrConnection);
                con.Open();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (con != null)
                    con.Close();
            }
        }

        public class ListDataBase
        {
            public string ComanyName;
            public string DataBaseName;
            public string ServerName;
            public string ConnectionString;

        }

        internal DataView GetDataView(string sql)
        {
            System.Data.SqlClient.SqlConnection Con = new System.Data.SqlClient.SqlConnection(ConStr);
            System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch
            {
                throw;
            }
            finally
            {
                Con.Close();
            }

            return ds.Tables[(0)].DefaultView;
        }

        public void SendMail(String Subject, string StrShortBody, string StrBodyData, int Type)
        {
            Attachment attach = null;
            try
            {

                //MailMessage message = new MailMessage();
                //message.From = new MailAddress("omSystem101@gmail.com");
                //message.To.Add(new MailAddress("omSystem101@gmail.com"));
                //message.Subject = Subject;
                //message.Body = StrBodyData;
                //SmtpClient client = new SmtpClient();
                ////if (StrBodyData != null)
                ////{
                ////    string myPath = "";
                ////    if (Type == 1)
                ////        myPath = System.Web.HttpContext.Current.Server.MapPath("~/TestFile/Error.txt");
                ////    else if (Type == 2)
                ////        myPath = System.Web.HttpContext.Current.Server.MapPath("~/TestFile/Info.txt");
                ////    else
                ////        myPath = System.Web.HttpContext.Current.Server.MapPath("~/TestFile/Test.txt");

                ////    File.WriteAllText(myPath, StrBodyData);
                ////    attach = new Attachment(myPath);
                ////    message.Attachments.Add(attach);
                ////}
                //client.Send(message);


                MailMessage Msg = new MailMessage();
                Msg.From = new MailAddress("omSystem101@gmail.com");
                Msg.To.Add("omSystem101@gmail.com");
                Msg.Subject = Subject;
                Msg.Body = StrBodyData;


                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.Credentials = new System.Net.NetworkCredential("omSystem101@gmail.com", "manali@112233");

                smtp.EnableSsl = true;
                smtp.Send(Msg);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                if (attach != null)
                    attach.Dispose();
            }
        }

    }
    class DBAssemblyInfo
    {

        #region Assembly Attribute Accessors

        public string AssemblyTitle
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                if (attributes.Length > 0)
                {
                    AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                    if (titleAttribute.Title != "")
                    {
                        return titleAttribute.Title;
                    }
                }
                return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
            }
        }

        public string AssemblyVersion
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        public string AssemblyDescription
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyDescriptionAttribute)attributes[0]).Description;
            }
        }

        public string AssemblyProduct
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyProductAttribute)attributes[0]).Product;
            }
        }

        public string AssemblyCopyright
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
            }
        }

        public string AssemblyCompany
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCompanyAttribute)attributes[0]).Company;
            }
        }

        public string AssemblyGUID
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(System.Runtime.InteropServices.GuidAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((System.Runtime.InteropServices.GuidAttribute)attributes[0]).Value;
            }
        }

        public string AssemblyTrademark
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTrademarkAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyTrademarkAttribute)attributes[0]).Trademark;
            }
        }
        #endregion
    }
}
