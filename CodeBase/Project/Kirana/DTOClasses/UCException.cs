﻿using System;
using System.Data;
using System.Configuration;


namespace OM
{
    public class UCException
    {
        public int errorCode { get; set; }
        public String errorMessage { get; set; }
    }

    public static class ErrorCode
    {
        public static int OK = 0;
        public static int Failed = 101;
        public static int InternalError = 501;
    }
}
