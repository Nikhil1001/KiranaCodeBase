﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTOClasses
{
    public class UCSaleBills
    {
        public UCTVoucherEntry tvoucherentry;
        public IList<UCTStock> tstock= new List<UCTStock>();
        public IList<UCTVoucherRefDetails> tVoucherrefdetails = new List<UCTVoucherRefDetails>();
        public UCMLedger mledger;
    }
    public class UCSaleBillsList
    {
        public List<UCSaleBills> salesBills = new List<UCSaleBills>();
    }

    public class UCTVoucherEntry
    {
        public long pkvoucherno { get; set; }
        public long voucheruserno { get; set; }
        public long vouchertypecode { get; set; }
        public string salebillno { get; set; }
        public long ledgerno { get; set; }
        public DateTime voucherdate { get; set; }
        [Newtonsoft.Json.JsonConverter(typeof(OM.IsoDateTimeConverterWithMicroSeconds))]
        public DateTime vouchertime { get; set; }
        public double billamount { get; set; }
        public string remark { get; set; }
        public bool iscancel { get; set; }
        public string ratetype { get; set; }
        public string paymenttype { get; set; }
        public double billdiscamount { get; set; }
        public double chargamount { get; set; }
        public long statusno { get; set; }
    }

    public class UCTStock
    {
        public long pkvouchertrnno { get; set; }
        public long fkvoucherno { get; set; }
        public string barcode { get; set; }
        public string itemname { get; set; }
        public string langitemname { get; set; }
        public long itemno { get; set; }
        public double qty { get; set; }
        public double rate { get; set; }
        public double mrp { get; set; }
        public double amount { get; set; }
        public double netrate { get; set; }
        public double netamount { get; set; }
        public double taxpercentage { get; set; }
        public double taxamount { get; set; }
        public double discamount { get; set; }
        public string uomname { get; set; }
    }

    public class UCTVoucherRefDetails
    {
        public long pkreftrnno { get; set; }
        public long fkvoucherno { get; set; }
        public long ledgerno { get; set; }
        public long refno { get; set; }
        public long typeofref { get; set; }
        public double amount { get; set; }
        public long discamount { get; set; }
        public double balamount { get; set; }
        [Newtonsoft.Json.JsonConverter(typeof(OM.IsoDateTimeConverterWithMicroSeconds))]
        public DateTime paydate { get; set; }
        public string paytype { get; set; }
        public string cardno { get; set; }
        public string remark { get; set; }
    }

   
}
