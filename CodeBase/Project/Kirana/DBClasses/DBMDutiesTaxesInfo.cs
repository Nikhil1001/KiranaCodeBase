﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Xml.Linq;
using JitControls;

namespace OM
{
    class DBMDutiesTaxesInfo
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        CommandCollection commandcollection = new CommandCollection();
        public static string strerrormsg;

        public bool AddMDutiesTaxesInfo(MDutiesTaxesInfo mdutiestaxesinfo)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMDutiesTaxesInfo";

            cmd.Parameters.AddWithValue("@PkSrNo", mdutiestaxesinfo.PkSrNo);

            cmd.Parameters.AddWithValue("@LedgerNo", mdutiestaxesinfo.LedgerNo);

            cmd.Parameters.AddWithValue("@TaxOnLedgerNo", mdutiestaxesinfo.TaxOnLedgerNo);

            cmd.Parameters.AddWithValue("@FromDate", mdutiestaxesinfo.FromDate);

            cmd.Parameters.AddWithValue("@CalculationMethod", mdutiestaxesinfo.CalculationMethod);

            cmd.Parameters.AddWithValue("@Percentage", mdutiestaxesinfo.Percentage);

            cmd.Parameters.AddWithValue("@UserID", mdutiestaxesinfo.UserID);

            cmd.Parameters.AddWithValue("@UserDate", mdutiestaxesinfo.UserDate);

            if (ObjTrans.ExecuteNonQuery(cmd, CommonFunctions.ConStr) == true)
            {
                return true;
            }
            else
            {
                mdutiestaxesinfo.msg = ObjTrans.ErrorMessage;
                return false;
            }
        }

        public bool DeleteMDutiesTaxesInfo(MDutiesTaxesInfo mdutiestaxesinfo)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteMDutiesTaxesInfo";

            cmd.Parameters.AddWithValue("@PkSrNo", mdutiestaxesinfo.PkSrNo);
            if (ObjTrans.ExecuteNonQuery(cmd, CommonFunctions.ConStr) == true)
            {
                return true;
            }
            else
            {
                mdutiestaxesinfo.msg = ObjTrans.ErrorMessage;
                return false;
            }
        }

        public DataView GetAllMDutiesTaxesInfo()
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from MDutiesTaxesInfo order by PkSrNo";
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public DataView GetMDutiesTaxesInfoByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from MDutiesTaxesInfo where PkSrNo =" + ID;
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public MDutiesTaxesInfo ModifyMDutiesTaxesInfoByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql;
            SqlCommand cmd;
            sql = "Select * from MDutiesTaxesInfo where PkSrNo =" + ID;
            cmd = new SqlCommand(sql, Con);
            cmd.Connection = Con;
            Con.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (dr.HasRows)
            {
                MDutiesTaxesInfo MM = new MDutiesTaxesInfo();
                while (dr.Read())
                {
                    MM.PkSrNo = Convert.ToInt32(dr["PkSrNo"]);
                    if (!Convert.IsDBNull(dr["LedgerNo"])) MM.LedgerNo = Convert.ToInt64(dr["LedgerNo"]);
                    if (!Convert.IsDBNull(dr["TaxOnLedgerNo"])) MM.TaxOnLedgerNo = Convert.ToInt64(dr["TaxOnLedgerNo"]);
                    if (!Convert.IsDBNull(dr["FromDate"])) MM.FromDate = Convert.ToDateTime(dr["FromDate"]);
                    if (!Convert.IsDBNull(dr["CalculationMethod"])) MM.CalculationMethod = Convert.ToString(dr["CalculationMethod"]);
                    if (!Convert.IsDBNull(dr["Percentage"])) MM.Percentage = Convert.ToInt64(dr["Percentage"]);
                    if (!Convert.IsDBNull(dr["UserID"])) MM.UserID = Convert.ToInt64(dr["UserID"]);
                    if (!Convert.IsDBNull(dr["UserDate"])) MM.UserDate = Convert.ToDateTime(dr["UserDate"]);
                    if (!Convert.IsDBNull(dr["ModifiedBy"])) MM.ModifiedBy = Convert.ToString(dr["ModifiedBy"]);
                }
                dr.Close();
                return MM;
            }
            else
                dr.Close();
            return new MDutiesTaxesInfo();
        }



        public bool AddMItemTaxInfo(MItemTaxInfo mitemtaxinfo)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMItemTaxInfo";

            cmd.Parameters.AddWithValue("@PkSrNo", mitemtaxinfo.PkSrNo);

            cmd.Parameters.AddWithValue("@ItemNo", mitemtaxinfo.ItemNo);

            cmd.Parameters.AddWithValue("@TaxLedgerNo", mitemtaxinfo.TaxLedgerNo);

            cmd.Parameters.AddWithValue("@SalesLedgerNo", mitemtaxinfo.SalesLedgerNo);

            cmd.Parameters.AddWithValue("@FromDate", mitemtaxinfo.FromDate);

            cmd.Parameters.AddWithValue("@CalculationMethod", mitemtaxinfo.CalculationMethod);

            cmd.Parameters.AddWithValue("@Percentage", mitemtaxinfo.Percentage);

            cmd.Parameters.AddWithValue("@FKTaxSettingNo", mitemtaxinfo.FKTaxSettingNo);

            cmd.Parameters.AddWithValue("@UserID", mitemtaxinfo.UserID);

            cmd.Parameters.AddWithValue("@UserDate", mitemtaxinfo.UserDate);

            cmd.Parameters.AddWithValue("@TaxTypeNo", mitemtaxinfo.TaxTypeNo);

            cmd.Parameters.AddWithValue("@TransactionTypeNo", mitemtaxinfo.TransactionTypeNo);

            cmd.Parameters.AddWithValue("@IsActive", mitemtaxinfo.IsActive);

            cmd.Parameters.AddWithValue("@HSNCode", mitemtaxinfo.HSNCode);

            cmd.Parameters.AddWithValue("@HSNNo", mitemtaxinfo.HSNNo);

            if (ObjTrans.ExecuteNonQuery(cmd, CommonFunctions.ConStr) == true)
            {
                return true;
            }
            else
            {
                mitemtaxinfo.msg = ObjTrans.ErrorMessage;
                return false;
            }
        }


        public bool AddMItemTaxInfo1(MItemTaxInfo mitemtaxinfo)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMItemTaxInfo";

            cmd.Parameters.AddWithValue("@PkSrNo", mitemtaxinfo.PkSrNo);

            cmd.Parameters.AddWithValue("@ItemNo", mitemtaxinfo.ItemNo);

            cmd.Parameters.AddWithValue("@TaxLedgerNo", mitemtaxinfo.TaxLedgerNo);

            cmd.Parameters.AddWithValue("@SalesLedgerNo", mitemtaxinfo.SalesLedgerNo);

            cmd.Parameters.AddWithValue("@FromDate", mitemtaxinfo.FromDate);

            cmd.Parameters.AddWithValue("@CalculationMethod", mitemtaxinfo.CalculationMethod);

            cmd.Parameters.AddWithValue("@Percentage", mitemtaxinfo.Percentage);

            cmd.Parameters.AddWithValue("@FKTaxSettingNo", mitemtaxinfo.FKTaxSettingNo);

            cmd.Parameters.AddWithValue("@UserID", mitemtaxinfo.UserID);

            cmd.Parameters.AddWithValue("@UserDate", mitemtaxinfo.UserDate);

            cmd.Parameters.AddWithValue("@TaxTypeNo", mitemtaxinfo.TaxTypeNo);

            cmd.Parameters.AddWithValue("@TransactionTypeNo", mitemtaxinfo.TransactionTypeNo);

            cmd.Parameters.AddWithValue("@IsActive", mitemtaxinfo.IsActive);

            cmd.Parameters.AddWithValue("@HSNCode", mitemtaxinfo.HSNCode);

            cmd.Parameters.AddWithValue("@HSNNo", mitemtaxinfo.HSNNo);

            commandcollection.Add(cmd);

            return true;
        }

        public bool DeleteMItemTaxInfo(MItemTaxInfo mitemtaxinfo)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteMItemTaxInfo";

            cmd.Parameters.AddWithValue("@PkSrNo", mitemtaxinfo.PkSrNo);
            if (ObjTrans.ExecuteNonQuery(cmd, CommonFunctions.ConStr) == true)
            {
                return true;
            }
            else
            {
                mitemtaxinfo.msg = ObjTrans.ErrorMessage;
                return false;
            }
        }

        public DataView GetAllMItemTaxInfo()
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from MItemTaxInfo order by PkSrNo";
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public DataView GetMItemTaxInfoByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from MItemTaxInfo where PkSrNo =" + ID;
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public MItemTaxInfo ModifyMItemTaxInfoByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql;
            SqlCommand cmd;
            sql = "Select * from MItemTaxInfo where PkSrNo =" + ID;
            cmd = new SqlCommand(sql, Con);
            cmd.Connection = Con;
            Con.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (dr.HasRows)
            {
                MItemTaxInfo MM = new MItemTaxInfo();
                while (dr.Read())
                {
                    MM.PkSrNo = Convert.ToInt32(dr["PkSrNo"]);
                    if (!Convert.IsDBNull(dr["ItemNo"])) MM.ItemNo = Convert.ToInt64(dr["ItemNo"]);
                    if (!Convert.IsDBNull(dr["LedgerNo"])) MM.TaxLedgerNo = Convert.ToInt64(dr["TaxLedgerNo"]);
                    if (!Convert.IsDBNull(dr["SalesTaxLedgerNo"])) MM.SalesLedgerNo = Convert.ToInt64(dr["SalesLedgerNo"]);
                    if (!Convert.IsDBNull(dr["FromDate"])) MM.FromDate = Convert.ToDateTime(dr["FromDate"]);
                    if (!Convert.IsDBNull(dr["CalculationMethod"])) MM.CalculationMethod = Convert.ToString(dr["CalculationMethod"]);
                    if (!Convert.IsDBNull(dr["Percentage"])) MM.Percentage = Convert.ToDouble(dr["Percentage"]);
                    if (!Convert.IsDBNull(dr["UserID"])) MM.UserID = Convert.ToInt64(dr["UserID"]);
                    if (!Convert.IsDBNull(dr["UserDate"])) MM.UserDate = Convert.ToDateTime(dr["UserDate"]);
                    if (!Convert.IsDBNull(dr["TaxTypeNo"])) MM.TaxTypeNo = Convert.ToInt64(dr["TaxTypeNo"]);
                    if (!Convert.IsDBNull(dr["TransactionTypeNo"])) MM.TransactionTypeNo = Convert.ToInt64(dr["TransactionTypeNo"]);
                    if (!Convert.IsDBNull(dr["IsActive"])) MM.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    if (!Convert.IsDBNull(dr["HSNCode"])) MM.HSNCode = Convert.ToString(dr["HSNCode"]);
                    if (!Convert.IsDBNull(dr["HSNNo"])) MM.HSNNo = Convert.ToInt64(dr["HSNNo"]);
                    if (!Convert.IsDBNull(dr["ModifiedBy"])) MM.ModifiedBy = Convert.ToString(dr["ModifiedBy"]);
                }
                dr.Close();
                return MM;
            }
            else
                dr.Close();
            return new MItemTaxInfo();
        }



        public bool ExecuteNonQueryStatements()
        {

            SqlConnection cn = null;
            cn = new SqlConnection(CommonFunctions.ConStr);
            cn.Open();

            SqlTransaction myTrans;
            myTrans = cn.BeginTransaction();
            //cmd.Transaction = myTrans;

            try
            {
                for (int i = 0; (i < this.commandcollection.Count); i = (i + 1))
                {
                    if ((this.commandcollection[i] != null))
                    {
                        commandcollection[i].Connection = cn;
                        commandcollection[i].Transaction = myTrans;
                        if (commandcollection[i].CommandText == "AddMItemTaxInfo1")
                        {
                            commandcollection[i].CommandText = "AddMItemTaxInfo1";
                        }
                        else if (commandcollection[i] != null)
                            commandcollection[i].ExecuteNonQuery();
                    }
                }

                myTrans.Commit();
                return true;
            }
            catch (Exception e)
            {
                myTrans.Rollback();

                if (e.GetBaseException().Message == "")
                {
                    strerrormsg = e.Message;
                }
                else
                {
                    strerrormsg = e.GetBaseException().Message;
                }
                return false;
            }
            finally
            {
                cn.Close();
            }
            //________________________________________________________________________________________________________________________________________________________________________________________________________________________
        }
    }

    public class MDutiesTaxesInfo
    {
        private long mPkSrNo;
        private long mLedgerNo;
        private long mTaxOnLedgerNo;
        private DateTime mFromDate;
        private string mCalculationMethod;
        /*25-Apr-2014
        * Bug - unable to save floating point value. solved by changing data type to double
        */
        private double mPercentage;
        private long mUserID;
        private DateTime mUserDate;
        private string mModifiedBy;
        private string Mmsg;

        public long PkSrNo
        {
            get { return mPkSrNo; }
            set { mPkSrNo = value; }
        }
        public long LedgerNo
        {
            get { return mLedgerNo; }
            set { mLedgerNo = value; }
        }
        public long TaxOnLedgerNo
        {
            get { return mTaxOnLedgerNo; }
            set { mTaxOnLedgerNo = value; }
        }
        public DateTime FromDate
        {
            get { return mFromDate; }
            set { mFromDate = value; }
        }
        public string CalculationMethod
        {
            get { return mCalculationMethod; }
            set { mCalculationMethod = value; }
        }
        /*25-Apr-2014
        * Bug - unable to save floating point value. solved by changing data type to double
        */
        public double Percentage
        {
            get { return mPercentage; }
            set { mPercentage = value; }
        }
        public long UserID
        {
            get { return mUserID; }
            set { mUserID = value; }
        }
        public DateTime UserDate
        {
            get { return mUserDate; }
            set { mUserDate = value; }
        }
        public string ModifiedBy
        {
            get { return mModifiedBy; }
            set { mModifiedBy = value; }
        }
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }

    public class MItemTaxInfo
    {
        private long mPkSrNo;
        private long mItemNo;
        private long mTaxLedgerNo;
        private long mSalesLedgerNo;
        private DateTime mFromDate = DateTime.Now.Date;
        private string mCalculationMethod = "2";
        private double mPercentage;
        private long mFKTaxSettingNo;
        private long mUserID;
        private DateTime mUserDate = DateTime.Now.Date;
        private string mModifiedBy;
        private long mTaxTypeNo;
        private long mTransactionTypeNo;
        private bool mIsActive = true;
        private string mHSNCode = "";
        private long mHSNNo = 0;
        private double mIGSTPercent;
        private double mCGSTPercent;
        private double mSGSTPercent;
        private double mUTGSTPercent;
        private double mCessPercent;
        private string Mmsg;

        public long PkSrNo
        {
            get { return mPkSrNo; }
            set { mPkSrNo = value; }
        }
        public long ItemNo
        {
            get { return mItemNo; }
            set { mItemNo = value; }
        }
        public long TaxLedgerNo
        {
            get { return mTaxLedgerNo; }
            set { mTaxLedgerNo = value; }
        }
        public long SalesLedgerNo
        {
            get { return mSalesLedgerNo; }
            set { mSalesLedgerNo = value; }
        }
        public DateTime FromDate
        {
            get { return mFromDate; }
            set { mFromDate = value; }
        }
        public string CalculationMethod
        {
            get { return mCalculationMethod; }
            set { mCalculationMethod = value; }
        }
        public double Percentage
        {
            get { return mPercentage; }
            set { mPercentage = value; }
        }
        public long FKTaxSettingNo
        {
            get { return mFKTaxSettingNo; }
            set { mFKTaxSettingNo = value; }
        }
        public long UserID
        {
            get { return mUserID; }
            set { mUserID = value; }
        }
        public DateTime UserDate
        {
            get { return mUserDate; }
            set { mUserDate = value; }
        }
        public string ModifiedBy
        {
            get { return mModifiedBy; }
            set { mModifiedBy = value; }
        }
        public long TaxTypeNo
        {
            get { return mTaxTypeNo; }
            set { mTaxTypeNo = value; }
        }
        public long TransactionTypeNo
        {
            get { return mTransactionTypeNo; }
            set { mTransactionTypeNo = value; }
        }
        public bool IsActive
        {
            get { return mIsActive; }
            set { mIsActive = value; }
        }
        public string HSNCode
        {
            get { return mHSNCode; }
            set { mHSNCode = value; }
        }
        public long HSNNo
        {
            get { return mHSNNo; }
            set { mHSNNo = value; }
        }
        public double IGSTPercent
        {
            get { return mIGSTPercent; }
            set { mIGSTPercent = value; }
        }
        public double CGSTPercent
        {
            get { return mCGSTPercent; }
            set { mCGSTPercent = value; }
        }
        public double SGSTPercent
        {
            get { return mSGSTPercent; }
            set { mSGSTPercent = value; }
        }
        public double UTGSTPercent
        {
            get { return mUTGSTPercent; }
            set { mUTGSTPercent = value; }
        }
        public double CessPercent
        {
            get { return mCessPercent; }
            set { mCessPercent = value; }
        }
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }
}
