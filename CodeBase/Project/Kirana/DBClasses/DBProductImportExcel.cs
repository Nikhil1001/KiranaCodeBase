﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OM;
using System.Data;
using System.Data.OleDb;



namespace OM
{

    class DBProductImportExcel
    {
        JitControls.Transaction.Transactions objT = new JitControls.Transaction.Transactions();
        bool IsvalidateCheckFailed = true;

        public void ImportProduct(string strFileName)
        {
            try
            {
                dropTempTable();
                createTempTable();
                copyProductDataFromExcelToDB(strFileName);
                validateTempProductData();
                updatePkValues();
                insertNewStockGroupRecords(3); //Brand
                insertNewUOMRecords();
                updatePkValues(); //update Pks so we can get correct CategoryNo while processing SubCategory records
                insertNewProductRecords(); //Product Data
                insertStockData(); //Stock Data
                dropTempTable();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        private void dropTempTable()
        {
            string sql = "";

            sql = " IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MStockItemsImport]') AND type in (N'U')) " + Environment.NewLine +
                        " DROP TABLE [dbo].[MStockItemsImport] " + Environment.NewLine;

            if (!objT.Execute(sql, CommonFunctions.ConStr))
            {
                if (!String.IsNullOrEmpty(objT.ErrorMessage))
                {
                    throw new Exception(objT.ErrorMessage);
                }
                else
                {
                    throw new Exception("Unknown Error :: Can't drop temp table.");
                }
            }
        }

        private void createTempTable()
        {
            try
            {
                string sql = "";

                sql = " SET ANSI_NULLS ON" + Environment.NewLine +
                            " SET QUOTED_IDENTIFIER ON" + Environment.NewLine +
                            " SET ANSI_PADDING ON" + Environment.NewLine +
                            " CREATE TABLE [dbo].[MStockItemsImport](" + Environment.NewLine +
                            " 	[PkSrNo] [numeric](18, 0) IDENTITY(1,1) NOT NULL," + Environment.NewLine +
                            " 	[Barcode] [varchar](50) NOT NULL," + Environment.NewLine +
                            " 	[ItemName] [varchar](500) NOT NULL," + Environment.NewLine +
                            " 	[UOM] [varchar](50) NULL," + Environment.NewLine +
                            " 	[MRP] [numeric](18, 2) NOT NULL," + Environment.NewLine +
                            " 	[Brand] [varchar](100) NOT NULL," + Environment.NewLine +
                            " 	[HSNCode] [varchar](50) NULL CONSTRAINT [DF_MStockItemsImport_HSNCode]  DEFAULT ('')," + Environment.NewLine +
                            " 	[SaleRate] [numeric](18, 2) NULL CONSTRAINT [DF_MStockItemsImport_SaleRate]  DEFAULT ((0))," + Environment.NewLine +
                            " 	[Stock] [numeric](18, 2) NULL CONSTRAINT [DF_MStockItemsImport_Stock]  DEFAULT ((0))," + Environment.NewLine +
                            " 	[ItemNo] [numeric](18, 0) NULL CONSTRAINT [DF_MStockItemsImport_ItemNo]  DEFAULT ((0))," + Environment.NewLine +
                            " 	[BrandNo] [numeric](18, 0) NULL CONSTRAINT [DF_MStockItemsImport_BrandNo]  DEFAULT ((0))," + Environment.NewLine +
                            " 	[UOMNo] [numeric](18, 0) NULL CONSTRAINT [DF_MStockItemsImport_UOMNo]  DEFAULT ((0))," + Environment.NewLine +
                            " 	[HSNNo] [numeric](18, 0) NULL CONSTRAINT [DF_MStockItemsImport_HSNNo]  DEFAULT ((0))," + Environment.NewLine +
                            " 	[GSTPercent] [varchar](20) NULL CONSTRAINT [DF_MStockItemsImport_GSTPercent]  DEFAULT ('')," + Environment.NewLine +
                            " 	[StatusRemark] [varchar](max) NULL CONSTRAINT [DF_MStockItemsImport_StatusRemark]  DEFAULT ('')," + Environment.NewLine +
                            " 	[IsValid] [numeric](18, 0) NULL CONSTRAINT [DF_MStockItemsImport_IsValid]  DEFAULT ((0))," + Environment.NewLine +
                            " 	[IsError] [numeric](18, 0) NULL CONSTRAINT [DF_MStockItemsImport_IsError]  DEFAULT ((0))," + Environment.NewLine +
                            "  CONSTRAINT [PK_MStockItemsImport] PRIMARY KEY CLUSTERED " + Environment.NewLine +
                            " (" + Environment.NewLine +
                            " 	[PkSrNo] ASC" + Environment.NewLine +
                            " )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = OFF) ON [PRIMARY]" + Environment.NewLine +
                            " ) ON [PRIMARY]" + Environment.NewLine +
                            " " + Environment.NewLine +
                            " SET ANSI_PADDING OFF" + Environment.NewLine;

                if (!objT.Execute(sql, CommonFunctions.ConStr))
                {
                    if (!String.IsNullOrEmpty(objT.ErrorMessage))
                    {
                        throw new Exception(objT.ErrorMessage);
                    }
                    else
                    {
                        throw new Exception("Unknown Error :: Can't create temp table.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void copyProductDataFromExcelToDB(string strFileName)
        {
            string[,] strColumnMapping = new string[,] {{ "Upc", "Barcode" }, 
                                                        { "Product", "ItemName" }, 
                                                        { "Uom", "UOM" }, 
                                                        { "MRP", "MRP" }, 
                                                        { "Brand", "Brand" }, 
                                                        { "SaleRate", "SaleRate" }, 
                                                        { "Stock", "Stock" },
                                                        { "[Hsn Code]", "HSNCode" },
                                                        { "GSTPercent", "GSTPercent" }};

            OleDbConnection olecon = null;
            try
            {

                string connstring;
                if (strFileName.EndsWith("xls", StringComparison.CurrentCultureIgnoreCase))
                {
                    connstring = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                           "Data Source=" + strFileName + ";Extended Properties=\"Excel 8.0; HDR=Yes; IMEX=1;\";";
                }
                else if (strFileName.EndsWith("xlsx", StringComparison.CurrentCultureIgnoreCase))
                {
                    connstring = "Provider=Microsoft.ACE.OLEDB.12.0;" +
                           "Data Source=" + strFileName + ";Extended Properties=\"Excel 12.0 xml; HDR=Yes; IMEX=1;\";";
                }
                else
                {
                    throw new Exception("Invalid file format or extention. Only *.xls and *.xlsx files supported.");
                }

                olecon = new OleDbConnection(connstring);
                olecon.Open();

                DataTable schemaTable = olecon.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                string strTableName = "[" + schemaTable.Rows[0].ItemArray[2].ToString() + "]";
                string strInputColumnNames = "";

                for (int i = 0; i < strColumnMapping.GetLength(0); i++)
                {
                    strInputColumnNames += "," + strColumnMapping[i, 0];
                }

                strInputColumnNames = strInputColumnNames.Substring(1);

                OleDbDataAdapter olecomm1 = new OleDbDataAdapter("SELECT " + strInputColumnNames + " FROM " + strTableName, olecon);

                DataSet ds = new DataSet();
                olecomm1.Fill(ds);

                DataTable dtInput = ds.Tables[0];


                StringBuilder strbldr = new StringBuilder();
                string sql = "";

                string strOutputColumnNames = "";

                for (int i = 0; i < strColumnMapping.GetLength(0); i++)
                {
                    strOutputColumnNames += "," + strColumnMapping[i, 1];
                }

                strOutputColumnNames = strOutputColumnNames.Substring(1);


                int iTotalRows = dtInput.Rows.Count;
                for (int i = 0; i < iTotalRows; i++)
                {
                    sql = "Insert into MStockItemsImport (" + strOutputColumnNames + ") " +
                        " values(";
                    string strValues = "";
                    for (int j = 0; j < dtInput.Columns.Count; j++)
                    {
                        strValues += ",'" + dtInput.Rows[i][j].ToString().Replace("'", "''") + "'";
                    }
                    strValues = strValues.Substring(1);
                    sql += strValues + ");" + Environment.NewLine;

                    strbldr.Append(sql);

                    if (i > 0 && i % 500 == 0)
                    {
                        objT.ExecuteQuery(strbldr.ToString(), CommonFunctions.ConStr);
                        strbldr = new StringBuilder();
                        //ObjFunction.WriteMessage(i + " record(s) copied ...");
                    }
                }


                sql = "UPDATE MStockItemsImport SET UOM = 'NOS' Where UOM = '' OR UOM = 'NA'" + Environment.NewLine;
                strbldr.Append(sql);

                objT.ExecuteQuery(strbldr.ToString(), CommonFunctions.ConStr);

                //ObjFunction.WriteMessage("All record(s) copied successfully ...");
            }
            catch (Exception ex)
            {
                //ObjFunction.WriteMessage("Error while copying record(s) to temp table !!! Error : " + ex.Message);
                throw ex;
            }
            finally
            {
                if (olecon != null) olecon.Close();
            }
        }

        private void validateTempProductData()
        {
            bool isAllOk = true;


            int iEffected = objT.ExecuteQuery("UPDATE MStockItemsImport SET IsValid = 0", CommonFunctions.ConStr);


            int iEffectedBarcode = objT.ExecuteQuery(" UPDATE MStockItemsImport SET IsValid = IsValid - (IsValid % 10) + 1 " + Environment.NewLine +
                                    " WHERE PkSrNo NOT IN ( " + Environment.NewLine +
                                    "     SELECT MAX(PkSrNo) FROM MStockItemsImport GROUP BY Barcode, MRP " + Environment.NewLine +
                                    " ) ", CommonFunctions.ConStr);

            if (iEffectedBarcode > 0)
            {
                //ObjFunction.WriteMessage(iEffectedBarcode + " Duplicate Barcode found !!! ...");
                isAllOk = false;
            }

            int iEffectedLongDesc = objT.ExecuteQuery(" UPDATE MStockItemsImport SET IsValid = IsValid - (IsValid % 100) + 10 + (IsValid % 10) WHERE Brand +'-'+ ItemName IN( " +
                                                " SELECT     Brand +'-'+ ItemName " +
                                                " FROM         MStockItemsImport " +
                                                " GROUP BY Brand, ItemName, MRP " +
                                                " HAVING      (COUNT(PkSrNo) > 1) " +
                                                " ) ", CommonFunctions.ConStr);

            if (iEffectedLongDesc > 0)
            {
                //ObjFunction.WriteMessage(iEffectedLongDesc + " Duplicate Long Description found !!!");
                isAllOk = false;
            }



            int iEffectedBlankFields = objT.ExecuteQuery(" UPDATE MStockItemsImport SET IsValid = IsValid - (IsValid % 1000000) + 100000 + (IsValid % 100000) " +
                                                " WHERE Barcode = '' OR ItemName = '' OR Brand = ''  ", CommonFunctions.ConStr);
            if (iEffectedBlankFields > 0)
            {
                //ObjFunction.WriteMessage(iEffectedBarcode + " Records found having Blank values !!!");
                isAllOk = false;
            }




            int iEffectedGSTPercent = objT.ExecuteQuery(" UPDATE MStockItemsImport SET IsValid = IsValid - (IsValid % 100000000) + 10000000 + (IsValid % 10000000) " +
                                               " WHERE  ( ISNUMERIC(GSTPercent)=0 or GSTPercent Not IN (Select Distinct Percentage From MItemTaxSetting Where TaxTypeNo=" + GroupType.GST + ") )", CommonFunctions.ConStr);
            if (iEffectedGSTPercent > 0)
            {
                //ObjFunction.WriteMessage(iEffectedBarcode + " Records found having Blank values !!!");
                isAllOk = false;
            }



            if (!isAllOk)
            {
                //throw new Exception("Product validation failed.");
                IsvalidateCheckFailed = false;
            }
        }

        private void updatePkValues()
        {
            int iEffected = objT.ExecuteQuery("UPDATE MStockItemsImport SET MStockItemsImport.BrandNo = MStockGroup.StockGroupNo " +
                                                    " FROM         MStockGroup INNER JOIN " +
                                                    " MStockItemsImport ON MStockGroup.StockGroupName = MStockItemsImport.Brand " +
                                                    " WHERE     (MStockGroup.ControlGroup = 3) AND MStockItemsImport.BrandNo = 0 AND MStockItemsImport.IsValid = 0 ", CommonFunctions.ConStr);

            iEffected = objT.ExecuteQuery("UPDATE MStockItemsImport SET MStockItemsImport.UOMNo = MUOM.UOMNo " +
                                                    " FROM         MUOM INNER JOIN " +
                                                    " MStockItemsImport ON MUOM.UOMName = MStockItemsImport.UOM " +
                                                    " WHERE MStockItemsImport.UOMNo = 0 AND MStockItemsImport.IsValid = 0 ", CommonFunctions.ConStr);

            iEffected = objT.ExecuteQuery("UPDATE MStockItemsImport SET MStockItemsImport.ItemNo = MStockBarcode.ItemNo " +
                                                    " FROM         MStockBarcode INNER JOIN " +
                                                    " MStockItemsImport ON MStockBarcode.Barcode = MStockItemsImport.Barcode " +
                                                    " WHERE MStockItemsImport.ItemNo = 0 AND MStockItemsImport.IsValid = 0 ", CommonFunctions.ConStr);

            //HSNNo
            iEffected = objT.ExecuteQuery("UPDATE MStockItemsImport SET MStockItemsImport.HSNNo = MStockHSNCode.HSNNo " +
                                                    " FROM         MStockHSNCode INNER JOIN " +
                                                    " MStockItemsImport ON MStockHSNCode.HSNCode= MStockItemsImport.HSNCode " +
                                                    " WHERE MStockItemsImport.HSNNo = 0 AND MStockItemsImport.IsValid = 0 ", CommonFunctions.ConStr);


        }

        private void insertNewStockGroupRecords(int iType)
        {
            CommonFunctions ObjFunction = new CommonFunctions();

            DataTable dtToAdd = null;
            string sql = "";
            bool isSaveControlSubGroup = false;
            switch (iType)
            {
                case 22:
                    sql = "Select Distinct SubCategory, CategoryNo From MStockItemsImport Where SubCategoryNo = 0 AND CategoryNo <> 0 AND IsValid = 0 ";
                    isSaveControlSubGroup = true;
                    iType = 2;
                    break;
                case 2:
                    sql = "Select Distinct Category From MStockItemsImport Where CategoryNo = 0 AND IsValid = 0 ";
                    break;
                case 3:
                    sql = "Select Distinct Brand From MStockItemsImport Where BrandNo = 0 AND IsValid = 0 ";
                    break;
                case 4:
                    sql = "Select Distinct Department From MStockItemsImport Where DepartmentNo = 0 AND IsValid = 0 ";
                    break;
                case 5:
                    sql = "Select Distinct Manufacturer From MStockItemsImport Where ManufacturerNo = 0 AND IsValid = 0 ";
                    break;
            }

            dtToAdd = ObjFunction.GetDataView(sql).Table;

            int iTotalRecordsToBeProcessed = dtToAdd.Rows.Count;

            for (int i = 0; i < iTotalRecordsToBeProcessed; i++)
            {
                DBMStockGroup dbMStockGroup = new DBMStockGroup();
                MStockGroup mstockgroup = new MStockGroup();

                mstockgroup.StockGroupName = dtToAdd.Rows[i][0].ToString();
                mstockgroup.ControlGroup = iType;
                mstockgroup.ControlSubGroup = !isSaveControlSubGroup ? 0 : Convert.ToInt64(dtToAdd.Rows[i][1].ToString());
                mstockgroup.LanguageName = "";
                mstockgroup.IsActive = true;
                mstockgroup.UserDate = DBGetVal.ServerTime;
                mstockgroup.UserId = DBGetVal.UserID;

                if (!dbMStockGroup.AddMStockGroup(mstockgroup))
                {
                    throw new Exception("Error while inserting New record in StockGroup Table !!! iType = " + iType + " AND  Error Message = " + mstockgroup.msg);
                }

            }

        }

        private void insertNewUOMRecords()
        {
            DataTable dtToAdd = null;
            string sql = "Select Distinct UOM From MStockItemsImport Where UOMNo = 0 AND IsValid = 0 ";

            CommonFunctions ObjFunction = new CommonFunctions();
            dtToAdd = ObjFunction.GetDataView(sql).Table;

            int iTotalRecordsToBeProcessed = dtToAdd.Rows.Count;
            for (int i = 0; i < iTotalRecordsToBeProcessed; i++)
            {
                DBMUOM dbmuom = new DBMUOM();
                MUOM muom = new MUOM();

                muom.UOMName = dtToAdd.Rows[i][0].ToString();
                muom.UOMShortCode = muom.UOMName;
                muom.UserDate = DateTime.Now;

                if (!dbmuom.AddMUOM(muom))
                {
                    throw new Exception("Error while inserting New record in MUOM Table !!! Error Message = " + muom.msg);
                }


            }
        }

        private void insertNewProductRecords()
        {
            CommonFunctions ObjFunction = new CommonFunctions();
            DataTable dtBarcode = null;
            string sql = "Select Barcode, Min(PkSrNo), Count(Barcode) From MStockItemsImport " +
                            " Where IsValid = 0 And ItemNo = 0 AND " +
                            " BrandNo <> 0  AND UOMNo <> 0 " +
                            " GROUP BY Barcode";

            dtBarcode = ObjFunction.GetDataView(sql).Table;


            DBMStockItems dbStockItems = new DBMStockItems();
            string strPkSrNos = "";

            int iTotalRecordsToBeProcessed = dtBarcode.Rows.Count;

            for (int i = 0; i < dtBarcode.Rows.Count; i++)
            {
                sql = "Select * From MStockItemsImport Where Barcode = '" + dtBarcode.Rows[i][0].ToString() + "'";
                DataTable dtProduct = ObjFunction.GetDataView(sql).Table;

                #region Fetch GST Tax Details using HSN Code
                string strHSNCode = dtProduct.Rows[0]["HSNCode"].ToString().Trim();
                string GSTPercent = dtProduct.Rows[0]["GSTPercent"].ToString().Trim();
                DataTable dtItemTaxSetting_Sales_Gst = null, dtItemTaxSetting_Purchase_Gst = null;


                sql = "SELECT MItemTaxSetting.* " +
                         " FROM MItemTaxSetting " +
                         " WHERE   MItemTaxSetting.Percentage = " + GSTPercent + " " +
                             " AND MItemTaxSetting.TransactionTypeNo = " + GroupType.SalesAccount + " " +
                             " AND MItemTaxSetting.TaxTypeNo = " + GroupType.GST + " " +
                             " AND MItemTaxSetting.IsActive='True' ";
                dtItemTaxSetting_Sales_Gst = ObjFunction.GetDataView(sql).Table;
                sql = "SELECT MItemTaxSetting.* " +
                            " FROM MItemTaxSetting " +
                            " WHERE   MItemTaxSetting.Percentage = " + GSTPercent + " " +
                                " AND MItemTaxSetting.TransactionTypeNo = " + GroupType.PurchaseAccount + " " +
                                " AND MItemTaxSetting.TaxTypeNo = " + GroupType.GST + " " +
                                " AND MItemTaxSetting.IsActive='True' ";
                dtItemTaxSetting_Purchase_Gst = ObjFunction.GetDataView(sql).Table;

                #endregion

                #region MStockItems
                MStockItems mStockItems = new MStockItems();

                mStockItems.ItemName = dtProduct.Rows[0]["ItemName"].ToString();
                mStockItems.ItemShortCode = dtProduct.Rows[0]["ItemName"].ToString();
                mStockItems.FKStockDeptNo = 10;
                mStockItems.GroupNo = Convert.ToInt64(dtProduct.Rows[0]["BrandNo"].ToString()); //Brand
                mStockItems.GroupNo1 = 8;
                mStockItems.FkCategoryNo = 1;
                mStockItems.UOMDefault = Convert.ToInt64(dtProduct.Rows[0]["UOMNo"].ToString());
                mStockItems.UOMPrimary = Convert.ToInt64(dtProduct.Rows[0]["UOMNo"].ToString());
                mStockItems.ShortCode = dtProduct.Rows[0]["Barcode"].ToString();
                mStockItems.LangFullDesc = "";
                mStockItems.LangShortDesc = "";
                mStockItems.FKStockLocationNo = 1;
                mStockItems.FkDepartmentNo = 1;
                mStockItems.IsActive = true;
                mStockItems.IsFixedBarcode = true;
                mStockItems.FkStockGroupTypeNo = 1;
                mStockItems.FactorVal = 1;
                mStockItems.GodownNo = 2;
                mStockItems.DiscountType = 1;
                mStockItems.IsQtyRead = false;
                mStockItems.CompanyNo = 1;
                mStockItems.UserId = DBGetVal.UserID;
                mStockItems.UserDate = DBGetVal.ServerTime;
                dbStockItems.AddMStockItems(mStockItems);
                #endregion

                #region MStockBarcode
                MStockBarcode mStockBarcode = new MStockBarcode();
                mStockBarcode.Barcode = dtProduct.Rows[0]["Barcode"].ToString();
                mStockBarcode.CompanyNo = 1;
                mStockBarcode.UserID = DBGetVal.UserID;
                mStockBarcode.UserDate = DBGetVal.ServerTime;
                #endregion

                #region MRateSetting
                dbStockItems.AddMStockBarcode(mStockBarcode);

                //need to check multiple MRP found if so insert multiple here
                for (int j = 0; j < dtProduct.Rows.Count; j++)
                {
                    MRateSetting mRateSetting = new MRateSetting();
                    mRateSetting.PkSrNo = 0;
                    mRateSetting.UOMNo = Convert.ToInt64(dtProduct.Rows[j]["UOMNo"].ToString());
                    if (Convert.ToDouble(dtProduct.Rows[j]["SaleRate"].ToString()) == 0)
                    {
                        mRateSetting.ASaleRate = Convert.ToDouble(dtProduct.Rows[j]["MRP"].ToString());
                        mRateSetting.BSaleRate = Convert.ToDouble(dtProduct.Rows[j]["MRP"].ToString());
                    }
                    else
                    {
                        mRateSetting.ASaleRate = Convert.ToDouble(dtProduct.Rows[j]["SaleRate"].ToString());
                        mRateSetting.BSaleRate = Convert.ToDouble(dtProduct.Rows[j]["SaleRate"].ToString());
                    }
                    mRateSetting.MRP = Convert.ToDouble(dtProduct.Rows[j]["MRP"].ToString());
                    mRateSetting.PurRate = 0;
                    mRateSetting.FromDate = Convert.ToDateTime("01-01-1900");
                    mRateSetting.StockConversion = 1;
                    mRateSetting.MKTQty = 1;
                    mRateSetting.IsActive = true;
                    mRateSetting.CompanyNo = 1;
                    mRateSetting.UserID = DBGetVal.UserID;
                    mRateSetting.UserDate = DBGetVal.ServerTime;
                    dbStockItems.AddMRateSetting2(mRateSetting);
                    strPkSrNos += "," + dtProduct.Rows[j]["PkSrNo"].ToString() + "";
                }
                #endregion

                #region Save Tax details

                #region GST Tax Details
                if (dtItemTaxSetting_Sales_Gst != null && dtItemTaxSetting_Purchase_Gst != null)
                {
                    for (int g = 0; g < dtItemTaxSetting_Sales_Gst.Rows.Count; g++)
                    {
                        MItemTaxInfo mItemTaxInfo = new MItemTaxInfo();
                        mItemTaxInfo.TaxLedgerNo = Convert.ToInt64(dtItemTaxSetting_Sales_Gst.Rows[g]["TaxLedgerNo"].ToString());
                        mItemTaxInfo.SalesLedgerNo = Convert.ToInt64(dtItemTaxSetting_Sales_Gst.Rows[g]["SalesLedgerNo"].ToString());
                        mItemTaxInfo.Percentage = Convert.ToDouble(dtItemTaxSetting_Sales_Gst.Rows[g]["Percentage"].ToString());
                        mItemTaxInfo.FKTaxSettingNo = Convert.ToInt64(dtItemTaxSetting_Sales_Gst.Rows[g]["PkSrNo"].ToString());
                        mItemTaxInfo.TaxTypeNo = Convert.ToInt64(dtItemTaxSetting_Sales_Gst.Rows[g]["TaxTypeNo"].ToString());
                        mItemTaxInfo.TransactionTypeNo = Convert.ToInt64(dtItemTaxSetting_Sales_Gst.Rows[g]["TransactionTypeNo"].ToString());
                        mItemTaxInfo.HSNCode = strHSNCode;
                        mItemTaxInfo.IsActive = true;
                        mItemTaxInfo.HSNNo = 0;
                        mItemTaxInfo.FromDate = DBGetVal.ServerTime;
                        mItemTaxInfo.IGSTPercent = Convert.ToDouble(dtItemTaxSetting_Sales_Gst.Rows[g]["IGSTPercent"].ToString());
                        mItemTaxInfo.CGSTPercent = Convert.ToDouble(dtItemTaxSetting_Sales_Gst.Rows[g]["CGSTPercent"].ToString());
                        mItemTaxInfo.SGSTPercent = Convert.ToDouble(dtItemTaxSetting_Sales_Gst.Rows[g]["SGSTPercent"].ToString());
                        mItemTaxInfo.UTGSTPercent = Convert.ToDouble(dtItemTaxSetting_Sales_Gst.Rows[g]["UTGSTPercent"].ToString());
                        mItemTaxInfo.CessPercent = Convert.ToDouble(dtItemTaxSetting_Sales_Gst.Rows[g]["CessPercent"].ToString());
                        mItemTaxInfo.UserDate = DBGetVal.ServerTime;
                        mItemTaxInfo.UserID = DBGetVal.UserID;
                        dbStockItems.AddMItemTaxInfo(mItemTaxInfo);
                        if (mItemTaxInfo.FromDate <= DBGetVal.ServerTime.Date)
                        {
                            break;
                        }
                    }

                    for (int g = 0; g < dtItemTaxSetting_Purchase_Gst.Rows.Count; g++)
                    {
                        MItemTaxInfo mItemTaxInfo = new MItemTaxInfo();
                        mItemTaxInfo.TaxLedgerNo = Convert.ToInt64(dtItemTaxSetting_Purchase_Gst.Rows[g]["TaxLedgerNo"].ToString());
                        mItemTaxInfo.SalesLedgerNo = Convert.ToInt64(dtItemTaxSetting_Purchase_Gst.Rows[g]["SalesLedgerNo"].ToString());
                        mItemTaxInfo.Percentage = Convert.ToDouble(dtItemTaxSetting_Purchase_Gst.Rows[g]["Percentage"].ToString());
                        mItemTaxInfo.FKTaxSettingNo = Convert.ToInt64(dtItemTaxSetting_Purchase_Gst.Rows[g]["PkSrNo"].ToString());
                        mItemTaxInfo.TaxTypeNo = Convert.ToInt64(dtItemTaxSetting_Purchase_Gst.Rows[g]["TaxTypeNo"].ToString());
                        mItemTaxInfo.TransactionTypeNo = Convert.ToInt64(dtItemTaxSetting_Purchase_Gst.Rows[g]["TransactionTypeNo"].ToString());
                        mItemTaxInfo.HSNCode = strHSNCode;
                        mItemTaxInfo.FromDate = DBGetVal.ServerTime;
                        mItemTaxInfo.IsActive = true;
                        mItemTaxInfo.UserDate = DBGetVal.ServerTime;
                        mItemTaxInfo.UserID = DBGetVal.UserID;
                        mItemTaxInfo.IGSTPercent = Convert.ToDouble(dtItemTaxSetting_Purchase_Gst.Rows[g]["IGSTPercent"].ToString());
                        mItemTaxInfo.CGSTPercent = Convert.ToDouble(dtItemTaxSetting_Purchase_Gst.Rows[g]["CGSTPercent"].ToString());
                        mItemTaxInfo.SGSTPercent = Convert.ToDouble(dtItemTaxSetting_Purchase_Gst.Rows[g]["SGSTPercent"].ToString());
                        mItemTaxInfo.UTGSTPercent = Convert.ToDouble(dtItemTaxSetting_Purchase_Gst.Rows[g]["UTGSTPercent"].ToString());
                        mItemTaxInfo.CessPercent = Convert.ToDouble(dtItemTaxSetting_Purchase_Gst.Rows[g]["CessPercent"].ToString());
                        dbStockItems.AddMItemTaxInfo(mItemTaxInfo);
                        if (mItemTaxInfo.FromDate <= DBGetVal.ServerTime.Date)
                        {
                            break;
                        }
                    }
                }
                #endregion

                #endregion


                if (i > 0 && i % 20 == 0)
                {
                    if (dbStockItems.ExecuteNonQueryStatements_ImportItemMaster())
                    {
                        strPkSrNos = strPkSrNos.Substring(1);
                        dbStockItems = new DBMStockItems();
                        objT.ExecuteQuery("Update MStockItemsImport Set IsError = 0, StatusRemark = 'OK'" +
                            " Where PkSrNo in (" + strPkSrNos + ")", CommonFunctions.ConStr);
                        strPkSrNos = "";
                    }
                    else
                    {
                        strPkSrNos = strPkSrNos.Substring(1);
                        objT.ExecuteQuery("Update MStockItemsImport Set IsError = 1, StatusRemark = 'Unable to Add Product. Error : " + DBMStockItems.strerrormsg.Replace("'", "''") + "'" +
                            " Where PkSrNo in (" + strPkSrNos + ")", CommonFunctions.ConStr);
                        strPkSrNos = "";
                    }
                }
            }

            if (strPkSrNos.Length > 0)
            {
                if (dbStockItems.ExecuteNonQueryStatements_ImportItemMaster())
                {
                    strPkSrNos = strPkSrNos.Substring(1);
                    dbStockItems = new DBMStockItems();
                    objT.ExecuteQuery("Update MStockItemsImport Set IsError = 0, StatusRemark = 'OK'" +
                        " Where PkSrNo in (" + strPkSrNos + ")", CommonFunctions.ConStr);
                    strPkSrNos = "";
                }
                else
                {
                    strPkSrNos = strPkSrNos.Substring(1);
                    objT.ExecuteQuery("Update MStockItemsImport Set IsError = 1, StatusRemark = 'Unable to Add Product. Error : " + DBMStockItems.strerrormsg.Replace("'", "''") + "'" +
                        " Where PkSrNo in (" + strPkSrNos + ")", CommonFunctions.ConStr);
                }
            }

            ObjFunction.SetAppSettings();

        }

        private void insertStockData()
        {
            updatePkValues();

            JitControls.Transaction.QueryOutPut ObjQry = new JitControls.Transaction.QueryOutPut();
            //UPDATE ITEMNO PK BEFORE PROCEED
            //update Pks so we can get correct ItemNo while processing Stock records
            string sql = "";
            int iEffected = 0;
            #region fetch PkVoucherNo
            long OpStockPkVoucherNo = ObjQry.ReturnLong("Select PkVoucherNo from TVoucherEntry WHERE VoucherTypeCode = 0", CommonFunctions.ConStr);

            if (OpStockPkVoucherNo == 0)
            {
                sql = "Insert Into TVoucherEntry (VoucherTypeCode, VoucherUserNo, VoucherDate, VoucherTime, Narration, Reference, ChequeNo, ClearingDate, BilledAmount, ChallanNo, Remark, " +
                      " InwardLocationCode, MacNo, IsCancel, PayTypeNo, RateTypeNo, TaxTypeNo, IsVoucherLock, UserID, UserDate, ModifiedBy, OrderType, ReturnAmount, Visibility, " +
                      " DiscPercent, DiscAmt, MixMode) " +
                      " Values (0, 1, '1-Jan-1900', getdate(), 'Op. Stock', '', 0, '1-Jan-1900', 0, '', '', " +
                      " 0, 0, 'false', 0, 6, 0, 'false', 1, getdate(), NULL, 0, 0, 0, " +
                      " 0, 0, 0)";
                iEffected = objT.ExecuteQuery(sql, CommonFunctions.ConStr);
                if (iEffected == 0)
                {
                    throw new Exception("Product data added. Failed to update Stock. ERROR DESC : Unable to add record for opening stock entry. More details :: " + objT.ErrorMessage);
                }

                OpStockPkVoucherNo = ObjQry.ReturnLong("Select PkVoucherNo from TVoucherEntry WHERE VoucherTypeCode = 0", CommonFunctions.ConStr);

                if (OpStockPkVoucherNo == 0)
                {
                    throw new Exception("Product data added. Failed to update Stock. ERROR DESC : Unable fetch record of opening stock entry.");
                }
            }

            #endregion

            sql = "INSERT Into TStock (FKVoucherNo, FkVoucherTrnNo, FkVoucherSrNo, GroupNo, ItemNo, TrnCode, Quantity, BilledQuantity, Rate, Amount, NetRate, NetAmount, " +
                      "TaxPercentage, TaxAmount, DiscPercentage, DiscAmount, DiscRupees, DiscPercentage2, DiscAmount2, DiscRupees2, FkUomNo, FkStockBarCodeNo, " +
                      "FkRateSettingNo, FkItemTaxInfo, IsVoucherLock, FreeQty, FreeUOMNo, UserID, UserDate, ModifiedBy, LandedRate,DiscountType,HamaliInKg ) " +
                      "SELECT " + OpStockPkVoucherNo + ", 0, 1, MStockItems.GroupNo, MStockItems.ItemNo, 1, MStockItemsImport.Stock, MStockItemsImport.Stock, MStockItemsImport.MRP, MStockItemsImport.Stock * MStockItemsImport.MRP, MStockItemsImport.MRP * 100 / (100 + 0), MStockItemsImport.Stock * MStockItemsImport.MRP * 100 / (100 + 0), " +
                      " 0, MStockItemsImport.Stock * MStockItemsImport.MRP * 0 / (100 + 0), 0, 0, 0, 0, 0, 0, MStockItemsImport.UOMNo, MStockBarcode.PkStockBarcodeNo, " +
                      " MRateSetting.PkSrNo, 0 As FkItemTaxInfo, 'False', 0, 0, 1, getdate(), NULL, 0 ,1,0" +
                      " FROM " +
                      " MStockItems INNER JOIN " +
                      " MStockItemsImport ON MStockItemsImport.ItemNo = MStockItems.ItemNo INNER JOIN " +
                      " MStockBarcode ON MStockBarcode.ItemNo = MStockItems.ItemNo INNER JOIN " +
                      " MRateSetting ON MRateSetting.ItemNo = MStockItems.ItemNo AND MRateSetting.MRP = MStockItemsImport.MRP " +
                      " WHERE MStockItemsImport.Stock > 0 And (MStockItemsImport.IsValid=0 or MStockItemsImport.itemNo<>0) ";
            iEffected = objT.ExecuteQuery(sql, CommonFunctions.ConStr);

            sql = "INSERT Into TStockGodown (FKStockTrnNo, ItemNo, GodownNo, Qty, ActualQty, UserID, UserDate, ModifiedBy) " +
                      "SELECT PkStockTrnNo, ItemNo, 2, Quantity, BilledQuantity, UserID, UserDate, ModifiedBy " +
                      " FROM " +
                      " TStock WHERE FKVoucherNo = " + OpStockPkVoucherNo + " ";
            iEffected = objT.ExecuteQuery(sql, CommonFunctions.ConStr);

            sql = "EXEC StockUpdateAll ";
            iEffected = objT.ExecuteQuery(sql, CommonFunctions.ConStr);


        }

    }
}
