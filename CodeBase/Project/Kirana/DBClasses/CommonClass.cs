﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Management;
using Kirana;
using System.Data.SqlClient;
using System.Net.NetworkInformation;
using System.Security.AccessControl;
using System.Net;
using System.Threading;
using JitControls;
using Newtonsoft.Json;
using System.Xml.Linq;
using System.Net.Mail;

namespace OM
{
    internal class CommonFunctions : JitFunctions
    {
        private DataSet dset = new DataSet();
        private Transaction.GetDataSet objTrans = new Transaction.GetDataSet();
        private Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();
        private Transaction.Transactions objT = new Transaction.Transactions();

        internal static string SystemName = "Kirana";//"Wines";
        internal static string UserName = "", CompanyName = "", BrName = "", ServerName = "", DatabaseName = "", DefaultDatabaseName = "", DefaultServerName = "";
        internal static string ApplicationError = "", SessionExpired = "", ErrorMessge = "", strErrorMsg = "", ConStrServer, ConStr, ConStrTools;
        internal static long VoucherLock;
        internal static string ErrorTitle = SystemName + " System";
        internal static object MainPassword = "";
        internal Security secure = new Security();
        internal static string ReportPath, AboutInfo, SecureInfo;
        internal static MDIParent1 mMain;
        internal static Color MainFrom = OM.ThemeColor.Body_Back_Color;
        DBAssemblyInfo dbAss = new DBAssemblyInfo();
        internal Control ctrlFocus = null; bool flagFocus = true;
        internal static DataTable dtHelp = new DataTable();

        internal static DataTable dtAppSettings;
        private static string LUserID, LPassword;
        internal static string DBName = "Kirana0001";

        internal SqlDataReader GetData(string Sql)
        {
            SqlConnection con = new SqlConnection(ConStr);
            con.Open();
            SqlCommand cmd = new SqlCommand(Sql, con);

            return cmd.ExecuteReader(CommandBehavior.CloseConnection);
        }

        internal bool SetConnection(string dbName)
        {
            try
            {
                string sysDrive = System.IO.Path.GetPathRoot(Environment.SystemDirectory);
                OMCommonClass cc = new OMCommonClass();
                //bool flag = false;

                string fname = sysDrive + "Windows\\System32\\" + dbAss.AssemblyTitle + " RegisteredSerial.dat";
                StreamReader objreader = new StreamReader(fname);

                string str = objreader.ReadLine(); SecureInfo = SecureInfo + "Mac ID:" + secure.psDecrypt(str) + "\n";
                str = objreader.ReadLine();
                MainPassword = secure.psDecrypt(str); SecureInfo = SecureInfo + "PWD:" + secure.psDecrypt(str) + "\n";
                str = objreader.ReadLine();
                ServerName = secure.psDecrypt(str); SecureInfo = SecureInfo + "Data Source Name:" + secure.psDecrypt(str);
                str = objreader.ReadLine();
                DatabaseName = secure.psDecrypt(str);
                str = objreader.ReadLine();

                objreader.Close();
                objreader = null;

                //Version Change Checking
                fname = sysDrive + "Windows\\System32\\" + dbAss.AssemblyTitle + " Registered.dat";
                objreader = new StreamReader(fname);
                str = objreader.ReadLine();
                str = objreader.ReadLine();
                str = objreader.ReadLine();
                str = objreader.ReadLine();
                if (str.IndexOf(dbAss.AssemblyVersion) == -1)
                {
                    str = objreader.ReadLine();
                    str = objreader.ReadLine();
                    str = objreader.ReadLine();
                    DateTime dt = Convert.ToDateTime(str.Substring(("Installation Date:").ToString().Length).Trim());
                    objreader.Close();
                    objreader = null;
                    OMCommonClass c1 = new OMCommonClass(true);
                    c1.setPath(dbAss.AssemblyTitle);
                    c1.WriteUpdateVersionFile(dt, dbAss.AssemblyTitle, dbAss.AssemblyProduct, dbAss.AssemblyVersion, dbAss.AssemblyCopyright, dbAss.AssemblyGUID);
                    string strMsg = "";
                    strMsg += "\n===========================================================";
                    strMsg += "\n                    System Information                     ";
                    strMsg += "\n===========================================================";
                    strMsg += "\nRetailer System: " + Application.ProductVersion;
                    strMsg += "\nCompany Name:" + Application.CompanyName;

                    strMsg += "\nInstallation Date: \t\t" + dt;
                    strMsg += "\nModified Date: \t\t" + DateTime.Now;
                    strMsg += "\nAssembly Title: \t\t" + String.Format("About " + dbAss.AssemblyTitle);
                    strMsg += "\nAssembly Product: \t\t" + dbAss.AssemblyProduct;
                    strMsg += "\nAssembly Version: \t\t" + String.Format("Version {0}", dbAss.AssemblyVersion);
                    strMsg += "\nAssembly Copyright:\t" + dbAss.AssemblyCopyright;
                    strMsg += "\nAssembly GUID:\t\t" + dbAss.AssemblyGUID;
                    strMsg += "\n";
                    strMsg += "\nDeveloped By:";
                    strMsg += "\nDeveloped By:";
                    strMsg += "\n===========================================================";
                    strMsg += "\nThank you for using our software.";

                    OMMessageBox.Show(dbAss.AssemblyTitle + " Version is change. New Version system is more user freindly\n" + strMsg, CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);

                }
                else
                {
                    objreader.Close();
                    objreader = null;
                }

                //==============================

                AboutInfo = cc.GetAbountInfo(dbAss.AssemblyTitle);
                ConStr = "Data Source=" + ServerName + ";Initial Catalog=" + dbName + ";Integrated Security=true;Connection Timeout=10000;";
                return true;
            }
            catch (Exception ex)
            {
                OMMessageBox.Show("System Setting is not properly set.......", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                ErrorMessge = ex.Message;
                return false;
            }
        }

        internal static string RegCompName()
        {
            DBAssemblyInfo dbAss = new DBAssemblyInfo();
            return "Powered By " + dbAss.AssemblyCompany;
        }

        internal System.Data.SqlClient.SqlDataReader GetData(string Sql, string ConStr)
        {
            System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(ConStr);
            con.Open();
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(Sql, con);

            return cmd.ExecuteReader(CommandBehavior.CloseConnection);
        }

        internal DataView GetDataView(string sql)
        {
            System.Data.SqlClient.SqlConnection Con = new System.Data.SqlClient.SqlConnection(ConStr);
            System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch
            {
                throw;
            }
            finally
            {
                Con.Close();
            }

            return ds.Tables[(0)].DefaultView;
        }

        internal DataView GetDataView(string sql, string str)
        {
            System.Data.SqlClient.SqlConnection Con = new System.Data.SqlClient.SqlConnection(str);
            System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch
            {
                throw;
            }
            finally
            {
                Con.Close();
            }

            return ds.Tables[(0)].DefaultView;
        }

        internal void FillCombo(ComboBox cmb)
        {
            //The Fillcombo is used for filling of dropdownlist the qurey code is filled in value propery and name in text property 
            // Dim text As String

            DataTable dt = new DataTable();
            dt.Columns.Add("ID"); dt.Columns.Add("Desc");
            DataRow dr = dt.NewRow();
            dr[0] = "0";
            dr[1] = " ------ Select ------ ";
            dt.Rows.Add(dr);

            cmb.DisplayMember = dt.Columns[1].ColumnName;
            cmb.ValueMember = dt.Columns[0].ColumnName;
            cmb.DataSource = dt;
            cmb.SelectedIndex = 0;
        }

        internal void FillComb(ComboBox cmb, string FColName, string SColName)
        {
            //The Fillcombo is used for filling of dropdownlist the qurey code is filled in value propery and name in text property 
            // Dim text As String

            DataTable dt = new DataTable();
            dt.Columns.Add(FColName); dt.Columns.Add(SColName);
            DataRow dr = dt.NewRow();
            dr[0] = "0";
            dr[1] = " ------ Select ------ ";
            dt.Rows.Add(dr);

            cmb.DisplayMember = dt.Columns[1].ColumnName;
            cmb.ValueMember = dt.Columns[0].ColumnName;
            cmb.DataSource = dt;
            cmb.SelectedIndex = 0;
        }

        internal void FillCombo(ComboBox cmb, string Str)
        {
            //The Fillcombo is used for filling of dropdownlist the qurey code is filled in value propery and name in text property 
            // Dim text As String

            dset = objTrans.FillDset("NewTable", Str, ConStr);
            //i = 0;

            DataTable dt = dset.Tables[0];
            DataRow dr = dt.NewRow();
            dr[0] = "0";

            dr[1] = " ------ Select ------ ";

            dset.Tables[0].Rows.InsertAt(dr, 0);
            cmb.DisplayMember = dset.Tables[0].Columns[1].ColumnName;
            cmb.ValueMember = dset.Tables[0].Columns[0].ColumnName;
            cmb.DataSource = dset.Tables[0];
            cmb.SelectedIndex = 0;
        }

        internal void FillComb(ComboBox cmb, string Str)
        {
            //The Fillcombo is used for filling of dropdownlist the qurey code is filled in value propery and name in text property 
            // Dim text As String
            dset = objTrans.FillDset("NewTable", Str, ConStr);
            //i = 0;


            cmb.DisplayMember = dset.Tables[0].Columns[1].ColumnName;
            cmb.ValueMember = dset.Tables[0].Columns[0].ColumnName;
            cmb.DataSource = dset.Tables[0];
            cmb.SelectedIndex = 0;
        }

        internal void FillCombo(ComboBox cmb, string Str, string AddStr)
        {
            //The Fillcombo is used for filling of dropdownlist the qurey code is filled in value propery and name in text property 
            // Dim text As String
            dset = objTrans.FillDset("NewTable", Str, ConStr);
            //i = 0;

            DataTable dt = dset.Tables[0];
            DataRow dr = dt.NewRow();
            dr[0] = "0";
            dr[1] = " ------ Select ------ ";
            dset.Tables[0].Rows.InsertAt(dr, 0);

            dr = dt.NewRow();
            dr[0] = "-1";
            dr[1] = AddStr;
            dset.Tables[0].Rows.InsertAt(dr, 1);

            cmb.DisplayMember = dset.Tables[0].Columns[1].ColumnName;
            cmb.ValueMember = dset.Tables[0].Columns[0].ColumnName;
            cmb.DataSource = dset.Tables[0];
            cmb.SelectedIndex = 0;
        }

        internal void FillCombo(DataGridViewComboBoxColumn cmb, string Str)
        {
            dset = objTrans.FillDset("NewTable", Str, ConStr);
            //i = 0;

            DataTable dt = dset.Tables[0];
            DataRow dr = dt.NewRow();
            dr[0] = "0";
            dr[1] = " ------ Select ------ ";

            dset.Tables[0].Rows.InsertAt(dr, 0);
            cmb.DisplayMember = dset.Tables[0].Columns[1].ColumnName;
            cmb.ValueMember = dset.Tables[0].Columns[0].ColumnName;
            cmb.DataSource = dset.Tables[0];

        }

        internal void FillComb(DataGridViewComboBoxColumn cmb, string Str)
        {
            //The Fillcombo is used for filling of dropdownlist the qurey code is filled in value propery and name in text property 
            // Dim text As String
            dset = objTrans.FillDset("NewTable", Str, ConStr);
            //i = 0;


            cmb.DisplayMember = dset.Tables[0].Columns[1].ColumnName;
            cmb.ValueMember = dset.Tables[0].Columns[0].ColumnName;
            cmb.DataSource = dset.Tables[0];
        }

        internal void FillCombo(DataGridViewComboBoxCell cmb, string Str)
        {

            dset = objTrans.FillDset("NewTable", Str, ConStr);
            //i = 0;

            DataTable dt = dset.Tables[0];
            DataRow dr = dt.NewRow();
            dr[0] = "0";

            dr[1] = " ------ Select ------ ";
            dset.Tables[0].Rows.InsertAt(dr, 0);
            cmb.DisplayMember = dset.Tables[0].Columns[1].ColumnName;
            cmb.ValueMember = dset.Tables[0].Columns[0].ColumnName;
            cmb.DataSource = dset.Tables[0];

        }

        internal void FillComb(DataGridViewComboBoxCell cmb, string Str)
        {

            dset = objTrans.FillDset("NewTable", Str, ConStr);
            //i = 0;

            cmb.DisplayMember = dset.Tables[0].Columns[1].ColumnName;
            cmb.ValueMember = dset.Tables[0].Columns[0].ColumnName;
            cmb.DataSource = dset.Tables[0];

        }

        internal void FillLanguage(ComboBox cmb, int Type)
        {
            //The Fillcombo is used for filling of dropdownlist the qurey code is filled in value propery and name in text property 
            // Dim text As String

            DataTable dt = new DataTable();
            dt.Columns.Add("NO");
            dt.Columns.Add("Name");

            DataRow dr;
            if (Type == 1)
            {
                dr = dt.NewRow();
                dr[0] = "1";
                dr[1] = "English";
                dt.Rows.Add(dr);
            }
            dr = dt.NewRow();
            dr[0] = "2";
            dr[1] = "Marathi";
            dt.Rows.Add(dr);

            //dr = dt.NewRow();
            //dr[0] = "3";
            //dr[1] = "Hindi";
            //dt.Rows.Add(dr);

            cmb.DisplayMember = dt.Columns[1].ColumnName;
            cmb.ValueMember = dt.Columns[0].ColumnName;
            cmb.DataSource = dt;
            cmb.SelectedIndex = 0;
        }

        internal void FillList(ListBox lst, string Str)
        {
            //The Fillcombo is used for filling of dropdownlist the qurey code is filled in value propery and name in text property 
            // Dim text As String
            dset = objTrans.FillDset("NewTable", Str, ConStr);
            //i = 0;


            lst.DisplayMember = dset.Tables[0].Columns[1].ColumnName;
            lst.ValueMember = dset.Tables[0].Columns[0].ColumnName;
            lst.DataSource = dset.Tables[0];
            //cmb.Text = "";
        }

        internal void FillList(ListBox lst, string ColName1, string ColName2)
        {
            //The Fillcombo is used for filling of dropdownlist the qurey code is filled in value propery and name in text property 
            // Dim text As String
            //i = 0;
            DataTable dt = new DataTable();
            dt.Columns.Add(ColName1);
            dt.Columns.Add(ColName2);

            lst.DisplayMember = dt.Columns[1].ColumnName;
            lst.ValueMember = dt.Columns[0].ColumnName;
            lst.DataSource = dt;
            //cmb.Text = "";
        }

        internal void FillDays(ComboBox cmb)
        {
            DataTable dtDays = new DataTable();
            dtDays.Columns.Add("No", Type.GetType("System.Int32"));
            dtDays.Columns.Add("Name");

            for (int i = 1; i < 32; i++)
            {

                DataRow dr = dtDays.NewRow();
                dr[0] = i;
                dr[1] = i;
                dtDays.Rows.Add(dr);
            }
            cmb.DisplayMember = "Name";
            cmb.ValueMember = "No";
            cmb.DataSource = dtDays;
        }

        internal void FillMonth(ComboBox cmb)
        {
            DataTable dtMonth = new DataTable();
            dtMonth.Columns.Add("No", Type.GetType("System.Int32"));
            dtMonth.Columns.Add("Name");
            string strMonth = "";
            for (int i = 1; i <= 12; i++)
            {
                if (i == 1) strMonth = "Jan";
                else if (i == 2) strMonth = "Feb";
                else if (i == 3) strMonth = "Mar";
                else if (i == 4) strMonth = "Apr";
                else if (i == 5) strMonth = "May";
                else if (i == 6) strMonth = "Jun";
                else if (i == 7) strMonth = "Jul";
                else if (i == 8) strMonth = "Aug";
                else if (i == 9) strMonth = "Sep";
                else if (i == 10) strMonth = "Oct";
                else if (i == 11) strMonth = "Nov";
                else if (i == 12) strMonth = "Dec";

                DataRow dr = dtMonth.NewRow();
                dr[0] = i;
                dr[1] = strMonth;
                dtMonth.Rows.Add(dr);
            }
            cmb.DisplayMember = "Name";
            cmb.ValueMember = "No";
            cmb.DataSource = dtMonth.DefaultView;
        }

        internal void FillYear(ComboBox cmb)
        {
            DataTable dtYear = new DataTable();
            dtYear.Columns.Add("No", Type.GetType("System.Int32"));
            dtYear.Columns.Add("Name");

            for (int i = 1901; i <= DateTime.Now.Year; i++)
            {

                DataRow dr = dtYear.NewRow();
                dr[0] = i;
                dr[1] = i;
                dtYear.Rows.Add(dr);
            }
            cmb.DisplayMember = "Name";
            cmb.ValueMember = "No";
            cmb.DataSource = dtYear;
        }

        internal void FillWeek(ComboBox cmb)
        {
            DataTable dtMonth = new DataTable();
            dtMonth.Columns.Add("No", Type.GetType("System.Int32"));
            dtMonth.Columns.Add("Name");
            string strMonth = "";
            for (int i = 0; i <= 6; i++)
            {
                if (i == 0) strMonth = "Sunday";
                else if (i == 1) strMonth = "Monday";
                else if (i == 2) strMonth = "Tuesday";
                else if (i == 3) strMonth = "Wednesday";
                else if (i == 4) strMonth = "Thursday";
                else if (i == 5) strMonth = "Friday";
                else if (i == 6) strMonth = "Saturday";

                DataRow dr = dtMonth.NewRow();
                dr[0] = i;
                dr[1] = strMonth;
                dtMonth.Rows.Add(dr);
            }
            cmb.DisplayMember = "Name";
            cmb.ValueMember = "No";
            cmb.DataSource = dtMonth.DefaultView;
        }

        internal void OpenForm(Form frmChild, Form frmParent)
        {
            bool flag = true;
            foreach (Form frm in Application.OpenForms)
            {
                if (frmChild.Name != "SalesRegister" && frmChild.Name != "VatRegister" && frmChild.Name != "ReportViewSource")
                {
                    if (frm.Name == frmChild.Name || frm.Name == frmChild.Name + "AE")
                    {
                        flag = false; break;
                    }
                }
            }
            if (flag == true)
            {

                frmChild.BackColor = ThemeColor.Body_Back_Color;// frmChild.BackColor = Color.FromArgb(230, 235, 235);

                //frmChild.StartPosition = FormStartPosition.CenterScreen;

                FormatControl(frmChild.Controls);

                KeyPressFormat(frmChild.Controls);

                //SetHelpInfo(frmChild);
                if (frmChild.Name != "ReportViewSource")
                {
                    frmChild.MaximizeBox = false;
                    frmChild.MinimizeBox = false;
                    frmChild.MdiParent = frmParent;
                    frmChild.ControlBox = false;
                    frmChild.Activated += new EventHandler(FormActivated);
                    frmChild.Resize += new EventHandler(frmChild_Resize);
                    //  frmChild.Paint += new PaintEventHandler(frmChild_Paint);
                    frmChild.BackColor = MainFrom;

                    setToolBarControl(frmParent.Controls, frmChild);
                    frmChild.StartPosition = FormStartPosition.Manual;
                    frmChild.Top = 0;
                    frmChild.FormBorderStyle = FormBorderStyle.None;
                    frmChild.Text = "";
                    frmChild.KeyPreview = true;
                    DBGetVal.MainForm.lblDispTaskbar.Visible = false;
                    frmChild.Show();
                    frmChild.Dock = DockStyle.Fill;
                }
                else
                {
                    frmChild.WindowState = FormWindowState.Maximized;
                    frmChild.ShowDialog();
                }
            }
        }

        public DialogResult OpenDialog(Form frmChild, Form frmParent)
        {
            FormatControl(frmChild.Controls);
            KeyPressFormat(frmChild.Controls);
            frmChild.BackColor = OM.ThemeColor.Body_Back_Color;
            frmChild.FormBorderStyle = FormBorderStyle.None;
            frmChild.StartPosition = FormStartPosition.CenterScreen;
            frmChild.Resize += new EventHandler(frmChild_Resize);
            frmChild.Paint += new PaintEventHandler(frmChildOther_Paint);
            frmChild.KeyPreview = true;
            return frmChild.ShowDialog(frmParent);
        }

        internal void OpenForm(Form frmChild)
        {
            bool flag = true;
            foreach (Form frm in Application.OpenForms)
            {
                if (frmChild.Name != "SalesRegister" && frmChild.Name != "VatRegister" && frmChild.Name != "KeyBoard")
                {
                    if (frm.Name == frmChild.Name || frm.Name == frmChild.Name + "AE")
                    {
                        flag = false; break;
                    }
                }
            }
            if (flag == true)
            {
                frmChild.MinimizeBox = false;
                frmChild.BackColor = ThemeColor.Body_Back_Color;// frmChild.BackColor = Color.FromArgb(213, 225, 230);
                frmChild.MaximizeBox = false;
                frmChild.FormBorderStyle = FormBorderStyle.None;
                frmChild.StartPosition = FormStartPosition.CenterScreen;
                frmChild.Resize += new EventHandler(frmChild_Resize);
                frmChild.Paint += new PaintEventHandler(frmChildOther_Paint);
                FormatControl(frmChild.Controls);
                frmChild.KeyPreview = true;
                KeyPressFormat(frmChild.Controls);
                frmChild.ShowDialog();
            }
        }

        internal void frmChild_Paint(object sender, PaintEventArgs g)
        {
            Rectangle rect = ((Form)(sender)).ClientRectangle;
            if (rect.Height != 0 && rect.Width != 0)
            {
                LinearGradientBrush brush = new LinearGradientBrush(rect, Color.AliceBlue, Color.LightSteelBlue, 60);

                g.Graphics.FillRectangle(brush, rect);
            }
        }

        internal void frmChildOther_Paint(object sender, PaintEventArgs g)
        {
            Rectangle rect = ((Form)(sender)).ClientRectangle;
            if (rect.Height != 0 && rect.Width != 0)
            {
                LinearGradientBrush brush = new LinearGradientBrush(rect, Color.AliceBlue, Color.LightSteelBlue, 60);
                g.Graphics.FillRectangle(brush, rect);


                Pen pen = new Pen(Color.SteelBlue, 2);
                pen.Alignment = System.Drawing.Drawing2D.PenAlignment.Outset;
                g.Graphics.DrawRectangle(pen, rect);
            }
        }

        internal void frmChild_Resize(object sender, EventArgs e)
        {
            foreach (Control ctrl in ((Form)sender).Controls)
            {
                if (ctrl is Panel)
                {
                    if (ctrl.Name == "pnlMain")
                    {
                        ctrl.Location = new Point(((Form)sender).Width / 2 - ctrl.Width / 2, (((((Form)sender).Height / 2) - (ctrl.Height / 2)) / 2) / 2);
                    }
                    else if (ctrl.Name == "pnlMainForm")
                    {
                        ctrl.Location = new Point(((Form)sender).Width / 2 - ctrl.Width / 2, 5);
                        ctrl.Height = ((Form)sender).Height - 10;
                        // ((Panel)ctrl).BorderStyle = BorderStyle.FixedSingle;
                        //((Panel)ctrl).Paint += new PaintEventHandler(panelBorder_Paint);
                        //panelBorder_Paint(new PaintEventArgs(((Panel)ctrl).CreateGraphics(), ((Panel)ctrl).ClientRectangle));
                    }
                }
            }
        }

        internal void panelBorder_Paint(object sender, PaintEventArgs e)
        {
            Pen p = new Pen(Color.CadetBlue, 1);
            e.Graphics.DrawRectangle(Pens.CadetBlue,
            e.ClipRectangle.Left,
            e.ClipRectangle.Top,
            e.ClipRectangle.Width - 1,
            e.ClipRectangle.Height - 1);
            // base.OnPaint(e);
        }

        internal void FormActivated(object sender, EventArgs e)
        {
            foreach (Form frm in Application.OpenForms)
            {
                if (frm.Name == ((Form)sender).Name)
                {
                    foreach (ToolBarButton tbl in mMain.toolBar1.Buttons)
                    {
                        if (tbl.Name == frm.Name)
                            mMain.lblMainTitle.Text = tbl.Text;
                    }
                    break;
                }
            }
        }

        internal void setToolBarControl(System.Windows.Forms.Control.ControlCollection ctrls, Form frmChild)
        {
            foreach (Control ctrl in ctrls)
            {
                if (ctrl is ToolBar)
                {
                    if (((ToolBar)ctrl).Buttons.Count == 0) ((Panel)ctrls.Owner).Visible = true;
                    ToolBarButton tbl = new ToolBarButton();
                    tbl.Name = frmChild.Name;
                    tbl.Text = frmChild.Text;
                    tbl.ImageIndex = 0;
                    tbl.Style = ToolBarButtonStyle.PushButton;
                    ((ToolBar)ctrl).Buttons.Add(tbl);
                    mMain.lblMainTitle.Text = tbl.Text;
                }
                else if (ctrl is System.Windows.Forms.Panel)
                {
                    setToolBarControl(ctrl.Controls, frmChild);
                }
            }

        }

        private void ButtonMouseHover(object sender, EventArgs e)
        {
            //((Button)sender).BackgroundImage = global::Kirana.Properties.Resources.btnBGOver;
            //((Button)sender).BackgroundImageLayout = ImageLayout.Stretch;
            //((Button)sender).ForeColor = Color.Black;

            //((Button)sender).BackColor = Color.LightSteelBlue;
            ((Button)sender).FlatAppearance.BorderSize = 1;
            ((Button)sender).FlatAppearance.BorderColor = ThemeColor.Button_Border_Active; //  ((Button)sender).FlatAppearance.BorderColor = Color.Red;
        }

        private void ButtonMouseLeave(object sender, EventArgs e)
        {
            if (((Button)sender).Focused == false)
            {
                //((Button)sender).BackgroundImage = global::Kirana.Properties.Resources.btnBG;
                //((Button)sender).BackgroundImageLayout = ImageLayout.Stretch;
                //((Button)sender).ForeColor = Color.White;
                ((Button)sender).FlatAppearance.BorderSize = 1;
                ((Button)sender).FlatAppearance.BorderColor = ThemeColor.Button_Border;// ((Button)sender).FlatAppearance.BorderColor = Color.White;

                //((Button)sender).ForeColor = Color.White;
                //((Button)sender).BackColor = Color.SteelBlue;
            }
        }

        internal void FormatControl(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            DataGridView ctrl1 = null;
            DateTimePicker ctrl2 = null;
            Button ctrl3 = null;
            foreach (Control ctrl in ctrls)
            {
                if (ctrl is DataGridView)
                {

                    ctrl1 = new DataGridView();
                    ctrl1 = (DataGridView)ctrl;
                    ctrl1.BackgroundColor = Color.White;
                    ctrl1.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(193, 211, 219);
                    ctrl1.ColumnHeadersDefaultCellStyle.Font = GetFont();
                    ctrl1.ColumnHeadersHeight = 20;
                    ctrl1.RowTemplate.Height = 20;
                    ctrl1.RowHeadersDefaultCellStyle.BackColor = Color.FromArgb(193, 211, 219);
                    ctrl1.RowHeadersDefaultCellStyle.Font = GetFont();
                    ctrl1.RowTemplate.DefaultCellStyle.SelectionBackColor = Color.FromArgb(255, 192, 130);
                    ctrl1.RowTemplate.DefaultCellStyle.SelectionForeColor = Color.Black;
                    ctrl1.RowTemplate.DefaultCellStyle.Font = GetSimpleFont(FontStyle.Regular);
                    ctrl1.DefaultCellStyle.Font = GetSimpleFont(FontStyle.Regular);
                    ctrl1.RowTemplate.DefaultCellStyle.ForeColor = Color.Black;
                    ctrl1.RowTemplate.Resizable = DataGridViewTriState.False;
                    if (ctrl1.Rows.Count > 0) ctrl1.Rows[0].DefaultCellStyle.Font = GetFont();
                    ctrl1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
                    ctrl1.RowHeadersVisible = false;
                    ctrl1.ColumnAdded += new DataGridViewColumnEventHandler(DatGridView_ColumnAdded);
                    ctrl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));

                    for (int i = 0; i < ctrl1.Columns.Count; i++)
                        ctrl1.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;

                }
                else if (ctrl is DateTimePicker)
                {
                    ((DateTimePicker)ctrl).GotFocus += new EventHandler(ControlGotFocus);
                    ((DateTimePicker)ctrl).LostFocus += new EventHandler(ControlLostFocus);

                    ctrl2 = new DateTimePicker();
                    ctrl2 = (DateTimePicker)ctrl;
                    //ctrl2.MinDate = DBGetVal.FromDate;
                    //ctrl2.MaxDate = DBGetVal.ToDate;
                    ctrl2.Format = DateTimePickerFormat.Custom;
                    ctrl2.CustomFormat = "dd-MMM-yyyy";
                    ctrl2.Font = GetFont();
                    ctrl2.Width = 110;
                }
                //else if (ctrl is JitControls.OMButton)
                //{
                //    System.Windows.Forms.Button ctrl4 = new System.Windows.Forms.Button();
                //    ctrl4 = (System.Windows.Forms.Button)ctrl;
                //    //ctrl3.BackColor = Color.Black;
                //    ctrl4.Font = GetFont(FontStyle.Regular);
                //    ctrl4.Text = ctrl4.Text.ToUpper();

                //    if (ctrl4.Text == "SAVE") ctrl4.Text = ctrl4.Text.Replace("S", "&S");
                //    else if (ctrl4.Text == "DELETE") ctrl4.Text = ctrl4.Text.Replace("D", "&D");
                //    else if (ctrl4.Text == "PRINT") ctrl4.Text = ctrl4.Text.Replace("P", "&P");
                //    else if (ctrl4.Text == "EXIT") ctrl4.Text = ctrl4.Text.Replace("X", "&X");
                //    else if (ctrl4.Text == "SHOW") ctrl4.Text = ctrl4.Text.Replace("S", "&S");
                //    else if (ctrl4.Text == "UPDATE") ctrl4.Text = ctrl4.Text.Replace("U", "&U");
                //    else if (ctrl4.Text == "NEW") ctrl4.Text = ctrl4.Text.Replace("N", "&N");
                //    else if (ctrl4.Text == "SEARCH") ctrl4.Text = ctrl4.Text.Replace("H", "&H");
                //    else if (ctrl4.Text == "SEARCH1") ctrl4.Text = ctrl4.Text.Replace("1", "&1");
                //    else if (ctrl4.Text == "SEARCH2") ctrl4.Text = ctrl4.Text.Replace("2", "&2");
                //    else if (ctrl4.Text == "CLOSE") ctrl4.Text = ctrl4.Text.Replace("E", "&E");
                //    else if (ctrl4.Text == "CANCEL") ctrl4.Text = ctrl4.Text.Replace("C", "&C");
                //    else if (ctrl4.Text == "PRINT BARCODE") ctrl4.Text = ctrl4.Text.Replace("P", "&P");

                //    ctrl4.BackgroundImage = global::Kirana.Properties.Resources.btnBG;
                //    ctrl4.BackgroundImageLayout = ImageLayout.Stretch;
                //    ctrl4.ForeColor = Color.Black;
                //    ctrl4.FlatStyle = FlatStyle.Flat;
                //    ctrl4.FlatAppearance.BorderSize = 0;

                //    ctrl4.GradientBottom = Color.FromArgb(226, 236, 239);// System.Drawing.Color.FromArgb(63, 92, 103);
                //    ctrl4.GradientBottomOnMouse = Color.LightGoldenrodYellow;// System.Drawing.Color.FromArgb(101, 221, 237);
                //    ctrl4.GradientMiddle = Color.FromArgb(226, 236, 239);
                //    ctrl4.GradientMiddleOnMouse = Color.LightGoldenrodYellow; //System.Drawing.Color.White;
                //    ctrl4.GradientShow = true;
                //    ctrl4.GradientTop = System.Drawing.Color.FromArgb(63, 92, 103);
                //    ctrl4.GradientTopOnMouse = Color.Orange;// System.Drawing.Color.Silver;
                //    ctrl4.CornerRadius = 1;
                //    //ctrl4.MouseHover += new EventHandler(ButtonMouseHover);
                //    //ctrl4.GotFocus += new EventHandler(ButtonMouseHover);
                //    //ctrl4.MouseLeave += new EventHandler(ButtonMouseLeave);
                //    //ctrl4.LostFocus += new EventHandler(ButtonMouseLeave);
                //    if (ctrl4.Text == "<<" || ctrl4.Text == "<" || ctrl4.Text == ">" || ctrl4.Text == ">>")
                //        ctrl4.Font = GetSimpleFont(FontStyle.Bold);
                //    if (ctrl4.Name == "btnShowAll") ctrl4.Width = 102;
                //    //ctrl1.FlatAppearance.BorderColor = Color.Black;
                //}
                else if (ctrl is Button)
                {
                    ctrl3 = new Button();
                    ctrl3 = (Button)ctrl;
                    //ctrl3.BackColor = Color.Black;
                    ctrl3.Font = GetFont(FontStyle.Bold);
                    ctrl3.Text = ctrl3.Text.ToUpper();

                    if (ctrl3.Text == "SAVE") ctrl3.Text = ctrl3.Text.Replace("S", "&S");
                    else if (ctrl3.Text == "DELETE") ctrl3.Text = ctrl3.Text.Replace("D", "&D");
                    else if (ctrl3.Text == "PRINT") ctrl3.Text = ctrl3.Text.Replace("P", "&P");
                    else if (ctrl3.Text == "EXIT") ctrl3.Text = ctrl3.Text.Replace("X", "&X");
                    else if (ctrl3.Text == "SHOW") ctrl3.Text = ctrl3.Text.Replace("S", "&S");
                    else if (ctrl3.Text == "UPDATE") ctrl3.Text = ctrl3.Text.Replace("U", "&U");
                    else if (ctrl3.Text == "NEW") ctrl3.Text = ctrl3.Text.Replace("N", "&N");
                    else if (ctrl3.Text == "SEARCH") ctrl3.Text = ctrl3.Text.Replace("H", "&H");
                    else if (ctrl3.Text == "SEARCH1") ctrl3.Text = ctrl3.Text.Replace("1", "&1");
                    else if (ctrl3.Text == "SEARCH2") ctrl3.Text = ctrl3.Text.Replace("2", "&2");
                    else if (ctrl3.Text == "CLOSE") ctrl3.Text = ctrl3.Text.Replace("E", "&E");
                    else if (ctrl3.Text == "CANCEL") ctrl3.Text = ctrl3.Text.Replace("C", "&C");
                    else if (ctrl3.Text == "PRINT BARCODE") ctrl3.Text = ctrl3.Text.Replace("P", "&P");

                    //ctrl3.BackgroundImage = global::Kirana.Properties.Resources.btnBG;
                    //ctrl3.BackgroundImageLayout = ImageLayout.Stretch;
                    ctrl3.ForeColor = ThemeColor.Button_ForeColor;
                    //  ctrl3.ForeColor = Color.White;
                    ctrl3.FlatStyle = FlatStyle.Flat;
                    ctrl3.FlatAppearance.BorderSize = 1;
                    // ctrl3.FlatAppearance.BorderColor = Color.White;
                    ctrl3.FlatAppearance.BorderColor = ThemeColor.Button_Border;
                    // ctrl3.BackColor = Color.Coral;
                    ctrl3.BackColor = ThemeColor.Button_BackGround;
                    ctrl3.FlatAppearance.MouseOverBackColor = ThemeColor.Button_BackGround_Hover;
                    ctrl3.FlatAppearance.MouseDownBackColor = ThemeColor.Button_BackGround_Active;

                    //ctrl3.FlatAppearance.MouseOverBackColor = Color.Crimson;// Color.SkyBlue;
                    //ctrl3.FlatAppearance.MouseDownBackColor = Color.FromArgb(255, 128, 0);
                    ctrl3.GotFocus += new EventHandler(ButtonMouseHover);
                    ctrl3.LostFocus += new EventHandler(ButtonMouseLeave);
                    //if (ctrl3.Text == "<<" || ctrl3.Text == "<" || ctrl3.Text == ">" || ctrl3.Text == ">>")
                    //  ctrl3.Font = GetSimpleFont(FontStyle.Bold);
                    if (ctrl3.Name == "btnShowAll") ctrl3.Width = 102;
                    //ctrl1.FlatAppearance.BorderColor = Color.Black;
                }

                else if (ctrl is ProgressBar)
                {
                    ((ProgressBar)ctrl).BackColor = Color.FromArgb(78, 116, 133);
                    ((ProgressBar)ctrl).ForeColor = Color.FromArgb(193, 211, 219);
                }
                else if (ctrl is TextBox)
                {
                    ((TextBox)ctrl).GotFocus += new EventHandler(ControlGotFocus);
                    ((TextBox)ctrl).LostFocus += new EventHandler(ControlLostFocus);

                    ((TextBox)ctrl).BorderStyle = BorderStyle.Fixed3D;
                    ((TextBox)ctrl).Font = GetFont();
                    if (((TextBox)ctrl).Multiline == false)
                        ((TextBox)ctrl).Height = 24;
                }
                else if (ctrl is CheckBox)
                {
                    ((CheckBox)ctrl).GotFocus += new EventHandler(ControlGotFocus);
                    ((CheckBox)ctrl).LostFocus += new EventHandler(ControlLostFocus);
                }
                else if (ctrl is ComboBox)
                {
                    ((ComboBox)ctrl).GotFocus += new EventHandler(ControlGotFocus);
                    ((ComboBox)ctrl).LostFocus += new EventHandler(ControlLostFocus);

                    ((ComboBox)ctrl).Font = GetFont();
                    ((ComboBox)ctrl).Height = 24;
                }
                else if (ctrl is ListBox)
                {
                    ((ListBox)ctrl).GotFocus += new EventHandler(ControlGotFocus);
                    ((ListBox)ctrl).LostFocus += new EventHandler(ControlLostFocus);

                    ((ListBox)ctrl).Font = GetFont();
                    ((ListBox)ctrl).ForeColor = Color.Maroon;
                    new ListSearch(((ListBox)ctrl));
                }
                else if (ctrl is JitControls.OMLabel)
                {
                    JitControls.OMLabel ctrl5 = new JitControls.OMLabel();
                    ctrl5 = (JitControls.OMLabel)ctrl;

                    if (ctrl.Name == "lblMsg")
                    {
                        ctrl5.GradientBottom = Color.FromArgb(255, 224, 192);
                        ctrl5.GradientTop = Color.FromArgb(255, 224, 192);
                        ctrl5.GradientMiddle = System.Drawing.Color.White;
                        ctrl5.BorderStyle = BorderStyle.FixedSingle;
                        ((JitControls.OMLabel)ctrl).Font = new Font("Verdana", 11, FontStyle.Bold);
                    }
                    else
                    {
                        ctrl5.GradientBottom = Color.FromArgb(226, 236, 239);// Color.FromArgb(213, 225, 230);
                        ctrl5.GradientTop = Color.FromArgb(63, 92, 103);// Color.FromArgb(78, 116, 133);
                        ctrl5.GradientMiddle = Color.FromArgb(226, 236, 239);// System.Drawing.Color.White;
                        ctrl5.GradientShow = true;
                        ctrl5.BorderStyle = BorderStyle.None;
                        ((JitControls.OMLabel)ctrl).Font = GetFont();
                    }
                }
                else if (ctrl is Label)
                {

                    if (ctrl.Name != "lblTitle" && ctrl.Name != "lblTotalAmt" && ctrl.Name != "lblMsg")
                        ((Label)ctrl).Font = GetFont();

                    if (ctrl.Name == "lblHead" || ctrl.Name == "lblFooter")
                        ctrl.Height = 20;
                    if (ctrl.Name.IndexOf("Star") > 0)
                    {
                        ctrl.ForeColor = Color.Red;
                        ctrl.Font = new Font("Arial", 9, FontStyle.Regular);
                    }
                    if (ctrl.Name.IndexOf("ChkHelp") > 0)
                    {
                        ctrl.ForeColor = Color.Gray;
                        ctrl.Font = new Font("Arial", 7, FontStyle.Regular);
                    }
                }
                else if (ctrl is JitControls.OMPanel)
                {
                    JitControls.OMPanel ctrl5 = new JitControls.OMPanel();
                    ctrl5 = (JitControls.OMPanel)ctrl;
                    if (ctrl5.Name == "pnlStatus")
                    {
                        ctrl5.GradientBottom = Color.FromArgb(213, 225, 230);
                        ctrl5.GradientTop = Color.FromArgb(78, 116, 133);
                        ctrl5.GradientMiddle = System.Drawing.Color.White;
                        ctrl5.GradientShow = true;
                        ctrl5.BorderStyle = BorderStyle.None;
                    }
                    else
                    {
                        ctrl5.GradientBottom = Color.FromArgb(78, 116, 133);
                        ctrl5.GradientTop = OM.ThemeColor.Panle_Back_Color;
                        //    ctrl5.GradientTop = OM.ThemeColor.Panle_Back_Color;
                        ctrl5.GradientMiddle = System.Drawing.Color.White;
                        ctrl5.GradientShow = true;
                        ctrl5.BorderStyle = BorderStyle.None;
                    }
                    FormatControl(ctrl.Controls);
                }
                else if (ctrl is System.Windows.Forms.Panel)
                {
                    FormatControl(ctrl.Controls);

                    if (ctrl.Name == "pnlMainForm")
                    {
                        ctrl.Location = new Point(((Form)(ctrl.Parent)).Width / 2 - ctrl.Width / 2, 5);
                        ctrl.Height = ((Form)(ctrl.Parent)).Height - 10;
                        //((Panel)ctrl).BorderStyle = BorderStyle.FixedSingle;
                    }
                    else if (ctrl.Name == "pnlMain")
                    {
                        ctrl.Location = new Point(((Form)(ctrl.Parent)).Width / 2 - ctrl.Width / 2, (((((Form)(ctrl.Parent)).Height / 2) - (ctrl.Height / 2)) / 2) / 2);
                        //((JitControls.OMGPanel)ctrl).LightingColor = Color.FromArgb(213, 225, 230);
                        //((Panel)ctrl).BorderStyle = BorderStyle.FixedSingle;
                        //((Panel)ctrl).BackColor = Color.FromArgb(185, 206, 215);
                    }

                    if (ctrl is JitControls.OMBPanel)
                    {
                        ((JitControls.OMBPanel)ctrl).BorderStyle = BorderStyle.None;
                        ((JitControls.OMBPanel)ctrl).BorderColor = Color.SteelBlue;
                        ((JitControls.OMBPanel)ctrl).BorderRadius = 2;
                    }
                }

                else if (ctrl is System.Windows.Forms.GroupBox)
                {
                    FormatControl(ctrl.Controls);
                }
                else if (ctrl is JitControls.OMTabControl)
                {
                    ctrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
                    ((JitControls.OMTabControl)(ctrl)).ScrollButtonStyle = JitControls.OMScrollButtonStyle.Never;
                    JitControls.VsTabDrawer VSTab = new JitControls.VsTabDrawer();
                    VSTab.GradientActiveTop = Color.FromArgb(78, 116, 133);
                    VSTab.GradientActiveBottom = Color.FromArgb(213, 225, 230);
                    VSTab.GradientActiveShow = true;
                    ((JitControls.OMTabControl)(ctrl)).TabDrawer = VSTab;
                    ((JitControls.OMTabControl)(ctrl)).ActiveColor = Color.FromArgb(182, 202, 211);
                    ((JitControls.OMTabControl)(ctrl)).ForeColor = Color.Black;
                    ((JitControls.OMTabControl)(ctrl)).InactiveColor = Color.FromArgb(213, 225, 230);
                    FormatControl(ctrl.Controls);
                    foreach (Control ctrltab in ((JitControls.OMTabControl)(ctrl)).Controls)
                    {
                        if (((JitControls.OMTabPage)ctrltab) is JitControls.OMTabPage)
                            ((JitControls.OMTabPage)ctrltab).BackColor = Color.FromArgb(213, 225, 230);
                    }
                }
                else if (ctrl is JitControls.OMTabPage)
                {
                    //((JitControls.OMTabPage)(ctrl)).BackgroundImage = global::Kirana.Properties.Resources.btnBG;
                    FormatControl(ctrl.Controls);
                }
                else if (ctrl is System.Windows.Forms.TabControl)
                {
                    ctrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
                    ((TabControl)(ctrl)).Appearance = TabAppearance.FlatButtons;
                    FormatControl(ctrl.Controls);
                    foreach (TabPage tbpg in ((TabControl)(ctrl)).TabPages)
                        tbpg.BackColor = Color.FromArgb(213, 225, 230);
                }
                else if (ctrl is System.Windows.Forms.TabPage)
                {
                    ((TabPage)(ctrl)).BackgroundImage = global::Kirana.Properties.Resources.btnBG;
                    ((TabPage)(ctrl)).BackgroundImageLayout = ImageLayout.Stretch;
                    FormatControl(ctrl.Controls);
                }
                else if (ctrl is System.Windows.Forms.PictureBox)
                {
                    if (ctrl.Name.IndexOf("pctbCalc") >= 0)
                    {
                        ((PictureBox)ctrl).MouseHover += new EventHandler(picturebox_MouseHover);
                        ((PictureBox)ctrl).DoubleClick += new EventHandler(picturebox_DoubleClick);
                    }
                }
            }
        }

        #region Calculator Methods
        public void picturebox_MouseHover(object sender, EventArgs e)
        {
            ToolTip tip = new ToolTip();
            tip.IsBalloon = true;
            tip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            tip.ToolTipTitle = "Calculator";
            tip.SetToolTip(((PictureBox)sender), "Ctrl + Alt +C");
        }
        public void DisplayCalc()
        {
            try
            {
                System.Diagnostics.ProcessStartInfo pr = new System.Diagnostics.ProcessStartInfo("Calc.exe");
                pr.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                System.Diagnostics.Process newProc1 = null;
                newProc1 = System.Diagnostics.Process.Start(pr);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, ErrorTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void picturebox_DoubleClick(object sender, EventArgs e)
        {
            DisplayCalc();
        }
        #endregion

        private void DatGridView_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
        }

        private void ControlGotFocus(object sender, EventArgs e)
        {

            if (((Control)sender) is TextBox)
            {
                ((TextBox)sender).SelectionStart = 0;
                ((TextBox)sender).SelectionLength = ((TextBox)sender).Text.Length;
                ((Control)sender).BackColor = Color.FromArgb(255, 224, 192);
            }
            else if (((Control)sender) is CheckBox)
            {
                //((Control)sender).BackColor = Color.FromArgb(255, 224, 192);

                Graphics g = ((Control)((Control)sender).Parent).CreateGraphics();
                Pen penBorder = new Pen(Color.FromArgb(255, 102, 102), 2);
                Rectangle rectBorder = new Rectangle(((Control)sender).Location.X - 1, ((Control)sender).Location.Y - 1, ((Control)sender).Width + 1, ((Control)sender).Height + 1);
                g.DrawRectangle(penBorder, rectBorder);
            }
            else
                ((Control)sender).BackColor = Color.FromArgb(255, 224, 192);
        }

        private void ControlLostFocus(object sender, EventArgs e)
        {
            if (((Control)sender) is CheckBox)
            {
                Graphics g = ((Control)((Control)sender).Parent).CreateGraphics();

                //    g.Clear(((Control)((Control)sender).Parent).BackColor);
                Pen penBorder = new Pen(((Control)((Control)sender).Parent).BackColor, 2);
                Rectangle rectBorder = new Rectangle(((Control)sender).Location.X - 1, ((Control)sender).Location.Y - 1, ((Control)sender).Width + 1, ((Control)sender).Height + 1);
                g.DrawRectangle(penBorder, rectBorder);

                //((Control)sender).BackColor = ((Control)((Control)sender).Parent).BackColor;
            }
            else
                ((Control)sender).BackColor = Color.White;
        }

        internal void GetVoucherLock()
        {
            try
            {
                string strNo = ObjQry.ReturnString("Select [Dfhr4pV0l8zivvu/b51VEg==] From [fk5PeDBBu3VfeawHXhrpjg==]", CommonFunctions.ConStr);
                if (strNo == "")
                    VoucherLock = 0;
                else
                    VoucherLock = Convert.ToInt64(secure.psDecrypt(strNo));
            }
            catch (Exception e)
            {
                VoucherLock = 0;
                CommonFunctions.ErrorMessge = e.Message;
            }
        }

        internal bool AllowVoucher()
        {
            long count = ObjQry.ReturnLong("Select count(*) From TVoucherEntry", CommonFunctions.ConStr);
            if (VoucherLock <= count)
                return false;
            else
                return true;
        }

        internal void setVoucherLock(string IsLock, Control.ControlCollection ctrls)
        {
            //Button ctrl1 = null;
            foreach (Control ctrl in ctrls)
            {
                if (ctrl is Button)
                {
                    if (((Button)ctrl).Name.ToUpper() == "BTNSAVE")
                    {
                        if (secure.psDecrypt(IsLock).ToString() != "0")
                            ((Button)ctrl).Visible = false;
                    }
                    else if (((Button)ctrl).Name.ToUpper() == "BTNDELETE")
                    {
                        if (secure.psDecrypt(IsLock).ToString() != "0")
                            ((Button)ctrl).Visible = false;
                    }
                }
                else if (ctrl is System.Windows.Forms.Panel)
                {
                    setVoucherLock(IsLock, ctrl.Controls);
                }
                else if (ctrl is System.Windows.Forms.GroupBox)
                {
                    setVoucherLock(IsLock, ctrl.Controls);
                }
                else if (ctrl is System.Windows.Forms.TabControl)
                {
                    setVoucherLock(IsLock, ctrl.Controls);
                }
                else if (ctrl is System.Windows.Forms.TabPage)
                {
                    setVoucherLock(IsLock, ctrl.Controls);
                }
            }
        }

        internal void ActiveNewForm()
        {
            foreach (Form frm in Application.OpenForms)
            {
                if (frm.Name == DBGetVal.NewCustForm.Name)
                {
                    frm.Activate();
                    break;
                }
            }
        }

        #region KeyDown Events
        public void KeyPressFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyPress += new KeyPressEventHandler(ControlKeyPress);
                ctrl.KeyDown += new KeyEventHandler(ControlKeyDown);
                if (ctrl is Panel)
                    KeyPressFormat(ctrl.Controls);
                else if (ctrl is GroupBox)
                    KeyPressFormat(ctrl.Controls);
                else if (ctrl is JitControls.OMBPanel)
                    KeyPressFormat(ctrl.Controls);
                else if (ctrl is JitControls.OMPanel)
                    KeyPressFormat(ctrl.Controls);
                else if (ctrl is JitControls.OMGPanel)
                    KeyPressFormat(ctrl.Controls);
                else if (ctrl is JitControls.OMTabControl)
                    KeyPressFormat(ctrl.Controls);
                else if (ctrl is JitControls.OMTabPage)
                    KeyPressFormat(ctrl.Controls);
            }
        }

        public void GetFocusControl(System.Windows.Forms.Control.ControlCollection ctrls, int index)
        {
            foreach (Control ctrl in ctrls)
            {
                if (ctrl.TabIndex == index)
                {
                    ctrlFocus = ctrl;
                    flagFocus = false;
                    break;
                }

                if (ctrl is Panel)
                    GetFocusControl(ctrl.Controls, index);
                else if (ctrl is GroupBox)
                    GetFocusControl(ctrl.Controls, index);
                if (flagFocus == false) break;
            }
        }

        private void ControlKeyPress(object sender, KeyPressEventArgs e)
        {
            Form frm = null;
            int index = ((Control)sender).TabIndex + 1;
            if (((Control)sender).Name == "txtTaxPercent") index = 7;
            if (((Control)sender).Name == "lstColor") index = 7;
            if (((Control)sender).Name == "dgPrevRate") index = 13;
            if (((Control)sender).Name == "lstUOM") index = 550;
            if (((Control)sender).Name == "lstItem") index = 550;
            flagFocus = true;
            if (((Control)sender).Parent is Form)
            {
                GetFocusControl(((Control)sender).Parent.Controls, index);
                frm = (Form)((Control)sender).Parent;
            }
            else if (((Control)sender).Parent.Parent is Form)
            {
                GetFocusControl(((Control)sender).Parent.Parent.Controls, index);
                frm = (Form)((Control)sender).Parent.Parent;
            }
            else if (((Control)sender).Parent.Parent.Parent is Form)
            {
                GetFocusControl(((Control)sender).Parent.Parent.Parent.Controls, index);
                frm = (Form)((Control)sender).Parent.Parent.Parent;
            }
            else if (((Control)sender).Parent.Parent.Parent.Parent is Form)
            {
                GetFocusControl(((Control)sender).Parent.Parent.Parent.Parent.Controls, index);
                frm = (Form)((Control)sender).Parent.Parent.Parent.Parent;
            }
            if (sender is ComboBox)
            {
                ComboBox cmb = ((ComboBox)sender);
                if (cmb.DroppedDown == false) cmb.DroppedDown = true;
                AutoComplete(ref cmb, e, true);
                if ((int)e.KeyChar == 13)
                    cmb.DroppedDown = false;

                if (cmb.DataSource != null) { if (GetComboValue(cmb) == 0) return; }
            }
            //GetFocusControl(this.Controls, index);


            if ((int)e.KeyChar == 13)
            {
                if (ctrlFocus != null)
                {
                    ctrlFocus.Focus();
                    if (ctrlFocus is ComboBox)
                    {
                        if (((ComboBox)ctrlFocus).SelectedIndex <= 0)
                            ((ComboBox)ctrlFocus).SelectedIndex = 0;
                        if (GetComboValue(((ComboBox)ctrlFocus)) == 0)
                            ((ComboBox)ctrlFocus).DroppedDown = true;

                    }
                }
            }
            //if ((Keys)e.KeyChar == Keys.Escape)
            //{
            //    if (frm.Name != "PurchaseAE")
            //        frm.Close();
            //}
        }

        private void ControlKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue >= 49 && e.KeyValue <= 57 && e.Control)
            {
                for (int i = 1; i <= 9; i++)
                {
                    int index = (int)Convert.ToChar(i.ToString());
                    if (e.KeyValue == index && e.Control)
                    {
                        if (mMain.toolBar1.Buttons.Count >= i)
                            mMain.toolBar1_ButtonClick(sender, new ToolBarButtonClickEventArgs(mMain.toolBar1.Buttons[i - 1]));
                    }
                }
            }
            else if (e.KeyCode == Keys.F1)
            {
                ShowHelp();
            }
            else if (e.KeyCode == Keys.X && e.Control)
            {
                Form frm = null;

                if (((Control)sender).Parent is Form)
                    frm = (Form)((Control)sender).Parent;
                else if (((Control)sender).Parent.Parent is Form)
                    frm = (Form)((Control)sender).Parent.Parent;
                else if (((Control)sender).Parent.Parent.Parent is Form)
                    frm = (Form)((Control)sender).Parent.Parent.Parent;
                else if (((Control)sender).Parent.Parent.Parent.Parent is Form)
                    frm = (Form)((Control)sender).Parent.Parent.Parent.Parent;
                frm.Close();
            }
        }
        #endregion

        #region Help Related Methods
        Panel pnl = new Panel();
        Label lbl = new Label();
        public void SetHelpInfo(Form frm)
        {
            string FormName = frm.Name, strHelp = ""; //bool flag = false;
            for (int i = 0; i < dtHelp.Rows.Count; i++)
            {
                if (FormName.ToUpper() == dtHelp.Rows[i].ItemArray[0].ToString().ToUpper())
                    strHelp = dtHelp.Rows[i].ItemArray[1].ToString().ToUpper();
            }
            if (strHelp == "") strHelp = "Help Controls not available";


            pnl = new Panel();
            lbl = new Label();
            lbl.AutoSize = false;
            lbl.Text = strHelp;// "Save : F2   Exit: Esc   First:Ctrl+Right Arrow  Next:Ctrl+Left Arrow  Prev:Ctrl+Down Arrow  Last:Ctrl+Up Arrow";
            lbl.Paint += new PaintEventHandler(lbl_Paint);
            lbl.ForeColor = Color.Red; lbl.BorderStyle = BorderStyle.None;
            lbl.Font = GetFont();
            lbl.TextAlign = ContentAlignment.MiddleCenter;
            lbl.Dock = DockStyle.Fill;
            pnl.Controls.Add(lbl);
            pnl.Dock = DockStyle.Bottom;
            pnl.Height = 30;
            pnl.BorderStyle = BorderStyle.None;
            pnl.Visible = false;
            pnl.BackColor = Color.FromArgb(255, 224, 192);
            frm.Controls.Add(pnl);
            pnl.BringToFront();

        }
        private void lbl_Paint(object sender, PaintEventArgs e)
        {
            Pen penBorder = new Pen(Color.MidnightBlue, 2);
            Rectangle rectBorder = new Rectangle(e.ClipRectangle.X, e.ClipRectangle.Y, e.ClipRectangle.Width - 1, e.ClipRectangle.Height - 1);
            e.Graphics.DrawRectangle(penBorder, rectBorder);
        }
        private void ShowHelp()
        {
            if (pnl.Visible == false)
            {
                pnl.Visible = true;
            }
            else
            {
                pnl.Visible = false;
            }
        }
        #endregion

        internal void SetAppSettings()
        {
            dtAppSettings = GetDataView("SELECT * FROM MSettings").Table;
        }

        internal string GetAppSettings(AppSettings app)
        {
            string expression = "PkSettingNo=" + (int)app;
            string strVal = "";
            try
            {
                if (dtAppSettings.Rows.Count > 0)
                {
                    DataRow[] result = dtAppSettings.Select(expression);
                    strVal = result[0].ItemArray[3].ToString();
                }
            }
            catch (Exception e)
            {
                strVal = "";
                CommonFunctions.ErrorMessge = e.Message;
            }
            return strVal;
        }

        internal string GetAppSettings(long No)
        {
            string expression = "PkSettingNo=" + No;
            string strVal = "";
            try
            {
                if (dtAppSettings.Rows.Count > 0)
                {
                    DataRow[] result = dtAppSettings.Select(expression);
                    strVal = result[0].ItemArray[3].ToString();
                }
            }
            catch (Exception e)
            {
                strVal = "";
                CommonFunctions.ErrorMessge = e.Message;
            }
            return strVal;
        }

        internal string GetAppSettingsLabel(AppSettings app)
        {
            string expression = "PkSettingNo=" + (int)app;
            string strVal = "";
            try
            {
                if (dtAppSettings.Rows.Count > 0)
                {
                    DataRow[] result = dtAppSettings.Select(expression);
                    strVal = result[0].ItemArray[1].ToString();
                }
            }
            catch (Exception e)
            {
                strVal = "";
                CommonFunctions.ErrorMessge = e.Message;
            }
            return strVal;
        }

        internal void SetReportPath()
        {
            if (Application.StartupPath.IndexOf("bin") > 0)
                ReportPath = Application.StartupPath.Replace("\\bin\\Debug", "");
            else
                ReportPath = Application.StartupPath;
            ReportPath += "\\Reports\\";
        }

        internal void GetTimeUserPasswords()
        {
            //bool flag = false;
            StreamReader objreader = null;
            string fname = Application.StartupPath + "\\Security.dat";
            try
            {
                if (File.Exists(fname) == true)
                {
                    objreader = new StreamReader(fname);
                    LUserID = secure.psDecrypt(objreader.ReadLine());
                    LPassword = secure.psDecrypt(objreader.ReadLine());
                    objreader.Close();
                    objreader = null;
                }
                else
                {
                    LUserID = "";
                    LPassword = "";
                }
            }
            catch
            {
                LUserID = "";
                LPassword = "";
            }
        }

        internal static DateTime GetTime()
        {
            JitControls.ServerDateTime sdt = new JitControls.ServerDateTime();
            //if (LUserID != "" && LPassword != "")
            //{

            Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();
            DateTime dtServer = ObjQry.ReturnDate("Select GetDate()", ConStr);
            //DateTime dtServer = sdt.GetTime(ServerName.Replace("\\SQLEXPRESS", ""), LUserID, LPassword);
            if (dtServer.ToString("dd-MMM-yyyy") == "01-Jan-1900")
                return DateTime.Now;
            else
                return dtServer;
            //}
            //else
            //    return DateTime.Now;
        }

        internal Font GetLangFont()
        {
            Font FT = GetFont();
            DataTable dtLang = GetDataView("Select FontName,FontSize,FontBold From MLanguage Where LanguageNo=" + GetAppSettings(AppSettings.O_Language) + "").Table;
            if (dtLang.Rows.Count > 0)
            {
                if (Convert.ToBoolean(dtLang.Rows[0].ItemArray[2].ToString()) == true)
                    FT = new Font(dtLang.Rows[0].ItemArray[0].ToString(), Convert.ToInt64(dtLang.Rows[0].ItemArray[1].ToString()), FontStyle.Bold);
                else
                    FT = new Font(dtLang.Rows[0].ItemArray[0].ToString(), Convert.ToInt64(dtLang.Rows[0].ItemArray[1].ToString()), FontStyle.Regular);
            }
            return FT;
        }

        internal string ChecklLangVal(string strVal)
        {
            string[] strSplit = { " " };
            string[] strEng = strVal.Split(strSplit, StringSplitOptions.RemoveEmptyEntries);
            string sql = "", strReturn = "";
            int LangVal = Convert.ToInt32(GetAppSettings(AppSettings.O_Language));
            while (true)
            {
                string strPending = "";
                string strLang = "";
                for (int i = 0; i < strEng.Length; i++)
                {

                    sql = "Select PkSrNo, EnglishVal, MarathiVal, HindiVal From MLanguageDictionary Where EnglishVal = '" + strEng[i].Replace("'", "''") + "'";
                    DataTable dt = GetDataView(sql).Table;
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0][LangVal].ToString() != "")
                            strLang += " " + dt.Rows[0][LangVal].ToString();
                        else
                            strPending = strEng[i];
                    }
                    else if (strPending.Length == 0)
                    {
                        strPending = strEng[i];
                    }
                }

                if (strPending.Length > 0)
                {
                    strReturn = "";
                    break;
                }
                else
                {
                    strReturn = strLang.Trim();
                    break;
                }
            }
            return strReturn;
        }

        internal void ExceptionDisplay(string strError)
        {
            try
            {
                if (GetAppSettings(AppSettings.O_IsExceptionDisplay) != "")
                {
                    if (Convert.ToBoolean(GetAppSettings(AppSettings.O_IsExceptionDisplay)) == true)
                    {
                        MessageBox.Show(strError, ErrorTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception e)
            {
                try
                {
                    if (GetAppSettings(AppSettings.O_IsExceptionDisplay) != "")
                    {
                        if (Convert.ToBoolean(GetAppSettings(AppSettings.O_IsExceptionDisplay)) == true)
                        {
                            MessageBox.Show(strError, ErrorTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            ErrorMessge = e.Message;
                        }
                    }
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message, ErrorTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        internal void SetVouchers(DataTable dtSetVouchers, long PkVoucherTrnNo)
        {
            for (int i = 0; i < dtSetVouchers.Rows.Count; i++)
            {
                if (dtSetVouchers.Rows[i].ItemArray[0].ToString() == PkVoucherTrnNo.ToString())
                {
                    dtSetVouchers.Rows[i][2] = "1";
                    dtSetVouchers.AcceptChanges();
                    return;
                }
            }
        }

        internal void FillDiscType(ComboBox cmb)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ID", Type.GetType("System.Int32")); dt.Columns.Add("Desc");
            DataRow dr = dt.NewRow();
            //dr[0] = "0";
            //dr[1] = " ------ Select ------ ";
            //dt.Rows.Add(dr);

            //dr = dt.NewRow();
            dr[0] = 1;
            dr[1] = "Percent";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr[0] = 2;
            dr[1] = "Rupees";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr[0] = 3;
            dr[1] = "Product";
            dt.Rows.Add(dr);

            cmb.DisplayMember = dt.Columns[1].ColumnName;
            cmb.ValueMember = dt.Columns[0].ColumnName;
            cmb.DataSource = dt;
            cmb.SelectedIndex = 0;
        }

        internal bool CheckAllowMenu(long value)
        {
            bool MenuFlag = false;
            GetToolStripItems(DBGetVal.MainForm.menuStrip.Items, value, out MenuFlag);
            if (MenuFlag == false)
                OMMessageBox.Show("This Function not allowed", ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
            return MenuFlag;
        }

        internal void GetToolStripItems(ToolStripItemCollection dropDownItems, long value, out bool flag)
        {
            flag = false;
            foreach (ToolStripItem item in dropDownItems)
            {
                if (item.Name == value.ToString())
                {
                    flag = true;
                    break;
                }
                if (item is ToolStripMenuItem)
                {
                    ToolStripMenuItem it = (ToolStripMenuItem)item;
                    if (it.HasDropDownItems)
                    {
                        if (flag == true) break;
                        flag = false;
                        GetToolStripItems(it.DropDownItems, value, out flag);
                    }
                }
            }
        }

        internal void CheckVersion()
        {
            try
            {
                string AppRegVer = ObjQry.ReturnString("Select AppVersion From MSetting", ConStr);
                if (secure.psDecrypt(AppRegVer) != new DBAssemblyInfo().AssemblyVersion)
                {
                    OMMessageBox.Show("Application Version is Incorrect." + Environment.NewLine + "Please contact HelpDesk.", ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    Application.Exit();
                }
            }
            catch
            (Exception exc)
            {
                OMMessageBox.Show("Application Version is Incorrect." + Environment.NewLine + "Please contact Help Desk.", ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                CommonFunctions.ErrorMessge = exc.Message;
                Application.Exit();
            }
        }

        internal void ExecuteScript()
        {
            try
            {
                string FileScript = Application.StartupPath + "\\RetailerScript.txt";
                string strData = "";
                if (File.Exists(FileScript) == true)
                {
                    bool isAllOk = true;
                    System.IO.StreamReader rd = new StreamReader(FileScript);
                    strData = rd.ReadToEnd();
                    rd.Close();

                    string[] strSplit = { "<BREAK>" };

                    string[] strScripts = strData.Split(strSplit, StringSplitOptions.None);

                    for (int i = 0; i < strScripts.Length; i++)
                    {
                        SqlCommand cmd = new SqlCommand();
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = strScripts[i];

                        //if (objT.Execute(strData, CommonFunctions.ConStr) == true)
                        if (objT.ExecuteNonQuery(cmd, CommonFunctions.ConStr) == false)
                        {
                            isAllOk = false;
                            break;
                        }
                    }

                    if (isAllOk)
                    {
                        File.Delete(FileScript);
                    }
                }
            }
            catch (Exception e)
            {
                strErrorMsg = e.Message;
            }
        }

        #region REST WS related Functions

        public class UCResponse
        {
            public string strResponseBody;
            public string strHeaderTokenNo;
        }

        public UCResponse getResponseJSON_MVC(string strFunctionName, string httpMethod, string strBody)
        {
            try
            {

                HttpWebResponse ClientResp = SendRequest_MVC(@"http://www.rshopnow.com/API" + strFunctionName, httpMethod, strBody);
                if (ClientResp.StatusCode == HttpStatusCode.OK)
                {
                    Encoding enc = System.Text.Encoding.GetEncoding("utf-8");
                    StreamReader responseStream = new StreamReader(ClientResp.GetResponseStream(), enc);
                    string result = string.Empty;
                    result = responseStream.ReadToEnd();
                    responseStream.Close();

                    UCResponse ucResponse = new UCResponse();
                    ucResponse.strResponseBody = result;
                    
                    return ucResponse;
                }
                else
                {
                    //WriteMessage((string.Format("Get Client properties request Failed. Status Code: {0}, Status Description: {1}", ClientResp.StatusCode, ClientResp.StatusDescription)), LogLevel.Error,null);
                    throw new Exception((string.Format("Get Client properties request Failed. Status Code: {0}, Status Description: {1}", ClientResp.StatusCode, ClientResp.StatusDescription)));

                }
            }
            catch (WebException ex)
            {
                throw new Exception(ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        public string getResponseJSON(string strFunctionName, string httpMethod, string strBody)
        {
            try
            {

                HttpWebResponse ClientResp = SendRequest(@"http://rs.rshopnow.com/ServerAccess.asmx" + strFunctionName, httpMethod, strBody);
                // HttpWebResponse ClientResp = SendRequest(@"http://localhost:49842/ServerAccess.asmx" + strFunctionName, httpMethod, strBody);

                if (ClientResp.StatusCode == HttpStatusCode.OK)
                {
                    Encoding enc = System.Text.Encoding.GetEncoding("utf-8");
                    StreamReader responseStream = new StreamReader(ClientResp.GetResponseStream(), Encoding.UTF8);
                    string result = string.Empty;
                    result = responseStream.ReadToEnd();
                    responseStream.Close();

                    System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                    doc.LoadXml(result);
                    string jsonText = JsonConvert.SerializeXmlNode(doc);

                    XDocument ResultXML = XDocument.Parse(result);

                    return ResultXML.Root.Value.ToString();
                }
                else
                {
                    //WriteMessage((string.Format("Get Client properties request Failed. Status Code: {0}, Status Description: {1}", ClientResp.StatusCode, ClientResp.StatusDescription)), LogLevel.Error,null);
                    throw new Exception((string.Format("Get Client properties request Failed. Status Code: {0}, Status Description: {1}", ClientResp.StatusCode, ClientResp.StatusDescription)));

                }
            }
            catch (WebException ex)
            {
                throw new Exception(ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        private HttpWebResponse SendRequest_MVC(string serviceURL, string httpMethod,
            string requestBody)
        {
            HttpWebResponse httpwr = null;
            try
            {
                WebRequest req = WebRequest.Create(serviceURL);

                if (serviceURL.StartsWith("https", StringComparison.CurrentCultureIgnoreCase))
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
                }

                req.Method = httpMethod;
                req.ContentType = @"application/json";
                // req.Headers.Add("POS-TYPE", "WPOS");
                //                req.Headers.Add("POS-VERSION", new DBAssemblyInfo().AssemblyVersion);


                if (!string.IsNullOrEmpty(requestBody))
                    WriteRequest(req, requestBody);
                
                httpwr = req.GetResponse() as HttpWebResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
            }
            return httpwr;
        }


        private HttpWebResponse SendRequest(string serviceURL, string httpMethod, string requestBody)
        {
            HttpWebResponse httpwr = null;
            try
            {

                WebRequest req = WebRequest.Create(serviceURL);

                if (serviceURL.StartsWith("https", StringComparison.CurrentCultureIgnoreCase))
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
                }

                req.Method = httpMethod;
                req.ContentType = "application/x-www-form-urlencoded";
                if (!string.IsNullOrEmpty(requestBody))
                    WriteRequest(req, requestBody);
                httpwr = req.GetResponse() as HttpWebResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
            }
            return httpwr;
        }

        private void WriteRequest(WebRequest req, string input)
        {
            req.ContentLength = Encoding.UTF8.GetByteCount(input);
            using (Stream stream = req.GetRequestStream())
            {
                stream.Write(Encoding.UTF8.GetBytes(input), 0, Encoding.UTF8.GetByteCount(input));
            }
        }

        #endregion

        public void SendMail(String Subject, string StrShortBody, string StrBodyData, int Type)
        {
            Attachment attach = null;
            try
            {

                //MailMessage message = new MailMessage();
                //message.From = new MailAddress("omSystem101@gmail.com");
                //message.To.Add(new MailAddress("omSystem101@gmail.com"));
                //message.Subject = Subject;
                //message.Body = StrBodyData;
                //SmtpClient client = new SmtpClient();
                ////if (StrBodyData != null)
                ////{
                ////    string myPath = "";
                ////    if (Type == 1)
                ////        myPath = System.Web.HttpContext.Current.Server.MapPath("~/TestFile/Error.txt");
                ////    else if (Type == 2)
                ////        myPath = System.Web.HttpContext.Current.Server.MapPath("~/TestFile/Info.txt");
                ////    else
                ////        myPath = System.Web.HttpContext.Current.Server.MapPath("~/TestFile/Test.txt");

                ////    File.WriteAllText(myPath, StrBodyData);
                ////    attach = new Attachment(myPath);
                ////    message.Attachments.Add(attach);
                ////}
                //client.Send(message);


                MailMessage Msg = new MailMessage();
                Msg.From = new MailAddress("omSystem101@gmail.com");
                Msg.To.Add("omSystem101@gmail.com");
                Msg.Subject = Subject;
                Msg.Body = StrBodyData;


                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.Credentials = new System.Net.NetworkCredential("omSystem101@gmail.com", "manali@112233");

                smtp.EnableSsl = true;
                smtp.Send(Msg);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                if (attach != null)
                    attach.Dispose();
            }
        }



    }
    class IsoDateTimeConverterWithMicroSeconds : Newtonsoft.Json.Converters.IsoDateTimeConverter
    {
        public IsoDateTimeConverterWithMicroSeconds()
        {
            DateTimeFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.fff'Z'";
        }
    }
    public class ThemeColor
    {

        private static Color Header_Color = Color.FromArgb(170, 0, 255);
        private static Color Back_Color = Color.FromArgb(212,255, 255);


        //private static Color Header_Color = Color.Coral;
        //private static Color Back_Color = Color.FromArgb(225, 221, 163);

       //private static Color Header_Color = Color.FromArgb(66, 70, 79); //Color.Coral ;// 
       //private static Color Back_Color = Color.FromArgb(206, 206, 214); //Color.FromArgb(225, 221, 163);

        private static Color List_Color = Color.Bisque; //Color.FromArgb(255, 224, 192);
        private static Color Select_Color = Color.LightSteelBlue;

        public static Color Button_BackGround = Header_Color;
        public static Color Button_ForeColor = Color.White;
        public static Color Button_BackGround_Hover = Color.FromArgb(79, 83, 95);
        public static Color Button_BackGround_Active = Color.FromArgb(97, 100, 111);
        public static Color Button_Border = Color.White;
        //public static Color Button_Border_Active = Color.FromArgb(255, 129, 0);
        public static Color Button_Border_Active = Color.Black;

        public static Color Body_Back_Color = Back_Color;//225, 221, 163
        public static Color Body_Header_Color = Header_Color;//Coral

        public static Color Panle_Back_Color = Header_Color;//Color.FromArgb(206, 206, 214);//255, 224, 192
        public static Color Panle_Lable_Fore_Color = Color.White; //Maroon

        public static Color List_Back_Color = List_Color;
        public static Color List_Select_Color = Select_Color;
        public static Color List_Fore_Color = Color.Maroon;

    }

    /// <summary>
    /// This static class is used for DBGet Val
    /// </summary>
    public class DBGetVal
    {
        private static MDIParent1 mMainForm;
        private static string mCompanyName;
        private static int mCompanyNo;
        private static int mUID;
        private static string mUserName;
        private static DateTime mFromDate;
        private static DateTime mToDate;
        private static string mDBName;
        private static Form mNewCustForm;
        private static string mDirectoryPath;
        private static bool mIsAdmin;
        private static string mCompanyAddress;
        private static string mMacID;
        private static string mHostName;
        private static System.Net.IPAddress mMachineIPAddress;
        private static long mMacNo;
        private static long mReportDisType = 2;
        private static DateTime mServerTime;
        private static string mGSTNo;

        /// <summary>
        /// MainForm is static member of DBGet Val
        /// </summary>
        public static MDIParent1 MainForm
        {
            get { return mMainForm; }
            set { mMainForm = value; }
        }
        /// <summary>
        /// CompanyName is static member of DBGet Val
        /// </summary>
        public static string CompanyName
        {
            get { return mCompanyName; }
            set { mCompanyName = value; }
        }
        /// <summary>
        /// UserID is static member of DBGet Val
        /// </summary>
        public static int UserID
        {
            get
            {
                return mUID;
            }
            set
            {
                mUID = value;
            }
        }
        /// <summary>
        /// UserName is static member of DBGet Val
        /// </summary>
        public static string UserName
        {
            get { return mUserName; }
            set { mUserName = value; }
        }
        /// <summary>
        /// CompanyNo is static member of DBGet Val
        /// </summary>
        public static int CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// FromDate is static member of DBGet Val
        /// </summary>
        public static DateTime FromDate
        {
            get { return mFromDate; }
            set { mFromDate = value; }
        }
        /// <summary>
        /// ToDate is static member of DBGet Val
        /// </summary>
        public static DateTime ToDate
        {
            get { return mToDate; }
            set { mToDate = value; }
        }
        /// <summary>
        /// DBName is static member of DBGet Val
        /// </summary>
        public static string DBName
        {
            get { return mDBName; }
            set { mDBName = value; }
        }
        /// <summary>
        /// RegCompName is static member of DBGet Val
        /// </summary>
        public static string RegCompName
        {
            get
            {
                DBAssemblyInfo db = new DBAssemblyInfo();
                return "Powered By " + db.AssemblyCompany;
            }
        }
        /// <summary>
        /// NewCustForm is static member of DBGet Val
        /// </summary>
        public static Form NewCustForm
        {
            get { return mNewCustForm; }
            set { mNewCustForm = value; }
        }
        /// <summary>
        /// DirectoryPath is static member of DBGet Val
        /// </summary>
        public static string DirectoryPath
        {
            get { return mDirectoryPath; }
            set { mDirectoryPath = value; }
        }
        /// <summary>
        /// IsAdmin is static member of DBGet Val
        /// </summary>
        public static bool IsAdmin
        {
            get { return mIsAdmin; }
            set { mIsAdmin = value; }
        }
        /// <summary>
        /// CompanyAddress is static member of DBGet Val
        /// </summary>
        public static string CompanyAddress
        {
            get { return mCompanyAddress; }
            set { mCompanyAddress = value; }
        }
        /// <summary>
        /// MacID is static member of DBGet Val
        /// </summary>
        public static string MacID
        {
            get { return mMacID; }
            set { mMacID = value; }
        }
        /// <summary>
        /// MachineIPAddress is static member of DBGet Val
        /// </summary>
        public static System.Net.IPAddress MachineIPAddress
        {
            get { return mMachineIPAddress; }
            set { mMachineIPAddress = value; }
        }
        /// <summary>
        /// HostName is static member of DBGet Val
        /// </summary>
        public static string HostName
        {
            get { return mHostName; }
            set { mHostName = value; }
        }
        /// <summary>
        /// MacNo is static member of DBGet Val
        /// </summary>
        public static long MacNo
        {
            get { return mMacNo; }
            set { mMacNo = value; }
        }
        /// <summary>
        /// ReportDisType is static member of DBGet Val
        /// </summary>
        public static long ReportDisType
        {
            get { return mReportDisType; }
        }
        /// <summary>
        /// ServerTime is static member of DBGet Val
        /// </summary>
        public static DateTime ServerTime
        {
            get { return mServerTime; }
            set { mServerTime = value; }
        }

        public static string GSTNo
        {
            get { return mGSTNo; }
            set { mGSTNo = value; }
        }
    }

    /// <summary>
    /// This static class is used for DB Cell Value
    /// </summary>
    public static class DBCellValue
    {
        private static string mRate;
        private static string mQuanity;
        private static string mDisc;

        /// <summary>
        /// Rate is static member of DB Cell Value
        /// </summary>
        public static string Rate
        {
            get { return mRate; }
            set { mRate = value; }
        }
        /// <summary>
        /// Quanity is static member of DB Cell Value
        /// </summary>
        public static string Quanity
        {
            get { return mQuanity; }
            set { mQuanity = value; }
        }
        /// <summary>
        /// Disc is static member of DB Cell Value
        /// </summary>
        public static string Disc
        {
            get { return mDisc; }
            set { mDisc = value; }
        }
    }


    /// <summary>
    /// This static class is used for Group Type
    /// </summary>
    public static class GroupType
    {
        /// <summary>
        /// CapitalAccount is static member of Group Type
        /// </summary>
        public static long CapitalAccount
        {
            get { return 1; }
        }
        /// <summary>
        /// LoansLiabilities is static member of Group Type
        /// </summary>
        public static long LoansLiabilities
        {
            get { return 2; }
        }
        /// <summary>
        /// CurrentLiabilities is static member of Group Type
        /// </summary>
        public static long CurrentLiabilities
        {
            get { return 3; }
        }
        /// <summary>
        /// FixedAssets is static member of Group Type
        /// </summary>
        public static long FixedAssets
        {
            get { return 4; }
        }
        /// <summary>
        /// Investments is static member of Group Type
        /// </summary>
        public static long Investments
        {
            get { return 5; }
        }
        /// <summary>
        /// CurrentAssets is static member of Group Type
        /// </summary>
        public static long CurrentAssets
        {
            get { return 6; }
        }
        /// <summary>
        /// BranchDivision is static member of Group Type
        /// </summary>
        public static long BranchDivision
        {
            get { return 7; }
        }
        /// <summary>
        /// MiscExpensesAssets is static member of Group Type
        /// </summary>
        public static long MiscExpensesAssets
        {
            get { return 8; }
        }
        /// <summary>
        /// SuspenseAccount is static member of Group Type
        /// </summary>
        public static long SuspenseAccount
        {
            get { return 9; }
        }
        /// <summary>
        /// SalesAccount is static member of Group Type
        /// </summary>
        public static long SalesAccount
        {
            get { return 10; }
        }
        /// <summary>
        /// PurchaseAccount is static member of Group Type
        /// </summary>
        public static long PurchaseAccount
        {
            get { return 11; }
        }
        /// <summary>
        /// DirectIncome is static member of Group Type
        /// </summary>
        public static long DirectIncome
        {
            get { return 12; }
        }
        /// <summary>
        /// DirectExpenses is static member of Group Type
        /// </summary>
        public static long DirectExpenses
        {
            get { return 13; }
        }
        /// <summary>
        /// IndirectIncome is static member of Group Type
        /// </summary>
        public static long IndirectIncome
        {
            get { return 14; }
        }
        /// <summary>
        /// InDirectExpenses is static member of Group Type
        /// </summary>
        public static long InDirectExpenses
        {
            get { return 15; }
        }
        /// <summary>
        /// ReserveAndSurplus is static member of Group Type
        /// </summary>
        public static long ReserveAndSurplus
        {
            get { return 16; }
        }
        /// <summary>
        /// BankODAccount is static member of Group Type
        /// </summary>
        public static long BankODAccount
        {
            get { return 17; }
        }
        /// <summary>
        /// SecuredLoans is static member of Group Type
        /// </summary>
        public static long SecuredLoans
        {
            get { return 18; }
        }
        /// <summary>
        /// UnSecuredLoans is static member of Group Type
        /// </summary>
        public static long UnSecuredLoans
        {
            get { return 19; }
        }
        /// <summary>
        /// DutiesAndTaxes is static member of Group Type
        /// </summary>
        public static long DutiesAndTaxes
        {
            get { return 20; }
        }
        /// <summary>
        /// Provisions is static member of Group Type
        /// </summary>
        public static long Provisions
        {
            get { return 21; }
        }
        /// <summary>
        /// SundryCreditors is static member of Group Type
        /// </summary>
        public static long SundryCreditors
        {
            get { return 22; }
        }
        /// <summary>
        /// StockInhand is static member of Group Type
        /// </summary>
        public static long StockInhand
        {
            get { return 23; }
        }
        /// <summary>
        /// DepositAssets is static member of Group Type
        /// </summary>
        public static long DepositAssets
        {
            get { return 24; }
        }
        /// <summary>
        /// LoansAndAdvanceAssets is static member of Group Type
        /// </summary>
        public static long LoansAndAdvanceAssets
        {
            get { return 25; }
        }
        /// <summary>
        /// SundryDebtors is static member of Group Type
        /// </summary>
        public static long SundryDebtors
        {
            get { return 26; }
        }
        /// <summary>
        /// CashInhand is static member of Group Type
        /// </summary>
        public static long CashInhand
        {
            get { return 27; }
        }
        /// <summary>
        /// BankAccounts is static member of Group Type
        /// </summary>
        public static long BankAccounts
        {
            get { return 28; }
        }
        /// <summary>
        /// Primary is static member of Group Type
        /// </summary>
        public static long Primary
        {
            get { return 29; }
        }
        /// <summary>
        /// Transporter is static member of Group Type
        /// </summary>
        public static long Transporter
        {
            get { return 30; }
        }

        public static long VAT
        {
            get { return 32; }
        }

        public static long GST
        {
            get { return 38; }
        }
    }

    /// <summary>
    /// This static class is used for VoucherType
    /// </summary>
    public static class VchType
    {
        /// <summary>
        /// Contra is static member of VoucherType
        /// </summary>
        public static long Contra
        {
            get { return 1; }
        }
        /// <summary>
        /// CreditNote is static member of VoucherType
        /// </summary>
        public static long CreditNote
        {
            get { return 2; }
        }
        /// <summary>
        /// DebitNote is static member of VoucherType
        /// </summary>
        public static long DebitNote
        {
            get { return 3; }
        }
        /// <summary>
        /// DeliveryNote is static member of VoucherType
        /// </summary>
        public static long DeliveryNote
        {
            get { return 4; }
        }
        /// <summary>
        /// Journal is static member of VoucherType
        /// </summary>
        public static long Journal
        {
            get { return 5; }
        }
        /// <summary>
        /// Memorandum is static member of VoucherType
        /// </summary>
        public static long Memorandum
        {
            get { return 6; }
        }
        /// <summary>
        /// Payment is static member of VoucherType
        /// </summary>
        public static long Payment
        {
            get { return 7; }
        }
        /// <summary>
        /// PhysicalStock is static member of VoucherType
        /// </summary>
        public static long PhysicalStock
        {
            get { return 8; }
        }
        /// <summary>
        /// Purchase is static member of VoucherType
        /// </summary>
        public static long Purchase
        {
            get { return 9; }
        }
        /// <summary>
        /// PurchaseOrder is static member of VoucherType
        /// </summary>
        public static long PurchaseOrder
        {
            get { return 10; }
        }
        /// <summary>
        /// Receipt is static member of VoucherType
        /// </summary>
        public static long Receipt
        {
            get { return 11; }
        }
        /// <summary>
        /// RejectionIn is static member of VoucherType
        /// </summary>
        public static long RejectionIn
        {
            get { return 12; }
        }
        /// <summary>
        /// Contra is static member of VoucherType
        /// </summary>
        public static long RejectionOut
        {
            get { return 13; }
        }
        /// <summary>
        /// ReversingJournal is static member of VoucherType
        /// </summary>
        public static long ReversingJournal
        {
            get { return 14; }
        }
        /// <summary>
        /// Sales is static member of VoucherType
        /// </summary>
        public static long Sales
        {
            get { return 15; }
        }
        /// <summary>
        /// SalesOrder is static member of VoucherType
        /// </summary>
        public static long SalesOrder
        {
            get { return 16; }
        }
        /// <summary>
        /// StockJournal is static member of VoucherType
        /// </summary>
        public static long StockJournal
        {
            get { return 17; }
        }
        /// <summary>
        /// GRNEntry is static member of VoucherType
        /// </summary>
        public static long GRNEntry
        {
            get { return 18; }
        }
        /// <summary>
        /// InternalTransfer is static member of VoucherType
        /// </summary>
        public static long InternalTransfer
        {
            get { return 19; }
        }
        /// <summary>
        /// DeliveryChallan is static member of VoucherType
        /// </summary>
        public static long DeliveryChallan
        {
            get { return 20; }
        }
        /// <summary>
        /// PurchaseVoucher is static member of VoucherType
        /// </summary>
        public static long PurchaseVoucher
        {
            get { return 21; }
        }
        /// <summary>
        /// SalesVoucher is static member of VoucherType
        /// </summary>
        public static long SalesVoucher
        {
            get { return 22; }
        }
        /// <summary>
        /// StockInward is static member of VoucherType
        /// </summary>
        public static long StockInward
        {
            get { return 23; }
        }
        /// <summary>
        /// StockOutward is static member of VoucherType
        /// </summary>
        public static long StockOutward
        {
            get { return 24; }
        }
        /// <summary>
        /// CashDepositeInBank is static member of VoucherType
        /// </summary>
        public static long CashDepositeInBank
        {
            get { return 25; }
        }
        /// <summary>
        /// CashWithdrawalFromBank is static member of VoucherType
        /// </summary>
        public static long CashWithdrawalFromBank
        {
            get { return 26; }
        }
        /// <summary>
        /// CashReceipt is static member of VoucherType
        /// </summary>
        public static long CashReceipt
        {
            get { return 27; }
        }
        /// <summary>
        /// BankReceipt is static member of VoucherType
        /// </summary>
        public static long BankReceipt
        {
            get { return 28; }
        }
        /// <summary>
        /// BankPayment is static member of VoucherType
        /// </summary>
        public static long BankPayment
        {
            get { return 29; }
        }
        /// <summary>
        /// SalesReceipt is static member of VoucherType
        /// </summary>
        public static long SalesReceipt
        {
            get { return 30; }
        }
        /// <summary>
        /// PurchasePayment is static member of VoucherType
        /// </summary>
        public static long PurchasePayment
        {
            get { return 31; }
        }
        /// <summary>
        /// StockTransfer is static member of VoucherType
        /// </summary>
        public static long StockTransfer
        {
            get { return 32; }
        }
        public static long ExpensesEntry
        {
            get { return 33; }
        }
        public static long CashPayment
        {
            get { return 34; }
        }

    }

    /// <summary>
    /// This static class is used for Others
    /// </summary>
    public static class Others
    {

        public static long Party = 501;
        public static long Discount1 = 502;
        public static long Discount2 = 503;
        public static long Discount3 = 504;
        public static long Discount4 = 505;
        public static long Charges1 = 506;
        public static long Charges2 = 507;
        public static long RoundOff = 508;
        public static long ItemDisc = 509;
        public static long BTaxItemDisc = 510;
        public static long BTaxDisc = 511;
        public static long Freight = 512;
    }

    /// <summary>
    /// This static class is used for Scheme Type
    /// </summary>
    public static class SchemeType
    {
        /// <summary>
        /// MTD is static member of Scheme Type
        /// </summary>
        public static long MTD = 1;
        /// <summary>
        /// TVB is static member of Scheme Type
        /// </summary>
        public static long TVB = 2;
        /// <summary>
        /// TSKU is static member of Scheme Type
        /// </summary>
        public static long TSKU = 3;
        /// <summary>
        /// TSKUC is static member of Scheme Type
        /// </summary>
        public static long TSKUC = 4;
        /// <summary>
        /// PSKU is static member of Scheme Type
        /// </summary>
        public static long PSKU = 5;
    }

    /// <summary>
    /// This static class is used for Stock Count Type
    /// </summary>
    public static class StockCountType
    {
        /// <summary>
        /// NA is static member of Stock Count Type
        /// </summary>
        public static long NA = 1;
        /// <summary>
        /// Daily is static member of Stock Count Type
        /// </summary>
        public static long Daily = 2;
        /// <summary>
        /// Weekly is static member of Stock Count Type
        /// </summary>
        public static long Weekly = 3;
        /// <summary>
        /// Monthly is static member of Stock Count Type
        /// </summary>
        public static long Monthly = 4;
        /// <summary>
        /// Yearly is static member of Stock Count Type
        /// </summary>
        public static long Yearly = 5;
    }

    /// <summary>
    /// This static class is used for Barcode Printer Type
    /// </summary>
    public static class BarcodePrinterType
    {
        /// <summary>
        /// TSC is static member of Barcode Printer Type
        /// </summary>
        public static long TSC = 1;
        /// <summary>
        /// Godex is static member of Barcode Printer Type
        /// </summary>
        public static long Godex = 2;
    }

    public enum UiMode
    {
        None = 0,
        View = 1,
        Add = 2,
        Edit = 3,
        Search = 4
    }


    /// <summary>
    /// This enumeration performs an AppSettings
    /// </summary>
    public enum AppSettings
    {
        /// <summary>
        /// This field use for S_PartyAC
        /// </summary>
        S_PartyAC = 1,
        /// <summary>
        /// This field use for S_TaxType
        /// </summary>
        S_TaxType = 2,
        /// <summary>
        /// This field use for S_Transporter
        /// </summary>
        S_Transporter = 3,
        /// <summary>
        /// This field use for S_Discount1
        /// </summary>
        S_Discount1 = 4,
        /// <summary>
        /// This field use for S_Discount2
        /// </summary>
        S_Discount2 = 5,
        /// <summary>
        /// This field use for S_Discount3
        /// </summary>
        S_Discount3 = 6,
        /// <summary>
        /// This field use for S_Charges1
        /// </summary>
        S_Charges1 = 8,
        /// <summary>
        /// This field use for S_RoundOfAcc
        /// </summary>
        S_RoundOfAcc = 12,
        /// <summary>
        /// This field use for P_PartyAC
        /// </summary>
        P_PartyAC = 13,
        /// <summary>
        /// This field use for P_TaxType
        /// </summary>
        P_TaxType = 14,
        /// <summary>
        /// This field use for P_Transporter
        /// </summary>
        P_Transporter = 15,
        /// <summary>
        /// This field use for P_Discount1
        /// </summary>
        P_Discount1 = 16,
        /// <summary>
        /// This field use for P_Charges1
        /// </summary>
        P_Charges1 = 20,
        /// <summary>
        /// This field use for P_Charges2
        /// </summary>
        P_Charges2 = 21,
        /// <summary>
        /// This field use for P_Charges2Display
        /// </summary>
        P_Charges2Display = 22,
        /// <summary>
        /// This field use for P_RoundOfAcc
        /// </summary>
        P_RoundOfAcc = 24,
        /// <summary>
        /// This field use for S_StopOnRate
        /// </summary>
        S_StopOnRate = 25,
        /// <summary>
        /// This field use for S_TaxType
        /// </summary>
        S_StopOnQty = 26,
        /// <summary>
        /// This field use for S_IsBarcodeEnabled
        /// </summary>
        S_IsBarcodeEnabled = 27,
        /// <summary>
        /// This field use for S_IsAllowsDuplicatesItemsInSameBill
        /// </summary>
        S_IsAllowsDuplicatesItemsInSameBill = 28,
        /// <summary>
        /// This field use for S_ItemNameType
        /// </summary>
        S_ItemNameType = 29,
        /// <summary>
        /// This field use for S_Rate
        /// </summary>
        S_Rate = 30,
        /// <summary>
        /// This field use for S_SubAmount
        /// </summary>
        S_SubAmount = 31,
        /// <summary>
        /// This field use for S_TaxAmount
        /// </summary>
        S_TaxAmount = 32,
        /// <summary>
        /// This field use for S_Qty
        /// </summary>
        S_Qty = 33,
        /// <summary>
        /// This field use for S_GrandTotal
        /// </summary>
        S_GrandTotal = 34,
        /// <summary>
        /// This field use for P_Rate
        /// </summary>
        P_Rate = 35,
        /// <summary>
        /// This field use for P_SubAmount
        /// </summary>
        P_SubAmount = 36,
        /// <summary>
        /// This field use for P_TaxAmount
        /// </summary>
        P_TaxAmount = 37,
        /// <summary>
        /// This field use for P_Qty
        /// </summary>
        P_Qty = 38,
        /// <summary>
        /// This field use for P_GrandTotal
        /// </summary>
        P_GrandTotal = 39,
        /// <summary>
        /// This field use for P_PurchaseAcc
        /// </summary>
        P_PurchaseAcc = 40,
        /// <summary>
        /// This field use for S_IsReverseRateCalc
        /// </summary>
        S_IsReverseRateCalc = 41,
        /// <summary>
        /// This field use for P_IsReverseRateCalc
        /// </summary>
        P_IsReverseRateCalc = 42,
        /// <summary>
        /// This field use for ARateLabel
        /// </summary>
        ARateLabel = 43,
        /// <summary>
        /// This field use for ARateIsActive
        /// </summary>
        ARateIsActive = 44,
        /// <summary>
        /// This field use for BRateLabel
        /// </summary>
        BRateLabel = 45,
        /// <summary>
        /// This field use for BRateIsActive
        /// </summary>
        BRateIsActive = 46,
        /// <summary>
        /// This field use for CRateLabel
        /// </summary>
        CRateLabel = 47,
        /// <summary>
        /// This field use for CRateIsActive
        /// </summary>
        CRateIsActive = 48,
        /// <summary>
        /// This field use for DRateLabel
        /// </summary>
        DRateLabel = 49,
        /// <summary>
        /// This field use for DRateIsActive
        /// </summary>
        DRateIsActive = 50,
        /// <summary>
        /// This field use for ERateLabel
        /// </summary>
        ERateLabel = 51,
        /// <summary>
        /// This field use for ERateIsActive
        /// </summary>
        ERateIsActive = 52,
        /// <summary>
        /// This field use for S_ItemDisc
        /// </summary>
        S_ItemDisc = 53,
        /// <summary>
        /// This field use for P_ATaxItemDisc
        /// </summary>
        P_ATaxItemDisc = 54,
        /// <summary>
        /// This field use for S_Discount1Display
        /// </summary>
        S_Discount1Display = 55,
        /// <summary>
        /// This field use for S_Charges1Display
        /// </summary>
        S_Charges1Display = 59,
        /// <summary>
        /// This field use for S_StopOnDate
        /// </summary>
        S_StopOnDate = 63,
        /// <summary>
        /// This field use for S_StopOnParty
        /// </summary>
        S_StopOnParty = 64,
        /// <summary>
        /// This field use for S_StopOnRateType
        /// </summary>
        S_StopOnRateType = 65,
        /// <summary>
        /// This field use for S_StopOnTaxType
        /// </summary>
        S_StopOnTaxType = 66,
        /// <summary>
        /// This field use for SB_SrNo
        /// </summary>
        SB_SrNo = 67,
        /// <summary>
        /// This field use for SB_ItemName
        /// </summary>
        SB_ItemName = 68,
        /// <summary>
        /// This field use for SB_Quantity
        /// </summary>
        SB_Quantity = 69,
        /// <summary>
        /// This field use for SB_UOM
        /// </summary>
        SB_UOM = 70,
        /// <summary>
        /// This field use for SB_Rate
        /// </summary>
        SB_Rate = 71,
        /// <summary>
        /// This field use for SB_NetRate
        /// </summary>
        SB_NetRate = 72,
        /// <summary>
        /// This field use for SB_DiscPercentage
        /// </summary>
        SB_DiscPercentage = 73,
        /// <summary>
        /// This field use for SB_DiscAmount
        /// </summary>
        SB_DiscAmount = 74,
        /// <summary>
        /// This field use for SB_DiscRupees
        /// </summary>
        SB_DiscRupees = 75,
        /// <summary>
        /// This field use for SB_DiscPercentage2
        /// </summary>
        SB_DiscPercentage2 = 76,
        /// <summary>
        /// This field use for SB_DiscAmount2
        /// </summary>
        SB_DiscAmount2 = 77,
        /// <summary>
        /// This field use for SB_DiscRupees2
        /// </summary>
        SB_DiscRupees2 = 78,
        /// <summary>
        /// This field use for SB_NetAmt
        /// </summary>
        SB_NetAmt = 79,
        /// <summary>
        /// This field use for SB_Amount
        /// </summary>
        SB_Amount = 80,
        /// <summary>
        /// This field use for SB_Barcode
        /// </summary>
        SB_Barcode = 81,
        /// <summary>
        /// This field use for SB_PkStockTrnNo
        /// </summary>
        SB_PkStockTrnNo = 82,
        /// <summary>
        /// This field use for SB_PkBarCodeNo
        /// </summary>
        SB_PkBarCodeNo = 83,
        /// <summary>
        /// This field use for SB_PkVoucherNo
        /// </summary>
        SB_PkVoucherNo = 84,
        /// <summary>
        /// This field use for SB_ItemNo
        /// </summary>
        SB_ItemNo = 85,
        /// <summary>
        /// This field use for SB_UOMNo
        /// </summary>
        SB_UOMNo = 86,
        /// <summary>
        /// This field use for SB_TaxLedgerNo
        /// </summary>
        SB_TaxLedgerNo = 87,
        /// <summary>
        /// This field use for SB_SalesLedgerNo
        /// </summary>
        SB_SalesLedgerNo = 88,
        /// <summary>
        /// This field use for SB_PkRateSettingNo
        /// </summary>
        SB_PkRateSettingNo = 89,
        /// <summary>
        /// This field use for SB_PkItemTaxInfo
        /// </summary>
        SB_PkItemTaxInfo = 90,
        /// <summary>
        /// This field use for SB_StockFactor
        /// </summary>
        SB_StockFactor = 91,
        /// <summary>
        /// This field use for SB_ActualQty
        /// </summary>
        SB_ActualQty = 92,
        /// <summary>
        /// This field use for SB_MKTQuantity
        /// </summary>
        SB_MKTQuantity = 93,
        /// <summary>
        /// This field use for SB_TaxPercentage
        /// </summary>
        SB_TaxPercentage = 94,
        /// <summary>
        /// This field use for S_StopOnGrid
        /// </summary>
        S_StopOnGrid = 95,
        /// <summary>
        /// This field use for S_IsShowSalesHistoryEnabled
        /// </summary>
        S_IsShowSalesHistoryEnabled = 96,
        /// <summary>
        /// This field use for S_IsShowPurchaseHistoryEnabled
        /// </summary>
        S_IsShowPurchaseHistoryEnabled = 97,
        /// <summary>
        /// This field use for S_IsUseLastSaleRateEnabled
        /// </summary>
        S_IsUseLastSaleRateEnabled = 98,
        /// <summary>
        /// This field use for S_IsStopOnSaleHistoryListEnabled
        /// </summary>
        S_IsStopOnSaleHistoryListEnabled = 99,
        /// <summary>
        /// This field use for S_Rate_DecimalDigits
        /// </summary>
        S_Rate_DecimalDigits = 100,
        /// <summary>
        /// This field use for S_Subtotal_DecimalDigits
        /// </summary>
        S_Subtotal_DecimalDigits = 101,
        /// <summary>
        /// This field use for S_Grandtotal_DecimalDigits
        /// </summary>
        S_Grandtotal_DecimalDigits = 102,
        /// <summary>
        /// This field use for S_TaxAmount_DecimalDigits
        /// </summary>
        S_TaxAmount_DecimalDigits = 103,
        /// <summary>
        /// This field use for S_TaxItemWise_DecimalDigits
        /// </summary>
        S_TaxItemWise_DecimalDigits = 104,
        /// <summary>
        /// This field use for S_DiscountAmount_DecimalDigits
        /// </summary>
        S_DiscountAmount_DecimalDigits = 105,
        /// <summary>
        /// This field use for S_DiscountItemWise_DecimalDigits
        /// </summary>
        S_DiscountItemWise_DecimalDigits = 106,
        /// <summary>
        /// This field use for S_Qty_DecimalDigits
        /// </summary>
        S_Qty_DecimalDigits = 107,
        /// <summary>
        /// This field use for S_Rate_RoundOffDigits
        /// </summary>
        S_Rate_RoundOffDigits = 108,
        /// <summary>
        /// This field use for S_Subtotal_RoundOffDigits
        /// </summary>
        S_Subtotal_RoundOffDigits = 109,
        /// <summary>
        /// This field use for S_Grandtotal_RoundOffDigits
        /// </summary>
        S_Grandtotal_RoundOffDigits = 110,
        /// <summary>
        /// This field use for S_TaxAmount_RoundOffDigits
        /// </summary>
        S_TaxAmount_RoundOffDigits = 111,
        /// <summary>
        /// This field use for S_TaxItemWise_RoundOffDigits
        /// </summary>
        S_TaxItemWise_RoundOffDigits = 112,
        /// <summary>
        /// This field use for S_DiscountAmount_RoundOffDigits
        /// </summary>
        S_DiscountAmount_RoundOffDigits = 113,
        /// <summary>
        /// This field use for S_DiscountItemWise_RoundOffDigits
        /// </summary>
        S_DiscountItemWise_RoundOffDigits = 114,
        /// <summary>
        /// This field use for S_Qty_RoundOffDigits
        /// </summary>
        S_Qty_RoundOffDigits = 115,
        /// <summary>
        /// This field use for S_Rate_RoundOffType
        /// </summary>
        S_Rate_RoundOffType = 116,
        /// <summary>
        /// This field use for S_Subtotal_RoundOffType
        /// </summary>
        S_Subtotal_RoundOffType = 117,
        /// <summary>
        /// This field use for S_Grandtotal_RoundOffType
        /// </summary>
        S_Grandtotal_RoundOffType = 118,
        /// <summary>
        /// This field use for S_TaxAmount_RoundOffType
        /// </summary>
        S_TaxAmount_RoundOffType = 119,
        /// <summary>
        /// This field use for S_TaxItemWise_RoundOffType
        /// </summary>
        S_TaxItemWise_RoundOffType = 120,
        /// <summary>
        /// This field use for S_DiscountAmount_RoundOffType
        /// </summary>
        S_DiscountAmount_RoundOffType = 121,
        /// <summary>
        /// This field use for S_DiscountItemWise_RoundOffType
        /// </summary>
        S_DiscountItemWise_RoundOffType = 122,
        /// <summary>
        /// This field use for S_Qty_RoundOffType
        /// </summary>
        S_Qty_RoundOffType = 123,
        /// <summary>
        /// This field use for P_Rate_DecimalDigits
        /// </summary>
        P_Rate_DecimalDigits = 124,
        /// <summary>
        /// This field use for P_Subtotal_DecimalDigits
        /// </summary>
        P_Subtotal_DecimalDigits = 125,
        /// <summary>
        /// This field use for P_Grandtotal_DecimalDigits
        /// </summary>
        P_Grandtotal_DecimalDigits = 126,
        /// <summary>
        /// This field use for P_TaxAmount_DecimalDigits
        /// </summary>
        P_TaxAmount_DecimalDigits = 127,
        /// <summary>
        /// This field use for P_TaxItemWise_DecimalDigits
        /// </summary>
        P_TaxItemWise_DecimalDigits = 128,
        /// <summary>
        /// This field use for P_DiscountAmount_DecimalDigits
        /// </summary>
        P_DiscountAmount_DecimalDigits = 129,
        /// <summary>
        /// This field use for P_DiscountItemWise_DecimalDigits
        /// </summary>
        P_DiscountItemWise_DecimalDigits = 130,
        /// <summary>
        /// This field use for P_Qty_DecimalDigits
        /// </summary>
        P_Qty_DecimalDigits = 131,
        /// <summary>
        /// This field use for P_Rate_RoundOffDigits
        /// </summary>
        P_Rate_RoundOffDigits = 132,
        /// <summary>
        /// This field use for P_Subtotal_RoundOffDigits
        /// </summary>
        P_Subtotal_RoundOffDigits = 133,
        /// <summary>
        /// This field use for P_Grandtotal_RoundOffDigits
        /// </summary>
        P_Grandtotal_RoundOffDigits = 134,
        /// <summary>
        /// This field use for P_TaxAmount_RoundOffDigits
        /// </summary>
        P_TaxAmount_RoundOffDigits = 135,
        /// <summary>
        /// This field use for P_TaxItemWise_RoundOffDigits
        /// </summary>
        P_TaxItemWise_RoundOffDigits = 136,
        /// <summary>
        /// This field use for P_DiscountAmount_RoundOffDigits
        /// </summary>
        P_DiscountAmount_RoundOffDigits = 137,
        /// <summary>
        /// This field use for P_DiscountItemWise_RoundOffDigits
        /// </summary>
        P_DiscountItemWise_RoundOffDigits = 138,
        /// <summary>
        /// This field use for P_Qty_RoundOffDigits
        /// </summary>
        P_Qty_RoundOffDigits = 139,
        /// <summary>
        /// This field use for P_Rate_RoundOffType
        /// </summary>
        P_Rate_RoundOffType = 140,
        /// <summary>
        /// This field use for P_Subtotal_RoundOffType
        /// </summary>
        P_Subtotal_RoundOffType = 141,
        /// <summary>
        /// This field use for P_Grandtotal_RoundOffType
        /// </summary>
        P_Grandtotal_RoundOffType = 142,
        /// <summary>
        /// This field use for P_TaxAmount_RoundOffType
        /// </summary>
        P_TaxAmount_RoundOffType = 143,
        /// <summary>
        /// This field use for P_TaxItemWise_RoundOffType
        /// </summary>
        P_TaxItemWise_RoundOffType = 144,
        /// <summary>
        /// This field use for P_DiscountAmount_RoundOffType
        /// </summary>
        P_DiscountAmount_RoundOffType = 145,
        /// <summary>
        /// This field use for P_DiscountItemWise_RoundOffType
        /// </summary>
        P_DiscountItemWise_RoundOffType = 146,
        /// <summary>
        /// This field use for P_Qty_RoundOffType
        /// </summary>
        P_Qty_RoundOffType = 147,
        /// <summary>
        /// This field use for S_RateType
        /// </summary>
        S_RateType = 148,
        /// <summary>
        /// This field use for S_ShowRateHistoryAutomatically
        /// </summary>
        S_ShowRateHistoryAutomatically = 149,
        /// <summary>
        /// This field use for S_HideRatePopupAutomatically
        /// </summary>
        S_HideRatePopupAutomatically = 150,
        /// <summary>
        /// This field use for S_HideRatePopupAutomatically_Seconds
        /// </summary>
        S_HideRatePopupAutomatically_Seconds = 151,
        /// <summary>
        /// This field use for S_OutwardLocation
        /// </summary>
        S_OutwardLocation = 152,
        /// <summary>
        /// This field use for O_DepartmentDisplay
        /// </summary>
        O_DepartmentDisplay = 153,
        /// <summary>
        /// This field use for O_CategoryDisplay
        /// </summary>
        O_CategoryDisplay = 154,
        /// <summary>
        /// This field use for O_BarCodeDisplay
        /// </summary>
        O_BarCodeDisplay = 155,
        /// <summary>
        /// This field use for O_StockLocation
        /// </summary>
        O_StockLocation = 156,
        /// <summary>
        /// This field use for SB_TaxAmount
        /// </summary>
        SB_TaxAmount = 157,
        /// <summary>
        /// This field use for SB_SalesVchNo
        /// </summary>
        SB_SalesVchNo = 158,
        /// <summary>
        /// This field use for SB_TaxVchNo
        /// </summary>
        SB_TaxVchNo = 159,
        /// <summary>
        /// This field use for SB_StockCompanyNo
        /// </summary>
        SB_StockCompanyNo = 160,
        /// <summary>
        /// This field use for S_IsAllowSingleFirmChq
        /// </summary>
        S_IsAllowSingleFirmChq = 161,
        /// <summary>
        /// This field use for S_IsAllowMultipleChq
        /// </summary>
        S_IsAllowMultipleChq = 162,
        /// <summary>
        /// This field use for S_IsDisplayRateType
        /// </summary>
        S_IsDisplayRateType = 163,
        /// <summary>
        /// This field use for S_RateTypeAskPassword
        /// </summary>
        S_RateTypeAskPassword = 164,
        /// <summary>
        /// This field use for ARatePassword
        /// </summary>
        ARatePassword = 165,
        /// <summary>
        /// This field use for ARateDBEffect
        /// </summary>
        ARateDBEffect = 166,
        /// <summary>
        /// This field use for BRatePassword
        /// </summary>
        BRatePassword = 167,
        /// <summary>
        /// This field use for BRateDBEffect
        /// </summary>
        BRateDBEffect = 168,
        /// <summary>
        /// This field use for CRatePassword
        /// </summary>
        CRatePassword = 169,
        /// <summary>
        /// This field use for CRateDBEffect
        /// </summary>
        CRateDBEffect = 170,
        /// <summary>
        /// This field use for DRatePassword
        /// </summary>
        DRatePassword = 171,
        /// <summary>
        /// This field use for DRateDBEffect
        /// </summary>
        DRateDBEffect = 172,
        /// <summary>
        /// This field use for ERatePassword
        /// </summary>
        ERatePassword = 173,
        /// <summary>
        /// This field use for ERateDBEffect
        /// </summary>
        ERateDBEffect = 174,
        /// <summary>
        /// This field use for PB_SrNo
        /// </summary>
        PB_SrNo = 175,
        /// <summary>
        /// This field use for PB_ItemName
        /// </summary>
        PB_ItemName = 176,
        /// <summary>
        /// This field use for PB_Quantity
        /// </summary>
        PB_Quantity = 177,
        /// <summary>
        /// This field use for PB_UOM
        /// </summary>
        PB_UOM = 178,
        /// <summary>
        /// This field use for PB_Rate
        /// </summary>
        PB_Rate = 179,
        /// <summary>
        /// This field use for PB_NetRate
        /// </summary>
        PB_NetRate = 180,
        /// <summary>
        /// This field use for PB_FreeQty
        /// </summary>
        PB_FreeQty = 181,
        /// <summary>
        /// This field use for PB_FreeUOMName
        /// </summary>
        PB_FreeUOMName = 182,
        /// <summary>
        /// This field use for PB_DiscPercentage
        /// </summary>
        PB_DiscPercentage = 183,
        /// <summary>
        /// This field use for PB_DiscAmount
        /// </summary>
        PB_DiscAmount = 184,
        /// <summary>
        /// This field use for PB_DiscRupees
        /// </summary>
        PB_DiscRupees = 185,
        /// <summary>
        /// This field use for PB_DiscPercentage2
        /// </summary>
        PB_DiscPercentage2 = 186,
        /// <summary>
        /// This field use for PB_DiscAmount2
        /// </summary>
        PB_DiscAmount2 = 187,
        /// <summary>
        /// This field use for PB_NetAmt
        /// </summary>
        PB_NetAmt = 188,
        /// <summary>
        /// This field use for PB_TaxPercentage
        /// </summary>
        PB_TaxPercentage = 189,
        /// <summary>
        /// This field use for PB_TaxAmount
        /// </summary>
        PB_TaxAmount = 190,
        /// <summary>
        /// This field use for PB_DiscRupees2_Charges
        /// </summary>
        PB_DiscRupees2_Charges = 191,
        /// <summary>
        /// This field use for PB_Amount
        /// </summary>
        PB_Amount = 192,
        /// <summary>
        /// This field use for PB_Barcode
        /// </summary>
        PB_Barcode = 193,
        /// <summary>
        /// This field use for PB_PkStockTrnNo
        /// </summary>
        PB_PkStockTrnNo = 194,
        /// <summary>
        /// This field use for PB_PkBarCodeNo
        /// </summary>
        PB_PkBarCodeNo = 195,
        /// <summary>
        /// This field use for PB_PkVoucherNo
        /// </summary>
        PB_PkVoucherNo = 196,
        /// <summary>
        /// This field use for PB_ItemNo
        /// </summary>
        PB_ItemNo = 197,
        /// <summary>
        /// This field use for PB_UOMNo
        /// </summary>
        PB_UOMNo = 198,
        /// <summary>
        /// This field use for PB_TaxLedgerNo
        /// </summary>
        PB_TaxLedgerNo = 199,
        /// <summary>
        /// This field use for PB_SalesLedgerNo
        /// </summary>
        PB_SalesLedgerNo = 200,
        /// <summary>
        /// This field use for PB_PkRateSettingNo
        /// </summary>
        PB_PkRateSettingNo = 201,
        /// <summary>
        /// This field use for PB_PkItemTaxInfo
        /// </summary>
        PB_PkItemTaxInfo = 202,
        /// <summary>
        /// This field use for PB_StockFactor
        /// </summary>
        PB_StockFactor = 203,
        /// <summary>
        /// This field use for PB_ActualQty
        /// </summary>
        PB_ActualQty = 204,
        /// <summary>
        /// This field use for PB_MKTQuantity
        /// </summary>
        PB_MKTQuantity = 205,
        /// <summary>
        /// This field use for PB_SalesVchNo
        /// </summary>
        PB_SalesVchNo = 206,
        /// <summary>
        /// This field use for PB_TaxVchNo
        /// </summary>
        PB_TaxVchNo = 207,
        /// <summary>
        /// This field use for PB_StockCompanyNo
        /// </summary>
        PB_StockCompanyNo = 208,
        /// <summary>
        /// This field use for ARateSuperMode
        /// </summary>
        ARateSuperMode = 209,
        /// <summary>
        /// This field use for BRateSuperMode
        /// </summary>
        BRateSuperMode = 210,
        /// <summary>
        /// This field use for CRateSuperMode
        /// </summary>
        CRateSuperMode = 211,
        /// <summary>
        /// This field use for DRateSuperMode
        /// </summary>
        DRateSuperMode = 212,
        /// <summary>
        /// This field use for ERateSuperMode
        /// </summary>
        ERateSuperMode = 213,
        /// <summary>
        /// This field use for P_StopOnRate
        /// </summary>
        P_StopOnRate = 214,
        /// <summary>
        /// This field use for P_StopOnQty
        /// </summary>
        P_StopOnQty = 215,
        /// <summary>
        /// This field use for P_IsBarCodeDisplay
        /// </summary>
        P_IsBarCodeDisplay = 216,
        /// <summary>
        /// This field use for P_AllowsDuplicatesItems
        /// </summary>
        P_AllowsDuplicatesItems = 217,
        /// <summary>
        /// This field use for P_ItemNameType
        /// </summary>
        P_ItemNameType = 218,
        /// <summary>
        /// This field use for P_Discount1Display
        /// </summary>
        P_Discount1Display = 219,
        /// <summary>
        /// This field use for P_Charges1Display
        /// </summary>
        P_Charges1Display = 220,
        /// <summary>
        /// This field use for P_StopOnDate
        /// </summary>
        P_StopOnDate = 221,
        /// <summary>
        /// This field use for P_StopOnParty
        /// </summary>
        P_StopOnParty = 222,
        /// <summary>
        /// This field use for P_StopOnHeaderDisc
        /// </summary>
        P_StopOnHeaderDisc = 223,
        /// <summary>
        /// This field use for P_StopOnTaxType
        /// </summary>
        P_StopOnTaxType = 224,
        /// <summary>
        /// This field use for P_StopOnGrid
        /// </summary>
        P_StopOnGrid = 225,
        /// <summary>
        /// This field use for P_IsShowSalesHistoryEnabled
        /// </summary>
        P_IsShowSalesHistoryEnabled = 226,
        /// <summary>
        /// This field use for P_IsShowPurchaseHistoryEnabled
        /// </summary>
        P_IsShowPurchaseHistoryEnabled = 227,
        /// <summary>
        /// This field use for P_IsUseLastSaleRateEnabled
        /// </summary>
        P_IsUseLastSaleRateEnabled = 228,
        /// <summary>
        /// This field use for P_IsStopOnSaleHistoryListEnabled
        /// </summary>
        P_IsStopOnSaleHistoryListEnabled = 229,
        /// <summary>
        /// This field use for P_RateType
        /// </summary>
        P_RateType = 230,
        /// <summary>
        /// This field use for P_ShowRateHistoryAutomatically
        /// </summary>
        P_ShowRateHistoryAutomatically = 231,
        /// <summary>
        /// This field use for P_HideRatePopupAutomatically
        /// </summary>
        P_HideRatePopupAutomatically = 232,
        /// <summary>
        /// This field use for P_HideRatePopupAutomatically_Seconds
        /// </summary>
        P_HideRatePopupAutomatically_Seconds = 233,
        /// <summary>
        /// This field use for P_OutwardLocation
        /// </summary>
        P_OutwardLocation = 234,
        /// <summary>
        /// This field use for P_IsAllowSingleFirmChq
        /// </summary>
        P_IsAllowSingleFirmChq = 235,
        /// <summary>
        /// This field use for P_IsAllowMultipleChq
        /// </summary>
        P_IsAllowMultipleChq = 236,
        /// <summary>
        /// This field use for P_IsDisplayRateType
        /// </summary>
        P_IsDisplayRateType = 237,
        /// <summary>
        /// This field use for P_RateTypeAskPassword
        /// </summary>
        P_RateTypeAskPassword = 238,
        /// <summary>
        /// This field use for P_IsBarcodeEnabled
        /// </summary>
        P_IsBarcodeEnabled = 239,
        /// <summary>
        /// This field use for P_IsAllowsDuplicatesItemsInSameBill
        /// </summary>
        P_IsAllowsDuplicatesItemsInSameBill = 240,
        /// <summary>
        /// This field use for TaxTypeGridDisplay
        /// </summary>
        TaxTypeGridDisplay = 241,
        /// <summary>
        /// This field use for S_IsBillPrint
        /// </summary>
        S_IsBillPrint = 242,
        /// <summary>
        /// This field use for ReportDisplay
        /// </summary>
        ReportDisplay = 243,
        /// <summary>
        /// This field use for PB_BarCodePrinting
        /// </summary>
        PB_BarCodePrinting = 244,
        /// <summary>
        /// This field use for S_AskPayableAmount
        /// </summary>
        S_AskPayableAmount = 245,
        /// <summary>
        /// This field use for P_BTaxItemDisc
        /// </summary>
        P_BTaxItemDisc = 246,
        /// <summary>
        /// This field use for Multiple_Firm_Accounting_Mode
        /// </summary>
        Multiple_Firm_Accounting_Mode = 247,
        /// <summary>
        /// This field use for Allow_Cheque_Pay_Type
        /// </summary>
        Allow_Cheque_Pay_Type = 248,
        /// <summary>
        /// This field use for O_PrintBarCode
        /// </summary>
        O_PrintBarCode = 249,
        /// <summary>
        /// This field use for P_AllBarCodePrint
        /// </summary>
        P_AllBarCodePrint = 250,
        /// <summary>
        /// This field use for IsExcelReport
        /// </summary>
        IsExcelReport = 251,
        /// <summary>
        /// This field use for S_IsManualBillNo
        /// </summary>
        S_IsManualBillNo = 252,
        /// <summary>
        /// This field use for S_ChargeLabelName
        /// </summary>
        S_ChargeLabelName = 253,
        /// <summary>
        /// This field use for S_CreditCardDigitLimit
        /// </summary>
        S_CreditCardDigitLimit = 254,
        /// <summary>
        /// This field use for O_TopSalesValue
        /// </summary>
        O_TopSalesValue = 255,
        /// <summary>
        /// This field use for S_SettingValue
        /// </summary>
        S_SettingValue = 256,
        /// <summary>
        /// This field use for S_FooterValue
        /// </summary>
        S_FooterValue = 257,
        /// <summary>
        /// This field use for S_OrderType
        /// </summary>
        S_OrderType = 258,
        /// <summary>
        /// This field use for O_IsBrandFilter
        /// </summary>
        O_IsBrandFilter = 259,
        /// <summary>
        /// This field use for O_ShowLastBill
        /// </summary>
        O_ShowLastBill = 260,
        /// <summary>
        /// This field use for PB_FreeUOMNo
        /// </summary>
        PB_FreeUOMNo = 261,
        /// <summary>
        /// This field use for S_Footer2Value
        /// </summary>
        S_Footer2Value = 262,
        /// <summary>
        /// This field use for AutoUploadData
        /// </summary>
        AutoUploadData = 263,
        /// <summary>
        /// This field use for AutoUploadHrs
        /// </summary>
        AutoUploadHrs = 264,
        /// <summary>
        /// This field use for AutoUploadMins
        /// </summary>
        AutoUploadMins = 265,
        /// <summary>
        /// This field use for O_Bilingual
        /// </summary>
        O_Bilingual = 266,
        /// <summary>
        /// This field use for O_Language
        /// </summary>
        O_Language = 267,
        /// <summary>
        /// This field use for O_DefaultBillPrint
        /// </summary>
        O_DefaultBillPrint = 268,
        /// <summary>
        /// This field use for PB_MRP
        /// </summary>
        PB_MRP = 269,
        /// <summary>
        /// This field use for O_SOD
        /// </summary>
        O_SOD = 270,
        /// <summary>
        /// This field use for O_EOD
        /// </summary>
        O_EOD = 271,
        /// <summary>
        /// This field use for S_ShowSavingBill
        /// </summary>
        S_ShowSavingBill = 272,
        /// <summary>
        /// This field use for S_ShowOutStanding
        /// </summary>
        S_ShowOutStanding = 273,
        /// <summary>
        /// This field use for O_UpDownLoadPath
        /// </summary>
        O_UpDownLoadPath = 274,
        /// <summary>
        /// This field use for O_UpDownTime
        /// </summary>
        O_UpDownTime = 275,
        /// <summary>
        /// This field use for AutoUploadType
        /// </summary>
        AutoUploadType = 276,
        /// <summary>
        /// This field use for O_BackUpPath
        /// </summary>
        O_BackUpPath = 277,
        /// <summary>
        /// This field use for ZipUploadHrs
        /// </summary>
        ZipUploadHrs = 278,
        /// <summary>
        /// This field use for ZipUploadMins
        /// </summary>
        ZipUploadMins = 279,
        /// <summary>
        /// This field use for O_IsExceptionDisplay
        /// </summary>
        O_IsExceptionDisplay = 280,
        /// <summary>
        /// This field use for S_IsFooterLevelDisc
        /// </summary>
        S_IsFooterLevelDisc = 281,
        /// <summary>
        /// This field use for S_IsItemLevelDisc
        /// </summary>
        S_IsItemLevelDisc = 282,
        /// <summary>
        /// This field use for S_IsBillWithMRP
        /// </summary>
        S_IsBillWithMRP = 283,
        /// <summary>
        /// This field use for S_IsAddressInBill
        /// </summary>
        S_IsAddressInBill = 284,
        /// <summary>
        /// This field use for S_IsBillRoundOff
        /// </summary>
        S_IsBillRoundOff = 285,
        /// <summary>
        /// This field use for P_IsBillRoundOff
        /// </summary>
        P_IsBillRoundOff = 286,
        /// <summary>
        /// This field use for S_IsAddressInBillHomeDelivery
        /// </summary>
        S_IsAddressInBillHomeDelivery = 287,
        /// <summary>
        /// This field use for S_IsAddressInBillCouterBill
        /// </summary>
        S_IsAddressInBillCouterBill = 288,
        /// <summary>
        /// This field use for P_AutoMFGMapping
        /// </summary>
        P_AutoMFGMapping = 289,
        /// <summary>
        /// This field use for O_BarCodePrintType
        /// </summary>
        O_BarCodePrintType = 290,
        /// <summary>
        /// This field use for O_BillWithMRP
        /// </summary>
        O_BillWithMRP = 291,
        /// <summary>
        /// This field use for O_UpDownLink
        /// </summary>
        O_UpDownLink = 292,
        /// <summary>
        /// This field use for O_SODUploadBackup
        /// </summary>
        O_SODUploadBackup = 293,
        /// <summary>
        /// This field use for S_ShowSchemeDetails
        /// </summary>
        S_ShowSchemeDetails = 294,
        /// <summary>
        /// This field use for S_DefaultPartyAC
        /// </summary>
        S_DefaultPartyAC = 295,
        /// <summary>
        /// This field use for P_DefaultPartyAC
        /// </summary>
        P_DefaultPartyAC = 296,
        /// <summary>
        /// This field use for O_ToolsSetupPath
        /// </summary>
        O_ToolsSetupPath = 297,
        O_MultiyRate = 298,
        O_LoginCount = 299,
        S_HamaliRs = 300,
        O_SystemLock = 301,
        S_DiscountItemMasterWise = 302,
        S_BillPrintWisePrinterSetting = 303,
        S_DefaultDiscountType = 304,
        S_GodownPrinting = 305,
        S_DefaultStockLocation = 306,
        S_StopOnDisc = 307,
        O_AddvanceSearch = 308,
        SB_MRP = 309,
        SB_SchemeDetailsNo = 310,
        SB_SchemeFromNo = 311,
        SB_SchemeToNo = 312,
        SB_RewardFromNo = 313,
        SB_RewardToNo = 314,
        SB_ItemLevelDiscNo = 315,
        SB_FKItemLevelDiscNo = 316,
        SB_downNo = 317,
        SB_DiscountType = 318,
        SB_HamaliInKg = 319,
        SB_HSNCode = 320,
        SB_IGSTPercent = 321,
        SB_IGSTAmount = 322,
        SB_CGSTPercent = 323,
        SB_CGSTAmount = 324,
        SB_SGSTPercent = 325,
        SB_SGSTAmount = 326,
        SB_UTGSTPercent = 327,
        SB_UTGSTAmount = 328,
        SB_CessPercent = 329,
        SB_CessAmount = 330,
        SB_IsQtyRead = 331,
        S_IsBillUpdate = 332,
        S_BillUpdatePwd = 333
    }

}