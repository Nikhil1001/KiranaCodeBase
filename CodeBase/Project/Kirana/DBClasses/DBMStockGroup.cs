﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using JitControls;

namespace OM
{
    class DBMStockGroup
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();

        public bool AddMStockGroup(MStockGroup mstockgroup)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMStockGroup";

            cmd.Parameters.AddWithValue("@StockGroupNo", mstockgroup.StockGroupNo);

            cmd.Parameters.AddWithValue("@LanguageName", mstockgroup.LanguageName);

            cmd.Parameters.AddWithValue("@StockGroupName", mstockgroup.StockGroupName);

            cmd.Parameters.AddWithValue("@ControlGroup", mstockgroup.ControlGroup);

            cmd.Parameters.AddWithValue("@ControlSubGroup", mstockgroup.ControlSubGroup);

            cmd.Parameters.AddWithValue("@IsActive", mstockgroup.IsActive);

            cmd.Parameters.AddWithValue("@UserId", mstockgroup.UserId);

            cmd.Parameters.AddWithValue("@UserDate", mstockgroup.UserDate);

            cmd.Parameters.AddWithValue("@CompanyNo", mstockgroup.CompanyNo);

            cmd.Parameters.AddWithValue("@MfgCompNo", mstockgroup.MfgCompNo);

            cmd.Parameters.AddWithValue("@Margin", mstockgroup.Margin);

            cmd.Parameters.AddWithValue("IsApplyToAll", mstockgroup.IsApplyToAll);


            if (ObjTrans.ExecuteNonQuery(cmd, CommonFunctions.ConStr) == true)
            {
                return true;
            }
            else
            {
                mstockgroup.msg = ObjTrans.ErrorMessage;
                return false;
            }
        }

        public bool AddMStockGroup1(MStockGroup mstockgroup)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMStockGroup1";

            cmd.Parameters.AddWithValue("@StockGroupNo", mstockgroup.StockGroupNo);

            cmd.Parameters.AddWithValue("@LanguageName", mstockgroup.LanguageName);

            cmd.Parameters.AddWithValue("@StockGroupName", mstockgroup.StockGroupName);

            cmd.Parameters.AddWithValue("@ControlGroup", mstockgroup.ControlGroup);

            cmd.Parameters.AddWithValue("@ControlSubGroup", mstockgroup.ControlSubGroup);

            cmd.Parameters.AddWithValue("@IsActive", mstockgroup.IsActive);

            cmd.Parameters.AddWithValue("@UserId", mstockgroup.UserId);

            cmd.Parameters.AddWithValue("@UserDate", mstockgroup.UserDate);

            cmd.Parameters.AddWithValue("@CompanyNo", mstockgroup.CompanyNo);

            cmd.Parameters.AddWithValue("@GlobalCode", mstockgroup.GlobalCode);

            cmd.Parameters.AddWithValue("@MfgCompNo", mstockgroup.MfgCompNo);

            cmd.Parameters.AddWithValue("@Margin", mstockgroup.Margin);

            cmd.Parameters.AddWithValue("@IsApplyToAll", mstockgroup.IsApplyToAll);

            if (ObjTrans.ExecuteNonQuery(cmd, CommonFunctions.ConStr) == true)
            {
                return true;
            }
            else
            {
                mstockgroup.msg = ObjTrans.ErrorMessage;
                return false;
            }
        }

        public bool DeleteMStockGroup(MStockGroup mstockgroup)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteMStockGroup";

            cmd.Parameters.AddWithValue("@StockGroupNo", mstockgroup.StockGroupNo);
            if (ObjTrans.ExecuteNonQuery(cmd, CommonFunctions.ConStr) == true)
            {
                return true;
            }
            else
            {
                mstockgroup.msg = ObjTrans.ErrorMessage;
                return false;
            }
        }
        
        public DataView GetAllMStockGroup()
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from MStockGroup order by StockGroupNo";
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }
        
        public DataView GetMStockGroupByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from MStockGroup where StockGroupNo =" + ID;
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public MStockGroup ModifyMStockGroupByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql;
            SqlCommand cmd;
            sql = "Select * from MStockGroup where StockGroupNo =" + ID;
            cmd = new SqlCommand(sql, Con);
            cmd.Connection = Con;
            Con.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (dr.HasRows)
            {
                MStockGroup MM = new MStockGroup();
                while (dr.Read())
                {
                    MM.StockGroupNo = Convert.ToInt32(dr["StockGroupNo"]);
                    if (!Convert.IsDBNull(dr["StockGroupName"])) MM.StockGroupName = Convert.ToString(dr["StockGroupName"]);
                    if (!Convert.IsDBNull(dr["LanguageName"])) MM.LanguageName = Convert.ToString(dr["LanguageName"]);
                    if (!Convert.IsDBNull(dr["ControlGroup"])) MM.ControlGroup = Convert.ToInt64(dr["ControlGroup"]);
                    if (!Convert.IsDBNull(dr["ControlSubGroup"])) MM.ControlSubGroup = Convert.ToInt64(dr["ControlSubGroup"]);
                    if (!Convert.IsDBNull(dr["IsActive"])) MM.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    if (!Convert.IsDBNull(dr["UserId"])) MM.UserId = Convert.ToInt64(dr["UserId"]);
                    if (!Convert.IsDBNull(dr["UserDate"])) MM.UserDate = Convert.ToDateTime(dr["UserDate"]);
                    if (!Convert.IsDBNull(dr["ModifiedBy"])) MM.ModifiedBy = Convert.ToString(dr["ModifiedBy"]);
                    if (!Convert.IsDBNull(dr["CompanyNo"])) MM.CompanyNo = Convert.ToInt64(dr["CompanyNo"]);
                    if (!Convert.IsDBNull(dr["MfgCompNo"])) MM.MfgCompNo = Convert.ToInt64(dr["MfgCompNo"]);
                    if (!Convert.IsDBNull(dr["Margin"])) MM.Margin = Convert.ToDouble(dr["Margin"]);
                    if (!Convert.IsDBNull(dr["IsApplyToAll"])) MM.IsApplyToAll = Convert.ToBoolean(dr["IsApplyToAll"]);
                }
                dr.Close();
                return MM;
            }
            else
                dr.Close();
            return new MStockGroup();
        }

        public DataView GetBySearch(string Column, string Value,int Grno)
        {

            string sql = null;
            switch (Column)
            {
                case "0":
                    sql = "Select StockGroupNo,StockGroupName AS 'StockGroup Name' ,Case when (IsActive = 'True') Then 'True' Else 'False' End As 'Status' from MStockGroup Where StockGroupNo<>1 and ControlGroup=" + Grno + " order by StockGroupName";
                    break;
                case "StockGroupName":
                    sql = "SELECT     StockGroupNo, StockGroupName AS 'StockGroup Name' ,Case when (IsActive = 'True') Then 'True' Else 'False' End As 'Status' FROM         MStockGroup WHERE   (StockGroupNo <> 1) AND (StockGroupName LIKE '" + Value.Trim().Replace("'","''") + "' + '%') and ControlGroup=" + Grno + " ORDER BY 'StockGroup Name'";
                            
                    break;

            }
            DataSet ds = new DataSet();
            try
            {
                ds = ObjDset.FillDset("New", sql, CommonFunctions.ConStr);
            }
            catch (SqlException e)
            {
                CommonFunctions.ErrorMessge = e.Message;
            }
            return ds.Tables[0].DefaultView;
        }
    }

    /// <summary>
    /// This Class use for MStockGroup
    /// </summary>
    public class MStockGroup
    {
        private long mStockGroupNo;
        private string mStockGroupName;
        private string mLanguageName;
        private long mControlGroup;
        private long mControlSubGroup;
        private bool mIsActive;
        private long mUserId;
        private DateTime mUserDate;
        private string mModifiedBy;
        private long mCompanyNo;
        private int mStatusNo;
        private long mGlobalCode;
        private long mMfgCompNo;
        private double mMargin;
        private bool mIsApplyToAll;
        private string Mmsg;

        /// <summary>
        /// This Properties use for StockGroupNo
        /// </summary>
        public long StockGroupNo
        {
            get { return mStockGroupNo; }
            set { mStockGroupNo = value; }
        }
        /// <summary>
        /// This Properties use for StockGroupName
        /// </summary>
        public string StockGroupName
        {
            get { return mStockGroupName; }
            set { mStockGroupName = value; }
        }
        /// <summary>
        /// This Properties use for LanguageName
        /// </summary>
        public string LanguageName
        {
            get { return mLanguageName; }
            set { mLanguageName = value; }
        }
        /// <summary>
        /// This Properties use for ControlGroup
        /// </summary>
        public long ControlGroup
        {
            get { return mControlGroup; }
            set { mControlGroup = value; }
        }
        /// <summary>
        /// This Properties use for ControlSubGroup
        /// </summary>
        public long ControlSubGroup
        {
            get { return mControlSubGroup; }
            set { mControlSubGroup = value; }
        }
        /// <summary>
        /// This Properties use for IsActive
        /// </summary>
        public bool IsActive
        {
            get { return mIsActive; }
            set { mIsActive = value; }
        }
        /// <summary>
        /// This Properties use for UserId
        /// </summary>
        public long UserId
        {
            get { return mUserId; }
            set { mUserId = value; }
        }
        /// <summary>
        /// This Properties use for UserDate
        /// </summary>
        public DateTime UserDate
        {
            get { return mUserDate; }
            set { mUserDate = value; }
        }
        /// <summary>
        /// This Properties use for ModifiedBy
        /// </summary>
        public string ModifiedBy
        {
            get { return mModifiedBy; }
            set { mModifiedBy = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public int StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for GlobalCode
        /// </summary>
        public long GlobalCode
        {
            get { return mGlobalCode; }
            set { mGlobalCode = value; }
        }
        /// <summary>
        /// This Properties use for MfgCompNo
        /// </summary>
        public long MfgCompNo
        {
            get { return mMfgCompNo; }
            set { mMfgCompNo = value; }
        }
        /// <summary>
        /// This Properties use for Margin
        /// </summary>
        public double Margin
        {
            get { return mMargin; }
            set { mMargin = value; }
        }
        /// <summary>
        /// This Properties use for IsApplyToAll
        /// </summary>
        public bool IsApplyToAll
        {
            get { return mIsApplyToAll; }
            set { mIsApplyToAll = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }
}
