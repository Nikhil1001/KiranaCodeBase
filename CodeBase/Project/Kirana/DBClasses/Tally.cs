﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace OM
{
    [XmlRoot(ElementName = "GSTDETAILS.LIST")]
    public class GSTDETAILS
    {
        [XmlElement(ElementName = "APPLICABLEFROM")]
        public string APPLICABLEFROM = "20170701";
        [XmlElement(ElementName = "HSNCODE")]
        public string HSNCODE = "";
        [XmlElement(ElementName = "CALCULATIONTYPE")]
        public string CALCULATIONTYPE = "";
        [XmlElement(ElementName = "STATEWISEDETAILS.LIST")]
        public STATEWISEDETAILS STATEWISEDETAILS { get; set; }
        [XmlElement(ElementName = "TAXABILITY")]
        public string TAXABILITY = "Taxable";
    }

    [XmlRoot(ElementName = "LANGUAGENAME.LIST")]
    public class LANGUAGENAME
    {
        [XmlElement(ElementName = "NAME")]
        public string NAME = "";
    }

    [XmlRoot(ElementName = "LEDGER")]
    public class LEDGER
    {
        [XmlElement(ElementName = "ADDRESS.LIST")]
        public List<ADDRESS> ADDRESS = new List<ADDRESS>();
        [XmlElement(ElementName = "BANKACCHOLDERNAME")]
        public string BANKACCHOLDERNAME = "";
        [XmlElement(ElementName = "BANKBRANCHNAME")]
        public string BANKBRANCHNAME = "";
        [XmlElement(ElementName = "BANKDETAILS")]
        public string BANKDETAILS = "";
        [XmlElement(ElementName = "BANKINGCONFIGBANK")]
        public string BANKINGCONFIGBANK = "";
        [XmlElement(ElementName = "COUNTRYNAME")]
        public string COUNTRYNAME = "";
        [XmlElement(ElementName = "COUNTRYOFRESIDENCE")]
        public string COUNTRYOFRESIDENCE = "";
        [XmlElement(ElementName = "GSTAPPLICABLE")]
        public string GSTAPPLICABLE = "";
        [XmlElement(ElementName = "GSTDETAILS.LIST")]
        public GSTDETAILS GSTDETAILS { get; set; }
        [XmlElement(ElementName = "GSTDUTYHEAD")]
        public string GSTDUTYHEAD = "";
        [XmlElement(ElementName = "GSTTYPEOFSUPPLY")]
        public string GSTTYPEOFSUPPLY = "";
        [XmlElement(ElementName = "IFSCODE")]
        public string IFSCODE = "";
        [XmlElement(ElementName = "LANGUAGENAME.LIST")]
        public LANGUAGENAME LANGUAGENAME { get; set; }
        [XmlElement(ElementName = "LEDSTATENAME")]
        public string LEDSTATENAME = "";
        [XmlElement(ElementName = "MAILINGNAME")]
        public string MAILINGNAME = "";
        [XmlAttribute(AttributeName = "NAME")]
        public string NAME = "";
        [XmlElement(ElementName = "PARENT")]
        public string PARENT = "";
        [XmlElement(ElementName = "PARTYGSTIN")]
        public string PARTYGSTIN = "";
        [XmlAttribute(AttributeName = "RESERVEDNAME")]
        public string RESERVEDNAME = "";
        [XmlElement(ElementName = "ROUNDINGMETHOD")]
        public string ROUNDINGMETHOD = "";
        [XmlElement(ElementName = "STARTINGFROM")]
        public string STARTINGFROM = "";
        [XmlElement(ElementName = "TAXCLASSIFICATIONNAME")]
        public string TAXCLASSIFICATIONNAME { get; set; }
        [XmlElement(ElementName = "TAXTYPE")]
        public string TAXTYPE = "";
        [XmlElement(ElementName = "VATAPPLICABLE")]
        public string VATAPPLICABLE = "";
        [XmlElement(ElementName = "ISBILLWISEON")]
        public string ISBILLWISEON = "YES";
        [XmlElement(ElementName = "LEDGERPHONE")]
        public string LEDGERPHONE = "";
    }

    [XmlRoot(ElementName = "ADDRESS.LIST")]
    public class ADDRESS
    {
        [XmlElement(ElementName = "ADDRESS")]
        public string address = "";

    }

    [XmlRoot(ElementName = "UNIT")]
    public class UNIT
    {
        [XmlElement(ElementName = "NAME")]
        public string NAME = "";
        [XmlAttribute(AttributeName = "NAME")]
        public string _NAME = "";
        [XmlElement(ElementName = "ISSIMPLEUNIT")]
        public string ISSIMPLEUNIT = "Yes";
        [XmlAttribute(AttributeName = "RESERVEDNAME")]
        public string RESERVEDNAME = "Yes";
    }

    [XmlRoot(ElementName = "SALESLIST.LIST")]
    public class SALESLIST
    {
        [XmlElement(ElementName = "NAME")]
        public string NAME = "";
        [XmlElement(ElementName = "CLASSRATE")]
        public double CLASSRATE { get; set; }
    }

    [XmlRoot(ElementName = "PURCHASELIST.LIST")]
    public class PURCHASELIST
    {
        [XmlElement(ElementName = "NAME")]
        public string NAME = "";
        [XmlElement(ElementName = "CLASSRATE")]
        public double CLASSRATE { get; set; }
    }

    [XmlRoot(ElementName = "STOCKGROUP")]
    public class STOCKGROUP
    {
        [XmlElement(ElementName = "LANGUAGENAME.LIST")]
        public LANGUAGENAME LANGUAGENAME { get; set; }
        [XmlAttribute(AttributeName = "NAME")]
        public string NAME = "";
        [XmlAttribute(AttributeName = "RESERVEDNAME")]
        public string RESERVEDNAME { get; set; }
    }

    [XmlRoot(ElementName = "STOCKITEM")]
    public class STOCKITEM
    {
        [XmlElement(ElementName = "PARENT")]
        public string PARENT = "";
        [XmlElement(ElementName = "GSTAPPLICABLE")]
        public string GSTAPPLICABLE = "Applicable";
        [XmlElement(ElementName = "GSTTYPEOFSUPPLY")]
        public string GSTTYPEOFSUPPLY = "Goods";
        [XmlElement(ElementName = "EXCISEAPPLICABILITY")]
        public string EXCISEAPPLICABILITY = "Applicable";
        [XmlElement(ElementName = "VATAPPLICABLE")]
        public string VATAPPLICABLE = "Applicable";
        [XmlElement(ElementName = "BASEUNITS")]
        public string BASEUNITS = "";
        [XmlElement(ElementName = "GSTDETAILS.LIST")]
        public GSTDETAILS GSTDETAILS { get; set; }
        [XmlElement(ElementName = "LANGUAGENAME")]
        public LANGUAGENAME LANGUAGENAME { get; set; }
        [XmlElement(ElementName = "SALESLIST.LIST")]
        public SALESLIST SALESLIST { get; set; }
        [XmlElement(ElementName = "PURCHASELIST.LIST")]
        public PURCHASELIST PURCHASELIST { get; set; }
        [XmlElement(ElementName = "ALTERID")]
        public string ALTERID = "";

        [XmlAttribute(AttributeName = "NAME")]
        public string NAME = "";
        [XmlAttribute(AttributeName = "RESERVEDNAME")]
        public string RESERVEDNAME = "";
    }


    [XmlRoot(ElementName = "RATEDETAILS.LIST")]
    public class RATEDETAILS
    {
        [XmlElement(ElementName = "GSTRATE")]
        public string GSTRATE = "";
        [XmlElement(ElementName = "GSTRATEDUTYHEAD")]
        public string GSTRATEDUTYHEAD = "";
        [XmlElement(ElementName = "GSTRATEVALUATIONTYPE")]
        public string GSTRATEVALUATIONTYPE = "Based on Value";
    }

    [XmlRoot(ElementName = "STATEWISEDETAILS.LIST")]
    public class STATEWISEDETAILS
    {
        [XmlElement(ElementName = "RATEDETAILS.LIST")]
        public List<RATEDETAILS> RATEDETAILS = new List<RATEDETAILS>();
        [XmlElement(ElementName = "STATENAME")]
        public string STATENAME = "Any";
    }


    [XmlRoot(ElementName = "VOUCHER")]
    public class VOUCHER
    {
        [XmlElement(ElementName = "ADDRESS")]
        public List<ADDRESS> ADDRESS = new List<ADDRESS>();
        [XmlElement(ElementName = "VOUCHERNUMBER")]
        public string VOUCHERNUMBER = "";
        [XmlElement(ElementName = "REFERENCE")]
        public string REFERENCE = "";
        [XmlElement(ElementName = "BASICBUYERADDRESS")]
        public string BASICBUYERADDRESS = "";
        [XmlElement(ElementName = "DATE")]
        public string DATE = "";
        [XmlElement(ElementName = "STATENAME")]
        public string STATENAME = "";
        [XmlElement(ElementName = "NARRATION")]
        public string NARRATION = "";
        [XmlElement(ElementName = "PARTYGSTIN")]
        public string PARTYGSTIN = "";
        [XmlElement(ElementName = "PARTYNAME")]
        public string PARTYNAME = "";
        [XmlElement(ElementName = "VOUCHERTYPENAME")]
        public string VOUCHERTYPENAME = "";
        [XmlElement(ElementName = "PARTYLEDGERNAME")]
        public string PARTYLEDGERNAME = "";
        [XmlElement(ElementName = "BASICBASEPARTYNAME")]
        public string BASICBASEPARTYNAME { get; set; }
        [XmlElement(ElementName = "FBTPAYMENTTYPE")]
        public string FBTPAYMENTTYPE { get; set; }
        [XmlElement(ElementName = "PERSISTEDVIEW")]
        public string PERSISTEDVIEW { get; set; }
        [XmlElement(ElementName = "PLACEOFSUPPLY")]
        public string PLACEOFSUPPLY { get; set; }
        [XmlElement(ElementName = "CONSIGNEEGSTIN")]
        public string CONSIGNEEGSTIN { get; set; }
        [XmlElement(ElementName = "BASICBUYERNAME")]
        public string BASICBUYERNAME { get; set; }
        [XmlElement(ElementName = "BASICDATETIMEOFINVOICE")]
        public string BASICDATETIMEOFINVOICE { get; set; }
        [XmlElement(ElementName = "BASICDATETIMEOFREMOVAL")]
        public string BASICDATETIMEOFREMOVAL { get; set; }
        [XmlElement(ElementName = "CONSIGNEEPINNUMBER")]
        public string CONSIGNEEPINNUMBER { get; set; }
        [XmlElement(ElementName = "CONSIGNEESTATENAME")]
        public string CONSIGNEESTATENAME { get; set; }
        [XmlElement(ElementName = "EFFECTIVEDATE")]
        public string EFFECTIVEDATE { get; set; }
        [XmlElement(ElementName = "LEDGERENTRIES.LIST")]
        public List<LEDGERENTRIES> LEDGERENTRIES { get; set; }
        [XmlElement(ElementName = "ALLINVENTORYENTRIES.LIST")]
        public List<ALLINVENTORYENTRIES> ALLINVENTORYENTRIES { get; set; }
        [XmlElement(ElementName = "ALLLEDGERENTRIES.LIST")]
        public List<ALLLEDGERENTRIES> ALLLEDGERENTRIES { get; set; }
        [XmlAttribute(AttributeName = "REMOTEID")]
        public string REMOTEID { get; set; }
        [XmlAttribute(AttributeName = "VCHKEY")]
        public string VCHKEY { get; set; }
        [XmlAttribute(AttributeName = "VCHTYPE")]
        public string VCHTYPE { get; set; }
        [XmlAttribute(AttributeName = "ACTION")]
        public string ACTION = "Create";
        [XmlAttribute(AttributeName = "OBJVIEW")]
        public string OBJVIEW { get; set; }
    }


    [XmlRoot(ElementName = "BILLALLOCATIONS.LIST")]
    public class BILLALLOCATIONS
    {
        [XmlElement(ElementName = "ISDEEMEDPOSITIVE")]
        public string ISDEEMEDPOSITIVE { get; set; }
        [XmlElement(ElementName = "ISLASTDEEMEDPOSITIVE")]
        public string ISLASTDEEMEDPOSITIVE { get; set; }

        [XmlElement(ElementName = "NAME")]
        public string Name { get; set; }
        [XmlElement(ElementName = "BILLTYPE")]
        public string BILLTYPE { get; set; }
        [XmlElement(ElementName = "AMOUNT")]
        public string AMOUNT { get; set; }
        [XmlElement(ElementName = "DATE")]
        public string DATE { get; set; }
    }

    [XmlRoot(ElementName = "LEDGERENTRIES.LIST")]
    public class LEDGERENTRIES
    {
        [XmlElement(ElementName = "ISDEEMEDPOSITIVE")]
        public string ISDEEMEDPOSITIVE { get; set; }
        [XmlElement(ElementName = "ISLASTDEEMEDPOSITIVE")]
        public string ISLASTDEEMEDPOSITIVE { get; set; }

        [XmlElement(ElementName = "LEDGERNAME")]
        public string LEDGERNAME { get; set; }
        [XmlElement(ElementName = "AMOUNT")]
        public string AMOUNT { get; set; }
        [XmlElement(ElementName = "BILLALLOCATIONS.LIST")]
        public BILLALLOCATIONS BILLALLOCATIONS { get; set; }
        [XmlElement(ElementName = "ROUNDTYPE")]
        public string ROUNDTYPE { get; set; }
        [XmlElement(ElementName = "METHODTYPE")]
        public string METHODTYPE { get; set; }
        [XmlElement(ElementName = "VATEXPAMOUNT")]
        public string VATEXPAMOUNT { get; set; }

    }

    [XmlRoot(ElementName = "BATCHALLOCATIONS.LIST")]
    public class BATCHALLOCATIONS
    {
        [XmlElement(ElementName = "ISDEEMEDPOSITIVE")]
        public string ISDEEMEDPOSITIVE { get; set; }
        [XmlElement(ElementName = "ISLASTDEEMEDPOSITIVE")]
        public string ISLASTDEEMEDPOSITIVE { get; set; }

        [XmlElement(ElementName = "GODOWNNAME")]
        public string GODOWNNAME { get; set; }
        [XmlElement(ElementName = "BATCHNAME")]
        public string BATCHNAME { get; set; }
        [XmlElement(ElementName = "AMOUNT")]
        public string AMOUNT { get; set; }
        [XmlElement(ElementName = "ACTUALQTY")]
        public string ACTUALQTY { get; set; }
        [XmlElement(ElementName = "BILLEDQTY")]
        public string BILLEDQTY { get; set; }

    }


    [XmlRoot(ElementName = "ACCOUNTINGALLOCATIONS.LIST")]
    public class ACCOUNTINGALLOCATIONS
    {
        [XmlElement(ElementName = "ISDEEMEDPOSITIVE")]
        public string ISDEEMEDPOSITIVE { get; set; }
        [XmlElement(ElementName = "ISLASTDEEMEDPOSITIVE")]
        public string ISLASTDEEMEDPOSITIVE { get; set; }

        [XmlElement(ElementName = "LEDGERNAME")]
        public string LEDGERNAME { get; set; }
        [XmlElement(ElementName = "CLASSRATE")]
        public string CLASSRATE { get; set; }
        [XmlElement(ElementName = "AMOUNT")]
        public string AMOUNT { get; set; }
        [XmlElement(ElementName = "RATEDETAILS.LIST")]
        public List<RATEDETAILS> RATEDETAILS { get; set; }


    }

    [XmlRoot(ElementName = "ALLINVENTORYENTRIES.LIST")]
    public class ALLINVENTORYENTRIES
    {
        [XmlElement(ElementName = "ISDEEMEDPOSITIVE")]
        public string ISDEEMEDPOSITIVE { get; set; }
        [XmlElement(ElementName = "ISLASTDEEMEDPOSITIVE")]
        public string ISLASTDEEMEDPOSITIVE { get; set; }

        [XmlElement(ElementName = "STOCKITEMNAME")]
        public string STOCKITEMNAME { get; set; }
        [XmlElement(ElementName = "RATE")]
        public string RATE { get; set; }
        [XmlElement(ElementName = "AMOUNT")]
        public string AMOUNT { get; set; }
        [XmlElement(ElementName = "ACTUALQTY")]
        public string ACTUALQTY { get; set; }
        [XmlElement(ElementName = "BILLEDQTY")]
        public string BILLEDQTY { get; set; }
        [XmlElement(ElementName = "BATCHALLOCATIONS.LIST")]
        public BATCHALLOCATIONS BATCHALLOCATIONS { get; set; }
        [XmlElement(ElementName = "ACCOUNTINGALLOCATIONS.LIST")]
        public ACCOUNTINGALLOCATIONS ACCOUNTINGALLOCATIONS { get; set; }
    }


    [XmlRoot(ElementName = "BANKALLOCATIONS.LIST")]
    public class BANKALLOCATIONS
    {
        [XmlElement(ElementName = "ISDEEMEDPOSITIVE")]
        public string ISDEEMEDPOSITIVE { get; set; }
        [XmlElement(ElementName = "ISLASTDEEMEDPOSITIVE")]
        public string ISLASTDEEMEDPOSITIVE { get; set; }

        [XmlElement(ElementName = "DATE")]
        public string DATE { get; set; }
        [XmlElement(ElementName = "INSTRUMENTDATE")]
        public string INSTRUMENTDATE { get; set; }
        [XmlElement(ElementName = "NAME")]
        public string NAME { get; set; }
        [XmlElement(ElementName = "TRANSACTIONTYPE")]
        public string TRANSACTIONTYPE { get; set; }
        [XmlElement(ElementName = "PAYMENTFAVOURING")]
        public string PAYMENTFAVOURING { get; set; }
        [XmlElement(ElementName = "STATUS")]
        public string STATUS { get; set; }
        [XmlElement(ElementName = "PAYMENTMODE")]
        public string PAYMENTMODE { get; set; }
        [XmlElement(ElementName = "BANKPARTYNAME")]
        public string BANKPARTYNAME { get; set; }
        [XmlElement(ElementName = "AMOUNT")]
        public string AMOUNT { get; set; }
        [XmlElement(ElementName = "BANKNAME")]
        public string BANKNAME { get; set; }

        [XmlElement(ElementName = "UNIQUEREFERENCENUMBER")]
        public string UNIQUEREFERENCENUMBER { get; set; }

        [XmlElement(ElementName = "TRANSFERMODE")]
        public string TRANSFERMODE { get; set; }
    }

    [XmlRoot(ElementName = "ALLLEDGERENTRIES.LIST")]
    public class ALLLEDGERENTRIES
    {
        [XmlElement(ElementName = "ISDEEMEDPOSITIVE")]
        public string ISDEEMEDPOSITIVE { get; set; }
        [XmlElement(ElementName = "ISLASTDEEMEDPOSITIVE")]
        public string ISLASTDEEMEDPOSITIVE { get; set; }

        [XmlElement(ElementName = "LEDGERNAME")]
        public string LEDGERNAME { get; set; }
        [XmlElement(ElementName = "GSTCLASS")]
        public string GSTCLASS { get; set; }
        [XmlElement(ElementName = "AMOUNT")]
        public string AMOUNT { get; set; }
        [XmlElement(ElementName = "BANKALLOCATIONS.LIST")]
        public BANKALLOCATIONS BANKALLOCATIONS { get; set; }
        [XmlElement(ElementName = "BILLALLOCATIONS.LIST")]
        public List<BILLALLOCATIONS> BILLALLOCATIONS = new List<BILLALLOCATIONS>();

    }

    [XmlRoot(ElementName = "TALLYMESSAGE")]
    public class TALLYMESSAGE
    {
        [XmlElement(ElementName = "LEDGER")]
        public LEDGER LEDGER { get; set; }
        [XmlElement(ElementName = "UNIT")]
        public UNIT UNIT { get; set; }
        [XmlElement(ElementName = "STOCKGROUP")]
        public STOCKGROUP STOCKGROUP { get; set; }
        [XmlElement(ElementName = "STOCKITEM")]
        public STOCKITEM STOCKITEM { get; set; }
        [XmlElement(ElementName = "VOUCHER")]
        public VOUCHER VOUCHER { get; set; }
        [XmlAttribute(AttributeName = "UDF", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string UDF { get; set; }

    }

    [XmlRoot(ElementName = "REQUESTDATA")]
    public class REQUESTDATA
    {
        [XmlElement(ElementName = "TALLYMESSAGE")]
        public List<TALLYMESSAGE> TALLYMESSAGE = new List<TALLYMESSAGE>();
    }

    [XmlRoot(ElementName = "IMPORTDATA")]
    public class IMPORTDATA
    {
        [XmlElement(ElementName = "REQUESTDATA")]
        public REQUESTDATA REQUESTDATA = new REQUESTDATA();
    }

    [XmlRoot(ElementName = "BODY")]
    public class BODY
    {
        [XmlElement(ElementName = "IMPORTDATA")]
        public IMPORTDATA IMPORTDATA = new IMPORTDATA();
    }

    [XmlRoot(ElementName = "ENVELOPE")]
    public class ENVELOPE
    {
        [XmlElement(ElementName = "BODY")]
        public BODY BODY = new BODY();
    }

}
