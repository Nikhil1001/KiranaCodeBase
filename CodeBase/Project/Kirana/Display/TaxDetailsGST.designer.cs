﻿namespace Kirana.Display
{
    partial class TaxDetailsGST
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DTToDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.DTPFromDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlMain = new JitControls.OMBPanel();
            this.rb3b = new System.Windows.Forms.RadioButton();
            this.rbBillWise = new System.Windows.Forms.RadioButton();
            this.lblSearchName = new System.Windows.Forms.Label();
            this.rbMonthWise = new System.Windows.Forms.RadioButton();
            this.rbGSTR1 = new System.Windows.Forms.RadioButton();
            this.rbDayWise = new System.Windows.Forms.RadioButton();
            this.btnExit = new System.Windows.Forms.Button();
            this.BtnExport = new System.Windows.Forms.Button();
            this.rbGSTR = new System.Windows.Forms.RadioButton();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // DTToDate
            // 
            this.DTToDate.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTToDate.Location = new System.Drawing.Point(343, 38);
            this.DTToDate.Name = "DTToDate";
            this.DTToDate.Size = new System.Drawing.Size(130, 23);
            this.DTToDate.TabIndex = 1;
            this.DTToDate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DTToDate_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(265, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 16);
            this.label2.TabIndex = 100;
            this.label2.Text = "To Date :";
            // 
            // DTPFromDate
            // 
            this.DTPFromDate.CustomFormat = "MMM-yyyy";
            this.DTPFromDate.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTPFromDate.Location = new System.Drawing.Point(102, 38);
            this.DTPFromDate.Name = "DTPFromDate";
            this.DTPFromDate.Size = new System.Drawing.Size(131, 23);
            this.DTPFromDate.TabIndex = 0;
            this.DTPFromDate.ValueChanged += new System.EventHandler(this.DTPFromDate_ValueChanged);
            this.DTPFromDate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DTPFromDate_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 16);
            this.label1.TabIndex = 48;
            this.label1.Text = "From Date :";
            // 
            // pnlMain
            // 
            this.pnlMain.BorderColor = System.Drawing.Color.Gray;
            this.pnlMain.BorderRadius = 3;
            this.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMain.Controls.Add(this.rbGSTR);
            this.pnlMain.Controls.Add(this.rb3b);
            this.pnlMain.Controls.Add(this.rbBillWise);
            this.pnlMain.Controls.Add(this.lblSearchName);
            this.pnlMain.Controls.Add(this.rbMonthWise);
            this.pnlMain.Controls.Add(this.rbGSTR1);
            this.pnlMain.Controls.Add(this.rbDayWise);
            this.pnlMain.Controls.Add(this.btnExit);
            this.pnlMain.Controls.Add(this.BtnExport);
            this.pnlMain.Controls.Add(this.label1);
            this.pnlMain.Controls.Add(this.DTPFromDate);
            this.pnlMain.Controls.Add(this.label2);
            this.pnlMain.Controls.Add(this.DTToDate);
            this.pnlMain.Location = new System.Drawing.Point(9, 8);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(694, 202);
            this.pnlMain.TabIndex = 109;
            // 
            // rb3b
            // 
            this.rb3b.AutoSize = true;
            this.rb3b.Location = new System.Drawing.Point(218, 85);
            this.rb3b.Name = "rb3b";
            this.rb3b.Size = new System.Drawing.Size(71, 17);
            this.rb3b.TabIndex = 123458;
            this.rb3b.TabStop = true;
            this.rb3b.Text = "GSTR 3B";
            this.rb3b.UseVisualStyleBackColor = true;
            this.rb3b.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
            // 
            // rbBillWise
            // 
            this.rbBillWise.AutoSize = true;
            this.rbBillWise.Location = new System.Drawing.Point(564, 85);
            this.rbBillWise.Name = "rbBillWise";
            this.rbBillWise.Size = new System.Drawing.Size(111, 17);
            this.rbBillWise.TabIndex = 118;
            this.rbBillWise.TabStop = true;
            this.rbBillWise.Text = "Bill Wise Summary";
            this.rbBillWise.UseVisualStyleBackColor = true;
            // 
            // lblSearchName
            // 
            this.lblSearchName.BackColor = System.Drawing.Color.Coral;
            this.lblSearchName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSearchName.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblSearchName.ForeColor = System.Drawing.Color.White;
            this.lblSearchName.Location = new System.Drawing.Point(0, 0);
            this.lblSearchName.Name = "lblSearchName";
            this.lblSearchName.Size = new System.Drawing.Size(692, 23);
            this.lblSearchName.TabIndex = 123457;
            this.lblSearchName.Text = "GST Reports";
            this.lblSearchName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rbMonthWise
            // 
            this.rbMonthWise.AutoSize = true;
            this.rbMonthWise.Location = new System.Drawing.Point(426, 85);
            this.rbMonthWise.Name = "rbMonthWise";
            this.rbMonthWise.Size = new System.Drawing.Size(128, 17);
            this.rbMonthWise.TabIndex = 117;
            this.rbMonthWise.TabStop = true;
            this.rbMonthWise.Text = "Month Wise Summary";
            this.rbMonthWise.UseVisualStyleBackColor = true;
            // 
            // rbGSTR1
            // 
            this.rbGSTR1.AutoSize = true;
            this.rbGSTR1.Location = new System.Drawing.Point(20, 85);
            this.rbGSTR1.Name = "rbGSTR1";
            this.rbGSTR1.Size = new System.Drawing.Size(91, 17);
            this.rbGSTR1.TabIndex = 112;
            this.rbGSTR1.TabStop = true;
            this.rbGSTR1.Text = "GSTR1 (CSV)";
            this.rbGSTR1.UseVisualStyleBackColor = true;
            this.rbGSTR1.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
            // 
            // rbDayWise
            // 
            this.rbDayWise.AutoSize = true;
            this.rbDayWise.Location = new System.Drawing.Point(299, 85);
            this.rbDayWise.Name = "rbDayWise";
            this.rbDayWise.Size = new System.Drawing.Size(117, 17);
            this.rbDayWise.TabIndex = 116;
            this.rbDayWise.TabStop = true;
            this.rbDayWise.Text = "Day Wise Summary";
            this.rbDayWise.UseVisualStyleBackColor = true;
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(296, 135);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(93, 58);
            this.btnExit.TabIndex = 110;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // BtnExport
            // 
            this.BtnExport.Location = new System.Drawing.Point(170, 135);
            this.BtnExport.Name = "BtnExport";
            this.BtnExport.Size = new System.Drawing.Size(93, 58);
            this.BtnExport.TabIndex = 103;
            this.BtnExport.Text = "Export\r\nExcel";
            this.BtnExport.UseVisualStyleBackColor = false;
            this.BtnExport.Click += new System.EventHandler(this.BtnExport_Click);
            // 
            // rbGSTR
            // 
            this.rbGSTR.AutoSize = true;
            this.rbGSTR.Location = new System.Drawing.Point(128, 85);
            this.rbGSTR.Name = "rbGSTR";
            this.rbGSTR.Size = new System.Drawing.Size(61, 17);
            this.rbGSTR.TabIndex = 123459;
            this.rbGSTR.TabStop = true;
            this.rbGSTR.Text = "GSTR1";
            this.rbGSTR.UseVisualStyleBackColor = true;
            // 
            // TaxDetailsGST
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(715, 224);
            this.Controls.Add(this.pnlMain);
            this.Name = "TaxDetailsGST";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TaxDetails";
            this.Load += new System.EventHandler(this.TaxDetailsGST_Load);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.DateTimePicker DTToDate;
        private System.Windows.Forms.Label label2;
        internal System.Windows.Forms.DateTimePicker DTPFromDate;
        private System.Windows.Forms.Label label1;
        private JitControls.OMBPanel pnlMain;
        internal System.Windows.Forms.Button BtnExport;
        internal System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.RadioButton rbGSTR1;
        private System.Windows.Forms.Label lblSearchName;
        private System.Windows.Forms.RadioButton rbBillWise;
        private System.Windows.Forms.RadioButton rbMonthWise;
        private System.Windows.Forms.RadioButton rbDayWise;
        private System.Windows.Forms.RadioButton rb3b;
        private System.Windows.Forms.RadioButton rbGSTR;
    }
}