﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;
using NPOI.HSSF.Model; // InternalWorkbook
using NPOI.HSSF.UserModel;
using System.IO;
using System.Data.OleDb; // HSSFWorkbook, HSSFSheet


namespace Kirana.Display
{
    public partial class TaxDetailsGST : Form
    {
        HSSFWorkbook wb;
        HSSFSheet sh;


        CommonFunctions ObjFunction = new CommonFunctions();
        long VchTypeCode = 0;

        public TaxDetailsGST()
        {
            InitializeComponent();
        }

        public TaxDetailsGST(long VchTypeCode)
        {
            InitializeComponent();
            this.rbGSTR1.Visible = false;
            if (VchTypeCode == VchType.Sales)
            {
                this.Text = "Sales - GST Tax Detail";
                this.rbGSTR1.Visible = true;
            }
            else if (VchTypeCode == VchType.Purchase)
            {
                this.Text = "Purchase - GST Tax Detail";
            }
            else if (VchTypeCode == VchType.RejectionIn)
            {
                this.Text = "Sales Return - GST Tax Detail";
            }
            else if (VchTypeCode == VchType.RejectionOut)
            {
                this.Text = "Purchase Return - GST Tax Detail";
            }
            this.VchTypeCode = VchTypeCode;
        }

        private void TaxDetailsGST_Load(object sender, EventArgs e)
        {
            if (rbGSTR1.Visible == true)
                rbGSTR1.Checked = true;

        }

        private void DTPFromDate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt16(e.KeyChar) == 13)
            {
                DTToDate.Focus();
            }
        }

        private void DTToDate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt16(e.KeyChar) == 13)
            {
                BtnExport.Focus();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /*13-Apr-2014
        * For set Min date issue solved.
        * */

        private void DTPFromDate_ValueChanged(object sender, EventArgs e)
        {
            DTToDate.MinDate = DTPFromDate.Value;
        }

        private void BtnExport_Click(object sender, EventArgs e)
        {

            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                string destFileName = fbd.SelectedPath;
                if (!destFileName.EndsWith("\\"))
                {
                    destFileName += "\\";
                }

                if (VchTypeCode == VchType.Sales)
                {
                    if (rbDayWise.Checked == true)
                        destFileName += "Sales_GST_Tax_Day_Wise_Details.xls";
                    else if (rbMonthWise.Checked == true)
                        destFileName += "Sales_GST_Tax_Month_Wise_Details.xls";

                }
                else if (VchTypeCode == VchType.Purchase)
                {
                    if (rbDayWise.Checked == true)
                        destFileName += "Purchase_GST_Tax_Day_Wise_Details.xls";
                    else if (rbMonthWise.Checked == true)
                        destFileName += "Purchase_GST_Tax_Month_Wise_Details.xls";

                }
                else if (VchTypeCode == VchType.RejectionIn)
                {
                    if (rbDayWise.Checked == true)
                        destFileName += "Sales_Return_GST_Tax_Day_Wise_Details.xls";
                    else if (rbMonthWise.Checked == true)
                        destFileName += "Sales_Return_GST_Tax_Month_Wise_Details.xls";

                }
                else if (VchTypeCode == VchType.RejectionOut)
                {
                    if (rbDayWise.Checked == true)
                        destFileName += "Purchase_Return_GST_Tax_Day_Wise_Details.xls";
                    else if (rbMonthWise.Checked == true)
                        destFileName += "Purchase_Return_GST_Tax_Month_Wise_Details.xls";
                }

                Display.WorkInProgress wip = new Kirana.Display.WorkInProgress();
                wip.StrStatus = "Exporting data to excel file, please wait.";
                wip.argument = destFileName;
                wip.bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork_ExcelImportData);
                wip.bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted_ExcelImportData);
                ObjFunction.OpenForm(wip);
            }
        }

        private void bgWorker_DoWork_ExcelImportData(object sender, DoWorkEventArgs e)
        {
            CrystalDecisions.CrystalReports.Engine.ReportDocument childForm = null;
            string[] ReportSession = null;

            if (rbDayWise.Checked == true)
            {
                ReportSession = new string[3];
                ReportSession[0] = DTPFromDate.Value.ToString(Format.DDMMMYYYY);
                ReportSession[1] = DTToDate.Value.ToString(Format.DDMMMYYYY);
                ReportSession[2] = VchTypeCode.ToString();

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                    childForm = ObjFunction.GetReportObject("Reports.GetTaxDetailsGST_DayWise");
                else
                    childForm = ObjFunction.LoadReportObject("GetTaxDetailsGST_DayWise.rpt", CommonFunctions.ReportPath);

                if (childForm != null)
                {
                    DBReportGenerate objRpt = new DBReportGenerate(childForm, ReportSession);
                    objRpt.PrintReportXLS(e.Argument.ToString());
                }
                else
                {
                    throw new Exception("Report file not found.");
                }
            }
            else if (rbMonthWise.Checked == true)
            {
                ReportSession = new string[3];
                ReportSession[0] = DTPFromDate.Value.ToString(Format.DDMMMYYYY);
                ReportSession[1] = DTToDate.Value.ToString(Format.DDMMMYYYY);
                ReportSession[2] = VchTypeCode.ToString();

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                    childForm = ObjFunction.GetReportObject("Reports.GetTaxDetailsGST_MonthWise");
                else
                    childForm = ObjFunction.LoadReportObject("GetTaxDetailsGST_MonthWise.rpt", CommonFunctions.ReportPath);

                if (childForm != null)
                {
                    DBReportGenerate objRpt = new DBReportGenerate(childForm, ReportSession);
                    objRpt.PrintReportXLS(e.Argument.ToString());
                }
                else
                {
                    throw new Exception("Report file not found.");
                }
            }
            else if (rbBillWise.Checked == true)
            {

                try
                {
                    ExportGSTTaxDetails(e.Argument.ToString());
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            else if (rb3b.Checked == true)
            {

                try
                {
                    ExportGST3B(e.Argument.ToString());
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            else if (rbGSTR1.Checked == true)
            {
                try
                {
                    //  ExportGSTDetails(e.Argument.ToString()+"GST\\");
                    ExportGSTDetails_CSV(e.Argument.ToString());
                    ExportGSTTaxDetails_Json(e.Argument.ToString());
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            else if (rbGSTR.Checked == true)
            {
                try
                {
                    //  ExportGSTDetails(e.Argument.ToString()+"GST\\");
                    ExportGSTDetails(e.Argument.ToString());
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }


        }

        private void bgWorker_RunWorkerCompleted_ExcelImportData(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                //ObjFunction.ExceptionDisplay(e.Error);
                OMMessageBox.Show("Error : " + e.Error.Message + Environment.NewLine + "Details : " + e.Error.ToString());
            }
            else
            {
                OMMessageBox.Show("Data export to excel file completed...");
            }
        }

        private void ExportGSTDetails_CSV(string destFileName)
        {

            DataTable dt;
            DataSet ds;
            DataTable dtHeader;

            string FromDate = "01-" + DTPFromDate.Text;
            string ToDate = Convert.ToDateTime(DTPFromDate.Text).AddMonths(1).ToString("dd-MMM-yyyy");
            string FullDestFilePath = destFileName + "\\GST";
            if (!System.IO.Directory.Exists(FullDestFilePath))
            {
                System.IO.Directory.CreateDirectory(FullDestFilePath);
            }

            //B2B
            string sourceFileName = AppDomain.CurrentDomain.BaseDirectory + "Templates\\" + "B2BInvoices.csv";
            string b2bFilePath = FullDestFilePath + "\\" + "B2BInvoices_" + DTPFromDate.Text + ".csv";
            System.IO.File.Copy(sourceFileName, b2bFilePath, true);
            ds = GetDataView("Exec GetTaxDetailsGST_b2b  '" + FromDate + "','" + ToDate + "'," + VchTypeCode + " ");
            dt = ds.Tables[1].DefaultView.Table;
            dt.Columns.Remove("IGSTAmount");
            dt.Columns.Remove("CGSTAmount");
            dt.Columns.Remove("SGSTAmount");
            string ColumnsName = "[GSTIN/UIN of Recipient],[Invoice Number],[Invoice date],[Invoice Value],[Place Of Supply],[Reverse Charge],[Invoice Type],[E-Commerce GSTIN],[Rate],[Taxable Value],[Cess Amount]";
            copyDataFromDBTOCSV(dt, b2bFilePath, ColumnsName);


            //B2C Small
            sourceFileName = AppDomain.CurrentDomain.BaseDirectory + "Templates\\" + "B2C(Small).csv";
            string b2CsFilePath = FullDestFilePath + "\\" + "B2C(Small)_" + DTPFromDate.Text + ".csv";
            System.IO.File.Copy(sourceFileName, b2CsFilePath, true);
            dt = ObjFunction.GetDataView("Exec GetTaxDetailsGST_b2cs '" + FromDate + "','" + ToDate + "'," + VchTypeCode + " ").Table;
            dt.Columns.Remove("IGSTAmount");
            dt.Columns.Remove("CGSTAmount");
            dt.Columns.Remove("SGSTAmount");
            ColumnsName = "[Type],[Place Of Supply],[Rate],[Taxable Value],[Cess Amount],[E-Commerce GSTIN]";
            copyDataFromDBTOCSV(dt, b2CsFilePath, ColumnsName);

            //HSNDetails

            sourceFileName = AppDomain.CurrentDomain.BaseDirectory + "Templates\\" + "HSNDetails.csv";
            string HSNDetails = FullDestFilePath + "\\" + "HSNDetails_" + DTPFromDate.Text + ".csv";
            System.IO.File.Copy(sourceFileName, HSNDetails, true);
            dt = ObjFunction.GetDataView("Exec GetTaxDetailsGST_HSN '" + FromDate + "','" + ToDate + "'," + VchTypeCode + " ").Table;
            ColumnsName = "[HSN],[Description],[UQC],[Total Quantity],[Total Value],[Taxable Value],[Integrated Tax Amount],[Central Tax Amount],[State/UT Tax Amount],[Cess Amount]";
            copyDataFromDBTOCSV(dt, HSNDetails, ColumnsName);


            //documents
            sourceFileName = AppDomain.CurrentDomain.BaseDirectory + "Templates\\" + "DocumentIssue.csv";
            string documentsFilePath = FullDestFilePath + "\\" + "DocumentIssue_" + DTPFromDate.Text + ".csv";
            System.IO.File.Copy(sourceFileName, documentsFilePath, true);
            dt = ObjFunction.GetDataView("Select 'Invoice for outward supply' as Nature, ISNULL( Max(VoucherUserNo),0) As MaxBill,ISNull(Min(VoucherUserNo),0) As MinBill,Count(*) As TotalBill " +
                " , " +
                "( Select Count(*) As TotalBill From TVoucherEntry  " +
                            " Where VoucherTypeCode=" + VchTypeCode + " And TVoucherEntry.IsCancel = 'True' And (TVoucherEntry.VoucherDate >= '" + FromDate + "') AND (TVoucherEntry.VoucherDate < '" + ToDate + "') ) As TotalBill" +

                                             " From TVoucherEntry " +
                                             " Where  VoucherTypeCode=" + VchTypeCode + " And (TVoucherEntry.VoucherDate >= '" + FromDate + "') AND (TVoucherEntry.VoucherDate < '" + ToDate + "') ").Table;

            ColumnsName = "[Nature  of Document],[SrNoFrom],[SrNoTo],[Total Number],[Cancelled]";
            copyDataFromDBTOCSV(dt, documentsFilePath, ColumnsName);

        }

        private void ExportGSTDetails(string destFileName)
        {
            try
            {
                DataTable dt;
                DataSet ds;
                DataTable dtHeader;

                string FromDate = "01-" + DTPFromDate.Text;
                string ToDate = Convert.ToDateTime(DTPFromDate.Text).AddMonths(1).ToString("dd-MMM-yyyy");

                string sourceFileName = AppDomain.CurrentDomain.BaseDirectory + "Templates\\" + "GSTR1_Excel_Workbook_Template.xls";

                destFileName += "GSTR1_Excel_Workbook_Details_" + DTPFromDate.Text + ".xls";
                System.IO.File.Copy(sourceFileName, destFileName, true);
                if (System.IO.File.Exists(destFileName))
                {
                    using (var fs = new FileStream(destFileName, FileMode.Open, FileAccess.Read))
                    {
                        wb = new HSSFWorkbook(fs);
                    }
                    int StartRow = 4;

                    #region B2b

                    sh = (HSSFSheet)wb.GetSheet("b2b");
                    ds = GetDataView("Exec GetTaxDetailsGST_b2b  '" + FromDate + "','" + ToDate + "'," + VchTypeCode + " ");
                    dt = ds.Tables[1].DefaultView.Table;
                    if (dt.Rows.Count > 65520)
                        throw new Exception("Please specify less period");

                    dtHeader = ds.Tables[0].DefaultView.Table;
                    if (dtHeader.Rows.Count == 1)
                    {
                        sh.GetRow(2).GetCell(0).SetCellValue(Convert.ToDouble(dtHeader.Rows[0][0].ToString()));
                        sh.GetRow(2).GetCell(1).SetCellValue(Convert.ToDouble(dtHeader.Rows[0][1].ToString()));
                        sh.GetRow(2).GetCell(3).SetCellValue(Convert.ToDouble(dtHeader.Rows[0][2].ToString()));
                        sh.GetRow(2).GetCell(12).SetCellValue(Convert.ToDouble(dtHeader.Rows[0][3].ToString()));
                        sh.GetRow(2).GetCell(13).SetCellValue(Convert.ToDouble(dtHeader.Rows[0][4].ToString()));
                        sh.GetRow(2).GetCell(9).SetCellValue(Convert.ToDouble(dtHeader.Rows[0][5].ToString()));
                        sh.GetRow(2).GetCell(10).SetCellValue(Convert.ToDouble(dtHeader.Rows[0][6].ToString()));
                        sh.GetRow(2).GetCell(11).SetCellValue(Convert.ToDouble(dtHeader.Rows[0][7].ToString()));
                    }

                    if (dt.Rows.Count > 65520)
                        throw new Exception("Please specify less period");

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (sh.GetRow(i + StartRow) == null)
                            sh.CreateRow(i + StartRow);

                        for (int j = 0; j < dt.Columns.Count; j++)
                        {
                            if (sh.GetRow(i + StartRow).GetCell(j) == null)
                                sh.GetRow(i + StartRow).CreateCell(j);


                            if (dt.Rows[i][j].ToString() != null)
                            {
                                if (j == 2)
                                    sh.GetRow(i + StartRow).GetCell(j).SetCellValue(Convert.ToDateTime(dt.Rows[i][j].ToString()).Date);
                                else if (j == 0 || j == 4 || j == 5 || j == 6 || j == 7)
                                    sh.GetRow(i + StartRow).GetCell(j).SetCellValue(dt.Rows[i][j].ToString());
                                else
                                    sh.GetRow(i + StartRow).GetCell(j).SetCellValue(Convert.ToDouble(dt.Rows[i][j].ToString()));
                            }
                        }
                    }

                    #endregion

                    #region b2cl

                    sh = (HSSFSheet)wb.GetSheet("b2cl");
                    ds = GetDataView("Exec GetTaxDetailsGST_b2cl '" + FromDate + "','" + ToDate + "'," + VchTypeCode + " ");
                    dt = ds.Tables[1].DefaultView.Table;
                    if (dt.Rows.Count > 65520)
                        throw new Exception("Please specify less period");

                    dtHeader = ds.Tables[0].DefaultView.Table;
                    if (dtHeader.Rows.Count == 1)
                    {
                        sh.GetRow(2).GetCell(0).SetCellValue(Convert.ToDouble(dtHeader.Rows[0][0].ToString()));
                        sh.GetRow(2).GetCell(2).SetCellValue(Convert.ToDouble(dtHeader.Rows[0][1].ToString()));
                        sh.GetRow(2).GetCell(8).SetCellValue(Convert.ToDouble(dtHeader.Rows[0][2].ToString()));
                        sh.GetRow(2).GetCell(9).SetCellValue(Convert.ToDouble(dtHeader.Rows[0][3].ToString()));
                        sh.GetRow(2).GetCell(5).SetCellValue(Convert.ToDouble(dtHeader.Rows[0][4].ToString()));
                        sh.GetRow(2).GetCell(6).SetCellValue(Convert.ToDouble(dtHeader.Rows[0][5].ToString()));
                        sh.GetRow(2).GetCell(7).SetCellValue(Convert.ToDouble(dtHeader.Rows[0][6].ToString()));
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (sh.GetRow(i + StartRow) == null)
                            sh.CreateRow(i + StartRow);

                        for (int j = 0; j < dt.Columns.Count; j++)
                        {
                            if (sh.GetRow(i + StartRow).GetCell(j) == null)
                            {
                                sh.GetRow(i + StartRow).CreateCell(j);
                            }
                            if (dt.Rows[i][j].ToString() != null)
                            {
                                if (j == 3 || j == 10)
                                    sh.GetRow(i + StartRow).GetCell(j).SetCellValue(dt.Rows[i][j].ToString());
                                else if (j == 1)
                                    sh.GetRow(i + StartRow).GetCell(j).SetCellValue(Convert.ToDateTime(dt.Rows[i][j].ToString()).Date);
                                else
                                    sh.GetRow(i + StartRow).GetCell(j).SetCellValue(Convert.ToDouble(dt.Rows[i][j].ToString()));
                            }
                        }
                    }

                    #endregion

                    #region b2cs

                    sh = (HSSFSheet)wb.GetSheet("b2cs");
                    dt = ObjFunction.GetDataView("Exec GetTaxDetailsGST_b2cs '" + FromDate + "','" + ToDate + "'," + VchTypeCode + " ").Table;
                    if (dt.Rows.Count > 65520)
                        throw new Exception("Please specify less period");

                    if (dt.Rows.Count != 0)
                    {
                        var TotalNet = dt.Compute("Sum(NetAmount)", "");
                        var TotalCess = dt.Compute("Sum(CessAmount)", "");
                        var TotalIGST = dt.Compute("Sum(IGSTAmount)", "");
                        var TotalCGSTAmount = dt.Compute("Sum(CGSTAmount)", "");
                        var TotalSGSTAmount = dt.Compute("Sum(SGSTAmount)", "");

                        sh.GetRow(2).GetCell(3).SetCellValue(Convert.ToDouble(TotalIGST));
                        sh.GetRow(2).GetCell(4).SetCellValue(Convert.ToDouble(TotalCGSTAmount));
                        sh.GetRow(2).GetCell(5).SetCellValue(Convert.ToDouble(TotalSGSTAmount));
                        sh.GetRow(2).GetCell(6).SetCellValue(Convert.ToDouble(TotalNet));
                        sh.GetRow(2).GetCell(7).SetCellValue(Convert.ToDouble(TotalCess));
                    }
                    else
                    {
                        sh.GetRow(2).GetCell(3).SetCellValue(0);
                        sh.GetRow(2).GetCell(4).SetCellValue(0);
                        sh.GetRow(2).GetCell(5).SetCellValue(0);
                        sh.GetRow(2).GetCell(6).SetCellValue(0);
                        sh.GetRow(2).GetCell(7).SetCellValue(0);

                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (sh.GetRow(i + StartRow) == null)
                            sh.CreateRow(i + StartRow);

                        for (int j = 0; j < dt.Columns.Count; j++)
                        {
                            if (sh.GetRow(i + StartRow).GetCell(j) == null)
                                sh.GetRow(i + StartRow).CreateCell(j);

                            if (dt.Rows[i][j].ToString() != null)
                            {
                                if (j == 0 || j == 1 || j == 8)
                                    sh.GetRow(i + StartRow).GetCell(j).SetCellValue(dt.Rows[i][j].ToString());
                                else
                                    sh.GetRow(i + StartRow).GetCell(j).SetCellValue(Convert.ToDouble(dt.Rows[i][j].ToString()));
                            }
                        }
                    }

                    #endregion

                    #region HSN

                    sh = (HSSFSheet)wb.GetSheet("hsn");
                    dt = ObjFunction.GetDataView("Exec GetTaxDetailsGST_HSN '" + FromDate + "','" + ToDate + "'," + VchTypeCode + " ").Table;
                    if (dt.Rows.Count > 65520)
                        throw new Exception("Please specify less period");

                    var varTotalHSN = (from r in dt.AsEnumerable() select r["HSNCode"]).Distinct().ToList();
                    if (dt.Rows.Count != 0)
                    {
                        var TaxableAmount = dt.Compute("Sum(TaxableAmount)", "");
                        var TotalValue = dt.Compute("Sum(TotalValue)", "");
                        var IGSTAmount = dt.Compute("Sum(IGSTAmount)", "");
                        var CGSTAmount = dt.Compute("Sum(CGSTAmount)", "");
                        var SGSTAmount = dt.Compute("Sum(SGSTAmount)", "");
                        var CessAmount = dt.Compute("Sum(CessAmount)", "");

                        sh.GetRow(2).GetCell(0).SetCellValue(varTotalHSN.Count);
                        sh.GetRow(2).GetCell(4).SetCellValue(Convert.ToDouble(TotalValue));
                        sh.GetRow(2).GetCell(5).SetCellValue(Convert.ToDouble(TaxableAmount));
                        sh.GetRow(2).GetCell(6).SetCellValue(Convert.ToDouble(IGSTAmount));
                        sh.GetRow(2).GetCell(7).SetCellValue(Convert.ToDouble(CGSTAmount));
                        sh.GetRow(2).GetCell(8).SetCellValue(Convert.ToDouble(SGSTAmount));
                        sh.GetRow(2).GetCell(9).SetCellValue(Convert.ToDouble(CessAmount));
                    }
                    else
                    {
                        sh.GetRow(2).GetCell(0).SetCellValue(0);
                        sh.GetRow(2).GetCell(4).SetCellValue(0);
                        sh.GetRow(2).GetCell(5).SetCellValue(0);
                        sh.GetRow(2).GetCell(6).SetCellValue(0);
                        sh.GetRow(2).GetCell(7).SetCellValue(0);
                        sh.GetRow(2).GetCell(8).SetCellValue(0);
                        sh.GetRow(2).GetCell(9).SetCellValue(0);

                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (sh.GetRow(i + StartRow) == null)
                            sh.CreateRow(i + StartRow);

                        for (int j = 0; j < dt.Columns.Count; j++)
                        {
                            if (sh.GetRow(i + StartRow).GetCell(j) == null)
                                sh.GetRow(i + StartRow).CreateCell(j);

                            if (dt.Rows[i][j].ToString() != null)
                            {
                                if (j == 0 || j == 1 || j == 2)
                                {
                                    sh.GetRow(i + StartRow).GetCell(j).SetCellValue(dt.Rows[i][j].ToString());
                                }
                                else
                                    sh.GetRow(i + StartRow).GetCell(j).SetCellValue(Convert.ToDouble(dt.Rows[i][j].ToString()));


                            }
                        }
                    }
                    #endregion

                    #region Data
                    sh = (HSSFSheet)wb.GetSheet("documents");
                    dt = ObjFunction.GetDataView("Select ISNULL( Max(VoucherUserNo),0) As MaxBill,ISNull(Min(VoucherUserNo),0) As MinBill,Count(*) As TotalBill " +
                                               " From TVoucherEntry " +
                                               " Where  VoucherTypeCode=" + VchTypeCode + " And (TVoucherEntry.VoucherDate >= '" + FromDate + "') AND (TVoucherEntry.VoucherDate < '" + ToDate + "') ").Table;
                    if (dt.Rows.Count != 0)
                    {

                        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();
                        double TotalCancelBill = ObjQry.ReturnDouble("Select Count(*) As TotalBill From TVoucherEntry  " +
                            " Where VoucherTypeCode=" + VchTypeCode + " And TVoucherEntry.IsCancel = 'True' And (TVoucherEntry.VoucherDate >= '" + FromDate + "') AND (TVoucherEntry.VoucherDate < '" + ToDate + "') ", CommonFunctions.ConStr);

                        sh.GetRow(2).GetCell(3).SetCellValue(Convert.ToDouble(dt.Rows[0].ItemArray[2].ToString()));
                        sh.GetRow(2).GetCell(4).SetCellValue(TotalCancelBill);

                        int StartRowIndex = StartRow;
                        if (sh.GetRow(StartRowIndex) == null)
                            sh.CreateRow(StartRowIndex);

                        //Summary of documents issued during the tax period
                        if (sh.GetRow(StartRowIndex).GetCell(0) == null)
                            sh.GetRow(StartRowIndex).CreateCell(0);
                        sh.GetRow(StartRowIndex).GetCell(0).SetCellValue("Invoice for outward supply");

                        //Sr. No. From
                        if (sh.GetRow(StartRowIndex).GetCell(1) == null)
                            sh.GetRow(StartRowIndex).CreateCell(1);
                        sh.GetRow(StartRowIndex).GetCell(1).SetCellValue(Convert.ToDouble(dt.Rows[0].ItemArray[1].ToString()));

                        //Sr. No. To
                        if (sh.GetRow(StartRowIndex).GetCell(2) == null)
                            sh.GetRow(StartRowIndex).CreateCell(2);
                        sh.GetRow(StartRowIndex).GetCell(2).SetCellValue(Convert.ToDouble(dt.Rows[0].ItemArray[0].ToString()));

                        //Total Number
                        if (sh.GetRow(StartRowIndex).GetCell(3) == null)
                            sh.GetRow(StartRowIndex).CreateCell(3);
                        sh.GetRow(StartRowIndex).GetCell(3).SetCellValue(Convert.ToDouble(dt.Rows[0].ItemArray[2].ToString()));

                        //Cancelled
                        if (sh.GetRow(StartRowIndex).GetCell(4) == null)
                            sh.GetRow(StartRowIndex).CreateCell(4);
                        sh.GetRow(StartRowIndex).GetCell(4).SetCellValue(TotalCancelBill);
                    }


                    #endregion

                    #region Nil

                    sh = (HSSFSheet)wb.GetSheet("nil");
                    dt = ObjFunction.GetDataView("Exec GetTaxDetailsGST_Nil '" + FromDate + "','" + ToDate + "'," + VchTypeCode + " ").Table;
                    if (dt.Rows.Count > 65520)
                        throw new Exception("Please specify less period");

                    if (dt.Rows.Count != 0)
                    {
                        var VarNetAmount = dt.Compute("Sum(NetAmount)", "");

                        sh.GetRow(2).GetCell(0).SetCellValue(dt.Rows.Count);
                        sh.GetRow(2).GetCell(2).SetCellValue(Convert.ToDouble(VarNetAmount));
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (sh.GetRow(i + StartRow) == null)
                            sh.CreateRow(i + StartRow);

                        for (int j = 0; j < dt.Columns.Count; j++)
                        {
                            if (sh.GetRow(i + StartRow).GetCell(j) == null)
                            {
                                sh.GetRow(i + StartRow).CreateCell(j);
                            }
                            if (dt.Rows[i][j].ToString() != null)
                            {
                                if (j == 2)
                                {
                                    sh.GetRow(i + StartRow).GetCell(j).SetCellValue(Convert.ToDouble(dt.Rows[i][j].ToString()));
                                }
                                else
                                    sh.GetRow(i + StartRow).GetCell(j).SetCellValue(dt.Rows[i][j].ToString());
                            }
                        }
                    }

                    #endregion
                    //HSSFFormulaEvaluator.EvaluateAllFormulaCells(wb);
                    using (var fs = new FileStream(destFileName, FileMode.Open, FileAccess.Write))
                    {
                        wb.Write(fs);
                        fs.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void copyDataFromDBTOCSV(DataTable dt, string sourceFileName, string StrColumnsName)
        {
            string sql = "";
            OleDbConnection olecon = null;
            try
            {
                FileInfo file = new FileInfo(sourceFileName);

                string connstring = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                       "Data Source=" + file.DirectoryName + ";Extended Properties=\"Text;HDR=YES;FMT=Delimited(,)\";";

                olecon = new OleDbConnection(connstring);
                olecon.Open();
                string strTableName = "[" + file.Name + "]";

                OleDbCommand olecomm = new OleDbCommand();
                olecomm.Connection = olecon;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    sql = "Insert into " + strTableName + " " +
                        " (" + StrColumnsName + ")" +
                        " values(";


                    string strValues = "";
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        if (dt.Columns[j].DataType == typeof(DateTime))
                        {
                            strValues += ",'" + Convert.ToDateTime(dt.Rows[i][j].ToString().Replace("'", "''")).ToString("dd-MMM-yy") + "'";
                        }
                        else
                            strValues += ",'" + dt.Rows[i][j].ToString().Replace("'", "''") + "'";
                    }
                    strValues = strValues.Substring(1);
                    sql += strValues + ");" + Environment.NewLine;

                    olecomm.CommandText = sql;
                    olecomm.ExecuteNonQuery();

                }
                olecon.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (olecon != null) olecon.Close();
            }
        }

        public DataSet GetDataView(string sql)
        {
            System.Data.SqlClient.SqlConnection Con = new System.Data.SqlClient.SqlConnection(CommonFunctions.ConStr);
            System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch
            {
                throw;
            }
            finally
            {
                Con.Close();
            }

            return ds;
        }

        private void rb_CheckedChanged(object sender, EventArgs e)
        {
            if (rbGSTR1.Checked == true || rbGSTR.Checked == true)
            {
                DTPFromDate.Format = DateTimePickerFormat.Custom;
                DTPFromDate.CustomFormat = "MMM-yyyy";
                DTPFromDate.Text = DBGetVal.ServerTime.ToString("MMM-yyyy");
                DTToDate.Text = Convert.ToDateTime(DTPFromDate.Text).AddMonths(1).AddDays(-1).ToString("dd-MMM-yyyy");
                this.DTToDate.Visible = false;
                label2.Visible = false;
                label1.Text = "Select Month :";
            }
            else
            {
                this.DTToDate.Visible = true;
                label2.Visible = true;
                DTPFromDate.Text = "01-" + DBGetVal.ServerTime.ToString("MMM-yyyy");
                DTPFromDate.Format = DateTimePickerFormat.Custom;
                DTPFromDate.CustomFormat = "dd-MMM-yyyy";
                DTToDate.Text = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                DTToDate.MinDate = DTPFromDate.Value;//For Min Date Set Purpose
            }

        }

        private void ExportGST3B(string destFileName)
        {
            try
            {

                string str = "", strDisc = "", strChrg = "";
                long RndOff = 0;
                if (VchType.Sales == VchTypeCode)
                {
                    strDisc += ObjFunction.GetAppSettings(AppSettings.S_Discount1);
                    strChrg += ObjFunction.GetAppSettings(AppSettings.S_Charges1);
                    RndOff = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_RoundOfAcc));
                }
                else
                {
                    strDisc += ObjFunction.GetAppSettings(AppSettings.P_Discount1);
                    strChrg += ObjFunction.GetAppSettings(AppSettings.P_Charges1);
                    RndOff = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_RoundOfAcc));
                }

                string sourceFileName = AppDomain.CurrentDomain.BaseDirectory + "Templates\\" + "GST3B.xls";
                destFileName += "GST3B_" + DTPFromDate.Text + "_To_" + DTToDate.Text + ".xls";
                System.IO.File.Copy(sourceFileName, destFileName, true);
                if (System.IO.File.Exists(destFileName))
                {
                    using (var fs = new FileStream(destFileName, FileMode.Open, FileAccess.Read))
                    {
                        wb = new HSSFWorkbook(fs);
                    }
                    sh = (HSSFSheet)wb.GetSheet("3B");

                    sh.GetRow(6).GetCell(33).SetCellValue(Convert.ToDateTime(DTPFromDate.Text).Year.ToString());
                    sh.GetRow(7).GetCell(33).SetCellValue(Convert.ToDateTime(DTPFromDate.Text).ToString("MMM"));
                    str = "Exec GetTaxDetailsGST_3B '" + Convert.ToDateTime(DTPFromDate.Text).ToString("dd-MMM-yyyy") + "','" + Convert.ToDateTime(DTToDate.Text).ToString("dd-MMM-yyyy") + "'," + VchType.Sales + "," + GroupType.GST + "";
                    DataSet ds = GetDataView(str);
                    DataTable dtTaxDetails = ds.Tables[0].DefaultView.Table;

                    if (dtTaxDetails.Rows.Count != 0)
                    {

                        sh.GetRow(9).GetCell(15).SetCellValue("" + DBGetVal.GSTNo);
                        sh.GetRow(10).GetCell(15).SetCellValue("" + DBGetVal.CompanyName);
                        sh.GetRow(16).GetCell(13).SetCellValue(Convert.ToDouble(dtTaxDetails.Compute("Sum(NetAmount)", "").ToString()));
                        sh.GetRow(16).GetCell(19).SetCellValue(Convert.ToDouble(dtTaxDetails.Compute("Sum(IGSTAmount)", "").ToString()));
                        sh.GetRow(16).GetCell(26).SetCellValue(Convert.ToDouble(dtTaxDetails.Compute("Sum(CGSTAmount)", "").ToString()));
                        sh.GetRow(16).GetCell(30).SetCellValue(Convert.ToDouble(dtTaxDetails.Compute("Sum(SGSTAmount)", "").ToString()));
                        sh.GetRow(16).GetCell(37).SetCellValue(Convert.ToDouble(dtTaxDetails.Compute("Sum(CessAmount)", "").ToString()));
                    }
                    str = "Exec GetTaxDetailsGST_3B '" + Convert.ToDateTime(DTPFromDate.Text).ToString("dd-MMM-yyyy") + "','" + Convert.ToDateTime(DTToDate.Text).ToString("dd-MMM-yyyy") + "'," + VchType.Purchase + "," + GroupType.GST + "";
                    ds = GetDataView(str);
                    dtTaxDetails = ds.Tables[0].DefaultView.Table;
                    if (dtTaxDetails.Rows.Count != 0)
                    {
                        sh.GetRow(42).GetCell(13).SetCellValue(Convert.ToDouble(dtTaxDetails.Compute("Sum(IGSTAmount)", "").ToString()));
                        sh.GetRow(42).GetCell(23).SetCellValue(Convert.ToDouble(dtTaxDetails.Compute("Sum(CGSTAmount)", "").ToString()));
                        sh.GetRow(42).GetCell(28).SetCellValue(Convert.ToDouble(dtTaxDetails.Compute("Sum(SGSTAmount)", "").ToString()));
                        sh.GetRow(42).GetCell(34).SetCellValue(Convert.ToDouble(dtTaxDetails.Compute("Sum(CessAmount)", "").ToString()));

                        sh.GetRow(46).GetCell(13).SetCellValue(Convert.ToDouble(dtTaxDetails.Compute("Sum(IGSTAmount)", "").ToString()));
                        sh.GetRow(46).GetCell(23).SetCellValue(Convert.ToDouble(dtTaxDetails.Compute("Sum(CGSTAmount)", "").ToString()));
                        sh.GetRow(46).GetCell(28).SetCellValue(Convert.ToDouble(dtTaxDetails.Compute("Sum(SGSTAmount)", "").ToString()));
                        sh.GetRow(46).GetCell(34).SetCellValue(Convert.ToDouble(dtTaxDetails.Compute("Sum(CessAmount)", "").ToString()));
                    }


                    using (var fs = new FileStream(destFileName, FileMode.Open, FileAccess.Write))
                    {
                        wb.Write(fs);
                        fs.Close();
                    }
                }


            }
            catch (Exception ex)
            {
            }
        }

        private void ExportGSTTaxDetails(string destFileName)
        {
            try
            {
                string str = "", strDisc = "", strChrg = "";
                long RndOff = 0;
                if (VchType.Sales == VchTypeCode)
                {
                    strDisc += ObjFunction.GetAppSettings(AppSettings.S_Discount1);
                    strChrg += ObjFunction.GetAppSettings(AppSettings.S_Charges1);
                    RndOff = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_RoundOfAcc));
                }
                else
                {
                    strDisc += ObjFunction.GetAppSettings(AppSettings.P_Discount1);
                    strChrg += ObjFunction.GetAppSettings(AppSettings.P_Charges1);
                    RndOff = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_RoundOfAcc));
                }

                string sourceFileName = AppDomain.CurrentDomain.BaseDirectory + "Templates\\" + "GSTTaxDetails.xls";
                destFileName += "GSTTaxDetails_" + DTPFromDate.Text + "_To_" + DTToDate.Text + ".xls";
                System.IO.File.Copy(sourceFileName, destFileName, true);
                if (System.IO.File.Exists(destFileName))
                {
                    using (var fs = new FileStream(destFileName, FileMode.Open, FileAccess.Read))
                    {
                        wb = new HSSFWorkbook(fs);
                    }
                    sh = (HSSFSheet)wb.GetSheet("Sheet1");

                    str = "Exec GetTaxDetailsGST_Header '" + Convert.ToDateTime(DTPFromDate.Text).ToString("dd-MMM-yyyy") + "','" + Convert.ToDateTime(DTToDate.Text).ToString("dd-MMM-yyyy") + "'," + VchTypeCode + ",'" + strDisc + "','" + strChrg + "'," + RndOff + "," + GroupType.GST + "";
                    DataSet ds = GetDataView(str);
                    DataTable dtHeader = ds.Tables[0].DefaultView.Table;
                    DataTable dtTaxDetails = ds.Tables[1].DefaultView.Table;

                    if (dtHeader.Rows.Count == 0)
                        throw new Exception("No Data Found.");


                    #region Header Rows
                    string tmpVal = "";
                    sh.GetRow(0).GetCell(0).SetCellValue(DBGetVal.CompanyName);
                    sh.GetRow(1).GetCell(0).SetCellValue(DBGetVal.CompanyAddress);
                    sh.GetRow(3).GetCell(0).SetCellValue("From Date " + DTPFromDate.Text);
                    sh.GetRow(4).GetCell(0).SetCellValue("ToDate Date " + DTToDate.Text);

                    sh.GetRow(7).GetCell(4).SetCellValue(Convert.ToDouble(dtHeader.Compute("Sum(BilledAmount)", "").ToString()));
                    sh.GetRow(7).GetCell(5).SetCellValue(Convert.ToDouble(dtHeader.Compute("Sum(Disc)", "").ToString()));
                    sh.GetRow(7).GetCell(6).SetCellValue(Convert.ToDouble(dtHeader.Compute("Sum(RndOff)", "").ToString()));
                    sh.GetRow(7).GetCell(7).SetCellValue(Convert.ToDouble(dtHeader.Compute("Sum(Charges)", "").ToString()));
                    sh.GetRow(7).GetCell(33).SetCellValue(Convert.ToDouble(dtHeader.Compute("Sum(SAmt)", "").ToString()));
                    sh.GetRow(7).GetCell(34).SetCellValue(Convert.ToDouble(dtHeader.Compute("Sum(TAmt)", "").ToString()));
                    //0
                    sh.GetRow(7).GetCell(8).SetCellValue(0.00);
                    sh.GetRow(7).GetCell(9).SetCellValue(0.00);
                    sh.GetRow(7).GetCell(10).SetCellValue(0.00);
                    sh.GetRow(7).GetCell(11).SetCellValue(0.00);
                    sh.GetRow(7).GetCell(12).SetCellValue(0.00);
                    //5
                    tmpVal = dtTaxDetails.Compute("Sum(NetAmount)", "IGSTPercent=5").ToString();
                    sh.GetRow(7).GetCell(13).SetCellValue((!string.IsNullOrEmpty(tmpVal) ? Convert.ToDouble(tmpVal) : 0.0));
                    tmpVal = dtTaxDetails.Compute("Sum(IGSTAmount)", "IGSTPercent=5").ToString();
                    sh.GetRow(7).GetCell(14).SetCellValue((!string.IsNullOrEmpty(tmpVal) ? Convert.ToDouble(tmpVal) : 0.0));
                    tmpVal = dtTaxDetails.Compute("Sum(CGSTAmount)", "IGSTPercent=5").ToString();
                    sh.GetRow(7).GetCell(15).SetCellValue((!string.IsNullOrEmpty(tmpVal) ? Convert.ToDouble(tmpVal) : 0.0));
                    tmpVal = dtTaxDetails.Compute("Sum(SGSTAmount)", "IGSTPercent=5").ToString();
                    sh.GetRow(7).GetCell(16).SetCellValue((!string.IsNullOrEmpty(tmpVal) ? Convert.ToDouble(tmpVal) : 0.0));
                    tmpVal = dtTaxDetails.Compute("Sum(CessAmount)", "IGSTPercent=5").ToString();
                    sh.GetRow(7).GetCell(17).SetCellValue((!string.IsNullOrEmpty(tmpVal) ? Convert.ToDouble(tmpVal) : 0.0));
                    //12
                    tmpVal = dtTaxDetails.Compute("Sum(NetAmount)", "IGSTPercent=12").ToString();
                    sh.GetRow(7).GetCell(18).SetCellValue((!string.IsNullOrEmpty(tmpVal) ? Convert.ToDouble(tmpVal) : 0.0));
                    tmpVal = dtTaxDetails.Compute("Sum(IGSTAmount)", "IGSTPercent=12").ToString();
                    sh.GetRow(7).GetCell(19).SetCellValue((!string.IsNullOrEmpty(tmpVal) ? Convert.ToDouble(tmpVal) : 0.0));
                    tmpVal = dtTaxDetails.Compute("Sum(CGSTAmount)", "IGSTPercent=12").ToString();
                    sh.GetRow(7).GetCell(20).SetCellValue((!string.IsNullOrEmpty(tmpVal) ? Convert.ToDouble(tmpVal) : 0.0));
                    tmpVal = dtTaxDetails.Compute("Sum(SGSTAmount)", "IGSTPercent=12").ToString();
                    sh.GetRow(7).GetCell(21).SetCellValue((!string.IsNullOrEmpty(tmpVal) ? Convert.ToDouble(tmpVal) : 0.0));
                    tmpVal = dtTaxDetails.Compute("Sum(CessAmount)", "IGSTPercent=12").ToString();
                    sh.GetRow(7).GetCell(22).SetCellValue((!string.IsNullOrEmpty(tmpVal) ? Convert.ToDouble(tmpVal) : 0.0));
                    //18
                    tmpVal = dtTaxDetails.Compute("Sum(NetAmount)", "IGSTPercent=18").ToString();
                    sh.GetRow(7).GetCell(23).SetCellValue((!string.IsNullOrEmpty(tmpVal) ? Convert.ToDouble(tmpVal) : 0.0));
                    tmpVal = dtTaxDetails.Compute("Sum(IGSTAmount)", "IGSTPercent=18").ToString();
                    sh.GetRow(7).GetCell(24).SetCellValue((!string.IsNullOrEmpty(tmpVal) ? Convert.ToDouble(tmpVal) : 0.0));
                    tmpVal = dtTaxDetails.Compute("Sum(CGSTAmount)", "IGSTPercent=18").ToString();
                    sh.GetRow(7).GetCell(25).SetCellValue((!string.IsNullOrEmpty(tmpVal) ? Convert.ToDouble(tmpVal) : 0.0));
                    tmpVal = dtTaxDetails.Compute("Sum(SGSTAmount)", "IGSTPercent=18").ToString();
                    sh.GetRow(7).GetCell(26).SetCellValue((!string.IsNullOrEmpty(tmpVal) ? Convert.ToDouble(tmpVal) : 0.0));
                    tmpVal = dtTaxDetails.Compute("Sum(CessAmount)", "IGSTPercent=18").ToString();
                    sh.GetRow(7).GetCell(27).SetCellValue((!string.IsNullOrEmpty(tmpVal) ? Convert.ToDouble(tmpVal) : 0.0));
                    //28
                    tmpVal = dtTaxDetails.Compute("Sum(NetAmount)", "IGSTPercent=28").ToString();
                    sh.GetRow(7).GetCell(28).SetCellValue((!string.IsNullOrEmpty(tmpVal) ? Convert.ToDouble(tmpVal) : 0.0));
                    tmpVal = dtTaxDetails.Compute("Sum(IGSTAmount)", "IGSTPercent=28").ToString();
                    sh.GetRow(7).GetCell(29).SetCellValue((!string.IsNullOrEmpty(tmpVal) ? Convert.ToDouble(tmpVal) : 0.0));
                    tmpVal = dtTaxDetails.Compute("Sum(CGSTAmount)", "IGSTPercent=28").ToString();
                    sh.GetRow(7).GetCell(30).SetCellValue((!string.IsNullOrEmpty(tmpVal) ? Convert.ToDouble(tmpVal) : 0.0));
                    tmpVal = dtTaxDetails.Compute("Sum(SGSTAmount)", "IGSTPercent=28").ToString();
                    sh.GetRow(7).GetCell(31).SetCellValue((!string.IsNullOrEmpty(tmpVal) ? Convert.ToDouble(tmpVal) : 0.0));
                    tmpVal = dtTaxDetails.Compute("Sum(CessAmount)", "IGSTPercent=28").ToString();
                    sh.GetRow(7).GetCell(32).SetCellValue((!string.IsNullOrEmpty(tmpVal) ? Convert.ToDouble(tmpVal) : 0.0));

                    #endregion

                    #region Data

                    int RowIndex = 9; int TotalCell = 35;
                    for (int i = 0; i < dtHeader.Rows.Count; i++)
                    {
                        if (sh.GetRow(i + RowIndex) == null)
                            sh.CreateRow(i + RowIndex);
                        int ColIndex = 0;

                        for (int c = 0; c <= TotalCell; c++)
                        {
                            if (sh.GetRow(i + RowIndex).GetCell(c) == null)
                                sh.GetRow(i + RowIndex).CreateCell(c);
                        }
                        //Cell Date
                        sh.GetRow(RowIndex + i).GetCell(ColIndex).SetCellValue(Convert.ToDateTime(dtHeader.Rows[i].ItemArray[2].ToString()).ToString("dd-MMM-yyyy")); ColIndex++;
                        //Cell DocNo
                        sh.GetRow(RowIndex + i).GetCell(ColIndex).SetCellValue(dtHeader.Rows[i].ItemArray[1].ToString()); ColIndex++;
                        //Cell LedgerName
                        sh.GetRow(RowIndex + i).GetCell(ColIndex).SetCellValue(dtHeader.Rows[i].ItemArray[3].ToString()); ColIndex++;
                        //Cell GSTNo
                        sh.GetRow(RowIndex + i).GetCell(ColIndex).SetCellValue(dtHeader.Rows[i].ItemArray[9].ToString()); ColIndex++;
                        //Cell FinalAmt
                        sh.GetRow(RowIndex + i).GetCell(ColIndex).SetCellValue(Convert.ToDouble(dtHeader.Rows[i].ItemArray[4].ToString())); ColIndex++;
                        //Cell Disc
                        sh.GetRow(RowIndex + i).GetCell(ColIndex).SetCellValue(Convert.ToDouble(dtHeader.Rows[i].ItemArray[5].ToString())); ColIndex++;
                        //Cell RndOff
                        sh.GetRow(RowIndex + i).GetCell(ColIndex).SetCellValue(Convert.ToDouble(dtHeader.Rows[i].ItemArray[6].ToString())); ColIndex++;
                        //Cell Charges
                        sh.GetRow(RowIndex + i).GetCell(ColIndex).SetCellValue(Convert.ToDouble(dtHeader.Rows[i].ItemArray[7].ToString())); ColIndex++;

                        for (int c = ColIndex; c < TotalCell - 1; c++)
                        {
                            sh.GetRow(RowIndex + i).GetCell(c).SetCellValue(0.00);
                        }
                        DataRow[] dr = dtTaxDetails.Select("FkVoucherNo=" + dtHeader.Rows[i].ItemArray[0].ToString(), "");
                        int CIndex = ColIndex;
                        foreach (DataRow item in dr)
                        {
                            int StartIndex = 0;
                            if (Convert.ToDouble(item.ItemArray[1].ToString()) == 0)
                                StartIndex = CIndex;
                            else if (Convert.ToDouble(item.ItemArray[1].ToString()) == 5)
                                StartIndex = CIndex + 5;
                            else if (Convert.ToDouble(item.ItemArray[1].ToString()) == 12)
                                StartIndex = CIndex + 10;
                            else if (Convert.ToDouble(item.ItemArray[1].ToString()) == 18)
                                StartIndex = CIndex + 15;
                            else if (Convert.ToDouble(item.ItemArray[1].ToString()) == 28)
                                StartIndex = CIndex + 20;

                            //Cell IGST
                            sh.GetRow(RowIndex + i).GetCell(StartIndex).SetCellValue(Convert.ToDouble(item[6].ToString())); StartIndex++;
                            //Cell IGST
                            sh.GetRow(RowIndex + i).GetCell(StartIndex).SetCellValue(Convert.ToDouble(item[2].ToString())); StartIndex++;
                            //Cell CGST
                            sh.GetRow(RowIndex + i).GetCell(StartIndex).SetCellValue(Convert.ToDouble(item[3].ToString())); StartIndex++;
                            //Cell SGST
                            sh.GetRow(RowIndex + i).GetCell(StartIndex).SetCellValue(Convert.ToDouble(item[4].ToString())); StartIndex++;
                            //Cell CESS
                            sh.GetRow(RowIndex + i).GetCell(StartIndex).SetCellValue(Convert.ToDouble(item[5].ToString())); StartIndex++;
                        }

                        ColIndex = TotalCell - 3; ColIndex++;
                        //Cell Amt
                        sh.GetRow(RowIndex + i).GetCell(ColIndex).SetCellValue(Convert.ToDouble(dtHeader.Rows[i].ItemArray[10].ToString())); ColIndex++;

                        //Cell Tax
                        sh.GetRow(RowIndex + i).GetCell(ColIndex).SetCellValue(Convert.ToDouble(dtHeader.Rows[i].ItemArray[11].ToString())); ColIndex++;

                        //Cell State Name
                        sh.GetRow(RowIndex + i).GetCell(ColIndex).SetCellValue(dtHeader.Rows[i].ItemArray[8].ToString()); ColIndex++;
                    }

                    #endregion

                    using (var fs = new FileStream(destFileName, FileMode.Open, FileAccess.Write))
                    {
                        wb.Write(fs);
                        fs.Close();
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void ExportGSTTaxDetails_Json(string destFileName)
        {
            RootObject objRoot = new RootObject();
            objRoot.gstin = DBGetVal.CompanyName;
            objRoot.fp = Convert.ToDateTime(DTPFromDate.Text).ToString("MM") + Convert.ToDateTime(DTPFromDate.Text).ToString("yyyy");
            objRoot.cur_gt = 0;
            objRoot.version = "GST2.2";
            objRoot.hash = "hash";

            DataTable dt;
            DataSet ds;
            //string FromDate = DTPFromDate.Text;
            string FromDate = "01-" + DTPFromDate.Text;
            string ToDate = Convert.ToDateTime(DTPFromDate.Text).AddMonths(1).ToString("dd-MMM-yyyy");

            #region For b2b

            ds = GetDataView("Exec GetTaxDetailsGST_b2b  '" + FromDate + "','" + ToDate + "'," + VchTypeCode + " ");
            DataView dv = ds.Tables[1].DefaultView;
            dv.Sort = "GSTIN,VoucherUserNo asc";
            dt = dv.ToTable();

            List<B2b> lstb2b = new List<B2b>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                B2b b2b = new B2b();
                b2b.ctin = dt.Rows[i]["GSTIN"].ToString();


                string strGst = b2b.ctin;
                List<Inv> lstinv = new List<Inv>();
                bool flag_gst = true;

                while (flag_gst)
                {

                    Inv inv = new Inv();

                    #region For Inv
                    inv.inum = dt.Rows[i]["VoucherUserNo"].ToString();
                    inv.idt = Convert.ToDateTime(dt.Rows[i]["VoucherDate"].ToString()).ToString("dd-MM-yyyy");
                    inv.val = Convert.ToDouble(dt.Rows[i]["Amount"].ToString());
                    inv.pos = dt.Rows[i]["PlaceOfSupply"].ToString().Substring(0, 2);
                    inv.rchrg = "N";
                    inv.inv_typ = "R";

                    #region For Items

                    string StrInv = dt.Rows[i]["VoucherUserNo"].ToString();
                    bool flag = true;
                    List<Itm> lstitems = new List<Itm>();
                    while (flag)
                    {

                        lstitems.Add(GetItem((int)Convert.ToDouble(dt.Rows[i]["IGSTPercent"].ToString()),
                                               Convert.ToDouble(dt.Rows[i]["NetAmount"].ToString()),
                                                Convert.ToDouble(dt.Rows[i]["CGSTAmount"].ToString()),
                                                Convert.ToDouble(dt.Rows[i]["SGSTAmount"].ToString()),
                                                Convert.ToDouble(dt.Rows[i]["CessAmount"].ToString()))); i++;

                        if (dt.Rows.Count <= i || StrInv != dt.Rows[i]["VoucherUserNo"].ToString())
                        {
                            flag = false; i--;
                            inv.itms = lstitems;
                        }

                    }
                    #endregion

                    #endregion

                    lstinv.Add(inv); i++;

                    if (dt.Rows.Count <= i || strGst != dt.Rows[i]["GSTIN"].ToString())
                    {
                        flag_gst = false; i--;
                    }
                }
                b2b.inv = lstinv;
                lstb2b.Add(b2b);

            }
            objRoot.b2b = lstb2b;
            #endregion

            #region For B2cs

            dt = ObjFunction.GetDataView("Exec GetTaxDetailsGST_b2cs '" + FromDate + "','" + ToDate + "'," + VchTypeCode + " ").Table;
            List<B2cs> lstb2cs = new List<B2cs>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                B2cs b2cs = new B2cs();
                b2cs.typ = "OE";
                b2cs.pos = dt.Rows[i]["PlaceOfSupply"].ToString().Substring(0, 2);
                b2cs.rt = (int)Convert.ToDouble(dt.Rows[i]["IGSTPercent"].ToString());
                b2cs.sply_ty = "INTRA";
                b2cs.txval = Convert.ToDouble(dt.Rows[i]["NetAmount"].ToString());
                b2cs.camt = Convert.ToDouble(dt.Rows[i]["CGSTAmount"].ToString());
                b2cs.samt = Convert.ToDouble(dt.Rows[i]["SGSTAmount"].ToString());
                b2cs.csamt = Convert.ToDouble(dt.Rows[i]["CessAmount"].ToString());
                lstb2cs.Add(b2cs);
            }
            objRoot.b2cs = lstb2cs;

            #endregion

            #region For HSN

            dt = ObjFunction.GetDataView("Exec GetTaxDetailsGST_HSN '" + FromDate + "','" + ToDate + "'," + VchTypeCode + " ").Table;
            Hsn hsn = new Hsn();
            List<Datum> lstDatum = new List<Datum>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Datum item = new Datum();
                item.num = i + 1;
                item.hsn_sc = dt.Rows[i]["HSNCode"].ToString();
                item.desc = dt.Rows[i]["ItemName"].ToString();
                item.uqc = dt.Rows[i]["UOMName"].ToString();
                item.qty = Convert.ToDouble(dt.Rows[i]["TotalQuantity"].ToString());
                item.val = Convert.ToDouble(dt.Rows[i]["TotalValue"].ToString());
                item.txval = Convert.ToDouble(dt.Rows[i]["TaxableAmount"].ToString());
                item.iamt = Convert.ToDouble(dt.Rows[i]["IGSTAmount"].ToString());
                item.camt = Convert.ToDouble(dt.Rows[i]["CGSTAmount"].ToString());
                item.samt = Convert.ToDouble(dt.Rows[i]["SGSTAmount"].ToString());
                item.csamt = Convert.ToDouble(dt.Rows[i]["CessAmount"].ToString());
                lstDatum.Add(item);
            }
            hsn.data = lstDatum;
            objRoot.hsn = hsn;

            #endregion

            string json = Newtonsoft.Json.JsonConvert.SerializeObject(objRoot, Newtonsoft.Json.Formatting.None,
                     new Newtonsoft.Json.JsonSerializerSettings
                     {
                         NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore
                     });



            string FullDestFilePath = destFileName + "\\GST";
            if (!System.IO.Directory.Exists(FullDestFilePath))
            {
                System.IO.Directory.CreateDirectory(FullDestFilePath);
            }

            string sourceFileName = AppDomain.CurrentDomain.BaseDirectory + "Templates\\" + "offline_json.json";
            string b2bFilePath = FullDestFilePath + "\\" + "offline_" + DBGetVal.GSTNo + "_" + DTPFromDate.Text + ".json";
            System.IO.File.Copy(sourceFileName, b2bFilePath, true);
            File.WriteAllText(b2bFilePath, json);


        }

        private Itm GetItem(int Trate, double NetAmount, double CGSTAmount, double SGSTAmount, double CessAmount)
        {
            Itm item = new Itm();
            item.num = Convert.ToInt32(Trate.ToString() + "01");

            ItmDet dtls = new ItmDet();
            dtls.txval = NetAmount;
            dtls.rt = Trate;
            dtls.camt = CGSTAmount;
            dtls.samt = SGSTAmount;
            dtls.csamt = CessAmount;
            item.itm_det = dtls;
            return item;
        }

    }

    #region JSon DBClassess

    public class ItmDet
    {
        public double txval { get; set; }
        public int rt { get; set; }
        public double camt { get; set; }
        public double samt { get; set; }
        public double csamt { get; set; }
    }

    public class Itm
    {
        public int num { get; set; }
        public ItmDet itm_det { get; set; }
    }

    public class Inv
    {
        public string inum { get; set; }
        public string idt { get; set; }
        public double val { get; set; }
        public string pos { get; set; }
        public string rchrg { get; set; }
        public string inv_typ { get; set; }
        public List<Itm> itms { get; set; }
    }

    public class B2b
    {
        public string ctin { get; set; }
        public List<Inv> inv { get; set; }
    }

    public class B2cs
    {
        public string sply_ty { get; set; }
        public int rt { get; set; }
        public string typ { get; set; }
        public string pos { get; set; }
        public double txval { get; set; }
        public double camt { get; set; }
        public double samt { get; set; }
        public double csamt { get; set; }
    }

    public class Datum
    {
        public int num { get; set; }
        public string hsn_sc { get; set; }
        public string desc { get; set; }
        public string uqc { get; set; }
        public double qty { get; set; }
        public double val { get; set; }
        public double txval { get; set; }
        public double iamt { get; set; }
        public double samt { get; set; }
        public double camt { get; set; }
        public double csamt { get; set; }
    }

    public class Hsn
    {
        public List<Datum> data { get; set; }
    }

    public class RootObject
    {
        public string gstin { get; set; }
        public string fp { get; set; }
        public int gt { get; set; }
        public int cur_gt { get; set; }
        public string version { get; set; }
        public string hash { get; set; }
        public List<B2b> b2b { get; set; }
        public List<B2cs> b2cs { get; set; }
        public Hsn hsn { get; set; }
    }

    #endregion Json BDClasses
}
