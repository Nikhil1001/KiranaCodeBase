﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Kirana.Display
{
    public partial class WorkInProgress : Form
    {
        public object argument = null;
        public WorkInProgress()
        {
            InitializeComponent();
        }

        public string StrStatus { set { lblStatus.Text = value; } }

        private void WorkInProgress_Shown(object sender, EventArgs e)
        {
            Application.DoEvents();
            if (argument != null)
            {
                bgWorker.RunWorkerAsync(argument);
            }
            else
            {
                bgWorker.RunWorkerAsync();
            }
        }

        private void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Application.DoEvents();
            this.Close();
        }

    }
}
