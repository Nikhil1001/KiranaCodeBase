﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Display
{
    /// <summary>
    /// This class is used for Minimum Level of Stock.
    /// </summary>
    public partial class MinLevelStock : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public MinLevelStock()
        {
            InitializeComponent();
        }

        private void MinLevelStock_Load(object sender, EventArgs e)
        {
            //DTPFromDate.Text = "01-" + DBGetVal.ServerTime.ToString("MMM-yyyy");
            DTPToDate.Text = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
            //DTPToDate.MinDate = DTPFromDate.Value;
        }
        
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                int valActDeAct = 0;
                if (rdActive.Checked == true) valActDeAct = 0;
                else if (rdDeActive.Checked == true) valActDeAct = 1;
                else if (rdActiveDeActive.Checked == true) valActDeAct = -1;

                string[] ReportSession = new string[4];
                ReportSession[0] = DBGetVal.CompanyNo.ToString();
                ReportSession[1] = Convert.ToDateTime(DTPToDate.Text).ToString("dd-MMM-yyyy");// Convert.ToDateTime(DTPFromDate.Text).ToString("dd-MMM-yyyy");
                ReportSession[2] = Convert.ToDateTime(DTPToDate.Text).ToString("dd-MMM-yyyy");
                ReportSession[3] = valActDeAct.ToString();
                Form NewF = null;
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                    NewF = new Display.ReportViewSource(new Reports.MinLevelStock(), ReportSession);
                else
                    NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("MinLevelStock.rpt", CommonFunctions.ReportPath), ReportSession);
                ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void DTPFromDate_ValueChanged(object sender, EventArgs e)
        {
            DTPToDate.MinDate = DTPFromDate.Value;
        }

       
        }
    }

