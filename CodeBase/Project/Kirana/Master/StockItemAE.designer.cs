﻿namespace Kirana.Master
{
    partial class StockItemAE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.EP = new System.Windows.Forms.ErrorProvider(this.components);
            this.pnlMain = new JitControls.OMBPanel();
            this.btnNewBrand = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblStar10 = new System.Windows.Forms.Label();
            this.lblStar9 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.cmbCSTSales = new System.Windows.Forms.ComboBox();
            this.cmbVatPurchase = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.cmbVatSales = new System.Windows.Forms.ComboBox();
            this.panel1 = new JitControls.OMBPanel();
            this.lblStar5 = new System.Windows.Forms.Label();
            this.lblStar1 = new System.Windows.Forms.Label();
            this.lblStar7 = new System.Windows.Forms.Label();
            this.lblStar8 = new System.Windows.Forms.Label();
            this.lblStar6 = new System.Windows.Forms.Label();
            this.lblStar4 = new System.Windows.Forms.Label();
            this.lblStar3 = new System.Windows.Forms.Label();
            this.lblStar2 = new System.Windows.Forms.Label();
            this.cmbStockDept = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtMaxLevelQty = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtReOrderLevelQty = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.cmbStockLocationName = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtLanguage = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.lbl1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblLowerOther = new System.Windows.Forms.Label();
            this.txtStockConOther = new System.Windows.Forms.TextBox();
            this.lblDefaultOther = new System.Windows.Forms.Label();
            this.lblLower = new System.Windows.Forms.Label();
            this.txtStockCon = new System.Windows.Forms.TextBox();
            this.lblDefault = new System.Windows.Forms.Label();
            this.lblPur = new System.Windows.Forms.Label();
            this.txtPurRate = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cmbDefaultUOM = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.chkActive = new System.Windows.Forms.CheckBox();
            this.cmbLowerUOM = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.gvRateSetting = new System.Windows.Forms.DataGridView();
            this.SrNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BarCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MRP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMName = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ASaleRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BSaleRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CSaleRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DSaleRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ESaleRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MKTQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PurRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RateVariation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Chk = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.StockConversion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkSrNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BarCodeNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsActive = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Hid = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.txtMRP = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtBarcobe = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cmbCategoryName = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cmbDepartmentName = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbGroupNo2 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbCompanyName = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbGroupNo1 = new System.Windows.Forms.ComboBox();
            this.txtItemName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtShortName = new System.Windows.Forms.TextBox();
            this.txtSearchBarCode = new System.Windows.Forms.TextBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.BtnExit = new System.Windows.Forms.Button();
            this.btnPrev = new System.Windows.Forms.Button();
            this.btnFirst = new System.Windows.Forms.Button();
            this.btnLast = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.EP)).BeginInit();
            this.pnlMain.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvRateSetting)).BeginInit();
            this.SuspendLayout();
            // 
            // EP
            // 
            this.EP.ContainerControl = this;
            // 
            // pnlMain
            // 
            this.pnlMain.BorderColor = System.Drawing.Color.SteelBlue;
            this.pnlMain.BorderRadius = 2;
            this.pnlMain.Controls.Add(this.btnNewBrand);
            this.pnlMain.Controls.Add(this.groupBox1);
            this.pnlMain.Controls.Add(this.panel1);
            this.pnlMain.Controls.Add(this.txtSearchBarCode);
            this.pnlMain.Controls.Add(this.btnUpdate);
            this.pnlMain.Controls.Add(this.label23);
            this.pnlMain.Controls.Add(this.btnCancel);
            this.pnlMain.Controls.Add(this.BtnSave);
            this.pnlMain.Controls.Add(this.btnNew);
            this.pnlMain.Controls.Add(this.btnSearch);
            this.pnlMain.Controls.Add(this.btnDelete);
            this.pnlMain.Controls.Add(this.btnNext);
            this.pnlMain.Controls.Add(this.BtnExit);
            this.pnlMain.Controls.Add(this.btnPrev);
            this.pnlMain.Controls.Add(this.btnFirst);
            this.pnlMain.Controls.Add(this.btnLast);
            this.pnlMain.Location = new System.Drawing.Point(21, 6);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(789, 478);
            this.pnlMain.TabIndex = 10004;
            // 
            // btnNewBrand
            // 
            this.btnNewBrand.Location = new System.Drawing.Point(421, 408);
            this.btnNewBrand.Name = "btnNewBrand";
            this.btnNewBrand.Size = new System.Drawing.Size(80, 60);
            this.btnNewBrand.TabIndex = 10004;
            this.btnNewBrand.Text = "New\r\nBrand";
            this.btnNewBrand.UseVisualStyleBackColor = true;
            this.btnNewBrand.Click += new System.EventHandler(this.btnNewBrand_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblStar10);
            this.groupBox1.Controls.Add(this.lblStar9);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.cmbCSTSales);
            this.groupBox1.Controls.Add(this.cmbVatPurchase);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.cmbVatSales);
            this.groupBox1.Location = new System.Drawing.Point(6, 344);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(431, 56);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tax Details";
            // 
            // lblStar10
            // 
            this.lblStar10.AutoSize = true;
            this.lblStar10.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar10.Location = new System.Drawing.Point(414, 33);
            this.lblStar10.Name = "lblStar10";
            this.lblStar10.Size = new System.Drawing.Size(11, 13);
            this.lblStar10.TabIndex = 190018;
            this.lblStar10.Text = "*";
            // 
            // lblStar9
            // 
            this.lblStar9.AutoSize = true;
            this.lblStar9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar9.Location = new System.Drawing.Point(261, 33);
            this.lblStar9.Name = "lblStar9";
            this.lblStar9.Size = new System.Drawing.Size(11, 13);
            this.lblStar9.TabIndex = 190017;
            this.lblStar9.Text = "*";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(40, 59);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(28, 13);
            this.label27.TabIndex = 10010;
            this.label27.Text = "CST";
            this.label27.Visible = false;
            // 
            // cmbCSTSales
            // 
            this.cmbCSTSales.FormattingEnabled = true;
            this.cmbCSTSales.Location = new System.Drawing.Point(106, 54);
            this.cmbCSTSales.MaxLength = 25;
            this.cmbCSTSales.Name = "cmbCSTSales";
            this.cmbCSTSales.Size = new System.Drawing.Size(230, 21);
            this.cmbCSTSales.TabIndex = 10006;
            this.cmbCSTSales.Visible = false;
            // 
            // cmbVatPurchase
            // 
            this.cmbVatPurchase.FormattingEnabled = true;
            this.cmbVatPurchase.Location = new System.Drawing.Point(278, 27);
            this.cmbVatPurchase.MaxLength = 25;
            this.cmbVatPurchase.Name = "cmbVatPurchase";
            this.cmbVatPurchase.Size = new System.Drawing.Size(135, 21);
            this.cmbVatPurchase.TabIndex = 22;
            this.cmbVatPurchase.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbVatPurchase_KeyDown);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(305, 10);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(52, 13);
            this.label26.TabIndex = 10009;
            this.label26.Text = "Purchase";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(148, 10);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(33, 13);
            this.label25.TabIndex = 10008;
            this.label25.Text = "Sales";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(39, 33);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(28, 13);
            this.label24.TabIndex = 10007;
            this.label24.Text = "VAT";
            // 
            // cmbVatSales
            // 
            this.cmbVatSales.FormattingEnabled = true;
            this.cmbVatSales.Location = new System.Drawing.Point(104, 26);
            this.cmbVatSales.Name = "cmbVatSales";
            this.cmbVatSales.Size = new System.Drawing.Size(153, 21);
            this.cmbVatSales.TabIndex = 21;
            this.cmbVatSales.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbVatSales_KeyDown);
            // 
            // panel1
            // 
            this.panel1.BorderColor = System.Drawing.Color.Silver;
            this.panel1.BorderRadius = 3;
            this.panel1.Controls.Add(this.lblStar5);
            this.panel1.Controls.Add(this.lblStar1);
            this.panel1.Controls.Add(this.lblStar7);
            this.panel1.Controls.Add(this.lblStar8);
            this.panel1.Controls.Add(this.lblStar6);
            this.panel1.Controls.Add(this.lblStar4);
            this.panel1.Controls.Add(this.lblStar3);
            this.panel1.Controls.Add(this.lblStar2);
            this.panel1.Controls.Add(this.cmbStockDept);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.txtMaxLevelQty);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.txtReOrderLevelQty);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.cmbStockLocationName);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtLanguage);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.lbl1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.lblLowerOther);
            this.panel1.Controls.Add(this.txtStockConOther);
            this.panel1.Controls.Add(this.lblDefaultOther);
            this.panel1.Controls.Add(this.lblLower);
            this.panel1.Controls.Add(this.txtStockCon);
            this.panel1.Controls.Add(this.lblDefault);
            this.panel1.Controls.Add(this.lblPur);
            this.panel1.Controls.Add(this.txtPurRate);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.cmbDefaultUOM);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.chkActive);
            this.panel1.Controls.Add(this.cmbLowerUOM);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.gvRateSetting);
            this.panel1.Controls.Add(this.txtMRP);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.txtBarcobe);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.cmbCategoryName);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.cmbDepartmentName);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.cmbGroupNo2);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.cmbCompanyName);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.cmbGroupNo1);
            this.panel1.Controls.Add(this.txtItemName);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtShortName);
            this.panel1.Location = new System.Drawing.Point(6, 10);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(773, 328);
            this.panel1.TabIndex = 0;
            // 
            // lblStar5
            // 
            this.lblStar5.AutoSize = true;
            this.lblStar5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar5.Location = new System.Drawing.Point(458, 114);
            this.lblStar5.Name = "lblStar5";
            this.lblStar5.Size = new System.Drawing.Size(11, 13);
            this.lblStar5.TabIndex = 190024;
            this.lblStar5.Text = "*";
            // 
            // lblStar1
            // 
            this.lblStar1.AutoSize = true;
            this.lblStar1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar1.Location = new System.Drawing.Point(458, 10);
            this.lblStar1.Name = "lblStar1";
            this.lblStar1.Size = new System.Drawing.Size(11, 13);
            this.lblStar1.TabIndex = 190023;
            this.lblStar1.Text = "*";
            // 
            // lblStar7
            // 
            this.lblStar7.AutoSize = true;
            this.lblStar7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar7.Location = new System.Drawing.Point(467, 234);
            this.lblStar7.Name = "lblStar7";
            this.lblStar7.Size = new System.Drawing.Size(11, 13);
            this.lblStar7.TabIndex = 190022;
            this.lblStar7.Text = "*";
            // 
            // lblStar8
            // 
            this.lblStar8.AutoSize = true;
            this.lblStar8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar8.Location = new System.Drawing.Point(237, 297);
            this.lblStar8.Name = "lblStar8";
            this.lblStar8.Size = new System.Drawing.Size(11, 13);
            this.lblStar8.TabIndex = 190021;
            this.lblStar8.Text = "*";
            // 
            // lblStar6
            // 
            this.lblStar6.AutoSize = true;
            this.lblStar6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar6.Location = new System.Drawing.Point(458, 140);
            this.lblStar6.Name = "lblStar6";
            this.lblStar6.Size = new System.Drawing.Size(11, 13);
            this.lblStar6.TabIndex = 190020;
            this.lblStar6.Text = "*";
            // 
            // lblStar4
            // 
            this.lblStar4.AutoSize = true;
            this.lblStar4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar4.Location = new System.Drawing.Point(458, 87);
            this.lblStar4.Name = "lblStar4";
            this.lblStar4.Size = new System.Drawing.Size(11, 13);
            this.lblStar4.TabIndex = 190019;
            this.lblStar4.Text = "*";
            // 
            // lblStar3
            // 
            this.lblStar3.AutoSize = true;
            this.lblStar3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar3.Location = new System.Drawing.Point(458, 61);
            this.lblStar3.Name = "lblStar3";
            this.lblStar3.Size = new System.Drawing.Size(11, 13);
            this.lblStar3.TabIndex = 190018;
            this.lblStar3.Text = "*";
            // 
            // lblStar2
            // 
            this.lblStar2.AutoSize = true;
            this.lblStar2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar2.Location = new System.Drawing.Point(458, 36);
            this.lblStar2.Name = "lblStar2";
            this.lblStar2.Size = new System.Drawing.Size(11, 13);
            this.lblStar2.TabIndex = 190017;
            this.lblStar2.Text = "*";
            // 
            // cmbStockDept
            // 
            this.cmbStockDept.FormattingEnabled = true;
            this.cmbStockDept.Location = new System.Drawing.Point(152, 33);
            this.cmbStockDept.MaxLength = 25;
            this.cmbStockDept.Name = "cmbStockDept";
            this.cmbStockDept.Size = new System.Drawing.Size(300, 21);
            this.cmbStockDept.TabIndex = 174;
            this.cmbStockDept.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbStockDept_KeyDown);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(17, 37);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(68, 13);
            this.label19.TabIndex = 175;
            this.label19.Text = "Department :";
            // 
            // txtMaxLevelQty
            // 
            this.txtMaxLevelQty.Location = new System.Drawing.Point(370, 166);
            this.txtMaxLevelQty.Name = "txtMaxLevelQty";
            this.txtMaxLevelQty.Size = new System.Drawing.Size(81, 20);
            this.txtMaxLevelQty.TabIndex = 11;
            this.txtMaxLevelQty.Text = "0.00";
            this.txtMaxLevelQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMaxLevelQty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMaxLevelQty_KeyDown);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(264, 169);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(84, 13);
            this.label18.TabIndex = 173;
            this.label18.Text = "Max Level Qty : ";
            // 
            // txtReOrderLevelQty
            // 
            this.txtReOrderLevelQty.Location = new System.Drawing.Point(151, 166);
            this.txtReOrderLevelQty.Name = "txtReOrderLevelQty";
            this.txtReOrderLevelQty.Size = new System.Drawing.Size(94, 20);
            this.txtReOrderLevelQty.TabIndex = 10;
            this.txtReOrderLevelQty.Text = "0.00";
            this.txtReOrderLevelQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtReOrderLevelQty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtReOrderLevelQty_KeyDown);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(16, 169);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(72, 13);
            this.label17.TabIndex = 171;
            this.label17.Text = "Min Level Qty";
            // 
            // cmbStockLocationName
            // 
            this.cmbStockLocationName.FormattingEnabled = true;
            this.cmbStockLocationName.Location = new System.Drawing.Point(837, 273);
            this.cmbStockLocationName.Name = "cmbStockLocationName";
            this.cmbStockLocationName.Size = new System.Drawing.Size(148, 21);
            this.cmbStockLocationName.TabIndex = 2;
            this.cmbStockLocationName.Visible = false;
            this.cmbStockLocationName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbStockLocationName_KeyDown);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(828, 273);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(85, 13);
            this.label16.TabIndex = 169;
            this.label16.Text = "Stock Location :";
            this.label16.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 114);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 51;
            this.label1.Text = "Item Desc :";
            // 
            // txtLanguage
            // 
            this.txtLanguage.BackColor = System.Drawing.Color.White;
            this.txtLanguage.ForeColor = System.Drawing.Color.Black;
            this.txtLanguage.Location = new System.Drawing.Point(693, 33);
            this.txtLanguage.Name = "txtLanguage";
            this.txtLanguage.ReadOnly = true;
            this.txtLanguage.Size = new System.Drawing.Size(300, 20);
            this.txtLanguage.TabIndex = 4;
            this.txtLanguage.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(547, 36);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 13);
            this.label10.TabIndex = 167;
            this.label10.Text = "Language B.N";
            this.label10.Visible = false;
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Location = new System.Drawing.Point(494, 263);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(13, 13);
            this.lbl1.TabIndex = 166;
            this.lbl1.Text = "1";
            this.lbl1.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(494, 237);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 13);
            this.label3.TabIndex = 165;
            this.label3.Text = "1";
            this.label3.Visible = false;
            // 
            // lblLowerOther
            // 
            this.lblLowerOther.AutoSize = true;
            this.lblLowerOther.Location = new System.Drawing.Point(711, 263);
            this.lblLowerOther.Name = "lblLowerOther";
            this.lblLowerOther.Size = new System.Drawing.Size(35, 13);
            this.lblLowerOther.TabIndex = 164;
            this.lblLowerOther.Text = "label3";
            this.lblLowerOther.Visible = false;
            // 
            // txtStockConOther
            // 
            this.txtStockConOther.Location = new System.Drawing.Point(630, 260);
            this.txtStockConOther.Name = "txtStockConOther";
            this.txtStockConOther.Size = new System.Drawing.Size(70, 20);
            this.txtStockConOther.TabIndex = 16;
            this.txtStockConOther.Visible = false;
            this.txtStockConOther.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtStockConOther_KeyPress);
            // 
            // lblDefaultOther
            // 
            this.lblDefaultOther.AutoSize = true;
            this.lblDefaultOther.Location = new System.Drawing.Point(512, 263);
            this.lblDefaultOther.Name = "lblDefaultOther";
            this.lblDefaultOther.Size = new System.Drawing.Size(35, 13);
            this.lblDefaultOther.TabIndex = 162;
            this.lblDefaultOther.Text = "label3";
            this.lblDefaultOther.Visible = false;
            // 
            // lblLower
            // 
            this.lblLower.AutoSize = true;
            this.lblLower.Location = new System.Drawing.Point(711, 237);
            this.lblLower.Name = "lblLower";
            this.lblLower.Size = new System.Drawing.Size(41, 13);
            this.lblLower.TabIndex = 161;
            this.lblLower.Text = "label16";
            this.lblLower.Visible = false;
            // 
            // txtStockCon
            // 
            this.txtStockCon.Location = new System.Drawing.Point(630, 234);
            this.txtStockCon.Name = "txtStockCon";
            this.txtStockCon.Size = new System.Drawing.Size(70, 20);
            this.txtStockCon.TabIndex = 15;
            this.txtStockCon.Visible = false;
            this.txtStockCon.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtStockCon_KeyPress);
            // 
            // lblDefault
            // 
            this.lblDefault.AutoSize = true;
            this.lblDefault.Location = new System.Drawing.Point(512, 237);
            this.lblDefault.Name = "lblDefault";
            this.lblDefault.Size = new System.Drawing.Size(13, 13);
            this.lblDefault.TabIndex = 159;
            this.lblDefault.Text = "1";
            this.lblDefault.Visible = false;
            // 
            // lblPur
            // 
            this.lblPur.AutoSize = true;
            this.lblPur.Location = new System.Drawing.Point(711, 289);
            this.lblPur.Name = "lblPur";
            this.lblPur.Size = new System.Drawing.Size(12, 13);
            this.lblPur.TabIndex = 158;
            this.lblPur.Text = "/";
            // 
            // txtPurRate
            // 
            this.txtPurRate.Location = new System.Drawing.Point(630, 286);
            this.txtPurRate.Name = "txtPurRate";
            this.txtPurRate.Size = new System.Drawing.Size(70, 20);
            this.txtPurRate.TabIndex = 18;
            this.txtPurRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPurRate_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(494, 289);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(78, 13);
            this.label15.TabIndex = 156;
            this.label15.Text = "Purchase Rate";
            // 
            // cmbDefaultUOM
            // 
            this.cmbDefaultUOM.FormattingEnabled = true;
            this.cmbDefaultUOM.Location = new System.Drawing.Point(630, 180);
            this.cmbDefaultUOM.Name = "cmbDefaultUOM";
            this.cmbDefaultUOM.Size = new System.Drawing.Size(93, 21);
            this.cmbDefaultUOM.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(494, 155);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 13);
            this.label4.TabIndex = 55;
            this.label4.Text = "Active Status :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(494, 183);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(69, 13);
            this.label14.TabIndex = 154;
            this.label14.Text = "Default UOM";
            // 
            // chkActive
            // 
            this.chkActive.Checked = true;
            this.chkActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkActive.Location = new System.Drawing.Point(630, 155);
            this.chkActive.Margin = new System.Windows.Forms.Padding(0);
            this.chkActive.Name = "chkActive";
            this.chkActive.Size = new System.Drawing.Size(44, 17);
            this.chkActive.TabIndex = 19;
            this.chkActive.Text = "Yes";
            this.chkActive.UseVisualStyleBackColor = true;
            this.chkActive.CheckedChanged += new System.EventHandler(this.chkActive_CheckedChanged);
            this.chkActive.KeyDown += new System.Windows.Forms.KeyEventHandler(this.chkActive_KeyDown);
            // 
            // cmbLowerUOM
            // 
            this.cmbLowerUOM.FormattingEnabled = true;
            this.cmbLowerUOM.Location = new System.Drawing.Point(630, 207);
            this.cmbLowerUOM.Name = "cmbLowerUOM";
            this.cmbLowerUOM.Size = new System.Drawing.Size(93, 21);
            this.cmbLowerUOM.TabIndex = 14;
            this.cmbLowerUOM.Leave += new System.EventHandler(this.cmbLowerUOM_Leave);
            this.cmbLowerUOM.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbLowerUOM_KeyDown);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(494, 210);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 13);
            this.label13.TabIndex = 152;
            this.label13.Text = "Lower UOM";
            // 
            // gvRateSetting
            // 
            this.gvRateSetting.AllowUserToAddRows = false;
            this.gvRateSetting.AllowUserToDeleteRows = false;
            this.gvRateSetting.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvRateSetting.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SrNo,
            this.Date,
            this.BarCode,
            this.MRP,
            this.UOMName,
            this.ASaleRate,
            this.BSaleRate,
            this.CSaleRate,
            this.DSaleRate,
            this.ESaleRate,
            this.MKTQty,
            this.PurRate,
            this.RateVariation,
            this.Chk,
            this.StockConversion,
            this.PkSrNo,
            this.BarCodeNo,
            this.IsActive,
            this.Hid});
            this.gvRateSetting.Location = new System.Drawing.Point(18, 190);
            this.gvRateSetting.Name = "gvRateSetting";
            this.gvRateSetting.Size = new System.Drawing.Size(443, 96);
            this.gvRateSetting.TabIndex = 12;
            this.gvRateSetting.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.gvRateSetting_CellValueChanged);
            this.gvRateSetting.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.gvRateSetting_CellFormatting);
            this.gvRateSetting.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.gvRateSetting_CellEndEdit);
            this.gvRateSetting.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gvRateSetting_KeyDown);
            // 
            // SrNo
            // 
            this.SrNo.DataPropertyName = "SrNo";
            this.SrNo.HeaderText = "Sr";
            this.SrNo.Name = "SrNo";
            this.SrNo.ReadOnly = true;
            this.SrNo.Visible = false;
            this.SrNo.Width = 40;
            // 
            // Date
            // 
            this.Date.DataPropertyName = "FromDate";
            dataGridViewCellStyle1.NullValue = null;
            this.Date.DefaultCellStyle = dataGridViewCellStyle1;
            this.Date.HeaderText = "Date";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            this.Date.Visible = false;
            this.Date.Width = 80;
            // 
            // BarCode
            // 
            this.BarCode.DataPropertyName = "Barcode";
            this.BarCode.HeaderText = "BarCode";
            this.BarCode.Name = "BarCode";
            this.BarCode.ReadOnly = true;
            this.BarCode.Visible = false;
            this.BarCode.Width = 120;
            // 
            // MRP
            // 
            this.MRP.DataPropertyName = "MRP";
            this.MRP.HeaderText = "MRP";
            this.MRP.Name = "MRP";
            this.MRP.ReadOnly = true;
            this.MRP.Visible = false;
            // 
            // UOMName
            // 
            this.UOMName.DataPropertyName = "UOMName";
            this.UOMName.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.UOMName.HeaderText = "UOM";
            this.UOMName.Name = "UOMName";
            this.UOMName.Width = 80;
            // 
            // ASaleRate
            // 
            this.ASaleRate.DataPropertyName = "ASaleRate";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ASaleRate.DefaultCellStyle = dataGridViewCellStyle2;
            this.ASaleRate.HeaderText = "ASaleRate";
            this.ASaleRate.Name = "ASaleRate";
            this.ASaleRate.Width = 80;
            // 
            // BSaleRate
            // 
            this.BSaleRate.DataPropertyName = "BSaleRate";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.BSaleRate.DefaultCellStyle = dataGridViewCellStyle3;
            this.BSaleRate.HeaderText = "BSaleRate";
            this.BSaleRate.Name = "BSaleRate";
            this.BSaleRate.Width = 80;
            // 
            // CSaleRate
            // 
            this.CSaleRate.DataPropertyName = "CSaleRate";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.CSaleRate.DefaultCellStyle = dataGridViewCellStyle4;
            this.CSaleRate.HeaderText = "CSaleRate";
            this.CSaleRate.Name = "CSaleRate";
            this.CSaleRate.Width = 80;
            // 
            // DSaleRate
            // 
            this.DSaleRate.DataPropertyName = "DSaleRate";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DSaleRate.DefaultCellStyle = dataGridViewCellStyle5;
            this.DSaleRate.HeaderText = "DSaleRate";
            this.DSaleRate.Name = "DSaleRate";
            this.DSaleRate.Width = 80;
            // 
            // ESaleRate
            // 
            this.ESaleRate.DataPropertyName = "ESaleRate";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ESaleRate.DefaultCellStyle = dataGridViewCellStyle6;
            this.ESaleRate.HeaderText = "ESaleRate";
            this.ESaleRate.Name = "ESaleRate";
            this.ESaleRate.Width = 80;
            // 
            // MKTQty
            // 
            this.MKTQty.DataPropertyName = "MKTQty";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.MKTQty.DefaultCellStyle = dataGridViewCellStyle7;
            this.MKTQty.HeaderText = "MKTQty";
            this.MKTQty.Name = "MKTQty";
            this.MKTQty.Width = 60;
            // 
            // PurRate
            // 
            this.PurRate.DataPropertyName = "PurRate";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PurRate.DefaultCellStyle = dataGridViewCellStyle8;
            this.PurRate.HeaderText = "PurRate";
            this.PurRate.Name = "PurRate";
            this.PurRate.ReadOnly = true;
            this.PurRate.Visible = false;
            this.PurRate.Width = 80;
            // 
            // RateVariation
            // 
            this.RateVariation.DataPropertyName = "PerOfRateVariation";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.RateVariation.DefaultCellStyle = dataGridViewCellStyle9;
            this.RateVariation.HeaderText = "Rate Variation";
            this.RateVariation.Name = "RateVariation";
            this.RateVariation.Visible = false;
            this.RateVariation.Width = 50;
            // 
            // Chk
            // 
            this.Chk.DataPropertyName = "Chk";
            this.Chk.HeaderText = ".";
            this.Chk.Name = "Chk";
            this.Chk.ReadOnly = true;
            this.Chk.Visible = false;
            this.Chk.Width = 50;
            // 
            // StockConversion
            // 
            this.StockConversion.DataPropertyName = "StockConversion";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.StockConversion.DefaultCellStyle = dataGridViewCellStyle10;
            this.StockConversion.HeaderText = "Stock Conversion";
            this.StockConversion.Name = "StockConversion";
            this.StockConversion.ReadOnly = true;
            this.StockConversion.Visible = false;
            // 
            // PkSrNo
            // 
            this.PkSrNo.DataPropertyName = "PkSrNo";
            this.PkSrNo.HeaderText = "PkSrNo";
            this.PkSrNo.Name = "PkSrNo";
            this.PkSrNo.ReadOnly = true;
            this.PkSrNo.Visible = false;
            this.PkSrNo.Width = 20;
            // 
            // BarCodeNo
            // 
            this.BarCodeNo.DataPropertyName = "FkBcdSrNo";
            this.BarCodeNo.HeaderText = "BarCodeNo";
            this.BarCodeNo.Name = "BarCodeNo";
            this.BarCodeNo.ReadOnly = true;
            this.BarCodeNo.Visible = false;
            // 
            // IsActive
            // 
            this.IsActive.DataPropertyName = "IsActive";
            this.IsActive.HeaderText = "On/Off";
            this.IsActive.Name = "IsActive";
            this.IsActive.Width = 50;
            // 
            // Hid
            // 
            this.Hid.DataPropertyName = "HidChk";
            this.Hid.HeaderText = "Hid";
            this.Hid.Name = "Hid";
            this.Hid.Visible = false;
            // 
            // txtMRP
            // 
            this.txtMRP.Location = new System.Drawing.Point(161, 294);
            this.txtMRP.Name = "txtMRP";
            this.txtMRP.Size = new System.Drawing.Size(70, 20);
            this.txtMRP.TabIndex = 17;
            this.txtMRP.Leave += new System.EventHandler(this.txtMRP_Leave);
            this.txtMRP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMRP_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 297);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(31, 13);
            this.label12.TabIndex = 71;
            this.label12.Text = "MRP";
            // 
            // txtBarcobe
            // 
            this.txtBarcobe.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBarcobe.Location = new System.Drawing.Point(152, 7);
            this.txtBarcobe.MaxLength = 50;
            this.txtBarcobe.Name = "txtBarcobe";
            this.txtBarcobe.Size = new System.Drawing.Size(300, 20);
            this.txtBarcobe.TabIndex = 0;
            this.txtBarcobe.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarcobe_KeyDown);
            this.txtBarcobe.Leave += new System.EventHandler(this.txtBarcobe_Leave);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(17, 10);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 13);
            this.label11.TabIndex = 69;
            this.label11.Text = "Barcode";
            // 
            // cmbCategoryName
            // 
            this.cmbCategoryName.FormattingEnabled = true;
            this.cmbCategoryName.Location = new System.Drawing.Point(693, 106);
            this.cmbCategoryName.MaxLength = 25;
            this.cmbCategoryName.Name = "cmbCategoryName";
            this.cmbCategoryName.Size = new System.Drawing.Size(300, 21);
            this.cmbCategoryName.TabIndex = 7;
            this.cmbCategoryName.Visible = false;
            this.cmbCategoryName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbCategoryName_KeyDown);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(557, 109);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 13);
            this.label9.TabIndex = 63;
            this.label9.Text = "Category Name  :";
            this.label9.Visible = false;
            // 
            // cmbDepartmentName
            // 
            this.cmbDepartmentName.FormattingEnabled = true;
            this.cmbDepartmentName.Location = new System.Drawing.Point(693, 84);
            this.cmbDepartmentName.MaxLength = 25;
            this.cmbDepartmentName.Name = "cmbDepartmentName";
            this.cmbDepartmentName.Size = new System.Drawing.Size(300, 21);
            this.cmbDepartmentName.TabIndex = 8;
            this.cmbDepartmentName.Visible = false;
            this.cmbDepartmentName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbDepartmentName_KeyDown);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(547, 88);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(99, 13);
            this.label8.TabIndex = 61;
            this.label8.Text = "Department Name :";
            this.label8.Visible = false;
            // 
            // cmbGroupNo2
            // 
            this.cmbGroupNo2.FormattingEnabled = true;
            this.cmbGroupNo2.Location = new System.Drawing.Point(152, 58);
            this.cmbGroupNo2.MaxLength = 25;
            this.cmbGroupNo2.Name = "cmbGroupNo2";
            this.cmbGroupNo2.Size = new System.Drawing.Size(300, 21);
            this.cmbGroupNo2.TabIndex = 1;
            this.cmbGroupNo2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbGroupNo2_KeyDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 62);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 59;
            this.label7.Text = "Category :";
            // 
            // cmbCompanyName
            // 
            this.cmbCompanyName.FormattingEnabled = true;
            this.cmbCompanyName.Location = new System.Drawing.Point(151, 137);
            this.cmbCompanyName.MaxLength = 25;
            this.cmbCompanyName.Name = "cmbCompanyName";
            this.cmbCompanyName.Size = new System.Drawing.Size(301, 21);
            this.cmbCompanyName.TabIndex = 9;
            this.cmbCompanyName.Leave += new System.EventHandler(this.cmbCompanyName_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 140);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 13);
            this.label6.TabIndex = 57;
            this.label6.Text = "Company Name :";
            // 
            // cmbGroupNo1
            // 
            this.cmbGroupNo1.FormattingEnabled = true;
            this.cmbGroupNo1.Location = new System.Drawing.Point(152, 84);
            this.cmbGroupNo1.MaxLength = 25;
            this.cmbGroupNo1.Name = "cmbGroupNo1";
            this.cmbGroupNo1.Size = new System.Drawing.Size(300, 21);
            this.cmbGroupNo1.TabIndex = 3;
            this.cmbGroupNo1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbGroupNo1_KeyDown);
            // 
            // txtItemName
            // 
            this.txtItemName.Location = new System.Drawing.Point(152, 111);
            this.txtItemName.MaxLength = 50;
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.Size = new System.Drawing.Size(300, 20);
            this.txtItemName.TabIndex = 5;
            this.txtItemName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtItemName_KeyDown);
            this.txtItemName.Leave += new System.EventHandler(this.txtItemName_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 53;
            this.label2.Text = "Brand Name :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(546, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 52;
            this.label5.Text = "Language Desc :";
            this.label5.Visible = false;
            // 
            // txtShortName
            // 
            this.txtShortName.Location = new System.Drawing.Point(693, 59);
            this.txtShortName.MaxLength = 50;
            this.txtShortName.Name = "txtShortName";
            this.txtShortName.Size = new System.Drawing.Size(300, 20);
            this.txtShortName.TabIndex = 6;
            this.txtShortName.Visible = false;
            this.txtShortName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtShortName_KeyDown);
            // 
            // txtSearchBarCode
            // 
            this.txtSearchBarCode.Location = new System.Drawing.Point(573, 351);
            this.txtSearchBarCode.Name = "txtSearchBarCode";
            this.txtSearchBarCode.Size = new System.Drawing.Size(159, 20);
            this.txtSearchBarCode.TabIndex = 10003;
            this.txtSearchBarCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearchBarCode_KeyDown);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(90, 408);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(80, 60);
            this.btnUpdate.TabIndex = 24;
            this.btnUpdate.Text = "&Update";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(444, 354);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(109, 13);
            this.label23.TabIndex = 10002;
            this.label23.Text = "BarCode Search(F7) :";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(90, 408);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 60);
            this.btnCancel.TabIndex = 24;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(7, 408);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(80, 60);
            this.BtnSave.TabIndex = 23;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(7, 408);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(80, 60);
            this.btnNew.TabIndex = 23;
            this.btnNew.Text = "&New";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.VisibleChanged += new System.EventHandler(this.btnNew_VisibleChanged);
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(173, 408);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(80, 60);
            this.btnSearch.TabIndex = 25;
            this.btnSearch.Text = "Searc&h";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(256, 408);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(80, 60);
            this.btnDelete.TabIndex = 26;
            this.btnDelete.Text = "&Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btndelete_Click);
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(650, 434);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(37, 27);
            this.btnNext.TabIndex = 30;
            this.btnNext.Text = ">";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // BtnExit
            // 
            this.BtnExit.Location = new System.Drawing.Point(339, 408);
            this.BtnExit.Name = "BtnExit";
            this.BtnExit.Size = new System.Drawing.Size(80, 60);
            this.BtnExit.TabIndex = 27;
            this.BtnExit.Text = "Exit";
            this.BtnExit.UseVisualStyleBackColor = true;
            this.BtnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.Location = new System.Drawing.Point(610, 434);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(37, 27);
            this.btnPrev.TabIndex = 29;
            this.btnPrev.Text = "<";
            this.btnPrev.UseVisualStyleBackColor = true;
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // btnFirst
            // 
            this.btnFirst.Location = new System.Drawing.Point(570, 434);
            this.btnFirst.Name = "btnFirst";
            this.btnFirst.Size = new System.Drawing.Size(37, 27);
            this.btnFirst.TabIndex = 28;
            this.btnFirst.Text = "|<";
            this.btnFirst.UseVisualStyleBackColor = true;
            this.btnFirst.Click += new System.EventHandler(this.btnFirst_Click);
            // 
            // btnLast
            // 
            this.btnLast.Location = new System.Drawing.Point(690, 434);
            this.btnLast.Name = "btnLast";
            this.btnLast.Size = new System.Drawing.Size(37, 27);
            this.btnLast.TabIndex = 31;
            this.btnLast.Text = ">|";
            this.btnLast.UseVisualStyleBackColor = true;
            this.btnLast.Click += new System.EventHandler(this.btnLast_Click);
            // 
            // StockItemAE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 523);
            this.Controls.Add(this.pnlMain);
            this.Name = "StockItemAE";
            this.Text = "Item Master";
            this.Load += new System.EventHandler(this.StockItemAE_Load);
            this.Activated += new System.EventHandler(this.StockItemAE_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.StockItemAE_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.StockItemAE_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.EP)).EndInit();
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvRateSetting)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox chkActive;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtItemName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtShortName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ErrorProvider EP;
        private JitControls.OMBPanel panel1;
        private System.Windows.Forms.ComboBox cmbGroupNo1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.Button btnLast;
        private System.Windows.Forms.Button btnFirst;
        private System.Windows.Forms.Button BtnExit;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.ComboBox cmbCompanyName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbDepartmentName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbGroupNo2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbCategoryName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtBarcobe;
        private System.Windows.Forms.TextBox txtMRP;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridView gvRateSetting;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cmbDefaultUOM;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cmbLowerUOM;
        private System.Windows.Forms.TextBox txtPurRate;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblPur;
        private System.Windows.Forms.Label lblDefault;
        private System.Windows.Forms.Label lblLower;
        private System.Windows.Forms.TextBox txtStockCon;
        private System.Windows.Forms.TextBox txtStockConOther;
        private System.Windows.Forms.Label lblDefaultOther;
        private System.Windows.Forms.Label lblLowerOther;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtLanguage;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbStockLocationName;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtReOrderLevelQty;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtMaxLevelQty;
        private System.Windows.Forms.TextBox txtSearchBarCode;
        private System.Windows.Forms.Label label23;
        private JitControls.OMBPanel pnlMain;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox cmbVatSales;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ComboBox cmbCSTSales;
        private System.Windows.Forms.ComboBox cmbVatPurchase;
        private System.Windows.Forms.DataGridViewTextBoxColumn SrNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn BarCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn MRP;
        private System.Windows.Forms.DataGridViewComboBoxColumn UOMName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ASaleRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn BSaleRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn CSaleRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn DSaleRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ESaleRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn MKTQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn PurRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn RateVariation;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Chk;
        private System.Windows.Forms.DataGridViewTextBoxColumn StockConversion;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkSrNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn BarCodeNo;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsActive;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Hid;
        private System.Windows.Forms.ComboBox cmbStockDept;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btnNewBrand;
        private System.Windows.Forms.Label lblStar2;
        private System.Windows.Forms.Label lblStar10;
        private System.Windows.Forms.Label lblStar9;
        private System.Windows.Forms.Label lblStar6;
        private System.Windows.Forms.Label lblStar4;
        private System.Windows.Forms.Label lblStar3;
        private System.Windows.Forms.Label lblStar7;
        private System.Windows.Forms.Label lblStar8;
        private System.Windows.Forms.Label lblStar1;
        private System.Windows.Forms.Label lblStar5;
    }
}