﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Master
{
    /// <summary>
    /// This class is used for User AE
    /// </summary>
    public partial class UserAE : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();
        DBMUser dbUser = new DBMUser();
        DBMUser user = new DBMUser();
        Security secure = new Security();
        MUser mUser = new MUser();
        MUserMenuMaster mUserMenu = new MUserMenuMaster();
       
        static string UserNm;
        
        bool NodeCheckFlag = true;
        string[] strNode; int cntRow = 0, cntCol = 0;

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public UserAE()
        {
            InitializeComponent();
            
        }

        private void UserAE_Load(object sender, EventArgs e)
        {
            try
            {
                //TxtUserCode.Text = ObjQry.ReturnString("Select Isnull(Max(UserCode) , 0 ) + 1 from MUser", CommonFunctions.ConStr);
                ObjFunction.FillCombo(cmbCityName, "Select CityNo,CityName From MCity Where IsActive='true'");
                ObjFunction.FillCombo(cmbAccYear, "SELECT     AccYearNo, AccYearName FROM MAccYear");
                ObjFunction.FillCombo(cmbCompanyName, "SELECT CompanyNo, CompanyName FROM MCompany");
                ObjFunction.FillCombo(cmbLocationName, "SELECT LocationNo, LocationName FROM MLocation Where IsActive='true'");
                if (User.RequestUserNo != 0)
                {
                    UserNm = "";
                    FillControls(User.RequestUserNo);
                    if (DBGetVal.UserID != 1)
                    {
                        TVW.Enabled = false;
                        btnClear.Visible = false;
                        rdAdmin.Enabled = false; rdUser.Enabled = false; //rdAgent.Enabled = false;
                        chkIsClose.Enabled = false;
                    }
                    DataTable dtTree = ObjFunction.GetDataView("SELECT * FROM MUserMenuMaster where FKUserId =" + User.RequestUserNo + "").Table;
                    strNode = new string[dtTree.Columns.Count - 4];
                    for (int i = 0; i < dtTree.Columns.Count - 4; i++)
                    {
                        strNode[i] = secure.psDecrypt(dtTree.Rows[0].ItemArray[i + 2].ToString());
                    }
                    DisplayNodes(new TreeNode(), 0);
                }
                else
                {
                    if (DBGetVal.UserID > 1)
                    {
                        TVW.Enabled = false;
                        btnClear.Visible = false;
                        rdAdmin.Enabled = false; rdUser.Enabled = false; //rdAgent.Enabled = false;
                        chkIsClose.Enabled = false;

                        UserNm = "";
                        FillControls(DBGetVal.UserID);

                        DataTable dtTree = ObjFunction.GetDataView("SELECT * FROM MUserMenuMaster where FKUserId =" + DBGetVal.UserID + "").Table;
                        strNode = new string[dtTree.Columns.Count - 4];
                        for (int i = 0; i < dtTree.Columns.Count - 4; i++)
                        {
                            strNode[i] = secure.psDecrypt(dtTree.Rows[0].ItemArray[i + 2].ToString());
                        }
                        DisplayNodes(new TreeNode(), 0);
                    }
                    else
                        InitNodes(new TreeNode(), 0);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillControls(long ID)
        {
            try
            {
                MUser MF = new MUser();
                MF = dbUser.ModifyMUserByID(ID);
                TxtUserCode.Text = MF.UsersUserCode;
                UserNm = MF.UserName;
                cmbAccYear.SelectedValue = MF.FkAccYearNo.ToString();
                cmbCompanyName.SelectedValue = MF.FkCompanyNo.ToString();
                cmbLocationName.SelectedValue = MF.FkLocationNo.ToString();
                TxtUserName.Text = MF.UserName;
                TxtPassword.Text = secure.psDecrypt(MF.Password);
                txtAdd.Text = MF.UserAddress;
                txtPhoneNo.Text = MF.PhoneNo;
                cmbCityName.SelectedValue = MF.CityCode.ToString();
                chkIsClose.Checked = (MF.IsClose == 0) ? true : false;
                if (MF.UserType == 1)
                {
                    rdAdmin.Checked = true;
                    chkIsClose.Checked = false; chkIsClose.Enabled = false;
                    TxtUserName.ReadOnly = true;
                }
                //else if (MF.UserType == 2)
                //    rdAgent.Checked = true;
                else if (MF.UserType == 3)
                    rdUser.Checked = true;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (Validations() == true)
                {

                    mUser.UserCode = User.RequestUserNo;
                    mUser.UsersUserCode = TxtUserCode.Text;
                    mUser.UserName = (TxtUserName.Text.Trim());
                    mUser.Password = secure.psEncrypt(TxtPassword.Text.Trim());
                    mUser.UserAddress = txtAdd.Text;
                    mUser.PhoneNo = txtPhoneNo.Text;
                    mUser.FkAccYearNo = ObjFunction.GetComboValue(cmbAccYear);
                    mUser.FkCompanyNo = ObjFunction.GetComboValue(cmbCompanyName);
                    mUser.FkLocationNo = ObjFunction.GetComboValue(cmbLocationName);
                    mUser.CityCode = ObjFunction.GetComboValue(cmbCityName);
                    mUser.IsClose = (chkIsClose.Checked == true) ? 1 : 0;
                    if (rdAdmin.Checked == true) mUser.UserType = 1;
                    //else if (rdAgent.Checked == true) mUser.UserType = 2;
                    else if (rdUser.Checked == true) mUser.UserType = 3;

                    if (dbUser.AddMUser(mUser, 2) == true)
                    {
                        string[] str = SaveNodes();
                        mUserMenu.PKSrNo = 0;

                        for (int i = 0; i < str.Length; i++)
                        {
                            switch (i)
                            {
                                case 0: mUserMenu.Master = secure.psEncrypt(str[i].ToString()); break;
                                case 1: mUserMenu.Sales = secure.psEncrypt(str[i].ToString()); break;
                                case 2: mUserMenu.Purchase = secure.psEncrypt(str[i].ToString()); break;
                                case 3: mUserMenu.Accounts = secure.psEncrypt(str[i].ToString()); break;
                                case 4: mUserMenu.Reports = secure.psEncrypt(str[i].ToString()); break;
                                case 5: mUserMenu.Settings = secure.psEncrypt(str[i].ToString()); break;
                                case 6: mUserMenu.Utilities = secure.psEncrypt(str[i].ToString()); break;
                                case 7: mUserMenu.Hidden = secure.psEncrypt(str[i].ToString()); break;
                            }
                        }
                        dbUser.AddMUserMenuMaster(mUserMenu);
                        if (dbUser.ExecuteNonQueryStatements() == true)
                        {
                            OMMessageBox.Show("User Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);

                            this.Close();
                            Form childForm = new Master.User();
                            childForm.Text = "User Creation";
                            ObjFunction.OpenForm(childForm, DBGetVal.MainForm);
                        }
                        else
                        {
                            OMMessageBox.Show("User not saved", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            UserNm = ""; User.RequestUserNo = 0;
            this.Close();
            Form NewF = new User();

            NewF.Text = "User Creation";
            ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
        }

        private bool Validations()
        {
            bool flag = false;
            EP.SetError(TxtUserName, ""); EP.SetError(TxtPassword, "");

            if (TxtUserName.Text == "")
            {
                EP.SetError(TxtUserName, "Enter User Name");
                EP.SetIconAlignment(TxtUserName, ErrorIconAlignment.MiddleRight);
                TxtUserName.Focus();
            }
            else if (ObjFunction.GetComboValue(cmbAccYear) <= 0)
            {
                EP.SetError(cmbAccYear, "Select AccYear");
                EP.SetIconAlignment(cmbAccYear, ErrorIconAlignment.MiddleRight);
                cmbAccYear.Focus();
            }
            else if (ObjFunction.GetComboValue(cmbCompanyName) <= 0)
            {
                EP.SetError(cmbCompanyName, "Select Company");
                EP.SetIconAlignment(cmbCompanyName, ErrorIconAlignment.MiddleRight);
                cmbCompanyName.Focus();
            }
            else if (ObjFunction.GetComboValue(cmbLocationName) <= 0)
            {
                EP.SetError(cmbLocationName, "Select Location");
                EP.SetIconAlignment(cmbLocationName, ErrorIconAlignment.MiddleRight);
                cmbLocationName.Focus();
            }
            else if (TxtPassword.Text == "")
            {
                EP.SetError(TxtPassword, "Enter Password");
                EP.SetIconAlignment(TxtPassword, ErrorIconAlignment.MiddleRight);
                TxtPassword.Focus();
            }
            //else if (rdAgent.Checked == true)
            //{
            //    if (cc.IsInteger(TxtPassword) == false)
            //    {
            //        EP.SetError(TxtPassword, "Pleas Enter Password in numeric");
            //        EP.SetIconAlignment(TxtPassword, ErrorIconAlignment.MiddleRight);
            //        TxtPassword.Focus();
            //    }
            //    else
            //        flag = true;
            //}
            else
                flag = true;

            return flag;
        }

        private void UserAE_FormClosing(object sender, FormClosingEventArgs e)
        {
            UserNm = ""; User.RequestUserNo = 0;
        }

        //public void getNodes(long Srno)
        //{
        //    TreeNode TD = new TreeNode(); TreeNode tdTemp;
        //    DataTable dtTree = ObjFunction.GetDataView("Select SrNo,MenuID,MenuName From MMenuMaster Where ControlMenu=" + Srno + "").Table;
        //    if (dtTree.Rows.Count > 0)
        //    {
        //        for (int i = 0; i < dtTree.Rows.Count; i++)
        //        {
        //          TD = new TreeNode(dtTree.Rows[i].ItemArray[2].ToString());
        //          TD = getNodes1(TD, Convert.ToInt64(dtTree.Rows[i].ItemArray[0].ToString()));
        //          TVW.Nodes.Add(TD);
        //        }
        //    }
        //    //return TD;
        //}

        private TreeNode InitNodes(TreeNode TD, long Srno)
        {
            try
            {
                TreeNode tdtemp;
                DataTable dtTree = dbUser.GetNodesByNodeID(Srno).Table;// ObjFunction.GetDataView("Select SrNo,MenuID,MenuName From MMenuMaster Where ControlMenu=" + Srno + "").Table;
                if (dtTree.Rows.Count > 0)
                {
                    for (int i = 0; i < dtTree.Rows.Count; i++)
                    {
                        tdtemp = new TreeNode(dtTree.Rows[i].ItemArray[2].ToString());
                        tdtemp = InitNodes(tdtemp, Convert.ToInt64(dtTree.Rows[i].ItemArray[0].ToString()));
                        if (Srno == 0)
                            TVW.Nodes.Add(tdtemp);
                        else
                            TD.Nodes.Add(tdtemp);
                    }
                }
               
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
            return TD;
        }

        private TreeNode DisplayNodes(TreeNode TD, long Srno)
        {
            try
            {
                TreeNode tdtemp;
                DataTable dtTree = dbUser.GetNodesByNodeID(Srno).Table;// ObjFunction.GetDataView("Select SrNo,MenuID,MenuName From MMenuMaster Where ControlMenu=" + Srno + "").Table;
                if (dtTree.Rows.Count > 0)
                {
                    for (int i = 0; i < dtTree.Rows.Count; i++)
                    {
                        tdtemp = new TreeNode(dtTree.Rows[i].ItemArray[2].ToString());
                        if (Srno == 0)
                        {
                            cntCol = 0;
                            if (cntCol < strNode[cntRow].Length)
                                if (strNode[cntRow][cntCol].ToString() == "1") tdtemp.Checked = true;
                            cntCol++;
                        }
                        else
                        {
                            if (cntCol < strNode[cntRow].Length)
                                if (strNode[cntRow][cntCol].ToString() == "1") tdtemp.Checked = true;
                            cntCol++;
                        }

                        tdtemp = DisplayNodes(tdtemp, Convert.ToInt64(dtTree.Rows[i].ItemArray[0].ToString()));
                        if (Srno == 0)
                        { TVW.Nodes.Add(tdtemp); cntRow++; }
                        else
                            TD.Nodes.Add(tdtemp);

                    }
                }
               
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
            return TD;
        }

        private string[] SaveNodes()
        {
            string[] strMain = new string[TVW.Nodes.Count];
            try
            {               
                string str = "";
                int cntNode = 0;
                foreach (TreeNode td in TVW.Nodes)
                {
                    if (td.Checked == true) str = "1"; else str = "0";
                    str = SaveChildNodes(td, str);
                    strMain[cntNode] = str;
                    cntNode++;
                }              
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
            return strMain;
        }

        private string SaveChildNodes(TreeNode TD, string str)
        {
            try
            {
                foreach (TreeNode td in TD.Nodes)
                {
                    if (td.Checked == true) str += "1"; else str += "0";
                    str = SaveChildNodes(td, str);
                }                
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
            return str;
        }

        private void TVW_AfterCheck(object sender, TreeViewEventArgs e)
        {
            try
            {
                if (NodeCheckFlag == true)
                    CheckNodes(e.Node.Nodes, e.Node.Checked);
                NodeCheckFlag = false;

                if (((TreeNode)(e.Node.Parent)) != null)
                    CheckParentNodes(((TreeNode)(e.Node.Parent)));
                if (e.Node.Checked == true)
                    e.Node.Expand();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void CheckParentNodes(TreeNode td)
        {
            bool flag = false;
            foreach (TreeNode nd in td.Nodes)
            {
                if (nd.Checked == true)
                {
                    flag = true;
                    break;
                }
            }
            td.Checked = flag;
        }

        private void CheckNodes(TreeNodeCollection tdc, bool flag)
        {
            foreach (TreeNode nd in tdc)
            {
                nd.Checked = flag;
                CheckNodes(nd.Nodes, flag);
            }
        }

        private void TVW_MouseClick(object sender, MouseEventArgs e)
        {
            NodeCheckFlag = true;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            CheckNodes(TVW.Nodes, false);
            NodeCheckFlag = true;
        }

        private void TxtUserName_Leave(object sender, EventArgs e)
        {
            try
            {
                EP.SetError(TxtUserName, "");
                if (TxtUserName.Text != "")
                {
                    if (UserNm.ToUpper() != TxtUserName.Text.ToUpper())
                    {
                        if (ObjQry.ReturnInteger("Select Count(*) from MUser Where UserName = '" + TxtUserName.Text.Replace("'", "''") + "'", CommonFunctions.ConStr) != 0)
                        {
                            EP.SetError(TxtUserName, "Enter User Name");
                            EP.SetIconAlignment(TxtUserName, ErrorIconAlignment.MiddleRight);
                            TxtUserName.Focus();
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void TxtUserName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtAdd.GotFocus += delegate { txtAdd.Select(txtAdd.Text.Length, txtAdd.Text.Length); };
            }
        }

        private void cmbCityName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (chkIsClose.Enabled == false)
                {
                    TVW.Focus();
                }
                else
                {
                    chkIsClose.Focus();
                }
            }
        }

        private void txtPhoneNo_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMaskedNumeric(txtPhoneNo);
            
        }

       

    }
}
