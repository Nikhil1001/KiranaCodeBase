﻿namespace Kirana.Master
{
    partial class ManufacturerDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMain = new JitControls.OMBPanel();
            this.lblChkHelp1 = new System.Windows.Forms.Label();
            this.lblMFG = new System.Windows.Forms.Label();
            this.lblSuplierCount = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblChkHelp = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.dgMF = new System.Windows.Forms.DataGridView();
            this.dgSupplier = new System.Windows.Forms.DataGridView();
            this.LedgerNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SupplierList = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlSearch = new System.Windows.Forms.Panel();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.lblSearch = new System.Windows.Forms.Label();
            this.ManufacturerList = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MFNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MFLedgerNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkSrNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Chk = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.checkk = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.chkSelectAll = new System.Windows.Forms.CheckBox();
            this.pnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgSupplier)).BeginInit();
            this.pnlSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.BorderColor = System.Drawing.Color.Gray;
            this.pnlMain.BorderRadius = 3;
            this.pnlMain.Controls.Add(this.chkSelectAll);
            this.pnlMain.Controls.Add(this.lblChkHelp1);
            this.pnlMain.Controls.Add(this.lblMFG);
            this.pnlMain.Controls.Add(this.lblSuplierCount);
            this.pnlMain.Controls.Add(this.btnCancel);
            this.pnlMain.Controls.Add(this.lblChkHelp);
            this.pnlMain.Controls.Add(this.btnExit);
            this.pnlMain.Controls.Add(this.btnSave);
            this.pnlMain.Controls.Add(this.dgMF);
            this.pnlMain.Controls.Add(this.dgSupplier);
            this.pnlMain.Controls.Add(this.pnlSearch);
            this.pnlMain.Location = new System.Drawing.Point(8, 7);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(770, 518);
            this.pnlMain.TabIndex = 0;
            // 
            // lblChkHelp1
            // 
            this.lblChkHelp1.Location = new System.Drawing.Point(10, 395);
            this.lblChkHelp1.Name = "lblChkHelp1";
            this.lblChkHelp1.Size = new System.Drawing.Size(345, 23);
            this.lblChkHelp1.TabIndex = 60054;
            this.lblChkHelp1.Text = "Select Supplier - Press Enter Key";
            // 
            // lblMFG
            // 
            this.lblMFG.Location = new System.Drawing.Point(408, 439);
            this.lblMFG.Name = "lblMFG";
            this.lblMFG.Size = new System.Drawing.Size(345, 64);
            this.lblMFG.TabIndex = 60053;
            this.lblMFG.Text = "**";
            // 
            // lblSuplierCount
            // 
            this.lblSuplierCount.Location = new System.Drawing.Point(10, 421);
            this.lblSuplierCount.Name = "lblSuplierCount";
            this.lblSuplierCount.Size = new System.Drawing.Size(345, 23);
            this.lblSuplierCount.TabIndex = 60052;
            this.lblSuplierCount.Text = "**";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(99, 451);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 60);
            this.btnCancel.TabIndex = 60051;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblChkHelp
            // 
            this.lblChkHelp.Location = new System.Drawing.Point(408, 398);
            this.lblChkHelp.Name = "lblChkHelp";
            this.lblChkHelp.Size = new System.Drawing.Size(242, 29);
            this.lblChkHelp.TabIndex = 60050;
            this.lblChkHelp.Text = "Select/Deselect Manufacturer - Press SpaceBar \r\nSave - Press F4";
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(185, 451);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(80, 60);
            this.btnExit.TabIndex = 10;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(13, 451);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(80, 60);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // dgMF
            // 
            this.dgMF.AllowUserToAddRows = false;
            this.dgMF.AllowUserToDeleteRows = false;
            this.dgMF.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMF.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ManufacturerList,
            this.MFNo,
            this.MFLedgerNo,
            this.PkSrNo,
            this.Chk,
            this.checkk});
            this.dgMF.Location = new System.Drawing.Point(411, 68);
            this.dgMF.Name = "dgMF";
            this.dgMF.Size = new System.Drawing.Size(328, 324);
            this.dgMF.TabIndex = 2;
            this.dgMF.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgMF_CellDoubleClick);
            this.dgMF.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgMF_CellContentDoubleClick);
            this.dgMF.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgMF_CellFormatting);
            this.dgMF.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgMF_CellClick);
            this.dgMF.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgMF_KeyDown);
            // 
            // dgSupplier
            // 
            this.dgSupplier.AllowUserToAddRows = false;
            this.dgSupplier.AllowUserToDeleteRows = false;
            this.dgSupplier.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSupplier.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LedgerNo,
            this.SupplierList});
            this.dgSupplier.Location = new System.Drawing.Point(13, 68);
            this.dgSupplier.Name = "dgSupplier";
            this.dgSupplier.ReadOnly = true;
            this.dgSupplier.Size = new System.Drawing.Size(367, 324);
            this.dgSupplier.TabIndex = 1;
            this.dgSupplier.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgSupplier_CellDoubleClick);
            this.dgSupplier.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgSupplier_CellClick);
            this.dgSupplier.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgSupplier_KeyDown);
            // 
            // LedgerNo
            // 
            this.LedgerNo.DataPropertyName = "LedgerNo";
            this.LedgerNo.HeaderText = "LedgerNo";
            this.LedgerNo.Name = "LedgerNo";
            this.LedgerNo.ReadOnly = true;
            this.LedgerNo.Visible = false;
            // 
            // SupplierList
            // 
            this.SupplierList.DataPropertyName = "LedgerName";
            this.SupplierList.HeaderText = "Supplier List";
            this.SupplierList.Name = "SupplierList";
            this.SupplierList.ReadOnly = true;
            this.SupplierList.Width = 330;
            // 
            // pnlSearch
            // 
            this.pnlSearch.Controls.Add(this.txtSearch);
            this.pnlSearch.Controls.Add(this.lblSearch);
            this.pnlSearch.Location = new System.Drawing.Point(11, 12);
            this.pnlSearch.Name = "pnlSearch";
            this.pnlSearch.Size = new System.Drawing.Size(731, 47);
            this.pnlSearch.TabIndex = 0;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(125, 13);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(564, 20);
            this.txtSearch.TabIndex = 1;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            // 
            // lblSearch
            // 
            this.lblSearch.AutoSize = true;
            this.lblSearch.Location = new System.Drawing.Point(10, 16);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(88, 13);
            this.lblSearch.TabIndex = 0;
            this.lblSearch.Text = "Search Supplier :";
            // 
            // ManufacturerList
            // 
            this.ManufacturerList.DataPropertyName = "MfgCompName";
            this.ManufacturerList.HeaderText = "Manufacturer List";
            this.ManufacturerList.Name = "ManufacturerList";
            this.ManufacturerList.ReadOnly = true;
            this.ManufacturerList.Width = 300;
            // 
            // MFNo
            // 
            this.MFNo.DataPropertyName = "MfgCompNo";
            this.MFNo.HeaderText = "MFNo";
            this.MFNo.Name = "MFNo";
            this.MFNo.Visible = false;
            // 
            // MFLedgerNo
            // 
            this.MFLedgerNo.DataPropertyName = "MFLedgerNo";
            this.MFLedgerNo.HeaderText = "MFLedgerNo";
            this.MFLedgerNo.Name = "MFLedgerNo";
            this.MFLedgerNo.Visible = false;
            // 
            // PkSrNo
            // 
            this.PkSrNo.DataPropertyName = "MPkSrNo";
            this.PkSrNo.HeaderText = "PkSrNo";
            this.PkSrNo.Name = "PkSrNo";
            this.PkSrNo.Visible = false;
            // 
            // Chk
            // 
            this.Chk.DataPropertyName = "Chk";
            this.Chk.HeaderText = "Chk";
            this.Chk.Name = "Chk";
            this.Chk.Visible = false;
            // 
            // checkk
            // 
            this.checkk.DataPropertyName = "checkk";
            this.checkk.HeaderText = "Select";
            this.checkk.Name = "checkk";
            this.checkk.Width = 50;
            // 
            // chkSelectAll
            // 
            this.chkSelectAll.AutoSize = true;
            this.chkSelectAll.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkSelectAll.Location = new System.Drawing.Point(661, 397);
            this.chkSelectAll.Name = "chkSelectAll";
            this.chkSelectAll.Size = new System.Drawing.Size(77, 17);
            this.chkSelectAll.TabIndex = 60055;
            this.chkSelectAll.Text = "Select (F2)";
            this.chkSelectAll.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkSelectAll.UseVisualStyleBackColor = true;
            this.chkSelectAll.CheckedChanged += new System.EventHandler(this.chkSelectAll_CheckedChanged);
            // 
            // ManufacturerDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(795, 537);
            this.Controls.Add(this.pnlMain);
            this.Name = "ManufacturerDetails";
            this.Text = "Manufacturer Details";
            this.Load += new System.EventHandler(this.ManufactuerDetails_Load);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgSupplier)).EndInit();
            this.pnlSearch.ResumeLayout(false);
            this.pnlSearch.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private JitControls.OMBPanel pnlMain;
        private System.Windows.Forms.Panel pnlSearch;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label lblSearch;
        private System.Windows.Forms.DataGridView dgSupplier;
        private System.Windows.Forms.DataGridView dgMF;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblChkHelp;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblMFG;
        private System.Windows.Forms.Label lblSuplierCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn LedgerNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn SupplierList;
        private System.Windows.Forms.Label lblChkHelp1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ManufacturerList;
        private System.Windows.Forms.DataGridViewTextBoxColumn MFNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn MFLedgerNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkSrNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Chk;
        private System.Windows.Forms.DataGridViewCheckBoxColumn checkk;
        private System.Windows.Forms.CheckBox chkSelectAll;
    }
}