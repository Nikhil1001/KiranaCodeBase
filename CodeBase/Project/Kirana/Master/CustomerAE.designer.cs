﻿namespace Kirana.Master
{
    partial class CustomerAE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomerAE));
            this.EP = new System.Windows.Forms.ErrorProvider(this.components);
            this.pnlMain = new JitControls.OMBPanel();
            this.label12 = new System.Windows.Forms.Label();
            this.txtGSTINNo = new System.Windows.Forms.TextBox();
            this.lblGSTIN = new System.Windows.Forms.Label();
            this.btnLangAdd = new System.Windows.Forms.Button();
            this.cmbOState = new System.Windows.Forms.ComboBox();
            this.txtLangAdd = new System.Windows.Forms.TextBox();
            this.btnLangLedgerName = new System.Windows.Forms.Button();
            this.txtLangLedgerName = new System.Windows.Forms.TextBox();
            this.lblConsentDate = new System.Windows.Forms.Label();
            this.pnlCustInfo = new JitControls.OMBPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.lblDeActiveCount = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblActiveCount = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblTotalCount = new System.Windows.Forms.Label();
            this.lstBldgName = new System.Windows.Forms.ListBox();
            this.lblStar3 = new System.Windows.Forms.Label();
            this.lblStar1 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.BtnExit = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrev = new System.Windows.Forms.Button();
            this.btnLast = new System.Windows.Forms.Button();
            this.btnFirst = new System.Windows.Forms.Button();
            this.txtLedgerName = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtMobileNo1 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtOAddress = new System.Windows.Forms.TextBox();
            this.BtnSave = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.dtpEnroll = new System.Windows.Forms.DateTimePicker();
            this.lblChkHelp = new System.Windows.Forms.Label();
            this.chkSendSMS = new System.Windows.Forms.CheckBox();
            this.chkEnroll = new System.Windows.Forms.CheckBox();
            this.lblStar4 = new System.Windows.Forms.Label();
            this.cmbSign = new System.Windows.Forms.ComboBox();
            this.label33 = new System.Windows.Forms.Label();
            this.btnOccNew = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rdSelf = new System.Windows.Forms.RadioButton();
            this.rdCompany = new System.Windows.Forms.RadioButton();
            this.cmbOCity = new System.Windows.Forms.ComboBox();
            this.txtOPhone1 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtEmailID = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbMale = new System.Windows.Forms.RadioButton();
            this.rbFemale = new System.Windows.Forms.RadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.lblStar2 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.txtOPhone2 = new System.Windows.Forms.TextBox();
            this.chkActive = new System.Windows.Forms.CheckBox();
            this.lblPh2 = new System.Windows.Forms.Label();
            this.lblMob2 = new System.Windows.Forms.Label();
            this.cmbYear = new System.Windows.Forms.ComboBox();
            this.txtMobileNo2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCreditLimit = new System.Windows.Forms.TextBox();
            this.txtOpBalance = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbMonth = new System.Windows.Forms.ComboBox();
            this.cmbDay = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbOccupation = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbQualification = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.l = new System.Windows.Forms.Label();
            this.txtContactPer = new System.Windows.Forms.TextBox();
            this.txtOPinCode = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.cmbOArea = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.EP)).BeginInit();
            this.pnlMain.SuspendLayout();
            this.pnlCustInfo.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // EP
            // 
            this.EP.ContainerControl = this;
            // 
            // pnlMain
            // 
            this.pnlMain.BorderColor = System.Drawing.Color.Gray;
            this.pnlMain.BorderRadius = 3;
            this.pnlMain.Controls.Add(this.cmbOArea);
            this.pnlMain.Controls.Add(this.label24);
            this.pnlMain.Controls.Add(this.label13);
            this.pnlMain.Controls.Add(this.label12);
            this.pnlMain.Controls.Add(this.txtGSTINNo);
            this.pnlMain.Controls.Add(this.cmbOCity);
            this.pnlMain.Controls.Add(this.lblGSTIN);
            this.pnlMain.Controls.Add(this.btnLangAdd);
            this.pnlMain.Controls.Add(this.cmbOState);
            this.pnlMain.Controls.Add(this.txtLangAdd);
            this.pnlMain.Controls.Add(this.btnLangLedgerName);
            this.pnlMain.Controls.Add(this.txtLangLedgerName);
            this.pnlMain.Controls.Add(this.lblConsentDate);
            this.pnlMain.Controls.Add(this.pnlCustInfo);
            this.pnlMain.Controls.Add(this.lstBldgName);
            this.pnlMain.Controls.Add(this.lblStar3);
            this.pnlMain.Controls.Add(this.lblStar1);
            this.pnlMain.Controls.Add(this.label32);
            this.pnlMain.Controls.Add(this.btnSearch);
            this.pnlMain.Controls.Add(this.btnDelete);
            this.pnlMain.Controls.Add(this.BtnExit);
            this.pnlMain.Controls.Add(this.btnNext);
            this.pnlMain.Controls.Add(this.btnPrev);
            this.pnlMain.Controls.Add(this.btnLast);
            this.pnlMain.Controls.Add(this.btnFirst);
            this.pnlMain.Controls.Add(this.txtLedgerName);
            this.pnlMain.Controls.Add(this.label23);
            this.pnlMain.Controls.Add(this.txtMobileNo1);
            this.pnlMain.Controls.Add(this.label21);
            this.pnlMain.Controls.Add(this.txtOAddress);
            this.pnlMain.Controls.Add(this.BtnSave);
            this.pnlMain.Controls.Add(this.btnNew);
            this.pnlMain.Controls.Add(this.btnCancel);
            this.pnlMain.Controls.Add(this.btnUpdate);
            resources.ApplyResources(this.pnlMain, "pnlMain");
            this.pnlMain.Name = "pnlMain";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            // 
            // txtGSTINNo
            // 
            resources.ApplyResources(this.txtGSTINNo, "txtGSTINNo");
            this.txtGSTINNo.Name = "txtGSTINNo";
            this.txtGSTINNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGSTINNo_KeyDown);
            // 
            // lblGSTIN
            // 
            resources.ApplyResources(this.lblGSTIN, "lblGSTIN");
            this.lblGSTIN.Name = "lblGSTIN";
            // 
            // btnLangAdd
            // 
            resources.ApplyResources(this.btnLangAdd, "btnLangAdd");
            this.btnLangAdd.Name = "btnLangAdd";
            this.btnLangAdd.UseVisualStyleBackColor = true;
            this.btnLangAdd.Click += new System.EventHandler(this.txtLangAdd_Leave);
            // 
            // cmbOState
            // 
            this.cmbOState.FormattingEnabled = true;
            resources.ApplyResources(this.cmbOState, "cmbOState");
            this.cmbOState.Name = "cmbOState";
            this.cmbOState.Leave += new System.EventHandler(this.cmbOState_Leave);
            this.cmbOState.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbOState_KeyDown);
            // 
            // txtLangAdd
            // 
            this.txtLangAdd.BackColor = System.Drawing.Color.White;
            this.txtLangAdd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtLangAdd, "txtLangAdd");
            this.txtLangAdd.ForeColor = System.Drawing.Color.Black;
            this.txtLangAdd.Name = "txtLangAdd";
            this.txtLangAdd.Leave += new System.EventHandler(this.txtLangAdd_Leave);
            // 
            // btnLangLedgerName
            // 
            resources.ApplyResources(this.btnLangLedgerName, "btnLangLedgerName");
            this.btnLangLedgerName.Name = "btnLangLedgerName";
            this.btnLangLedgerName.UseVisualStyleBackColor = true;
            this.btnLangLedgerName.Click += new System.EventHandler(this.btnLangLedgerName_Click);
            // 
            // txtLangLedgerName
            // 
            this.txtLangLedgerName.BackColor = System.Drawing.Color.White;
            this.txtLangLedgerName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtLangLedgerName, "txtLangLedgerName");
            this.txtLangLedgerName.ForeColor = System.Drawing.Color.Black;
            this.txtLangLedgerName.Name = "txtLangLedgerName";
            this.txtLangLedgerName.Leave += new System.EventHandler(this.txtLangLedgerName_Leave);
            // 
            // lblConsentDate
            // 
            resources.ApplyResources(this.lblConsentDate, "lblConsentDate");
            this.lblConsentDate.Name = "lblConsentDate";
            // 
            // pnlCustInfo
            // 
            this.pnlCustInfo.BorderColor = System.Drawing.Color.Gray;
            this.pnlCustInfo.BorderRadius = 3;
            this.pnlCustInfo.Controls.Add(this.label9);
            this.pnlCustInfo.Controls.Add(this.lblDeActiveCount);
            this.pnlCustInfo.Controls.Add(this.label10);
            this.pnlCustInfo.Controls.Add(this.lblActiveCount);
            this.pnlCustInfo.Controls.Add(this.label11);
            this.pnlCustInfo.Controls.Add(this.lblTotalCount);
            resources.ApplyResources(this.pnlCustInfo, "pnlCustInfo");
            this.pnlCustInfo.Name = "pnlCustInfo";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // lblDeActiveCount
            // 
            this.lblDeActiveCount.ForeColor = System.Drawing.Color.Red;
            resources.ApplyResources(this.lblDeActiveCount, "lblDeActiveCount");
            this.lblDeActiveCount.Name = "lblDeActiveCount";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // lblActiveCount
            // 
            this.lblActiveCount.ForeColor = System.Drawing.Color.Green;
            resources.ApplyResources(this.lblActiveCount, "lblActiveCount");
            this.lblActiveCount.Name = "lblActiveCount";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // lblTotalCount
            // 
            this.lblTotalCount.ForeColor = System.Drawing.Color.Blue;
            resources.ApplyResources(this.lblTotalCount, "lblTotalCount");
            this.lblTotalCount.Name = "lblTotalCount";
            // 
            // lstBldgName
            // 
            this.lstBldgName.FormattingEnabled = true;
            resources.ApplyResources(this.lstBldgName, "lstBldgName");
            this.lstBldgName.Name = "lstBldgName";
            this.lstBldgName.SelectedIndexChanged += new System.EventHandler(this.lstBldgName_SelectedIndexChanged);
            this.lstBldgName.Leave += new System.EventHandler(this.lstBldgName_Leave);
            this.lstBldgName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstBldgName_KeyDown);
            // 
            // lblStar3
            // 
            resources.ApplyResources(this.lblStar3, "lblStar3");
            this.lblStar3.Name = "lblStar3";
            // 
            // lblStar1
            // 
            resources.ApplyResources(this.lblStar1, "lblStar1");
            this.lblStar1.Name = "lblStar1";
            // 
            // label32
            // 
            resources.ApplyResources(this.label32, "label32");
            this.label32.Name = "label32";
            // 
            // btnSearch
            // 
            resources.ApplyResources(this.btnSearch, "btnSearch");
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnDelete
            // 
            resources.ApplyResources(this.btnDelete, "btnDelete");
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btndelete_Click);
            // 
            // BtnExit
            // 
            resources.ApplyResources(this.BtnExit, "BtnExit");
            this.BtnExit.Name = "BtnExit";
            this.BtnExit.UseVisualStyleBackColor = true;
            this.BtnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // btnNext
            // 
            resources.ApplyResources(this.btnNext, "btnNext");
            this.btnNext.Name = "btnNext";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrev
            // 
            resources.ApplyResources(this.btnPrev, "btnPrev");
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.UseVisualStyleBackColor = true;
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // btnLast
            // 
            resources.ApplyResources(this.btnLast, "btnLast");
            this.btnLast.Name = "btnLast";
            this.btnLast.UseVisualStyleBackColor = true;
            this.btnLast.Click += new System.EventHandler(this.btnLast_Click);
            // 
            // btnFirst
            // 
            resources.ApplyResources(this.btnFirst, "btnFirst");
            this.btnFirst.Name = "btnFirst";
            this.btnFirst.UseVisualStyleBackColor = true;
            this.btnFirst.Click += new System.EventHandler(this.btnFirst_Click);
            // 
            // txtLedgerName
            // 
            resources.ApplyResources(this.txtLedgerName, "txtLedgerName");
            this.txtLedgerName.Name = "txtLedgerName";
            this.txtLedgerName.TextChanged += new System.EventHandler(this.txtLedgerName_TextChanged);
            this.txtLedgerName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtLedgerName_KeyDown);
            this.txtLedgerName.Leave += new System.EventHandler(this.txtLedgerName_Leave);
            // 
            // label23
            // 
            resources.ApplyResources(this.label23, "label23");
            this.label23.Name = "label23";
            // 
            // txtMobileNo1
            // 
            resources.ApplyResources(this.txtMobileNo1, "txtMobileNo1");
            this.txtMobileNo1.Name = "txtMobileNo1";
            this.txtMobileNo1.TextChanged += new System.EventHandler(this.txtValid_TextChanged);
            this.txtMobileNo1.Leave += new System.EventHandler(this.txtMobileNo1_Leave);
            // 
            // label21
            // 
            resources.ApplyResources(this.label21, "label21");
            this.label21.Name = "label21";
            // 
            // txtOAddress
            // 
            resources.ApplyResources(this.txtOAddress, "txtOAddress");
            this.txtOAddress.Name = "txtOAddress";
            this.txtOAddress.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtOAddress_KeyDown);
            this.txtOAddress.Leave += new System.EventHandler(this.TxtAddress_Leave);
            // 
            // BtnSave
            // 
            resources.ApplyResources(this.BtnSave, "BtnSave");
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // btnNew
            // 
            resources.ApplyResources(this.btnNew, "btnNew");
            this.btnNew.Name = "btnNew";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnCancel
            // 
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnUpdate
            // 
            resources.ApplyResources(this.btnUpdate, "btnUpdate");
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // dtpEnroll
            // 
            this.dtpEnroll.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            resources.ApplyResources(this.dtpEnroll, "dtpEnroll");
            this.dtpEnroll.Name = "dtpEnroll";
            this.dtpEnroll.Leave += new System.EventHandler(this.dtpEnroll_Leave);
            this.dtpEnroll.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpEnroll_KeyDown);
            // 
            // lblChkHelp
            // 
            resources.ApplyResources(this.lblChkHelp, "lblChkHelp");
            this.lblChkHelp.Name = "lblChkHelp";
            // 
            // chkSendSMS
            // 
            resources.ApplyResources(this.chkSendSMS, "chkSendSMS");
            this.chkSendSMS.Name = "chkSendSMS";
            this.chkSendSMS.UseVisualStyleBackColor = true;
            this.chkSendSMS.KeyDown += new System.Windows.Forms.KeyEventHandler(this.chkSendSMS_KeyDown);
            // 
            // chkEnroll
            // 
            resources.ApplyResources(this.chkEnroll, "chkEnroll");
            this.chkEnroll.Name = "chkEnroll";
            this.chkEnroll.UseVisualStyleBackColor = true;
            this.chkEnroll.CheckedChanged += new System.EventHandler(this.chkEnroll_CheckedChanged);
            this.chkEnroll.KeyDown += new System.Windows.Forms.KeyEventHandler(this.chkEnroll_KeyDown);
            // 
            // lblStar4
            // 
            resources.ApplyResources(this.lblStar4, "lblStar4");
            this.lblStar4.Name = "lblStar4";
            // 
            // cmbSign
            // 
            this.cmbSign.FormattingEnabled = true;
            resources.ApplyResources(this.cmbSign, "cmbSign");
            this.cmbSign.Name = "cmbSign";
            // 
            // label33
            // 
            resources.ApplyResources(this.label33, "label33");
            this.label33.Name = "label33";
            // 
            // btnOccNew
            // 
            resources.ApplyResources(this.btnOccNew, "btnOccNew");
            this.btnOccNew.Name = "btnOccNew";
            this.btnOccNew.UseVisualStyleBackColor = true;
            this.btnOccNew.Click += new System.EventHandler(this.btnOccNew_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.rdSelf);
            this.panel2.Controls.Add(this.rdCompany);
            this.panel2.Controls.Add(this.txtOPhone1);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.txtEmailID);
            this.panel2.Controls.Add(this.label25);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // rdSelf
            // 
            resources.ApplyResources(this.rdSelf, "rdSelf");
            this.rdSelf.Checked = true;
            this.rdSelf.Name = "rdSelf";
            this.rdSelf.TabStop = true;
            this.rdSelf.UseVisualStyleBackColor = true;
            // 
            // rdCompany
            // 
            resources.ApplyResources(this.rdCompany, "rdCompany");
            this.rdCompany.Name = "rdCompany";
            this.rdCompany.UseVisualStyleBackColor = true;
            // 
            // cmbOCity
            // 
            this.cmbOCity.FormattingEnabled = true;
            resources.ApplyResources(this.cmbOCity, "cmbOCity");
            this.cmbOCity.Name = "cmbOCity";
            this.cmbOCity.Leave += new System.EventHandler(this.cmbOCity_Leave);
            this.cmbOCity.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbOCity_KeyDown);
            // 
            // txtOPhone1
            // 
            resources.ApplyResources(this.txtOPhone1, "txtOPhone1");
            this.txtOPhone1.Name = "txtOPhone1";
            this.txtOPhone1.TextChanged += new System.EventHandler(this.txtValid_TextChanged);
            // 
            // label22
            // 
            resources.ApplyResources(this.label22, "label22");
            this.label22.Name = "label22";
            // 
            // txtEmailID
            // 
            resources.ApplyResources(this.txtEmailID, "txtEmailID");
            this.txtEmailID.Name = "txtEmailID";
            this.txtEmailID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEmailID_KeyDown);
            // 
            // label25
            // 
            resources.ApplyResources(this.label25, "label25");
            this.label25.Name = "label25";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rbMale);
            this.panel1.Controls.Add(this.rbFemale);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // rbMale
            // 
            resources.ApplyResources(this.rbMale, "rbMale");
            this.rbMale.Checked = true;
            this.rbMale.Name = "rbMale";
            this.rbMale.TabStop = true;
            this.rbMale.UseVisualStyleBackColor = true;
            // 
            // rbFemale
            // 
            resources.ApplyResources(this.rbFemale, "rbFemale");
            this.rbFemale.Name = "rbFemale";
            this.rbFemale.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // lblStar2
            // 
            resources.ApplyResources(this.lblStar2, "lblStar2");
            this.lblStar2.Name = "lblStar2";
            // 
            // btnAdd
            // 
            resources.ApplyResources(this.btnAdd, "btnAdd");
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtOPhone2
            // 
            resources.ApplyResources(this.txtOPhone2, "txtOPhone2");
            this.txtOPhone2.Name = "txtOPhone2";
            this.txtOPhone2.TextChanged += new System.EventHandler(this.txtValid_TextChanged);
            this.txtOPhone2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtOPhone2_KeyDown);
            // 
            // chkActive
            // 
            resources.ApplyResources(this.chkActive, "chkActive");
            this.chkActive.Checked = true;
            this.chkActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkActive.Name = "chkActive";
            this.chkActive.UseVisualStyleBackColor = true;
            this.chkActive.CheckedChanged += new System.EventHandler(this.chkActive_CheckedChanged);
            // 
            // lblPh2
            // 
            resources.ApplyResources(this.lblPh2, "lblPh2");
            this.lblPh2.Name = "lblPh2";
            // 
            // lblMob2
            // 
            resources.ApplyResources(this.lblMob2, "lblMob2");
            this.lblMob2.Name = "lblMob2";
            // 
            // cmbYear
            // 
            this.cmbYear.FormattingEnabled = true;
            resources.ApplyResources(this.cmbYear, "cmbYear");
            this.cmbYear.Name = "cmbYear";
            // 
            // txtMobileNo2
            // 
            resources.ApplyResources(this.txtMobileNo2, "txtMobileNo2");
            this.txtMobileNo2.Name = "txtMobileNo2";
            this.txtMobileNo2.TextChanged += new System.EventHandler(this.txtValid_TextChanged);
            this.txtMobileNo2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMobileNo2_KeyDown);
            this.txtMobileNo2.Leave += new System.EventHandler(this.txtMobileNo2_Leave);
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // txtCreditLimit
            // 
            resources.ApplyResources(this.txtCreditLimit, "txtCreditLimit");
            this.txtCreditLimit.Name = "txtCreditLimit";
            this.txtCreditLimit.TextChanged += new System.EventHandler(this.txtCreditLimit_TextChanged);
            this.txtCreditLimit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCreditLimit_KeyDown);
            this.txtCreditLimit.Leave += new System.EventHandler(this.txtCreditLimit_Leave);
            // 
            // txtOpBalance
            // 
            resources.ApplyResources(this.txtOpBalance, "txtOpBalance");
            this.txtOpBalance.Name = "txtOpBalance";
            this.txtOpBalance.TextChanged += new System.EventHandler(this.txtOpBalance_TextChanged);
            this.txtOpBalance.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtOpBalance_KeyDown);
            this.txtOpBalance.Leave += new System.EventHandler(this.txtOpBalance_Leave);
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // cmbMonth
            // 
            this.cmbMonth.FormattingEnabled = true;
            this.cmbMonth.Items.AddRange(new object[] {
            resources.GetString("cmbMonth.Items"),
            resources.GetString("cmbMonth.Items1"),
            resources.GetString("cmbMonth.Items2"),
            resources.GetString("cmbMonth.Items3"),
            resources.GetString("cmbMonth.Items4"),
            resources.GetString("cmbMonth.Items5"),
            resources.GetString("cmbMonth.Items6"),
            resources.GetString("cmbMonth.Items7"),
            resources.GetString("cmbMonth.Items8"),
            resources.GetString("cmbMonth.Items9"),
            resources.GetString("cmbMonth.Items10"),
            resources.GetString("cmbMonth.Items11")});
            resources.ApplyResources(this.cmbMonth, "cmbMonth");
            this.cmbMonth.Name = "cmbMonth";
            // 
            // cmbDay
            // 
            this.cmbDay.FormattingEnabled = true;
            resources.ApplyResources(this.cmbDay, "cmbDay");
            this.cmbDay.Name = "cmbDay";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // cmbOccupation
            // 
            this.cmbOccupation.FormattingEnabled = true;
            resources.ApplyResources(this.cmbOccupation, "cmbOccupation");
            this.cmbOccupation.Name = "cmbOccupation";
            this.cmbOccupation.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbOccupation_KeyDown);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // cmbQualification
            // 
            this.cmbQualification.FormattingEnabled = true;
            resources.ApplyResources(this.cmbQualification, "cmbQualification");
            this.cmbQualification.Name = "cmbQualification";
            this.cmbQualification.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbQualification_KeyDown);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.Name = "label19";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // l
            // 
            resources.ApplyResources(this.l, "l");
            this.l.Name = "l";
            // 
            // txtContactPer
            // 
            resources.ApplyResources(this.txtContactPer, "txtContactPer");
            this.txtContactPer.Name = "txtContactPer";
            // 
            // txtOPinCode
            // 
            resources.ApplyResources(this.txtOPinCode, "txtOPinCode");
            this.txtOPinCode.Name = "txtOPinCode";
            this.txtOPinCode.TextChanged += new System.EventHandler(this.txtOPinCode_TextChanged);
            // 
            // label18
            // 
            resources.ApplyResources(this.label18, "label18");
            this.label18.Name = "label18";
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.Name = "label17";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.txtOPinCode);
            this.panel3.Controls.Add(this.dtpEnroll);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.lblChkHelp);
            this.panel3.Controls.Add(this.label19);
            this.panel3.Controls.Add(this.lblStar2);
            this.panel3.Controls.Add(this.chkSendSMS);
            this.panel3.Controls.Add(this.chkEnroll);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.chkActive);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.btnAdd);
            this.panel3.Controls.Add(this.cmbQualification);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.cmbOccupation);
            this.panel3.Controls.Add(this.l);
            this.panel3.Controls.Add(this.txtContactPer);
            this.panel3.Controls.Add(this.lblStar4);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.cmbSign);
            this.panel3.Controls.Add(this.cmbDay);
            this.panel3.Controls.Add(this.label33);
            this.panel3.Controls.Add(this.cmbMonth);
            this.panel3.Controls.Add(this.btnOccNew);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.panel2);
            this.panel3.Controls.Add(this.txtOpBalance);
            this.panel3.Controls.Add(this.panel1);
            this.panel3.Controls.Add(this.txtCreditLimit);
            this.panel3.Controls.Add(this.txtMobileNo2);
            this.panel3.Controls.Add(this.cmbYear);
            this.panel3.Controls.Add(this.lblMob2);
            this.panel3.Controls.Add(this.lblPh2);
            this.panel3.Controls.Add(this.txtOPhone2);
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.Name = "label13";
            // 
            // cmbOArea
            // 
            this.cmbOArea.FormattingEnabled = true;
            resources.ApplyResources(this.cmbOArea, "cmbOArea");
            this.cmbOArea.Name = "cmbOArea";
            this.cmbOArea.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbOArea_KeyDown);
            // 
            // label24
            // 
            resources.ApplyResources(this.label24, "label24");
            this.label24.Name = "label24";
            // 
            // CustomerAE
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.panel3);
            this.Name = "CustomerAE";
            this.Deactivate += new System.EventHandler(this.CustomerAE_Deactivate);
            this.Load += new System.EventHandler(this.CustomerAE_Load);
            this.Activated += new System.EventHandler(this.CustomerAE_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CustomerAE_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.EP)).EndInit();
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.pnlCustInfo.ResumeLayout(false);
            this.pnlCustInfo.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtOPinCode;
        private System.Windows.Forms.TextBox txtOPhone2;
        private System.Windows.Forms.Label lblPh2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cmbOState;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cmbOCity;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtOAddress;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtOPhone1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtMobileNo1;
        private System.Windows.Forms.Label lblMob2;
        private System.Windows.Forms.TextBox txtMobileNo2;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtEmailID;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtLedgerName;
        private System.Windows.Forms.Label l;
        private System.Windows.Forms.TextBox txtContactPer;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.Button btnLast;
        private System.Windows.Forms.Button btnFirst;
        private System.Windows.Forms.ErrorProvider EP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtOpBalance;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button BtnExit;
        private JitControls.OMBPanel pnlMain;
        private System.Windows.Forms.ComboBox cmbQualification;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbOccupation;
        private System.Windows.Forms.TextBox txtCreditLimit;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chkActive;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton rdCompany;
        private System.Windows.Forms.RadioButton rdSelf;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.ComboBox cmbDay;
        private System.Windows.Forms.ComboBox cmbYear;
        private System.Windows.Forms.ComboBox cmbMonth;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label lblStar1;
        private System.Windows.Forms.Label lblStar3;
        private System.Windows.Forms.Label lblStar2;
        private System.Windows.Forms.ListBox lstBldgName;
        private System.Windows.Forms.RadioButton rbFemale;
        private System.Windows.Forms.RadioButton rbMale;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnOccNew;
        private System.Windows.Forms.ComboBox cmbSign;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label lblStar4;
        private System.Windows.Forms.CheckBox chkSendSMS;
        private System.Windows.Forms.CheckBox chkEnroll;
        private System.Windows.Forms.Label lblChkHelp;
        private System.Windows.Forms.Label lblDeActiveCount;
        private System.Windows.Forms.Label lblActiveCount;
        private System.Windows.Forms.Label lblTotalCount;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private JitControls.OMBPanel pnlCustInfo;
        private System.Windows.Forms.Label lblConsentDate;
        private System.Windows.Forms.DateTimePicker dtpEnroll;
        private System.Windows.Forms.Button btnLangLedgerName;
        private System.Windows.Forms.TextBox txtLangLedgerName;
        private System.Windows.Forms.Button btnLangAdd;
        private System.Windows.Forms.TextBox txtLangAdd;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtGSTINNo;
        private System.Windows.Forms.Label lblGSTIN;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cmbOArea;
        private System.Windows.Forms.Label label24;
    }
}