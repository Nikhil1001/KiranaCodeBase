﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Master
{
    /// <summary>
    /// This class is used for Customer AE
    /// </summary>
    public partial class CustomerAE : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        DBMLedger dbLedger = new DBMLedger();
        MLedger mLedger = new MLedger();
        MLedgerDetails mLedgerDetails = new MLedgerDetails();
        string LedgerNm, mobNo, mobNo2;
        DataTable dtSearch = new DataTable();
        DataTable dtLedger = new DataTable();
        int cntRow;
        long LedgerUserNo, ID, CheckEnroll = 0;
        /// <summary>
        /// This field used for Customer Short ID
        /// </summary>
        public long ShortID = 0;
        bool isDoActivateProcess = false;
        DateTime tempEnollDate;
        bool FlagBilingual;
        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public CustomerAE()
        {
            InitializeComponent();
        }

        /// <summary>
        /// This is class of parameterised Constructor
        /// </summary>
        public CustomerAE(long shortid)
        {
            InitializeComponent();
            ShortID = shortid;
            CheckEnroll = 0;
        }

        private void CustomerAE_Load(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_Bilingual)) == true)
                {
                    FlagBilingual = true;
                    SetFlag(true);
                }
                else
                {
                    FlagBilingual = false;
                    SetFlag(false);
                }

                ObjFunction.FillCombo(cmbOCity, "Select CityNo,CityName From MCity where CityNo=0 and IsActive='true' order by CityName");
                ObjFunction.FillCombo(cmbOArea, "Select AreaNo,AreaName From MArea where AreaNo=0 and IsActive='true' order by AreaName");
                ObjFunction.FillCombo(cmbOState, "Select StateNo,StateName From MState where IsActive='true' and Stateno in (select StateNo From MCity Where IsActive='true') order by StateName");
                ObjFunction.FillCombo(cmbSign, "Select SignCode, CASE WHEN SignCode = 2 THEN 'To Pay' WHEN SignCode = 1 THEN 'To Receive' END AS 'Test' from MSign order by SignName");
                ObjFunction.FillCombo(cmbQualification, "Select QualificationNo,QualificationName from MQualification where IsActive='true'order by QualificationName");
                ObjFunction.FillCombo(cmbOccupation, "Select OccupationNo,OccupationName from MOccupation where IsActive='true'  order by OccupationName");
                FillMonth();
                FillDays();
                FillYear();
                DisplayCustomerCount();
                tempEnollDate = DBGetVal.ServerTime.Date;
                dtLedger = ObjFunction.GetDataView("Select LedgerName from MLedger Where GroupNo=" + GroupType.SundryDebtors + " order by LedgerName").Table;
                if (ShortID == 0)
                {
                    ObjFunction.LockButtons(true, this.Controls);
                    ObjFunction.LockControls(false, this.Controls);
                    LedgerNm = "";
                    mobNo = "";
                    mobNo2 = "";
                    //lblCrDr.Text = "";
                    dtSearch = ObjFunction.GetDataView("Select LedgerNo from MLedger Where GroupNo=" + GroupType.SundryDebtors + " order by LedgerName").Table;
                    if (dtSearch.Rows.Count > 0)
                    {
                        if (Customer.RequestCustomerNo == 0)
                            ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                        else
                            ID = Customer.RequestCustomerNo;
                        FillControls();
                        SetNavigation();
                    }
                    setDisplay(true);
                    btnNew.Focus();

                }
                else
                {
                    btnNew_Click(sender, new EventArgs());
                    setDisable(false);
                }
                KeyDownFormat(this.Controls);
                lstBldgName.Visible = false;
                lblConsentDate.ForeColor = Color.Green;
                dtpEnroll.MaxDate = DBGetVal.ServerTime.Date;
                
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void SetFlag(bool flag)
        {
            txtLangLedgerName.Visible = flag;
            btnLangLedgerName.Visible = flag;
            txtLangAdd.Visible = flag;
            btnLangAdd.Visible = flag;
        }


        private void DisplayCustomerCount()
        {
            lblTotalCount.Text = ObjQry.ReturnLong("SELECT  COUNT(*) FROM  MLedger WHERE  (GroupNo = " + GroupType.SundryDebtors + ")", CommonFunctions.ConStr).ToString();
            lblActiveCount.Text = ObjQry.ReturnLong("SELECT  COUNT(*) FROM  MLedger WHERE  (GroupNo = " + GroupType.SundryDebtors + ") AND (IsActive = 'true')", CommonFunctions.ConStr).ToString();
            lblDeActiveCount.Text = ObjQry.ReturnLong("SELECT  COUNT(*) FROM  MLedger WHERE  (GroupNo = " + GroupType.SundryDebtors + ") AND (IsActive = 'false')", CommonFunctions.ConStr).ToString();
            label9.Font = ObjFunction.GetFont(FontStyle.Regular, 7);
            lblTotalCount.Font = ObjFunction.GetFont(FontStyle.Regular, 7);
            label10.Font = ObjFunction.GetFont(FontStyle.Regular, 7);
            lblActiveCount.Font = ObjFunction.GetFont(FontStyle.Regular, 7);
            label11.Font = ObjFunction.GetFont(FontStyle.Regular, 7);
            lblDeActiveCount.Font = ObjFunction.GetFont(FontStyle.Regular, 7);
        }

        private void FillControls()
        {
            try
            {
                EP.SetError(txtLedgerName, "");
                EP.SetError(txtMobileNo1, "");
                EP.SetError(txtMobileNo2, "");
                EP.SetError(txtContactPer, "");
                EP.SetError(txtOpBalance, "");
                EP.SetError(txtCreditLimit, "");
                EP.SetError(txtEmailID, "");
                EP.SetError(chkEnroll, "");
                mLedger = dbLedger.ModifyMLedgerByID(ID);
                mLedgerDetails = dbLedger.ModifyMLedgerDetailsByID(ID);
                LedgerNm = mLedger.LedgerName;
                txtLedgerName.Text = mLedger.LedgerName;
                LedgerUserNo = 0;
                txtContactPer.Text = mLedger.ContactPerson;
                ObjFunction.FillCombo(cmbSign, "Select SignCode, CASE WHEN SignCode = 2 THEN 'To Pay' WHEN SignCode = 1 THEN 'To Receive' END AS 'Test' from MSign order by SignName");
                cmbSign.SelectedValue = mLedger.SignCode.ToString();


                txtOpBalance.Text = mLedger.OpeningBalance.ToString();
                chkActive.Checked = mLedger.IsActive;
                if (chkActive.Checked == true)
                    chkActive.Text = "Yes";
                else
                    chkActive.Text = "No";

                //Ledger Details
                txtOAddress.Text = mLedgerDetails.Address;
                ObjFunction.FillCombo(cmbQualification, "Select QualificationNo,QualificationName from MQualification where (IsActive='true' or QualificationNo=" + mLedgerDetails.QualificationNo + ") order by QualificationName");
                ObjFunction.FillCombo(cmbOccupation, "Select OccupationNo,OccupationName from MOccupation where (IsActive='true' or OccupationNo=" + mLedgerDetails.OccupationNo + ")  order by OccupationName");

                ObjFunction.FillCombo(cmbOState, "Select StateNo,StateName From MState where (IsActive='true' or StateNo= " + mLedgerDetails.StateNo + ") order by StateName");
                cmbOState.SelectedValue = mLedgerDetails.StateNo.ToString();
                ObjFunction.FillCombo(cmbOCity, "Select CityNo,CityName From MCity where StateNo = " + ObjFunction.GetComboValue(cmbOState) + " and (IsActive='true' or CityNo=" + mLedgerDetails.CityNo + ") order by CityName");
                cmbOCity.SelectedValue = mLedgerDetails.CityNo.ToString();
                ObjFunction.FillCombo(cmbOArea, "Select AreaNo,AreaName From MArea where CityNo = " + ObjFunction.GetComboValue(cmbOCity) + " and (IsActive='true' or AreaNo=" + mLedgerDetails.AreaNo + ") order by AreaName");
                cmbOArea.SelectedValue = mLedgerDetails.AreaNo.ToString();
             
                
                txtOPinCode.Text = mLedgerDetails.PinCode;
                txtOPhone1.Text = mLedgerDetails.PhNo1;
                txtOPhone2.Text = mLedgerDetails.PhNo2;
                txtMobileNo1.Text = mLedgerDetails.MobileNo1;
                mobNo = mLedgerDetails.MobileNo1;
                mobNo2 = mLedgerDetails.MobileNo2;
                txtMobileNo2.Text = mLedgerDetails.MobileNo2;
                txtEmailID.Text = mLedgerDetails.EmailID;
                cmbDay.Text = mLedgerDetails.DOB.Day.ToString();
                cmbMonth.SelectedIndex = (mLedgerDetails.DOB.Month) - 1;
                cmbYear.Text = mLedgerDetails.DOB.Year.ToString();
                txtGSTINNo.Text = mLedgerDetails.CSTNo;
                cmbQualification.SelectedValue = mLedgerDetails.QualificationNo.ToString();
                cmbOccupation.SelectedValue = mLedgerDetails.OccupationNo.ToString();
                if (mLedgerDetails.CustomerType.ToString() == "0")
                    rdSelf.Checked = true;
                else
                    rdCompany.Checked = true;
                if (mLedgerDetails.Gender.ToString() == "0")
                    rbMale.Checked = true;
                else
                    rbFemale.Checked = true;
                txtCreditLimit.Text = mLedgerDetails.CreditLimit.ToString("0.00");
                lstBldgName.Visible = false;

                chkEnroll.Checked = mLedger.IsEnroll;
                dtpEnroll.Value = mLedger.UserDate;
                tempEnollDate = mLedger.UserDate;
                if (chkEnroll.Checked == true)
                {
                    chkSendSMS.Visible = true;
                    dtpEnroll.Visible = true;
                }
                else
                {
                    chkSendSMS.Visible = false;
                    dtpEnroll.Visible = false;
                }
                chkSendSMS.Checked = mLedger.IsSendSMS;

                if (ObjQry.ReturnLong("Select Count(*) from TVoucherRefDetails where LedgerNo=" + mLedger.LedgerNo + " and TypeOfRef=5 ", CommonFunctions.ConStr) == 0)
                {
                    txtOpBalance.ReadOnly = false;
                    cmbSign.Enabled = false;
                }
                else
                {
                    txtOpBalance.ReadOnly = true;
                    cmbSign.Enabled = false;
                }

                if (mLedgerDetails.ConsentDate.ToString(Format.DDMMMYYYY) != "01-Jan-1900")
                    lblConsentDate.Text = "Cons.Dt:" + mLedgerDetails.ConsentDate.ToString(Format.DDMMMYYYY);
                else lblConsentDate.Text = "-----";
                txtLangLedgerName.Text = mLedger.LangLedgerName;
                txtLangAdd.Text = mLedgerDetails.LangAddress;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void setValue()
        {
            try
            {
                if (Validations() == true)
                {
                    bool flag = false;

                    dbLedger = new DBMLedger();
                    mLedger = new MLedger();
                    mLedger.LedgerNo = ID;
                    mLedger.LedgerUserNo = LedgerUserNo.ToString();
                    mLedger.LedgerName = txtLedgerName.Text.Trim().ToUpper();
                    mLedger.InvFlag = true;// chkActiveInv.Checked;
                    mLedger.MaintainBillByBill = false;// chkActiveBill.Checked;
                    mLedger.IsActive = chkActive.Checked;
                    mLedger.ContactPerson = txtContactPer.Text.Trim();
                    mLedger.GroupNo = GroupType.SundryDebtors;
                    mLedger.OpeningBalance = Convert.ToDouble(txtOpBalance.Text.Trim());
                    mLedger.SignCode = ObjFunction.GetComboValue(cmbSign);


                    mLedger.CompanyNo = DBGetVal.CompanyNo;
                    mLedger.UserID = DBGetVal.UserID;
                    mLedger.UserDate = (dtpEnroll.Visible == true) ? Convert.ToDateTime(dtpEnroll.Text) : tempEnollDate;// DBGetVal.ServerTime.Date;
                    mLedger.LedgerStatus = (ID == 0) ? 1 : 2;
                    mLedger.IsEnroll = chkEnroll.Checked;
                    mLedger.IsSendSMS = chkSendSMS.Checked;
                    mLedger.LangLedgerName = txtLangLedgerName.Text;
                    flag = dbLedger.AddMLedger(mLedger);

                    if (flag == true)
                    {
                        mLedgerDetails = new MLedgerDetails();           

                        mLedgerDetails.LedgerDetailsNo = ObjQry.ReturnLong("Select LedgerDetailsNo From MLedgerDetails Where LedgerNo=" + mLedger.LedgerNo + "", CommonFunctions.ConStr);
                        mLedgerDetails.LedgerNo = ID;
                        mLedgerDetails.Address = txtOAddress.Text.Trim();
                        mLedgerDetails.StateNo = ObjFunction.GetComboValue(cmbOState);
                        mLedgerDetails.CityNo = ObjFunction.GetComboValue(cmbOCity);
                        mLedgerDetails.AreaNo = ObjFunction.GetComboValue(cmbOArea);
                        mLedgerDetails.PinCode = txtOPinCode.Text.Trim();
                        mLedgerDetails.PhNo1 = txtOPhone1.Text.Trim();
                        mLedgerDetails.PhNo2 = txtOPhone2.Text.Trim();
                        mLedgerDetails.MobileNo1 = txtMobileNo1.Text.Trim();
                        mLedgerDetails.MobileNo2 = txtMobileNo2.Text.Trim();
                        mLedgerDetails.EmailID = txtEmailID.Text.Trim();
                        mLedgerDetails.DOB = Convert.ToDateTime(cmbDay.Text + "-" + cmbMonth.Text + "-" + cmbYear.Text);
                        mLedgerDetails.QualificationNo = ObjFunction.GetComboValue(cmbQualification);
                        mLedgerDetails.OccupationNo = ObjFunction.GetComboValue(cmbOccupation);
                        mLedgerDetails.CustomerType = (rdSelf.Checked == true) ? 0 : 1;
                        mLedgerDetails.Gender = (rbMale.Checked == true) ? 0 : 1;
                        mLedgerDetails.CreditLimit = (txtCreditLimit.Text.Trim() == "") ? 0 : Convert.ToDouble(txtCreditLimit.Text.Trim());
                        mLedgerDetails.PANNo = "";
                        mLedgerDetails.VATNo = "";
                        mLedgerDetails.CSTNo = txtGSTINNo.Text;
                        mLedgerDetails.LangAddress = txtLangAdd.Text;
                        mLedgerDetails.UserID = DBGetVal.UserID;
                        mLedgerDetails.UserDate = DBGetVal.ServerTime.Date;

                        dbLedger.AddMLedgerDetails(mLedgerDetails);
                    }

                    if (dbLedger.ExecuteNonQueryStatements() == true)
                    {
                        if (ID == 0)
                        {
                            OMMessageBox.Show("Customer Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            dtSearch = ObjFunction.GetDataView("Select LedgerNo from MLedger Where GroupNo=" + GroupType.SundryDebtors + " order by LedgerName ").Table;
                            dtLedger = ObjFunction.GetDataView("Select LedgerName from MLedger Where GroupNo=" + GroupType.SundryDebtors + " order by LedgerName").Table;
                            ID = ObjQry.ReturnLong("Select Max(LedgerNo) FRom MLedger", CommonFunctions.ConStr);
                            if (ShortID == 0)
                            {
                                SetNavigation();
                                FillControls();
                            }
                            else
                            {
                                ShortID = ID;
                                this.Close();
                            }
                        }
                        else
                        {
                            OMMessageBox.Show("Customer Updated Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            FillControls();
                        }

                        ObjFunction.LockButtons(true, this.Controls);
                        ObjFunction.LockControls(false, this.Controls);
                        btnNew.Focus();
                    }

                    else
                        OMMessageBox.Show("Customer Not Added ", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);

                    btnOccNew.Enabled = false;
                    DisplayCustomerCount();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private bool Validations()
        {
            bool flag = true;
            EP.SetError(txtLedgerName, "");
            EP.SetError(txtMobileNo1, "");
            EP.SetError(txtMobileNo2, "");
            EP.SetError(txtContactPer, "");
            EP.SetError(txtOpBalance, "");
            EP.SetError(txtCreditLimit, "");
            EP.SetError(txtEmailID, "");
            EP.SetError(cmbSign, "");
            EP.SetError(chkEnroll, "");

            if (txtMobileNo1.Text.Trim() == "")
            {
                txtMobileNo1.Text = "1111111111";
                //EP.SetError(txtMobileNo1, "Enter Mobile Number");
                //EP.SetIconAlignment(txtMobileNo1, ErrorIconAlignment.MiddleRight);
                //txtMobileNo1.Focus();

            }
            
            if (txtLedgerName.Text.Trim() == "")
            {
                EP.SetError(txtLedgerName, "Enter Customer Name");
                EP.SetIconAlignment(txtLedgerName, ErrorIconAlignment.MiddleRight);
                txtLedgerName.Focus();
            }
           
            if (flag == true)
            {
                if (LedgerNm.ToUpper() != txtLedgerName.Text.Trim().ToUpper())
                {
                    if (ObjQry.ReturnInteger("Select Count(*) from MLedger where LedgerName = '" + txtLedgerName.Text.Trim().ToUpper().Replace("'", "''") + "' And GroupNo=" + GroupType.SundryDebtors + "", CommonFunctions.ConStr) != 0)
                    {
                        EP.SetError(txtLedgerName, "Duplicate Ledger Name");
                        EP.SetIconAlignment(txtLedgerName, ErrorIconAlignment.MiddleRight);
                        txtLedgerName.Focus();
                        flag = false;
                    }
                    else
                        flag = true;
                }
            }


            if (flag == true)
            {
                if (mobNo != txtMobileNo1.Text.Trim())
                {
                    if (txtMobileNo1.Text.Trim() != "1111111111")
                    {
                        if (ObjQry.ReturnInteger("Select Count(*) from MLedgerDetails where MobileNo1='" + txtMobileNo1.Text.Trim() + "'", CommonFunctions.ConStr) != 0)
                        {
                            EP.SetError(txtMobileNo1, "Duplicate Mobile No");
                            EP.SetIconAlignment(txtMobileNo1, ErrorIconAlignment.MiddleRight);
                            txtMobileNo1.Focus();
                            flag = false;
                        }
                        else
                            flag = true;
                    }
                }
                if (flag == true)
                {
                    if (txtMobileNo1.Text.Trim().Length < 10)
                    {
                        EP.SetError(txtMobileNo1, "Enter Valid Mobile No");
                        EP.SetIconAlignment(txtMobileNo1, ErrorIconAlignment.MiddleRight);
                        txtMobileNo1.Focus();
                        flag = false;
                    }
                   
                }
            }
            if (flag == true)
            {
                if (txtMobileNo2.Visible == true)
                {
                    if (mobNo2 != txtMobileNo2.Text.Trim())
                    {
                        if (txtMobileNo2.Text.Trim() != "1111111111")
                        {
                            if (ObjQry.ReturnInteger("Select Count(*) from MLedgerDetails where MobileNo2='" + txtMobileNo2.Text.Trim() + "'", CommonFunctions.ConStr) != 0)
                            {
                                EP.SetError(txtMobileNo2, "Duplicate Mobile No");
                                EP.SetIconAlignment(txtMobileNo2, ErrorIconAlignment.MiddleRight);
                                txtMobileNo2.Focus();
                                flag = false;
                            }
                            else
                                flag = true;
                        }
                    }
                    if (flag == true)
                    {
                        if (txtMobileNo2.Text.Trim().Length < 10)
                        {
                            EP.SetError(txtMobileNo2, "Enter Valid Mobile No");
                            EP.SetIconAlignment(txtMobileNo2, ErrorIconAlignment.MiddleRight);
                            txtMobileNo1.Focus();
                            flag = false;
                        }
                        else
                            flag = true;
                    }
                }
                else
                    flag = true;
            }
           

            return flag;
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                long CityNo = 0, stateNo = 0;
                if (txtCreditLimit.Text.Trim() == "")
                {
                    txtCreditLimit.Text = "0.00";
                }
                if (txtOpBalance.Text.Trim() == "")
                {
                    txtOpBalance.Text = "0.00";
                }
                if (ObjFunction.GetComboValue(cmbOState) == 0)
                {
                    CityNo = ObjQry.ReturnLong("Select CityCode from MCompany where CompanyNo=" + DBGetVal.CompanyNo + "", CommonFunctions.ConStr);
                    stateNo = ObjQry.ReturnLong("Select StateNo from Mcity where CityNo=" + CityNo + "", CommonFunctions.ConStr);
                    cmbOState.SelectedValue = stateNo.ToString();
                    cmbOCity.SelectedValue = CityNo.ToString();
                }
                if (ObjFunction.GetComboValue(cmbOCity) == 0)
                {
                    CityNo = ObjQry.ReturnLong("Select CityCode from MCompany where CompanyNo=" + DBGetVal.CompanyNo + "", CommonFunctions.ConStr);
                    stateNo = ObjQry.ReturnLong("Select StateNo from Mcity where CityNo=" + CityNo + "", CommonFunctions.ConStr);
                    ObjFunction.FillCombo(cmbOCity, "Select CityNo,CityName From MCity where StateNo = " + stateNo + " order by CityName");
                    cmbOState.SelectedValue = stateNo.ToString();
                    cmbOCity.SelectedValue = CityNo.ToString();
                }
                if (ObjFunction.GetComboValue(cmbOArea) == 0)
                {
                    cmbOArea.SelectedValue = "1";
                }

            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
            setValue();

            lstBldgName.Visible = false;
        }

        #region Navigation Methods
        private void NavigationDisplay(int type)
        {
            try
            {
                long No = 0;
                if (type == 5)
                {
                    No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                    ID = No;
                }
                else if (type == 1)
                {
                    No = Convert.ToInt64(dtSearch.Rows[0].ItemArray[0].ToString());
                    cntRow = 0;
                    ID = No;
                }
                else if (type == 2)
                {
                    No = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    cntRow = dtSearch.Rows.Count - 1;
                    ID = No;
                }
                else
                {
                    if (type == 3)
                    {
                        cntRow = cntRow + 1;
                    }
                    else if (type == 4)
                    {
                        cntRow = cntRow - 1;
                    }

                    if (cntRow < 0)
                    {
                        OMMessageBox.Show("This is First Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow + 1;
                    }
                    else if (cntRow > dtSearch.Rows.Count - 1)
                    {
                        OMMessageBox.Show("This is Last Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow - 1;
                    }
                    else
                    {
                        No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                        ID = No;
                    }

                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }


            FillControls();

        }

        private void SetNavigation()
        {
            cntRow = 0;
            for (int i = 0; i < dtSearch.Rows.Count; i++)
            {
                if (Convert.ToInt64(dtSearch.Rows[i].ItemArray[0].ToString()) == ID)
                {
                    cntRow = i;
                    break;
                }
            }
        }

        private void setDisplay(bool flag)
        {
            btnFirst.Visible = flag;
            btnPrev.Visible = flag;
            btnNext.Visible = flag;
            btnLast.Visible = flag;
            btnDelete.Visible = flag;
        }

        private void setDisable(bool flag)
        {
            BtnSave.Visible = !flag;
            btnCancel.Visible = !flag;
            btnUpdate.Visible = flag;
            btnSearch.Visible = flag;
            BtnExit.Visible = flag;
            btnFirst.Visible = flag;
            btnPrev.Visible = flag;
            btnNext.Visible = flag;
            btnLast.Visible = flag;
            btnDelete.Visible = flag;
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            NavigationDisplay(1);
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            NavigationDisplay(4);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            NavigationDisplay(3);
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            NavigationDisplay(2);
        }

        #endregion

        #region KeyDown Events
        private void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is GroupBox)
                    KeyDownFormat(ctrl.Controls);
            }
        }

        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left && e.Control)
            {
                if (btnPrev.Enabled) btnPrev_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Up && e.Control)
            {
                if (btnFirst.Enabled) btnFirst_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Right && e.Control)
            {
                if (btnNext.Enabled) btnNext_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Down && e.Control)
            {
                if (btnLast.Enabled) btnLast_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F2)
            {
                if (BtnSave.Visible) BtnSave_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Escape)
            {
                if (BtnSave.Visible) BtnSave.Focus();
            }

        }
        #endregion

        private void BtnExit_Click(object sender, EventArgs e)
        {
            Customer.RequestCustomerNo = 0;
            this.Close();
        }

        private void CustomerAE_FormClosing(object sender, FormClosingEventArgs e)
        {
            LedgerNm = "";
            mobNo = "";
            ID = 0;
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (chkActive.Checked == false) return;
                dbLedger = new DBMLedger();
                mLedger = new MLedger();
                mLedger.LedgerNo = ID;

                if (ObjQry.ReturnLong("Select Count(*) from TVoucherEntry Inner Join TvoucherDetails on TVoucherEntry.PkVoucherNo=TVoucherDetails.FkVoucherNo where TVoucherEntry.VoucherDate>='" + DBGetVal.FromDate + "' and TVoucherEntry.VoucherDate<='" + DBGetVal.ToDate + "' and TVoucherDetails.LedgerNo=" + ID + "", CommonFunctions.ConStr) > 0)
                {
                    OMMessageBox.Show("Sorry You can not delete this record?", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                }
                else
                {

                    if (OMMessageBox.Show("Are you sure want to delete this record?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        if (dbLedger.DeleteMLedger(mLedger) == true)
                        {
                            OMMessageBox.Show("Customer Deleted Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            FillControls();
                        }
                        else
                        {
                            OMMessageBox.Show("Customer not Deleted", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        }
                    }
                }
                lstBldgName.Visible = false;
                DisplayCustomerCount();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            Form NewF = new Customer();
            this.Close();
            ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
        }

        private void txtOpBalance_Leave(object sender, EventArgs e)
        {
            try
            {
                //EP.SetError(txtOpBalance, "");
                if (txtOpBalance.Text != "")
                {
                    if (ObjFunction.CheckValidAmount(txtOpBalance.Text.Trim()) == false)
                    {
                        EP.SetError(txtOpBalance, "Enter Valid Balance Amount");
                        EP.SetIconAlignment(txtOpBalance, ErrorIconAlignment.MiddleRight);
                        txtOpBalance.Focus();
                    }
                }
                else
                    txtOpBalance.Text = "0.00";
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                LedgerNm = "";
                mobNo = "";
                ID = 0;
                //lblCrDr.Text = "";
                ObjFunction.InitialiseControl(this.Controls);
                ObjFunction.LockButtons(false, this.Controls);
                ObjFunction.LockControls(true, this.Controls);
                ObjFunction.FillCombo(cmbOState, "Select StateNo,StateName From MState where (IsActive='true') AND Stateno in (select StateNo From MCity Where IsActive='true') order by StateName");
                ObjFunction.FillCombo(cmbOCity, "Select CityNo,CityName From MCity where CityNo=0 and IsActive='true' order by CityName");
                ObjFunction.FillCombo(cmbQualification, "Select QualificationNo,QualificationName from MQualification where IsActive='true'order by QualificationName");
                ObjFunction.FillCombo(cmbOccupation, "Select OccupationNo,OccupationName from MOccupation where IsActive='true'  order by OccupationName");
                btnOccNew.Enabled = true;
                //long tempNo = ObjQry.ReturnLong("Select OccupationNo from MOccupation where OccupationName='Doctor'", CommonFunctions.ConStr);
                cmbOccupation.SelectedValue = 0;
                chkActive.Checked = true;
                txtOpBalance.Text = "0.00";
                txtLedgerName.Focus();
                cmbSign.SelectedIndex = 2;
                txtOpBalance.ReadOnly = false;
                lstBldgName.Visible = false;
                CheckEnroll = 1;
                chkEnroll.Checked = true;
                CheckEnroll = 0;
                chkSendSMS.Visible = true;
                chkSendSMS.Checked = true;
                dtpEnroll.Visible = true;
                dtpEnroll.Value = DBGetVal.ServerTime.Date;
                tempEnollDate = DBGetVal.ServerTime.Date;
                lblConsentDate.Text = "-----";
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (ShortID == 0)
            {
                NavigationDisplay(5);
                ObjFunction.LockButtons(true, this.Controls);
                ObjFunction.LockControls(false, this.Controls);
            }

            else this.Close();
            lstBldgName.Visible = false;
            btnOccNew.Enabled = false;
            btnNew.Focus();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            ObjFunction.LockButtons(false, this.Controls);
            ObjFunction.LockControls(true, this.Controls);
            btnOccNew.Enabled = true;

            if (ObjQry.ReturnLong("Select Count(*) from TVoucherRefDetails where LedgerNo=" + mLedger.LedgerNo + " and TypeOfRef=5 ", CommonFunctions.ConStr) == 0)
            {
                txtOpBalance.ReadOnly = false;
                cmbSign.Enabled = false;
            }
            else
            {
                txtOpBalance.ReadOnly = true;
                cmbSign.Enabled = false;
            }

            txtLedgerName.Focus();
            lstBldgName.Visible = false;            
        }

        private void chkActive_CheckedChanged(object sender, EventArgs e)
        {
            if (chkActive.Checked == true)
                chkActive.Text = "Yes";
            else
                chkActive.Text = "No";
        }

        private void txtCreditLimit_Leave(object sender, EventArgs e)
        {
            try
            {
                EP.SetError(txtCreditLimit, "");
                if (txtCreditLimit.Text != "")
                {
                    if (ObjFunction.CheckValidAmount(txtCreditLimit.Text.Trim()) == false)
                    {
                        EP.SetError(txtCreditLimit, "Enter Valid Credit Amount");
                        EP.SetIconAlignment(txtCreditLimit, ErrorIconAlignment.MiddleRight);
                        txtCreditLimit.Focus();
                    }
                    else
                    {
                        if (txtOPhone2.Visible == true)
                            txtOPhone2.Focus();
                    }
                }
                else
                {
                    txtCreditLimit.Text = "0.00";
                    if (lblMob2.Visible == true)
                        txtOPhone2.Focus();
                    else
                        BtnSave.Focus();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            bool flagV = txtMobileNo2.Visible;
            lblPh2.Visible = !flagV;
            lblMob2.Visible = !flagV;
            txtOPhone2.Visible = !flagV;
            txtMobileNo2.Visible = !flagV;
            lstBldgName.Visible = false;
        }

        private void txtCreditLimit_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                EP.SetError(txtCreditLimit, "");
                if (txtCreditLimit.Text != "")
                {
                    if (ObjFunction.CheckValidAmount(txtCreditLimit.Text.Trim()) == false)
                    {
                        EP.SetError(txtCreditLimit, "Enter Valid Credit Amount");
                        EP.SetIconAlignment(txtCreditLimit, ErrorIconAlignment.MiddleRight);
                        txtCreditLimit.Focus();
                    }
                    else
                    {
                        if (lblMob2.Visible == true)
                            txtMobileNo2.Focus();
                        else
                            rbMale.Focus();
                    }
                }
                else
                {
                    txtCreditLimit.Text = "0.00";
                    if (lblMob2.Visible == true)
                        txtMobileNo2.Focus();
                    else
                        rbMale.Focus();
                }
            }
        }

        private void FillMonth()
        {
            try
            {
                DataTable dtMonth = new DataTable();
                dtMonth.Columns.Add("No", Type.GetType("System.Int32"));
                dtMonth.Columns.Add("Name");
                string strMonth = "";
                for (int i = 1; i <= 12; i++)
                {
                    if (i == 1) strMonth = "Jan";
                    else if (i == 2) strMonth = "Feb";
                    else if (i == 3) strMonth = "Mar";
                    else if (i == 4) strMonth = "Apr";
                    else if (i == 5) strMonth = "May";
                    else if (i == 6) strMonth = "Jun";
                    else if (i == 7) strMonth = "Jul";
                    else if (i == 8) strMonth = "Aug";
                    else if (i == 9) strMonth = "Sep";
                    else if (i == 10) strMonth = "Oct";
                    else if (i == 11) strMonth = "Nov";
                    else if (i == 12) strMonth = "Dec";

                    DataRow dr = dtMonth.NewRow();
                    dr[0] = i;
                    dr[1] = strMonth;
                    dtMonth.Rows.Add(dr);
                }
                cmbMonth.DisplayMember = "Name";
                cmbMonth.ValueMember = "No";
                cmbMonth.DataSource = dtMonth.DefaultView;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillYear()
        {
            try
            {
                DataTable dtYear = new DataTable();
                dtYear.Columns.Add("No", Type.GetType("System.Int32"));
                dtYear.Columns.Add("Name");

                for (int i = 1901; i <= DateTime.Now.Year; i++)
                {

                    DataRow dr = dtYear.NewRow();
                    dr[0] = i;
                    dr[1] = i;
                    dtYear.Rows.Add(dr);
                }
                cmbYear.DisplayMember = "Name";
                cmbYear.ValueMember = "No";
                cmbYear.DataSource = dtYear;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillDays()
        {
            try
            {
                DataTable dtDays = new DataTable();
                dtDays.Columns.Add("No", Type.GetType("System.Int32"));
                dtDays.Columns.Add("Name");

                for (int i = 1; i < 32; i++)
                {

                    DataRow dr = dtDays.NewRow();
                    dr[0] = i;
                    dr[1] = i;
                    dtDays.Rows.Add(dr);
                }
                cmbDay.DisplayMember = "Name";
                cmbDay.ValueMember = "No";
                cmbDay.DataSource = dtDays;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void txtValid_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMaskedNumeric((TextBox)sender);
        }

        private void cmbOState_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    e.SuppressKeyPress = true;
                    long Cid = ObjFunction.GetComboValue(cmbOCity);
                    ObjFunction.FillCombo(cmbOCity, "Select CityNo,CityName From MCity where StateNo = " + ObjFunction.GetComboValue(cmbOState) + " and (IsActive='true' or CityNo=" + Cid + ") order by CityName");
                    cmbOCity.SelectedValue = Cid;
                    cmbOCity.Focus();
                }
                catch (Exception exc)
                {
                    ObjFunction.ExceptionDisplay(exc.Message);
                }
            }
        }

        private void cmbOState_Leave(object sender, EventArgs e)
        {
            cmbOState_KeyDown(sender, new KeyEventArgs(Keys.Enter));
        }

        #region Ledger Name Searching Functionality
        private void txtLedgerName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DataRow[] dr = null;
                if (txtLedgerName.Text.Trim() != "")
                {
                    dr = dtLedger.Select("LedgerName like '" + txtLedgerName.Text.Trim().Replace("'", "''") + "%'");
                    if (dr.Length > 0)
                    {
                        lstBldgName.Visible = true;
                        lstBldgName.Items.Clear();
                        for (int i = 0; i < dr.Length; i++)
                        {
                            lstBldgName.Items.Add(dr[i].ItemArray[0].ToString());
                        }
                    }
                    else
                    {
                        lstBldgName.Visible = false;
                    }
                }
                else
                {
                    lstBldgName.Visible = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void txtLedgerName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                if (lstBldgName.Visible)
                {
                    lstBldgName.Focus();
                }
            }
            else if (e.KeyCode == Keys.Enter)
                lstBldgName.Visible = false;
        }

        private void lstBldgName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                txtLedgerName.Text = lstBldgName.Text;
                txtLedgerName.Focus();
                lstBldgName.Visible = false;
            }
            else if (e.KeyCode == Keys.Escape)
            {
                e.SuppressKeyPress = true;
                txtLedgerName.Focus();
                lstBldgName.Visible = false;
            }
        }

        private void lstBldgName_Leave(object sender, EventArgs e)
        {
            txtLedgerName.Focus();
            lstBldgName.Visible = false;
        }
        #endregion


        private void txtLedgerName_Leave(object sender, EventArgs e)
        {
            try
            {
                if (LedgerNm.ToUpper() != txtLedgerName.Text.Trim().ToUpper())
                {
                    if (ObjQry.ReturnInteger("Select Count(*) from MLedger where LedgerName = '" + txtLedgerName.Text.Trim().ToUpper().Replace("'", "''") + "' And GroupNo=" + GroupType.SundryDebtors + "", CommonFunctions.ConStr) != 0)
                    {
                        EP.SetError(txtLedgerName, "Duplicate Ledger Name");
                        EP.SetIconAlignment(txtLedgerName, ErrorIconAlignment.MiddleRight);
                        txtLedgerName.Focus();

                    }
                    else if (FlagBilingual == true)
                    {
                        //txtLangFullDesc.Focus();
                        if (txtLangLedgerName.Text.Trim().Length == 0)
                        {
                            btnLangLedgerName_Click(btnLangLedgerName, null);
                        }
                    }
                }
                else if (FlagBilingual == true)
                {
                    //txtLangFullDesc.Focus();
                    if (txtLangLedgerName.Text.Trim().Length == 0)
                    {
                        btnLangLedgerName_Click(btnLangLedgerName, null);
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void txtOpBalance_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(txtOpBalance, 2, 9, JitFunctions.MaskedType.NotNegative);
        }

        private void txtCreditLimit_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(txtCreditLimit, 2, 9, JitFunctions.MaskedType.NotNegative);
        }

        private void txtOPinCode_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(txtOPinCode, 0, 6, JitFunctions.MaskedType.NotNegative);
        }

        //private void txtOAddress_GotFocus(object sender, EventArgs e)
        //{
        //    //txtOAddress.SelectionLength = 0;
        //   // txtOAddress.Select(txtOAddress.Text.Length, 0);
        //    //txtOAddress.Select(txtOAddress.Text.Length, txtOAddress.Text.Length);
        //}

        private void btnOccNew_Click(object sender, EventArgs e)
        {
            try
            {
                Form NewF = new Master.OccupationAE(-1);
                ObjFunction.OpenForm(NewF);
                long OccNo = ObjFunction.GetComboValue(cmbOccupation);
                if (((Master.OccupationAE)NewF).ShortID != 0)
                {
                    ObjFunction.FillCombo(cmbOccupation, "Select OccupationNo,OccupationName from MOccupation order by OccupationName");
                    if (((Master.OccupationAE)NewF).ShortID > 0)
                        cmbOccupation.SelectedValue = ((Master.OccupationAE)NewF).ShortID;
                    else
                        cmbOccupation.SelectedValue = OccNo;
                    cmbOccupation.Focus();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbOCity_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                /*13-Apr-2014
                * Exception Handling Purpose.
                * */
                try
                {
                    e.SuppressKeyPress = true;
                    long Cid = ObjFunction.GetComboValue(cmbOArea);
                    ObjFunction.FillCombo(cmbOArea, "Select AreaNo,AreaName From MArea where CityNo = " + ObjFunction.GetComboValue(cmbOCity) + " and (IsActive='true' or AreaNo=" + Cid + ") order by AreaName");
                    cmbOArea.SelectedValue = Cid;
                    cmbOArea.Focus();
                }
                catch (Exception exc)
                {
                    ObjFunction.ExceptionDisplay(exc.Message);//For Common Error Displayed Purpose
                }
            }
        }

        private void CustomerAE_Deactivate(object sender, EventArgs e)
        {
            isDoActivateProcess = true;
        }

        private void CustomerAE_Activated(object sender, EventArgs e)
        {
            try
            {
                if (isDoActivateProcess == true)
                {
                    if (cmbOState.Enabled == true)
                    {
                        long Sid = ObjFunction.GetComboValue(cmbOState);
                        ObjFunction.FillCombo(cmbOState, "Select StateNo,StateName From MState where (IsActive='true' or StateNo= " + mLedgerDetails.StateNo + ") AND Stateno in (select StateNo From MCity Where IsActive='true') order by StateName");
                        cmbOState.SelectedValue = Sid;
                    }
                    if (cmbOCity.Enabled == true)
                    {
                        long Cid = ObjFunction.GetComboValue(cmbOCity);
                        ObjFunction.FillCombo(cmbOCity, "Select CityNo,CityName From MCity where StateNo = " + ObjFunction.GetComboValue(cmbOState) + " and (IsActive='true' or CityNo=" + mLedgerDetails.CityNo + ") order by CityName");
                        cmbOCity.SelectedValue = Cid;
                    }
                    if (cmbQualification.Enabled == true)
                    {
                        long CustID = ObjFunction.GetComboValue(cmbQualification);
                        ObjFunction.FillCombo(cmbQualification, "Select QualificationNo,QualificationName from MQualification where (IsActive='true' or QualificationNo=" + mLedgerDetails.QualificationNo + ") order by QualificationName");
                        cmbQualification.SelectedValue = CustID;
                    }
                    if (cmbOccupation.Enabled == true)
                    {
                        long OccId = ObjFunction.GetComboValue(cmbOccupation);
                        ObjFunction.FillCombo(cmbOccupation, "Select OccupationNo,OccupationName from MOccupation where (IsActive='true' or OccupationNo=" + mLedgerDetails.OccupationNo + ")  order by OccupationName");
                        cmbOccupation.SelectedValue = OccId;
                    }
                }
                txtLangLedgerName.Font = ObjFunction.GetLangFont();
                txtLangAdd.Font = ObjFunction.GetLangFont();
                
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbQualification_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                cmbOccupation.Focus();
            }
        }

        private void cmbOccupation_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                chkActive.Focus();
            }
        }

        private void txtOpBalance_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                //if (txtOpBalance.Text != "")
                //{
                //    if (Convert.ToDouble(txtOpBalance.Text) >= 0)
                //        lblCrDr.Text = "To Receive";
                //    else
                //        lblCrDr.Text = "To Pay";

                //}
                //else
                //    lblCrDr.Text = "";
                cmbSign.Focus();
            }
        }

        private void txtOAddress_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            //    string stgr = txtOAddress.Text;
            //    txtOAddress.Text = stgr;
            //}
        }

        private void txtEmailID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtOAddress.GotFocus += delegate { txtOAddress.Select(txtOAddress.Text.Length, txtOAddress.Text.Length); };
            }
        }

        private void txtMobileNo1_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtMobileNo1.Text != "")
                {
                    if (txtMobileNo1.Text.Trim().Length < 10)
                    {
                        EP.SetError(txtMobileNo1, "Enter Valid Mobile No");
                        EP.SetIconAlignment(txtMobileNo1, ErrorIconAlignment.MiddleRight);
                        txtMobileNo1.Focus();
                    }
                    else if (mobNo != txtMobileNo1.Text.Trim())
                    {
                        if (txtMobileNo1.Text.Trim() != "1111111111")
                        {
                            if (ObjQry.ReturnInteger("Select Count(*) from MLedgerDetails where MobileNo1='" + txtMobileNo1.Text.Trim() + "'", CommonFunctions.ConStr) != 0)
                            {
                                EP.SetError(txtMobileNo1, "Duplicate Mobile No");
                                EP.SetIconAlignment(txtMobileNo1, ErrorIconAlignment.MiddleRight);
                                txtMobileNo1.Focus();
                            }
                            else
                                EP.SetError(txtMobileNo1, "");
                        }
                    }
                }
                else
                    EP.SetError(txtMobileNo1, "");
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

        }

        private void txtMobileNo2_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtMobileNo2.Visible == true)
                {
                    if (txtMobileNo2.Text != "")
                    {
                        if (txtMobileNo2.Text.Trim().Length < 10)
                        {
                            EP.SetError(txtMobileNo2, "Enter Valid Mobile No");
                            EP.SetIconAlignment(txtMobileNo2, ErrorIconAlignment.MiddleRight);
                            txtMobileNo2.Focus();
                        }
                        else if (mobNo2 != txtMobileNo2.Text.Trim())
                        {
                            if (txtMobileNo2.Text.Trim() != "1111111111")
                            {
                                if (ObjQry.ReturnInteger("Select Count(*) from MLedgerDetails where MobileNo1='" + txtMobileNo1.Text.Trim() + "'", CommonFunctions.ConStr) != 0)
                                {
                                    EP.SetError(txtMobileNo2, "Duplicate Mobile No");
                                    EP.SetIconAlignment(txtMobileNo2, ErrorIconAlignment.MiddleRight);
                                    txtMobileNo2.Focus();
                                }
                                else
                                    EP.SetError(txtMobileNo2, "");
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void txtOPhone2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                txtMobileNo2.Focus();
            }
        }

        private void txtMobileNo2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                rbMale.Focus();
            }
        }

        private void chkEnroll_CheckedChanged(object sender, EventArgs e)
        {
            EP.SetError(chkEnroll, "");
            if (CheckEnroll == 0)
            {
                if (chkEnroll.Checked == true)
                {
                    if (txtMobileNo1.Text != "")
                    {
                        if (txtMobileNo1.Text.Trim().Length < 10 || txtMobileNo1.Text.Trim() == "1111111111")
                        {
                            chkEnroll.Checked = false;
                            EP.SetError(chkEnroll, "Enter Valid Mobile No");
                            EP.SetIconAlignment(chkEnroll, ErrorIconAlignment.MiddleRight);
                            chkEnroll.Focus();
                        }
                        else
                        {
                            dtpEnroll.Visible = true;
                            dtpEnroll.Value = DBGetVal.ServerTime.Date;
                            chkSendSMS.Visible = true;
                            dtpEnroll.Focus();
                            //chkSendSMS.Focus();
                        }
                    }
                    else
                    {
                        chkEnroll.Checked = false;
                        EP.SetError(chkEnroll, "Enter Valid Mobile No");
                        EP.SetIconAlignment(chkEnroll, ErrorIconAlignment.MiddleRight);
                        chkEnroll.Focus();
                    }
                }
                else
                {
                    chkSendSMS.Visible = false;
                    chkSendSMS.Checked = false;
                    dtpEnroll.Visible = false;
                }
            }
        }

        private void chkEnroll_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
            if (e.KeyCode == Keys.Enter && chkSendSMS.Visible == true)
                dtpEnroll.Focus();//chkSendSMS.Focus();
            else if (e.KeyCode == Keys.Enter)
                BtnSave.Focus();
        }

        private void chkSendSMS_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
            if (e.KeyCode == Keys.Enter)
                BtnSave.Focus();
        }

        private void dtpEnroll_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                dtpEnroll_Leave(sender, new EventArgs());
            }
        }

        private void dtpEnroll_Leave(object sender, EventArgs e)
        {
            chkSendSMS.Focus();
        }

        private void btnLangLedgerName_Click(object sender, EventArgs e)
        {
               
            try
            {
                Utilities.KeyBoard frmkb;
                if (txtLangLedgerName.Text.Trim().Length > 0)
                {
                    //string val = ObjFunction.ChecklLangVal(txtItemName.Text.Trim());
                    //if (val == "")
                    //{
                    frmkb = new Utilities.KeyBoard(1, txtLedgerName.Text.Trim(), txtLangLedgerName.Text, "", "");
                    ObjFunction.OpenForm(frmkb);
                    if (frmkb.DS == DialogResult.OK)
                    {
                        txtLangLedgerName.Text = frmkb.strLanguage.Trim();
                        frmkb.Close();
                    }
                    //}
                    //else
                    //    txtLangFullDesc.Text = val;
                }
                else
                {
                    string val = ObjFunction.ChecklLangVal(txtLedgerName.Text.Trim());
                    if (val == "")
                    {
                        frmkb = new Utilities.KeyBoard(4, txtLedgerName.Text.Trim(), txtLedgerName.Text, "", "");
                        ObjFunction.OpenForm(frmkb);
                        if (frmkb.DS == DialogResult.OK)
                        {
                            txtLangLedgerName.Text = frmkb.strLanguage.Trim();
                            frmkb.Close();
                        }
                    }
                    else
                        txtLangLedgerName.Text = val;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void txtLangLedgerName_Leave(object sender, EventArgs e)
        {
            if (txtLangLedgerName.Text.Trim() == "")
                txtLangLedgerName.Text = ObjFunction.ChecklLangVal(txtLedgerName.Text);
        }

        private void TxtAddress_Leave(object sender, EventArgs e)
        {
            if (FlagBilingual == true)
            {
                //txtLangFullDesc.Focus();
                if (txtLangAdd.Text.Trim().Length == 0)
                {
                    btnLangAdd_Click(btnLangAdd, null);
                }
            }
        }

        private void btnLangAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Utilities.KeyBoard frmkb;
                if (txtLangAdd.Text.Trim().Length > 0)
                {
                    //string val = ObjFunction.ChecklLangVal(txtItemName.Text.Trim());
                    //if (val == "")
                    //{
                    frmkb = new Utilities.KeyBoard(1, txtOAddress.Text.Trim(), txtLangAdd.Text, "", "");
                    ObjFunction.OpenForm(frmkb);
                    if (frmkb.DS == DialogResult.OK)
                    {
                        txtLangAdd.Text = frmkb.strLanguage.Trim();
                        frmkb.Close();
                    }
                    //}
                    //else
                    //    txtLangFullDesc.Text = val;
                }
                else
                {
                    string val = ObjFunction.ChecklLangVal(txtOAddress.Text.Trim());
                    if (val == "")
                    {
                        frmkb = new Utilities.KeyBoard(4, txtOAddress.Text.Trim(), txtOAddress.Text, "", "");
                        ObjFunction.OpenForm(frmkb);
                        if (frmkb.DS == DialogResult.OK)
                        {
                            txtLangAdd.Text = frmkb.strLanguage.Trim();
                            frmkb.Close();
                        }
                    }
                    else
                        txtLangAdd.Text = val;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

        }

        private void txtLangAdd_Leave(object sender, EventArgs e)
        {
            if (txtLangAdd.Text.Trim() == "")
                txtLangAdd.Text = ObjFunction.ChecklLangVal(txtOAddress.Text);

        }

        private void lstBldgName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbOCity_Leave(object sender, EventArgs e)
        {
            cmbOCity_KeyDown(sender, new KeyEventArgs(Keys.Enter));
        }

        private void cmbOArea_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                txtGSTINNo.Focus();
            }
        }

        private void txtGSTINNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                BtnSave.Focus();
            }
        }       
        
    }
}
