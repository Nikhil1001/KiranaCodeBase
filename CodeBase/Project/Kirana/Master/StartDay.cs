﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;


namespace Kirana.Master
{
    /// <summary>
    /// This class is used for Start Day
    /// </summary>
    public partial class StartDay : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        DBMSettings dbMSettings = new DBMSettings();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();
        DateTime StartDate, EndDate;

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public StartDay()
        {
            InitializeComponent();
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            try
            {
                StartDate = dtpDate.Value;
                EndDate = dtpDate.Value.AddDays(-1);
                dbMSettings.AddAppSettings(AppSettings.O_SOD, StartDate.ToString("dd-MMM-yyyy"));
                if (ObjFunction.GetAppSettings(AppSettings.O_EOD) != EndDate.ToString("dd-MMM-yyyy"))
                {
                    dbMSettings.AddAppSettings(AppSettings.O_EOD, EndDate.ToString("dd-MMM-yyyy"));
                }
                if (dbMSettings.ExecuteNonQueryStatements() == true)
                {
                }
                ObjFunction.SetAppSettings();

                //Zip Database Backup
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_SODUploadBackup)) == true)
                {
                    DBAutoBackup dbAuto = new DBAutoBackup();
                    dbAuto.ExportBackUp();
                    this.Close();
                }
                //   ObjTrans.ExecuteQuery("Exec StockUpdateAll", CommonFunctions.ConStr);
                // ObjTrans.ExecuteQuery("Exec SetMismatchVouchers 15,11," + DBGetVal.CompanyNo, CommonFunctions.ConStr);
                //   ObjTrans.ExecuteQuery("Exec SetMismatchVouchers 9,7," + DBGetVal.CompanyNo, CommonFunctions.ConStr);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void StartDay_Load(object sender, EventArgs e)
        {
            dtpDate.Enabled = false;
            BtnOk.Focus();
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            MDIParent1.CloseFlag = true;
            Application.Exit();
        }

        private void CheckMFGCompany()
        {
            DataTable MLedger = ObjFunction.GetDataView(" SELECT DISTINCT TVoucherDetails.LedgerNo FROM  TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo Where (TVoucherEntry.VoucherTypeCode = " + VchType.Purchase + ") AND (TVoucherEntry.VoucherDate ='" + DBGetVal.ServerTime.Date.AddDays(-1) + "' ) AND (TVoucherDetails.VoucherSrNo = 1) And (TVoucherDetails.LedgerNo Not In(" + Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_PartyAC)) + ")) ").Table;
            for (int L = 0; L < MLedger.Rows.Count; L++)
            {
                bool flag = false;
                DBMManufacturerCompany dbMManufacturerCompany = new DBMManufacturerCompany();
                MManufacturerDetails mManufactuerDetails = new MManufacturerDetails();

                string sql = "SELECT DISTINCT MManufacturerCompany.MfgCompNo FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo INNER JOIN TStock ON TVoucherEntry.PkVoucherNo = TStock.FKVoucherNo INNER JOIN " +
                           " MStockItems ON TStock.ItemNo = MStockItems.ItemNo INNER JOIN MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo INNER JOIN MManufacturerCompany ON MStockGroup.MfgCompNo = MManufacturerCompany.MfgCompNo " +
                           " WHERE (TVoucherEntry.VoucherTypeCode = " + VchType.Purchase + ") AND (TVoucherEntry.VoucherDate ='" + DBGetVal.ServerTime.Date.AddDays(-1) + "' ) AND (TVoucherDetails.VoucherSrNo = 1) AND  (TVoucherDetails.LedgerNo = " + Convert.ToInt64(MLedger.Rows[L].ItemArray[0].ToString()) + ") " +
                           " And MManufacturerCompany.MfgCompNo Not In(Select ManufacturerNo From MManufacturerDetails  Where LedgerNo = " + Convert.ToInt64(MLedger.Rows[L].ItemArray[0].ToString()) + ") ";


                DataTable dt = new DataTable();
                dt = ObjFunction.GetDataView(sql).Table;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    mManufactuerDetails = new MManufacturerDetails();
                    mManufactuerDetails.LedgerNo = Convert.ToInt64(MLedger.Rows[L].ItemArray[0].ToString());
                    mManufactuerDetails.PkSrNo = 0;
                    mManufactuerDetails.ManufacturerNo = Convert.ToInt64(dt.Rows[i].ItemArray[0].ToString());
                    mManufactuerDetails.UserID = DBGetVal.UserID;
                    mManufactuerDetails.CompanyNo = DBGetVal.CompanyNo;
                    mManufactuerDetails.UserID = DBGetVal.UserID;
                    mManufactuerDetails.UserDate = DBGetVal.ServerTime.Date;
                    dbMManufacturerCompany.AddMManufacturerDetails(mManufactuerDetails);
                    flag = true;
                }
                if (flag == true)
                {
                    dbMManufacturerCompany.ExecuteNonQueryStatements();
                }
            }
        }

        #region Update Mismatch auto receipts
        private void MatchingVouchers()
        {

            DataTable dtUpdateDiff = new DataTable();
            dtUpdateDiff.Clear();
            dtUpdateDiff = ObjFunction.GetDataView("Select LedgerNo,VoucherDate,Sum(DayTotAmt) as Diff, Sum(rType) as totRType, Max(PkVoucherNo) FROM " +
                " (SELECT     TVoucherDetails.LedgerNo, TVoucherEntry.VoucherDate, SUM(TVoucherDetails.Debit) AS DayTotAmt, 1 as rType, 0 as PkVoucherNo " +
                " FROM         TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo " +
                " WHERE     (TVoucherDetails.SrNo = 501) AND (TVoucherEntry.IsCancel = 'False') AND (TVoucherEntry.VoucherTypeCode = 15) AND (TVoucherEntry.PayTypeNo = 2) " +
                " GROUP BY TVoucherDetails.LedgerNo, TVoucherEntry.VoucherDate " +
                " UNION ALL " +
                " SELECT     TVoucherDetails.LedgerNo, TVoucherEntry.VoucherDate, SUM(TVoucherDetails.Credit) * - 1 AS DayTotAmt, 2 as rType, MIN(TVoucherEntry.PkVoucherNo) as PkVoucherNo " +
                " FROM         TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo " +
                " WHERE     (TVoucherDetails.SrNo = 501) AND (TVoucherEntry.IsCancel = 'False') AND (TVoucherEntry.VoucherTypeCode = 11) AND (TVoucherEntry.PayTypeNo = 2) " +
                " GROUP BY TVoucherDetails.LedgerNo, TVoucherEntry.VoucherDate " +
                " ) As Tbl1 GROUP BY LedgerNo,VoucherDate " +
                " HAVING Sum(DayTotAmt) <> 0 ORDER BY LedgerNo,VoucherDate", CommonFunctions.ConStr).Table;



            long CashLedgerNo = 0, VoucherUserNo = 0;
            if (dtUpdateDiff.Rows.Count > 0)
            {
                //fetch ledgerno for 2
                long tempId = ObjQry.ReturnLong("select TOP 1 PKVoucherNo from tvoucherentry where vouchertypecode = 11 ORDER BY PkVoucherNo DESC", CommonFunctions.ConStr);
                CashLedgerNo = ObjQry.ReturnLong("select LedgerNo from tvoucherdetails where fkvoucherno = " + tempId + " and VoucherSrNo=2 and Debit!=0 and srno=0", CommonFunctions.ConStr);
            }

            for (int i = 0; i < dtUpdateDiff.Rows.Count; i++)
            {
                string sql = "";
                switch (Convert.ToInt16(dtUpdateDiff.Rows[i].ItemArray[3].ToString()))
                {
                    case 1:
                        VoucherUserNo = ObjQry.ReturnLong("select max(VoucherUSerNo) From TVoucherEntry Where VoucherTypeCode=11", CommonFunctions.ConStr);
                        VoucherUserNo++;
                        sql = " Insert into TVoucherEntry(VoucherTypeCode,VoucherUserNo,VoucherDate,VoucherTime,Narration,Reference,ChequeNo,CompanyNo, " +
                              " BilledAmount,ChallanNo,Remark,InwardLocationCode,MacNo,IsCancel,PayTypeNo,RateTypeNo,TaxTypeNo,IsVoucherLock,VoucherStatus,UserID,UserDate,ModifiedBy, " +
                              " OrderType,ReturnAmount,Visibility,DiscPercent,DiscAmt) " +
                              " Values(11," + VoucherUserNo + ",'" + dtUpdateDiff.Rows[i].ItemArray[1].ToString() + "','" + dtUpdateDiff.Rows[i].ItemArray[1].ToString() + "','Receipt Bill','',0," + DBGetVal.CompanyNo + "," + dtUpdateDiff.Rows[i].ItemArray[2].ToString() + ", " +
                              " '','',0,0,0,2,0,0,0,3,1,'" + dtUpdateDiff.Rows[i].ItemArray[1].ToString() + "',NULL,2,0,0,0,0)";
                        ObjTrans.Execute(sql, CommonFunctions.ConStr);
                        if (ObjTrans.ErrorMessage != "")
                        {
                            MessageBox.Show("Error In Cash Receipt");
                            return;
                        }
                        long PkVoucherTrnNo = ObjQry.ReturnLong("select max(PkVoucherNo) From TVoucherEntry", CommonFunctions.ConStr);

                        sql = "Insert Into TVoucherDetails (FkVoucherNo,VoucherSrNo,SignCode,LedgerNo,Debit,Credit,SrNo,CompanyNo,Narration) Values(" + PkVoucherTrnNo + ",1, " +
                              " 2," + dtUpdateDiff.Rows[i].ItemArray[0].ToString() + ",0," + dtUpdateDiff.Rows[i].ItemArray[2].ToString() + ",501," + DBGetVal.CompanyNo + ",'')";

                        ObjTrans.Execute(sql, CommonFunctions.ConStr);
                        sql = "Insert Into TVoucherDetails (FkVoucherNo,VoucherSrNo,SignCode,LedgerNo,Debit,Credit,SrNo,CompanyNo,Narration) Values(" + PkVoucherTrnNo + ",2, " +
                              " 1," + CashLedgerNo + "," + dtUpdateDiff.Rows[i].ItemArray[2].ToString() + ",0,0," + DBGetVal.CompanyNo + ",'')";

                        ObjTrans.Execute(sql, CommonFunctions.ConStr);
                        if (ObjTrans.ErrorMessage != "")
                        {
                            MessageBox.Show("Error In Cash Receipt");
                            return;
                        }
                        break;
                    case 2:
                        sql = "DELETE FROM TVoucherDetails WHERE FkVoucherNo = " + dtUpdateDiff.Rows[i].ItemArray[4].ToString() + "";
                        ObjTrans.Execute(sql, CommonFunctions.ConStr);
                        if (ObjTrans.ErrorMessage != "")
                        {
                            MessageBox.Show("Error In Cash Receipt");
                            return;
                        }
                        sql = "DELETE FROM TVoucherEntry WHERE PkVoucherNo = " + dtUpdateDiff.Rows[i].ItemArray[4].ToString() + "";
                        ObjTrans.Execute(sql, CommonFunctions.ConStr);
                        if (ObjTrans.ErrorMessage != "")
                        {
                            MessageBox.Show("Error In Cash Receipt");
                            return;
                        }
                        break;
                    case 3:
                        sql = "UPDATE TVoucherDetails SET Credit = Credit + " + dtUpdateDiff.Rows[i].ItemArray[2].ToString() +
                                    " Where FkVoucherNo = " + dtUpdateDiff.Rows[i].ItemArray[4].ToString() +
                                    " AND SrNo = 501 AND vouchersrno = 1";

                        ObjTrans.Execute(sql, CommonFunctions.ConStr);
                        if (ObjTrans.ErrorMessage != "")
                        {
                            MessageBox.Show("Error In Cash Receipt");
                            return;
                        }
                        sql = "UPDATE TVoucherDetails SET Debit = Debit + " + dtUpdateDiff.Rows[i].ItemArray[2].ToString() +
                                    " Where FkVoucherNo = " + dtUpdateDiff.Rows[i].ItemArray[4].ToString() +
                                    " AND SrNo = 0 AND vouchersrno = 2";

                        ObjTrans.Execute(sql, CommonFunctions.ConStr);
                        if (ObjTrans.ErrorMessage != "")
                        {
                            MessageBox.Show("Error In Cash Receipt");
                            return;
                        }
                        sql = "UPDATE TVoucherEntry SET BilledAmount = BilledAmount + " + dtUpdateDiff.Rows[i].ItemArray[2].ToString() +
                                    " Where PkVoucherNo = " + dtUpdateDiff.Rows[i].ItemArray[4].ToString() +
                                    " ";

                        ObjTrans.Execute(sql, CommonFunctions.ConStr);
                        if (ObjTrans.ErrorMessage != "")
                        {
                            MessageBox.Show("Error In Cash Receipt");
                            return;
                        }
                        break;
                }
            }

        #endregion
        }
    }
}
