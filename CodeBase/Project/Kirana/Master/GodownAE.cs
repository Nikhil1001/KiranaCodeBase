﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Master
{
    /// <summary>
    /// This class is used for GodownAE
    /// </summary>
    public partial class GodownAE : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        DBMGodown dbGodown = new DBMGodown();
        MGodown mGodown = new MGodown();
        string GodownNm;
        DataTable dtSearch = new DataTable();
        int cntRow;
        long ID;

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public GodownAE()
        {
            InitializeComponent();
        }

        private void GodownAE_Load(object sender, EventArgs e)
        {
            try
            {
                ObjFunction.LockButtons(true, this.Controls);
                ObjFunction.LockControls(false, this.Controls);

                ObjFunction.FillCombo(cmbLocation, "Select LocationNo,LocationName From MLocation Where IsActive = 'True' ");
                //ObjFunction.FillCombo(cmbControlGroup, "Select GodownNo,GodownName From MGodown where LocationNo="+ObjFunction.GetComboValue(cmbLocation)+"");
                ObjFunction.FillCombo(cmbControlGroup, "Select GodownNo,GodownName From MGodown Where IsActive = 'True' ");

                ObjFunction.LockButtons(true, this.Controls);
                ObjFunction.LockControls(false, this.Controls);

                GodownNm = "";
                dtSearch = ObjFunction.GetDataView("Select GodownNo From MGodown Where GodownNo <> 1 order by GodownName").Table;

                if (dtSearch.Rows.Count > 0)
                {
                    if (Godown.RequestGodownNo == 0)
                        ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    else
                        ID = Godown.RequestGodownNo;
                    FillControls();
                    SetNavigation();
                }
                setDisplay(true);
                btnNew.Focus();
                KeyDownFormat(this.Controls);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

 
        private void FillControls()
        {
            try
            {
                EP.SetError(txtGodownName, "");
                EP.SetError(cmbLocation, "");
                EP.SetError(cmbControlGroup, "");


                mGodown = dbGodown.ModifyMGodownByID(ID);
                GodownNm = mGodown.GodownName;
                txtGodownName.Text = mGodown.GodownName;
                chkActive.Checked = mGodown.IsActive;
                if (chkActive.Checked == true)
                    chkActive.Text = "Yes";
                else
                    chkActive.Text = "No";
                ObjFunction.FillCombo(cmbLocation, "Select LocationNo,LocationName From MLocation Where (IsActive = 'True' OR LocationNo = " + mGodown.LocationNo + ")");
                ObjFunction.FillCombo(cmbControlGroup, "Select GodownNo,GodownName From MGodown Where (IsActive = 'True' OR GodownNo=" + mGodown.ControlGroup + ")");
                cmbLocation.SelectedValue = mGodown.LocationNo.ToString();
                cmbControlGroup.SelectedValue = mGodown.ControlGroup.ToString();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
    
        }

        private void SetValue()
        {
            try
            {
                if (Validations() == true)
                {
                    dbGodown = new DBMGodown();
                    mGodown = new MGodown();

                    mGodown.GodownNo = ID;
                    mGodown.GodownName = txtGodownName.Text.Trim().ToUpper();
                    mGodown.LocationNo = Convert.ToInt64(cmbLocation.SelectedValue);
                    mGodown.ControlGroup = Convert.ToInt64(cmbControlGroup.SelectedValue);
                    mGodown.IsActive = chkActive.Checked;
                    mGodown.UserID = DBGetVal.UserID;
                    mGodown.UserDate = DBGetVal.ServerTime.Date;
                    mGodown.CompanyNo = DBGetVal.CompanyNo;

                    if (dbGodown.AddMGodown(mGodown) == true)
                    {
                        if (ID == 0)
                        {
                            OMMessageBox.Show("Godown Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            dtSearch = ObjFunction.GetDataView("Select GodownNo From MGodown Where GodownNo <> 1 order by GodownName").Table;
                            ID = ObjQry.ReturnLong("Select Max(GodownNo) FRom MGodown", CommonFunctions.ConStr);
                            SetNavigation();
                            FillControls();
                        }
                        else
                        {
                            OMMessageBox.Show("Godown Updated Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            FillControls();
                        }

                        ObjFunction.LockButtons(true, this.Controls);
                        ObjFunction.LockControls(false, this.Controls);
                    }
                    else
                    {
                        OMMessageBox.Show("Godown not saved", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            Godown.RequestGodownNo = 0;
            this.Close();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            SetValue();
        }

        private bool Validations()
        {
            bool flag = false;
            EP.SetError(txtGodownName, ""); 
            EP.SetError(cmbLocation, "");
            EP.SetError(cmbControlGroup, "");
            if (txtGodownName.Text.Trim() == "")
            {
                EP.SetError(txtGodownName, "Enter Godown Name");
                EP.SetIconAlignment(txtGodownName, ErrorIconAlignment.MiddleRight);
                txtGodownName.Focus();
            }
            else if (ObjFunction.GetComboValue(cmbLocation) <= 0)
            {
                EP.SetError(cmbLocation, "Select State Name");
                EP.SetIconAlignment(cmbLocation, ErrorIconAlignment.MiddleRight);
                cmbLocation.Focus();
            }
            else if (ObjFunction.GetComboValue(cmbControlGroup) <= 0)
            {
                EP.SetError(cmbControlGroup, "Select City Name");
                EP.SetIconAlignment(cmbControlGroup, ErrorIconAlignment.MiddleRight);
                cmbControlGroup.Focus();
            }
            else if (GodownNm.ToUpper() != txtGodownName.Text.Trim().ToUpper())
            {
                if (ObjQry.ReturnInteger("Select Count(*) from MGodown where GodownName = '" + txtGodownName.Text.Trim().Replace("'", "''") + "'", CommonFunctions.ConStr) != 0)
                {
                    EP.SetError(txtGodownName, "Duplicate Godown Name");
                    EP.SetIconAlignment(txtGodownName, ErrorIconAlignment.MiddleRight);
                    txtGodownName.Focus();
                    
                }
                else
                    flag = true;
            }
            else
                flag = true;
            return flag;
        }

        private void GodownAE_FormClosing(object sender, FormClosingEventArgs e)
        {
            ID = 0;
            GodownNm = "";
        }

        
        #region Navigation Methods
        private void NavigationDisplay(int type)
        {
            try
            {
                long No = 0;
                if (type == 5)
                {
                    No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                    ID = No;
                }
                else if (type == 1)
                {
                    No = Convert.ToInt64(dtSearch.Rows[0].ItemArray[0].ToString());
                    cntRow = 0;
                    ID = No;
                }
                else if (type == 2)
                {
                    No = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    cntRow = dtSearch.Rows.Count - 1;
                    ID = No;
                }
                else
                {
                    if (type == 3)
                    {
                        cntRow = cntRow + 1;
                    }
                    else if (type == 4)
                    {
                        cntRow = cntRow - 1;
                    }

                    if (cntRow < 0)
                    {
                        OMMessageBox.Show("This is First Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow + 1;
                    }
                    else if (cntRow > dtSearch.Rows.Count - 1)
                    {
                        OMMessageBox.Show("This is Last Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow - 1;
                    }
                    else
                    {
                        No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                        ID = No;
                    }

                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
            FillControls();

        }

        private void SetNavigation()
        {
            cntRow = 0;
            for (int i = 0; i < dtSearch.Rows.Count; i++)
            {
                if (Convert.ToInt64(dtSearch.Rows[i].ItemArray[0].ToString()) == ID)
                {
                    cntRow = i;
                    break;
                }
            }
        }

        private void setDisplay(bool flag)
        {
            btnFirst.Visible = flag;
            btnPrev.Visible = flag;
            btnNext.Visible = flag;
            btnLast.Visible = flag;
            btnDelete.Visible = flag;
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            NavigationDisplay(1);
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            NavigationDisplay(4);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            NavigationDisplay(3);
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            NavigationDisplay(2);
        }

        #endregion

        #region KeyDown Events
        private void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is GroupBox)
                    KeyDownFormat(ctrl.Controls);
            }
        }

        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left && e.Control)
            {
                if (btnPrev.Enabled) btnPrev_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Up && e.Control)
            {
                if (btnFirst.Enabled) btnFirst_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Right && e.Control)
            {
                if (btnNext.Enabled) btnNext_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Down && e.Control)
            {
                if (btnLast.Enabled) btnLast_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F2)
            {
                if (BtnSave.Visible) BtnSave_Click(sender, e);
            }
        }
        #endregion

        private void chkActive_CheckedChanged(object sender, EventArgs e)
        {
            if (chkActive.Checked == true)
                chkActive.Text = "Yes";
            else
                chkActive.Text = "No";
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (chkActive.Checked == false) return;
                dbGodown = new DBMGodown();
                mGodown = new MGodown();

                mGodown.GodownNo = ID;
                if (OMMessageBox.Show("Are you sure want to delete this record?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (dbGodown.DeleteMGodown(mGodown) == true)
                    {
                        OMMessageBox.Show("Godown Deleted Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        FillControls();
                        //Form NewF = new Godown();
                        //this.Close();
                        //ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                    }
                    else
                    {
                        OMMessageBox.Show("Godown not Deleted", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            Form NewF = new Godown();
            this.Close();
            ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                ID = 0;
                GodownNm = "";
                ObjFunction.InitialiseControl(this.Controls);
                ObjFunction.LockButtons(false, this.Controls);
                ObjFunction.LockControls(true, this.Controls);

                ObjFunction.FillCombo(cmbLocation, "Select LocationNo,LocationName From MLocation  Where IsActive = 'True' ");
                ObjFunction.FillCombo(cmbControlGroup, "Select GodownNo,GodownName From MGodown  Where IsActive = 'True' ");

                txtGodownName.Focus();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            NavigationDisplay(5);

            ObjFunction.LockButtons(true, this.Controls);
            ObjFunction.LockControls(false, this.Controls);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            ObjFunction.LockButtons(false, this.Controls);
            ObjFunction.LockControls(true, this.Controls);
            txtGodownName.Focus();
        }

        private void txtGodownName_Leave_1(object sender, EventArgs e)
        {
            try
            {
                EP.SetError(txtGodownName, "");
                if (txtGodownName.Text.Trim() != "")
                {
                    if (GodownNm.ToUpper() != txtGodownName.Text.Trim().ToUpper())
                    {
                        if (ObjQry.ReturnInteger("Select Count(*) from MGodown where GodownName = '" + txtGodownName.Text.Trim().Replace("'", "''") + "'", CommonFunctions.ConStr) != 0)
                        {
                            EP.SetError(txtGodownName, "Duplicate Godown Name");
                            EP.SetIconAlignment(txtGodownName, ErrorIconAlignment.MiddleRight);
                            txtGodownName.Focus();

                        }
                        else
                            cmbLocation.Focus();
                    }

                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }
    }
}
