﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;
using System.Data.OleDb;

namespace Kirana.Master
{
    public partial class GSTPercentage : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();
        public DialogResult DS = DialogResult.OK;

        public DataTable dt_HSN = null;

        public GSTPercentage()
        {
            InitializeComponent();
        }

        private void GSTPercentage_Load(object sender, EventArgs e)
        {
            ObjFunction.FillCombo(cmbGSTPercent, "SELECT MItemTaxSetting.PkSrNo, " +
                        " (cast(MItemTaxSetting.Percentage as varchar)+ ' %') as Percentage " +
                        " FROM MItemTaxSetting " +
                        " WHERE (MItemTaxSetting.TransactionTypeNo = " + GroupType.SalesAccount + ") " +
                            " AND (MItemTaxSetting.TaxTypeNo = " + GroupType.GST + " AND MItemTaxSetting.IsActive='True') " +
                            " ");

        }

        public bool Validations()
        {
            bool flag = false;
            EP.SetError(cmbGSTPercent, "");
            EP.SetError(TxtHSNNo, "");
            EP.SetError(TxtHSNDesc, "");
            EP.SetError(txtIGST, "");
            EP.SetError(txtCGST, "");
            EP.SetError(txtSGST, "");
            EP.SetError(txtUTGST, "");

            if (ObjFunction.GetComboValue(cmbGSTPercent) == 0)
            {
                EP.SetError(cmbGSTPercent, "Select Tax %");
                EP.SetIconAlignment(cmbGSTPercent, ErrorIconAlignment.MiddleRight);
                cmbGSTPercent.Focus();
            }
            else if (TxtHSNNo.Text.Trim() == "")
            {
                EP.SetError(TxtHSNNo, "Enter HSN Code");
                EP.SetIconAlignment(TxtHSNNo, ErrorIconAlignment.MiddleRight);
                TxtHSNNo.Focus();
            }
            else if (ObjQry.ReturnLong("Select ISNULL( HSNNO,0) From MStockHSNCode Where HSNCode=" + TxtHSNNo.Text.Trim() + " ", CommonFunctions.ConStr) != 0)
            {
                EP.SetError(TxtHSNNo, "Duplicate HSN Code");
                EP.SetIconAlignment(TxtHSNNo, ErrorIconAlignment.MiddleRight);
                TxtHSNNo.Focus();
            }
            else if (TxtHSNDesc.Text.Trim() == "")
            {
                EP.SetError(TxtHSNNo, "Enter HSN Decription");
                EP.SetIconAlignment(TxtHSNNo, ErrorIconAlignment.MiddleRight);
                TxtHSNNo.Focus();
            }
            else if (txtIGST.Text.Trim() == "")
            {
                EP.SetError(txtIGST, "Enter IGST");
                EP.SetIconAlignment(txtIGST, ErrorIconAlignment.MiddleRight);
                txtIGST.Focus();
            }
            else if (txtCGST.Text.Trim() == "")
            {
                EP.SetError(txtCGST, "Enter CGST");
                EP.SetIconAlignment(txtCGST, ErrorIconAlignment.MiddleRight);
                txtCGST.Focus();
            }
            else if (txtSGST.Text.Trim() == "")
            {
                EP.SetError(txtSGST, "Enter SGST");
                EP.SetIconAlignment(txtSGST, ErrorIconAlignment.MiddleRight);
                txtSGST.Focus();
            }
            else if (txtUTGST.Text.Trim() == "")
            {
                EP.SetError(txtUTGST, "Enter UGST");
                EP.SetIconAlignment(txtUTGST, ErrorIconAlignment.MiddleRight);
                txtUTGST.Focus();
            }
            else flag = true;

            return flag;
        }

        private void txtPercentage_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked((TextBox)sender, 2, 3);
        }

        private void cmbGSTPercent_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                fillGstTaxDetails(ObjFunction.GetComboValue(cmbGSTPercent));
                BindGrid(ObjFunction.GetComboValue(cmbGSTPercent));
                if (txtCGST.ReadOnly == true)
                {
                    TxtHSNNo.Focus();
                }
                else
                {
                    txtIGST.Focus();
                    double TaxPer = Convert.ToDouble(cmbGSTPercent.Text.Replace("%", "").Trim());
                    if (TaxPer != 0)
                    {
                        txtCGST.Text = (TaxPer / 2).ToString("0.00");
                        txtSGST.Text = (TaxPer / 2).ToString("0.00");
                    }

                }
            }
        }

        private void fillGstTaxDetails(long GstTaxPercentNo_S)
        {

            DataTable dtItemTax = ObjFunction.GetDataView("Select TOP 1 * From MStockGSTTaxDetails Where FkTaxSettingNo = " + GstTaxPercentNo_S).Table;
            if (dtItemTax.Rows.Count == 0)
            {
                txtIGST.Text = "";// Format.DoubleFloating;
                txtCGST.Text = "";// Format.DoubleFloating;
                txtSGST.Text = "";// Format.DoubleFloating;
                txtUTGST.Text = "";// Format.DoubleFloating;
                SetFlag(false);
                return;
            }
            txtIGST.Text = Convert.ToDouble(dtItemTax.Rows[0]["IGSTPercent"].ToString()).ToString(Format.DoubleFloating);
            txtCGST.Text = Convert.ToDouble(dtItemTax.Rows[0]["CGSTPercent"].ToString()).ToString(Format.DoubleFloating);
            txtSGST.Text = Convert.ToDouble(dtItemTax.Rows[0]["SGSTPercent"].ToString()).ToString(Format.DoubleFloating);
            txtUTGST.Text = Convert.ToDouble(dtItemTax.Rows[0]["UTGSTPercent"].ToString()).ToString(Format.DoubleFloating);
            SetFlag(true);

        }

        private void SetFlag(bool flag)
        {
            txtIGST.ReadOnly = flag;
            txtCGST.ReadOnly = flag;
            txtSGST.ReadOnly = flag;
            txtUTGST.ReadOnly = flag;
        }

        private void BindGrid(long GstTaxPercentNo_S)
        {
            while (dgDetails.Rows.Count > 0)
                dgDetails.Rows.RemoveAt(0);

            string Sql = " SELECT MStockHSNCode.HSNCode, MStockHSNCode.HSNDescription, MStockHSNCode.HSNNo " +
                " FROM MStockGSTTaxDetails INNER JOIN MStockHSNCode ON MStockGSTTaxDetails.HSNNo = MStockHSNCode.HSNNo " +
                " Where FkTaxSettingNo =  " + GstTaxPercentNo_S;
            DataTable dtItemTax = ObjFunction.GetDataView(Sql).Table;
            for (int i = 0; i < dtItemTax.Rows.Count; i++)
            {
                dgDetails.Rows.Add();
                dgDetails.Rows[i].Cells[0].Value = i + 1;
                dgDetails.Rows[i].Cells[1].Value = dtItemTax.Rows[i]["HSNCode"].ToString();
                dgDetails.Rows[i].Cells[2].Value = dtItemTax.Rows[i]["HSNDescription"].ToString();
                dgDetails.Rows[i].Cells[3].Value = dtItemTax.Rows[i]["HSNNo"].ToString();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked((TextBox)sender, -1, 8);
        }

        private void btnAddHSN_Click(object sender, EventArgs e)
        {
            if (Validations())
            {

                DBMStockGSTTaxDetails dbStockSGT = new DBMStockGSTTaxDetails();

                double TaxPer = Convert.ToDouble(cmbGSTPercent.Text.Replace("%", "").Trim());
                MStockHSNCode mHSNCode = new MStockHSNCode();
                mHSNCode.HSNNo = 0;
                mHSNCode.HSNCode = TxtHSNNo.Text.Trim();
                mHSNCode.HSNDescription = TxtHSNDesc.Text.Trim();
                mHSNCode.IsActive = true;
                dbStockSGT.AddMStockHSNCode(mHSNCode);
                //For Sale
                MStockGSTTaxDetails GSTTaxDlts = new MStockGSTTaxDetails();
                GSTTaxDlts.TaxDetailsNo = 0;
                GSTTaxDlts.StateCode = "27";
                GSTTaxDlts.HSNCode = TxtHSNNo.Text.Trim();
                GSTTaxDlts.TotalPercent = TaxPer;
                GSTTaxDlts.IGSTPercent = Convert.ToDouble(txtIGST.Text);
                GSTTaxDlts.CGSTPercent = Convert.ToDouble(txtCGST.Text);
                GSTTaxDlts.SGSTPercent = Convert.ToDouble(txtSGST.Text);
                GSTTaxDlts.UTGSTPercent = Convert.ToDouble(txtUTGST.Text);
                GSTTaxDlts.CessPercent = 0;
                GSTTaxDlts.FkTaxSettingNo = ObjFunction.GetComboValue(cmbGSTPercent);
                GSTTaxDlts.FromDate = DateTime.Now.Date;
                GSTTaxDlts.IsActive = true;
                GSTTaxDlts.UserID = DBGetVal.UserID;
                GSTTaxDlts.UserDate = DBGetVal.ServerTime;
                dbStockSGT.AddMStockGstTaxDetails(GSTTaxDlts);

                //For Purchase
                GSTTaxDlts = new MStockGSTTaxDetails();
                GSTTaxDlts.TaxDetailsNo = 0;
                GSTTaxDlts.StateCode = "27";
                GSTTaxDlts.HSNCode = TxtHSNNo.Text.Trim();
                GSTTaxDlts.TotalPercent = TaxPer;
                GSTTaxDlts.IGSTPercent = Convert.ToDouble(txtIGST.Text);
                GSTTaxDlts.CGSTPercent = Convert.ToDouble(txtCGST.Text);
                GSTTaxDlts.SGSTPercent = Convert.ToDouble(txtSGST.Text);
                GSTTaxDlts.UTGSTPercent = Convert.ToDouble(txtUTGST.Text);
                GSTTaxDlts.CessPercent = 0;

                string Str = " SELECT ISNULL(PkSrNo,0) " +
                    " FROM MItemTaxSetting  " +
                    " WHERE (TransactionTypeNo = " + GroupType.PurchaseAccount + ") " +
                    " AND (TaxTypeNo = " + GroupType.GST + " )" +
                    " AND (Percentage=" + TaxPer + ") " +
                    " AND (IsActive='True')  ";

                GSTTaxDlts.FkTaxSettingNo = ObjQry.ReturnLong(Str, CommonFunctions.ConStr);
                GSTTaxDlts.FromDate = DateTime.Now.Date;
                GSTTaxDlts.IsActive = true;
                GSTTaxDlts.UserID = DBGetVal.UserID;
                GSTTaxDlts.UserDate = DBGetVal.ServerTime;
                dbStockSGT.AddMStockGstTaxDetails(GSTTaxDlts);

                if (dbStockSGT.ExecuteNonQueryStatements())
                {
                    OMMessageBox.Show(" GST Tax Details Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                    fillGstTaxDetails(ObjFunction.GetComboValue(cmbGSTPercent));
                    BindGrid(ObjFunction.GetComboValue(cmbGSTPercent));
                    TxtHSNNo.Text = "";
                    TxtHSNDesc.Text = "";
                }
                else
                {
                    OMMessageBox.Show(" GST Tax not saved", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                }

            }
        }

        private void txtIGST_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked((TextBox)sender, 2, 3);
        }

        private void btnPurVat_Click(object sender, EventArgs e)
        {
            try
            {
                long SalesVatNo = 0;
                Form NewF = new Master.PercentageVAT(GroupType.SalesAccount, GroupType.GST);
                ObjFunction.OpenForm(NewF);
                if (((PercentageVAT)NewF).DS == DialogResult.OK)
                {
                    SalesVatNo = ObjFunction.GetComboValue(cmbGSTPercent);
                    ObjFunction.FillCombo(cmbGSTPercent, "SELECT MItemTaxSetting.PkSrNo, (cast(MItemTaxSetting.Percentage as varchar)+ ' %') as Percentage FROM MItemTaxSetting INNER JOIN MLedger ON MItemTaxSetting.SalesLedgerNo = MLedger.LedgerNo INNER JOIN " +
                        "   MLedger AS MLedger_1 ON MItemTaxSetting.TaxLedgerNo = MLedger_1.LedgerNo " +
                        " WHERE     (MLedger.GroupNo = " + GroupType.SalesAccount + ") AND (MLedger_1.GroupNo = " + GroupType.GST + ") And MItemTaxSetting.IsActive='True' Order by  MItemTaxSetting.Percentage ");
                    cmbGSTPercent.SelectedValue = SalesVatNo;
                }
                cmbGSTPercent.Focus();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);//For Common Error Displayed Purpose
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            DS = DialogResult.Cancel;
            this.Close();
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Excel Files (*.xls)|*.xls";
            ofd.Title = "Please select an Excel file to import HSN Code Master Data.";
            ofd.Multiselect = false;

            if (ofd.ShowDialog(this) == DialogResult.OK)
            {

                Display.WorkInProgress wip = new Kirana.Display.WorkInProgress();
                wip.StrStatus = "Import HSN Code data, please wait.";
                wip.argument = ofd.FileName;
                wip.bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork_ExcelImportData);
                wip.bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted_ExcelImportData);
                ObjFunction.OpenForm(wip);
            }
        }

        private void bgWorker_DoWork_ExcelImportData(object sender, DoWorkEventArgs e)
        {
            ImportHSNCode(e.Argument.ToString());
        }

        private void bgWorker_RunWorkerCompleted_ExcelImportData(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                OMMessageBox.Show("Error : " + e.Error.Message + Environment.NewLine + "Details : " + e.Error.ToString());
            }
            else
            {
                OMMessageBox.Show("Data Import file completed...");
            }
        }

        #region HSC Code Data Save Methodes

        private void ImportHSNCode(string StrPath)
        {
            try
            {
                copyCustomerDataFromExcel(StrPath);

                Save_Data();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

        }

        private void copyCustomerDataFromExcel(string strFileName)
        {
            string[,] strColumnMapping = new string[,] {{ "HSN_CODE", "HSN_CODE" }, 
                                                        { "HSN_DESCRIPTION", "HSN_DESCRIPTION" },                                             
                                                        { "TAX_PERCENT", "TAX_PERCENT" }};

            OleDbConnection olecon = null;
            try
            {
                string connstring;
                if (strFileName.EndsWith("xls", StringComparison.CurrentCultureIgnoreCase))
                {
                    connstring = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                           "Data Source=" + strFileName + ";Extended Properties=\"Excel 8.0; HDR=Yes; IMEX=1;\";";
                }
                else if (strFileName.EndsWith("xlsx", StringComparison.CurrentCultureIgnoreCase))
                {
                    connstring = "Provider=Microsoft.ACE.OLEDB.12.0;" +
                           "Data Source=" + strFileName + ";Extended Properties=\"Excel 12.0 xml; HDR=Yes; IMEX=1;\";";
                }
                else
                {
                    throw new Exception("Invalid file format or extention. Only *.xls and *.xlsx files supported.");
                }

                olecon = new OleDbConnection(connstring);
                olecon.Open();

                DataTable schemaTable = olecon.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                string strTableName = "[" + schemaTable.Rows[0].ItemArray[2].ToString() + "]";
                string strInputColumnNames = "";

                for (int i = 0; i < strColumnMapping.GetLength(0); i++)
                {
                    strInputColumnNames += "," + strColumnMapping[i, 0];
                }

                strInputColumnNames = strInputColumnNames.Substring(1);

                OleDbDataAdapter olecomm1 = new OleDbDataAdapter("SELECT " + strInputColumnNames + " FROM " + strTableName, olecon);

                DataSet ds = new DataSet();
                olecomm1.Fill(ds);

                dt_HSN = ds.Tables[0];

                if (dt_HSN.Columns.Count != 3)
                {
                    throw new System.Data.TypedDataSetGeneratorException("Input data Column count doesn't match");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (olecon != null) olecon.Close();
            }
        }

        private void Save_Data()
        {
            for (int i = 0; i < dt_HSN.Rows.Count; i++)
            {
                if (CheckValue(dt_HSN.Rows[i]["TAX_PERCENT"].ToString()))
                {
                    long PkNo = ObjQry.ReturnLong("Select ISNULL(HSNNo,0)  From MStockHSNCode Where HSNCode='" + dt_HSN.Rows[i]["HSN_CODE"].ToString() + "' ", CommonFunctions.ConStr);
                    if (PkNo == 0)
                    {
                        DBMStockGSTTaxDetails dbMStockGstTaxDetails = new DBMStockGSTTaxDetails();

                        #region For sales
                        MStockGSTTaxDetails mStockGstTaxDetails = new MStockGSTTaxDetails();
                        mStockGstTaxDetails.TaxDetailsNo = 0;
                        mStockGstTaxDetails.StateCode = "27";
                        mStockGstTaxDetails.HSNCode = dt_HSN.Rows[i]["HSN_CODE"].ToString();
                        mStockGstTaxDetails.HSNNo = dbMStockGstTaxDetails.getHSNNo(dt_HSN.Rows[i]["HSN_CODE"].ToString(), dt_HSN.Rows[i]["HSN_DESCRIPTION"].ToString());
                        mStockGstTaxDetails.TotalPercent = Convert.ToDouble(dt_HSN.Rows[i]["TAX_PERCENT"].ToString());
                        mStockGstTaxDetails.IGSTPercent = mStockGstTaxDetails.TotalPercent;
                        mStockGstTaxDetails.CGSTPercent = (mStockGstTaxDetails.TotalPercent == 0) ? 0 : mStockGstTaxDetails.TotalPercent / 2;
                        mStockGstTaxDetails.SGSTPercent = (mStockGstTaxDetails.TotalPercent == 0) ? 0 : mStockGstTaxDetails.TotalPercent / 2;
                        mStockGstTaxDetails.UTGSTPercent = 0;
                        mStockGstTaxDetails.CessPercent = 0;
                        mStockGstTaxDetails.FkTaxSettingNo = getTaxSettingNo(mStockGstTaxDetails.TotalPercent, GroupType.SalesAccount);
                        mStockGstTaxDetails.FromDate = DateTime.Now.Date;
                        mStockGstTaxDetails.IsActive = true;
                        mStockGstTaxDetails.UserID = 0;
                        mStockGstTaxDetails.UserDate = DateTime.Now;

                        if (!dbMStockGstTaxDetails.AddMStockGstTaxDetails1(mStockGstTaxDetails))
                        {
                            throw new Exception("Error while saving data. GST TAX details :: \"HSN Code\" = \"" + mStockGstTaxDetails.HSNCode + "\", \"GST Percent\" = \"" + mStockGstTaxDetails.TotalPercent + "\"", new Exception(DBMStockGSTTaxDetails.strerrormsg));
                        }
                        #endregion

                        #region For Purchase

                        mStockGstTaxDetails = new MStockGSTTaxDetails();
                        mStockGstTaxDetails.TaxDetailsNo = 0;
                        mStockGstTaxDetails.StateCode = "27";
                        mStockGstTaxDetails.HSNCode = dt_HSN.Rows[i]["HSN_CODE"].ToString();
                        mStockGstTaxDetails.HSNNo = dbMStockGstTaxDetails.getHSNNo(dt_HSN.Rows[i]["HSN_CODE"].ToString(), dt_HSN.Rows[i]["HSN_DESCRIPTION"].ToString());
                        mStockGstTaxDetails.TotalPercent = Convert.ToDouble(dt_HSN.Rows[i]["TAX_PERCENT"].ToString());
                        mStockGstTaxDetails.IGSTPercent = mStockGstTaxDetails.TotalPercent;
                        mStockGstTaxDetails.CGSTPercent = (mStockGstTaxDetails.TotalPercent == 0) ? 0 : mStockGstTaxDetails.TotalPercent / 2;
                        mStockGstTaxDetails.SGSTPercent = (mStockGstTaxDetails.TotalPercent == 0) ? 0 : mStockGstTaxDetails.TotalPercent / 2;
                        mStockGstTaxDetails.UTGSTPercent = 0;
                        mStockGstTaxDetails.CessPercent = 0;
                        mStockGstTaxDetails.FkTaxSettingNo = getTaxSettingNo(mStockGstTaxDetails.TotalPercent, GroupType.PurchaseAccount);
                        mStockGstTaxDetails.FromDate = DateTime.Now.Date;
                        mStockGstTaxDetails.IsActive = true;
                        mStockGstTaxDetails.UserID = 0;
                        mStockGstTaxDetails.UserDate = DateTime.Now;

                        if (!dbMStockGstTaxDetails.AddMStockGstTaxDetails1(mStockGstTaxDetails))
                        {
                            throw new Exception("Error while saving data. GST TAX details :: \"HSN Code\" = \"" + mStockGstTaxDetails.HSNCode + "\", \"GST Percent\" = \"" + mStockGstTaxDetails.TotalPercent + "\"", new Exception(DBMStockGSTTaxDetails.strerrormsg));
                        }
                        #endregion
                    }
                }
            }
        }

        public long getTaxSettingNo(double taxSlabPercent, long TransactionTypeNo)
        {
            long TaxSettingNo = ObjQry.ReturnLong("Select PkSrNo FROM MItemTaxSetting " +
                " Where Percentage = " + taxSlabPercent + " AND IsActive = 'True' " +
                    " AND TaxTypeNo = " + GroupType.GST + " AND TransactionTypeNo = " + TransactionTypeNo + " ", CommonFunctions.ConStr);

            if (TaxSettingNo <= 0)
            {
                addTaxSetting(taxSlabPercent, TransactionTypeNo);

                TaxSettingNo = ObjQry.ReturnLong("Select PkSrNo FROM MItemTaxSetting " +
                " Where Percentage = " + taxSlabPercent + " AND IsActive = 'True' " +
                    " AND TaxTypeNo = " + GroupType.GST + " AND TransactionTypeNo = " + TransactionTypeNo + " ", CommonFunctions.ConStr);
                if (TaxSettingNo <= 0)
                {
                    throw new Exception("Unable to add Tax Settings: Unknown Error");
                }
            }

            return TaxSettingNo;
        }

        private void addTaxSetting(double taxSlabPercent, long TransactionTypeNo)
        {
            DBMLedger dbLedger = new DBMLedger();
            MLedger mLedger = new MLedger();
            mLedger.LedgerNo = 0;
            mLedger.LedgerUserNo = "0";
            mLedger.LedgerName = "GST @ " + taxSlabPercent.ToString(Format.DoubleFloating) + " % " +
                                    (TransactionTypeNo == GroupType.SalesAccount ? "Sales" : "Purchase");
            mLedger.GroupNo = GroupType.GST;
            mLedger.ContactPerson = "";
            mLedger.InvFlag = false;
            mLedger.MaintainBillByBill = false;
            mLedger.IsActive = true;
            mLedger.LedgerStatus = 1;
            mLedger.UserID = 0;
            mLedger.UserDate = DateTime.Now;
            dbLedger.AddMLedger(mLedger);

            mLedger = new MLedger();
            mLedger.LedgerNo = 0;
            mLedger.LedgerUserNo = "0";
            mLedger.LedgerName = (TransactionTypeNo == GroupType.SalesAccount ? "Sales" : "Purchase") + " @ " +
                                    taxSlabPercent.ToString(Format.DoubleFloating) + " % GST";
            mLedger.GroupNo = TransactionTypeNo;
            mLedger.ContactPerson = "";
            mLedger.InvFlag = false;
            mLedger.MaintainBillByBill = false;
            mLedger.IsActive = true;
            mLedger.LedgerStatus = 1;
            mLedger.UserID = 0;
            mLedger.UserDate = DateTime.Now;
            dbLedger.AddMLedger(mLedger);

            MItemTaxSetting mItemTaxSetting = new MItemTaxSetting();
            mItemTaxSetting.PkSrNo = 0;
            mItemTaxSetting.TaxSettingName = taxSlabPercent.ToString(Format.DoubleFloating) + " % GST " + (TransactionTypeNo == GroupType.SalesAccount ? "Sales" : "Purchase");
            mItemTaxSetting.Percentage = taxSlabPercent;
            mItemTaxSetting.IsActive = true;
            mItemTaxSetting.CalculationMethod = "2";
            mItemTaxSetting.UserID = 0;
            mItemTaxSetting.UserDate = DateTime.Now;
            mItemTaxSetting.TaxTypeNo = GroupType.GST;
            mItemTaxSetting.TransactionTypeNo = TransactionTypeNo;

            dbLedger.AddMItemTaxSetting(mItemTaxSetting);

            if (!dbLedger.ExecuteNonQueryStatements() == true)
            {
                throw new Exception("Error while adding new Tax setting : Err : " + DBMLedger.strerrormsg);
            }
        }

        #endregion

        public bool CheckValue(string Value)
        {
            try
            {
                Convert.ToDouble(Value);
                return true;
            }
            catch( Exception ex)
            {
                return false;
            }
        }
    }
}
