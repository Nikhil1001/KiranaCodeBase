﻿namespace Kirana.Master
{
    partial class StockItemSAE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.EP = new System.Windows.Forms.ErrorProvider(this.components);
            this.pnlMain = new JitControls.OMBPanel();
            this.panel1 = new JitControls.OMBPanel();
            this.label34 = new System.Windows.Forms.Label();
            this.cmbDepartmentName = new System.Windows.Forms.ComboBox();
            this.lblChkHelp1 = new System.Windows.Forms.Label();
            this.chkIsQtyRead = new System.Windows.Forms.CheckBox();
            this.pnlHamli = new System.Windows.Forms.Panel();
            this.label39 = new System.Windows.Forms.Label();
            this.txtHamali = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.cmbDiscountType = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.cmbGodownNo = new System.Windows.Forms.ComboBox();
            this.txtMargin = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.lblManufacturerCompanyName = new System.Windows.Forms.Label();
            this.lblASaleRate = new System.Windows.Forms.Label();
            this.txtASaleRate = new System.Windows.Forms.TextBox();
            this.lblBSaleRate = new System.Windows.Forms.Label();
            this.txtBSaleRate = new System.Windows.Forms.TextBox();
            this.lblStar17 = new System.Windows.Forms.Label();
            this.lblStar13 = new System.Windows.Forms.Label();
            this.lblStar14 = new System.Windows.Forms.Label();
            this.lblChkHelp = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtShortCode = new System.Windows.Forms.TextBox();
            this.btnLangLongDesc = new System.Windows.Forms.Button();
            this.btnLangShortDesc = new System.Windows.Forms.Button();
            this.lblStar16 = new System.Windows.Forms.Label();
            this.lblStar15 = new System.Windows.Forms.Label();
            this.txtLangShortDesc = new System.Windows.Forms.TextBox();
            this.txtLangFullDesc = new System.Windows.Forms.TextBox();
            this.txtItemShortDesc = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblStar5 = new System.Windows.Forms.Label();
            this.lblStar1 = new System.Windows.Forms.Label();
            this.lblStar8 = new System.Windows.Forms.Label();
            this.lblStar4 = new System.Windows.Forms.Label();
            this.cmbStockLocationName = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtLanguage = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.lblPur = new System.Windows.Forms.Label();
            this.txtPurRate = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cmbUom = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.chkActive = new System.Windows.Forms.CheckBox();
            this.txtMRP = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtBarcobe = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cmbCategoryName = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbGroupNo1 = new System.Windows.Forms.ComboBox();
            this.txtItemName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblStar12 = new System.Windows.Forms.Label();
            this.pnlBarCodePrint = new JitControls.OMPanel();
            this.lblExpDate = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.dtpPackingDate = new System.Windows.Forms.DateTimePicker();
            this.txtExpDays = new System.Windows.Forms.TextBox();
            this.txtwt = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.rbSmallMode = new System.Windows.Forms.RadioButton();
            this.rbBigMod = new System.Windows.Forms.RadioButton();
            this.txtStartNo = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.btnCancelPrintBarcode = new System.Windows.Forms.Button();
            this.btnOKPrintBarCode = new System.Windows.Forms.Button();
            this.txtNoOfPrint = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.btnImports = new System.Windows.Forms.Button();
            this.lblCollectionDetails = new System.Windows.Forms.Label();
            this.gbGSTDetails = new System.Windows.Forms.GroupBox();
            this.TxtCESS = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtHSNCode = new System.Windows.Forms.TextBox();
            this.btnAddGST = new System.Windows.Forms.Button();
            this.txtSGST = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtSearchBarCode = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtCGST = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txtUTGST = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.txtIGST = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.cmbGSTPercent = new System.Windows.Forms.ComboBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.txtMaxLevelQty = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtMinLevelQty = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.btnPrintBarCodeAdvance = new System.Windows.Forms.Button();
            this.lblmsg = new System.Windows.Forms.Label();
            this.btnPrintBarCode = new System.Windows.Forms.Button();
            this.btnNewBrand = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.BtnExit = new System.Windows.Forms.Button();
            this.btnPrev = new System.Windows.Forms.Button();
            this.btnFirst = new System.Windows.Forms.Button();
            this.btnLast = new System.Windows.Forms.Button();
            this.cmbHSNCode = new System.Windows.Forms.ComboBox();
            this.BtnSave = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnPurVat = new System.Windows.Forms.Button();
            this.btnSalesVat = new System.Windows.Forms.Button();
            this.lblStar10 = new System.Windows.Forms.Label();
            this.lblStar9 = new System.Windows.Forms.Label();
            this.cmbVatPurchase = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.cmbVatSales = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbGroupNo2 = new System.Windows.Forms.ComboBox();
            this.cmbStockDept = new System.Windows.Forms.ComboBox();
            this.lblStar2 = new System.Windows.Forms.Label();
            this.lblStar3 = new System.Windows.Forms.Label();
            this.txtDiscPer = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.EP)).BeginInit();
            this.pnlMain.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnlHamli.SuspendLayout();
            this.pnlBarCodePrint.SuspendLayout();
            this.gbGSTDetails.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // EP
            // 
            this.EP.ContainerControl = this;
            // 
            // pnlMain
            // 
            this.pnlMain.BorderColor = System.Drawing.Color.SteelBlue;
            this.pnlMain.BorderRadius = 2;
            this.pnlMain.Controls.Add(this.panel1);
            this.pnlMain.Controls.Add(this.pnlBarCodePrint);
            this.pnlMain.Controls.Add(this.btnImports);
            this.pnlMain.Controls.Add(this.lblCollectionDetails);
            this.pnlMain.Controls.Add(this.gbGSTDetails);
            this.pnlMain.Controls.Add(this.btnPrintBarCodeAdvance);
            this.pnlMain.Controls.Add(this.lblmsg);
            this.pnlMain.Controls.Add(this.btnPrintBarCode);
            this.pnlMain.Controls.Add(this.btnNewBrand);
            this.pnlMain.Controls.Add(this.btnSearch);
            this.pnlMain.Controls.Add(this.btnDelete);
            this.pnlMain.Controls.Add(this.btnNext);
            this.pnlMain.Controls.Add(this.BtnExit);
            this.pnlMain.Controls.Add(this.btnPrev);
            this.pnlMain.Controls.Add(this.btnFirst);
            this.pnlMain.Controls.Add(this.btnLast);
            this.pnlMain.Controls.Add(this.cmbHSNCode);
            this.pnlMain.Controls.Add(this.BtnSave);
            this.pnlMain.Controls.Add(this.btnUpdate);
            this.pnlMain.Controls.Add(this.btnCancel);
            this.pnlMain.Controls.Add(this.btnNew);
            this.pnlMain.Location = new System.Drawing.Point(1, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(838, 529);
            this.pnlMain.TabIndex = 10004;
            // 
            // panel1
            // 
            this.panel1.BorderColor = System.Drawing.Color.Silver;
            this.panel1.BorderRadius = 3;
            this.panel1.Controls.Add(this.label36);
            this.panel1.Controls.Add(this.txtDiscPer);
            this.panel1.Controls.Add(this.label34);
            this.panel1.Controls.Add(this.cmbDepartmentName);
            this.panel1.Controls.Add(this.lblChkHelp1);
            this.panel1.Controls.Add(this.chkIsQtyRead);
            this.panel1.Controls.Add(this.pnlHamli);
            this.panel1.Controls.Add(this.label35);
            this.panel1.Controls.Add(this.cmbDiscountType);
            this.panel1.Controls.Add(this.label32);
            this.panel1.Controls.Add(this.cmbGodownNo);
            this.panel1.Controls.Add(this.txtMargin);
            this.panel1.Controls.Add(this.label31);
            this.panel1.Controls.Add(this.lblManufacturerCompanyName);
            this.panel1.Controls.Add(this.lblASaleRate);
            this.panel1.Controls.Add(this.txtASaleRate);
            this.panel1.Controls.Add(this.lblBSaleRate);
            this.panel1.Controls.Add(this.txtBSaleRate);
            this.panel1.Controls.Add(this.lblStar17);
            this.panel1.Controls.Add(this.lblStar13);
            this.panel1.Controls.Add(this.lblStar14);
            this.panel1.Controls.Add(this.lblChkHelp);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.txtShortCode);
            this.panel1.Controls.Add(this.btnLangLongDesc);
            this.panel1.Controls.Add(this.btnLangShortDesc);
            this.panel1.Controls.Add(this.lblStar16);
            this.panel1.Controls.Add(this.lblStar15);
            this.panel1.Controls.Add(this.txtLangShortDesc);
            this.panel1.Controls.Add(this.txtLangFullDesc);
            this.panel1.Controls.Add(this.txtItemShortDesc);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.lblStar5);
            this.panel1.Controls.Add(this.lblStar1);
            this.panel1.Controls.Add(this.lblStar8);
            this.panel1.Controls.Add(this.lblStar4);
            this.panel1.Controls.Add(this.cmbStockLocationName);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtLanguage);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.lblPur);
            this.panel1.Controls.Add(this.txtPurRate);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.cmbUom);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.chkActive);
            this.panel1.Controls.Add(this.txtMRP);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.txtBarcobe);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.cmbCategoryName);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.cmbGroupNo1);
            this.panel1.Controls.Add(this.txtItemName);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.lblStar12);
            this.panel1.Location = new System.Drawing.Point(6, 29);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(825, 292);
            this.panel1.TabIndex = 0;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(17, 186);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(68, 13);
            this.label34.TabIndex = 200017;
            this.label34.Text = "Department :";
            // 
            // cmbDepartmentName
            // 
            this.cmbDepartmentName.FormattingEnabled = true;
            this.cmbDepartmentName.Location = new System.Drawing.Point(152, 186);
            this.cmbDepartmentName.MaxLength = 25;
            this.cmbDepartmentName.Name = "cmbDepartmentName";
            this.cmbDepartmentName.Size = new System.Drawing.Size(300, 21);
            this.cmbDepartmentName.TabIndex = 9;
            this.cmbDepartmentName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbDepartmentName_KeyDown);
            // 
            // lblChkHelp1
            // 
            this.lblChkHelp1.AutoSize = true;
            this.lblChkHelp1.Location = new System.Drawing.Point(472, 271);
            this.lblChkHelp1.Name = "lblChkHelp1";
            this.lblChkHelp1.Size = new System.Drawing.Size(128, 13);
            this.lblChkHelp1.TabIndex = 200016;
            this.lblChkHelp1.Text = "(Is Qty Read By Machine)";
            // 
            // chkIsQtyRead
            // 
            this.chkIsQtyRead.Location = new System.Drawing.Point(425, 270);
            this.chkIsQtyRead.Margin = new System.Windows.Forms.Padding(0);
            this.chkIsQtyRead.Name = "chkIsQtyRead";
            this.chkIsQtyRead.Size = new System.Drawing.Size(44, 17);
            this.chkIsQtyRead.TabIndex = 200015;
            this.chkIsQtyRead.Text = "No";
            this.chkIsQtyRead.UseVisualStyleBackColor = true;
            this.chkIsQtyRead.CheckedChanged += new System.EventHandler(this.chkIsQtyRead_CheckedChanged);
            // 
            // pnlHamli
            // 
            this.pnlHamli.Controls.Add(this.label39);
            this.pnlHamli.Controls.Add(this.txtHamali);
            this.pnlHamli.Controls.Add(this.label38);
            this.pnlHamli.Location = new System.Drawing.Point(472, 134);
            this.pnlHamli.Name = "pnlHamli";
            this.pnlHamli.Size = new System.Drawing.Size(277, 31);
            this.pnlHamli.TabIndex = 200014;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(121, 8);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(23, 13);
            this.label39.TabIndex = 200016;
            this.label39.Text = "Kg.";
            // 
            // txtHamali
            // 
            this.txtHamali.Location = new System.Drawing.Point(55, 5);
            this.txtHamali.Name = "txtHamali";
            this.txtHamali.Size = new System.Drawing.Size(62, 20);
            this.txtHamali.TabIndex = 200015;
            this.txtHamali.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtHamali.TextChanged += new System.EventHandler(this.txtHamali_TextChanged);
            this.txtHamali.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHamali_KeyDown);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(7, 8);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(45, 13);
            this.label38.TabIndex = 0;
            this.label38.Text = "Hamali :";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(16, 137);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(79, 13);
            this.label35.TabIndex = 200011;
            this.label35.Text = "DiscountType :";
            // 
            // cmbDiscountType
            // 
            this.cmbDiscountType.FormattingEnabled = true;
            this.cmbDiscountType.Location = new System.Drawing.Point(152, 134);
            this.cmbDiscountType.MaxLength = 25;
            this.cmbDiscountType.Name = "cmbDiscountType";
            this.cmbDiscountType.Size = new System.Drawing.Size(300, 21);
            this.cmbDiscountType.TabIndex = 8;
            this.cmbDiscountType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbDiscountType_KeyDown);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(16, 162);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(85, 13);
            this.label32.TabIndex = 200009;
            this.label32.Text = "Stock Location :";
            // 
            // cmbGodownNo
            // 
            this.cmbGodownNo.FormattingEnabled = true;
            this.cmbGodownNo.Location = new System.Drawing.Point(152, 159);
            this.cmbGodownNo.MaxLength = 25;
            this.cmbGodownNo.Name = "cmbGodownNo";
            this.cmbGodownNo.Size = new System.Drawing.Size(300, 21);
            this.cmbGodownNo.TabIndex = 7;
            this.cmbGodownNo.Leave += new System.EventHandler(this.cmbGodownNo_Leave);
            this.cmbGodownNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbGodownNo_KeyDown);
            // 
            // txtMargin
            // 
            this.txtMargin.Location = new System.Drawing.Point(788, 10);
            this.txtMargin.Name = "txtMargin";
            this.txtMargin.Size = new System.Drawing.Size(62, 20);
            this.txtMargin.TabIndex = 200003;
            this.txtMargin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMargin.Visible = false;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(709, 13);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(61, 13);
            this.label31.TabIndex = 200002;
            this.label31.Text = "Cat. Margin";
            this.label31.Visible = false;
            // 
            // lblManufacturerCompanyName
            // 
            this.lblManufacturerCompanyName.AutoSize = true;
            this.lblManufacturerCompanyName.Location = new System.Drawing.Point(479, 9);
            this.lblManufacturerCompanyName.Name = "lblManufacturerCompanyName";
            this.lblManufacturerCompanyName.Size = new System.Drawing.Size(90, 13);
            this.lblManufacturerCompanyName.TabIndex = 200001;
            this.lblManufacturerCompanyName.Text = "lblMfgCompName";
            this.lblManufacturerCompanyName.Visible = false;
            // 
            // lblASaleRate
            // 
            this.lblASaleRate.AutoSize = true;
            this.lblASaleRate.Location = new System.Drawing.Point(239, 217);
            this.lblASaleRate.Name = "lblASaleRate";
            this.lblASaleRate.Size = new System.Drawing.Size(17, 13);
            this.lblASaleRate.TabIndex = 190025;
            this.lblASaleRate.Text = "lbl";
            // 
            // txtASaleRate
            // 
            this.txtASaleRate.Location = new System.Drawing.Point(226, 239);
            this.txtASaleRate.Name = "txtASaleRate";
            this.txtASaleRate.Size = new System.Drawing.Size(77, 20);
            this.txtASaleRate.TabIndex = 13;
            this.txtASaleRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtASaleRate.TextChanged += new System.EventHandler(this.txtMasked_TextChanged);
            this.txtASaleRate.Leave += new System.EventHandler(this.Control_Leave);
            // 
            // lblBSaleRate
            // 
            this.lblBSaleRate.AutoSize = true;
            this.lblBSaleRate.Location = new System.Drawing.Point(333, 217);
            this.lblBSaleRate.Name = "lblBSaleRate";
            this.lblBSaleRate.Size = new System.Drawing.Size(17, 13);
            this.lblBSaleRate.TabIndex = 190027;
            this.lblBSaleRate.Text = "lbl";
            // 
            // txtBSaleRate
            // 
            this.txtBSaleRate.Location = new System.Drawing.Point(320, 239);
            this.txtBSaleRate.Name = "txtBSaleRate";
            this.txtBSaleRate.Size = new System.Drawing.Size(77, 20);
            this.txtBSaleRate.TabIndex = 14;
            this.txtBSaleRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBSaleRate.TextChanged += new System.EventHandler(this.txtMasked_TextChanged);
            this.txtBSaleRate.Leave += new System.EventHandler(this.Control_Leave);
            // 
            // lblStar17
            // 
            this.lblStar17.AutoSize = true;
            this.lblStar17.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar17.Location = new System.Drawing.Point(458, 34);
            this.lblStar17.Name = "lblStar17";
            this.lblStar17.Size = new System.Drawing.Size(11, 13);
            this.lblStar17.TabIndex = 190044;
            this.lblStar17.Text = "*";
            // 
            // lblStar13
            // 
            this.lblStar13.AutoSize = true;
            this.lblStar13.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar13.Location = new System.Drawing.Point(320, 217);
            this.lblStar13.Name = "lblStar13";
            this.lblStar13.Size = new System.Drawing.Size(11, 13);
            this.lblStar13.TabIndex = 190032;
            this.lblStar13.Text = "*";
            // 
            // lblStar14
            // 
            this.lblStar14.AutoSize = true;
            this.lblStar14.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar14.Location = new System.Drawing.Point(414, 217);
            this.lblStar14.Name = "lblStar14";
            this.lblStar14.Size = new System.Drawing.Size(11, 13);
            this.lblStar14.TabIndex = 190033;
            this.lblStar14.Text = "*";
            // 
            // lblChkHelp
            // 
            this.lblChkHelp.AutoSize = true;
            this.lblChkHelp.Location = new System.Drawing.Point(198, 274);
            this.lblChkHelp.Name = "lblChkHelp";
            this.lblChkHelp.Size = new System.Drawing.Size(215, 13);
            this.lblChkHelp.TabIndex = 190041;
            this.lblChkHelp.Text = "( Press SPACE BAR or cilck using MOUSE )";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(17, 33);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(60, 13);
            this.label13.TabIndex = 190042;
            this.label13.Text = "Short Code";
            // 
            // txtShortCode
            // 
            this.txtShortCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtShortCode.Location = new System.Drawing.Point(152, 31);
            this.txtShortCode.MaxLength = 50;
            this.txtShortCode.Name = "txtShortCode";
            this.txtShortCode.Size = new System.Drawing.Size(300, 20);
            this.txtShortCode.TabIndex = 1;
            this.txtShortCode.Leave += new System.EventHandler(this.txtShortCode_Leave);
            // 
            // btnLangLongDesc
            // 
            this.btnLangLongDesc.Location = new System.Drawing.Point(761, 80);
            this.btnLangLongDesc.Name = "btnLangLongDesc";
            this.btnLangLongDesc.Size = new System.Drawing.Size(21, 21);
            this.btnLangLongDesc.TabIndex = 190040;
            this.btnLangLongDesc.Text = "..";
            this.btnLangLongDesc.UseVisualStyleBackColor = true;
            this.btnLangLongDesc.Click += new System.EventHandler(this.btnLangLongDesc_Click);
            // 
            // btnLangShortDesc
            // 
            this.btnLangShortDesc.Location = new System.Drawing.Point(761, 108);
            this.btnLangShortDesc.Name = "btnLangShortDesc";
            this.btnLangShortDesc.Size = new System.Drawing.Size(21, 21);
            this.btnLangShortDesc.TabIndex = 190039;
            this.btnLangShortDesc.Text = "..";
            this.btnLangShortDesc.UseVisualStyleBackColor = true;
            this.btnLangShortDesc.Click += new System.EventHandler(this.btnLangShortDesc_Click);
            // 
            // lblStar16
            // 
            this.lblStar16.AutoSize = true;
            this.lblStar16.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar16.Location = new System.Drawing.Point(785, 107);
            this.lblStar16.Name = "lblStar16";
            this.lblStar16.Size = new System.Drawing.Size(11, 13);
            this.lblStar16.TabIndex = 190038;
            this.lblStar16.Text = "*";
            this.lblStar16.Visible = false;
            // 
            // lblStar15
            // 
            this.lblStar15.AutoSize = true;
            this.lblStar15.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar15.Location = new System.Drawing.Point(785, 82);
            this.lblStar15.Name = "lblStar15";
            this.lblStar15.Size = new System.Drawing.Size(11, 13);
            this.lblStar15.TabIndex = 190037;
            this.lblStar15.Text = "*";
            // 
            // txtLangShortDesc
            // 
            this.txtLangShortDesc.BackColor = System.Drawing.Color.White;
            this.txtLangShortDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLangShortDesc.Font = new System.Drawing.Font("Shivaji01", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLangShortDesc.ForeColor = System.Drawing.Color.Black;
            this.txtLangShortDesc.Location = new System.Drawing.Point(479, 105);
            this.txtLangShortDesc.Name = "txtLangShortDesc";
            this.txtLangShortDesc.Size = new System.Drawing.Size(276, 25);
            this.txtLangShortDesc.TabIndex = 6;
            this.txtLangShortDesc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtLangShortDesc_KeyDown);
            this.txtLangShortDesc.Leave += new System.EventHandler(this.txtLangShortDesc_Leave);
            // 
            // txtLangFullDesc
            // 
            this.txtLangFullDesc.BackColor = System.Drawing.Color.White;
            this.txtLangFullDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLangFullDesc.Font = new System.Drawing.Font("Shivaji01", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLangFullDesc.ForeColor = System.Drawing.Color.Black;
            this.txtLangFullDesc.Location = new System.Drawing.Point(479, 79);
            this.txtLangFullDesc.Name = "txtLangFullDesc";
            this.txtLangFullDesc.Size = new System.Drawing.Size(276, 25);
            this.txtLangFullDesc.TabIndex = 4;
            this.txtLangFullDesc.Leave += new System.EventHandler(this.txtLangFullDesc_Leave);
            // 
            // txtItemShortDesc
            // 
            this.txtItemShortDesc.Location = new System.Drawing.Point(152, 109);
            this.txtItemShortDesc.MaxLength = 50;
            this.txtItemShortDesc.Name = "txtItemShortDesc";
            this.txtItemShortDesc.Size = new System.Drawing.Size(300, 20);
            this.txtItemShortDesc.TabIndex = 5;
            this.txtItemShortDesc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtItemShortDesc_KeyDown);
            this.txtItemShortDesc.Leave += new System.EventHandler(this.txtItemShortDesc_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 190029;
            this.label3.Text = "Item Short Desc ";
            // 
            // lblStar5
            // 
            this.lblStar5.AutoSize = true;
            this.lblStar5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar5.Location = new System.Drawing.Point(458, 84);
            this.lblStar5.Name = "lblStar5";
            this.lblStar5.Size = new System.Drawing.Size(11, 13);
            this.lblStar5.TabIndex = 190024;
            this.lblStar5.Text = "*";
            // 
            // lblStar1
            // 
            this.lblStar1.AutoSize = true;
            this.lblStar1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar1.Location = new System.Drawing.Point(458, 10);
            this.lblStar1.Name = "lblStar1";
            this.lblStar1.Size = new System.Drawing.Size(11, 13);
            this.lblStar1.TabIndex = 190023;
            this.lblStar1.Text = "*";
            // 
            // lblStar8
            // 
            this.lblStar8.AutoSize = true;
            this.lblStar8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar8.Location = new System.Drawing.Point(226, 217);
            this.lblStar8.Name = "lblStar8";
            this.lblStar8.Size = new System.Drawing.Size(11, 13);
            this.lblStar8.TabIndex = 190021;
            this.lblStar8.Text = "*";
            // 
            // lblStar4
            // 
            this.lblStar4.AutoSize = true;
            this.lblStar4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar4.Location = new System.Drawing.Point(458, 58);
            this.lblStar4.Name = "lblStar4";
            this.lblStar4.Size = new System.Drawing.Size(11, 13);
            this.lblStar4.TabIndex = 190019;
            this.lblStar4.Text = "*";
            // 
            // cmbStockLocationName
            // 
            this.cmbStockLocationName.FormattingEnabled = true;
            this.cmbStockLocationName.Location = new System.Drawing.Point(837, 273);
            this.cmbStockLocationName.Name = "cmbStockLocationName";
            this.cmbStockLocationName.Size = new System.Drawing.Size(148, 21);
            this.cmbStockLocationName.TabIndex = 200000;
            this.cmbStockLocationName.Visible = false;
            this.cmbStockLocationName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbStockLocationName_KeyDown);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(828, 273);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(85, 13);
            this.label16.TabIndex = 169;
            this.label16.Text = "Stock Location :";
            this.label16.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 51;
            this.label1.Text = "Item Desc ";
            // 
            // txtLanguage
            // 
            this.txtLanguage.BackColor = System.Drawing.Color.White;
            this.txtLanguage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLanguage.Font = new System.Drawing.Font("Shivaji01", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLanguage.ForeColor = System.Drawing.Color.Black;
            this.txtLanguage.Location = new System.Drawing.Point(479, 52);
            this.txtLanguage.Name = "txtLanguage";
            this.txtLanguage.ReadOnly = true;
            this.txtLanguage.Size = new System.Drawing.Size(276, 25);
            this.txtLanguage.TabIndex = 7;
            this.txtLanguage.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(862, 41);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 13);
            this.label10.TabIndex = 167;
            this.label10.Text = "Language B.N";
            this.label10.Visible = false;
            // 
            // lblPur
            // 
            this.lblPur.AutoSize = true;
            this.lblPur.Location = new System.Drawing.Point(987, 141);
            this.lblPur.Name = "lblPur";
            this.lblPur.Size = new System.Drawing.Size(12, 13);
            this.lblPur.TabIndex = 158;
            this.lblPur.Text = "/";
            this.lblPur.Visible = false;
            // 
            // txtPurRate
            // 
            this.txtPurRate.Location = new System.Drawing.Point(413, 239);
            this.txtPurRate.Name = "txtPurRate";
            this.txtPurRate.Size = new System.Drawing.Size(77, 20);
            this.txtPurRate.TabIndex = 15;
            this.txtPurRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPurRate.TextChanged += new System.EventHandler(this.txtMasked_TextChanged);
            this.txtPurRate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPurRate_KeyDown);
            this.txtPurRate.Leave += new System.EventHandler(this.Control_Leave);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(427, 217);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(78, 13);
            this.label15.TabIndex = 156;
            this.label15.Text = "Purchase Rate";
            // 
            // cmbUom
            // 
            this.cmbUom.FormattingEnabled = true;
            this.cmbUom.Location = new System.Drawing.Point(6, 239);
            this.cmbUom.Name = "cmbUom";
            this.cmbUom.Size = new System.Drawing.Size(101, 21);
            this.cmbUom.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 273);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 13);
            this.label4.TabIndex = 55;
            this.label4.Text = "Active Status (F6)";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(9, 217);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(32, 13);
            this.label14.TabIndex = 154;
            this.label14.Text = "UOM";
            // 
            // chkActive
            // 
            this.chkActive.Checked = true;
            this.chkActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkActive.Location = new System.Drawing.Point(153, 272);
            this.chkActive.Margin = new System.Windows.Forms.Padding(0);
            this.chkActive.Name = "chkActive";
            this.chkActive.Size = new System.Drawing.Size(44, 17);
            this.chkActive.TabIndex = 17;
            this.chkActive.Text = "Yes";
            this.chkActive.UseVisualStyleBackColor = true;
            this.chkActive.CheckedChanged += new System.EventHandler(this.chkActive_CheckedChanged);
            this.chkActive.KeyDown += new System.Windows.Forms.KeyEventHandler(this.chkActive_KeyDown);
            // 
            // txtMRP
            // 
            this.txtMRP.Location = new System.Drawing.Point(127, 239);
            this.txtMRP.Name = "txtMRP";
            this.txtMRP.Size = new System.Drawing.Size(77, 20);
            this.txtMRP.TabIndex = 12;
            this.txtMRP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMRP.TextChanged += new System.EventHandler(this.txtMasked_TextChanged);
            this.txtMRP.Leave += new System.EventHandler(this.Control_Leave);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(136, 217);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(31, 13);
            this.label12.TabIndex = 71;
            this.label12.Text = "MRP";
            // 
            // txtBarcobe
            // 
            this.txtBarcobe.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBarcobe.Location = new System.Drawing.Point(152, 7);
            this.txtBarcobe.MaxLength = 50;
            this.txtBarcobe.Name = "txtBarcobe";
            this.txtBarcobe.Size = new System.Drawing.Size(300, 20);
            this.txtBarcobe.TabIndex = 0;
            this.txtBarcobe.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarcobe_KeyDown);
            this.txtBarcobe.Leave += new System.EventHandler(this.txtBarcobe_Leave);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(17, 10);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 13);
            this.label11.TabIndex = 69;
            this.label11.Text = "Barcode";
            // 
            // cmbCategoryName
            // 
            this.cmbCategoryName.FormattingEnabled = true;
            this.cmbCategoryName.Location = new System.Drawing.Point(1011, 110);
            this.cmbCategoryName.MaxLength = 25;
            this.cmbCategoryName.Name = "cmbCategoryName";
            this.cmbCategoryName.Size = new System.Drawing.Size(300, 21);
            this.cmbCategoryName.TabIndex = 3009;
            this.cmbCategoryName.Visible = false;
            this.cmbCategoryName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbCategoryName_KeyDown);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(875, 113);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 13);
            this.label9.TabIndex = 63;
            this.label9.Text = "Category Name  :";
            this.label9.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(865, 92);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(99, 13);
            this.label8.TabIndex = 61;
            this.label8.Text = "Department Name :";
            this.label8.Visible = false;
            // 
            // cmbGroupNo1
            // 
            this.cmbGroupNo1.FormattingEnabled = true;
            this.cmbGroupNo1.Location = new System.Drawing.Point(152, 55);
            this.cmbGroupNo1.MaxLength = 25;
            this.cmbGroupNo1.Name = "cmbGroupNo1";
            this.cmbGroupNo1.Size = new System.Drawing.Size(300, 21);
            this.cmbGroupNo1.TabIndex = 2;
            this.cmbGroupNo1.Leave += new System.EventHandler(this.cmbGroupNo1_Leave);
            this.cmbGroupNo1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbGroupNo1_KeyDown);
            // 
            // txtItemName
            // 
            this.txtItemName.Location = new System.Drawing.Point(152, 82);
            this.txtItemName.MaxLength = 50;
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.Size = new System.Drawing.Size(300, 20);
            this.txtItemName.TabIndex = 3;
            this.txtItemName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtItemName_KeyDown);
            this.txtItemName.Leave += new System.EventHandler(this.txtItemName_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 53;
            this.label2.Text = "Brand Name ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(862, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 52;
            this.label5.Text = "Language Desc :";
            this.label5.Visible = false;
            // 
            // lblStar12
            // 
            this.lblStar12.AutoSize = true;
            this.lblStar12.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar12.Location = new System.Drawing.Point(127, 217);
            this.lblStar12.Name = "lblStar12";
            this.lblStar12.Size = new System.Drawing.Size(11, 13);
            this.lblStar12.TabIndex = 190031;
            this.lblStar12.Text = "*";
            // 
            // pnlBarCodePrint
            // 
            this.pnlBarCodePrint.BorderRadius = 3;
            this.pnlBarCodePrint.Controls.Add(this.lblExpDate);
            this.pnlBarCodePrint.Controls.Add(this.label30);
            this.pnlBarCodePrint.Controls.Add(this.dtpPackingDate);
            this.pnlBarCodePrint.Controls.Add(this.txtExpDays);
            this.pnlBarCodePrint.Controls.Add(this.txtwt);
            this.pnlBarCodePrint.Controls.Add(this.label29);
            this.pnlBarCodePrint.Controls.Add(this.label28);
            this.pnlBarCodePrint.Controls.Add(this.label20);
            this.pnlBarCodePrint.Controls.Add(this.label6);
            this.pnlBarCodePrint.Controls.Add(this.rbSmallMode);
            this.pnlBarCodePrint.Controls.Add(this.rbBigMod);
            this.pnlBarCodePrint.Controls.Add(this.txtStartNo);
            this.pnlBarCodePrint.Controls.Add(this.label22);
            this.pnlBarCodePrint.Controls.Add(this.btnCancelPrintBarcode);
            this.pnlBarCodePrint.Controls.Add(this.btnOKPrintBarCode);
            this.pnlBarCodePrint.Controls.Add(this.txtNoOfPrint);
            this.pnlBarCodePrint.Controls.Add(this.label21);
            this.pnlBarCodePrint.CornerRadius = 3;
            this.pnlBarCodePrint.GradientBottom = System.Drawing.Color.LightGray;
            this.pnlBarCodePrint.GradientMiddle = System.Drawing.Color.White;
            this.pnlBarCodePrint.GradientShow = true;
            this.pnlBarCodePrint.GradientTop = System.Drawing.Color.Silver;
            this.pnlBarCodePrint.Location = new System.Drawing.Point(767, 26);
            this.pnlBarCodePrint.Name = "pnlBarCodePrint";
            this.pnlBarCodePrint.Size = new System.Drawing.Size(253, 263);
            this.pnlBarCodePrint.TabIndex = 190045;
            this.pnlBarCodePrint.Visible = false;
            // 
            // lblExpDate
            // 
            this.lblExpDate.AutoSize = true;
            this.lblExpDate.BackColor = System.Drawing.Color.Transparent;
            this.lblExpDate.Location = new System.Drawing.Point(113, 167);
            this.lblExpDate.Name = "lblExpDate";
            this.lblExpDate.Size = new System.Drawing.Size(10, 13);
            this.lblExpDate.TabIndex = 1000710;
            this.lblExpDate.Text = ":";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Location = new System.Drawing.Point(113, 97);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(92, 13);
            this.label30.TabIndex = 1000709;
            this.label30.Text = "(Eg. 250 gm,2 Kg)";
            // 
            // dtpPackingDate
            // 
            this.dtpPackingDate.Location = new System.Drawing.Point(115, 117);
            this.dtpPackingDate.Name = "dtpPackingDate";
            this.dtpPackingDate.Size = new System.Drawing.Size(100, 20);
            this.dtpPackingDate.TabIndex = 2004;
            this.dtpPackingDate.ValueChanged += new System.EventHandler(this.dtpPackingDate_ValueChanged);
            this.dtpPackingDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpPackingDate_KeyDown);
            // 
            // txtExpDays
            // 
            this.txtExpDays.Location = new System.Drawing.Point(114, 140);
            this.txtExpDays.MaxLength = 5;
            this.txtExpDays.Name = "txtExpDays";
            this.txtExpDays.Size = new System.Drawing.Size(100, 20);
            this.txtExpDays.TabIndex = 2005;
            this.txtExpDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtExpDays.TextChanged += new System.EventHandler(this.txtExpDays_TextChanged);
            this.txtExpDays.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtExpDays_KeyDown);
            this.txtExpDays.Leave += new System.EventHandler(this.txtExpDays_Leave);
            // 
            // txtwt
            // 
            this.txtwt.Location = new System.Drawing.Point(114, 71);
            this.txtwt.MaxLength = 50;
            this.txtwt.Name = "txtwt";
            this.txtwt.Size = new System.Drawing.Size(100, 20);
            this.txtwt.TabIndex = 2003;
            this.txtwt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Location = new System.Drawing.Point(9, 167);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(60, 13);
            this.label29.TabIndex = 1000705;
            this.label29.Text = "Exp. Date :";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Location = new System.Drawing.Point(9, 141);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(61, 13);
            this.label28.TabIndex = 1000704;
            this.label28.Text = "Exp. Days :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Location = new System.Drawing.Point(7, 115);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(78, 13);
            this.label20.TabIndex = 1000703;
            this.label20.Text = "Packing Date :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(7, 73);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 1000702;
            this.label6.Text = "Wt :";
            // 
            // rbSmallMode
            // 
            this.rbSmallMode.AutoSize = true;
            this.rbSmallMode.BackColor = System.Drawing.Color.Transparent;
            this.rbSmallMode.Location = new System.Drawing.Point(150, 193);
            this.rbSmallMode.Name = "rbSmallMode";
            this.rbSmallMode.Size = new System.Drawing.Size(80, 17);
            this.rbSmallMode.TabIndex = 2006;
            this.rbSmallMode.Text = "Small Mode";
            this.rbSmallMode.UseVisualStyleBackColor = false;
            this.rbSmallMode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rbSmallMode_KeyDown);
            // 
            // rbBigMod
            // 
            this.rbBigMod.AutoSize = true;
            this.rbBigMod.BackColor = System.Drawing.Color.Transparent;
            this.rbBigMod.Checked = true;
            this.rbBigMod.Location = new System.Drawing.Point(59, 194);
            this.rbBigMod.Name = "rbBigMod";
            this.rbBigMod.Size = new System.Drawing.Size(70, 17);
            this.rbBigMod.TabIndex = 2006;
            this.rbBigMod.TabStop = true;
            this.rbBigMod.Text = "Big Mode";
            this.rbBigMod.UseVisualStyleBackColor = false;
            this.rbBigMod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rbBigMod_KeyDown);
            // 
            // txtStartNo
            // 
            this.txtStartNo.Location = new System.Drawing.Point(114, 43);
            this.txtStartNo.MaxLength = 5;
            this.txtStartNo.Name = "txtStartNo";
            this.txtStartNo.Size = new System.Drawing.Size(100, 20);
            this.txtStartNo.TabIndex = 2002;
            this.txtStartNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtStartNo.TextChanged += new System.EventHandler(this.txtStartNo_TextChanged);
            this.txtStartNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStartNo_KeyDown);
            this.txtStartNo.Leave += new System.EventHandler(this.txtStartNo_Leave);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Location = new System.Drawing.Point(7, 46);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(52, 13);
            this.label22.TabIndex = 1000701;
            this.label22.Text = "Start No :";
            // 
            // btnCancelPrintBarcode
            // 
            this.btnCancelPrintBarcode.Location = new System.Drawing.Point(120, 216);
            this.btnCancelPrintBarcode.Name = "btnCancelPrintBarcode";
            this.btnCancelPrintBarcode.Size = new System.Drawing.Size(73, 24);
            this.btnCancelPrintBarcode.TabIndex = 2008;
            this.btnCancelPrintBarcode.Text = "Cancel";
            this.btnCancelPrintBarcode.UseVisualStyleBackColor = true;
            this.btnCancelPrintBarcode.Click += new System.EventHandler(this.btnCancelPrintBarcode_Click);
            // 
            // btnOKPrintBarCode
            // 
            this.btnOKPrintBarCode.Location = new System.Drawing.Point(42, 217);
            this.btnOKPrintBarCode.Name = "btnOKPrintBarCode";
            this.btnOKPrintBarCode.Size = new System.Drawing.Size(62, 24);
            this.btnOKPrintBarCode.TabIndex = 2007;
            this.btnOKPrintBarCode.Text = "OK";
            this.btnOKPrintBarCode.UseVisualStyleBackColor = true;
            this.btnOKPrintBarCode.Click += new System.EventHandler(this.btnOKPrintBarCode_Click);
            // 
            // txtNoOfPrint
            // 
            this.txtNoOfPrint.Location = new System.Drawing.Point(114, 16);
            this.txtNoOfPrint.MaxLength = 5;
            this.txtNoOfPrint.Name = "txtNoOfPrint";
            this.txtNoOfPrint.Size = new System.Drawing.Size(100, 20);
            this.txtNoOfPrint.TabIndex = 2001;
            this.txtNoOfPrint.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNoOfPrint.TextChanged += new System.EventHandler(this.txtNoOfPrint_TextChanged);
            this.txtNoOfPrint.Leave += new System.EventHandler(this.txtNoOfPrint_Leave);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Location = new System.Drawing.Point(8, 19);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(65, 13);
            this.label21.TabIndex = 1000201;
            this.label21.Text = "No Of Print :";
            // 
            // btnImports
            // 
            this.btnImports.Location = new System.Drawing.Point(674, 468);
            this.btnImports.Name = "btnImports";
            this.btnImports.Size = new System.Drawing.Size(153, 29);
            this.btnImports.TabIndex = 190023;
            this.btnImports.Text = "Import Items";
            this.btnImports.UseVisualStyleBackColor = true;
            this.btnImports.Click += new System.EventHandler(this.btnImports_Click);
            // 
            // lblCollectionDetails
            // 
            this.lblCollectionDetails.BackColor = System.Drawing.Color.Coral;
            this.lblCollectionDetails.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblCollectionDetails.ForeColor = System.Drawing.Color.White;
            this.lblCollectionDetails.Location = new System.Drawing.Point(0, 0);
            this.lblCollectionDetails.Name = "lblCollectionDetails";
            this.lblCollectionDetails.Size = new System.Drawing.Size(838, 23);
            this.lblCollectionDetails.TabIndex = 190022;
            this.lblCollectionDetails.Text = "Product Master";
            this.lblCollectionDetails.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbGSTDetails
            // 
            this.gbGSTDetails.Controls.Add(this.TxtCESS);
            this.gbGSTDetails.Controls.Add(this.label33);
            this.gbGSTDetails.Controls.Add(this.txtHSNCode);
            this.gbGSTDetails.Controls.Add(this.btnAddGST);
            this.gbGSTDetails.Controls.Add(this.txtSGST);
            this.gbGSTDetails.Controls.Add(this.label27);
            this.gbGSTDetails.Controls.Add(this.txtSearchBarCode);
            this.gbGSTDetails.Controls.Add(this.label23);
            this.gbGSTDetails.Controls.Add(this.txtCGST);
            this.gbGSTDetails.Controls.Add(this.label42);
            this.gbGSTDetails.Controls.Add(this.txtUTGST);
            this.gbGSTDetails.Controls.Add(this.label40);
            this.gbGSTDetails.Controls.Add(this.txtIGST);
            this.gbGSTDetails.Controls.Add(this.label41);
            this.gbGSTDetails.Controls.Add(this.label43);
            this.gbGSTDetails.Controls.Add(this.cmbGSTPercent);
            this.gbGSTDetails.Controls.Add(this.label44);
            this.gbGSTDetails.Controls.Add(this.label45);
            this.gbGSTDetails.Controls.Add(this.txtMaxLevelQty);
            this.gbGSTDetails.Controls.Add(this.label17);
            this.gbGSTDetails.Controls.Add(this.txtMinLevelQty);
            this.gbGSTDetails.Controls.Add(this.label18);
            this.gbGSTDetails.Location = new System.Drawing.Point(6, 323);
            this.gbGSTDetails.Name = "gbGSTDetails";
            this.gbGSTDetails.Size = new System.Drawing.Size(825, 112);
            this.gbGSTDetails.TabIndex = 190021;
            this.gbGSTDetails.TabStop = false;
            this.gbGSTDetails.Text = "GST Details";
            // 
            // TxtCESS
            // 
            this.TxtCESS.Location = new System.Drawing.Point(684, 52);
            this.TxtCESS.Name = "TxtCESS";
            this.TxtCESS.ReadOnly = true;
            this.TxtCESS.Size = new System.Drawing.Size(77, 20);
            this.TxtCESS.TabIndex = 16000032;
            this.TxtCESS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(625, 56);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(46, 13);
            this.label33.TabIndex = 16000031;
            this.label33.Text = "CESS %";
            // 
            // txtHSNCode
            // 
            this.txtHSNCode.Location = new System.Drawing.Point(73, 15);
            this.txtHSNCode.Name = "txtHSNCode";
            this.txtHSNCode.Size = new System.Drawing.Size(117, 20);
            this.txtHSNCode.TabIndex = 18;
            this.txtHSNCode.TextChanged += new System.EventHandler(this.txtHSNCode_TextChanged);
            // 
            // btnAddGST
            // 
            this.btnAddGST.Location = new System.Drawing.Point(655, 8);
            this.btnAddGST.Name = "btnAddGST";
            this.btnAddGST.Size = new System.Drawing.Size(57, 24);
            this.btnAddGST.TabIndex = 190021;
            this.btnAddGST.Text = "Add";
            this.btnAddGST.UseVisualStyleBackColor = true;
            this.btnAddGST.Click += new System.EventHandler(this.btnAddGST_Click);
            // 
            // txtSGST
            // 
            this.txtSGST.Location = new System.Drawing.Point(535, 52);
            this.txtSGST.Name = "txtSGST";
            this.txtSGST.ReadOnly = true;
            this.txtSGST.Size = new System.Drawing.Size(77, 20);
            this.txtSGST.TabIndex = 16000028;
            this.txtSGST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(475, 56);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(47, 13);
            this.label27.TabIndex = 16000027;
            this.label27.Text = "SGST %";
            // 
            // txtSearchBarCode
            // 
            this.txtSearchBarCode.Location = new System.Drawing.Point(662, 87);
            this.txtSearchBarCode.Name = "txtSearchBarCode";
            this.txtSearchBarCode.Size = new System.Drawing.Size(159, 20);
            this.txtSearchBarCode.TabIndex = 10003;
            this.txtSearchBarCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearchBarCode_KeyDown);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(533, 90);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(109, 13);
            this.label23.TabIndex = 10002;
            this.label23.Text = "BarCode Search(F7) :";
            // 
            // txtCGST
            // 
            this.txtCGST.Location = new System.Drawing.Point(227, 52);
            this.txtCGST.Name = "txtCGST";
            this.txtCGST.ReadOnly = true;
            this.txtCGST.Size = new System.Drawing.Size(77, 20);
            this.txtCGST.TabIndex = 16000026;
            this.txtCGST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(167, 56);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(47, 13);
            this.label42.TabIndex = 16000025;
            this.label42.Text = "CGST %";
            // 
            // txtUTGST
            // 
            this.txtUTGST.Location = new System.Drawing.Point(385, 52);
            this.txtUTGST.Name = "txtUTGST";
            this.txtUTGST.ReadOnly = true;
            this.txtUTGST.Size = new System.Drawing.Size(77, 20);
            this.txtUTGST.TabIndex = 16000024;
            this.txtUTGST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(317, 56);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(55, 13);
            this.label40.TabIndex = 16000023;
            this.label40.Text = "UTGST %";
            // 
            // txtIGST
            // 
            this.txtIGST.Location = new System.Drawing.Point(77, 52);
            this.txtIGST.Name = "txtIGST";
            this.txtIGST.ReadOnly = true;
            this.txtIGST.Size = new System.Drawing.Size(77, 20);
            this.txtIGST.TabIndex = 16000022;
            this.txtIGST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(21, 56);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(43, 13);
            this.label41.TabIndex = 10012;
            this.label41.Text = "IGST %";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(211, 19);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(40, 13);
            this.label43.TabIndex = 10011;
            this.label43.Text = "GST %";
            // 
            // cmbGSTPercent
            // 
            this.cmbGSTPercent.FormattingEnabled = true;
            this.cmbGSTPercent.Location = new System.Drawing.Point(272, 14);
            this.cmbGSTPercent.Name = "cmbGSTPercent";
            this.cmbGSTPercent.Size = new System.Drawing.Size(362, 21);
            this.cmbGSTPercent.TabIndex = 19;
            this.cmbGSTPercent.SelectedValueChanged += new System.EventHandler(this.cmbGSTPercent_SelectedValueChanged);
            this.cmbGSTPercent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbGSTPercent_KeyDown);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(12, 19);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(40, 13);
            this.label44.TabIndex = 10009;
            this.label44.Text = "HSN #";
            // 
            // label45
            // 
            this.label45.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label45.Location = new System.Drawing.Point(2, 45);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(819, 39);
            this.label45.TabIndex = 16000029;
            // 
            // txtMaxLevelQty
            // 
            this.txtMaxLevelQty.Location = new System.Drawing.Point(761, 36);
            this.txtMaxLevelQty.Name = "txtMaxLevelQty";
            this.txtMaxLevelQty.Size = new System.Drawing.Size(81, 20);
            this.txtMaxLevelQty.TabIndex = 10;
            this.txtMaxLevelQty.Text = "0.00";
            this.txtMaxLevelQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMaxLevelQty.Visible = false;
            this.txtMaxLevelQty.TextChanged += new System.EventHandler(this.txtMasked_TextChanged);
            this.txtMaxLevelQty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMaxLevelQty_KeyDown);
            this.txtMaxLevelQty.Leave += new System.EventHandler(this.txtMaxLevelQty_Leave);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(407, 39);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(72, 13);
            this.label17.TabIndex = 171;
            this.label17.Text = "Min Level Qty";
            this.label17.Visible = false;
            // 
            // txtMinLevelQty
            // 
            this.txtMinLevelQty.Location = new System.Drawing.Point(542, 36);
            this.txtMinLevelQty.Name = "txtMinLevelQty";
            this.txtMinLevelQty.Size = new System.Drawing.Size(94, 20);
            this.txtMinLevelQty.TabIndex = 9;
            this.txtMinLevelQty.Text = "0.00";
            this.txtMinLevelQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMinLevelQty.Visible = false;
            this.txtMinLevelQty.TextChanged += new System.EventHandler(this.txtMasked_TextChanged);
            this.txtMinLevelQty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMinLevelQty_KeyDown);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(655, 39);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(81, 13);
            this.label18.TabIndex = 173;
            this.label18.Text = "Max Level Qty  ";
            this.label18.Visible = false;
            // 
            // btnPrintBarCodeAdvance
            // 
            this.btnPrintBarCodeAdvance.Location = new System.Drawing.Point(590, 439);
            this.btnPrintBarCodeAdvance.Name = "btnPrintBarCodeAdvance";
            this.btnPrintBarCodeAdvance.Size = new System.Drawing.Size(80, 60);
            this.btnPrintBarCodeAdvance.TabIndex = 10006;
            this.btnPrintBarCodeAdvance.Text = "BarCode \r\nPrint &Advance";
            this.btnPrintBarCodeAdvance.UseVisualStyleBackColor = true;
            this.btnPrintBarCodeAdvance.Click += new System.EventHandler(this.btnPrintBarCodeAdvance_Click);
            // 
            // lblmsg
            // 
            this.lblmsg.AutoSize = true;
            this.lblmsg.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmsg.Location = new System.Drawing.Point(9, 505);
            this.lblmsg.Name = "lblmsg";
            this.lblmsg.Size = new System.Drawing.Size(302, 17);
            this.lblmsg.TabIndex = 10004;
            this.lblmsg.Text = "This Barcode Present In Gobal Master";
            this.lblmsg.Visible = false;
            // 
            // btnPrintBarCode
            // 
            this.btnPrintBarCode.Location = new System.Drawing.Point(504, 439);
            this.btnPrintBarCode.Name = "btnPrintBarCode";
            this.btnPrintBarCode.Size = new System.Drawing.Size(80, 60);
            this.btnPrintBarCode.TabIndex = 31;
            this.btnPrintBarCode.Text = "BarCode \r\n&Print";
            this.btnPrintBarCode.UseVisualStyleBackColor = true;
            this.btnPrintBarCode.Click += new System.EventHandler(this.btnPrintBarCode_Click);
            // 
            // btnNewBrand
            // 
            this.btnNewBrand.Location = new System.Drawing.Point(421, 439);
            this.btnNewBrand.Name = "btnNewBrand";
            this.btnNewBrand.Size = new System.Drawing.Size(80, 60);
            this.btnNewBrand.TabIndex = 30;
            this.btnNewBrand.Text = "New\r\n&Brand (F4)";
            this.btnNewBrand.UseVisualStyleBackColor = true;
            this.btnNewBrand.Click += new System.EventHandler(this.btnNewBrand_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(173, 439);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(80, 60);
            this.btnSearch.TabIndex = 27;
            this.btnSearch.Text = "Searc&h";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(256, 439);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(80, 60);
            this.btnDelete.TabIndex = 28;
            this.btnDelete.Text = "&Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btndelete_Click);
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(754, 439);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(37, 27);
            this.btnNext.TabIndex = 34;
            this.btnNext.Text = ">";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // BtnExit
            // 
            this.BtnExit.Location = new System.Drawing.Point(339, 439);
            this.BtnExit.Name = "BtnExit";
            this.BtnExit.Size = new System.Drawing.Size(80, 60);
            this.BtnExit.TabIndex = 29;
            this.BtnExit.Text = "Exit";
            this.BtnExit.UseVisualStyleBackColor = true;
            this.BtnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.Location = new System.Drawing.Point(714, 439);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(37, 27);
            this.btnPrev.TabIndex = 33;
            this.btnPrev.Text = "<";
            this.btnPrev.UseVisualStyleBackColor = true;
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // btnFirst
            // 
            this.btnFirst.Location = new System.Drawing.Point(674, 439);
            this.btnFirst.Name = "btnFirst";
            this.btnFirst.Size = new System.Drawing.Size(37, 27);
            this.btnFirst.TabIndex = 32;
            this.btnFirst.Text = "|<";
            this.btnFirst.UseVisualStyleBackColor = true;
            this.btnFirst.Click += new System.EventHandler(this.btnFirst_Click);
            // 
            // btnLast
            // 
            this.btnLast.Location = new System.Drawing.Point(794, 439);
            this.btnLast.Name = "btnLast";
            this.btnLast.Size = new System.Drawing.Size(37, 27);
            this.btnLast.TabIndex = 35;
            this.btnLast.Text = ">|";
            this.btnLast.UseVisualStyleBackColor = true;
            this.btnLast.Click += new System.EventHandler(this.btnLast_Click);
            // 
            // cmbHSNCode
            // 
            this.cmbHSNCode.FormattingEnabled = true;
            this.cmbHSNCode.Location = new System.Drawing.Point(457, 337);
            this.cmbHSNCode.Name = "cmbHSNCode";
            this.cmbHSNCode.Size = new System.Drawing.Size(152, 21);
            this.cmbHSNCode.TabIndex = 18;
            this.cmbHSNCode.Visible = false;
            this.cmbHSNCode.SelectedValueChanged += new System.EventHandler(this.cmbHSNCode_SelectedValueChanged);
            this.cmbHSNCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbHSNCode_KeyDown);
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(7, 439);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(80, 60);
            this.BtnSave.TabIndex = 20;
            this.BtnSave.Text = "&Save (F8)";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(90, 439);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(80, 60);
            this.btnUpdate.TabIndex = 25;
            this.btnUpdate.Text = "&Update";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(90, 439);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 60);
            this.btnCancel.TabIndex = 26;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(7, 439);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(80, 60);
            this.btnNew.TabIndex = 24;
            this.btnNew.Text = "&New";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.VisibleChanged += new System.EventHandler(this.btnNew_VisibleChanged);
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnPurVat);
            this.groupBox1.Controls.Add(this.btnSalesVat);
            this.groupBox1.Controls.Add(this.lblStar10);
            this.groupBox1.Controls.Add(this.lblStar9);
            this.groupBox1.Controls.Add(this.cmbVatPurchase);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.cmbVatSales);
            this.groupBox1.Location = new System.Drawing.Point(918, 343);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(439, 54);
            this.groupBox1.TabIndex = 106;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tax Details";
            this.groupBox1.Visible = false;
            // 
            // btnPurVat
            // 
            this.btnPurVat.Location = new System.Drawing.Point(370, 25);
            this.btnPurVat.Name = "btnPurVat";
            this.btnPurVat.Size = new System.Drawing.Size(62, 24);
            this.btnPurVat.TabIndex = 190020;
            this.btnPurVat.Text = "Add";
            this.btnPurVat.UseVisualStyleBackColor = true;
            this.btnPurVat.Click += new System.EventHandler(this.btnPurVat_Click);
            // 
            // btnSalesVat
            // 
            this.btnSalesVat.Location = new System.Drawing.Point(189, 25);
            this.btnSalesVat.Name = "btnSalesVat";
            this.btnSalesVat.Size = new System.Drawing.Size(62, 24);
            this.btnSalesVat.TabIndex = 190019;
            this.btnSalesVat.Text = "Add";
            this.btnSalesVat.UseVisualStyleBackColor = true;
            // 
            // lblStar10
            // 
            this.lblStar10.AutoSize = true;
            this.lblStar10.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar10.Location = new System.Drawing.Point(353, 33);
            this.lblStar10.Name = "lblStar10";
            this.lblStar10.Size = new System.Drawing.Size(11, 13);
            this.lblStar10.TabIndex = 190018;
            this.lblStar10.Text = "*";
            // 
            // lblStar9
            // 
            this.lblStar9.AutoSize = true;
            this.lblStar9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar9.Location = new System.Drawing.Point(172, 31);
            this.lblStar9.Name = "lblStar9";
            this.lblStar9.Size = new System.Drawing.Size(11, 13);
            this.lblStar9.TabIndex = 190017;
            this.lblStar9.Text = "*";
            // 
            // cmbVatPurchase
            // 
            this.cmbVatPurchase.FormattingEnabled = true;
            this.cmbVatPurchase.Location = new System.Drawing.Point(257, 27);
            this.cmbVatPurchase.MaxLength = 25;
            this.cmbVatPurchase.Name = "cmbVatPurchase";
            this.cmbVatPurchase.Size = new System.Drawing.Size(90, 21);
            this.cmbVatPurchase.TabIndex = 19234432;
            this.cmbVatPurchase.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbVatPurchase_KeyDown);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(284, 10);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(52, 13);
            this.label26.TabIndex = 10009;
            this.label26.Text = "Purchase";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(101, 11);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(33, 13);
            this.label25.TabIndex = 10008;
            this.label25.Text = "Sales";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(14, 33);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(28, 13);
            this.label24.TabIndex = 10007;
            this.label24.Text = "VAT";
            // 
            // cmbVatSales
            // 
            this.cmbVatSales.FormattingEnabled = true;
            this.cmbVatSales.Location = new System.Drawing.Point(69, 26);
            this.cmbVatSales.Name = "cmbVatSales";
            this.cmbVatSales.Size = new System.Drawing.Size(100, 21);
            this.cmbVatSales.TabIndex = 183432432;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(917, 118);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(65, 13);
            this.label19.TabIndex = 175;
            this.label19.Text = "Department ";
            this.label19.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(917, 81);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 59;
            this.label7.Text = "Category ";
            this.label7.Visible = false;
            // 
            // cmbGroupNo2
            // 
            this.cmbGroupNo2.FormattingEnabled = true;
            this.cmbGroupNo2.Location = new System.Drawing.Point(1052, 78);
            this.cmbGroupNo2.MaxLength = 25;
            this.cmbGroupNo2.Name = "cmbGroupNo2";
            this.cmbGroupNo2.Size = new System.Drawing.Size(300, 21);
            this.cmbGroupNo2.TabIndex = 900000;
            this.cmbGroupNo2.Visible = false;
            this.cmbGroupNo2.SelectedIndexChanged += new System.EventHandler(this.cmbGroupNo2_SelectedIndexChanged);
            this.cmbGroupNo2.Leave += new System.EventHandler(this.cmbGroupNo2_Leave);
            this.cmbGroupNo2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbGroupNo2_KeyDown);
            // 
            // cmbStockDept
            // 
            this.cmbStockDept.FormattingEnabled = true;
            this.cmbStockDept.Location = new System.Drawing.Point(101, 201);
            this.cmbStockDept.MaxLength = 25;
            this.cmbStockDept.Name = "cmbStockDept";
            this.cmbStockDept.Size = new System.Drawing.Size(300, 21);
            this.cmbStockDept.TabIndex = 8564;
            this.cmbStockDept.Visible = false;
            this.cmbStockDept.Leave += new System.EventHandler(this.cmbStockDept_Leave);
            this.cmbStockDept.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbStockDept_KeyDown);
            // 
            // lblStar2
            // 
            this.lblStar2.AutoSize = true;
            this.lblStar2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar2.Location = new System.Drawing.Point(1358, 118);
            this.lblStar2.Name = "lblStar2";
            this.lblStar2.Size = new System.Drawing.Size(11, 13);
            this.lblStar2.TabIndex = 190017;
            this.lblStar2.Text = "*";
            this.lblStar2.Visible = false;
            // 
            // lblStar3
            // 
            this.lblStar3.AutoSize = true;
            this.lblStar3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar3.Location = new System.Drawing.Point(1358, 81);
            this.lblStar3.Name = "lblStar3";
            this.lblStar3.Size = new System.Drawing.Size(11, 13);
            this.lblStar3.TabIndex = 190018;
            this.lblStar3.Text = "*";
            this.lblStar3.Visible = false;
            // 
            // txtDiscPer
            // 
            this.txtDiscPer.Enabled = false;
            this.txtDiscPer.Location = new System.Drawing.Point(538, 239);
            this.txtDiscPer.Name = "txtDiscPer";
            this.txtDiscPer.ReadOnly = true;
            this.txtDiscPer.Size = new System.Drawing.Size(77, 20);
            this.txtDiscPer.TabIndex = 200018;
            this.txtDiscPer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(537, 220);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(15, 13);
            this.label36.TabIndex = 200019;
            this.label36.Text = "%";
            // 
            // StockItemSAE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(840, 534);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.cmbGroupNo2);
            this.Controls.Add(this.lblStar3);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cmbStockDept);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblStar2);
            this.Controls.Add(this.label19);
            this.Name = "StockItemSAE";
            this.Text = "Item Master";
            this.Deactivate += new System.EventHandler(this.StockItemSAE_Deactivate);
            this.Load += new System.EventHandler(this.StockItemAE_Load);
            this.Activated += new System.EventHandler(this.StockItemAE_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.StockItemAE_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.StockItemAE_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.EP)).EndInit();
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlHamli.ResumeLayout(false);
            this.pnlHamli.PerformLayout();
            this.pnlBarCodePrint.ResumeLayout(false);
            this.pnlBarCodePrint.PerformLayout();
            this.gbGSTDetails.ResumeLayout(false);
            this.gbGSTDetails.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkActive;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtItemName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ErrorProvider EP;
        private JitControls.OMBPanel panel1;
        private System.Windows.Forms.ComboBox cmbGroupNo1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.Button btnLast;
        private System.Windows.Forms.Button btnFirst;
        private System.Windows.Forms.Button BtnExit;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.ComboBox cmbDepartmentName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbGroupNo2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbCategoryName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtBarcobe;
        private System.Windows.Forms.TextBox txtMRP;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cmbUom;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtPurRate;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblPur;
        private System.Windows.Forms.TextBox txtLanguage;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbStockLocationName;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtMinLevelQty;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtMaxLevelQty;
        private System.Windows.Forms.TextBox txtSearchBarCode;
        private System.Windows.Forms.Label label23;
        private JitControls.OMBPanel pnlMain;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox cmbVatSales;
        private System.Windows.Forms.ComboBox cmbVatPurchase;
        private System.Windows.Forms.ComboBox cmbStockDept;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btnNewBrand;
        private System.Windows.Forms.Label lblStar2;
        private System.Windows.Forms.Label lblStar10;
        private System.Windows.Forms.Label lblStar9;
        private System.Windows.Forms.Label lblStar4;
        private System.Windows.Forms.Label lblStar3;
        private System.Windows.Forms.Label lblStar8;
        private System.Windows.Forms.Label lblStar1;
        private System.Windows.Forms.Label lblStar5;
        private System.Windows.Forms.TextBox txtASaleRate;
        private System.Windows.Forms.Label lblASaleRate;
        private System.Windows.Forms.TextBox txtBSaleRate;
        private System.Windows.Forms.Label lblBSaleRate;
        private System.Windows.Forms.TextBox txtItemShortDesc;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblStar14;
        private System.Windows.Forms.Label lblStar13;
        private System.Windows.Forms.Label lblStar12;
        private System.Windows.Forms.TextBox txtLangShortDesc;
        private System.Windows.Forms.TextBox txtLangFullDesc;
        private System.Windows.Forms.Label lblStar15;
        private System.Windows.Forms.Label lblStar16;
        private System.Windows.Forms.Button btnLangLongDesc;
        private System.Windows.Forms.Button btnLangShortDesc;
        private System.Windows.Forms.Label lblChkHelp;
        private System.Windows.Forms.Label lblStar17;
        private System.Windows.Forms.TextBox txtShortCode;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnPrintBarCode;
        private JitControls.OMPanel pnlBarCodePrint;
        private System.Windows.Forms.RadioButton rbSmallMode;
        private System.Windows.Forms.RadioButton rbBigMod;
        private System.Windows.Forms.TextBox txtStartNo;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button btnCancelPrintBarcode;
        private System.Windows.Forms.Button btnOKPrintBarCode;
        private System.Windows.Forms.TextBox txtNoOfPrint;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtpPackingDate;
        private System.Windows.Forms.TextBox txtExpDays;
        private System.Windows.Forms.TextBox txtwt;
        private System.Windows.Forms.Label lblExpDate;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button btnSalesVat;
        private System.Windows.Forms.Button btnPurVat;
        private System.Windows.Forms.Label lblmsg;
        private System.Windows.Forms.Label lblManufacturerCompanyName;
        private System.Windows.Forms.Button btnPrintBarCodeAdvance;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtMargin;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.ComboBox cmbDiscountType;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox cmbGodownNo;
        private System.Windows.Forms.Panel pnlHamli;
        private System.Windows.Forms.TextBox txtHamali;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.GroupBox gbGSTDetails;
        private System.Windows.Forms.TextBox txtSGST;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtCGST;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtUTGST;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txtIGST;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.ComboBox cmbGSTPercent;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.ComboBox cmbHSNCode;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Button btnAddGST;
        private System.Windows.Forms.TextBox txtHSNCode;
        private System.Windows.Forms.Label lblCollectionDetails;
        private System.Windows.Forms.Button btnImports;
        private System.Windows.Forms.Label lblChkHelp1;
        private System.Windows.Forms.CheckBox chkIsQtyRead;
        private System.Windows.Forms.TextBox TxtCESS;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtDiscPer;
    }
}