﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using System.Data.SqlClient;
using JitControls;

namespace Kirana.Master
{
    public partial class ItemAdvanceSearch : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Color clrColorRow = Color.FromArgb(255, 224, 192);
        public long ItemNo = 0;
        public string BarCode = "";
        public string ItemName = "";
        int SearchTimerCount = 0;
        private string RateType = "ASaleRate";
        DataSet ds;

        public ItemAdvanceSearch()
        {
            InitializeComponent();
        }



        public ItemAdvanceSearch(DataSet ds)
        {
            InitializeComponent();
            this.ds = ds;



        }

        public void BindGrid()
        {
            /*13-Apr-2014
            * Exception Handling Purpose.
            * */
            try
            {
                DataView dv = new DataView();
                dv = ds.Tables[0].DefaultView;
                dataGridView1.DataSource = dv.Table;
                dataGridView1.CurrentCell = null;
              //  if (dataGridView1.Rows.Count > 0)
                //{
                  //  dataGridView1.CurrentCell = dataGridView1[1, 0];
                  //  dataGridView1_CurrentCellChanged(dataGridView1, new EventArgs());
               // }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);//For Common Error Displayed Purpose
            }
        }

        public DataView GetBySearch(int Column, string Value)
        {
            string sql = null;
            Value = Value.Trim().Replace("'", "''").Replace(" ", "%");
            sql = " SELECT MStockItems.ShowItemName, MUOM.UOMName AS UOM, MStockBarcode.Barcode,(CONVERT(DECIMAL(10,2)," + RateType + "))  As Rate ," +
                  " (CONVERT(DECIMAL(10,2), MRateSetting.MRP)) AS MRP, " +
                 " IsNull(MStockItemBalance.CurrentStock,0) AS CurrentStock, MStockItems.ItemNo  " +
                 " FROM MStockItems INNER JOIN " +
                 " MUOM ON MStockItems.UOMDefault = MUOM.UOMNo INNER JOIN " +
                 " MStockBarcode ON MStockItems.ItemNo = MStockBarcode.ItemNo INNER JOIN " +
                 " MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo LEFT OUTER JOIN " +
                 " MStockItemBalance ON MRateSetting.MRP = MStockItemBalance.MRP AND MRateSetting.ItemNo = MStockItemBalance.ItemNo " +
                 " WHERE MStockItems.ItemNo <> 1 " +
                 " AND ( MStockItems.ShowItemName like '%" + Value + "%' " +
                 " OR MStockBarcode.Barcode='" + Value + "' " +
                 " ) " +
                 " AND MStockItems.IsActive='true' " +
                 " ORDER BY MStockItems.ShowItemName";

            DataSet ds = new DataSet();
            /*13-Apr-2014
            * Exception Handling Purpose.
            * */
            try
            {
                ds = ObjDset.FillDset("New", sql, CommonFunctions.ConStr);
            }
            catch (SqlException e)
            {
                CommonFunctions.ErrorMessge = e.Message;
            }
            return ds.Tables[0].DefaultView;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            BindGrid();
        }

        private void TxtSearch_TextChanged(object sender, EventArgs e)
        {


            try
            {
                DataView DV = new DataView(ds.Tables[0].DefaultView.Table);
                DV.RowFilter = string.Format("ShowItemName LIKE '%{0}%' OR Barcode='{1}'", TxtSearch.Text, TxtSearch.Text);
                DV.Sort = "ShowItemName ASC";
                dataGridView1.DataSource = DV;
                dataGridView1.CurrentCell = null;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace);
            }

            // (dataGridView1.DataSource as DataTable).DefaultView.RowFilter = " ShowItemName  like '%" + TxtSearch.Text.Trim() + "%' OR  Barcode='" + TxtSearch.Text.Trim() + "' ";
        }

        private void TxtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                if (dataGridView1.Rows.Count > 0)
                {
                    dataGridView1.Focus();
                    dataGridView1.CurrentCell = dataGridView1[1, 0];
                }
            }
            else if (e.KeyCode == Keys.Enter)
            {
                    dataGridView1_KeyDown(sender, new KeyEventArgs(Keys.Enter));
            }
            else if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (dataGridView1.CurrentCell == null)
                    return;
                ItemNo = Convert.ToInt64(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[6].Value);
                BarCode = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[2].Value.ToString();
                ItemName = dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();
                this.Close();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
            else if (e.KeyCode == Keys.Home)
            {
                TxtSearch.Focus();
            }
        }

        private void AdvancedSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void dataGridView1_CurrentCellChanged(object sender, EventArgs e)
        {
            /*13-Apr-2014
            * Exception Handling Purpose.
            * */
            try
            {
                if (dataGridView1.CurrentCell != null)
                {
                    for (int i = 0; i < dataGridView1.Rows.Count; i++)
                    {
                        dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.White;
                    }
                    //dataGridView1.Rows[dataGridView1.CurrentCell.RowIndex].DefaultCellStyle.BackColor = clrColorRow;
                    dataGridView1.CurrentRow.DefaultCellStyle.SelectionBackColor = Color.Yellow;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);//For Common Error Displayed Purpose
            }
        }

        private void ItemAdvanceSearch_Load(object sender, EventArgs e)
        {


            dataGridView1.RowTemplate.DefaultCellStyle.Font = null;
            dataGridView1.DefaultCellStyle.Font = ObjFunction.GetFont(FontStyle.Regular, 14);
            dataGridView1.RowTemplate.Height = 26;
            dataGridView1.BackgroundColor = Color.FromArgb(255, 255, 210);
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

        }

        private void ItemAdvanceSearch_Shown(object sender, EventArgs e)
        {
            ItemNo = 0;
            BarCode = "";
            ItemName = "";
            TxtSearch.Font = ObjFunction.GetFont(FontStyle.Regular, 16);
            BindGrid();
        }

        private void tmrSearch_Tick(object sender, EventArgs e)
        {
            if (SearchTimerCount >= 7)
            {
                tmrSearch.Enabled = false;
                SearchTimerCount = 0;

            }
            else
            {
                SearchTimerCount++;
            }
        }
    }
}
