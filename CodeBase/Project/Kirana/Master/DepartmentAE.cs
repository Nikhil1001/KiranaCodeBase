﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Master
{
    /// <summary>
    /// This class is used for Department AE
    /// </summary>
    public partial class DepartmentAE : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        DBMStockDepartment dbMStockDepartment = new DBMStockDepartment();
        MStockDepartment mStockDepartment = new MStockDepartment();
        string DeptNm; 
        DataTable dtSearch = new DataTable();
        int cntRow;
        long ID;

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public DepartmentAE()
        {
            InitializeComponent();
        }

        private void DepartmentAE_Load(object sender, EventArgs e)
        {
            try
            {
                ObjFunction.LockButtons(true, this.Controls);
                ObjFunction.LockControls(false, this.Controls);
                DeptNm = "";
                dtSearch = ObjFunction.GetDataView("Select DepartmentNo From MStockDepartment order by DepartmentName").Table;
                ObjFunction.FillCombo(cmbControlGroup, "SELECT     DepartmentNo, DepartmentName FROM         MStockDepartment WHERE     (IsActive = 'True') ORDER BY DepartmentName");
                if (Department.RequestDeptNo != 0)
                {
                    DeptNm = "";
                    FillControls();
                    dtSearch = ObjFunction.GetDataView("Select DepartmentNo From MStockDepartment").Table;
                    SetNavigation();
                    setDisplay(true);
                }
                else
                {
                    setDisplay(false);
                }
                KeyDownFormat(this.Controls);
                if (dtSearch.Rows.Count > 0)
                {
                    if (Department.RequestDeptNo == 0)
                        ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    else
                        ID = Department.RequestDeptNo;
                    FillControls();
                    SetNavigation();
                }
                setDisplay(true);
                btnNew.Focus();
                KeyDownFormat(this.Controls);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillControls()
        {
            try
            {
                EP.SetError(txtDeptName, "");

                mStockDepartment = dbMStockDepartment.ModifyMStockDepartmentByID(ID);
                DeptNm = mStockDepartment.DepartmentName;
                txtDeptName.Text = mStockDepartment.DepartmentName;
                cmbControlGroup.SelectedValue = mStockDepartment.ControlGroup.ToString();
                chkActive.Checked = mStockDepartment.IsActive;
                if (chkActive.Checked == true)
                    chkActive.Text = "Yes";
                else
                    chkActive.Text = "No";
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void SetValue()
        {
            try
            {
                if (Validations() == true)
                {
                    dbMStockDepartment = new DBMStockDepartment();
                    mStockDepartment = new MStockDepartment();
                    mStockDepartment.DepartmentNo = ID;

                    mStockDepartment.DepartmentName = txtDeptName.Text.Trim();
                    mStockDepartment.ControlGroup = ObjFunction.GetComboValue(cmbControlGroup);
                    mStockDepartment.IsActive = chkActive.Checked;
                    mStockDepartment.UserId = DBGetVal.UserID;
                    mStockDepartment.UserDate = DBGetVal.ServerTime.Date;
                    mStockDepartment.CompanyNo = DBGetVal.CompanyNo;

                    if (dbMStockDepartment.AddMStockDepartment(mStockDepartment) == true)
                    {
                        if (ID == 0)
                        {
                            OMMessageBox.Show("Department Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            dtSearch = ObjFunction.GetDataView("Select DepartmentNo From MStockDepartment order by DepartmentName").Table;
                            ID = ObjQry.ReturnLong("Select Max(DepartmentNo) From MStockDepartment", CommonFunctions.ConStr);
                            ObjFunction.FillCombo(cmbControlGroup, "SELECT     DepartmentNo, DepartmentName FROM         MStockDepartment WHERE     (IsActive = 'True') ORDER BY DepartmentName");
                            SetNavigation();
                            FillControls();
                        }
                        else
                        {
                            OMMessageBox.Show("Department Updated Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            FillControls();
                        }

                        ObjFunction.LockButtons(true, this.Controls);
                        ObjFunction.LockControls(false, this.Controls);
                    }
                    else
                    {
                        OMMessageBox.Show("Department not saved", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
            
        }
        

        private void BtnExit_Click(object sender, EventArgs e)
        {
            Department.RequestDeptNo = 0;            
            this.Close();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            SetValue();
        }

        private bool Validations()
        {
            bool flag = false;
            EP.SetError(txtDeptName, ""); 
            if (txtDeptName.Text.Trim() == "")
            {

                EP.SetError(txtDeptName, "Enter Department Name");
                EP.SetIconAlignment(txtDeptName, ErrorIconAlignment.MiddleRight);
                txtDeptName.Focus();
            }            
            else if (DeptNm != txtDeptName.Text.Trim())
            {
                if (ObjQry.ReturnInteger("Select Count(*) from MStockDepartment where DepartmentName = '" + txtDeptName.Text.Trim().Replace("'", "''") + "'", CommonFunctions.ConStr) != 0)
                {
                    EP.SetError(txtDeptName, "Duplicate Deparment Name");
                    EP.SetIconAlignment(txtDeptName, ErrorIconAlignment.MiddleRight);
                    txtDeptName.Focus();
                }
                else
                    flag = true;
            }
            else
                flag = true;
            return flag;
        }

        private void DepartmentAE_FormClosing(object sender, FormClosingEventArgs e)
        {
            Department.RequestDeptNo = 0;
            DeptNm = "";
        }

        private void txtDepartmentName_Leave(object sender, EventArgs e)
        {
            try
            {
                EP.SetError(txtDeptName, "");
                if (txtDeptName.Text.Trim() != "")
                {
                    if (DeptNm != txtDeptName.Text.Trim())
                    {
                        if (ObjQry.ReturnInteger("Select Count(*) from MStockDepartment where DepartmentName = '" + txtDeptName.Text.Trim().Replace("'", "''") + "'", CommonFunctions.ConStr) != 0)
                        {
                            EP.SetError(txtDeptName, "Duplicate Deparment Name");
                            EP.SetIconAlignment(txtDeptName, ErrorIconAlignment.MiddleRight);
                            txtDeptName.Focus();
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #region Navigation Methods
        private void NavigationDisplay(int type)
        {
            try
            {
                long No = 0;
                if (type == 5)
                {
                    No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                    ID = No;
                }
                if (type == 1)
                {
                    No = Convert.ToInt64(dtSearch.Rows[0].ItemArray[0].ToString());
                    cntRow = 0;
                    ID = No;
                }
                else if (type == 2)
                {
                    No = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    cntRow = dtSearch.Rows.Count - 1;
                    ID = No;
                }
                else
                {
                    if (type == 3)
                    {
                        cntRow = cntRow + 1;
                    }
                    else if (type == 4)
                    {
                        cntRow = cntRow - 1;
                    }

                    if (cntRow < 0)
                    {
                        OMMessageBox.Show("This is First Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow + 1;
                    }
                    else if (cntRow > dtSearch.Rows.Count - 1)
                    {
                        OMMessageBox.Show("This is Last Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow - 1;
                    }
                    else
                    {
                        No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                        ID = No;
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
            FillControls();
        }

        private void SetNavigation()
        {
            cntRow = 0;
            for (int i = 0; i < dtSearch.Rows.Count; i++)
            {
                if (Convert.ToInt64(dtSearch.Rows[i].ItemArray[0].ToString()) == ID)
                {
                    cntRow = i;
                    break;
                }
            }
        }

        private void setDisplay(bool flag)
        {
            btnFirst.Visible = flag;
            btnPrev.Visible = flag;
            btnNext.Visible = flag;
            btnLast.Visible = flag;
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            NavigationDisplay(1);
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            NavigationDisplay(4);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            NavigationDisplay(3);
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            NavigationDisplay(2);
        }

        #endregion

        #region KeyDown Events
        private void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is GroupBox)
                    KeyDownFormat(ctrl.Controls);
            }
        }

        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left && e.Control)
            {
                if (btnPrev.Enabled) btnPrev_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Up && e.Control)
            {
                if (btnFirst.Enabled) btnFirst_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Right && e.Control)
            {
                if (btnNext.Enabled) btnNext_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Down && e.Control)
            {
                if (btnLast.Enabled) btnLast_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F2)
            {
                if (BtnSave.Visible) BtnSave_Click(sender, e);
            }
            //else if (e.KeyCode == Keys.Escape)
            //{
            //    BtnExit_Click(sender, e);
            //}
        }
        #endregion

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                ID = 0;
                ObjFunction.InitialiseControl(this.Controls);
                ObjFunction.LockButtons(false, this.Controls);
                ObjFunction.LockControls(true, this.Controls);
                chkActive.Checked = true;
                txtDeptName.Focus();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            ObjFunction.LockButtons(false, this.Controls);
            ObjFunction.LockControls(true, this.Controls);
            chkActive.Checked = true;
            txtDeptName.Focus();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            NavigationDisplay(5);
            ObjFunction.LockButtons(true, this.Controls);
            ObjFunction.LockControls(false, this.Controls);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            Form NewF = new Department();
            this.Close();
            ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (chkActive.Checked == false) return;
                dbMStockDepartment = new DBMStockDepartment();
                mStockDepartment = new MStockDepartment();

                mStockDepartment.DepartmentNo = ID;
                if (OMMessageBox.Show("Are you sure want to delete this record?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (dbMStockDepartment.DeleteMStockDepartment(mStockDepartment) == true)
                    {
                        OMMessageBox.Show("Department Deleted Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        FillControls();
                    }
                    else
                    {
                        OMMessageBox.Show("Department not Deleted", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void chkActive_CheckedChanged(object sender, EventArgs e)
        {
            if (chkActive.Checked == true)
                chkActive.Text = "Yes";
            else
                chkActive.Text = "No";
        }
    }
}
