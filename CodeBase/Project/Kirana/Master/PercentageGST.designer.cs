﻿namespace Kirana.Master
{
    partial class PercentageGST
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.BtnPerCancel = new System.Windows.Forms.Button();
            this.btnPecOk = new System.Windows.Forms.Button();
            this.label32 = new System.Windows.Forms.Label();
            this.EP = new System.Windows.Forms.ErrorProvider(this.components);
            this.lblheader = new System.Windows.Forms.Label();
            this.TxtIGST = new System.Windows.Forms.TextBox();
            this.TxtCGST = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtSGST = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtUTGST = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtCESS = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblTotalGST = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.EP)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnPerCancel
            // 
            this.BtnPerCancel.Location = new System.Drawing.Point(113, 202);
            this.BtnPerCancel.Name = "BtnPerCancel";
            this.BtnPerCancel.Size = new System.Drawing.Size(83, 44);
            this.BtnPerCancel.TabIndex = 6;
            this.BtnPerCancel.Text = "Cancel";
            this.BtnPerCancel.UseVisualStyleBackColor = true;
            this.BtnPerCancel.Click += new System.EventHandler(this.BtnPerCancel_Click);
            // 
            // btnPecOk
            // 
            this.btnPecOk.Location = new System.Drawing.Point(25, 202);
            this.btnPecOk.Name = "btnPecOk";
            this.btnPecOk.Size = new System.Drawing.Size(83, 44);
            this.btnPecOk.TabIndex = 5;
            this.btnPecOk.Text = "Add";
            this.btnPecOk.UseVisualStyleBackColor = true;
            this.btnPecOk.Click += new System.EventHandler(this.btnPecOk_Click);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Location = new System.Drawing.Point(7, 40);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(49, 13);
            this.label32.TabIndex = 58;
            this.label32.Text = "IGST % :";
            // 
            // EP
            // 
            this.EP.ContainerControl = this;
            // 
            // lblheader
            // 
            this.lblheader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblheader.BackColor = OM.ThemeColor.Body_Header_Color;
            this.lblheader.ForeColor = System.Drawing.Color.White;
            this.lblheader.Location = new System.Drawing.Point(0, 0);
            this.lblheader.Name = "lblheader";
            this.lblheader.Size = new System.Drawing.Size(221, 23);
            this.lblheader.TabIndex = 912;
            this.lblheader.Text = "GST Tax Details";
            this.lblheader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TxtIGST
            // 
            this.TxtIGST.Location = new System.Drawing.Point(119, 36);
            this.TxtIGST.MaxLength = 5;
            this.TxtIGST.Name = "TxtIGST";
            this.TxtIGST.Size = new System.Drawing.Size(87, 20);
            this.TxtIGST.TabIndex = 0;
            this.TxtIGST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtIGST.TextChanged += new System.EventHandler(this.txtPercentage_TextChanged);
            this.TxtIGST.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtIGST_KeyDown);
            this.TxtIGST.Leave += new System.EventHandler(this.Txt_Leave);
            // 
            // TxtCGST
            // 
            this.TxtCGST.Location = new System.Drawing.Point(119, 62);
            this.TxtCGST.MaxLength = 5;
            this.TxtCGST.Name = "TxtCGST";
            this.TxtCGST.Size = new System.Drawing.Size(87, 20);
            this.TxtCGST.TabIndex = 1;
            this.TxtCGST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtCGST.TextChanged += new System.EventHandler(this.txtPercentage_TextChanged);
            this.TxtCGST.Leave += new System.EventHandler(this.Txt_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(7, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 913;
            this.label1.Text = "CGST % :";
            // 
            // TxtSGST
            // 
            this.TxtSGST.Location = new System.Drawing.Point(119, 88);
            this.TxtSGST.MaxLength = 5;
            this.TxtSGST.Name = "TxtSGST";
            this.TxtSGST.Size = new System.Drawing.Size(87, 20);
            this.TxtSGST.TabIndex = 2;
            this.TxtSGST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtSGST.TextChanged += new System.EventHandler(this.txtPercentage_TextChanged);
            this.TxtSGST.Leave += new System.EventHandler(this.Txt_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(7, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 915;
            this.label2.Text = "SGST % :";
            // 
            // TxtUTGST
            // 
            this.TxtUTGST.Location = new System.Drawing.Point(119, 114);
            this.TxtUTGST.MaxLength = 5;
            this.TxtUTGST.Name = "TxtUTGST";
            this.TxtUTGST.Size = new System.Drawing.Size(87, 20);
            this.TxtUTGST.TabIndex = 3;
            this.TxtUTGST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtUTGST.TextChanged += new System.EventHandler(this.txtPercentage_TextChanged);
            this.TxtUTGST.Leave += new System.EventHandler(this.Txt_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(7, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 917;
            this.label3.Text = "UTGST % :";
            // 
            // TxtCESS
            // 
            this.TxtCESS.Location = new System.Drawing.Point(119, 140);
            this.TxtCESS.MaxLength = 5;
            this.TxtCESS.Name = "TxtCESS";
            this.TxtCESS.Size = new System.Drawing.Size(87, 20);
            this.TxtCESS.TabIndex = 4;
            this.TxtCESS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtCESS.TextChanged += new System.EventHandler(this.txtPercentage_TextChanged);
            this.TxtCESS.Leave += new System.EventHandler(this.Txt_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(7, 144);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 919;
            this.label4.Text = "CESS % :";
            // 
            // lblTotalGST
            // 
            this.lblTotalGST.AutoSize = true;
            this.lblTotalGST.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalGST.Location = new System.Drawing.Point(7, 174);
            this.lblTotalGST.Name = "lblTotalGST";
            this.lblTotalGST.Size = new System.Drawing.Size(85, 13);
            this.lblTotalGST.TabIndex = 921;
            this.lblTotalGST.Text = "Total GST=40 %";
            // 
            // PercentageGST
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(221, 255);
            this.Controls.Add(this.BtnPerCancel);
            this.Controls.Add(this.lblTotalGST);
            this.Controls.Add(this.btnPecOk);
            this.Controls.Add(this.TxtCESS);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtUTGST);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TxtSGST);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TxtCGST);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtIGST);
            this.Controls.Add(this.lblheader);
            this.Controls.Add(this.label32);
            this.Name = "PercentageGST";
            this.Text = "TaxPercentage";
            this.Load += new System.EventHandler(this.TaxPercentage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.EP)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnPerCancel;
        private System.Windows.Forms.Button btnPecOk;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ErrorProvider EP;
        private System.Windows.Forms.Label lblheader;
        private System.Windows.Forms.TextBox TxtCGST;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtIGST;
        private System.Windows.Forms.TextBox TxtCESS;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtUTGST;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtSGST;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblTotalGST;
    }
}