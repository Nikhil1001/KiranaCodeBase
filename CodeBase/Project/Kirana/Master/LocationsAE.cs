﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Master
{
    /// <summary>
    /// This class is used for LocationsAE
    /// </summary>
    public partial class LocationsAE : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        DBMLocation dbLocation = new DBMLocation();
        MLocation mLocation = new MLocation();
        string LocationNm;
        DataTable dtSearch = new DataTable();
        int cntRow;
        bool isDoProcess = false;
        long ID;

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public LocationsAE()
        {
            InitializeComponent();
        }
        private void LocationAE_Load(object sender, EventArgs e)
        {
            try
            {
                linkLabel1.Visible = false;
                linkLabel2.Visible = false;
                linkLabel3.Visible = false;
                linkLabel4.Visible = false;
                ObjFunction.FillCombo(cmbCountry, "Select CountryNo,CountryName From MCountry Where IsActive = 'True' ORDER BY CountryName");
                ObjFunction.FillCombo(cmbControlGroup, "SELECT LocationNo, LocationName FROM MLocation WHERE (IsActive='True') ORDER BY LocationName");
                ObjFunction.FillCombo(cmbRegion, "SELECT RegionNo, RegionName FROM MRegion Where IsActive = 'True' ORDER BY RegionName");
                ObjFunction.LockButtons(true, this.Controls);
                ObjFunction.LockControls(false, this.Controls);
                LocationNm = "";
                dtSearch = ObjFunction.GetDataView("Select LocationNo From MLocation Where (LocationNo <> 1) order by LocationName").Table;

                if (Locations.RequestLocNo != 0)
                {
                    LocationNm = "";
                    FillControls();
                    dtSearch = ObjFunction.GetDataView("Select LocationNo From MLocation").Table;
                    SetNavigation();
                    setDisplay(true);
                }
                else
                {
                    setDisplay(false);
                }
                //KeyDownFormat(this.Controls);
                if (dtSearch.Rows.Count > 0)
                {
                    if (Locations.RequestLocNo == 0)
                        ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    else
                        ID = Locations.RequestLocNo;
                    FillControls();
                    SetNavigation();
                }
                setDisplay(true);
                btnNew.Focus();
                KeyDownFormat(this.Controls);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillControls()
        {
            try
            {
                EP.SetError(txtLocationName, "");
                EP.SetError(txtshortName, "");
                EP.SetError(cmbCountry, "");
                EP.SetError(cmbState, "");
                EP.SetError(cmbCity, "");
                EP.SetError(cmbControlGroup, "");
                EP.SetError(cmbRegion, "");

                mLocation = dbLocation.ModifyMLocationByID(ID);
                LocationNm = mLocation.LocationName;
                txtLocationName.Text = mLocation.LocationName;
                txtshortName.Text = mLocation.LocationShortCode;

                chkActive.Checked = mLocation.IsActive;
                ObjFunction.FillCombo(cmbRegion, "SELECT RegionNo, RegionName FROM MRegion Where (IsActive = 'True' OR RegionNo=" + mLocation.RegionNo + ")  ORDER BY RegionName");
                cmbRegion.SelectedValue = mLocation.RegionNo.ToString();
                if (chkActive.Checked == true)
                    chkActive.Text = "Yes";
                else
                    chkActive.Text = "No";
                ObjFunction.FillCombo(cmbControlGroup, "SELECT LocationNo, LocationName FROM MLocation WHERE (IsActive='True' OR LocationNo=" + mLocation.ControlGroup + ") ORDER BY LocationName");
                cmbControlGroup.SelectedValue = mLocation.ControlGroup.ToString();
                ObjFunction.FillCombo(cmbCountry, "Select CountryNo,CountryName From MCountry Where (IsActive = 'True' OR CountryNo=" + mLocation.CountryNo + ") ORDER BY CountryName");
                cmbCountry.SelectedValue = mLocation.CountryNo.ToString();
                ObjFunction.FillCombo(cmbState, "Select StateNo,StateName From MState Where CountryNo = " + ObjFunction.GetComboValue(cmbCountry) + " And (IsActive = 'True' OR StateNo=" + mLocation.StateNo + ") ORDER BY StateName");
                cmbState.SelectedValue = mLocation.StateNo.ToString();
                ObjFunction.FillCombo(cmbCity, "Select CityNo, CityName From MCity Where StateNo = " + ObjFunction.GetComboValue(cmbState) + " And (IsActive = 'True' OR CityNo =" + mLocation.CityNo + ") ORDER BY CityName");
                cmbCity.SelectedValue = mLocation.CityNo.ToString();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void SetValue()
        {
            try
            {
                if (Validations() == true)
                {
                    dbLocation = new DBMLocation();
                    mLocation = new MLocation();
                    mLocation.LocationNo = ID;

                    mLocation.LocationName = txtLocationName.Text.Trim();
                    mLocation.LocationShortCode = txtshortName.Text.Trim();
                    mLocation.IsActive = chkActive.Checked;
                    mLocation.RegionNo = ObjFunction.GetComboValue(cmbRegion);
                    mLocation.CountryNo = ObjFunction.GetComboValue(cmbCountry);
                    mLocation.StateNo = ObjFunction.GetComboValue(cmbState);
                    mLocation.CityNo = ObjFunction.GetComboValue(cmbCity);
                    mLocation.ControlGroup = ObjFunction.GetComboValue(cmbControlGroup);
                    mLocation.UserID = DBGetVal.UserID;
                    mLocation.UserDate = DBGetVal.ServerTime.Date;
                    mLocation.CompanyNo = DBGetVal.CompanyNo;

                    if (dbLocation.AddMLocation(mLocation) == true)
                    {
                        OMMessageBox.Show("Location Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        if (ID == 0)
                        {
                            dtSearch = ObjFunction.GetDataView("Select LocationNo From MLocation Where (LocationNo <> 1) order by LocationName").Table;
                            ID = ObjQry.ReturnLong("Select Max(LocationNo) From MLocation", CommonFunctions.ConStr);
                            SetNavigation();
                            FillControls();
                        }
                        else
                        {
                            FillControls();
                        }

                        ObjFunction.LockButtons(true, this.Controls);
                        ObjFunction.LockControls(false, this.Controls);
                        btnNew.Focus();
                    }
                    else
                    {
                        OMMessageBox.Show("Location not saved", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            Locations.RequestLocNo = 0;
            this.Close();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            SetValue();
        }

        private bool Validations()
        {
            bool flag = false;
            EP.SetError(txtLocationName, "");
            EP.SetError(txtshortName, "");
            EP.SetError(cmbCountry, "");
            EP.SetError(cmbState, "");
            EP.SetError(cmbCity, "");
            EP.SetError(cmbControlGroup, "");
            EP.SetError(cmbRegion, "");

            if (txtLocationName.Text.Trim() == "")
            {

                EP.SetError(txtLocationName, "Enter Location Name");
                EP.SetIconAlignment(txtLocationName, ErrorIconAlignment.MiddleRight);
                txtLocationName.Focus();
            }
            else if (txtshortName.Text.Trim() == "")
            {

                EP.SetError(txtshortName, "Enter Short Name");
                EP.SetIconAlignment(txtshortName, ErrorIconAlignment.MiddleRight);
                txtshortName.Focus();
            }
            else if (ObjFunction.GetComboValue(cmbControlGroup) <= 0)
            {
                EP.SetError(cmbControlGroup, "Select Control Group");
                EP.SetIconAlignment(cmbControlGroup, ErrorIconAlignment.MiddleRight);
                cmbControlGroup.Focus();
            }
            else if (ObjFunction.GetComboValue(cmbRegion) <= 0)
            {
                EP.SetError(cmbRegion, "Select Region ");
                EP.SetIconAlignment(cmbRegion, ErrorIconAlignment.MiddleRight);
                cmbRegion.Focus();
            }
            else if (ObjFunction.GetComboValue(cmbCountry) <= 0)
            {
                EP.SetError(cmbCountry, "Enter Country Name");
                EP.SetIconAlignment(cmbCountry, ErrorIconAlignment.MiddleRight);
                cmbCountry.Focus();
             }
            else if (ObjFunction.GetComboValue(cmbState) <= 0)
            {
                EP.SetError(cmbState, "Enter State Name");
                EP.SetIconAlignment(cmbState, ErrorIconAlignment.MiddleRight);
                cmbState.Focus();
            }
            else if (ObjFunction.GetComboValue(cmbCity) <= 0)
            {
                EP.SetError(cmbCity, "Enter City Name");
                EP.SetIconAlignment(cmbCity, ErrorIconAlignment.MiddleRight);
                cmbCity.Focus();
            }
            else if (LocationNm != txtLocationName.Text.Trim())
            {
                if (ObjQry.ReturnInteger("Select Count(*) from MLocation where LocationName = '" + txtLocationName.Text.Trim().Replace("'", "''") + "'", CommonFunctions.ConStr) != 0)
                {
                    EP.SetError(txtLocationName, "Duplicate Location Name");
                    EP.SetIconAlignment(txtLocationName, ErrorIconAlignment.MiddleRight);
                    txtLocationName.Focus();
                }
                else
                    flag = true;
            }
            else
                flag = true;
            return flag;
        }

        private void LocationAE_FormClosing(object sender, FormClosingEventArgs e)
        {
            Locations.RequestLocNo = 0;
            LocationNm = "";
        }

        private void txtLocationName_Leave(object sender, EventArgs e)
        {
            try
            {
                EP.SetError(txtLocationName, "");
                if (txtLocationName.Text.Trim() != "")
                {
                    if (LocationNm != txtLocationName.Text.Trim())
                    {
                        if (ObjQry.ReturnInteger("Select Count(*) from MLocation where LocationName = '" + txtLocationName.Text.Trim().Replace("'", "''") + "'", CommonFunctions.ConStr) != 0)
                        {
                            EP.SetError(txtLocationName, "Duplicate Location Name");
                            EP.SetIconAlignment(txtLocationName, ErrorIconAlignment.MiddleRight);
                            txtLocationName.Focus();
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #region Navigation Methods
        private void NavigationDisplay(int type)
        {
            try
            {
                long No = 0;
                if (type == 5)
                {
                    No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                    ID = No;
                }
                if (type == 1)
                {
                    No = Convert.ToInt64(dtSearch.Rows[0].ItemArray[0].ToString());
                    cntRow = 0;
                    ID = No;
                }
                else if (type == 2)
                {
                    No = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    cntRow = dtSearch.Rows.Count - 1;
                    ID = No;
                }
                else
                {
                    if (type == 3)
                    {
                        cntRow = cntRow + 1;
                    }
                    else if (type == 4)
                    {
                        cntRow = cntRow - 1;
                    }

                    if (cntRow < 0)
                    {
                        OMMessageBox.Show("This is First Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow + 1;
                    }
                    else if (cntRow > dtSearch.Rows.Count - 1)
                    {
                        OMMessageBox.Show("This is Last Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow - 1;
                    }
                    else
                    {
                        No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                        ID = No;
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
            FillControls();

        }

        private void SetNavigation()
        {
            cntRow = 0;
            for (int i = 0; i < dtSearch.Rows.Count; i++)
            {
                if (Convert.ToInt64(dtSearch.Rows[i].ItemArray[0].ToString()) == ID)
                {
                    cntRow = i;
                    break;
                }
            }
        }

        private void setDisplay(bool flag)
        {
            btnFirst.Visible = flag;
            btnPrev.Visible = flag;
            btnNext.Visible = flag;
            btnLast.Visible = flag;
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            NavigationDisplay(1);
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            NavigationDisplay(4);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            NavigationDisplay(3);
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            NavigationDisplay(2);
        }

        #endregion

        #region KeyDown Events
        private void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is GroupBox)
                    KeyDownFormat(ctrl.Controls);
            }
        }

        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left && e.Control)
            {
                if (btnPrev.Enabled) btnPrev_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Up && e.Control)
            {
                if (btnFirst.Enabled) btnFirst_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Right && e.Control)
            {
                if (btnNext.Enabled) btnNext_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Down && e.Control)
            {
                if (btnLast.Enabled) btnLast_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F2)
            {
                if (BtnSave.Visible) BtnSave_Click(sender, e);
            }
            //else if (e.KeyCode == Keys.Escape)
            //{
            //    BtnExit_Click(sender, e);
            //}
        }
        #endregion

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                ID = 0;
                ObjFunction.InitialiseControl(this.Controls);
                ObjFunction.LockButtons(false, this.Controls);
                ObjFunction.LockControls(true, this.Controls);
                ObjFunction.FillCombo(cmbCountry, "Select CountryNo,CountryName From MCountry Where IsActive = 'True' ORDER BY CountryName");
                ObjFunction.FillCombo(cmbControlGroup, "SELECT LocationNo, LocationName FROM MLocation WHERE (IsActive='True') ORDER BY LocationName");
                ObjFunction.FillCombo(cmbRegion, "SELECT RegionNo, RegionName FROM MRegion Where IsActive = 'True' ORDER BY RegionName");
                txtLocationName.Focus();
                LocationNm = "";
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            ObjFunction.LockButtons(false, this.Controls);
            ObjFunction.LockControls(true, this.Controls);
            txtLocationName.Focus();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            NavigationDisplay(5);

            ObjFunction.LockButtons(true, this.Controls);
            ObjFunction.LockControls(false, this.Controls);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            Form NewF = new Locations();
            this.Close();
            ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (chkActive.Checked == false) return;
                dbLocation = new DBMLocation();
                mLocation = new MLocation();

                mLocation.LocationNo = ID;
                if (OMMessageBox.Show("Are you sure want to delete this record?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (dbLocation.DeleteMLocation(mLocation) == true)
                    {
                        OMMessageBox.Show("Location Deleted Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        FillControls();
                        //Form NewF = new Godown();
                        //this.Close();
                        //ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                    }
                    else
                    {
                        OMMessageBox.Show("Location not Deleted", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void chkActive_CheckedChanged(object sender, EventArgs e)
        {
            if (chkActive.Checked == true)
                chkActive.Text = "Yes";
            else
                chkActive.Text = "No";
        }

        private void cmbCountry_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == 13)
            {
                try
                {
                    ObjFunction.FillCombo(cmbState, "Select StateNo,StateName From MState Where CountryNo = " + ObjFunction.GetComboValue(cmbCountry) + " And IsActive = 'True' ORDER BY StateName");
                    cmbState.Focus();
                }
                catch (Exception exc)
                {
                    ObjFunction.ExceptionDisplay(exc.Message);
                }
            }
        }

        private void cmbState_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == 13)
            {
                try
                {
                    ObjFunction.FillCombo(cmbCity, "Select CityNo, CityName From MCity Where StateNo = " + ObjFunction.GetComboValue(cmbState) + " And IsActive = 'True' ORDER BY CityName");
                    cmbCity.Focus();
                }
                catch (Exception exc)
                {
                    ObjFunction.ExceptionDisplay(exc.Message);
                }
            }
        }

        private void LocationsAE_Activated(object sender, EventArgs e)
        {
            try
            {
                if (isDoProcess)
                {
                    if (cmbRegion.Enabled == true)
                    {
                        long tID = ObjFunction.GetComboValue(cmbRegion);
                        ObjFunction.FillCombo(cmbRegion, "SELECT RegionNo, RegionName FROM MRegion Where (IsActive = 'True' OR RegionNo=" + mLocation.RegionNo + ")  ORDER BY RegionName");
                        cmbRegion.SelectedValue = tID;
                    }
                    if (cmbControlGroup.Enabled == true)
                    {
                        long tID = ObjFunction.GetComboValue(cmbControlGroup);
                        ObjFunction.FillCombo(cmbControlGroup, "SELECT LocationNo, LocationName FROM MLocation WHERE (IsActive='True' OR LocationNo=" + mLocation.ControlGroup + ") ORDER BY LocationName");
                        cmbControlGroup.SelectedValue = tID;
                    }
                    if (cmbCountry.Enabled == true)
                    {
                        long tID = ObjFunction.GetComboValue(cmbCountry);
                        ObjFunction.FillCombo(cmbCountry, "Select CountryNo,CountryName From MCountry Where (IsActive = 'True' OR CountryNo=" + mLocation.CountryNo + ") ORDER BY CountryName");
                        cmbCountry.SelectedValue = tID;
                    }
                    if (cmbState.Enabled == true)
                    {
                        long tID = ObjFunction.GetComboValue(cmbState);
                        ObjFunction.FillCombo(cmbState, "Select StateNo,StateName From MState Where CountryNo = " + ObjFunction.GetComboValue(cmbCountry) + " And (IsActive = 'True' OR StateNo=" + mLocation.StateNo + ") ORDER BY StateName");
                        cmbState.SelectedValue = tID;
                    }
                    if (cmbCity.Enabled == true)
                    {
                        long tID = ObjFunction.GetComboValue(cmbCity);

                        ObjFunction.FillCombo(cmbCity, "Select CityNo, CityName From MCity Where StateNo = " + ObjFunction.GetComboValue(cmbState) + " And (IsActive = 'True' OR CityNo =" + mLocation.CityNo + ") ORDER BY CityName");
                        cmbCity.SelectedValue = tID;
                    }
                    isDoProcess = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void LocationsAE_Deactivate(object sender, EventArgs e)
        {
            isDoProcess = true;
        }      
   }
}
