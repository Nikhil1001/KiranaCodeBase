﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Master
{
    /// <summary>
    /// This class is used for State AE
    /// </summary>
    public partial class StateAE : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        DBMState dbState = new DBMState();
        MState mState = new MState();
        string StateNm;
        DataTable dtSearch = new DataTable();
        int cntRow;
        long ID;
        
        bool isDoProcess = false;

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public StateAE()
        {
            InitializeComponent();
        }
        private void StateAE_Load(object sender, EventArgs e)
        {
            try
            {
                linkLabel1.Visible = false;
                ObjFunction.FillCombo(cmbCountry, "Select CountryNo,CountryName From MCountry Where IsActive ='True' Order By CountryName");
                ObjFunction.FillCombo(cmbRegion, "select RegionNo,RegionName from MRegion Where IsActive ='True' Order By RegionName");
                ObjFunction.LockButtons(true, this.Controls);
                ObjFunction.LockControls(false, this.Controls);
                StateNm = "";
                dtSearch = ObjFunction.GetDataView("Select StateNo From MState order by StateName").Table;
                //if (State.RequestStateNo != 0)
                //{
                //    StateNm = "";
                //    FillControls();
                //    dtSearch = ObjFunction.GetDataView("Select StateNo From MState").Table;
                //    SetNavigation();
                //    setDisplay(true);
                //}
                //else
                //{
                //    setDisplay(false);
                //}
                //KeyDownFormat(this.Controls);
                if (dtSearch.Rows.Count > 0)
                {
                    if (State.RequestStateNo == 0)
                        ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    else
                        ID = State.RequestStateNo;
                    FillControls();
                    SetNavigation();
                }
                setDisplay(true);
                btnNew.Focus();
                KeyDownFormat(this.Controls);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillControls()
        {
            try
            {
                EP.SetError(txtStateName, "");
                EP.SetError(txtshortName, "");
                EP.SetError(cmbCountry, "");
                EP.SetError(cmbRegion, "");


                mState = dbState.ModifyMStateByID(ID);
                StateNm = mState.StateName.ToUpper();
                txtStateName.Text = mState.StateName;
                txtshortName.Text = mState.StateShortCode;
                ObjFunction.FillCombo(cmbRegion, "select RegionNo,RegionName from MRegion Where (IsActive ='True' OR RegionNo= " + mState.RegionNo + ")  Order By RegionName");
                cmbRegion.SelectedValue = mState.RegionNo.ToString();
                chkActive.Checked = mState.IsActive;
                if (chkActive.Checked == true)
                    chkActive.Text = "Yes";
                else
                    chkActive.Text = "No";
                ObjFunction.FillCombo(cmbCountry, "Select CountryNo,CountryName From MCountry Where (IsActive ='True' OR CountryNo=" + mState.CountryNo + ") Order By CountryName");
                cmbCountry.SelectedValue = mState.CountryNo.ToString();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void SetValue()
        {
            try
            {
                if (Validations() == true)
                {
                    dbState = new DBMState();
                    mState = new MState();
                    mState.StateNo = ID;

                    mState.StateName = txtStateName.Text.Trim();
                    mState.StateShortCode = txtshortName.Text.Trim();
                    mState.IsActive = chkActive.Checked;
                    mState.CountryNo = ObjFunction.GetComboValue(cmbCountry);
                    mState.RegionNo = ObjFunction.GetComboValue(cmbRegion);
                    mState.UserID = DBGetVal.UserID;
                    mState.UserDate = DBGetVal.ServerTime.Date;
                    mState.CompanyNo = DBGetVal.CompanyNo;

                    if (dbState.AddMState(mState) == true)
                    {
                        if (ID == 0)
                        {
                            OMMessageBox.Show("State Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            dtSearch = ObjFunction.GetDataView("Select StateNo From MState order by StateName").Table;
                            ID = ObjQry.ReturnLong("Select Max(StateNo) From MState", CommonFunctions.ConStr);
                            SetNavigation();
                            FillControls();
                        }
                        else
                        {
                            OMMessageBox.Show("State Updated Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            FillControls();
                        }

                        ObjFunction.LockButtons(true, this.Controls);
                        ObjFunction.LockControls(false, this.Controls);
                    }
                    else
                    {
                        OMMessageBox.Show("State not saved", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            State.RequestStateNo = 0;
            this.Close();

        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            SetValue();
        }

        private bool Validations()
        {
            bool flag = false;
            EP.SetError(txtStateName, ""); 
            EP.SetError(txtshortName, "");
            EP.SetError(cmbCountry, "");
            EP.SetError(cmbRegion, "");
            if (txtStateName.Text.Trim() == "")
            {

                EP.SetError(txtStateName, "Enter State Name");
                EP.SetIconAlignment(txtStateName, ErrorIconAlignment.MiddleRight);
                txtStateName.Focus();
            }
            else if (txtshortName.Text.Trim() == "")
            {

                EP.SetError(txtshortName, "Enter Short Name");
                EP.SetIconAlignment(txtshortName, ErrorIconAlignment.MiddleRight);
                txtshortName.Focus();
            }
            else if (ObjFunction.GetComboValue(cmbCountry) <= 0)
            {
                EP.SetError(cmbCountry, "Select Country Name");
                EP.SetIconAlignment(cmbCountry, ErrorIconAlignment.MiddleRight);
                cmbCountry.Focus();
            }
            else if (ObjFunction.GetComboValue(cmbRegion) <= 0)
            {
                EP.SetError(cmbRegion, "Select Region");
                EP.SetIconAlignment(cmbRegion, ErrorIconAlignment.MiddleRight);
                cmbRegion.Focus();
            }
            else if (StateNm != txtStateName.Text.Trim().ToUpper())
            {
                if (ObjQry.ReturnInteger("Select Count(*) from MState where StateName = '" + txtStateName.Text.Trim().ToUpper().Replace("'", "''") + "'", CommonFunctions.ConStr) != 0)
                {
                    EP.SetError(txtStateName, "Duplicate State Name");
                    EP.SetIconAlignment(txtStateName, ErrorIconAlignment.MiddleRight);
                    txtStateName.Focus();
                }
                else
                    flag = true;
            }
            else
                flag = true;
            return flag;
        }

        private void StateAE_FormClosing(object sender, FormClosingEventArgs e)
        {
            State.RequestStateNo = 0;
            StateNm = "";
        }

        private void txtStateName_Leave(object sender, EventArgs e)
        {
            try
            {
                EP.SetError(txtStateName, "");
                if (txtStateName.Text.Trim() != "")
                {
                    if (StateNm != txtStateName.Text.Trim().ToUpper())
                    {
                        if (ObjQry.ReturnInteger("Select Count(*) from MState where StateName = '" + txtStateName.Text.Trim().ToUpper().Replace("'", "''") + "'", CommonFunctions.ConStr) != 0)
                        {
                            EP.SetError(txtStateName, "Duplicate State Name");
                            EP.SetIconAlignment(txtStateName, ErrorIconAlignment.MiddleRight);
                            txtStateName.Focus();
                        }
                    }

                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #region Navigation Methods
        private void NavigationDisplay(int type)
        {
            try
            {
                long No = 0;
                if (type == 5)
                {
                    No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                    ID = No;
                }
                if (type == 1)
                {
                    No = Convert.ToInt64(dtSearch.Rows[0].ItemArray[0].ToString());
                    cntRow = 0;
                    ID = No;
                }
                else if (type == 2)
                {
                    No = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    cntRow = dtSearch.Rows.Count - 1;
                    ID = No;
                }
                else
                {
                    if (type == 3)
                    {
                        cntRow = cntRow + 1;
                    }
                    else if (type == 4)
                    {
                        cntRow = cntRow - 1;
                    }

                    if (cntRow < 0)
                    {
                        OMMessageBox.Show("This is First Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow + 1;
                    }
                    else if (cntRow > dtSearch.Rows.Count - 1)
                    {
                        OMMessageBox.Show("This is Last Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow - 1;
                    }
                    else
                    {
                        No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                        ID = No;
                    }

                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

            FillControls();

        }

        private void SetNavigation()
        {
            cntRow = 0;
            for (int i = 0; i < dtSearch.Rows.Count; i++)
            {
                if (Convert.ToInt64(dtSearch.Rows[i].ItemArray[0].ToString()) == ID)
                {
                    cntRow = i;
                    break;
                }
            }
        }

        private void setDisplay(bool flag)
        {
            btnFirst.Visible = flag;
            btnPrev.Visible = flag;
            btnNext.Visible = flag;
            btnLast.Visible = flag;
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            NavigationDisplay(1);
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            NavigationDisplay(4);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            NavigationDisplay(3);
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            NavigationDisplay(2);
        }

        #endregion

        #region KeyDown Events
        private void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is GroupBox)
                    KeyDownFormat(ctrl.Controls);
            }
        }

        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left && e.Control)
            {
                if (btnPrev.Enabled) btnPrev_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Up && e.Control)
            {
                if (btnFirst.Enabled) btnFirst_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Right && e.Control)
            {
                if (btnNext.Enabled) btnNext_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Down && e.Control)
            {
                if (btnLast.Enabled) btnLast_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F2)
            {
                if (BtnSave.Visible) BtnSave_Click(sender, e);
            }
            //else if (e.KeyCode == Keys.Escape)
            //{
            //    BtnExit_Click(sender, e);
            //}
        }
        #endregion

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                ID = 0;
                StateNm = "";
                mState = new MState();
                ObjFunction.InitialiseControl(this.Controls);
                ObjFunction.LockButtons(false, this.Controls);
                ObjFunction.LockControls(true, this.Controls);
                ObjFunction.FillCombo(cmbCountry, "Select CountryNo,CountryName From MCountry Where IsActive ='True' Order By CountryName");
                ObjFunction.FillCombo(cmbRegion, "select RegionNo,RegionName from MRegion Where IsActive ='True' Order By RegionName");
                txtStateName.Focus();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            NavigationDisplay(5);

            ObjFunction.LockButtons(true, this.Controls);
            ObjFunction.LockControls(false, this.Controls);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            Form NewF = new State();
            this.Close();
            ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (chkActive.Checked == false) return;
                dbState = new DBMState();
                mState = new MState();

                mState.StateNo = ID;
                if (OMMessageBox.Show("Are you sure want to delete this record?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (dbState.DeleteMState(mState) == true)
                    {
                        OMMessageBox.Show("State Deleted Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);

                        FillControls();
                    }
                    else
                    {
                        OMMessageBox.Show("State not Deleted", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            ObjFunction.LockButtons(false, this.Controls);
            ObjFunction.LockControls(true, this.Controls);
            txtStateName.Focus();
        }

        private void chkActive_CheckedChanged(object sender, EventArgs e)
        {
            if (chkActive.Checked == true)
                chkActive.Text = "Yes";
            else
                chkActive.Text = "No";
        }

        private void StateAE_Activated(object sender, EventArgs e)
        {
            try
            {
                if (isDoProcess)
                {
                    if (cmbCountry.Enabled == true)
                    {
                        long tID = ObjFunction.GetComboValue(cmbCountry);
                        ObjFunction.FillCombo(cmbCountry, "Select CountryNo,CountryName From MCountry Where (IsActive ='True' Or CountryNo = " + mState.CountryNo + ") Order By CountryName");
                        cmbCountry.SelectedValue = tID;
                    }

                    if (cmbRegion.Enabled == true)
                    {
                        long tID = ObjFunction.GetComboValue(cmbRegion);
                        ObjFunction.FillCombo(cmbRegion, "select RegionNo,RegionName from MRegion Where (IsActive ='True' or RegionNo = " + mState.RegionNo + ") Order By RegionName");
                        cmbRegion.SelectedValue = tID;
                    }
                    isDoProcess = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void StateAE_Deactivate(object sender, EventArgs e)
        {
            isDoProcess = true;
        }

        
       

    }
}
