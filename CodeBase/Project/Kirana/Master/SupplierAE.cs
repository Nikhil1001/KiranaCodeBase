﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Master
{
    /// <summary>
    /// This class is used for Supplier AE
    /// </summary>
    public partial class SupplierAE : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        DBMLedger dbLedger = new DBMLedger();
        MLedger mLedger = new MLedger();
        MLedgerDetails mLedgerDetails = new MLedgerDetails();
        MLedgerDistDetails mLedgerDistDetails = new MLedgerDistDetails();
        string LedgerNm;
        DataTable dtSearch = new DataTable();
        int cntRow;
        long LedgerUserNo, ID;
        /// <summary>
        /// This field is used for Short ID
        /// </summary>
        public long ShortID = 0;
        bool isDoProcess = false;
        string mobNo,mobNo2;

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public SupplierAE()
        {
            InitializeComponent();
        }

        /// <summary>
        /// This is class of Parameterised Constructor
        /// </summary>
        public SupplierAE(long shortid)
        {
            InitializeComponent();
            ShortID = shortid;
        }

        private void SupplierAE_Load(object sender, EventArgs e)
        {
            try
            {
                ObjFunction.FillCombo(cmbOCity, "Select CityNo,CityName From MCity  Where CityNo=0 AND IsActive = 'True' order by CityName");
                ObjFunction.FillCombo(cmbOState, "Select StateNo,StateName From MState Where IsActive ='True' and Stateno in (select StateNo From MCity Where IsActive='true') order by StateName");
                //ObjFunction.FillCombo(cmbSign, "Select SignCode,SignName from MSign order by SignName");
                ObjFunction.FillCombo(cmbSign, "Select SignCode, CASE WHEN SignCode = 2 THEN 'To Pay' WHEN SignCode = 1 THEN 'To Receive' END AS 'Test' from MSign order by SignName");

                DisplaySupplierCount();
                if (ShortID == 0)
                {
                    ObjFunction.LockButtons(true, this.Controls);
                    ObjFunction.LockControls(false, this.Controls);
                    LedgerNm = "";
                    mobNo = "";
                    mobNo2 = "";
                    dtSearch = ObjFunction.GetDataView("Select LedgerNo from MLedger Where GroupNo=" + GroupType.SundryCreditors + " order by LedgerName").Table;
                    if (dtSearch.Rows.Count > 0)
                    {
                        if (Supplier.RequestSupplierNo == 0)
                            ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                        else
                            ID = Supplier.RequestSupplierNo;
                        FillControls();
                        SetNavigation();
                    }
                    setDisplay(true);
                    btnNew.Focus();
                }
                else
                {
                    btnNew_Click(sender, new EventArgs());
                    setDisable(false);
                }
                KeyDownFormat(this.Controls);
                pnlDistAgent.Visible = false;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void DisplaySupplierCount()
        {
            lblTotalCount.Text = ObjQry.ReturnLong("SELECT  COUNT(*) FROM  MLedger WHERE  (GroupNo = " + GroupType.SundryCreditors + ")", CommonFunctions.ConStr).ToString();
            lblActiveCount.Text = ObjQry.ReturnLong("SELECT  COUNT(*) FROM  MLedger WHERE  (GroupNo = " + GroupType.SundryCreditors + ") AND (IsActive = 'true')", CommonFunctions.ConStr).ToString();
            lblDeActiveCount.Text = ObjQry.ReturnLong("SELECT  COUNT(*) FROM  MLedger WHERE  (GroupNo = " + GroupType.SundryCreditors + ") AND (IsActive = 'false')", CommonFunctions.ConStr).ToString();
            label9.Font = ObjFunction.GetFont(FontStyle.Regular, 7);
            lblTotalCount.Font = ObjFunction.GetFont(FontStyle.Regular, 7);
            label10.Font = ObjFunction.GetFont(FontStyle.Regular, 7);
            lblActiveCount.Font = ObjFunction.GetFont(FontStyle.Regular, 7);
            label11.Font = ObjFunction.GetFont(FontStyle.Regular, 7);
            lblDeActiveCount.Font = ObjFunction.GetFont(FontStyle.Regular, 7);
        }

        private void setDisable(bool flag)
        {
            BtnSave.Visible = !flag;
            btnCancel.Visible = !flag;
            btnUpdate.Visible = flag;
            btnSearch.Visible = flag;
            BtnExit.Visible = flag;
            btnFirst.Visible = flag;
            btnPrev.Visible = flag;
            btnNext.Visible = flag;
            btnLast.Visible = flag;
            btnDelete.Visible = flag;
        }

        private void FillControls()
        {
            try
            {
                EP.SetError(txtLedgerName, ""); EP.SetError(txtContactPer, "");
                EP.SetError(txtOpBalance, ""); EP.SetError(txtCreditLimit, "");
                EP.SetError(txtEmailID, ""); EP.SetError(txtMobileNo1, "");
                EP.SetError(txtMobileNo2, "");
                EP.SetError(cmbSign, "");
                EP.SetError(txtEmailID, "");
                mLedger = dbLedger.ModifyMLedgerByID(ID);
                mLedgerDetails = dbLedger.ModifyMLedgerDetailsByID(ID);
                LedgerNm = mLedger.LedgerName.ToUpper();
                txtLedgerName.Text = mLedger.LedgerName;
                LedgerUserNo = 0;
                txtContactPer.Text = mLedger.ContactPerson;

                //ObjFunction.FillCombo(cmbSign, "Select SignCode,SignName from MSign  order by SignName");
                ObjFunction.FillCombo(cmbSign, "Select SignCode, CASE WHEN SignCode = 2 THEN 'To Pay' WHEN SignCode = 1 THEN 'To Receive' END AS 'Test' from MSign order by SignName");
                if (txtMobileNo1.Text.Trim() == "")
                {
                    txtMobileNo1.Text = "1111111111";
                    //EP.SetError(txtMobileNo1, "Enter Mobile Number");
                    //EP.SetIconAlignment(txtMobileNo1, ErrorIconAlignment.MiddleRight);
                    //txtMobileNo1.Focus();
                }
                //if (mLedger.SignCode == 1)//Debit
                //{
                //    mLedger.OpeningBalance = Convert.ToDouble(mLedger.OpeningBalance.ToString()) * -1;
                //    txtOpBalance.Text = mLedger.OpeningBalance.ToString();
                //    lblCrDr.Text = "To Receive";
                //}
                //else if (mLedger.SignCode == 2)//Credit
                //{
                //    txtOpBalance.Text = mLedger.OpeningBalance.ToString();
                //    lblCrDr.Text = "To Pay";
                //}
                cmbSign.SelectedValue = mLedger.SignCode.ToString();
                txtOpBalance.Text = mLedger.OpeningBalance.ToString();
                chkActive.Checked = mLedger.IsActive;
                if (chkActive.Checked == true)
                    chkActive.Text = "Yes";
                else
                    chkActive.Text = "No";

                //Ledger Details
                txtOAddress.Text = mLedgerDetails.Address;
                ObjFunction.FillCombo(cmbOState, "Select StateNo,StateName From MState Where (IsActive ='True' OR StateNo=" + mLedgerDetails.StateNo + ")  order by StateName");
                cmbOState.SelectedValue = mLedgerDetails.StateNo.ToString();

                //ObjFunction.FillCombo(cmbOCity, "Select CityNo,CityName From MCity where StateNo = " + ObjFunction.GetComboValue(cmbOState) + " order by CityName");
                ObjFunction.FillCombo(cmbOCity, "Select CityNo,CityName From MCity where StateNo = " + ObjFunction.GetComboValue(cmbOState) + " AND (IsActive='True' OR CityNo=" + mLedgerDetails.CityNo + ")  order by CityName");
                cmbOCity.SelectedValue = mLedgerDetails.CityNo.ToString();
                txtOPinCode.Text = mLedgerDetails.PinCode;
                txtOPhone1.Text = mLedgerDetails.PhNo1;
                txtOPhone2.Text = mLedgerDetails.PhNo2;
                txtMobileNo1.Text = mLedgerDetails.MobileNo1;
                txtMobileNo2.Text = mLedgerDetails.MobileNo2;
                mobNo = mLedgerDetails.MobileNo1;
                mobNo2 = mLedgerDetails.MobileNo2;
                txtEmailID.Text = mLedgerDetails.EmailID;
                txtCreditLimit.Text = mLedgerDetails.CreditLimit.ToString("0.00");
                txtPanNo.Text = mLedgerDetails.PANNo;
                txtVatNo.Text = mLedgerDetails.VATNo;
                txtGSTIN.Text = mLedgerDetails.CSTNo;

                if (ObjQry.ReturnLong("Select Count(*) from TVoucherRefDetails where LedgerNo=" + mLedger.LedgerNo + " and TypeOfRef=5 ", CommonFunctions.ConStr) == 0)
                {
                    txtOpBalance.ReadOnly = false;
                    cmbSign.Enabled = false;
                }
                else
                {
                    txtOpBalance.ReadOnly = true;
                    cmbSign.Enabled = true;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void setValue()
        {
            try
            {
                if (Validations() == true)
                {
                    bool flag = false;

                    dbLedger = new DBMLedger();
                    mLedger = new MLedger();
                    mLedger.LedgerNo = ID;
                    mLedger.LedgerUserNo = LedgerUserNo.ToString();
                    mLedger.LedgerName = txtLedgerName.Text.Trim().ToUpper();
                    mLedger.InvFlag = true;// chkActiveInv.Checked;
                    mLedger.MaintainBillByBill = false;// chkActiveBill.Checked;
                    mLedger.IsActive = chkActive.Checked;
                    mLedger.ContactPerson = txtContactPer.Text.Trim();
                    mLedger.GroupNo = GroupType.SundryCreditors;
                    mLedger.OpeningBalance = Math.Abs(Convert.ToDouble(txtOpBalance.Text.Trim()));
                    mLedger.SignCode = ObjFunction.GetComboValue(cmbSign);
                    //if (Convert.ToDouble(txtOpBalance.Text.Trim()) >= 0)
                    //{
                    //    mLedger.SignCode = 2;
                    //}
                    //else
                    //{
                    //    mLedger.SignCode = 1;
                    //}

                    mLedger.CompanyNo = DBGetVal.CompanyNo;
                    mLedger.LedgerStatus = (ID == 0) ? 1 : 2;
                    mLedger.IsEnroll = false;
                    mLedger.IsSendSMS = false;
                    mLedger.UserID = DBGetVal.UserID;
                    mLedger.UserDate = DBGetVal.ServerTime.Date;
                    flag = dbLedger.AddMLedger(mLedger);

                    if (flag == true)
                    {
                        mLedgerDetails = new MLedgerDetails();

                        mLedgerDetails.LedgerDetailsNo = ObjQry.ReturnLong("Select LedgerDetailsNo From MLedgerDetails Where LedgerNo=" + mLedger.LedgerNo + "", CommonFunctions.ConStr);
                        mLedgerDetails.LedgerNo = ID;
                        mLedgerDetails.Address = txtOAddress.Text.Trim();
                        mLedgerDetails.StateNo = ObjFunction.GetComboValue(cmbOState);
                        mLedgerDetails.CityNo = ObjFunction.GetComboValue(cmbOCity);
                        mLedgerDetails.PinCode = txtOPinCode.Text.Trim();
                        mLedgerDetails.PhNo1 = txtOPhone1.Text.Trim();
                        mLedgerDetails.PhNo2 = txtOPhone2.Text.Trim();
                        mLedgerDetails.MobileNo1 = txtMobileNo1.Text.Trim();
                        mLedgerDetails.MobileNo2 = txtMobileNo2.Text.Trim();
                        mLedgerDetails.EmailID = txtEmailID.Text.Trim();
                        mLedgerDetails.DOB = Convert.ToDateTime("01/01/1900");
                        mLedgerDetails.QualificationNo = 0;
                        mLedgerDetails.OccupationNo = 0;
                        mLedgerDetails.CustomerType = 0;
                        mLedgerDetails.CreditLimit = (txtCreditLimit.Text.Trim() == "") ? 0 : Convert.ToDouble(txtCreditLimit.Text.Trim());
                        mLedgerDetails.PANNo = txtPanNo.Text.Trim();
                        mLedgerDetails.VATNo = txtVatNo.Text.Trim();
                        mLedgerDetails.CSTNo = txtGSTIN.Text.Trim();

                        mLedgerDetails.UserID = DBGetVal.UserID;
                        mLedgerDetails.UserDate = DBGetVal.ServerTime.Date;

                        dbLedger.AddMLedgerDetails(mLedgerDetails);
                    }

                    if (dbLedger.ExecuteNonQueryStatements() == true)
                    {
                        if (ID == 0)
                        {
                            OMMessageBox.Show("Supplier Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            dtSearch = ObjFunction.GetDataView("Select LedgerNo from MLedger Where GroupNo=" + GroupType.SundryCreditors + " order by LedgerName ").Table;
                            ID = ObjQry.ReturnLong("Select Max(LedgerNo) FRom MLedger", CommonFunctions.ConStr);
                            if (ShortID == 0)
                            {
                                SetNavigation();
                                FillControls();
                            }
                            else
                            {
                                ShortID = ID;
                                this.Close();
                            }
                        }
                        else
                        {
                            OMMessageBox.Show("Supplier Updated Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            FillControls();
                        }

                        ObjFunction.LockButtons(true, this.Controls);
                        ObjFunction.LockControls(false, this.Controls);
                    }

                    else
                        OMMessageBox.Show("Supplier Not Added ", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                    DisplaySupplierCount();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private bool Validations()
        {
            bool flag = false;
            EP.SetError(txtLedgerName, ""); EP.SetError(txtContactPer, "");
            EP.SetError(txtOpBalance, ""); EP.SetError(txtCreditLimit, "");
            EP.SetError(txtEmailID, ""); EP.SetError(txtMobileNo1, "");
            EP.SetError(txtMobileNo2, "");
            EP.SetError(txtEmailID, "");
            if (txtMobileNo1.Text.Trim() == "")
            {
                txtMobileNo1.Text = "1111111111";
                //EP.SetError(txtMobileNo1, "Enter Mobile Number");
                //EP.SetIconAlignment(txtMobileNo1, ErrorIconAlignment.MiddleRight);
                //txtMobileNo1.Focus();
            }
            if (txtMobileNo2.Text.Trim() == "")
            {
                txtMobileNo2.Text = "1111111111";
                //EP.SetError(txtMobileNo1, "Enter Mobile Number");
                //EP.SetIconAlignment(txtMobileNo1, ErrorIconAlignment.MiddleRight);
                //txtMobileNo1.Focus();
            }
            if (txtLedgerName.Text.Trim() == "")
            {
                EP.SetError(txtLedgerName, "Enter Supplier Name");
                EP.SetIconAlignment(txtLedgerName, ErrorIconAlignment.MiddleRight);
                txtLedgerName.Focus();
            }

            else if (txtContactPer.Text.Trim() == "")
            {
                EP.SetError(txtContactPer, "Enter Name");
                EP.SetIconAlignment(txtContactPer, ErrorIconAlignment.MiddleRight);
                txtContactPer.Focus();
            }

            else if (txtOpBalance.Text.Trim() == "")
            {
                EP.SetError(txtOpBalance, "Enter Opening Balance");
                EP.SetIconAlignment(txtOpBalance, ErrorIconAlignment.MiddleRight);
                txtOpBalance.Focus();
            }
            else if (ObjFunction.CheckValidAmount(txtOpBalance.Text.Trim()) == false)
            {
                EP.SetError(txtOpBalance, "Enter Valid Balance Amount");
                EP.SetIconAlignment(txtOpBalance, ErrorIconAlignment.MiddleRight);
                txtOpBalance.Focus();
            }
            else if (ObjFunction.GetComboValue(cmbSign) <= 0)
            {
                EP.SetError(cmbSign, "Select Type");
                EP.SetIconAlignment(cmbSign, ErrorIconAlignment.MiddleRight);
                cmbSign.Focus();
            }
            else if (ObjFunction.CheckValidAmount(txtCreditLimit.Text.Trim()) == false)
            {
                EP.SetError(txtCreditLimit, "Enter Valid Credit Amount");
                EP.SetIconAlignment(txtCreditLimit, ErrorIconAlignment.MiddleRight);
                txtCreditLimit.Focus();
            }
            else if (LedgerNm != txtLedgerName.Text.Trim().ToUpper())
            {
                if (ObjQry.ReturnInteger("Select Count(*) from MLedger where LedgerName = '" + txtLedgerName.Text.Trim().Replace("'", "''") + "' And GroupNo=" + GroupType.SundryCreditors + "", CommonFunctions.ConStr) != 0)
                {
                    EP.SetError(txtLedgerName, "Duplicate Ledger Name");
                    EP.SetIconAlignment(txtLedgerName, ErrorIconAlignment.MiddleRight);
                    txtLedgerName.Focus();
                }
                else
                    flag = true;
            }
            else if (txtEmailID.Text.Trim() != "")
            {
                if (ObjFunction.CheckValidEmailID(txtEmailID.Text.Trim()) == false)
                {
                    EP.SetError(txtEmailID, "Enter Valid Email ID");
                    EP.SetIconAlignment(txtEmailID, ErrorIconAlignment.MiddleRight);
                    txtEmailID.Focus();
                    flag = false;
                }
                else
                    flag = true;
            }
            else
                flag = true;
            if (flag == true)
            {
                if (mobNo != txtMobileNo1.Text.Trim())
                {
                    if (txtMobileNo1.Text.Trim() != "1111111111")
                    {
                        //if (ObjQry.ReturnInteger("Select Count(*) from MLedgerDetails where MobileNo1='" + txtMobileNo1.Text.Trim() + "'", CommonFunctions.ConStr) != 0)
                        //{
                        //    EP.SetError(txtMobileNo1, "Duplicate Mobile No");
                        //    EP.SetIconAlignment(txtMobileNo1, ErrorIconAlignment.MiddleRight);
                        //    txtMobileNo1.Focus();
                        //    flag = false;
                        //}
                        //else
                            flag = true;
                    }
                }
                if (flag == true)
                {
                    if (txtMobileNo1.Text.Trim().Length < 10)
                    {
                        EP.SetError(txtMobileNo1, "Enter Valid Mobile No");
                        EP.SetIconAlignment(txtMobileNo1, ErrorIconAlignment.MiddleRight);
                        txtMobileNo1.Focus();
                        flag = false;
                    }
                    else
                        flag = true;
                }
            }
            if (flag == true)
            {
                if (txtMobileNo2.Visible == true)
                {
                    if (mobNo2 != txtMobileNo2.Text.Trim())
                    {
                        //if (ObjQry.ReturnInteger("Select Count(*) from MLedgerDetails where MobileNo2='" + txtMobileNo2.Text.Trim() + "'", CommonFunctions.ConStr) != 0)
                        //{
                        //    EP.SetError(txtMobileNo2, "Duplicate Mobile No");
                        //    EP.SetIconAlignment(txtMobileNo2, ErrorIconAlignment.MiddleRight);
                        //    txtMobileNo2.Focus();
                        //    flag = false;
                        //}
                        //else
                            flag = true;
                    }
                    if (flag == true)
                    {
                        if (txtMobileNo2.Text.Trim().Length < 10)
                        {
                            EP.SetError(txtMobileNo2, "Enter Valid Mobile No");
                            EP.SetIconAlignment(txtMobileNo2, ErrorIconAlignment.MiddleRight);
                            txtMobileNo1.Focus();
                            flag = false;
                        }
                        else
                            flag = true;
                    }
                }
                else
                    flag = true;
            }
            if (flag == true)
            {
                if (txtEmailID.Text.Trim() != "")
                {
                    if (ObjFunction.CheckValidEmailID(txtEmailID.Text.Trim()) == false)
                    {
                        EP.SetError(txtEmailID, "Enter Valid Email ID");
                        EP.SetIconAlignment(txtEmailID, ErrorIconAlignment.MiddleRight);
                        txtEmailID.Focus();
                        flag = false;
                    }
                    else
                    {
                        flag = true;
                    }
                }
            }

            return flag;
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            setValue();
            btnDistAgent.Enabled = true;
        }

        #region Navigation Methods
        private void NavigationDisplay(int type)
        {
            try
            {
                long No = 0;
                if (type == 5)
                {
                    No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                    ID = No;
                }
                else if (type == 1)
                {
                    No = Convert.ToInt64(dtSearch.Rows[0].ItemArray[0].ToString());
                    cntRow = 0;
                    ID = No;
                }
                else if (type == 2)
                {
                    No = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    cntRow = dtSearch.Rows.Count - 1;
                    ID = No;
                }
                else
                {
                    if (type == 3)
                    {
                        cntRow = cntRow + 1;
                    }
                    else if (type == 4)
                    {
                        cntRow = cntRow - 1;
                    }

                    if (cntRow < 0)
                    {
                        OMMessageBox.Show("This is First Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow + 1;
                    }
                    else if (cntRow > dtSearch.Rows.Count - 1)
                    {
                        OMMessageBox.Show("This is Last Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow - 1;
                    }
                    else
                    {
                        No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                        ID = No;
                    }

                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

            FillControls();

        }

        private void SetNavigation()
        {
            cntRow = 0;
            for (int i = 0; i < dtSearch.Rows.Count; i++)
            {
                if (Convert.ToInt64(dtSearch.Rows[i].ItemArray[0].ToString()) == ID)
                {
                    cntRow = i;
                    break;
                }
            }
        }

        private void setDisplay(bool flag)
        {
            btnFirst.Visible = flag;
            btnPrev.Visible = flag;
            btnNext.Visible = flag;
            btnLast.Visible = flag;
            btnDelete.Visible = flag;
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            NavigationDisplay(1);
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            NavigationDisplay(4);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            NavigationDisplay(3);
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            NavigationDisplay(2);
        }

        #endregion

        #region KeyDown Events
        private void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is GroupBox)
                    KeyDownFormat(ctrl.Controls);
            }
        }

        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left && e.Control)
            {
                if (btnPrev.Enabled) btnPrev_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Up && e.Control)
            {
                if (btnFirst.Enabled) btnFirst_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Right && e.Control)
            {
                if (btnNext.Enabled) btnNext_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Down && e.Control)
            {
                if (btnLast.Enabled) btnLast_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F2)
            {
                if (BtnSave.Visible) BtnSave_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Escape)
            {
                if (BtnSave.Visible) BtnSave.Focus();
            }

        }
        #endregion

        private void BtnExit_Click(object sender, EventArgs e)
        {
            Supplier.RequestSupplierNo = 0;
            this.Close();
        }

        private void SupplierAE_FormClosing(object sender, FormClosingEventArgs e)
        {
            LedgerNm = "";
            ID = 0;
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (chkActive.Checked == false) return;
                dbLedger = new DBMLedger();
                mLedger = new MLedger();
                mLedger.LedgerNo = ID;
                if (OMMessageBox.Show("Are you sure want to delete this record?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (dbLedger.DeleteMLedger(mLedger) == true)
                    {
                        OMMessageBox.Show("Supplier Deleted Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        FillControls();
                    }
                    else
                    {
                        OMMessageBox.Show("Supplier not Deleted", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                }
                DisplaySupplierCount();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            Form NewF = new Supplier();
            this.Close();
            ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
        }

        private void txtOpBalance_Leave(object sender, EventArgs e)
        {
            try
            {
                EP.SetError(txtOpBalance, "");
                if (txtOpBalance.Text != "")
                {
                    if (ObjFunction.CheckValidAmount(txtOpBalance.Text.Trim()) == false)
                    {
                        EP.SetError(txtOpBalance, "Enter Valid Balance Amount");
                        EP.SetIconAlignment(txtOpBalance, ErrorIconAlignment.MiddleRight);
                        txtOpBalance.Focus();
                    }
                    else
                    {
                        //if (Convert.ToDouble(txtOpBalance.Text) <= 0)
                        //    lblCrDr.Text = "To Receive";
                        //else
                        //    lblCrDr.Text = "To Pay";
                    }
                }
                else
                {
                    txtOpBalance.Text = "0.00";
                    //if (Convert.ToDouble(txtOpBalance.Text) <= 0)
                    //    lblCrDr.Text = "To Receive";
                    //else
                    //    lblCrDr.Text = "To Pay";
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                btnDistAgent.Enabled = false;
                ID = 0;
                LedgerNm = "";
                mLedger = new MLedger();
                mLedgerDetails = new MLedgerDetails();
                ObjFunction.InitialiseControl(this.Controls);
                ObjFunction.LockButtons(false, this.Controls);
                ObjFunction.LockControls(true, this.Controls);
                txtOpBalance.ReadOnly = false;
                ObjFunction.FillCombo(cmbOCity, "Select CityNo,CityName From MCity  Where CityNo=0 AND IsActive = 'True' order by CityName");
                ObjFunction.FillCombo(cmbOState, "Select StateNo,StateName From MState Where IsActive ='True' and Stateno in (select StateNo From MCity Where IsActive='true') order by StateName");
                chkActive.Checked = true;
                cmbSign.SelectedIndex = 1;
                txtLedgerName.Focus();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            btnDistAgent.Enabled = true;
            if (ShortID == 0)
            {
                NavigationDisplay(5);
                ObjFunction.LockButtons(true, this.Controls);
                ObjFunction.LockControls(false, this.Controls);
            }
            else this.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            ObjFunction.LockButtons(false, this.Controls);
            ObjFunction.LockControls(true, this.Controls);

            if (ObjQry.ReturnLong("Select Count(*) from TVoucherRefDetails where LedgerNo=" + mLedger.LedgerNo + " and TypeOfRef=5 ", CommonFunctions.ConStr) == 0)
            {
                txtOpBalance.ReadOnly = false;
                cmbSign.Enabled = false;
            }
            else
            {
                txtOpBalance.ReadOnly = true;
                cmbSign.Enabled = true;
            }

            txtLedgerName.Focus();
        }

        private void chkActive_CheckedChanged(object sender, EventArgs e)
        {
            if (chkActive.Checked == true)
                chkActive.Text = "Yes";
            else
                chkActive.Text = "No";
        }

        private void txtCreditLimit_Leave(object sender, EventArgs e)
        {
            try
            {
                EP.SetError(txtCreditLimit, "");
                if (txtCreditLimit.Text != "")
                {
                    if (ObjFunction.CheckValidAmount(txtCreditLimit.Text.Trim()) == false)
                    {
                        EP.SetError(txtCreditLimit, "Enter Valid Credit Amount");
                        EP.SetIconAlignment(txtCreditLimit, ErrorIconAlignment.MiddleRight);
                        txtCreditLimit.Focus();
                    }
                }
                else
                    txtCreditLimit.Text = "0.00";
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnDistAgent_Click(object sender, EventArgs e)
        {
            try
            {
                pnlDistAgent.Visible = true;
                dgvDistAgent.Rows.Clear();
                string sqlQuery = "SELECT  MLedgerDistDetails.PkSrNo, MLedgerDistDetails.DistAgentName, MLedgerDistDetails.MobileNo, MLedgerDistDetails.Remark " +
                                "FROM MLedgerDistDetails INNER JOIN " +
                                "MLedger ON MLedgerDistDetails.LedgerNo = MLedger.LedgerNo " +
                                "WHERE (MLedger.LedgerNo = " + ID + ")";
                DataTable dt = ObjFunction.GetDataView(sqlQuery).Table;
                if (dt.Rows.Count > 0)
                {
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        dgvDistAgent.Rows.Add();
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            dgvDistAgent.Rows[j].Cells[i].Value = dt.Rows[j].ItemArray[i];
                        }
                    }
                }
                else
                    dgvDistAgent.Rows.Add();
                dgvDistAgent.Focus();
                dgvDistAgent.CurrentCell = dgvDistAgent[1, 0];
                dgvDistAgent.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                dgvDistAgent.Columns[1].Width = 180;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgvDistAgent_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                pnlDistAgent.Visible = false;
            }
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    if ((dgvDistAgent.CurrentCell.RowIndex == dgvDistAgent.Rows.Count - 1) && (dgvDistAgent.CurrentCell.ColumnIndex == 3))
                    {
                        if (dgvDistAgent.CurrentRow.Cells[1].Value != null && dgvDistAgent.CurrentRow.Cells[1].Value.ToString() != "")
                        {
                            dgvDistAgent.Rows.Add();
                            dgvDistAgent.CurrentCell = dgvDistAgent.Rows[dgvDistAgent.Rows.Count - 1].Cells[1];
                        }
                    }

                    else if (dgvDistAgent.CurrentCell.ColumnIndex == 1)
                    {
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { dgvDistAgent.CurrentCell.RowIndex, 2, dgvDistAgent });
                    }
                    else if (dgvDistAgent.CurrentCell.ColumnIndex == 2)
                    {
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { dgvDistAgent.CurrentCell.RowIndex, 3, dgvDistAgent });
                    }
                    else if (dgvDistAgent.CurrentCell.ColumnIndex == 3)
                    {
                        MovetoNext move2n = new MovetoNext(m2n);
                        if (dgvDistAgent.CurrentCell.RowIndex != dgvDistAgent.Rows.Count - 1)
                            BeginInvoke(move2n, new object[] { dgvDistAgent.CurrentCell.RowIndex + 1, 1, dgvDistAgent });
                    }
                    //else
                    //{
                    //    if(dgvDistAgent.CurrentCell.Value !=null)
                    //        dgvDistAgent.CurrentCell = dgvDistAgent.Rows[dgvDistAgent.CurrentCell.RowIndex].Cells[dgvDistAgent.CurrentCell.ColumnIndex + 1];
                    //}
                }
                catch (Exception exc)
                {
                    ObjFunction.ExceptionDisplay(exc.Message);
                }
            }
            if (e.KeyCode == Keys.F7)
                btnOk.Focus();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                bool flag = false;

                if (dgvDistAgent.Rows.Count > 0)
                {
                    if (ValidateDistAgent())
                    {
                        dbLedger = new DBMLedger();
                        for (int i = 0; i < dgvDistAgent.Rows.Count; i++)
                        {
                            if ((dgvDistAgent.Rows[i].Cells[1].Value != null && dgvDistAgent.Rows[i].Cells[1].Value.ToString() != "") || (dgvDistAgent.Rows[i].Cells[2].Value != null && dgvDistAgent.Rows[i].Cells[2].Value.ToString() != "") || (dgvDistAgent.Rows[i].Cells[3].Value != null && dgvDistAgent.Rows[i].Cells[3].Value.ToString() != ""))
                            {
                                mLedgerDistDetails = new MLedgerDistDetails();
                                mLedgerDistDetails.PkSrNo = (dgvDistAgent.Rows[i].Cells[0].Value == null) ? 0 : Convert.ToInt64(dgvDistAgent.Rows[i].Cells[0].Value.ToString());
                                mLedgerDistDetails.DistAgentName = (dgvDistAgent.Rows[i].Cells[1].Value == null) ? "" : dgvDistAgent.Rows[i].Cells[1].Value.ToString();
                                mLedgerDistDetails.MobileNo = (dgvDistAgent.Rows[i].Cells[2].Value == null) ? "" : dgvDistAgent.Rows[i].Cells[2].Value.ToString();
                                mLedgerDistDetails.LedgerNo = ID;
                                mLedgerDistDetails.Remark = (dgvDistAgent.Rows[i].Cells[3].Value == null) ? "" : dgvDistAgent.Rows[i].Cells[3].Value.ToString();
                                flag = dbLedger.AddMLedgerDistDetails(mLedgerDistDetails);
                            }
                        }
                        if (flag == true)
                        {
                            if (dbLedger.ExecuteNonQueryStatements() == true)
                            {
                                OMMessageBox.Show("Distributor/Agent Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                                pnlDistAgent.Visible = false;
                            }
                            else
                            {
                                OMMessageBox.Show("Distributor/Agent Not Added", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            }
                        }
                        else
                        {
                            OMMessageBox.Show("Properly fill Distributor Details...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private bool ValidateDistAgent()
        {
            bool flag=true;
            for (int i = 0; i < dgvDistAgent.Rows.Count; i++)
            {
                if (dgvDistAgent.Rows[i].Cells[2].ErrorText != "")
                {
                    dgvDistAgent.Focus();
                    dgvDistAgent.CurrentCell = dgvDistAgent[2, i];
                    flag = false;
                    break;
                }
            }
            return flag;
        }

        private void txtValid_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMaskedNumeric((TextBox)sender);
        }

        private void cmbOState_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    e.SuppressKeyPress = true;
                    ObjFunction.FillCombo(cmbOCity, "Select CityNo,CityName From MCity where StateNo = " + ObjFunction.GetComboValue(cmbOState) + " AND  (IsActive='True' OR CityNo=" + mLedgerDetails.CityNo + ") order by CityName");
                    cmbOCity.Focus();
                }
                catch (Exception exc)
                {
                    ObjFunction.ExceptionDisplay(exc.Message);
                }
            }
        }

        private void cmbOState_Leave(object sender, EventArgs e)
        {
            cmbOState_KeyDown(sender, new KeyEventArgs(Keys.Enter));
        }

        private void btnDistCancel_Click(object sender, EventArgs e)
        {
            pnlDistAgent.Visible = false;
        }

        private void dgvDistAgent_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (dgvDistAgent.CurrentCell.ColumnIndex == 2)
            {
                TextBox txt1 = (TextBox)e.Control;
                txt1.TextChanged += new EventHandler(txt_TextChanged);
            }
        }

        private void txt_TextChanged(object sender, EventArgs e)
        {
            if (dgvDistAgent.CurrentCell.ColumnIndex == 2)
            {
                ObjFunction.SetMasked((TextBox)sender, -1, 10);
            }
        }

        private void txtOpBalance_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(txtOpBalance, 2, 9, JitFunctions.MaskedType.NotNegative);
        }

        private void txtCreditLimit_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(txtCreditLimit, 2, 9, JitFunctions.MaskedType.NotNegative);
        }

        private void txtOPinCode_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(txtOPinCode, 0, 6);
        }

        private void SupplierAE_Activated(object sender, EventArgs e)
        {
            try
            {
                if (isDoProcess)
                {
                    if (cmbOCity.Enabled == true)
                    {
                        ObjFunction.FillCombo(cmbOCity, "Select CityNo,CityName From MCity  Where CityNo=0 AND (IsActive = 'True' OR  CityNo=" + mLedgerDetails.CityNo + ") order by CityName");
                    }
                    if (cmbOState.Enabled == true)
                    {
                        ObjFunction.FillCombo(cmbOState, "Select StateNo,StateName From MState Where (IsActive ='True' OR StateNo=" + mLedgerDetails.StateNo + ")  and Stateno in (select StateNo From MCity Where IsActive='true') order by StateName");
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void SupplierAE_Deactivate(object sender, EventArgs e)
        {
            isDoProcess = true;
        }

        private void cmbOCity_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                txtOPhone1.Focus();
            }
        }

        private void txtOpBalance_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtOAddress.GotFocus += delegate { txtOAddress.Select(txtOAddress.Text.Length, txtOAddress.Text.Length); };
                e.SuppressKeyPress = true;
                if (txtOpBalance.Text != "")
                {
                    //if (Convert.ToDouble(txtOpBalance.Text) <= 0)
                    //    lblCrDr.Text = "To Receive";
                    //else
                    //    lblCrDr.Text = "To Pay";
                    
                }
                else
                {
                    //lblCrDr.Text = "";
                }
                cmbSign.Focus();

            }
        }

        private void dgvDistAgent_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 2)
                {
                    if (dgvDistAgent.Rows[e.RowIndex].Cells[2].Value != null && dgvDistAgent.Rows[e.RowIndex].Cells[2].Value.ToString() != "")
                    {
                        string str = dgvDistAgent.Rows[e.RowIndex].Cells[2].Value.ToString();
                        if (str.Length < 10)
                        {
                            dgvDistAgent.Rows[e.RowIndex].Cells[2].ErrorText = "Enter Valid Mobile No.";
                        }
                        else
                            dgvDistAgent.Rows[e.RowIndex].Cells[2].ErrorText = "";
                    }
                }

                if (dgvDistAgent.CurrentCell.ColumnIndex == 1)
                {
                    MovetoNext move2n = new MovetoNext(m2n);
                    BeginInvoke(move2n, new object[] { e.RowIndex, e.ColumnIndex + 1, dgvDistAgent });
                }
                else if (dgvDistAgent.CurrentCell.ColumnIndex == 2)
                {
                    MovetoNext move2n = new MovetoNext(m2n);
                    BeginInvoke(move2n, new object[] { e.RowIndex, e.ColumnIndex + 1, dgvDistAgent });
                }
                else if (dgvDistAgent.CurrentCell.ColumnIndex == 3)
                {
                    MovetoNext move2n = new MovetoNext(m2n);
                    if (e.RowIndex != dgvDistAgent.Rows.Count - 1)
                        BeginInvoke(move2n, new object[] { e.RowIndex + 1, 1, dgvDistAgent });
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private delegate void MovetoNext(int RowIndex, int ColIndex, DataGridView dg);

        private void m2n(int RowIndex, int ColIndex, DataGridView dg)
        {
            dg.CurrentCell = dg.Rows[RowIndex].Cells[ColIndex];
        }

        private void txtLedgerName_Leave(object sender, EventArgs e)
        {
            try
            {
                if (LedgerNm.ToUpper() != txtLedgerName.Text.Trim().ToUpper())
                {
                    if (ObjQry.ReturnInteger("Select Count(*) from MLedger where LedgerName = '" + txtLedgerName.Text.Trim().ToUpper().Replace("'", "''") + "' And GroupNo=" + GroupType.SundryCreditors + "", CommonFunctions.ConStr) != 0)
                    {
                        EP.SetError(txtLedgerName, "Duplicate Ledger Name");
                        EP.SetIconAlignment(txtLedgerName, ErrorIconAlignment.MiddleRight);
                        txtLedgerName.Focus();

                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnMfg_Click(object sender, EventArgs e)
        {
            if (ID != 0)
            {
                Form NewF = new Master.ManufacturerDetails(ID);
                ObjFunction.OpenForm(NewF);
            }
        }
    }
}
