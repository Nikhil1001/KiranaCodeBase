﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Master
{
    /// <summary>
    /// This class is used for Stock Group AE
    /// </summary>
    public partial class StockGroupAE : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        DBMStockGroup dbStockGroup = new DBMStockGroup();
        MStockGroup mStockGroup = new MStockGroup();
        DataTable dtGroup = new DataTable();
        string StockGroupNm, MsgName;
        DataTable dtSearch = new DataTable();
        int cntRow, GrType;
        long ID;
        bool FlagBilingual;
        bool isDoProcess = false, IsLeave = false;

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public StockGroupAE()
        {
            InitializeComponent();
        }

        /// <summary>
        /// This is class of Parameterised Constructor
        /// </summary>
        public StockGroupAE(int GroupType)
        {
            InitializeComponent();

            if (GroupType == 2)
            {
                GrType = GroupType;
                this.Text = "Category Entry";
                this.Name = "Category Entry";
                this.MsgName = "Category";
            }
            else if (GroupType == 3)
            {
                GrType = GroupType;
                this.Text = "Brand Entry";
                this.Name = "Brand Enttry";
                this.MsgName = "Brand ";
            }
            else if (GroupType == 4)
            {
                GrType = GroupType;
                this.Text = "Department Entry";
                this.Name = "Department";
                this.MsgName = "Department Name";
            }
        }

        private void StockGroupAE_Load(object sender, EventArgs e)
        {
            try
            {
                txtLanguage.Font = ObjFunction.GetLangFont();
                btnLangDesc.Enabled = false;
                btnNewManufacturer.Enabled = false;
                btnNewManufacturer.Enabled = false;
                dtGroup = ObjFunction.GetDataView("Select StockGroupName from MStockGroup Where ControlGroup=" + GrType + " order by StockGroupName").Table;
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_Bilingual)) == true && GrType == 3)
                {
                    FlagBilingual = true;
                    SetFlag(true);
                }
                else
                {
                    FlagBilingual = false;
                    SetFlag(false);
                }
                ObjFunction.LockButtons(true, this.Controls);
                ObjFunction.LockControls(false, this.Controls);

                ObjFunction.FillCombo(cmbDepartment, "Select StockGroupNo,StockGroupName From MStockGroup Where ControlGroup=4 AND IsActive = 'True'");
                ObjFunction.FillCombo(cmbManufacturerCompanyName, "Select MfgCompNo,MfgCompName From MManufacturerCompany Where IsActive = 'True' order by MfgCompName");
                if (GrType == 2)
                {
                    cmbDepartment.Visible = true; lblDept.Visible = true; lblStar3.Visible = true;
                    cmbCategory.Visible = false; lblCategory.Visible = false; lblStar4.Visible = false;
                    cmbManufacturerCompanyName.Visible = false; lblMfgComp.Visible = false; lblStar5.Visible = false; btnNewManufacturer.Visible = false;
                }
                else if (GrType == 3)
                {
                    cmbDepartment.Visible = true; lblDept.Visible = true; lblStar3.Visible = true;
                    lblMargin.Visible = false; txtMargin.Visible = false; lblPercetage.Visible = false; chkApplyToAll.Visible = false;
                    cmbCategory.Visible = true; lblCategory.Visible = true; lblStar4.Visible = true;
                    cmbManufacturerCompanyName.Visible = true; lblMfgComp.Visible = true; lblStar5.Visible = true; btnNewManufacturer.Visible = true;
                    ObjFunction.FillCombo(cmbCategory, "Select StockGroupNo,StockGroupName From MStockGroup Where StockGroupNo=0 AND IsActive = 'True' ");
                }
                else if (GrType == 4)
                {
                    cmbDepartment.Visible = false; lblDept.Visible = false; lblStar3.Visible = false;
                    lblMargin.Visible = false; txtMargin.Visible = false; lblPercetage.Visible = false; chkApplyToAll.Visible = false;
                    cmbCategory.Visible = false; lblCategory.Visible = false; lblStar4.Visible = false;
                    cmbManufacturerCompanyName.Visible = false; lblMfgComp.Visible = false; lblStar5.Visible = false; btnNewManufacturer.Visible = false;
                }

                StockGroupNm = "";
                dtSearch = ObjFunction.GetDataView("Select StockGroupNo From MStockGroup Where ControlGroup=" + GrType + " ORDER BY StockGroupName").Table;
                if (dtSearch.Rows.Count > 0)
                {
                    if (StockGroup.RequestStockGroupNo == 0)
                        ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    else
                        ID = StockGroup.RequestStockGroupNo;
                    FillControls();
                    SetNavigation();
                }
                setDisplay(true);
                btnNew.Focus();
                KeyDownFormat(this.Controls);
                lstBrandName.Visible = false;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void SetFlag(bool flag)
        {
            txtLanguage.Visible = flag;
            lblStar2.Visible = flag;
            label4.Visible = flag;
            btnLangDesc.Visible = flag;
            lblMfgComp.Visible = flag;
            cmbManufacturerCompanyName.Visible = flag;
            lblStar5.Visible = flag;
            btnNewManufacturer.Visible = flag;
        }

        private void FillControls()
        {
            try
            {
                EP.SetError(txtStockGroupName, "");
                EP.SetError(cmbManufacturerCompanyName, "");
                mStockGroup = dbStockGroup.ModifyMStockGroupByID(ID);
                StockGroupNm = mStockGroup.StockGroupName;
                txtStockGroupName.Text = mStockGroup.StockGroupName;
                txtLanguage.Text = mStockGroup.LanguageName;
                chkActive.Checked = mStockGroup.IsActive;
                if (chkActive.Checked == true)
                    chkActive.Text = "Yes";
                else
                    chkActive.Text = "No";


                if (GrType == 2)
                {
                    ObjFunction.FillCombo(cmbDepartment, "Select StockGroupNo,StockGroupName From MStockGroup Where ControlGroup=4 AND (IsActive = 'True' OR  StockGroupNo=" + mStockGroup.ControlSubGroup + ")");
                    cmbDepartment.SelectedValue = mStockGroup.ControlSubGroup;
                    txtMargin.Text = mStockGroup.Margin.ToString();
                    chkApplyToAll.Checked = mStockGroup.IsApplyToAll;

                }
                else if (GrType == 3)
                {
                    ObjFunction.FillCombo(cmbDepartment, "Select StockGroupNo,StockGroupName From MStockGroup Where ControlGroup=4 AND (IsActive = 'True' OR  StockGroupNo in(Select ControlSubGroup From MStockGRoup Where StockGRoupNo=" + mStockGroup.ControlSubGroup + "))");
                    cmbDepartment.SelectedValue = ObjQry.ReturnLong("Select ControlSubGroup From MStockGRoup Where StockGRoupNo=" + mStockGroup.ControlSubGroup + "", CommonFunctions.ConStr);
                    ObjFunction.FillCombo(cmbCategory, "Select StockGRoupNo,StockGroupName From MStockGRoup Where (IsActive='True' OR StockGRoupNo=" + mStockGroup.ControlSubGroup + ")  AND ControlGroup=2 AND ControlSubGroup=" + ObjFunction.GetComboValue(cmbDepartment) + "");
                    //ObjFunction.FillCombo(cmbCategory, "Select StockGRoupNo,StockGroupName From MStockGRoup Where ControlGroup=2 AND ControlSubGroup=" + ObjFunction.GetComboValue(cmbDepartment) + "");
                    cmbCategory.SelectedValue = mStockGroup.ControlSubGroup;
                    ObjFunction.FillCombo(cmbManufacturerCompanyName, "Select MfgCompNo,MfgCompName From MManufacturerCompany Where IsActive = 'True' OR MfgCompNo=" + mStockGroup.MfgCompNo + " Order by MfgCompName");
                    cmbManufacturerCompanyName.SelectedValue = ObjQry.ReturnLong("Select MfgCompNo From MManufacturerCompany Where MfgCompNo=" + mStockGroup.MfgCompNo, CommonFunctions.ConStr);
                }
                lstBrandName.Visible = false;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void SetValue()
        {
            try
            {
                lstBrandName.Visible = false;
                if (Validations() == true)
                {
                    dbStockGroup = new DBMStockGroup();
                    mStockGroup = new MStockGroup();

                    mStockGroup.StockGroupNo = ID;
                    mStockGroup.StockGroupName = txtStockGroupName.Text.Trim().ToUpper();
                    mStockGroup.LanguageName = txtLanguage.Text.Trim();
                    mStockGroup.ControlGroup = GrType;
                    mStockGroup.IsActive = chkActive.Checked;
                    mStockGroup.UserId = DBGetVal.UserID;
                    mStockGroup.UserDate = DBGetVal.ServerTime.Date;
                    mStockGroup.CompanyNo = DBGetVal.CompanyNo;


                    if (GrType == 2)
                    {
                        mStockGroup.ControlSubGroup = ObjFunction.GetComboValue(cmbDepartment);
                        mStockGroup.MfgCompNo = 0;
                        mStockGroup.Margin = Convert.ToDouble(txtMargin.Text.Trim());
                        mStockGroup.IsApplyToAll = chkApplyToAll.Checked;
                    }
                    else if (GrType == 3)
                    {
                        mStockGroup.ControlSubGroup = ObjFunction.GetComboValue(cmbCategory);
                        mStockGroup.MfgCompNo = ObjFunction.GetComboValue(cmbManufacturerCompanyName);
                        mStockGroup.Margin = 0;
                        mStockGroup.IsApplyToAll = false;
                    }
                    else if (GrType == 4)
                    {
                        mStockGroup.ControlSubGroup = 0;
                        mStockGroup.MfgCompNo = 0;
                        mStockGroup.Margin = 0;
                        mStockGroup.IsApplyToAll = false;
                    }

                    if (dbStockGroup.AddMStockGroup(mStockGroup) == true)
                    {
                        if (ID == 0)
                        {
                            OMMessageBox.Show(MsgName + " Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            dtSearch = ObjFunction.GetDataView("Select StockGroupNo From MStockGroup Where  ControlGroup=" + GrType + " ORDER BY StockGroupName").Table;
                            dtGroup = ObjFunction.GetDataView("Select StockGroupName from MStockGroup Where ControlGroup=" + GrType + " order by StockGroupName").Table;
                            ID = ObjQry.ReturnLong("Select Max(StockGroupNo) FRom MStockGroup", CommonFunctions.ConStr);
                            SetNavigation();
                            FillControls();
                        }
                        else
                        {
                            OMMessageBox.Show(MsgName + " Updated Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            FillControls();
                        }

                        ObjFunction.LockButtons(true, this.Controls);
                        ObjFunction.LockControls(false, this.Controls);
                    }
                    else
                    {
                        OMMessageBox.Show(MsgName + " not saved", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            StockGroup.RequestStockGroupNo = 0;
            this.Close();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            SetValue();
        }

        private bool Validations()
        {
            bool flag = false;
            EP.SetError(txtStockGroupName, "");
            EP.SetError(txtMargin, "");
            EP.SetError(cmbDepartment, "");
            EP.SetError(cmbCategory, "");
            EP.SetError(cmbManufacturerCompanyName, "");
            if (txtStockGroupName.Text.Trim() == "")
            {
                EP.SetError(txtStockGroupName, "Enter " + MsgName);
                EP.SetIconAlignment(txtStockGroupName, ErrorIconAlignment.MiddleRight);
                txtStockGroupName.Focus();
            }
            else if (txtMargin.Visible == true && txtMargin.Text.Trim() == "")
            {
                EP.SetError(txtMargin, "Enter Margin");
                EP.SetIconAlignment(txtMargin, ErrorIconAlignment.MiddleRight);
                txtMargin.Focus();
            }
            else if (StockGroupNm != txtStockGroupName.Text.Trim().ToUpper())
            {
                if (ObjQry.ReturnInteger("Select Count(*) from MStockGroup where StockGroupName = '" + txtStockGroupName.Text.Trim().Replace("'", "''") + "' AND ControlGroup=" + GrType + "", CommonFunctions.ConStr) != 0)
                {
                    EP.SetError(txtStockGroupName, "Duplicate " + MsgName);
                    EP.SetIconAlignment(txtStockGroupName, ErrorIconAlignment.MiddleRight);
                    txtStockGroupName.Focus();
                }
                else
                    flag = true;
            }

            else
                flag = true;


            if (flag == true)
            {
                if (GrType == 2)
                {
                    if (ObjFunction.GetComboValue(cmbDepartment) <= 0)
                    {
                        EP.SetError(cmbDepartment, "Select Department");
                        EP.SetIconAlignment(cmbDepartment, ErrorIconAlignment.MiddleRight);
                        cmbDepartment.Focus();
                        flag = false;
                    }
                }
                else if (GrType == 3)
                {
                    if (ObjFunction.GetComboValue(cmbDepartment) <= 0)
                    {
                        EP.SetError(cmbDepartment, "Select Department");
                        EP.SetIconAlignment(cmbDepartment, ErrorIconAlignment.MiddleRight);
                        cmbDepartment.Focus();
                        flag = false;
                    }
                    else if (ObjFunction.GetComboValue(cmbCategory) <= 0)
                    {
                        EP.SetError(cmbCategory, "Select Category");
                        EP.SetIconAlignment(cmbCategory, ErrorIconAlignment.MiddleRight);
                        cmbCategory.Focus();
                        flag = false;
                    }
                    else if (ObjFunction.GetComboValue(cmbManufacturerCompanyName) <= 0)
                    {
                        EP.SetError(cmbManufacturerCompanyName, "Select Company Name");
                        EP.SetIconAlignment(cmbManufacturerCompanyName, ErrorIconAlignment.MiddleRight);
                        cmbManufacturerCompanyName.Focus();
                        flag = false;
                    }
                }
            }
            return flag;
        }

        private void StockGroupAE_FormClosing(object sender, FormClosingEventArgs e)
        {
            ID = 0;
            StockGroupNm = "";
        }

        private void txtStockGroupName_Leave(object sender, EventArgs e)
        {
            try
            {
                EP.SetError(txtStockGroupName, "");
                if (IsLeave == false)
                {
                    if (txtStockGroupName.Text.Trim() != "")
                    {
                        txtLanguage.Text = "";
                        if (StockGroupNm != txtStockGroupName.Text.Trim().ToUpper())
                        {
                            if (ObjQry.ReturnInteger("Select Count(*) from MStockGroup where StockGroupName = '" + txtStockGroupName.Text.Trim().Replace("'", "''") + "' AND ControlGroup=" + GrType + "", CommonFunctions.ConStr) != 0)
                            {
                                if (GrType != 3)
                                {
                                    EP.SetError(txtStockGroupName, "Duplicate " + MsgName);
                                    EP.SetIconAlignment(txtStockGroupName, ErrorIconAlignment.MiddleRight);
                                    txtStockGroupName.Focus();
                                }
                                else
                                {

                                    long GrNo = ObjQry.ReturnLong("Select StockGroupNo from MStockGroup where StockGroupName = '" + txtStockGroupName.Text.Trim().Replace("'", "''") + "' AND ControlGroup=" + GrType + "", CommonFunctions.ConStr);
                                    ID = GrNo;
                                    FillControls();
                                    //ObjFunction.LockButtons(false, this.Controls);
                                    //ObjFunction.LockControls(true, this.Controls);
                                    chkActive.Checked = true;
                                    BtnSave.Focus();

                                }
                            }
                            else if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_Bilingual)) == true && GrType == 3 && lstBrandName.Visible == false)
                            {
                                txtLanguage.Focus();
                                if (txtLanguage.Text.Trim().Length == 0)
                                {
                                    btnLangDesc_Click(btnLangDesc, null);
                                }
                            }
                            else
                            {
                                if (GrType == 2)
                                    txtMargin.Focus();
                                else
                                    chkActive.Focus();
                            }
                        }
                        else
                        {
                            if (FlagBilingual == true)
                            {
                                txtLanguage.Focus();
                                if (txtLanguage.Text.Trim().Length == 0)
                                {
                                    btnLangDesc_Click(btnLangDesc, null);
                                }
                            }
                            else
                            {
                                if (GrType == 2)
                                    txtMargin.Focus();
                                else
                                    chkActive.Focus();
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnLangDesc_Click(object sender, EventArgs e)
        {
            try
            {
                Utilities.KeyBoard frmkb;
                if (txtLanguage.Text.Trim().Length > 0)
                {
                    string val = ObjFunction.ChecklLangVal(txtStockGroupName.Text.Trim());
                    if (val == "")
                    {
                        frmkb = new Utilities.KeyBoard(1, txtStockGroupName.Text.Trim(), txtLanguage.Text, "", "");
                        ObjFunction.OpenForm(frmkb);
                        if (frmkb.DS == DialogResult.OK)
                        {
                            txtLanguage.Text = frmkb.strLanguage.Trim();
                            chkActive.Focus();
                            frmkb.Close();
                        }
                        else
                        {
                            frmkb.Close();
                            txtLanguage.Focus();
                        }
                    }
                    else
                        txtLanguage.Text = val;
                }
                else
                {
                    string val = ObjFunction.ChecklLangVal(txtStockGroupName.Text.Trim());
                    if (val == "")
                    {
                        frmkb = new Utilities.KeyBoard(4, txtStockGroupName.Text.Trim(), txtLanguage.Text, "", "");
                        ObjFunction.OpenForm(frmkb);
                        if (frmkb.DS == DialogResult.OK)
                        {
                            txtLanguage.Text = frmkb.strLanguage.Trim();
                            chkActive.Focus();
                            frmkb.Close();
                        }
                        else
                        {
                            frmkb.Close();
                            txtLanguage.Focus();
                        }
                    }
                    else
                        txtLanguage.Text = val;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #region Navigation Methods
        private void NavigationDisplay(int type)
        {
            try
            {
                long No = 0;
                if (type == 5)
                {
                    No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                    ID = No;
                }
                else if (type == 1)
                {
                    No = Convert.ToInt64(dtSearch.Rows[0].ItemArray[0].ToString());
                    cntRow = 0;
                    ID = No;
                }
                else if (type == 2)
                {
                    No = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    cntRow = dtSearch.Rows.Count - 1;
                    ID = No;
                }
                else
                {
                    if (type == 3)
                    {
                        cntRow = cntRow + 1;
                    }
                    else if (type == 4)
                    {
                        cntRow = cntRow - 1;
                    }

                    if (cntRow < 0)
                    {
                        OMMessageBox.Show("This is First Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow + 1;
                    }
                    else if (cntRow > dtSearch.Rows.Count - 1)
                    {
                        OMMessageBox.Show("This is Last Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow - 1;
                    }
                    else
                    {
                        No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                        ID = No;
                    }

                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
            FillControls();

        }

        private void SetNavigation()
        {
            cntRow = 0;
            for (int i = 0; i < dtSearch.Rows.Count; i++)
            {
                if (Convert.ToInt64(dtSearch.Rows[i].ItemArray[0].ToString()) == ID)
                {
                    cntRow = i;
                    break;
                }
            }
        }

        private void setDisplay(bool flag)
        {
            btnFirst.Visible = flag;
            btnPrev.Visible = flag;
            btnNext.Visible = flag;
            btnLast.Visible = flag;
            btnDelete.Visible = flag;
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            NavigationDisplay(1);
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            NavigationDisplay(4);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            NavigationDisplay(3);
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            NavigationDisplay(2);
        }

        #endregion

        #region KeyDown Events
        private void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is GroupBox)
                    KeyDownFormat(ctrl.Controls);
            }
        }

        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left && e.Control)
            {
                if (btnPrev.Enabled) btnPrev_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Up && e.Control)
            {
                if (btnFirst.Enabled) btnFirst_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Right && e.Control)
            {
                if (btnNext.Enabled) btnNext_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Down && e.Control)
            {
                if (btnLast.Enabled) btnLast_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F2)
            {
                if (BtnSave.Visible) BtnSave_Click(sender, e);
            }

        }
        #endregion

        private void chkActive_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkActive.Checked == true)
                    chkActive.Text = "Yes";
                else
                {
                    if (GrType == 3)
                    {
                        long ItmCnt = ObjQry.ReturnLong("Select Count(*) from MStockItems Where GroupNo=" + ID + " and IsActive='true'", CommonFunctions.ConStr);
                        if (ItmCnt > 0)
                        {
                            if (OMMessageBox.Show("All active SKUs in this brand would be deactivated. Do you want to proceed ? ", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question, OMMessageBoxDefaultButton.Button2) == DialogResult.Yes)
                            {
                                ObjTrans.Execute("Update MStockItems set IsActive='False' where GroupNo=" + ID + "", CommonFunctions.ConStr);

                            }
                            else
                            {
                                chkActive.Checked = true;
                                chkActive.Text = "Yes";
                                return;
                            }
                        }
                        else chkActive.Text = "No";
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (chkActive.Checked == false) return;
                dbStockGroup = new DBMStockGroup();
                mStockGroup = new MStockGroup();

                mStockGroup.StockGroupNo = ID;
                if (GrType != 3)
                {
                    if (ObjQry.ReturnLong("Select Count(*) from MStockItems Where GroupNo=" + ID + "", CommonFunctions.ConStr) > 0)
                    {
                        OMMessageBox.Show("Sorry You Can not delete this record..", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                    }
                    else
                    {
                        if (OMMessageBox.Show("Are you sure want to delete this record?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            if (dbStockGroup.DeleteMStockGroup(mStockGroup) == true)
                            {
                                OMMessageBox.Show(MsgName + " Deleted Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                                FillControls();
                            }
                            else
                            {
                                OMMessageBox.Show(MsgName + " not Deleted", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            }

                        }
                    }
                }
                else
                {
                    long ItmCnt = ObjQry.ReturnLong("Select Count(*) from MStockItems Where GroupNo=" + ID + " and IsActive='true'", CommonFunctions.ConStr);
                    if (ItmCnt > 0)
                    {
                        if (OMMessageBox.Show("All active SKUs in this brand would be deactivated. Do you want to proceed ? ", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            ObjTrans.Execute("Update MStockItems set IsActive='False' where GroupNo=" + ID + "", CommonFunctions.ConStr);
                            if (dbStockGroup.DeleteMStockGroup(mStockGroup) == true)
                            {
                                OMMessageBox.Show(MsgName + " Deleted Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                                FillControls();
                            }
                            else
                            {
                                OMMessageBox.Show(MsgName + " not Deleted", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            }
                        }
                    }
                    else if (OMMessageBox.Show("Are you sure want to delete this record?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        if (dbStockGroup.DeleteMStockGroup(mStockGroup) == true)
                        {
                            OMMessageBox.Show(MsgName + " Deleted Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            FillControls();
                        }
                        else
                        {
                            OMMessageBox.Show(MsgName + " not Deleted", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        }

                    }
                }
                lstBrandName.Visible = false;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            Form NewF = new StockGroup(GrType);
            this.Close();
            ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                ID = 0;
                StockGroupNm = "";
                mStockGroup = new MStockGroup();
                ObjFunction.InitialiseControl(this.Controls);
                ObjFunction.LockButtons(false, this.Controls);
                ObjFunction.LockControls(true, this.Controls);

                if (cmbDepartment.Visible == true)
                {
                    ObjFunction.FillCombo(cmbDepartment, "Select StockGroupNo,StockGroupName From MStockGroup " +
                        " Where IsActive='True' AND ControlGroup=4");
                }
                cmbDepartment.SelectedValue = 10;
                // ObjFunction.FillCombo(cmbCategory, "Select StockGRoupNo,StockGroupName From MStockGRoup Where IsActive='True' AND ControlGroup=2 ");
                cmbDepartment_KeyDown(cmbDepartment, new KeyEventArgs(Keys.Enter));
                cmbCategory.SelectedValue = 8;
                cmbManufacturerCompanyName.SelectedValue = 0;
                btnLangDesc.Enabled = true;
                btnNewManufacturer.Enabled = true;
                txtStockGroupName.Focus();
                chkActive.Checked = true;
                lstBrandName.Visible = false;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            NavigationDisplay(5);
            ObjFunction.LockButtons(true, this.Controls);
            ObjFunction.LockControls(false, this.Controls);
            btnLangDesc.Enabled = false;
            btnNewManufacturer.Enabled = false;
            lstBrandName.Visible = false;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            ObjFunction.LockButtons(false, this.Controls);
            ObjFunction.LockControls(true, this.Controls);
            btnLangDesc.Enabled = true;
            btnNewManufacturer.Enabled = true;
            lstBrandName.Visible = false;
            txtStockGroupName.Focus();
        }

        private void chkActive_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (GrType == 2)
                {
                    cmbDepartment.Focus();
                }
                else if (GrType == 3)
                {
                    cmbDepartment.Focus();
                }
                else if (GrType == 4)
                {
                    BtnSave.Focus();
                }
            }
        }

        private void cmbDepartment_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    e.SuppressKeyPress = true;
                    if (GrType == 2)
                    {
                        BtnSave.Focus();
                    }
                    else if (GrType == 3)
                    {
                        long preCat = ObjFunction.GetComboValue(cmbCategory);
                        ObjFunction.FillCombo(cmbCategory, "Select StockGRoupNo,StockGroupName From MStockGRoup Where IsActive='True' AND ControlGroup=2 AND ControlSubGroup=" + ObjFunction.GetComboValue(cmbDepartment) + "");
                        cmbCategory.SelectedValue = preCat;
                        cmbCategory.Focus();
                    }
                }
                catch (Exception exc)
                {
                    ObjFunction.ExceptionDisplay(exc.Message);
                }
            }
        }

        private void cmbCategory_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                cmbManufacturerCompanyName.Focus();
                //BtnSave.Focus();
            }
        }

        private void txtStockGroupName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                e.SuppressKeyPress = true;
                if (lstBrandName.Visible)
                {
                    IsLeave = true;
                    lstBrandName.Focus();
                }
                else
                    IsLeave = false;
            }

            else if (e.KeyCode == Keys.Enter)
            {
                IsLeave = false;
                lstBrandName.Visible = false;
                txtStockGroupName_Leave(sender, e);
            }


        }

        private void StockGroupAE_Activated(object sender, EventArgs e)
        {
            try
            {
                if (isDoProcess)
                {
                    if (GrType == 2)
                    {
                        if (cmbDepartment.Enabled == true)
                        {
                            long tID = ObjFunction.GetComboValue(cmbDepartment);
                            ObjFunction.FillCombo(cmbDepartment, "Select StockGroupNo,StockGroupName From MStockGroup Where ControlGroup=4 AND (IsActive = 'True' OR  StockGroupNo=" + mStockGroup.ControlSubGroup + ")");
                            cmbDepartment.SelectedValue = tID;
                        }

                    }
                    else if (GrType == 3)
                    {
                        if (cmbCategory.Enabled == true)
                        {

                            long tID = ObjFunction.GetComboValue(cmbCategory);

                            ObjFunction.FillCombo(cmbCategory, "Select StockGRoupNo,StockGroupName From MStockGRoup Where IsActive='True' AND StockGRoupNo=" + mStockGroup.ControlSubGroup + "  AND ControlGroup=2 AND ControlSubGroup=" + ObjFunction.GetComboValue(cmbDepartment) + "");

                            cmbCategory.SelectedValue = tID;
                        }
                        long tCompnoID = ObjFunction.GetComboValue(cmbManufacturerCompanyName);
                        ObjFunction.FillCombo(cmbManufacturerCompanyName, "Select MfgCompNo,MfgCompName From MManufacturerCompany Where IsActive = 'True' And MfgCompNo=" + mStockGroup.MfgCompNo + " Order By MfgCompName");
                        cmbManufacturerCompanyName.SelectedValue = tCompnoID;
                    }
                    isDoProcess = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void StockGroupAE_Deactivate(object sender, EventArgs e)
        {
            isDoProcess = true;
        }

        private void txtStockGroupName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DataRow[] dr = null;
                if (GrType == 3)
                {
                    if (txtStockGroupName.Text.Trim() != "")
                    {
                        dr = dtGroup.Select("StockGroupname like '" + txtStockGroupName.Text.Trim().Replace("'", "''") + "%'");
                        if (dr.Length > 0)
                        {
                            lstBrandName.Visible = true;
                            lstBrandName.Items.Clear();
                            for (int i = 0; i < dr.Length; i++)
                            {
                                lstBrandName.Items.Add(dr[i].ItemArray[0].ToString());
                            }
                        }
                        else
                        {
                            lstBrandName.Visible = false;
                        }
                    }
                    else
                    {
                        lstBrandName.Visible = false;
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void lstBrandName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    e.SuppressKeyPress = true;
                    txtStockGroupName.Text = lstBrandName.Text;
                    //txtStockGroupName.Focus();
                    long GrNo = ObjQry.ReturnLong("Select StockGroupNo from MStockGroup where StockGroupName = '" + txtStockGroupName.Text.Trim().Replace("'", "''") + "' AND ControlGroup=" + GrType + "", CommonFunctions.ConStr);
                    ID = GrNo;
                    FillControls();
                    //lstBrandName.Visible = false;
                    //ObjFunction.LockButtons(false, this.Controls);
                    //ObjFunction.LockControls(true, this.Controls);
                    chkActive.Checked = true;
                    BtnSave.Focus();
                }
                catch (Exception exc)
                {
                    ObjFunction.ExceptionDisplay(exc.Message);
                }

            }
            else if (e.KeyCode == Keys.Escape)
            {
                e.SuppressKeyPress = true;
                txtStockGroupName.Focus();
                lstBrandName.Visible = false;
            }
        }

        private void lstBrandName_Leave(object sender, EventArgs e)
        {
            txtStockGroupName.Focus();
            lstBrandName.Visible = false;
        }

        private void cmbManufacturerCompanyName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                BtnSave.Focus();
            }
        }

        private void btnNewManufacturer_Click(object sender, EventArgs e)
        {
            try
            {
                Form NewF = new Master.ManufacturerCompanyAE(-1);
                ObjFunction.OpenForm(NewF);
                long MfgCompNo = ObjFunction.GetComboValue(cmbManufacturerCompanyName);
                if (((Master.ManufacturerCompanyAE)NewF).ShortID != 0)
                {
                    ObjFunction.FillCombo(cmbManufacturerCompanyName, "Select MfgCompNo,MfgCompName From MManufacturerCompany Where IsActive = 'True' order by MfgCompName");
                    if (((Master.ManufacturerCompanyAE)NewF).ShortID > 0)
                        cmbManufacturerCompanyName.SelectedValue = ((Master.ManufacturerCompanyAE)NewF).ShortID;
                    else
                        cmbManufacturerCompanyName.SelectedValue = MfgCompNo;
                    cmbManufacturerCompanyName.Focus();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void txtMargin_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(((TextBox)sender), 2, 4, JitFunctions.MaskedType.NotNegative);
        }

        private void txtMargin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                chkApplyToAll.Focus();
            }

        }

        private void chkApplyToAll_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                chkActive.Focus();
            }
        }

    }
}
