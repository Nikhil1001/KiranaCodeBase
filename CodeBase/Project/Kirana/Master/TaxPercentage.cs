﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Master
{
    public partial class PercentageVAT : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();
        DBMLedger dbLedger = new DBMLedger();
        MLedger mLedger = new MLedger();
        MItemTaxSetting mItemTaxSetting = new MItemTaxSetting();
        public DialogResult DS = DialogResult.OK;
        long TransactionTypeNo = 0, TaxTypeNo;
        string strTransactionTypeName = "", strTaxTypeName = "";

        private PercentageVAT()
        {
            InitializeComponent();
        }

        public PercentageVAT(long TransactionTypeNo, long TaxTypeNo)
            : this()
        {
            this.TransactionTypeNo = TransactionTypeNo;
            this.TaxTypeNo = TaxTypeNo;
        }

        private void txtPercentage_Leave(object sender, EventArgs e)
        {
            /*13-Apr-2014
            * Exception Handling Purpose.
            * */
            try
            {
                if (txtPercentage.Text.Trim() != "")
                {
                    if (ObjQry.ReturnInteger("Select Count(*) FROM MItemTaxSetting " +
                                    " INNER JOIN MLedger ON MItemTaxSetting.SalesLedgerNo = MLedger.LedgerNo " +
                                    " INNER JOIN MLedger AS MLedger_1 ON MItemTaxSetting.TaxLedgerNo = MLedger_1.LedgerNo " +
                                    " WHERE (MLedger.GroupNo = " + TransactionTypeNo + ") " +
                                        " AND (MLedger_1.GroupNo = " + TaxTypeNo + ") " +
                                        " AND (MItemTaxSetting.Percentage= " + Convert.ToDouble(txtPercentage.Text.Trim()) + ")", CommonFunctions.ConStr) != 0)
                    {
                        EP.SetError(txtPercentage, "Duplicate Percentage");
                        EP.SetIconAlignment(txtPercentage, ErrorIconAlignment.MiddleRight);
                        txtPercentage.Focus();
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);//For Common Error Displayed Purpose
            }
        }

        private void TaxPercentage_Load(object sender, EventArgs e)
        {
            txtPercentage.Focus();
            if (TransactionTypeNo == GroupType.SalesAccount)
            {
                strTransactionTypeName = "Sales";
                if (TaxTypeNo == GroupType.VAT)
                {
                    strTaxTypeName = "VAT";
                }
            }
            else if (TransactionTypeNo == GroupType.PurchaseAccount)
            {
                strTransactionTypeName = "Purchase";
                if (TaxTypeNo == GroupType.VAT)
                {
                    strTaxTypeName = "VAT";
                }
            }

        }

        public bool Validations()
        {
            bool flag = false;
            EP.SetError(txtPercentage, "");
            if (txtPercentage.Text.Trim() == "")
            {
                EP.SetError(txtPercentage, "Enter Percentage");
                EP.SetIconAlignment(txtPercentage, ErrorIconAlignment.MiddleRight);
                txtPercentage.Focus();
            }
            else if (ObjQry.ReturnInteger(" Select Count(*) FROM MItemTaxSetting " +
                        " INNER JOIN MLedger ON MItemTaxSetting.SalesLedgerNo = MLedger.LedgerNo " +
                        " INNER JOIN MLedger AS MLedger_1 ON MItemTaxSetting.TaxLedgerNo = MLedger_1.LedgerNo " +
                        " WHERE (MLedger.GroupNo = " + TransactionTypeNo + ") " +
                                " AND (MLedger_1.GroupNo = " + TaxTypeNo + ") " +
                                " AND  (MItemTaxSetting.Percentage= " + Convert.ToDouble(txtPercentage.Text.Trim()) + ")", CommonFunctions.ConStr) != 0)
            {
                EP.SetError(txtPercentage, "Duplicate Percentage");
                EP.SetIconAlignment(txtPercentage, ErrorIconAlignment.MiddleRight);
                txtPercentage.Focus();
            }
            else
                flag = true;

            return flag;
        }

        private void btnPecOk_Click(object sender, EventArgs e)
        {
            /*13-Apr-2014
            * Exception Handling Purpose.
            * */
            try
            {
                if (Validations() == true)
                {

                    if (TaxTypeNo == GroupType.VAT)
                    {
                        dbLedger = new DBMLedger();
                        mLedger = new MLedger();
                        mLedger.LedgerNo = 0;
                        mLedger.LedgerUserNo = "0";
                        mLedger.LedgerName = strTaxTypeName + " @ " + txtPercentage.Text.Trim() + " % " + strTransactionTypeName;
                        mLedger.GroupNo = TaxTypeNo;
                        mLedger.ContactPerson = "";
                        mLedger.InvFlag = false;
                        mLedger.MaintainBillByBill = false;
                        mLedger.IsActive = true;
                        mLedger.LedgerStatus = 1;
                        mLedger.UserID = DBGetVal.UserID;
                        mLedger.UserDate = DBGetVal.ServerTime.Date;
                        dbLedger.AddMLedger(mLedger);

                        mLedger = new MLedger();
                        mLedger.LedgerNo = 0;
                        mLedger.LedgerUserNo = "0";
                        mLedger.LedgerName = strTransactionTypeName + " @ " + txtPercentage.Text.Trim() + " % " + strTaxTypeName;
                        mLedger.GroupNo = TransactionTypeNo;
                        mLedger.ContactPerson = "";
                        mLedger.InvFlag = false;
                        mLedger.MaintainBillByBill = false;
                        mLedger.IsActive = true;
                        mLedger.LedgerStatus = 1;
                        mLedger.UserID = DBGetVal.UserID;
                        mLedger.UserDate = DBGetVal.ServerTime.Date;
                        dbLedger.AddMLedger(mLedger);

                        mItemTaxSetting = new MItemTaxSetting();
                        mItemTaxSetting.PkSrNo = 0;
                        mItemTaxSetting.TaxSettingName = txtPercentage.Text.Trim() + " % " + strTaxTypeName + " " + strTransactionTypeName;
                        mItemTaxSetting.Percentage = Convert.ToDouble(txtPercentage.Text.Trim());
                        mItemTaxSetting.IsActive = true;
                        mItemTaxSetting.CalculationMethod = "2";
                        mItemTaxSetting.UserID = DBGetVal.UserID;
                        mItemTaxSetting.UserDate = DBGetVal.ServerTime.Date;
                        mItemTaxSetting.TaxTypeNo = TaxTypeNo;
                        mItemTaxSetting.TransactionTypeNo = TransactionTypeNo;

                        dbLedger.AddMItemTaxSetting(mItemTaxSetting);

                        if (dbLedger.ExecuteNonQueryStatements() == true)
                        {
                            OMMessageBox.Show("Data Saved Successfuly", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            DS = DialogResult.OK;
                            this.Close();
                        }
                        else
                        {
                            OMMessageBox.Show("Data not Saved Successfuly", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        }
                    }
                    else if (TaxTypeNo == GroupType.GST)
                    {
                        try
                        {
                            getTaxSettingNo(Convert.ToDouble(txtPercentage.Text), GroupType.SalesAccount);
                            getTaxSettingNo(Convert.ToDouble(txtPercentage.Text), GroupType.PurchaseAccount);
                            DS = DialogResult.OK;
                            this.Close();
                        }
                        catch (Exception ex)
                        {
                            OMMessageBox.Show(ex.Message, CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);//For Common Error Displayed Purpose
            }
        }

        private void BtnPerCancel_Click(object sender, EventArgs e)
        {
            DS = DialogResult.Cancel;
            this.Close();
        }

        private void txtPercentage_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked((TextBox)sender, 2, 3);
        }


        public long getTaxSettingNo(double taxSlabPercent, long TransactionTypeNo)
        {
            long TaxSettingNo = ObjQry.ReturnLong("Select PkSrNo FROM MItemTaxSetting " +
                " Where Percentage = " + taxSlabPercent + " AND IsActive = 'True' " +
                    " AND TaxTypeNo = " + GroupType.GST + " AND TransactionTypeNo = " + TransactionTypeNo + " ", CommonFunctions.ConStr);

            if (TaxSettingNo <= 0)
            {
                addTaxSetting(taxSlabPercent, TransactionTypeNo);

                TaxSettingNo = ObjQry.ReturnLong("Select PkSrNo FROM MItemTaxSetting " +
                " Where Percentage = " + taxSlabPercent + " AND IsActive = 'True' " +
                    " AND TaxTypeNo = " + GroupType.GST + " AND TransactionTypeNo = " + TransactionTypeNo + " ", CommonFunctions.ConStr);
                if (TaxSettingNo <= 0)
                {
                    throw new Exception("Unable to add Tax Settings: Unknown Error");
                }
            }

            return TaxSettingNo;
        }

        private void addTaxSetting(double taxSlabPercent, long TransactionTypeNo)
        {
            DBMLedger dbLedger = new DBMLedger();
            MLedger mLedger = new MLedger();
            mLedger.LedgerNo = 0;
            mLedger.LedgerUserNo = "0";
            mLedger.LedgerName = "GST @ " + taxSlabPercent.ToString(Format.DoubleFloating) + " % " +
                                    (TransactionTypeNo == GroupType.SalesAccount ? "Sales" : "Purchase");
            mLedger.GroupNo = GroupType.GST;
            mLedger.ContactPerson = "";
            mLedger.InvFlag = false;
            mLedger.MaintainBillByBill = false;
            mLedger.IsActive = true;
            mLedger.LedgerStatus = 1;
            mLedger.UserID = 0;
            mLedger.UserDate = DateTime.Now;
            dbLedger.AddMLedger(mLedger);

            mLedger = new MLedger();
            mLedger.LedgerNo = 0;
            mLedger.LedgerUserNo = "0";
            mLedger.LedgerName = (TransactionTypeNo == GroupType.SalesAccount ? "Sales" : "Purchase") + " @ " +
                                    taxSlabPercent.ToString(Format.DoubleFloating) + " % GST";
            mLedger.GroupNo = TransactionTypeNo;
            mLedger.ContactPerson = "";
            mLedger.InvFlag = false;
            mLedger.MaintainBillByBill = false;
            mLedger.IsActive = true;
            mLedger.LedgerStatus = 1;
            mLedger.UserID = 0;
            mLedger.UserDate = DateTime.Now;
            dbLedger.AddMLedger(mLedger);

            MItemTaxSetting mItemTaxSetting = new MItemTaxSetting();
            mItemTaxSetting.PkSrNo = 0;
            mItemTaxSetting.TaxSettingName = taxSlabPercent.ToString(Format.DoubleFloating) + " % GST " + (TransactionTypeNo == GroupType.SalesAccount ? "Sales" : "Purchase");
            mItemTaxSetting.Percentage = taxSlabPercent;
            mItemTaxSetting.IsActive = true;
            mItemTaxSetting.CalculationMethod = "2";
            mItemTaxSetting.UserID = 0;
            mItemTaxSetting.UserDate = DateTime.Now;
            mItemTaxSetting.TaxTypeNo = GroupType.GST;
            mItemTaxSetting.TransactionTypeNo = TransactionTypeNo;

            dbLedger.AddMItemTaxSetting(mItemTaxSetting);

            if (!dbLedger.ExecuteNonQueryStatements() == true)
            {
                throw new Exception("Error while adding new Tax setting : Err : " + DBMLedger.strerrormsg);
            }
        }
    }
}
