﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Master
{
    /// <summary>
    /// This class is used for Manufacturer Company AE
    /// </summary>
    public partial class ManufacturerCompanyAE : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        DBMManufacturerCompany dbManufacturerCompany = new DBMManufacturerCompany();
        MManufacturerCompany mManufacturerCompany = new MManufacturerCompany();
        string ManufacturerCompanyNm;
        DataTable dtSearch = new DataTable();
        DataTable dtMFG = new DataTable();
        int cntRow;
        long ID;
        
        bool IsLeave = false;//isDoProcess = false
        /// <summary>
        /// This field is used for Short ID 
        /// </summary>
        public long ShortID = 0;

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public ManufacturerCompanyAE()
        {
            InitializeComponent();
        }

        /// <summary>
        /// This is class of Parameterised Constructor
        /// </summary>
        public ManufacturerCompanyAE(long shortid)
        {
            InitializeComponent();
            ShortID = shortid;
        }

        private void ManufacturerCompanyAE_Load(object sender, EventArgs e)
        {
            try
            {
                ObjFunction.LockButtons(true, this.Controls);
                ObjFunction.LockControls(false, this.Controls);

                ManufacturerCompanyNm = "";
                dtMFG = ObjFunction.GetDataView("Select MfgCompName from MManufacturerCompany order by  MfgCompName").Table;
                dtSearch = ObjFunction.GetDataView("Select MfgCompNo From MManufacturerCompany order by MfgCompName").Table;
                if (ShortID == 0)
                {
                    if (dtSearch.Rows.Count > 0)
                    {
                        if (ManufacturerCompany.RequestManufacturerCompanyNo == 0)
                            ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                        else
                            ID = ManufacturerCompany.RequestManufacturerCompanyNo;
                        FillControls();
                        SetNavigation();
                    }
                    setDisplay(true);
                    btnNew.Focus();
                }
                else
                {
                    btnNew_Click(sender, new EventArgs());
                    setDisable(false);
                }
                KeyDownFormat(this.Controls);
                lstMfgName.Visible = false;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillControls()
        {
            try
            {
                EP.SetError(txtManufacturerCompanyName, "");
                mManufacturerCompany = dbManufacturerCompany.ModifyMManufacturerCompanyByID(ID);
                ManufacturerCompanyNm = mManufacturerCompany.MfgCompName;
                txtManufacturerCompanyName.Text = mManufacturerCompany.MfgCompName;
                txtShortName.Text = mManufacturerCompany.MfgCompShortCode;
                chkActive.Checked = mManufacturerCompany.IsActive;
                if (chkActive.Checked == true)
                    chkActive.Text = "Yes";
                else
                    chkActive.Text = "No";
                lstMfgName.Visible = false;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void setDisable(bool flag)
        {
            btnSave.Visible = !flag;
            btnCancel.Visible = !flag;
            btnUpdate.Visible = flag;
            btnSearch.Visible = flag;
            btnExit.Visible = flag;
            btnFirst.Visible = flag;
            btnPrev.Visible = flag;
            btnNext.Visible = flag;
            btnLast.Visible = flag;
            btnDelete.Visible = flag;
        }

        private void SetValue()
        {
            try
            {
                if (Validations() == true)
                {
                    dbManufacturerCompany = new DBMManufacturerCompany();
                    mManufacturerCompany = new MManufacturerCompany();

                    mManufacturerCompany.MfgCompNo = ID;
                    mManufacturerCompany.MfgCompName = txtManufacturerCompanyName.Text.Trim().ToUpper();
                    mManufacturerCompany.MfgCompShortCode = txtShortName.Text.Trim().ToUpper();
                    mManufacturerCompany.IsActive = chkActive.Checked;
                    mManufacturerCompany.UserID = DBGetVal.UserID;
                    mManufacturerCompany.UserDate = DBGetVal.ServerTime.Date;
                    mManufacturerCompany.CompanyNo = DBGetVal.CompanyNo;

                    if (dbManufacturerCompany.AddMManufacturerCompany(mManufacturerCompany) == true)
                    {
                        if (ID == 0)
                        {
                            OMMessageBox.Show("Manufacturer Company Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            dtSearch = ObjFunction.GetDataView("Select MfgCompNo From MManufacturerCompany order by MfgCompName").Table;
                            ID = ObjQry.ReturnLong("Select Max(MfgCompNo) FRom MManufacturerCompany", CommonFunctions.ConStr);
                            if (ShortID == 0)
                            {
                                SetNavigation();
                                FillControls();
                            }
                            else
                            {
                                ShortID = ID;
                                this.Close();
                            }
                        }
                        else
                        {
                            OMMessageBox.Show("Manufacturer Company Updated Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            FillControls();
                        }

                        ObjFunction.LockButtons(true, this.Controls);
                        ObjFunction.LockControls(false, this.Controls);
                    }
                    else
                    {
                        OMMessageBox.Show("Manufacturer Company not saved", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            ManufacturerCompany.RequestManufacturerCompanyNo = 0;
            this.Close();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            SetValue();
        }

        private bool Validations()
        {
            bool flag = false;
            EP.SetError(txtManufacturerCompanyName, "");

            try
            {
                if (txtManufacturerCompanyName.Text.Trim() == "")
                {
                    EP.SetError(txtManufacturerCompanyName, "Enter Manufacturer Company Name");
                    EP.SetIconAlignment(txtManufacturerCompanyName, ErrorIconAlignment.MiddleRight);
                    txtManufacturerCompanyName.Focus();
                }
                else if (ManufacturerCompanyNm != txtManufacturerCompanyName.Text.Trim().ToUpper())
                {
                    if (ObjQry.ReturnInteger("Select Count(*) from MManufacturerCompany where MfgCompName = '" + txtManufacturerCompanyName.Text.Trim().Replace("'", "''") + "'", CommonFunctions.ConStr) != 0)
                    {
                        EP.SetError(txtManufacturerCompanyName, "Duplicate Manufacturer Company  Name");
                        EP.SetIconAlignment(txtManufacturerCompanyName, ErrorIconAlignment.MiddleRight);
                        txtManufacturerCompanyName.Focus();
                    }
                    else
                        flag = true;
                }
                else
                    flag = true;
                return flag;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return flag;
            }

        }

        private void CityAE_FormClosing(object sender, FormClosingEventArgs e)
        {
            ID = 0;
            ManufacturerCompanyNm = "";
        }

        private void txtManufacturerCompanyName_Leave(object sender, EventArgs e)
        {
            try
            {
                EP.SetError(txtManufacturerCompanyName, "");
                if (IsLeave == false)
                {
                    if (txtManufacturerCompanyName.Text.Trim() != "")
                    {
                        if (ManufacturerCompanyNm != txtManufacturerCompanyName.Text.Trim().ToUpper())
                        {
                            if (ObjQry.ReturnInteger("Select Count(*) from MManufacturerCompany where MfgCompName = '" + txtManufacturerCompanyName.Text.Trim().Replace("'", "''") + "'", CommonFunctions.ConStr) != 0)
                            {
                                EP.SetError(txtManufacturerCompanyName, "Duplicate Manufacturer Company Name");
                                EP.SetIconAlignment(txtManufacturerCompanyName, ErrorIconAlignment.MiddleRight);
                                txtManufacturerCompanyName.Focus();
                            }
                        }

                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #region Navigation Methods
        private void NavigationDisplay(int type)
        {
            try
            {
                long No = 0;
                if (dtSearch.Rows.Count > 0)
                {
                    if (type == 5)
                    {
                        No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                        ID = No;
                    }
                    else if (type == 1)
                    {
                        No = Convert.ToInt64(dtSearch.Rows[0].ItemArray[0].ToString());
                        cntRow = 0;
                        ID = No;
                    }
                    else if (type == 2)
                    {
                        No = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                        cntRow = dtSearch.Rows.Count - 1;
                        ID = No;
                    }
                    else
                    {
                        if (type == 3)
                        {
                            cntRow = cntRow + 1;
                        }
                        else if (type == 4)
                        {
                            cntRow = cntRow - 1;
                        }

                        if (cntRow < 0)
                        {
                            OMMessageBox.Show("This is First Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            cntRow = cntRow + 1;
                        }
                        else if (cntRow > dtSearch.Rows.Count - 1)
                        {
                            OMMessageBox.Show("This is Last Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            cntRow = cntRow - 1;
                        }
                        else
                        {
                            No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                            ID = No;
                        }

                    }


                    FillControls();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

        }

        private void SetNavigation()
        {
            cntRow = 0;
            for (int i = 0; i < dtSearch.Rows.Count; i++)
            {
                if (Convert.ToInt64(dtSearch.Rows[i].ItemArray[0].ToString()) == ID)
                {
                    cntRow = i;
                    break;
                }
            }
        }

        private void setDisplay(bool flag)
        {
            btnFirst.Visible = flag;
            btnPrev.Visible = flag;
            btnNext.Visible = flag;
            btnLast.Visible = flag;
            btnDelete.Visible = flag;
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            NavigationDisplay(1);
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            NavigationDisplay(4);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            NavigationDisplay(3);
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            NavigationDisplay(2);
        }

        #endregion

        #region KeyDown Events
        private void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is GroupBox)
                    KeyDownFormat(ctrl.Controls);
            }
        }

        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left && e.Control)
            {
                if (btnPrev.Enabled) btnPrev_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Up && e.Control)
            {
                if (btnFirst.Enabled) btnFirst_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Right && e.Control)
            {
                if (btnNext.Enabled) btnNext_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Down && e.Control)
            {
                if (btnLast.Enabled) btnLast_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F2)
            {
                if (btnSave.Visible) BtnSave_Click(sender, e);
            }

        }
        #endregion

        private void chkActive_CheckedChanged(object sender, EventArgs e)
        {
            if (chkActive.Checked == true)
                chkActive.Text = "Yes";
            else
                chkActive.Text = "No";

        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (chkActive.Checked == false) return;
                dbManufacturerCompany = new DBMManufacturerCompany();
                mManufacturerCompany = new MManufacturerCompany();

                mManufacturerCompany.MfgCompNo = ID;
                if (OMMessageBox.Show("Are you sure want to delete this record?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (dbManufacturerCompany.DeleteMManufacturerCompany(mManufacturerCompany) == true)
                    {
                        OMMessageBox.Show("Manufacturer Company Deleted Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        FillControls();
                    }
                    else
                    {
                        OMMessageBox.Show("Manufacturer Company not Deleted", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }

                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

        }


        private void btnSearch_Click(object sender, EventArgs e)
        {
            Form NewF = new ManufacturerCompany();
            this.Close();
            ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            ObjFunction.LockButtons(false, this.Controls);
            ObjFunction.LockControls(true, this.Controls);
            txtManufacturerCompanyName.Focus();
            lstMfgName.Visible = false;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (ShortID == 0)
            {
                NavigationDisplay(5);
                ObjFunction.LockButtons(true, this.Controls);
                ObjFunction.LockControls(false, this.Controls);
            }
            else this.Close();
            lstMfgName.Visible = false;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                ID = 0;
                ManufacturerCompanyNm = "";
                mManufacturerCompany = new MManufacturerCompany();
                ObjFunction.InitialiseControl(this.Controls);
                ObjFunction.LockButtons(false, this.Controls);
                ObjFunction.LockControls(true, this.Controls);
                txtManufacturerCompanyName.Focus();
                chkActive.Checked = true;
                lstMfgName.Visible = false;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void CityAE_Activated(object sender, EventArgs e)
        {
            //try
            //{
            //    isDoProcess = false;
            //}
            //catch (Exception exc)
            //{
            //    ObjFunction.ExceptionDisplay(exc.Message);
            //}
        }

        private void CityAE_Deactivate(object sender, EventArgs e)
        {
            //isDoProcess = true;
        }

        private void txtManufacturerCompanyName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                e.SuppressKeyPress = true;
                if (lstMfgName.Visible)
                {
                    IsLeave = true;
                    lstMfgName.Focus();
                }
                else
                    IsLeave = false;
            }
            else if (e.KeyCode == Keys.Enter)
            {
                IsLeave = false;
                lstMfgName.Visible = false;
                txtManufacturerCompanyName_Leave(sender, e);
            }
            //if (e.KeyCode == Keys.Escape)
            //{
            //    e.SuppressKeyPress = true;
            //    txtManufacturerCompanyName.Focus();
            //    lstMfgName.Visible = false;
            //}
        }

        private void txtManufacturerCompanyName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DataRow[] dr = null;
                    if (txtManufacturerCompanyName.Text.Trim() != "")
                    {
                        dr = dtMFG.Select("MfgCompName like '" + txtManufacturerCompanyName.Text.Trim().Replace("'", "''") + "%'");
                        if (dr.Length > 0)
                        {
                            lstMfgName.Visible = true;
                            lstMfgName.Items.Clear();
                            for (int i = 0; i < dr.Length; i++)
                            {
                                lstMfgName.Items.Add(dr[i].ItemArray[0].ToString());
                            }
                        }
                        else
                        {
                            lstMfgName.Visible = false;
                        }
                    }
                    else
                    {
                        lstMfgName.Visible = false;
                    }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void lstMfgName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    e.SuppressKeyPress = true;
                    txtManufacturerCompanyName.Text = lstMfgName.Text;
                    long MFGNo = ObjQry.ReturnLong("Select MfgCompNo from MManufacturerCompany where MfgCompName = '" + txtManufacturerCompanyName.Text.Trim().Replace("'", "''") + "' ", CommonFunctions.ConStr);
                    ID = MFGNo;
                    FillControls();
                    chkActive.Checked = true;
                    btnSave.Focus();
                }
                catch (Exception exc)
                {
                    ObjFunction.ExceptionDisplay(exc.Message);
                }

            }
            else if (e.KeyCode == Keys.Escape)
            {
                e.SuppressKeyPress = true;
                txtManufacturerCompanyName.Focus();
                lstMfgName.Visible = false;
            }
        }

        private void lstMfgName_Leave(object sender, EventArgs e)
        {
            txtManufacturerCompanyName.Focus();
            lstMfgName.Visible = false;
        }
    }
}
