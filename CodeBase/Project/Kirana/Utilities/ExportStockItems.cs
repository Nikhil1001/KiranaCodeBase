﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
using System.Data.OleDb;


namespace Kirana.Utilities
{
    /// <summary>
    /// This class used for Export Stock Items
    /// </summary>
    public partial class ExportStockItems : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.QueryOutPut objQry = new Transaction.QueryOutPut();
        DataTable dt = new DataTable();
        DBMStockItems dbStockItems = new DBMStockItems();
        MStockItems mStockItems = new MStockItems();
        MStockBarcode mStockBarcode = new MStockBarcode();
        MRateSetting mRateSetting = new MRateSetting();
        DBMLedger dbLedger = new DBMLedger();
        MLedger mLedger = new MLedger();
        DataTable dtLang;
        string sql;

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public ExportStockItems()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnExportItems_Click(object sender, EventArgs e)
        {
            try
            {
                dt = ObjFunction.GetDataView(sql).Table;

                if (dt.Rows.Count > 0)
                {


                    int col = 1;
                    CreateExcel excel = new CreateExcel();
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        excel.createHeaders(col, i + 1, dt.Columns[i].ColumnName, excel.ColName(col, i + 1), excel.ColName(col, i + 1), 1, Color.Gainsboro, true, 20, Color.Black, 12, CreateExcel.ExAlign.Left);
                    }
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        for (int j = 0; j < dt.Columns.Count; j++)
                        {
                            if (j == 6 || j == 7 || j == 9)
                                excel.addData(i + col + 1, j + 1, dt.Rows[i].ItemArray[j].ToString(), excel.ColName(i + col + 1, j + 1), excel.ColName(i + col + 1, j + 1), Format.NoFloating, 0, CreateExcel.ExAlign.Left, false, dtLang.Rows[0].ItemArray[0].ToString(), 16);
                            else
                                excel.addData(i + col + 1, j + 1, dt.Rows[i].ItemArray[j].ToString(), excel.ColName(i + col + 1, j + 1), excel.ColName(i + col + 1, j + 1), Format.NoFloating, 0, CreateExcel.ExAlign.Left, false);
                        }
                    }
                    //excel.CompleteDoc("ItemExportList_" + DateTime.Now.ToString("dd-MMM-yyyy") + "_" + DateTime.Now.ToShortTimeString());
                    //excel.CompleteDoc("ItemExportList");

                }
            }
            catch (Exception ex)
            {
                OMMessageBox.Show("Items are not exported", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                CommonFunctions.ErrorMessge = ex.Message;
            }
        }

        private void ExportStockItems_Load(object sender, EventArgs e)
        {
            dtLang = ObjFunction.GetDataView("Select FontName,FontSize,FontBold From MLanguage Where LanguageNo=" + ObjFunction.GetAppSettings(AppSettings.O_Language) + "").Table;

            panel1.Visible = false;
            btnSave.Visible = false;
            sql = "SELECT DISTINCT MStockItems.ItemNo, MStockBarcode.Barcode, " +
                           " (SELECT  StockGroupName " +
                           " FROM  MStockGroup " +
                           " WHERE  (ControlGroup = 4) AND (StockGroupNo = MStockItems.FKStockDeptNo)) AS Department, " +
                           " (SELECT  StockGroupName " +
                           " FROM   MStockGroup AS MStockGroup_1 " +
                           " WHERE  (ControlGroup = 2) AND (StockGroupNo = MStockItems.GroupNo1)) AS Category, MStockItems.ItemName, MStockItems.ItemShortCode AS ShortDesc, " +
                           " MStockItems.LangFullDesc, MStockItems.LangShortDesc, " +
                           " (SELECT  StockGroupName " +
                           " FROM   MStockGroup AS MStockGroup_1 " +
                           " WHERE  (ControlGroup = 3) AND (StockGroupNo = MStockItems.GroupNo)) AS BrandName, " +
                           " (SELECT  LanguageName " +
                           " FROM  MStockGroup AS MStockGroup_2 " +
                           " WHERE  (ControlGroup = 3) AND (StockGroupNo = MStockItems.GroupNo)) AS 'Language - Brand', " +
                           " (SELECT  UOMName " +
                           " FROM  MUOM " +
                           " WHERE  (MStockItems.UOMPrimary = UOMNo)) AS UomName, ISNULL(MRateSetting.MRP,0) As MRP, ISNULL(MRateSetting.PurRate,0) As PurRate, ISNULL(MRateSetting.ASaleRate,0) AS 'WholeSale', ISNULL(MRateSetting.BSaleRate,0) As Retail, " +
                           " ISNULL(MStockItems.MinLevel,0) As MinLevel, ISNULL(MStockItems.MaxLevel,0) As MaxLevel, " +
                           " (SELECT Percentage FROM MItemTaxSetting AS MItemTaxSetting_2 WHERE (PkSrNo IN (SELECT  MItemTaxSetting.Percentage " +
                           " FROM   MItemTaxSetting INNER JOIN " +
                           " MLedger ON MItemTaxSetting.SalesLedgerNo = MLedger.LedgerNo INNER JOIN " +
                           " MLedger AS MLedger_1 ON MItemTaxSetting.TaxLedgerNo = MLedger_1.LedgerNo INNER JOIN " +
                           " MItemTaxInfo ON MItemTaxSetting.PkSrNo = MItemTaxInfo.FKTaxSettingNo " +
                           " WHERE  (MLedger.GroupNo = 10) AND (MLedger_1.GroupNo = 32) AND (MItemTaxSetting.IsActive = 'True') AND (MItemTaxInfo.ItemNo = MStockItems.ItemNo)))) " +
                           " AS 'VAT Sale', " +
                           "  (SELECT Percentage FROM MItemTaxSetting AS MItemTaxSetting_2 WHERE (PkSrNo IN (SELECT  MItemTaxSetting_1.Percentage " +
                           " FROM  MItemTaxSetting AS MItemTaxSetting_1 INNER JOIN " +
                           " MLedger AS MLedger_2 ON MItemTaxSetting_1.SalesLedgerNo = MLedger_2.LedgerNo INNER JOIN " +
                           " MLedger AS MLedger_1 ON MItemTaxSetting_1.TaxLedgerNo = MLedger_1.LedgerNo INNER JOIN " +
                           " MItemTaxInfo AS MItemTaxInfo_1 ON MItemTaxSetting_1.PkSrNo = MItemTaxInfo_1.FKTaxSettingNo " +
                           " WHERE   (MLedger_2.GroupNo = 11) AND (MLedger_1.GroupNo = 32) AND (MItemTaxSetting_1.IsActive = 'True') AND " +
                           " (MItemTaxInfo_1.ItemNo = MStockItems.ItemNo)))) AS 'VAT Purchase' " +
                           " FROM   MStockItems INNER JOIN " +
                           " MStockBarcode ON MStockItems.ItemNo = MStockBarcode.ItemNo INNER JOIN " +
                           " MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo ";
            sql = "SELECT MStockItems.ItemNo, MStockBarcode.Barcode, MStockGroup_2.StockGroupName AS DepartmentName, MStockGroup_1.StockGroupName AS CategoryName, MStockItems.ItemName, MStockItems.ItemShortCode AS ShortDesc, MStockItems.LangFullDesc, MStockItems.LangShortDesc,  " +
                " MStockGroup.StockGroupName AS BrandName, MStockGroup.LanguageName AS 'Language-Brand', MUOM.UOMName, MRateSetting.MRP, MRateSetting.PurRate,  MRateSetting.ASaleRate AS 'WholeSale', MRateSetting.BSaleRate AS 'Retail', IsNull(MStockItems.MinLevel,0) AS MinLevel, IsNull(MStockItems.MaxLevel,0) AS MaxLevel, " +
                " IsNull((SELECT     Percentage FROM MItemTaxSetting AS MItemTaxSetting_2 WHERE (PkSrNo IN (SELECT Max(MItemTaxSetting.PkSrNo) FROM  MItemTaxSetting INNER JOIN MLedger ON MItemTaxSetting.SalesLedgerNo = MLedger.LedgerNo INNER JOIN MLedger AS MLedger_1 ON MItemTaxSetting.TaxLedgerNo = MLedger_1.LedgerNo INNER JOIN " +
                " MItemTaxInfo ON MItemTaxSetting.PkSrNo = MItemTaxInfo.FKTaxSettingNo WHERE (MLedger.GroupNo = 10) AND (MLedger_1.GroupNo = 32) AND (MItemTaxSetting.IsActive = 'True') AND  (MItemTaxInfo.ItemNo = MStockItems.ItemNo)))),0) AS 'VAT Sale'," +
                " IsNull((SELECT Percentage FROM  MItemTaxSetting AS MItemTaxSetting_2 WHERE (PkSrNo IN (SELECT Max(MItemTaxSetting_1.PkSrNo) FROM MItemTaxSetting AS MItemTaxSetting_1 INNER JOIN MLedger AS MLedger_2 ON MItemTaxSetting_1.SalesLedgerNo = MLedger_2.LedgerNo INNER JOIN MLedger AS MLedger_1 ON MItemTaxSetting_1.TaxLedgerNo = MLedger_1.LedgerNo INNER JOIN " +
                " MItemTaxInfo AS MItemTaxInfo_1 ON MItemTaxSetting_1.PkSrNo = MItemTaxInfo_1.FKTaxSettingNo WHERE      (MLedger_2.GroupNo = 11) AND (MLedger_1.GroupNo = 32) AND (MItemTaxSetting_1.IsActive = 'True') AND  (MItemTaxInfo_1.ItemNo = MStockItems.ItemNo)))),0) AS 'VAT Purchase' " +
                " FROM MStockBarcode INNER JOIN MUOM INNER JOIN dbo.GetItemRateAll(NULL, NULL, NULL, NULL, NULL, NULL) AS MRateSetting ON MUOM.UOMNo = MRateSetting.UOMNo INNER JOIN MStockItems ON MRateSetting.ItemNo = MStockItems.ItemNo ON MStockBarcode.PkStockBarcodeNo = MRateSetting.FkBcdSrNo INNER JOIN " +
                " MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo INNER JOIN MStockGroup AS MStockGroup_1 ON MStockItems.GroupNo1 = MStockGroup_1.StockGroupNo INNER JOIN " +
                " MStockGroup AS MStockGroup_2 ON MStockItems.FKStockDeptNo = MStockGroup_2.StockGroupNo";
            //, MStockItems.FKStockDeptNo, MStockItems.GroupNo, MStockItems.GroupNo1,MStockItems.FkCategoryNo, MStockItems.UOMDefault
        }

        private void btnImportItems_Click(object sender, EventArgs e)
        {
            try
            {
                

                while (dataGridView1.Rows.Count > 0)
                    dataGridView1.Rows.RemoveAt(0);
                OF.Filter = "Excel files (*.xls or .xlsx)|.xls;*.xlsx";
                if (OF.ShowDialog() == DialogResult.OK)
                {
                    string file = OF.FileName;
                    btnSave.Visible = true;
                   
                    dt = ObjFunction.GetDataView(sql).Table;

                    Excel.Application app = new Excel.Application();

                    Excel.Workbook wb = app.Workbooks.Open(@file, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                            Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                            Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                            Type.Missing, Type.Missing);//Firstfile\\print Express tables

                    Excel.Worksheet sheet = (Excel.Worksheet)wb.Sheets["Sheet1"];

                    Excel.Range excelRange = sheet.UsedRange;
                    int j = 0;
                    DataTable dt1 = new DataTable();
                    foreach (Microsoft.Office.Interop.Excel.Range row in excelRange.Rows)
                    {
                        int rowNumber = row.Row;

                        string[] A4D4 = GetRange("A" + rowNumber + ":S" + rowNumber + "", sheet);

                        if (j == 0)
                        {
                            for (int i = 0; i < A4D4.Length; i++)
                            {
                                dt1.Columns.Add(A4D4[i], typeof(string));
                            }
                        }
                        else
                        {
                            DataRow dr = dt1.NewRow();
                            for (int i = 0; i < A4D4.Length; i++)
                            {
                                dr[i] = A4D4[i];
                            }
                            dt1.Rows.Add(dr);
                        }
                        j++;
                    }
                    dataGridView1.DataSource = dt1.DefaultView;
                    dataGridView1.Columns[6].DefaultCellStyle.Font = ObjFunction.GetLangFont();
                    //dataGridView1.Columns[7].DefaultCellStyle.Font = ObjFunction.GetLangFont();
                    dataGridView1.Columns[9].DefaultCellStyle.Font = ObjFunction.GetLangFont();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private string[] GetRange(string range, Excel.Worksheet excelWorksheet)
        {
            string[] str = range.Split(':');
            int ch1 = (int)Convert.ToChar(str[0].Substring(0, 1));
            int ch2 = (int)Convert.ToChar(str[1].Substring(0, 1));
            string[] array1 = new string[ch2 - ch1 + 1];
            int k = ch2 - ch1 + 1;
            int j = 0;
            for (int i = 1; i <= k; i++)
            {
                //array1[j] = excelWorksheet.Cells[str[0].Substring(1, 1), Convert.ToChar(i)].ToString();
                array1[j] = ((Excel.Range)excelWorksheet.Cells[str[0].Substring(1, str[0].Length - 1), i]).Value2 == null ? "" : ((Excel.Range)excelWorksheet.Cells[str[0].Substring(1, str[0].Length - 1), i]).Value2.ToString();
                j++;
            }
            //excelWorksheet.Cells[str[0].Substring(1, 1), str];
            //Microsoft.Office.Interop.Excel.Range workingRangeCells =
            //  excelWorksheet.get_Range(range, Type.Missing);
            ////workingRangeCells.Select();

            //System.Array array = (System.Array)workingRangeCells.Cells.Value2;
            //string[] arrayS = array.OfType<object>().Select(o => o.ToString()).ToArray();//Convert.ToString(array);

            return array1;
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == ColIndex.Department || e.ColumnIndex == ColIndex.Category || e.ColumnIndex == ColIndex.ItemName || e.ColumnIndex == ColIndex.ShortDesc || e.ColumnIndex == ColIndex.LabgFullDesc || e.ColumnIndex == ColIndex.LangShortDesc || e.ColumnIndex == ColIndex.BrandName || e.ColumnIndex == ColIndex.LangBrand)
            {
                if (e.Value != null)
                {
                    if (e.RowIndex > dt.Rows.Count - 1)
                    {
                        dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.Yellow;
                    }
                    else
                    {
                        if (!dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.Equals(dt.Rows[e.RowIndex].ItemArray[e.ColumnIndex]))
                        {
                            dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.Yellow;
                            //btnSave.Visible = true;
                        }
                    }
                }
                else
                {

                }
            }
            if (e.ColumnIndex == ColIndex.MRP || e.ColumnIndex == ColIndex.PurRate || e.ColumnIndex == ColIndex.ASaleRate || e.ColumnIndex == ColIndex.BSaleRate)
            {
                if (String.IsNullOrEmpty(e.Value.ToString()) == false)
                {
                    if (e.RowIndex > dt.Rows.Count - 1)
                    {
                        dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.Yellow;
                    }
                    else
                    {
                        if (!Convert.ToDouble(dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value).Equals(Convert.ToDouble(dt.Rows[e.RowIndex].ItemArray[e.ColumnIndex].ToString())))
                        {
                            dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.Yellow;
                            //btnSave.Visible = true;
                        }
                    }
                }
            }
            if (e.ColumnIndex == ColIndex.MinLevel || e.ColumnIndex == ColIndex.MaxLevel)
            {
                if (String.IsNullOrEmpty(e.Value.ToString()) == false)
                {
                    if (e.RowIndex > dt.Rows.Count - 1)
                    {
                        dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.Yellow;
                    }
                    else
                    {
                        if (!Convert.ToInt64(dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value).Equals(Convert.ToInt64(dt.Rows[e.RowIndex].ItemArray[e.ColumnIndex].ToString())))
                        {
                            dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.Yellow;
                            //btnSave.Visible = true;
                        }
                    }
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            long count = 0;
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                if ((dataGridView1.Rows[i].Cells[ColIndex.ItemName].Style.BackColor == Color.Yellow || dataGridView1.Rows[i].Cells[ColIndex.ShortDesc].Style.BackColor == Color.Yellow
                    || dataGridView1.Rows[i].Cells[ColIndex.Department].Style.BackColor == Color.Yellow || dataGridView1.Rows[i].Cells[ColIndex.Category].Style.BackColor == Color.Yellow 
                    || dataGridView1.Rows[i].Cells[ColIndex.LabgFullDesc].Style.BackColor == Color.Yellow || dataGridView1.Rows[i].Cells[ColIndex.LangShortDesc].Style.BackColor == Color.Yellow 
                    || dataGridView1.Rows[i].Cells[ColIndex.BrandName].Style.BackColor == Color.Yellow || dataGridView1.Rows[i].Cells[ColIndex.LangBrand].Style.BackColor == Color.Yellow 
                    || dataGridView1.Rows[i].Cells[ColIndex.MRP].Style.BackColor == Color.Yellow || dataGridView1.Rows[i].Cells[ColIndex.PurRate].Style.BackColor == Color.Yellow 
                    || dataGridView1.Rows[i].Cells[ColIndex.ASaleRate].Style.BackColor == Color.Yellow || dataGridView1.Rows[i].Cells[ColIndex.BSaleRate].Style.BackColor == Color.Yellow 
                    || dataGridView1.Rows[i].Cells[ColIndex.MinLevel].Style.BackColor == Color.Yellow || dataGridView1.Rows[i].Cells[ColIndex.MaxLevel].Style.BackColor == Color.Yellow) 
                    && dataGridView1.Rows[i].Cells[ColIndex.ItemNo].Value != null && dataGridView1.Rows[i].Cells[ColIndex.ItemNo].Value.ToString() != "")
                {
                    if (dataGridView1.Rows[i].Cells[ColIndex.Department].Style.BackColor == Color.Yellow)
                    {
                        if (objQry.ReturnLong("SELECT StockGroupNo From MStockGroup WHERE ControlGroup=4 AND StockGroupName='" + dataGridView1.Rows[i].Cells[ColIndex.Department].Value + "'", CommonFunctions.ConStr) == 0)
                        {
                            DBMStockGroup dbStockGroup = new DBMStockGroup();
                            MStockGroup mStockGroup = new MStockGroup();

                            mStockGroup.StockGroupNo = 0;
                            mStockGroup.StockGroupName = dataGridView1.Rows[i].Cells[ColIndex.Department].Value.ToString();
                            mStockGroup.LanguageName = "";
                            mStockGroup.ControlGroup = 4;
                            mStockGroup.IsActive = true;
                            mStockGroup.UserId = DBGetVal.UserID;
                            mStockGroup.UserDate = DBGetVal.ServerTime.Date;
                            mStockGroup.CompanyNo = DBGetVal.CompanyNo;
                            mStockGroup.ControlSubGroup = 0;
                            mStockGroup.Margin = 0;
                            dbStockGroup.AddMStockGroup(mStockGroup);
                        }
                    }

                    if (dataGridView1.Rows[i].Cells[ColIndex.Category].Style.BackColor == Color.Yellow)
                    {
                        if (objQry.ReturnLong("SELECT StockGroupNo From MStockGroup WHERE ControlGroup=2 AND StockGroupName='" + dataGridView1.Rows[i].Cells[ColIndex.Category].Value + "'", CommonFunctions.ConStr) == 0)
                        {
                            DBMStockGroup dbStockGroup = new DBMStockGroup();
                            MStockGroup mStockGroup = new MStockGroup();

                            mStockGroup.StockGroupNo = 0;
                            mStockGroup.StockGroupName = dataGridView1.Rows[i].Cells[ColIndex.Category].Value.ToString();
                            mStockGroup.LanguageName = "";
                            mStockGroup.ControlGroup = 2;
                            mStockGroup.IsActive = true;
                            mStockGroup.UserId = DBGetVal.UserID;
                            mStockGroup.UserDate = DBGetVal.ServerTime.Date;
                            mStockGroup.CompanyNo = DBGetVal.CompanyNo;
                            mStockGroup.ControlSubGroup = objQry.ReturnLong("SELECT StockGroupNo From MStockGroup WHERE ControlGroup=4 AND StockGroupName='" + dataGridView1.Rows[i].Cells[ColIndex.Department].Value + "'", CommonFunctions.ConStr);
                            mStockGroup.Margin = 0;
                            dbStockGroup.AddMStockGroup(mStockGroup);
                        }
                    }

                    if (dataGridView1.Rows[i].Cells[ColIndex.BrandName].Style.BackColor == Color.Yellow)
                    {
                        if (objQry.ReturnLong("SELECT StockGroupNo From MStockGroup WHERE ControlGroup=2 AND StockGroupName='" + dataGridView1.Rows[i].Cells[ColIndex.Category].Value + "'", CommonFunctions.ConStr) == 0)
                        {
                            DBMStockGroup dbStockGroup = new DBMStockGroup();
                            MStockGroup mStockGroup = new MStockGroup();

                            mStockGroup.StockGroupNo = 0;
                            mStockGroup.StockGroupName = dataGridView1.Rows[i].Cells[ColIndex.BrandName].Value.ToString();
                            mStockGroup.LanguageName = dataGridView1.Rows[i].Cells[ColIndex.LangBrand].Value.ToString();
                            mStockGroup.ControlGroup = 3;
                            mStockGroup.IsActive = true;
                            mStockGroup.UserId = DBGetVal.UserID;
                            mStockGroup.UserDate = DBGetVal.ServerTime.Date;
                            mStockGroup.CompanyNo = DBGetVal.CompanyNo;
                            mStockGroup.ControlSubGroup = objQry.ReturnLong("SELECT StockGroupNo From MStockGroup WHERE ControlGroup=2 AND StockGroupName='" + dataGridView1.Rows[i].Cells[ColIndex.Category].Value + "'", CommonFunctions.ConStr);
                            mStockGroup.Margin = 0;
                            dbStockGroup.AddMStockGroup(mStockGroup);
                        }
                    }

                    dbStockItems = new DBMStockItems();
                    mStockItems = new MStockItems();

                    mStockItems.ItemNo = Convert.ToInt64(dataGridView1.Rows[i].Cells[ColIndex.ItemNo].Value);//ID;
                    mStockItems.ItemName = dataGridView1.Rows[i].Cells[ColIndex.ItemName].Value == null || dataGridView1.Rows[i].Cells[ColIndex.ItemName].Value.ToString() == "" ? dt.Rows[i].ItemArray[ColIndex.ItemName].ToString() : dataGridView1.Rows[i].Cells[ColIndex.ItemName].Value.ToString();
                    mStockItems.ItemShortCode = dataGridView1.Rows[i].Cells[ColIndex.ShortDesc].Value == null || dataGridView1.Rows[i].Cells[ColIndex.ShortDesc].Value.ToString() == "" ? dt.Rows[i].ItemArray[ColIndex.ShortDesc].ToString() : dataGridView1.Rows[i].Cells[ColIndex.ShortDesc].Value.ToString();
                    mStockItems.FKStockDeptNo = objQry.ReturnLong("SELECT StockGroupNo From MStockGroup WHERE ControlGroup=4 AND StockGroupName='" + dataGridView1.Rows[i].Cells[ColIndex.Department].Value + "'",CommonFunctions.ConStr);
                    mStockItems.GroupNo1 = objQry.ReturnLong("SELECT StockGroupNo From MStockGroup WHERE ControlGroup=2 AND StockGroupName='" + dataGridView1.Rows[i].Cells[ColIndex.Category].Value + "'", CommonFunctions.ConStr);
                    mStockItems.GroupNo = objQry.ReturnLong("SELECT StockGroupNo From MStockGroup WHERE ControlGroup=3 AND StockGroupName='" + dataGridView1.Rows[i].Cells[ColIndex.BrandName].Value + "'", CommonFunctions.ConStr);
                    mStockItems.FkCategoryNo = 1;
                    //mStockItems.CompanyNo = ObjFunction.GetComboValue(cmbCompanyName);
                    mStockItems.FkDepartmentNo = 1;
                    mStockItems.FKStockLocationNo = 1;// ObjFunction.GetComboValue(cmbStockLocationName);
                    mStockItems.UOMDefault = objQry.ReturnLong("SELECT UOMNo From MUOM WHERE UOMName='" + dataGridView1.Rows[i].Cells[ColIndex.UOM].Value + "'", CommonFunctions.ConStr);
                    mStockItems.UOMPrimary = mStockItems.UOMDefault;
                    mStockItems.CompanyNo = DBGetVal.CompanyNo;
                    mStockItems.MinLevel = dataGridView1.Rows[i].Cells[ColIndex.MinLevel].Value == null || dataGridView1.Rows[i].Cells[ColIndex.MinLevel].Value.ToString() == "" ? Convert.ToInt64(dt.Rows[i].ItemArray[ColIndex.MinLevel]) : Convert.ToInt64(dataGridView1.Rows[i].Cells[ColIndex.MinLevel].Value);
                    mStockItems.MaxLevel = dataGridView1.Rows[i].Cells[ColIndex.MaxLevel].Value == null || dataGridView1.Rows[i].Cells[ColIndex.MaxLevel].Value.ToString() == "" ? Convert.ToInt64(dt.Rows[i].ItemArray[ColIndex.MaxLevel]) : Convert.ToInt64(dataGridView1.Rows[i].Cells[ColIndex.MaxLevel].Value);
                    mStockItems.LangFullDesc = dataGridView1.Rows[i].Cells[ColIndex.LabgFullDesc].Value == null || dataGridView1.Rows[i].Cells[ColIndex.LabgFullDesc].Value.ToString() == "" ? dt.Rows[i].ItemArray[ColIndex.LabgFullDesc].ToString() : dataGridView1.Rows[i].Cells[ColIndex.LabgFullDesc].Value.ToString();
                    mStockItems.LangShortDesc = dataGridView1.Rows[i].Cells[ColIndex.LangShortDesc].Value == null || dataGridView1.Rows[i].Cells[ColIndex.LangShortDesc].Value.ToString() == "" ? dt.Rows[i].ItemArray[ColIndex.LangShortDesc].ToString() : dataGridView1.Rows[i].Cells[ColIndex.LangShortDesc].Value.ToString();
                    //mStockItems.ReOrderLevelQty = Convert.ToDouble(txtReOrderLevelQty.Text);
                    mStockItems.IsActive = true;
                    mStockItems.IsFixedBarcode = false;
                    mStockItems.UserId = DBGetVal.UserID;
                    mStockItems.UserDate = DBGetVal.ServerTime.Date;
                    dbStockItems.AddMStockItems(mStockItems);

                    mStockBarcode = new MStockBarcode();
                    long barcodeno = objQry.ReturnLong("select isNull((PkStockBarcodeNo),0) from MStockBarcode where Barcode='" + dataGridView1.Rows[i].Cells[ColIndex.BarCode].Value + "' AND ItemNo=" + mStockItems.ItemNo + "", CommonFunctions.ConStr);
                    mStockBarcode.PkStockBarcodeNo = barcodeno;
                    mStockBarcode.Barcode = dataGridView1.Rows[i].Cells[ColIndex.BarCode].Value.ToString();
                    mStockBarcode.IsActive = true;
                    mStockBarcode.UserID = DBGetVal.UserID;
                    mStockBarcode.UserDate = DBGetVal.ServerTime.Date;
                    dbStockItems.AddMStockBarcode(mStockBarcode);

                    mRateSetting = new MRateSetting();
                    mRateSetting.PkSrNo = objQry.ReturnLong("select max(PkSrNo) From MRateSetting where itemno= " + Convert.ToInt64(dataGridView1.Rows[i].Cells[ColIndex.ItemNo].Value), CommonFunctions.ConStr);
                    mRateSetting.FromDate = Convert.ToDateTime((DateTime.Now).ToString("dd-MMM-yyyy"));// FromDate;
                    mRateSetting.PurRate = dataGridView1.Rows[i].Cells[ColIndex.PurRate].Value == null || dataGridView1.Rows[i].Cells[ColIndex.PurRate].Value.ToString() == "" ? Convert.ToDouble(dt.Rows[i].ItemArray[ColIndex.PurRate]) : Convert.ToDouble(dataGridView1.Rows[i].Cells[ColIndex.PurRate].Value);
                    mRateSetting.UOMNo = mStockItems.UOMDefault;
                    mRateSetting.ASaleRate = dataGridView1.Rows[i].Cells[ColIndex.ASaleRate].Value == null || dataGridView1.Rows[i].Cells[ColIndex.ASaleRate].Value.ToString() == "" ? Convert.ToDouble(dt.Rows[i].ItemArray[ColIndex.ASaleRate]) : Convert.ToDouble(dataGridView1.Rows[i].Cells[ColIndex.ASaleRate].Value);
                    mRateSetting.BSaleRate = dataGridView1.Rows[i].Cells[ColIndex.BSaleRate].Value == null || dataGridView1.Rows[i].Cells[ColIndex.BSaleRate].Value.ToString() == "" ? Convert.ToDouble(dt.Rows[i].ItemArray[ColIndex.BSaleRate]) : Convert.ToDouble(dataGridView1.Rows[i].Cells[ColIndex.BSaleRate].Value);
                    mRateSetting.CSaleRate = 0; 
                    mRateSetting.DSaleRate = 0; 
                    mRateSetting.ESaleRate = 0; 
                    mRateSetting.StockConversion = 1;
                    mRateSetting.PerOfRateVariation = 1;
                    mRateSetting.MKTQty = 1;
                    mRateSetting.MRP = dataGridView1.Rows[i].Cells[ColIndex.MRP].Value == null || dataGridView1.Rows[i].Cells[ColIndex.MRP].Value.ToString() == "" ? 0 : Convert.ToDouble(dataGridView1.Rows[i].Cells[ColIndex.MRP].Value); 
                    mRateSetting.IsActive = true;
                    mRateSetting.UserID = DBGetVal.UserID;
                    mRateSetting.UserDate = DBGetVal.ServerTime.Date;
                    dbStockItems.AddMRateSetting2(mRateSetting);

                    

                    //VatSale %
                    MItemTaxInfo mItemTaxInfo = new MItemTaxInfo();
                    mItemTaxInfo.PkSrNo = objQry.ReturnLong("SELECT PkSrNo FROM  MItemTaxInfo WHERE   (FKTaxSettingNo=2) AND (ItemNo = " + Convert.ToInt64(dataGridView1.Rows[i].Cells[ColIndex.ItemNo].Value) + ")", CommonFunctions.ConStr);
                    if (mItemTaxInfo.PkSrNo != 0)
                    {
                        DataTable dtItemTaxInfo = ObjFunction.GetDataView("SELECT MItemTaxSetting.TaxLedgerNo, MItemTaxSetting.SalesLedgerNo, MItemTaxSetting.Percentage FROM MItemTaxSetting INNER JOIN " +
                            " MLedger ON MItemTaxSetting.SalesLedgerNo = MLedger.LedgerNo INNER JOIN MLedger AS MLedger_1 ON MItemTaxSetting.TaxLedgerNo = MLedger_1.LedgerNo " +
                            " WHERE     (MLedger.GroupNo = " + GroupType.SalesAccount + ") AND (MLedger_1.GroupNo = 32) AND (MItemTaxSetting.Percentage = " + dataGridView1.Rows[i].Cells[ColIndex.VatSale].Value.ToString() + ")").Table;
                        if (dtItemTaxInfo.Rows.Count > 0)
                        {
                            mItemTaxInfo.TaxLedgerNo = Convert.ToInt64(dtItemTaxInfo.Rows[0].ItemArray[0].ToString());
                            mItemTaxInfo.SalesLedgerNo = Convert.ToInt64(dtItemTaxInfo.Rows[0].ItemArray[1].ToString());
                            mItemTaxInfo.FromDate = DBGetVal.ServerTime.Date;
                            mItemTaxInfo.CalculationMethod = "2";
                            mItemTaxInfo.Percentage = Convert.ToDouble(dtItemTaxInfo.Rows[0].ItemArray[2].ToString());
                            mItemTaxInfo.FKTaxSettingNo = 2;
                            mItemTaxInfo.UserID = DBGetVal.UserID;
                            mItemTaxInfo.UserDate = DBGetVal.ServerTime.Date;
                            dbStockItems.AddMItemTaxInfo(mItemTaxInfo);
                        }
                    }

                    //VatPurchase %
                    mItemTaxInfo = new MItemTaxInfo();
                    mItemTaxInfo.PkSrNo = objQry.ReturnLong("SELECT PkSrNo FROM  MItemTaxInfo WHERE   (FKTaxSettingNo=1) AND (ItemNo = " + Convert.ToInt64(dataGridView1.Rows[i].Cells[ColIndex.ItemNo].Value) + ")", CommonFunctions.ConStr);
                    if (mItemTaxInfo.PkSrNo != 0)
                    {
                        DataTable dtItemTaxInfo = ObjFunction.GetDataView("SELECT MItemTaxSetting.TaxLedgerNo, MItemTaxSetting.SalesLedgerNo, MItemTaxSetting.Percentage FROM MItemTaxSetting INNER JOIN " +
                            " MLedger ON MItemTaxSetting.SalesLedgerNo = MLedger.LedgerNo INNER JOIN MLedger AS MLedger_1 ON MItemTaxSetting.TaxLedgerNo = MLedger_1.LedgerNo " +
                            " WHERE     (MLedger.GroupNo = " + GroupType.PurchaseAccount + ") AND (MLedger_1.GroupNo = 32) AND (MItemTaxSetting.Percentage = " + dataGridView1.Rows[i].Cells[ColIndex.VatPur].Value.ToString() + ")").Table;
                        if (dtItemTaxInfo.Rows.Count > 0)
                        {
                            mItemTaxInfo.TaxLedgerNo = Convert.ToInt64(dtItemTaxInfo.Rows[0].ItemArray[0].ToString());
                            mItemTaxInfo.SalesLedgerNo = Convert.ToInt64(dtItemTaxInfo.Rows[0].ItemArray[1].ToString());
                            mItemTaxInfo.FromDate = DBGetVal.ServerTime.Date;
                            mItemTaxInfo.CalculationMethod = "2";
                            mItemTaxInfo.Percentage = Convert.ToDouble(dtItemTaxInfo.Rows[0].ItemArray[2].ToString());
                            mItemTaxInfo.FKTaxSettingNo = 1;
                            mItemTaxInfo.UserID = DBGetVal.UserID;
                            mItemTaxInfo.UserDate = DBGetVal.ServerTime.Date;
                            dbStockItems.AddMItemTaxInfo(mItemTaxInfo);
                        }
                    }

                    dbStockItems.ExecuteNonQueryStatements();


                    count++;
                }
            }
            OMMessageBox.Show(count + " Item's Updated  Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
        }

        private static class ColIndex
        {
            public static int ItemNo = 0;
            public static int BarCode = 1;
            public static int Department = 2;
            public static int Category = 3;
            public static int ItemName = 4;
            public static int ShortDesc = 5;
            public static int LabgFullDesc = 6;
            public static int LangShortDesc = 7;
            public static int BrandName = 8;
            public static int LangBrand = 9;
            public static int UOM = 10;
            public static int MRP = 11;
            public static int PurRate = 12;
            public static int ASaleRate = 13;
            public static int BSaleRate = 14;
            public static int MinLevel = 15;
            public static int MaxLevel = 16;
            public static int VatSale = 17;
            public static int VatPur = 18;
        }
    }
}
