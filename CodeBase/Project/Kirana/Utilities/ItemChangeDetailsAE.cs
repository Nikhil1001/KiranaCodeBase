﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Utilities
{
    /// <summary>
    /// This class is used for Item Change Details
    /// </summary>
    public partial class ItemChangeDetailsAE : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();
        DBMStockItems dbMStockItems = new DBMStockItems();
        int rowindex;

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public ItemChangeDetailsAE()
        {
            InitializeComponent();
        }

        private void ItemChangeDetailsAE_Load(object sender, EventArgs e)
        {
            //cmbDepartment.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_DepartmentDisplay));
            //lblDepartment.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_DepartmentDisplay));
            cmbCategory.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_CategoryDisplay));
            lblCategory.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_CategoryDisplay));
            lstBoxMain.Visible = false;
            lstBoxBrand.Visible = false;
            lstBoxUomDef.Visible = false;
            lstBoxUomPri.Visible = false;
            lstCategory.Visible = false;
            lstDepartment.Visible = false;

            ObjFunction.FillCombo(cmbDepartment, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=4 ORDER BY StockGroupName");
            ObjFunction.FillList(lstBoxMain, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=2 ORDER BY StockGroupName");
            ObjFunction.FillList(lstBoxBrand, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=3 ORDER BY StockGroupName");

            ObjFunction.FillCombo(cmbMainGroup, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=2 AND ControlSubGroup=0 ORDER BY StockGroupName");
            ObjFunction.FillCombo(cmbBrandName, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=3 ORDER BY StockGroupName");
            ObjFunction.FillCombo(cmbItemName, "SELECT Distinct ItemNo,ItemName From MStockItems ORDER BY ItemName");
            //ObjFunction.FillCombo(cmbDepartment, "SELECT DepartmentNo, DepartmentName FROM MStockDepartment WHERE IsActive = 'True' ORDER BY DepartmentName");
            ObjFunction.FillCombo(cmbCategory, "SELECT CategoryNo, CategoryName FROM MStockCategory WHERE IsActive = 'True' ORDER BY CategoryName");
            //ObjFunction.FillCombo(cmbCategory, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=2 AND ControlSubGroup=0 ORDER BY StockGroupName");

            ObjFunction.FillCombo(cmbTMainGroup, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=2 ORDER BY StockGroupName");
            ObjFunction.FillCombo(cmbTBrandName, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=3 ORDER BY StockGroupName");
            ObjFunction.FillCombo(cmbTDepart, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=4 ORDER BY StockGroupName");
            ObjFunction.FillCombo(cmbTCategory, "SELECT CategoryNo, CategoryName FROM MStockCategory WHERE IsActive = 'True' ORDER BY CategoryName");

            cmbDepartment.Focus();
            txtTotProducts.Text = "0";
            btnCancel.Visible = true;
            KeyDownFormat(this.Controls);
            dgItemChange.Columns[ColIndex.SrNo].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dgItemChange.Columns[ColIndex.chk].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            
            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_Bilingual)) == true)
            {
                dgItemChange.Columns[ColIndex.LangFullDesc].Visible = true;
                dgItemChange.Columns[ColIndex.LangFullDesc].DefaultCellStyle.Font = ObjFunction.GetLangFont();
            }
            else
                dgItemChange.Columns[ColIndex.LangFullDesc].Visible = false;

            ShowColumns();
        }


        private void ShowColumns()
        {
            dgItemChange.Columns[ColIndex.SrNo].DisplayIndex = 0;
            dgItemChange.Columns[ColIndex.DeptName].DisplayIndex = 1;
            dgItemChange.Columns[ColIndex.CategoryName].DisplayIndex = 2;
            dgItemChange.Columns[ColIndex.BrandName].DisplayIndex = 3;
            dgItemChange.Columns[ColIndex.ItemName].DisplayIndex = 5;
            dgItemChange.Columns[ColIndex.LangFullDesc].DisplayIndex = 6;
            dgItemChange.Columns[ColIndex.Barcode].DisplayIndex = 7;
            dgItemChange.Columns[ColIndex.UomDefault].DisplayIndex = 8;
            dgItemChange.Columns[ColIndex.UomPrimary].DisplayIndex = 9;
            dgItemChange.Columns[ColIndex.Status].DisplayIndex = 10;
            dgItemChange.Columns[ColIndex.Company].DisplayIndex = 11;
            
        }

        private void BindGrid()
        {

            string sql = " SELECT 0 AS SrNo, MStockGroup_2.StockGroupName AS DepartmentName,MStockGroup.StockGroupName AS MainGroupName, MStockGroup_1.StockGroupName AS BrandName,MStockItems.ItemName,  MStockBarcode.Barcode, " +
                          " UOMDefault.UOMName AS UOMDefault, UOMPrimary.UOMName AS UOMLower, MStockCategory.CategoryName,  MStockItems.IsActive, MCompany.CompanyName, MStockItems.IsFixedBarcode, 'False' AS SelectAll, " +
                          " MStockItems.ItemNo, MStockBarcode.PkStockBarcodeNo, MStockItems.UOMDefault AS UOMDefaultNo,MStockItems.UOMPrimary AS UOMPrimaryNo, " +
                          " MStockItems.GroupNo1, MStockItems.GroupNo, UOMDefault.UOMNo,MStockItems.UOMPrimary AS UOMPrimary, MStockCategory.CategoryNo, MStockItems.FKStockDeptNo,MStockItems.LangFullDesc  " +
                          " FROM  MStockBarcode INNER JOIN " +
                          " MStockItems ON MStockBarcode.ItemNo = MStockItems.ItemNo INNER JOIN " +
                          " MUOM AS UOMPrimary ON MStockItems.UOMPrimary = UOMPrimary.UOMNo INNER JOIN " +
                          " MUOM AS UOMDefault ON MStockItems.UOMDefault = UOMDefault.UOMNo INNER JOIN " +
                          " MStockGroup ON MStockItems.GroupNo1 = MStockGroup.StockGroupNo INNER JOIN " +
                          " MStockGroup AS MStockGroup_1 ON MStockItems.GroupNo = MStockGroup_1.StockGroupNo INNER JOIN " +
                          " MStockDepartment ON MStockItems.FkDepartmentNo = MStockDepartment.DepartmentNo INNER JOIN " +
                          " MStockCategory ON MStockItems.FkCategoryNo = MStockCategory.CategoryNo INNER JOIN " +
                          " MCompany ON MStockItems.CompanyNo = MCompany.CompanyNo  INNER JOIN " +
                          " MStockGroup AS MStockGroup_2 ON MStockItems.FKStockDeptNo = MStockGroup_2.StockGroupNo ";
            string strclause = " WHERE ";
            string strWhere = "";
            if (ObjFunction.GetComboValue(cmbMainGroup) != 0)
            {
                strWhere = "or MStockItems.GroupNo1 = " + ObjFunction.GetComboValue(cmbMainGroup) + " ";
            }
            if (ObjFunction.GetComboValue(cmbBrandName) != 0)
            {
                strWhere += "or MStockItems.GroupNo = " + ObjFunction.GetComboValue(cmbBrandName) + " ";
            }
            if (ObjFunction.GetComboValue(cmbItemName) != 0)
            {
                strWhere += "or MStockItems.ItemNo = " + ObjFunction.GetComboValue(cmbItemName) + " ";
            }
            if (ObjFunction.GetComboValue(cmbDepartment) != 0)
            {
                if (ObjFunction.GetComboValue(cmbMainGroup) != 0)
                    strWhere += "AND MStockItems.FKStockDeptNo = " + ObjFunction.GetComboValue(cmbDepartment) + " ";
                else
                    strWhere += "or MStockItems.FKStockDeptNo = " + ObjFunction.GetComboValue(cmbDepartment) + " ";
            }
            if (ObjFunction.GetComboValue(cmbCategory) != 0)
            {
                strWhere += "or MStockItems.FkCategoryNo = " + ObjFunction.GetComboValue(cmbCategory) + " ";
            }
            if (strWhere != "")
            {
                strWhere = strWhere.Substring(2, strWhere.Length - 2);
                sql += strclause + strWhere;
            }
            sql += " ORDER BY MStockBarcode.Barcode, MStockItems.ItemName, MainGroupName, BrandName ";
            DataTable dt = ObjFunction.GetDataView(sql).Table;

            if (dt.Rows.Count > 0)
            {

                dgItemChange.DataSource = dt.DefaultView;
                for (int i = 0; i < dgItemChange.Rows.Count; i++)
                {
                    dgItemChange.Rows[i].Cells[ColIndex.chk].Value = false;
                }
            }
            else
            {
                DisplayMessage("Records not found...!!");
                while (dgItemChange.Rows.Count > 0)
                {
                    dgItemChange.Rows.RemoveAt(0);
                }
            }
            dgItemChange.Columns[ColIndex.LangFullDesc].DefaultCellStyle.Font = ObjFunction.GetLangFont();

        }

        private void CalculateTotProducts()
        {
            txtTotProducts.Text = Convert.ToString(dgItemChange.Rows.Count);
        }

        private void cmbMainGroup_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (cmbMainGroup.SelectedIndex > 0)
                {
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_IsBrandFilter)) == true)
                        ObjFunction.FillCombo(cmbBrandName, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=3 AND ControlSubGroup=" + ObjFunction.GetComboValue(cmbMainGroup) + " ORDER BY StockGroupName");
                    else
                        ObjFunction.FillCombo(cmbBrandName, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=3 ORDER BY StockGroupName");
                    BindGrid();
                    CalculateTotProducts();
                }
                else
                    changeValue();
            }
        }

        private void changeValue()
        {
            if (cmbMainGroup.SelectedIndex == 0 && cmbBrandName.SelectedIndex == 0 && cmbItemName.SelectedIndex == 0 && cmbCategory.SelectedIndex == 0 && cmbDepartment.SelectedIndex == 0)
            {
                while (dgItemChange.Rows.Count > 0)
                {
                    dgItemChange.Rows.RemoveAt(0);
                }
                CalculateTotProducts();
            }
            else
            {
                BindGrid();
                CalculateTotProducts();
            }
        }

        private void cmbBrandName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (cmbBrandName.SelectedIndex > 0)
                {
                    BindGrid();
                    CalculateTotProducts();
                }
                else
                    changeValue();
            }
        }

        private void cmbItemName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (cmbItemName.SelectedIndex > 0)
                {
                    BindGrid();
                    CalculateTotProducts();
                }
                else
                    changeValue();
            }
        }

        private void cmbDepartment_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (cmbDepartment.SelectedIndex > 0)
                {
                    ObjFunction.FillCombo(cmbMainGroup, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=2 AND ControlSubGroup="+ ObjFunction.GetComboValue(cmbDepartment) +" ORDER BY StockGroupName");
                    BindGrid();
                    CalculateTotProducts();
                }
                else
                    changeValue();
            }
        }

        private void DisplayMessage(string str)
        {
            lblMsg.Visible = true;
            lblMsg.Text = str;
            Application.DoEvents();
            System.Threading.Thread.Sleep(1200);
            lblMsg.Visible = false;
        }

        private void dgItemChange_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                e.Value = (e.RowIndex + 1).ToString();

            }
        }

        private bool Validations()
        {
            bool flag = false;
            bool fl = false;
            for (int i = 0; i < dgItemChange.Rows.Count; i++)
            {
                if (Convert.ToBoolean(dgItemChange.Rows[i].Cells[ColIndex.chk].FormattedValue) == true)
                {
                    fl = true;
                    break;
                }
            }
            if (fl == false)
            {
                DisplayMessage("Please Select Atleast One Item");
            }
            flag = fl;
            return flag;
        }

        private void btnApplyChanges_Click(object sender, EventArgs e)
        {
            if (Validations() == true)
            {
                for (int i = 0; i < dgItemChange.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(dgItemChange.Rows[i].Cells[ColIndex.chk].FormattedValue) == true)
                    {
                        MStockItems mStockItems = new MStockItems();
                        mStockItems.ItemNo = Convert.ToInt64(dgItemChange.Rows[i].Cells[ColIndex.ItemNo].Value);
                        mStockItems.ItemName = Convert.ToString(dgItemChange.Rows[i].Cells[ColIndex.ItemName].Value);
                        mStockItems.GroupNo = Convert.ToInt64(dgItemChange.Rows[i].Cells[ColIndex.BrandId].Value);
                        mStockItems.GroupNo1 = Convert.ToInt64(dgItemChange.Rows[i].Cells[ColIndex.MainGroupID].Value);
                        mStockItems.UOMDefault = Convert.ToInt64(dgItemChange.Rows[i].Cells[ColIndex.UOMNO].Value);
                        mStockItems.UOMPrimary = Convert.ToInt64(dgItemChange.Rows[i].Cells[ColIndex.UOMPrimaryNo].Value);
                        mStockItems.IsActive = Convert.ToBoolean(dgItemChange.Rows[i].Cells[ColIndex.Status].Value);
                        mStockItems.IsFixedBarcode = Convert.ToBoolean(dgItemChange.Rows[i].Cells[ColIndex.FixedBarcode].Value);
                        mStockItems.FkCategoryNo = Convert.ToInt64(dgItemChange.Rows[i].Cells[ColIndex.CategoryNo].Value);
                        mStockItems.FKStockDeptNo = Convert.ToInt64(dgItemChange.Rows[i].Cells[ColIndex.DeptNo].Value);
                        mStockItems.LangFullDesc = dgItemChange.Rows[i].Cells[ColIndex.LangFullDesc].Value.ToString();
                        mStockItems.LangShortDesc = "";
                        dbMStockItems.UpdateStockItems(mStockItems);

                        MStockBarcode mStockBarcode = new MStockBarcode();
                        mStockBarcode.PkStockBarcodeNo = Convert.ToInt64(dgItemChange.Rows[i].Cells[ColIndex.StockBarcodeNo].Value);
                        mStockBarcode.Barcode = Convert.ToString(dgItemChange.Rows[i].Cells[ColIndex.Barcode].Value);
                        dbMStockItems.UpdateStockBarcode(mStockBarcode);

                        MRateSetting mRateSetting = new MRateSetting();
                        mRateSetting.ItemNo = Convert.ToInt64(dgItemChange.Rows[i].Cells[ColIndex.ItemNo].Value);
                        mRateSetting.UOMNo = Convert.ToInt64(dgItemChange.Rows[i].Cells[ColIndex.UOMNO].Value);
                        dbMStockItems.UpdateMRateSettingUom(mRateSetting);



                    }
                }
                if (dbMStockItems.ExecuteNonQueryStatements() == true)
                {
                    DisplayMessage("Item Save Successfully...");
                    ChkSelect.Checked = false;
                    btnCancel_Click(sender, e);
                }
            }
        }

        #region KeyDown Events
        private void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is GroupBox)
                    KeyDownFormat(ctrl.Controls);
            }
        }

        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F7)
            {
                ChkSelect.Checked = !ChkSelect.Checked;
                ChkSelect_CheckedChanged(sender, e);
            }


        }
        #endregion

        private void ChkSelect_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < dgItemChange.Rows.Count; i++)
            {
                dgItemChange.Rows[i].Cells[ColIndex.chk].Value = ChkSelect.Checked;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ChkSelect.Checked = false;
            while (dgItemChange.Rows.Count > 0)
            {
                dgItemChange.Rows.RemoveAt(0);
            }
            cmbItemName.SelectedIndex = 0;
            cmbMainGroup.SelectedIndex = 0;
            cmbBrandName.SelectedIndex = 0;
            cmbDepartment.SelectedIndex = 0;
            cmbCategory.SelectedIndex = 0;
            cmbTBrandName.SelectedIndex = 0;
            cmbTCategory.SelectedIndex = 0;
            cmbTDepart.SelectedIndex = 0;
            cmbTMainGroup.SelectedIndex = 0;
            cmbDepartment.Focus();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgItemChange_KeyDown(object sender, KeyEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            int po_X = dgv.GetCellDisplayRectangle(dgv.CurrentCell.ColumnIndex, dgv.CurrentRow.Index, false).Left + dgv.Left;
            int po_Y = dgv.GetCellDisplayRectangle(dgv.CurrentCell.ColumnIndex, dgv.CurrentRow.Index, false).Bottom + dgv.Top;

            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;

                if (dgv.CurrentCell.ColumnIndex == ColIndex.MainGroup)
                {
                    lstBoxMain.Location = new System.Drawing.Point(po_X, po_Y);
                    lstBoxMain.Visible = true;
                    lstBoxMain.Focus();
                }
                if (dgv.CurrentCell.ColumnIndex == ColIndex.BrandName)
                {
                    lstBoxBrand.Location = new System.Drawing.Point(po_X, po_Y);
                    lstBoxBrand.Visible = true;
                    lstBoxBrand.Focus();
                }
                if (dgv.CurrentCell.ColumnIndex == ColIndex.UomDefault)
                {
                    lstBoxUomDef.Location = new System.Drawing.Point(po_X, po_Y);
                    ObjFunction.FillList(lstBoxUomDef, "SELECT DISTINCT MUOM.UOMNo, MUOM.UOMName FROM  MUOM INNER JOIN MRateSetting ON MUOM.UOMNo = MRateSetting.UOMNo where MUOM.IsActive='true' ORDER BY MUOM.UOMName");//WHERE (MRateSetting.ItemNo =" + Convert.ToInt64(dgv.CurrentRow.Cells[ColIndex.ItemNo].Value) + ") ORDER BY MUOM.UOMName");
                    lstBoxUomDef.Visible = true;
                    lstBoxUomDef.Focus();
                }
                if (dgv.CurrentCell.ColumnIndex == ColIndex.UomPrimary)
                {
                    lstBoxUomPri.Location = new System.Drawing.Point(po_X, po_Y);
                    ObjFunction.FillList(lstBoxUomPri, "SELECT DISTINCT MUOM.UOMNo, MUOM.UOMName FROM  MUOM INNER JOIN MRateSetting ON MUOM.UOMNo = MRateSetting.UOMNo where MUOM.IsActive='true' ORDER BY MUOM.UOMName");//WHERE (MRateSetting.ItemNo =" + Convert.ToInt64(dgv.CurrentRow.Cells[ColIndex.ItemNo].Value) + ") ORDER BY MUOM.UOMName");
                    lstBoxUomPri.Visible = true;
                    lstBoxUomPri.Focus();
                }
                if (dgv.CurrentCell.ColumnIndex == ColIndex.CategoryNo)
                {
                    lstCategory.Location = new System.Drawing.Point(po_X, po_Y);
                    ObjFunction.FillList(lstCategory, "SELECT CategoryNo, CategoryName FROM MStockCategory WHERE IsActive = 'True' ORDER BY CategoryName");
                    lstCategory.Visible = true;
                    lstCategory.Focus();
                }
                if (dgv.CurrentCell.ColumnIndex == ColIndex.DeptName)
                {
                    lstDepartment.Location = new System.Drawing.Point(po_X, po_Y);
                    ObjFunction.FillList(lstDepartment, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=4 ORDER BY StockGroupName");
                    lstDepartment.Visible = true;
                    lstDepartment.Focus();
                }
            }
            if (e.KeyCode == Keys.Escape)
            {
                btnApplyChanges.Focus();
            }
        }

        private void lstBoxMain_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                rowindex = dgItemChange.CurrentCell.RowIndex;
                dgItemChange.Rows[rowindex].Cells[ColIndex.MainGroup].Value = lstBoxMain.Text;
                dgItemChange.Rows[rowindex].Cells[ColIndex.MainGroupID].Value = lstBoxMain.SelectedValue;
                lstBoxMain.Visible = false;
                dgItemChange.Focus();
                dgItemChange.CurrentCell = dgItemChange[ColIndex.BrandName, rowindex];
            }
            if (e.KeyCode == Keys.Escape)
            {
                lstBoxMain.Visible = false;
                dgItemChange.Focus();
            }
        }

        private void lstBoxBrand_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                rowindex = dgItemChange.CurrentCell.RowIndex;
                dgItemChange.Rows[rowindex].Cells[ColIndex.BrandName].Value = lstBoxBrand.Text;
                dgItemChange.Rows[rowindex].Cells[ColIndex.BrandId].Value = lstBoxBrand.SelectedValue;
                lstBoxBrand.Visible = false;
                dgItemChange.Focus();
                dgItemChange.CurrentCell = dgItemChange[ColIndex.ItemName, rowindex];
            }
            if (e.KeyCode == Keys.Escape)
            {
                lstBoxBrand.Visible = false;
                dgItemChange.Focus();
            }
        }

        private void lstBoxUomDef_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                rowindex = dgItemChange.CurrentCell.RowIndex;
                dgItemChange.Rows[rowindex].Cells[ColIndex.UomDefault].Value = lstBoxUomDef.Text;
                dgItemChange.Rows[rowindex].Cells[ColIndex.UOMNO].Value = lstBoxUomDef.SelectedValue;
                lstBoxUomDef.Visible = false;
                dgItemChange.Focus();
                dgItemChange.CurrentCell = dgItemChange[ColIndex.UomPrimary, rowindex];
            }
            if (e.KeyCode == Keys.Escape)
            {
                lstBoxUomDef.Visible = false;
                dgItemChange.Focus();
            }
        }

        private void lstBoxUomPri_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                rowindex = dgItemChange.CurrentCell.RowIndex;
                dgItemChange.Rows[rowindex].Cells[ColIndex.UomPrimary].Value = lstBoxUomPri.Text;
                dgItemChange.Rows[rowindex].Cells[ColIndex.UOMPrimaryNo].Value = lstBoxUomPri.SelectedValue;
                lstBoxUomPri.Visible = false;
                dgItemChange.Focus();
                dgItemChange.CurrentCell = dgItemChange[ColIndex.Status, rowindex];
            }
            if (e.KeyCode == Keys.Escape)
            {
                lstBoxUomPri.Visible = false;
                dgItemChange.Focus();
            }
        }

        private void cmbTMainGroup_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (dgItemChange.Rows.Count > 0)
                {
                    if (cmbTMainGroup.SelectedIndex > 0)
                        ChangeGridValue(ColIndex.MainGroup, ColIndex.MainGroupID, cmbTMainGroup);
                }
            }
        }

        private void ChangeGridValue(int ColIndex, int ColIndexValue, ComboBox cmb)
        {
            for (int i = 0; i < dgItemChange.Rows.Count; i++)
            {
                dgItemChange.Rows[i].Cells[ColIndex].Value = cmb.Text;
                dgItemChange.Rows[i].Cells[ColIndexValue].Value = ObjFunction.GetComboValue(cmb);
            }
        }

        private void cmbTBrandName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (dgItemChange.Rows.Count > 0)
                {
                    if (cmbTBrandName.SelectedIndex > 0)
                        ChangeGridValue(ColIndex.BrandName, ColIndex.BrandId, cmbTBrandName);
                }
            }
        }

        private void cmbTDepart_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (dgItemChange.Rows.Count > 0)
                {
                    if (cmbTDepart.SelectedIndex > 0)
                        ChangeGridValue(ColIndex.DeptName, ColIndex.DeptNo, cmbTDepart);
                }
            }
        }

        private void cmbTCategory_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (dgItemChange.Rows.Count > 0)
                {
                    if (cmbTCategory.SelectedIndex > 0)
                        ChangeGridValue(ColIndex.CategoryName, ColIndex.CategoryNo, cmbTCategory);
                }
            }
        }

        private void cmbCategory_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (cmbCategory.SelectedIndex > 0)
                {
                    BindGrid();
                    CalculateTotProducts();
                }
                else
                    changeValue();
            }
        }

        private void lstCategory_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                rowindex = dgItemChange.CurrentCell.RowIndex;
                dgItemChange.Rows[rowindex].Cells[ColIndex.CategoryName].Value = lstCategory.Text;
                dgItemChange.Rows[rowindex].Cells[ColIndex.CategoryNo].Value = lstCategory.SelectedValue;
                lstCategory.Visible = false;
                dgItemChange.Focus();
                dgItemChange.CurrentCell = dgItemChange[8, rowindex];
            }
            if (e.KeyCode == Keys.Escape)
            {
                lstCategory.Visible = false;
                dgItemChange.Focus();
            }
        }

        private static class ColIndex
        {
            public static int SrNo = 0;
            public static int DeptName = 1;
            public static int MainGroup = 2;
            public static int BrandName = 3;
            public static int ItemName = 4;
            public static int Barcode = 5;
            public static int UomDefault = 6;
            public static int UomPrimary = 7;
            public static int CategoryName = 8;
            public static int Status = 9;
            public static int Company = 10;
            public static int FixedBarcode = 11;
            public static int chk = 12;
            public static int ItemNo = 13;
            public static int StockBarcodeNo = 14;
            public static int UomDefaultNo = 15;
            public static int UomPriNo = 16;
            public static int MainGroupID = 17;
            public static int BrandId = 18;
            public static int UOMNO = 19;
            public static int UOMPrimaryNo = 20;
            public static int CategoryNo = 21;
            public static int DeptNo = 22;
            public static int LangFullDesc = 23;
        }

        private void lstDepartment_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                rowindex = dgItemChange.CurrentCell.RowIndex;
                dgItemChange.Rows[rowindex].Cells[ColIndex.DeptName].Value = lstDepartment.Text;
                dgItemChange.Rows[rowindex].Cells[ColIndex.DeptNo].Value = lstDepartment.SelectedValue;
                lstDepartment.Visible = false;
                dgItemChange.Focus();
                dgItemChange.CurrentCell = dgItemChange[2, rowindex];
            }
            if (e.KeyCode == Keys.Escape)
            {
                lstDepartment.Visible = false;
                dgItemChange.Focus();
            }
        }


        private void dgItemChange_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (dgItemChange.Rows.Count > 0)
            {
                if (e.ColumnIndex == ColIndex.BrandName || e.ColumnIndex == ColIndex.MainGroup || e.ColumnIndex == ColIndex.Barcode || e.ColumnIndex == ColIndex.CategoryName || e.ColumnIndex == ColIndex.DeptName || e.ColumnIndex == ColIndex.FixedBarcode || e.ColumnIndex == ColIndex.ItemName || e.ColumnIndex == ColIndex.LangFullDesc
                    || e.ColumnIndex == ColIndex.UomDefault || e.ColumnIndex == ColIndex.UomPrimary)
                    dgItemChange.Rows[dgItemChange.CurrentCell.RowIndex].Cells[ColIndex.chk].Value = true;
                else if (e.ColumnIndex == ColIndex.Status)
                    dgItemChange.Rows[dgItemChange.CurrentCell.RowIndex].Cells[ColIndex.chk].Value = true;
            }
        }

        private void dgItemChange_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (dgItemChange.Rows.Count > 0)
            {
                if (e.ColumnIndex == ColIndex.Status || e.ColumnIndex == ColIndex.FixedBarcode)
                    dgItemChange.Rows[e.RowIndex].Cells[ColIndex.chk].Value = true;
            }
        }

        private void btnItem_Click(object sender, EventArgs e)
        {
            string sql = "Select ItemNo,ItemName,ItemShortCode,LangFullDesc,LangShortDesc From MStockItems order by ItemName";
            DataTable dt = new DataTable();

            dt = ObjFunction.GetDataView(sql).Table;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string LangFullDesc = "", LangShortDesc = "";
                Utilities.KeyBoard frmkb;

                //LangFullDesc
                if (Convert.IsDBNull(dt.Rows[i][1]) == false && dt.Rows[i][1].ToString().Trim().Length > 0 && (Convert.IsDBNull(dt.Rows[i][3]) == true || dt.Rows[i][3].ToString().Trim().Length == 0))
                {
                    frmkb = new Utilities.KeyBoard(4, dt.Rows[i][1].ToString(), dt.Rows[i][3].ToString(), "", "");
                    ObjFunction.OpenForm(frmkb);
                    if (frmkb.DS == DialogResult.OK)
                    {
                        LangFullDesc = frmkb.strLanguage.Trim();
                        frmkb.Close();
                    }
                }
                //LangShortDesc
                if (Convert.IsDBNull(dt.Rows[i][2]) == false && dt.Rows[i][2].ToString().Trim().Length > 0 && (Convert.IsDBNull(dt.Rows[i][4]) == true || dt.Rows[i][4].ToString().Trim().Length == 0))
                {
                    frmkb = new Utilities.KeyBoard(4, dt.Rows[i][2].ToString(), dt.Rows[i][4].ToString(), "", "");
                    ObjFunction.OpenForm(frmkb);
                    if (frmkb.DS == DialogResult.OK)
                    {
                        LangShortDesc = frmkb.strLanguage.Trim();
                        frmkb.Close();
                    }
                }

                if (LangFullDesc.Trim().Length > 0 || LangShortDesc.Trim().Length > 0)
                {
                    ObjTrans.Execute("Update MstockItems Set LangFullDesc='" + LangFullDesc.Trim().Replace("'", "''") + "',LangShortDesc='" + LangShortDesc.Trim().Replace("'", "''") + "' where Itemno=" + dt.Rows[i][0].ToString() + "", CommonFunctions.ConStr);
                }
            }
        }

        private void btnBrand_Click(object sender, EventArgs e)
        {
            string sql = "Select StockGroupNo,StockGroupName,LanguageName From MStockGRoup Where ControlGroup=3 order by StockGroupName";
            DataTable dt = new DataTable();

            dt = ObjFunction.GetDataView(sql).Table;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string LangFullDesc = "";
                Utilities.KeyBoard frmkb;

                //LangFullDesc
                if (Convert.IsDBNull(dt.Rows[i][1]) == false && dt.Rows[i][1].ToString().Trim().Length > 0 && (Convert.IsDBNull(dt.Rows[i][2]) == true || dt.Rows[i][2].ToString().Trim().Length == 0))
                {
                    frmkb = new Utilities.KeyBoard(4, dt.Rows[i][1].ToString(), dt.Rows[i][2].ToString(), "", "");
                    ObjFunction.OpenForm(frmkb);
                    if (frmkb.DS == DialogResult.OK)
                    {
                        LangFullDesc = frmkb.strLanguage.Trim();
                        frmkb.Close();
                    }
                }
               

                if (LangFullDesc.Trim().Length > 0 )
                {
                    ObjTrans.Execute("Update MStockGroup Set LanguageName='" + LangFullDesc.Trim().Replace("'", "''") + "' where StockGroupNo=" + dt.Rows[i][0].ToString() + "", CommonFunctions.ConStr);
                }
            }
        }

        
    }
}
