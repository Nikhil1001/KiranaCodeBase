﻿namespace Kirana.Utilities
{
    partial class ExportStockItems
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExportItems = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnImportItems = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.OF = new System.Windows.Forms.OpenFileDialog();
            this.rbItemList = new System.Windows.Forms.RadioButton();
            this.rbStockList = new System.Windows.Forms.RadioButton();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnExportItems
            // 
            this.btnExportItems.Location = new System.Drawing.Point(15, 18);
            this.btnExportItems.Name = "btnExportItems";
            this.btnExportItems.Size = new System.Drawing.Size(88, 60);
            this.btnExportItems.TabIndex = 2;
            this.btnExportItems.Text = "Export Items";
            this.btnExportItems.UseVisualStyleBackColor = true;
            this.btnExportItems.Click += new System.EventHandler(this.btnExportItems_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(254, 18);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(76, 60);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnImportItems
            // 
            this.btnImportItems.Location = new System.Drawing.Point(126, 18);
            this.btnImportItems.Name = "btnImportItems";
            this.btnImportItems.Size = new System.Drawing.Size(109, 60);
            this.btnImportItems.TabIndex = 4;
            this.btnImportItems.Text = "Import Item List";
            this.btnImportItems.UseVisualStyleBackColor = true;
            this.btnImportItems.Click += new System.EventHandler(this.btnImportItems_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnBrowse);
            this.panel1.Controls.Add(this.txtPath);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(367, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(393, 59);
            this.panel1.TabIndex = 5;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(275, 11);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 2;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(56, 15);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(196, 20);
            this.txtPath.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Path :";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(15, 111);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(843, 336);
            this.dataGridView1.TabIndex = 6;
            this.dataGridView1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView1_CellFormatting);
            // 
            // pnlMain
            // 
            this.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMain.Controls.Add(this.rbStockList);
            this.pnlMain.Controls.Add(this.rbItemList);
            this.pnlMain.Controls.Add(this.btnSave);
            this.pnlMain.Controls.Add(this.btnExportItems);
            this.pnlMain.Controls.Add(this.dataGridView1);
            this.pnlMain.Controls.Add(this.btnExit);
            this.pnlMain.Controls.Add(this.panel1);
            this.pnlMain.Controls.Add(this.btnImportItems);
            this.pnlMain.Location = new System.Drawing.Point(12, 12);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(877, 525);
            this.pnlMain.TabIndex = 7;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(15, 450);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(76, 60);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // OF
            // 
            this.OF.Filter = "*.xsl|OM|*.xslx|*.xslx";
            this.OF.InitialDirectory = "C:\\";
            // 
            // rbItemList
            // 
            this.rbItemList.AutoSize = true;
            this.rbItemList.Location = new System.Drawing.Point(15, 85);
            this.rbItemList.Name = "rbItemList";
            this.rbItemList.Size = new System.Drawing.Size(61, 17);
            this.rbItemList.TabIndex = 8;
            this.rbItemList.TabStop = true;
            this.rbItemList.Text = "ItmeList";
            this.rbItemList.UseVisualStyleBackColor = true;
            // 
            // rbStockList
            // 
            this.rbStockList.AutoSize = true;
            this.rbStockList.Location = new System.Drawing.Point(126, 85);
            this.rbStockList.Name = "rbStockList";
            this.rbStockList.Size = new System.Drawing.Size(69, 17);
            this.rbStockList.TabIndex = 9;
            this.rbStockList.TabStop = true;
            this.rbStockList.Text = "StockList";
            this.rbStockList.UseVisualStyleBackColor = true;
            // 
            // ExportStockItems
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(901, 563);
            this.Controls.Add(this.pnlMain);
            this.Name = "ExportStockItems";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Export Stock Items";
            this.Load += new System.EventHandler(this.ExportStockItems_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnExportItems;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnImportItems;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.OpenFileDialog OF;
        private System.Windows.Forms.RadioButton rbStockList;
        private System.Windows.Forms.RadioButton rbItemList;
    }
}