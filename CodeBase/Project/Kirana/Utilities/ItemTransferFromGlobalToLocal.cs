﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using OM;
using JitControls;

namespace Kirana.Utilities
{
    /// <summary>
    /// This class is used Item Transfer from Global to Local
    /// </summary>
    public partial class ItemTransferFromGlobalToLocal : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();
        DBMStockItems dbMStockItems = new DBMStockItems();
        MRateSetting mRateSettig = new MRateSetting();
        MStockBarcode mStockBarcode = new MStockBarcode();
        MItemTaxInfo mItemTaxInfo = new MItemTaxInfo();
        MStockItems mStockItems = new MStockItems();
        DBMStockGroup dbStockGroup = new DBMStockGroup();
        MStockGroup mStockGroup = new MStockGroup();
        DBMUOM dbmUOM = new DBMUOM();
        MUOM mUOM = new MUOM();
        DBMManufacturerCompany dbManufacturerCompany = new DBMManufacturerCompany();
        MManufacturerCompany mManufacturerCompany = new MManufacturerCompany();
        DBMLedger dbLedger = new DBMLedger();
        MLedger mLedger = new MLedger();
        DBTVaucherEntry dbTVoucherEntry = new DBTVaucherEntry();
        TVoucherEntry tVoucherEntry = new TVoucherEntry();
        TStock tStock = new TStock();
        TStockGodown tStockGodown = new TStockGodown();
        SqlCommand cmd;
        SqlConnection con;
        DataSet dsDatabase;
        SqlDataAdapter da;

        /// <summary>
        /// This is class Constructor
        /// </summary>
        public ItemTransferFromGlobalToLocal()
        {
            InitializeComponent();
        }

        private void ItemTransferFromGlobalToLocal_Load(object sender, EventArgs e)
        {
            try
            {
                KeyDownFormat(this.Controls);
                lblWait.Font = new Font("Verdana", 14, FontStyle.Bold);
                lblWait.ForeColor = Color.Green;
                string strQuery = "";
                if (ObjQry.ReturnLong("Select Count(*) from MStockGroup", CommonFunctions.ConStr) <= 10)
                {
                    ObjTrans.Execute("set Identity_Insert " + ObjFunction.GetDatabaseName(CommonFunctions.ConStr) + "  On  ", CommonFunctions.ConStr);

                    strQuery = "insert " + ObjFunction.GetDatabaseName(CommonFunctions.ConStr) + ".dbo.MStockGroup " + "(" + GetColumns("MStockGroup") + ")";

                    strQuery = strQuery + " Select * from " + ObjFunction.GetDatabaseName(CommonFunctions.ConStr) + ".dbo.MGlobalStockGroup where ControlGroup!=3 ";

                    strQuery = strQuery + " set Identity_Insert " + ObjFunction.GetDatabaseName(CommonFunctions.ConStr) + ".dbo.MStockGroup " + " OFF ";

                    ObjTrans.Execute(strQuery, CommonFunctions.ConStr);

                    ObjTrans.Execute("Update MGlobalStockItems set IsActive='false'", CommonFunctions.ConStr);
                }
                CalculateProducts();
                ObjFunction.FillList(lstBrand, "Select StockGroupNo,StockGroupName From MGlobalStockGroup Where ControlGroup=3 and  StockGroupNo in(select Distinct GroupNo from MGlobalStockItems where IsActive='false') order by StockGroupName");//and IsActive='false'
                ObjFunction.FillCombo(cmbBrand, "Select StockGroupNo,StockGroupName From MGlobalStockGroup Where ControlGroup=3 and  StockGroupNo in(select Distinct GroupNo from MGlobalStockItems where IsActive='false') order by StockGroupName");
                ObjFunction.FillCombo(cmbCategory, "Select StockGroupNo,StockGroupName From MGlobalStockGroup Where ControlGroup=2 and  StockGroupNo in(select Distinct GroupNo1 from MGlobalStockItems where IsActive='false') order by StockGroupName");
                ObjFunction.FillCombo(cmbDepartment, "Select StockGroupNo,StockGroupName From MGlobalStockGroup Where ControlGroup=4 and  StockGroupNo in(select Distinct FkStockDeptNo from MGlobalStockItems where IsActive='false') order by StockGroupName");
                txtBarcode.Focus();
                DisplayColumns();
                formatPics();

                ObjFunction.FillList(lstSTax, "SELECT MItemTaxSetting.PkSrNo, (cast(MItemTaxSetting.Percentage as varchar)+ ' %') as Percentage FROM MItemTaxSetting INNER JOIN MLedger ON MItemTaxSetting.SalesLedgerNo = MLedger.LedgerNo INNER JOIN " +
                                    "   MLedger AS MLedger_1 ON MItemTaxSetting.TaxLedgerNo = MLedger_1.LedgerNo " +
                                    " WHERE     (MLedger.GroupNo = " + GroupType.SalesAccount + ") AND (MLedger_1.GroupNo = 32) And MItemTaxSetting.IsActive='True'  Order by  MItemTaxSetting.Percentage ");
                ObjFunction.FillList(lstPTax, "SELECT MItemTaxSetting.PkSrNo, (cast(MItemTaxSetting.Percentage as varchar)+ ' %') as Percentage FROM MItemTaxSetting INNER JOIN MLedger ON MItemTaxSetting.SalesLedgerNo = MLedger.LedgerNo INNER JOIN " +
                                    "   MLedger AS MLedger_1 ON MItemTaxSetting.TaxLedgerNo = MLedger_1.LedgerNo " +
                                    " WHERE     (MLedger.GroupNo = " + GroupType.PurchaseAccount + ") AND (MLedger_1.GroupNo = 32) And MItemTaxSetting.IsActive='True'  Order by  MItemTaxSetting.Percentage ");
                ChkSelect.Checked = false;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #region KeyDown Events
        private void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is GroupBox)
                    KeyDownFormat(ctrl.Controls);
            }
        }

        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F8)
            {
                if (BtnSave.Visible)
                {
                    if (gvRateSetting.CurrentCell.ColumnIndex == ColIndex.Select)
                        gvRateSetting.CurrentCell = gvRateSetting[ColIndex.ItemName, gvRateSetting.CurrentRow.Index];
                    BtnSave_Click(sender, e);
                }
            }
            if (e.KeyCode == Keys.F2)
            {
                ChkSelect.Checked = !ChkSelect.Checked;
                ChkSelect_CheckedChanged(sender, e);
            }
        }
        #endregion

        private string GetColumns(string tableName)
        {
            //         strConnect = "Server=.\\SQLEXPRESS;Initial Catalog=Retailer0002;Integrated Security=SSPI;Pooling=False";
            string str = "";
            try
            {
                string strConnect = CommonFunctions.ConStr;

                con = new SqlConnection(strConnect);
                con.Open();

                cmd = new SqlCommand();
                cmd.CommandText = "sp_Columns " + tableName;
                cmd.CommandType = CommandType.Text;

                da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                da.SelectCommand.Connection = con;

                dsDatabase = new DataSet();
                da.Fill(dsDatabase, "Columns");

                for (int i = 0; i < dsDatabase.Tables["Columns"].Rows.Count; i++)
                {
                    if (i == 0)
                        str = Convert.ToString(dsDatabase.Tables[0].Rows[i].ItemArray[3]);
                    else
                        str = str + "," + Convert.ToString(dsDatabase.Tables[0].Rows[i].ItemArray[3]);


                }
                return str;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return str;
            }
        }

        private void BindGrid()
        {
            try
            {
                BtnSave.Enabled = true;
                string sql = "", strQry = "";//, strCon = "";
                CalculateProducts();
                
                //if ((ObjFunction.GetComboValue(cmbBrand) == 0) && (txtBarcode.Text.Trim() != ""))
                //{
                //    long BrNo=ObjQry.ReturnLong(" SELECT MGlobalStockItems.GroupNo FROM MGlobalStockItems INNER JOIN " +
                //          " MGlobalStockBarcode ON MGlobalStockItems.ItemNo = MGlobalStockBarcode.ItemNo where Barcode='" + txtBarcode.Text + "'", CommonFunctions.ConStr);
                //    cmbBrand.SelectedValue = BrNo;
                //}

                sql = "SELECT   0 AS srNo, MGlobalStockGroup.StockGroupName, MGlobalStockItems.ItemName, MGlobalRateSetting.FromDate, MGlobalStockBarcode.Barcode, MGlobalStockItems.ShortCode, MGlobalRateSetting.MRP, " +
                      " MGlobalUOM.UOMName, MGlobalRateSetting.ASaleRate, MGlobalRateSetting.BSaleRate, MGlobalRateSetting.CSaleRate, MGlobalRateSetting.DSaleRate, " +
                      " MGlobalRateSetting.ESaleRate, MGlobalRateSetting.MKTQty, MGlobalRateSetting.PurRate, MGlobalRateSetting.PerOfRateVariation, MGlobalRateSetting.IsActive, " +
                      " MGlobalRateSetting.StockConversion, MGlobalRateSetting.PkSrNo, MGlobalStockBarcode.PkStockBarcodeNo, MGlobalStockItems.ItemNo, 'false' AS chk, " +
                      " MGlobalRateSetting.IsActive AS HidChk, MGlobalUOM.UOMNo, MGlobalStockGroup.LanguageName, " +
                      " MGlobalStockItems.LangFullDesc, MGlobalStockGroup.StockGroupNo, MGlobalStockItems.UOMPrimary, MGlobalStockItems.UOMDefault, " +
                      " (CASE WHEN (ISNull(MGlobalStockItems.FKStockDeptNo, 1950) = 0) THEN 1950 ELSE MGlobalStockItems.FKStockDeptNo END) AS Expr1, " +
                      " (CASE WHEN (ISNull(MGlobalStockItems.FkCategoryNo, 1) = 0) THEN 1 ELSE MGlobalStockItems.FkCategoryNo END) AS Expr2, " +
                      " (CASE WHEN (ISNull(MGlobalStockItems.FkDepartmentNo, 1) = 0) THEN 1 ELSE MGlobalStockItems.FkDepartmentNo END) AS Expr3, MGlobalStockItems.GroupNo1, " +
                      " MGlobalStockItems.ItemShortCode, MGlobalStockItems.IsFixedBarcode, MGlobalStockGroup_1.StockGroupName AS CategoryName, " +
                      " MGlobalStockGroup_2.StockGroupName AS DeptName, MGlobalStockItems.GlobalCode, ISNULL " +
                      " ((SELECT     MGlobalItemTaxInfo.Percentage " +
                      " FROM         MGlobalItemTaxInfo INNER JOIN " +
                      " MLedger ON MGlobalItemTaxInfo.SalesLedgerNo = MLedger.LedgerNo INNER JOIN " +
                      " MLedger AS MLedger_1 ON MGlobalItemTaxInfo.TaxLedgerNo = MLedger_1.LedgerNo " +
                      " WHERE     (MLedger.GroupNo = " + GroupType.SalesAccount + ") AND (MLedger_1.GroupNo = 32) AND (MGlobalItemTaxInfo.ItemNo = MGlobalStockItems.ItemNo)), 0) AS SVat, ISNULL " +
                      " ((SELECT     MGlobalItemTaxInfo_1.Percentage " +
                      " FROM         MGlobalItemTaxInfo AS MGlobalItemTaxInfo_1 INNER JOIN " +
                      " MLedger AS MLedger_2 ON MGlobalItemTaxInfo_1.SalesLedgerNo = MLedger_2.LedgerNo INNER JOIN " +
                      " MLedger AS MLedger_1 ON MGlobalItemTaxInfo_1.TaxLedgerNo = MLedger_1.LedgerNo " +
                      " WHERE     (MLedger_2.GroupNo = " + GroupType.PurchaseAccount + ") AND (MLedger_1.GroupNo = 32) AND (MGlobalItemTaxInfo_1.ItemNo = MGlobalStockItems.ItemNo)), 0) AS PVat, " +
                      " 0 AS OpeningStock, MGlobalManufacturerCompany.MfgCompName, 'false' AS SelectBarnd, MGlobalManufacturerCompany.MfgCompNo, " +
                      " IsNull((SELECT     MGlobalItemTaxInfo_1.FKTaxSettingNo " +
                      " FROM          MLedger AS MLedger_1 INNER JOIN " +
                      " MGlobalItemTaxInfo AS MGlobalItemTaxInfo_1 ON MLedger_1.LedgerNo = MGlobalItemTaxInfo_1.SalesLedgerNo " +
                      " WHERE      (MGlobalItemTaxInfo_1.ItemNo = MGlobalStockItems.ItemNo) AND (MLedger_1.GroupNo = " + GroupType.SalesAccount + ")),0) AS STax, " +
                      "IsNull((SELECT     MGlobalItemTaxInfo_1.FKTaxSettingNo " +
                      " FROM          MLedger AS MLedger_1 INNER JOIN " +
                      " MGlobalItemTaxInfo AS MGlobalItemTaxInfo_1 ON MLedger_1.LedgerNo = MGlobalItemTaxInfo_1.SalesLedgerNo " +
                      " WHERE      (MGlobalItemTaxInfo_1.ItemNo = MGlobalStockItems.ItemNo) AND (MLedger_1.GroupNo = " + GroupType.PurchaseAccount + ")),0) AS PTax " +
                      " FROM         MGlobalStockBarcode INNER JOIN " +
                      " MGlobalUOM INNER JOIN " +
                      " dbo.GetGlobalItemRateAll(NULL, NULL, NULL, NULL, NULL, NULL) AS MGlobalRateSetting ON MGlobalUOM.UOMNo = MGlobalRateSetting.UOMNo INNER JOIN " +
                      " MGlobalStockItems ON MGlobalRateSetting.ItemNo = MGlobalStockItems.ItemNo ON  " +
                      " MGlobalStockBarcode.PkStockBarcodeNo = MGlobalRateSetting.FkBcdSrNo INNER JOIN " +
                      " MGlobalStockGroup ON MGlobalStockItems.GroupNo = MGlobalStockGroup.StockGroupNo INNER JOIN " +
                      " MGlobalStockGroup AS MGlobalStockGroup_1 ON MGlobalStockItems.GroupNo1 = MGlobalStockGroup_1.StockGroupNo INNER JOIN " +
                      " MGlobalStockGroup AS MGlobalStockGroup_2 ON MGlobalStockItems.FKStockDeptNo = MGlobalStockGroup_2.StockGroupNo INNER JOIN " +
                      " MGlobalManufacturerCompany ON MGlobalStockGroup.MfgCompNo = MGlobalManufacturerCompany.MfgCompNo " +
                      " WHERE     (MGlobalStockItems.IsActive = 'false')";

                //" SELECT     0 AS srNo, MGlobalStockItems.ItemName, MGlobalRateSetting.FromDate, MGlobalStockBarcode.Barcode, MGlobalStockItems.ShortCode, MGlobalRateSetting.MRP, " +
                //  " MGlobalUOM.UOMName, MGlobalRateSetting.ASaleRate, MGlobalRateSetting.BSaleRate, MGlobalRateSetting.CSaleRate, MGlobalRateSetting.DSaleRate, " +
                //  " MGlobalRateSetting.ESaleRate, MGlobalRateSetting.MKTQty, MGlobalRateSetting.PurRate, MGlobalRateSetting.PerOfRateVariation, MGlobalRateSetting.IsActive, " +
                //  " MGlobalRateSetting.StockConversion, MGlobalRateSetting.PkSrNo, MGlobalStockBarcode.PkStockBarcodeNo, MGlobalStockItems.ItemNo, 'false' AS chk, " +
                //  " MGlobalRateSetting.IsActive AS HidChk, MGlobalUOM.UOMNo, MGlobalStockGroup.StockGroupName, MGlobalStockGroup.LanguageName, " +
                //  " MGlobalStockItems.LangFullDesc, MGlobalStockGroup.StockGroupNo, MGlobalStockItems.UOMPrimary, MGlobalStockItems.UOMDefault, " +
                //  " (CASE WHEN (ISNull(MGlobalStockItems.FKStockDeptNo, 1950) = 0) THEN 1950 ELSE MGlobalStockItems.FKStockDeptNo END) AS Expr1, " +
                //  " (CASE WHEN (ISNull(MGlobalStockItems.FkCategoryNo, 1) = 0) THEN 1 ELSE MGlobalStockItems.FkCategoryNo END) AS Expr2, " +
                //  " (CASE WHEN (ISNull(MGlobalStockItems.FkDepartmentNo, 1) = 0) THEN 1 ELSE MGlobalStockItems.FkDepartmentNo END) AS Expr3, MGlobalStockItems.GroupNo1, " +
                //  " MGlobalStockItems.ItemShortCode, MGlobalStockItems.IsFixedBarcode, MGlobalStockGroup_1.StockGroupName AS CategoryName, " +
                //  " MGlobalStockGroup_2.StockGroupName AS DeptName, MGlobalStockItems.GlobalCode, ISNULL " +
                //  " ((SELECT     MGlobalItemTaxInfo.Percentage " +
                //  " FROM         MGlobalItemTaxInfo INNER JOIN " +
                //  " MLedger ON MGlobalItemTaxInfo.SalesLedgerNo = MLedger.LedgerNo INNER JOIN " +
                //  " MLedger AS MLedger_1 ON MGlobalItemTaxInfo.TaxLedgerNo = MLedger_1.LedgerNo " +
                //  " WHERE     (MLedger.GroupNo = " + GroupType.SalesAccount + ") AND (MLedger_1.GroupNo = 32) AND (MGlobalItemTaxInfo.ItemNo = MGlobalStockItems.ItemNo)), 0) AS SVat, ISNULL " +
                //  " ((SELECT     MGlobalItemTaxInfo_1.Percentage " +
                //  " FROM         MGlobalItemTaxInfo AS MGlobalItemTaxInfo_1 INNER JOIN " +
                //  " MLedger AS MLedger_2 ON MGlobalItemTaxInfo_1.SalesLedgerNo = MLedger_2.LedgerNo INNER JOIN " +
                //  " MLedger AS MLedger_1 ON MGlobalItemTaxInfo_1.TaxLedgerNo = MLedger_1.LedgerNo " +
                //  " WHERE     (MLedger_2.GroupNo = " + GroupType.PurchaseAccount + ") AND (MLedger_1.GroupNo = 32) AND (MGlobalItemTaxInfo_1.ItemNo = MGlobalStockItems.ItemNo)), 0) AS PVat,  " +
                //  " 0 AS OpeningStock, 'false' AS SelectBarnd, MGlobalManufacturerCompany.MfgCompName, MGlobalManufacturerCompany.MfgCompNo, " +
                //  " MGlobalItemTaxInfo_2.FKTaxSettingNo AS STax, MGlobalItemTaxInfo_3.FKTaxSettingNo AS PTax " +
                //  " FROM         MGlobalStockBarcode INNER JOIN " +
                //  " MGlobalUOM INNER JOIN " +
                //  " dbo.GetGlobalItemRateAll(NULL, NULL, NULL, NULL, NULL, NULL) AS MGlobalRateSetting ON MGlobalUOM.UOMNo = MGlobalRateSetting.UOMNo INNER JOIN " +
                //  " MGlobalStockItems ON MGlobalRateSetting.ItemNo = MGlobalStockItems.ItemNo ON " +
                //  " MGlobalStockBarcode.PkStockBarcodeNo = MGlobalRateSetting.FkBcdSrNo INNER JOIN " +
                //  " MGlobalStockGroup ON MGlobalStockItems.GroupNo = MGlobalStockGroup.StockGroupNo INNER JOIN " +
                //  " MGlobalStockGroup AS MGlobalStockGroup_1 ON MGlobalStockItems.GroupNo1 = MGlobalStockGroup_1.StockGroupNo INNER JOIN " +
                //  " MGlobalStockGroup AS MGlobalStockGroup_2 ON MGlobalStockItems.FKStockDeptNo = MGlobalStockGroup_2.StockGroupNo INNER JOIN " +
                //  " MGlobalManufacturerCompany ON MGlobalStockGroup.MfgCompNo = MGlobalManufacturerCompany.MfgCompNo INNER JOIN " +
                //  " MGlobalItemTaxInfo AS MGlobalItemTaxInfo_2 ON MGlobalStockItems.ItemNo = MGlobalItemTaxInfo_2.ItemNo INNER JOIN " +
                //  " MGlobalItemTaxInfo AS MGlobalItemTaxInfo_3 ON MGlobalStockItems.ItemNo = MGlobalItemTaxInfo_3.PkSrNo " +
                //  " WHERE   (MGlobalStockItems.IsActive = 'false')";


                //" SELECT     0 AS srNo, MGlobalStockItems.ItemName, MGlobalRateSetting.FromDate, MGlobalStockBarcode.Barcode,MGlobalStockItems.ShortCode, MGlobalRateSetting.MRP, MGlobalUOM.UOMName, MGlobalRateSetting.ASaleRate, " +
                //          " MGlobalRateSetting.BSaleRate, MGlobalRateSetting.CSaleRate, MGlobalRateSetting.DSaleRate, MGlobalRateSetting.ESaleRate, MGlobalRateSetting.MKTQty, MGlobalRateSetting.PurRate, " +
                //          " MGlobalRateSetting.PerOfRateVariation, MGlobalRateSetting.IsActive, MGlobalRateSetting.StockConversion, MGlobalRateSetting.PkSrNo, MGlobalStockBarcode.PkStockBarcodeNo, " +
                //          " MGlobalStockItems.ItemNo, 'false' AS chk, MGlobalRateSetting.IsActive AS HidChk, MGlobalUOM.UOMNo, MGlobalStockGroup.StockGroupName, MGlobalStockGroup.LanguageName, " +
                //          " MGlobalStockItems.LangFullDesc, MGlobalStockGroup.StockGroupNo, MGlobalStockItems.UOMPrimary, MGlobalStockItems.UOMDefault, " +
                //          " (CASE WHEN (ISNull(MGlobalStockItems.FKStockDeptNo, 1950) = 0) THEN 1950 ELSE MGlobalStockItems.FKStockDeptNo END) AS Expr1, " +
                //          " (CASE WHEN (ISNull(MGlobalStockItems.FkCategoryNo, 1) = 0) THEN 1 ELSE MGlobalStockItems.FkCategoryNo END) AS Expr2, (CASE WHEN (ISNull(MGlobalStockItems.FkDepartmentNo,  1) = 0) THEN 1 ELSE MGlobalStockItems.FkDepartmentNo END) AS Expr3 ,MGlobalStockItems.GroupNo1, " +
                //          " MGlobalStockItems.ItemShortCode ,MGlobalStockItems.IsFixedBarcode, MGlobalStockGroup_1.StockGroupName AS CategoryName, " +
                //          " MGlobalStockGroup_2.StockGroupName AS DeptName,MGlobalStockItems.GlobalCode,isNull((SELECT     MGlobalItemTaxInfo.Percentage " +
                //          " FROM	MGlobalItemTaxInfo INNER JOIN MLedger ON MGlobalItemTaxInfo.SalesLedgerNo = MLedger.LedgerNo INNER JOIN " +
                //          " MLedger AS MLedger_1 ON MGlobalItemTaxInfo.TaxLedgerNo = MLedger_1.LedgerNo WHERE     (MLedger.GroupNo = " + GroupType.SalesAccount + ") AND (MLedger_1.GroupNo = 32) and MGlobalItemTaxInfo.ItemNo= MGlobalStockItems.ItemNo),0) as SVat,isNull((SELECT     MGlobalItemTaxInfo.Percentage " +
                //          " FROM	MGlobalItemTaxInfo INNER JOIN MLedger ON MGlobalItemTaxInfo.SalesLedgerNo = MLedger.LedgerNo INNER JOIN " +
                //          " MLedger AS MLedger_1 ON MGlobalItemTaxInfo.TaxLedgerNo = MLedger_1.LedgerNo  WHERE     (MLedger.GroupNo = " + GroupType.PurchaseAccount + ") AND (MLedger_1.GroupNo = 32) and MGlobalItemTaxInfo.ItemNo= MGlobalStockItems.ItemNo),0) as PVat, 0 as OpeningStock, 'false' AS SelectBarnd, MGlobalManufacturerCompany.MfgCompName, MGlobalManufacturerCompany.MfgCompNo,0 As STax,0 As PTax "+
                //          " FROM MGlobalStockBarcode INNER JOIN MGlobalUOM INNER JOIN dbo.GetGlobalItemRateAll(NULL, NULL, NULL, NULL, NULL, NULL) AS MGlobalRateSetting ON MGlobalUOM.UOMNo = MGlobalRateSetting.UOMNo INNER JOIN " +
                //          " MGlobalStockItems ON MGlobalRateSetting.ItemNo = MGlobalStockItems.ItemNo ON MGlobalStockBarcode.PkStockBarcodeNo = MGlobalRateSetting.FkBcdSrNo INNER JOIN " +
                //          " MGlobalStockGroup ON MGlobalStockItems.GroupNo = MGlobalStockGroup.StockGroupNo INNER JOIN " +
                //          " MGlobalStockGroup AS MGlobalStockGroup_1 ON MGlobalStockItems.GroupNo1 = MGlobalStockGroup_1.StockGroupNo INNER JOIN " +
                //          " MGlobalStockGroup AS MGlobalStockGroup_2 ON MGlobalStockItems.FKStockDeptNo = MGlobalStockGroup_2.StockGroupNo INNER JOIN "+
                //          "  MGlobalManufacturerCompany ON MGlobalStockGroup.MfgCompNo = MGlobalManufacturerCompany.MfgCompNo " +
                //          " WHERE  MGlobalStockItems.IsActive='false' ";
                string strwhere = "";
                if (ObjFunction.GetComboValue(cmbBrand) != 0)
                {
                    strwhere = strwhere + " and MGlobalStockItems.GroupNo=" + ObjFunction.GetComboValue(cmbBrand) + "";
                    //strCon = "Or";
                }
                if (ObjFunction.GetComboValue(cmbCategory) != 0)
                {
                    strwhere = strwhere + " and MGlobalStockItems.GroupNo1=" + ObjFunction.GetComboValue(cmbCategory) + "";
                    //strCon = "Or";
                }
                if (ObjFunction.GetComboValue(cmbDepartment) != 0)
                {
                    strwhere = strwhere + " and MGlobalStockItems.FkStockDeptNo=" + ObjFunction.GetComboValue(cmbDepartment) + "";
                    //strCon = "Or";
                }
                if (txtBarcode.Text.Trim() != "")
                {
                    strwhere = strwhere + " and MGlobalStockBarcode.Barcode='" + txtBarcode.Text.ToString().Trim() + "'";
                    //strCon = "Or";
                }
                //else
                strQry = sql + strwhere;

                //if(txtBarcode.Text.Trim()!="")
                //{
                //    if (strQry == "")
                //        strQry = sql + strCon + "(MGlobalStockBarcode.Barcode = '" + txtBarcode.Text.Trim() + "')";
                //    else
                //        strQry = strQry + strCon + "(MGlobalStockBarcode.Barcode = '" + txtBarcode.Text.Trim() + "')";
                //}

                DataTable dt = new DataTable();
                if (strwhere != "")
                {
                    DisplayMessageForWait(true);
                    dt = ObjFunction.GetDataView(strQry).Table;
                }
                gvRateSetting.Rows.Clear();
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        gvRateSetting.Rows.Add();
                        for (int j = 0; j < gvRateSetting.Columns.Count; j++)
                        {
                            gvRateSetting.Rows[i].Cells[j].Value = dt.Rows[i].ItemArray[j].ToString();
                        }
                        if (txtBarcode.Text.Trim() != "")
                        {
                            if (txtBarcode.Text.Trim().ToUpper() == dt.Rows[i].ItemArray[ColIndex.BarCode].ToString())
                            {
                                gvRateSetting.Rows[i].DefaultCellStyle.BackColor = Color.LightBlue;
                                gvRateSetting.CurrentCell = gvRateSetting[ColIndex.MRP, i];
                            }
                            else
                                gvRateSetting.Rows[i].DefaultCellStyle.BackColor = Color.White;
                        }
                    }
                    DisplayColumns();
                    if (gvRateSetting.Rows.Count > 0)
                    {
                        gvRateSetting.CurrentCell = gvRateSetting[0, 0];
                        gvRateSetting.Focus();
                    }
                }
                else
                {
                    if (strwhere != "")
                    {
                        DisplayMessage("Records Not Found.....");
                        BtnCancel_Click(BtnCancel, new EventArgs());
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
            finally
            {
                DisplayMessageForWait(false);
                this.Cursor = Cursors.Default;
            }
        }

        private void DisplayMessage(string str)
        {
            lblMsg.Visible = true;
            lblMsg.Text = str;
            Application.DoEvents();
            System.Threading.Thread.Sleep(700);
            lblMsg.Visible = false;
        }

        private void cmbBrand_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    if (cmbBrand.SelectedIndex == 0)
                    {
                        ObjFunction.FillCombo(cmbCategory, "Select StockGroupNo,StockGroupName From MGlobalStockGroup Where ControlGroup=2 and  StockGroupNo in(select Distinct GroupNo1 from MGlobalStockItems where IsActive='false') order by StockGroupName");
                        ObjFunction.FillCombo(cmbDepartment, "Select StockGroupNo,StockGroupName From MGlobalStockGroup Where ControlGroup=4 and  StockGroupNo in(select Distinct FkStockDeptNo from MGlobalStockItems where IsActive='false') order by StockGroupName");
                        txtBarcode.Focus();
                    }
                    else
                    {
                        ObjFunction.FillCombo(cmbCategory, "Select StockGroupNo,StockGroupName From MGlobalStockGroup Where ControlGroup=2 and  StockGroupNo in(select Distinct GroupNo1 from MGlobalStockItems where IsActive='false' and groupno=" + ObjFunction.GetComboValue(cmbBrand) + " ) order by StockGroupName");
                        ObjFunction.FillCombo(cmbDepartment, "Select StockGroupNo,StockGroupName From MGlobalStockGroup Where ControlGroup=4 and  StockGroupNo in(select Distinct FkStockDeptNo from MGlobalStockItems where IsActive='false' and groupno=" + ObjFunction.GetComboValue(cmbBrand) + " ) order by StockGroupName");
                    }
                    //txtBarcode.Text = "";
                    //cmbCategory.SelectedValue = "0";
                    //cmbDepartment.SelectedValue = "0";
                    BindGrid();
                    ChkSelect.Checked = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void txtBarcode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    if (txtBarcode.Text.Trim() != "")
                    {
                        if (ObjFunction.GetComboValue(cmbBrand) == 0)
                        {
                            BindGrid();
                        }
                        else
                        {
                            bool flag = false;
                            for (int i = 0; i < gvRateSetting.Rows.Count; i++)
                            {
                                if (txtBarcode.Text.Trim().ToUpper() == gvRateSetting.Rows[i].Cells[ColIndex.BarCode].Value.ToString())
                                {
                                    gvRateSetting.Rows[i].DefaultCellStyle.BackColor = Color.LightBlue;
                                    gvRateSetting.CurrentCell = gvRateSetting[ColIndex.MRP, i];
                                    flag = true;
                                }
                                else
                                {
                                    gvRateSetting.Rows[i].DefaultCellStyle.BackColor = Color.White;
                                }
                            }
                            if (flag == true)
                                gvRateSetting.Focus();
                            else
                            {
                                while (gvRateSetting.Rows.Count > 0)
                                    gvRateSetting.Rows.RemoveAt(0);
                                DisplayMessage("Records Not Found.....");
                                txtBarcode.Focus();
                            }
                        }
                    }
                    else
                        cmbDepartment.Focus();
                    //SearchBarcode(txtBarcode.Text.Trim());
                    ChkSelect.Checked = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void DisplayColumns()
        {
            gvRateSetting.Columns[ColIndex.ASaleRate].HeaderText = ObjFunction.GetAppSettings(AppSettings.ARateLabel);
            gvRateSetting.Columns[ColIndex.BSaleRate].HeaderText = ObjFunction.GetAppSettings(AppSettings.BRateLabel);
            gvRateSetting.Columns[ColIndex.CSaleRate].HeaderText = ObjFunction.GetAppSettings(AppSettings.CRateLabel);
            gvRateSetting.Columns[ColIndex.DSaleRate].HeaderText = ObjFunction.GetAppSettings(AppSettings.DRateLabel);
            gvRateSetting.Columns[ColIndex.ESaleRate].HeaderText = ObjFunction.GetAppSettings(AppSettings.ERateLabel);
        }

        private void gvRateSetting_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 0)
                e.Value = (e.RowIndex + 1).ToString();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            SetValue();
            Display(false);
        }

        private void SetValue()
        {
            try
            {
                
                bool Flag = false, SaveFlag = false;
                long ID = 0, CatId = 0, DeptID = 0, UOMID = 0, compID = 0, CountRows = 0, BrandID = 0;
                Application.DoEvents();
                //pnlMain.Enabled = false;
                DisplayMessageForWait(true);
                //this.Enabled = false;
                this.Cursor = Cursors.WaitCursor;
                if (Validations())
                {
                    BtnSave.Enabled = false;
                    for (int i = 0; i < gvRateSetting.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(gvRateSetting.Rows[i].Cells[ColIndex.Select].Value) == true)
                        {
                            //if (Validations(i) == true)
                            {
                                CountRows++;
                                compID = ObjQry.ReturnLong(" SELECT  ISNULL(MfgCompNo, 0) AS Expr1 FROM  MManufacturerCompany " +
                                                           " WHERE  (MfgCompGlobalCode = " +
                                                           " (SELECT  ISNULL(MGlobalManufacturerCompany.GlobalCode, 0) AS Expr1 " +
                                                           " FROM  MGlobalStockGroup INNER JOIN " +
                                                           " MGlobalStockItems ON MGlobalStockGroup.StockGroupNo = MGlobalStockItems.GroupNo INNER JOIN " +
                                                           " MGlobalManufacturerCompany ON MGlobalStockGroup.MfgCompNo = MGlobalManufacturerCompany.MfgCompNo " +
                                                           " WHERE  (MGlobalStockItems.ItemNo = " + Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.ItemNo].Value) + ") AND (MGlobalStockGroup.ControlGroup = 3)))", CommonFunctions.ConStr);

                                //ObjQry.ReturnLong("Select IsNull(StockGroupNo,0) from MStockGroup where GlobalCode= (SELECT MGlobalStockGroup.GlobalCode " +
                                //                        " FROM MGlobalStockGroup INNER JOIN  MGlobalStockItems ON MGlobalStockGroup.StockGroupNo = MGlobalStockItems.FKStockDeptNo where MGlobalStockItems.ItemNo=" + Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.ItemNo].Value) + ")" +
                                //                        " AND ControlGroup=4", CommonFunctions.ConStr);

                                dbManufacturerCompany = new DBMManufacturerCompany();
                                mManufacturerCompany = new MManufacturerCompany();
                                if (compID == 0)
                                {
                                    mManufacturerCompany.MfgCompNo = 0;
                                    mManufacturerCompany.MfgCompName = gvRateSetting.Rows[i].Cells[ColIndex.MfgCompName].Value.ToString().ToUpper();
                                    mManufacturerCompany.MfgCompShortCode = "";
                                    mManufacturerCompany.IsActive = true;
                                    mManufacturerCompany.UserID = DBGetVal.UserID;
                                    mManufacturerCompany.UserDate = DBGetVal.ServerTime.Date;
                                    mManufacturerCompany.CompanyNo = DBGetVal.CompanyNo;
                                    if (dbManufacturerCompany.AddMManufacturerCompany(mManufacturerCompany) == true)
                                    {
                                        compID = ObjQry.ReturnLong("SELECT  ISNULL(MfgCompNo, 0) AS Expr1  FROM  MManufacturerCompany WHERE  (UPPER(MfgCompName) = '" + gvRateSetting.Rows[i].Cells[ColIndex.MfgCompName].Value.ToString().ToUpper() + "'", CommonFunctions.ConStr);
                                        //compID = ObjQry.ReturnLong("Select IsNull(StockGroupNo,0) from MStockGroup where Upper(StockGroupName)='" + gvRateSetting.Rows[i].Cells[ColIndex.DeptName].Value.ToString().ToUpper() + "' AND ControlGroup=4", CommonFunctions.ConStr);
                                    }
                                }


                                //DeptID = ObjQry.ReturnLong("Select IsNull(StockGroupNo,0) from MStockGroup where Upper(StockGroupName)='" + gvRateSetting.Rows[i].Cells[ColIndex.DeptName].Value.ToString().ToUpper() + "' AND ControlGroup=4", CommonFunctions.ConStr);
                                DeptID = ObjQry.ReturnLong("Select IsNull(StockGroupNo,0) from MStockGroup where GlobalCode= (SELECT MGlobalStockGroup.GlobalCode " +
                                                            " FROM MGlobalStockGroup INNER JOIN  MGlobalStockItems ON MGlobalStockGroup.StockGroupNo = MGlobalStockItems.FKStockDeptNo where MGlobalStockItems.ItemNo=" + Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.ItemNo].Value) + ")" +
                                                            " AND ControlGroup=4", CommonFunctions.ConStr);

                                dbStockGroup = new DBMStockGroup();
                                mStockGroup = new MStockGroup();
                                if (DeptID == 0)
                                {
                                    mStockGroup.StockGroupNo = 0;
                                    mStockGroup.StockGroupName = gvRateSetting.Rows[i].Cells[ColIndex.DeptName].Value.ToString().ToUpper();
                                    mStockGroup.LanguageName = "";
                                    mStockGroup.ControlGroup = 4;
                                    mStockGroup.IsActive = true;
                                    mStockGroup.UserId = DBGetVal.UserID;
                                    mStockGroup.UserDate = DBGetVal.ServerTime.Date;
                                    mStockGroup.CompanyNo = DBGetVal.CompanyNo;
                                    mStockGroup.ControlSubGroup = 0;
                                    mStockGroup.MfgCompNo = 0;
                                    mStockGroup.Margin = 0;
                                    if (dbStockGroup.AddMStockGroup(mStockGroup) == true)
                                    {
                                        DeptID = ObjQry.ReturnLong("Select IsNull(StockGroupNo,0) from MStockGroup where Upper(StockGroupName)='" + gvRateSetting.Rows[i].Cells[ColIndex.DeptName].Value.ToString().ToUpper() + "' AND ControlGroup=4", CommonFunctions.ConStr);
                                    }
                                }



                                CatId = ObjQry.ReturnLong("Select IsNull(StockGroupNo,0) from MStockGroup where GlobalCode=(SELECT IsNull(MGlobalStockGroup.GlobalCode,0) " +
                                                          " FROM  MGlobalStockGroup INNER JOIN MGlobalStockItems ON MGlobalStockGroup.StockGroupNo = MGlobalStockItems.GroupNo1 where MGlobalStockItems.ItemNo=" + Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.ItemNo].Value) + ")" +
                                                          " AND ControlGroup=2", CommonFunctions.ConStr);


                                dbStockGroup = new DBMStockGroup();
                                mStockGroup = new MStockGroup();
                                if (CatId == 0)
                                {
                                    mStockGroup.StockGroupNo = 0;
                                    mStockGroup.StockGroupName = gvRateSetting.Rows[i].Cells[ColIndex.CategoryName].Value.ToString().ToUpper();
                                    mStockGroup.LanguageName = "";
                                    mStockGroup.ControlGroup = 2;
                                    mStockGroup.IsActive = true;
                                    mStockGroup.UserId = DBGetVal.UserID;
                                    mStockGroup.UserDate = DBGetVal.ServerTime.Date;
                                    mStockGroup.CompanyNo = DBGetVal.CompanyNo;
                                    mStockGroup.ControlSubGroup = ObjQry.ReturnLong("SELECT StockGroupNo From MStockGroup WHERE ControlGroup=4 AND Upper(StockGroupName)='" + gvRateSetting.Rows[i].Cells[ColIndex.DeptName].Value.ToString().ToUpper() + "'", CommonFunctions.ConStr);
                                    mStockGroup.MfgCompNo = 0;
                                    mStockGroup.Margin = 0;
                                    if (dbStockGroup.AddMStockGroup(mStockGroup) == true)
                                    {
                                        CatId = ObjQry.ReturnLong("Select IsNull(StockGroupNo,0) from MStockGroup where Upper(StockGroupName)='" + gvRateSetting.Rows[i].Cells[ColIndex.CategoryName].Value.ToString().ToUpper() + "' AND ControlGroup=2", CommonFunctions.ConStr);
                                    }
                                }
                                //if (Flag == false)
                                //{
                                BrandID = ObjQry.ReturnLong("Select IsNull(StockGroupNo,0) from MStockGroup where GlobalCode=(SELECT MGlobalStockGroup.GlobalCode " +
                                                          " FROM  MGlobalStockGroup INNER JOIN MGlobalStockItems ON MGlobalStockGroup.StockGroupNo = MGlobalStockItems.GroupNo where MGlobalStockItems.ItemNo=" + Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.ItemNo].Value) + ")" +
                                                          " AND ControlGroup=3", CommonFunctions.ConStr);
                                if (BrandID == 0)
                                {
                                    dbStockGroup = new DBMStockGroup();
                                    mStockGroup = new MStockGroup();
                                    //ID = brandID;//ObjQry.ReturnLong("Select IsNull(StockGroupNo,0) from MStockGroup where Upper(StockGroupName)='" + gvRateSetting.Rows[i].Cells[ColIndex.BrandName].Value.ToString().ToUpper() + "'", CommonFunctions.ConStr);
                                    mStockGroup.StockGroupNo = BrandID;
                                    mStockGroup.StockGroupName = gvRateSetting.Rows[i].Cells[ColIndex.BrandName].Value.ToString().ToUpper();
                                    mStockGroup.LanguageName = gvRateSetting.Rows[i].Cells[ColIndex.BarndLanguageName].Value.ToString();
                                    mStockGroup.ControlGroup = 3;
                                    mStockGroup.IsActive = true;
                                    mStockGroup.UserId = DBGetVal.UserID;
                                    mStockGroup.UserDate = DBGetVal.ServerTime.Date;
                                    mStockGroup.CompanyNo = DBGetVal.CompanyNo;
                                    mStockGroup.ControlSubGroup = ObjQry.ReturnLong("SELECT StockGroupNo From MStockGroup WHERE ControlGroup=2 AND Upper(StockGroupName)='" + gvRateSetting.Rows[i].Cells[ColIndex.CategoryName].Value.ToString().ToUpper() + "'", CommonFunctions.ConStr);
                                    mStockGroup.MfgCompNo = ObjQry.ReturnLong("SELECT MfgCompNo From MManufacturerCompany WHERE  Upper(MfgCompName)='" + gvRateSetting.Rows[i].Cells[ColIndex.MfgCompName].Value.ToString().ToUpper() + "'", CommonFunctions.ConStr);
                                    mStockGroup.Margin = 0;
                                    if (dbStockGroup.AddMStockGroup(mStockGroup) == true)
                                    {
                                        BrandID = ObjQry.ReturnLong("Select IsNull(StockGroupNo,0) from MStockGroup where Upper(StockGroupName)='" + gvRateSetting.Rows[i].Cells[ColIndex.BrandName].Value.ToString().ToUpper() + "'", CommonFunctions.ConStr);
                                    }
                                }
                                //}

                                ObjTrans.Execute("Update MStockGroup set isActive='true' where StockGroupNo=" + BrandID + "", CommonFunctions.ConStr);

                                UOMID = ObjQry.ReturnLong("Select IsNull(UomNo,0) from MUOM where Upper(UomName)='" + gvRateSetting.Rows[i].Cells[ColIndex.UOMNAme].Value.ToString().ToUpper() + "'", CommonFunctions.ConStr);
                                if (UOMID == 0)
                                {
                                    dbmUOM = new DBMUOM();
                                    mUOM = new MUOM();

                                    mUOM.UOMNo = UOMID;
                                    mUOM.UOMName = gvRateSetting.Rows[i].Cells[ColIndex.UOMNAme].Value.ToString().ToUpper();
                                    mUOM.UOMShortCode = gvRateSetting.Rows[i].Cells[ColIndex.UOMNAme].Value.ToString().ToUpper();
                                    mUOM.IsActive = true;
                                    mUOM.UserID = DBGetVal.UserID;
                                    mUOM.UserDate = DBGetVal.ServerTime.Date;
                                    if (dbmUOM.AddMUOM(mUOM) == true)
                                    {
                                        UOMID = ObjQry.ReturnLong("Select IsNull(UomNo,0) from MUOM where Upper(UomName)='" + gvRateSetting.Rows[i].Cells[ColIndex.UOMNAme].Value.ToString().ToUpper() + "'", CommonFunctions.ConStr);
                                    }
                                }
                                Flag = true;
                                dbMStockItems = new DBMStockItems();
                                mStockItems = new MStockItems();
                                mStockItems.ItemNo = 0;//Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.ItemNo].Value);
                                mStockItems.ItemName = gvRateSetting.Rows[i].Cells[ColIndex.ItemName].Value.ToString();
                                mStockItems.ItemShortCode = gvRateSetting.Rows[i].Cells[ColIndex.ItemShortCode].Value.ToString();//ObjQry.ReturnString("Select ItemShortCode From MGlobalStockItems Where ItemNo=" + mStockItems.ItemNo + "", CommonFunctions.ConStr);// ".";
                                mStockItems.FKStockDeptNo = DeptID;//Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.FKStockDeptNo].Value);
                                mStockItems.GroupNo = BrandID;//Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.BrandNo].Value);
                                mStockItems.FkCategoryNo = Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.FkCategoryNo].Value);
                                mStockItems.FkDepartmentNo = Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.FkDepartmentNo].Value);
                                mStockItems.FKStockLocationNo = 1;
                                mStockItems.GroupNo1 = CatId;//Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.GroupNo1].Value);
                                mStockItems.MaxLevel = 0;
                                mStockItems.MinLevel = 0;
                                mStockItems.ReOrderLevelQty = 0;
                                mStockItems.UOMDefault = UOMID;//Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.UOMDefault].Value);
                                mStockItems.UOMPrimary = UOMID;//Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.UOMPrimary].Value);
                                mStockItems.LangFullDesc = gvRateSetting.Rows[i].Cells[ColIndex.ItemLanguageFullDesc].Value.ToString();
                                mStockItems.LangShortDesc = gvRateSetting.Rows[i].Cells[ColIndex.ItemLanguageFullDesc].Value.ToString();
                                mStockItems.IsActive = true;
                                mStockItems.IsFixedBarcode = Convert.ToBoolean(gvRateSetting.Rows[i].Cells[ColIndex.IsFixedBarcode].FormattedValue);
                                mStockItems.CompanyNo = DBGetVal.CompanyNo;
                                mStockItems.UserId = DBGetVal.UserID;
                                mStockItems.UserDate = DBGetVal.ServerTime;
                                mStockItems.ShortCode = gvRateSetting.Rows[i].Cells[ColIndex.ShortCode].Value.ToString();
                                mStockItems.GlobalCode = (gvRateSetting.Rows[i].Cells[ColIndex.GlobalCode].Value.ToString() == "" || gvRateSetting.Rows[i].Cells[ColIndex.GlobalCode].Value.ToString() == null) ? 0 : Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.GlobalCode].Value.ToString());
                                mStockItems.FkStockGroupTypeNo = 1;
                                mStockItems.ControlUnder = 0;
                                mStockItems.FactorVal = 0;

                                dbMStockItems.AddMStockItems1(mStockItems);


                                //Stock BarCode Entry
                                mStockBarcode = new MStockBarcode();
                                mStockBarcode.PkStockBarcodeNo = 0;//Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.BarCodeNo].Value.ToString());
                                mStockBarcode.ItemNo = Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.ItemNo].Value);
                                mStockBarcode.Barcode = gvRateSetting.Rows[i].Cells[ColIndex.BarCode].Value.ToString();
                                mStockBarcode.IsActive = true;//chkActive.Checked;
                                mStockBarcode.UserID = DBGetVal.UserID;
                                mStockBarcode.UserDate = DBGetVal.ServerTime;
                                dbMStockItems.AddMStockBarcode(mStockBarcode);

                                //Rate Setting
                                mRateSettig = new MRateSetting();
                                mRateSettig.PkSrNo = 0;//Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.PkSrNo].Value);
                                mRateSettig.FkBcdSrNo = Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.BarCodeNo].Value);
                                mRateSettig.ItemNo = Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.ItemNo].Value);
                                mRateSettig.PurRate = Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.PurRate].Value);
                                mRateSettig.MRP = Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.MRP].Value);
                                mRateSettig.UOMNo = UOMID;//Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.UOM].Value);
                                mRateSettig.ASaleRate = Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.ASaleRate].Value);
                                mRateSettig.BSaleRate = Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.BSaleRate].Value);
                                mRateSettig.CSaleRate = Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.CSaleRate].Value);
                                mRateSettig.DSaleRate = Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.DSaleRate].Value);
                                mRateSettig.ESaleRate = Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.ESaleRate].Value);
                                mRateSettig.StockConversion = Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.StockConversion].Value);
                                mRateSettig.PerOfRateVariation = Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.RateVariation].Value);
                                mRateSettig.MKTQty = Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.MKTQty].Value);
                                mRateSettig.IsActive = Convert.ToBoolean(gvRateSetting.Rows[i].Cells[ColIndex.IsActive].Value);
                                mRateSettig.UserDate = DBGetVal.ServerTime.Date;
                                mRateSettig.FromDate = Convert.ToDateTime(gvRateSetting.Rows[i].Cells[ColIndex.Date].Value);
                                mRateSettig.UserID = DBGetVal.UserID;
                                mRateSettig.CompanyNo = DBGetVal.CompanyNo;
                                dbMStockItems.AddMRateSetting2(mRateSettig);

                                //if (Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.STax].Value.ToString()) == 0)
                                //{
                                //    long LedgerNo = ObjQry.ReturnLong("Select isNull(LedgerNo,0) from MLedger where Upper(LedgerName)='VAT @ " + Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.SVat].Value.ToString()) + "%' and GroupNo=32", CommonFunctions.ConStr);

                                //    if (LedgerNo == 0)
                                //    {
                                //        dbLedger = new DBMLedger();
                                //        mLedger = new MLedger();
                                //        mLedger.LedgerNo = 0;
                                //        mLedger.LedgerUserNo = "0";
                                //        mLedger.LedgerName = "VAT @ " + gvRateSetting.Rows[i].Cells[ColIndex.SVat].Value.ToString() + "%";
                                //        mLedger.OpeningBalance = 0;
                                //        mLedger.GroupNo = 32;
                                //        mLedger.SignCode = 1;
                                //        mLedger.ContactPerson = "";
                                //        mLedger.InvFlag = false;
                                //        mLedger.MaintainBillByBill = false;
                                //        mLedger.IsActive = true;
                                //        mLedger.CompanyNo = DBGetVal.CompanyNo;
                                //        mLedger.LedgerStatus = 1;
                                //        mLedger.UserID = DBGetVal.UserID;
                                //        mLedger.UserDate = DBGetVal.ServerTime.Date;
                                //        dbLedger.AddMLedger(mLedger);
                                //        dbLedger.ExecuteNonQueryStatements();
                                //    }
                                //}
                                //if (Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.STax].Value.ToString()) == 0)
                                //{
                                //    long SalesLedNo = ObjQry.ReturnLong("Select LedgerNo from MLedger where Upper(LedgerName)='SALES @ " + gvRateSetting.Rows[i].Cells[ColIndex.SVat].Value.ToString() + "%' and GroupNo=" + GroupType.SalesAccount + " ", CommonFunctions.ConStr);
                                //    if (SalesLedNo == 0)
                                //    {
                                //        dbLedger = new DBMLedger();
                                //        mLedger = new MLedger();
                                //        mLedger.LedgerNo = 0;
                                //        mLedger.LedgerUserNo = "0";
                                //        mLedger.LedgerName = "SALES @ " + gvRateSetting.Rows[i].Cells[ColIndex.SVat].Value.ToString() + "%";
                                //        mLedger.OpeningBalance = 0;
                                //        mLedger.GroupNo = GroupType.SalesAccount;
                                //        mLedger.SignCode = 1;
                                //        mLedger.ContactPerson = "";
                                //        mLedger.InvFlag = false;
                                //        mLedger.MaintainBillByBill = false;
                                //        mLedger.IsActive = true;
                                //        mLedger.CompanyNo = DBGetVal.CompanyNo;
                                //        mLedger.LedgerStatus = 1;
                                //        mLedger.UserID = DBGetVal.UserID;
                                //        mLedger.UserDate = DBGetVal.ServerTime.Date;
                                //        dbLedger.AddMLedger(mLedger);
                                //        dbLedger.ExecuteNonQueryStatements();
                                //    }
                                //}

                                //Sales Tax 
                                DataTable dtItemTaxInfo = new DataTable();
                                //DataTable dtTax = ObjFunction.GetDataView("Select * From MGlobalItemTaxInfo Where ItemNo=" + Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.ItemNo].Value) + " ANd SalesLedgerNo in(Select LedgerNo From MLedger Where GroupNo=" + GroupType.SalesAccount + ")").Table;
                                //if (Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.STax].Value.ToString()) == 0)
                                //{
                                //    dtItemTaxInfo = ObjFunction.GetDataView("SELECT MItemTaxSetting.TaxLedgerNo, MItemTaxSetting.SalesLedgerNo, MItemTaxSetting.Percentage FROM MItemTaxSetting INNER JOIN " +
                                //       " MLedger ON MItemTaxSetting.SalesLedgerNo = MLedger.LedgerNo INNER JOIN MLedger AS MLedger_1 ON MItemTaxSetting.TaxLedgerNo = MLedger_1.LedgerNo " +
                                //       " WHERE     (MLedger.GroupNo = " + GroupType.SalesAccount + ") AND (MLedger_1.GroupNo = 32) AND (MItemTaxSetting.Percentage = " + Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.SVat].Value) + ")").Table;

                                //}
                                //else
                                //{
                                //    dtItemTaxInfo = ObjFunction.GetDataView("SELECT MItemTaxSetting.TaxLedgerNo, MItemTaxSetting.SalesLedgerNo, MItemTaxSetting.Percentage FROM MItemTaxSetting INNER JOIN " +
                                //       " MLedger ON MItemTaxSetting.SalesLedgerNo = MLedger.LedgerNo INNER JOIN MLedger AS MLedger_1 ON MItemTaxSetting.TaxLedgerNo = MLedger_1.LedgerNo " +
                                //       " WHERE     (MLedger.GroupNo = " + GroupType.SalesAccount + ") AND (MLedger_1.GroupNo = 32) AND (MItemTaxSetting.TaxLedgerNo = " + Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.STax].Value) + ")").Table;
                                //}
                                dtItemTaxInfo = ObjFunction.GetDataView("SELECT   PkSrNo,TaxLedgerNo, SalesLedgerNo, Percentage  FROM  MItemTaxSetting  WHERE  (PkSrNo = " + Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.STax].Value) + ")").Table;
                                //dtItemTaxInfo = ObjFunction.GetDataView(" SELECT   PkSrNo,TaxLedgerNo, SalesLedgerNo, Percentage  FROM  MItemTaxSetting  WHERE  (PkSrNo =  (SELECT  FKTaxSettingNo FROM    MGlobalItemTaxInfo " +
                                //                                        " WHERE   (PkSrNo = " + Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.STax].Value) + ")))").Table;
                                if (dtItemTaxInfo.Rows.Count > 0)
                                {
                                    mItemTaxInfo.PkSrNo = 0;//Convert.ToInt64(dtTax.Rows[0][0].ToString());
                                    //mItemTaxInfo.ItemNo = Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.ItemNo].Value);
                                    mItemTaxInfo.TaxLedgerNo = Convert.ToInt64(dtItemTaxInfo.Rows[0].ItemArray[1].ToString());
                                    mItemTaxInfo.SalesLedgerNo = Convert.ToInt64(dtItemTaxInfo.Rows[0].ItemArray[2].ToString());
                                    mItemTaxInfo.FromDate = DBGetVal.ServerTime.Date;
                                    mItemTaxInfo.CalculationMethod = "2";
                                    mItemTaxInfo.Percentage = Convert.ToDouble(dtItemTaxInfo.Rows[0][3].ToString());
                                    mItemTaxInfo.FKTaxSettingNo = Convert.ToInt64(dtItemTaxInfo.Rows[0][0].ToString());
                                    mItemTaxInfo.UserID = DBGetVal.UserID;
                                    mItemTaxInfo.UserDate = DBGetVal.ServerTime.Date;
                                    dbMStockItems.AddMItemTaxInfo(mItemTaxInfo);
                                }
                                //if (Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.PTax].Value.ToString()) == 0)
                                //{
                                //    long PurLedNo = ObjQry.ReturnLong("Select LedgerNo from MLedger where Upper(LedgerName)='PURCHASE @ " + gvRateSetting.Rows[i].Cells[ColIndex.SVat].Value.ToString() + "%' and GroupNo=" + GroupType.PurchaseAccount + " ", CommonFunctions.ConStr);
                                //    if (PurLedNo == 0)
                                //    {
                                //        dbLedger = new DBMLedger();
                                //        mLedger = new MLedger();
                                //        mLedger.LedgerNo = 0;
                                //        mLedger.LedgerUserNo = "0";
                                //        mLedger.LedgerName = "PURCHASE @ " + gvRateSetting.Rows[i].Cells[ColIndex.SVat].Value.ToString() + "%";
                                //        mLedger.OpeningBalance = 0;
                                //        mLedger.GroupNo = GroupType.PurchaseAccount;
                                //        mLedger.SignCode = 1;
                                //        mLedger.ContactPerson = "";
                                //        mLedger.InvFlag = false;
                                //        mLedger.MaintainBillByBill = false;
                                //        mLedger.IsActive = true;
                                //        mLedger.CompanyNo = DBGetVal.CompanyNo;
                                //        mLedger.LedgerStatus = 1;
                                //        mLedger.UserID = DBGetVal.UserID;
                                //        mLedger.UserDate = DBGetVal.ServerTime.Date;
                                //        dbLedger.AddMLedger(mLedger);
                                //        dbLedger.ExecuteNonQueryStatements();
                                //    }
                                //}
                                //Purchase Tax
                                //dtTax = ObjFunction.GetDataView("Select * From MGlobalItemTaxInfo Where ItemNo=" + Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.ItemNo].Value) + " ANd SalesLedgerNo in(Select LedgerNo From MLedger Where GroupNo=" + GroupType.PurchaseAccount + ")").Table;
                                //if (Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.STax].Value.ToString()) == 0)
                                //{
                                //    dtItemTaxInfo = ObjFunction.GetDataView("SELECT MItemTaxSetting.TaxLedgerNo, MItemTaxSetting.SalesLedgerNo, MItemTaxSetting.Percentage FROM MItemTaxSetting INNER JOIN " +
                                //       " MLedger ON MItemTaxSetting.SalesLedgerNo = MLedger.LedgerNo INNER JOIN MLedger AS MLedger_1 ON MItemTaxSetting.TaxLedgerNo = MLedger_1.LedgerNo " +
                                //       " WHERE     (MLedger.GroupNo = " + GroupType.PurchaseAccount + ") AND (MLedger_1.GroupNo = 32) AND (MItemTaxSetting.Percentage = " + Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.PVat].Value) + ")").Table;
                                //}
                                //else
                                //{
                                //    dtItemTaxInfo = ObjFunction.GetDataView("SELECT MItemTaxSetting.TaxLedgerNo, MItemTaxSetting.SalesLedgerNo, MItemTaxSetting.Percentage FROM MItemTaxSetting INNER JOIN " +
                                //       " MLedger ON MItemTaxSetting.SalesLedgerNo = MLedger.LedgerNo INNER JOIN MLedger AS MLedger_1 ON MItemTaxSetting.TaxLedgerNo = MLedger_1.LedgerNo " +
                                //       " WHERE     (MLedger.GroupNo = " + GroupType.PurchaseAccount + ") AND (MLedger_1.GroupNo = 32) AND (MItemTaxSetting.TaxLedgerNo = " + Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.PTax].Value) + ")").Table;
                                //}
                                dtItemTaxInfo = ObjFunction.GetDataView("SELECT   PkSrNo,TaxLedgerNo, SalesLedgerNo, Percentage  FROM  MItemTaxSetting  WHERE  (PkSrNo = " + Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.PTax].Value) + ")").Table;
                                //dtItemTaxInfo = ObjFunction.GetDataView(" SELECT   PkSrNo,TaxLedgerNo, SalesLedgerNo, Percentage  FROM  MItemTaxSetting  WHERE  (PkSrNo =  (SELECT  FKTaxSettingNo FROM    MGlobalItemTaxInfo " +
                                //                                        " WHERE   (PkSrNo = " + Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.PTax].Value) + ")))").Table;
                                if (dtItemTaxInfo.Rows.Count > 0)
                                {
                                    mItemTaxInfo = new MItemTaxInfo();
                                    mItemTaxInfo.PkSrNo = 0;//Convert.ToInt64(dtTax.Rows[0][0].ToString());
                                    //mItemTaxInfo.ItemNo = Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.ItemNo].Value);
                                    mItemTaxInfo.TaxLedgerNo = Convert.ToInt64(dtItemTaxInfo.Rows[0].ItemArray[1].ToString());
                                    mItemTaxInfo.SalesLedgerNo = Convert.ToInt64(dtItemTaxInfo.Rows[0].ItemArray[2].ToString());
                                    mItemTaxInfo.FromDate = DBGetVal.ServerTime.Date;
                                    mItemTaxInfo.CalculationMethod = "2";
                                    mItemTaxInfo.Percentage = Convert.ToDouble(dtItemTaxInfo.Rows[0][3].ToString());
                                    mItemTaxInfo.FKTaxSettingNo = Convert.ToInt64(dtItemTaxInfo.Rows[0][0].ToString());
                                    mItemTaxInfo.UserID = DBGetVal.UserID;
                                    mItemTaxInfo.UserDate = DBGetVal.ServerTime.Date;
                                    dbMStockItems.AddMItemTaxInfo(mItemTaxInfo);
                                }
                                if (dbMStockItems.ExecuteNonQueryStatements() == true)
                                {
                                    SaveFlag = true;
                                    ObjTrans.Execute("Update MGlobalStockItems Set IsActive='true' where ItemNo=" + Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.ItemNo].Value) + "", CommonFunctions.ConStr);
                                    gvRateSetting.Rows[i].DefaultCellStyle.BackColor = Color.LightYellow;
                                    if (Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.OpStock].Value) > 0)
                                    {
                                        long ItNo = ObjQry.ReturnLong("Select ItemNo from MStockBarcode where Upper(Barcode) ='" + gvRateSetting.Rows[i].Cells[ColIndex.BarCode].Value.ToString().Trim().ToUpper() + "'", CommonFunctions.ConStr);
                                        ID = ObjQry.ReturnLong("SELECT PkVoucherNo FROM TVoucherEntry WHERE (VoucherTypeCode = 0) AND (VoucherDate = '1/1/1900')", CommonFunctions.ConStr);
                                        DataTable dtStock = ObjFunction.GetDataView("SELECT  PkSrNo, FkBcdSrNo, ItemNo, UOMNo FROM  dbo.GetItemRateAll(" + ItNo + ",NULL,NULL,Null,NULL,NULL) AS GetItemRateAll_1").Table;
                                        if (dtStock.Rows.Count > 0)
                                        {
                                            long ItemTaxNo = ObjQry.ReturnLong("SELECT PkSrNo FROM dbo.GetItemTaxAll(" + ItNo + ",NULL," + GroupType.SalesAccount + ",NULL,NULL) AS GetItemTaxAll_1", CommonFunctions.ConStr);
                                            dbTVoucherEntry = new DBTVaucherEntry();
                                            tVoucherEntry = new TVoucherEntry();
                                            tVoucherEntry.PkVoucherNo = ID;
                                            tVoucherEntry.VoucherTypeCode = 0;
                                            tVoucherEntry.VoucherUserNo = 0;
                                            tVoucherEntry.VoucherDate = Convert.ToDateTime("01-01-1900");
                                            tVoucherEntry.VoucherTime = DBGetVal.ServerTime;
                                            tVoucherEntry.Narration = "Sales Bill";
                                            tVoucherEntry.Reference = "";
                                            tVoucherEntry.ChequeNo = 0;
                                            tVoucherEntry.ClearingDate = Convert.ToDateTime("01-01-1900");
                                            tVoucherEntry.CompanyNo = DBGetVal.CompanyNo;
                                            tVoucherEntry.BilledAmount = 0;
                                            tVoucherEntry.ChallanNo = "";
                                            tVoucherEntry.Remark = "";
                                            tVoucherEntry.MacNo = DBGetVal.MacNo;
                                            tVoucherEntry.PayTypeNo = 0;
                                            tVoucherEntry.RateTypeNo = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_RateType));
                                            tVoucherEntry.TaxTypeNo = 0;
                                            tVoucherEntry.UserID = DBGetVal.UserID;
                                            tVoucherEntry.UserDate = DBGetVal.ServerTime.Date;
                                            dbTVoucherEntry.AddTVoucherEntry(tVoucherEntry);

                                            tStock = new TStock();
                                            tStock.PkStockTrnNo = 0;
                                            tStock.GroupNo = GroupType.CapitalAccount;
                                            tStock.ItemNo = ItNo;
                                            tStock.FkVoucherSrNo = 1;
                                            tStock.TrnCode = 1;
                                            tStock.Quantity = Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.OpStock].Value);
                                            tStock.BilledQuantity = Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.OpStock].Value);
                                            tStock.Rate = Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.ASaleRate].Value);
                                            tStock.Amount = tStock.Rate + tStock.Quantity;
                                            tStock.TaxPercentage = 0;
                                            tStock.TaxAmount = 0;
                                            tStock.DiscPercentage = 0;
                                            tStock.DiscAmount = 0;
                                            tStock.DiscRupees = 0;
                                            tStock.DiscPercentage2 = 0;
                                            tStock.DiscAmount2 = 0;
                                            tStock.DiscRupees2 = 0;
                                            tStock.NetRate = 0;
                                            tStock.NetAmount = 0;
                                            tStock.FkStockBarCodeNo = Convert.ToInt64(dtStock.Rows[0].ItemArray[1].ToString());
                                            tStock.FkUomNo = Convert.ToInt64(dtStock.Rows[0].ItemArray[3].ToString());
                                            tStock.FkRateSettingNo = Convert.ToInt64(dtStock.Rows[0].ItemArray[0].ToString());
                                            tStock.FkItemTaxInfo = ItemTaxNo;//Convert.ToInt64(dgBill[ColIndex.PkItemTaxInfo, i].Value.ToString());
                                            tStock.UserID = DBGetVal.UserID;
                                            tStock.UserDate = DBGetVal.ServerTime.Date;
                                            tStock.CompanyNo = DBGetVal.CompanyNo;
                                            tStock.LandedRate = 0;
                                            dbTVoucherEntry.AddTStock(tStock);

                                            tStockGodown = new TStockGodown();
                                            tStockGodown.PKStockGodownNo = 0;
                                            tStockGodown.ItemNo = ItNo;
                                            tStockGodown.GodownNo = 2;
                                            tStockGodown.Qty = Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.OpStock].Value);
                                            tStockGodown.ActualQty = Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.OpStock].Value);
                                            tStockGodown.UserID = DBGetVal.UserID;
                                            tStockGodown.UserDate = DBGetVal.ServerTime.Date;
                                            tStockGodown.CompanyNo = DBGetVal.CompanyNo;
                                            dbTVoucherEntry.AddTStockGodown(tStockGodown);

                                            long tempid = dbTVoucherEntry.ExecuteNonQueryStatements();
                                        }
                                    }


                                }
                                else
                                    SaveFlag = false;
                            }
                        }
                    }
                    if (Flag == true)
                    {
                        if (SaveFlag == true)
                        {
                            ObjTrans.Execute("exec StockUpdateAll", CommonFunctions.ConStr);
                            OMMessageBox.Show(" Item Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            ObjFunction.FillList(lstBrand, "Select StockGroupNo,StockGroupName From MGlobalStockGroup Where ControlGroup=3 and  StockGroupNo in(select Distinct GroupNo from MGlobalStockItems where IsActive='false') order by StockGroupName");//and IsActive='false'
                            //ObjFunction.FillCombo(cmbBrand, "Select StockGroupNo,StockGroupName From MGlobalStockGroup Where ControlGroup=3 and  StockGroupNo in(select Distinct GroupNo from MGlobalStockItems where IsActive='false') order by StockGroupName");
                            if (gvRateSetting.Rows.Count > CountRows)
                            {
                                long BrID = ObjFunction.GetComboValue(cmbBrand);
                                cmbBrand.SelectedValue = BrID;
                            }
                            else
                            {
                                if (cmbBrand.SelectedIndex + 1 == cmbBrand.Items.Count)
                                {
                                    ObjFunction.FillCombo(cmbBrand, "Select StockGroupNo,StockGroupName From MGlobalStockGroup Where ControlGroup=3 and  StockGroupNo in(select Distinct GroupNo from MGlobalStockItems where IsActive='false') order by StockGroupName");
                                    if (cmbBrand.Items.Count > 1)
                                        cmbBrand.SelectedIndex = 1;
                                    else
                                        cmbBrand.SelectedIndex = 0;
                                }
                                else
                                {
                                    int SelectIndex = cmbBrand.SelectedIndex;
                                    ObjFunction.FillCombo(cmbBrand, "Select StockGroupNo,StockGroupName From MGlobalStockGroup Where ControlGroup=3 and  StockGroupNo in(select Distinct GroupNo from MGlobalStockItems where IsActive='false') order by StockGroupName");
                                    cmbBrand.SelectedIndex = SelectIndex;
                                }
                            }
                            cmbCategory.SelectedIndex = 0;
                            cmbDepartment.SelectedIndex = 0;
                            txtBarcode.Text = "";
                            BindGrid();
                            CalculateProducts();
                            gvRateSetting.Focus();
                            if (gvRateSetting.Rows.Count > 0)
                                gvRateSetting.CurrentCell = gvRateSetting.Rows[0].Cells[ColIndex.Select];
                            BtnSave.Enabled = true;
                            ChkSelect.Checked = false;
                        }
                        else
                        {
                            OMMessageBox.Show(" Item Not Add ", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            BtnSave.Enabled = true;
                        }
                    }
                    else
                    {
                        OMMessageBox.Show("Atleast Select One Item", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        if (gvRateSetting.Rows.Count > 0)
                            gvRateSetting.CurrentCell = gvRateSetting.Rows[0].Cells[ColIndex.Select];
                        BtnSave.Enabled = true;
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
            finally
            {
                DisplayMessageForWait(false);
                this.Cursor = Cursors.Default;
            }
        }

        private bool Validations()
        {
            bool VFlag = true;

            try
            {
                for (int row = 0; row < gvRateSetting.Rows.Count; row++)
                {
                    bool colorFlag = true;
                    if (Convert.ToBoolean(gvRateSetting.Rows[row].Cells[ColIndex.Select].Value) == true)
                    {
                        if (gvRateSetting.Rows[row].Cells[ColIndex.ItemName].Value == null || gvRateSetting.Rows[row].Cells[ColIndex.ItemName].Value.ToString() == "" || gvRateSetting.Rows[row].Cells[ColIndex.BarCode].Value.ToString() == "" || gvRateSetting.Rows[row].Cells[ColIndex.BarCode].Value == null || gvRateSetting.Rows[row].Cells[ColIndex.ShortCode].Value.ToString() == "" || gvRateSetting.Rows[row].Cells[ColIndex.ShortCode].Value == null)
                        {
                            VFlag = false;
                            if (gvRateSetting.Rows[row].Cells[ColIndex.ItemName].Value == null || gvRateSetting.Rows[row].Cells[ColIndex.ItemName].Value.ToString() == "")
                            {
                                gvRateSetting.Rows[row].Cells[ColIndex.ItemName].ErrorText = "Enter Item Name";
                                gvRateSetting.Rows[row].Cells[ColIndex.Select].Value = false;
                                gvRateSetting.Rows[row].Cells[ColIndex.Select].ReadOnly = true;
                            }
                            if (gvRateSetting.Rows[row].Cells[ColIndex.BarCode].Value == null || gvRateSetting.Rows[row].Cells[ColIndex.BarCode].Value.ToString() == "")
                            {
                                gvRateSetting.Rows[row].Cells[ColIndex.BarCode].ErrorText = "Enter Barcode";
                                gvRateSetting.Rows[row].Cells[ColIndex.Select].Value = false;
                                gvRateSetting.Rows[row].Cells[ColIndex.Select].ReadOnly = true;
                            }
                            if (gvRateSetting.Rows[row].Cells[ColIndex.ShortCode].Value == null || gvRateSetting.Rows[row].Cells[ColIndex.ShortCode].Value.ToString() == "")
                            {
                                gvRateSetting.Rows[row].Cells[ColIndex.ShortCode].ErrorText = "Enter Short Code";
                                gvRateSetting.Rows[row].Cells[ColIndex.Select].Value = false;
                                gvRateSetting.Rows[row].Cells[ColIndex.Select].ReadOnly = true;
                            }
                            gvRateSetting.Rows[row].DefaultCellStyle.BackColor = Color.LightBlue;
                        }
                        else
                        {
                            //DataTable dt = ObjFunction.GetDataView(" SELECT     IsNull(Sum(Case when ((MStockItems.ItemName = '" + gvRateSetting.Rows[row].Cells[ColIndex.ItemName].Value.ToString().ToUpper() + "') AND (MStockItems.GroupNo = " + Convert.ToInt64(gvRateSetting.Rows[row].Cells[ColIndex.BrandNo].Value) + ") AND (MStockItems.GroupNo1 = " + Convert.ToInt64(gvRateSetting.Rows[row].Cells[ColIndex.GroupNo1].Value) + ")) then 1 else 0 end ),0) , " +
                            //                         " isNull(Sum(case when  (MStockBarcode.Barcode = '" + gvRateSetting.Rows[row].Cells[ColIndex.BarCode].Value.ToString() + "' or MStockBarcode.Barcode='" + gvRateSetting.Rows[row].Cells[ColIndex.ShortCode].Value.ToString() + "') then 1 else 0 end),0), " +
                            //                         " isNull(Sum(case when (MStockItems.ShortCode='" + gvRateSetting.Rows[row].Cells[ColIndex.BarCode].Value.ToString() + "' or MStockItems.ShortCode = '" + gvRateSetting.Rows[row].Cells[ColIndex.ShortCode].Value.ToString() + "' ) then 1 else 0 end ),0) " +
                            //                         " FROM MStockItems INNER JOIN MStockBarcode ON MStockItems.ItemNo = MStockBarcode.ItemNo ").Table;

                            DataTable dt = ObjFunction.GetDataView(" SELECT     IsNull(Sum(Case when ((MStockItems.ItemName = '" + gvRateSetting.Rows[row].Cells[ColIndex.ItemName].Value.ToString().ToUpper().Replace("'", "''") + "') AND (MStockGroup.StockGroupName = '" + gvRateSetting.Rows[row].Cells[ColIndex.BrandName].Value.ToString().ToUpper() + "') AND (Cateroty.StockGroupName = '" + gvRateSetting.Rows[row].Cells[ColIndex.CategoryName].Value.ToString().ToUpper() + "')) then 1 else 0 end ),0) , " +
                                                    " isNull(Sum(case when  (MStockBarcode.Barcode = '" + gvRateSetting.Rows[row].Cells[ColIndex.BarCode].Value.ToString() + "' or MStockBarcode.Barcode='" + gvRateSetting.Rows[row].Cells[ColIndex.ShortCode].Value.ToString() + "') then 1 else 0 end),0), " +
                                                    " isNull(Sum(case when (MStockItems.ShortCode='" + gvRateSetting.Rows[row].Cells[ColIndex.BarCode].Value.ToString() + "' or MStockItems.ShortCode = '" + gvRateSetting.Rows[row].Cells[ColIndex.ShortCode].Value.ToString() + "' ) then 1 else 0 end ),0) " +
                                                    " FROM MStockGroup INNER JOIN MStockItems INNER JOIN " +
                                                    " MStockBarcode ON MStockItems.ItemNo = MStockBarcode.ItemNo ON MStockGroup.StockGroupNo = MStockItems.GroupNo INNER JOIN " +
                                                    " MStockGroup AS Dept ON MStockItems.FKStockDeptNo = Dept.StockGroupNo INNER JOIN " +
                                                    " MStockGroup AS Cateroty ON MStockItems.GroupNo1 = Cateroty.StockGroupNo ").Table;
                            if (dt.Rows.Count > 0)
                            {
                                if (Convert.ToInt64(dt.Rows[0].ItemArray[0].ToString()) > 0)
                                {
                                    gvRateSetting.Rows[row].Cells[ColIndex.ItemName].ErrorText = "Duplicate Item Name";
                                    gvRateSetting.Rows[row].Cells[ColIndex.Select].Value = false;
                                    gvRateSetting.Rows[row].Cells[ColIndex.Select].ReadOnly = true;
                                    VFlag = false;
                                    colorFlag = false;
                                }
                                if (Convert.ToInt64(dt.Rows[0].ItemArray[1].ToString()) > 0)
                                {
                                    gvRateSetting.Rows[row].Cells[ColIndex.BarCode].ErrorText = "Duplicate Barcode";
                                    gvRateSetting.Rows[row].Cells[ColIndex.Select].Value = false;
                                    gvRateSetting.Rows[row].Cells[ColIndex.Select].ReadOnly = true;
                                    VFlag = false;
                                    colorFlag = false;
                                }
                                if (Convert.ToInt64(dt.Rows[0].ItemArray[2].ToString()) > 0)
                                {
                                    gvRateSetting.Rows[row].Cells[ColIndex.ShortCode].ErrorText = "Duplicate Short Code";
                                    gvRateSetting.Rows[row].Cells[ColIndex.Select].Value = false;
                                    gvRateSetting.Rows[row].Cells[ColIndex.Select].ReadOnly = true;
                                    VFlag = false;
                                    colorFlag = false;
                                }
                                if (colorFlag==false)
                                gvRateSetting.Rows[row].DefaultCellStyle.BackColor = Color.LightBlue;
                            }
                        }
                    }
                }
                return VFlag;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return false;
            }

        }

        #region ColumnIndex
        private static class ColIndex
        {
            public static int Sr = 0;
            public static int BrandName = 1;
            public static int ItemName = 2;
            public static int Date = 3;
            public static int BarCode = 4;
            public static int ShortCode = 5;
            public static int MRP = 6;
            public static int UOMNAme = 7;
            public static int ASaleRate = 8;
            public static int BSaleRate = 9;
            public static int CSaleRate = 10;
            public static int DSaleRate = 11;
            public static int ESaleRate = 12;
            public static int MKTQty = 13;
            public static int PurRate = 14;
            public static int RateVariation = 15;
            public static int IsActive = 16;
            public static int StockConversion = 17;
            public static int PkSrNo = 18;
            public static int BarCodeNo = 19;
            public static int ItemNo = 20;
            public static int Chk = 21;
            public static int HidChk = 22;
            public static int UOM = 23;
            public static int BarndLanguageName = 24;
            public static int ItemLanguageFullDesc = 25;
            public static int BrandNo = 26;
            public static int UOMPrimary = 27;
            public static int UOMDefault = 28;
            public static int FKStockDeptNo = 29;
            public static int FkCategoryNo = 30;
            public static int FkDepartmentNo = 31;
            public static int GroupNo1 = 32;
            public static int ItemShortCode = 33;
            public static int IsFixedBarcode = 34;
            public static int CategoryName = 35;
            public static int DeptName = 36;
            public static int GlobalCode = 37;
            public static int SVat = 38;
            public static int PVat = 39;
            public static int OpStock = 40;
            public static int MfgCompName = 41;
            public static int Select = 42;
            public static int MfgCompNo = 43;
            public static int STax = 44;
            public static int PTax = 45;

        }
        #endregion

        private void gvRateSetting_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            bool FlagMoveNext = false;
            try
            {
                if (e.ColumnIndex == ColIndex.ItemName)
                {
                    if (gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].FormattedValue.ToString().Trim() == "")
                    {
                        gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "Enter Item Name";
                        gvRateSetting.CurrentCell = gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex];
                        gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].Value = false;
                        gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].ReadOnly = true;
                        FlagMoveNext = false;

                    }
                    else
                    {
                        if (ObjQry.ReturnInteger("Select Count(*) from MStockItems where ItemName = '" + gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToUpper().Replace("'", "''") + "' " +
                       " AND GroupNo = " + Convert.ToInt64(gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.BrandNo].Value) +
                       " AND GroupNo1 = " + Convert.ToInt64(gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.GroupNo1].Value) + " ", CommonFunctions.ConStr) != 0)
                        {
                            gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "Duplicate Item Name";
                            gvRateSetting.CurrentCell = gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex];
                            FlagMoveNext = false;
                            gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].Value = false;
                            gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].ReadOnly = true;
                        }
                        else
                        {
                            gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";
                            gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.ItemShortCode].Value = gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                            FlagMoveNext = true;
                            gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].ReadOnly = false;
                        }
                    }
                }
                if (e.ColumnIndex == ColIndex.ItemLanguageFullDesc)
                {
                    if (gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].FormattedValue.ToString().Trim() == "")
                    {
                        gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "Enter Language  Full Desc";
                        gvRateSetting.CurrentCell = gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex];
                        gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].Value = false;
                        gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].ReadOnly = true;
                        FlagMoveNext = false;
                    }
                    else
                    {
                        gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";
                        FlagMoveNext = true;
                        gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].ReadOnly = false;
                    }

                }
                if (e.ColumnIndex == ColIndex.MRP)
                {
                    if (gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].FormattedValue.ToString().Trim() == "")
                    {
                        gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "Enter MRP";
                        gvRateSetting.CurrentCell = gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex];
                        gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].Value = false;
                        gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].ReadOnly = true;
                        FlagMoveNext = false;
                        gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].ReadOnly = false;
                    }
                    else
                    {
                        gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";
                        FlagMoveNext = true;
                        gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.ASaleRate].Value = gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.MRP].Value;
                        gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.BSaleRate].Value = gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.MRP].Value;
                        gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].ReadOnly = false;
                    }
                }
                if (e.ColumnIndex == ColIndex.ASaleRate)
                {
                    if (gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].FormattedValue.ToString().Trim() == "")
                    {
                        gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "Enter Whole Sale Rate";
                        gvRateSetting.CurrentCell = gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex];
                        gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].Value = false;
                        gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].ReadOnly = true;
                        FlagMoveNext = false;
                    }
                    else
                    {
                        gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";
                        FlagMoveNext = true;
                        gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.BSaleRate].Value = gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.ASaleRate].Value;
                        gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].ReadOnly = false;
                    }
                }
                if (e.ColumnIndex == ColIndex.BSaleRate)
                {
                    if (gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].FormattedValue.ToString().Trim() == "")
                    {
                        gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "Enter Retail Sale Rate";
                        gvRateSetting.CurrentCell = gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex];
                        gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].Value = false;
                        gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].ReadOnly = true;
                        FlagMoveNext = false;
                    }
                    else
                    {
                        gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";
                        FlagMoveNext = true;
                        gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].ReadOnly = false;
                    }
                }
                if (e.ColumnIndex == ColIndex.BarCode)
                {
                    if (gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].FormattedValue.ToString().Trim() == "")
                    {
                        gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "Enter Barcode";
                        gvRateSetting.CurrentCell = gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex];
                        gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].Value = false;
                        gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].ReadOnly = true;
                        FlagMoveNext = false;
                    }
                    else
                    {
                        if (ObjQry.ReturnInteger("Select Count(*) FROM MStockBarcode INNER JOIN MStockItems ON MStockBarcode.ItemNo = MStockItems.ItemNo where (Barcode = '" + gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.BarCode].Value.ToString() + "') or (MStockItems.ShortCode='" + gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.BarCode].Value.ToString() + "')", CommonFunctions.ConStr) != 0)
                        {
                            gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "Duplicate Barcode";
                            gvRateSetting.CurrentCell = gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex];
                            gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].Value = false;
                            gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].ReadOnly = true;
                            FlagMoveNext = false;
                        }
                        else
                        {
                            gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";
                            FlagMoveNext = true;
                            gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].ReadOnly = false;
                        }
                    }
                }
                if (e.ColumnIndex == ColIndex.ShortCode)
                {
                    if (gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].FormattedValue.ToString().Trim() == "")
                    {
                        gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "Enter ShortCode";
                        gvRateSetting.CurrentCell = gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex];
                        gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].Value = false;
                        gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].ReadOnly = true;
                        FlagMoveNext = false;
                    }
                    else if (gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() != "0")
                    {
                        if (ObjQry.ReturnInteger("Select Count(*) FROM MStockBarcode INNER JOIN MStockItems ON MStockBarcode.ItemNo = MStockItems.ItemNo where (ShortCode = '" + gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.ShortCode].Value.ToString() + "' ) or (MStockBarcode.Barcode='" + gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.ShortCode].Value.ToString() + "')", CommonFunctions.ConStr) != 0)
                        {
                            gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "Duplicate ShortCode";
                            gvRateSetting.CurrentCell = gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex];
                            gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].Value = false;
                            gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].ReadOnly = true;
                            FlagMoveNext = false;
                        }
                        else
                        {
                            gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";
                            FlagMoveNext = true;
                            gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Select].ReadOnly = false;
                        }
                    }

                }
                if (FlagMoveNext == true)
                {
                    for (int i = e.ColumnIndex; i < gvRateSetting.Columns.Count; i++)
                    {
                        if ((i + 1) < gvRateSetting.Columns.Count)
                        {
                            if (gvRateSetting.Rows[e.RowIndex].Cells[i + 1].Visible == true)
                            {
                                MovetoNext move2n = new MovetoNext(m2n);
                                BeginInvoke(move2n, new object[] { e.RowIndex, i + 1, gvRateSetting });
                                gvRateSetting.Focus();
                                break;
                            }
                        }
                    }
                }

                else if (e.ColumnIndex != ColIndex.Select)
                {
                    MovetoNext move2n = new MovetoNext(m2n);
                    BeginInvoke(move2n, new object[] { e.RowIndex, e.ColumnIndex, gvRateSetting });
                    gvRateSetting.Focus();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private delegate void MovetoNext(int RowIndex, int ColIndex, DataGridView dg);

        private void m2n(int RowIndex, int ColIndex, DataGridView dg)
        {
            dg.CurrentCell = dg.Rows[RowIndex].Cells[ColIndex];
        }

        private void gvRateSetting_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    if (gvRateSetting.Rows.Count > 0 && e.ColumnIndex != ColIndex.Chk)//&& e.ColumnIndex!=ColIndex.IsActive)
                    {
                        gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Chk].Value = true;
                        for (int i = 0; i < gvRateSetting.Columns.Count - 1; i++)
                        {
                            if (gvRateSetting.Rows[e.RowIndex].Cells[i].Value == null || gvRateSetting.Rows[e.RowIndex].Cells[i].Value.ToString().Length == 0 || gvRateSetting.Rows[e.RowIndex].Cells[i].ErrorText != "")
                            {
                                gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Chk].Value = false;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void gvRateSetting_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                TextBox txt = (TextBox)e.Control;
                if (gvRateSetting.CurrentCell.ColumnIndex == ColIndex.MRP)
                {
                    TextBox txt1 = (TextBox)e.Control;
                    txt1.TextChanged += new EventHandler(txt_TextChanged);
                }
                if (gvRateSetting.CurrentCell.ColumnIndex == ColIndex.ASaleRate)
                {
                    TextBox txt1 = (TextBox)e.Control;
                    txt1.TextChanged += new EventHandler(txt_TextChanged);
                }
                if (gvRateSetting.CurrentCell.ColumnIndex == ColIndex.BSaleRate)
                {
                    TextBox txt1 = (TextBox)e.Control;
                    txt1.TextChanged += new EventHandler(txt_TextChanged);
                }
                if (gvRateSetting.CurrentCell.ColumnIndex == ColIndex.PurRate)
                {
                    TextBox txt1 = (TextBox)e.Control;
                    txt1.TextChanged += new EventHandler(txt_TextChanged);
                }
                if (gvRateSetting.CurrentCell.ColumnIndex == ColIndex.SVat)
                {
                    TextBox txt1 = (TextBox)e.Control;
                    txt1.TextChanged += new EventHandler(txtVat_TextChanged);
                }
                if (gvRateSetting.CurrentCell.ColumnIndex == ColIndex.PVat)
                {
                    TextBox txt1 = (TextBox)e.Control;
                    txt1.TextChanged += new EventHandler(txtVat_TextChanged);
                }
                if (gvRateSetting.CurrentCell.ColumnIndex == ColIndex.OpStock)
                {
                    TextBox txt1 = (TextBox)e.Control;
                    txt1.TextChanged += new EventHandler(txtOpStock_TextChanged);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void txt_TextChanged(object sender, EventArgs e)
        {
            if (gvRateSetting.CurrentCell.ColumnIndex == ColIndex.MRP || (gvRateSetting.CurrentCell.ColumnIndex == ColIndex.ASaleRate) || (gvRateSetting.CurrentCell.ColumnIndex == ColIndex.BSaleRate) || (gvRateSetting.CurrentCell.ColumnIndex == ColIndex.PurRate))
                ObjFunction.SetMasked((TextBox)sender, 2, 5);
        }
        private void txtVat_TextChanged(object sender, EventArgs e)
        {
            if (gvRateSetting.CurrentCell.ColumnIndex == ColIndex.PVat || gvRateSetting.CurrentCell.ColumnIndex == ColIndex.SVat)
                ObjFunction.SetMasked((TextBox)sender, 2, 3);
        }
        private void txtOpStock_TextChanged(object sender, EventArgs e)
        {
            if (gvRateSetting.CurrentCell.ColumnIndex == ColIndex.OpStock)
                ObjFunction.SetMaskedNumeric((TextBox)sender);
        }

        private void gvRateSetting_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    if (gvRateSetting.Rows.Count > 0)
                    {
                        for (int i = gvRateSetting.CurrentCell.ColumnIndex; i < gvRateSetting.Columns.Count; i++)
                        {
                            if ((i + 1) < gvRateSetting.Columns.Count)
                            {
                                if (gvRateSetting.Rows[gvRateSetting.CurrentCell.RowIndex].Cells[i + 1].Visible == true)
                                {
                                    gvRateSetting.CurrentCell = gvRateSetting[i + 1, gvRateSetting.CurrentCell.RowIndex];
                                    gvRateSetting.Focus();
                                    break;
                                }
                            }
                        }
                    }
                }

                if (e.KeyCode == Keys.F9)
                {
                    e.SuppressKeyPress = true;
                    txtBarcode.Focus();
                }
                if (e.KeyCode == Keys.F6)
                {
                    e.SuppressKeyPress = true;
                    bool TFlag = false;
                    if (gvRateSetting.Rows.Count > 0)
                    {
                        for (int i = 0; i < gvRateSetting.Columns.Count; i++)
                        {
                            if (gvRateSetting.Rows[gvRateSetting.CurrentCell.RowIndex].Cells[i].ErrorText != "")
                            {
                                TFlag = true;
                                break;
                            }
                        }
                        if (TFlag == false)
                        {
                            gvRateSetting.Rows[gvRateSetting.CurrentCell.RowIndex].Cells[ColIndex.Select].Value = true;
                            txtBarcode.Text = "";
                            txtBarcode.Focus();
                        }
                    }

                }
                if (e.KeyCode == Keys.Escape)
                {
                    e.SuppressKeyPress = true;
                    BtnSave.Focus();
                }
                if (e.KeyCode == Keys.F4 && gvRateSetting.CurrentCell.ColumnIndex == ColIndex.SVat)
                {
                    if (lstSTax.Items.Count > 0)
                    {
                        pnlSTax.Visible = true;
                        lstSTax.Focus();
                        lstSTax.SelectedIndex = 0;
                    }
                }
                if (e.KeyCode == Keys.F4 && gvRateSetting.CurrentCell.ColumnIndex == ColIndex.PVat)
                {
                    if (lstPTax.Items.Count > 0)
                    {
                        pnlPTax.Visible = true;
                        lstPTax.Focus();
                        lstPTax.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void formatPics()
        {
            try
            {
                pnlSTax.Top = 130;
                pnlSTax.Left = 500;
                pnlSTax.Width = 120;
                pnlSTax.Height = 80;

                pnlPTax.Top = 130;
                pnlPTax.Left = 500;
                pnlPTax.Width = 120;
                pnlPTax.Height = 80;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                ObjFunction.FillCombo(cmbBrand, "Select StockGroupNo,StockGroupName From MGlobalStockGroup Where ControlGroup=3 and  StockGroupNo in(select Distinct GroupNo from MGlobalStockItems where IsActive='false') order by StockGroupName");
                ObjFunction.FillCombo(cmbCategory, "Select StockGroupNo,StockGroupName From MGlobalStockGroup Where ControlGroup=2 and  StockGroupNo in(select Distinct GroupNo1 from MGlobalStockItems where IsActive='false') order by StockGroupName");
                ObjFunction.FillCombo(cmbDepartment, "Select StockGroupNo,StockGroupName From MGlobalStockGroup Where ControlGroup=4 and  StockGroupNo in(select Distinct FkStockDeptNo from MGlobalStockItems where IsActive='false') order by StockGroupName");
                cmbBrand.SelectedValue = "0";
                cmbCategory.SelectedValue = "0";
                cmbDepartment.SelectedValue = "0";
                txtBarcode.Text = "";
                cmbBrand.Focus();
                Display(false);
                gvRateSetting.Rows.Clear();
                ChkSelect.Checked = false;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void Display(bool flag)
        {
            pnlPTax.Visible = flag;
            pnlSTax.Visible = flag;
        }
        private void lstBrand_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    cmbBrand.SelectedValue = lstBrand.SelectedValue;
                    ObjFunction.FillCombo(cmbCategory, "Select StockGroupNo,StockGroupName From MGlobalStockGroup Where ControlGroup=2 and  StockGroupNo in(select Distinct GroupNo1 from MGlobalStockItems where IsActive='false' and groupno=" + ObjFunction.GetComboValue(cmbBrand) + " ) order by StockGroupName");
                    ObjFunction.FillCombo(cmbDepartment, "Select StockGroupNo,StockGroupName From MGlobalStockGroup Where ControlGroup=4 and  StockGroupNo in(select Distinct FkStockDeptNo from MGlobalStockItems where IsActive='false' and groupno=" + ObjFunction.GetComboValue(cmbBrand) + " ) order by StockGroupName");
                    txtBarcode.Text = "";
                    cmbCategory.SelectedValue = "0";
                    cmbDepartment.SelectedValue = "0";
                    BindGrid();
                    txtBarcode.Focus();
                    ChkSelect.Checked = false;
                }
                if (e.KeyCode == Keys.Escape)
                {
                    e.SuppressKeyPress = true;
                    cmbBrand.Focus();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CalculateProducts()
        {
            try
            {
                txtTotal.Text = ObjQry.ReturnLong("Select Count(*) from MGlobalStockItems ", CommonFunctions.ConStr).ToString();
                txtShifted.Text = ObjQry.ReturnLong("Select Count(*) from MGlobalStockItems where IsActive='true'", CommonFunctions.ConStr).ToString();
                txtBalance.Text = ObjQry.ReturnLong("Select Count(*) from MGlobalStockItems where IsActive='false'", CommonFunctions.ConStr).ToString();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbCategory_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    //ObjFunction.FillCombo(cmbCategory, "Select StockGroupNo,StockGroupName From MGlobalStockGroup Where ControlGroup=2 and  StockGroupNo in(select Distinct GroupNo1 from MGlobalStockItems where IsActive='false') order by StockGroupName");
                    //ObjFunction.FillCombo(cmbDepartment, "Select StockGroupNo,StockGroupName From MGlobalStockGroup Where ControlGroup=4 and  StockGroupNo in(select Distinct FkStockDeptNo from MGlobalStockItems where IsActive='false' and groupno1=" + ObjFunction.GetComboValue(cmbCategory) + " ) order by StockGroupName");
                    //cmbDepartment.SelectedValue = "0";
                    if (ObjFunction.GetComboValue(cmbBrand) == 0 && ObjFunction.GetComboValue(cmbCategory) == 0 && ObjFunction.GetComboValue(cmbDepartment) == 0 && txtBarcode.Text.Trim() == "")
                        DisplayMessage("Please Select One ComboBox OR Type Barcode");
                    else
                        BindGrid();
                    ChkSelect.Checked = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbDepartment_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (cmbDepartment.SelectedIndex == 0 && cmbBrand.SelectedIndex == 0)
                        ObjFunction.FillCombo(cmbCategory, "Select StockGroupNo,StockGroupName From MGlobalStockGroup Where ControlGroup=2 and  StockGroupNo in(select Distinct GroupNo1 from MGlobalStockItems where IsActive='false') order by StockGroupName");
                    else if (cmbDepartment.SelectedIndex != 0 && cmbBrand.SelectedIndex == 0)
                        ObjFunction.FillCombo(cmbCategory, "Select StockGroupNo,StockGroupName From MGlobalStockGroup Where ControlGroup=2 and  StockGroupNo in(select Distinct GroupNo1 from MGlobalStockItems where IsActive='false' and FkStockDeptNo =" + ObjFunction.GetComboValue(cmbDepartment) + ") order by StockGroupName");
                    cmbCategory.SelectedValue = "0";
                    BindGrid();
                    e.SuppressKeyPress = true;
                    cmbCategory.Focus();
                    ChkSelect.Checked = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void lstTax_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    e.SuppressKeyPress = true;
                    pnlSTax.Visible = false;
                    gvRateSetting.Focus();
                    gvRateSetting.CurrentCell = gvRateSetting[gvRateSetting.CurrentCell.ColumnIndex, gvRateSetting.CurrentRow.Index];
                }
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    if (gvRateSetting.CurrentCell.ColumnIndex == ColIndex.SVat)
                    {
                        gvRateSetting.CurrentRow.Cells[ColIndex.SVat].Value = (lstSTax.Text.ToString().Substring(0, lstSTax.Text.ToString().Length - 2)).Trim();
                        gvRateSetting.CurrentRow.Cells[ColIndex.STax].Value = Convert.ToInt64(lstSTax.SelectedValue);
                    }
                    pnlSTax.Visible = false;
                    gvRateSetting.Focus();
                    gvRateSetting.CurrentCell = gvRateSetting[gvRateSetting.CurrentCell.ColumnIndex, gvRateSetting.CurrentRow.Index];
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void lstPTax_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    e.SuppressKeyPress = true;
                    pnlPTax.Visible = false;
                    gvRateSetting.Focus();
                    gvRateSetting.CurrentCell = gvRateSetting[gvRateSetting.CurrentCell.ColumnIndex, gvRateSetting.CurrentRow.Index];
                }
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    if (gvRateSetting.CurrentCell.ColumnIndex == ColIndex.PVat)
                    {
                        gvRateSetting.CurrentRow.Cells[ColIndex.PVat].Value = (lstPTax.Text.ToString().Substring(0, lstPTax.Text.ToString().Length - 2)).Trim();
                        gvRateSetting.CurrentRow.Cells[ColIndex.PTax].Value = Convert.ToInt64(lstPTax.SelectedValue);
                    }
                    pnlPTax.Visible = false;
                    gvRateSetting.Focus();
                    gvRateSetting.CurrentCell = gvRateSetting[gvRateSetting.CurrentCell.ColumnIndex, gvRateSetting.CurrentRow.Index];
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void ChkSelect_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < gvRateSetting.Rows.Count; i++)
            {
                gvRateSetting.Rows[i].Cells[ColIndex.Select].Value = ChkSelect.Checked;
            }
        }

        private void ItemTransferFromGlobalToLocal_Activated(object sender, EventArgs e)
        {
            ObjFunction.FillList(lstSTax, "SELECT MItemTaxSetting.PkSrNo, (cast(MItemTaxSetting.Percentage as varchar)+ ' %') as Percentage FROM MItemTaxSetting INNER JOIN MLedger ON MItemTaxSetting.SalesLedgerNo = MLedger.LedgerNo INNER JOIN " +
                                   "   MLedger AS MLedger_1 ON MItemTaxSetting.TaxLedgerNo = MLedger_1.LedgerNo " +
                                   " WHERE     (MLedger.GroupNo = " + GroupType.SalesAccount + ") AND (MLedger_1.GroupNo = 32) And MItemTaxSetting.IsActive='True'  Order by  MItemTaxSetting.Percentage ");
            ObjFunction.FillList(lstPTax, "SELECT MItemTaxSetting.PkSrNo, (cast(MItemTaxSetting.Percentage as varchar)+ ' %') as Percentage FROM MItemTaxSetting INNER JOIN MLedger ON MItemTaxSetting.SalesLedgerNo = MLedger.LedgerNo INNER JOIN " +
                                "   MLedger AS MLedger_1 ON MItemTaxSetting.TaxLedgerNo = MLedger_1.LedgerNo " +
                                " WHERE     (MLedger.GroupNo = " + GroupType.PurchaseAccount + ") AND (MLedger_1.GroupNo = 32) And MItemTaxSetting.IsActive='True'  Order by  MItemTaxSetting.Percentage ");

        }

        private void cmbBrand_Leave(object sender, EventArgs e)
        {
            if (cmbBrand.SelectedIndex == 0)
            {
                ObjFunction.FillCombo(cmbCategory, "Select StockGroupNo,StockGroupName From MGlobalStockGroup Where ControlGroup=2 and  StockGroupNo in(select Distinct GroupNo1 from MGlobalStockItems where IsActive='false') order by StockGroupName");
                ObjFunction.FillCombo(cmbDepartment, "Select StockGroupNo,StockGroupName From MGlobalStockGroup Where ControlGroup=4 and  StockGroupNo in(select Distinct FkStockDeptNo from MGlobalStockItems where IsActive='false') order by StockGroupName");
            //    txtBarcode.Focus();
            }
            else
            {
                ObjFunction.FillCombo(cmbCategory, "Select StockGroupNo,StockGroupName From MGlobalStockGroup Where ControlGroup=2 and  StockGroupNo in(select Distinct GroupNo1 from MGlobalStockItems where IsActive='false' and groupno=" + ObjFunction.GetComboValue(cmbBrand) + " ) order by StockGroupName");
                ObjFunction.FillCombo(cmbDepartment, "Select StockGroupNo,StockGroupName From MGlobalStockGroup Where ControlGroup=4 and  StockGroupNo in(select Distinct FkStockDeptNo from MGlobalStockItems where IsActive='false' and groupno=" + ObjFunction.GetComboValue(cmbBrand) + " ) order by StockGroupName");
            }
            BindGrid();
            ChkSelect.Checked = false;
        }

        private void cmbDepartment_Leave(object sender, EventArgs e)
        {
            if (cmbDepartment.SelectedIndex == 0 && cmbBrand.SelectedIndex == 0)
                ObjFunction.FillCombo(cmbCategory, "Select StockGroupNo,StockGroupName From MGlobalStockGroup Where ControlGroup=2 and  StockGroupNo in(select Distinct GroupNo1 from MGlobalStockItems where IsActive='false') order by StockGroupName");
            else if (cmbDepartment.SelectedIndex != 0 && cmbBrand.SelectedIndex == 0)
                ObjFunction.FillCombo(cmbCategory, "Select StockGroupNo,StockGroupName From MGlobalStockGroup Where ControlGroup=2 and  StockGroupNo in(select Distinct GroupNo1 from MGlobalStockItems where IsActive='false' and FkStockDeptNo =" + ObjFunction.GetComboValue(cmbDepartment) + ") order by StockGroupName");
            cmbCategory.SelectedValue = "0";
            //cmbCategory.Focus();
            BindGrid();
            ChkSelect.Checked = false;
        }

        private void cmbCategory_Leave(object sender, EventArgs e)
        {
            if (ObjFunction.GetComboValue(cmbBrand) == 0 && ObjFunction.GetComboValue(cmbCategory) == 0 && ObjFunction.GetComboValue(cmbDepartment) == 0 && txtBarcode.Text.Trim() == "")
                DisplayMessage("Please Select One ComboBox OR Type Barcode");
            else
                BindGrid();
            ChkSelect.Checked = false;
        }

        private void DisplayMessageForWait(bool flag)
        {

            try
            {
                lblWait.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
                lblWait.Text = "Processing ...";
                Application.DoEvents();
                lblWait.Visible = flag;
                Application.DoEvents();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }
    }
}
