﻿namespace Kirana.Vouchers
{
    partial class StockConvert
    { 
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            this.EP = new System.Windows.Forms.ErrorProvider(this.components);
            this.pnlMain = new JitControls.OMBPanel();
            this.pnlStockConvert = new JitControls.OMBPanel();
            this.pnlSearch = new System.Windows.Forms.Panel();
            this.btnCancelSearch = new System.Windows.Forms.Button();
            this.lblSearch = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.lblMsg = new System.Windows.Forms.Label();
            this.pnlGroup1 = new System.Windows.Forms.Panel();
            this.lstGroup1 = new System.Windows.Forms.ListBox();
            this.pnlGroup2 = new System.Windows.Forms.Panel();
            this.lstGroup2 = new System.Windows.Forms.ListBox();
            this.pnlItemName = new System.Windows.Forms.Panel();
            this.dgItemList = new System.Windows.Forms.DataGridView();
            this.iItemNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iUOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iMRP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iStock = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iUOMStk = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iSaleTax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iPurTax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iCompany = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iBarcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iRateSettingNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMDefault = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PurRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GowodnNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscountType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlUOM = new System.Windows.Forms.Panel();
            this.lstUOM = new System.Windows.Forms.ListBox();
            this.pnlRate = new System.Windows.Forms.Panel();
            this.lstRate = new System.Windows.Forms.ListBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dgBill = new System.Windows.Forms.DataGridView();
            this.BtnSearch = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrev = new System.Windows.Forms.Button();
            this.btnLast = new System.Windows.Forms.Button();
            this.btnFirst = new System.Windows.Forms.Button();
            this.BtnExit = new System.Windows.Forms.Button();
            this.BtnNew = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.BtnUpdate = new System.Windows.Forms.Button();
            this.txtRemark = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtInvNo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpBillDate = new System.Windows.Forms.DateTimePicker();
            this.dgListItem = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SaleRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PurchaseRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NetRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscPerce = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscRupees = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscPercentage2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscAmount2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscRupees2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NetAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Barcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkStockTrnNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkSrNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkVoucherNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkRateSettingNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkItemTaxInfo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StockFactor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActualQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MKTQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxPerce = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FkStockGodownNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StockCompanyNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkStockTrnNoTo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkStockGodownNoTo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TempMRP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TempQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GodownNoFrom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BtnConvert = new System.Windows.Forms.DataGridViewButtonColumn();
            this.FItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FUOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FStockFactor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FConvertStock = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FItemNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FUOMNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FFKRateSettingNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StatusNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SUOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SStockFK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SItemNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SUOMNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FkRateSettingNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SGodownNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.EP)).BeginInit();
            this.pnlMain.SuspendLayout();
            this.pnlStockConvert.SuspendLayout();
            this.pnlSearch.SuspendLayout();
            this.pnlGroup1.SuspendLayout();
            this.pnlGroup2.SuspendLayout();
            this.pnlItemName.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgItemList)).BeginInit();
            this.pnlUOM.SuspendLayout();
            this.pnlRate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgListItem)).BeginInit();
            this.SuspendLayout();
            // 
            // EP
            // 
            this.EP.ContainerControl = this;
            // 
            // pnlMain
            // 
            this.pnlMain.BorderColor = System.Drawing.Color.Gray;
            this.pnlMain.BorderRadius = 3;
            this.pnlMain.Controls.Add(this.pnlStockConvert);
            this.pnlMain.Controls.Add(this.pnlSearch);
            this.pnlMain.Controls.Add(this.lblMsg);
            this.pnlMain.Controls.Add(this.pnlGroup1);
            this.pnlMain.Controls.Add(this.pnlGroup2);
            this.pnlMain.Controls.Add(this.pnlItemName);
            this.pnlMain.Controls.Add(this.pnlUOM);
            this.pnlMain.Controls.Add(this.pnlRate);
            this.pnlMain.Controls.Add(this.panel4);
            this.pnlMain.Controls.Add(this.dgBill);
            this.pnlMain.Controls.Add(this.BtnSearch);
            this.pnlMain.Controls.Add(this.btnDelete);
            this.pnlMain.Controls.Add(this.btnNext);
            this.pnlMain.Controls.Add(this.btnPrev);
            this.pnlMain.Controls.Add(this.btnLast);
            this.pnlMain.Controls.Add(this.btnFirst);
            this.pnlMain.Controls.Add(this.BtnExit);
            this.pnlMain.Controls.Add(this.BtnNew);
            this.pnlMain.Controls.Add(this.BtnCancel);
            this.pnlMain.Controls.Add(this.BtnSave);
            this.pnlMain.Controls.Add(this.BtnUpdate);
            this.pnlMain.Controls.Add(this.txtRemark);
            this.pnlMain.Controls.Add(this.label4);
            this.pnlMain.Controls.Add(this.label1);
            this.pnlMain.Controls.Add(this.txtInvNo);
            this.pnlMain.Controls.Add(this.label2);
            this.pnlMain.Controls.Add(this.dtpBillDate);
            this.pnlMain.Location = new System.Drawing.Point(12, 12);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(1198, 517);
            this.pnlMain.TabIndex = 5566;
            // 
            // pnlStockConvert
            // 
            this.pnlStockConvert.BorderColor = System.Drawing.Color.Gray;
            this.pnlStockConvert.BorderRadius = 3;
            this.pnlStockConvert.Controls.Add(this.label3);
            this.pnlStockConvert.Controls.Add(this.dgListItem);
            this.pnlStockConvert.Location = new System.Drawing.Point(277, 167);
            this.pnlStockConvert.Name = "pnlStockConvert";
            this.pnlStockConvert.Size = new System.Drawing.Size(553, 190);
            this.pnlStockConvert.TabIndex = 5577;
            this.pnlStockConvert.Visible = false;
            this.pnlStockConvert.VisibleChanged += new System.EventHandler(this.pnlStockConvert_VisibleChanged);
            // 
            // pnlSearch
            // 
            this.pnlSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSearch.Controls.Add(this.btnCancelSearch);
            this.pnlSearch.Controls.Add(this.lblSearch);
            this.pnlSearch.Controls.Add(this.txtSearch);
            this.pnlSearch.Location = new System.Drawing.Point(326, 100);
            this.pnlSearch.Name = "pnlSearch";
            this.pnlSearch.Size = new System.Drawing.Size(190, 73);
            this.pnlSearch.TabIndex = 5576;
            this.pnlSearch.Visible = false;
            // 
            // btnCancelSearch
            // 
            this.btnCancelSearch.Location = new System.Drawing.Point(55, 39);
            this.btnCancelSearch.Name = "btnCancelSearch";
            this.btnCancelSearch.Size = new System.Drawing.Size(79, 23);
            this.btnCancelSearch.TabIndex = 503;
            this.btnCancelSearch.Text = "Cancel";
            this.btnCancelSearch.UseVisualStyleBackColor = true;
            this.btnCancelSearch.Click += new System.EventHandler(this.btnCancelSearch_Click);
            // 
            // lblSearch
            // 
            this.lblSearch.AutoSize = true;
            this.lblSearch.Location = new System.Drawing.Point(4, 14);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(45, 13);
            this.lblSearch.TabIndex = 502;
            this.lblSearch.Text = "Inv No :";
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(96, 11);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(85, 20);
            this.txtSearch.TabIndex = 0;
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            // 
            // lblMsg
            // 
            this.lblMsg.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.lblMsg.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMsg.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg.ForeColor = System.Drawing.Color.Maroon;
            this.lblMsg.Location = new System.Drawing.Point(174, 267);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(481, 52);
            this.lblMsg.TabIndex = 5568;
            this.lblMsg.Text = "label4";
            this.lblMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMsg.Visible = false;
            // 
            // pnlGroup1
            // 
            this.pnlGroup1.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.pnlGroup1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlGroup1.Controls.Add(this.lstGroup1);
            this.pnlGroup1.Location = new System.Drawing.Point(23, 167);
            this.pnlGroup1.Name = "pnlGroup1";
            this.pnlGroup1.Size = new System.Drawing.Size(118, 46);
            this.pnlGroup1.TabIndex = 5570;
            this.pnlGroup1.Visible = false;
            this.pnlGroup1.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            // 
            // lstGroup1
            // 
            this.lstGroup1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstGroup1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstGroup1.FormattingEnabled = true;
            this.lstGroup1.Location = new System.Drawing.Point(8, 8);
            this.lstGroup1.Name = "lstGroup1";
            this.lstGroup1.Size = new System.Drawing.Size(101, 28);
            this.lstGroup1.TabIndex = 516;
            this.lstGroup1.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            this.lstGroup1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lstGroup1_KeyPress);
            this.lstGroup1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstGroup1_KeyDown);
            // 
            // pnlGroup2
            // 
            this.pnlGroup2.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.pnlGroup2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlGroup2.Controls.Add(this.lstGroup2);
            this.pnlGroup2.Location = new System.Drawing.Point(24, 216);
            this.pnlGroup2.Name = "pnlGroup2";
            this.pnlGroup2.Size = new System.Drawing.Size(118, 47);
            this.pnlGroup2.TabIndex = 5571;
            this.pnlGroup2.Visible = false;
            this.pnlGroup2.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            // 
            // lstGroup2
            // 
            this.lstGroup2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstGroup2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstGroup2.FormattingEnabled = true;
            this.lstGroup2.Location = new System.Drawing.Point(8, 8);
            this.lstGroup2.Name = "lstGroup2";
            this.lstGroup2.Size = new System.Drawing.Size(101, 28);
            this.lstGroup2.TabIndex = 517;
            this.lstGroup2.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            this.lstGroup2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lstGroup2_KeyPress);
            // 
            // pnlItemName
            // 
            this.pnlItemName.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.pnlItemName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlItemName.Controls.Add(this.dgItemList);
            this.pnlItemName.Location = new System.Drawing.Point(44, 95);
            this.pnlItemName.Name = "pnlItemName";
            this.pnlItemName.Size = new System.Drawing.Size(57, 47);
            this.pnlItemName.TabIndex = 5569;
            this.pnlItemName.Visible = false;
            this.pnlItemName.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            // 
            // dgItemList
            // 
            this.dgItemList.AllowUserToAddRows = false;
            this.dgItemList.AllowUserToDeleteRows = false;
            this.dgItemList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgItemList.BackgroundColor = OM.ThemeColor.Panle_Back_Color;
            this.dgItemList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgItemList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iItemNo,
            this.iItemName,
            this.iRate,
            this.iUOM,
            this.iMRP,
            this.iStock,
            this.iUOMStk,
            this.iSaleTax,
            this.iPurTax,
            this.iCompany,
            this.iBarcode,
            this.iRateSettingNo,
            this.UOMDefault,
            this.PurRate,
            this.GowodnNo,
            this.DiscountType});
            this.dgItemList.Location = new System.Drawing.Point(3, 5);
            this.dgItemList.Name = "dgItemList";
            this.dgItemList.Size = new System.Drawing.Size(600, 30);
            this.dgItemList.TabIndex = 515;
            this.dgItemList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgItemList_KeyDown);
            this.dgItemList.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            // 
            // iItemNo
            // 
            this.iItemNo.DataPropertyName = "ItemNo";
            this.iItemNo.HeaderText = "ItemNo";
            this.iItemNo.Name = "iItemNo";
            this.iItemNo.ReadOnly = true;
            this.iItemNo.ToolTipText = "ItemNo";
            this.iItemNo.Visible = false;
            // 
            // iItemName
            // 
            this.iItemName.DataPropertyName = "ItemName";
            this.iItemName.HeaderText = "Item Description";
            this.iItemName.Name = "iItemName";
            this.iItemName.ReadOnly = true;
            this.iItemName.ToolTipText = "Product Name";
            this.iItemName.Width = 200;
            // 
            // iRate
            // 
            this.iRate.DataPropertyName = "SaleRate";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle20.Format = "N2";
            dataGridViewCellStyle20.NullValue = null;
            this.iRate.DefaultCellStyle = dataGridViewCellStyle20;
            this.iRate.HeaderText = "Rate";
            this.iRate.Name = "iRate";
            this.iRate.ReadOnly = true;
            this.iRate.ToolTipText = "Sales Rate";
            this.iRate.Width = 60;
            // 
            // iUOM
            // 
            this.iUOM.DataPropertyName = "UOMName";
            this.iUOM.HeaderText = "UOM";
            this.iUOM.Name = "iUOM";
            this.iUOM.ReadOnly = true;
            this.iUOM.ToolTipText = "UOM";
            this.iUOM.Width = 60;
            // 
            // iMRP
            // 
            this.iMRP.DataPropertyName = "MRP";
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle21.Format = "N2";
            this.iMRP.DefaultCellStyle = dataGridViewCellStyle21;
            this.iMRP.HeaderText = "MRP";
            this.iMRP.Name = "iMRP";
            this.iMRP.ReadOnly = true;
            this.iMRP.ToolTipText = "MRP";
            this.iMRP.Width = 60;
            // 
            // iStock
            // 
            this.iStock.DataPropertyName = "Stock";
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle22.Format = "N2";
            this.iStock.DefaultCellStyle = dataGridViewCellStyle22;
            this.iStock.HeaderText = "Stock";
            this.iStock.Name = "iStock";
            this.iStock.ReadOnly = true;
            this.iStock.ToolTipText = "Stock QTY";
            this.iStock.Width = 60;
            // 
            // iUOMStk
            // 
            this.iUOMStk.DataPropertyName = "stkUOM";
            this.iUOMStk.HeaderText = "UOM";
            this.iUOMStk.Name = "iUOMStk";
            this.iUOMStk.ReadOnly = true;
            this.iUOMStk.ToolTipText = "Stock UOM";
            this.iUOMStk.Visible = false;
            this.iUOMStk.Width = 50;
            // 
            // iSaleTax
            // 
            this.iSaleTax.DataPropertyName = "SaleTax";
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle23.Format = "N2";
            dataGridViewCellStyle23.NullValue = null;
            this.iSaleTax.DefaultCellStyle = dataGridViewCellStyle23;
            this.iSaleTax.HeaderText = "S.Tax";
            this.iSaleTax.Name = "iSaleTax";
            this.iSaleTax.ReadOnly = true;
            this.iSaleTax.ToolTipText = "Sales Tax Percent";
            this.iSaleTax.Visible = false;
            this.iSaleTax.Width = 50;
            // 
            // iPurTax
            // 
            this.iPurTax.DataPropertyName = "PurTax";
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle24.Format = "N2";
            dataGridViewCellStyle24.NullValue = null;
            this.iPurTax.DefaultCellStyle = dataGridViewCellStyle24;
            this.iPurTax.HeaderText = "P.Tax";
            this.iPurTax.Name = "iPurTax";
            this.iPurTax.ReadOnly = true;
            this.iPurTax.ToolTipText = "Purchase Tax Percent";
            this.iPurTax.Visible = false;
            this.iPurTax.Width = 50;
            // 
            // iCompany
            // 
            this.iCompany.DataPropertyName = "CompanyNo";
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle25.Format = "N0";
            dataGridViewCellStyle25.NullValue = null;
            this.iCompany.DefaultCellStyle = dataGridViewCellStyle25;
            this.iCompany.HeaderText = "CNo";
            this.iCompany.Name = "iCompany";
            this.iCompany.ReadOnly = true;
            this.iCompany.ToolTipText = "Company No";
            this.iCompany.Visible = false;
            this.iCompany.Width = 30;
            // 
            // iBarcode
            // 
            this.iBarcode.DataPropertyName = "Barcode";
            this.iBarcode.HeaderText = "Barcode";
            this.iBarcode.Name = "iBarcode";
            this.iBarcode.ReadOnly = true;
            this.iBarcode.ToolTipText = "Barcode";
            this.iBarcode.Width = 125;
            // 
            // iRateSettingNo
            // 
            this.iRateSettingNo.DataPropertyName = "RateSettingNo";
            this.iRateSettingNo.HeaderText = "RateSettingNo";
            this.iRateSettingNo.Name = "iRateSettingNo";
            this.iRateSettingNo.ReadOnly = true;
            this.iRateSettingNo.Visible = false;
            // 
            // UOMDefault
            // 
            this.UOMDefault.DataPropertyName = "UOMDefault";
            this.UOMDefault.HeaderText = "UOMDefault";
            this.UOMDefault.Name = "UOMDefault";
            this.UOMDefault.ReadOnly = true;
            this.UOMDefault.Visible = false;
            // 
            // PurRate
            // 
            this.PurRate.DataPropertyName = "PurRate";
            this.PurRate.HeaderText = "PurRate";
            this.PurRate.Name = "PurRate";
            this.PurRate.ReadOnly = true;
            this.PurRate.Visible = false;
            // 
            // GowodnNo
            // 
            this.GowodnNo.DataPropertyName = "GodownNo";
            this.GowodnNo.HeaderText = "GowodnNo";
            this.GowodnNo.Name = "GowodnNo";
            this.GowodnNo.Visible = false;
            // 
            // DiscountType
            // 
            this.DiscountType.DataPropertyName = "DiscountType";
            this.DiscountType.HeaderText = "DiscountType";
            this.DiscountType.Name = "DiscountType";
            this.DiscountType.Visible = false;
            // 
            // pnlUOM
            // 
            this.pnlUOM.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.pnlUOM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlUOM.Controls.Add(this.lstUOM);
            this.pnlUOM.Location = new System.Drawing.Point(23, 269);
            this.pnlUOM.Name = "pnlUOM";
            this.pnlUOM.Size = new System.Drawing.Size(118, 50);
            this.pnlUOM.TabIndex = 5572;
            this.pnlUOM.Visible = false;
            this.pnlUOM.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            // 
            // lstUOM
            // 
            this.lstUOM.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstUOM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstUOM.FormattingEnabled = true;
            this.lstUOM.Location = new System.Drawing.Point(8, 8);
            this.lstUOM.Name = "lstUOM";
            this.lstUOM.Size = new System.Drawing.Size(101, 28);
            this.lstUOM.TabIndex = 516;
            this.lstUOM.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            this.lstUOM.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lstUOM_KeyPress);
            // 
            // pnlRate
            // 
            this.pnlRate.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.pnlRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRate.Controls.Add(this.lstRate);
            this.pnlRate.Location = new System.Drawing.Point(667, 123);
            this.pnlRate.Name = "pnlRate";
            this.pnlRate.Size = new System.Drawing.Size(118, 70);
            this.pnlRate.TabIndex = 5573;
            this.pnlRate.Visible = false;
            this.pnlRate.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            // 
            // lstRate
            // 
            this.lstRate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstRate.FormattingEnabled = true;
            this.lstRate.Location = new System.Drawing.Point(8, 8);
            this.lstRate.Name = "lstRate";
            this.lstRate.Size = new System.Drawing.Size(101, 41);
            this.lstRate.TabIndex = 517;
            this.lstRate.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            this.lstRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lstRate_KeyPress);
            // 
            // panel4
            // 
            this.panel4.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Location = new System.Drawing.Point(667, 212);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(118, 47);
            this.panel4.TabIndex = 5574;
            this.panel4.Visible = false;
            // 
            // dgBill
            // 
            this.dgBill.AllowUserToAddRows = false;
            this.dgBill.AllowUserToDeleteRows = false;
            this.dgBill.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgBill.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.ItemName,
            this.Quantity,
            this.UOM,
            this.SaleRate,
            this.Rate,
            this.PurchaseRate,
            this.NetRate,
            this.DiscPerce,
            this.DiscAmt,
            this.DiscRupees,
            this.DiscPercentage2,
            this.DiscAmount2,
            this.DiscRupees2,
            this.NetAmt,
            this.Amount,
            this.Barcode,
            this.PkStockTrnNo,
            this.PkSrNo,
            this.PkVoucherNo,
            this.ItemNo,
            this.UOMNo,
            this.PkRateSettingNo,
            this.PkItemTaxInfo,
            this.StockFactor,
            this.ActualQty,
            this.MKTQuantity,
            this.TaxPerce,
            this.TaxAmount,
            this.FkStockGodownNo,
            this.StockCompanyNo,
            this.PkStockTrnNoTo,
            this.PkStockGodownNoTo,
            this.TempMRP,
            this.TempQty,
            this.GodownNoFrom,
            this.BtnConvert,
            this.FItemName,
            this.FUOM,
            this.FStockFactor,
            this.FConvertStock,
            this.FItemNo,
            this.FUOMNo,
            this.FFKRateSettingNo,
            this.StatusNo});
            this.dgBill.Location = new System.Drawing.Point(17, 69);
            this.dgBill.Name = "dgBill";
            this.dgBill.Size = new System.Drawing.Size(1159, 330);
            this.dgBill.TabIndex = 5;
            this.dgBill.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgBill_CellValueChanged);
            this.dgBill.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgBill_CellFormatting);
            this.dgBill.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgBill_CellEndEdit);
            this.dgBill.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgBill_EditingControlShowing);
            this.dgBill.CurrentCellChanged += new System.EventHandler(this.dgBill_CurrentCellChanged);
            this.dgBill.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgBill_KeyDown);
            this.dgBill.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgBill_CellContentClick);
            // 
            // BtnSearch
            // 
            this.BtnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnSearch.Location = new System.Drawing.Point(191, 451);
            this.BtnSearch.Name = "BtnSearch";
            this.BtnSearch.Size = new System.Drawing.Size(80, 60);
            this.BtnSearch.TabIndex = 5557;
            this.BtnSearch.Text = "&Search";
            this.BtnSearch.UseVisualStyleBackColor = true;
            this.BtnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDelete.Location = new System.Drawing.Point(705, 451);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(80, 60);
            this.btnDelete.TabIndex = 5558;
            this.btnDelete.Text = "&Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Visible = false;
            this.btnDelete.Click += new System.EventHandler(this.btndelete_Click);
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnNext.Location = new System.Drawing.Point(442, 487);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(37, 24);
            this.btnNext.TabIndex = 5562;
            this.btnNext.Text = ">";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPrev.Location = new System.Drawing.Point(405, 487);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(37, 24);
            this.btnPrev.TabIndex = 5561;
            this.btnPrev.Text = "<";
            this.btnPrev.UseVisualStyleBackColor = true;
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // btnLast
            // 
            this.btnLast.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLast.Location = new System.Drawing.Point(479, 487);
            this.btnLast.Name = "btnLast";
            this.btnLast.Size = new System.Drawing.Size(37, 24);
            this.btnLast.TabIndex = 5563;
            this.btnLast.Text = ">|";
            this.btnLast.UseVisualStyleBackColor = true;
            this.btnLast.Click += new System.EventHandler(this.btnLast_Click);
            // 
            // btnFirst
            // 
            this.btnFirst.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnFirst.Location = new System.Drawing.Point(368, 487);
            this.btnFirst.Name = "btnFirst";
            this.btnFirst.Size = new System.Drawing.Size(37, 24);
            this.btnFirst.TabIndex = 5560;
            this.btnFirst.Text = "|<";
            this.btnFirst.UseVisualStyleBackColor = true;
            this.btnFirst.Click += new System.EventHandler(this.btnFirst_Click);
            // 
            // BtnExit
            // 
            this.BtnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnExit.Location = new System.Drawing.Point(281, 451);
            this.BtnExit.Name = "BtnExit";
            this.BtnExit.Size = new System.Drawing.Size(80, 60);
            this.BtnExit.TabIndex = 5559;
            this.BtnExit.Text = "E&xit";
            this.BtnExit.UseVisualStyleBackColor = true;
            this.BtnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // BtnNew
            // 
            this.BtnNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnNew.Location = new System.Drawing.Point(19, 451);
            this.BtnNew.Name = "BtnNew";
            this.BtnNew.Size = new System.Drawing.Size(80, 60);
            this.BtnNew.TabIndex = 5553;
            this.BtnNew.Text = "&New";
            this.BtnNew.UseVisualStyleBackColor = true;
            this.BtnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnCancel.Location = new System.Drawing.Point(105, 451);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(80, 60);
            this.BtnCancel.TabIndex = 5555;
            this.BtnCancel.Text = "&Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnSave.Location = new System.Drawing.Point(19, 451);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(80, 60);
            this.BtnSave.TabIndex = 5554;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // BtnUpdate
            // 
            this.BtnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnUpdate.Location = new System.Drawing.Point(619, 451);
            this.BtnUpdate.Name = "BtnUpdate";
            this.BtnUpdate.Size = new System.Drawing.Size(80, 60);
            this.BtnUpdate.TabIndex = 5556;
            this.BtnUpdate.Text = "&Update";
            this.BtnUpdate.UseVisualStyleBackColor = true;
            this.BtnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // txtRemark
            // 
            this.txtRemark.Location = new System.Drawing.Point(136, 42);
            this.txtRemark.Name = "txtRemark";
            this.txtRemark.Size = new System.Drawing.Size(547, 20);
            this.txtRemark.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 13);
            this.label4.TabIndex = 5552;
            this.label4.Text = "Remark/Narration :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 501;
            this.label1.Text = "No :";
            // 
            // txtInvNo
            // 
            this.txtInvNo.BackColor = System.Drawing.Color.White;
            this.txtInvNo.Location = new System.Drawing.Point(136, 14);
            this.txtInvNo.Name = "txtInvNo";
            this.txtInvNo.ReadOnly = true;
            this.txtInvNo.Size = new System.Drawing.Size(100, 20);
            this.txtInvNo.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(331, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 502;
            this.label2.Text = "Date";
            // 
            // dtpBillDate
            // 
            this.dtpBillDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpBillDate.Location = new System.Drawing.Point(384, 14);
            this.dtpBillDate.Name = "dtpBillDate";
            this.dtpBillDate.Size = new System.Drawing.Size(145, 20);
            this.dtpBillDate.TabIndex = 1;
            this.dtpBillDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpBillDate_KeyDown);
            // 
            // dgListItem
            // 
            this.dgListItem.AllowUserToAddRows = false;
            this.dgListItem.AllowUserToDeleteRows = false;
            this.dgListItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgListItem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Description,
            this.SUOM,
            this.SStockFK,
            this.SItemNo,
            this.SUOMNo,
            this.FkRateSettingNo,
            this.SGodownNo});
            this.dgListItem.Location = new System.Drawing.Point(0, 28);
            this.dgListItem.Name = "dgListItem";
            this.dgListItem.ReadOnly = true;
            this.dgListItem.Size = new System.Drawing.Size(547, 159);
            this.dgListItem.TabIndex = 0;
            this.dgListItem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgListItem_KeyDown);
            // 
            // label3
            // 
            this.label3.BackColor = OM.ThemeColor.Body_Header_Color;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(553, 25);
            this.label3.TabIndex = 1;
            this.label3.Text = "Select Item";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Sr";
            this.Column1.HeaderText = "Sr";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 30;
            // 
            // ItemName
            // 
            this.ItemName.DataPropertyName = "ItemName";
            this.ItemName.HeaderText = "Description";
            this.ItemName.Name = "ItemName";
            this.ItemName.Width = 250;
            // 
            // Quantity
            // 
            this.Quantity.DataPropertyName = "Quantity";
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Quantity.DefaultCellStyle = dataGridViewCellStyle26;
            this.Quantity.HeaderText = "Qty";
            this.Quantity.Name = "Quantity";
            this.Quantity.Width = 75;
            // 
            // UOM
            // 
            this.UOM.DataPropertyName = "UOMName";
            this.UOM.HeaderText = "UOM";
            this.UOM.Name = "UOM";
            this.UOM.ReadOnly = true;
            this.UOM.Width = 50;
            // 
            // SaleRate
            // 
            this.SaleRate.DataPropertyName = "MRP";
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.SaleRate.DefaultCellStyle = dataGridViewCellStyle27;
            this.SaleRate.HeaderText = "MRP";
            this.SaleRate.Name = "SaleRate";
            this.SaleRate.ReadOnly = true;
            this.SaleRate.Visible = false;
            // 
            // Rate
            // 
            this.Rate.DataPropertyName = "Rate";
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Rate.DefaultCellStyle = dataGridViewCellStyle28;
            this.Rate.HeaderText = "Sale Rate";
            this.Rate.Name = "Rate";
            this.Rate.ReadOnly = true;
            this.Rate.Visible = false;
            // 
            // PurchaseRate
            // 
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PurchaseRate.DefaultCellStyle = dataGridViewCellStyle29;
            this.PurchaseRate.HeaderText = "Purchase Rate";
            this.PurchaseRate.Name = "PurchaseRate";
            this.PurchaseRate.ReadOnly = true;
            this.PurchaseRate.Visible = false;
            this.PurchaseRate.Width = 120;
            // 
            // NetRate
            // 
            this.NetRate.DataPropertyName = "NetRate";
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.NetRate.DefaultCellStyle = dataGridViewCellStyle30;
            this.NetRate.HeaderText = "NetRate";
            this.NetRate.Name = "NetRate";
            this.NetRate.ReadOnly = true;
            this.NetRate.Visible = false;
            this.NetRate.Width = 65;
            // 
            // DiscPerce
            // 
            this.DiscPerce.DataPropertyName = "DiscPercentage";
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DiscPerce.DefaultCellStyle = dataGridViewCellStyle31;
            this.DiscPerce.HeaderText = "Disc%";
            this.DiscPerce.Name = "DiscPerce";
            this.DiscPerce.Visible = false;
            this.DiscPerce.Width = 55;
            // 
            // DiscAmt
            // 
            this.DiscAmt.DataPropertyName = "DiscAmount";
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DiscAmt.DefaultCellStyle = dataGridViewCellStyle32;
            this.DiscAmt.HeaderText = "DiscAmt";
            this.DiscAmt.Name = "DiscAmt";
            this.DiscAmt.ReadOnly = true;
            this.DiscAmt.Visible = false;
            this.DiscAmt.Width = 65;
            // 
            // DiscRupees
            // 
            this.DiscRupees.DataPropertyName = "DiscRupees";
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DiscRupees.DefaultCellStyle = dataGridViewCellStyle33;
            this.DiscRupees.HeaderText = "DiscRupees";
            this.DiscRupees.Name = "DiscRupees";
            this.DiscRupees.Visible = false;
            // 
            // DiscPercentage2
            // 
            this.DiscPercentage2.DataPropertyName = "DiscPercentage2";
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DiscPercentage2.DefaultCellStyle = dataGridViewCellStyle34;
            this.DiscPercentage2.HeaderText = "Disc % 2";
            this.DiscPercentage2.Name = "DiscPercentage2";
            this.DiscPercentage2.Visible = false;
            // 
            // DiscAmount2
            // 
            this.DiscAmount2.DataPropertyName = "DiscAmount2";
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DiscAmount2.DefaultCellStyle = dataGridViewCellStyle35;
            this.DiscAmount2.HeaderText = "DiscAmt2";
            this.DiscAmount2.Name = "DiscAmount2";
            this.DiscAmount2.Visible = false;
            // 
            // DiscRupees2
            // 
            this.DiscRupees2.DataPropertyName = "DiscRupees2";
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DiscRupees2.DefaultCellStyle = dataGridViewCellStyle36;
            this.DiscRupees2.HeaderText = "DiscRupees2";
            this.DiscRupees2.Name = "DiscRupees2";
            this.DiscRupees2.Visible = false;
            // 
            // NetAmt
            // 
            this.NetAmt.DataPropertyName = "NetAmt";
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.NetAmt.DefaultCellStyle = dataGridViewCellStyle37;
            this.NetAmt.HeaderText = "NetAmt";
            this.NetAmt.Name = "NetAmt";
            this.NetAmt.ReadOnly = true;
            this.NetAmt.Visible = false;
            this.NetAmt.Width = 85;
            // 
            // Amount
            // 
            this.Amount.DataPropertyName = "Amount";
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Amount.DefaultCellStyle = dataGridViewCellStyle38;
            this.Amount.HeaderText = "Amount";
            this.Amount.Name = "Amount";
            this.Amount.ReadOnly = true;
            this.Amount.Visible = false;
            this.Amount.Width = 85;
            // 
            // Barcode
            // 
            this.Barcode.DataPropertyName = "Barcode";
            this.Barcode.HeaderText = "Barcode";
            this.Barcode.Name = "Barcode";
            this.Barcode.Visible = false;
            // 
            // PkStockTrnNo
            // 
            this.PkStockTrnNo.DataPropertyName = "PkStockTrnNo";
            this.PkStockTrnNo.HeaderText = "PkStockTrnNo";
            this.PkStockTrnNo.Name = "PkStockTrnNo";
            this.PkStockTrnNo.Visible = false;
            // 
            // PkSrNo
            // 
            this.PkSrNo.DataPropertyName = "PkStockBarcodeNo";
            this.PkSrNo.HeaderText = "PkBarcodeNo";
            this.PkSrNo.Name = "PkSrNo";
            this.PkSrNo.Visible = false;
            // 
            // PkVoucherNo
            // 
            this.PkVoucherNo.DataPropertyName = "PkVoucherNo";
            this.PkVoucherNo.HeaderText = "PkVoucherNo";
            this.PkVoucherNo.Name = "PkVoucherNo";
            this.PkVoucherNo.Visible = false;
            // 
            // ItemNo
            // 
            this.ItemNo.DataPropertyName = "ItemNo";
            this.ItemNo.HeaderText = "ItemNo";
            this.ItemNo.Name = "ItemNo";
            this.ItemNo.Visible = false;
            // 
            // UOMNo
            // 
            this.UOMNo.DataPropertyName = "UOMNo";
            this.UOMNo.HeaderText = "UOMNo";
            this.UOMNo.Name = "UOMNo";
            this.UOMNo.Visible = false;
            // 
            // PkRateSettingNo
            // 
            this.PkRateSettingNo.HeaderText = "RateSettingNo";
            this.PkRateSettingNo.Name = "PkRateSettingNo";
            this.PkRateSettingNo.Visible = false;
            // 
            // PkItemTaxInfo
            // 
            this.PkItemTaxInfo.HeaderText = "ItemTaxInfoNo";
            this.PkItemTaxInfo.Name = "PkItemTaxInfo";
            this.PkItemTaxInfo.Visible = false;
            // 
            // StockFactor
            // 
            this.StockFactor.DataPropertyName = "StockConversion";
            this.StockFactor.HeaderText = "StockFactor";
            this.StockFactor.Name = "StockFactor";
            this.StockFactor.Visible = false;
            // 
            // ActualQty
            // 
            this.ActualQty.HeaderText = "ActualQty";
            this.ActualQty.Name = "ActualQty";
            this.ActualQty.Visible = false;
            // 
            // MKTQuantity
            // 
            this.MKTQuantity.DataPropertyName = "MKTQty";
            this.MKTQuantity.HeaderText = "MKTQty";
            this.MKTQuantity.Name = "MKTQuantity";
            // 
            // TaxPerce
            // 
            this.TaxPerce.DataPropertyName = "TaxPercentage";
            this.TaxPerce.HeaderText = "TaxPerce";
            this.TaxPerce.Name = "TaxPerce";
            this.TaxPerce.Visible = false;
            // 
            // TaxAmount
            // 
            this.TaxAmount.DataPropertyName = "TaxAmount";
            this.TaxAmount.HeaderText = "TaxAmt";
            this.TaxAmount.Name = "TaxAmount";
            this.TaxAmount.Visible = false;
            // 
            // FkStockGodownNo
            // 
            this.FkStockGodownNo.DataPropertyName = "FkStockGodownNo";
            this.FkStockGodownNo.HeaderText = "FkStockGodownNo";
            this.FkStockGodownNo.Name = "FkStockGodownNo";
            this.FkStockGodownNo.Visible = false;
            // 
            // StockCompanyNo
            // 
            this.StockCompanyNo.HeaderText = "StockCompanyNo";
            this.StockCompanyNo.Name = "StockCompanyNo";
            this.StockCompanyNo.Visible = false;
            // 
            // PkStockTrnNoTo
            // 
            this.PkStockTrnNoTo.HeaderText = "PkStockTrnNoTo";
            this.PkStockTrnNoTo.Name = "PkStockTrnNoTo";
            this.PkStockTrnNoTo.Visible = false;
            // 
            // PkStockGodownNoTo
            // 
            this.PkStockGodownNoTo.HeaderText = "PkStockGodownNoTo";
            this.PkStockGodownNoTo.Name = "PkStockGodownNoTo";
            this.PkStockGodownNoTo.Visible = false;
            // 
            // TempMRP
            // 
            this.TempMRP.DataPropertyName = "TempMRP";
            this.TempMRP.HeaderText = "TempMRP";
            this.TempMRP.Name = "TempMRP";
            this.TempMRP.Visible = false;
            // 
            // TempQty
            // 
            this.TempQty.HeaderText = "TempQty";
            this.TempQty.Name = "TempQty";
            this.TempQty.Visible = false;
            // 
            // GodownNoFrom
            // 
            this.GodownNoFrom.HeaderText = "GodownNoFrom";
            this.GodownNoFrom.Name = "GodownNoFrom";
            this.GodownNoFrom.Visible = false;
            // 
            // BtnConvert
            // 
            this.BtnConvert.HeaderText = "Convert";
            this.BtnConvert.Name = "BtnConvert";
            this.BtnConvert.Text = "Convert";
            // 
            // FItemName
            // 
            this.FItemName.HeaderText = "FDecription";
            this.FItemName.Name = "FItemName";
            this.FItemName.ReadOnly = true;
            this.FItemName.Width = 200;
            // 
            // FUOM
            // 
            this.FUOM.HeaderText = "FUOM";
            this.FUOM.Name = "FUOM";
            this.FUOM.ReadOnly = true;
            this.FUOM.Width = 60;
            // 
            // FStockFactor
            // 
            this.FStockFactor.HeaderText = "MKTQty";
            this.FStockFactor.Name = "FStockFactor";
            this.FStockFactor.ReadOnly = true;
            this.FStockFactor.Width = 80;
            // 
            // FConvertStock
            // 
            this.FConvertStock.HeaderText = "ConvertQty";
            this.FConvertStock.Name = "FConvertStock";
            this.FConvertStock.ReadOnly = true;
            // 
            // FItemNo
            // 
            this.FItemNo.HeaderText = "FItemNo";
            this.FItemNo.Name = "FItemNo";
            this.FItemNo.ReadOnly = true;
            this.FItemNo.Visible = false;
            // 
            // FUOMNo
            // 
            this.FUOMNo.HeaderText = "FUOMNo";
            this.FUOMNo.Name = "FUOMNo";
            this.FUOMNo.Visible = false;
            // 
            // FFKRateSettingNo
            // 
            this.FFKRateSettingNo.HeaderText = "FKRateSettingNo";
            this.FFKRateSettingNo.Name = "FFKRateSettingNo";
            this.FFKRateSettingNo.Visible = false;
            // 
            // StatusNo
            // 
            this.StatusNo.HeaderText = "StatusNo";
            this.StatusNo.Name = "StatusNo";
            this.StatusNo.Visible = false;
            // 
            // Description
            // 
            this.Description.HeaderText = "Description";
            this.Description.Name = "Description";
            this.Description.ReadOnly = true;
            this.Description.Width = 300;
            // 
            // SUOM
            // 
            this.SUOM.HeaderText = "UOM";
            this.SUOM.Name = "SUOM";
            this.SUOM.ReadOnly = true;
            // 
            // SStockFK
            // 
            this.SStockFK.HeaderText = "MKTQty";
            this.SStockFK.Name = "SStockFK";
            this.SStockFK.ReadOnly = true;
            // 
            // SItemNo
            // 
            this.SItemNo.HeaderText = "ItemNo";
            this.SItemNo.Name = "SItemNo";
            this.SItemNo.ReadOnly = true;
            this.SItemNo.Visible = false;
            // 
            // SUOMNo
            // 
            this.SUOMNo.HeaderText = "UomNo";
            this.SUOMNo.Name = "SUOMNo";
            this.SUOMNo.ReadOnly = true;
            this.SUOMNo.Visible = false;
            // 
            // FkRateSettingNo
            // 
            this.FkRateSettingNo.HeaderText = "RateSettingNo";
            this.FkRateSettingNo.Name = "FkRateSettingNo";
            this.FkRateSettingNo.ReadOnly = true;
            this.FkRateSettingNo.Visible = false;
            // 
            // SGodownNo
            // 
            this.SGodownNo.HeaderText = "GodownNo";
            this.SGodownNo.Name = "SGodownNo";
            this.SGodownNo.ReadOnly = true;
            this.SGodownNo.Visible = false;
            // 
            // StockConvert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1233, 553);
            this.Controls.Add(this.pnlMain);
            this.Name = "StockConvert";
            this.Text = "Stock Transfer";
            this.Load += new System.EventHandler(this.StockConvert_Load);
            ((System.ComponentModel.ISupportInitialize)(this.EP)).EndInit();
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.pnlStockConvert.ResumeLayout(false);
            this.pnlSearch.ResumeLayout(false);
            this.pnlSearch.PerformLayout();
            this.pnlGroup1.ResumeLayout(false);
            this.pnlGroup2.ResumeLayout(false);
            this.pnlItemName.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgItemList)).EndInit();
            this.pnlUOM.ResumeLayout(false);
            this.pnlRate.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgListItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtInvNo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpBillDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtRemark;
        private System.Windows.Forms.ErrorProvider EP;
        private System.Windows.Forms.Button BtnSearch;
        private System.Windows.Forms.Button BtnUpdate;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.Button btnLast;
        private System.Windows.Forms.Button btnFirst;
        private System.Windows.Forms.Button BtnExit;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Button BtnNew;
        private System.Windows.Forms.Button BtnCancel;
        private JitControls.OMBPanel pnlMain;
        private System.Windows.Forms.Panel pnlSearch;
        private System.Windows.Forms.Button btnCancelSearch;
        private System.Windows.Forms.Label lblSearch;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.Panel pnlGroup1;
        private System.Windows.Forms.ListBox lstGroup1;
        private System.Windows.Forms.Panel pnlGroup2;
        private System.Windows.Forms.ListBox lstGroup2;
        private System.Windows.Forms.Panel pnlItemName;
        private System.Windows.Forms.DataGridView dgItemList;
        private System.Windows.Forms.Panel pnlUOM;
        private System.Windows.Forms.ListBox lstUOM;
        private System.Windows.Forms.Panel pnlRate;
        private System.Windows.Forms.ListBox lstRate;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView dgBill;
        private System.Windows.Forms.DataGridViewTextBoxColumn iItemNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn iItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn iRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn iUOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn iMRP;
        private System.Windows.Forms.DataGridViewTextBoxColumn iStock;
        private System.Windows.Forms.DataGridViewTextBoxColumn iUOMStk;
        private System.Windows.Forms.DataGridViewTextBoxColumn iSaleTax;
        private System.Windows.Forms.DataGridViewTextBoxColumn iPurTax;
        private System.Windows.Forms.DataGridViewTextBoxColumn iCompany;
        private System.Windows.Forms.DataGridViewTextBoxColumn iBarcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn iRateSettingNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMDefault;
        private System.Windows.Forms.DataGridViewTextBoxColumn PurRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn GowodnNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscountType;
        private JitControls.OMBPanel pnlStockConvert;
        private System.Windows.Forms.DataGridView dgListItem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn SaleRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rate;
        private System.Windows.Forms.DataGridViewTextBoxColumn PurchaseRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn NetRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscPerce;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscAmt;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscRupees;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscPercentage2;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscAmount2;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscRupees2;
        private System.Windows.Forms.DataGridViewTextBoxColumn NetAmt;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Barcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkStockTrnNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkSrNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkVoucherNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkRateSettingNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkItemTaxInfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn StockFactor;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActualQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn MKTQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaxPerce;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaxAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn FkStockGodownNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn StockCompanyNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkStockTrnNoTo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkStockGodownNoTo;
        private System.Windows.Forms.DataGridViewTextBoxColumn TempMRP;
        private System.Windows.Forms.DataGridViewTextBoxColumn TempQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn GodownNoFrom;
        private System.Windows.Forms.DataGridViewButtonColumn BtnConvert;
        private System.Windows.Forms.DataGridViewTextBoxColumn FItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn FUOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn FStockFactor;
        private System.Windows.Forms.DataGridViewTextBoxColumn FConvertStock;
        private System.Windows.Forms.DataGridViewTextBoxColumn FItemNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn FUOMNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn FFKRateSettingNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn StatusNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn SUOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn SStockFK;
        private System.Windows.Forms.DataGridViewTextBoxColumn SItemNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn SUOMNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn FkRateSettingNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn SGodownNo;
    }
}