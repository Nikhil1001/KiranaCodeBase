﻿namespace Kirana.Utilities
{
    partial class ItemTransferFromGlobalToLocal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lstBrand = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.txtBalance = new System.Windows.Forms.TextBox();
            this.txtShifted = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbBrand = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbDepartment = new System.Windows.Forms.ComboBox();
            this.cmbCategory = new System.Windows.Forms.ComboBox();
            this.gvRateSetting = new System.Windows.Forms.DataGridView();
            this.BtnSave = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblWait = new System.Windows.Forms.Label();
            this.lblChkHelp = new System.Windows.Forms.Label();
            this.ChkSelect = new System.Windows.Forms.CheckBox();
            this.pnlPTax = new System.Windows.Forms.Panel();
            this.lstPTax = new System.Windows.Forms.ListBox();
            this.pnlSTax = new System.Windows.Forms.Panel();
            this.lstSTax = new System.Windows.Forms.ListBox();
            this.lblMsg = new JitControls.OMLabel();
            this.SrNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BrandName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BarCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShortCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MRP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ASaleRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BSaleRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CSaleRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DSaleRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ESaleRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MKTQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PurRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RateVariation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsActive = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.StockConversion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkSrNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BarCodeNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Chk = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.HidChk = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.UomNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LanguageName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LangFullDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BrandNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMPrimary = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMDefault = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FKStockDeptNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FkCategoryNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FkDepartmentNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GroupNo1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShortName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsFixBarcode = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.CategoryName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeptName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GlobalCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Svat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pvat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OpStock = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MfgCompName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TRSelect = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.MfgCompNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PTax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gvRateSetting)).BeginInit();
            this.panel1.SuspendLayout();
            this.pnlPTax.SuspendLayout();
            this.pnlSTax.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstBrand
            // 
            this.lstBrand.FormattingEnabled = true;
            this.lstBrand.Location = new System.Drawing.Point(13, 13);
            this.lstBrand.Name = "lstBrand";
            this.lstBrand.Size = new System.Drawing.Size(218, 394);
            this.lstBrand.TabIndex = 100;
            this.lstBrand.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstBrand_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(13, 419);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Total";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(13, 480);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Balance";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(13, 450);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Active";
            // 
            // txtTotal
            // 
            this.txtTotal.Enabled = false;
            this.txtTotal.Location = new System.Drawing.Point(99, 416);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(130, 20);
            this.txtTotal.TabIndex = 5;
            this.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtBalance
            // 
            this.txtBalance.Enabled = false;
            this.txtBalance.Location = new System.Drawing.Point(99, 477);
            this.txtBalance.Name = "txtBalance";
            this.txtBalance.Size = new System.Drawing.Size(130, 20);
            this.txtBalance.TabIndex = 7;
            this.txtBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtShifted
            // 
            this.txtShifted.Enabled = false;
            this.txtShifted.Location = new System.Drawing.Point(99, 447);
            this.txtShifted.Name = "txtShifted";
            this.txtShifted.Size = new System.Drawing.Size(130, 20);
            this.txtShifted.TabIndex = 6;
            this.txtShifted.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Brand";
            // 
            // cmbBrand
            // 
            this.cmbBrand.FormattingEnabled = true;
            this.cmbBrand.Location = new System.Drawing.Point(107, 14);
            this.cmbBrand.Name = "cmbBrand";
            this.cmbBrand.Size = new System.Drawing.Size(236, 21);
            this.cmbBrand.TabIndex = 0;
            this.cmbBrand.Leave += new System.EventHandler(this.cmbBrand_Leave);
            this.cmbBrand.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbBrand_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(390, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Barcode";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(479, 15);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(236, 20);
            this.txtBarcode.TabIndex = 1;
            this.txtBarcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarcode_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Department";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(390, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Category";
            // 
            // cmbDepartment
            // 
            this.cmbDepartment.FormattingEnabled = true;
            this.cmbDepartment.Location = new System.Drawing.Point(107, 56);
            this.cmbDepartment.Name = "cmbDepartment";
            this.cmbDepartment.Size = new System.Drawing.Size(236, 21);
            this.cmbDepartment.TabIndex = 2;
            this.cmbDepartment.Leave += new System.EventHandler(this.cmbDepartment_Leave);
            this.cmbDepartment.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbDepartment_KeyDown);
            // 
            // cmbCategory
            // 
            this.cmbCategory.FormattingEnabled = true;
            this.cmbCategory.Location = new System.Drawing.Point(479, 56);
            this.cmbCategory.Name = "cmbCategory";
            this.cmbCategory.Size = new System.Drawing.Size(236, 21);
            this.cmbCategory.TabIndex = 3;
            this.cmbCategory.Leave += new System.EventHandler(this.cmbCategory_Leave);
            this.cmbCategory.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbCategory_KeyDown);
            // 
            // gvRateSetting
            // 
            this.gvRateSetting.AllowUserToAddRows = false;
            this.gvRateSetting.AllowUserToDeleteRows = false;
            this.gvRateSetting.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvRateSetting.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SrNo,
            this.BrandName,
            this.ItemName,
            this.Date,
            this.BarCode,
            this.ShortCode,
            this.MRP,
            this.UOMName,
            this.ASaleRate,
            this.BSaleRate,
            this.CSaleRate,
            this.DSaleRate,
            this.ESaleRate,
            this.MKTQty,
            this.PurRate,
            this.RateVariation,
            this.IsActive,
            this.StockConversion,
            this.PkSrNo,
            this.BarCodeNo,
            this.ItemNo,
            this.Chk,
            this.HidChk,
            this.UomNo,
            this.LanguageName,
            this.LangFullDesc,
            this.BrandNo,
            this.UOMPrimary,
            this.UOMDefault,
            this.FKStockDeptNo,
            this.FkCategoryNo,
            this.FkDepartmentNo,
            this.GroupNo1,
            this.ShortName,
            this.IsFixBarcode,
            this.CategoryName,
            this.DeptName,
            this.GlobalCode,
            this.Svat,
            this.Pvat,
            this.OpStock,
            this.MfgCompName,
            this.TRSelect,
            this.MfgCompNo,
            this.STax,
            this.PTax});
            this.gvRateSetting.Location = new System.Drawing.Point(21, 96);
            this.gvRateSetting.Name = "gvRateSetting";
            this.gvRateSetting.Size = new System.Drawing.Size(1000, 341);
            this.gvRateSetting.TabIndex = 10;
            this.gvRateSetting.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.gvRateSetting_CellValueChanged);
            this.gvRateSetting.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.gvRateSetting_CellFormatting);
            this.gvRateSetting.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.gvRateSetting_CellEndEdit);
            this.gvRateSetting.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.gvRateSetting_EditingControlShowing);
            this.gvRateSetting.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gvRateSetting_KeyDown);
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(21, 443);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(166, 23);
            this.BtnSave.TabIndex = 8;
            this.BtnSave.Text = "Apply Changes (F8)";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(204, 443);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 9;
            this.BtnCancel.Text = "Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(296, 443);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 11;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblWait);
            this.panel1.Controls.Add(this.lblChkHelp);
            this.panel1.Controls.Add(this.ChkSelect);
            this.panel1.Controls.Add(this.pnlPTax);
            this.panel1.Controls.Add(this.pnlSTax);
            this.panel1.Controls.Add(this.btnExit);
            this.panel1.Controls.Add(this.BtnCancel);
            this.panel1.Controls.Add(this.BtnSave);
            this.panel1.Controls.Add(this.gvRateSetting);
            this.panel1.Controls.Add(this.cmbCategory);
            this.panel1.Controls.Add(this.cmbDepartment);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtBarcode);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.cmbBrand);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(245, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1026, 486);
            this.panel1.TabIndex = 0;
            // 
            // lblWait
            // 
            this.lblWait.Location = new System.Drawing.Point(389, 455);
            this.lblWait.Name = "lblWait";
            this.lblWait.Size = new System.Drawing.Size(366, 27);
            this.lblWait.TabIndex = 60051;
            this.lblWait.Text = "label8";
            this.lblWait.Visible = false;
            // 
            // lblChkHelp
            // 
            this.lblChkHelp.Location = new System.Drawing.Point(536, 444);
            this.lblChkHelp.Name = "lblChkHelp";
            this.lblChkHelp.Size = new System.Drawing.Size(141, 23);
            this.lblChkHelp.TabIndex = 60050;
            this.lblChkHelp.Text = "Change VAT - Press F4. ";
            // 
            // ChkSelect
            // 
            this.ChkSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ChkSelect.AutoSize = true;
            this.ChkSelect.BackColor = System.Drawing.Color.Transparent;
            this.ChkSelect.Location = new System.Drawing.Point(692, 443);
            this.ChkSelect.Name = "ChkSelect";
            this.ChkSelect.Size = new System.Drawing.Size(85, 17);
            this.ChkSelect.TabIndex = 514;
            this.ChkSelect.Text = "SelectAll(F2)";
            this.ChkSelect.UseVisualStyleBackColor = false;
            this.ChkSelect.CheckedChanged += new System.EventHandler(this.ChkSelect_CheckedChanged);
            // 
            // pnlPTax
            // 
            this.pnlPTax.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.pnlPTax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPTax.Controls.Add(this.lstPTax);
            this.pnlPTax.Location = new System.Drawing.Point(618, 216);
            this.pnlPTax.Name = "pnlPTax";
            this.pnlPTax.Size = new System.Drawing.Size(118, 50);
            this.pnlPTax.TabIndex = 5547;
            this.pnlPTax.Visible = false;
            // 
            // lstPTax
            // 
            this.lstPTax.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstPTax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstPTax.FormattingEnabled = true;
            this.lstPTax.Location = new System.Drawing.Point(8, 8);
            this.lstPTax.Name = "lstPTax";
            this.lstPTax.Size = new System.Drawing.Size(101, 28);
            this.lstPTax.TabIndex = 516;
            this.lstPTax.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstPTax_KeyDown);
            // 
            // pnlSTax
            // 
            this.pnlSTax.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.pnlSTax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSTax.Controls.Add(this.lstSTax);
            this.pnlSTax.Location = new System.Drawing.Point(582, 160);
            this.pnlSTax.Name = "pnlSTax";
            this.pnlSTax.Size = new System.Drawing.Size(118, 50);
            this.pnlSTax.TabIndex = 5546;
            this.pnlSTax.Visible = false;
            // 
            // lstSTax
            // 
            this.lstSTax.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstSTax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstSTax.FormattingEnabled = true;
            this.lstSTax.Location = new System.Drawing.Point(8, 8);
            this.lstSTax.Name = "lstSTax";
            this.lstSTax.Size = new System.Drawing.Size(101, 28);
            this.lstSTax.TabIndex = 516;
            this.lstSTax.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstTax_KeyDown);
            // 
            // lblMsg
            // 
            this.lblMsg.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.lblMsg.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMsg.CornerRadius = 3;
            this.lblMsg.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg.ForeColor = System.Drawing.Color.Maroon;
            this.lblMsg.GradientBottom = OM.ThemeColor.Panle_Back_Color;
            this.lblMsg.GradientMiddle = System.Drawing.Color.White;
            this.lblMsg.GradientShow = true;
            this.lblMsg.GradientTop = OM.ThemeColor.Panle_Back_Color;
            this.lblMsg.Location = new System.Drawing.Point(365, 261);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(481, 52);
            this.lblMsg.TabIndex = 507;
            this.lblMsg.Text = "label4";
            this.lblMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMsg.Visible = false;
            // 
            // SrNo
            // 
            this.SrNo.DataPropertyName = "SrNo";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.SrNo.DefaultCellStyle = dataGridViewCellStyle1;
            this.SrNo.HeaderText = "Sr";
            this.SrNo.Name = "SrNo";
            this.SrNo.ReadOnly = true;
            this.SrNo.Width = 40;
            // 
            // BrandName
            // 
            this.BrandName.DataPropertyName = "StockGroupName";
            this.BrandName.HeaderText = "BrandName";
            this.BrandName.Name = "BrandName";
            this.BrandName.ReadOnly = true;
            // 
            // ItemName
            // 
            this.ItemName.DataPropertyName = "ItemName";
            this.ItemName.HeaderText = "Item";
            this.ItemName.Name = "ItemName";
            this.ItemName.Width = 200;
            // 
            // Date
            // 
            this.Date.DataPropertyName = "FromDate";
            dataGridViewCellStyle2.NullValue = null;
            this.Date.DefaultCellStyle = dataGridViewCellStyle2;
            this.Date.HeaderText = "Date";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            this.Date.Visible = false;
            this.Date.Width = 5;
            // 
            // BarCode
            // 
            this.BarCode.DataPropertyName = "Barcode";
            this.BarCode.HeaderText = "BarCode";
            this.BarCode.Name = "BarCode";
            this.BarCode.Width = 120;
            // 
            // ShortCode
            // 
            this.ShortCode.DataPropertyName = "ShortCode";
            this.ShortCode.HeaderText = "ShortCode";
            this.ShortCode.Name = "ShortCode";
            this.ShortCode.Width = 120;
            // 
            // MRP
            // 
            this.MRP.DataPropertyName = "MRP";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.MRP.DefaultCellStyle = dataGridViewCellStyle3;
            this.MRP.HeaderText = "MRP";
            this.MRP.Name = "MRP";
            this.MRP.Width = 80;
            // 
            // UOMName
            // 
            this.UOMName.DataPropertyName = "UOMName";
            this.UOMName.HeaderText = "UOM";
            this.UOMName.Name = "UOMName";
            this.UOMName.ReadOnly = true;
            this.UOMName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.UOMName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.UOMName.Visible = false;
            this.UOMName.Width = 80;
            // 
            // ASaleRate
            // 
            this.ASaleRate.DataPropertyName = "ASaleRate";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ASaleRate.DefaultCellStyle = dataGridViewCellStyle4;
            this.ASaleRate.HeaderText = "ASaleRate";
            this.ASaleRate.Name = "ASaleRate";
            this.ASaleRate.Width = 80;
            // 
            // BSaleRate
            // 
            this.BSaleRate.DataPropertyName = "BSaleRate";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.BSaleRate.DefaultCellStyle = dataGridViewCellStyle5;
            this.BSaleRate.HeaderText = "BSaleRate";
            this.BSaleRate.Name = "BSaleRate";
            this.BSaleRate.Width = 80;
            // 
            // CSaleRate
            // 
            this.CSaleRate.DataPropertyName = "CSaleRate";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.CSaleRate.DefaultCellStyle = dataGridViewCellStyle6;
            this.CSaleRate.HeaderText = "CSaleRate";
            this.CSaleRate.Name = "CSaleRate";
            this.CSaleRate.Visible = false;
            this.CSaleRate.Width = 80;
            // 
            // DSaleRate
            // 
            this.DSaleRate.DataPropertyName = "DSaleRate";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DSaleRate.DefaultCellStyle = dataGridViewCellStyle7;
            this.DSaleRate.HeaderText = "DSaleRate";
            this.DSaleRate.Name = "DSaleRate";
            this.DSaleRate.Visible = false;
            this.DSaleRate.Width = 80;
            // 
            // ESaleRate
            // 
            this.ESaleRate.DataPropertyName = "ESaleRate";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ESaleRate.DefaultCellStyle = dataGridViewCellStyle8;
            this.ESaleRate.HeaderText = "ESaleRate";
            this.ESaleRate.Name = "ESaleRate";
            this.ESaleRate.Visible = false;
            this.ESaleRate.Width = 80;
            // 
            // MKTQty
            // 
            this.MKTQty.DataPropertyName = "MKTQty";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.MKTQty.DefaultCellStyle = dataGridViewCellStyle9;
            this.MKTQty.HeaderText = "MKTQty";
            this.MKTQty.Name = "MKTQty";
            this.MKTQty.Visible = false;
            this.MKTQty.Width = 60;
            // 
            // PurRate
            // 
            this.PurRate.DataPropertyName = "PurRate";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PurRate.DefaultCellStyle = dataGridViewCellStyle10;
            this.PurRate.HeaderText = "PurRate";
            this.PurRate.Name = "PurRate";
            this.PurRate.Width = 80;
            // 
            // RateVariation
            // 
            this.RateVariation.DataPropertyName = "PerOfRateVariation";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.RateVariation.DefaultCellStyle = dataGridViewCellStyle11;
            this.RateVariation.HeaderText = "Rate Variation";
            this.RateVariation.Name = "RateVariation";
            this.RateVariation.Visible = false;
            this.RateVariation.Width = 50;
            // 
            // IsActive
            // 
            this.IsActive.DataPropertyName = "IsActive";
            this.IsActive.HeaderText = "Active";
            this.IsActive.Name = "IsActive";
            this.IsActive.Visible = false;
            // 
            // StockConversion
            // 
            this.StockConversion.DataPropertyName = "StockConversion";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.StockConversion.DefaultCellStyle = dataGridViewCellStyle12;
            this.StockConversion.HeaderText = "Stock Conversion";
            this.StockConversion.Name = "StockConversion";
            this.StockConversion.ReadOnly = true;
            this.StockConversion.Visible = false;
            // 
            // PkSrNo
            // 
            this.PkSrNo.DataPropertyName = "PkSrNo";
            this.PkSrNo.HeaderText = "PkSrNo";
            this.PkSrNo.Name = "PkSrNo";
            this.PkSrNo.ReadOnly = true;
            this.PkSrNo.Visible = false;
            this.PkSrNo.Width = 20;
            // 
            // BarCodeNo
            // 
            this.BarCodeNo.DataPropertyName = "PkStockBarcodeNo";
            this.BarCodeNo.HeaderText = "BarCodeNo";
            this.BarCodeNo.Name = "BarCodeNo";
            this.BarCodeNo.ReadOnly = true;
            this.BarCodeNo.Visible = false;
            // 
            // ItemNo
            // 
            this.ItemNo.DataPropertyName = "ItemNo";
            this.ItemNo.HeaderText = "ItemNo";
            this.ItemNo.Name = "ItemNo";
            this.ItemNo.Visible = false;
            // 
            // Chk
            // 
            this.Chk.DataPropertyName = "chk";
            this.Chk.HeaderText = ".";
            this.Chk.Name = "Chk";
            this.Chk.Visible = false;
            this.Chk.Width = 50;
            // 
            // HidChk
            // 
            this.HidChk.DataPropertyName = "HidChk";
            this.HidChk.HeaderText = "Hidden";
            this.HidChk.Name = "HidChk";
            this.HidChk.Visible = false;
            // 
            // UomNo
            // 
            this.UomNo.DataPropertyName = "UOMNo";
            this.UomNo.HeaderText = "UOMNo";
            this.UomNo.Name = "UomNo";
            this.UomNo.Visible = false;
            // 
            // LanguageName
            // 
            this.LanguageName.DataPropertyName = "LanguageName";
            this.LanguageName.HeaderText = "LanguageName";
            this.LanguageName.Name = "LanguageName";
            this.LanguageName.ReadOnly = true;
            this.LanguageName.Visible = false;
            this.LanguageName.Width = 150;
            // 
            // LangFullDesc
            // 
            this.LangFullDesc.DataPropertyName = "LangFullDesc";
            this.LangFullDesc.HeaderText = "LangFullDesc";
            this.LangFullDesc.Name = "LangFullDesc";
            this.LangFullDesc.Visible = false;
            this.LangFullDesc.Width = 200;
            // 
            // BrandNo
            // 
            this.BrandNo.DataPropertyName = "StockGroupNo";
            this.BrandNo.HeaderText = "BrandNo";
            this.BrandNo.Name = "BrandNo";
            this.BrandNo.Visible = false;
            // 
            // UOMPrimary
            // 
            this.UOMPrimary.DataPropertyName = "UOMPrimary";
            this.UOMPrimary.HeaderText = "UOMPrimary";
            this.UOMPrimary.Name = "UOMPrimary";
            this.UOMPrimary.Visible = false;
            // 
            // UOMDefault
            // 
            this.UOMDefault.DataPropertyName = "UOMDefault";
            this.UOMDefault.HeaderText = "UOMDefault";
            this.UOMDefault.Name = "UOMDefault";
            this.UOMDefault.Visible = false;
            // 
            // FKStockDeptNo
            // 
            this.FKStockDeptNo.DataPropertyName = "FKStockDeptNo";
            this.FKStockDeptNo.HeaderText = "FKStockDeptNo";
            this.FKStockDeptNo.Name = "FKStockDeptNo";
            this.FKStockDeptNo.Visible = false;
            // 
            // FkCategoryNo
            // 
            this.FkCategoryNo.DataPropertyName = "FkCategoryNo";
            this.FkCategoryNo.HeaderText = "FkCategoryNo";
            this.FkCategoryNo.Name = "FkCategoryNo";
            this.FkCategoryNo.Visible = false;
            // 
            // FkDepartmentNo
            // 
            this.FkDepartmentNo.DataPropertyName = "FkDepartmentNo";
            this.FkDepartmentNo.HeaderText = "FkDepartmentNo";
            this.FkDepartmentNo.Name = "FkDepartmentNo";
            this.FkDepartmentNo.Visible = false;
            // 
            // GroupNo1
            // 
            this.GroupNo1.DataPropertyName = "GroupNo1";
            this.GroupNo1.HeaderText = "GroupNo1";
            this.GroupNo1.Name = "GroupNo1";
            this.GroupNo1.Visible = false;
            // 
            // ShortName
            // 
            this.ShortName.DataPropertyName = "ItemShortCode";
            this.ShortName.HeaderText = "ShortName";
            this.ShortName.Name = "ShortName";
            this.ShortName.Visible = false;
            // 
            // IsFixBarcode
            // 
            this.IsFixBarcode.DataPropertyName = "IsFixedBarcode";
            this.IsFixBarcode.HeaderText = "IsFixBarcode";
            this.IsFixBarcode.Name = "IsFixBarcode";
            this.IsFixBarcode.Visible = false;
            // 
            // CategoryName
            // 
            this.CategoryName.HeaderText = "CategoryName";
            this.CategoryName.Name = "CategoryName";
            this.CategoryName.Visible = false;
            // 
            // DeptName
            // 
            this.DeptName.HeaderText = "DeptName";
            this.DeptName.Name = "DeptName";
            this.DeptName.Visible = false;
            // 
            // GlobalCode
            // 
            this.GlobalCode.HeaderText = "GlobalCode";
            this.GlobalCode.Name = "GlobalCode";
            this.GlobalCode.Visible = false;
            // 
            // Svat
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Svat.DefaultCellStyle = dataGridViewCellStyle13;
            this.Svat.HeaderText = "SVAT";
            this.Svat.Name = "Svat";
            this.Svat.ReadOnly = true;
            this.Svat.Width = 50;
            // 
            // Pvat
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Pvat.DefaultCellStyle = dataGridViewCellStyle14;
            this.Pvat.HeaderText = "PVAT";
            this.Pvat.Name = "Pvat";
            this.Pvat.ReadOnly = true;
            this.Pvat.Width = 50;
            // 
            // OpStock
            // 
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.OpStock.DefaultCellStyle = dataGridViewCellStyle15;
            this.OpStock.HeaderText = "Stock";
            this.OpStock.Name = "OpStock";
            this.OpStock.Width = 50;
            // 
            // MfgCompName
            // 
            this.MfgCompName.DataPropertyName = "MfgCompName";
            this.MfgCompName.HeaderText = "MfgCompName";
            this.MfgCompName.Name = "MfgCompName";
            this.MfgCompName.ReadOnly = true;
            // 
            // TRSelect
            // 
            this.TRSelect.DataPropertyName = "Select";
            this.TRSelect.FalseValue = "False";
            this.TRSelect.HeaderText = "Select";
            this.TRSelect.Name = "TRSelect";
            this.TRSelect.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.TRSelect.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.TRSelect.TrueValue = "True";
            this.TRSelect.Width = 80;
            // 
            // MfgCompNo
            // 
            this.MfgCompNo.DataPropertyName = "MfgCompNo";
            this.MfgCompNo.HeaderText = "MfgCompNo";
            this.MfgCompNo.Name = "MfgCompNo";
            this.MfgCompNo.ReadOnly = true;
            this.MfgCompNo.Visible = false;
            // 
            // STax
            // 
            this.STax.DataPropertyName = "STax";
            this.STax.HeaderText = "STax";
            this.STax.Name = "STax";
            this.STax.Visible = false;
            // 
            // PTax
            // 
            this.PTax.DataPropertyName = "PTax";
            this.PTax.HeaderText = "PTax";
            this.PTax.Name = "PTax";
            this.PTax.Visible = false;
            // 
            // ItemTransferFromGlobalToLocal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 510);
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.txtShifted);
            this.Controls.Add(this.txtBalance);
            this.Controls.Add(this.txtTotal);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lstBrand);
            this.Controls.Add(this.panel1);
            this.Name = "ItemTransferFromGlobalToLocal";
            this.Text = "Item Activation";
            this.Load += new System.EventHandler(this.ItemTransferFromGlobalToLocal_Load);
            this.Activated += new System.EventHandler(this.ItemTransferFromGlobalToLocal_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.gvRateSetting)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlPTax.ResumeLayout(false);
            this.pnlSTax.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstBrand;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.TextBox txtBalance;
        private System.Windows.Forms.TextBox txtShifted;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbBrand;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbDepartment;
        private System.Windows.Forms.ComboBox cmbCategory;
        private System.Windows.Forms.DataGridView gvRateSetting;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Panel panel1;
        private JitControls.OMLabel lblMsg;
        private System.Windows.Forms.Panel pnlSTax;
        private System.Windows.Forms.ListBox lstSTax;
        private System.Windows.Forms.Panel pnlPTax;
        private System.Windows.Forms.ListBox lstPTax;
        private System.Windows.Forms.CheckBox ChkSelect;
        private System.Windows.Forms.Label lblChkHelp;
        private System.Windows.Forms.Label lblWait;
        private System.Windows.Forms.DataGridViewTextBoxColumn SrNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn BrandName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn BarCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShortCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn MRP;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ASaleRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn BSaleRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn CSaleRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn DSaleRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ESaleRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn MKTQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn PurRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn RateVariation;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsActive;
        private System.Windows.Forms.DataGridViewTextBoxColumn StockConversion;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkSrNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn BarCodeNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemNo;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Chk;
        private System.Windows.Forms.DataGridViewCheckBoxColumn HidChk;
        private System.Windows.Forms.DataGridViewTextBoxColumn UomNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn LanguageName;
        private System.Windows.Forms.DataGridViewTextBoxColumn LangFullDesc;
        private System.Windows.Forms.DataGridViewTextBoxColumn BrandNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMPrimary;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMDefault;
        private System.Windows.Forms.DataGridViewTextBoxColumn FKStockDeptNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn FkCategoryNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn FkDepartmentNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn GroupNo1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShortName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsFixBarcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn CategoryName;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeptName;
        private System.Windows.Forms.DataGridViewTextBoxColumn GlobalCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn Svat;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pvat;
        private System.Windows.Forms.DataGridViewTextBoxColumn OpStock;
        private System.Windows.Forms.DataGridViewTextBoxColumn MfgCompName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn TRSelect;
        private System.Windows.Forms.DataGridViewTextBoxColumn MfgCompNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn STax;
        private System.Windows.Forms.DataGridViewTextBoxColumn PTax;
    }
}