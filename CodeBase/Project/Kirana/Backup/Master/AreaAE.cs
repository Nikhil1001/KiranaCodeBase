﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Master
{
    /// <summary>
    /// This class is used for Area AE
    /// </summary>
    public partial class AreaAE : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();


        DBMArea dbArea = new DBMArea();
        MArea mArea = new MArea();
        string AreaNm;
        DataTable dtSearch = new DataTable();
        int cntRow;
        long ID;
        
        bool isDoProcess = false;

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public AreaAE()
        {
            InitializeComponent();
        }

        private void AreaAE_Load(object sender, EventArgs e)
        {
            try
            {
                linkLabel1.Visible = false;
                linkLabel2.Visible = false;
                linkLabel3.Visible = false;
                linkLabel4.Visible = false;
                ObjFunction.FillCombo(cmbCountry, "Select CountryNo,CountryName From MCountry Where IsActive='true'");
                ObjFunction.LockButtons(true, this.Controls);
                ObjFunction.LockControls(false, this.Controls);
                AreaNm = "";
                dtSearch = ObjFunction.GetDataView("Select AreaNo From MArea order by AreaName").Table;


                if (dtSearch.Rows.Count > 0)
                {
                    if (Area.RequestAreaNo == 0)
                        ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    else
                        ID = Area.RequestAreaNo;
                    FillControls();
                    SetNavigation();
                }
                setDisplay(true);
                btnNew.Focus();
                KeyDownFormat(this.Controls);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillControls()
        {
            try
            {
                EP.SetError(txtAreaName, "");
                EP.SetError(txtshortName, "");
                EP.SetError(cmbCity, "");
                EP.SetError(cmbCountry, "");
                EP.SetError(cmbState, "");
                EP.SetError(cmbRegion, "");

                mArea = dbArea.ModifyMAreaByID(ID);
                AreaNm = mArea.AreaName.ToUpper();
                txtAreaName.Text = mArea.AreaName.ToUpper();
                txtshortName.Text = mArea.AreaShortCode;
                chkActive.Checked = mArea.IsActive;
                if (chkActive.Checked == true)
                    chkActive.Text = "Yes";
                else
                    chkActive.Text = "No";
                ObjFunction.FillCombo(cmbCountry, "Select CountryNo,CountryName From MCountry Where IsActive='true' or CountryNo=" + mArea.CountryNo + "");
                cmbCountry.SelectedValue = mArea.CountryNo.ToString();
                cmbState.SelectedValue = mArea.StateNo.ToString();
                cmbCity.SelectedValue = mArea.CityNo.ToString();
                cmbRegion.SelectedValue = mArea.RegionNo.ToString();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void SetValue()
        {
            try
            {
                if (Validations() == true)
                {
                    dbArea = new DBMArea();
                    mArea = new MArea();
                    mArea.AreaNo = ID;

                    mArea.AreaName = txtAreaName.Text.Trim();
                    mArea.AreaShortCode = txtshortName.Text.Trim();
                    mArea.IsActive = chkActive.Checked;
                    mArea.CountryNo = ObjFunction.GetComboValue(cmbCountry);
                    mArea.StateNo = ObjFunction.GetComboValue(cmbState);
                    mArea.CityNo = ObjFunction.GetComboValue(cmbCity);
                    mArea.RegionNo = ObjFunction.GetComboValue(cmbRegion);
                    mArea.UserID = DBGetVal.UserID;
                    mArea.UserDate = DBGetVal.ServerTime.Date;
                    mArea.CompanyNo = DBGetVal.CompanyNo;

                    if (dbArea.AddMArea(mArea) == true)
                    {
                        if (ID == 0)
                        {
                            OMMessageBox.Show("Area Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            dtSearch = ObjFunction.GetDataView("Select AreaNo From MArea order by AreaName").Table;
                            ID = ObjQry.ReturnLong("Select Max(AreaNo) From MArea", CommonFunctions.ConStr);
                            SetNavigation();
                            FillControls();
                        }
                        else
                        {
                            OMMessageBox.Show("Area Updated Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            FillControls();
                        }

                        ObjFunction.LockButtons(true, this.Controls);
                        ObjFunction.LockControls(false, this.Controls);
                    }
                    else
                    {
                        OMMessageBox.Show("Area not saved", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            Area.RequestAreaNo = 0;
            this.Close();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            SetValue();
        }

        private bool Validations()
        {
            bool flag = false;
            EP.SetError(txtAreaName, ""); 
            EP.SetError(txtshortName, "");
            EP.SetError(cmbCity, "");
            EP.SetError(cmbCountry, "");
            EP.SetError(cmbState, "");
            EP.SetError(cmbRegion, "");

            if (txtAreaName.Text.Trim() == "")
            {

                EP.SetError(txtAreaName, "Enter Area Name");
                EP.SetIconAlignment(txtAreaName, ErrorIconAlignment.MiddleRight);
                txtAreaName.Focus();
            }
            else if (txtshortName.Text.Trim() == "")
            {

                EP.SetError(txtshortName, "Enter Short Name");
                EP.SetIconAlignment(txtshortName, ErrorIconAlignment.MiddleRight);
                txtshortName.Focus();
            }
            else if (ObjFunction.GetComboValue(cmbCountry) <= 0)
            {
                EP.SetError(cmbCountry, "Select Country Name");
                EP.SetIconAlignment(cmbCountry, ErrorIconAlignment.MiddleRight);
                cmbCountry.Focus();
            }
            if (ObjFunction.GetComboValue(cmbState) <= 0)
            {
                EP.SetError(cmbState, "Select State Name");
                EP.SetIconAlignment(cmbState, ErrorIconAlignment.MiddleRight);
                cmbState.Focus();
            }
            if (ObjFunction.GetComboValue(cmbCity) <= 0)
            {
                EP.SetError(cmbCity, "Select City Name");
                EP.SetIconAlignment(cmbCity, ErrorIconAlignment.MiddleRight);
                cmbCity.Focus();
            }
            if (ObjFunction.GetComboValue(cmbRegion) <= 0)
            {
                EP.SetError(cmbRegion, "Select Region Name");
                EP.SetIconAlignment(cmbRegion, ErrorIconAlignment.MiddleRight);
                cmbRegion.Focus();
            }
            else if (AreaNm != txtAreaName.Text.Trim().ToUpper())
            {
                if (ObjQry.ReturnInteger("Select Count(*) from MArea where AreaName = '" + txtAreaName.Text.Trim().Replace("'", "''") + "'", CommonFunctions.ConStr) != 0)
                {
                    EP.SetError(txtAreaName, "Duplicate Area Name");
                    EP.SetIconAlignment(txtAreaName, ErrorIconAlignment.MiddleRight);
                    txtAreaName.Focus();
                }
                else
                    flag = true;
            }
            else
                flag = true;
            return flag;
        }

        //private void AreaAE_FormClosing(object sender, FormClosingEventArgs e)
        //{
        //    Area.RequestAreaNo = 0;
        //    AreaNm = "";
        //}

        private void txtAreaName_Leave(object sender, EventArgs e)
        {
            try
            {
                EP.SetError(txtAreaName, "");
                if (txtAreaName.Text.Trim() != "")
                {
                    if (AreaNm != txtAreaName.Text.Trim().ToUpper())
                    {
                        if (ObjQry.ReturnInteger("Select Count(*) from MArea where AreaName = '" + txtAreaName.Text.Trim().Replace("'", "''") + "'", CommonFunctions.ConStr) != 0)
                        {
                            EP.SetError(txtAreaName, "Duplicate Area Name");
                            EP.SetIconAlignment(txtAreaName, ErrorIconAlignment.MiddleRight);
                            txtAreaName.Focus();
                        }
                    }

                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #region Navigation Methods
        private void NavigationDisplay(int type)
        {
            try
            {
                long No = 0;
                if (type == 5)
                {
                    if (dtSearch.Rows.Count > 0)
                        No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                    ID = No;
                }
                if (type == 1)
                {
                    No = Convert.ToInt64(dtSearch.Rows[0].ItemArray[0].ToString());
                    cntRow = 0;
                    ID = No;
                }
                else if (type == 2)
                {
                    No = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    cntRow = dtSearch.Rows.Count - 1;
                    ID = No;
                }
                else
                {
                    if (type == 3)
                    {
                        cntRow = cntRow + 1;
                    }
                    else if (type == 4)
                    {
                        cntRow = cntRow - 1;
                    }

                    if (cntRow < 0)
                    {
                        OMMessageBox.Show("This is First Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow + 1;
                    }
                    else if (cntRow > dtSearch.Rows.Count - 1)
                    {
                        OMMessageBox.Show("This is Last Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow - 1;
                    }
                    else
                    {
                        No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                        ID = No;
                    }
                }
                FillControls();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

        }

        private void SetNavigation()
        {
            cntRow = 0;
            for (int i = 0; i < dtSearch.Rows.Count; i++)
            {
                if (Convert.ToInt64(dtSearch.Rows[i].ItemArray[0].ToString()) == ID)
                {
                    cntRow = i;
                    break;
                }
            }
        }

        private void setDisplay(bool flag)
        {
            btnFirst.Visible = flag;
            btnPrev.Visible = flag;
            btnNext.Visible = flag;
            btnLast.Visible = flag;
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            NavigationDisplay(1);
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            NavigationDisplay(4);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            NavigationDisplay(3);
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            NavigationDisplay(2);
        }

        #endregion

        #region KeyDown Events
        private void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is GroupBox)
                    KeyDownFormat(ctrl.Controls);
            }
        }

        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left && e.Control)
            {
                if (btnPrev.Enabled) btnPrev_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Up && e.Control)
            {
                if (btnFirst.Enabled) btnFirst_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Right && e.Control)
            {
                if (btnNext.Enabled) btnNext_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Down && e.Control)
            {
                if (btnLast.Enabled) btnLast_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F2)
            {
                if (BtnSave.Visible) BtnSave_Click(sender, e);
            }
           
        }
        #endregion

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                ID = 0;
                mArea = new MArea();
                ObjFunction.InitialiseControl(this.Controls);
                ObjFunction.LockButtons(false, this.Controls);
                ObjFunction.LockControls(true, this.Controls);
                ObjFunction.FillCombo(cmbCountry, "Select CountryNo,CountryName From MCountry Where IsActive='true'");
                chkActive.Checked = true;
                txtAreaName.Focus();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

        }

        
        private void btnSearch_Click(object sender, EventArgs e)
        {
            Form NewF = new Area();
            this.Close();
            ObjFunction.OpenForm(NewF, DBGetVal.MainForm);

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (chkActive.Checked == false) return;
                dbArea = new DBMArea();
                mArea = new MArea();

                mArea.AreaNo = ID;
                if (OMMessageBox.Show("Are you sure want to delete this record?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (dbArea.DeleteMArea(mArea) == true)
                    {
                        OMMessageBox.Show("Area Deleted Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        FillControls();

                    }
                    else
                    {
                        OMMessageBox.Show("Area not Deleted", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        
       
        private void btnCancel_Click(object sender, EventArgs e)
        {
            NavigationDisplay(5);

            ObjFunction.LockButtons(true, this.Controls);
            ObjFunction.LockControls(false, this.Controls);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            ObjFunction.LockButtons(false, this.Controls);
            ObjFunction.LockControls(true, this.Controls);
            txtAreaName.Focus();
        }

        private void chkActive_CheckedChanged(object sender, EventArgs e)
        {
            if (chkActive.Checked == true)
                chkActive.Text = "Yes";
            else
                chkActive.Text = "No";
        }

        private void cmbCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ObjFunction.FillCombo(cmbState, "Select StateNo,StateName From MState where CountryNo=" + ObjFunction.GetComboValue(cmbCountry) + " And  (IsActive='True' OR StateNo=" + mArea.StateNo + ")");
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ObjFunction.FillCombo(cmbCity, "Select CityNo,CityName From MCity where StateNo=" + ObjFunction.GetComboValue(cmbState) + " And  (IsActive='True' OR CityNo=" + mArea.CityNo + ")");
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ObjFunction.FillCombo(cmbRegion, "Select RegionNo,RegionName From MRegion ");
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void AreaAE_Activated(object sender, EventArgs e)
        {
            try
            {
                if (isDoProcess)
                {
                    if (cmbCountry.Enabled == true)
                    {
                        long tID = ObjFunction.GetComboValue(cmbCountry);
                        ObjFunction.FillCombo(cmbCountry, "Select CountryNo,CountryName From MCountry Where IsActive='true' or CountryNo=" + mArea.CountryNo + "");
                        cmbRegion.SelectedValue = tID;
                    }
                    isDoProcess = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void AreaAE_Deactivate(object sender, EventArgs e)
        {
            isDoProcess = true;
        }
        
    }
}
