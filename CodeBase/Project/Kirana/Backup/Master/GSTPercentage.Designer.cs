﻿namespace Kirana.Master
{
    partial class GSTPercentage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PnlPercentage = new System.Windows.Forms.Panel();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnAddHSN = new System.Windows.Forms.Button();
            this.TxtHSNDesc = new System.Windows.Forms.TextBox();
            this.TxtHSNNo = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.dgDetails = new System.Windows.Forms.DataGridView();
            this.SrNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HSNCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HSNDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HSNNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtSGST = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtCGST = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txtUTGST = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.txtIGST = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.btnPurVat = new System.Windows.Forms.Button();
            this.cmbGSTPercent = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.EP = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnImport = new System.Windows.Forms.Button();
            this.PnlPercentage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EP)).BeginInit();
            this.SuspendLayout();
            // 
            // PnlPercentage
            // 
            this.PnlPercentage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlPercentage.Controls.Add(this.btnImport);
            this.PnlPercentage.Controls.Add(this.btnExit);
            this.PnlPercentage.Controls.Add(this.btnAddHSN);
            this.PnlPercentage.Controls.Add(this.TxtHSNDesc);
            this.PnlPercentage.Controls.Add(this.TxtHSNNo);
            this.PnlPercentage.Controls.Add(this.label44);
            this.PnlPercentage.Controls.Add(this.dgDetails);
            this.PnlPercentage.Controls.Add(this.txtSGST);
            this.PnlPercentage.Controls.Add(this.label27);
            this.PnlPercentage.Controls.Add(this.txtCGST);
            this.PnlPercentage.Controls.Add(this.label42);
            this.PnlPercentage.Controls.Add(this.txtUTGST);
            this.PnlPercentage.Controls.Add(this.label40);
            this.PnlPercentage.Controls.Add(this.txtIGST);
            this.PnlPercentage.Controls.Add(this.label41);
            this.PnlPercentage.Controls.Add(this.label45);
            this.PnlPercentage.Controls.Add(this.btnPurVat);
            this.PnlPercentage.Controls.Add(this.cmbGSTPercent);
            this.PnlPercentage.Controls.Add(this.label32);
            this.PnlPercentage.Location = new System.Drawing.Point(14, 13);
            this.PnlPercentage.Name = "PnlPercentage";
            this.PnlPercentage.Size = new System.Drawing.Size(604, 430);
            this.PnlPercentage.TabIndex = 0;
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(483, 391);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(110, 37);
            this.btnExit.TabIndex = 16000044;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnAddHSN
            // 
            this.btnAddHSN.Location = new System.Drawing.Point(533, 44);
            this.btnAddHSN.Name = "btnAddHSN";
            this.btnAddHSN.Size = new System.Drawing.Size(62, 24);
            this.btnAddHSN.TabIndex = 16000043;
            this.btnAddHSN.Text = "Add";
            this.btnAddHSN.UseVisualStyleBackColor = true;
            this.btnAddHSN.Click += new System.EventHandler(this.btnAddHSN_Click);
            // 
            // TxtHSNDesc
            // 
            this.TxtHSNDesc.Location = new System.Drawing.Point(210, 47);
            this.TxtHSNDesc.Name = "TxtHSNDesc";
            this.TxtHSNDesc.Size = new System.Drawing.Size(317, 20);
            this.TxtHSNDesc.TabIndex = 2;
            // 
            // TxtHSNNo
            // 
            this.TxtHSNNo.Location = new System.Drawing.Point(77, 47);
            this.TxtHSNNo.Name = "TxtHSNNo";
            this.TxtHSNNo.Size = new System.Drawing.Size(120, 20);
            this.TxtHSNNo.TabIndex = 1;
            this.TxtHSNNo.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(3, 47);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(40, 13);
            this.label44.TabIndex = 16000040;
            this.label44.Text = "HSN #";
            // 
            // dgDetails
            // 
            this.dgDetails.AllowUserToAddRows = false;
            this.dgDetails.AllowUserToDeleteRows = false;
            this.dgDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SrNo,
            this.HSNCode,
            this.HSNDescription,
            this.HSNNo});
            this.dgDetails.Location = new System.Drawing.Point(3, 137);
            this.dgDetails.Name = "dgDetails";
            this.dgDetails.ReadOnly = true;
            this.dgDetails.Size = new System.Drawing.Size(592, 248);
            this.dgDetails.TabIndex = 16000039;
            // 
            // SrNo
            // 
            this.SrNo.HeaderText = "Sr#";
            this.SrNo.Name = "SrNo";
            this.SrNo.ReadOnly = true;
            this.SrNo.Width = 30;
            // 
            // HSNCode
            // 
            this.HSNCode.HeaderText = "HSN";
            this.HSNCode.Name = "HSNCode";
            this.HSNCode.ReadOnly = true;
            this.HSNCode.Width = 80;
            // 
            // HSNDescription
            // 
            this.HSNDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.HSNDescription.HeaderText = "Description";
            this.HSNDescription.Name = "HSNDescription";
            this.HSNDescription.ReadOnly = true;
            // 
            // HSNNo
            // 
            this.HSNNo.HeaderText = "HsnNo";
            this.HSNNo.Name = "HSNNo";
            this.HSNNo.ReadOnly = true;
            this.HSNNo.Visible = false;
            // 
            // txtSGST
            // 
            this.txtSGST.Location = new System.Drawing.Point(335, 111);
            this.txtSGST.Name = "txtSGST";
            this.txtSGST.ReadOnly = true;
            this.txtSGST.Size = new System.Drawing.Size(77, 20);
            this.txtSGST.TabIndex = 6;
            this.txtSGST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSGST.TextChanged += new System.EventHandler(this.txtIGST_TextChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(265, 114);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(47, 13);
            this.label27.TabIndex = 16000036;
            this.label27.Text = "SGST %";
            // 
            // txtCGST
            // 
            this.txtCGST.Location = new System.Drawing.Point(335, 84);
            this.txtCGST.Name = "txtCGST";
            this.txtCGST.ReadOnly = true;
            this.txtCGST.Size = new System.Drawing.Size(77, 20);
            this.txtCGST.TabIndex = 4;
            this.txtCGST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCGST.TextChanged += new System.EventHandler(this.txtIGST_TextChanged);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(265, 87);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(47, 13);
            this.label42.TabIndex = 16000034;
            this.label42.Text = "CGST %";
            // 
            // txtUTGST
            // 
            this.txtUTGST.Location = new System.Drawing.Point(76, 111);
            this.txtUTGST.Name = "txtUTGST";
            this.txtUTGST.ReadOnly = true;
            this.txtUTGST.Size = new System.Drawing.Size(77, 20);
            this.txtUTGST.TabIndex = 5;
            this.txtUTGST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtUTGST.TextChanged += new System.EventHandler(this.txtIGST_TextChanged);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(6, 114);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(55, 13);
            this.label40.TabIndex = 16000032;
            this.label40.Text = "UTGST %";
            // 
            // txtIGST
            // 
            this.txtIGST.Location = new System.Drawing.Point(76, 84);
            this.txtIGST.Name = "txtIGST";
            this.txtIGST.ReadOnly = true;
            this.txtIGST.Size = new System.Drawing.Size(77, 20);
            this.txtIGST.TabIndex = 3;
            this.txtIGST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtIGST.TextChanged += new System.EventHandler(this.txtIGST_TextChanged);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(6, 87);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(43, 13);
            this.label41.TabIndex = 16000030;
            this.label41.Text = "IGST %";
            // 
            // label45
            // 
            this.label45.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label45.Location = new System.Drawing.Point(3, 80);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(592, 55);
            this.label45.TabIndex = 16000038;
            // 
            // btnPurVat
            // 
            this.btnPurVat.Location = new System.Drawing.Point(210, 11);
            this.btnPurVat.Name = "btnPurVat";
            this.btnPurVat.Size = new System.Drawing.Size(62, 24);
            this.btnPurVat.TabIndex = 190021;
            this.btnPurVat.Text = "Add";
            this.btnPurVat.UseVisualStyleBackColor = true;
            this.btnPurVat.Click += new System.EventHandler(this.btnPurVat_Click);
            // 
            // cmbGSTPercent
            // 
            this.cmbGSTPercent.FormattingEnabled = true;
            this.cmbGSTPercent.Location = new System.Drawing.Point(77, 13);
            this.cmbGSTPercent.Name = "cmbGSTPercent";
            this.cmbGSTPercent.Size = new System.Drawing.Size(120, 21);
            this.cmbGSTPercent.TabIndex = 0;
            this.cmbGSTPercent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbGSTPercent_KeyDown);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(3, 16);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(68, 13);
            this.label32.TabIndex = 58;
            this.label32.Text = "Percentage :";
            // 
            // EP
            // 
            this.EP.ContainerControl = this;
            // 
            // btnImport
            // 
            this.btnImport.Location = new System.Drawing.Point(278, 11);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(134, 24);
            this.btnImport.TabIndex = 16000045;
            this.btnImport.Text = "Import";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // GSTPercentage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(622, 446);
            this.Controls.Add(this.PnlPercentage);
            this.Name = "GSTPercentage";
            this.Text = "TaxPercentage";
            this.Load += new System.EventHandler(this.GSTPercentage_Load);
            this.PnlPercentage.ResumeLayout(false);
            this.PnlPercentage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EP)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PnlPercentage;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ErrorProvider EP;
        private System.Windows.Forms.ComboBox cmbGSTPercent;
        private System.Windows.Forms.Button btnPurVat;
        private System.Windows.Forms.TextBox txtSGST;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtCGST;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtUTGST;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txtIGST;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.DataGridView dgDetails;
        private System.Windows.Forms.DataGridViewTextBoxColumn SrNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn HSNCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn HSNDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn HSNNo;
        private System.Windows.Forms.Button btnAddHSN;
        private System.Windows.Forms.TextBox TxtHSNDesc;
        private System.Windows.Forms.TextBox TxtHSNNo;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnImport;
    }
}