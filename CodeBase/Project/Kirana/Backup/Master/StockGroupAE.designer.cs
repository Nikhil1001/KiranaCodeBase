﻿namespace Kirana.Master
{
    partial class StockGroupAE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.chkActive = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtStockGroupName = new System.Windows.Forms.TextBox();
            this.EP = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkApplyToAll = new System.Windows.Forms.CheckBox();
            this.lblPercetage = new System.Windows.Forms.Label();
            this.txtMargin = new System.Windows.Forms.TextBox();
            this.lblMargin = new System.Windows.Forms.Label();
            this.btnNewManufacturer = new System.Windows.Forms.Button();
            this.lblStar5 = new System.Windows.Forms.Label();
            this.cmbManufacturerCompanyName = new System.Windows.Forms.ComboBox();
            this.lblMfgComp = new System.Windows.Forms.Label();
            this.lstBrandName = new System.Windows.Forms.ListBox();
            this.lblChkHelp = new System.Windows.Forms.Label();
            this.btnLangDesc = new System.Windows.Forms.Button();
            this.lblStar2 = new System.Windows.Forms.Label();
            this.lblStar4 = new System.Windows.Forms.Label();
            this.lblStar3 = new System.Windows.Forms.Label();
            this.lblStar1 = new System.Windows.Forms.Label();
            this.lblCategory = new System.Windows.Forms.Label();
            this.cmbCategory = new System.Windows.Forms.ComboBox();
            this.lblDept = new System.Windows.Forms.Label();
            this.cmbDepartment = new System.Windows.Forms.ComboBox();
            this.txtLanguage = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrev = new System.Windows.Forms.Button();
            this.btnLast = new System.Windows.Forms.Button();
            this.btnFirst = new System.Windows.Forms.Button();
            this.BtnExit = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.pnlMain = new JitControls.OMBPanel();
            ((System.ComponentModel.ISupportInitialize)(this.EP)).BeginInit();
            this.panel1.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkActive
            // 
            this.chkActive.AutoSize = true;
            this.chkActive.Checked = true;
            this.chkActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkActive.Location = new System.Drawing.Point(128, 107);
            this.chkActive.Name = "chkActive";
            this.chkActive.Size = new System.Drawing.Size(44, 17);
            this.chkActive.TabIndex = 2;
            this.chkActive.Text = "Yes";
            this.chkActive.UseVisualStyleBackColor = true;
            this.chkActive.CheckedChanged += new System.EventHandler(this.chkActive_CheckedChanged);
            this.chkActive.KeyDown += new System.Windows.Forms.KeyEventHandler(this.chkActive_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 53;
            this.label3.Text = "Active Status :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 51;
            this.label1.Text = " Name :";
            // 
            // txtStockGroupName
            // 
            this.txtStockGroupName.Location = new System.Drawing.Point(128, 8);
            this.txtStockGroupName.MaxLength = 25;
            this.txtStockGroupName.Name = "txtStockGroupName";
            this.txtStockGroupName.Size = new System.Drawing.Size(243, 20);
            this.txtStockGroupName.TabIndex = 0;
            this.txtStockGroupName.TextChanged += new System.EventHandler(this.txtStockGroupName_TextChanged);
            this.txtStockGroupName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStockGroupName_KeyDown);
            this.txtStockGroupName.Leave += new System.EventHandler(this.txtStockGroupName_Leave);
            // 
            // EP
            // 
            this.EP.ContainerControl = this;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.chkApplyToAll);
            this.panel1.Controls.Add(this.lblPercetage);
            this.panel1.Controls.Add(this.txtMargin);
            this.panel1.Controls.Add(this.lblMargin);
            this.panel1.Controls.Add(this.btnNewManufacturer);
            this.panel1.Controls.Add(this.lblStar5);
            this.panel1.Controls.Add(this.cmbManufacturerCompanyName);
            this.panel1.Controls.Add(this.lblMfgComp);
            this.panel1.Controls.Add(this.lstBrandName);
            this.panel1.Controls.Add(this.lblChkHelp);
            this.panel1.Controls.Add(this.btnLangDesc);
            this.panel1.Controls.Add(this.lblStar2);
            this.panel1.Controls.Add(this.lblStar4);
            this.panel1.Controls.Add(this.lblStar3);
            this.panel1.Controls.Add(this.lblStar1);
            this.panel1.Controls.Add(this.lblCategory);
            this.panel1.Controls.Add(this.cmbCategory);
            this.panel1.Controls.Add(this.lblDept);
            this.panel1.Controls.Add(this.cmbDepartment);
            this.panel1.Controls.Add(this.txtLanguage);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtStockGroupName);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.chkActive);
            this.panel1.Location = new System.Drawing.Point(22, 22);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(412, 246);
            this.panel1.TabIndex = 0;
            // 
            // chkApplyToAll
            // 
            this.chkApplyToAll.AutoSize = true;
            this.chkApplyToAll.Location = new System.Drawing.Point(299, 38);
            this.chkApplyToAll.Name = "chkApplyToAll";
            this.chkApplyToAll.Size = new System.Drawing.Size(82, 17);
            this.chkApplyToAll.TabIndex = 190051;
            this.chkApplyToAll.Text = "Apply To All";
            this.chkApplyToAll.UseVisualStyleBackColor = true;
            this.chkApplyToAll.KeyDown += new System.Windows.Forms.KeyEventHandler(this.chkApplyToAll_KeyDown);
            // 
            // lblPercetage
            // 
            this.lblPercetage.AutoSize = true;
            this.lblPercetage.Location = new System.Drawing.Point(209, 39);
            this.lblPercetage.Name = "lblPercetage";
            this.lblPercetage.Size = new System.Drawing.Size(15, 13);
            this.lblPercetage.TabIndex = 190050;
            this.lblPercetage.Text = "%";
            // 
            // txtMargin
            // 
            this.txtMargin.Location = new System.Drawing.Point(128, 36);
            this.txtMargin.Name = "txtMargin";
            this.txtMargin.Size = new System.Drawing.Size(76, 20);
            this.txtMargin.TabIndex = 190049;
            this.txtMargin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMargin.TextChanged += new System.EventHandler(this.txtMargin_TextChanged);
            this.txtMargin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMargin_KeyDown);
            // 
            // lblMargin
            // 
            this.lblMargin.AutoSize = true;
            this.lblMargin.Location = new System.Drawing.Point(11, 38);
            this.lblMargin.Name = "lblMargin";
            this.lblMargin.Size = new System.Drawing.Size(42, 13);
            this.lblMargin.TabIndex = 190048;
            this.lblMargin.Text = "Margin:";
            // 
            // btnNewManufacturer
            // 
            this.btnNewManufacturer.Location = new System.Drawing.Point(376, 196);
            this.btnNewManufacturer.Name = "btnNewManufacturer";
            this.btnNewManufacturer.Size = new System.Drawing.Size(21, 21);
            this.btnNewManufacturer.TabIndex = 190047;
            this.btnNewManufacturer.Text = "..";
            this.btnNewManufacturer.UseVisualStyleBackColor = true;
            this.btnNewManufacturer.Click += new System.EventHandler(this.btnNewManufacturer_Click);
            // 
            // lblStar5
            // 
            this.lblStar5.AutoSize = true;
            this.lblStar5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar5.Location = new System.Drawing.Point(395, 199);
            this.lblStar5.Name = "lblStar5";
            this.lblStar5.Size = new System.Drawing.Size(11, 13);
            this.lblStar5.TabIndex = 190046;
            this.lblStar5.Text = "*";
            // 
            // cmbManufacturerCompanyName
            // 
            this.cmbManufacturerCompanyName.FormattingEnabled = true;
            this.cmbManufacturerCompanyName.Location = new System.Drawing.Point(127, 196);
            this.cmbManufacturerCompanyName.Name = "cmbManufacturerCompanyName";
            this.cmbManufacturerCompanyName.Size = new System.Drawing.Size(243, 21);
            this.cmbManufacturerCompanyName.TabIndex = 190045;
            this.cmbManufacturerCompanyName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbManufacturerCompanyName_KeyDown);
            // 
            // lblMfgComp
            // 
            this.lblMfgComp.AutoSize = true;
            this.lblMfgComp.Location = new System.Drawing.Point(6, 204);
            this.lblMfgComp.Name = "lblMfgComp";
            this.lblMfgComp.Size = new System.Drawing.Size(68, 13);
            this.lblMfgComp.TabIndex = 190044;
            this.lblMfgComp.Text = "Mfg.  Name :";
            // 
            // lstBrandName
            // 
            this.lstBrandName.FormattingEnabled = true;
            this.lstBrandName.Location = new System.Drawing.Point(128, 64);
            this.lstBrandName.Name = "lstBrandName";
            this.lstBrandName.Size = new System.Drawing.Size(243, 95);
            this.lstBrandName.TabIndex = 190043;
            this.lstBrandName.Visible = false;
            this.lstBrandName.Leave += new System.EventHandler(this.lstBrandName_Leave);
            this.lstBrandName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstBrandName_KeyDown);
            // 
            // lblChkHelp
            // 
            this.lblChkHelp.AutoSize = true;
            this.lblChkHelp.Location = new System.Drawing.Point(176, 108);
            this.lblChkHelp.Name = "lblChkHelp";
            this.lblChkHelp.Size = new System.Drawing.Size(215, 13);
            this.lblChkHelp.TabIndex = 190042;
            this.lblChkHelp.Text = "( Press SPACE BAR or cilck using MOUSE )";
            // 
            // btnLangDesc
            // 
            this.btnLangDesc.Location = new System.Drawing.Point(377, 75);
            this.btnLangDesc.Name = "btnLangDesc";
            this.btnLangDesc.Size = new System.Drawing.Size(21, 21);
            this.btnLangDesc.TabIndex = 190041;
            this.btnLangDesc.Text = "..";
            this.btnLangDesc.UseVisualStyleBackColor = true;
            this.btnLangDesc.Click += new System.EventHandler(this.btnLangDesc_Click);
            // 
            // lblStar2
            // 
            this.lblStar2.AutoSize = true;
            this.lblStar2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar2.Location = new System.Drawing.Point(396, 79);
            this.lblStar2.Name = "lblStar2";
            this.lblStar2.Size = new System.Drawing.Size(11, 13);
            this.lblStar2.TabIndex = 190022;
            this.lblStar2.Text = "*";
            // 
            // lblStar4
            // 
            this.lblStar4.AutoSize = true;
            this.lblStar4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar4.Location = new System.Drawing.Point(395, 168);
            this.lblStar4.Name = "lblStar4";
            this.lblStar4.Size = new System.Drawing.Size(11, 13);
            this.lblStar4.TabIndex = 190021;
            this.lblStar4.Text = "*";
            // 
            // lblStar3
            // 
            this.lblStar3.AutoSize = true;
            this.lblStar3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar3.Location = new System.Drawing.Point(396, 133);
            this.lblStar3.Name = "lblStar3";
            this.lblStar3.Size = new System.Drawing.Size(11, 13);
            this.lblStar3.TabIndex = 190020;
            this.lblStar3.Text = "*";
            // 
            // lblStar1
            // 
            this.lblStar1.AutoSize = true;
            this.lblStar1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar1.Location = new System.Drawing.Point(396, 11);
            this.lblStar1.Name = "lblStar1";
            this.lblStar1.Size = new System.Drawing.Size(11, 13);
            this.lblStar1.TabIndex = 190018;
            this.lblStar1.Text = "*";
            // 
            // lblCategory
            // 
            this.lblCategory.AutoSize = true;
            this.lblCategory.Location = new System.Drawing.Point(6, 168);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(55, 13);
            this.lblCategory.TabIndex = 58;
            this.lblCategory.Text = "Category :";
            // 
            // cmbCategory
            // 
            this.cmbCategory.FormattingEnabled = true;
            this.cmbCategory.Location = new System.Drawing.Point(127, 162);
            this.cmbCategory.Name = "cmbCategory";
            this.cmbCategory.Size = new System.Drawing.Size(243, 21);
            this.cmbCategory.TabIndex = 57;
            this.cmbCategory.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbCategory_KeyDown);
            // 
            // lblDept
            // 
            this.lblDept.AutoSize = true;
            this.lblDept.Location = new System.Drawing.Point(7, 136);
            this.lblDept.Name = "lblDept";
            this.lblDept.Size = new System.Drawing.Size(68, 13);
            this.lblDept.TabIndex = 56;
            this.lblDept.Text = "Department :";
            // 
            // cmbDepartment
            // 
            this.cmbDepartment.FormattingEnabled = true;
            this.cmbDepartment.Location = new System.Drawing.Point(128, 130);
            this.cmbDepartment.Name = "cmbDepartment";
            this.cmbDepartment.Size = new System.Drawing.Size(243, 21);
            this.cmbDepartment.TabIndex = 55;
            this.cmbDepartment.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbDepartment_KeyDown);
            // 
            // txtLanguage
            // 
            this.txtLanguage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLanguage.Font = new System.Drawing.Font("Shivaji01", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLanguage.Location = new System.Drawing.Point(128, 74);
            this.txtLanguage.MaxLength = 25;
            this.txtLanguage.Name = "txtLanguage";
            this.txtLanguage.Size = new System.Drawing.Size(243, 25);
            this.txtLanguage.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 54;
            this.label4.Text = "Language :";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(106, 274);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 60);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(23, 275);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(80, 60);
            this.btnNew.TabIndex = 5;
            this.btnNew.Text = "&New";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(106, 274);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(80, 60);
            this.btnUpdate.TabIndex = 6;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(189, 274);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(80, 60);
            this.btnSearch.TabIndex = 8;
            this.btnSearch.Text = "Searc&h";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(272, 274);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(80, 60);
            this.btnDelete.TabIndex = 9;
            this.btnDelete.Text = "&Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(103, 341);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(37, 27);
            this.btnNext.TabIndex = 13;
            this.btnNext.Text = ">";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.Location = new System.Drawing.Point(63, 341);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(37, 27);
            this.btnPrev.TabIndex = 12;
            this.btnPrev.Text = "<";
            this.btnPrev.UseVisualStyleBackColor = true;
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // btnLast
            // 
            this.btnLast.Location = new System.Drawing.Point(143, 341);
            this.btnLast.Name = "btnLast";
            this.btnLast.Size = new System.Drawing.Size(37, 27);
            this.btnLast.TabIndex = 14;
            this.btnLast.Text = ">|";
            this.btnLast.UseVisualStyleBackColor = true;
            this.btnLast.Click += new System.EventHandler(this.btnLast_Click);
            // 
            // btnFirst
            // 
            this.btnFirst.Location = new System.Drawing.Point(23, 341);
            this.btnFirst.Name = "btnFirst";
            this.btnFirst.Size = new System.Drawing.Size(37, 27);
            this.btnFirst.TabIndex = 11;
            this.btnFirst.Text = "|<";
            this.btnFirst.UseVisualStyleBackColor = true;
            this.btnFirst.Click += new System.EventHandler(this.btnFirst_Click);
            // 
            // BtnExit
            // 
            this.BtnExit.Location = new System.Drawing.Point(355, 274);
            this.BtnExit.Name = "BtnExit";
            this.BtnExit.Size = new System.Drawing.Size(80, 60);
            this.BtnExit.TabIndex = 10;
            this.BtnExit.Text = "Exit";
            this.BtnExit.UseVisualStyleBackColor = true;
            this.BtnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(23, 274);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(80, 60);
            this.BtnSave.TabIndex = 4;
            this.BtnSave.Text = "Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // pnlMain
            // 
            this.pnlMain.BorderColor = System.Drawing.Color.Gray;
            this.pnlMain.BorderRadius = 3;
            this.pnlMain.Controls.Add(this.panel1);
            this.pnlMain.Controls.Add(this.btnCancel);
            this.pnlMain.Controls.Add(this.BtnSave);
            this.pnlMain.Controls.Add(this.btnNew);
            this.pnlMain.Controls.Add(this.BtnExit);
            this.pnlMain.Controls.Add(this.btnUpdate);
            this.pnlMain.Controls.Add(this.btnFirst);
            this.pnlMain.Controls.Add(this.btnSearch);
            this.pnlMain.Controls.Add(this.btnLast);
            this.pnlMain.Controls.Add(this.btnDelete);
            this.pnlMain.Controls.Add(this.btnPrev);
            this.pnlMain.Controls.Add(this.btnNext);
            this.pnlMain.Location = new System.Drawing.Point(21, 24);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(456, 382);
            this.pnlMain.TabIndex = 15;
            // 
            // StockGroupAE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(504, 418);
            this.Controls.Add(this.pnlMain);
            this.Name = "StockGroupAE";
            this.Text = "StockGroup Master";
            this.Deactivate += new System.EventHandler(this.StockGroupAE_Deactivate);
            this.Load += new System.EventHandler(this.StockGroupAE_Load);
            this.Activated += new System.EventHandler(this.StockGroupAE_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.StockGroupAE_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.EP)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox chkActive;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtStockGroupName;
        private System.Windows.Forms.ErrorProvider EP;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.Button btnLast;
        private System.Windows.Forms.Button btnFirst;
        private System.Windows.Forms.Button BtnExit;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.TextBox txtLanguage;
        private System.Windows.Forms.Label label4;
        private JitControls.OMBPanel pnlMain;
        private System.Windows.Forms.Label lblDept;
        private System.Windows.Forms.ComboBox cmbDepartment;
        private System.Windows.Forms.Label lblCategory;
        private System.Windows.Forms.ComboBox cmbCategory;
        private System.Windows.Forms.Label lblStar4;
        private System.Windows.Forms.Label lblStar3;
        private System.Windows.Forms.Label lblStar1;
        private System.Windows.Forms.Label lblStar2;
        private System.Windows.Forms.Button btnLangDesc;
        private System.Windows.Forms.Label lblChkHelp;
        private System.Windows.Forms.ListBox lstBrandName;
        private System.Windows.Forms.ComboBox cmbManufacturerCompanyName;
        private System.Windows.Forms.Label lblMfgComp;
        private System.Windows.Forms.Label lblStar5;
        private System.Windows.Forms.Button btnNewManufacturer;
        private System.Windows.Forms.Label lblPercetage;
        private System.Windows.Forms.TextBox txtMargin;
        private System.Windows.Forms.Label lblMargin;
        private System.Windows.Forms.CheckBox chkApplyToAll;
    }
}