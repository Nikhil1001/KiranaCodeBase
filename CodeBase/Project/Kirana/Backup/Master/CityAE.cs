﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Master
{
    /// <summary>
    /// This class is used for City AE
    /// </summary>
    public partial class CityAE : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        DBMCity dbCity = new DBMCity();
        MCity mCity = new MCity();
        string CityNm;
        DataTable dtSearch = new DataTable();
        int cntRow;
        long ID;
        
        bool isDoProcess = false;

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public CityAE()
        {
            InitializeComponent();
        }

        private void CityAE_Load(object sender, EventArgs e)
        {
            try
            {
                ObjFunction.LockButtons(true, this.Controls);
                ObjFunction.LockControls(false, this.Controls);

                ObjFunction.FillCombo(cmbCountry, "Select CountryNo,CountryName From MCountry where IsActive='true'");
                ObjFunction.FillCombo(cmbState, "Select StateNo,StateName From MState Where CountryNo = " + ObjFunction.GetComboValue(cmbCountry) + " And (IsActive = 'True') ORDER BY StateName");
                ObjFunction.FillCombo(cmbRegion, "select RegionNo,RegionName from MRegion where IsActive='true'");

                CityNm = "";
                dtSearch = ObjFunction.GetDataView("Select CityNo From MCity order by CityName").Table;

                if (dtSearch.Rows.Count > 0)
                {
                    if (City.RequestCityNo == 0)
                        ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    else
                        ID = City.RequestCityNo;
                    FillControls();
                    SetNavigation();
                }
                setDisplay(true);
                btnNew.Focus();
                KeyDownFormat(this.Controls);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillControls()
        {
            try
            {
                EP.SetError(txtCityName, "");
                EP.SetError(txtshortName, "");
                EP.SetError(cmbCountry, "");
                EP.SetError(cmbState, "");
                EP.SetError(cmbRegion, "");
                mCity = dbCity.ModifyMCityByID(ID);
                CityNm = mCity.CityName;
                txtCityName.Text = mCity.CityName;
                txtshortName.Text = mCity.CityShortCode;
                chkActive.Checked = mCity.IsActive;
                if (chkActive.Checked == true)
                    chkActive.Text = "Yes";
                else
                    chkActive.Text = "No";
                ObjFunction.FillCombo(cmbCountry, "Select CountryNo,CountryName From MCountry where (IsActive='true' or CountryNo=" + mCity.CountryNo + ")");
                cmbCountry.SelectedValue = mCity.CountryNo.ToString();
                ObjFunction.FillCombo(cmbState, "Select StateNo,StateName From MState Where CountryNo = " + ObjFunction.GetComboValue(cmbCountry) + " And (IsActive = 'True' or StateNo=" + mCity.StateNo + ")ORDER BY StateName");
                cmbState.SelectedValue = mCity.StateNo.ToString();
                ObjFunction.FillCombo(cmbRegion, "select RegionNo,RegionName from MRegion where (IsActive='true' or RegionNo=" + mCity.RegionNo + ")");
                cmbRegion.SelectedValue = mCity.RegionNo.ToString();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void SetValue()
        {
            try
            {
                if (Validations() == true)
                {
                    dbCity = new DBMCity();
                    mCity = new MCity();

                    mCity.CityNo = ID;
                    mCity.CityName = txtCityName.Text.Trim().ToUpper();
                    mCity.CityShortCode = txtshortName.Text.Trim().ToUpper();
                    mCity.IsActive = chkActive.Checked;
                    mCity.CountryNo = ObjFunction.GetComboValue(cmbCountry);
                    mCity.StateNo = ObjFunction.GetComboValue(cmbState);
                    mCity.RegionNo = ObjFunction.GetComboValue(cmbRegion);
                    mCity.UserID = DBGetVal.UserID;
                    mCity.UserDate = DBGetVal.ServerTime.Date;
                    mCity.CompanyNo = DBGetVal.CompanyNo;

                    if (dbCity.AddMCity(mCity) == true)
                    {
                        if (ID == 0)
                        {
                            OMMessageBox.Show("City Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            dtSearch = ObjFunction.GetDataView("Select CityNo From MCity order by CityName").Table;
                            ID = ObjQry.ReturnLong("Select Max(CityNo) FRom MCity", CommonFunctions.ConStr);
                            SetNavigation();
                            FillControls();
                        }
                        else
                        {
                            OMMessageBox.Show("City Updated Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            FillControls();
                        }

                        ObjFunction.LockButtons(true, this.Controls);
                        ObjFunction.LockControls(false, this.Controls);
                    }
                    else
                    {
                        OMMessageBox.Show("City not saved", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            City.RequestCityNo = 0;
            this.Close();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            SetValue();
        }

        private bool Validations()
        {
            bool flag = false;
            EP.SetError(txtCityName, "");
            EP.SetError(txtshortName, "");
            EP.SetError(cmbCountry, "");
            EP.SetError(cmbState, "");
            EP.SetError(cmbRegion, "");


            if (txtCityName.Text.Trim() == "")
            {

                EP.SetError(txtCityName, "Enter City Name");
                EP.SetIconAlignment(txtCityName, ErrorIconAlignment.MiddleRight);
                txtCityName.Focus();
            }
            else if (txtshortName.Text.Trim() == "")
            {

                EP.SetError(txtshortName, "Enter Short Name");
                EP.SetIconAlignment(txtshortName, ErrorIconAlignment.MiddleRight);
                txtshortName.Focus();
            }
            else if (ObjFunction.GetComboValue(cmbCountry) <= 0)
            {
                EP.SetError(cmbCountry, "Select Country");
                EP.SetIconAlignment(cmbCountry, ErrorIconAlignment.MiddleRight);
                cmbCountry.Focus();
            }
            else if (ObjFunction.GetComboValue(cmbState) <= 0)
            {
                EP.SetError(cmbState, "Select State");
                EP.SetIconAlignment(cmbState, ErrorIconAlignment.MiddleRight);
                cmbState.Focus();
            }
            else if (ObjFunction.GetComboValue(cmbRegion) <= 0)
            {
                EP.SetError(cmbRegion, "Select Region");
                EP.SetIconAlignment(cmbRegion, ErrorIconAlignment.MiddleRight);
                cmbRegion.Focus();
            }
            else if (CityNm != txtCityName.Text.Trim().ToUpper())
            {
                if (ObjQry.ReturnInteger("Select Count(*) from MCity where CityName = '" + txtCityName.Text.Trim().Replace("'", "''") + "'", CommonFunctions.ConStr) != 0)
                {
                    EP.SetError(txtCityName, "Duplicate City Name");
                    EP.SetIconAlignment(txtCityName, ErrorIconAlignment.MiddleRight);
                    txtCityName.Focus();
                }
                else
                    flag = true;
            }
            else
                flag = true;
            return flag;
        }

        private void CityAE_FormClosing(object sender, FormClosingEventArgs e)
        {
            ID = 0;
            CityNm = "";
        }

        private void txtCityName_Leave(object sender, EventArgs e)
        {
            try
            {
                EP.SetError(txtCityName, "");
                if (txtCityName.Text.Trim() != "")
                {
                    if (CityNm != txtCityName.Text.Trim().ToUpper())
                    {
                        if (ObjQry.ReturnInteger("Select Count(*) from MCity where CityName = '" + txtCityName.Text.Trim().Replace("'", "''") + "'", CommonFunctions.ConStr) != 0)
                        {
                            EP.SetError(txtCityName, "Duplicate City Name");
                            EP.SetIconAlignment(txtCityName, ErrorIconAlignment.MiddleRight);
                            txtCityName.Focus();
                        }
                    }

                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #region Navigation Methods
        private void NavigationDisplay(int type)
        {
            try
            {
                long No = 0;
                if (type == 5)
                {
                    if (dtSearch.Rows.Count > 0)
                        No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                    ID = No;
                }
                else if (type == 1)
                {
                    No = Convert.ToInt64(dtSearch.Rows[0].ItemArray[0].ToString());
                    cntRow = 0;
                    ID = No;
                }
                else if (type == 2)
                {
                    No = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    cntRow = dtSearch.Rows.Count - 1;
                    ID = No;
                }
                else
                {
                    if (type == 3)
                    {
                        cntRow = cntRow + 1;
                    }
                    else if (type == 4)
                    {
                        cntRow = cntRow - 1;
                    }

                    if (cntRow < 0)
                    {
                        OMMessageBox.Show("This is First Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow + 1;
                    }
                    else if (cntRow > dtSearch.Rows.Count - 1)
                    {
                        OMMessageBox.Show("This is Last Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow - 1;
                    }
                    else
                    {
                        No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                        ID = No;
                    }

                }


                FillControls();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

        }

        private void SetNavigation()
        {
            cntRow = 0;
            for (int i = 0; i < dtSearch.Rows.Count; i++)
            {
                if (Convert.ToInt64(dtSearch.Rows[i].ItemArray[0].ToString()) == ID)
                {
                    cntRow = i;
                    break;
                }
            }
        }

        private void setDisplay(bool flag)
        {
            btnFirst.Visible = flag;
            btnPrev.Visible = flag;
            btnNext.Visible = flag;
            btnLast.Visible = flag;
            btnDelete.Visible = flag;
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            NavigationDisplay(1);
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            NavigationDisplay(4);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            NavigationDisplay(3);
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            NavigationDisplay(2);
        }

        #endregion

        #region KeyDown Events
        private void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is GroupBox)
                    KeyDownFormat(ctrl.Controls);
            }
        }

        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left && e.Control)
            {
                if (btnPrev.Enabled) btnPrev_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Up && e.Control)
            {
                if (btnFirst.Enabled) btnFirst_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Right && e.Control)
            {
                if (btnNext.Enabled) btnNext_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Down && e.Control)
            {
                if (btnLast.Enabled) btnLast_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F2)
            {
                if (btnSave.Visible) BtnSave_Click(sender, e);
            }

        }
        #endregion

        private void chkActive_CheckedChanged(object sender, EventArgs e)
        {
            if (chkActive.Checked == true)
                chkActive.Text = "Yes";
            else
                chkActive.Text = "No";

        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (chkActive.Checked == false) return;
                dbCity = new DBMCity();
                mCity = new MCity();

                mCity.CityNo = ID;
                if (OMMessageBox.Show("Are you sure want to delete this record?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (dbCity.DeleteMCity(mCity) == true)
                    {
                        OMMessageBox.Show("City Deleted Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        FillControls();
                        //Form NewF = new City();
                        //this.Close();
                        //ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                    }
                    else
                    {
                        OMMessageBox.Show("City not Deleted", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }

                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

        }


        private void btnSearch_Click(object sender, EventArgs e)
        {
            Form NewF = new City();
            this.Close();
            ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            ObjFunction.LockButtons(false, this.Controls);
            ObjFunction.LockControls(true, this.Controls);
            txtCityName.Focus();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            NavigationDisplay(5);

            ObjFunction.LockButtons(true, this.Controls);
            ObjFunction.LockControls(false, this.Controls);
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                ID = 0;
                CityNm = "";
                mCity = new MCity();
                ObjFunction.InitialiseControl(this.Controls);
                ObjFunction.LockButtons(false, this.Controls);
                ObjFunction.LockControls(true, this.Controls);
                ObjFunction.FillCombo(cmbCountry, "Select CountryNo,CountryName From MCountry where IsActive='true'");
                ObjFunction.FillCombo(cmbRegion, "select RegionNo,RegionName from MRegion where IsActive='true'");
                txtCityName.Focus();
                chkActive.Checked = true;
                cmbCountry.SelectedIndex = 0;
                ObjFunction.FillCombo(cmbState, "Select StateNo,StateName From MState Where CountryNo = " + ObjFunction.GetComboValue(cmbCountry) + " And (IsActive = 'True') ORDER BY StateName");
                cmbState.SelectedIndex = 0;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbCountry_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                ObjFunction.FillCombo(cmbState, "Select StateNo,StateName From MState Where (IsActive='true' OR StateNo=" + mCity.StateNo + ") And CountryNo = " + ObjFunction.GetComboValue(cmbCountry) + "");
                cmbState.Focus();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void CityAE_Activated(object sender, EventArgs e)
        {
            try
            {
                if (isDoProcess)
                {
                    if (cmbCountry.Enabled == true)
                    {
                        long tID = ObjFunction.GetComboValue(cmbCountry);
                        ObjFunction.FillCombo(cmbCountry, "Select CountryNo,CountryName From MCountry Where (IsActive ='True' Or CountryNo = " + mCity.CountryNo + ") Order By CountryName");
                        cmbCountry.SelectedValue = tID;
                    }
                    if (cmbState.Enabled == true)
                    {
                        long tID = ObjFunction.GetComboValue(cmbState);
                        ObjFunction.FillCombo(cmbState, "Select StateNo,StateName From MState Where CountryNo = " + ObjFunction.GetComboValue(cmbCountry) + " And (IsActive = 'True' or StateNo=" + mCity.StateNo + ")ORDER BY StateName");
                        cmbState.SelectedValue = tID;
                    }
                    if (cmbRegion.Enabled == true)
                    {
                        long tID = ObjFunction.GetComboValue(cmbRegion);
                        ObjFunction.FillCombo(cmbRegion, "select RegionNo,RegionName from MRegion Where (IsActive ='True' or RegionNo = " + mCity.RegionNo + ") Order By RegionName");
                        cmbRegion.SelectedValue = tID;
                    }
                    isDoProcess = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void CityAE_Deactivate(object sender, EventArgs e)
        {
            isDoProcess = true;
        }



    }
}
