﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Master
{
    /// <summary>
    /// This class is used for Manufacturer Details
    /// </summary>
    public partial class ManufacturerDetails : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        DBMManufacturerCompany dbMManufacturerCompany = new DBMManufacturerCompany();
        MManufacturerDetails mManufactuerDetails = new MManufacturerDetails();
        DataTable dtDelete = new DataTable();
        /// <summary>
        /// This field is used for Short ID
        /// </summary>
        public long ShortID = 0;

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public ManufacturerDetails()
        {
            InitializeComponent();
        }

        /// <summary>
        /// This is class of Parameterised Constructor
        /// </summary>
        public ManufacturerDetails(long shortid)
        {
            InitializeComponent();
            ShortID = shortid;
        }

        private void ManufactuerDetails_Load(object sender, EventArgs e)
        {
            try
            {
                InitDelTable();
                if (ShortID == 0)
                {
                    BindGrid();
                    new GridSearch(dgSupplier, 1);
                }
                else
                {
                    setDisable(false);
                    BindMF();
                }
                KeyDownFormat(this.Controls);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void setDisable(bool flag)
        {
            btnCancel.Visible = flag;
            pnlSearch.Visible = flag;
            dgSupplier.Enabled = flag;
            lblChkHelp1.Visible = flag;
            lblSuplierCount.Visible = flag;
            dgSupplier.Visible = flag;
            lblChkHelp.Location = new Point(lblChkHelp1.Location.X, lblChkHelp1.Location.Y - (lblChkHelp1.Height * 2));// lblChkHelp1.Location;
            lblMFG.Location = new Point(lblChkHelp1.Location.X, lblChkHelp1.Location.Y-7);// lblChkHelp1.Location;
            dgMF.Location = lblSearch.Location;// new Point(txtSearch.Location.X, dgSupplier.Location.Y);
            chkSelectAll.Location = new Point(lblChkHelp.Location.X + lblChkHelp.Width + 10, lblChkHelp.Location.Y);
            btnExit.Location = btnCancel.Location;
            pnlMain.Size = new Size(pnlMain.Width / 2-25, pnlMain.Height);
            this.Width = this.Width / 2;
            this.StartPosition = FormStartPosition.CenterScreen;
            
        }

        #region KeyDown Events
        private void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is GroupBox)
                    KeyDownFormat(ctrl.Controls);
            }
        }

        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F4)
            {
                btnSave_Click(sender, new EventArgs());
            }
            else if (e.KeyCode == Keys.F2)
            {
                chkSelectAll.Checked = !chkSelectAll.Checked;
            }

        }
        #endregion

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void BindGrid()
        {
            try
            {
                lblMFG.Text = "";
                lblSuplierCount.Text = "";
                while (dgSupplier.Rows.Count > 0)
                    dgSupplier.Rows.RemoveAt(0);
                while (dgMF.Rows.Count > 0)
                    dgMF.Rows.RemoveAt(0);

                DataView dv = new DataView();
                string str = txtSearch.Text;
                if (str.Trim() != "")
                {
                    if (str.Length == 0)
                    {
                        dgSupplier.DataSource = dbMManufacturerCompany.GetBySearchSupplier("0", txtSearch.Text);
                    }
                    else
                    {
                        dgSupplier.DataSource = dbMManufacturerCompany.GetBySearchSupplier("1", txtSearch.Text);
                    }
                }
                else
                {
                    dgSupplier.DataSource = dbMManufacturerCompany.GetBySearchSupplier("0", txtSearch.Text);
                }
                if (dgSupplier != null)
                    lblSuplierCount.Text = "Supplier :" + dgSupplier.Rows.Count;
                else
                    lblSuplierCount.Text = "Supplier : xxx";
                dgMF.Enabled = false;
                dgSupplier.Enabled = true;
                txtSearch.Enabled = true;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void BindMF()
        {

            try
            {
                dgMF.Columns[0].Width = 275;
                while (dgMF.Rows.Count > 0)
                    dgMF.Rows.RemoveAt(0);
                long LedgerNo;
                if (ShortID == 0)
                    LedgerNo = Convert.ToInt64(dgSupplier.Rows[dgSupplier.CurrentRow.Index].Cells[0].Value.ToString());
                else
                    LedgerNo = ShortID;
                string sql = " SELECT MManufacturerCompany.MfgCompName, MManufacturerCompany.MfgCompNo, ISNULL(MManufacturerDetails.LedgerNo, " + LedgerNo + ") AS MFLedgerNo,ISNULL(MManufacturerDetails.PkSrNo, 0) AS MPkSrNo, CASE WHEN (ISNULL(MManufacturerDetails.PkSrNo, 0) = 0) THEN 0 ELSE 1 END AS Chk,CASE WHEN (ISNULL(MManufacturerDetails.PkSrNo, 0) = 0) THEN 'false' ELSE 'true' END AS Checkk " +
                          " FROM MManufacturerDetails RIGHT OUTER JOIN MManufacturerCompany ON MManufacturerDetails.ManufacturerNo = MManufacturerCompany.MfgCompNo " +
                          " AND MManufacturerDetails.LedgerNo = " + LedgerNo + " Where MManufacturerCompany.IsActive='True'" +
                          "  ORDER BY  Chk desc,MManufacturerCompany.MfgCompName ";
                DataTable dt = ObjFunction.GetDataView(sql).Table;
                //dgMF.DataSource = dt;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dgMF.Rows.Add();
                    for (int j = 0; j < dgMF.ColumnCount; j++)
                    {
                        if (j == 5)
                        {
                            if (dt.Rows[i].ItemArray[j].ToString() != "")
                                dgMF.Rows[i].Cells[j].Value = Convert.ToBoolean(dt.Rows[i].ItemArray[j].ToString());
                            else dgMF.Rows[i].Cells[j].Value = false;
                        }
                        else dgMF.Rows[i].Cells[j].Value = dt.Rows[i].ItemArray[j].ToString();
                    }
                }
                Calculate();

            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SetValue();
        }

        private void SetValue()
        {
            try
            {
                bool flag = false;
                dbMManufacturerCompany = new DBMManufacturerCompany();
                for (int i = 0; i < dgMF.Rows.Count; i++)
                {
                    //if (Convert.ToInt64(dgMF.Rows[i].Cells[3].Value.ToString()) == 0 && Convert.ToInt64(dgMF.Rows[i].Cells[4].Value.ToString()) == 1)
                    if (Convert.ToBoolean(dgMF.Rows[i].Cells[5].Value.ToString()) == true)
                    {
                        mManufactuerDetails = new MManufacturerDetails();
                        mManufactuerDetails.LedgerNo = Convert.ToInt64(dgMF.Rows[i].Cells[2].Value.ToString());
                        mManufactuerDetails.PkSrNo = Convert.ToInt64(dgMF.Rows[i].Cells[3].Value.ToString());
                        mManufactuerDetails.ManufacturerNo = Convert.ToInt64(dgMF.Rows[i].Cells[1].Value.ToString());
                        mManufactuerDetails.UserID = DBGetVal.UserID;
                        mManufactuerDetails.CompanyNo = DBGetVal.CompanyNo;
                        mManufactuerDetails.UserID = DBGetVal.UserID;
                        mManufactuerDetails.UserDate = DBGetVal.ServerTime.Date;
                        dbMManufacturerCompany.AddMManufacturerDetails(mManufactuerDetails);
                        flag = true;
                    }
                    //else if (Convert.ToInt64(dgMF.Rows[i].Cells[3].Value.ToString()) != 0 && Convert.ToInt64(dgMF.Rows[i].Cells[4].Value.ToString()) == 0)
                    else if (Convert.ToBoolean(dgMF.Rows[i].Cells[5].Value.ToString()) == false)
                    {
                        mManufactuerDetails.PkSrNo = Convert.ToInt64(dgMF.Rows[i].Cells[3].Value.ToString());
                        dbMManufacturerCompany.DeleteMManufacturerDetails(mManufactuerDetails);
                        flag = true;
                    }
                }
                if (flag == true)
                {
                    if (dbMManufacturerCompany.ExecuteNonQueryStatements() == true)
                    {
                        OMMessageBox.Show("Manufacturer Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        if (ShortID == 0)
                        {
                            BindMF();

                            dgSupplier.Enabled = true;
                            txtSearch.Enabled = true;
                            dgMF.Enabled = false;
                            dgSupplier.Focus();
                        }
                        else
                        {
                            //ShortID = ID;
                            this.Close();
                        }
                    }
                    else
                    {
                        OMMessageBox.Show("Manufacturer not saved", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                    }
                }
                else
                {
                    if (ShortID == 0)
                    {
                        dgSupplier.Enabled = true;
                        txtSearch.Enabled = true;
                        dgMF.Enabled = false;
                        dgSupplier.Focus();
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Down)
                {
                    e.SuppressKeyPress = true;
                    if (dgSupplier.Rows.Count > 0)
                    {
                        dgSupplier.Focus();
                        dgSupplier.CurrentCell = dgSupplier[1, 0];
                    }
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    dgSupplier.Focus();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #region Delete code

        private void InitDelTable()
        {
            dtDelete.Columns.Add();
            dtDelete.Columns.Add();
        }

        private void DeleteDtls(int Type, long PkNo)
        {
            DataRow dr = null;
            dr = dtDelete.NewRow();
            dr[0] = Type;
            dr[1] = PkNo;
            dtDelete.Rows.Add(dr);
        }

        private void DeleteValues()
        {
            try
            {
                if (dtDelete != null)
                {
                    for (int i = 0; i < dtDelete.Rows.Count; i++)
                    {
                        if (Convert.ToInt32(dtDelete.Rows[i].ItemArray[0]) == 1)
                        {
                            mManufactuerDetails.PkSrNo = Convert.ToInt64(dtDelete.Rows[i].ItemArray[1]);
                            dbMManufacturerCompany.DeleteMManufacturerDetails(mManufactuerDetails);
                        }
                    }
                    dtDelete.Rows.Clear();
                }

            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }


        #endregion

        private void Calculate()
        {
            long Selected = 0, DeSelected = 0;//, Total = 0;
            for (int i = 0; i < dgMF.Rows.Count; i++)
            {

                if (Convert.ToBoolean(dgMF.Rows[i].Cells[5].Value) == false)
                    DeSelected++;
                else
                    Selected++;
            }
            lblMFG.Text = "Selected :" + Selected + "\r\nDeselected :" + DeSelected + "\r\nTotal :" + (Selected + DeSelected);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                dgSupplier.Enabled = true; txtSearch.Enabled = true;
                dgMF.Enabled = false;
                while (dgMF.Rows.Count > 0)
                    dgMF.Rows.RemoveAt(0);
                txtSearch.Text = "";
                dgSupplier.Focus();
                lblMFG.Text = "";
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgSupplier_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.F3 || e.KeyCode == Keys.Space)
                {
                    e.SuppressKeyPress = true;
                    dgSupplier.Enabled = false; txtSearch.Enabled = false;
                    dgSupplier.CurrentCell.Style.BackColor = Color.White;
                    dgMF.Enabled = true;
                    BindMF();
                    dgMF.Focus();
                }
                else if (e.KeyCode == Keys.Up || e.KeyCode == Keys.Down)
                {
                    try
                    {
                        while (dgMF.Rows.Count > 0)
                            dgMF.Rows.RemoveAt(0);
                        dgSupplier.CurrentCell.Style.BackColor = Color.White;
                        lblMFG.Text = "";
                    }
                    catch (Exception exc)
                    {
                        ObjFunction.ExceptionDisplay(exc.Message);
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgSupplier_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                while (dgMF.Rows.Count > 0)
                    dgMF.Rows.RemoveAt(0);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgSupplier_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            dgSupplier_KeyDown(sender, new KeyEventArgs(Keys.Enter));
        }

        private void dgMF_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //if (dgMF != null)
            //    dgMF_KeyDown(sender, new KeyEventArgs(Keys.Space));
        }

        private void dgMF_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (Convert.ToBoolean(dgMF.Rows[e.RowIndex].Cells[5].Value) == false)
                   dgMF.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                else
                    dgMF.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.SkyBlue;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

        }

        private void dgMF_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5)
            {
                if (Convert.ToBoolean(dgMF.Rows[e.RowIndex].Cells[5].FormattedValue) == false)
                {
                    dgMF.Rows[e.RowIndex].Cells[5].Value = true;
                    dgMF.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.SkyBlue;
                }
                else
                {
                    dgMF.Rows[e.RowIndex].Cells[5].Value = false;
                    dgMF.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                }

                Calculate();
            }
        }

        private void dgMF_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Space)
                {
                    e.SuppressKeyPress = true;
                    if (Convert.ToBoolean(dgMF.Rows[dgMF.CurrentRow.Index].Cells[5].Value) == false)
                    {
                        if (dgMF.CurrentCell.ColumnIndex == 0) dgMF.Rows[dgMF.CurrentRow.Index].Cells[5].Value = true;
                        dgMF.Rows[dgMF.CurrentRow.Index].DefaultCellStyle.BackColor = Color.SkyBlue;
                    }
                    else
                    {
                        if (dgMF.CurrentCell.ColumnIndex == 0) dgMF.Rows[dgMF.CurrentRow.Index].Cells[5].Value = false;
                        dgMF.Rows[dgMF.CurrentRow.Index].DefaultCellStyle.BackColor = Color.White;
                    }
                    //if (Convert.ToInt64(dgMF.Rows[dgMF.CurrentRow.Index].Cells[3].Value.ToString()) == 0 && Convert.ToInt64(dgMF.Rows[dgMF.CurrentRow.Index].Cells[4].Value.ToString()) == 0)
                    //{
                    //    dgMF.Rows[dgMF.CurrentRow.Index].Cells[4].Value = 1;
                    //    //dgMF.Rows[dgMF.CurrentRow.Index].DefaultCellStyle.BackColor = Color.SkyBlue;
                    //    dgMF.Rows[dgMF.CurrentRow.Index].Cells[5].Value = !Convert.ToBoolean(dgMF.Rows[dgMF.CurrentRow.Index].Cells[5].Value);
                    //}
                    //else if (Convert.ToInt64(dgMF.Rows[dgMF.CurrentRow.Index].Cells[3].Value.ToString()) == 0 && Convert.ToInt64(dgMF.Rows[dgMF.CurrentRow.Index].Cells[4].Value.ToString()) == 1)
                    //{
                    //    dgMF.Rows[dgMF.CurrentRow.Index].Cells[4].Value = 0;
                    //    //dgMF.Rows[dgMF.CurrentRow.Index].DefaultCellStyle.BackColor = Color.White;
                    //    dgMF.Rows[dgMF.CurrentRow.Index].Cells[5].Value = !Convert.ToBoolean(dgMF.Rows[dgMF.CurrentRow.Index].Cells[5].Value);
                    //}
                    //else if (Convert.ToInt64(dgMF.Rows[dgMF.CurrentRow.Index].Cells[3].Value.ToString()) != 0 && Convert.ToInt64(dgMF.Rows[dgMF.CurrentRow.Index].Cells[4].Value.ToString()) == 1)
                    //{
                    //    dgMF.Rows[dgMF.CurrentRow.Index].Cells[4].Value = 0;
                    //    //dgMF.Rows[dgMF.CurrentRow.Index].DefaultCellStyle.BackColor = Color.White;
                    //    dgMF.Rows[dgMF.CurrentRow.Index].Cells[5].Value = !Convert.ToBoolean(dgMF.Rows[dgMF.CurrentRow.Index].Cells[5].Value);
                    //}
                    //else if (Convert.ToInt64(dgMF.Rows[dgMF.CurrentRow.Index].Cells[3].Value.ToString()) != 0 && Convert.ToInt64(dgMF.Rows[dgMF.CurrentRow.Index].Cells[4].Value.ToString()) == 0)
                    //{
                    //    dgMF.Rows[dgMF.CurrentRow.Index].Cells[4].Value = 1;
                    //    //dgMF.Rows[dgMF.CurrentRow.Index].DefaultCellStyle.BackColor = Color.SkyBlue;
                    //    dgMF.Rows[dgMF.CurrentRow.Index].Cells[5].Value = !Convert.ToBoolean(dgMF.Rows[dgMF.CurrentRow.Index].Cells[5].Value);
                    //}
                    //dgMF.Rows[dgMF.CurrentRow.Index].Cells[5].Value = !Convert.ToBoolean(dgMF.Rows[dgMF.CurrentRow.Index].Cells[5].Value);
                    Calculate();
                    //if (dgMF.CurrentRow.Index >= 0 && dgMF.CurrentRow.Index < dgMF.Rows.Count - 1)
                    //    dgMF.CurrentCell = dgMF[0, dgMF.CurrentRow.Index + 1];
                    //else if (dgMF.CurrentRow.Index == dgMF.Rows.Count - 1)
                    //{
                    //    dgMF.CurrentCell = dgMF[0, dgMF.CurrentRow.Index];
                    //}
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    e.SuppressKeyPress = true;
                    dgSupplier.Enabled = false; txtSearch.Enabled = false;
                    dgMF.Enabled = true;
                    btnSave.Focus();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgMF_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //dgMF_KeyDown(sender,new KeyEventArgs(Keys.Space));
        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < dgMF.Rows.Count; i++)
                    dgMF.Rows[i].Cells[5].Value = chkSelectAll.Checked;
                dgMF.CurrentCell = dgMF[5, 0];
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        

    }
}
