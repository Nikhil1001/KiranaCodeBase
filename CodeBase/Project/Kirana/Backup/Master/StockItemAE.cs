﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Master
{
    /// <summary>
    /// This class is used for Stock Item AE
    /// </summary>
    public partial class StockItemAE : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        DBMStockItems dbStockItems = new DBMStockItems();
        MStockItems mStockItems = new MStockItems();
        MStockBarcode mStockBarcode = new MStockBarcode();
        DBMLedger dbLedger = new DBMLedger();
        MLedger mLedger = new MLedger();


        string ItemNm, BarcodeNm;
        DataTable dtSearch = new DataTable();
        //DataTable dt;
        int cntRow;//, nw, ColumnIndex;
        int defaultUOMRowNo = -1, LowerUOMRowNo = -1;
        long PreDefaultUom = 0, PreLowerUom = 0;
        long ID;

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public StockItemAE()
        {
            InitializeComponent();
        }

        private void StockItemAE_Activated(object sender, EventArgs e)
        {
            FillComboAllMasters();
        }

        private void FillComboAllMasters()
        {
            long iselected = 0;
            //iselected = ObjFunction.GetComboValue(cmbGroupNo1);
            //ObjFunction.FillCombo(cmbGroupNo1, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=3 ORDER BY StockGroupName");
            //cmbGroupNo1.SelectedValue = iselected;

            iselected = ObjFunction.GetComboValue(cmbCompanyName);
            ObjFunction.FillCombo(cmbCompanyName, "SELECT CompanyNo, CompanyName FROM MCompany WHERE CompanyUserCode != 'FALSE' ORDER BY CompanyName");
            cmbCompanyName.SelectedValue = iselected;

            iselected = ObjFunction.GetComboValue(cmbGroupNo2);
            ObjFunction.FillCombo(cmbGroupNo2, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=2 ORDER BY StockGroupName");
            cmbGroupNo2.SelectedValue = iselected;

            //iselected = ObjFunction.GetComboValue(cmbDepartmentName);
            //ObjFunction.FillCombo(cmbDepartmentName, "SELECT DepartmentNo, DepartmentName FROM MStockDepartment WHERE IsActive = 'True' ORDER BY DepartmentName");
            //cmbDepartmentName.SelectedValue = iselected;

            iselected = ObjFunction.GetComboValue(cmbCategoryName);
            ObjFunction.FillCombo(cmbCategoryName, "SELECT CategoryNo, CategoryName FROM MStockCategory WHERE IsActive = 'True' ORDER BY CategoryName");
            cmbCategoryName.SelectedValue = iselected;

            //iselected = ObjFunction.GetComboValue(cmbStockLocationName);
            //ObjFunction.FillCombo(cmbStockLocationName, "SELECT MStockLocation.StockLocationNo, MGodown.GodownName + ' - ' + MStockLocation.StockLocationName AS StockLocationName FROM MStockLocation INNER JOIN MGodown ON MStockLocation.GodownNo = MGodown.GodownNo WHERE (MStockLocation.IsActive = 'true') ORDER BY StockLocationName");
            //cmbStockLocationName.SelectedValue = iselected;

            DataGridViewComboBoxColumn cmbGridUom = gvRateSetting.Columns[ColIndex.UOM] as DataGridViewComboBoxColumn;
            if (cmbGridUom != null)
            {
                ObjFunction.FillCombo(cmbGridUom, "SELECT UOMNo,UOMName from MUOM WHERE IsActive = 'True' ORDER BY UOMName");
            }

            iselected = ObjFunction.GetComboValue(cmbVatSales);
            ObjFunction.FillCombo(cmbVatSales, "SELECT MItemTaxSetting.PkSrNo, MItemTaxSetting.TaxSettingName FROM MItemTaxSetting INNER JOIN MLedger ON MItemTaxSetting.SalesLedgerNo = MLedger.LedgerNo INNER JOIN " +
                "   MLedger AS MLedger_1 ON MItemTaxSetting.TaxLedgerNo = MLedger_1.LedgerNo " +
                " WHERE     (MLedger.GroupNo = " + GroupType.SalesAccount + ") AND (MLedger_1.GroupNo = 32) And MItemTaxSetting.IsActive='True' Order by  MItemTaxSetting.TaxSettingName ");
            cmbVatSales.SelectedValue = iselected;

            iselected = ObjFunction.GetComboValue(cmbVatPurchase);
            ObjFunction.FillCombo(cmbVatPurchase, "SELECT MItemTaxSetting.PkSrNo, MItemTaxSetting.TaxSettingName FROM MItemTaxSetting INNER JOIN MLedger ON MItemTaxSetting.SalesLedgerNo = MLedger.LedgerNo INNER JOIN " +
                "   MLedger AS MLedger_1 ON MItemTaxSetting.TaxLedgerNo = MLedger_1.LedgerNo " +
                " WHERE     (MLedger.GroupNo = " + GroupType.PurchaseAccount + ") AND (MLedger_1.GroupNo = 32) And MItemTaxSetting.IsActive='True' Order by  MItemTaxSetting.TaxSettingName ");
            cmbVatPurchase.SelectedValue = iselected;

            iselected = ObjFunction.GetComboValue(cmbCSTSales);
            ObjFunction.FillCombo(cmbCSTSales, "SELECT MItemTaxSetting.PkSrNo, MItemTaxSetting.TaxSettingName FROM MItemTaxSetting INNER JOIN MLedger ON MItemTaxSetting.SalesLedgerNo = MLedger.LedgerNo INNER JOIN " +
                "   MLedger AS MLedger_1 ON MItemTaxSetting.TaxLedgerNo = MLedger_1.LedgerNo " +
                " WHERE     (MLedger.GroupNo = " + GroupType.SalesAccount + ") AND (MLedger_1.GroupNo = 35) And MItemTaxSetting.IsActive='True' Order by  MItemTaxSetting.TaxSettingName ");
            cmbCSTSales.SelectedValue = iselected;
        }

        private void CheckActive()
        {
            //cmbDepartmentName.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_DepartmentDisplay));
            //label8.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_DepartmentDisplay));

            //cmbCategoryName.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_CategoryDisplay));
            //label9.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_CategoryDisplay));

            txtBarcobe.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_BarCodeDisplay));
            label11.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_BarCodeDisplay));

            //cmbStockLocationName.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_StockLocation));
            //label16.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_StockLocation));
        }

        private void StockItemAE_Load(object sender, EventArgs e)
        {
            btnNewBrand.Visible = false;
            FillComboAllMasters();

            ObjFunction.FillCombo(cmbStockDept, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=4 ORDER BY StockGroupName");
            ObjFunction.FillCombo(cmbGroupNo1, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=3 AND StockGroupNo=0 ORDER BY StockGroupName");
            ObjFunction.FillCombo(cmbGroupNo2, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=2 AND StockGroupNo=0 ORDER BY StockGroupName");

            CheckActive();
            ObjFunction.LockButtons(true, this.Controls);
            ObjFunction.LockControls(false, this.Controls);
            txtSearchBarCode.Enabled = true;

            ItemNm = "";
            BarcodeNm = "";

            dtSearch = ObjFunction.GetDataView("Select ItemNo From MStockItems_V(NULL,NULL,NULL,NULL,NULL,NULL,NULL) WHERE ItemNo<>1 ORDER BY ItemName").Table;

            if (dtSearch.Rows.Count > 0)
            {
                if (StockItem.RequestItemNo == 0)
                    ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                else
                    ID = StockItem.RequestItemNo;
                FillControls();
                SetNavigation();
            }

            DisplayColumns();
            setDisplay(true);
            btnNew.Focus();
            KeyDownFormat(this.Controls);
            gvRateSetting.ClearSelection();
        }

        private void StockItemAE_FormClosing(object sender, FormClosingEventArgs e)
        {
            ID = 0;
            ItemNm = "";
            BarcodeNm = "";

        }

        private void FillControls()
        {
           // EP.SetError(txtItemName, "");
            EP.SetError(txtShortName, "");
            EP.SetError(cmbStockDept, "");
            EP.SetError(cmbGroupNo1, "");
            EP.SetError(cmbGroupNo2, "");
            EP.SetError(cmbDepartmentName, "");
            EP.SetError(cmbCategoryName, "");
            EP.SetError(cmbCompanyName, "");
            EP.SetError(txtBarcobe, "");
            EP.SetError(txtMRP, "");
            EP.SetError(gvRateSetting, "");
            EP.SetError(txtReOrderLevelQty, "");
            //EP.SetError(cmbStockLocationName, "");
            EP.SetError(txtMaxLevelQty, "");
            EP.SetError(txtReOrderLevelQty, "");
            EP.SetError(cmbVatSales, "");
            EP.SetError(cmbVatPurchase, "");

            gvRateSetting.Enabled = false;

            mStockItems = dbStockItems.ModifyMStockItemsByID(ID);
            ItemNm = mStockItems.ItemName;

            cmbCompanyName.SelectedValue = mStockItems.CompanyNo.ToString();
            txtItemName.Text = mStockItems.ItemName;
            txtShortName.Text = mStockItems.ItemShortCode;
            cmbStockDept.SelectedValue = mStockItems.FKStockDeptNo.ToString();

            ObjFunction.FillCombo(cmbGroupNo2, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=2 AND ControlSubGroup=" + ObjFunction.GetComboValue(cmbStockDept) + " ORDER BY StockGroupName");
            cmbGroupNo2.SelectedValue = mStockItems.GroupNo1.ToString();


            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_IsBrandFilter)) == true)
                ObjFunction.FillCombo(cmbGroupNo1, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=3 AND ControlSubGroup=" + ObjFunction.GetComboValue(cmbGroupNo2) + " ORDER BY StockGroupName");
            else
                ObjFunction.FillCombo(cmbGroupNo1, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=3 ORDER BY StockGroupName");

           // ObjFunction.FillCombo(cmbGroupNo1, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=3 AND ControlSubGroup=" + ObjFunction.GetComboValue(cmbGroupNo2) + " ORDER BY StockGroupName");
            cmbGroupNo1.SelectedValue = mStockItems.GroupNo.ToString();

            cmbCategoryName.SelectedValue = mStockItems.FkCategoryNo.ToString();
            cmbDepartmentName.SelectedValue = mStockItems.FkDepartmentNo.ToString();
            //cmbStockLocationName.SelectedValue = mStockItems.FKStockLocationNo.ToString();
            txtReOrderLevelQty.Text = mStockItems.MinLevel.ToString();
            txtMaxLevelQty.Text = mStockItems.MaxLevel.ToString();
            chkActive.Checked = mStockItems.IsActive;
            if (chkActive.Checked == true)
                chkActive.Text = "Yes";
            else
                chkActive.Text = "No";


            InitRateSetting();

            DataTable dtItemTax = ObjFunction.GetDataView("Select * From dbo.GetItemTaxAll(" + ID + ", NULL, NULL,NULL,NULL) ").Table;
            DataRow[] dr = dtItemTax.Select("GroupNo=" + GroupType.SalesAccount + " AND TaxTypeNo=32");
            if (dr.Length > 0)
                cmbVatSales.SelectedValue = dr[0][10].ToString();

            dr = dtItemTax.Select("GroupNo=" + GroupType.PurchaseAccount + " AND TaxTypeNo=32");
            if (dr.Length > 0)
                cmbVatPurchase.SelectedValue = dr[0][10].ToString();

            dr = dtItemTax.Select("GroupNo=" + GroupType.SalesAccount + " AND TaxTypeNo=35");
            if (dr.Length > 0)
                cmbCSTSales.SelectedValue = dr[0][10].ToString();

            FillUomCombos();

            cmbDefaultUOM.SelectedValue = mStockItems.UOMDefault.ToString();
            cmbLowerUOM.SelectedValue = mStockItems.UOMPrimary.ToString();

            FormatStockConversionControls();

            if (Convert.ToBoolean(gvRateSetting.Rows[0].Cells[ColIndex.IsActive].FormattedValue) == true
                && gvRateSetting.Rows[0].Cells[ColIndex.BarCode].Value != null)
            {
                txtBarcobe.Text = gvRateSetting.Rows[0].Cells[ColIndex.BarCode].Value.ToString();
                txtMRP.Text = gvRateSetting.Rows[0].Cells[ColIndex.MRP].Value.ToString();
                BarcodeNm = txtBarcobe.Text.Trim().ToUpper();
            }
            else
            {
                txtBarcobe.Text = ObjQry.ReturnString("select Barcode from MStockBarcode where ItemNo=" + ID + "", CommonFunctions.ConStr);
                BarcodeNm = txtBarcobe.Text.Trim().ToUpper();
                txtMRP.Text = "0.00";
            }
           
            btnNew.Focus();
        }

        private void SetValue()
        {
            if (Validations() == true)
            {
                dbStockItems = new DBMStockItems();
                mStockItems = new MStockItems();

                mStockItems.ItemNo = ID;
                mStockItems.ItemName = txtItemName.Text.Trim().ToUpper();
                mStockItems.ItemShortCode = txtShortName.Text.Trim().ToUpper();
                mStockItems.FKStockDeptNo = ObjFunction.GetComboValue(cmbStockDept);//Stock Department
                mStockItems.GroupNo = ObjFunction.GetComboValue(cmbGroupNo1);//Brand Name
                mStockItems.GroupNo1 = ObjFunction.GetComboValue(cmbGroupNo2);//Main Group
                mStockItems.FkCategoryNo = ObjFunction.GetComboValue(cmbCategoryName);
                mStockItems.CompanyNo = ObjFunction.GetComboValue(cmbCompanyName);
                mStockItems.FkDepartmentNo = ObjFunction.GetComboValue(cmbDepartmentName);
                mStockItems.FKStockLocationNo = 1;// ObjFunction.GetComboValue(cmbStockLocationName);
                mStockItems.UOMDefault = ObjFunction.GetComboValue(cmbDefaultUOM);
                mStockItems.UOMPrimary = ObjFunction.GetComboValue(cmbLowerUOM);
                mStockItems.CompanyNo = ObjFunction.GetComboValue(cmbCompanyName);
                mStockItems.MinLevel = Convert.ToDouble(txtReOrderLevelQty.Text);
                mStockItems.MaxLevel = Convert.ToDouble(txtMaxLevelQty.Text);
                //mStockItems.ReOrderLevelQty = Convert.ToDouble(txtReOrderLevelQty.Text);
                mStockItems.IsActive = chkActive.Checked;
                mStockItems.IsFixedBarcode = false;
                mStockItems.UserId = DBGetVal.UserID;
                mStockItems.UserDate = DBGetVal.ServerTime.Date;
                dbStockItems.AddMStockItems(mStockItems);

                mStockBarcode = new MStockBarcode();
                long barcodeno = ObjQry.ReturnLong("select isNull((PkStockBarcodeNo),0) from MStockBarcode where Barcode='" + txtBarcobe.Text.Trim() + "' AND ItemNo=" + ID + "", CommonFunctions.ConStr);
                if (barcodeno != 0)
                {
                    mStockBarcode.PkStockBarcodeNo = barcodeno;
                }
                else
                {
                    mStockBarcode.PkStockBarcodeNo = (gvRateSetting.Rows[0].Cells[ColIndex.BarCodeNo].Value == null) ? 0 : Convert.ToInt64(gvRateSetting.Rows[0].Cells[ColIndex.BarCodeNo].Value);//ObjQry.ReturnLong("select isNull((PkStockBarcodeNo),0) from MStockBarcode where Barcode='" + txtBarcobe.Text.Trim() + "'",CommonFunctions.ConStr);
                }

                mStockBarcode.Barcode = txtBarcobe.Text;
                mStockBarcode.IsActive = chkActive.Checked;
                mStockBarcode.UserID = DBGetVal.UserID;
                mStockBarcode.UserDate = DBGetVal.ServerTime;
                dbStockItems.AddMStockBarcode(mStockBarcode);
                MRateSetting mRateSetting = new MRateSetting();
                for (int i = 0; i < gvRateSetting.Rows.Count; i++)
                {
                    mRateSetting = new MRateSetting();
                    if (gvRateSetting.Rows[i].Cells[ColIndex.UOM].Value != null &&
                        gvRateSetting.Rows[i].Cells[ColIndex.ASaleRate].Value != null &&
                        //gvRateSetting.Rows[i].Cells[ColIndex.BarCode].Value != null && 
                        gvRateSetting.Rows[i].Cells[ColIndex.BSaleRate].Value != null &&
                        gvRateSetting.Rows[i].Cells[ColIndex.CSaleRate].Value != null &&
                        gvRateSetting.Rows[i].Cells[ColIndex.DSaleRate].Value != null &&
                        gvRateSetting.Rows[i].Cells[ColIndex.ESaleRate].Value != null &&
                        gvRateSetting.Rows[i].Cells[ColIndex.MKTQty].Value != null &&
                        gvRateSetting.Rows[i].Cells[ColIndex.MRP].Value != null &&
                        gvRateSetting.Rows[i].Cells[ColIndex.RateVariation].Value.ToString() != "0" &&
                        gvRateSetting.Rows[i].Cells[ColIndex.StockConversion].Value.ToString() != "0" &&
                        gvRateSetting.Rows[i].Cells[ColIndex.Date].Value != null)
                    {
                        if (Convert.ToBoolean(gvRateSetting.Rows[i].Cells[ColIndex.Chk].FormattedValue) == true)
                        {
                            if (Convert.ToBoolean(gvRateSetting.Rows[i].Cells[ColIndex.IsActive].FormattedValue) == true)
                            {
                                long RatePk = (gvRateSetting.Rows[i].Cells[ColIndex.PkSrNo].Value == null) ? 0 : Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.PkSrNo].Value);
                                if (RatePk != 0)
                                {
                                    if (ObjQry.ReturnLong("SELECT PkSrNo FROM MRateSetting WHERE (PkSrNo = " + RatePk + ") AND (UOMNo =" + Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.UOM].Value) + " ) AND (MRP = " + ((gvRateSetting.Rows[i].Cells[ColIndex.MRP].Value == null) ? 0 : Convert.ToDouble(txtMRP.Text)) + ")", CommonFunctions.ConStr) != 0)
                                    {
                                        MRateSetting mRateSettings = new MRateSetting();
                                        mRateSettings.PkSrNo = RatePk;
                                        mRateSettings.IsActive = false;
                                        dbStockItems.UpdateMRateSetting(mRateSettings);
                                        mRateSettings.PkSrNo = 0;
                                    }
                                }
                                if (Convert.ToBoolean(gvRateSetting.Rows[i].Cells[ColIndex.HidChk].FormattedValue) == false)
                                {

                                    mRateSetting.PkSrNo = 0;
                                    if (gvRateSetting.Rows[i].Cells[ColIndex.Date].Value.ToString() == "0")
                                    {
                                        mRateSetting.FromDate = Convert.ToDateTime("01-01-1900"); /*DateTime.Now*/
                                    }
                                    else
                                    {
                                        mRateSetting.FromDate = Convert.ToDateTime(gvRateSetting.Rows[i].Cells[ColIndex.Date].Value);
                                    }
                                }
                                else
                                {
                                    if (Convert.ToBoolean(gvRateSetting.Rows[i].Cells[ColIndex.Chk].FormattedValue) == true)
                                    {
                                        mRateSetting.PkSrNo = 0;
                                        mRateSetting.FromDate = Convert.ToDateTime("01-01-1900");// DateTime.Now;
                                    }
                                }
                                mRateSetting.PurRate = Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.PurRate].Value);
                                mRateSetting.UOMNo = Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.UOM].Value);
                                mRateSetting.ASaleRate = (gvRateSetting.Rows[i].Cells[ColIndex.ASaleRate].Value == null) ? 0 : Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.ASaleRate].Value);
                                mRateSetting.BSaleRate = (gvRateSetting.Rows[i].Cells[ColIndex.BSaleRate].Value == null) ? 0 : Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.BSaleRate].Value);
                                mRateSetting.CSaleRate = (gvRateSetting.Rows[i].Cells[ColIndex.CSaleRate].Value == null) ? 0 : Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.CSaleRate].Value);
                                mRateSetting.DSaleRate = (gvRateSetting.Rows[i].Cells[ColIndex.DSaleRate].Value == null) ? 0 : Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.DSaleRate].Value);
                                mRateSetting.ESaleRate = (gvRateSetting.Rows[i].Cells[ColIndex.ESaleRate].Value == null) ? 0 : Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.ESaleRate].Value);
                                mRateSetting.StockConversion = Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.StockConversion].Value);
                                mRateSetting.PerOfRateVariation = Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.RateVariation].Value);
                                mRateSetting.MKTQty = Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.MKTQty].Value);
                                mRateSetting.MRP = (gvRateSetting.Rows[i].Cells[ColIndex.MRP].Value == null) ? 0 : Convert.ToDouble(txtMRP.Text);
                                mRateSetting.IsActive = Convert.ToBoolean(gvRateSetting.Rows[i].Cells[ColIndex.IsActive].FormattedValue);//true;
                                mRateSetting.UserID = DBGetVal.UserID;
                                mRateSetting.UserDate = DBGetVal.ServerTime.Date;


                                dbStockItems.AddMRateSetting2(mRateSetting);
                            }
                            else
                            {
                                mRateSetting.PkSrNo = (gvRateSetting.Rows[i].Cells[ColIndex.PkSrNo].Value == null) ? 0 : Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.PkSrNo].Value);
                                mRateSetting.IsActive = Convert.ToBoolean(gvRateSetting.Rows[i].Cells[ColIndex.IsActive].FormattedValue);
                                dbStockItems.UpdateMRateSetting(mRateSetting);
                            }


                        }
                    }
                }

                DataTable dtItemTax = ObjFunction.GetDataView("Select * From dbo.GetItemTaxAll(" + ID + ", NULL, NULL,NULL,NULL)").Table;
                DataTable dtMainItemTax = ObjFunction.GetDataView("Select * From MItemTaxSetting Where PkSrNo in (" + ObjFunction.GetComboValue(cmbVatSales) + "," + ObjFunction.GetComboValue(cmbVatPurchase) + "," + ObjFunction.GetComboValue(cmbCSTSales) + ")").Table;
                if (ObjFunction.GetComboValue(cmbVatSales) != 0)
                {
                    MItemTaxInfo mItemTaxInfo = new MItemTaxInfo();

                    DataRow[] drMain = dtMainItemTax.Select("PkSrNo=" + ObjFunction.GetComboValue(cmbVatSales) + "");
                    DataRow[] dr = dtItemTax.Select("GroupNo=" + GroupType.SalesAccount + " AND TaxTypeNo=32");

                    if (dr.Length > 0)
                    {
                        if (DBGetVal.ServerTime.Date != Convert.ToDateTime(dr[0][4].ToString()).Date)
                        {
                            if (Convert.ToInt64(dr[0][10].ToString()) == ObjFunction.GetComboValue(cmbVatSales))
                                mItemTaxInfo.PkSrNo = Convert.ToInt64(dr[0][0].ToString());
                            else
                                mItemTaxInfo.PkSrNo = 0;
                        }
                        else
                            mItemTaxInfo.PkSrNo = Convert.ToInt64(dr[0][0].ToString());
                    }
                    else
                    {
                        mItemTaxInfo.PkSrNo = 0;
                    }
                    mItemTaxInfo.TaxLedgerNo = Convert.ToInt64(drMain[0][2].ToString());
                    mItemTaxInfo.SalesLedgerNo = Convert.ToInt64(drMain[0][3].ToString());
                    mItemTaxInfo.FromDate = DBGetVal.ServerTime.Date;
                    mItemTaxInfo.CalculationMethod = "2";
                    mItemTaxInfo.Percentage = Convert.ToDouble(drMain[0][5].ToString());
                    mItemTaxInfo.FKTaxSettingNo = Convert.ToInt64(drMain[0][0].ToString());
                    mItemTaxInfo.UserID = DBGetVal.UserID;
                    mItemTaxInfo.UserDate = DBGetVal.ServerTime.Date;
                    dbStockItems.AddMItemTaxInfo(mItemTaxInfo);
                }

                if (ObjFunction.GetComboValue(cmbVatPurchase) != 0)
                {
                    MItemTaxInfo mItemTaxInfo = new MItemTaxInfo();

                    DataRow[] drMain = dtMainItemTax.Select("PkSrNo=" + ObjFunction.GetComboValue(cmbVatPurchase) + "");
                    DataRow[] dr = dtItemTax.Select("GroupNo=" + GroupType.PurchaseAccount + " AND TaxTypeNo=32");

                    if (dr.Length > 0)
                    {
                        if (DBGetVal.ServerTime.Date != Convert.ToDateTime(dr[0][4].ToString()).Date)
                        {
                            if (Convert.ToInt64(dr[0][10].ToString()) == ObjFunction.GetComboValue(cmbVatPurchase))
                                mItemTaxInfo.PkSrNo = Convert.ToInt64(dr[0][0].ToString());
                            else
                                mItemTaxInfo.PkSrNo = 0;
                        }
                        else
                            mItemTaxInfo.PkSrNo = Convert.ToInt64(dr[0][0].ToString());
                    }
                    else
                    {
                        mItemTaxInfo.PkSrNo = 0;
                    }
                    mItemTaxInfo.TaxLedgerNo = Convert.ToInt64(drMain[0][2].ToString());
                    mItemTaxInfo.SalesLedgerNo = Convert.ToInt64(drMain[0][3].ToString());
                    mItemTaxInfo.FromDate = DBGetVal.ServerTime.Date;
                    mItemTaxInfo.CalculationMethod = "2";
                    mItemTaxInfo.Percentage = Convert.ToDouble(drMain[0][5].ToString());
                    mItemTaxInfo.FKTaxSettingNo = Convert.ToInt64(drMain[0][0].ToString());
                    mItemTaxInfo.UserID = DBGetVal.UserID;
                    mItemTaxInfo.UserDate = DBGetVal.ServerTime.Date;
                    dbStockItems.AddMItemTaxInfo(mItemTaxInfo);
                }

                if (ObjFunction.GetComboValue(cmbCSTSales) != 0)
                {
                    MItemTaxInfo mItemTaxInfo = new MItemTaxInfo();

                    DataRow[] drMain = dtMainItemTax.Select("PkSrNo=" + ObjFunction.GetComboValue(cmbCSTSales) + "");
                    DataRow[] dr = dtItemTax.Select("GroupNo=" + GroupType.SalesAccount + " AND TaxTypeNo=35");

                    if (dr.Length > 0)
                    {
                        if (Convert.ToInt64(dr[0][10].ToString()) == ObjFunction.GetComboValue(cmbCSTSales))
                            mItemTaxInfo.PkSrNo = Convert.ToInt64(dr[0][0].ToString());
                        else
                            mItemTaxInfo.PkSrNo = 0;
                    }
                    else
                    {
                        mItemTaxInfo.PkSrNo = 0;
                    }
                    mItemTaxInfo.TaxLedgerNo = Convert.ToInt64(drMain[0][2].ToString());
                    mItemTaxInfo.SalesLedgerNo = Convert.ToInt64(drMain[0][3].ToString());
                    mItemTaxInfo.FromDate = DBGetVal.ServerTime.Date;
                    mItemTaxInfo.CalculationMethod = "2";
                    mItemTaxInfo.Percentage = Convert.ToDouble(drMain[0][5].ToString());
                    mItemTaxInfo.FKTaxSettingNo = Convert.ToInt64(drMain[0][0].ToString());
                    mItemTaxInfo.UserID = DBGetVal.UserID;
                    mItemTaxInfo.UserDate = DBGetVal.ServerTime.Date;
                    dbStockItems.AddMItemTaxInfo(mItemTaxInfo);
                }

                if (dbStockItems.ExecuteNonQueryStatements() == true)
                {
                    if (ID == 0)
                    {
                        OMMessageBox.Show(" Item Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        dtSearch = ObjFunction.GetDataView("Select ItemNo From MStockItems WHERE ItemNo<>1 Order By ItemName").Table;
                        ID = ObjQry.ReturnLong("Select Max(ItemNo) FRom MStockItems", CommonFunctions.ConStr);
                        SetNavigation();
                        FillControls();
                    }
                    else
                    {
                        OMMessageBox.Show(" Item Updated Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        FillControls();
                    }

                    ObjFunction.LockButtons(true, this.Controls);
                    ObjFunction.LockControls(false, this.Controls);
                    txtSearchBarCode.Enabled = true;
                    //btnNew_Click(new object(), new EventArgs());
                }
                else
                {
                    OMMessageBox.Show(" Item not saved", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                }
            }
        }

        private bool Validations()
        {
            bool flag = true;
            EP.SetError(cmbStockDept, "");
            EP.SetError(cmbGroupNo2, "");
            EP.SetError(cmbCompanyName, "");
            EP.SetError(cmbGroupNo1, "");
            //EP.SetError(txtItemName, "");
            EP.SetError(txtShortName, "");
            EP.SetError(cmbDepartmentName, "");
            EP.SetError(cmbCategoryName, "");
            EP.SetError(txtBarcobe, "");
            EP.SetError(txtMRP, "");
            EP.SetError(gvRateSetting, "");
            EP.SetError(cmbVatPurchase, "");
            EP.SetError(cmbVatSales, "");

            if (ObjFunction.GetComboValue(cmbStockDept) <= 0)
            {
                EP.SetError(cmbStockDept, "Select Department");
                EP.SetIconAlignment(cmbStockDept, ErrorIconAlignment.MiddleRight);
                if (flag) { flag = false; cmbStockDept.Focus(); }
            }
            if (ObjFunction.GetComboValue(cmbGroupNo2) <= 0)
            {
                EP.SetError(cmbGroupNo2, "Select Main Group Name");
                EP.SetIconAlignment(cmbGroupNo2, ErrorIconAlignment.MiddleRight);
                if (flag) { flag = false; cmbGroupNo2.Focus(); }
            }
            if (ObjFunction.GetComboValue(cmbVatSales) <= 0)
            {
                EP.SetError(cmbVatSales, "Select Vat Sales Name");
                EP.SetIconAlignment(cmbVatSales, ErrorIconAlignment.MiddleRight);
                if (flag) { flag = false; cmbVatSales.Focus(); }
            }
            if (ObjFunction.GetComboValue(cmbVatPurchase) <= 0)
            {
                EP.SetError(cmbVatPurchase, "Select Vat Purchase Name");
                EP.SetIconAlignment(cmbVatPurchase, ErrorIconAlignment.MiddleRight);
                if (flag) { flag = false; cmbVatPurchase.Focus(); }
            }
            if (ObjFunction.GetComboValue(cmbCompanyName) <= 0)
            {
                EP.SetError(cmbCompanyName, "Select Company Name");
                EP.SetIconAlignment(cmbCompanyName, ErrorIconAlignment.MiddleRight);
                if (flag) { flag = false; cmbCompanyName.Focus(); }
            }
            if (ObjFunction.GetComboValue(cmbGroupNo1) <= 0)
            {
                EP.SetError(cmbGroupNo1, "Select Brand Name");
                EP.SetIconAlignment(cmbGroupNo1, ErrorIconAlignment.MiddleRight);
                if (flag) { flag = false; cmbGroupNo1.Focus(); }
            }
            if (txtItemName.Text.Trim() == "")
            {
                EP.SetError(txtItemName, "Enter Item Name");
                EP.SetIconAlignment(txtItemName, ErrorIconAlignment.MiddleRight);
                if (flag) { flag = false; txtItemName.Focus(); }
            }
            if (ItemNm != txtItemName.Text)
            {
                if (ObjQry.ReturnInteger("Select Count(*) from MStockItems where ItemName = '" + txtItemName.Text + "' " +
                    " AND GroupNo = " + ObjFunction.GetComboValue(cmbGroupNo1) +
                    " AND GroupNo1 = " + ObjFunction.GetComboValue(cmbGroupNo2) +
                    " AND CompanyNo = " + ObjFunction.GetComboValue(cmbCompanyName), CommonFunctions.ConStr) != 0)
                {
                    EP.SetError(txtItemName, "Duplicate Item Name");
                    EP.SetIconAlignment(txtItemName, ErrorIconAlignment.MiddleRight);
                    if (flag) { flag = false; txtItemName.Focus(); }
                }
            }
            //if (txtShortName.Text.Trim() == "")
            //{
            //    EP.SetError(txtShortName, "Enter Short Name");
            //    EP.SetIconAlignment(txtShortName, ErrorIconAlignment.MiddleRight);
            //    if (flag) { flag = false; txtShortName.Focus(); }
            //}
            //if (ObjFunction.GetComboValue(cmbDepartmentName) <= 0)
            //{
            //    EP.SetError(cmbDepartmentName, "Select Department Name");
            //    EP.SetIconAlignment(cmbDepartmentName, ErrorIconAlignment.MiddleRight);
            //    if (flag) { flag = false; cmbDepartmentName.Focus(); }
            //}
            //if (ObjFunction.GetComboValue(cmbCategoryName) <= 0)
            //{
            //    EP.SetError(cmbCategoryName, "Select Category Name");
            //    EP.SetIconAlignment(cmbCategoryName, ErrorIconAlignment.MiddleRight);
            //    if (flag) { flag = false; cmbCategoryName.Focus(); }
            //}
            if (BarcodeNm != txtBarcobe.Text)
            {
                if (ObjQry.ReturnInteger("Select Count(*) from MStockBarcode where Barcode = '" + txtBarcobe.Text + "'", CommonFunctions.ConStr) != 0)
                //if (ObjQry.ReturnInteger("Select Count(*) from MStockBarcode B INNER JOIN MStockItems I " +
                //         " ON B.ItemNo = I.ItemNo where B.Barcode = '" + txtBarcobe.Text + "' " +
                //         " AND GroupNo = " + ObjFunction.GetComboValue(cmbGroupNo1) +
                //         " AND GroupNo1 = " + ObjFunction.GetComboValue(cmbGroupNo2) +
                //         " AND I.ItemNo <> " + ID, CommonFunctions.ConStr) != 0)
                {

                    EP.SetError(txtBarcobe, "Duplicate Barcode Name");
                    EP.SetIconAlignment(txtBarcobe, ErrorIconAlignment.MiddleRight);
                    if (flag) { flag = false; txtBarcobe.Focus(); }
                }
            }
            if (txtMRP.Text.Trim() == "")
            {
                EP.SetError(txtMRP, "Enter MRP");
                EP.SetIconAlignment(txtMRP, ErrorIconAlignment.MiddleRight);
                if (flag) { flag = false; txtMRP.Focus(); }
            }

            bool rsflag = false;
            for (int i = 0; i < gvRateSetting.Rows.Count; i++)
            {
                if (Convert.ToBoolean(gvRateSetting.Rows[0].Cells[ColIndex.Chk].FormattedValue) == true
                    ||
                    Convert.ToInt64(gvRateSetting.Rows[0].Cells[ColIndex.PkSrNo].FormattedValue) != 0)
                {
                    rsflag = true;
                    break;
                }
            }

            if (rsflag == false)
            {
                EP.SetError(gvRateSetting, "Enter Atleast One Rate");
                EP.SetIconAlignment(gvRateSetting, ErrorIconAlignment.MiddleRight);
                if (flag) { flag = false; gvRateSetting.Focus(); }
            }

            return flag;
        }

        private void DisplayColumns()
        {
            gvRateSetting.Columns[ColIndex.ASaleRate].Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ARateIsActive));
            gvRateSetting.Columns[ColIndex.BSaleRate].Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.BRateIsActive));
            gvRateSetting.Columns[ColIndex.CSaleRate].Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.CRateIsActive));
            gvRateSetting.Columns[ColIndex.DSaleRate].Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.DRateIsActive));
            gvRateSetting.Columns[ColIndex.ESaleRate].Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ERateIsActive));

            gvRateSetting.Columns[ColIndex.ASaleRate].HeaderText = ObjFunction.GetAppSettings(AppSettings.ARateLabel);
            gvRateSetting.Columns[ColIndex.BSaleRate].HeaderText = ObjFunction.GetAppSettings(AppSettings.BRateLabel);
            gvRateSetting.Columns[ColIndex.CSaleRate].HeaderText = ObjFunction.GetAppSettings(AppSettings.CRateLabel);
            gvRateSetting.Columns[ColIndex.DSaleRate].HeaderText = ObjFunction.GetAppSettings(AppSettings.DRateLabel);
            gvRateSetting.Columns[ColIndex.ESaleRate].HeaderText = ObjFunction.GetAppSettings(AppSettings.ERateLabel);
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            StockItem.RequestItemNo = 0;
            this.Close();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            SetValue();
            btnNewBrand.Visible = false;
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            if (chkActive.Checked == false) return;
            dbStockItems = new DBMStockItems();
            mStockItems = new MStockItems();

            mStockItems.ItemNo = ID;
            if (OMMessageBox.Show("Are you sure want to delete this record?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (dbStockItems.DeleteMStockItems(mStockItems) == true)
                {
                    OMMessageBox.Show("Item Deleted Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                    FillControls();
                }
                else
                {
                    OMMessageBox.Show("Item not Deleted", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                }

            }

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            Form NewF = new StockItem();
            this.Close();
            ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            //nw = 0;
            ID = 0;
            ObjFunction.InitialiseControl(this.Controls);
            ObjFunction.LockButtons(false, this.Controls);
            ObjFunction.LockControls(true, this.Controls);

            chkActive.Checked = true;

            if (cmbCompanyName.Items.Count == 2) cmbCompanyName.SelectedIndex = 1;
            if (cmbDepartmentName.Items.Count == 2) cmbDepartmentName.SelectedIndex = 1;
            if (cmbCategoryName.Items.Count == 2) cmbCategoryName.SelectedIndex = 1;
            if (cmbStockLocationName.Items.Count == 2) cmbStockLocationName.SelectedIndex = 1;
            if (cmbGroupNo1.Items.Count == 2) cmbGroupNo1.SelectedIndex = 1;
            if (cmbGroupNo2.Items.Count == 2) cmbGroupNo2.SelectedIndex = 1;

            if (cmbDepartmentName.Visible == false) cmbDepartmentName.SelectedValue = "1";
            if (cmbCategoryName.Visible == false) cmbCategoryName.SelectedValue = "1";
            //if (cmbStockLocationName.Visible == false) cmbStockLocationName.SelectedValue = "1";

            InitRateSetting();
            //cmbGroupNo2.Focus();
            txtBarcobe.Focus();
            BarcodeNm = "";
            ItemNm = "";

            txtSearchBarCode.Enabled = false;
            //txtLanguage.Enabled = false;
            gvRateSetting.Enabled = true;
            DisplayLabel();
            btnNewBrand.Visible = true;
        }

        private void DisplayLabel()
        {
            label3.Visible = false;
            lblDefault.Visible = false;
            lblDefaultOther.Visible = false;
            lbl1.Visible = false;
            lblLower.Visible = false;
            lblLowerOther.Visible = false;
            txtStockCon.Visible = false;
            txtStockConOther.Visible = false;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            NavigationDisplay(5);

            ObjFunction.LockButtons(true, this.Controls);
            ObjFunction.LockControls(false, this.Controls);
            txtSearchBarCode.Enabled = true;
            btnNewBrand.Visible = false;
            InitRateSetting();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            ObjFunction.LockButtons(false, this.Controls);
            ObjFunction.LockControls(true, this.Controls);
            chkActive.Checked = true;
            InitRateSetting();

            gvRateSetting.Enabled = true;
            txtSearchBarCode.Enabled = false;

            txtBarcobe.Focus();
            btnNewBrand.Visible = false;
        }

        #region Grid RateSetting Methods

        private void InitRateSetting()
        {
            DataTable dt = new DataTable();
            string str = "SELECT 0 AS srNo, MRateSetting.FromDate, MStockBarcode.Barcode, MRateSetting.MRP, MUOM.UOMNo, MRateSetting.ASaleRate, MRateSetting.BSaleRate, " +
                        " MRateSetting.CSaleRate, MRateSetting.DSaleRate, MRateSetting.ESaleRate, MRateSetting.MKTQty, MRateSetting.PurRate, MRateSetting.PerOfRateVariation, " +
                        " 'false' AS Chk, MRateSetting.StockConversion, MRateSetting.PkSrNo, MRateSetting.FkBcdSrNo, MRateSetting.IsActive, MRateSetting.IsActive as HidChk" +
                        " FROM  MStockBarcode INNER JOIN " +
                        " MUOM INNER JOIN " +
                        " dbo.GetItemRateAll(" + ID + ",null,null,null,null,NULL) as MRateSetting ON MUOM.UOMNo = MRateSetting.UOMNo INNER JOIN " +
                        " MStockItems ON MRateSetting.ItemNo = MStockItems.ItemNo ON MStockBarcode.PkStockBarcodeNo = MRateSetting.FkBcdSrNo ";
            dt = ObjFunction.GetDataView(str).Table;
            gvRateSetting.Rows.Clear();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                gvRateSetting.Rows.Add();
                for (int j = 0; j < gvRateSetting.Columns.Count; j++)
                {
                    if (j == ColIndex.IsActive)
                        gvRateSetting.Rows[i].Cells[ColIndex.IsActive].Value = false;

                    if (j == ColIndex.HidChk)
                        gvRateSetting.Rows[i].Cells[ColIndex.HidChk].Value = false;

                    gvRateSetting.Rows[i].Cells[j].Value = dt.Rows[i].ItemArray[j];
                }
                gvRateSetting.Rows[i].Cells[ColIndex.Chk].Value = "False";
            }
            for (int i = gvRateSetting.Rows.Count; i < 3; i++)
            {
                gvRateSetting.Rows.Add();
                gvRateSetting.Rows[i].Cells[ColIndex.Sr].Value = "0";
                gvRateSetting.Rows[i].Cells[ColIndex.Date].Value = "0";

                gvRateSetting.Rows[i].Cells[ColIndex.PurRate].Value = "0";

                for (int irate = 5; irate < 10; irate++)
                {
                    gvRateSetting.Rows[i].Cells[irate].Value = "0";
                }

                gvRateSetting.Rows[i].Cells[ColIndex.StockConversion].Value = "0";
                gvRateSetting.Rows[i].Cells[ColIndex.PkSrNo].Value = "0";
                gvRateSetting.Rows[i].Cells[ColIndex.BarCodeNo].Value = "0";
                gvRateSetting.Rows[i].Cells[ColIndex.MRP].Value = "0";
                gvRateSetting.Rows[i].Cells[ColIndex.MKTQty].Value = "1";
                gvRateSetting.Rows[i].Cells[ColIndex.RateVariation].Value = "10";
            }
            gvRateSetting.ClearSelection();
            gvRateSetting.CurrentCell = null;
        }

        private void gvRateSetting_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 0)
                e.Value = (e.RowIndex + 1).ToString();
        }

        private delegate void MovetoNextCell(int RowIndex, int ColIndex, DataGridView dg);

        private void m2nC(int RowIndex, int ColIndex, DataGridView dg)
        {
            dg.CurrentCell = dg.Rows[RowIndex].Cells[ColIndex];
        }

        private void gvRateSetting_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (gvRateSetting.Rows.Count > 0 && e.ColumnIndex != ColIndex.Chk)//&& e.ColumnIndex!=ColIndex.IsActive)
                {
                    gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Chk].Value = true;
                    if (e.ColumnIndex != ColIndex.IsActive)
                        gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.IsActive].Value = true;
                    for (int i = 0; i < gvRateSetting.Columns.Count - 1; i++)
                    {
                        if (
                             (gvRateSetting.Rows[e.RowIndex].Cells[i].Value == null || gvRateSetting.Rows[e.RowIndex].Cells[i].Value.ToString().Length == 0)
                             &&
                             (i != ColIndex.BarCode)
                           )
                        {
                            gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.Chk].Value = false;
                            //gvRateSetting.Rows[e.RowIndex].Cells[ColIndex.IsActive].Value = false;
                            break;
                        }
                    }
                }
            }
        }

        private void gvRateSetting_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (gvRateSetting.Columns.Count - 1 != e.ColumnIndex)
            {
                MovetoNextCell move2n = new MovetoNextCell(m2nC);
                int colIndex = -1;
                for (int i = e.ColumnIndex + 1; i < gvRateSetting.Columns.Count; i++)
                {
                    if (gvRateSetting.Columns[i].Visible == true)
                    {
                        colIndex = i;
                        break;
                    }
                }
                if (colIndex != -1)
                    BeginInvoke(move2n, new object[] { e.RowIndex, colIndex, gvRateSetting });
            }
        }

        private void gvRateSetting_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Down && e.KeyCode != Keys.Up && e.KeyCode != Keys.Left && e.KeyCode != Keys.Right)
                e.SuppressKeyPress = true;

            if (e.KeyCode == Keys.Escape)
            {
                gvRateSetting.ClearSelection();
                gvRateSetting.CurrentCell = null;
                PreDefaultUom = Convert.ToInt64(cmbDefaultUOM.SelectedValue);// mStockItems.UOMDefault;
                PreLowerUom = Convert.ToInt64(cmbLowerUOM.SelectedValue);// mStockItems.UOMPrimary;
                cmbDefaultUOM.Focus();
                FillUomCombos();

                if (cmbDefaultUOM.Items.Count > 2)
                {
                    cmbDefaultUOM.SelectedValue = PreDefaultUom;
                    cmbLowerUOM.SelectedValue = PreLowerUom;
                }
                else if (cmbDefaultUOM.Items.Count == 2)
                {
                    cmbDefaultUOM.SelectedIndex = 1;
                    cmbLowerUOM.SelectedIndex = 1;
                }
            }
            if (e.Modifiers == Keys.Control && e.KeyCode == Keys.D)
            {

                DataGridView gv = (DataGridView)sender;
                if (gv.CurrentCell.ColumnIndex == 5 && gv.Columns[5].Visible == true)
                {
                    if (gv.Columns[6].Visible == true)
                        gv.CurrentRow.Cells[6].Value = gv.CurrentCell.Value.ToString();
                    if (gv.Columns[7].Visible == true)
                        gv.CurrentRow.Cells[7].Value = gv.CurrentCell.Value.ToString();
                    if (gv.Columns[8].Visible == true)
                        gv.CurrentRow.Cells[8].Value = gv.CurrentCell.Value.ToString();
                    if (gv.Columns[9].Visible == true)
                        gv.CurrentRow.Cells[9].Value = gv.CurrentCell.Value.ToString();
                }
                else if (gv.CurrentCell.ColumnIndex == 6 && gv.Columns[6].Visible == true)
                {
                    if (gv.Columns[7].Visible == true)
                        gv.CurrentRow.Cells[7].Value = gv.CurrentCell.Value.ToString();
                    if (gv.Columns[8].Visible == true)
                        gv.CurrentRow.Cells[8].Value = gv.CurrentCell.Value.ToString();
                    if (gv.Columns[9].Visible == true)
                        gv.CurrentRow.Cells[9].Value = gv.CurrentCell.Value.ToString();
                }
                else if (gv.CurrentCell.ColumnIndex == 7 && gv.Columns[7].Visible == true)
                {
                    if (gv.Columns[8].Visible == true)
                        gv.CurrentRow.Cells[8].Value = gv.CurrentCell.Value.ToString();
                    if (gv.Columns[9].Visible == true)
                        gv.CurrentRow.Cells[9].Value = gv.CurrentCell.Value.ToString();
                }
                else if (gv.CurrentCell.ColumnIndex == 8 && gv.Columns[8].Visible == true)
                {

                    if (gv.Columns[9].Visible == true)
                        gv.CurrentRow.Cells[9].Value = gv.CurrentCell.Value.ToString();
                }
            }
        }

        private void FillUomCombos()
        {
            string str = "0";
            for (int i = 0; i < gvRateSetting.Rows.Count; i++)
            {
                if (gvRateSetting.Rows[i].Cells[ColIndex.UOM].Value != null && Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.UOM].Value) != 0)
                {
                    str = str + "," + Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.UOM].Value).ToString();
                }
            }

            ObjFunction.FillCombo(cmbLowerUOM, "Select UOMNo,UOMName from MUOM where UOMNO in(" + str + ") AND IsActive = 'True' Order By UOMName");
            //ObjFunction.FillCombo(cmbPurchaseUOM, "Select UOMNo,UOMName from MUOM where UOMNO in(" + str + ") AND IsActive = 'True' Order By UOMName");
            ObjFunction.FillCombo(cmbDefaultUOM, "Select UOMNo,UOMName from MUOM where UOMNO in(" + str + ") AND IsActive = 'True' Order By UOMName");
        }

        #endregion

        #region Navigation Methods
        private void NavigationDisplay(int type)
        {
            long No = 0;
            if (type == 5)
            {
                No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                ID = No;
            }
            else if (type == 1)
            {
                No = Convert.ToInt64(dtSearch.Rows[0].ItemArray[0].ToString());
                cntRow = 0;
                ID = No;
            }
            else if (type == 2)
            {
                No = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                cntRow = dtSearch.Rows.Count - 1;
                ID = No;
            }
            else
            {
                if (type == 3)
                {
                    cntRow = cntRow + 1;
                }
                else if (type == 4)
                {
                    cntRow = cntRow - 1;
                }

                if (cntRow < 0)
                {
                    OMMessageBox.Show("This is First Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                    cntRow = cntRow + 1;
                }
                else if (cntRow > dtSearch.Rows.Count - 1)
                {
                    OMMessageBox.Show("This is Last Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                    cntRow = cntRow - 1;
                }
                else
                {
                    No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                    ID = No;
                }

            }


            FillControls();

        }

        private void SetNavigation()
        {
            cntRow = 0;
            for (int i = 0; i < dtSearch.Rows.Count; i++)
            {
                if (Convert.ToInt64(dtSearch.Rows[i].ItemArray[0].ToString()) == ID)
                {
                    cntRow = i;
                    break;
                }
            }
        }

        private void setDisplay(bool flag)
        {
            btnFirst.Visible = flag;
            btnPrev.Visible = flag;
            btnNext.Visible = flag;
            btnLast.Visible = flag;
            btnDelete.Visible = flag;
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            NavigationDisplay(1);
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            NavigationDisplay(4);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            NavigationDisplay(3);
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            NavigationDisplay(2);
        }

        #endregion

        #region KeyDown Events
        private void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is GroupBox)
                    KeyDownFormat(ctrl.Controls);
            }
        }

        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left && e.Control)
            {
                if (btnPrev.Enabled) btnPrev_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Up && e.Control)
            {
                if (btnFirst.Enabled) btnFirst_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Right && e.Control)
            {
                if (btnNext.Enabled) btnNext_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Down && e.Control)
            {
                if (btnLast.Enabled) btnLast_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F2)
            {
                if (BtnSave.Visible) BtnSave_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F7)
            {
                if (btnNew.Visible)
                    txtBarcobe.Focus();
            }
            //else if (e.KeyCode == Keys.Escape)
            //{
            //    BtnExit_Click(sender, e);
            //}
        }
        #endregion

        private void cmbUOM_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {


            }
        }

        private void txtPurRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                //if (cmbLowerUOM.Items.Count == 2)
                //{
                //    gvRateSetting.Rows[0].Cells[11].Value = 1;
                //    double amt = Convert.ToDouble(txtPurRate.Text);// / (Convert.ToDouble(gvRateSetting.Rows[0].Cells[10].Value) * Convert.ToDouble(gvRateSetting.Rows[0].Cells[11].Value));
                //    gvRateSetting.Rows[0].Cells[4].Value = amt.ToString();
                //}
                //if (cmbLowerUOM.Items.Count > 2)
                //{
                for (int i = 0; i < gvRateSetting.Rows.Count; i++)
                {

                    if (i == defaultUOMRowNo)
                    {
                        //gvRateSetting.Rows[i].Cells[11].Value = txtStockCon.Text;
                        double amt = Convert.ToDouble(txtPurRate.Text);// / (Convert.ToDouble(gvRateSetting.Rows[i].Cells[10].Value) * Convert.ToDouble(gvRateSetting.Rows[i].Cells[11].Value));
                        gvRateSetting.Rows[i].Cells[ColIndex.PurRate].Value = amt.ToString();
                    }
                    else //if (gvRateSetting.Rows[i].Cells[3].FormattedValue.ToString() == lblDefaultOther.Text)
                    {
                        //gvRateSetting.Rows[i].Cells[11].Value = txtStockConOther.Text;
                        double amt = Convert.ToDouble(txtPurRate.Text) * ((Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.MKTQty].Value) * Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.StockConversion].Value)) / (Convert.ToDouble(gvRateSetting.Rows[defaultUOMRowNo].Cells[ColIndex.MKTQty].Value) * Convert.ToDouble(gvRateSetting.Rows[defaultUOMRowNo].Cells[ColIndex.StockConversion].Value)));
                        gvRateSetting.Rows[i].Cells[ColIndex.PurRate].Value = amt.ToString();
                    }
                    //else
                    //{
                    //    gvRateSetting.Rows[i].Cells[11].Value = 1;
                    //    double amt = Convert.ToDouble(txtPurRate.Text) / (Convert.ToDouble(gvRateSetting.Rows[i].Cells[10].Value) * Convert.ToDouble(gvRateSetting.Rows[i].Cells[11].Value));
                    //    gvRateSetting.Rows[i].Cells[4].Value = amt.ToString();
                    //}
                }
                //}
            }
        }

        private void txtStockConOther_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                int ii = 0;
                for (int j = 0; j < gvRateSetting.Rows.Count; j++)
                {
                    if (ii == 0 && j != LowerUOMRowNo)
                    {
                        gvRateSetting.Rows[j].Cells[ColIndex.StockConversion].Value = txtStockCon.Text;
                        ii = 1;
                    }
                    else if (ii == 1 && j != LowerUOMRowNo)
                    {
                        gvRateSetting.Rows[j].Cells[ColIndex.StockConversion].Value = txtStockConOther.Text;
                    }
                }
            }
        }

        private void txtStockCon_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                int ii = 0;
                for (int j = 0; j < gvRateSetting.Rows.Count; j++)
                {
                    if (ii == 0 && j != LowerUOMRowNo)
                    {
                        gvRateSetting.Rows[j].Cells[ColIndex.StockConversion].Value = txtStockCon.Text;
                        ii = 1;
                    }

                }
                if (txtStockConOther.Visible == false)
                {
                    txtMRP.Focus();
                }
                gvRateSetting.Columns[ColIndex.BarCode].Visible = false;
            }

        }

        private void txtBarcobe_Leave(object sender, EventArgs e)
        {
            EP.SetError(txtBarcobe, "");
            if (txtBarcobe.Text != "")
            {
                if (BarcodeNm != txtBarcobe.Text)
                {
                    if (ObjQry.ReturnInteger("Select Count(*) from MStockBarcode where Barcode = '" + txtBarcobe.Text + "'", CommonFunctions.ConStr) != 0)
                    {
                        EP.SetError(txtBarcobe, "Duplicate Barcode Name");
                        EP.SetIconAlignment(txtBarcobe, ErrorIconAlignment.MiddleRight);
                        txtBarcobe.Focus();
                    }
                }
                gvRateSetting.Columns[ColIndex.BarCode].Visible = false;
            }
            gvRateSetting.ClearSelection();
            gvRateSetting.CurrentCell = null;
        }

        private bool IsVarchar(string str)
        {
            // Variable to collect the Return value of the TryParse method.
            bool isNum = false;          
            for (int i = 0; i < str.Length; i++)
            {
                if ((Convert.ToChar(str[i]) >= 65 && Convert.ToChar(str[i]) <= 90) || (Convert.ToChar(str[i]) >= 97 && Convert.ToChar(str[i]) <= 122) || Convert.ToChar(str[i]) == 32)
                    isNum = true;
                else
                {
                    isNum = false;
                    break;
                }
            }
            return isNum;
        }

        private void txtMRP_Leave(object sender, EventArgs e)
        {
            EP.SetError(txtMRP, "");
            if (txtMRP.Text.Trim() == "")
            {
                EP.SetError(txtMRP, "Enter MRP");
                EP.SetIconAlignment(txtMRP, ErrorIconAlignment.MiddleRight);
                txtMRP.Focus();
            }

        }

        private void txtMRP_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (txtMRP.Text.Trim() != "")
                {
                    for (int i = 0; i < gvRateSetting.Rows.Count; i++)
                    {
                        gvRateSetting.Rows[i].Cells[ColIndex.MRP].Value = txtMRP.Text;
                    }
                }
            }
        }

        private void txtItemName_Leave(object sender, EventArgs e)
        {
           EP.SetError(txtItemName, "");
            if (txtItemName.Text != "")
            {
                if (ItemNm != txtItemName.Text)
                {
                    if (ObjQry.ReturnInteger("Select Count(*) from MStockItems where ItemName = '" + txtItemName.Text + "' " +
                    " AND GroupNo = " + ObjFunction.GetComboValue(cmbGroupNo1) +
                    " AND GroupNo1 = " + ObjFunction.GetComboValue(cmbGroupNo2) +
                    " AND CompanyNo = " + ObjFunction.GetComboValue(cmbCompanyName), CommonFunctions.ConStr) != 0)
                    {
                        EP.SetError(txtItemName, "Duplicate Item Name");
                        EP.SetIconAlignment(txtItemName, ErrorIconAlignment.MiddleRight);
                        txtItemName.Focus();
                    }
                    else
                        cmbCompanyName.Focus();
                }
            }
        }

        private void txtItemName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtItemName_Leave(sender, new EventArgs());
            }
        }

        private void chkActive_CheckedChanged(object sender, EventArgs e)
        {
            if (chkActive.Checked == true)
                chkActive.Text = "Yes";
            else
                chkActive.Text = "No";
        }

        private void FormatStockConversionControls()
        {
            if (ObjFunction.GetComboValue(cmbLowerUOM) != 0)
            {
                lblLower.Text = cmbLowerUOM.Text;
                lblLowerOther.Text = cmbLowerUOM.Text;
                defaultUOMRowNo = -1;
                LowerUOMRowNo = -1;

                for (int i = 0; i < gvRateSetting.Rows.Count; i++)
                {
                    if (Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.UOM].Value) == ObjFunction.GetComboValue(cmbDefaultUOM))
                    {
                        defaultUOMRowNo = i;
                        lblPur.Text = "/ " + gvRateSetting.Rows[i].Cells[ColIndex.MKTQty].Value + " " + cmbDefaultUOM.Text;
                        if (gvRateSetting.Rows[i].Cells[ColIndex.PurRate].Value != null)
                        {
                            txtPurRate.Text = gvRateSetting.Rows[i].Cells[ColIndex.PurRate].Value.ToString();
                        }
                        else
                        {
                            txtPurRate.Text = "0.00";
                        }
                    }

                    if (Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.UOM].Value) == ObjFunction.GetComboValue(cmbLowerUOM))
                    {
                        gvRateSetting.Rows[i].Cells[ColIndex.StockConversion].Value = 1;
                        LowerUOMRowNo = i;
                        //if (gvRateSetting.Rows[i].Cells[11].Value != null)
                        //    txtStockCon.Text = gvRateSetting.Rows[i].Cells[11].Value.ToString();
                    }
                    //else
                    //{
                    //    if (gvRateSetting.Rows[i].Cells[4].Value != null)
                    //        txtPurRate.Text = gvRateSetting.Rows[i].Cells[4].Value.ToString();
                    //}
                }

                DisplayLabel();
                //if (Convert.ToBoolean(gvRateSetting.Rows[2].Cells[ColIndex.Chk].FormattedValue) == true)
                if (Convert.ToBoolean(gvRateSetting.Rows[2].Cells[ColIndex.Chk].FormattedValue) == true ||
                    Convert.ToInt64(gvRateSetting.Rows[2].Cells[ColIndex.PkSrNo].FormattedValue) != 0)
                {
                    lbl1.Visible = true;
                    lblDefaultOther.Visible = true;
                    lblLowerOther.Visible = true;
                    txtStockConOther.Visible = true;
                }

                //if (Convert.ToBoolean(gvRateSetting.Rows[1].Cells[ColIndex.Chk].FormattedValue) == true)
                if (Convert.ToBoolean(gvRateSetting.Rows[1].Cells[ColIndex.Chk].FormattedValue) == true ||
                    Convert.ToInt64(gvRateSetting.Rows[1].Cells[ColIndex.PkSrNo].FormattedValue) != 0)
                {
                    label3.Visible = true;
                    lblDefault.Visible = true;
                    lblLower.Visible = true;
                    txtStockCon.Visible = true;
                }

                int ii = 0;
                for (int j = 0; j < gvRateSetting.Rows.Count; j++)
                {
                    if (ii == 0 && j != LowerUOMRowNo)
                    {
                        lblDefault.Text = gvRateSetting.Rows[j].Cells[ColIndex.UOM].FormattedValue.ToString();
                        if (gvRateSetting.Rows[j].Cells[ColIndex.StockConversion].Value != null)
                            txtStockCon.Text = gvRateSetting.Rows[j].Cells[ColIndex.StockConversion].Value.ToString();
                        ii = 1;
                    }
                    else if (ii == 1 && j != LowerUOMRowNo)
                    {
                        lblDefaultOther.Text = gvRateSetting.Rows[j].Cells[ColIndex.UOM].FormattedValue.ToString();
                        if (gvRateSetting.Rows[j].Cells[ColIndex.StockConversion].Value != null)
                            txtStockConOther.Text = gvRateSetting.Rows[j].Cells[ColIndex.StockConversion].Value.ToString();
                    }
                    //else
                    //{
                    //    if (gvRateSetting.Rows[j].Cells[4].Value != null)
                    //        txtPurRate.Text = gvRateSetting.Rows[j].Cells[4].Value.ToString();
                    //}
                }
            }
        }

        private void cmbLowerUOM_Leave(object sender, EventArgs e)
        {
            FormatStockConversionControls();

            MovetoNext move2n = new MovetoNext(SetFoucs);
            BeginInvoke(move2n, new object[] { 7 });
        }

        private void FillLanguageGroupName()
        {
            txtLanguage.Text = ObjQry.ReturnString(" select LanguageName from MStockGroup where StockGroupNo=" + ObjFunction.GetComboValue(cmbGroupNo1) + "", CommonFunctions.ConStr);
        }

        private void formatGvRateSetting()
        {
            EP.SetError(cmbCompanyName, "");
            if (ObjFunction.GetComboValue(cmbCompanyName) != 0)
            {
                DataGridViewComboBoxColumn cmbGridUom = gvRateSetting.Columns[ColIndex.UOM] as DataGridViewComboBoxColumn;

                if (cmbGridUom != null)
                {
                    ObjFunction.FillCombo(cmbGridUom, "Select UOMNo,UOMName from MUOM WHERE IsActive = 'True' ORDER BY UOMName");
                }

                if (txtBarcobe.Text != "")
                {
                    //gvRateSetting.Columns[ColIndex.BarCode].Visible = true;
                    if (gvRateSetting.Rows.Count == 0)
                        gvRateSetting.Rows.Add();
                    //gvRateSetting.Focus();
                    // gvRateSetting.CurrentCell = gvRateSetting[ColIndex.UOM, 0];
                    gvRateSetting.Rows[0].Cells[ColIndex.MKTQty].Value = 1;
                    gvRateSetting.Rows[0].Cells[ColIndex.BarCode].Value = txtBarcobe.Text;
                }
                else
                {
                    gvRateSetting.Columns[ColIndex.BarCode].Visible = false;
                    if (gvRateSetting.Rows.Count == 0)
                        gvRateSetting.Rows.Add();
                    // gvRateSetting.Focus();
                    //  gvRateSetting.CurrentCell = gvRateSetting[ColIndex.UOM, 0];
                    gvRateSetting.Rows[0].Cells[ColIndex.MKTQty].Value = 1;
                }
            }
            else
            {
                EP.SetError(cmbCompanyName, "Please Select Company");
                EP.SetIconAlignment(txtBarcobe, ErrorIconAlignment.MiddleRight);
                cmbCompanyName.Focus();
            }
            gvRateSetting.CurrentCell = null;
        }

        private void cmbGroupNo1_Leave(object sender, EventArgs e)
        {
            FillLanguageGroupName();
        }

        private void cmbLowerUOM_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                FormatStockConversionControls();
                MovetoNext move2n = new MovetoNext(SetFoucs);
                BeginInvoke(move2n, new object[] { 7 });
            }
        }

        private delegate void MovetoNext(int type);

        private void SetFoucs(int i)
        {
            switch (i)
            {
                case 0:
                    if (cmbDepartmentName.Visible == true)
                    {
                        cmbDepartmentName.Focus();
                    }
                    else if (cmbCategoryName.Visible == true)
                    {
                        cmbCategoryName.Focus();
                    }
                    else if (cmbDepartmentName.Visible == true)
                    {
                        cmbDepartmentName.Focus();

                    }
                    else if (txtBarcobe.Visible == true)
                    {
                        txtBarcobe.Focus();
                    }
                    else
                    {
                        txtReOrderLevelQty.Focus();
                    }
                    break;
                case 1:

                    if (cmbCategoryName.Visible == true)
                    {
                        cmbCategoryName.Focus();
                    }
                    else if (txtBarcobe.Visible == true)
                    {
                        txtBarcobe.Focus();
                    }
                    else if (cmbStockLocationName.Visible == true)
                    {
                        cmbStockLocationName.Focus();
                    }
                    else
                    {
                        gvRateSetting.Focus();
                        gvRateSetting.CurrentCell = gvRateSetting[ColIndex.UOM, 0];
                    }
                    break;
                case 2:
                    if (txtBarcobe.Visible == true)
                    {
                        txtBarcobe.Focus();
                    }
                    else if (cmbStockLocationName.Visible == true)
                    {
                        cmbStockLocationName.Focus();
                    }
                    else
                    {
                        txtReOrderLevelQty.Focus();
                    }
                    break;
                case 3:
                    if (cmbStockLocationName.Visible == true)
                    {
                        cmbStockLocationName.Focus();
                    }
                    else
                    {
                        txtReOrderLevelQty.Focus();
                    }
                    break;

                case 4:
                    txtReOrderLevelQty.Focus();
                    break;
                case 5:
                    gvRateSetting.Focus();
                    gvRateSetting.CurrentCell = gvRateSetting[ColIndex.UOM, 0];
                    break;
                case 6:
                    txtMaxLevelQty.Focus();
                    break;
                case 7:
                    if (txtStockCon.Visible == true)
                        txtStockCon.Focus();
                    else if (txtStockConOther.Visible == true)
                        txtStockConOther.Focus();
                    else
                        txtMRP.Focus();
                    break;


            }
        }

        #region ColumnIndex
        private static class ColIndex
        {
            public static int Sr = 0;
            public static int Date = 1;
            public static int BarCode = 2;
            public static int MRP = 3;
            public static int UOM = 4;
            public static int ASaleRate = 5;
            public static int BSaleRate = 6;
            public static int CSaleRate = 7;
            public static int DSaleRate = 8;
            public static int ESaleRate = 9;
            public static int MKTQty = 10;
            public static int PurRate = 11;
            public static int RateVariation = 12;
            public static int Chk = 13;
            public static int StockConversion = 14;
            public static int PkSrNo = 15;
            public static int BarCodeNo = 16;
            public static int IsActive = 17;
            public static int HidChk = 18;
        }
        #endregion

        private void cmbDepartmentName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                MovetoNext move2n = new MovetoNext(SetFoucs);
                BeginInvoke(move2n, new object[] { 9 });
            }

        }

        private void cmbCategoryName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                MovetoNext move2n = new MovetoNext(SetFoucs);
                BeginInvoke(move2n, new object[] { 8 });
            }
        }

        private void txtBarcobe_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (ObjFunction.GetComboValue(cmbStockDept) > 0)
                {
                    cmbStockDept.Focus();
                }
                else
                    cmbStockDept.Focus();
            }
        }

        private void cmbStockLocationName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                EP.SetError(cmbStockLocationName, "");
                if (ObjFunction.GetComboValue(cmbStockLocationName) <= 0)
                {
                    EP.SetError(cmbStockLocationName, "Select Stock Location Name");
                    cmbStockLocationName.Focus();

                }
                else
                {
                    cmbGroupNo1.Focus();
                    //MovetoNext move2n = new MovetoNext(SetFoucs);
                    //BeginInvoke(move2n, new object[] { 3 });
                }
            }
        }

        private void txtShortName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                cmbCategoryName.Focus();
                //MovetoNext move2n = new MovetoNext(SetFoucs);
                //BeginInvoke(move2n, new object[] { 0 });
            }

        }

        private void txtReOrderLevelQty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                EP.SetError(txtReOrderLevelQty, "");
                if (txtReOrderLevelQty.Text == "")
                {
                    txtReOrderLevelQty.Text = "0.00";
                    MovetoNext move2n = new MovetoNext(SetFoucs);
                    BeginInvoke(move2n, new object[] { 6 });
                    //MovetoNext move2n = new MovetoNext(SetFoucs);
                    //BeginInvoke(move2n, new object[] { 5 });
                }
                else if (ObjFunction.CheckValidAmount(txtReOrderLevelQty.Text) == false)
                {
                    EP.SetError(txtReOrderLevelQty, "Enter Valid Qty");
                    EP.SetIconAlignment(txtReOrderLevelQty, ErrorIconAlignment.MiddleRight);
                    MovetoNext move2n = new MovetoNext(SetFoucs);
                    BeginInvoke(move2n, new object[] { 4 });
                }
                else
                {

                    MovetoNext move2n = new MovetoNext(SetFoucs);
                    BeginInvoke(move2n, new object[] { 6 });
                    //MovetoNext move2n = new MovetoNext(SetFoucs);
                    //BeginInvoke(move2n, new object[] { 5 });
                }
                formatGvRateSetting();
            }
        }

        private void txtMaxLevelQty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                EP.SetError(txtMaxLevelQty, "");
                if (txtMaxLevelQty.Text == "")
                {
                    txtMaxLevelQty.Text = "0.00";
                    MovetoNext move2n = new MovetoNext(SetFoucs);
                    BeginInvoke(move2n, new object[] { 5 });
                }
                else if (ObjFunction.CheckValidAmount(txtMaxLevelQty.Text) == false)
                {
                    EP.SetError(txtMaxLevelQty, "Enter Valid Qty");
                    EP.SetIconAlignment(txtMaxLevelQty, ErrorIconAlignment.MiddleRight);
                    txtMaxLevelQty.Focus();
                }
                else
                {
                    MovetoNext move2n = new MovetoNext(SetFoucs);
                    BeginInvoke(move2n, new object[] { 5 });
                }
            }
        }

        private void chkFix_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                cmbVatSales.Focus();
            }
        }

        private void SearchBarcode()
        {
            string str = "SELECT MStockItems.ItemNo FROM dbo.MStockItems_V(NULL,NULL,NULL,NULL,NULL,NULL,NULL) AS MStockItems INNER JOIN MStockBarcode ON MStockItems.ItemNo = MStockBarcode.ItemNo " +
                " WHERE (MStockItems.ItemNo <> 1) AND (MStockBarcode.Barcode LIKE '" + txtSearchBarCode.Text + "')";
            long INo = ObjQry.ReturnLong(str, CommonFunctions.ConStr);
            if (INo != 0)
            {
                ItemNm = "";
                BarcodeNm = "";

                dtSearch = ObjFunction.GetDataView("Select ItemNo From MStockItems_V(NULL,NULL,NULL,NULL,NULL,NULL,NULL) WHERE ItemNo<>1 ORDER BY ItemName").Table;
                StockItem.RequestItemNo = INo;
                if (dtSearch.Rows.Count > 0)
                {
                    if (StockItem.RequestItemNo == 0)
                        ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    else
                        ID = StockItem.RequestItemNo;
                    FillControls();
                    SetNavigation();
                }

                setDisplay(true);
                btnNew.Focus();
                txtSearchBarCode.Text = "";
                txtSearchBarCode.Enabled = true;
            }
            else
            {
                OMMessageBox.Show("BarCode does'nt exist", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                txtSearchBarCode.Focus();
            }
        }

        private void txtSearchBarCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (btnNew.Visible)
                    SearchBarcode();
            }
        }

        private void btnNew_VisibleChanged(object sender, EventArgs e)
        {
            txtSearchBarCode.Enabled = btnNew.Visible;
        }

        private void StockItemAE_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F7)
            {
                txtSearchBarCode.Focus();
            }
        }

        private void cmbVatPurchase_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (e.KeyCode == Keys.Enter)
                {
                    EP.SetError(cmbVatPurchase, "");
                    e.SuppressKeyPress = true;
                    if (ObjFunction.GetComboValue(cmbVatPurchase) == 0)
                    {
                        EP.SetError(cmbVatPurchase, "Select Purchase Tax");
                        cmbVatPurchase.Focus();
                    }
                    else
                    {
                        BtnSave.Focus();
                    }
                }

            }
        }

        private void cmbVatSales_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                EP.SetError(cmbVatSales, "");
                e.SuppressKeyPress = true;
                if (ObjFunction.GetComboValue(cmbVatSales) == 0)
                {
                    EP.SetError(cmbVatSales, "Select Sales Tax");
                    cmbVatSales.Focus();
                }
                else
                {
                    cmbVatPurchase.Focus();
                }
            }

        }

        private void cmbCompanyName_Leave(object sender, EventArgs e)
        {
            formatGvRateSetting();
        }

        private void cmbStockDept_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                long iselected = ObjFunction.GetComboValue(cmbGroupNo2);
                ObjFunction.FillCombo(cmbGroupNo2, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=2 AND ControlSubGroup="+ ObjFunction.GetComboValue(cmbStockDept) +" ORDER BY StockGroupName");
                cmbGroupNo2.SelectedValue = iselected;
                cmbGroupNo2.Focus();
            }
        }

        private void cmbGroupNo2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                long  iselected = ObjFunction.GetComboValue(cmbGroupNo1);
                if(Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_IsBrandFilter))==true)
                    ObjFunction.FillCombo(cmbGroupNo1, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=3 AND ControlSubGroup=" + ObjFunction.GetComboValue(cmbGroupNo2) + " ORDER BY StockGroupName");
                else
                    ObjFunction.FillCombo(cmbGroupNo1, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=3 ORDER BY StockGroupName");
                cmbGroupNo1.SelectedValue = iselected;
                cmbGroupNo1.Focus();
            }
        }

        private void cmbGroupNo1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (ObjFunction.GetComboValue(cmbGroupNo1) <= 0)
                    cmbGroupNo1.Focus();
                else
                    txtItemName.Focus();
            }
        }

        private void chkActive_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                cmbVatSales.Focus();
            }
        }

        private void btnNewBrand_Click(object sender, EventArgs e)
        {
            if (ObjFunction.CheckAllowMenu(146) == false) return;
            Form NewF = new Master.BrandAE(ObjFunction.GetComboValue(cmbStockDept),ObjFunction.GetComboValue(cmbGroupNo2));
            ObjFunction.OpenForm(NewF);

            if (((Master.BrandAE)NewF).ShortID != 0)
            {
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_IsBrandFilter)) == true)
                    ObjFunction.FillCombo(cmbGroupNo1, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=3 AND ControlSubGroup=" + ObjFunction.GetComboValue(cmbGroupNo2) + " ORDER BY StockGroupName");
                else
                    ObjFunction.FillCombo(cmbGroupNo1, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=3 ORDER BY StockGroupName");
                if (((Master.BrandAE)NewF).ShortID > 0)
                    cmbGroupNo1.SelectedValue = ((Master.BrandAE)NewF).ShortID;
                else
                    cmbGroupNo1.SelectedValue = 0;
                cmbGroupNo1.Focus();
            }
        }
      
    }
}
