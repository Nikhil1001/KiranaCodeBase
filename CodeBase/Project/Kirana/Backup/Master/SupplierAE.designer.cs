﻿namespace Kirana.Master
{
    partial class SupplierAE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SupplierAE));
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblChkHelp = new System.Windows.Forms.Label();
            this.txtGSTIN = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtVatNo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPanNo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chkActive = new System.Windows.Forms.CheckBox();
            this.label19 = new System.Windows.Forms.Label();
            this.cmbOCity = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCreditLimit = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtOPinCode = new System.Windows.Forms.TextBox();
            this.txtOPhone2 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.cmbOState = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtOAddress = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtMobileNo1 = new System.Windows.Forms.TextBox();
            this.txtOPhone1 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtMobileNo2 = new System.Windows.Forms.TextBox();
            this.txtEmailID = new System.Windows.Forms.TextBox();
            this.lblDeActiveCount = new System.Windows.Forms.Label();
            this.lblActiveCount = new System.Windows.Forms.Label();
            this.lblTotalCount = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.pnlDistAgent = new System.Windows.Forms.Panel();
            this.btnDistCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.dgvDistAgent = new System.Windows.Forms.DataGridView();
            this.PkSrNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DistributorAgentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MobileNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Remark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label32 = new System.Windows.Forms.Label();
            this.txtLedgerName = new System.Windows.Forms.TextBox();
            this.l = new System.Windows.Forms.Label();
            this.txtContactPer = new System.Windows.Forms.TextBox();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrev = new System.Windows.Forms.Button();
            this.btnLast = new System.Windows.Forms.Button();
            this.btnFirst = new System.Windows.Forms.Button();
            this.EP = new System.Windows.Forms.ErrorProvider(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.txtOpBalance = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.cmbSign = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.BtnExit = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.pnlMain = new JitControls.OMBPanel();
            this.pnlCustInfo = new JitControls.OMBPanel();
            this.btnMfg = new System.Windows.Forms.Button();
            this.lblStar4 = new System.Windows.Forms.Label();
            this.lblStar2 = new System.Windows.Forms.Label();
            this.lblStar1 = new System.Windows.Forms.Label();
            this.btnDistAgent = new System.Windows.Forms.Button();
            this.panel3.SuspendLayout();
            this.pnlDistAgent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDistAgent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EP)).BeginInit();
            this.pnlMain.SuspendLayout();
            this.pnlCustInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.lblChkHelp);
            this.panel3.Controls.Add(this.txtGSTIN);
            this.panel3.Controls.Add(this.pnlDistAgent);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.txtVatNo);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.txtPanNo);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.chkActive);
            this.panel3.Controls.Add(this.label19);
            this.panel3.Controls.Add(this.cmbOCity);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.txtCreditLimit);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.txtOPinCode);
            this.panel3.Controls.Add(this.txtOPhone2);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.cmbOState);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.txtOAddress);
            this.panel3.Controls.Add(this.label21);
            this.panel3.Controls.Add(this.txtMobileNo1);
            this.panel3.Controls.Add(this.txtOPhone1);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Controls.Add(this.label23);
            this.panel3.Controls.Add(this.label24);
            this.panel3.Controls.Add(this.label25);
            this.panel3.Controls.Add(this.txtMobileNo2);
            this.panel3.Controls.Add(this.txtEmailID);
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // lblChkHelp
            // 
            resources.ApplyResources(this.lblChkHelp, "lblChkHelp");
            this.lblChkHelp.Name = "lblChkHelp";
            // 
            // txtGSTIN
            // 
            resources.ApplyResources(this.txtGSTIN, "txtGSTIN");
            this.txtGSTIN.Name = "txtGSTIN";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // txtVatNo
            // 
            resources.ApplyResources(this.txtVatNo, "txtVatNo");
            this.txtVatNo.Name = "txtVatNo";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // txtPanNo
            // 
            resources.ApplyResources(this.txtPanNo, "txtPanNo");
            this.txtPanNo.Name = "txtPanNo";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // chkActive
            // 
            resources.ApplyResources(this.chkActive, "chkActive");
            this.chkActive.Name = "chkActive";
            this.chkActive.UseVisualStyleBackColor = true;
            this.chkActive.CheckedChanged += new System.EventHandler(this.chkActive_CheckedChanged);
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.Name = "label19";
            // 
            // cmbOCity
            // 
            this.cmbOCity.FormattingEnabled = true;
            resources.ApplyResources(this.cmbOCity, "cmbOCity");
            this.cmbOCity.Name = "cmbOCity";
            this.cmbOCity.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbOCity_KeyDown);
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // txtCreditLimit
            // 
            resources.ApplyResources(this.txtCreditLimit, "txtCreditLimit");
            this.txtCreditLimit.Name = "txtCreditLimit";
            this.txtCreditLimit.TextChanged += new System.EventHandler(this.txtCreditLimit_TextChanged);
            this.txtCreditLimit.Leave += new System.EventHandler(this.txtCreditLimit_Leave);
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // txtOPinCode
            // 
            resources.ApplyResources(this.txtOPinCode, "txtOPinCode");
            this.txtOPinCode.Name = "txtOPinCode";
            this.txtOPinCode.TextChanged += new System.EventHandler(this.txtOPinCode_TextChanged);
            // 
            // txtOPhone2
            // 
            resources.ApplyResources(this.txtOPhone2, "txtOPhone2");
            this.txtOPhone2.Name = "txtOPhone2";
            this.txtOPhone2.TextChanged += new System.EventHandler(this.txtValid_TextChanged);
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.Name = "label15";
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.Name = "label17";
            // 
            // cmbOState
            // 
            this.cmbOState.FormattingEnabled = true;
            resources.ApplyResources(this.cmbOState, "cmbOState");
            this.cmbOState.Name = "cmbOState";
            this.cmbOState.Leave += new System.EventHandler(this.cmbOState_Leave);
            this.cmbOState.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbOState_KeyDown);
            // 
            // label18
            // 
            resources.ApplyResources(this.label18, "label18");
            this.label18.Name = "label18";
            // 
            // txtOAddress
            // 
            resources.ApplyResources(this.txtOAddress, "txtOAddress");
            this.txtOAddress.Name = "txtOAddress";
            // 
            // label21
            // 
            resources.ApplyResources(this.label21, "label21");
            this.label21.Name = "label21";
            // 
            // txtMobileNo1
            // 
            resources.ApplyResources(this.txtMobileNo1, "txtMobileNo1");
            this.txtMobileNo1.Name = "txtMobileNo1";
            this.txtMobileNo1.TextChanged += new System.EventHandler(this.txtValid_TextChanged);
            // 
            // txtOPhone1
            // 
            resources.ApplyResources(this.txtOPhone1, "txtOPhone1");
            this.txtOPhone1.Name = "txtOPhone1";
            this.txtOPhone1.TextChanged += new System.EventHandler(this.txtValid_TextChanged);
            // 
            // label22
            // 
            resources.ApplyResources(this.label22, "label22");
            this.label22.Name = "label22";
            // 
            // label23
            // 
            resources.ApplyResources(this.label23, "label23");
            this.label23.Name = "label23";
            // 
            // label24
            // 
            resources.ApplyResources(this.label24, "label24");
            this.label24.Name = "label24";
            // 
            // label25
            // 
            resources.ApplyResources(this.label25, "label25");
            this.label25.Name = "label25";
            // 
            // txtMobileNo2
            // 
            resources.ApplyResources(this.txtMobileNo2, "txtMobileNo2");
            this.txtMobileNo2.Name = "txtMobileNo2";
            this.txtMobileNo2.TextChanged += new System.EventHandler(this.txtValid_TextChanged);
            // 
            // txtEmailID
            // 
            resources.ApplyResources(this.txtEmailID, "txtEmailID");
            this.txtEmailID.Name = "txtEmailID";
            // 
            // lblDeActiveCount
            // 
            this.lblDeActiveCount.ForeColor = System.Drawing.Color.Red;
            resources.ApplyResources(this.lblDeActiveCount, "lblDeActiveCount");
            this.lblDeActiveCount.Name = "lblDeActiveCount";
            // 
            // lblActiveCount
            // 
            this.lblActiveCount.ForeColor = System.Drawing.Color.Green;
            resources.ApplyResources(this.lblActiveCount, "lblActiveCount");
            this.lblActiveCount.Name = "lblActiveCount";
            // 
            // lblTotalCount
            // 
            this.lblTotalCount.ForeColor = System.Drawing.Color.Blue;
            resources.ApplyResources(this.lblTotalCount, "lblTotalCount");
            this.lblTotalCount.Name = "lblTotalCount";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // pnlDistAgent
            // 
            this.pnlDistAgent.BackColor = System.Drawing.Color.Coral;
            this.pnlDistAgent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDistAgent.Controls.Add(this.btnDistCancel);
            this.pnlDistAgent.Controls.Add(this.btnOk);
            this.pnlDistAgent.Controls.Add(this.dgvDistAgent);
            resources.ApplyResources(this.pnlDistAgent, "pnlDistAgent");
            this.pnlDistAgent.Name = "pnlDistAgent";
            // 
            // btnDistCancel
            // 
            resources.ApplyResources(this.btnDistCancel, "btnDistCancel");
            this.btnDistCancel.BackColor = System.Drawing.SystemColors.Control;
            this.btnDistCancel.Name = "btnDistCancel";
            this.btnDistCancel.UseVisualStyleBackColor = false;
            this.btnDistCancel.Click += new System.EventHandler(this.btnDistCancel_Click);
            // 
            // btnOk
            // 
            resources.ApplyResources(this.btnOk, "btnOk");
            this.btnOk.BackColor = System.Drawing.SystemColors.Control;
            this.btnOk.Name = "btnOk";
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // dgvDistAgent
            // 
            this.dgvDistAgent.AllowUserToAddRows = false;
            this.dgvDistAgent.AllowUserToDeleteRows = false;
            this.dgvDistAgent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDistAgent.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PkSrNo,
            this.DistributorAgentName,
            this.MobileNo,
            this.Remark});
            resources.ApplyResources(this.dgvDistAgent, "dgvDistAgent");
            this.dgvDistAgent.Name = "dgvDistAgent";
            this.dgvDistAgent.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDistAgent_CellEndEdit);
            this.dgvDistAgent.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvDistAgent_EditingControlShowing);
            this.dgvDistAgent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvDistAgent_KeyDown);
            // 
            // PkSrNo
            // 
            this.PkSrNo.DataPropertyName = "PkSrNo";
            resources.ApplyResources(this.PkSrNo, "PkSrNo");
            this.PkSrNo.Name = "PkSrNo";
            // 
            // DistributorAgentName
            // 
            this.DistributorAgentName.DataPropertyName = "DistAgentName";
            resources.ApplyResources(this.DistributorAgentName, "DistributorAgentName");
            this.DistributorAgentName.Name = "DistributorAgentName";
            // 
            // MobileNo
            // 
            this.MobileNo.DataPropertyName = "MobileNo";
            resources.ApplyResources(this.MobileNo, "MobileNo");
            this.MobileNo.Name = "MobileNo";
            // 
            // Remark
            // 
            this.Remark.DataPropertyName = "Remark";
            resources.ApplyResources(this.Remark, "Remark");
            this.Remark.Name = "Remark";
            // 
            // label32
            // 
            resources.ApplyResources(this.label32, "label32");
            this.label32.Name = "label32";
            // 
            // txtLedgerName
            // 
            resources.ApplyResources(this.txtLedgerName, "txtLedgerName");
            this.txtLedgerName.Name = "txtLedgerName";
            this.txtLedgerName.Leave += new System.EventHandler(this.txtLedgerName_Leave);
            // 
            // l
            // 
            resources.ApplyResources(this.l, "l");
            this.l.Name = "l";
            // 
            // txtContactPer
            // 
            resources.ApplyResources(this.txtContactPer, "txtContactPer");
            this.txtContactPer.Name = "txtContactPer";
            // 
            // btnNext
            // 
            resources.ApplyResources(this.btnNext, "btnNext");
            this.btnNext.Name = "btnNext";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrev
            // 
            resources.ApplyResources(this.btnPrev, "btnPrev");
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.UseVisualStyleBackColor = true;
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // btnLast
            // 
            resources.ApplyResources(this.btnLast, "btnLast");
            this.btnLast.Name = "btnLast";
            this.btnLast.UseVisualStyleBackColor = true;
            this.btnLast.Click += new System.EventHandler(this.btnLast_Click);
            // 
            // btnFirst
            // 
            resources.ApplyResources(this.btnFirst, "btnFirst");
            this.btnFirst.Name = "btnFirst";
            this.btnFirst.UseVisualStyleBackColor = true;
            this.btnFirst.Click += new System.EventHandler(this.btnFirst_Click);
            // 
            // EP
            // 
            this.EP.ContainerControl = this;
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // txtOpBalance
            // 
            resources.ApplyResources(this.txtOpBalance, "txtOpBalance");
            this.txtOpBalance.Name = "txtOpBalance";
            this.txtOpBalance.TextChanged += new System.EventHandler(this.txtOpBalance_TextChanged);
            this.txtOpBalance.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtOpBalance_KeyDown);
            this.txtOpBalance.Leave += new System.EventHandler(this.txtOpBalance_Leave);
            // 
            // label33
            // 
            resources.ApplyResources(this.label33, "label33");
            this.label33.Name = "label33";
            // 
            // cmbSign
            // 
            this.cmbSign.FormattingEnabled = true;
            resources.ApplyResources(this.cmbSign, "cmbSign");
            this.cmbSign.Name = "cmbSign";
            // 
            // btnCancel
            // 
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnNew
            // 
            resources.ApplyResources(this.btnNew, "btnNew");
            this.btnNew.Name = "btnNew";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnUpdate
            // 
            resources.ApplyResources(this.btnUpdate, "btnUpdate");
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnSearch
            // 
            resources.ApplyResources(this.btnSearch, "btnSearch");
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnDelete
            // 
            resources.ApplyResources(this.btnDelete, "btnDelete");
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btndelete_Click);
            // 
            // BtnExit
            // 
            resources.ApplyResources(this.BtnExit, "BtnExit");
            this.BtnExit.Name = "BtnExit";
            this.BtnExit.UseVisualStyleBackColor = true;
            this.BtnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // BtnSave
            // 
            resources.ApplyResources(this.BtnSave, "BtnSave");
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // pnlMain
            // 
            this.pnlMain.BorderColor = System.Drawing.Color.Gray;
            this.pnlMain.BorderRadius = 3;
            this.pnlMain.Controls.Add(this.pnlCustInfo);
            this.pnlMain.Controls.Add(this.btnMfg);
            this.pnlMain.Controls.Add(this.lblStar4);
            this.pnlMain.Controls.Add(this.lblStar2);
            this.pnlMain.Controls.Add(this.lblStar1);
            this.pnlMain.Controls.Add(this.btnDistAgent);
            this.pnlMain.Controls.Add(this.label32);
            this.pnlMain.Controls.Add(this.btnCancel);
            this.pnlMain.Controls.Add(this.btnUpdate);
            this.pnlMain.Controls.Add(this.BtnSave);
            this.pnlMain.Controls.Add(this.btnSearch);
            this.pnlMain.Controls.Add(this.btnNew);
            this.pnlMain.Controls.Add(this.btnDelete);
            this.pnlMain.Controls.Add(this.BtnExit);
            this.pnlMain.Controls.Add(this.panel3);
            this.pnlMain.Controls.Add(this.cmbSign);
            this.pnlMain.Controls.Add(this.label33);
            this.pnlMain.Controls.Add(this.txtOpBalance);
            this.pnlMain.Controls.Add(this.btnNext);
            this.pnlMain.Controls.Add(this.btnPrev);
            this.pnlMain.Controls.Add(this.label1);
            this.pnlMain.Controls.Add(this.btnLast);
            this.pnlMain.Controls.Add(this.btnFirst);
            this.pnlMain.Controls.Add(this.l);
            this.pnlMain.Controls.Add(this.txtContactPer);
            this.pnlMain.Controls.Add(this.txtLedgerName);
            resources.ApplyResources(this.pnlMain, "pnlMain");
            this.pnlMain.Name = "pnlMain";
            // 
            // pnlCustInfo
            // 
            this.pnlCustInfo.BorderColor = System.Drawing.Color.Gray;
            this.pnlCustInfo.BorderRadius = 3;
            this.pnlCustInfo.Controls.Add(this.label9);
            this.pnlCustInfo.Controls.Add(this.lblDeActiveCount);
            this.pnlCustInfo.Controls.Add(this.label10);
            this.pnlCustInfo.Controls.Add(this.lblTotalCount);
            this.pnlCustInfo.Controls.Add(this.label11);
            this.pnlCustInfo.Controls.Add(this.lblActiveCount);
            resources.ApplyResources(this.pnlCustInfo, "pnlCustInfo");
            this.pnlCustInfo.Name = "pnlCustInfo";
            // 
            // btnMfg
            // 
            resources.ApplyResources(this.btnMfg, "btnMfg");
            this.btnMfg.Name = "btnMfg";
            this.btnMfg.UseVisualStyleBackColor = true;
            this.btnMfg.Click += new System.EventHandler(this.btnMfg_Click);
            // 
            // lblStar4
            // 
            resources.ApplyResources(this.lblStar4, "lblStar4");
            this.lblStar4.Name = "lblStar4";
            // 
            // lblStar2
            // 
            resources.ApplyResources(this.lblStar2, "lblStar2");
            this.lblStar2.Name = "lblStar2";
            // 
            // lblStar1
            // 
            resources.ApplyResources(this.lblStar1, "lblStar1");
            this.lblStar1.Name = "lblStar1";
            // 
            // btnDistAgent
            // 
            resources.ApplyResources(this.btnDistAgent, "btnDistAgent");
            this.btnDistAgent.Name = "btnDistAgent";
            this.btnDistAgent.UseVisualStyleBackColor = true;
            this.btnDistAgent.Click += new System.EventHandler(this.btnDistAgent_Click);
            // 
            // SupplierAE
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlMain);
            this.Name = "SupplierAE";
            this.Deactivate += new System.EventHandler(this.SupplierAE_Deactivate);
            this.Load += new System.EventHandler(this.SupplierAE_Load);
            this.Activated += new System.EventHandler(this.SupplierAE_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SupplierAE_FormClosing);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.pnlDistAgent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDistAgent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EP)).EndInit();
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.pnlCustInfo.ResumeLayout(false);
            this.pnlCustInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtOPinCode;
        private System.Windows.Forms.TextBox txtOPhone2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cmbOState;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cmbOCity;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtOAddress;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtOPhone1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtMobileNo1;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtMobileNo2;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtEmailID;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtLedgerName;
        private System.Windows.Forms.Label l;
        private System.Windows.Forms.TextBox txtContactPer;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.Button btnLast;
        private System.Windows.Forms.Button btnFirst;
        private System.Windows.Forms.ErrorProvider EP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbSign;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtOpBalance;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button BtnExit;
        private System.Windows.Forms.Button BtnSave;
        private JitControls.OMBPanel pnlMain;
        private System.Windows.Forms.TextBox txtCreditLimit;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chkActive;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtVatNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPanNo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtGSTIN;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnDistAgent;
        private System.Windows.Forms.Panel pnlDistAgent;
        private System.Windows.Forms.DataGridView dgvDistAgent;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkSrNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn DistributorAgentName;
        private System.Windows.Forms.DataGridViewTextBoxColumn MobileNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remark;
        private System.Windows.Forms.Label lblStar1;
        private System.Windows.Forms.Label lblStar4;
        private System.Windows.Forms.Label lblStar2;
        private System.Windows.Forms.Button btnDistCancel;
        private System.Windows.Forms.Label lblChkHelp;
        private System.Windows.Forms.Button btnMfg;
        private System.Windows.Forms.Label lblDeActiveCount;
        private System.Windows.Forms.Label lblActiveCount;
        private System.Windows.Forms.Label lblTotalCount;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private JitControls.OMBPanel pnlCustInfo;
    }
}