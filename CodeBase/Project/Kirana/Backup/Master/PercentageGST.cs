﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Master
{
    public partial class PercentageGST : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();
        DBMLedger dbLedger = new DBMLedger();
        MLedger mLedger = new MLedger();
        MItemTaxSetting mItemTaxSetting = new MItemTaxSetting();

        long TaxTypeNo=GroupType.GST;

         public PercentageGST()
        {
            InitializeComponent();
        }
      
        private void TaxPercentage_Load(object sender, EventArgs e)
        {
            lblTotalGST.Text = "";
            TxtIGST.Text = TxtCGST.Text = TxtSGST.Text = TxtUTGST.Text = TxtCESS.Text = "0.00";
            TxtIGST.Focus();
        }

        public bool Validations()
        {
            bool flag = false;
            EP.SetError(TxtIGST, "");
            EP.SetError(TxtCGST, "");
            EP.SetError(TxtSGST, "");
            EP.SetError(TxtUTGST, "");
            EP.SetError(TxtCESS, "");

            if (TxtIGST.Text.Trim() == "")
            {
                EP.SetError(TxtIGST, "Enter IGST");
                EP.SetIconAlignment(TxtIGST, ErrorIconAlignment.MiddleRight);
                TxtIGST.Focus();
            }
            else if (TxtCGST.Text.Trim() == "")
            {
                EP.SetError(TxtCGST, "Enter CGST");
                EP.SetIconAlignment(TxtCGST, ErrorIconAlignment.MiddleRight);
                TxtCGST.Focus();
            }
            else if (TxtSGST.Text.Trim() == "")
            {
                EP.SetError(TxtSGST, "Enter SGST");
                EP.SetIconAlignment(TxtSGST, ErrorIconAlignment.MiddleRight);
                TxtSGST.Focus();
            }
            else if (TxtUTGST.Text.Trim() == "")
            {
                EP.SetError(TxtUTGST, "Enter UTGST");
                EP.SetIconAlignment(TxtUTGST, ErrorIconAlignment.MiddleRight);
                TxtUTGST.Focus();
            }
            else if (TxtCESS.Text.Trim() == "")
            {
                EP.SetError(TxtCESS, "Enter UTGST");
                EP.SetIconAlignment(TxtCESS, ErrorIconAlignment.MiddleRight);
                TxtCESS.Focus();
            }
            else if (ObjQry.ReturnInteger(" Select Count(*) FROM MItemTaxSetting " +
             " INNER JOIN MLedger ON MItemTaxSetting.SalesLedgerNo = MLedger.LedgerNo " +
             " INNER JOIN MLedger AS MLedger_1 ON MItemTaxSetting.TaxLedgerNo = MLedger_1.LedgerNo " +
             " WHERE (MLedger.GroupNo = " + GroupType.SalesAccount + ") " +
                     " AND (MLedger_1.GroupNo = " + TaxTypeNo + ") " +
                     " AND MItemTaxSetting.IGSTPercent=" + TxtIGST.Text + " " +
                     " AND MItemTaxSetting.CGSTPercent=" + TxtCGST.Text + " " +
                " AND MItemTaxSetting.SGSTPercent=" + TxtSGST.Text + " " +
                " AND MItemTaxSetting.UTGSTPercent=" + TxtUTGST.Text + " " +
                " AND MItemTaxSetting.CessPercent=" + TxtCESS.Text + "", CommonFunctions.ConStr) != 0)
            {
                OMMessageBox.Show("Duplicate GST Tax Details", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                flag = false;
            }
            else
                flag = true;

            return flag;
        }

        private void btnPecOk_Click(object sender, EventArgs e)
        {
            /*13-Apr-2014
            * Exception Handling Purpose.
            * */
            try
            {
                if (Validations() == true)
                {
                    try
                    {
                        double TotatTax = Convert.ToDouble(TxtIGST.Text) + Convert.ToDouble(TxtCESS.Text);

                        addTaxSetting(TotatTax, GroupType.SalesAccount);

                        addTaxSetting(TotatTax, GroupType.PurchaseAccount);

                        OMMessageBox.Show("Data Saved Successfuly", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);

                        this.DialogResult = DialogResult.OK;
                    }
                    catch (Exception ex)
                    {
                        OMMessageBox.Show(ex.Message, CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);//For Common Error Displayed Purpose
            }
        }
     
        private void addTaxSetting(double taxSlabPercent, long TransactionTypeNo)
        {
            DBMLedger dbLedger = new DBMLedger();
            MLedger mLedger = new MLedger();
            mLedger.LedgerNo = 0;
            mLedger.LedgerUserNo = "0";
            mLedger.LedgerName = "GST @ " + taxSlabPercent.ToString(Format.DoubleFloating) + " % " +
                                    (TransactionTypeNo == GroupType.SalesAccount ? "Sales" : "Purchase");
            mLedger.GroupNo = GroupType.GST;
            mLedger.ContactPerson = "";
            mLedger.InvFlag = false;
            mLedger.MaintainBillByBill = false;
            mLedger.IsActive = true;
            mLedger.LedgerStatus = 1;
            mLedger.UserID = 0;
            mLedger.UserDate = DateTime.Now;
            dbLedger.AddMLedger(mLedger);

            mLedger = new MLedger();
            mLedger.LedgerNo = 0;
            mLedger.LedgerUserNo = "0";
            mLedger.LedgerName = (TransactionTypeNo == GroupType.SalesAccount ? "Sales" : "Purchase") + " @ " +
                                    taxSlabPercent.ToString(Format.DoubleFloating) + " % GST";
            mLedger.GroupNo = TransactionTypeNo;
            mLedger.ContactPerson = "";
            mLedger.InvFlag = false;
            mLedger.MaintainBillByBill = false;
            mLedger.IsActive = true;
            mLedger.LedgerStatus = 1;
            mLedger.UserID = 0;
            mLedger.UserDate = DateTime.Now;
            dbLedger.AddMLedger(mLedger);

            MItemTaxSetting mItemTaxSetting = new MItemTaxSetting();
            mItemTaxSetting.PkSrNo = 0;
            mItemTaxSetting.TaxSettingName = taxSlabPercent.ToString(Format.DoubleFloating) + " % GST " + (TransactionTypeNo == GroupType.SalesAccount ? "Sales" : "Purchase");
            mItemTaxSetting.Percentage = taxSlabPercent;
            mItemTaxSetting.IsActive = true;
            mItemTaxSetting.CalculationMethod = "2";
            mItemTaxSetting.UserID = 0;
            mItemTaxSetting.UserDate = DateTime.Now;
            mItemTaxSetting.TaxTypeNo = GroupType.GST;
            mItemTaxSetting.IGSTPercent = Convert.ToDouble(TxtIGST.Text);
            mItemTaxSetting.CGSTPercent = Convert.ToDouble(TxtCGST.Text);
            mItemTaxSetting.SGSTPercent = Convert.ToDouble(TxtSGST.Text);
            mItemTaxSetting.UTGSTPercent = Convert.ToDouble(TxtUTGST.Text);
            mItemTaxSetting.CessPercent = Convert.ToDouble(TxtCESS.Text);
            mItemTaxSetting.TransactionTypeNo = TransactionTypeNo;

            dbLedger.AddMItemTaxSetting(mItemTaxSetting);

            if (!dbLedger.ExecuteNonQueryStatements() == true)
            {
                throw new Exception("Error while adding new Tax setting : Err : " + DBMLedger.strerrormsg);
            }
        }

        private void BtnPerCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void txtPercentage_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked((TextBox)sender, 2, 3);
            CalculateTax();
        }

        private void CalculateTax()
        {
            double Total = 0;
            if (TxtIGST.Text.Trim() != "" &&
                TxtCESS.Text.Trim() != "")
            {
                Total = Convert.ToDouble(TxtIGST.Text) + Convert.ToDouble(TxtCESS.Text);
            }
            lblTotalGST.Text = "Total GST = " + Total.ToString("0.00") + "%";
        }

        private void TxtIGST_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (TxtIGST.Text.Trim() != "" && Convert.ToDouble(TxtIGST.Text) != 0)
                {
                    double IGST = Convert.ToDouble(TxtIGST.Text);
                    TxtIGST.Text = IGST.ToString("0.00");
                    TxtCGST.Text = (IGST / 2).ToString("0.00");
                    TxtSGST.Text = (IGST / 2).ToString("0.00");
                }
            }
        }

        private void Txt_Leave(object sender, EventArgs e)
        {
            if (((TextBox)sender).Text.Trim() != "")
            {
                ((TextBox)sender).Text = Convert.ToDouble(((TextBox)sender).Text).ToString("0.00");
            }
        }


    }
}
