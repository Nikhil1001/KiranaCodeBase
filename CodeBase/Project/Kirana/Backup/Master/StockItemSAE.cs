﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Master
{
    /// <summary>
    /// This class used for Stock ItemS AE
    /// </summary>
    public partial class StockItemSAE : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        DBMStockItems dbStockItems = new DBMStockItems();
        MStockItems mStockItems = new MStockItems();
        MStockBarcode mStockBarcode = new MStockBarcode();
        MRateSetting mRateSetting = new MRateSetting();
        DBMLedger dbLedger = new DBMLedger();
        MLedger mLedger = new MLedger();

        string ItemNm, ItemShort, BarcodeNm, ShortCodeNm, strParaBarCode = "";
        DataTable dtSearch = new DataTable();
        //DataTable dt;
        int cntRow;//, nw, ColumnIndex;
        //int defaultUOMRowNo = -1, LowerUOMRowNo = -1;
        /// <summary>
        /// This field is used for ShortID
        /// </summary>
        public long ShortID = 0;
        //long PreDefaultUom = 0, PreLowerUom = 0;
        long ID, PkSrNo, GID = 0;
        bool FlagChange, FlagBilingual;
        double PurRate = 0, MRP = 0, ASaleRate = 0, BSaleRate = 0;
        double MRPC = 0;
        DateTime FromDate;
        bool isDoProcess = true;
        long isVatSales = 0, isVatPur = 0;
        long GstTaxPercentNo_Sales = 0, GstHsnNo = 0;
        string StrShowItemName = "";
        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public StockItemSAE()
        {
            InitializeComponent();
        }

        /// <summary>
        /// This is class of (long) parameterised Constructor
        /// </summary>
        public StockItemSAE(long ShortID)
        {
            InitializeComponent();
            this.ShortID = ShortID;
        }

        /// <summary>
        /// This is class of (long,string) parameterised Constructor
        /// </summary>
        public StockItemSAE(long ShortID, string strParaBarCode)
        {
            InitializeComponent();
            this.ShortID = ShortID;
            this.strParaBarCode = strParaBarCode;
        }

        private void StockItemAE_Activated(object sender, EventArgs e)
        {
            try
            {
                FillComboAllMasters();
                txtLanguage.Font = ObjFunction.GetLangFont();
                txtLangFullDesc.Font = ObjFunction.GetLangFont();
                txtLangShortDesc.Font = ObjFunction.GetLangFont();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillComboAllMasters()
        {
            try
            {
                long iselected = 0;
                if (isDoProcess)
                {
                    iselected = ObjFunction.GetComboValue(cmbGroupNo1);
                    ObjFunction.FillCombo(cmbGroupNo1, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE ControlGroup=3 ORDER BY StockGroupName");//IsActive = 'True' AND
                    cmbGroupNo1.SelectedValue = iselected;

                    iselected = ObjFunction.GetComboValue(cmbGodownNo);
                    ObjFunction.FillCombo(cmbGodownNo, " Select GodownNo,GodownName From MGodown Where GodownNo<>1 And IsActive='true'");
                    cmbGodownNo.SelectedValue = iselected;

                    iselected = ObjFunction.GetComboValue(cmbUom);
                    ObjFunction.FillCombo(cmbUom, "SELECT UOMNo,UOMName from MUOM WHERE IsActive = 'True' or UOMNo=" + mStockItems.UOMDefault + " ORDER BY UOMName");
                    cmbUom.SelectedValue = iselected;

                    iselected = ObjFunction.GetComboValue(cmbHSNCode);
                    ObjFunction.FillCombo(cmbHSNCode, "SELECT MStockHSNCode.HSNNo, MStockHSNCode.HSNCode FROM MStockHSNCode " +
                        " WHERE (MStockHSNCode.IsActive = 'True') OR MStockHSNCode.HSNNo = " + GstHsnNo +
                        " Order by MStockHSNCode.HSNCode");
                    cmbHSNCode.SelectedValue = iselected;

                    iselected = ObjFunction.GetComboValue(cmbDepartmentName);
                    ObjFunction.FillCombo(cmbDepartmentName, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=4 ORDER BY StockGroupName");
                    cmbDepartmentName.SelectedValue = iselected;

                    iselected = ObjFunction.GetComboValue(cmbGSTPercent);
                    ObjFunction.FillCombo(cmbGSTPercent, "SELECT MItemTaxSetting.PkSrNo, " +
                      " (cast(MItemTaxSetting.IGSTPercent as varchar)+ ' / ')+" +
                      " (cast(MItemTaxSetting.CGSTPercent as varchar)+ ' / ')+" +
                      " (cast(MItemTaxSetting.SGSTPercent as varchar)+ ' / ')+" +
                      " (cast(MItemTaxSetting.UTGSTPercent as varchar)+ ' / ')+" +
                      " (cast(MItemTaxSetting.CessPercent as varchar))" +
                      " as Percentage " +
                      " FROM MItemTaxSetting " +
                      " WHERE (MItemTaxSetting.TransactionTypeNo = " + GroupType.SalesAccount + ") " +
                          " AND (MItemTaxSetting.TaxTypeNo = " + GroupType.GST + " AND MItemTaxSetting.IsActive='True') " +
                          " OR MItemTaxSetting.PkSrNo = " + GstTaxPercentNo_Sales + " Order by  MItemTaxSetting.IGSTPercent,MItemTaxSetting.CessPercent ");
                    cmbGSTPercent.SelectedValue = iselected;



                    lblManufacturerCompanyName.Text = "Company Name : " + ObjQry.ReturnString(" SELECT   MManufacturerCompany.MfgCompName FROM   MStockGroup INNER JOIN " +
                                                                                  " MManufacturerCompany ON MStockGroup.MfgCompNo = MManufacturerCompany.MfgCompNo " +
                                                                                  " WHERE  (MStockGroup.StockGroupNo = " + ObjFunction.GetComboValue(cmbGroupNo1) + ")", CommonFunctions.ConStr);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void CheckActive()
        {
            txtBarcobe.Visible = label11.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_BarCodeDisplay));
            lblStar1.Visible = lblStar17.Visible = label13.Visible = txtShortCode.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_BarCodeDisplay));

        }

        private void StockItemAE_Load(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_Bilingual)) == true)
                {
                    FlagBilingual = true;
                    SetFlag(true);
                }
                else
                {
                    FlagBilingual = false;
                    SetFlag(false);
                }
                btnNewBrand.Visible = false;
                FillComboAllMasters();


                if (ObjFunction.GetAppSettings(AppSettings.S_DefaultStockLocation) == "0")
                {
                    cmbGodownNo.Visible = true;
                    pnlHamli.Visible = true;
                    label32.Visible = true;
                }
                else
                {
                    label32.Visible = false;
                    cmbGodownNo.Visible = false;
                    pnlHamli.Visible = false;
                    cmbGodownNo.SelectedValue = ObjFunction.GetAppSettings(AppSettings.S_DefaultStockLocation);
                }

                ObjFunction.FillCombo(cmbGodownNo, " Select GodownNo,GodownName From MGodown Where GodownNo<>1 And IsActive='true'");
                ObjFunction.FillCombo(cmbGroupNo1, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE ControlGroup=3 AND StockGroupNo=0 ORDER BY StockGroupName");//IsActive = 'True' AND
                ObjFunction.FillCombo(cmbUom, "SELECT UOMNo,UOMName from MUOM WHERE IsActive = 'True' ORDER BY UOMName");
                ObjFunction.FillCombo(cmbDepartmentName, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=4 ORDER BY StockGroupName");

                FillDiscType(cmbDiscountType);

                CheckActive();
                ObjFunction.LockButtons(true, this.Controls);
                ObjFunction.LockControls(false, this.Controls);
                btnLangLongDesc.Enabled = false;
                btnLangShortDesc.Enabled = false;
                btnSalesVat.Enabled = false;
                btnPurVat.Enabled = false;
                btnAddGST.Enabled = false;

                txtSearchBarCode.Enabled = true;

                ItemNm = "";
                ItemShort = "";
                BarcodeNm = "";
                ShortCodeNm = "";
                PkSrNo = 0;
                lblManufacturerCompanyName.Text = "";
                dtSearch = ObjFunction.GetDataView("SELECT  MStockItems.ItemNo  FROM  MStockItems INNER JOIN  MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo Where MStockItems.ItemNo<>1 ORDER BY MStockGroup.StockGroupName, MStockItems.ItemNo").Table;
                PurRate = 0; MRP = 0; ASaleRate = 0; BSaleRate = 0;
                lblASaleRate.Text = ObjFunction.GetAppSettings(AppSettings.ARateLabel);
                lblBSaleRate.Text = ObjFunction.GetAppSettings(AppSettings.BRateLabel);

                FlagChange = false;
                if (ShortID == 0)
                {
                    if (dtSearch.Rows.Count > 0)
                    {
                        if (StockItem.RequestItemNo == 0)
                            ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                        else
                            ID = StockItem.RequestItemNo;
                        FillControls();
                        SetNavigation();
                    }

                    lblASaleRate.Text = ObjFunction.GetAppSettings(AppSettings.ARateLabel);
                    lblBSaleRate.Text = ObjFunction.GetAppSettings(AppSettings.BRateLabel);
                    setDisplay(true);
                    btnNew.Focus();
                }
                else
                {
                    if (ShortID < 0)
                    {
                        setDisplay(false);
                        btnNew_Click(sender, e);
                        if (strParaBarCode != "")
                        {
                            txtBarcobe.Text = strParaBarCode;
                            txtBarcobe_KeyDown(txtBarcobe, new KeyEventArgs(Keys.Enter));
                        }
                    }
                    else
                    {
                        ID = ShortID;
                        FillControls();
                        SetNavigation();
                        btnUpdate_Click(sender, e);
                    }
                }
                KeyDownFormat(this.Controls);
                btnPrintBarCode.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_PrintBarCode));
                btnPrintBarCodeAdvance.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_PrintBarCode));

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_PrintBarCode)) == true)
                {
                    btnPrintBarCode.Visible = (ID == 0) ? false : true;
                    btnPrintBarCodeAdvance.Visible = (ID == 0) ? false : true;
                    if (ID != 0)
                    {
                        if (chkActive.Checked == true)
                        {
                            btnPrintBarCode.Visible = true;
                            btnPrintBarCodeAdvance.Visible = true;
                        }
                        else
                        {
                            btnPrintBarCode.Visible = false;
                            btnPrintBarCodeAdvance.Visible = false;
                        }
                    }
                }
                lblmsg.Font = new Font("Verdana", 10F, System.Drawing.FontStyle.Bold);

            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        internal void FillDiscType(ComboBox cmb)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ID", Type.GetType("System.Int32")); dt.Columns.Add("Desc");
            DataRow dr = dt.NewRow();
            //dr[0] = "0";
            //dr[1] = " ------ Select ------ ";
            //dt.Rows.Add(dr);

            //dr = dt.NewRow();
            dr[0] = 1;
            dr[1] = "Percent";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr[0] = 2;
            dr[1] = "Rupees";
            dt.Rows.Add(dr);

            cmb.DisplayMember = dt.Columns[1].ColumnName;
            cmb.ValueMember = dt.Columns[0].ColumnName;
            cmb.DataSource = dt;
            cmb.SelectedIndex = 0;
        }

        private void SetFlag(bool flag)
        {
            txtLanguage.Visible = flag;
            txtLangFullDesc.Visible = flag;
            txtLangShortDesc.Visible = flag;
            lblStar15.Visible = flag;
            lblStar16.Visible = false;
            btnLangLongDesc.Visible = flag;
            btnLangShortDesc.Visible = flag;
        }

        private void StockItemAE_FormClosing(object sender, FormClosingEventArgs e)
        {
            ID = 0;
            MRPC = 0;
            ItemNm = "";
            ItemShort = "";
            BarcodeNm = "";
            ShortCodeNm = "";
            PkSrNo = 0;
        }

        private void FillControls()
        {
            try
            {
                EP.SetError(txtItemShortDesc, "");
                EP.SetError(cmbGroupNo1, "");
                EP.SetError(cmbDepartmentName, "");
                EP.SetError(cmbCategoryName, "");
                EP.SetError(txtBarcobe, "");
                EP.SetError(txtMRP, "");
                EP.SetError(cmbUom, "");
                EP.SetError(txtASaleRate, "");
                EP.SetError(txtBSaleRate, "");

                EP.SetError(txtMinLevelQty, "");
                EP.SetError(cmbGodownNo, "");
                EP.SetError(txtMaxLevelQty, "");
                EP.SetError(txtMinLevelQty, "");
                EP.SetError(txtLangFullDesc, "");
                EP.SetError(txtLangShortDesc, "");
                EP.SetError(txtShortCode, "");
                EP.SetError(cmbDiscountType, "");
                mStockItems = dbStockItems.ModifyMStockItemsByID(ID);

                FillComboAllMasters();
                fetchTaxInfoDetails();
                fillGstTaxDetails(GstTaxPercentNo_Sales);

                ItemNm = mStockItems.ItemName;
                ItemShort = mStockItems.ItemShortCode;

                txtItemName.Text = mStockItems.ItemName;
                txtItemShortDesc.Text = mStockItems.ItemShortCode;

                ObjFunction.FillCombo(cmbGroupNo1, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE ControlGroup=3   or StockGroupNo=" + mStockItems.GroupNo + " ORDER BY StockGroupName");//IsActive = 'True' AND
                cmbGroupNo1.SelectedValue = mStockItems.GroupNo.ToString();

                ObjFunction.FillCombo(cmbGodownNo, " Select GodownNo,GodownName From MGodown Where GodownNo<>1 And IsActive='true' or GodownNo=" + mStockItems.GodownNo + "");
                cmbGodownNo.SelectedValue = mStockItems.GodownNo.ToString();

                FillLanguageGroupName();
                cmbCategoryName.SelectedValue = mStockItems.FkCategoryNo.ToString();
                cmbDepartmentName.SelectedValue = mStockItems.FkDepartmentNo.ToString();
                cmbDiscountType.SelectedValue = mStockItems.DiscountType.ToString();
                txtMinLevelQty.Text = mStockItems.MinLevel.ToString();
                txtMaxLevelQty.Text = mStockItems.MaxLevel.ToString();
                txtLangFullDesc.Text = String.IsNullOrEmpty(mStockItems.LangFullDesc) ? "" : mStockItems.LangFullDesc.ToString();
                txtLangShortDesc.Text = String.IsNullOrEmpty(mStockItems.LangShortDesc) ? "" : mStockItems.LangShortDesc.ToString();
                txtShortCode.Text = (mStockItems.ShortCode == null) ? "" : mStockItems.ShortCode.ToString();
                ShortCodeNm = (mStockItems.ShortCode == null) ? "" : mStockItems.ShortCode.ToString();
                chkActive.Checked = mStockItems.IsActive;
                chkIsQtyRead.Checked = mStockItems.IsQtyRead;
                if (chkActive.Checked == true)
                {
                    chkActive.Text = "Yes";

                }
                else
                {
                    chkActive.Text = "No";

                }


                cmbHSNCode.SelectedValue = GstHsnNo.ToString();
                cmbGSTPercent.SelectedValue = GstTaxPercentNo_Sales.ToString();


                #region Rate And Barcode Method
                ObjFunction.FillCombo(cmbUom, "SELECT UOMNo,UOMName from MUOM WHERE IsActive = 'True' or UOMNo=" + mStockItems.UOMDefault + " ORDER BY UOMName");
                cmbUom.SelectedValue = mStockItems.UOMDefault.ToString();
                txtBarcobe.Text = ObjQry.ReturnString("select Barcode from MStockBarcode where ItemNo=" + ID + "", CommonFunctions.ConStr);
                BarcodeNm = txtBarcobe.Text.Trim().ToUpper();

                txtMRP.Text = "0.00";
                MRPC = 0;
                long barcodeno = ObjQry.ReturnLong("select isNull((PkStockBarcodeNo),0) from MStockBarcode where Barcode='" + txtBarcobe.Text.Trim() + "' AND ItemNo=" + ID + "", CommonFunctions.ConStr);
                DataTable dtRtSettingNos = ObjFunction.GetDataView("Select PkSrNo,PurRate,MRP,ASaleRate,BSaleRate,FromDate from dbo.GetItemRateAll(" + ID + "," + barcodeno + "," + ObjFunction.GetComboValue(cmbUom) + ",NULL,NULL,NUll) Order BY FromDate Desc").Table;
                if (dtRtSettingNos.Rows.Count > 0)
                {
                    txtPurRate.Text = dtRtSettingNos.Rows[0].ItemArray[1].ToString();
                    txtMRP.Text = dtRtSettingNos.Rows[0].ItemArray[2].ToString();
                    MRPC = Convert.ToDouble(dtRtSettingNos.Rows[0].ItemArray[2].ToString());
                    txtASaleRate.Text = dtRtSettingNos.Rows[0].ItemArray[3].ToString();
                    txtBSaleRate.Text = dtRtSettingNos.Rows[0].ItemArray[4].ToString();

                    PkSrNo = Convert.ToInt64(dtRtSettingNos.Rows[0].ItemArray[0].ToString());
                    FromDate = Convert.ToDateTime(dtRtSettingNos.Rows[0].ItemArray[5].ToString());

                    MRP = Convert.ToDouble(dtRtSettingNos.Rows[0].ItemArray[2].ToString());
                    PurRate = Convert.ToDouble(dtRtSettingNos.Rows[0].ItemArray[1].ToString());
                    ASaleRate = Convert.ToDouble(dtRtSettingNos.Rows[0].ItemArray[3].ToString());
                    BSaleRate = Convert.ToDouble(dtRtSettingNos.Rows[0].ItemArray[4].ToString());
                }
                else
                {
                    txtPurRate.Text = "0.00";
                    txtMRP.Text = "0.00";
                    txtASaleRate.Text = "0.00";
                    txtBSaleRate.Text = "0.00";
                    PkSrNo = 0;
                    MRPC = 0;
                    FromDate = Convert.ToDateTime("01-01-1900");
                    MRP = 0;
                    PurRate = 0;
                    ASaleRate = 0;
                    BSaleRate = 0;
                }
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_PrintBarCode)) == true)
                {
                    btnPrintBarCode.Visible = (ID == 0) ? false : true;
                    btnPrintBarCodeAdvance.Visible = (ID == 0) ? false : true;
                    if (ID != 0)
                    {
                        if (chkActive.Checked == true)
                        {
                            btnPrintBarCode.Visible = true;
                            btnPrintBarCodeAdvance.Visible = true;
                        }
                        else
                        {
                            btnPrintBarCode.Visible = false;
                            btnPrintBarCodeAdvance.Visible = false;
                        }
                    }
                }
                lblmsg.Visible = false;
                GID = 0;
                txtHamali.Text = mStockItems.HamaliInKg.ToString("0.00");
                #endregion

                StrShowItemName = mStockItems.ShowItemName;

                RateDiscCal();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void fetchTaxInfoDetails()
        {
            txtHSNCode.Text = "";
            isVatSales = 0; isVatPur = 0;
            GstTaxPercentNo_Sales = 0; GstHsnNo = 0;
            DataTable dtItemTax = ObjFunction.GetDataView("Select * From dbo.GetItemTaxAll(" + ID + ", NULL, NULL, NULL, NULL) ").Table;
            DataRow[] dr = dtItemTax.Select("GroupNo=" + GroupType.SalesAccount + " AND TaxTypeNo=" + GroupType.VAT + "");
            if (dr.Length > 0)
            {
                isVatSales = Convert.ToInt64(dr[0]["FkTaxSettingNo"].ToString());
            }
            dr = dtItemTax.Select("GroupNo=" + GroupType.PurchaseAccount + " AND TaxTypeNo=" + GroupType.VAT + "");
            if (dr.Length > 0)
            {
                isVatPur = Convert.ToInt64(dr[0]["FkTaxSettingNo"].ToString());
            }
            dr = dtItemTax.Select("GroupNo=" + GroupType.SalesAccount + " AND TaxTypeNo=" + GroupType.GST + "");
            if (dr.Length > 0)
            {
                GstTaxPercentNo_Sales = Convert.ToInt64(dr[0]["FkTaxSettingNo"].ToString());
                GstHsnNo = Convert.ToInt64(dr[0]["HSNNo"].ToString());
                txtHSNCode.Text = dr[0]["HSNCode"].ToString();
            }

        }

        private void fillGstTaxDetails(long GstTaxPercentNo_S)
        {
            DataTable dtItemTax = ObjFunction.GetDataView("Select  * From MItemTaxSetting Where PkSrNo= " + GstTaxPercentNo_S).Table;
            if (dtItemTax.Rows.Count == 0)
            {
                txtIGST.Text = "";// Format.DoubleFloating;
                txtCGST.Text = "";// Format.DoubleFloating;
                txtSGST.Text = "";// Format.DoubleFloating;
                txtUTGST.Text = "";// Format.DoubleFloating;
                TxtCESS.Text = "";
                return;
            }
            txtIGST.Text = Convert.ToDouble(dtItemTax.Rows[0]["IGSTPercent"].ToString()).ToString(Format.DoubleFloating);
            txtCGST.Text = Convert.ToDouble(dtItemTax.Rows[0]["CGSTPercent"].ToString()).ToString(Format.DoubleFloating);
            txtSGST.Text = Convert.ToDouble(dtItemTax.Rows[0]["SGSTPercent"].ToString()).ToString(Format.DoubleFloating);
            txtUTGST.Text = Convert.ToDouble(dtItemTax.Rows[0]["UTGSTPercent"].ToString()).ToString(Format.DoubleFloating);
            TxtCESS.Text = Convert.ToDouble(dtItemTax.Rows[0]["CESSPercent"].ToString()).ToString(Format.DoubleFloating);
        }

        private void SetValue()
        {
            try
            {
                if (txtMinLevelQty.Text.Trim() == "") txtMinLevelQty.Text = "0";
                if (txtMaxLevelQty.Text.Trim() == "") txtMaxLevelQty.Text = "0";
                if (Validations() == true)
                {
                    dbStockItems = new DBMStockItems();
                    mStockItems = new MStockItems();

                    mStockItems.ItemNo = ID;
                    mStockItems.ItemName = txtItemName.Text.Trim().ToUpper();
                    mStockItems.ItemShortCode = txtItemShortDesc.Text.Trim().ToUpper();
                    mStockItems.FKStockDeptNo = 10; //ObjFunction.GetComboValue(cmbStockDept);//Stock Department
                    mStockItems.GroupNo = ObjFunction.GetComboValue(cmbGroupNo1);//Brand Name
                    mStockItems.GroupNo1 = 8;
                    mStockItems.FkCategoryNo = 1;
                    mStockItems.FkDepartmentNo = ObjFunction.GetComboValue(cmbDepartmentName);
                    mStockItems.FKStockLocationNo = 1;// ObjFunction.GetComboValue(cmbStockLocationName);
                    mStockItems.UOMDefault = ObjFunction.GetComboValue(cmbUom);
                    mStockItems.UOMPrimary = ObjFunction.GetComboValue(cmbUom);
                    mStockItems.CompanyNo = DBGetVal.CompanyNo;
                    mStockItems.MinLevel = Convert.ToDouble(txtMinLevelQty.Text);
                    mStockItems.MaxLevel = Convert.ToDouble(txtMaxLevelQty.Text);
                    mStockItems.LangFullDesc = txtLangFullDesc.Text.Trim();
                    mStockItems.LangShortDesc = txtLangShortDesc.Text.Trim();
                    //mStockItems.ReOrderLevelQty = Convert.ToDouble(txtReOrderLevelQty.Text);
                    mStockItems.IsActive = chkActive.Checked;
                    mStockItems.IsFixedBarcode = false;
                    mStockItems.UserId = DBGetVal.UserID;
                    mStockItems.UserDate = DBGetVal.ServerTime.Date;
                    mStockItems.ShortCode = txtShortCode.Text.Trim();
                    mStockItems.FkStockGroupTypeNo = 0;
                    mStockItems.ControlUnder = 0;
                    mStockItems.FactorVal = 0;
                    mStockItems.MKTQty = 0;
                    mStockItems.DiscountType = ObjFunction.GetComboValue(cmbDiscountType);
                    mStockItems.GodownNo = ObjFunction.GetComboValue(cmbGodownNo);
                    mStockItems.HamaliInKg = Convert.ToDouble((txtHamali.Text.Trim() == "") ? "0" : txtHamali.Text);
                    mStockItems.Margin = 0;
                    mStockItems.IsQtyRead = chkIsQtyRead.Checked;

                    dbStockItems.AddMStockItems(mStockItems);

                    mStockBarcode = new MStockBarcode();
                    long barcodeno = ObjQry.ReturnLong("select isNull((PkStockBarcodeNo),0) from MStockBarcode where Barcode='" + BarcodeNm + "' AND ItemNo=" + ID + "", CommonFunctions.ConStr);
                    mStockBarcode.PkStockBarcodeNo = barcodeno;
                    mStockBarcode.Barcode = txtBarcobe.Text.Trim();
                    mStockBarcode.IsActive = chkActive.Checked;
                    mStockBarcode.UserID = DBGetVal.UserID;
                    mStockBarcode.UserDate = DBGetVal.ServerTime;

                    dbStockItems.AddMStockBarcode(mStockBarcode);


                    mRateSetting = new MRateSetting();

                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_MultiyRate)))
                    {
                        if (FlagChange == true && ID == 0)
                        {
                            mRateSetting.PkSrNo = 0;
                            mRateSetting.FromDate = Convert.ToDateTime("01-01-1900");
                        }
                        else if (FlagChange == true && ID != 0)
                        {
                            mRateSetting.PkSrNo = PkSrNo;
                            mRateSetting.IsActive = false;
                            dbStockItems.UpdateMRateSetting(mRateSetting);

                            mRateSetting = new MRateSetting();
                            mRateSetting.PkSrNo = 0;
                            mRateSetting.FromDate = Convert.ToDateTime("01-01-1900");
                        }
                        else if (FlagChange == false)
                        {
                            mRateSetting.PkSrNo = PkSrNo;
                            mRateSetting.FromDate = FromDate;
                        }
                    }
                    else
                    {
                        mRateSetting.PkSrNo = PkSrNo;
                        mRateSetting.FromDate = FromDate;
                    }
                    mRateSetting.PurRate = Convert.ToDouble(txtPurRate.Text.Trim());
                    mRateSetting.UOMNo = ObjFunction.GetComboValue(cmbUom);
                    mRateSetting.ASaleRate = Convert.ToDouble(txtASaleRate.Text.Trim());
                    mRateSetting.BSaleRate = Convert.ToDouble(txtBSaleRate.Text.Trim());
                    mRateSetting.CSaleRate = 0; // Convert.ToDouble(txtNosMRP.Text.Trim());
                    mRateSetting.DSaleRate = 0; // Convert.ToDouble(txtNosMRP.Text.Trim());
                    mRateSetting.ESaleRate = 0; // Convert.ToDouble(txtNosMRP.Text.Trim());
                    mRateSetting.StockConversion = 1;
                    mRateSetting.PerOfRateVariation = 1;
                    mRateSetting.MKTQty = 1;
                    mRateSetting.MRP = Convert.ToDouble(txtMRP.Text.Trim());
                    mRateSetting.IsActive = true;
                    mRateSetting.UserID = DBGetVal.UserID;
                    mRateSetting.UserDate = DBGetVal.ServerTime.Date;
                    mRateSetting.CompanyNo = DBGetVal.CompanyNo;
                    dbStockItems.AddMRateSetting2(mRateSetting);


                    DataTable dtItemTax = ObjFunction.GetDataView("Select * From dbo.GetItemTaxAll(" + ID + ", NULL, NULL,NULL,NULL)").Table;
                    DataTable dtMainItemTax = ObjFunction.GetDataView("Select * From MItemTaxSetting " +
                        " Where PkSrNo in (0,0,0,0,0," +
                        ObjFunction.GetComboValue(cmbGSTPercent) + ")").Table;

                    #region GST Sales and Purchase
                    if (ObjFunction.GetComboValue(cmbGSTPercent) != 0)
                    {
                        double GST_Percent = 0;
                        #region For Sales
                        MItemTaxInfo mItemTaxInfo = new MItemTaxInfo();

                        DataRow[] drMain = dtMainItemTax.Select("PkSrNo=" + ObjFunction.GetComboValue(cmbGSTPercent) + "");
                        DataRow[] dr = dtItemTax.Select("GroupNo=" + GroupType.SalesAccount + " AND TaxTypeNo=" + GroupType.GST + "");

                        if (dr.Length > 0)
                        {
                            if (DBGetVal.ServerTime.Date != Convert.ToDateTime(dr[0]["FromDate"].ToString()).Date)
                            {
                                mItemTaxInfo.PkSrNo = Convert.ToInt64(dr[0]["PkSrNo"].ToString());
                            }
                            else
                                mItemTaxInfo.PkSrNo = Convert.ToInt64(dr[0]["PkSrNo"].ToString());
                        }
                        else
                        {
                            mItemTaxInfo.PkSrNo = 0;
                        }
                        mItemTaxInfo.TaxLedgerNo = Convert.ToInt64(drMain[0]["TaxLedgerNo"].ToString());
                        mItemTaxInfo.SalesLedgerNo = Convert.ToInt64(drMain[0]["SalesLedgerNo"].ToString());
                        mItemTaxInfo.FromDate = DBGetVal.ServerTime.Date;
                        mItemTaxInfo.CalculationMethod = "2";
                        mItemTaxInfo.Percentage = Convert.ToDouble(drMain[0]["Percentage"].ToString());
                        GST_Percent = Convert.ToDouble(drMain[0]["Percentage"].ToString());
                        mItemTaxInfo.FKTaxSettingNo = Convert.ToInt64(drMain[0]["PkSrNo"].ToString());
                        mItemTaxInfo.UserID = DBGetVal.UserID;
                        mItemTaxInfo.UserDate = DBGetVal.ServerTime.Date;
                        mItemTaxInfo.TaxTypeNo = Convert.ToInt64(drMain[0]["TaxTypeNo"].ToString());
                        mItemTaxInfo.TransactionTypeNo = Convert.ToInt64(drMain[0]["TransactionTypeNo"].ToString());
                        mItemTaxInfo.HSNNo = ObjFunction.GetComboValue(cmbHSNCode);

                        mItemTaxInfo.IGSTPercent = Convert.ToDouble(txtIGST.Text);
                        mItemTaxInfo.CGSTPercent = Convert.ToDouble(txtCGST.Text);
                        mItemTaxInfo.SGSTPercent = Convert.ToDouble(txtSGST.Text);
                        mItemTaxInfo.UTGSTPercent = Convert.ToDouble(txtUTGST.Text);
                        mItemTaxInfo.CessPercent = Convert.ToDouble(TxtCESS.Text);

                        if (mItemTaxInfo.HSNNo > 0)
                        {
                            mItemTaxInfo.HSNCode = cmbHSNCode.Text;
                        }
                        else
                        {
                            mItemTaxInfo.HSNCode = txtHSNCode.Text;
                        }
                        mItemTaxInfo.IsActive = true;
                        dbStockItems.AddMItemTaxInfo(mItemTaxInfo);
                        #endregion

                        #region For Purchase
                        DataTable dtMainItemTax_Temp = ObjFunction.GetDataView("Select TOP 1 * From MItemTaxSetting " +
                                " Where TaxTypeNo = " + GroupType.GST +
                                " AND TransactionTypeNo = " + GroupType.PurchaseAccount +
                                " AND Percentage = " + GST_Percent +
                                " AND IsActive = 'True' ORDER BY PkSrNo Desc").Table;
                        if (dtMainItemTax_Temp.Rows.Count > 0)
                        {
                            mItemTaxInfo = new MItemTaxInfo();

                            DataRow drMain_p = dtMainItemTax_Temp.Rows[0];
                            dr = dtItemTax.Select("GroupNo=" + GroupType.PurchaseAccount + " AND TaxTypeNo=" + GroupType.GST + "");

                            if (dr.Length > 0)
                            {
                                if (DBGetVal.ServerTime.Date != Convert.ToDateTime(dr[0]["FromDate"].ToString()).Date)
                                {
                                    mItemTaxInfo.PkSrNo = Convert.ToInt64(dr[0]["PkSrNo"].ToString());
                                }
                                else
                                    mItemTaxInfo.PkSrNo = Convert.ToInt64(dr[0]["PkSrNo"].ToString());
                            }
                            else
                            {
                                mItemTaxInfo.PkSrNo = 0;
                            }
                            mItemTaxInfo.TaxLedgerNo = Convert.ToInt64(drMain_p["TaxLedgerNo"].ToString());
                            mItemTaxInfo.SalesLedgerNo = Convert.ToInt64(drMain_p["SalesLedgerNo"].ToString());
                            mItemTaxInfo.FromDate = DBGetVal.ServerTime.Date;
                            mItemTaxInfo.CalculationMethod = "2";
                            mItemTaxInfo.Percentage = Convert.ToDouble(drMain_p["Percentage"].ToString());
                            mItemTaxInfo.FKTaxSettingNo = Convert.ToInt64(drMain_p["PkSrNo"].ToString());
                            mItemTaxInfo.UserID = DBGetVal.UserID;
                            mItemTaxInfo.UserDate = DBGetVal.ServerTime.Date;
                            mItemTaxInfo.TaxTypeNo = Convert.ToInt64(drMain_p["TaxTypeNo"].ToString());
                            mItemTaxInfo.TransactionTypeNo = Convert.ToInt64(drMain_p["TransactionTypeNo"].ToString());
                            mItemTaxInfo.HSNNo = ObjFunction.GetComboValue(cmbHSNCode);
                            mItemTaxInfo.IGSTPercent = Convert.ToDouble(txtIGST.Text);
                            mItemTaxInfo.CGSTPercent = Convert.ToDouble(txtCGST.Text);
                            mItemTaxInfo.SGSTPercent = Convert.ToDouble(txtSGST.Text);
                            mItemTaxInfo.UTGSTPercent = Convert.ToDouble(txtUTGST.Text);
                            mItemTaxInfo.CessPercent = Convert.ToDouble(TxtCESS.Text);

                            if (mItemTaxInfo.HSNNo > 0)
                            {
                                mItemTaxInfo.HSNCode = cmbHSNCode.Text;
                            }
                            else
                            {
                                mItemTaxInfo.HSNCode = txtHSNCode.Text;
                            }
                            mItemTaxInfo.IsActive = true;
                            dbStockItems.AddMItemTaxInfo(mItemTaxInfo);
                        }
                        #endregion
                    }
                    #endregion

                    if (mStockItems.IsActive == true)//&& ID != 0
                    {
                        dbStockItems.UpdateGroupNo(true, ObjFunction.GetComboValue(cmbCategoryName));
                        dbStockItems.UpdateGroupNo(true, ObjFunction.GetComboValue(cmbGroupNo1));
                        dbStockItems.UpdateGroupNo(true, ObjFunction.GetComboValue(cmbGroupNo2));
                    }
                    //Nikhil GlobalCode
                    if (GID != 0 && ID == 0)
                        dbStockItems.UpdateGlobalStockItems(GID);

                    if (dbStockItems.ExecuteNonQueryStatements() == true)
                    {
                        if (ID == 0)
                        {
                            //if (GID == 0)
                            //    SetValueGolable();

                            OMMessageBox.Show(" Item Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            dtSearch = ObjFunction.GetDataView("Select ItemNo From MStockItems WHERE ItemNo<>1 Order By ItemNo").Table;
                            ID = ObjQry.ReturnLong("Select Max(ItemNo) FRom MStockItems", CommonFunctions.ConStr);
                            if (ShortID == 0)
                            {
                                SetNavigation();
                                FillControls();
                            }
                            else
                            {
                                ShortID = ID;
                                this.Close();
                            }
                        }
                        else
                        {
                            OMMessageBox.Show(" Item Updated Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            FillControls();
                            if (ShortID != 0)
                            {
                                ShortID = ID;
                                this.Close();
                            }
                        }

                        ObjFunction.LockButtons(true, this.Controls);
                        ObjFunction.LockControls(false, this.Controls);
                        btnLangLongDesc.Enabled = false;
                        btnLangShortDesc.Enabled = false;
                        btnSalesVat.Enabled = false;
                        btnPurVat.Enabled = false;
                        btnAddGST.Enabled = false;

                        txtSearchBarCode.Enabled = true;
                        //btnNew_Click(new object(), new EventArgs());
                        btnNew.Focus();
                    }
                    else
                    {
                        OMMessageBox.Show(" Item not saved", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void Control_Leave(object sender, EventArgs e)
        {
            try
            {
                if (((TextBox)sender).Name == "txtMRP")
                {
                    if (txtMRP.Text.Trim() != "")
                    {
                        if (MRPC != Convert.ToDouble(txtMRP.Text))
                        {
                            txtASaleRate.Text = txtMRP.Text.Trim();
                            txtBSaleRate.Text = txtMRP.Text.Trim();
                        }
                    }


                }
                if (ValidationRate() == true)
                {
                    if (((TextBox)sender).Name == "txtASaleRate")
                    {
                        if (Convert.ToDouble(((TextBox)sender).Text) != ASaleRate)
                            FlagChange = true;
                        else if (FlagChange != true) FlagChange = false;
                    }
                    else if (((TextBox)sender).Name == "txtBSaleRate")
                    {
                        if (Convert.ToDouble(((TextBox)sender).Text) != BSaleRate)
                            FlagChange = true;
                        else if (FlagChange != true) FlagChange = false;
                    }
                    else if (((TextBox)sender).Name == "txtMRP")
                    {
                        if (Convert.ToDouble(((TextBox)sender).Text) != MRP)
                            FlagChange = true;
                        else if (FlagChange != true) FlagChange = false;
                    }
                    else if (((TextBox)sender).Name == "txtPurRate")
                    {
                        if (Convert.ToDouble(((TextBox)sender).Text) != PurRate)
                            FlagChange = true;
                        else if (FlagChange != true) FlagChange = false;
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void RateDiscCal()
        {
            double MRP = Convert.ToDouble(txtMRP.Text);
            if (MRP == 0)
            {
                txtDiscPer.Text = "0.00";
                return;
            }
            double Rate = Convert.ToDouble(txtBSaleRate.Text);
            double DiscPer = ((MRP - Rate) * 100) / MRP;
            txtDiscPer.Text = DiscPer.ToString("0.00");

        }

        private bool ValidationRate()
        {
            bool flag = true;
            try
            {
                EP.SetError(txtPurRate, "");
                EP.SetError(txtASaleRate, "");
                EP.SetError(txtBSaleRate, "");
                EP.SetError(txtMRP, "");

                if (txtMRP.Text.Trim() == "")
                {
                    EP.SetError(txtMRP, "Enter MRP");
                    EP.SetIconAlignment(txtMRP, ErrorIconAlignment.MiddleRight);
                    if (flag) { flag = false; txtMRP.Focus(); }
                }
                else if (txtASaleRate.Text.Trim() == "")
                {
                    EP.SetError(txtASaleRate, "Enter " + ObjFunction.GetAppSettings(AppSettings.ARateLabel) + " Rate");
                    EP.SetIconAlignment(txtASaleRate, ErrorIconAlignment.MiddleRight);
                    if (flag) { flag = false; txtASaleRate.Focus(); }
                }
                else if (txtBSaleRate.Text.Trim() == "")
                {
                    EP.SetError(txtBSaleRate, "Enter " + ObjFunction.GetAppSettings(AppSettings.BRateLabel) + " Rate");
                    EP.SetIconAlignment(txtBSaleRate, ErrorIconAlignment.MiddleRight);
                    if (flag) { flag = false; txtBSaleRate.Focus(); }
                }
                else if (txtPurRate.Text.Trim() == "")
                {
                    EP.SetError(txtPurRate, "Enter PurRate");
                    EP.SetIconAlignment(txtPurRate, ErrorIconAlignment.MiddleRight);
                    if (flag) { flag = false; txtPurRate.Focus(); }
                }


                if (flag == true)
                {
                    if (ObjFunction.CheckValidAmount(txtASaleRate.Text.Trim()) == false)
                    {
                        EP.SetError(txtASaleRate, "Enter Valid " + ObjFunction.GetAppSettings(AppSettings.ARateLabel) + " Rate");
                        EP.SetIconAlignment(txtASaleRate, ErrorIconAlignment.MiddleRight);
                        txtASaleRate.Focus(); flag = false;
                    }
                    else if (ObjFunction.CheckValidAmount(txtBSaleRate.Text.Trim()) == false)
                    {
                        EP.SetError(txtBSaleRate, "Enter Valid " + ObjFunction.GetAppSettings(AppSettings.BRateLabel) + " Rate");
                        EP.SetIconAlignment(txtBSaleRate, ErrorIconAlignment.MiddleRight);
                        txtBSaleRate.Focus(); flag = false;
                    }
                    else if (ObjFunction.CheckValidAmount(txtMRP.Text.Trim()) == false)
                    {
                        EP.SetError(txtMRP, "Enter Valid MRP");
                        EP.SetIconAlignment(txtMRP, ErrorIconAlignment.MiddleRight);
                        txtMRP.Focus(); flag = false;
                    }
                    else if (ObjFunction.CheckValidAmount(txtPurRate.Text.Trim()) == false)
                    {
                        EP.SetError(txtPurRate, "Enter Valid PurRate");
                        EP.SetIconAlignment(txtPurRate, ErrorIconAlignment.MiddleRight);
                        txtPurRate.Focus(); flag = false;
                    }
                    else
                        flag = true;

                }

                if (flag == true)
                    RateDiscCal();

                return flag;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return false;
            }
        }

        private bool Validations()
        {
            bool flag = true;
            try
            {
                EP.SetError(cmbStockDept, "");
                EP.SetError(cmbGroupNo2, "");
                //EP.SetError(cmbCompanyName, "");
                EP.SetError(cmbGroupNo1, "");
                //EP.SetError(txtItemName, "");
                //EP.SetError(txtShortName, "");
                EP.SetError(cmbDepartmentName, "");
                EP.SetError(cmbCategoryName, "");
                EP.SetError(txtBarcobe, "");
                EP.SetError(txtMRP, "");
                EP.SetError(cmbUom, "");
                EP.SetError(txtLangFullDesc, "");
                EP.SetError(txtLangShortDesc, "");
                EP.SetError(txtMaxLevelQty, "");
                EP.SetError(txtShortCode, "");
                EP.SetError(txtBSaleRate, "");
                EP.SetError(txtASaleRate, "");
                EP.SetError(txtShortCode, "");
                EP.SetError(cmbGodownNo, "");
                EP.SetError(cmbDiscountType, "");

                if (txtShortCode.Text.Trim() == "")
                {
                    EP.SetError(txtShortCode, "Enter Short Code");
                    EP.SetIconAlignment(txtShortCode, ErrorIconAlignment.MiddleRight);
                    if (flag) { flag = false; txtShortCode.Focus(); }
                }
                if (ObjFunction.GetComboValue(cmbGodownNo) <= 0)
                {
                    EP.SetError(cmbGodownNo, "Select Godown");
                    EP.SetIconAlignment(cmbGodownNo, ErrorIconAlignment.MiddleRight);
                    if (flag) { flag = false; cmbGodownNo.Focus(); }
                }
                if (ObjFunction.GetComboValue(cmbDiscountType) <= 0)
                {
                    EP.SetError(cmbDiscountType, "Select Discount Type");
                    EP.SetIconAlignment(cmbDiscountType, ErrorIconAlignment.MiddleRight);
                    if (flag) { flag = false; cmbDiscountType.Focus(); }
                }
                if (ObjFunction.GetComboValue(cmbGroupNo1) <= 0)
                {
                    EP.SetError(cmbGroupNo1, "Select Brand Name");
                    EP.SetIconAlignment(cmbGroupNo1, ErrorIconAlignment.MiddleRight);
                    if (flag) { flag = false; cmbGroupNo1.Focus(); }
                }
                if (txtItemName.Text.Trim() == "")
                {
                    EP.SetError(txtItemName, "Enter Item Name");
                    EP.SetIconAlignment(txtItemName, ErrorIconAlignment.MiddleRight);
                    if (flag) { flag = false; txtItemName.Focus(); }
                }
                if (ObjFunction.GetComboValue(cmbUom) <= 0)
                {
                    EP.SetError(cmbUom, "Select UOM");
                    EP.SetIconAlignment(cmbUom, ErrorIconAlignment.MiddleRight);
                    if (flag) { flag = false; cmbUom.Focus(); }
                }
                if (txtASaleRate.Text.Trim() == "")
                {
                    EP.SetError(txtASaleRate, "Enter Value");
                    EP.SetIconAlignment(txtASaleRate, ErrorIconAlignment.MiddleRight);
                    if (flag) { flag = false; txtASaleRate.Focus(); }
                }
                if (txtBSaleRate.Text.Trim() == "")
                {
                    EP.SetError(txtBSaleRate, "Enter Value");
                    EP.SetIconAlignment(txtBSaleRate, ErrorIconAlignment.MiddleRight);
                    if (flag) { flag = false; txtBSaleRate.Focus(); }
                }
                if (ItemNm != txtItemName.Text.Trim())
                {
                    if (ObjQry.ReturnInteger("Select Count(*) from MStockItems where ItemName = '" + txtItemName.Text.Trim() + "' " +
                        " AND GroupNo = " + ObjFunction.GetComboValue(cmbGroupNo1) +
                        " AND GroupNo1 = " + ObjFunction.GetComboValue(cmbGroupNo2) +
                        " ", CommonFunctions.ConStr) != 0)
                    {
                        EP.SetError(txtItemName, "Duplicate Item Name");
                        EP.SetIconAlignment(txtItemName, ErrorIconAlignment.MiddleRight);
                        if (flag) { flag = false; txtItemName.Focus(); }
                    }
                }
                if (ItemShort != txtItemShortDesc.Text.Trim())
                {
                    if (ObjQry.ReturnInteger("Select Count(*) from MStockItems where ItemShortCode = '" + txtItemShortDesc.Text.Trim() + "' " +
                        " AND GroupNo = " + ObjFunction.GetComboValue(cmbGroupNo1) +
                        " AND GroupNo1 = " + ObjFunction.GetComboValue(cmbGroupNo2) +
                        " ", CommonFunctions.ConStr) != 0)
                    {
                        EP.SetError(txtItemShortDesc, "Duplicate Short Desc");
                        EP.SetIconAlignment(txtItemShortDesc, ErrorIconAlignment.MiddleRight);
                        if (flag) { flag = false; txtItemShortDesc.Focus(); }
                    }
                }

                if (BarcodeNm != txtBarcobe.Text)
                {
                    //f (ObjQry.ReturnInteger("Select Count(*) from MStockBarcode where Barcode = '" + txtBarcobe.Text + "'", CommonFunctions.ConStr) != 0)
                    if (ObjQry.ReturnInteger("Select Count(*) FROM MStockBarcode INNER JOIN MStockItems ON MStockBarcode.ItemNo = MStockItems.ItemNo where (Barcode = '" + txtBarcobe.Text.Trim().Replace("'", "''") + "' or MStockItems.ShortCode='" + txtBarcobe.Text.Trim().Replace("'", "''") + "') AND MStockItems.ItemNo<>" + ID + "", CommonFunctions.ConStr) != 0)
                    {
                        EP.SetError(txtBarcobe, "Duplicate Barcode");
                        EP.SetIconAlignment(txtBarcobe, ErrorIconAlignment.MiddleRight);
                        if (flag) { flag = false; txtBarcobe.Focus(); }
                    }
                }
                if (ShortCodeNm != txtShortCode.Text)//&& txtShortCode.Text.Trim()!="0"
                {
                    //if (ObjQry.ReturnInteger("Select count(*) from MStockItems where ShortCode='" + txtShortCode.Text + "'", CommonFunctions.ConStr) != 0)
                    if (ObjQry.ReturnInteger("Select Count(*) FROM MStockBarcode INNER JOIN MStockItems ON MStockBarcode.ItemNo = MStockItems.ItemNo where (Barcode = '" + txtShortCode.Text.Trim().Replace("'", "''") + "' or MStockItems.ShortCode='" + txtShortCode.Text.Trim().Replace("'", "''") + "') AND MStockItems.ItemNo<>" + ID + "", CommonFunctions.ConStr) != 0)
                    {
                        EP.SetError(txtShortCode, "Duplicate Short Code");
                        EP.SetIconAlignment(txtShortCode, ErrorIconAlignment.MiddleRight);
                        if (flag) { flag = false; txtShortCode.Focus(); }
                    }
                }
                if (txtMRP.Text.Trim() == "")
                {
                    EP.SetError(txtMRP, "Enter MRP");
                    EP.SetIconAlignment(txtMRP, ErrorIconAlignment.MiddleRight);
                    if (flag) { flag = false; txtMRP.Focus(); }
                }
                if (Convert.ToDouble(txtMaxLevelQty.Text) < Convert.ToDouble(txtMinLevelQty.Text))
                {
                    EP.SetError(txtMaxLevelQty, "Max Should Be Greater Than Min or Equal To");
                    EP.SetIconAlignment(txtMaxLevelQty, ErrorIconAlignment.MiddleRight);
                    if (flag) { flag = false; txtMaxLevelQty.Focus(); }
                }
                if (FlagBilingual == true)
                {
                    if (txtLangFullDesc.Text.Trim() == "")
                    {
                        EP.SetError(txtLangFullDesc, "Enter Language Full Description");
                        EP.SetIconAlignment(txtLangFullDesc, ErrorIconAlignment.MiddleRight);
                        if (flag) { flag = false; txtLangFullDesc.Focus(); }
                    }
                    //if (txtLangShortDesc.Text.Trim() == "")
                    //{
                    //    EP.SetError(txtLangShortDesc, "Enter Language Short Description");
                    //    EP.SetIconAlignment(txtLangShortDesc, ErrorIconAlignment.MiddleRight);
                    //    if (flag) { flag = false; txtLangShortDesc.Focus(); }
                    //}
                }
                else txtLangFullDesc.Text = ".";
                return flag;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return false;
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            StockItem.RequestItemNo = 0;
            this.Close();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            SetValue();
            btnNewBrand.Visible = false;
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (chkActive.Checked == false) return;
                dbStockItems = new DBMStockItems();
                mStockItems = new MStockItems();

                mStockItems.ItemNo = ID;
                if (OMMessageBox.Show("Are you sure want to delete this record?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (dbStockItems.DeleteMStockItems(mStockItems) == true)
                    {
                        OMMessageBox.Show("Item Deleted Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        FillControls();
                    }
                    else
                    {
                        OMMessageBox.Show("Item not Deleted", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }

                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Form NewF = new StockItem();
                this.Close();
                ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                //nw = 0;
                ID = 0;
                MRPC = 0;
                mStockItems = new MStockItems();
                ObjFunction.InitialiseControl(this.Controls);
                ObjFunction.LockButtons(false, this.Controls);
                ObjFunction.LockControls(true, this.Controls);
                btnLangLongDesc.Enabled = true;
                btnLangShortDesc.Enabled = true;

                isVatSales = 0;
                isVatPur = 0;
                GstTaxPercentNo_Sales = 0;
                FillComboAllMasters();

                chkActive.Checked = true;

                if (cmbDepartmentName.Items.Count == 2) cmbDepartmentName.SelectedIndex = 1;
                if (cmbCategoryName.Items.Count == 2) cmbCategoryName.SelectedIndex = 1;
                if (cmbStockLocationName.Items.Count == 2) cmbStockLocationName.SelectedIndex = 1;
                if (cmbGroupNo1.Items.Count == 2) cmbGroupNo1.SelectedIndex = 1;
                if (cmbGroupNo2.Items.Count == 2) cmbGroupNo2.SelectedIndex = 1;

                if (cmbDepartmentName.Visible == false) cmbDepartmentName.SelectedValue = "1";
                if (cmbCategoryName.Visible == false) cmbCategoryName.SelectedValue = "1";
                cmbStockDept.SelectedValue = 10;
                // ObjFunction.FillCombo(cmbCategory, "Select StockGRoupNo,StockGroupName From MStockGRoup Where IsActive='True' AND ControlGroup=2 ");
                cmbStockDept_KeyDown(cmbStockDept, new KeyEventArgs(Keys.Enter));
                cmbGroupNo2.SelectedValue = 8;
                //if (cmbStockLocationName.Visible == false) cmbStockLocationName.SelectedValue = "1";


                //cmbGroupNo2.Focus();
                txtBarcobe.Focus();
                BarcodeNm = "";
                ItemNm = "";
                ShortCodeNm = "";
                PkSrNo = 0;
                txtSearchBarCode.Enabled = false;
                //txtLanguage.Enabled = false;
                PurRate = 0; MRP = 0; ASaleRate = 0; BSaleRate = 0;
                FlagChange = false;
                FromDate = Convert.ToDateTime("01-01-1900");
                btnNewBrand.Visible = true;
                btnSalesVat.Enabled = true;
                btnPurVat.Enabled = true;
                btnAddGST.Enabled = true;

                txtPurRate.Text = "0";
                long tempUom = ObjQry.ReturnLong("Select UomNo from MUom where UomName='Nos'", CommonFunctions.ConStr);
                if (tempUom != 0)
                    cmbUom.SelectedValue = tempUom.ToString();
                else
                    cmbUom.SelectedIndex = 0;

                lblManufacturerCompanyName.Visible = false;
                lblmsg.Visible = false;
                txtHamali.Text = "";
                //pnlHamli.Visible = false;
                txtMargin.Enabled = false;

                cmbDiscountType.SelectedValue = ObjFunction.GetAppSettings(AppSettings.S_DefaultDiscountType);
                cmbGodownNo.SelectedValue = ObjFunction.GetAppSettings(AppSettings.S_DefaultStockLocation);
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_BarCodeDisplay)) == false)
                {
                    txtBarcobe.Text = txtShortCode.Text = ObjQry.ReturnLong("Select Max(ItemNo)+1000 From MStockItems ", CommonFunctions.ConStr).ToString();
                    cmbGroupNo1.Focus();
                }

                GID = 0;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (ShortID == 0)
            {
                NavigationDisplay(5);

                ObjFunction.LockButtons(true, this.Controls);
                ObjFunction.LockControls(false, this.Controls);
            }
            else
            {
                ShortID = 0;
                this.Close();
            }

            btnLangLongDesc.Enabled = false;
            btnLangShortDesc.Enabled = false;
            btnSalesVat.Enabled = false;
            btnPurVat.Enabled = false;
            btnAddGST.Enabled = false;
            txtSearchBarCode.Enabled = true;
            btnNewBrand.Visible = false;
            lblmsg.Visible = false;
            GID = 0;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                ObjFunction.LockButtons(false, this.Controls);
                ObjFunction.LockControls(true, this.Controls);
                btnLangLongDesc.Enabled = true;
                btnLangShortDesc.Enabled = true;
                btnSalesVat.Enabled = true;
                btnPurVat.Enabled = true;
                btnAddGST.Enabled = true;

                txtSearchBarCode.Enabled = false;

                txtBarcobe.Focus();
                btnNewBrand.Visible = false;
                lblmsg.Visible = false;
                GID = 0;
                txtMargin.Enabled = false;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #region Navigation Methods
        private void NavigationDisplay(int type)
        {
            try
            {
                long No = 0;
                if (dtSearch.Rows.Count > 0)
                {
                    if (type == 5)
                    {

                        No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                        ID = No;
                    }
                    else if (type == 1)
                    {
                        No = Convert.ToInt64(dtSearch.Rows[0].ItemArray[0].ToString());
                        cntRow = 0;
                        ID = No;
                    }
                    else if (type == 2)
                    {
                        No = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                        cntRow = dtSearch.Rows.Count - 1;
                        ID = No;
                    }
                    else
                    {
                        if (type == 3)
                        {
                            cntRow = cntRow + 1;
                        }
                        else if (type == 4)
                        {
                            cntRow = cntRow - 1;
                        }

                        if (cntRow < 0)
                        {
                            OMMessageBox.Show("This is First Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            cntRow = cntRow + 1;
                        }
                        else if (cntRow > dtSearch.Rows.Count - 1)
                        {
                            OMMessageBox.Show("This is Last Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            cntRow = cntRow - 1;
                        }
                        else
                        {
                            No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                            ID = No;
                        }

                    }
                    FillControls();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void SetNavigation()
        {
            cntRow = 0;
            for (int i = 0; i < dtSearch.Rows.Count; i++)
            {
                if (Convert.ToInt64(dtSearch.Rows[i].ItemArray[0].ToString()) == ID)
                {
                    cntRow = i;
                    break;
                }
            }
        }

        private void setDisplay(bool flag)
        {
            btnFirst.Visible = flag;
            btnPrev.Visible = flag;
            btnNext.Visible = flag;
            btnLast.Visible = flag;
            btnDelete.Visible = flag;
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            NavigationDisplay(1);
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            NavigationDisplay(4);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            NavigationDisplay(3);
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            NavigationDisplay(2);
        }

        #endregion

        #region KeyDown Events
        private void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is GroupBox)
                    KeyDownFormat(ctrl.Controls);
            }
        }

        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left && e.Control)
            {
                if (btnPrev.Enabled) btnPrev_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Up && e.Control)
            {
                if (btnFirst.Enabled) btnFirst_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Right && e.Control)
            {
                if (btnNext.Enabled) btnNext_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Down && e.Control)
            {
                if (btnLast.Enabled) btnLast_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F2 || e.KeyCode == Keys.F8)
            {
                if (BtnSave.Visible) BtnSave_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F7)
            {
                if (btnNew.Visible)
                    txtBarcobe.Focus();
            }
            else if (e.KeyCode == Keys.F6)
            {
                if (BtnSave.Visible)
                {
                    chkActive.Checked = true;//!chkActive.Checked;
                    BtnSave_Click(sender, e);
                }
            }
            else if (e.KeyCode == Keys.F5)
            {
                if (BtnSave.Visible) txtMRP.Focus();
            }
            else if (e.KeyCode == Keys.F4)
            {
                if (btnNewBrand.Visible) btnNewBrand_Click(sender, e);
            }
            //else if (e.KeyCode == Keys.Escape)
            //{
            //    BtnExit_Click(sender, e);
            //}
        }
        #endregion

        private void cmbUOM_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {


            }
        }

        private void txtBarcobe_Leave(object sender, EventArgs e)
        {
            try
            {
                EP.SetError(txtBarcobe, "");
                if (txtBarcobe.Text.Trim() != "")
                {
                    if (BarcodeNm != txtBarcobe.Text.Trim())
                    {
                        ItemNm = ""; ItemShort = ""; ShortCodeNm = ""; PkSrNo = 0;
                        FromDate = DBGetVal.ServerTime.Date;
                        ID = ObjQry.ReturnInteger("SELECT ISNULL(MStockItems.ItemNo,0) FROM MStockBarcode INNER JOIN MStockItems ON MStockBarcode.ItemNo = MStockItems.ItemNo WHERE  (MStockBarcode.Barcode = '" + txtBarcobe.Text.Trim().Replace("'", "''") + "') ", CommonFunctions.ConStr);
                        if (ID != 0)
                        {
                            FillControls();
                            txtBarcobe.Focus();

                        }
                        else if (ObjQry.ReturnInteger("Select Count(*) FROM MStockBarcode INNER JOIN MStockItems ON MStockBarcode.ItemNo = MStockItems.ItemNo where (Barcode = '" + txtBarcobe.Text.Trim().Replace("'", "''") + "' or MStockItems.ShortCode='" + txtBarcobe.Text.Trim().Replace("'", "''") + "') AND MStockItems.ItemNo<>" + ID + "", CommonFunctions.ConStr) != 0)
                        {
                            EP.SetError(txtBarcobe, "Duplicate Barcode");
                            EP.SetIconAlignment(txtBarcobe, ErrorIconAlignment.MiddleRight);
                            txtBarcobe.Focus();
                        }
                        else if (txtBarcobe.Text.Trim().Length > 8)
                        {
                            long tempUom = ObjQry.ReturnLong("Select UomNo from MUom where UomName='Nos'", CommonFunctions.ConStr);
                            cmbUom.SelectedValue = tempUom.ToString();
                            txtShortCode.Text = txtBarcobe.Text.Trim();
                        }
                        else
                            txtShortCode.Text = txtBarcobe.Text.Trim();
                    }

                }
                else
                {
                    EP.SetError(txtBarcobe, "Enter barcode.");
                    EP.SetIconAlignment(txtBarcobe, ErrorIconAlignment.MiddleRight);
                    txtBarcobe.Focus();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private bool IsVarchar(string str)
        {
            // Variable to collect the Return value of the TryParse method.
            bool isNum = false;
            for (int i = 0; i < str.Length; i++)
            {
                if ((Convert.ToChar(str[i]) >= 65 && Convert.ToChar(str[i]) <= 90) || (Convert.ToChar(str[i]) >= 97 && Convert.ToChar(str[i]) <= 122) || Convert.ToChar(str[i]) == 32)
                    isNum = true;
                else
                {
                    isNum = false;
                    break;
                }
            }
            return isNum;
        }

        private void txtItemName_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                txtItemName_Leave(sender, new EventArgs());
                txtLangFullDesc.Focus();
            }
        }

        private void txtItemName_Leave(object sender, EventArgs e)
        {
            try
            {
                EP.SetError(txtItemName, "");
                if (txtItemName.Text.Trim() != "")
                {
                    //txtLangFullDesc.Text = "";
                    if (ItemNm != txtItemName.Text.Trim())
                    {
                        if (ObjQry.ReturnInteger("Select Count(*) from MStockItems where ItemName = '" + txtItemName.Text.Trim() + "' " +
                        " AND GroupNo = " + ObjFunction.GetComboValue(cmbGroupNo1) +
                        " AND GroupNo1 = " + ObjFunction.GetComboValue(cmbGroupNo2) +
                        " ", CommonFunctions.ConStr) != 0)
                        {
                            EP.SetError(txtItemName, "Duplicate Item Name");
                            EP.SetIconAlignment(txtItemName, ErrorIconAlignment.MiddleRight);
                            txtItemName.Focus();
                        }
                        else if (FlagBilingual == true)
                        {
                            //txtLangFullDesc.Focus();
                            if (txtLangFullDesc.Text.Trim().Length == 0)
                            {
                                btnLangLongDesc_Click(btnLangLongDesc, null);
                            }
                        }
                        else
                            txtItemShortDesc.Focus();
                        //cmbCompanyName.Focus();
                    }
                    else
                    {
                        if (FlagBilingual == true)
                        {
                            //txtLangFullDesc.Focus();
                            if (txtLangFullDesc.Text.Trim().Length == 0)
                            {
                                btnLangLongDesc_Click(btnLangLongDesc, null);
                            }
                        }
                        else
                            txtItemShortDesc.Focus();
                    }
                }
                else
                {
                    txtItemName.Focus();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void txtItemShortDesc_Leave(object sender, EventArgs e)
        {
            try
            {
                EP.SetError(txtItemShortDesc, "");
                if (txtItemShortDesc.Text.Trim() != "")
                {
                    //txtLangFullDesc.Text = "";
                    if (ItemShort != txtItemShortDesc.Text.Trim())
                    {
                        if (ObjQry.ReturnInteger("Select Count(*) from MStockItems where ItemShortCode = '" + txtItemShortDesc.Text.Trim() + "' " +
                        " AND GroupNo = " + ObjFunction.GetComboValue(cmbGroupNo1) +
                        " AND GroupNo1 = " + ObjFunction.GetComboValue(cmbGroupNo2) +
                        " ", CommonFunctions.ConStr) != 0)
                        {
                            EP.SetError(txtItemShortDesc, "Duplicate Item Short Desc");
                            EP.SetIconAlignment(txtItemShortDesc, ErrorIconAlignment.MiddleRight);
                            txtItemShortDesc.Focus();
                        }
                        else if (FlagBilingual == true)
                        {
                            //txtLangFullDesc.Focus();
                            if (txtLangShortDesc.Text.Trim().Length == 0)
                            {
                                btnLangShortDesc_Click(btnLangShortDesc, null);
                            }
                        }
                        else
                            cmbGodownNo.Focus();
                        //cmbCompanyName.Focus();
                    }
                    else
                    {
                        if (FlagBilingual == true)
                        {
                            //txtLangFullDesc.Focus();
                            if (txtLangShortDesc.Text.Trim().Length == 0)
                            {
                                btnLangShortDesc_Click(btnLangShortDesc, null);
                            }
                        }
                        else
                            cmbStockDept.Focus();
                    }
                }
                else
                {
                    cmbStockDept.Focus();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void chkActive_CheckedChanged(object sender, EventArgs e)
        {
            if (chkActive.Checked == true)
                chkActive.Text = "Yes";
            else
                chkActive.Text = "No";
        }

        private void cmbLowerUOM_Leave(object sender, EventArgs e)
        {
            MovetoNext move2n = new MovetoNext(SetFoucs);
            BeginInvoke(move2n, new object[] { 7 });
        }

        private void FillLanguageGroupName()
        {
            try
            {
                lblManufacturerCompanyName.Visible = true;
                lblManufacturerCompanyName.Text = "Company Name : " + ObjQry.ReturnString(" SELECT   MManufacturerCompany.MfgCompName FROM   MStockGroup INNER JOIN " +
                                                                                  " MManufacturerCompany ON MStockGroup.MfgCompNo = MManufacturerCompany.MfgCompNo " +
                                                                                  " WHERE  (MStockGroup.StockGroupNo = " + ObjFunction.GetComboValue(cmbGroupNo1) + ")", CommonFunctions.ConStr);
                txtLanguage.Text = ObjQry.ReturnString(" select LanguageName from MStockGroup where StockGroupNo=" + ObjFunction.GetComboValue(cmbGroupNo1) + "", CommonFunctions.ConStr);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbLowerUOM_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                MovetoNext move2n = new MovetoNext(SetFoucs);
                BeginInvoke(move2n, new object[] { 7 });
            }
        }

        private delegate void MovetoNext(int type);

        private void SetFoucs(int i)
        {
            switch (i)
            {
                case 0:
                    if (cmbDepartmentName.Visible == true)
                    {
                        cmbDepartmentName.Focus();
                    }
                    else if (cmbCategoryName.Visible == true)
                    {
                        cmbCategoryName.Focus();
                    }
                    else if (cmbDepartmentName.Visible == true)
                    {
                        cmbDepartmentName.Focus();

                    }
                    else if (txtBarcobe.Visible == true)
                    {
                        txtBarcobe.Focus();
                    }
                    else
                    {
                        txtShortCode.Focus();
                        //txtMinLevelQty.Focus();
                    }
                    break;
                case 1:

                    if (cmbCategoryName.Visible == true)
                    {
                        cmbCategoryName.Focus();
                    }
                    else if (txtBarcobe.Visible == true)
                    {
                        txtBarcobe.Focus();
                    }
                    else if (cmbStockLocationName.Visible == true)
                    {
                        cmbStockLocationName.Focus();
                    }

                    break;
                case 2:
                    if (txtBarcobe.Visible == true)
                    {
                        txtBarcobe.Focus();
                    }
                    else if (cmbStockLocationName.Visible == true)
                    {
                        cmbStockLocationName.Focus();
                    }
                    else
                    {
                        txtShortCode.Focus();
                        //txtMinLevelQty.Focus();
                    }
                    break;
                case 3:
                    if (cmbStockLocationName.Visible == true)
                    {
                        cmbStockLocationName.Focus();
                    }
                    else
                    {
                        txtShortCode.Focus();
                        //txtMinLevelQty.Focus();
                    }
                    break;

                case 4:
                    txtMinLevelQty.Focus();
                    break;

                case 6:
                    txtMaxLevelQty.Focus();
                    break;
                case 7:

                    txtMRP.Focus();
                    break;


            }
        }

        #region ColumnIndex
        private static class ColIndex
        {
            public static int Sr = 0;
            public static int Date = 1;
            public static int BarCode = 2;
            public static int MRP = 3;
            public static int UOM = 4;
            public static int ASaleRate = 5;
            public static int BSaleRate = 6;
            public static int CSaleRate = 7;
            public static int DSaleRate = 8;
            public static int ESaleRate = 9;
            public static int MKTQty = 10;
            public static int PurRate = 11;
            public static int RateVariation = 12;
            public static int Chk = 13;
            public static int StockConversion = 14;
            public static int PkSrNo = 15;
            public static int BarCodeNo = 16;
            public static int IsActive = 17;
            public static int HidChk = 18;
        }
        #endregion

        private void cmbDepartmentName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (ObjFunction.GetComboValue(cmbDepartmentName) == 0)
                {
                    cmbDepartmentName.SelectedValue = 10;
                }
                cmbUom.Focus();
            }

        }

        private void cmbCategoryName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                MovetoNext move2n = new MovetoNext(SetFoucs);
                BeginInvoke(move2n, new object[] { 8 });
            }
        }

        private void txtBarcobe_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //e.SuppressKeyPress = true;

                //if (ObjFunction.GetComboValue(cmbStockDept) > 0)
                //{
                //    cmbStockDept.Focus();
                //}
                //else
                //    cmbStockDept.Focus();
                //cmbGroupNo1.Focus();
                if (txtBarcobe.Text.Trim() == "")
                {
                    txtBarcobe.Text = (ObjQry.ReturnLong("Select Max(Barcode) From MStockBarcode  Where  Len(Barcode)<7", CommonFunctions.ConStr) + 1).ToString();
                }
                txtShortCode.Focus();
            }
        }

        private void cmbStockLocationName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                EP.SetError(cmbStockLocationName, "");
                if (ObjFunction.GetComboValue(cmbStockLocationName) <= 0)
                {
                    EP.SetError(cmbStockLocationName, "Select Stock Location Name");
                    cmbStockLocationName.Focus();

                }
                else
                {
                    cmbGroupNo1.Focus();
                    //MovetoNext move2n = new MovetoNext(SetFoucs);
                    //BeginInvoke(move2n, new object[] { 3 });
                }
            }
        }

        private void txtShortName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                cmbCategoryName.Focus();
                //MovetoNext move2n = new MovetoNext(SetFoucs);
                //BeginInvoke(move2n, new object[] { 0 });
            }

        }

        private void txtMinLevelQty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                EP.SetError(txtMinLevelQty, "");
                if (txtMinLevelQty.Text == "")
                {
                    txtMinLevelQty.Text = "0.00";
                    MovetoNext move2n = new MovetoNext(SetFoucs);
                    BeginInvoke(move2n, new object[] { 6 });
                    //MovetoNext move2n = new MovetoNext(SetFoucs);
                    //BeginInvoke(move2n, new object[] { 5 });
                }
                else if (ObjFunction.CheckValidAmount(txtMinLevelQty.Text) == false)
                {
                    EP.SetError(txtMinLevelQty, "Enter Valid Qty");
                    EP.SetIconAlignment(txtMinLevelQty, ErrorIconAlignment.MiddleRight);
                    MovetoNext move2n = new MovetoNext(SetFoucs);
                    BeginInvoke(move2n, new object[] { 4 });
                }
                else
                {

                    MovetoNext move2n = new MovetoNext(SetFoucs);
                    BeginInvoke(move2n, new object[] { 6 });
                    //MovetoNext move2n = new MovetoNext(SetFoucs);
                    //BeginInvoke(move2n, new object[] { 5 });
                }

            }
        }

        private void txtMaxLevelQty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                EP.SetError(txtMaxLevelQty, "");
                e.SuppressKeyPress = true;
                if (txtMaxLevelQty.Text == "")
                {
                    txtMaxLevelQty.Text = "0.00";
                    MovetoNext move2n = new MovetoNext(SetFoucs);
                    BeginInvoke(move2n, new object[] { 5 });
                }

                if (ObjFunction.CheckValidAmount(txtMaxLevelQty.Text) == false)
                {
                    EP.SetError(txtMaxLevelQty, "Enter Valid Qty");
                    EP.SetIconAlignment(txtMaxLevelQty, ErrorIconAlignment.MiddleRight);
                    txtMaxLevelQty.Focus();
                }
                else if (Convert.ToDouble(txtMaxLevelQty.Text) < Convert.ToDouble(txtMinLevelQty.Text))
                {
                    EP.SetError(txtMaxLevelQty, "Max Should Be Greater Than Min or Equal To");
                    EP.SetIconAlignment(txtMaxLevelQty, ErrorIconAlignment.MiddleRight);
                    txtMaxLevelQty.Focus();
                }
                else
                {
                    cmbUom.Focus();
                }
            }
        }

        private void chkFix_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                cmbVatSales.Focus();
            }
        }

        private void SearchBarcode()
        {
            try
            {
                string str = ""; long INo = 0;
                str = "SELECT ItemNo FROM MStockBarcode WHERE (ItemNo <> 1) AND (Barcode LIKE '" + txtSearchBarCode.Text.Trim().Replace("'", "''") + "')";
                INo = ObjQry.ReturnLong(str, CommonFunctions.ConStr);
                if (INo != 0)
                {
                    ItemNm = "";
                    ItemShort = "";
                    BarcodeNm = "";
                    ShortCodeNm = "";
                    PkSrNo = 0;
                    dtSearch = ObjFunction.GetDataView("Select ItemNo From MStockItems_V(NULL,NULL,NULL,NULL,NULL,NULL,NULL) WHERE ItemNo<>1  ORDER BY ItemNo").Table;
                    StockItem.RequestItemNo = INo;
                    if (dtSearch.Rows.Count > 0)
                    {
                        if (StockItem.RequestItemNo == 0)
                            ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                        else
                            ID = StockItem.RequestItemNo;
                        FillControls();
                        SetNavigation();
                    }

                    setDisplay(true);
                    btnNew.Focus();
                    txtSearchBarCode.Text = "";
                    txtSearchBarCode.Enabled = true;
                }
                else
                {
                    str = "SELECT ItemNo FROM MStockItems WHERE (ItemNo <> 1) AND (ShortCode LIKE '" + txtSearchBarCode.Text.Trim().Replace("'", "''") + "')";
                    INo = ObjQry.ReturnLong(str, CommonFunctions.ConStr);

                    if (INo != 0)
                    {
                        ItemNm = "";
                        ItemShort = "";
                        BarcodeNm = "";
                        ShortCodeNm = "";
                        PkSrNo = 0;

                        dtSearch = ObjFunction.GetDataView("Select ItemNo From MStockItems_V(NULL,NULL,NULL,NULL,NULL,NULL,NULL) WHERE ItemNo<>1 ORDER BY ItemName").Table;
                        StockItem.RequestItemNo = INo;
                        if (dtSearch.Rows.Count > 0)
                        {
                            if (StockItem.RequestItemNo == 0)
                                ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                            else
                                ID = StockItem.RequestItemNo;
                            FillControls();
                            SetNavigation();
                        }

                        setDisplay(true);
                        btnNew.Focus();
                        txtSearchBarCode.Text = "";
                        txtSearchBarCode.Enabled = true;
                    }
                    else
                    {
                        OMMessageBox.Show("BarCode does'nt exist", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        txtSearchBarCode.Focus();
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void txtSearchBarCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (btnNew.Visible)
                    SearchBarcode();
            }
        }

        private void btnNew_VisibleChanged(object sender, EventArgs e)
        {
            txtSearchBarCode.Enabled = btnNew.Visible;
        }

        private void StockItemAE_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F7)
            {
                txtSearchBarCode.Focus();
            }
        }

        private void cmbVatPurchase_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    if (e.KeyCode == Keys.Enter)
                    {
                        EP.SetError(cmbVatPurchase, "");
                        e.SuppressKeyPress = true;
                        if (ObjFunction.GetComboValue(cmbVatPurchase) == 0)
                        {
                            EP.SetError(cmbVatPurchase, "Select Purchase Tax");
                            cmbVatPurchase.Focus();
                        }
                    }

                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbStockDept_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    cmbStockDept_Leave(sender, new EventArgs());
                    if (ObjFunction.GetComboValue(cmbStockDept) > 0) cmbGroupNo2.Focus();

                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbStockDept_Leave(object sender, EventArgs e)
        {
            try
            {
                //cmbStockDept_KeyDown(sender, new KeyEventArgs(Keys.Enter));

                long iselected = ObjFunction.GetComboValue(cmbStockDept);
                //  long iselected = ObjFunction.GetComboValue(cmbGroupNo2);
                long grpNo = ObjFunction.GetComboValue(cmbGroupNo2);
                if (iselected != 0)
                {
                    ObjFunction.FillCombo(cmbGroupNo2, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=2 AND ControlSubGroup=" + ObjFunction.GetComboValue(cmbStockDept) + " ORDER BY StockGroupName");
                    cmbGroupNo2.SelectedValue = grpNo;
                    //cmbGroupNo2.Focus();
                }
                //else
                // cmbStockDept.Focus();
                //else if (ObjFunction.GetComboValue(cmbStockDept) > 0)
                //cmbGroupNo2.Focus();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbGroupNo2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                cmbGroupNo2_Leave(sender, new EventArgs());
                if (ObjFunction.GetComboValue(cmbGroupNo2) > 0) txtMinLevelQty.Focus();//txtItemName.Focus();//cmbGroupNo1.Focus();

            }
        }

        private void cmbGroupNo2_Leave(object sender, EventArgs e)
        {
            long iselected = ObjFunction.GetComboValue(cmbGroupNo2);
            long Grpno = ObjFunction.GetComboValue(cmbGroupNo1);
        }

        private void cmbGroupNo1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                cmbGroupNo1_Leave(sender, new EventArgs());
                if (ObjFunction.GetComboValue(cmbGroupNo1) > 0)
                {
                    FillLanguageGroupName();
                    txtItemName.Focus();
                }//cmbStockDept.Focus();
            }
        }

        private void cmbGroupNo1_Leave(object sender, EventArgs e)
        {
            //cmbGroupNo1_KeyDown(sender, new KeyEventArgs(Keys.Enter));
            FillLanguageGroupName();
            if (ObjFunction.GetComboValue(cmbGroupNo1) <= 0)
            {
                cmbGroupNo1.Focus();
                lblManufacturerCompanyName.Text = "";
            }
            //else
            //    txtItemName.Focus();
        }

        private void txtItemShortDesc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (txtItemShortDesc.Text == "")
                    txtItemShortDesc.Text = txtItemName.Text.Trim();
                txtItemShortDesc_Leave(sender, new EventArgs());

                if (cmbGodownNo.Visible == true)
                    cmbGodownNo.Focus();
                else if (cmbDiscountType.Visible == true)
                    cmbDiscountType.Focus();
                else if (cmbDepartmentName.Visible == true)
                    cmbDepartmentName.Focus();
            }
        }

        private void chkActive_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                txtHSNCode.Focus();
            }
        }

        private void btnNewBrand_Click(object sender, EventArgs e)
        {
            try
            {

                if (ObjFunction.CheckAllowMenu(146) == false) return;
                Form NewF = new Master.BrandAE(ObjFunction.GetComboValue(cmbStockDept), ObjFunction.GetComboValue(cmbGroupNo2));
                ObjFunction.OpenForm(NewF);

                if (((Master.BrandAE)NewF).ShortID != 0)
                {
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_IsBrandFilter)) == true)
                        ObjFunction.FillCombo(cmbGroupNo1, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE ControlGroup=3 AND ControlSubGroup=" + ObjFunction.GetComboValue(cmbGroupNo2) + " ORDER BY StockGroupName");//IsActive = 'True' AND
                    else
                        ObjFunction.FillCombo(cmbGroupNo1, "SELECT StockGroupNo,StockGroupName From MStockGroup WHERE ControlGroup=3 ORDER BY StockGroupName");//IsActive = 'True' AND
                    if (((Master.BrandAE)NewF).ShortID > 0)
                    {
                        cmbGroupNo1.SelectedValue = ((Master.BrandAE)NewF).ShortID;
                        FillLanguageGroupName();
                    }
                    else
                        cmbGroupNo1.SelectedValue = 0;
                    cmbGroupNo1.Focus();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void txtMasked_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(((TextBox)sender), 2, 9, JitFunctions.MaskedType.NotNegative);
        }

        private void txtLangShortDesc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                //txtShortCode.Focus();
                //txtMinLevelQty.Focus();
                // cmbStockDept.Focus();
                cmbGodownNo.Focus();
            }
        }

        private void btnLangLongDesc_Click(object sender, EventArgs e)
        {
            try
            {
                Utilities.KeyBoard frmkb;
                if (txtLangFullDesc.Text.Trim().Length > 0)
                {
                    //string val = ObjFunction.ChecklLangVal(txtItemName.Text.Trim());
                    //if (val == "")
                    //{
                    frmkb = new Utilities.KeyBoard(1, txtItemName.Text.Trim(), txtLangFullDesc.Text, "", "");
                    ObjFunction.OpenForm(frmkb);
                    if (frmkb.DS == DialogResult.OK)
                    {
                        txtLangFullDesc.Text = frmkb.strLanguage.Trim();
                        frmkb.Close();
                    }
                    //}
                    //else
                    //    txtLangFullDesc.Text = val;
                }
                else
                {
                    string val = ObjFunction.ChecklLangVal(txtItemName.Text.Trim());
                    if (val == "")
                    {
                        frmkb = new Utilities.KeyBoard(4, txtItemName.Text.Trim(), txtLangFullDesc.Text, "", "");
                        ObjFunction.OpenForm(frmkb);
                        if (frmkb.DS == DialogResult.OK)
                        {
                            txtLangFullDesc.Text = frmkb.strLanguage.Trim();
                            frmkb.Close();
                        }
                    }
                    else
                        txtLangFullDesc.Text = val;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnLangShortDesc_Click(object sender, EventArgs e)
        {
            try
            {
                Utilities.KeyBoard frmkb;
                if (txtLangShortDesc.Text.Trim().Length > 0)
                {
                    //string val = ObjFunction.ChecklLangVal(txtItemShortDesc.Text.Trim());
                    //if (val == "")
                    //{
                    frmkb = new Utilities.KeyBoard(1, txtItemShortDesc.Text, txtLangShortDesc.Text, "", "");
                    ObjFunction.OpenForm(frmkb);
                    if (frmkb.DS == DialogResult.OK)
                    {
                        txtLangShortDesc.Text = frmkb.strLanguage.Trim();
                        frmkb.Close();
                    }
                    //}
                    //else
                    //    txtLangShortDesc.Text = val;

                }
                else
                {
                    string val = ObjFunction.ChecklLangVal(txtItemShortDesc.Text.Trim());
                    if (val == "")
                    {
                        frmkb = new Utilities.KeyBoard(4, txtItemShortDesc.Text, txtLangShortDesc.Text, "", "");
                        ObjFunction.OpenForm(frmkb);
                        if (frmkb.DS == DialogResult.OK)
                        {
                            txtLangShortDesc.Text = frmkb.strLanguage.Trim();
                            frmkb.Close();
                        }
                    }
                    else
                        txtLangShortDesc.Text = val;

                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void StockItemSAE_Deactivate(object sender, EventArgs e)
        {
            isDoProcess = true;
        }

        private void txtMaxLevelQty_Leave(object sender, EventArgs e)
        {
            //EP.SetError(txtMaxLevelQty, "");
            //if (Convert.ToDouble(txtMaxLevelQty.Text) < Convert.ToDouble(txtMinLevelQty.Text))
            //{
            //    EP.SetError(txtMaxLevelQty, "Max Should Be Greater Than Min or Equal To");
            //    EP.SetIconAlignment(txtMaxLevelQty, ErrorIconAlignment.MiddleRight);
            //    txtMaxLevelQty.Focus();
            //}
            // txtMaxLevelQty_KeyDown(sender, new KeyEventArgs(Keys.Enter));
        }

        private void txtShortCode_Leave(object sender, EventArgs e)
        {
            try
            {
                EP.SetError(txtShortCode, "");
                if (txtShortCode.Text != "")
                {
                    if (ShortCodeNm != txtShortCode.Text)//&& txtShortCode.Text.Trim() != "0"
                    {
                        if (ObjQry.ReturnInteger("Select Count(*) FROM MStockBarcode INNER JOIN MStockItems ON MStockBarcode.ItemNo = MStockItems.ItemNo where (ShortCode = '" + txtShortCode.Text.Trim() + "'  or MStockBarcode.Barcode='" + txtShortCode.Text.Trim() + "') AND MStockItems.ItemNo<>" + ID + "", CommonFunctions.ConStr) != 0)
                        {
                            EP.SetError(txtShortCode, "Duplicate Short Code");
                            EP.SetIconAlignment(txtShortCode, ErrorIconAlignment.MiddleRight);
                            txtShortCode.Focus();
                        }
                        else
                            cmbGroupNo1.Focus();
                    }

                }
                else
                {
                    EP.SetError(txtShortCode, "Enter Short Code.");
                    EP.SetIconAlignment(txtShortCode, ErrorIconAlignment.MiddleRight);
                    txtShortCode.Focus();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnPrintBarCode_Click(object sender, EventArgs e)
        {
            if (ID != 0)
            {
                pnlBarCodePrint.Visible = true;
                txtNoOfPrint.Text = "";
                txtStartNo.Text = "0";
                txtwt.Text = "";
                txtExpDays.Text = "";
                txtNoOfPrint.Enabled = true;
                txtStartNo.Enabled = true;
                rbBigMod.Enabled = true;
                rbSmallMode.Enabled = true;
                txtwt.Enabled = true;
                txtExpDays.Enabled = true;
                dtpPackingDate.Enabled = true;
                dtpPackingDate.Value = DBGetVal.ServerTime.Date;
                DateChange();
                txtNoOfPrint.Focus();
            }
        }

        private void btnOKPrintBarCode_Click(object sender, EventArgs e)
        {
            try
            {
                DBTBarCodePrint dbBarCodePrint = new DBTBarCodePrint();
                TBarCodePrint tBarCodePrint = new TBarCodePrint();
                tBarCodePrint.MacNo = DBGetVal.MacNo;
                tBarCodePrint.UserID = DBGetVal.UserID;
                dbBarCodePrint.DeleteTBarCodePrint(tBarCodePrint);

                tBarCodePrint = new TBarCodePrint();
                tBarCodePrint.PkSrNo = 0;
                tBarCodePrint.ItemNo = ID;
                tBarCodePrint.FKRateSettingNo = PkSrNo;
                tBarCodePrint.Quantity = Convert.ToInt64(txtNoOfPrint.Text);


                tBarCodePrint.MacNo = DBGetVal.MacNo;
                tBarCodePrint.UserID = DBGetVal.UserID;
                dbBarCodePrint.AddTBarCodePrint(tBarCodePrint);
                dbBarCodePrint.ExecuteNonQueryStatements();

                string[] ReportSession;
                ReportSession = new string[7];
                //ReportSession[0] = ID.ToString();
                //ReportSession[1] = txtNoOfPrint.Text;
                ReportSession[0] = "1";
                ReportSession[1] = txtStartNo.Text;
                ReportSession[2] = DBGetVal.MacNo.ToString();
                ReportSession[3] = DBGetVal.UserID.ToString();
                ReportSession[4] = txtwt.Text.Trim();
                ReportSession[5] = Convert.ToDateTime(dtpPackingDate.Value).ToString();
                ReportSession[6] = lblExpDate.Text.Trim();

                if (OMMessageBox.Show("Do you want Preview of barcode?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Form NewF;
                    if (rbBigMod.Checked == true)
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            NewF = new Display.ReportViewSource(new Reports.BarCodePrintBig(), ReportSession);
                        else
                            NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("BarCodePrintBig.rpt", CommonFunctions.ReportPath), ReportSession);
                        ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                    }
                    else if (rbSmallMode.Checked == true)
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            NewF = new Display.ReportViewSource(new Reports.BarCodePrint(), ReportSession);
                        else
                            NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("BarCodePrint.rpt", CommonFunctions.ReportPath), ReportSession);
                        ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                    }
                }
                else
                {
                    CrystalDecisions.CrystalReports.Engine.ReportDocument childForm = null;
                    if (rbBigMod.Checked == true)
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            childForm = new Reports.BarCodePrintBig();
                        else
                            childForm = ObjFunction.LoadReportObject("BarCodePrintBig.rpt", CommonFunctions.ReportPath);
                    }
                    else if (rbSmallMode.Checked == true)
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            childForm = new Reports.BarCodePrint();
                        else

                            childForm = ObjFunction.LoadReportObject("BarCodePrint.rpt", CommonFunctions.ReportPath);
                    }

                    if (childForm != null)
                    {
                        DBReportGenerate objRpt = new DBReportGenerate(childForm, ReportSession);
                        if (objRpt.PrintReport() == true)
                        {
                            OMMessageBox.Show("Printing barCode sucessfully...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        }
                        else
                        {
                            OMMessageBox.Show("Barcode not Print...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        OMMessageBox.Show("Barcode Print report not exist...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                }
                //dbBarCodePrint.DeleteTBarCodePrint(tBarCodePrint);
                pnlBarCodePrint.Visible = false;
                btnNew.Focus();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnCancelPrintBarcode_Click(object sender, EventArgs e)
        {
            pnlBarCodePrint.Visible = false;
            btnNew.Focus();
        }

        private void txtStartNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                //btnOKPrintBarCode.Focus();
                txtwt.Focus();
            }
        }

        private void txtExpDays_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(txtExpDays, -1, 10, JitFunctions.MaskedType.NotNegative);
        }

        private void dtpPackingDate_ValueChanged(object sender, EventArgs e)
        {
            DateChange();
        }

        private void txtExpDays_Leave(object sender, EventArgs e)
        {
            DateChange();
        }

        private void dtpPackingDate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                DateChange();
            }
        }

        private void DateChange()
        {
            DateTime dt;
            if (txtExpDays.Text.Trim() != "")
            {
                dt = Convert.ToDateTime(dtpPackingDate.Value).AddDays(Convert.ToInt64(txtExpDays.Text.Trim()));
                lblExpDate.Text = Convert.ToDateTime(dt).ToString("dd-MMM-yyyy");
            }
            else
            {
                txtExpDays.Text = "0";
                lblExpDate.Text = Convert.ToDateTime(dtpPackingDate.Value).ToString("dd-MMM-yyyy");
            }
        }

        private void txtExpDays_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                rbBigMod.Focus();
            }
        }

        private void rbBigMod_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (rbBigMod.Checked == false)
                    rbSmallMode.Focus();
                else
                    btnOKPrintBarCode.Focus();
            }
        }

        private void rbSmallMode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (rbSmallMode.Checked == false)
                    rbBigMod.Focus();
                else
                    btnOKPrintBarCode.Focus();
            }
        }

        private void txtNoOfPrint_Leave(object sender, EventArgs e)
        {
            if (txtNoOfPrint.Text.Trim() == "")
                txtNoOfPrint.Text = "1";
        }

        private void txtStartNo_Leave(object sender, EventArgs e)
        {
            if (txtStartNo.Text.Trim() == "")
                txtStartNo.Text = "0";
        }



        private void btnPurVat_Click(object sender, EventArgs e)
        {
            /*13-Apr-2014
            * Exception Handling Purpose.
			* Pur vat open the form
            * */
            try
            {
                long PurVatNo = 0;
                Form NewF = new Master.PercentageVAT(GroupType.PurchaseAccount, GroupType.VAT);
                ObjFunction.OpenForm(NewF);
                if (((PercentageVAT)NewF).DS == DialogResult.OK)
                {
                    PurVatNo = ObjFunction.GetComboValue(cmbVatPurchase);
                    ObjFunction.FillCombo(cmbVatPurchase, "SELECT MItemTaxSetting.PkSrNo, (cast(MItemTaxSetting.Percentage as varchar)+ ' %') as Percentage FROM MItemTaxSetting INNER JOIN MLedger ON MItemTaxSetting.SalesLedgerNo = MLedger.LedgerNo INNER JOIN " +
                       "   MLedger AS MLedger_1 ON MItemTaxSetting.TaxLedgerNo = MLedger_1.LedgerNo " +
                       " WHERE     (MLedger.GroupNo = " + GroupType.PurchaseAccount + ") AND (MLedger_1.GroupNo = " + GroupType.VAT + ") And MItemTaxSetting.IsActive='True' or MItemTaxSetting.PkSrNo=" + isVatPur + " Order by  MItemTaxSetting.Percentage ");
                    cmbVatPurchase.SelectedValue = PurVatNo;
                }
                cmbVatPurchase.Focus();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message); ;//For Common Error Displayed Purpose
            }
        }

        private void txtNoOfPrint_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMaskedNumeric((TextBox)sender);
        }

        private void txtStartNo_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMaskedNumeric((TextBox)sender);
        }

        private void txtLangFullDesc_Leave(object sender, EventArgs e)
        {
            if (txtLangFullDesc.Text.Trim() == "")
                txtLangFullDesc.Text = ObjFunction.ChecklLangVal(txtItemName.Text);
        }

        private void txtLangShortDesc_Leave(object sender, EventArgs e)
        {
            if (txtLangShortDesc.Text.Trim() == "")
                txtLangShortDesc.Text = ObjFunction.ChecklLangVal(txtItemShortDesc.Text);
        }

        private void txtFactor_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(((TextBox)sender), 3, 11, JitFunctions.MaskedType.NotNegative);
        }

        private void btnPrintBarCodeAdvance_Click(object sender, EventArgs e)
        {
            if (ID != 0)
            {
                Form frmChild = new Settings.BarcodePrinting(ID, PkSrNo, txtMRP.Text, txtBSaleRate.Text, cmbGroupNo1.Text, StrShowItemName, txtBarcobe.Text, cmbGroupNo1.Text + "" + txtItemName.Text);
                ObjFunction.OpenForm(frmChild);

            }
        }

        private void txtMargin2_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(((TextBox)sender), 2, 4, JitFunctions.MaskedType.NotNegative);
        }

        private void txtPurRate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                chkActive.Focus();
            }
        }

        private void cmbGroupNo2_SelectedIndexChanged(object sender, EventArgs e)
        {
            long iselected = ObjFunction.GetComboValue(cmbGroupNo2);
            txtMargin.Text = ObjQry.ReturnDouble("Select Margin from MStockGroup where StockGroupNo = " + iselected + "", CommonFunctions.ConStr).ToString("0.00");
        }

        private void cmbGodownNo_Leave(object sender, EventArgs e)
        {
            if (ObjFunction.GetComboValue(cmbGodownNo) != 0 && ObjFunction.GetComboValue(cmbGodownNo) != 2)
            {
                //  pnlHamli.Visible = true;
                txtHamali.Focus();
            }
            else
            {
                //pnlHamli.Visible = false;
                txtHamali.Focus();
            }
        }

        private void cmbGodownNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (ObjFunction.GetComboValue(cmbGodownNo) != 0 && ObjFunction.GetComboValue(cmbGodownNo) != 2)
                {
                    // pnlHamli.Visible = true;
                    if (txtHamali.Text.Trim() == "")
                        txtHamali.Text = "0";

                    txtHamali.Focus();
                }
                else
                {
                    if (txtHamali.Text.Trim() == "")
                        txtHamali.Text = "0";

                    // pnlHamli.Visible = false;
                    txtHamali.Focus();
                }
                if (txtHamali.Visible == true)
                    txtHamali.Focus();
                else if (cmbStockLocationName.Visible == true)
                    cmbStockLocationName.Focus();
                else if (cmbDepartmentName.Visible == true)
                    cmbDepartmentName.Focus();

            }
        }

        private void txtHamali_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (txtHamali.Text.Trim() == "") txtHamali.Text = "0";
                cmbDiscountType.Focus();
            }
        }

        private void txtHamali_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(((TextBox)sender), 2, 9, JitFunctions.MaskedType.NotNegative);
        }

        #region GST

        private void cmbGSTPercent_SelectedValueChanged(object sender, EventArgs e)
        {
            fillGstTaxDetails(ObjFunction.GetComboValue(cmbGSTPercent));
        }

        private void cmbHSNCode_SelectedValueChanged(object sender, EventArgs e)
        {
            string sql = "Select TOP 1 GTD.FkTaxSettingNo " +
                        " From MStockGSTTaxDetails As GTD " +
                        " INNER JOIN MItemTaxSetting As MTS ON MTS.PkSrNo = GTD.FkTaxSettingNo " +
                                " AND MTS.TransactionTypeNo = " + GroupType.SalesAccount + " " +
                        " WHERE GTD.HSNNo = " + ObjFunction.GetComboValue(cmbHSNCode) + " " +
                                " AND GTD.IsActive = 'True' " +
                        " ORDER BY GTD.FromDate";

            long GstTaxDetailNo = ObjQry.ReturnLong(sql, CommonFunctions.ConStr);

            if (GstTaxDetailNo > 0)
            {
                cmbGSTPercent.SelectedValue = GstTaxDetailNo;
                //cmbGSTPercent.Enabled = false;
            }
        }

        private void cmbHSNCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && cmbHSNCode.SelectedIndex == 0)
            {
                e.SuppressKeyPress = true;
                e.Handled = true;
                cmbGSTPercent.Focus();
            }
            else
            {
                BtnSave.Focus();
            }
        }

        private void cmbGSTPercent_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                e.Handled = true;
                BtnSave.Focus();
            }
        }


        #endregion

        private void btnAddGST_Click(object sender, EventArgs e)
        {
            PercentageGST PGST = new PercentageGST();
            DialogResult dr = ObjFunction.OpenDialog(PGST, this);
            if (dr == DialogResult.OK)
            {
                long iselected = 0;

                iselected = ObjFunction.GetComboValue(cmbGSTPercent);
                ObjFunction.FillCombo(cmbGSTPercent, "SELECT MItemTaxSetting.PkSrNo, " +
                          " (cast(MItemTaxSetting.IGSTPercent as varchar)+ ' / ')+" +
                          " (cast(MItemTaxSetting.CGSTPercent as varchar)+ ' / ')+" +
                          " (cast(MItemTaxSetting.SGSTPercent as varchar)+ ' / ')+" +
                          " (cast(MItemTaxSetting.UTGSTPercent as varchar)+ ' / ')+" +
                          " (cast(MItemTaxSetting.CessPercent as varchar))" +
                         " as Percentage " +
                         " FROM MItemTaxSetting " +
                         " WHERE (MItemTaxSetting.TransactionTypeNo = " + GroupType.SalesAccount + ") " +
                             " AND (MItemTaxSetting.TaxTypeNo = " + GroupType.GST + " AND MItemTaxSetting.IsActive='True') " +
                             " OR MItemTaxSetting.PkSrNo = " + GstTaxPercentNo_Sales + " Order by  MItemTaxSetting.IGSTPercent,MItemTaxSetting.CessPercent ");
                cmbGSTPercent.SelectedValue = iselected;
                cmbGSTPercent.DroppedDown = true;


            }
        }

        private void txtHSNCode_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(((TextBox)sender), -1, 8, JitFunctions.MaskedType.NotNegative);

            if (txtHSNCode.TextLength >= 8)
            {
                cmbHSNCode.SelectedIndex = cmbHSNCode.FindStringExact(txtHSNCode.Text);
            }
            else
            {
                if (cmbHSNCode.SelectedIndex != -1)
                    cmbHSNCode.SelectedIndex = 0;
            }
        }

        private void cmbDiscountType_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
            {
                e.SuppressKeyPress = true;
                if (cmbStockLocationName.Visible == true)
                    cmbStockLocationName.Focus();
                else if (cmbDepartmentName.Visible == true)
                    cmbDepartmentName.Focus();
                else
                    cmbUom.Focus();
            }
        }

        private void btnImports_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Excel Files (*.xls)|*.xls";
            ofd.Title = "Please select an Excel file to import Product Master Data.";
            ofd.Multiselect = false;

            if (ofd.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    DBProductImportExcel db = new DBProductImportExcel();
                    db.ImportProduct(ofd.FileName);

                    Kirana.DBClasses.ExportAndImportTally Ex = new Kirana.DBClasses.ExportAndImportTally();
                    ENVELOPE regData = Ex.FromXml(ofd.FileName);

                    foreach (TALLYMESSAGE tmess in regData.BODY.IMPORTDATA.REQUESTDATA.TALLYMESSAGE)
                    {
                        STOCKITEM item = tmess.STOCKITEM;
                        if (item != null)
                        {
                            SaveItems(item);
                        }
                    }

                }
                catch (Exception ex)
                {
                    OMMessageBox.Show(ex.Message);
                }
            }
        }

        private void chkIsQtyRead_CheckedChanged(object sender, EventArgs e)
        {
            if (chkIsQtyRead.Checked == true)
                chkIsQtyRead.Text = "Yes";
            else
                chkIsQtyRead.Text = "No";
        }

        private long GetBrandNo(string StockGroupName)
        {
            long cnt = ObjQry.ReturnLong("Select ISNULL(StockGroupNo,0) From MStockGroup Where StockGroupName='" + StockGroupName + "' ", CommonFunctions.ConStr);
            if (cnt != 0)
                return cnt;

            DBMStockGroup dbMStockGroup = new DBMStockGroup();
            MStockGroup mstockgroup = new MStockGroup();

            mstockgroup.StockGroupName = StockGroupName;
            mstockgroup.ControlGroup = 3;
            mstockgroup.ControlSubGroup = 3;
            mstockgroup.LanguageName = "";
            mstockgroup.IsActive = true;
            mstockgroup.UserDate = DBGetVal.ServerTime;
            mstockgroup.UserId = DBGetVal.UserID;

            if (!dbMStockGroup.AddMStockGroup(mstockgroup))
            {
                throw new Exception("Error while inserting New record in StockGroup Table" + mstockgroup.msg);
            }
            else
            {
                return ObjQry.ReturnLong("Select ISNULL(StockGroupNo,0) From MStockGroup Where StockGroupName='" + StockGroupName + "' ", CommonFunctions.ConStr);
            }


        }

        private long GetUOMNo(string UOMName)
        {
            long cnt = ObjQry.ReturnLong("Select ISNULL(UOMNo,0) From MUOM Where UOMName='" + UOMName + "' ", CommonFunctions.ConStr);
            if (cnt != 0)
                return cnt;

            DBMUOM dbmuom = new DBMUOM();
            MUOM muom = new MUOM();

            muom.UOMName = UOMName;
            muom.UOMShortCode = UOMName;
            muom.UserDate = DateTime.Now;

            if (!dbmuom.AddMUOM(muom))
            {
                throw new Exception("Error while inserting New record in MUOM Table !!! Error Message = " + muom.msg);
            }
            else
            {
                return ObjQry.ReturnLong("Select ISNULL(UOMNo,0) From MUOM Where UOMName='" + UOMName + "' ", CommonFunctions.ConStr);
            }

        }

        private void SaveItems(STOCKITEM item)
        {
            long cnt = ObjQry.ReturnLong("Select Count(*) From MStockItems Where ItemName='" + item.NAME + "' ", CommonFunctions.ConStr);
            if (cnt != 0)
                return;

            dbStockItems = new DBMStockItems();
            #region MStockItems
            MStockItems mStockItems = new MStockItems();
            mStockItems.ItemName = item.NAME;
            mStockItems.ItemShortCode = item.NAME;
            mStockItems.FKStockDeptNo = 10;
            mStockItems.GroupNo = GetBrandNo(item.PARENT);
            mStockItems.GroupNo1 = 8;
            mStockItems.FkCategoryNo = 1;
            mStockItems.UOMDefault = GetUOMNo(item.BASEUNITS);
            mStockItems.UOMPrimary = mStockItems.UOMDefault;
            mStockItems.ShortCode = item.ALTERID;
            mStockItems.LangFullDesc = "";
            mStockItems.LangShortDesc = "";
            mStockItems.FKStockLocationNo = 1;
            mStockItems.FkDepartmentNo = 1;
            mStockItems.IsActive = true;
            mStockItems.IsFixedBarcode = true;
            mStockItems.FkStockGroupTypeNo = 1;
            mStockItems.FactorVal = 1;
            mStockItems.GodownNo = 2;
            mStockItems.DiscountType = 1;
            mStockItems.IsQtyRead = false;
            mStockItems.CompanyNo = 1;
            mStockItems.UserId = DBGetVal.UserID;
            mStockItems.UserDate = DBGetVal.ServerTime;
            dbStockItems.AddMStockItems(mStockItems);
            #endregion

            #region MStockBarcode
            MStockBarcode mStockBarcode = new MStockBarcode();
            mStockBarcode.Barcode = item.ALTERID.Trim();
            mStockBarcode.CompanyNo = 1;
            mStockBarcode.UserID = DBGetVal.UserID;
            mStockBarcode.UserDate = DBGetVal.ServerTime;
            #endregion

            #region MRateSetting
            dbStockItems.AddMStockBarcode(mStockBarcode);

            //need to check multiple MRP found if so insert multiple here
            MRateSetting mRateSetting = new MRateSetting();
            mRateSetting.PkSrNo = 0;
            mRateSetting.UOMNo = mStockItems.UOMDefault;
            mRateSetting.ASaleRate = item.SALESLIST.CLASSRATE;
            mRateSetting.BSaleRate = item.SALESLIST.CLASSRATE;
            mRateSetting.PurRate = item.PURCHASELIST.CLASSRATE;
            mRateSetting.MRP = mRateSetting.ASaleRate;
            mRateSetting.FromDate = Convert.ToDateTime("01-01-1900");
            mRateSetting.StockConversion = 1;
            mRateSetting.MKTQty = 1;
            mRateSetting.IsActive = true;
            mRateSetting.CompanyNo = 1;
            mRateSetting.UserID = DBGetVal.UserID;
            mRateSetting.UserDate = DBGetVal.ServerTime;
            dbStockItems.AddMRateSetting2(mRateSetting);

            #endregion

            #region Save Tax details

            #region GST Tax Details

            #region Fetch GST Tax Details using HSN Code
            string sql;
            string strHSNCode = item.GSTDETAILS.HSNCODE;

            string GSTPercent = "0";
            foreach (RATEDETAILS IGST in item.GSTDETAILS.STATEWISEDETAILS.RATEDETAILS)
            {
                if (IGST.GSTRATEDUTYHEAD == "Integrated Tax")
                    GSTPercent = (IGST.GSTRATE == "") ? "0" : IGST.GSTRATE;
            }
            DataTable dtItemTaxSetting_Sales_Gst = null, dtItemTaxSetting_Purchase_Gst = null;


            sql = "SELECT MItemTaxSetting.* " +
                     " FROM MItemTaxSetting " +
                     " WHERE   MItemTaxSetting.Percentage = " + GSTPercent + " " +
                         " AND MItemTaxSetting.TransactionTypeNo = " + GroupType.SalesAccount + " " +
                         " AND MItemTaxSetting.TaxTypeNo = " + GroupType.GST + " " +
                         " AND MItemTaxSetting.IsActive='True' ";
            dtItemTaxSetting_Sales_Gst = ObjFunction.GetDataView(sql).Table;
            sql = "SELECT MItemTaxSetting.* " +
                        " FROM MItemTaxSetting " +
                        " WHERE   MItemTaxSetting.Percentage = " + GSTPercent + " " +
                            " AND MItemTaxSetting.TransactionTypeNo = " + GroupType.PurchaseAccount + " " +
                            " AND MItemTaxSetting.TaxTypeNo = " + GroupType.GST + " " +
                            " AND MItemTaxSetting.IsActive='True' ";
            dtItemTaxSetting_Purchase_Gst = ObjFunction.GetDataView(sql).Table;

            #endregion

            if (dtItemTaxSetting_Sales_Gst != null && dtItemTaxSetting_Purchase_Gst != null)
            {
                for (int g = 0; g < dtItemTaxSetting_Sales_Gst.Rows.Count; g++)
                {
                    MItemTaxInfo mItemTaxInfo = new MItemTaxInfo();
                    mItemTaxInfo.TaxLedgerNo = Convert.ToInt64(dtItemTaxSetting_Sales_Gst.Rows[g]["TaxLedgerNo"].ToString());
                    mItemTaxInfo.SalesLedgerNo = Convert.ToInt64(dtItemTaxSetting_Sales_Gst.Rows[g]["SalesLedgerNo"].ToString());
                    mItemTaxInfo.Percentage = Convert.ToDouble(dtItemTaxSetting_Sales_Gst.Rows[g]["Percentage"].ToString());
                    mItemTaxInfo.FKTaxSettingNo = Convert.ToInt64(dtItemTaxSetting_Sales_Gst.Rows[g]["PkSrNo"].ToString());
                    mItemTaxInfo.TaxTypeNo = Convert.ToInt64(dtItemTaxSetting_Sales_Gst.Rows[g]["TaxTypeNo"].ToString());
                    mItemTaxInfo.TransactionTypeNo = Convert.ToInt64(dtItemTaxSetting_Sales_Gst.Rows[g]["TransactionTypeNo"].ToString());
                    mItemTaxInfo.HSNCode = strHSNCode;
                    mItemTaxInfo.IsActive = true;
                    mItemTaxInfo.HSNNo = 0;
                    mItemTaxInfo.FromDate = DBGetVal.ServerTime;
                    mItemTaxInfo.IGSTPercent = Convert.ToDouble(dtItemTaxSetting_Sales_Gst.Rows[g]["IGSTPercent"].ToString());
                    mItemTaxInfo.CGSTPercent = Convert.ToDouble(dtItemTaxSetting_Sales_Gst.Rows[g]["CGSTPercent"].ToString());
                    mItemTaxInfo.SGSTPercent = Convert.ToDouble(dtItemTaxSetting_Sales_Gst.Rows[g]["SGSTPercent"].ToString());
                    mItemTaxInfo.UTGSTPercent = Convert.ToDouble(dtItemTaxSetting_Sales_Gst.Rows[g]["UTGSTPercent"].ToString());
                    mItemTaxInfo.CessPercent = Convert.ToDouble(dtItemTaxSetting_Sales_Gst.Rows[g]["CessPercent"].ToString());
                    mItemTaxInfo.UserDate = DBGetVal.ServerTime;
                    mItemTaxInfo.UserID = DBGetVal.UserID;
                    dbStockItems.AddMItemTaxInfo(mItemTaxInfo);
                    if (mItemTaxInfo.FromDate <= DBGetVal.ServerTime.Date)
                    {
                        break;
                    }
                }

                for (int g = 0; g < dtItemTaxSetting_Purchase_Gst.Rows.Count; g++)
                {
                    MItemTaxInfo mItemTaxInfo = new MItemTaxInfo();
                    mItemTaxInfo.TaxLedgerNo = Convert.ToInt64(dtItemTaxSetting_Purchase_Gst.Rows[g]["TaxLedgerNo"].ToString());
                    mItemTaxInfo.SalesLedgerNo = Convert.ToInt64(dtItemTaxSetting_Purchase_Gst.Rows[g]["SalesLedgerNo"].ToString());
                    mItemTaxInfo.Percentage = Convert.ToDouble(dtItemTaxSetting_Purchase_Gst.Rows[g]["Percentage"].ToString());
                    mItemTaxInfo.FKTaxSettingNo = Convert.ToInt64(dtItemTaxSetting_Purchase_Gst.Rows[g]["PkSrNo"].ToString());
                    mItemTaxInfo.TaxTypeNo = Convert.ToInt64(dtItemTaxSetting_Purchase_Gst.Rows[g]["TaxTypeNo"].ToString());
                    mItemTaxInfo.TransactionTypeNo = Convert.ToInt64(dtItemTaxSetting_Purchase_Gst.Rows[g]["TransactionTypeNo"].ToString());
                    mItemTaxInfo.HSNCode = strHSNCode;
                    mItemTaxInfo.FromDate = DBGetVal.ServerTime;
                    mItemTaxInfo.IsActive = true;
                    mItemTaxInfo.UserDate = DBGetVal.ServerTime;
                    mItemTaxInfo.UserID = DBGetVal.UserID;
                    mItemTaxInfo.IGSTPercent = Convert.ToDouble(dtItemTaxSetting_Purchase_Gst.Rows[g]["IGSTPercent"].ToString());
                    mItemTaxInfo.CGSTPercent = Convert.ToDouble(dtItemTaxSetting_Purchase_Gst.Rows[g]["CGSTPercent"].ToString());
                    mItemTaxInfo.SGSTPercent = Convert.ToDouble(dtItemTaxSetting_Purchase_Gst.Rows[g]["SGSTPercent"].ToString());
                    mItemTaxInfo.UTGSTPercent = Convert.ToDouble(dtItemTaxSetting_Purchase_Gst.Rows[g]["UTGSTPercent"].ToString());
                    mItemTaxInfo.CessPercent = Convert.ToDouble(dtItemTaxSetting_Purchase_Gst.Rows[g]["CessPercent"].ToString());
                    dbStockItems.AddMItemTaxInfo(mItemTaxInfo);
                    if (mItemTaxInfo.FromDate <= DBGetVal.ServerTime.Date)
                    {
                        break;
                    }
                }
            }
            #endregion

            #endregion

            if (dbStockItems.ExecuteNonQueryStatements_ImportItemMaster() == false)
            {
                throw new Exception("Items Not Save :" + DBMStockItems.strerrormsg);
            }
        }

    }
}
