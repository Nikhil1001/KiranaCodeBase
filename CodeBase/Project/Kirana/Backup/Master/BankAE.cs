﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Master
{
    /// <summary>
    /// This class is used for Bank AE
    /// </summary>
    public partial class BankAE : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();



        DBMLedger dbLedger = new DBMLedger();
        MLedger mLedger = new MLedger();
        string BankNm;
        DataTable dtSearch = new DataTable();
        int cntRow;
        long ID=0, LedgerUserNo=0;

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public BankAE()
        {
            InitializeComponent();
        }
        private void BankAE_Load(object sender, EventArgs e)
        {
            try
            {
                ObjFunction.FillCombo(cmbSignCode, "Select SignCode,SignName from MSign");

                ObjFunction.LockButtons(true, this.Controls);
                ObjFunction.LockControls(false, this.Controls);

                BankNm = "";
                dtSearch = ObjFunction.GetDataView("Select LedgerNo,LedgerName From MLedger where GroupNo=" + GroupType.BankAccounts + " order by LedgerName").Table;

                if (dtSearch.Rows.Count > 0)
                {
                    if (Bank.RequestBankNo != 0)
                    {
                        ID = Bank.RequestBankNo;
                        BankNm = "";
                    }
                    else
                    {
                        ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString()); ;
                    }
                    FillControls();
                    SetNavigation();
                }

                setDisplay(true);
                btnNew.Focus();
                KeyDownFormat(this.Controls);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillControls()
        {
            try
            {
                EP.SetError(txtBankName, "");
                EP.SetError(cmbSignCode, "");
                EP.SetError(txtContactNo, "");
                EP.SetError(txtOpeningBalance, "");

                mLedger = dbLedger.ModifyMLedgerByID(ID);
                BankNm = mLedger.LedgerName;
                txtBankName.Text = mLedger.LedgerName;
                txtOpeningBalance.Text = mLedger.OpeningBalance.ToString("0.00");
                cmbSignCode.SelectedValue = mLedger.SignCode.ToString();
                txtContactNo.Text = mLedger.ContactPerson;
                LedgerUserNo = Convert.ToInt32(mLedger.LedgerUserNo);
                chkActive.Checked = mLedger.IsActive;
                if (chkActive.Checked == true)
                    chkActive.Text = "Yes";
                else
                    chkActive.Text = "No";
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void SetValue()
        {
            try
            {
                if (Validations() == true)
                {

                    dbLedger = new DBMLedger();
                    mLedger = new MLedger();
                    mLedger.LedgerNo = ID;
                    mLedger.LedgerUserNo = LedgerUserNo.ToString();
                    mLedger.LedgerName = txtBankName.Text.Trim();
                    mLedger.OpeningBalance = Convert.ToDouble(txtOpeningBalance.Text.Trim());
                    mLedger.SignCode = ObjFunction.GetComboValue(cmbSignCode);
                    mLedger.GroupNo = GroupType.BankAccounts;


                    mLedger.InvFlag = false;
                    mLedger.MaintainBillByBill = false;

                    mLedger.ContactPerson = txtContactNo.Text.Trim();
                    mLedger.CompanyNo = DBGetVal.CompanyNo;
                    mLedger.LedgerStatus = (ID == 0) ? 1 : 2;

                    mLedger.IsActive = chkActive.Checked;
                    mLedger.UserID = DBGetVal.UserID;
                    mLedger.UserDate = DBGetVal.ServerTime.Date;

                    dbLedger.AddMLedger(mLedger);

                    if (dbLedger.ExecuteNonQueryStatements() == true)
                    {
                        if (ID == 0)
                        {
                            OMMessageBox.Show("Bank Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            dtSearch = ObjFunction.GetDataView("Select LedgerNo,LedgerName From MLedger where GroupNo=" + GroupType.BankAccounts + " order by LedgerName").Table;
                            ID = ObjQry.ReturnLong("Select Max(LedgerNo) From MLedger", CommonFunctions.ConStr);
                            SetNavigation();
                            FillControls();
                        }
                        else
                        {
                            OMMessageBox.Show("Bank Updated Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            FillControls();
                        }

                        ObjFunction.LockButtons(true, this.Controls);
                        ObjFunction.LockControls(false, this.Controls);


                    }
                    else
                    {
                        OMMessageBox.Show("Bank not saved", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        } 

        private void BtnExit_Click(object sender, EventArgs e)
        {
            Bank.RequestBankNo = 0;
            this.Close();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            SetValue();
        }

        private bool Validations()
        {
            bool flag = false;
            EP.SetError(txtBankName, "");
            EP.SetError(txtOpeningBalance, "");
            EP.SetError(cmbSignCode, "");

            if (txtBankName.Text.Trim() == "")
            {

                EP.SetError(txtBankName, "Enter Ledger Name");
                EP.SetIconAlignment(txtBankName, ErrorIconAlignment.MiddleRight);
                txtBankName.Focus();
            }
            else if (ObjFunction.CheckValidAmount(txtOpeningBalance.Text.Trim()) == false )
            {
                EP.SetError(txtOpeningBalance, "Enter Valid Amount");
                EP.SetIconAlignment(txtOpeningBalance, ErrorIconAlignment.MiddleRight);
                txtOpeningBalance.Focus();
            }
            else if (ObjFunction.GetComboValue(cmbSignCode) <= 0)
            {
                EP.SetError(cmbSignCode, "Select Type");
                EP.SetIconAlignment(cmbSignCode, ErrorIconAlignment.MiddleRight);
                cmbSignCode.Focus();
            }
            //else if ((txtContactNo.Text.Length > 0) && (txtContactNo.Text.Length < 10))
            //{
            //else if ((txtContactNo.Text.Length > 0) && txtContactNo.Text.Length > 10)
            //{
            //        EP.SetError(txtContactNo, "Contact number not correct");
            //        EP.SetIconAlignment(txtContactNo, ErrorIconAlignment.MiddleRight);
            //        txtContactNo.Focus();
            //    //}
            //}
            else if (BankNm != txtBankName.Text.Trim())
            {
                if (ObjQry.ReturnInteger("Select Count(*) from MLedger where LedgerName = '" + txtBankName.Text.Trim().Replace("'", "''") + "'", CommonFunctions.ConStr) != 0)
                {
                    EP.SetError(txtBankName, "Duplicate Ledger Name");
                    EP.SetIconAlignment(txtBankName, ErrorIconAlignment.MiddleRight);
                    txtBankName.Focus();
                }
                else
                    flag = true;
            }
            else
                flag = true;
            return flag;
        }

        private void BankAE_FormClosing(object sender, FormClosingEventArgs e)
        {
            Bank.RequestBankNo = 0;
            BankNm = "";
        }

        private void txtBankName_Leave(object sender, EventArgs e)
        {
            try
            {
                EP.SetError(txtBankName, "");
                if (txtBankName.Text.Trim() != "")
                {
                    if (BankNm != txtBankName.Text.Trim())
                    {
                        if (ObjQry.ReturnInteger("Select Count(*) from MLedger where LedgerName = '" + txtBankName.Text.Trim().Replace("'", "''") + "'", CommonFunctions.ConStr) != 0)
                        {
                            EP.SetError(txtBankName, "Duplicate Ledger Name");
                            EP.SetIconAlignment(txtBankName, ErrorIconAlignment.MiddleRight);
                            txtBankName.Focus();
                        }
                    }

                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #region Navigation Methods
        private void NavigationDisplay(int type)
        {
            try
            {
                long No = 0;
                if (type == 5)
                {
                    No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                    ID = No;
                }

                else if (type == 1)
                {
                    No = Convert.ToInt64(dtSearch.Rows[0].ItemArray[0].ToString());
                    cntRow = 0;
                    ID = No;
                }
                else if (type == 2)
                {
                    No = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    cntRow = dtSearch.Rows.Count - 1;
                    ID = No;
                }
                else
                {
                    if (type == 3)
                    {
                        cntRow = cntRow + 1;
                    }
                    else if (type == 4)
                    {
                        cntRow = cntRow - 1;
                    }

                    if (cntRow < 0)
                    {
                        OMMessageBox.Show("This is First Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow + 1;
                    }
                    else if (cntRow > dtSearch.Rows.Count - 1)
                    {
                        OMMessageBox.Show("This is Last Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow - 1;
                    }
                    else
                    {
                        No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                        ID = No;
                    }

                }


                FillControls();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

        }

        private void SetNavigation()
        {
            cntRow = 0;
            for (int i = 0; i < dtSearch.Rows.Count; i++)
            {
                if (Convert.ToInt64(dtSearch.Rows[i].ItemArray[0].ToString()) == ID)
                {
                    cntRow = i;
                    break;
                }
            }
        }

        private void setDisplay(bool flag)
        {
            btnFirst.Visible = flag;
            btnPrev.Visible = flag;
            btnNext.Visible = flag;
            btnLast.Visible = flag;
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            NavigationDisplay(1);
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            NavigationDisplay(4);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            NavigationDisplay(3);
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            NavigationDisplay(2);
        }

        #endregion

        #region KeyDown Events
        private void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is GroupBox)
                    KeyDownFormat(ctrl.Controls);
            }
        }

        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left && e.Control)
            {
                if(btnPrev.Enabled) btnPrev_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Up && e.Control)
            {
                if (btnFirst.Enabled) btnFirst_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Right && e.Control)
            {
                if (btnNext.Enabled) btnNext_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Down && e.Control)
            {
                if (btnLast.Enabled) btnLast_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F2)
            {
                if (BtnSave.Visible) BtnSave_Click(sender, e);
            }
            //else if (e.KeyCode == Keys.Escape)
            //{
            //    BtnExit_Click(sender, e);
            //}
        }
        #endregion

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                ID = 0;
                ObjFunction.InitialiseControl(this.Controls);
                ObjFunction.LockButtons(false, this.Controls);
                ObjFunction.LockControls(true, this.Controls);
                cmbSignCode.SelectedIndex = 0;
                BankNm = "";
                txtBankName.Focus();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            ObjFunction.LockButtons(false, this.Controls);
            ObjFunction.LockControls(true, this.Controls);
            txtBankName.Focus();
        }


        private void btnCancel_Click(object sender, EventArgs e)
        {
            NavigationDisplay(5);

            ObjFunction.LockButtons(true, this.Controls);
            ObjFunction.LockControls(false, this.Controls);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            Form NewF = new Bank();
            this.Close();
            ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (chkActive.Checked == false) return;
                dbLedger = new DBMLedger();
                mLedger = new MLedger();

                mLedger.LedgerNo = ID;
                if (OMMessageBox.Show("Are you sure want to delete this record?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (dbLedger.DeleteMLedger(mLedger) == true)
                    {
                        OMMessageBox.Show("Ledger Deleted Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        FillControls();

                    }
                    else
                    {
                        OMMessageBox.Show("Ledger not Deleted", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void chkActive_CheckedChanged(object sender, EventArgs e)
        {
            if (chkActive.Checked == true)
                chkActive.Text = "Yes";
            else
                chkActive.Text = "No";
        }

        private void txtContactNo_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(txtContactNo, -1, 15);
        }

        private void txtOpeningBalance_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(txtOpeningBalance, 2, 9, JitFunctions.MaskedType.NotNegative);
        }

        
    }
}
