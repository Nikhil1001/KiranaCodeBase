﻿namespace Kirana.Utilities
{
    partial class ItemChangeDetailsAE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlCombo = new System.Windows.Forms.Panel();
            this.cmbDepartment = new System.Windows.Forms.ComboBox();
            this.lblDepartment = new System.Windows.Forms.Label();
            this.cmbItemName = new System.Windows.Forms.ComboBox();
            this.cmbBrandName = new System.Windows.Forms.ComboBox();
            this.cmbMainGroup = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblCategory = new System.Windows.Forms.Label();
            this.cmbCategory = new System.Windows.Forms.ComboBox();
            this.dgItemChange = new System.Windows.Forms.DataGridView();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTotProducts = new System.Windows.Forms.TextBox();
            this.btnApplyChanges = new System.Windows.Forms.Button();
            this.lblMsg = new System.Windows.Forms.Label();
            this.ChkSelect = new System.Windows.Forms.CheckBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lstBoxMain = new System.Windows.Forms.ListBox();
            this.lstBoxBrand = new System.Windows.Forms.ListBox();
            this.lstBoxUomPri = new System.Windows.Forms.ListBox();
            this.pnlTemp = new System.Windows.Forms.Panel();
            this.cmbTCategory = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbTDepart = new System.Windows.Forms.ComboBox();
            this.cmbTBrandName = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbTMainGroup = new System.Windows.Forms.ComboBox();
            this.lblMainGroup = new System.Windows.Forms.Label();
            this.lstBoxUomDef = new System.Windows.Forms.ListBox();
            this.lstCategory = new System.Windows.Forms.ListBox();
            this.lstDepartment = new System.Windows.Forms.ListBox();
            this.pnlLang = new System.Windows.Forms.Panel();
            this.btnBrand = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnItem = new System.Windows.Forms.Button();
            this.SNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DepartmentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MainGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BrandName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Barcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DefaultUOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LowerUOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CategoryName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsActive = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ItmCompanyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsFixedBarcode = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.SelectAll = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ItemNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkStockBarcodeNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMDefault = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMPrimary = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MainGroupId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BrandId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMPrimaryNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CategoryNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DepartmentNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LanguageName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlCombo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgItemChange)).BeginInit();
            this.pnlTemp.SuspendLayout();
            this.pnlLang.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(238, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 501;
            this.label1.Text = "Category";
            // 
            // pnlCombo
            // 
            this.pnlCombo.BackColor = System.Drawing.Color.Transparent;
            this.pnlCombo.Controls.Add(this.cmbDepartment);
            this.pnlCombo.Controls.Add(this.lblDepartment);
            this.pnlCombo.Controls.Add(this.cmbItemName);
            this.pnlCombo.Controls.Add(this.cmbBrandName);
            this.pnlCombo.Controls.Add(this.cmbMainGroup);
            this.pnlCombo.Controls.Add(this.label3);
            this.pnlCombo.Controls.Add(this.label2);
            this.pnlCombo.Controls.Add(this.label1);
            this.pnlCombo.Location = new System.Drawing.Point(12, 3);
            this.pnlCombo.Name = "pnlCombo";
            this.pnlCombo.Size = new System.Drawing.Size(1014, 39);
            this.pnlCombo.TabIndex = 0;
            // 
            // cmbDepartment
            // 
            this.cmbDepartment.FormattingEnabled = true;
            this.cmbDepartment.Location = new System.Drawing.Point(85, 7);
            this.cmbDepartment.Name = "cmbDepartment";
            this.cmbDepartment.Size = new System.Drawing.Size(145, 21);
            this.cmbDepartment.TabIndex = 0;
            this.cmbDepartment.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbDepartment_KeyDown);
            // 
            // lblDepartment
            // 
            this.lblDepartment.AutoSize = true;
            this.lblDepartment.BackColor = System.Drawing.Color.Transparent;
            this.lblDepartment.Location = new System.Drawing.Point(3, 10);
            this.lblDepartment.Name = "lblDepartment";
            this.lblDepartment.Size = new System.Drawing.Size(62, 13);
            this.lblDepartment.TabIndex = 507;
            this.lblDepartment.Text = "Department";
            // 
            // cmbItemName
            // 
            this.cmbItemName.FormattingEnabled = true;
            this.cmbItemName.Location = new System.Drawing.Point(794, 8);
            this.cmbItemName.Name = "cmbItemName";
            this.cmbItemName.Size = new System.Drawing.Size(215, 21);
            this.cmbItemName.TabIndex = 3;
            this.cmbItemName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbItemName_KeyDown);
            // 
            // cmbBrandName
            // 
            this.cmbBrandName.FormattingEnabled = true;
            this.cmbBrandName.Location = new System.Drawing.Point(515, 8);
            this.cmbBrandName.Name = "cmbBrandName";
            this.cmbBrandName.Size = new System.Drawing.Size(228, 21);
            this.cmbBrandName.TabIndex = 2;
            this.cmbBrandName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbBrandName_KeyDown);
            // 
            // cmbMainGroup
            // 
            this.cmbMainGroup.FormattingEnabled = true;
            this.cmbMainGroup.Location = new System.Drawing.Point(303, 8);
            this.cmbMainGroup.Name = "cmbMainGroup";
            this.cmbMainGroup.Size = new System.Drawing.Size(154, 21);
            this.cmbMainGroup.TabIndex = 1;
            this.cmbMainGroup.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbMainGroup_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(751, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 503;
            this.label3.Text = "Item";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(467, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 502;
            this.label2.Text = "Brand";
            // 
            // lblCategory
            // 
            this.lblCategory.AutoSize = true;
            this.lblCategory.Location = new System.Drawing.Point(730, 449);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(49, 13);
            this.lblCategory.TabIndex = 508;
            this.lblCategory.Text = "Category";
            this.lblCategory.Visible = false;
            // 
            // cmbCategory
            // 
            this.cmbCategory.FormattingEnabled = true;
            this.cmbCategory.Location = new System.Drawing.Point(801, 446);
            this.cmbCategory.Name = "cmbCategory";
            this.cmbCategory.Size = new System.Drawing.Size(144, 21);
            this.cmbCategory.TabIndex = 400;
            this.cmbCategory.Visible = false;
            this.cmbCategory.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbCategory_KeyDown);
            // 
            // dgItemChange
            // 
            this.dgItemChange.AllowUserToAddRows = false;
            this.dgItemChange.AllowUserToDeleteRows = false;
            this.dgItemChange.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgItemChange.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SNo,
            this.DepartmentName,
            this.MainGroup,
            this.BrandName,
            this.ItemName,
            this.Barcode,
            this.DefaultUOM,
            this.LowerUOM,
            this.CategoryName,
            this.IsActive,
            this.ItmCompanyName,
            this.IsFixedBarcode,
            this.SelectAll,
            this.ItemNo,
            this.PkStockBarcodeNo,
            this.UOMDefault,
            this.UOMPrimary,
            this.MainGroupId,
            this.BrandId,
            this.UOMNo,
            this.UOMPrimaryNo,
            this.CategoryNo,
            this.DepartmentNo,
            this.LanguageName});
            this.dgItemChange.Location = new System.Drawing.Point(13, 102);
            this.dgItemChange.Name = "dgItemChange";
            this.dgItemChange.Size = new System.Drawing.Size(1013, 314);
            this.dgItemChange.TabIndex = 5;
            this.dgItemChange.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgItemChange_CellValueChanged);
            this.dgItemChange.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgItemChange_CellFormatting);
            this.dgItemChange.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgItemChange_CellEndEdit);
            this.dgItemChange.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgItemChange_KeyDown);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(902, 423);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 504;
            this.label5.Text = "Total Products :";
            // 
            // txtTotProducts
            // 
            this.txtTotProducts.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtTotProducts.Location = new System.Drawing.Point(1005, 420);
            this.txtTotProducts.Name = "txtTotProducts";
            this.txtTotProducts.ReadOnly = true;
            this.txtTotProducts.Size = new System.Drawing.Size(61, 20);
            this.txtTotProducts.TabIndex = 8;
            this.txtTotProducts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnApplyChanges
            // 
            this.btnApplyChanges.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnApplyChanges.Location = new System.Drawing.Point(13, 422);
            this.btnApplyChanges.Name = "btnApplyChanges";
            this.btnApplyChanges.Size = new System.Drawing.Size(130, 60);
            this.btnApplyChanges.TabIndex = 6;
            this.btnApplyChanges.Text = "Apply Changes";
            this.btnApplyChanges.UseVisualStyleBackColor = true;
            this.btnApplyChanges.Click += new System.EventHandler(this.btnApplyChanges_Click);
            // 
            // lblMsg
            // 
            this.lblMsg.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.lblMsg.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMsg.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg.ForeColor = System.Drawing.Color.Maroon;
            this.lblMsg.Location = new System.Drawing.Point(156, 218);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(481, 52);
            this.lblMsg.TabIndex = 509;
            this.lblMsg.Text = "label4";
            this.lblMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMsg.Visible = false;
            // 
            // ChkSelect
            // 
            this.ChkSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ChkSelect.AutoSize = true;
            this.ChkSelect.BackColor = System.Drawing.Color.Transparent;
            this.ChkSelect.Location = new System.Drawing.Point(1100, 422);
            this.ChkSelect.Name = "ChkSelect";
            this.ChkSelect.Size = new System.Drawing.Size(85, 17);
            this.ChkSelect.TabIndex = 510;
            this.ChkSelect.Text = "SelectAll(F2)";
            this.ChkSelect.UseVisualStyleBackColor = false;
            this.ChkSelect.CheckedChanged += new System.EventHandler(this.ChkSelect_CheckedChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.Location = new System.Drawing.Point(156, 422);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 60);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClose.Location = new System.Drawing.Point(259, 422);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(80, 60);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "E&xit";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lstBoxMain
            // 
            this.lstBoxMain.FormattingEnabled = true;
            this.lstBoxMain.Location = new System.Drawing.Point(22, 274);
            this.lstBoxMain.Name = "lstBoxMain";
            this.lstBoxMain.Size = new System.Drawing.Size(179, 95);
            this.lstBoxMain.TabIndex = 511;
            this.lstBoxMain.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstBoxMain_KeyDown);
            // 
            // lstBoxBrand
            // 
            this.lstBoxBrand.FormattingEnabled = true;
            this.lstBoxBrand.Location = new System.Drawing.Point(23, 121);
            this.lstBoxBrand.Name = "lstBoxBrand";
            this.lstBoxBrand.Size = new System.Drawing.Size(178, 95);
            this.lstBoxBrand.TabIndex = 512;
            this.lstBoxBrand.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstBoxBrand_KeyDown);
            // 
            // lstBoxUomPri
            // 
            this.lstBoxUomPri.FormattingEnabled = true;
            this.lstBoxUomPri.Location = new System.Drawing.Point(1056, 132);
            this.lstBoxUomPri.Name = "lstBoxUomPri";
            this.lstBoxUomPri.Size = new System.Drawing.Size(101, 95);
            this.lstBoxUomPri.TabIndex = 514;
            this.lstBoxUomPri.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstBoxUomPri_KeyDown);
            // 
            // pnlTemp
            // 
            this.pnlTemp.BackColor = System.Drawing.Color.Transparent;
            this.pnlTemp.Controls.Add(this.cmbTCategory);
            this.pnlTemp.Controls.Add(this.label7);
            this.pnlTemp.Controls.Add(this.cmbTDepart);
            this.pnlTemp.Controls.Add(this.cmbTBrandName);
            this.pnlTemp.Controls.Add(this.label4);
            this.pnlTemp.Controls.Add(this.label6);
            this.pnlTemp.Controls.Add(this.cmbTMainGroup);
            this.pnlTemp.Controls.Add(this.lblMainGroup);
            this.pnlTemp.Location = new System.Drawing.Point(11, 58);
            this.pnlTemp.Name = "pnlTemp";
            this.pnlTemp.Size = new System.Drawing.Size(1013, 38);
            this.pnlTemp.TabIndex = 515;
            // 
            // cmbTCategory
            // 
            this.cmbTCategory.FormattingEnabled = true;
            this.cmbTCategory.Location = new System.Drawing.Point(808, 8);
            this.cmbTCategory.Name = "cmbTCategory";
            this.cmbTCategory.Size = new System.Drawing.Size(141, 21);
            this.cmbTCategory.TabIndex = 1000;
            this.cmbTCategory.Visible = false;
            this.cmbTCategory.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbTCategory_KeyDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(753, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 510;
            this.label7.Text = "Category";
            this.label7.Visible = false;
            // 
            // cmbTDepart
            // 
            this.cmbTDepart.FormattingEnabled = true;
            this.cmbTDepart.Location = new System.Drawing.Point(86, 9);
            this.cmbTDepart.Name = "cmbTDepart";
            this.cmbTDepart.Size = new System.Drawing.Size(145, 21);
            this.cmbTDepart.TabIndex = 802;
            this.cmbTDepart.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbTDepart_KeyDown);
            // 
            // cmbTBrandName
            // 
            this.cmbTBrandName.FormattingEnabled = true;
            this.cmbTBrandName.Location = new System.Drawing.Point(516, 8);
            this.cmbTBrandName.Name = "cmbTBrandName";
            this.cmbTBrandName.Size = new System.Drawing.Size(228, 21);
            this.cmbTBrandName.TabIndex = 804;
            this.cmbTBrandName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbTBrandName_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(470, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 504;
            this.label4.Text = "Brand";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(2, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 508;
            this.label6.Text = "Department";
            // 
            // cmbTMainGroup
            // 
            this.cmbTMainGroup.FormattingEnabled = true;
            this.cmbTMainGroup.Location = new System.Drawing.Point(304, 9);
            this.cmbTMainGroup.Name = "cmbTMainGroup";
            this.cmbTMainGroup.Size = new System.Drawing.Size(155, 21);
            this.cmbTMainGroup.TabIndex = 803;
            this.cmbTMainGroup.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbTMainGroup_KeyDown);
            // 
            // lblMainGroup
            // 
            this.lblMainGroup.AutoSize = true;
            this.lblMainGroup.BackColor = System.Drawing.Color.Transparent;
            this.lblMainGroup.Location = new System.Drawing.Point(240, 13);
            this.lblMainGroup.Name = "lblMainGroup";
            this.lblMainGroup.Size = new System.Drawing.Size(49, 13);
            this.lblMainGroup.TabIndex = 502;
            this.lblMainGroup.Text = "Category";
            // 
            // lstBoxUomDef
            // 
            this.lstBoxUomDef.FormattingEnabled = true;
            this.lstBoxUomDef.Location = new System.Drawing.Point(1056, 274);
            this.lstBoxUomDef.Name = "lstBoxUomDef";
            this.lstBoxUomDef.Size = new System.Drawing.Size(101, 95);
            this.lstBoxUomDef.TabIndex = 513;
            this.lstBoxUomDef.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstBoxUomDef_KeyDown);
            // 
            // lstCategory
            // 
            this.lstCategory.FormattingEnabled = true;
            this.lstCategory.Location = new System.Drawing.Point(873, 133);
            this.lstCategory.Name = "lstCategory";
            this.lstCategory.Size = new System.Drawing.Size(120, 95);
            this.lstCategory.TabIndex = 516;
            this.lstCategory.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstCategory_KeyDown);
            // 
            // lstDepartment
            // 
            this.lstDepartment.FormattingEnabled = true;
            this.lstDepartment.Location = new System.Drawing.Point(872, 274);
            this.lstDepartment.Name = "lstDepartment";
            this.lstDepartment.Size = new System.Drawing.Size(120, 95);
            this.lstDepartment.TabIndex = 517;
            this.lstDepartment.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstDepartment_KeyDown);
            // 
            // pnlLang
            // 
            this.pnlLang.Controls.Add(this.btnBrand);
            this.pnlLang.Controls.Add(this.button2);
            this.pnlLang.Controls.Add(this.btnItem);
            this.pnlLang.Location = new System.Drawing.Point(357, 420);
            this.pnlLang.Name = "pnlLang";
            this.pnlLang.Size = new System.Drawing.Size(340, 65);
            this.pnlLang.TabIndex = 518;
            this.pnlLang.Visible = false;
            // 
            // btnBrand
            // 
            this.btnBrand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBrand.Location = new System.Drawing.Point(237, 5);
            this.btnBrand.Name = "btnBrand";
            this.btnBrand.Size = new System.Drawing.Size(80, 60);
            this.btnBrand.TabIndex = 11;
            this.btnBrand.Text = "Brand";
            this.btnBrand.UseVisualStyleBackColor = true;
            this.btnBrand.Click += new System.EventHandler(this.btnBrand_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button2.Location = new System.Drawing.Point(139, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(80, 60);
            this.button2.TabIndex = 10;
            this.button2.Text = "E&xit";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // btnItem
            // 
            this.btnItem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnItem.Location = new System.Drawing.Point(23, 3);
            this.btnItem.Name = "btnItem";
            this.btnItem.Size = new System.Drawing.Size(80, 60);
            this.btnItem.TabIndex = 9;
            this.btnItem.Text = "Item";
            this.btnItem.UseVisualStyleBackColor = true;
            this.btnItem.Click += new System.EventHandler(this.btnItem_Click);
            // 
            // SNo
            // 
            this.SNo.DataPropertyName = "SrNo";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.SNo.DefaultCellStyle = dataGridViewCellStyle1;
            this.SNo.HeaderText = "SNo";
            this.SNo.Name = "SNo";
            this.SNo.ReadOnly = true;
            this.SNo.Width = 40;
            // 
            // DepartmentName
            // 
            this.DepartmentName.DataPropertyName = "DepartmentName";
            this.DepartmentName.HeaderText = "DepartmentName";
            this.DepartmentName.Name = "DepartmentName";
            this.DepartmentName.ReadOnly = true;
            this.DepartmentName.Width = 120;
            // 
            // MainGroup
            // 
            this.MainGroup.DataPropertyName = "MainGroupName";
            this.MainGroup.HeaderText = "Category";
            this.MainGroup.Name = "MainGroup";
            this.MainGroup.ReadOnly = true;
            this.MainGroup.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.MainGroup.Width = 90;
            // 
            // BrandName
            // 
            this.BrandName.DataPropertyName = "BrandName";
            this.BrandName.HeaderText = "BrandName";
            this.BrandName.Name = "BrandName";
            this.BrandName.ReadOnly = true;
            this.BrandName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // ItemName
            // 
            this.ItemName.DataPropertyName = "ItemName";
            this.ItemName.HeaderText = "Item Name";
            this.ItemName.Name = "ItemName";
            this.ItemName.Width = 200;
            // 
            // Barcode
            // 
            this.Barcode.DataPropertyName = "Barcode";
            this.Barcode.HeaderText = "Barcode";
            this.Barcode.Name = "Barcode";
            this.Barcode.Width = 121;
            // 
            // DefaultUOM
            // 
            this.DefaultUOM.DataPropertyName = "UOMDefault";
            this.DefaultUOM.HeaderText = "UOM Default";
            this.DefaultUOM.Name = "DefaultUOM";
            this.DefaultUOM.ReadOnly = true;
            this.DefaultUOM.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DefaultUOM.Width = 95;
            // 
            // LowerUOM
            // 
            this.LowerUOM.DataPropertyName = "UOMLower";
            this.LowerUOM.HeaderText = "UOM Primary";
            this.LowerUOM.Name = "LowerUOM";
            this.LowerUOM.ReadOnly = true;
            this.LowerUOM.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.LowerUOM.Width = 96;
            // 
            // CategoryName
            // 
            this.CategoryName.DataPropertyName = "CategoryName";
            this.CategoryName.HeaderText = "CategoryName";
            this.CategoryName.Name = "CategoryName";
            this.CategoryName.ReadOnly = true;
            this.CategoryName.Visible = false;
            this.CategoryName.Width = 120;
            // 
            // IsActive
            // 
            this.IsActive.DataPropertyName = "IsActive";
            this.IsActive.HeaderText = "Status";
            this.IsActive.Name = "IsActive";
            this.IsActive.Width = 50;
            // 
            // ItmCompanyName
            // 
            this.ItmCompanyName.DataPropertyName = "CompanyName";
            this.ItmCompanyName.HeaderText = "CompanyName";
            this.ItmCompanyName.Name = "ItmCompanyName";
            this.ItmCompanyName.ReadOnly = true;
            // 
            // IsFixedBarcode
            // 
            this.IsFixedBarcode.DataPropertyName = "IsFixedBarcode";
            this.IsFixedBarcode.HeaderText = "FixedBarcode";
            this.IsFixedBarcode.Name = "IsFixedBarcode";
            this.IsFixedBarcode.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IsFixedBarcode.Visible = false;
            this.IsFixedBarcode.Width = 80;
            // 
            // SelectAll
            // 
            this.SelectAll.DataPropertyName = "SelectAll";
            this.SelectAll.HeaderText = "Chk";
            this.SelectAll.Name = "SelectAll";
            this.SelectAll.Visible = false;
            this.SelectAll.Width = 44;
            // 
            // ItemNo
            // 
            this.ItemNo.DataPropertyName = "ItemNo";
            this.ItemNo.HeaderText = "ItemNo";
            this.ItemNo.Name = "ItemNo";
            this.ItemNo.ReadOnly = true;
            this.ItemNo.Visible = false;
            this.ItemNo.Width = 50;
            // 
            // PkStockBarcodeNo
            // 
            this.PkStockBarcodeNo.DataPropertyName = "PkStockBarcodeNo";
            this.PkStockBarcodeNo.FillWeight = 50F;
            this.PkStockBarcodeNo.HeaderText = "StockBarcodeNo";
            this.PkStockBarcodeNo.Name = "PkStockBarcodeNo";
            this.PkStockBarcodeNo.Visible = false;
            // 
            // UOMDefault
            // 
            this.UOMDefault.DataPropertyName = "UOMDefaultNo";
            this.UOMDefault.HeaderText = "UOMDefault";
            this.UOMDefault.Name = "UOMDefault";
            this.UOMDefault.ReadOnly = true;
            this.UOMDefault.Visible = false;
            this.UOMDefault.Width = 50;
            // 
            // UOMPrimary
            // 
            this.UOMPrimary.DataPropertyName = "UOMPrimaryNo";
            this.UOMPrimary.HeaderText = "UOMPrimary";
            this.UOMPrimary.Name = "UOMPrimary";
            this.UOMPrimary.ReadOnly = true;
            this.UOMPrimary.Visible = false;
            this.UOMPrimary.Width = 50;
            // 
            // MainGroupId
            // 
            this.MainGroupId.DataPropertyName = "GroupNo1";
            this.MainGroupId.HeaderText = "MainGroupId";
            this.MainGroupId.Name = "MainGroupId";
            this.MainGroupId.Visible = false;
            // 
            // BrandId
            // 
            this.BrandId.DataPropertyName = "GroupNo";
            this.BrandId.HeaderText = "BrandId";
            this.BrandId.Name = "BrandId";
            this.BrandId.Visible = false;
            // 
            // UOMNo
            // 
            this.UOMNo.DataPropertyName = "UOMNo";
            this.UOMNo.HeaderText = "UOMNo";
            this.UOMNo.Name = "UOMNo";
            this.UOMNo.Visible = false;
            // 
            // UOMPrimaryNo
            // 
            this.UOMPrimaryNo.DataPropertyName = "UOMPrimary";
            this.UOMPrimaryNo.HeaderText = "UOMPrimaryNo";
            this.UOMPrimaryNo.Name = "UOMPrimaryNo";
            this.UOMPrimaryNo.Visible = false;
            // 
            // CategoryNo
            // 
            this.CategoryNo.DataPropertyName = "CategoryNo";
            this.CategoryNo.HeaderText = "CategoryNo";
            this.CategoryNo.Name = "CategoryNo";
            this.CategoryNo.Visible = false;
            // 
            // DepartmentNo
            // 
            this.DepartmentNo.DataPropertyName = "FKStockDeptNo";
            this.DepartmentNo.HeaderText = "DepartmentNo";
            this.DepartmentNo.Name = "DepartmentNo";
            this.DepartmentNo.Visible = false;
            // 
            // LanguageName
            // 
            this.LanguageName.DataPropertyName = "LangFullDesc";
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Shivaji01", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LanguageName.DefaultCellStyle = dataGridViewCellStyle2;
            this.LanguageName.HeaderText = "Item Name";
            this.LanguageName.Name = "LanguageName";
            this.LanguageName.Width = 200;
            // 
            // ItemChangeDetailsAE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 491);
            this.Controls.Add(this.cmbCategory);
            this.Controls.Add(this.lblCategory);
            this.Controls.Add(this.pnlLang);
            this.Controls.Add(this.lstDepartment);
            this.Controls.Add(this.lstCategory);
            this.Controls.Add(this.lstBoxMain);
            this.Controls.Add(this.lstBoxBrand);
            this.Controls.Add(this.lstBoxUomDef);
            this.Controls.Add(this.pnlTemp);
            this.Controls.Add(this.lstBoxUomPri);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.ChkSelect);
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.btnApplyChanges);
            this.Controls.Add(this.txtTotProducts);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dgItemChange);
            this.Controls.Add(this.pnlCombo);
            this.Name = "ItemChangeDetailsAE";
            this.Text = "Item Change Details";
            this.Load += new System.EventHandler(this.ItemChangeDetailsAE_Load);
            this.pnlCombo.ResumeLayout(false);
            this.pnlCombo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgItemChange)).EndInit();
            this.pnlTemp.ResumeLayout(false);
            this.pnlTemp.PerformLayout();
            this.pnlLang.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlCombo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbMainGroup;
        private System.Windows.Forms.ComboBox cmbItemName;
        private System.Windows.Forms.ComboBox cmbBrandName;
        private System.Windows.Forms.DataGridView dgItemChange;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtTotProducts;
        private System.Windows.Forms.Label lblCategory;
        private System.Windows.Forms.ComboBox cmbCategory;
        private System.Windows.Forms.ComboBox cmbDepartment;
        private System.Windows.Forms.Label lblDepartment;
        private System.Windows.Forms.Button btnApplyChanges;
        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.CheckBox ChkSelect;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.ListBox lstBoxMain;
        private System.Windows.Forms.ListBox lstBoxBrand;
        private System.Windows.Forms.ListBox lstBoxUomPri;
        private System.Windows.Forms.Panel pnlTemp;
        private System.Windows.Forms.ComboBox cmbTMainGroup;
        private System.Windows.Forms.Label lblMainGroup;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbTBrandName;
        private System.Windows.Forms.ComboBox cmbTCategory;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbTDepart;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListBox lstBoxUomDef;
        private System.Windows.Forms.ListBox lstCategory;
        private System.Windows.Forms.ListBox lstDepartment;
        private System.Windows.Forms.Panel pnlLang;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnItem;
        private System.Windows.Forms.Button btnBrand;
        private System.Windows.Forms.DataGridViewTextBoxColumn SNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn DepartmentName;
        private System.Windows.Forms.DataGridViewTextBoxColumn MainGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn BrandName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Barcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn DefaultUOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn LowerUOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn CategoryName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsActive;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItmCompanyName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsFixedBarcode;
        private System.Windows.Forms.DataGridViewCheckBoxColumn SelectAll;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkStockBarcodeNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMDefault;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMPrimary;
        private System.Windows.Forms.DataGridViewTextBoxColumn MainGroupId;
        private System.Windows.Forms.DataGridViewTextBoxColumn BrandId;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMPrimaryNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn CategoryNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn DepartmentNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn LanguageName;
    }
}