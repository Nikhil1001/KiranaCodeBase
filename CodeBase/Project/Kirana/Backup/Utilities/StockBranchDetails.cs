﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;
namespace Kirana.Utilities
{
    public partial class StockBranchDetails : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();
        long BranchNo = 0;
        public StockBranchDetails()
        {
            InitializeComponent();
        }

        private void StockBranchDetails_Load(object sender, EventArgs e)
        {
            dgBranch.DataSource = ObjFunction.GetDataView("Select 0 As SrNo,BranchName,'False' AS  Chk,Branchno From MBranch Where IsActive='True' order by BranchName").Table.DefaultView;
            dgBranch.CurrentCell = dgBranch[2, 0];
            dgBranch.Focus();
        }

        public void BindStockBranchDetails(long Branch)
        {
            string sql=" SELECT MStockItems_V_1.ItemName, MStockBranch.MinLevel, MStockBranch.MaxLevel, MStockItems_V_1.ItemNo, ISNULL(MStockBranch.PkSrNo, 0) AS PkSrNo "+
                       " FROM dbo.MStockItems_V(NULL, NULL, NULL, NULL, NULL, NULL, NULL) AS MStockItems_V_1 LEFT OUTER JOIN MStockBranch ON MStockItems_V_1.ItemNo = MStockBranch.ItemNo "+
                       " WHERE (MStockBranch.BaranchNo = "+Branch+") ORDER BY MStockItems_V_1.ItemName ";

            dgStockBarchDetails.DataSource = ObjFunction.GetDataView(sql).Table.DefaultView;
        }

        
        private void dgBranch_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            for (int i = 0; i < dgBranch.Rows.Count; i++)
            {
                if (e.RowIndex == i)
                {
                    dgBranch.Rows[i].Cells[2].Value = true;
                    if (Convert.ToBoolean(dgBranch.Rows[i].Cells[2].Value) == true)
                    {
                        BranchNo = Convert.ToInt64(dgBranch.Rows[i].Cells[3].Value);
                        BindStockBranchDetails(BranchNo);
                        dgStockBarchDetails.CurrentCell = dgStockBarchDetails[1, dgStockBarchDetails.CurrentRow.Index];
                        dgStockBarchDetails.Focus();
                    }
                }
                else
                {
                    dgStockBarchDetails.Rows[i].Cells[2].Value = false;
                }
            }
        }
    }
}
