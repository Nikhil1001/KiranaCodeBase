﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Utilities
{
    /// <summary>
    /// This class is used for Sales Voucher
    /// </summary>
    public partial class ItemMarginChange : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();
        DBMStockItems dbMStockItems = new DBMStockItems();
        MRateSetting mRateSettig = new MRateSetting();
        bool IsChanges = false;

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public ItemMarginChange()
        {
            InitializeComponent();
        }

        private void ItemMarginChange_Load(object sender, EventArgs e)
        {
            try
            {

                ObjFunction.FillCombo(cmbDepart, "SELECT DISTINCT MStockGroup.StockGroupNo, MStockGroup.StockGroupName  FROM   MStockGroup INNER JOIN  MStockItems ON MStockGroup.StockGroupNo = MStockItems.FkStockDeptNo  WHERE  (MStockGroup.IsActive = 'True') AND (MStockGroup.ControlGroup = 4) ORDER BY MStockGroup.StockGroupName");
                ObjFunction.FillCombo(cmbCategory, "Select StockGroupNo, StockGroupName from MStockGroup where ControlGroup = 2 and IsActive='True'"); //, "SELECT DISTINCT StockGroupNo,StockGroupName FROM MStockGroup WHERE (MStockGroup.IsActive = 'True') And ControlGroup = 2 order by StockGroupName");
                ObjFunction.FillCombo(cmbBrandName, "Select StockGroupNo, StockGroupName from MStockGroup where ControlGroup = 3 and IsActive='True'");    //, "SELECT DISTINCT MStockGroup.StockGroupNo, MStockGroup.StockGroupName  FROM   MStockGroup INNER JOIN  MStockItems ON MStockGroup.StockGroupNo = MStockItems.GroupNo  WHERE  (MStockGroup.IsActive = 'True') AND (MStockGroup.ControlGroup = 3) ORDER BY MStockGroup.StockGroupName");
                txtMargin2.Text = "0";
                txtMargin1.Text = "0";
                btnCancel.Visible = true;
                btnApplyChanges.Enabled = false;
                chkSelect.Enabled = false;
                btnOk.Enabled = false;

            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (cmbCategory.SelectedIndex >= 0)
                cmbCategory.SelectedIndex = 0;
            if (cmbDepart.SelectedIndex >= 0)
                cmbDepart.SelectedIndex = 0;
            if (cmbBrandName.SelectedIndex >= 0)
                cmbBrandName.SelectedIndex = 0;
            txtMargin1.Text = "";
            txtMargin2.Text = "";
            gvRateSetting.Rows.Clear();
            dgCategory.Rows.Clear();
            txtMargin2.Enabled = false;
            btnApplyChanges.Enabled = false;
            btnOk.Enabled = false;
            chkSelect.Enabled = false;
            chkSelect.Checked = false;
            cmbDepart.Focus();
        }


        private void btnApplyChanges_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsChanges)
                {
                    if (rbBrand.Checked == true)
                    {
                        dbMStockItems = new DBMStockItems();
                        for (int i = 0; i < gvRateSetting.Rows.Count; i++)
                        {
                            dbMStockItems.UpdateMStockItems(Convert.ToInt64(gvRateSetting.Rows[i].Cells[ColIndex.ItemNo].Value), Convert.ToDouble(gvRateSetting.Rows[i].Cells[ColIndex.Margin].Value));
                        }

                        if (dbMStockItems.ExecuteNonQueryStatements() == true)
                        {
                            OMMessageBox.Show("Item Save Successfully...");
                            btnCancel_Click(sender, e);
                            btnApplyChanges.Enabled = false;
                            btnOk.Enabled = false;
                            chkSelect.Enabled = false;
                            chkSelect.Checked = false;
                        }
                    }
                    else if (rbCategory.Checked == true)
                    {
                        if (rbNone.Checked == true)
                        {
                            dbMStockItems = new DBMStockItems();
                            for (int i = 0; i < dgCategory.Rows.Count; i++)
                            {
                                dbMStockItems.UpdateMCategory(Convert.ToInt64(dgCategory.Rows[i].Cells[ColIndex.ItemNo].Value), Convert.ToDouble(dgCategory.Rows[i].Cells[ColIndex.Margin].Value));
                            }
                            if (dbMStockItems.ExecuteNonQueryStatements() == true)
                            {
                                OMMessageBox.Show("Margin Changes Done Successfully...");
                                btnCancel_Click(sender, e);
                                btnApplyChanges.Enabled = false;
                                btnOk.Enabled = false;
                                chkSelect.Enabled = false;
                                chkSelect.Checked = false;
                            }
                            else
                            {
                                OMMessageBox.Show("Margin Changes Not Saved Successfully...");
                            }
                        }
                        else if (rbSKUAll.Checked == true)
                        {
                            dbMStockItems = new DBMStockItems();
                            for (int i = 0; i < dgCategory.Rows.Count; i++)
                            {
                                dbMStockItems.UpdateMCategory(Convert.ToInt64(dgCategory.Rows[i].Cells[ColIndex.ItemNo].Value), Convert.ToDouble(dgCategory.Rows[i].Cells[ColIndex.Margin].Value));
                                dbMStockItems.UpdateMStockItemsAll(Convert.ToInt64(dgCategory.Rows[i].Cells[ColIndex.ItemNo].Value), Convert.ToDouble(dgCategory.Rows[i].Cells[ColIndex.Margin].Value));
                            }
                            if (dbMStockItems.ExecuteNonQueryStatements() == true)
                            {
                                OMMessageBox.Show("Margin Changes Done Successfully...");
                                btnCancel_Click(sender, e);
                                btnApplyChanges.Enabled = false;
                                btnOk.Enabled = false;
                                chkSelect.Enabled = false;
                                chkSelect.Checked = false;
                            }
                            else
                            {
                                OMMessageBox.Show("Margin Changes Not Saved Successfully...");
                            }
                        }
                        else if (rbSkuPartial.Checked == true)
                        {
                            dbMStockItems = new DBMStockItems();
                            for (int i = 0; i < dgCategory.Rows.Count; i++)
                            {
                                dbMStockItems.UpdateMCategory(Convert.ToInt64(dgCategory.Rows[i].Cells[ColIndex.ItemNo].Value), Convert.ToDouble(dgCategory.Rows[i].Cells[ColIndex.Margin].Value));
                                dbMStockItems.UpdateMStockItemsZeroValued(Convert.ToInt64(dgCategory.Rows[i].Cells[ColIndex.ItemNo].Value), Convert.ToDouble(dgCategory.Rows[i].Cells[ColIndex.Margin].Value));
                            }
                            if (dbMStockItems.ExecuteNonQueryStatements() == true)
                            {
                                OMMessageBox.Show("Margin Changes Done Successfully...");
                                btnCancel_Click(sender, e);
                                btnApplyChanges.Enabled = false;
                                btnOk.Enabled = false;
                                chkSelect.Enabled = false;
                                chkSelect.Checked = false;
                            }
                            else
                            {
                                OMMessageBox.Show("Margin Changes Not Saved Successfully...");
                            }
                        }
                        else
                        {
                            OMMessageBox.Show("Select the Sku Option according to which you want the changes");
                            rbNone.Focus();
                        }
                    }
                }
                else
                {
                    OMMessageBox.Show("No Changes Found to apply");
                }
            }

            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #region ColumnIndex
        private static class ColIndex
        {
            public static int Sr = 0;
            public static int ItemName = 1;
            public static int Margin = 2;
            public static int ItemNo = 3;
            public static int Select = 4;
        }
        #endregion


        private delegate void MovetoNext(int RowIndex, int ColIndex, DataGridView dg);

        private void m2n(int RowIndex, int ColIndex, DataGridView dg)
        {
            dg.CurrentCell = dg.Rows[RowIndex].Cells[ColIndex];
        }


        private void BindGrid()
        {
            try
            {
                IsChanges = false;
                string sql = "";
                gvRateSetting.Rows.Clear();

                sql = "SELECT MStockGroup.StockGroupName + ' '+ MStockItems.ItemName AS ItemName,ISnull(MStockItems.Margin,0.00),MStockItems.ItemNo,'false' as chck from MStockItems Inner join MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo where MStockItems.GroupNo=" + cmbBrandName.SelectedValue + " and MStockItems.IsActive='True'";

                if (ObjFunction.GetComboValue(cmbDepart) > 0)
                {
                    sql += " and MStockItems.FkStockDeptNo=" + ObjFunction.GetComboValue(cmbDepart) + "";
                }
                if (ObjFunction.GetComboValue(cmbCategory) > 0)
                {
                    sql += " and MStockItems.GroupNo1=" + ObjFunction.GetComboValue(cmbCategory) + "";
                }


                DataTable dt = ObjFunction.GetDataView(sql).Table;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    gvRateSetting.Rows.Add();
                    gvRateSetting.Rows[i].Cells[0].Value = i + 1;
                    for (int j = 1; j < gvRateSetting.Columns.Count; j++)
                    {
                        gvRateSetting.Rows[i].Cells[j].Value = dt.Rows[i].ItemArray[j - 1];
                    }
                }
                if (gvRateSetting.Rows.Count > 0)
                {
                    gvRateSetting.Focus();
                    gvRateSetting.CurrentCell = gvRateSetting.Rows[0].Cells[ColIndex.Select];
                    txtMargin2.Enabled = true;
                    btnApplyChanges.Enabled = true;
                    btnOk.Enabled = true;
                    chkSelect.Enabled = true;
                }
                else
                {
                    txtMargin2.Enabled = false;
                    btnApplyChanges.Enabled = false;
                    btnOk.Enabled = false;
                    chkSelect.Enabled = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }


        private void DisplayMessage(string str)
        {
            try
            {
                lblMsg.Visible = true;
                lblMsg.Text = str;
                Application.DoEvents();
                System.Threading.Thread.Sleep(1200);
                lblMsg.Visible = false;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }


        private void cmbCategory_Leave(object sender, EventArgs e)
        {

            long iselected = ObjFunction.GetComboValue(cmbCategory);
            if (iselected != 0)
            {
                txtMargin1.Text = ObjQry.ReturnDouble("Select Margin from MStockGroup where StockGroupNo = " + iselected + "", CommonFunctions.ConStr).ToString("0.00");
                txtMargin2.Text = txtMargin1.Text;
            }
            cmbCategory_KeyDown(sender, new KeyEventArgs(Keys.Enter));
        }

        public void BindCategoryGrid()
        {
            IsChanges = false;
            if (ObjFunction.GetComboValue(cmbDepart) != 0)
            {
                string sql = "";
                dgCategory.Rows.Clear();

                sql = "Select StockGroupName as Category, IsNull(MStockGroup.Margin,0.00),StockGroupNo, 'False' as Chck from MStockGroup where ControlGroup = 2 and ControlSubGroup = " + ObjFunction.GetComboValue(cmbDepart) + " and IsActive='True'";

                DataTable dt = ObjFunction.GetDataView(sql).Table;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dgCategory.Rows.Add();
                    dgCategory.Rows[i].Cells[0].Value = i + 1;
                    for (int j = 1; j < dgCategory.Columns.Count; j++)
                    {
                        dgCategory.Rows[i].Cells[j].Value = dt.Rows[i].ItemArray[j - 1];
                    }
                }
                if (dgCategory.Rows.Count > 0)
                {
                    txtMargin2.Enabled = true;
                    btnApplyChanges.Enabled = true;
                    btnOk.Enabled = true;
                    chkSelect.Enabled = true;
                }
                else
                {
                    txtMargin2.Enabled = false;
                    btnApplyChanges.Enabled = false;
                    btnOk.Enabled = false;
                    chkSelect.Enabled = false;
                }
            }
            else
            {
                dgCategory.Rows.Clear();
            }
        }

        private void cmbDepart_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (rbBrand.Checked == true)
                {
                    if (ObjFunction.GetComboValue(cmbDepart) != 0)
                        ObjFunction.FillCombo(cmbCategory, "Select StockGroupNo, StockGroupName from MStockGroup where ControlGroup = 2 and ControlSubGroup = " + ObjFunction.GetComboValue(cmbDepart) + "");
                    else
                        ObjFunction.FillCombo(cmbCategory, "Select StockGroupNo, StockGroupName from MStockGroup where ControlGroup = 2 and IsActive='True'");
                    gvRateSetting.Rows.Clear();
                    dgCategory.Rows.Clear();
                    cmbCategory.Focus();
                    //gvRateSetting.CurrentCell = gvRateSetting.Rows[0].Cells[ColIndex.Select];
                  
                }
                else if (rbCategory.Checked == true)
                {
                    BindCategoryGrid();
                    if (dgCategory.Rows.Count > 0)
                    {
                        dgCategory.Focus();
                        dgCategory.CurrentCell = dgCategory.Rows[0].Cells[ColIndex.Select];
                    }
                }

            }

        }

        private void cmbCategory_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                //if (ObjFunction.GetComboValue(cmbCategory) != 0)
                //{
                //    ObjFunction.FillCombo(cmbBrandName, "Select StockGroupNo, StockGroupName from MStockGroup where ControlGroup = 3 and ControlSubGroup = " + ObjFunction.GetComboValue(cmbCategory) + " and IsActive='True'");
                //    gvRateSetting.Rows.Clear();
                //}
                //else
                //    ObjFunction.FillCombo(cmbBrandName, "Select StockGroupNo, StockGroupName from MStockGroup where ControlGroup = 3 and IsActive='True'");
                cmbBrandName.Focus();
            }
        }

        private void txtMargin1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                cmbBrandName.Focus();
            }
        }

        private void cmbBrandName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                BindGrid();
                txtMargin2.Focus();
            }
        }

        private void txtMargin1_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(((TextBox)sender), 2, 4, JitFunctions.MaskedType.NotNegative);
        }

        private void txtMargin2_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(((TextBox)sender), 2, 4, JitFunctions.MaskedType.NotNegative);
        }

        private void txtMargin2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (txtMargin2.Text == "")
                    txtMargin2.Text = "0.00";
                btnOk.Focus();
            }
        }

        private void gvRateSetting_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == ColIndex.Margin)
            {
                if (gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString() == "")
                    gvRateSetting.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "0.00";
            }
        }

        private void cmbDepart_Leave(object sender, EventArgs e)
        {
            cmbDepart_KeyDown(sender, new KeyEventArgs(Keys.Enter));
        }

        private void rbBrand_CheckedChanged(object sender, EventArgs e)
        {
            if (rbBrand.Checked == true)
            {
                pnlSKU.Visible = false;
                MakeVisible(true);
                gvRateSetting.Visible = true;
                gvRateSetting.Location = new Point(12, 68);
                dgCategory.Visible = false;

            }
            else if (rbCategory.Checked == true)
            {
                pnlSKU.Visible = true;
                MakeVisible(false);
                gvRateSetting.Visible = false;
                dgCategory.Location = new Point(12, 68);
                dgCategory.Visible = true;

            }
        }

        private void MakeVisible(bool isVisible)
        {
            lblCategory.Visible = isVisible;
            lbBrand.Visible = isVisible;
            lblMargin.Visible = isVisible;
            lblPerMargin.Visible = isVisible;
            cmbCategory.Visible = isVisible;
            cmbBrandName.Visible = isVisible;
            txtMargin1.Visible = isVisible;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (Validation())
            {
                IsChanges = true;
                if (txtMargin2.Text == "")
                    txtMargin2.Text = "0.00";
                if (rbBrand.Checked == true)
                {
                    for (int i = 0; i < gvRateSetting.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(gvRateSetting.Rows[i].Cells[ColIndex.Select].FormattedValue) == true)
                        {
                            gvRateSetting.Rows[i].Cells[ColIndex.Margin].Value = txtMargin2.Text;
                            gvRateSetting.Rows[i].Cells[ColIndex.Select].Value = false;
                        }
                    }
                }
                else if (rbCategory.Checked == true)
                {
                    for (int i = 0; i < dgCategory.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(dgCategory.Rows[i].Cells[ColIndex.Select].FormattedValue) == true)
                        {
                            dgCategory.Rows[i].Cells[ColIndex.Margin].Value = txtMargin2.Text;
                            dgCategory.Rows[i].Cells[ColIndex.Select].Value = false;
                        }
                    }
                }
                chkSelect.Checked = false;

            }

            else
            {
                OMMessageBox.Show("No Item Selected to Apply Margin");
                txtMargin2.Focus();
            }
        }

        private bool Validation()
        {
            bool flag = false;
            if (rbBrand.Checked == true)
            {
                for (int i = 0; i < gvRateSetting.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(gvRateSetting.Rows[i].Cells[ColIndex.Select].FormattedValue) == true)
                    {
                        flag = true;
                        break;
                    }
                }
            }
            else if (rbCategory.Checked == true)
            {
                for (int i = 0; i < dgCategory.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(dgCategory.Rows[i].Cells[ColIndex.Select].FormattedValue) == true)
                    {
                        flag = true;
                        break;
                    }
                }
            }

            return flag;
        }

        private void dgCategory_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                e.SuppressKeyPress = true;
                txtMargin2.Focus();
            }
            if (dgCategory.CurrentCell.ColumnIndex == ColIndex.Margin)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    IsChanges = true;
                    e.SuppressKeyPress = true;
                    if (dgCategory.CurrentCell.RowIndex < dgCategory.Rows.Count - 1)
                    {
                        dgCategory.CurrentCell = dgCategory.Rows[dgCategory.CurrentCell.RowIndex + 1].Cells[ColIndex.Margin];
                    }
                }
            }
        }

        private void gvRateSetting_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                e.SuppressKeyPress = true;
                txtMargin2.Focus();
            }
            if (gvRateSetting.CurrentCell.ColumnIndex == ColIndex.Margin)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    IsChanges = true;
                    e.SuppressKeyPress = true;
                    if (gvRateSetting.CurrentCell.RowIndex < gvRateSetting.Rows.Count - 1)
                    {
                        gvRateSetting.CurrentCell = gvRateSetting.Rows[gvRateSetting.CurrentCell.RowIndex + 1].Cells[ColIndex.Margin];
                    }
                }
            }
        }

        private void chkSelect_CheckedChanged(object sender, EventArgs e)
        {
            if (rbBrand.Checked == true)
            {
                for (int i = 0; i < gvRateSetting.Rows.Count; i++)
                {
                    gvRateSetting.Rows[i].Cells[ColIndex.Select].Value = chkSelect.Checked;
                }
            }
            else if (rbCategory.Checked == true)
            {
                for (int i = 0; i < dgCategory.Rows.Count; i++)
                {
                    dgCategory.Rows[i].Cells[ColIndex.Select].Value = chkSelect.Checked;
                }
            }

        }

        private void dgCategory_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == ColIndex.Margin)
            {
                if (dgCategory.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString() == "")
                    dgCategory.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "0.00";
            }
        }

        private void dgCategory_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            TextBox txt = (TextBox)e.Control;
            if (dgCategory.CurrentCell.ColumnIndex == ColIndex.Margin)
            {
                TextBox txt1 = (TextBox)e.Control;
                txt1.TextChanged += new EventHandler(txtMargin2_TextChanged);
            }
        }

        private void gvRateSetting_EditingControlShowing_1(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            TextBox txt = (TextBox)e.Control;
            if (gvRateSetting.CurrentCell.ColumnIndex == ColIndex.Margin)
            {
                TextBox txt1 = (TextBox)e.Control;
                txt1.TextChanged += new EventHandler(txtMargin2_TextChanged);
            }
        }

        private void dgCategory_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == ColIndex.Margin)
                IsChanges = true;
        }

        private void gvRateSetting_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == ColIndex.Margin)
                IsChanges = true;
        }


    }
}
