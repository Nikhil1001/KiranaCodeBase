﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Utilities
{
    public partial class TallyImportExport : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        DBMStockItems dbStockItems = new DBMStockItems();
        MStockItems mStockItems = new MStockItems();
        MStockBarcode mStockBarcode = new MStockBarcode();
        MRateSetting mRateSetting = new MRateSetting();

        DateTime FromDate, ToDate;



        public TallyImportExport()
        {
            InitializeComponent();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Excel Files (*.xml)|*.xml";
            ofd.Title = "Please select an Excel file to import Product Master Data.";
            ofd.Multiselect = false;

            if (ofd.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    txtImportPath.Text = ofd.FileName;
                }
                catch (Exception ex)
                {
                    OMMessageBox.Show(ex.Message);
                }
            }

        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            if (txtImportPath.Text.Trim() != "")
            {
                Display.WorkInProgress wip = new Kirana.Display.WorkInProgress();
                wip.StrStatus = "Import All Master in process, please wait.";
                wip.argument = txtImportPath.Text.Trim();
                wip.bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork_ItemMaster);
                wip.bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted_ItemMaster);
                ObjFunction.OpenForm(wip);
            }
        }

        #region Import All Master Methods

        private void bgWorker_DoWork_ItemMaster(object sender, DoWorkEventArgs e)
        {
            if (e.Argument.ToString() != "")
            {
                try
                {
                    Kirana.DBClasses.ExportAndImportTally Ex = new Kirana.DBClasses.ExportAndImportTally();
                    ENVELOPE regData = Ex.FromXml(e.Argument.ToString());

                    foreach (TALLYMESSAGE tmess in regData.BODY.IMPORTDATA.REQUESTDATA.TALLYMESSAGE)
                    {
                        STOCKITEM item = tmess.STOCKITEM;
                        VOUCHER vch = tmess.VOUCHER;
                        if (item != null)
                        {
                            SaveItems(item);
                        }
                        else if (vch != null)
                        {
 
                        }
                        

                        if (tmess.LEDGER != null)
                        {
                            SaveLedgers(tmess.LEDGER);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
        }

        private void bgWorker_RunWorkerCompleted_ItemMaster(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                OMMessageBox.Show(e.Error.Message);
            }
            else
            {
                OMMessageBox.Show("Item Master Saved Successfully...");
            }
        }

        private long GetStateNo(string StateName)
        {
            long cnt = ObjQry.ReturnLong("Select ISNULL(StateNo,0) From MState Where StateName='" + StateName + "' ", CommonFunctions.ConStr);
            if (cnt != 0)
                return cnt;
            else
                return ObjQry.ReturnLong("Select StateNo From MCompanyNo", CommonFunctions.ConStr);
        }

        private void SaveLedgers(LEDGER LG)
        {
            long GroupNo = (LG.PARENT == "Sundry Debtors") ? GroupType.SundryDebtors : GroupType.SundryCreditors;
            long cnt = ObjQry.ReturnLong("Select Count(*) From MLedger Where LedgerName='" + LG.NAME + "' and GroupNo=" + GroupNo + " ", CommonFunctions.ConStr);
            if (cnt != 0)
                return;

            DBMLedger dbMLedger = new DBMLedger();
            MLedger mLedger = new MLedger();
            mLedger.LedgerNo = 0;
            mLedger.LedgerUserNo = "";
            mLedger.ContactPerson = "";
            mLedger.IsActive = true;
            mLedger.InvFlag = true;
            mLedger.IsEnroll = true;
            mLedger.MaintainBillByBill = false;
            mLedger.LangLedgerName = "";
            mLedger.GroupNo = GroupNo;
            mLedger.LedgerName = LG.NAME.Trim();
            mLedger.CompanyNo = DBGetVal.CompanyNo;
            mLedger.UserID = DBGetVal.UserID;
            mLedger.UserDate = DateTime.Now;
            dbMLedger.AddMLedger(mLedger);

            MLedgerDetails mLedgerDetails = new MLedgerDetails();
            mLedgerDetails.StateNo = GetStateNo(LG.LEDSTATENAME.Trim());
            mLedgerDetails.MobileNo1 = LG.LEDGERPHONE;
            mLedgerDetails.Address = "";
            foreach (ADDRESS ad in LG.ADDRESS)
            {
                mLedgerDetails.Address = mLedgerDetails.Address + "\n" + ad.address;
            }
            mLedgerDetails.CSTNo = LG.PARTYGSTIN;

            mLedgerDetails.PinCode = "";
            mLedgerDetails.PhNo1 = "";
            mLedgerDetails.PhNo2 = "";
            mLedgerDetails.MobileNo2 = "";
            mLedgerDetails.EmailID = "";
            mLedgerDetails.DOB = Convert.ToDateTime("1-1-1900");
            mLedgerDetails.PANNo = "";
            mLedgerDetails.VATNo = "";
            mLedgerDetails.LangAddress = mLedgerDetails.Address;
            mLedgerDetails.UserID = DBGetVal.UserID;
            mLedgerDetails.UserDate = DBGetVal.ServerTime.Date;
            dbMLedger.AddMLedgerDetails(mLedgerDetails);

            if (dbMLedger.ExecuteNonQueryStatements() == false)
            {
                throw new Exception("Ledger Not Save :" + DBMStockItems.strerrormsg);
            }

        }


        #region Item Master Save Methods

        private long GetBrandNo(string StockGroupName)
        {
            long cnt = ObjQry.ReturnLong("Select ISNULL(StockGroupNo,0) From MStockGroup Where StockGroupName='" + StockGroupName + "' ", CommonFunctions.ConStr);
            if (cnt != 0)
                return cnt;

            DBMStockGroup dbMStockGroup = new DBMStockGroup();
            MStockGroup mstockgroup = new MStockGroup();

            mstockgroup.StockGroupName = StockGroupName;
            mstockgroup.ControlGroup = 3;
            mstockgroup.ControlSubGroup = 3;
            mstockgroup.LanguageName = "";
            mstockgroup.IsActive = true;
            mstockgroup.UserDate = DBGetVal.ServerTime;
            mstockgroup.UserId = DBGetVal.UserID;

            if (!dbMStockGroup.AddMStockGroup(mstockgroup))
            {
                throw new Exception("Error while inserting New record in StockGroup Table" + mstockgroup.msg);
            }
            else
            {
                return ObjQry.ReturnLong("Select ISNULL(StockGroupNo,0) From MStockGroup Where StockGroupName='" + StockGroupName + "' ", CommonFunctions.ConStr);
            }


        }

        private long GetUOMNo(string UOMName)
        {
            long cnt = ObjQry.ReturnLong("Select ISNULL(UOMNo,0) From MUOM Where UOMName='" + UOMName + "' ", CommonFunctions.ConStr);
            if (cnt != 0)
                return cnt;

            DBMUOM dbmuom = new DBMUOM();
            MUOM muom = new MUOM();

            muom.UOMName = UOMName;
            muom.UOMShortCode = UOMName;
            muom.UserDate = DateTime.Now;

            if (!dbmuom.AddMUOM(muom))
            {
                throw new Exception("Error while inserting New record in MUOM Table !!! Error Message = " + muom.msg);
            }
            else
            {
                return ObjQry.ReturnLong("Select ISNULL(UOMNo,0) From MUOM Where UOMName='" + UOMName + "' ", CommonFunctions.ConStr);
            }

        }

        private void SaveItems(STOCKITEM item)
        {
            long cnt = ObjQry.ReturnLong("Select Count(*) From MStockItems Where ItemName='" + item.NAME + "' ", CommonFunctions.ConStr);
            if (cnt != 0)
                return;
            if (item.GSTDETAILS.STATEWISEDETAILS == null)
                return;

            dbStockItems = new DBMStockItems();
            #region MStockItems
            MStockItems mStockItems = new MStockItems();
            mStockItems.ItemName = item.NAME;
            mStockItems.ItemShortCode = item.NAME;
            mStockItems.FKStockDeptNo = 10;
            mStockItems.GroupNo = GetBrandNo(item.PARENT);
            mStockItems.GroupNo1 = 8;
            mStockItems.FkCategoryNo = 1;
            mStockItems.UOMDefault = GetUOMNo(item.BASEUNITS);
            mStockItems.UOMPrimary = mStockItems.UOMDefault;
            mStockItems.ShortCode = item.ALTERID;
            mStockItems.LangFullDesc = "";
            mStockItems.LangShortDesc = "";
            mStockItems.FKStockLocationNo = 1;
            mStockItems.FkDepartmentNo = 1;
            mStockItems.IsActive = true;
            mStockItems.IsFixedBarcode = true;
            mStockItems.FkStockGroupTypeNo = 1;
            mStockItems.FactorVal = 1;
            mStockItems.GodownNo = 2;
            mStockItems.DiscountType = 1;
            mStockItems.IsQtyRead = false;
            mStockItems.CompanyNo = 1;
            mStockItems.UserId = DBGetVal.UserID;
            mStockItems.UserDate = DBGetVal.ServerTime;
            dbStockItems.AddMStockItems(mStockItems);
            #endregion

            #region MStockBarcode
            MStockBarcode mStockBarcode = new MStockBarcode();
            mStockBarcode.Barcode = item.ALTERID.Trim();
            mStockBarcode.CompanyNo = 1;
            mStockBarcode.UserID = DBGetVal.UserID;
            mStockBarcode.UserDate = DBGetVal.ServerTime;
            #endregion

            #region MRateSetting
            dbStockItems.AddMStockBarcode(mStockBarcode);

            //need to check multiple MRP found if so insert multiple here
            MRateSetting mRateSetting = new MRateSetting();
            mRateSetting.PkSrNo = 0;
            mRateSetting.UOMNo = mStockItems.UOMDefault;
            mRateSetting.ASaleRate = item.SALESLIST.CLASSRATE;
            mRateSetting.BSaleRate = item.SALESLIST.CLASSRATE;
            mRateSetting.PurRate = item.PURCHASELIST.CLASSRATE;
            mRateSetting.MRP = mRateSetting.ASaleRate;
            mRateSetting.FromDate = Convert.ToDateTime("01-01-1900");
            mRateSetting.StockConversion = 1;
            mRateSetting.MKTQty = 1;
            mRateSetting.IsActive = true;
            mRateSetting.CompanyNo = 1;
            mRateSetting.UserID = DBGetVal.UserID;
            mRateSetting.UserDate = DBGetVal.ServerTime;
            dbStockItems.AddMRateSetting2(mRateSetting);

            #endregion

            #region Save Tax details

            #region GST Tax Details

            #region Fetch GST Tax Details using HSN Code
            string sql;
            string strHSNCode = item.GSTDETAILS.HSNCODE;

            string GSTPercent = "0";
            
            foreach (RATEDETAILS IGST in item.GSTDETAILS.STATEWISEDETAILS.RATEDETAILS)
            {
                if (IGST.GSTRATEDUTYHEAD == "Integrated Tax")
                    GSTPercent = (IGST.GSTRATE == "") ? "0" : IGST.GSTRATE;
            }
            DataTable dtItemTaxSetting_Sales_Gst = null, dtItemTaxSetting_Purchase_Gst = null;


            sql = "SELECT MItemTaxSetting.* " +
                     " FROM MItemTaxSetting " +
                     " WHERE   MItemTaxSetting.Percentage = " + GSTPercent + " " +
                         " AND MItemTaxSetting.TransactionTypeNo = " + GroupType.SalesAccount + " " +
                         " AND MItemTaxSetting.TaxTypeNo = " + GroupType.GST + " " +
                         " AND MItemTaxSetting.IsActive='True' ";
            dtItemTaxSetting_Sales_Gst = ObjFunction.GetDataView(sql).Table;
            sql = "SELECT MItemTaxSetting.* " +
                        " FROM MItemTaxSetting " +
                        " WHERE   MItemTaxSetting.Percentage = " + GSTPercent + " " +
                            " AND MItemTaxSetting.TransactionTypeNo = " + GroupType.PurchaseAccount + " " +
                            " AND MItemTaxSetting.TaxTypeNo = " + GroupType.GST + " " +
                            " AND MItemTaxSetting.IsActive='True' ";
            dtItemTaxSetting_Purchase_Gst = ObjFunction.GetDataView(sql).Table;

            #endregion

            if (dtItemTaxSetting_Sales_Gst != null && dtItemTaxSetting_Purchase_Gst != null)
            {
                for (int g = 0; g < dtItemTaxSetting_Sales_Gst.Rows.Count; g++)
                {
                    MItemTaxInfo mItemTaxInfo = new MItemTaxInfo();
                    mItemTaxInfo.TaxLedgerNo = Convert.ToInt64(dtItemTaxSetting_Sales_Gst.Rows[g]["TaxLedgerNo"].ToString());
                    mItemTaxInfo.SalesLedgerNo = Convert.ToInt64(dtItemTaxSetting_Sales_Gst.Rows[g]["SalesLedgerNo"].ToString());
                    mItemTaxInfo.Percentage = Convert.ToDouble(dtItemTaxSetting_Sales_Gst.Rows[g]["Percentage"].ToString());
                    mItemTaxInfo.FKTaxSettingNo = Convert.ToInt64(dtItemTaxSetting_Sales_Gst.Rows[g]["PkSrNo"].ToString());
                    mItemTaxInfo.TaxTypeNo = Convert.ToInt64(dtItemTaxSetting_Sales_Gst.Rows[g]["TaxTypeNo"].ToString());
                    mItemTaxInfo.TransactionTypeNo = Convert.ToInt64(dtItemTaxSetting_Sales_Gst.Rows[g]["TransactionTypeNo"].ToString());
                    mItemTaxInfo.HSNCode = strHSNCode;
                    mItemTaxInfo.IsActive = true;
                    mItemTaxInfo.HSNNo = 0;
                    mItemTaxInfo.FromDate = DBGetVal.ServerTime;
                    mItemTaxInfo.IGSTPercent = Convert.ToDouble(dtItemTaxSetting_Sales_Gst.Rows[g]["IGSTPercent"].ToString());
                    mItemTaxInfo.CGSTPercent = Convert.ToDouble(dtItemTaxSetting_Sales_Gst.Rows[g]["CGSTPercent"].ToString());
                    mItemTaxInfo.SGSTPercent = Convert.ToDouble(dtItemTaxSetting_Sales_Gst.Rows[g]["SGSTPercent"].ToString());
                    mItemTaxInfo.UTGSTPercent = Convert.ToDouble(dtItemTaxSetting_Sales_Gst.Rows[g]["UTGSTPercent"].ToString());
                    mItemTaxInfo.CessPercent = Convert.ToDouble(dtItemTaxSetting_Sales_Gst.Rows[g]["CessPercent"].ToString());
                    mItemTaxInfo.UserDate = DBGetVal.ServerTime;
                    mItemTaxInfo.UserID = DBGetVal.UserID;
                    dbStockItems.AddMItemTaxInfo(mItemTaxInfo);
                    if (mItemTaxInfo.FromDate <= DBGetVal.ServerTime.Date)
                    {
                        break;
                    }
                }

                for (int g = 0; g < dtItemTaxSetting_Purchase_Gst.Rows.Count; g++)
                {
                    MItemTaxInfo mItemTaxInfo = new MItemTaxInfo();
                    mItemTaxInfo.TaxLedgerNo = Convert.ToInt64(dtItemTaxSetting_Purchase_Gst.Rows[g]["TaxLedgerNo"].ToString());
                    mItemTaxInfo.SalesLedgerNo = Convert.ToInt64(dtItemTaxSetting_Purchase_Gst.Rows[g]["SalesLedgerNo"].ToString());
                    mItemTaxInfo.Percentage = Convert.ToDouble(dtItemTaxSetting_Purchase_Gst.Rows[g]["Percentage"].ToString());
                    mItemTaxInfo.FKTaxSettingNo = Convert.ToInt64(dtItemTaxSetting_Purchase_Gst.Rows[g]["PkSrNo"].ToString());
                    mItemTaxInfo.TaxTypeNo = Convert.ToInt64(dtItemTaxSetting_Purchase_Gst.Rows[g]["TaxTypeNo"].ToString());
                    mItemTaxInfo.TransactionTypeNo = Convert.ToInt64(dtItemTaxSetting_Purchase_Gst.Rows[g]["TransactionTypeNo"].ToString());
                    mItemTaxInfo.HSNCode = strHSNCode;
                    mItemTaxInfo.FromDate = DBGetVal.ServerTime;
                    mItemTaxInfo.IsActive = true;
                    mItemTaxInfo.UserDate = DBGetVal.ServerTime;
                    mItemTaxInfo.UserID = DBGetVal.UserID;
                    mItemTaxInfo.IGSTPercent = Convert.ToDouble(dtItemTaxSetting_Purchase_Gst.Rows[g]["IGSTPercent"].ToString());
                    mItemTaxInfo.CGSTPercent = Convert.ToDouble(dtItemTaxSetting_Purchase_Gst.Rows[g]["CGSTPercent"].ToString());
                    mItemTaxInfo.SGSTPercent = Convert.ToDouble(dtItemTaxSetting_Purchase_Gst.Rows[g]["SGSTPercent"].ToString());
                    mItemTaxInfo.UTGSTPercent = Convert.ToDouble(dtItemTaxSetting_Purchase_Gst.Rows[g]["UTGSTPercent"].ToString());
                    mItemTaxInfo.CessPercent = Convert.ToDouble(dtItemTaxSetting_Purchase_Gst.Rows[g]["CessPercent"].ToString());
                    dbStockItems.AddMItemTaxInfo(mItemTaxInfo);
                    if (mItemTaxInfo.FromDate <= DBGetVal.ServerTime.Date)
                    {
                        break;
                    }
                }
            }
            #endregion

            #endregion

            if (dbStockItems.ExecuteNonQueryStatements_ImportItemMaster() == false)
            {
                throw new Exception("Items Not Save :" + DBMStockItems.strerrormsg);
            }
        }

        #endregion

        #endregion

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void bgWorker_DoWork_Voucher(object sender, DoWorkEventArgs e)
        {

            try
            {
                Kirana.DBClasses.ExportAndImportTally Ex = new Kirana.DBClasses.ExportAndImportTally();
                REQUESTDATA regData = new REQUESTDATA();
                LoadData(regData, FromDate, ToDate);
                e.Result = regData;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }


        private void bgWorker_RunWorkerCompleted_Voucher(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                OMMessageBox.Show(e.Error.Message);
            }
            else
            {
                try
                {
                    REQUESTDATA regData = (e.Result as REQUESTDATA);
                    FolderBrowserDialog fbd = new FolderBrowserDialog();
                    if (fbd.ShowDialog() == DialogResult.OK)
                    {
                        string destFileName = fbd.SelectedPath;
                        if (!destFileName.EndsWith("\\"))
                        {
                            destFileName += "\\";
                        }
                        destFileName += "DayBook_" + DateTime.Now.Date.ToString("dd-MMM-yyyy") + ".XML";
                        //System.IO.File.WriteAllText(destFileName, e.Result.ToString());
                        Kirana.DBClasses.XmlHelper.ToXmlFile(regData, destFileName);
                    }
                }
                catch (Exception exs)
                {
                    OMMessageBox.Show(exs.Message);
                }

            }
            OMMessageBox.Show("Item Master Saved Successfully...");
        }
        #region Voucher Export

        private void LoadData(REQUESTDATA regData, DateTime FromDate, DateTime ToDate)
        {
            LoadCustomerData(regData, FromDate, ToDate);
            LoadSupplierData(regData, FromDate, ToDate);
            LoadBankData(regData, FromDate, ToDate);
            LoadDiscountData(regData, FromDate, ToDate);
            LoadChargesData(regData, FromDate, ToDate);
            LoadRoundData(regData, FromDate, ToDate);
            LoadSaleAccountData(regData, FromDate, ToDate);
            LoadPurchaeAccountData(regData, FromDate, ToDate);
            LoadDutiesAndTaxesData(regData, FromDate, ToDate);

            LoadUOMData(regData, FromDate, ToDate);
            LoadStockGroupData(regData, FromDate, ToDate);
            LoadStockItemsData(regData, FromDate, ToDate);

            LoadSaleBillData(regData, FromDate, ToDate);
            LoadPurchaseBillData(regData, FromDate, ToDate);

            LoadSaleReturnData(regData, FromDate, ToDate);
            LoadPurchaseReturnData(regData, FromDate, ToDate);

            LoadSaleReciptsData(regData, FromDate, ToDate);
            LoadPurchaseReciptsData(regData, FromDate, ToDate);
            // LoadPurchaseReturnReciptsData(regData, FromDate, ToDate);
        }

        #region Customer,Supplier,Bank,Discount,Round Method

        private void LoadCustomerData(REQUESTDATA regData, DateTime FromDate, DateTime ToDate)
        {
            DataTable dtCust = featchLedgerData(FromDate, ToDate, 26);
            foreach (DataRow items in dtCust.Rows)
            {
                regData.TALLYMESSAGE.Add(featchLedgerData(items));
            }
        }

        private void LoadSupplierData(REQUESTDATA regData, DateTime FromDate, DateTime ToDate)
        {
            DataTable dtCust = featchLedgerData(FromDate, ToDate, 22);
            foreach (DataRow items in dtCust.Rows)
            {
                regData.TALLYMESSAGE.Add(featchLedgerData(items));
            }
        }

        private void LoadBankData(REQUESTDATA regData, DateTime FromDate, DateTime ToDate)
        {
            DataTable dtCust = featchLedgerData(FromDate, ToDate, 28);
            foreach (DataRow items in dtCust.Rows)
            {
                regData.TALLYMESSAGE.Add(featchLedgerData(items));
            }
        }

        private void LoadDiscountData(REQUESTDATA regData, DateTime FromDate, DateTime ToDate)
        {
            DataTable dtCust = featchLedgerData(FromDate, ToDate, 13);
            foreach (DataRow items in dtCust.Rows)
            {
                regData.TALLYMESSAGE.Add(featchLedgerData(items));
            }
        }

        private void LoadChargesData(REQUESTDATA regData, DateTime FromDate, DateTime ToDate)
        {
            DataTable dtCust = featchLedgerData(FromDate, ToDate, 15);
            foreach (DataRow items in dtCust.Rows)
            {
                regData.TALLYMESSAGE.Add(featchLedgerData(items));
            }
        }

        private void LoadRoundData(REQUESTDATA regData, DateTime FromDate, DateTime ToDate)
        {
            DataTable dtCust = featchLedgerData(FromDate, ToDate, 14);
            foreach (DataRow items in dtCust.Rows)
            {
                regData.TALLYMESSAGE.Add(featchLedgerData(items));
            }
        }

        private DataTable featchCustomerData(DateTime FromDate, DateTime ToDate, long GroupNo)
        {
            string Sql = " SELECT  Distinct   TVoucherDetails.LedgerNo " +
                " FROM TVoucherEntry INNER JOIN " +
                " TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo " +
                " And TVoucherDetails.Srno=501 INNER JOIN " +
                " MLedger ON TVoucherDetails.LedgerNo = MLedger.LedgerNo" +
                " Where TVoucherEntry.VoucherDate>='" + FromDate.Date.ToString("dd-MMM-yyyy") + "' And TVoucherEntry.VoucherDate<='" + ToDate.Date.ToString("dd-MMM-yyyy") + "'  " +
                " And MLedger.GroupNo=" + GroupNo + " And TVoucherEntry.TaxTypeNo=38 ";
            return ObjFunction.GetDataView(Sql).Table;
        }

        private DataTable featchLedgerData(DateTime FromDate, DateTime ToDate, long GroupNo)
        {
            string Sql = "SELECT     MLedger.LedgerNo, MLedger.LedgerName, MLedger.GroupNo, ISNull( MLedgerDetails.Address,'') AS Address, " +
               " MLedgerDetails.StateNo, MLedgerDetails.PinCode,ISNULL(MLedgerDetails.MobileNo1,'') As MobileNo1, MLedgerDetails.EmailID,  " +
               " MLedgerDetails.PANNo,ISNULL( MLedgerDetails.CSTNo,'') As CSTNo,ISNULL(MState.StateName,'') AS StateName ," +
               "ISNULL( MGroup.GroupName ,'') AS GroupName " +
               "FROM  MGroup INNER JOIN  " +
               " MLedger ON MGroup.GroupNo = MLedger.GroupNo LEFT OUTER JOIN " +
               " MState RIGHT OUTER JOIN " +
               " MLedgerDetails ON MState.StateNo = MLedgerDetails.StateNo ON MLedger.LedgerNo = MLedgerDetails.LedgerNo  INNER JOIN  " +
               " TVoucherDetails ON TVoucherDetails.LedgerNo = MLedger.LedgerNo  INNER JOIN  " +
               " TVoucherEntry  ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo " +
               " Where TVoucherEntry.VoucherDate>='" + FromDate.Date.ToString("dd-MMM-yyyy") + "' And TVoucherEntry.VoucherDate<='" + ToDate.Date.ToString("dd-MMM-yyyy") + "' " +
               " And MLedger.GroupNo=" + GroupNo + "  And TVoucherEntry.TaxTypeNo= 38 " +
               " Group By   MLedger.LedgerNo, MLedger.LedgerName, MLedger.GroupNo, Address,  MLedgerDetails.StateNo, MLedgerDetails.PinCode,MobileNo1, MLedgerDetails.EmailID, " +
               "    MLedgerDetails.PANNo, CSTNo,StateName , GroupName ";
            return ObjFunction.GetDataView(Sql).Table;
        }

        private TALLYMESSAGE featchLedgerData(DataRow dtData)
        {
            TALLYMESSAGE tallyMSG = new TALLYMESSAGE();
            LEDGER leg = new LEDGER();
            leg.NAME = dtData["LedgerName"].ToString();

            ADDRESS LstA = new ADDRESS();
            LstA.address = dtData["Address"].ToString();
            leg.ADDRESS.Add(LstA);

            leg.COUNTRYNAME = "India";
            leg.PARTYGSTIN = dtData["CSTNo"].ToString();
            leg.LEDSTATENAME = dtData["StateName"].ToString();
            leg.PARENT = dtData["GroupName"].ToString();
            LANGUAGENAME langName = new LANGUAGENAME();
            langName.NAME = leg.NAME;
            leg.LANGUAGENAME = langName;

            tallyMSG.LEDGER = leg;
            tallyMSG.UDF = "TallyUDF";

            return tallyMSG;
        }

        #endregion

        #region Sale And Purchase Account Details

        private void LoadSaleAccountData(REQUESTDATA regData, DateTime FromDate, DateTime ToDate)
        {
            DataTable dtCust = featchAccountData(FromDate, ToDate, 10);
            foreach (DataRow items in dtCust.Rows)
            {
                regData.TALLYMESSAGE.Add(featchAccountData(Convert.ToInt64(items[0].ToString())));
            }
        }

        private void LoadPurchaeAccountData(REQUESTDATA regData, DateTime FromDate, DateTime ToDate)
        {
            DataTable dtCust = featchAccountData(FromDate, ToDate, 11);
            foreach (DataRow items in dtCust.Rows)
            {
                regData.TALLYMESSAGE.Add(featchAccountData(Convert.ToInt64(items[0].ToString())));
            }
        }

        private void LoadDutiesAndTaxesData(REQUESTDATA regData, DateTime FromDate, DateTime ToDate)
        {
            featchDutiesAndTaxesData(regData, FromDate, ToDate);
        }

        private DataTable featchAccountData(DateTime FromDate, DateTime ToDate, long GroupNo)
        {
            string Sql = " SELECT DISTINCT MItemTaxInfo.SalesLedgerNo " +
                        " FROM         MItemTaxInfo INNER JOIN " +
                        " TVoucherEntry INNER JOIN " +
                        " TStock ON TVoucherEntry.PkVoucherNo = TStock.FKVoucherNo ON MItemTaxInfo.PkSrNo = TStock.FkItemTaxInfo INNER JOIN " +
                        " MLedger ON MItemTaxInfo.SalesLedgerNo = MLedger.LedgerNo " +
                        " Where TVoucherEntry.VoucherDate>='" + FromDate.Date.ToString("dd-MMM-yyyy") + "' And TVoucherEntry.VoucherDate<='" + ToDate.Date.ToString("dd-MMM-yyyy") + "' And (MItemTaxInfo.TaxTypeNo = 38)   And MLedger.GroupNo=" + GroupNo + " ";
            return ObjFunction.GetDataView(Sql).Table;
        }

        private TALLYMESSAGE featchAccountData(long LedgerNo)
        {
            string Sql = " SELECT MLedger.LedgerNo, MLedger.LedgerName, MLedger.GroupNo, ISNULL(MGroup.GroupName, '') AS GroupName, MItemTaxSetting.Percentage, MItemTaxSetting.IGSTPercent, " +
                        " MItemTaxSetting.CGSTPercent, MItemTaxSetting.SGSTPercent, MItemTaxSetting.UTGSTPercent, MItemTaxSetting.CessPercent " +
                        " FROM MGroup INNER JOIN " +
                        " MLedger ON MGroup.GroupNo = MLedger.GroupNo INNER JOIN " +
                        " MItemTaxSetting ON MLedger.LedgerNo = MItemTaxSetting.SalesLedgerNo " +
                        " WHERE     (MLedger.LedgerNo = " + LedgerNo + ")";
            DataTable dtData = ObjFunction.GetDataView(Sql).Table;
            TALLYMESSAGE tallyMSG = new TALLYMESSAGE();
            if (dtData.Rows.Count != 0)
            {
                LEDGER leg = new LEDGER();
                leg.NAME = (Convert.ToDouble(dtData.Rows[0]["Percentage"].ToString()) == 0) ? ((Convert.ToInt64(dtData.Rows[0]["GroupNo"].ToString()) == 10) ? "SALE GST EXEMPTED" : "PURCHASE GST EXEMPTED") : dtData.Rows[0]["LedgerName"].ToString();
                leg.PARENT = dtData.Rows[0]["GroupName"].ToString();
                leg.GSTAPPLICABLE = "Applicable";
                leg.TAXTYPE = "Others";
                leg.GSTTYPEOFSUPPLY = "Goods";

                LANGUAGENAME langName = new LANGUAGENAME();
                langName.NAME = leg.NAME;
                leg.LANGUAGENAME = langName;

                GSTDETAILS gstDetails = new GSTDETAILS();
                STATEWISEDETAILS gstStateDetails = new STATEWISEDETAILS();
                //IGST
                RATEDETAILS gstrates = new RATEDETAILS();
                gstrates.GSTRATE = (Convert.ToDouble(dtData.Rows[0]["IGSTPercent"].ToString()) == 0) ? "" : dtData.Rows[0]["IGSTPercent"].ToString();
                gstrates.GSTRATEDUTYHEAD = "Integrated Tax";
                gstStateDetails.RATEDETAILS.Add(gstrates);
                //CGST
                gstrates = new RATEDETAILS();
                gstrates.GSTRATE = (Convert.ToDouble(dtData.Rows[0]["CGSTPercent"].ToString()) == 0) ? "" : dtData.Rows[0]["CGSTPercent"].ToString();
                gstrates.GSTRATEDUTYHEAD = "Central Tax";
                gstStateDetails.RATEDETAILS.Add(gstrates);
                //SGST
                gstrates = new RATEDETAILS();
                gstrates.GSTRATE = (Convert.ToDouble(dtData.Rows[0]["SGSTPercent"].ToString()) == 0) ? "" : dtData.Rows[0]["SGSTPercent"].ToString();
                gstrates.GSTRATEDUTYHEAD = "State Tax";
                gstStateDetails.RATEDETAILS.Add(gstrates);
                //Cess
                gstrates = new RATEDETAILS();
                gstrates.GSTRATE = (Convert.ToDouble(dtData.Rows[0]["CessPercent"].ToString()) == 0) ? "" : dtData.Rows[0]["CessPercent"].ToString();
                gstrates.GSTRATEDUTYHEAD = "Cess";
                gstStateDetails.RATEDETAILS.Add(gstrates);


                gstDetails.STATEWISEDETAILS = gstStateDetails;

                leg.GSTDETAILS = gstDetails;
                tallyMSG.LEDGER = leg;
                tallyMSG.UDF = "TallyUDF";
            }
            return tallyMSG;
        }

        private void featchDutiesAndTaxesData(REQUESTDATA regData, DateTime FromDate, DateTime ToDate)
        {
            string Sql = " SELECT    'IGST '+Cast(TStock.IGSTPercent As varchar)+' %' As IGSTPercent, " +
                      " 'CGST '+Cast(TStock.CGSTPercent As varchar)+' %' As CGSTPercent, " +
                      " 'SGST '+Cast(TStock.SGSTPercent As varchar)+' %' As SGSTPercent, " +
                      " (Case When(TStock.CessPercent<>0) Then 'CESS '+Cast(TStock.CessPercent As varchar)+' %' Else '' end) As CessPercent " +
                      " FROM         TStock INNER JOIN " +
                      " TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo " +
                      " WHERE    TVoucherEntry.VoucherDate>='" + FromDate.Date.ToString("dd-MMM-yyyy") + "' And TVoucherEntry.VoucherDate<='" + ToDate.Date.ToString("dd-MMM-yyyy") + "'  And " +
                      " (TStock.TaxPercentage <> 0) And TVoucherEntry.taxTypeNo=38 " +
                      " GROUP BY   TStock.IGSTPercent, TStock.CGSTPercent, TStock.SGSTPercent, TStock.CessPercent ";
            DataTable dtData = ObjFunction.GetDataView(Sql).Table;

            var dataIGST = (from r in dtData.AsEnumerable()
                            select r["IGSTPercent"]).Distinct().ToList();
            foreach (var name in dataIGST)
            {
                if (name.ToString().Trim() == "")
                    continue;

                TALLYMESSAGE TM = featchDutiesAndTaxesData(name.ToString(), "Integrated Tax");
                regData.TALLYMESSAGE.Add(TM);
            }

            var dataCGST = (from r in dtData.AsEnumerable()
                            select r["CGSTPercent"]).Distinct().ToList();
            foreach (var name in dataCGST)
            {
                if (name.ToString().Trim() == "")
                    continue;

                TALLYMESSAGE TM = featchDutiesAndTaxesData(name.ToString(), "Central Tax");
                regData.TALLYMESSAGE.Add(TM);
            }

            var dataSGST = (from r in dtData.AsEnumerable()
                            select r["SGSTPercent"]).Distinct().ToList();
            foreach (var name in dataSGST)
            {
                if (name.ToString().Trim() == "")
                    continue;

                TALLYMESSAGE TM = featchDutiesAndTaxesData(name.ToString(), "State Tax");
                regData.TALLYMESSAGE.Add(TM);
            }
            var dataCESS = (from r in dtData.AsEnumerable()
                            select r["CessPercent"]).Distinct().ToList();
            foreach (var name in dataCESS)
            {
                if (name.ToString().Trim() == "")
                    continue;
                TALLYMESSAGE TM = featchDutiesAndTaxesData(name.ToString(), "Cess");
                regData.TALLYMESSAGE.Add(TM);
            }



        }

        private TALLYMESSAGE featchDutiesAndTaxesData(string LedgerName, string GSTHead)
        {
            TALLYMESSAGE tallyMSG = new TALLYMESSAGE();
            LEDGER leg = new LEDGER();
            leg.NAME = LedgerName;
            leg.PARENT = "Duties & Taxes";
            leg.GSTAPPLICABLE = "Applicable";
            leg.TAXTYPE = "GST";
            leg.GSTTYPEOFSUPPLY = "Goods";
            leg.GSTDUTYHEAD = GSTHead;
            leg.ROUNDINGMETHOD = "Normal Rounding";

            LANGUAGENAME langName = new LANGUAGENAME();
            langName.NAME = leg.NAME;
            leg.LANGUAGENAME = langName;

            tallyMSG.LEDGER = leg;
            tallyMSG.UDF = "TallyUDF";

            return tallyMSG;
        }

        #endregion

        #region Stock Items Data

        private void LoadUOMData(REQUESTDATA regData, DateTime FromDate, DateTime ToDate)
        {
            DataTable dtCust = featchUOMData(FromDate, ToDate);
            foreach (DataRow items in dtCust.Rows)
            {
                regData.TALLYMESSAGE.Add(featchUOMData(Convert.ToInt64(items[0].ToString())));
            }
        }

        private DataTable featchUOMData(DateTime FromDate, DateTime ToDate)
        {
            string Sql = " SELECT     MStockItems.UOMDefault " +
                        " FROM         TStock INNER JOIN " +
                        " TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN " +
                        " MStockItems ON TStock.ItemNo = MStockItems.ItemNo  " +
                        " WHERE  TVoucherEntry.VoucherDate>='" + FromDate.Date.ToString("dd-MMM-yyyy") + "' And TVoucherEntry.VoucherDate<='" + ToDate.Date.ToString("dd-MMM-yyyy") + "' And TVoucherEntry.TaxTypeNo=38" +
                        " Group By MStockItems.UOMDefault ";
            return ObjFunction.GetDataView(Sql).Table;
        }

        private TALLYMESSAGE featchUOMData(long UomNo)
        {
            string Sql = "Select  UOMName From MUOM Where UOMNo=" + UomNo + " ";
            DataTable dtData = ObjFunction.GetDataView(Sql).Table;
            TALLYMESSAGE tallyMSG = new TALLYMESSAGE();
            if (dtData.Rows.Count != 0)
            {
                UNIT uom = new UNIT();
                uom._NAME = dtData.Rows[0]["UOMName"].ToString();
                uom.NAME = dtData.Rows[0]["UOMName"].ToString();

                tallyMSG.UNIT = uom;
                tallyMSG.UDF = "TallyUDF";
            }
            return tallyMSG;
        }

        private void LoadStockGroupData(REQUESTDATA regData, DateTime FromDate, DateTime ToDate)
        {
            DataTable dtCust = featchStockGroupData(FromDate, ToDate);
            foreach (DataRow items in dtCust.Rows)
            {
                regData.TALLYMESSAGE.Add(featchStockGroupData(Convert.ToInt64(items[0].ToString())));
            }
        }

        private DataTable featchStockGroupData(DateTime FromDate, DateTime ToDate)
        {
            string Sql = " SELECT     MStockItems.GroupNo " +
                        " FROM         TStock INNER JOIN " +
                        " TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN " +
                        " MStockItems ON TStock.ItemNo = MStockItems.ItemNo  " +
                        " WHERE  TVoucherEntry.VoucherDate>='" + FromDate.Date.ToString("dd-MMM-yyyy") + "' And TVoucherEntry.VoucherDate<='" + ToDate.Date.ToString("dd-MMM-yyyy") + "' And TVoucherEntry.TaxTypeNo=38" +
                        " Group By MStockItems.GroupNo ";
            return ObjFunction.GetDataView(Sql).Table;
        }

        private TALLYMESSAGE featchStockGroupData(long GroupNo)
        {
            string Sql = "Select  StockGroupName From MStockGroup Where StockGroupno=" + GroupNo + " ";
            DataTable dtData = ObjFunction.GetDataView(Sql).Table;
            TALLYMESSAGE tallyMSG = new TALLYMESSAGE();
            if (dtData.Rows.Count != 0)
            {
                STOCKGROUP stockgroup = new STOCKGROUP();
                stockgroup.NAME = dtData.Rows[0]["StockGroupName"].ToString().Trim();

                LANGUAGENAME langName = new LANGUAGENAME();
                langName.NAME = stockgroup.NAME;
                stockgroup.LANGUAGENAME = langName;

                tallyMSG.STOCKGROUP = stockgroup;
                tallyMSG.UDF = "TallyUDF";
            }
            return tallyMSG;
        }

        private void LoadStockItemsData(REQUESTDATA regData, DateTime FromDate, DateTime ToDate)
        {
            DataTable dtCust = featchStockItemsData(FromDate, ToDate);
            foreach (DataRow items in dtCust.Rows)
            {
                regData.TALLYMESSAGE.Add(featchStockItemsData(items));
            }
        }

        private DataTable featchStockItemsData(DateTime FromDate, DateTime ToDate)
        {
            string Sql = " SELECT DISTINCT MStockItems.ItemName, MStockGroup.StockGroupName, MUOM.UOMName, MItemTaxInfo.HSNCode, " +
                         " MItemTaxInfo.IGSTPercent, MItemTaxInfo.CGSTPercent, MItemTaxInfo.SGSTPercent, MItemTaxInfo.CessPercent,MItemTaxInfo.PkSrNo " +
                       " FROM         MStockItems INNER JOIN " +
                       " MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo INNER JOIN " +
                       " MUOM ON MStockItems.UOMDefault = MUOM.UOMNo INNER JOIN " +
                       " TStock ON MStockItems.ItemNo = TStock.ItemNo INNER JOIN " +
                       " TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN " +
                       " MItemTaxInfo ON TStock.FkItemTaxInfo = MItemTaxInfo.PkSrNo " +
                       " WHERE  TVoucherEntry.VoucherDate>='" + FromDate.Date.ToString("dd-MMM-yyyy") + "' And TVoucherEntry.VoucherDate<='" + ToDate.Date.ToString("dd-MMM-yyyy") + "' " +
                       " order by MItemTaxInfo.PkSrNo ";
            return ObjFunction.GetDataView(Sql).Table;
        }

        private TALLYMESSAGE featchStockItemsData(DataRow dtData)
        {

            TALLYMESSAGE tallyMSG = new TALLYMESSAGE();
            STOCKITEM items = new STOCKITEM();
            items.NAME = dtData["ItemName"].ToString();
            items.PARENT = dtData["StockGroupName"].ToString().Trim();
            items.BASEUNITS = dtData["UOMName"].ToString();

            LANGUAGENAME langName = new LANGUAGENAME();
            langName.NAME = items.NAME;
            items.LANGUAGENAME = langName;

            GSTDETAILS gst = new GSTDETAILS();
            gst.HSNCODE = dtData["HSNCode"].ToString();

            STATEWISEDETAILS gstDetails = new STATEWISEDETAILS();
            //IGST
            RATEDETAILS gstrates = new RATEDETAILS();
            gstrates.GSTRATE = (Convert.ToDouble(dtData["IGSTPercent"].ToString()) == 0) ? "" : dtData["IGSTPercent"].ToString();
            gstrates.GSTRATEDUTYHEAD = "Integrated Tax";
            gstDetails.RATEDETAILS.Add(gstrates);
            //CGST
            gstrates = new RATEDETAILS();
            gstrates.GSTRATE = (Convert.ToDouble(dtData["CGSTPercent"].ToString()) == 0) ? "" : dtData["CGSTPercent"].ToString();
            gstrates.GSTRATEDUTYHEAD = "Central Tax";
            gstDetails.RATEDETAILS.Add(gstrates);
            //SGST
            gstrates = new RATEDETAILS();
            gstrates.GSTRATE = (Convert.ToDouble(dtData["SGSTPercent"].ToString()) == 0) ? "" : dtData["SGSTPercent"].ToString();
            gstrates.GSTRATEDUTYHEAD = "State Tax";
            gstDetails.RATEDETAILS.Add(gstrates);
            //Cess
            gstrates = new RATEDETAILS();
            gstrates.GSTRATE = (Convert.ToDouble(dtData["CessPercent"].ToString()) == 0) ? "" : dtData["CessPercent"].ToString();
            gstrates.GSTRATEDUTYHEAD = "Cess";
            gstDetails.RATEDETAILS.Add(gstrates);

            gst.STATEWISEDETAILS = gstDetails;

            items.GSTDETAILS = gst;
            tallyMSG.STOCKITEM = items;
            tallyMSG.UDF = "TallyUDF";

            return tallyMSG;
        }


        #endregion

        #region Sale Bill Data

        private void LoadSaleBillData(REQUESTDATA regData, DateTime FromDate, DateTime ToDate)
        {
            DataTable dtCust = featchSaleBillData(FromDate, ToDate);
            foreach (DataRow items in dtCust.Rows)
            {
                regData.TALLYMESSAGE.Add(featchSaleBillData(items));
            }
        }

        private DataTable featchSaleBillData(DateTime FromDate, DateTime ToDate)
        {
            string Sql = " SELECT     TVoucherEntry.VoucherUserNo,TVoucherEntry.VoucherDate, TVoucherEntry.VoucherTime, TVoucherEntry.BilledAmount, TVoucherEntry.Remark,  " +
            " TVoucherEntry.IsCancel, MPayType.PayTypeName, TVoucherEntry.RateTypeNo,  " +
            " TVoucherEntry.MixMode, ISNULL(TVoucherDetails.Debit, 0) AS DiscAmt ," +
            " MLedger.LedgerName,ISNull( MLedgerDetails.Address,'') AS Address,ISNull(MState.StateName,'') AS StateName, MPayType.PKPayTypeNo, TVoucherEntry.PkVoucherNo " +
            " ,IsNull(MLedgerDetails.CSTNo,'') AS GSTNo,(Debit+Credit) as Amount " +
            " FROM         TVoucherEntry INNER JOIN " +
            " MPayType ON TVoucherEntry.PayTypeNo = MPayType.PKPayTypeNo LEFT OUTER JOIN  " +
            " MLedger INNER JOIN " +
            " TVoucherDetails ON MLedger.LedgerNo = TVoucherDetails.LedgerNo ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo RIGHT OUTER JOIN " +
            " MState RIGHT OUTER JOIN " +
            " MLedgerDetails ON MState.StateNo = MLedgerDetails.StateNo ON TVoucherDetails.SrNo = 501 AND MLedger.LedgerNo = MLedgerDetails.LedgerNo " +
            " WHERE     (TVoucherEntry.VoucherTypeCode = 15)  And " +
            " TVoucherEntry.VoucherDate>='" + FromDate.Date.ToString("dd-MMM-yyyy") + "' And TVoucherEntry.VoucherDate<='" + ToDate.Date.ToString("dd-MMM-yyyy") + "' And TVoucherEntry.TaxTypeNo=38 " +
            " order by TVoucherEntry.VoucherDate ";

            return ObjFunction.GetDataView(Sql).Table;
        }

        private TALLYMESSAGE featchSaleBillData(DataRow dtData)
        {

            TALLYMESSAGE tallyMSG = new TALLYMESSAGE();

            VOUCHER vch = new VOUCHER();
            vch.VCHTYPE = "Sales";
            vch.OBJVIEW = "Invoice Voucher View";
            vch.VOUCHERNUMBER = dtData["VoucherUserNo"].ToString();
            vch.REFERENCE = dtData["VoucherUserNo"].ToString();

            ADDRESS LstA = new ADDRESS();
            LstA.address = dtData["Address"].ToString();
            vch.ADDRESS.Add(LstA);

            vch.DATE = Convert.ToDateTime(dtData["VoucherDate"].ToString()).ToString("yyyy-MM-dd").Replace("-", "");
            vch.STATENAME = dtData["StateName"].ToString();
            vch.PARTYGSTIN = dtData["GSTNo"].ToString();
            vch.PARTYLEDGERNAME = dtData["LedgerName"].ToString();
            vch.VOUCHERTYPENAME = "Sales";
            vch.PARTYNAME = dtData["LedgerName"].ToString();
            vch.BASICBASEPARTYNAME = dtData["LedgerName"].ToString();
            vch.PERSISTEDVIEW = "Invoice Voucher View";
            vch.PLACEOFSUPPLY = vch.STATENAME;
            vch.CONSIGNEEGSTIN = vch.PARTYGSTIN;
            vch.BASICBUYERNAME = vch.BASICBASEPARTYNAME;
            vch.BASICDATETIMEOFINVOICE = Convert.ToDateTime(dtData["VoucherDate"].ToString()).ToString("dd-MMM-yyyy") + " at " + Convert.ToDateTime(dtData["VoucherDate"].ToString()).ToString("t");
            vch.BASICDATETIMEOFREMOVAL = vch.BASICDATETIMEOFINVOICE;

            #region Ledger Details List

            List<LEDGERENTRIES> lst = new List<LEDGERENTRIES>();

            LEDGERENTRIES led = new LEDGERENTRIES();
            led.LEDGERNAME = vch.PARTYNAME;
            led.AMOUNT = "-" + dtData["Amount"].ToString();
            led.ISDEEMEDPOSITIVE = "Yes";
            led.ISLASTDEEMEDPOSITIVE = "Yes";

            BILLALLOCATIONS ballc = new BILLALLOCATIONS();
            ballc.Name = dtData["VoucherUserNo"].ToString();
            ballc.BILLTYPE = "New Ref";
            ballc.AMOUNT = led.AMOUNT;

            led.BILLALLOCATIONS = ballc;

            lst.Add(led);
            featchAllGSTTaxDetails(lst, Convert.ToInt64(dtData["PkVoucherNo"].ToString()));

            vch.LEDGERENTRIES = lst;
            #endregion

            List<ALLINVENTORYENTRIES> lstItems = featchStockAllItemsDetails(Convert.ToInt64(dtData["PkVoucherNo"].ToString()));
            vch.ALLINVENTORYENTRIES = lstItems;


            tallyMSG.VOUCHER = vch;
            tallyMSG.UDF = "TallyUDF";

            return tallyMSG;
        }

        private void featchAllGSTTaxDetails(List<LEDGERENTRIES> lst, long PkSrNo)
        {
            string StrRound = "9";
            string StrDisc = "3";
            string StrChar = "4";

            string sql = "SELECT  SUM( TStock.IGSTAmount) as IGSTAmount,SUM(TStock.CGSTAmount) AS CGSTAmount,SUM( TStock.SGSTAmount) AS SGSTAmount, " +
                       " SUM( TStock.CessAmount) AS CessAmount,TStock.TaxPercentage," +
                       " 'IGST '+Cast(TStock.IGSTPercent As varchar)+' %' As IGSTPercent, " +
                       " 'CGST '+Cast(TStock.CGSTPercent As varchar)+' %' As CGSTPercent, " +
                       " 'SGST '+Cast(TStock.SGSTPercent As varchar)+' %' As SGSTPercent, " +
                       " (Case When(TStock.CessPercent<>0) Then 'CESS '+Cast(TStock.CessPercent As varchar)+' %' Else '' end) As CessPercent " +
                       " FROM TStock INNER JOIN MItemTaxInfo ON TStock.FkItemTaxInfo = MItemTaxInfo.PkSrNo " +
                       " Where TStock.FkVoucherNo=" + PkSrNo + " And TStock.TaxPercentage<>0 " +
                       " Group by TStock.IGSTPercent, TStock.TaxPercentage,TStock.CGSTPercent,TStock.SGSTPercent,TStock.CessPercent ";
            DataTable dt = ObjFunction.GetDataView(sql).Table;
            foreach (DataRow dr in dt.Rows)
            {
                if (Convert.ToDouble(dr["IGSTAmount"].ToString()) != 0)
                {
                    LEDGERENTRIES items = new LEDGERENTRIES();
                    items.ROUNDTYPE = "Normal Rounding";
                    items.METHODTYPE = "GST";
                    items.LEDGERNAME = dr["IGSTPercent"].ToString();
                    items.AMOUNT = items.VATEXPAMOUNT = dr["IGSTAmount"].ToString();
                    items.ISDEEMEDPOSITIVE = "No";
                    items.ISLASTDEEMEDPOSITIVE = "No";
                    lst.Add(items);
                }
                if (Convert.ToDouble(dr["CGSTAmount"].ToString()) != 0)
                {
                    LEDGERENTRIES items = new LEDGERENTRIES();
                    items.ROUNDTYPE = "Normal Rounding";
                    items.METHODTYPE = "GST";
                    items.LEDGERNAME = dr["CGSTPercent"].ToString();
                    items.AMOUNT = items.VATEXPAMOUNT = dr["CGSTAmount"].ToString();
                    items.ISDEEMEDPOSITIVE = "No";
                    items.ISLASTDEEMEDPOSITIVE = "No";
                    lst.Add(items);
                }
                if (Convert.ToDouble(dr["SGSTAmount"].ToString()) != 0)
                {
                    LEDGERENTRIES items = new LEDGERENTRIES();
                    items.ROUNDTYPE = "Normal Rounding";
                    items.METHODTYPE = "GST";
                    items.LEDGERNAME = dr["SGSTPercent"].ToString();
                    items.AMOUNT = items.VATEXPAMOUNT = dr["SGSTAmount"].ToString();
                    items.ISDEEMEDPOSITIVE = "No";
                    items.ISLASTDEEMEDPOSITIVE = "No";
                    lst.Add(items);
                }
                if (Convert.ToDouble(dr["CessAmount"].ToString()) != 0)
                {
                    LEDGERENTRIES items = new LEDGERENTRIES();
                    items.ROUNDTYPE = "Normal Rounding";
                    items.METHODTYPE = "GST";
                    items.LEDGERNAME = dr["CessPercent"].ToString();
                    items.AMOUNT = items.VATEXPAMOUNT = dr["CessAmount"].ToString();
                    items.ISDEEMEDPOSITIVE = "No";
                    items.ISLASTDEEMEDPOSITIVE = "No";
                    lst.Add(items);
                }

            }

            string RoundAmt = ObjQry.ReturnString("Select (isNull(SUM(Credit - Debit),0)) FROM TVoucherDetails Where LedgerNo=" + StrRound + " And FkVoucherNo=" + PkSrNo + "", CommonFunctions.ConStr);
            string ChargAmt = ObjQry.ReturnString("Select (isNull(SUM(Credit - Debit),0))  FROM TVoucherDetails Where LedgerNo=" + StrChar + " And FkVoucherNo=" + PkSrNo + " ", CommonFunctions.ConStr);
            string DiscAmt = ObjQry.ReturnString("Select (isNull(SUM(Debit - Credit),0)*-1)  FROM TVoucherDetails Where LedgerNo=" + StrDisc + " And FkVoucherNo=" + PkSrNo + " ", CommonFunctions.ConStr);

            if (Convert.ToDouble(DiscAmt) != 0)
            {
                LEDGERENTRIES items = new LEDGERENTRIES();
                items.ROUNDTYPE = "Normal Rounding";
                items.LEDGERNAME = ObjQry.ReturnString("Select LedgerName From MLedger Where LedgerNo=" + StrDisc + "", CommonFunctions.ConStr);
                items.AMOUNT = items.VATEXPAMOUNT = DiscAmt;
                if (Convert.ToDouble(DiscAmt) < 0)
                {
                    items.ISDEEMEDPOSITIVE = "Yes";
                    items.ISLASTDEEMEDPOSITIVE = "Yes";
                }
                else
                {
                    items.ISDEEMEDPOSITIVE = "No";
                    items.ISLASTDEEMEDPOSITIVE = "No";
                }
                lst.Add(items);
            }

            if (Convert.ToDouble(ChargAmt) != 0)
            {
                LEDGERENTRIES items = new LEDGERENTRIES();
                items.ROUNDTYPE = "Normal Rounding";
                items.LEDGERNAME = ObjQry.ReturnString("Select LedgerName From MLedger Where LedgerNo=" + StrChar + "", CommonFunctions.ConStr);
                items.AMOUNT = items.VATEXPAMOUNT = ChargAmt;
                if (Convert.ToDouble(ChargAmt) < 0)
                {
                    items.ISDEEMEDPOSITIVE = "Yes";
                    items.ISLASTDEEMEDPOSITIVE = "Yes";
                }
                else
                {
                    items.ISDEEMEDPOSITIVE = "No";
                    items.ISLASTDEEMEDPOSITIVE = "No";
                }
                lst.Add(items);
            }
            if (Convert.ToDouble(RoundAmt) != 0)
            {
                LEDGERENTRIES items = new LEDGERENTRIES();
                items.ROUNDTYPE = "Normal Rounding";
                items.LEDGERNAME = ObjQry.ReturnString("Select LedgerName From MLedger Where LedgerNo=" + StrRound + "", CommonFunctions.ConStr);
                items.AMOUNT = items.VATEXPAMOUNT = RoundAmt;
                if (Convert.ToDouble(RoundAmt) < 0)
                {
                    items.ISDEEMEDPOSITIVE = "Yes";
                    items.ISLASTDEEMEDPOSITIVE = "Yes";
                }
                else
                {
                    items.ISDEEMEDPOSITIVE = "No";
                    items.ISLASTDEEMEDPOSITIVE = "No";
                }
                lst.Add(items);
            }

        }

        private List<ALLINVENTORYENTRIES> featchStockAllItemsDetails(long PkSrNo)
        {
            List<ALLINVENTORYENTRIES> lst = new List<ALLINVENTORYENTRIES>();
            string sql = " SELECT MStockGroup.StockGroupName, MStockItems.ItemName, TStock.NetRate, TStock.NetAmount, TStock.Quantity, " +
                         " (Case When(MItemTaxInfo.Percentage=0) Then 'SALE GST EXEMPTED' Else MLedger.LedgerName End) AS LedgerName, MItemTaxInfo.Percentage ," +
                         " MUOM.UOMName " +
                         " FROM         MLedger INNER JOIN " +
                         " MItemTaxInfo ON MLedger.LedgerNo = MItemTaxInfo.SalesLedgerNo INNER JOIN " +
                         " TStock INNER JOIN MStockItems ON TStock.ItemNo = MStockItems.ItemNo INNER JOIN " +
                         " MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo INNER JOIN " +
                         " MUOM ON TStock.FkUomNo = MUOM.UOMNo ON MItemTaxInfo.PkSrNo = TStock.FkItemTaxInfo " +
                         " WHERE     (TStock.FKVoucherNo = " + PkSrNo + ") ";
            DataTable dt = ObjFunction.GetDataView(sql).Table;
            foreach (DataRow dr in dt.Rows)
            {
                ALLINVENTORYENTRIES items = new ALLINVENTORYENTRIES();
                items.STOCKITEMNAME = dr["ItemName"].ToString();
                items.RATE = dr["NetRate"].ToString() + "/" + dr["UOMName"].ToString();
                items.AMOUNT = dr["NetAmount"].ToString();
                items.ACTUALQTY = items.BILLEDQTY = dr["Quantity"].ToString();
                items.ISDEEMEDPOSITIVE = "No";
                items.ISLASTDEEMEDPOSITIVE = "No";

                ACCOUNTINGALLOCATIONS aleg = new ACCOUNTINGALLOCATIONS();
                aleg.LEDGERNAME = dr["LedgerName"].ToString();
                aleg.AMOUNT = items.AMOUNT;
                aleg.CLASSRATE = "100.0000";
                aleg.ISDEEMEDPOSITIVE = "No";
                aleg.ISLASTDEEMEDPOSITIVE = "No";

                List<RATEDETAILS> lstrate = new List<RATEDETAILS>();
                RATEDETAILS rategst = new RATEDETAILS();
                rategst.GSTRATEDUTYHEAD = "Integrated Tax";
                lstrate.Add(rategst);

                rategst = new RATEDETAILS();
                rategst.GSTRATEDUTYHEAD = "Central Tax";
                lstrate.Add(rategst);

                rategst = new RATEDETAILS();
                rategst.GSTRATEDUTYHEAD = "State Tax";
                lstrate.Add(rategst);

                rategst = new RATEDETAILS();
                rategst.GSTRATEDUTYHEAD = "Cess";
                lstrate.Add(rategst);

                aleg.RATEDETAILS = lstrate;

                items.ACCOUNTINGALLOCATIONS = aleg;

                lst.Add(items);
            }
            return lst;
        }

        #endregion

        #region Sales Recipts

        private void LoadSaleReciptsData(REQUESTDATA regData, DateTime FromDate, DateTime ToDate)
        {
            DataTable dtCust = featchSaleReciptsData(FromDate, ToDate);
            foreach (DataRow items in dtCust.Rows)
            {
                regData.TALLYMESSAGE.Add(featchSaleReciptsData(items));
            }
        }

        private DataTable featchSaleReciptsData(DateTime FromDate, DateTime ToDate)
        {
            string Sql = " SELECT TVoucherEntry.PkVoucherNo, TVoucherEntry.VoucherUserNo, TVoucherEntry.VoucherDate," +
                         " TVoucherEntry.Narration, TVoucherEntry.BilledAmount, MPayType.PayTypeName,MLedger.LedgerName " +
                         " FROM MLedger INNER JOIN TVoucherDetails ON MLedger.LedgerNo = TVoucherDetails.LedgerNo INNER JOIN " +
                         " TVoucherEntry INNER JOIN MPayType ON TVoucherEntry.PayTypeNo = MPayType.PKPayTypeNo ON  " +
                         " TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo  AND TVoucherDetails.SrNo = 501  " +
                         " WHERE     (TVoucherEntry.VoucherTypeCode = 30)  And " +
                         " TVoucherEntry.VoucherDate>='" + FromDate.Date.ToString("dd-MMM-yyyy") + "' And TVoucherEntry.VoucherDate<='" + ToDate.Date.ToString("dd-MMM-yyyy") + "' " +
                         " order by TVoucherEntry.VoucherDate ";

            return ObjFunction.GetDataView(Sql).Table;
        }

        private List<ALLLEDGERENTRIES> FeatchVoucherPayTypeDetails(long PkSrNo)
        {
            List<ALLLEDGERENTRIES> lst = new List<ALLLEDGERENTRIES>();
            string Sql = " SELECT     MLedger.LedgerName,( TVoucherDetails.Debit + TVoucherDetails.Credit ) AS PartAmount, TVoucherDetails.SrNo, " +
                        " TVoucherDetails.Narration, TVoucherEntry.VoucherUserNo, TVoucherPayTypeDetails.Amount,MPayType.PayTypeName, " +
                        "IsNull(TVoucherChqCreditDetails.ChequeNo,'' ) as ChequeNo, TVoucherChqCreditDetails.ChequeDate, TVoucherChqCreditDetails.Amount AS Expr1, " +
                // " TVoucherChqCreditDetails.Remark ,"+
                        " '' AS Remark ," +
                        " IsNull( MOtherBank.BankName ,'') As BankName,IsNull( TVoucherChqCreditDetails.CreditCardNo,'') AS CreditCardNo ," +
                        " TVoucherChqCreditDetails.BranchNo " +
                        " FROM MOtherBank RIGHT OUTER JOIN " +
                        " TVoucherChqCreditDetails ON MOtherBank.BankNo = TVoucherChqCreditDetails.BankNo RIGHT OUTER JOIN " +
                        " MPayType INNER JOIN " +
                        " TVoucherPayTypeDetails ON MPayType.PKPayTypeNo = TVoucherPayTypeDetails.FKPayTypeNo INNER JOIN " +
                        " TVoucherDetails INNER JOIN " +
                        " MLedger ON TVoucherDetails.LedgerNo = MLedger.LedgerNo ON TVoucherPayTypeDetails.FKReceiptVoucherNo = TVoucherDetails.FkVoucherNo LEFT OUTER JOIN " +
                        " TVoucherEntry ON TVoucherPayTypeDetails.FKSalesVoucherNo = TVoucherEntry.PkVoucherNo ON TVoucherChqCreditDetails.FKVoucherNo = TVoucherPayTypeDetails.FKReceiptVoucherNo " +
                        " WHERE     (TVoucherDetails.FkVoucherNo = " + PkSrNo + ") ";

            DataTable dt = ObjFunction.GetDataView(Sql).Table;
            foreach (DataRow dr in dt.Rows)
            {
                ALLLEDGERENTRIES LstLedger = new ALLLEDGERENTRIES();
                LstLedger.LEDGERNAME = dr["LedgerName"].ToString();
                if (dr["SrNo"].ToString() == "501")
                {
                    LstLedger.ISDEEMEDPOSITIVE = "No";
                    LstLedger.ISLASTDEEMEDPOSITIVE = "No";

                    LstLedger.AMOUNT = dr["PartAmount"].ToString();
                    BILLALLOCATIONS billall = new BILLALLOCATIONS();
                    billall.AMOUNT = dr["PartAmount"].ToString();
                    billall.Name = dr["VoucherUserNo"].ToString();
                    billall.BILLTYPE = "Agst Ref";
                    LstLedger.BILLALLOCATIONS.Add(billall);
                }
                else
                {
                    LstLedger.ISDEEMEDPOSITIVE = "Yes";
                    LstLedger.ISLASTDEEMEDPOSITIVE = "Yes";
                    LstLedger.AMOUNT = "-" + dr["PartAmount"].ToString();
                    if (dr["PayTypeName"].ToString() != "Cash")
                    {
                        BANKALLOCATIONS billall = new BANKALLOCATIONS();
                        billall.AMOUNT = dr["PartAmount"].ToString();
                        billall.BANKNAME = dr["BankName"].ToString();
                        billall.UNIQUEREFERENCENUMBER = dr["ChequeNo"].ToString();
                        billall.TRANSFERMODE = dr["PayTypeName"].ToString();
                        LstLedger.BANKALLOCATIONS = billall;
                    }
                }
                lst.Add(LstLedger);
            }
            return lst;

        }

        private List<ALLLEDGERENTRIES> FeatchVoucherRefDetails(long PkSrNo)
        {

            if (ObjQry.ReturnLong("Select  Count(*) As Cnt From TVoucherRefDetails As Tvr Where Tvr.FkVoucherTrnno IN(Select PkVoucherTrnno From TVoucherDetails Where FkVoucherNo=" + PkSrNo + ") ", CommonFunctions.ConStr) == 0)
                return null;

            List<ALLLEDGERENTRIES> lst = new List<ALLLEDGERENTRIES>();
            string Sql = " SELECT  MPayType.PayTypeName, TVoucherEntry.Narration, TVoucherEntry.BilledAmount, TVoucherEntry.Remark, " +
                        " TVoucherChqCreditDetails.ChequeNo, TVoucherChqCreditDetails.ChequeDate,   " +
                        " MOtherBank.BankName, TVoucherChqCreditDetails.BranchNo, TVoucherChqCreditDetails.CreditCardNo, " +
                        " TVoucherChqCreditDetails.Amount, '' AS CheckRemark ,MLedger.LedgerName ,TVoucherDetails.SrNo" +
                        " FROM         MOtherBank RIGHT OUTER JOIN " +
                        " TVoucherEntry INNER JOIN " +
                        " MPayType ON TVoucherEntry.PayTypeNo = MPayType.PKPayTypeNo INNER JOIN " +
                        " MLedger INNER JOIN " +
                        " TVoucherDetails ON MLedger.LedgerNo = TVoucherDetails.LedgerNo ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo LEFT OUTER JOIN " +
                        " TVoucherChqCreditDetails ON TVoucherEntry.PkVoucherNo = TVoucherChqCreditDetails.FKVoucherNo ON MOtherBank.BankNo = TVoucherChqCreditDetails.BankNo " +
                        " WHERE     (TVoucherEntry.PkVoucherNo = " + PkSrNo + ")";
            DataTable dt = ObjFunction.GetDataView(Sql).Table;
            foreach (DataRow dr in dt.Rows)
            {
                ALLLEDGERENTRIES LstLedger = new ALLLEDGERENTRIES();
                LstLedger.LEDGERNAME = dr["LedgerName"].ToString();
                if (dr["SrNo"].ToString() == "501")
                {
                    LstLedger.AMOUNT = dr["BilledAmount"].ToString();
                    Sql = " Select Amount,0 AS DiscAmt,(Select VoucherUserNo From TVoucherEntry  Where PkVoucherNo in (Select FkVoucherNo From TVoucherDetails Where PkVoucherTrnno in (Select FkVoucherTrnno From TVoucherRefDetails Where RefNo=Tvr.RefNo And TypeOfRef=3) And SrNo=501)) as BillNo " +
                        " From TVoucherRefDetails As Tvr Where Tvr.FkVoucherTrnno IN(Select PkVoucherTrnno " +
                        " From TVoucherDetails Where FkVoucherNo=" + PkSrNo + ") ";
                    DataTable dtRef = ObjFunction.GetDataView(Sql).Table;
                    foreach (DataRow drref in dtRef.Rows)
                    {
                        LstLedger.ISDEEMEDPOSITIVE = "No";
                        LstLedger.ISLASTDEEMEDPOSITIVE = "No";

                        BILLALLOCATIONS billall = new BILLALLOCATIONS();
                        billall.AMOUNT = drref["Amount"].ToString();
                        billall.Name = drref["BillNo"].ToString();
                        billall.BILLTYPE = "Agst Ref";
                        LstLedger.BILLALLOCATIONS.Add(billall);
                    }
                }
                else
                {
                    LstLedger.AMOUNT = "-" + dr["BilledAmount"].ToString();
                    LstLedger.ISDEEMEDPOSITIVE = "Yes";
                    LstLedger.ISLASTDEEMEDPOSITIVE = "Yes";

                    if (dr["PayTypeName"].ToString() != "Cash")
                    {
                        BANKALLOCATIONS billall = new BANKALLOCATIONS();
                        billall.AMOUNT = dr["Amount"].ToString();
                        billall.BANKNAME = dr["BankName"].ToString();
                        billall.UNIQUEREFERENCENUMBER = dr["ChequeNo"].ToString();
                        billall.TRANSFERMODE = dr["PayTypeName"].ToString();
                        LstLedger.BANKALLOCATIONS = billall;
                    }
                }
                lst.Add(LstLedger);
            }
            return lst;

        }

        private TALLYMESSAGE featchSaleReciptsData(DataRow dtData)
        {

            TALLYMESSAGE tallyMSG = new TALLYMESSAGE();

            VOUCHER vch = new VOUCHER();
            vch.VCHTYPE = "Receipt";
            vch.OBJVIEW = "Accounting Voucher View";
            vch.VOUCHERNUMBER = dtData["VoucherUserNo"].ToString();
            vch.DATE = Convert.ToDateTime(dtData["VoucherDate"].ToString()).ToString("yyyy-MM-dd").Replace("-", "");
            vch.PARTYLEDGERNAME = dtData["LedgerName"].ToString();
            vch.VOUCHERTYPENAME = "Receipt";
            vch.PARTYNAME = dtData["LedgerName"].ToString();
            vch.BASICBASEPARTYNAME = dtData["LedgerName"].ToString();
            vch.PERSISTEDVIEW = "Accounting Voucher View";
            vch.PLACEOFSUPPLY = vch.STATENAME;
            vch.CONSIGNEEGSTIN = vch.PARTYGSTIN;

            //if VoucherPayType Details Not Null

            List<ALLLEDGERENTRIES> lstPayDtls = FeatchVoucherPayTypeDetails(Convert.ToInt64(dtData["PkVoucherNo"].ToString()));
            if (lstPayDtls.Count != 0)
            {
                vch.ALLLEDGERENTRIES = lstPayDtls;
            }
            else
            {
                List<ALLLEDGERENTRIES> lstRef = FeatchVoucherRefDetails(Convert.ToInt64(dtData["PkVoucherNo"].ToString()));
                if (lstRef.Count != 0)
                {
                    vch.ALLLEDGERENTRIES = lstRef;
                }
            }

            tallyMSG.VOUCHER = vch;
            tallyMSG.UDF = "TallyUDF";

            return tallyMSG;
        }


        #endregion

        #region Purchase Bill Data

        private void LoadPurchaseBillData(REQUESTDATA regData, DateTime FromDate, DateTime ToDate)
        {
            DataTable dtCust = featchPurchaseBillData(FromDate, ToDate);
            foreach (DataRow items in dtCust.Rows)
            {
                regData.TALLYMESSAGE.Add(featchPurchaseBillData(items));
            }
        }

        private DataTable featchPurchaseBillData(DateTime FromDate, DateTime ToDate)
        {
            string Sql = " SELECT     TVoucherEntry.VoucherUserNo,TVoucherEntry.VoucherDate, TVoucherEntry.VoucherTime, TVoucherEntry.BilledAmount, TVoucherEntry.Remark,  " +
            " TVoucherEntry.IsCancel, MPayType.PayTypeName, TVoucherEntry.RateTypeNo,  " +
            " TVoucherEntry.MixMode, ISNULL(TVoucherDetails.Debit, 0) AS DiscAmt , " +
            " MLedger.LedgerName,ISNull( MLedgerDetails.Address,'') AS Address,ISNull(MState.StateName,'') AS StateName, MPayType.PKPayTypeNo, TVoucherEntry.PkVoucherNo " +
            " ,IsNull(MLedgerDetails.CSTNo,'') AS GSTNo,(Debit+Credit) as Amount,TVoucherEntry.Reference " +
            " FROM         MState RIGHT OUTER JOIN " +
            " MLedgerDetails ON MState.StateNo = MLedgerDetails.StateNo RIGHT OUTER JOIN " +
            " MLedger INNER JOIN " +
            " TVoucherDetails ON MLedger.LedgerNo = TVoucherDetails.LedgerNo ON MLedgerDetails.LedgerNo = MLedger.LedgerNo RIGHT OUTER JOIN " +
            " TVoucherEntry INNER JOIN MPayType ON TVoucherEntry.PayTypeNo = MPayType.PKPayTypeNo ON " +
            " TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo AND TVoucherDetails.SrNo = 501 " +
            " WHERE     (TVoucherEntry.VoucherTypeCode = 9)  And " +
            " TVoucherEntry.VoucherDate>='" + FromDate.Date.ToString("dd-MMM-yyyy") + "' And TVoucherEntry.VoucherDate<='" + ToDate.Date.ToString("dd-MMM-yyyy") + "' And TVoucherEntry.TaxTypeNo=38" +
            " order by TVoucherEntry.VoucherDate ";

            return ObjFunction.GetDataView(Sql).Table;
        }

        private TALLYMESSAGE featchPurchaseBillData(DataRow dtData)
        {

            TALLYMESSAGE tallyMSG = new TALLYMESSAGE();

            VOUCHER vch = new VOUCHER();
            vch.VCHTYPE = "Purchase";
            vch.OBJVIEW = "Invoice Voucher View";
            vch.VOUCHERNUMBER = dtData["VoucherUserNo"].ToString();
            vch.REFERENCE = dtData["Reference"].ToString();
            vch.NARRATION = dtData["Remark"].ToString();

            ADDRESS add = new ADDRESS();
            add.address = dtData["Address"].ToString();
            vch.ADDRESS.Add(add);

            vch.DATE = Convert.ToDateTime(dtData["VoucherDate"].ToString()).ToString("yyyy-MM-dd").Replace("-", "");
            vch.STATENAME = dtData["StateName"].ToString();
            vch.PARTYGSTIN = dtData["GSTNo"].ToString();
            vch.PARTYLEDGERNAME = dtData["LedgerName"].ToString();
            vch.VOUCHERTYPENAME = "Purchase";
            vch.PARTYNAME = dtData["LedgerName"].ToString();
            vch.BASICBASEPARTYNAME = dtData["LedgerName"].ToString();
            vch.PERSISTEDVIEW = "Invoice Voucher View";
            vch.PLACEOFSUPPLY = vch.STATENAME;
            vch.CONSIGNEEGSTIN = vch.PARTYGSTIN;
            vch.BASICBUYERNAME = vch.BASICBASEPARTYNAME;
            vch.BASICDATETIMEOFINVOICE = Convert.ToDateTime(dtData["VoucherDate"].ToString()).ToString("dd-MMM-yyyy") + " at " + Convert.ToDateTime(dtData["VoucherDate"].ToString()).ToString("t");
            vch.BASICDATETIMEOFREMOVAL = vch.BASICDATETIMEOFINVOICE;

            #region Ledger Details List

            List<LEDGERENTRIES> lst = new List<LEDGERENTRIES>();

            LEDGERENTRIES led = new LEDGERENTRIES();
            led.LEDGERNAME = vch.PARTYNAME;
            led.AMOUNT = dtData["Amount"].ToString();
            led.ISDEEMEDPOSITIVE = "No";
            led.ISLASTDEEMEDPOSITIVE = "No";

            BILLALLOCATIONS ballc = new BILLALLOCATIONS();
            ballc.Name = dtData["Reference"].ToString();
            ballc.BILLTYPE = "New Ref";
            ballc.AMOUNT = led.AMOUNT;

            led.BILLALLOCATIONS = ballc;

            lst.Add(led);
            featchAllGSTTaxPurchaseDetails(lst, Convert.ToInt64(dtData["PkVoucherNo"].ToString()));

            vch.LEDGERENTRIES = lst;
            #endregion

            List<ALLINVENTORYENTRIES> lstItems = featchStockAllItemsDetails_purchase(Convert.ToInt64(dtData["PkVoucherNo"].ToString()));
            vch.ALLINVENTORYENTRIES = lstItems;


            tallyMSG.VOUCHER = vch;
            tallyMSG.UDF = "TallyUDF";

            return tallyMSG;
        }

        private void featchAllGSTTaxPurchaseDetails(List<LEDGERENTRIES> lst, long PkSrNo)
        {
            string StrRound = "9";
            string StrDisc = "3";
            string StrChar = "4";
            string StrDisDisc = "8";

            string sql = "SELECT  SUM( TStock.IGSTAmount) as IGSTAmount,SUM(TStock.CGSTAmount) AS CGSTAmount,SUM( TStock.SGSTAmount) AS SGSTAmount, " +
                       " SUM( TStock.CessAmount) AS CessAmount,TStock.TaxPercentage," +
                       " 'IGST '+Cast(TStock.IGSTPercent As varchar)+' %' As IGSTPercent, " +
                       " 'CGST '+Cast(TStock.CGSTPercent As varchar)+' %' As CGSTPercent, " +
                       " 'SGST '+Cast(TStock.SGSTPercent As varchar)+' %' As SGSTPercent, " +
                       " (Case When(TStock.CessPercent<>0) Then 'CESS '+Cast(TStock.CessPercent As varchar)+' %' Else '' end) As CessPercent " +
                       " FROM TStock INNER JOIN MItemTaxInfo ON TStock.FkItemTaxInfo = MItemTaxInfo.PkSrNo " +
                       " Where TStock.FkVoucherNo=" + PkSrNo + " And TStock.TaxPercentage<>0 " +
                       " Group by TStock.IGSTPercent, TStock.TaxPercentage,TStock.CGSTPercent,TStock.SGSTPercent,TStock.CessPercent ";
            DataTable dt = ObjFunction.GetDataView(sql).Table;
            foreach (DataRow dr in dt.Rows)
            {
                if (Convert.ToDouble(dr["IGSTAmount"].ToString()) != 0)
                {
                    LEDGERENTRIES items = new LEDGERENTRIES();
                    items.ROUNDTYPE = "Normal Rounding";
                    items.METHODTYPE = "GST";
                    items.LEDGERNAME = dr["IGSTPercent"].ToString();
                    items.AMOUNT = items.VATEXPAMOUNT = "-" + dr["IGSTAmount"].ToString();
                    items.ISDEEMEDPOSITIVE = "Yes";
                    items.ISLASTDEEMEDPOSITIVE = "Yes";
                    lst.Add(items);
                }
                if (Convert.ToDouble(dr["CGSTAmount"].ToString()) != 0)
                {
                    LEDGERENTRIES items = new LEDGERENTRIES();
                    items.ROUNDTYPE = "Normal Rounding";
                    items.METHODTYPE = "GST";
                    items.LEDGERNAME = dr["CGSTPercent"].ToString();
                    items.AMOUNT = items.VATEXPAMOUNT = "-" + dr["CGSTAmount"].ToString();
                    items.ISDEEMEDPOSITIVE = "Yes";
                    items.ISLASTDEEMEDPOSITIVE = "Yes";
                    lst.Add(items);
                }
                if (Convert.ToDouble(dr["SGSTAmount"].ToString()) != 0)
                {
                    LEDGERENTRIES items = new LEDGERENTRIES();
                    items.ROUNDTYPE = "Normal Rounding";
                    items.METHODTYPE = "GST";
                    items.LEDGERNAME = dr["SGSTPercent"].ToString();
                    items.AMOUNT = items.VATEXPAMOUNT = "-" + dr["SGSTAmount"].ToString();
                    items.ISDEEMEDPOSITIVE = "Yes";
                    items.ISLASTDEEMEDPOSITIVE = "Yes";
                    lst.Add(items);
                }
                if (Convert.ToDouble(dr["CessAmount"].ToString()) != 0)
                {
                    LEDGERENTRIES items = new LEDGERENTRIES();
                    items.ROUNDTYPE = "Normal Rounding";
                    items.METHODTYPE = "GST";
                    items.LEDGERNAME = dr["CessPercent"].ToString();
                    items.AMOUNT = items.VATEXPAMOUNT = "-" + dr["CessAmount"].ToString();
                    items.ISDEEMEDPOSITIVE = "Yes";
                    items.ISLASTDEEMEDPOSITIVE = "Yes";
                    lst.Add(items);
                }

            }


            string RoundAmt = ObjQry.ReturnString("Select (isNull(SUM(Credit - Debit),0)) FROM TVoucherDetails Where LedgerNo=" + StrRound + " And FkVoucherNo=" + PkSrNo + " ", CommonFunctions.ConStr);
            string ChargAmt = ObjQry.ReturnString("Select (isNull(SUM(Credit - Debit),0))  FROM TVoucherDetails Where LedgerNo=" + StrChar + " And FkVoucherNo=" + PkSrNo + " ", CommonFunctions.ConStr);
            string DiscAmt = ObjQry.ReturnString("Select (isNull(SUM(Debit - Credit),0)* -1)  FROM TVoucherDetails Where LedgerNo=" + StrDisc + " And SrNo Not in(510,509) And FkVoucherNo=" + PkSrNo + "", CommonFunctions.ConStr);
            string DisplayDiscAmt = ObjQry.ReturnString("Select (isNull(SUM(Debit - Credit),0)* -1)  FROM TVoucherDetails Where LedgerNo=" + StrDisDisc + " And FkVoucherNo=" + PkSrNo + " ", CommonFunctions.ConStr);

            if (Convert.ToDouble(DiscAmt) != 0)
            {
                LEDGERENTRIES items = new LEDGERENTRIES();
                items.ROUNDTYPE = "Normal Rounding";
                items.LEDGERNAME = ObjQry.ReturnString("Select LedgerName From MLedger Where LedgerNo=" + StrDisc + "", CommonFunctions.ConStr);
                items.AMOUNT = items.VATEXPAMOUNT = DiscAmt;
                if (Convert.ToDouble(DiscAmt) < 0)
                {
                    items.ISDEEMEDPOSITIVE = "Yes";
                    items.ISLASTDEEMEDPOSITIVE = "Yes";
                }
                else
                {
                    items.ISDEEMEDPOSITIVE = "No";
                    items.ISLASTDEEMEDPOSITIVE = "No";
                }
                lst.Add(items);
            }
            if (Convert.ToDouble(DisplayDiscAmt) != 0)
            {
                LEDGERENTRIES items = new LEDGERENTRIES();
                items.ROUNDTYPE = "Normal Rounding";
                items.LEDGERNAME = ObjQry.ReturnString("Select LedgerName From MLedger Where LedgerNo=" + StrDisDisc + "", CommonFunctions.ConStr);
                items.AMOUNT = items.VATEXPAMOUNT = DisplayDiscAmt;
                if (Convert.ToDouble(DisplayDiscAmt) < 0)
                {
                    items.ISDEEMEDPOSITIVE = "Yes";
                    items.ISLASTDEEMEDPOSITIVE = "Yes";
                }
                else
                {
                    items.ISDEEMEDPOSITIVE = "No";
                    items.ISLASTDEEMEDPOSITIVE = "No";
                }
                lst.Add(items);
            }

            if (Convert.ToDouble(ChargAmt) != 0)
            {
                LEDGERENTRIES items = new LEDGERENTRIES();
                items.ROUNDTYPE = "Normal Rounding";
                items.LEDGERNAME = ObjQry.ReturnString("Select LedgerName From MLedger Where LedgerNo=" + StrChar + "", CommonFunctions.ConStr);
                items.AMOUNT = items.VATEXPAMOUNT = ChargAmt;
                if (Convert.ToDouble(ChargAmt) < 0)
                {
                    items.ISDEEMEDPOSITIVE = "Yes";
                    items.ISLASTDEEMEDPOSITIVE = "Yes";
                }
                else
                {
                    items.ISDEEMEDPOSITIVE = "No";
                    items.ISLASTDEEMEDPOSITIVE = "No";
                }
                lst.Add(items);
            }
            if (Convert.ToDouble(RoundAmt) != 0)
            {
                LEDGERENTRIES items = new LEDGERENTRIES();
                items.ROUNDTYPE = "Normal Rounding";
                items.LEDGERNAME = ObjQry.ReturnString("Select LedgerName From MLedger Where LedgerNo=" + StrRound + "", CommonFunctions.ConStr);
                items.AMOUNT = items.VATEXPAMOUNT = RoundAmt;
                if (Convert.ToDouble(RoundAmt) < 0)
                {
                    items.ISDEEMEDPOSITIVE = "Yes";
                    items.ISLASTDEEMEDPOSITIVE = "Yes";
                }
                else
                {
                    items.ISDEEMEDPOSITIVE = "No";
                    items.ISLASTDEEMEDPOSITIVE = "No";
                }
                lst.Add(items);
            }

        }

        private List<ALLINVENTORYENTRIES> featchStockAllItemsDetails_purchase(long PkSrNo)
        {
            List<ALLINVENTORYENTRIES> lst = new List<ALLINVENTORYENTRIES>();
            string sql = " SELECT MStockGroup.StockGroupName, MStockItems.ItemName, TStock.NetRate, TStock.NetAmount, TStock.Quantity, " +
                         " (Case When(MItemTaxInfo.Percentage=0) Then 'Purchase GST EXEMPTED' Else MLedger.LedgerName End) AS LedgerName, MItemTaxInfo.Percentage ," +
                         " MUOM.UOMName " +
                         " FROM         MLedger INNER JOIN " +
                         " MItemTaxInfo ON MLedger.LedgerNo = MItemTaxInfo.SalesLedgerNo INNER JOIN " +
                         " TStock INNER JOIN MStockItems ON TStock.ItemNo = MStockItems.ItemNo INNER JOIN " +
                         " MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo INNER JOIN " +
                         " MUOM ON TStock.FkUomNo = MUOM.UOMNo ON MItemTaxInfo.PkSrNo = TStock.FkItemTaxInfo " +
                         " WHERE     (TStock.FKVoucherNo = " + PkSrNo + ") ";
            DataTable dt = ObjFunction.GetDataView(sql).Table;
            foreach (DataRow dr in dt.Rows)
            {
                ALLINVENTORYENTRIES items = new ALLINVENTORYENTRIES();
                items.STOCKITEMNAME = dr["ItemName"].ToString();
                items.RATE = dr["NetRate"].ToString() + "/" + dr["UOMName"].ToString();
                items.AMOUNT = "-" + dr["NetAmount"].ToString();
                items.ACTUALQTY = items.BILLEDQTY = dr["Quantity"].ToString();
                items.ISDEEMEDPOSITIVE = "Yes";
                items.ISLASTDEEMEDPOSITIVE = "Yes";

                ACCOUNTINGALLOCATIONS aleg = new ACCOUNTINGALLOCATIONS();
                aleg.LEDGERNAME = dr["LedgerName"].ToString();
                aleg.AMOUNT = items.AMOUNT;
                aleg.CLASSRATE = "100.0000";
                aleg.ISDEEMEDPOSITIVE = "Yes";
                aleg.ISLASTDEEMEDPOSITIVE = "Yes";

                List<RATEDETAILS> lstrate = new List<RATEDETAILS>();
                RATEDETAILS rategst = new RATEDETAILS();
                rategst.GSTRATEDUTYHEAD = "Integrated Tax";
                lstrate.Add(rategst);

                rategst = new RATEDETAILS();
                rategst.GSTRATEDUTYHEAD = "Central Tax";
                lstrate.Add(rategst);

                rategst = new RATEDETAILS();
                rategst.GSTRATEDUTYHEAD = "State Tax";
                lstrate.Add(rategst);

                rategst = new RATEDETAILS();
                rategst.GSTRATEDUTYHEAD = "Cess";
                lstrate.Add(rategst);

                aleg.RATEDETAILS = lstrate;

                items.ACCOUNTINGALLOCATIONS = aleg;

                lst.Add(items);
            }
            return lst;
        }

        #endregion

        #region Purchase Recipts

        private void LoadPurchaseReciptsData(REQUESTDATA regData, DateTime FromDate, DateTime ToDate)
        {
            DataTable dtCust = featchPurchaseReciptsData(FromDate, ToDate);
            foreach (DataRow items in dtCust.Rows)
            {
                regData.TALLYMESSAGE.Add(featchPurchaseReciptsData(items));
            }
        }

        private DataTable featchPurchaseReciptsData(DateTime FromDate, DateTime ToDate)
        {
            string Sql = " SELECT TVoucherEntry.PkVoucherNo, TVoucherEntry.VoucherUserNo, TVoucherEntry.VoucherDate," +
                         " TVoucherEntry.Narration, TVoucherEntry.BilledAmount, MPayType.PayTypeName,MLedger.LedgerName " +
                         " FROM MLedger INNER JOIN TVoucherDetails ON MLedger.LedgerNo = TVoucherDetails.LedgerNo INNER JOIN " +
                         " TVoucherEntry INNER JOIN MPayType ON TVoucherEntry.PayTypeNo = MPayType.PKPayTypeNo ON  " +
                         " TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo  AND TVoucherDetails.SrNo = 501  " +
                         " WHERE     (TVoucherEntry.VoucherTypeCode in(31,7))  And " +
                         " TVoucherEntry.VoucherDate>='" + FromDate.Date.ToString("dd-MMM-yyyy") + "' And TVoucherEntry.VoucherDate<='" + ToDate.Date.ToString("dd-MMM-yyyy") + "' " +
                         " order by TVoucherEntry.VoucherDate ";

            return ObjFunction.GetDataView(Sql).Table;
        }

        private List<ALLLEDGERENTRIES> FeatchVoucherPayTypeDetails_Purchase(long PkSrNo)
        {
            List<ALLLEDGERENTRIES> lst = new List<ALLLEDGERENTRIES>();
            string Sql = " SELECT     MLedger.LedgerName,( TVoucherDetails.Debit + TVoucherDetails.Credit ) AS PartAmount, TVoucherDetails.SrNo, " +
                        " TVoucherDetails.Narration, " +
                        " (Case When(TVoucherEntry.VoucherTypeCode=15 or TVoucherEntry.VoucherTypeCode=12) Then Cast( TVoucherEntry.VoucherUserNo as Varchar) Else TVoucherEntry.Reference End) As VoucherUserNo ," +
                        " TVoucherPayTypeDetails.Amount,MPayType.PayTypeName, " +
                        "IsNull(TVoucherChqCreditDetails.ChequeNo,'' ) as ChequeNo, TVoucherChqCreditDetails.ChequeDate, TVoucherChqCreditDetails.Amount AS Expr1, " +
                        " '' AS Remark,IsNull( MOtherBank.BankName ,'') As BankName,IsNull( TVoucherChqCreditDetails.CreditCardNo,'') AS CreditCardNo ," +
                        " TVoucherChqCreditDetails.BranchNo " +
                        " FROM MOtherBank RIGHT OUTER JOIN " +
                        " TVoucherChqCreditDetails ON MOtherBank.BankNo = TVoucherChqCreditDetails.BankNo RIGHT OUTER JOIN " +
                        " MPayType INNER JOIN " +
                        " TVoucherPayTypeDetails ON MPayType.PKPayTypeNo = TVoucherPayTypeDetails.FKPayTypeNo INNER JOIN " +
                        " TVoucherDetails INNER JOIN " +
                        " MLedger ON TVoucherDetails.LedgerNo = MLedger.LedgerNo ON TVoucherPayTypeDetails.FKReceiptVoucherNo = TVoucherDetails.FkVoucherNo LEFT OUTER JOIN " +
                        " TVoucherEntry ON TVoucherPayTypeDetails.FKSalesVoucherNo = TVoucherEntry.PkVoucherNo ON TVoucherChqCreditDetails.FKVoucherNo = TVoucherPayTypeDetails.FKReceiptVoucherNo " +
                        " WHERE     (TVoucherDetails.FkVoucherNo = " + PkSrNo + ") ";

            DataTable dt = ObjFunction.GetDataView(Sql).Table;
            foreach (DataRow dr in dt.Rows)
            {
                ALLLEDGERENTRIES LstLedger = new ALLLEDGERENTRIES();
                LstLedger.LEDGERNAME = dr["LedgerName"].ToString();
                if (dr["SrNo"].ToString() == "501")
                {
                    LstLedger.ISDEEMEDPOSITIVE = "Yes";
                    LstLedger.ISLASTDEEMEDPOSITIVE = "Yes";

                    LstLedger.AMOUNT = "-" + dr["PartAmount"].ToString();
                    BILLALLOCATIONS billall = new BILLALLOCATIONS();
                    billall.AMOUNT = LstLedger.AMOUNT;
                    billall.Name = dr["VoucherUserNo"].ToString();
                    billall.BILLTYPE = "Agst Ref";
                    LstLedger.BILLALLOCATIONS.Add(billall);
                }
                else
                {
                    LstLedger.ISDEEMEDPOSITIVE = "No";
                    LstLedger.ISLASTDEEMEDPOSITIVE = "No";
                    LstLedger.AMOUNT = dr["PartAmount"].ToString();
                    if (dr["PayTypeName"].ToString() != "Cash")
                    {
                        BANKALLOCATIONS billall = new BANKALLOCATIONS();
                        billall.AMOUNT = LstLedger.AMOUNT;
                        billall.BANKNAME = dr["BankName"].ToString();
                        billall.UNIQUEREFERENCENUMBER = dr["ChequeNo"].ToString();
                        billall.TRANSFERMODE = dr["PayTypeName"].ToString();
                        LstLedger.BANKALLOCATIONS = billall;
                    }
                }
                lst.Add(LstLedger);
            }
            return lst;

        }

        private List<ALLLEDGERENTRIES> FeatchVoucherRefDetails_Purchase(long PkSrNo)
        {

            if (ObjQry.ReturnLong("Select  Count(*) As Cnt From TVoucherRefDetails As Tvr Where Tvr.FkVoucherTrnno IN(Select PkVoucherTrnno From TVoucherDetails Where FkVoucherNo=" + PkSrNo + ") ", CommonFunctions.ConStr) == 0)
                return null;

            List<ALLLEDGERENTRIES> lst = new List<ALLLEDGERENTRIES>();
            string Sql = " SELECT  MPayType.PayTypeName, TVoucherEntry.Narration, TVoucherEntry.BilledAmount, TVoucherEntry.Remark, " +
                        " TVoucherChqCreditDetails.ChequeNo, TVoucherChqCreditDetails.ChequeDate,   " +
                        " MOtherBank.BankName, TVoucherChqCreditDetails.BranchNo, TVoucherChqCreditDetails.CreditCardNo, " +
                        " TVoucherChqCreditDetails.Amount, '' AS CheckRemark ,MLedger.LedgerName ,TVoucherDetails.SrNo" +
                        " FROM         MOtherBank RIGHT OUTER JOIN " +
                        " TVoucherEntry INNER JOIN " +
                        " MPayType ON TVoucherEntry.PayTypeNo = MPayType.PKPayTypeNo INNER JOIN " +
                        " MLedger INNER JOIN " +
                        " TVoucherDetails ON MLedger.LedgerNo = TVoucherDetails.LedgerNo ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo LEFT OUTER JOIN " +
                        " TVoucherChqCreditDetails ON TVoucherEntry.PkVoucherNo = TVoucherChqCreditDetails.FKVoucherNo ON MOtherBank.BankNo = TVoucherChqCreditDetails.BankNo " +
                        " WHERE     (TVoucherEntry.PkVoucherNo = " + PkSrNo + ")";
            DataTable dt = ObjFunction.GetDataView(Sql).Table;
            foreach (DataRow dr in dt.Rows)
            {
                ALLLEDGERENTRIES LstLedger = new ALLLEDGERENTRIES();
                LstLedger.LEDGERNAME = dr["LedgerName"].ToString();
                if (dr["SrNo"].ToString() == "501")
                {
                    LstLedger.AMOUNT = "-" + dr["BilledAmount"].ToString();
                    Sql = " Select Amount,0 AS DiscAmt,(Select VoucherUserNo From TVoucherEntry  Where PkVoucherNo in (Select FkVoucherNo From TVoucherDetails Where PkVoucherTrnno in (Select FkVoucherTrnno From TVoucherRefDetails Where RefNo=Tvr.RefNo And TypeOfRef=3) And SrNo=501)) as BillNo " +
                        " From TVoucherRefDetails As Tvr Where Tvr.FkVoucherTrnno IN(Select PkVoucherTrnno " +
                        " From TVoucherDetails Where FkVoucherNo=" + PkSrNo + ") ";
                    DataTable dtRef = ObjFunction.GetDataView(Sql).Table;
                    foreach (DataRow drref in dtRef.Rows)
                    {
                        LstLedger.ISDEEMEDPOSITIVE = "Yes";
                        LstLedger.ISLASTDEEMEDPOSITIVE = "Yes";
                        BILLALLOCATIONS billall = new BILLALLOCATIONS();
                        billall.AMOUNT = "-" + drref["Amount"].ToString();
                        billall.Name = drref["BillNo"].ToString();
                        billall.BILLTYPE = "Agst Ref";
                        LstLedger.BILLALLOCATIONS.Add(billall);
                    }
                }
                else
                {
                    LstLedger.AMOUNT = dr["BilledAmount"].ToString();
                    LstLedger.ISDEEMEDPOSITIVE = "No";
                    LstLedger.ISLASTDEEMEDPOSITIVE = "No";

                    if (dr["PayTypeName"].ToString() != "Cash")
                    {
                        BANKALLOCATIONS billall = new BANKALLOCATIONS();
                        billall.AMOUNT = dr["Amount"].ToString();
                        billall.BANKNAME = dr["BankName"].ToString();
                        billall.UNIQUEREFERENCENUMBER = dr["ChequeNo"].ToString();
                        billall.TRANSFERMODE = dr["PayTypeName"].ToString();
                        LstLedger.BANKALLOCATIONS = billall;
                    }
                }
                lst.Add(LstLedger);
            }
            return lst;

        }

        private TALLYMESSAGE featchPurchaseReciptsData(DataRow dtData)
        {

            TALLYMESSAGE tallyMSG = new TALLYMESSAGE();

            VOUCHER vch = new VOUCHER();
            vch.VCHTYPE = "Payment";
            vch.OBJVIEW = "Accounting Voucher View";
            vch.VOUCHERNUMBER = dtData["VoucherUserNo"].ToString();
            vch.DATE = Convert.ToDateTime(dtData["VoucherDate"].ToString()).ToString("yyyy-MM-dd").Replace("-", "");
            vch.PARTYLEDGERNAME = dtData["LedgerName"].ToString();
            vch.VOUCHERTYPENAME = "Payment";
            vch.PARTYNAME = dtData["LedgerName"].ToString();
            vch.BASICBASEPARTYNAME = dtData["LedgerName"].ToString();
            vch.PERSISTEDVIEW = "Accounting Voucher View";
            vch.PLACEOFSUPPLY = vch.STATENAME;
            vch.CONSIGNEEGSTIN = vch.PARTYGSTIN;

            //if VoucherPayType Details Not Null

            List<ALLLEDGERENTRIES> lstPayDtls = FeatchVoucherPayTypeDetails_Purchase(Convert.ToInt64(dtData["PkVoucherNo"].ToString()));
            if (lstPayDtls.Count != 0)
            {
                vch.ALLLEDGERENTRIES = lstPayDtls;
            }
            else
            {
                List<ALLLEDGERENTRIES> lstRef = FeatchVoucherRefDetails_Purchase(Convert.ToInt64(dtData["PkVoucherNo"].ToString()));
                if (lstRef.Count != 0)
                {
                    vch.ALLLEDGERENTRIES = lstRef;
                }
            }

            tallyMSG.VOUCHER = vch;
            tallyMSG.UDF = "TallyUDF";

            return tallyMSG;
        }


        #endregion

        #region Purchase Return Details

        private void LoadPurchaseReturnData(REQUESTDATA regData, DateTime FromDate, DateTime ToDate)
        {
            DataTable dtCust = featchPurchaseReturnData(FromDate, ToDate);
            foreach (DataRow items in dtCust.Rows)
            {
                regData.TALLYMESSAGE.Add(featchPurchaseReturnData(items));
            }
        }

        private DataTable featchPurchaseReturnData(DateTime FromDate, DateTime ToDate)
        {
            string Sql = " SELECT     TVoucherEntry.VoucherUserNo,TVoucherEntry.VoucherDate, TVoucherEntry.VoucherTime, TVoucherEntry.BilledAmount, TVoucherEntry.Remark,  " +
            " TVoucherEntry.IsCancel, MPayType.PayTypeName, TVoucherEntry.RateTypeNo,  " +
            " TVoucherEntry.MixMode, ISNULL(TVoucherDetails.Debit, 0) AS DiscAmt ," +
            " MLedger.LedgerName,ISNull( MLedgerDetails.Address,'') AS Address,ISNull(MState.StateName,'') AS StateName, MPayType.PKPayTypeNo, TVoucherEntry.PkVoucherNo " +
            " ,IsNull(MLedgerDetails.CSTNo,'') AS GSTNo,(Debit+Credit) as Amount,TVoucherEntry.Reference  " +
            " FROM         MState RIGHT OUTER JOIN " +
            " MLedgerDetails ON MState.StateNo = MLedgerDetails.StateNo RIGHT OUTER JOIN " +
            " MLedger INNER JOIN " +
            " TVoucherDetails ON MLedger.LedgerNo = TVoucherDetails.LedgerNo ON MLedgerDetails.LedgerNo = MLedger.LedgerNo RIGHT OUTER JOIN " +
            " TVoucherEntry INNER JOIN MPayType ON TVoucherEntry.PayTypeNo = MPayType.PKPayTypeNo ON " +
            " TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo AND TVoucherDetails.SrNo = 501 " +
            " WHERE     (TVoucherEntry.VoucherTypeCode = 13)  And " +
            " TVoucherEntry.VoucherDate>='" + FromDate.Date.ToString("dd-MMM-yyyy") + "' And TVoucherEntry.VoucherDate<='" + ToDate.Date.ToString("dd-MMM-yyyy") + "' And TVoucherEntry.TaxTypeNo=38 " +
            " order by TVoucherEntry.VoucherDate ";

            return ObjFunction.GetDataView(Sql).Table;
        }

        private TALLYMESSAGE featchPurchaseReturnData(DataRow dtData)
        {

            TALLYMESSAGE tallyMSG = new TALLYMESSAGE();

            VOUCHER vch = new VOUCHER();
            vch.VCHTYPE = "Debit Note";
            vch.OBJVIEW = "Invoice Voucher View";
            vch.VOUCHERNUMBER = dtData["VoucherUserNo"].ToString();
            vch.REFERENCE = dtData["Reference"].ToString();

            ADDRESS add = new ADDRESS();
            add.address = dtData["Address"].ToString();
            vch.ADDRESS.Add(add);

            vch.DATE = Convert.ToDateTime(dtData["VoucherDate"].ToString()).ToString("yyyy-MM-dd").Replace("-", "");
            vch.STATENAME = dtData["StateName"].ToString();
            vch.PARTYGSTIN = dtData["GSTNo"].ToString();
            vch.PARTYLEDGERNAME = dtData["LedgerName"].ToString();
            vch.VOUCHERTYPENAME = "Debit Note";
            vch.PARTYNAME = dtData["LedgerName"].ToString();
            vch.BASICBASEPARTYNAME = dtData["LedgerName"].ToString();
            vch.PERSISTEDVIEW = "Invoice Voucher View";
            vch.PLACEOFSUPPLY = vch.STATENAME;
            vch.CONSIGNEEGSTIN = vch.PARTYGSTIN;
            vch.BASICBUYERNAME = vch.BASICBASEPARTYNAME;
            vch.BASICDATETIMEOFINVOICE = Convert.ToDateTime(dtData["VoucherDate"].ToString()).ToString("dd-MMM-yyyy") + " at " + Convert.ToDateTime(dtData["VoucherDate"].ToString()).ToString("t");
            vch.BASICDATETIMEOFREMOVAL = vch.BASICDATETIMEOFINVOICE;

            #region Ledger Details List

            List<LEDGERENTRIES> lst = new List<LEDGERENTRIES>();

            LEDGERENTRIES led = new LEDGERENTRIES();
            led.LEDGERNAME = vch.PARTYNAME;
            led.AMOUNT = "-" + dtData["Amount"].ToString();
            led.ISDEEMEDPOSITIVE = "Yes";
            led.ISLASTDEEMEDPOSITIVE = "Yes";

            if (ObjQry.ReturnLong("Select  Count(*) As Cnt From TVoucherRefDetails As Tvr Where Tvr.FkVoucherTrnno IN(Select PkVoucherTrnno From TVoucherDetails Where FkVoucherNo=" + Convert.ToInt64(dtData["PkVoucherNo"].ToString()) + ") ", CommonFunctions.ConStr) != 0)
            {
                string Sql = " Select " +
                    //"(Select Reference From TVoucherEntry  Where PkVoucherNo in (Select FkVoucherNo From TVoucherDetails Where PkVoucherTrnno in (Select FkVoucherTrnno From TVoucherRefDetails " +
                          " (Select   (Case When(TVoucherEntry.VoucherTypeCode=9 or TVoucherEntry.VoucherTypeCode=13) Then Cast( TVoucherEntry.Reference as Varchar) Else TVoucherEntry.VoucherUserNo  End)  As Reference From TVoucherEntry  Where PkVoucherNo in (Select FkVoucherNo From TVoucherDetails Where PkVoucherTrnno in (Select FkVoucherTrnno From TVoucherRefDetails " +
                          " Where RefNo=Tvr.RefNo And TypeOfRef=3) And SrNo=501)) as BillNo " +
                          " From TVoucherRefDetails As Tvr Where Tvr.FkVoucherTrnno IN(Select PkVoucherTrnno " +
                          " From TVoucherDetails Where FkVoucherNo=" + dtData["PkVoucherNo"].ToString() + ") ";
                BILLALLOCATIONS ballc = new BILLALLOCATIONS();
                ballc.Name = ObjQry.ReturnString(Sql, CommonFunctions.ConStr).ToString();
                ballc.BILLTYPE = "Agst Ref";
                ballc.AMOUNT = led.AMOUNT;
                led.BILLALLOCATIONS = ballc;
            }
            else if (ObjQry.ReturnLong("Select  Count(*) As Cnt From TVoucherPayTypeDetails Where FkSalesVoucherNo=" + Convert.ToInt64(dtData["PkVoucherNo"].ToString()) + " ", CommonFunctions.ConStr) != 0)
            {
                BILLALLOCATIONS ballc = new BILLALLOCATIONS();
                ballc.Name = dtData["VoucherUserNo"].ToString();
                ballc.BILLTYPE = "New Ref";
                ballc.AMOUNT = led.AMOUNT;
                led.BILLALLOCATIONS = ballc;
            }


            lst.Add(led);
            featchAllGSTTaxDetails_PurchaseReturn(lst, Convert.ToInt64(dtData["PkVoucherNo"].ToString()));

            vch.LEDGERENTRIES = lst;
            #endregion

            List<ALLINVENTORYENTRIES> lstItems = featchStockAllItemsDetails_PurchaseReturn(Convert.ToInt64(dtData["PkVoucherNo"].ToString()));
            vch.ALLINVENTORYENTRIES = lstItems;


            tallyMSG.VOUCHER = vch;
            tallyMSG.UDF = "TallyUDF";

            return tallyMSG;
        }

        private void featchAllGSTTaxDetails_PurchaseReturn(List<LEDGERENTRIES> lst, long PkSrNo)
        {
            string StrRound = "5";
            string StrDisc = "3";
            string StrChar = "4";

            string sql = "SELECT  SUM( TStock.IGSTAmount) as IGSTAmount,SUM(TStock.CGSTAmount) AS CGSTAmount,SUM( TStock.SGSTAmount) AS SGSTAmount, " +
                       " SUM( TStock.CessAmount) AS CessAmount,TStock.TaxPercentage," +
                       " 'IGST '+Cast(TStock.IGSTPercent As varchar)+' %' As IGSTPercent, " +
                       " 'CGST '+Cast(TStock.CGSTPercent As varchar)+' %' As CGSTPercent, " +
                       " 'SGST '+Cast(TStock.SGSTPercent As varchar)+' %' As SGSTPercent, " +
                       " (Case When(TStock.CessPercent<>0) Then 'CESS '+Cast(TStock.CessPercent As varchar)+' %' Else '' end) As CessPercent " +
                       " FROM TStock INNER JOIN MItemTaxInfo ON TStock.FkItemTaxInfo = MItemTaxInfo.PkSrNo " +
                       " Where TStock.FkVoucherNo=" + PkSrNo + " And TStock.TaxPercentage<>0 " +
                       " Group by TStock.IGSTPercent, TStock.TaxPercentage,TStock.CGSTPercent,TStock.SGSTPercent,TStock.CessPercent ";
            DataTable dt = ObjFunction.GetDataView(sql).Table;
            foreach (DataRow dr in dt.Rows)
            {
                if (Convert.ToDouble(dr["IGSTAmount"].ToString()) != 0)
                {
                    LEDGERENTRIES items = new LEDGERENTRIES();
                    items.ROUNDTYPE = "Normal Rounding";
                    items.METHODTYPE = "GST";
                    items.LEDGERNAME = dr["IGSTPercent"].ToString();
                    items.AMOUNT = items.VATEXPAMOUNT = dr["IGSTAmount"].ToString();
                    items.ISDEEMEDPOSITIVE = "No";
                    items.ISLASTDEEMEDPOSITIVE = "No";
                    lst.Add(items);
                }
                if (Convert.ToDouble(dr["CGSTAmount"].ToString()) != 0)
                {
                    LEDGERENTRIES items = new LEDGERENTRIES();
                    items.ROUNDTYPE = "Normal Rounding";
                    items.METHODTYPE = "GST";
                    items.LEDGERNAME = dr["CGSTPercent"].ToString();
                    items.AMOUNT = items.VATEXPAMOUNT = dr["CGSTAmount"].ToString();
                    items.ISDEEMEDPOSITIVE = "No";
                    items.ISLASTDEEMEDPOSITIVE = "No";
                    lst.Add(items);
                }
                if (Convert.ToDouble(dr["SGSTAmount"].ToString()) != 0)
                {
                    LEDGERENTRIES items = new LEDGERENTRIES();
                    items.ROUNDTYPE = "Normal Rounding";
                    items.METHODTYPE = "GST";
                    items.LEDGERNAME = dr["SGSTPercent"].ToString();
                    items.AMOUNT = items.VATEXPAMOUNT = dr["SGSTAmount"].ToString();
                    items.ISDEEMEDPOSITIVE = "No";
                    items.ISLASTDEEMEDPOSITIVE = "No";
                    lst.Add(items);
                }
                if (Convert.ToDouble(dr["CessAmount"].ToString()) != 0)
                {
                    LEDGERENTRIES items = new LEDGERENTRIES();
                    items.ROUNDTYPE = "Normal Rounding";
                    items.METHODTYPE = "GST";
                    items.LEDGERNAME = dr["CessPercent"].ToString();
                    items.AMOUNT = items.VATEXPAMOUNT = dr["CessAmount"].ToString();
                    items.ISDEEMEDPOSITIVE = "No";
                    items.ISLASTDEEMEDPOSITIVE = "No";
                    lst.Add(items);
                }

            }

            string RoundAmt = ObjQry.ReturnString("Select (isNull(SUM(Credit - Debit),0)) FROM TVoucherDetails Where LedgerNo=" + StrRound + " And FkVoucherNo=" + PkSrNo + "", CommonFunctions.ConStr);
            string ChargAmt = ObjQry.ReturnString("Select (isNull(SUM(Credit - Debit),0))  FROM TVoucherDetails Where LedgerNo=" + StrChar + " And FkVoucherNo=" + PkSrNo + " ", CommonFunctions.ConStr);
            string DiscAmt = ObjQry.ReturnString("Select (isNull(SUM(Debit - Credit),0)*-1)  FROM TVoucherDetails Where LedgerNo=" + StrDisc + " And FkVoucherNo=" + PkSrNo + " ", CommonFunctions.ConStr);

            if (Convert.ToDouble(DiscAmt) != 0)
            {
                LEDGERENTRIES items = new LEDGERENTRIES();
                items.ROUNDTYPE = "Normal Rounding";
                items.LEDGERNAME = ObjQry.ReturnString("Select LedgerName From MLedger Where LedgerNo=" + StrDisc + "", CommonFunctions.ConStr);
                items.AMOUNT = items.VATEXPAMOUNT = DiscAmt;
                if (Convert.ToDouble(DiscAmt) < 0)
                {
                    items.ISDEEMEDPOSITIVE = "Yes";
                    items.ISLASTDEEMEDPOSITIVE = "Yes";
                }
                else
                {
                    items.ISDEEMEDPOSITIVE = "No";
                    items.ISLASTDEEMEDPOSITIVE = "No";
                }
                lst.Add(items);
            }

            if (Convert.ToDouble(ChargAmt) != 0)
            {
                LEDGERENTRIES items = new LEDGERENTRIES();
                items.ROUNDTYPE = "Normal Rounding";
                items.LEDGERNAME = ObjQry.ReturnString("Select LedgerName From MLedger Where LedgerNo=" + StrChar + "", CommonFunctions.ConStr);
                items.AMOUNT = items.VATEXPAMOUNT = ChargAmt;
                if (Convert.ToDouble(ChargAmt) < 0)
                {
                    items.ISDEEMEDPOSITIVE = "Yes";
                    items.ISLASTDEEMEDPOSITIVE = "Yes";
                }
                else
                {
                    items.ISDEEMEDPOSITIVE = "No";
                    items.ISLASTDEEMEDPOSITIVE = "No";
                }
                lst.Add(items);
            }
            if (Convert.ToDouble(RoundAmt) != 0)
            {
                LEDGERENTRIES items = new LEDGERENTRIES();
                items.ROUNDTYPE = "Normal Rounding";
                items.LEDGERNAME = ObjQry.ReturnString("Select LedgerName From MLedger Where LedgerNo=" + StrRound + "", CommonFunctions.ConStr);
                items.AMOUNT = items.VATEXPAMOUNT = RoundAmt;
                if (Convert.ToDouble(RoundAmt) < 0)
                {
                    items.ISDEEMEDPOSITIVE = "Yes";
                    items.ISLASTDEEMEDPOSITIVE = "Yes";
                }
                else
                {
                    items.ISDEEMEDPOSITIVE = "No";
                    items.ISLASTDEEMEDPOSITIVE = "No";
                }
                lst.Add(items);
            }

        }

        private List<ALLINVENTORYENTRIES> featchStockAllItemsDetails_PurchaseReturn(long PkSrNo)
        {
            List<ALLINVENTORYENTRIES> lst = new List<ALLINVENTORYENTRIES>();
            string sql = " SELECT MStockGroup.StockGroupName, MStockItems.ItemName, TStock.NetRate, TStock.NetAmount, TStock.Quantity, " +
                         " (Case When(MItemTaxInfo.Percentage=0) Then 'SALE GST EXEMPTED' Else MLedger.LedgerName End) AS LedgerName, MItemTaxInfo.Percentage ," +
                         " MUOM.UOMName " +
                         " FROM         MLedger INNER JOIN " +
                         " MItemTaxInfo ON MLedger.LedgerNo = MItemTaxInfo.SalesLedgerNo INNER JOIN " +
                         " TStock INNER JOIN MStockItems ON TStock.ItemNo = MStockItems.ItemNo INNER JOIN " +
                         " MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo INNER JOIN " +
                         " MUOM ON TStock.FkUomNo = MUOM.UOMNo ON MItemTaxInfo.PkSrNo = TStock.FkItemTaxInfo " +
                         " WHERE     (TStock.FKVoucherNo = " + PkSrNo + ") ";
            DataTable dt = ObjFunction.GetDataView(sql).Table;
            foreach (DataRow dr in dt.Rows)
            {
                ALLINVENTORYENTRIES items = new ALLINVENTORYENTRIES();
                items.STOCKITEMNAME = dr["ItemName"].ToString();
                items.RATE = dr["NetRate"].ToString() + "/" + dr["UOMName"].ToString();
                items.AMOUNT = dr["NetAmount"].ToString();
                items.ACTUALQTY = items.BILLEDQTY = dr["Quantity"].ToString();
                items.ISDEEMEDPOSITIVE = "No";
                items.ISLASTDEEMEDPOSITIVE = "No";

                ACCOUNTINGALLOCATIONS aleg = new ACCOUNTINGALLOCATIONS();
                aleg.LEDGERNAME = dr["LedgerName"].ToString();
                aleg.AMOUNT = items.AMOUNT;
                aleg.CLASSRATE = "100.0000";
                aleg.ISDEEMEDPOSITIVE = "No";
                aleg.ISLASTDEEMEDPOSITIVE = "No";

                List<RATEDETAILS> lstrate = new List<RATEDETAILS>();
                RATEDETAILS rategst = new RATEDETAILS();
                rategst.GSTRATEDUTYHEAD = "Integrated Tax";
                lstrate.Add(rategst);

                rategst = new RATEDETAILS();
                rategst.GSTRATEDUTYHEAD = "Central Tax";
                lstrate.Add(rategst);

                rategst = new RATEDETAILS();
                rategst.GSTRATEDUTYHEAD = "State Tax";
                lstrate.Add(rategst);

                rategst = new RATEDETAILS();
                rategst.GSTRATEDUTYHEAD = "Cess";
                lstrate.Add(rategst);

                aleg.RATEDETAILS = lstrate;

                items.ACCOUNTINGALLOCATIONS = aleg;

                lst.Add(items);
            }
            return lst;
        }

        #endregion

        #region Sale Return Bill Data

        private void LoadSaleReturnData(REQUESTDATA regData, DateTime FromDate, DateTime ToDate)
        {
            DataTable dtCust = featchSaleReturnData(FromDate, ToDate);
            foreach (DataRow items in dtCust.Rows)
            {
                regData.TALLYMESSAGE.Add(featchSaleReturnData(items));
            }
        }

        private DataTable featchSaleReturnData(DateTime FromDate, DateTime ToDate)
        {
            string Sql = " SELECT     TVoucherEntry.VoucherUserNo,TVoucherEntry.VoucherDate, TVoucherEntry.VoucherTime, TVoucherEntry.BilledAmount, TVoucherEntry.Remark,  " +
            " TVoucherEntry.IsCancel, MPayType.PayTypeName, TVoucherEntry.RateTypeNo,  " +
            " TVoucherEntry.MixMode, ISNULL(TVoucherDetails.Debit, 0) AS DiscAmt , " +
            " MLedger.LedgerName,ISNull( MLedgerDetails.Address,'') AS Address,ISNull(MState.StateName,'') AS StateName, MPayType.PKPayTypeNo, TVoucherEntry.PkVoucherNo " +
            " ,IsNull(MLedgerDetails.CSTNo,'') AS GSTNo,(Debit+Credit) as Amount,TVoucherEntry.Reference " +
            " FROM         MState RIGHT OUTER JOIN " +
            " MLedgerDetails ON MState.StateNo = MLedgerDetails.StateNo RIGHT OUTER JOIN " +
            " MLedger INNER JOIN " +
            " TVoucherDetails ON MLedger.LedgerNo = TVoucherDetails.LedgerNo ON MLedgerDetails.LedgerNo = MLedger.LedgerNo RIGHT OUTER JOIN " +
            " TVoucherEntry INNER JOIN MPayType ON TVoucherEntry.PayTypeNo = MPayType.PKPayTypeNo ON " +
            " TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo AND TVoucherDetails.SrNo = 501 " +
            " WHERE     (TVoucherEntry.VoucherTypeCode = 12)  And " +
            " TVoucherEntry.VoucherDate>='" + FromDate.Date.ToString("dd-MMM-yyyy") + "' And TVoucherEntry.VoucherDate<='" + ToDate.Date.ToString("dd-MMM-yyyy") + "' And TVoucherEntry.TaxTypeNo=38" +
            " order by TVoucherEntry.VoucherDate ";

            return ObjFunction.GetDataView(Sql).Table;
        }

        private TALLYMESSAGE featchSaleReturnData(DataRow dtData)
        {

            TALLYMESSAGE tallyMSG = new TALLYMESSAGE();

            VOUCHER vch = new VOUCHER();
            vch.VCHTYPE = "Credit Note";
            vch.OBJVIEW = "Invoice Voucher View";
            vch.VOUCHERNUMBER = dtData["VoucherUserNo"].ToString();
            vch.REFERENCE = dtData["VoucherUserNo"].ToString();
            vch.NARRATION = dtData["Remark"].ToString();

            ADDRESS add = new ADDRESS();
            add.address = dtData["Address"].ToString();
            vch.ADDRESS.Add(add);

            vch.DATE = Convert.ToDateTime(dtData["VoucherDate"].ToString()).ToString("yyyy-MM-dd").Replace("-", "");
            vch.STATENAME = dtData["StateName"].ToString();
            vch.PARTYGSTIN = dtData["GSTNo"].ToString();
            vch.PARTYLEDGERNAME = dtData["LedgerName"].ToString();
            vch.VOUCHERTYPENAME = "Credit Note";
            vch.PARTYNAME = dtData["LedgerName"].ToString();
            vch.BASICBASEPARTYNAME = dtData["LedgerName"].ToString();
            vch.PERSISTEDVIEW = "Invoice Voucher View";
            vch.PLACEOFSUPPLY = vch.STATENAME;
            vch.CONSIGNEEGSTIN = vch.PARTYGSTIN;
            vch.BASICBUYERNAME = vch.BASICBASEPARTYNAME;
            vch.BASICDATETIMEOFINVOICE = Convert.ToDateTime(dtData["VoucherDate"].ToString()).ToString("dd-MMM-yyyy") + " at " + Convert.ToDateTime(dtData["VoucherDate"].ToString()).ToString("t");
            vch.BASICDATETIMEOFREMOVAL = vch.BASICDATETIMEOFINVOICE;

            #region Ledger Details List

            List<LEDGERENTRIES> lst = new List<LEDGERENTRIES>();

            LEDGERENTRIES led = new LEDGERENTRIES();
            led.LEDGERNAME = vch.PARTYNAME;
            led.AMOUNT = dtData["Amount"].ToString();
            led.ISDEEMEDPOSITIVE = "No";
            led.ISLASTDEEMEDPOSITIVE = "No";

            if (ObjQry.ReturnLong("Select  Count(*) As Cnt From TVoucherRefDetails As Tvr Where Tvr.FkVoucherTrnno IN(Select PkVoucherTrnno From TVoucherDetails Where FkVoucherNo=" + Convert.ToInt64(dtData["PkVoucherNo"].ToString()) + ") ", CommonFunctions.ConStr) != 0)
            {
                string Sql = " Select " +
                            " (Select   (Case When(TVoucherEntry.VoucherTypeCode=15 or TVoucherEntry.VoucherTypeCode=12) Then Cast( TVoucherEntry.VoucherUserNo as Varchar) Else TVoucherEntry.Reference End)  As Reference From TVoucherEntry  Where PkVoucherNo in (Select FkVoucherNo From TVoucherDetails Where PkVoucherTrnno in (Select FkVoucherTrnno From TVoucherRefDetails " +
                            " Where RefNo=Tvr.RefNo And TypeOfRef=3) And SrNo=501)) as BillNo " +
                            " From TVoucherRefDetails As Tvr Where Tvr.FkVoucherTrnno IN(Select PkVoucherTrnno " +
                            " From TVoucherDetails Where FkVoucherNo=" + dtData["PkVoucherNo"].ToString() + ") ";
                BILLALLOCATIONS ballc = new BILLALLOCATIONS();
                ballc.Name = ObjQry.ReturnString(Sql, CommonFunctions.ConStr).ToString();
                ballc.BILLTYPE = "Agst Ref";
                ballc.AMOUNT = led.AMOUNT;
                led.BILLALLOCATIONS = ballc;
            }
            else if (ObjQry.ReturnLong("Select  Count(*) As Cnt From TVoucherPayTypeDetails Where FkSalesVoucherNo=" + Convert.ToInt64(dtData["PkVoucherNo"].ToString()) + " ", CommonFunctions.ConStr) != 0)
            {
                BILLALLOCATIONS ballc = new BILLALLOCATIONS();
                ballc.Name = vch.VOUCHERNUMBER;
                ballc.BILLTYPE = "New Ref";
                ballc.AMOUNT = led.AMOUNT;
                led.BILLALLOCATIONS = ballc;
            }


            lst.Add(led);
            featchAllGSTTaxSaleReturn(lst, Convert.ToInt64(dtData["PkVoucherNo"].ToString()));

            vch.LEDGERENTRIES = lst;
            #endregion

            List<ALLINVENTORYENTRIES> lstItems = featchStockAllItemsDetails_SaleReturn(Convert.ToInt64(dtData["PkVoucherNo"].ToString()));
            vch.ALLINVENTORYENTRIES = lstItems;


            tallyMSG.VOUCHER = vch;
            tallyMSG.UDF = "TallyUDF";

            return tallyMSG;
        }

        private void featchAllGSTTaxSaleReturn(List<LEDGERENTRIES> lst, long PkSrNo)
        {
            string StrRound = "9";
            string StrDisc = "3";
            string StrChar = "4";
            string StrDisDisc = "8";

            string sql = "SELECT  SUM( TStock.IGSTAmount) as IGSTAmount,SUM(TStock.CGSTAmount) AS CGSTAmount,SUM( TStock.SGSTAmount) AS SGSTAmount, " +
                       " SUM( TStock.CessAmount) AS CessAmount,TStock.TaxPercentage," +
                       " 'IGST '+Cast(TStock.IGSTPercent As varchar)+' %' As IGSTPercent, " +
                       " 'CGST '+Cast(TStock.CGSTPercent As varchar)+' %' As CGSTPercent, " +
                       " 'SGST '+Cast(TStock.SGSTPercent As varchar)+' %' As SGSTPercent, " +
                       " (Case When(TStock.CessPercent<>0) Then 'CESS '+Cast(TStock.CessPercent As varchar)+' %' Else '' end) As CessPercent " +
                       " FROM TStock INNER JOIN MItemTaxInfo ON TStock.FkItemTaxInfo = MItemTaxInfo.PkSrNo " +
                       " Where TStock.FkVoucherNo=" + PkSrNo + " And TStock.TaxPercentage<>0 " +
                       " Group by TStock.IGSTPercent, TStock.TaxPercentage,TStock.CGSTPercent,TStock.SGSTPercent,TStock.CessPercent ";
            DataTable dt = ObjFunction.GetDataView(sql).Table;
            foreach (DataRow dr in dt.Rows)
            {
                if (Convert.ToDouble(dr["IGSTAmount"].ToString()) != 0)
                {
                    LEDGERENTRIES items = new LEDGERENTRIES();
                    items.ROUNDTYPE = "Normal Rounding";
                    items.METHODTYPE = "GST";
                    items.LEDGERNAME = dr["IGSTPercent"].ToString();
                    items.AMOUNT = items.VATEXPAMOUNT = "-" + dr["IGSTAmount"].ToString();
                    items.ISDEEMEDPOSITIVE = "Yes";
                    items.ISLASTDEEMEDPOSITIVE = "Yes";
                    lst.Add(items);
                }
                if (Convert.ToDouble(dr["CGSTAmount"].ToString()) != 0)
                {
                    LEDGERENTRIES items = new LEDGERENTRIES();
                    items.ROUNDTYPE = "Normal Rounding";
                    items.METHODTYPE = "GST";
                    items.LEDGERNAME = dr["CGSTPercent"].ToString();
                    items.AMOUNT = items.VATEXPAMOUNT = "-" + dr["CGSTAmount"].ToString();
                    items.ISDEEMEDPOSITIVE = "Yes";
                    items.ISLASTDEEMEDPOSITIVE = "Yes";
                    lst.Add(items);
                }
                if (Convert.ToDouble(dr["SGSTAmount"].ToString()) != 0)
                {
                    LEDGERENTRIES items = new LEDGERENTRIES();
                    items.ROUNDTYPE = "Normal Rounding";
                    items.METHODTYPE = "GST";
                    items.LEDGERNAME = dr["SGSTPercent"].ToString();
                    items.AMOUNT = items.VATEXPAMOUNT = "-" + dr["SGSTAmount"].ToString();
                    items.ISDEEMEDPOSITIVE = "Yes";
                    items.ISLASTDEEMEDPOSITIVE = "Yes";
                    lst.Add(items);
                }
                if (Convert.ToDouble(dr["CessAmount"].ToString()) != 0)
                {
                    LEDGERENTRIES items = new LEDGERENTRIES();
                    items.ROUNDTYPE = "Normal Rounding";
                    items.METHODTYPE = "GST";
                    items.LEDGERNAME = dr["CessPercent"].ToString();
                    items.AMOUNT = items.VATEXPAMOUNT = "-" + dr["CessAmount"].ToString();
                    items.ISDEEMEDPOSITIVE = "Yes";
                    items.ISLASTDEEMEDPOSITIVE = "Yes";
                    lst.Add(items);
                }

            }


            string RoundAmt = ObjQry.ReturnString("Select (isNull(SUM(Credit - Debit),0)) FROM TVoucherDetails Where LedgerNo=" + StrRound + " And FkVoucherNo=" + PkSrNo + " ", CommonFunctions.ConStr);
            string ChargAmt = ObjQry.ReturnString("Select (isNull(SUM(Credit - Debit),0))  FROM TVoucherDetails Where LedgerNo=" + StrChar + " And FkVoucherNo=" + PkSrNo + " ", CommonFunctions.ConStr);
            string DiscAmt = ObjQry.ReturnString("Select (isNull(SUM(Debit - Credit),0)* -1)  FROM TVoucherDetails Where LedgerNo=" + StrDisc + " And SrNo Not in(510,509) And FkVoucherNo=" + PkSrNo + "", CommonFunctions.ConStr);
            string DisplayDiscAmt = ObjQry.ReturnString("Select (isNull(SUM(Debit - Credit),0)* -1)  FROM TVoucherDetails Where LedgerNo=" + StrDisDisc + " And FkVoucherNo=" + PkSrNo + " ", CommonFunctions.ConStr);

            if (Convert.ToDouble(DiscAmt) != 0)
            {
                LEDGERENTRIES items = new LEDGERENTRIES();
                items.ROUNDTYPE = "Normal Rounding";
                items.LEDGERNAME = ObjQry.ReturnString("Select LedgerName From MLedger Where LedgerNo=" + StrDisc + "", CommonFunctions.ConStr);
                items.AMOUNT = items.VATEXPAMOUNT = DiscAmt;
                if (Convert.ToDouble(DiscAmt) < 0)
                {
                    items.ISDEEMEDPOSITIVE = "Yes";
                    items.ISLASTDEEMEDPOSITIVE = "Yes";
                }
                else
                {
                    items.ISDEEMEDPOSITIVE = "No";
                    items.ISLASTDEEMEDPOSITIVE = "No";
                }
                lst.Add(items);
            }
            if (Convert.ToDouble(DisplayDiscAmt) != 0)
            {
                LEDGERENTRIES items = new LEDGERENTRIES();
                items.ROUNDTYPE = "Normal Rounding";
                items.LEDGERNAME = ObjQry.ReturnString("Select LedgerName From MLedger Where LedgerNo=" + StrDisDisc + "", CommonFunctions.ConStr);
                items.AMOUNT = items.VATEXPAMOUNT = DisplayDiscAmt;
                if (Convert.ToDouble(DisplayDiscAmt) < 0)
                {
                    items.ISDEEMEDPOSITIVE = "Yes";
                    items.ISLASTDEEMEDPOSITIVE = "Yes";
                }
                else
                {
                    items.ISDEEMEDPOSITIVE = "No";
                    items.ISLASTDEEMEDPOSITIVE = "No";
                }
                lst.Add(items);
            }

            if (Convert.ToDouble(ChargAmt) != 0)
            {
                LEDGERENTRIES items = new LEDGERENTRIES();
                items.ROUNDTYPE = "Normal Rounding";
                items.LEDGERNAME = ObjQry.ReturnString("Select LedgerName From MLedger Where LedgerNo=" + StrChar + "", CommonFunctions.ConStr);
                items.AMOUNT = items.VATEXPAMOUNT = ChargAmt;
                if (Convert.ToDouble(ChargAmt) < 0)
                {
                    items.ISDEEMEDPOSITIVE = "Yes";
                    items.ISLASTDEEMEDPOSITIVE = "Yes";
                }
                else
                {
                    items.ISDEEMEDPOSITIVE = "No";
                    items.ISLASTDEEMEDPOSITIVE = "No";
                }
                lst.Add(items);
            }
            if (Convert.ToDouble(RoundAmt) != 0)
            {
                LEDGERENTRIES items = new LEDGERENTRIES();
                items.ROUNDTYPE = "Normal Rounding";
                items.LEDGERNAME = ObjQry.ReturnString("Select LedgerName From MLedger Where LedgerNo=" + StrRound + "", CommonFunctions.ConStr);
                items.AMOUNT = items.VATEXPAMOUNT = RoundAmt;
                if (Convert.ToDouble(RoundAmt) < 0)
                {
                    items.ISDEEMEDPOSITIVE = "Yes";
                    items.ISLASTDEEMEDPOSITIVE = "Yes";
                }
                else
                {
                    items.ISDEEMEDPOSITIVE = "No";
                    items.ISLASTDEEMEDPOSITIVE = "No";
                }
                lst.Add(items);
            }

        }

        private List<ALLINVENTORYENTRIES> featchStockAllItemsDetails_SaleReturn(long PkSrNo)
        {
            List<ALLINVENTORYENTRIES> lst = new List<ALLINVENTORYENTRIES>();
            string sql = " SELECT MStockGroup.StockGroupName, MStockItems.ItemName, TStock.NetRate, TStock.NetAmount, TStock.Quantity, " +
                         " (Case When(MItemTaxInfo.Percentage=0) Then 'Purchase GST EXEMPTED' Else MLedger.LedgerName End) AS LedgerName, MItemTaxInfo.Percentage ," +
                         " MUOM.UOMName " +
                         " FROM         MLedger INNER JOIN " +
                         " MItemTaxInfo ON MLedger.LedgerNo = MItemTaxInfo.SalesLedgerNo INNER JOIN " +
                         " TStock INNER JOIN MStockItems ON TStock.ItemNo = MStockItems.ItemNo INNER JOIN " +
                         " MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo INNER JOIN " +
                         " MUOM ON TStock.FkUomNo = MUOM.UOMNo ON MItemTaxInfo.PkSrNo = TStock.FkItemTaxInfo " +
                         " WHERE     (TStock.FKVoucherNo = " + PkSrNo + ") ";
            DataTable dt = ObjFunction.GetDataView(sql).Table;
            foreach (DataRow dr in dt.Rows)
            {
                ALLINVENTORYENTRIES items = new ALLINVENTORYENTRIES();
                items.STOCKITEMNAME = dr["ItemName"].ToString();
                items.RATE = dr["NetRate"].ToString() + "/" + dr["UOMName"].ToString();
                items.AMOUNT = "-" + dr["NetAmount"].ToString();
                items.ACTUALQTY = items.BILLEDQTY = dr["Quantity"].ToString();
                items.ISDEEMEDPOSITIVE = "Yes";
                items.ISLASTDEEMEDPOSITIVE = "Yes";

                ACCOUNTINGALLOCATIONS aleg = new ACCOUNTINGALLOCATIONS();
                aleg.LEDGERNAME = dr["LedgerName"].ToString();
                aleg.AMOUNT = items.AMOUNT;
                aleg.CLASSRATE = "100.0000";
                aleg.ISDEEMEDPOSITIVE = "Yes";
                aleg.ISLASTDEEMEDPOSITIVE = "Yes";

                List<RATEDETAILS> lstrate = new List<RATEDETAILS>();
                RATEDETAILS rategst = new RATEDETAILS();
                rategst.GSTRATEDUTYHEAD = "Integrated Tax";
                lstrate.Add(rategst);

                rategst = new RATEDETAILS();
                rategst.GSTRATEDUTYHEAD = "Central Tax";
                lstrate.Add(rategst);

                rategst = new RATEDETAILS();
                rategst.GSTRATEDUTYHEAD = "State Tax";
                lstrate.Add(rategst);

                rategst = new RATEDETAILS();
                rategst.GSTRATEDUTYHEAD = "Cess";
                lstrate.Add(rategst);

                aleg.RATEDETAILS = lstrate;

                items.ACCOUNTINGALLOCATIONS = aleg;

                lst.Add(items);
            }
            return lst;
        }

        #endregion

        #region Purchase Return Recipts

        private void LoadPurchaseReturnReciptsData(REQUESTDATA regData, DateTime FromDate, DateTime ToDate)
        {
            DataTable dtCust = featchPurchaseReturnReciptsData(FromDate, ToDate);
            foreach (DataRow items in dtCust.Rows)
            {
                regData.TALLYMESSAGE.Add(featchPurchaseReturnReciptsData(items));
            }
        }

        private DataTable featchPurchaseReturnReciptsData(DateTime FromDate, DateTime ToDate)
        {
            string Sql = " SELECT TVoucherEntry.PkVoucherNo, TVoucherEntry.VoucherUserNo, TVoucherEntry.VoucherDate," +
                         " TVoucherEntry.Narration, TVoucherEntry.BilledAmount, MPayType.PayTypeName,MLedger.LedgerName " +
                         " FROM MLedger INNER JOIN TVoucherDetails ON MLedger.LedgerNo = TVoucherDetails.LedgerNo INNER JOIN " +
                         " TVoucherEntry INNER JOIN MPayType ON TVoucherEntry.PayTypeNo = MPayType.PKPayTypeNo ON  " +
                         " TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo  AND TVoucherDetails.SrNo = 501  " +
                         " WHERE     (TVoucherEntry.VoucherTypeCode = 11)  And " +
                         " TVoucherEntry.VoucherDate>='" + FromDate.Date.ToString("dd-MMM-yyyy") + "' And TVoucherEntry.VoucherDate<='" + ToDate.Date.ToString("dd-MMM-yyyy") + "' " +
                         " order by TVoucherEntry.VoucherDate ";

            return ObjFunction.GetDataView(Sql).Table;
        }

        private List<ALLLEDGERENTRIES> FeatchVoucherPayTypeDetails_PurchaseReturn(long PkSrNo)
        {
            List<ALLLEDGERENTRIES> lst = new List<ALLLEDGERENTRIES>();
            string Sql = " SELECT     MLedger.LedgerName,( TVoucherDetails.Debit + TVoucherDetails.Credit ) AS PartAmount, TVoucherDetails.SrNo, " +
                        " TVoucherDetails.Narration, TVoucherEntry.VoucherUserNo,TVoucherEntry.Reference, TVoucherPayTypeDetails.Amount,MPayType.PayTypeName, " +
                        "IsNull(TVoucherChqCreditDetails.ChequeNo,'' ) as ChequeNo, TVoucherChqCreditDetails.ChequeDate, TVoucherChqCreditDetails.Amount AS Expr1, " +
                        " IsNull( MOtherBank.BankName ,'') As BankName,IsNull( TVoucherChqCreditDetails.CreditCardNo,'') AS CreditCardNo ," +
                        " TVoucherChqCreditDetails.BranchNo " +
                        " FROM MOtherBank RIGHT OUTER JOIN " +
                        " TVoucherChqCreditDetails ON MOtherBank.BankNo = TVoucherChqCreditDetails.BankNo RIGHT OUTER JOIN " +
                        " MPayType INNER JOIN " +
                        " TVoucherPayTypeDetails ON MPayType.PKPayTypeNo = TVoucherPayTypeDetails.FKPayTypeNo INNER JOIN " +
                        " TVoucherDetails INNER JOIN " +
                        " MLedger ON TVoucherDetails.LedgerNo = MLedger.LedgerNo ON TVoucherPayTypeDetails.FKReceiptVoucherNo = TVoucherDetails.FkVoucherNo LEFT OUTER JOIN " +
                        " TVoucherEntry ON TVoucherPayTypeDetails.FKSalesVoucherNo = TVoucherEntry.PkVoucherNo ON TVoucherChqCreditDetails.FKVoucherNo = TVoucherPayTypeDetails.FKReceiptVoucherNo " +
                        " WHERE     (TVoucherDetails.FkVoucherNo = " + PkSrNo + ") ";

            DataTable dt = ObjFunction.GetDataView(Sql).Table;
            foreach (DataRow dr in dt.Rows)
            {
                ALLLEDGERENTRIES LstLedger = new ALLLEDGERENTRIES();
                LstLedger.LEDGERNAME = dr["LedgerName"].ToString();
                if (dr["SrNo"].ToString() == "501")
                {
                    LstLedger.ISDEEMEDPOSITIVE = "No";
                    LstLedger.ISLASTDEEMEDPOSITIVE = "No";

                    LstLedger.AMOUNT = dr["PartAmount"].ToString();
                    BILLALLOCATIONS billall = new BILLALLOCATIONS();
                    billall.AMOUNT = dr["PartAmount"].ToString();
                    billall.Name = dr["Reference"].ToString();
                    billall.BILLTYPE = "Agst Ref";
                    LstLedger.BILLALLOCATIONS.Add(billall);
                }
                else
                {
                    LstLedger.ISDEEMEDPOSITIVE = "Yes";
                    LstLedger.ISLASTDEEMEDPOSITIVE = "Yes";
                    LstLedger.AMOUNT = "-" + dr["PartAmount"].ToString();
                    if (dr["PayTypeName"].ToString() != "Cash")
                    {
                        BANKALLOCATIONS billall = new BANKALLOCATIONS();
                        billall.AMOUNT = dr["PartAmount"].ToString();
                        billall.BANKNAME = dr["BankName"].ToString();
                        billall.UNIQUEREFERENCENUMBER = dr["ChequeNo"].ToString();
                        billall.TRANSFERMODE = dr["PayTypeName"].ToString();
                        LstLedger.BANKALLOCATIONS = billall;
                    }
                }
                lst.Add(LstLedger);
            }
            return lst;

        }

        private List<ALLLEDGERENTRIES> FeatchVoucherRefDetails_PurchaseReturn(long PkSrNo)
        {

            if (ObjQry.ReturnLong("Select  Count(*) As Cnt From TVoucherRefDetails As Tvr Where Tvr.FkVoucherTrnno IN(Select PkVoucherTrnno From TVoucherDetails Where FkVoucherNo=" + PkSrNo + ") ", CommonFunctions.ConStr) == 0)
                return null;

            List<ALLLEDGERENTRIES> lst = new List<ALLLEDGERENTRIES>();
            string Sql = " SELECT  MPayType.PayTypeName, TVoucherEntry.Narration, TVoucherEntry.BilledAmount, TVoucherEntry.Remark, " +
                        " TVoucherChqCreditDetails.ChequeNo, TVoucherChqCreditDetails.ChequeDate,   " +
                        " MOtherBank.BankName, TVoucherChqCreditDetails.BranchNo, TVoucherChqCreditDetails.CreditCardNo, " +
                        " TVoucherChqCreditDetails.Amount, '' AS CheckRemark ,MLedger.LedgerName ,TVoucherDetails.SrNo" +
                        " FROM         MOtherBank RIGHT OUTER JOIN " +
                        " TVoucherEntry INNER JOIN " +
                        " MPayType ON TVoucherEntry.PayTypeNo = MPayType.PKPayTypeNo INNER JOIN " +
                        " MLedger INNER JOIN " +
                        " TVoucherDetails ON MLedger.LedgerNo = TVoucherDetails.LedgerNo ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo LEFT OUTER JOIN " +
                        " TVoucherChqCreditDetails ON TVoucherEntry.PkVoucherNo = TVoucherChqCreditDetails.FKVoucherNo ON MOtherBank.BankNo = TVoucherChqCreditDetails.BankNo " +
                        " WHERE     (TVoucherEntry.PkVoucherNo = " + PkSrNo + ")";
            DataTable dt = ObjFunction.GetDataView(Sql).Table;
            foreach (DataRow dr in dt.Rows)
            {
                ALLLEDGERENTRIES LstLedger = new ALLLEDGERENTRIES();
                LstLedger.LEDGERNAME = dr["LedgerName"].ToString();
                if (dr["SrNo"].ToString() == "501")
                {
                    LstLedger.AMOUNT = dr["BilledAmount"].ToString();
                    Sql = " Select Amount,0 AS DiscAmt,(Select Reference From TVoucherEntry  Where PkVoucherNo in (Select FkVoucherNo From TVoucherDetails Where PkVoucherTrnno in (Select FkVoucherTrnno From TVoucherRefDetails Where RefNo=Tvr.RefNo And TypeOfRef=3) And SrNo=501)) as BillNo " +
                       " From TVoucherRefDetails As Tvr Where Tvr.FkVoucherTrnno IN(Select PkVoucherTrnno " +
                       " From TVoucherDetails Where FkVoucherNo=" + PkSrNo + ") ";
                    DataTable dtRef = ObjFunction.GetDataView(Sql).Table;
                    foreach (DataRow drref in dtRef.Rows)
                    {
                        LstLedger.ISDEEMEDPOSITIVE = "No";
                        LstLedger.ISLASTDEEMEDPOSITIVE = "No";

                        BILLALLOCATIONS billall = new BILLALLOCATIONS();
                        billall.AMOUNT = drref["Amount"].ToString();
                        billall.Name = drref["BillNo"].ToString();
                        billall.BILLTYPE = "Agst Ref";
                        LstLedger.BILLALLOCATIONS.Add(billall);
                    }
                }
                else
                {
                    LstLedger.AMOUNT = "-" + dr["BilledAmount"].ToString();
                    LstLedger.ISDEEMEDPOSITIVE = "Yes";
                    LstLedger.ISLASTDEEMEDPOSITIVE = "Yes";

                    if (dr["PayTypeName"].ToString() != "Cash")
                    {
                        BANKALLOCATIONS billall = new BANKALLOCATIONS();
                        billall.AMOUNT = dr["Amount"].ToString();
                        billall.BANKNAME = dr["BankName"].ToString();
                        billall.UNIQUEREFERENCENUMBER = dr["ChequeNo"].ToString();
                        billall.TRANSFERMODE = dr["PayTypeName"].ToString();
                        LstLedger.BANKALLOCATIONS = billall;
                    }
                }
                lst.Add(LstLedger);
            }
            return lst;

        }

        private TALLYMESSAGE featchPurchaseReturnReciptsData(DataRow dtData)
        {

            TALLYMESSAGE tallyMSG = new TALLYMESSAGE();

            VOUCHER vch = new VOUCHER();
            vch.VCHTYPE = "Receipt";
            vch.OBJVIEW = "Accounting Voucher View";
            vch.VOUCHERNUMBER = dtData["VoucherUserNo"].ToString();
            vch.DATE = Convert.ToDateTime(dtData["VoucherDate"].ToString()).ToString("yyyy-MM-dd").Replace("-", "");
            vch.PARTYLEDGERNAME = dtData["LedgerName"].ToString();
            vch.VOUCHERTYPENAME = "Receipt";
            vch.PARTYNAME = dtData["LedgerName"].ToString();
            vch.BASICBASEPARTYNAME = dtData["LedgerName"].ToString();
            vch.PERSISTEDVIEW = "Accounting Voucher View";
            vch.PLACEOFSUPPLY = vch.STATENAME;
            vch.CONSIGNEEGSTIN = vch.PARTYGSTIN;

            //if VoucherPayType Details Not Null

            List<ALLLEDGERENTRIES> lstPayDtls = FeatchVoucherPayTypeDetails_PurchaseReturn(Convert.ToInt64(dtData["PkVoucherNo"].ToString()));
            if (lstPayDtls.Count != 0)
            {
                vch.ALLLEDGERENTRIES = lstPayDtls;
            }
            else
            {
                List<ALLLEDGERENTRIES> lstRef = FeatchVoucherRefDetails_PurchaseReturn(Convert.ToInt64(dtData["PkVoucherNo"].ToString()));
                if (lstRef.Count != 0)
                {
                    vch.ALLLEDGERENTRIES = lstRef;
                }
            }

            tallyMSG.VOUCHER = vch;
            tallyMSG.UDF = "TallyUDF";

            return tallyMSG;
        }


        #endregion

        private void TallyImportExport_Load(object sender, EventArgs e)
        {
            DTPFromDate.Text = "01-" + DBGetVal.ServerTime.ToString("MMM-yyyy");
            DTToDate.Text = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
            DTToDate.MinDate = DTPFromDate.Value;
        }

        #endregion



        private void btnExport_Click(object sender, EventArgs e)
        {
            FromDate = Convert.ToDateTime(DTPFromDate.Text);
            ToDate = Convert.ToDateTime(DTToDate.Text);

            Display.WorkInProgress wip = new Kirana.Display.WorkInProgress();
            wip.StrStatus = "Export All Master And Voucher in process, please wait.";
            wip.bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork_Voucher);
            wip.bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted_Voucher);
            ObjFunction.OpenForm(wip);

        }
    }
}
