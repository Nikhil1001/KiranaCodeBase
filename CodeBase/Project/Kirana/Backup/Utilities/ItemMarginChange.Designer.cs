﻿namespace Kirana.Utilities
{
    partial class ItemMarginChange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnApplyChanges = new System.Windows.Forms.Button();
            this.cmbDepart = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblMsg = new System.Windows.Forms.Label();
            this.lblMargin = new System.Windows.Forms.Label();
            this.txtMargin1 = new System.Windows.Forms.TextBox();
            this.lblCategory = new System.Windows.Forms.Label();
            this.cmbCategory = new System.Windows.Forms.ComboBox();
            this.lbBrand = new System.Windows.Forms.Label();
            this.cmbBrandName = new System.Windows.Forms.ComboBox();
            this.lblPerMargin = new System.Windows.Forms.Label();
            this.txtMargin2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.chkSelect = new System.Windows.Forms.CheckBox();
            this.pnlSKU = new System.Windows.Forms.Panel();
            this.rbSKUAll = new System.Windows.Forms.RadioButton();
            this.rbSkuPartial = new System.Windows.Forms.RadioButton();
            this.rbNone = new System.Windows.Forms.RadioButton();
            this.lblSku = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.rbCategory = new System.Windows.Forms.RadioButton();
            this.rbBrand = new System.Windows.Forms.RadioButton();
            this.dgCategory = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.gvRateSetting = new System.Windows.Forms.DataGridView();
            this.SrNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemMargin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Chck = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.pnlSKU.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRateSetting)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClose.Location = new System.Drawing.Point(253, 272);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(80, 60);
            this.btnClose.TabIndex = 12;
            this.btnClose.Text = "E&xit";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.Location = new System.Drawing.Point(156, 272);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 60);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnApplyChanges
            // 
            this.btnApplyChanges.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnApplyChanges.Location = new System.Drawing.Point(12, 272);
            this.btnApplyChanges.Name = "btnApplyChanges";
            this.btnApplyChanges.Size = new System.Drawing.Size(130, 60);
            this.btnApplyChanges.TabIndex = 10;
            this.btnApplyChanges.Text = "Apply Changes";
            this.btnApplyChanges.UseVisualStyleBackColor = true;
            this.btnApplyChanges.Click += new System.EventHandler(this.btnApplyChanges_Click);
            // 
            // cmbDepart
            // 
            this.cmbDepart.FormattingEnabled = true;
            this.cmbDepart.Location = new System.Drawing.Point(100, 40);
            this.cmbDepart.Name = "cmbDepart";
            this.cmbDepart.Size = new System.Drawing.Size(170, 21);
            this.cmbDepart.TabIndex = 0;
            this.cmbDepart.Leave += new System.EventHandler(this.cmbDepart_Leave);
            this.cmbDepart.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbDepart_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(10, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 508;
            this.label2.Text = "Department";
            // 
            // lblMsg
            // 
            this.lblMsg.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.lblMsg.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMsg.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg.ForeColor = System.Drawing.Color.Maroon;
            this.lblMsg.Location = new System.Drawing.Point(73, 133);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(481, 52);
            this.lblMsg.TabIndex = 514;
            this.lblMsg.Text = "label4";
            this.lblMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMsg.Visible = false;
            // 
            // lblMargin
            // 
            this.lblMargin.AutoSize = true;
            this.lblMargin.BackColor = System.Drawing.Color.Transparent;
            this.lblMargin.Location = new System.Drawing.Point(564, 42);
            this.lblMargin.Name = "lblMargin";
            this.lblMargin.Size = new System.Drawing.Size(39, 13);
            this.lblMargin.TabIndex = 515;
            this.lblMargin.Text = "Margin";
            // 
            // txtMargin1
            // 
            this.txtMargin1.Enabled = false;
            this.txtMargin1.Location = new System.Drawing.Point(619, 39);
            this.txtMargin1.Name = "txtMargin1";
            this.txtMargin1.Size = new System.Drawing.Size(60, 20);
            this.txtMargin1.TabIndex = 3;
            this.txtMargin1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMargin1.TextChanged += new System.EventHandler(this.txtMargin1_TextChanged);
            this.txtMargin1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMargin1_KeyDown);
            // 
            // lblCategory
            // 
            this.lblCategory.AutoSize = true;
            this.lblCategory.BackColor = System.Drawing.Color.Transparent;
            this.lblCategory.Location = new System.Drawing.Point(281, 43);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(49, 13);
            this.lblCategory.TabIndex = 5009;
            this.lblCategory.Text = "Category";
            // 
            // cmbCategory
            // 
            this.cmbCategory.FormattingEnabled = true;
            this.cmbCategory.Location = new System.Drawing.Point(371, 39);
            this.cmbCategory.Name = "cmbCategory";
            this.cmbCategory.Size = new System.Drawing.Size(170, 21);
            this.cmbCategory.TabIndex = 5010;
            this.cmbCategory.Leave += new System.EventHandler(this.cmbCategory_Leave);
            this.cmbCategory.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbCategory_KeyDown);
            // 
            // lbBrand
            // 
            this.lbBrand.AutoSize = true;
            this.lbBrand.BackColor = System.Drawing.Color.Transparent;
            this.lbBrand.Location = new System.Drawing.Point(712, 41);
            this.lbBrand.Name = "lbBrand";
            this.lbBrand.Size = new System.Drawing.Size(66, 13);
            this.lbBrand.TabIndex = 5011;
            this.lbBrand.Text = "Brand Name";
            // 
            // cmbBrandName
            // 
            this.cmbBrandName.FormattingEnabled = true;
            this.cmbBrandName.Location = new System.Drawing.Point(796, 37);
            this.cmbBrandName.Name = "cmbBrandName";
            this.cmbBrandName.Size = new System.Drawing.Size(170, 21);
            this.cmbBrandName.TabIndex = 5012;
            this.cmbBrandName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbBrandName_KeyDown);
            // 
            // lblPerMargin
            // 
            this.lblPerMargin.AutoSize = true;
            this.lblPerMargin.BackColor = System.Drawing.Color.Transparent;
            this.lblPerMargin.Location = new System.Drawing.Point(682, 41);
            this.lblPerMargin.Name = "lblPerMargin";
            this.lblPerMargin.Size = new System.Drawing.Size(15, 13);
            this.lblPerMargin.TabIndex = 5013;
            this.lblPerMargin.Text = "%";
            // 
            // txtMargin2
            // 
            this.txtMargin2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtMargin2.Location = new System.Drawing.Point(70, 242);
            this.txtMargin2.Name = "txtMargin2";
            this.txtMargin2.Size = new System.Drawing.Size(72, 20);
            this.txtMargin2.TabIndex = 5014;
            this.txtMargin2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMargin2.TextChanged += new System.EventHandler(this.txtMargin2_TextChanged);
            this.txtMargin2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMargin2_KeyDown);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(11, 246);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 5015;
            this.label1.Text = "Margin";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(146, 245);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 13);
            this.label3.TabIndex = 5016;
            this.label3.Text = "%";
            // 
            // chkSelect
            // 
            this.chkSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkSelect.AutoSize = true;
            this.chkSelect.BackColor = System.Drawing.Color.Transparent;
            this.chkSelect.Location = new System.Drawing.Point(383, 244);
            this.chkSelect.Name = "chkSelect";
            this.chkSelect.Size = new System.Drawing.Size(67, 17);
            this.chkSelect.TabIndex = 5021;
            this.chkSelect.Text = "SelectAll";
            this.chkSelect.UseVisualStyleBackColor = true;
            this.chkSelect.CheckedChanged += new System.EventHandler(this.chkSelect_CheckedChanged);
            // 
            // pnlSKU
            // 
            this.pnlSKU.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlSKU.BackColor = System.Drawing.Color.Transparent;
            this.pnlSKU.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSKU.Controls.Add(this.rbSKUAll);
            this.pnlSKU.Controls.Add(this.rbSkuPartial);
            this.pnlSKU.Controls.Add(this.rbNone);
            this.pnlSKU.Controls.Add(this.lblSku);
            this.pnlSKU.Location = new System.Drawing.Point(381, 272);
            this.pnlSKU.Name = "pnlSKU";
            this.pnlSKU.Size = new System.Drawing.Size(507, 60);
            this.pnlSKU.TabIndex = 5019;
            this.pnlSKU.Visible = false;
            // 
            // rbSKUAll
            // 
            this.rbSKUAll.AutoSize = true;
            this.rbSKUAll.Location = new System.Drawing.Point(322, 27);
            this.rbSKUAll.Name = "rbSKUAll";
            this.rbSKUAll.Size = new System.Drawing.Size(107, 17);
            this.rbSKUAll.TabIndex = 3;
            this.rbSKUAll.TabStop = true;
            this.rbSKUAll.Text = "Apply to All SKUs";
            this.rbSKUAll.UseVisualStyleBackColor = true;
            // 
            // rbSkuPartial
            // 
            this.rbSkuPartial.AutoSize = true;
            this.rbSkuPartial.Location = new System.Drawing.Point(113, 27);
            this.rbSkuPartial.Name = "rbSkuPartial";
            this.rbSkuPartial.Size = new System.Drawing.Size(175, 17);
            this.rbSkuPartial.TabIndex = 2;
            this.rbSkuPartial.TabStop = true;
            this.rbSkuPartial.Text = "Apply To SKU ( 0 or NULL only)";
            this.rbSkuPartial.UseVisualStyleBackColor = true;
            // 
            // rbNone
            // 
            this.rbNone.AutoSize = true;
            this.rbNone.Checked = true;
            this.rbNone.Location = new System.Drawing.Point(7, 27);
            this.rbNone.Name = "rbNone";
            this.rbNone.Size = new System.Drawing.Size(51, 17);
            this.rbNone.TabIndex = 1;
            this.rbNone.TabStop = true;
            this.rbNone.Text = "None";
            this.rbNone.UseVisualStyleBackColor = true;
            // 
            // lblSku
            // 
            this.lblSku.AutoSize = true;
            this.lblSku.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSku.Location = new System.Drawing.Point(4, 4);
            this.lblSku.Name = "lblSku";
            this.lblSku.Size = new System.Drawing.Size(32, 13);
            this.lblSku.TabIndex = 0;
            this.lblSku.Text = "SKU";
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOk.Location = new System.Drawing.Point(208, 240);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 5017;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(10, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Select Type :";
            // 
            // rbCategory
            // 
            this.rbCategory.AutoSize = true;
            this.rbCategory.BackColor = System.Drawing.Color.Transparent;
            this.rbCategory.Location = new System.Drawing.Point(267, 8);
            this.rbCategory.Name = "rbCategory";
            this.rbCategory.Size = new System.Drawing.Size(67, 17);
            this.rbCategory.TabIndex = 1;
            this.rbCategory.TabStop = true;
            this.rbCategory.Text = "Category";
            this.rbCategory.UseVisualStyleBackColor = false;
            // 
            // rbBrand
            // 
            this.rbBrand.AutoSize = true;
            this.rbBrand.BackColor = System.Drawing.Color.Transparent;
            this.rbBrand.Checked = true;
            this.rbBrand.Location = new System.Drawing.Point(120, 8);
            this.rbBrand.Name = "rbBrand";
            this.rbBrand.Size = new System.Drawing.Size(53, 17);
            this.rbBrand.TabIndex = 0;
            this.rbBrand.TabStop = true;
            this.rbBrand.Text = "Brand";
            this.rbBrand.UseVisualStyleBackColor = false;
            this.rbBrand.CheckedChanged += new System.EventHandler(this.rbBrand_CheckedChanged);
            // 
            // dgCategory
            // 
            this.dgCategory.AllowUserToAddRows = false;
            this.dgCategory.AllowUserToDeleteRows = false;
            this.dgCategory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgCategory.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewComboBoxColumn1});
            this.dgCategory.Location = new System.Drawing.Point(67, 68);
            this.dgCategory.Name = "dgCategory";
            this.dgCategory.Size = new System.Drawing.Size(876, 164);
            this.dgCategory.TabIndex = 5022;
            this.dgCategory.Visible = false;
            this.dgCategory.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgCategory_CellLeave);
            this.dgCategory.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgCategory_CellEndEdit);
            this.dgCategory.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgCategory_EditingControlShowing);
            this.dgCategory.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgCategory_KeyDown);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "SrNo";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewTextBoxColumn1.HeaderText = "Sr";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 40;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Category";
            this.dataGridViewTextBoxColumn2.HeaderText = "Category";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 500;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Margin";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = null;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewTextBoxColumn3.HeaderText = "Margin";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 180;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "StockGroupNo";
            this.dataGridViewTextBoxColumn4.HeaderText = "CategoryNo";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.DataPropertyName = "chck";
            this.dataGridViewComboBoxColumn1.HeaderText = "Select ";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // gvRateSetting
            // 
            this.gvRateSetting.AllowUserToAddRows = false;
            this.gvRateSetting.AllowUserToDeleteRows = false;
            this.gvRateSetting.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvRateSetting.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SrNo,
            this.ItemName,
            this.ItemMargin,
            this.ItemNo,
            this.Chck});
            this.gvRateSetting.Location = new System.Drawing.Point(12, 68);
            this.gvRateSetting.Name = "gvRateSetting";
            this.gvRateSetting.Size = new System.Drawing.Size(876, 164);
            this.gvRateSetting.TabIndex = 5021;
            this.gvRateSetting.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.gvRateSetting_CellLeave);
            this.gvRateSetting.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.gvRateSetting_EditingControlShowing_1);
            this.gvRateSetting.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gvRateSetting_KeyDown);
            // 
            // SrNo
            // 
            this.SrNo.DataPropertyName = "SrNo";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.SrNo.DefaultCellStyle = dataGridViewCellStyle3;
            this.SrNo.HeaderText = "Sr";
            this.SrNo.Name = "SrNo";
            this.SrNo.ReadOnly = true;
            this.SrNo.Width = 40;
            // 
            // ItemName
            // 
            this.ItemName.DataPropertyName = "ItemName";
            this.ItemName.HeaderText = "Item Name";
            this.ItemName.Name = "ItemName";
            this.ItemName.ReadOnly = true;
            this.ItemName.Width = 500;
            // 
            // ItemMargin
            // 
            this.ItemMargin.DataPropertyName = "Margin";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.ItemMargin.DefaultCellStyle = dataGridViewCellStyle4;
            this.ItemMargin.HeaderText = "Margin";
            this.ItemMargin.Name = "ItemMargin";
            this.ItemMargin.Width = 180;
            // 
            // ItemNo
            // 
            this.ItemNo.DataPropertyName = "ItemNo";
            this.ItemNo.HeaderText = "ItemNo";
            this.ItemNo.Name = "ItemNo";
            this.ItemNo.ReadOnly = true;
            this.ItemNo.Visible = false;
            // 
            // Chck
            // 
            this.Chck.DataPropertyName = "chck";
            this.Chck.HeaderText = "Select ";
            this.Chck.Name = "Chck";
            this.Chck.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // ItemMarginChange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 337);
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.dgCategory);
            this.Controls.Add(this.chkSelect);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pnlSKU);
            this.Controls.Add(this.rbCategory);
            this.Controls.Add(this.btnApplyChanges);
            this.Controls.Add(this.rbBrand);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lbBrand);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.cmbCategory);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmbBrandName);
            this.Controls.Add(this.txtMargin2);
            this.Controls.Add(this.lblCategory);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblPerMargin);
            this.Controls.Add(this.lblMargin);
            this.Controls.Add(this.cmbDepart);
            this.Controls.Add(this.txtMargin1);
            this.Controls.Add(this.gvRateSetting);
            this.Name = "ItemMarginChange";
            this.Text = "Item Margin Change";
            this.Load += new System.EventHandler(this.ItemMarginChange_Load);
            this.pnlSKU.ResumeLayout(false);
            this.pnlSKU.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRateSetting)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnApplyChanges;
        private System.Windows.Forms.ComboBox cmbDepart;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.Label lblMargin;
        private System.Windows.Forms.TextBox txtMargin1;
        private System.Windows.Forms.Label lblCategory;
        private System.Windows.Forms.ComboBox cmbCategory;
        private System.Windows.Forms.Label lbBrand;
        private System.Windows.Forms.ComboBox cmbBrandName;
        private System.Windows.Forms.Label lblPerMargin;
        private System.Windows.Forms.TextBox txtMargin2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton rbCategory;
        private System.Windows.Forms.RadioButton rbBrand;
        private System.Windows.Forms.Panel pnlSKU;
        private System.Windows.Forms.Label lblSku;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.RadioButton rbSKUAll;
        private System.Windows.Forms.RadioButton rbSkuPartial;
        private System.Windows.Forms.RadioButton rbNone;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkSelect;
        private System.Windows.Forms.DataGridView dgCategory;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridView gvRateSetting;
        private System.Windows.Forms.DataGridViewTextBoxColumn SrNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemMargin;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemNo;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Chck;
    }
}