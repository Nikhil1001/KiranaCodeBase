﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Utilities
{
    /// <summary>
    /// This class is used for Ledger Opening Balance
    /// </summary>
    public partial class LedgerOpeningBalance : Form
    {
        DBMLedger dbmledger = new DBMLedger();
        CommonFunctions ObjFunction = new CommonFunctions();
        MLedger mLedger = new MLedger();
        int GroupTypeCode;

        #region Initialize And Load Related Methods
        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public LedgerOpeningBalance()
        {
            InitializeComponent();
        }
        /// <summary>
        /// This is class of parameterised Constructor
        /// </summary>
        public LedgerOpeningBalance(int type)
        {
            InitializeComponent();
            GroupTypeCode = type;
            if (GroupTypeCode == 1)
            {
                this.Text = "Customer Opening Balance ";
                rdAllLedger.Text = "All Customer";
                rdLedgerwise.Text = "Customer Wise";
                TotalLedger.Text = "Total Customer :";
                lblChkHelp.Text = "To Pay enter data in format -200\r\nTo Receive enter data in format  200\r\n\r\n";
            }
            else if (GroupTypeCode == 2)
            {

                this.Text = "Supplier Opening Balance ";
                rdAllLedger.Text = "All Supplier";
                rdLedgerwise.Text = "Supplier Wise";
                TotalLedger.Text = "Total Supplier :";
                lblChkHelp.Text = "To Pay enter data in format 200\r\nTo Receive enter data in format  -200\r\n\r\n";
            }
            else if (GroupTypeCode == 3)
            {

                this.Text = "Bank Opening Balance ";
                rdAllLedger.Text = "All Bank";
                rdLedgerwise.Text = "Bank Wise";
                TotalLedger.Text = "Total Bank :";
                lblChkHelp.Text = "To Debit enter data in format 200\r\nTo Credit enter data in format  -200\r\n\r\n";
            }
            else if (GroupTypeCode == 4)
            {

                this.Text = "Tax Opening Balance ";
                rdAllLedger.Text = "All Tax";
                rdLedgerwise.Text = "Tax Wise";
                TotalLedger.Text = "Total Tax :";
                lblChkHelp.Text = "To Debit enter data in format 200\r\nTo Credit enter data in format  -200\r\n\r\n";
            }
            else if (GroupTypeCode == 5)
            {

                this.Text = "Other Opening Balance ";
                rdAllLedger.Text = "All Other";
                rdLedgerwise.Text = "Other Wise";
                TotalLedger.Text = "Total Other :";
                lblChkHelp.Text = "To Debit enter data in format 200\r\nTo Credit enter data in format  -200\r\n\r\n";
            }
        }

        private void LedgerOpeningBalance_Load(object sender, EventArgs e)
        {
            try
            {
                DataGridViewComboBoxColumn cmbSign = GridLedger.Columns[4] as DataGridViewComboBoxColumn;
                ObjFunction.FillComb(cmbSign, "SELECT SignCode, SignName FROM MSign");
                txtTotal.Text = "0";
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #endregion

        #region Radio Button Related Methods

        private void rdAllLedger_CheckedChanged(object sender, EventArgs e)
        {
            txtBoxSearch.Visible = false;
            TotalLedger.Visible = true;
            txtTotal.Visible = true;
            BindGrid();
        }

        private void rdLedgerwise_CheckedChanged(object sender, EventArgs e)
        {
            txtBoxSearch.Text = "";
            txtBoxSearch.Visible = true;
            txtBoxSearch.Focus();
            txtTotal.Text = "0";
            btnApplyChange.Enabled = false;
            GridLedger.Rows.Clear();
        }

        #endregion

        #region Grid Related Methods

        private void BindGrid()
        {
            try
            {
                string strQuery;
                strQuery = "SELECT 0 as SrNo, MLedger.LedgerName, MGroup.GroupName, MLedger.OpeningBalance,  MSign.SignCode ,  MLedger.LedgerNo, 'false' as chk,MLedger.OpeningBalance As OpBal " +
                           ",(Case When((Select count(*) from TVoucherRefDetails where LedgerNo=MLedger.LedgerNo and TypeOfRef=5)=0) Then 'false' else 'true' End) As chkOp " +
                           "FROM MGroup INNER JOIN " +
                           "MLedger ON MGroup.GroupNo = MLedger.GroupNo INNER JOIN " +
                                    "MSign ON MLedger.SignCode = MSign.SignCode ";
                if (GroupTypeCode == 1)
                    strQuery += " WHERE  MLedger.GroupNo IN (" + GroupType.SundryDebtors + ")";
                else if (GroupTypeCode == 2)
                    strQuery += " WHERE MLedger.GroupNo IN (" + GroupType.SundryCreditors + ")";
                else if (GroupTypeCode == 3)
                    strQuery += " WHERE MLedger.GroupNo IN (" + GroupType.BankAccounts + "," + GroupType.CashInhand + ")";
                else if (GroupTypeCode == 4)
                    strQuery += " WHERE MLedger.GroupNo IN (" + GroupType.DutiesAndTaxes + ") OR MLedger.GroupNo IN (select GroupNo From MGroup Where ControlGroup in(" + GroupType.DutiesAndTaxes + "))";
                else if (GroupTypeCode == 5)
                    strQuery += " WHERE MLedger.GroupNo NOT IN (" + GroupType.SundryDebtors + "," + GroupType.SundryCreditors + "," + GroupType.BankAccounts + "," + GroupType.DutiesAndTaxes + "," + GroupType.CashInhand + ") AND MLedger.GroupNo Not IN (select GroupNo From MGroup Where ControlGroup in(" + GroupType.DutiesAndTaxes + "))";
                if (rdAllLedger.Checked == false)
                    strQuery += " AND (MLedger.LedgerName LIKE '" + txtBoxSearch.Text.Trim() + "%') ";
                strQuery += " ORDER BY MLedger.LedgerName ";
                DataTable dt = ObjFunction.GetDataView(strQuery).Table;
                txtTotal.Text = dt.Rows.Count.ToString();
                GridLedger.Rows.Clear();
                GridLedger.Columns[0].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                GridLedger.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    GridLedger.Rows.Add();
                    for (int j = 0; j < GridLedger.Columns.Count; j++)
                    {
                        GridLedger.Rows[i].Cells[j].Value = dt.Rows[i].ItemArray[j];

                        if (j == 3 || j == 7)
                        {
                            if (GroupTypeCode == 1 || GroupTypeCode == 2)
                                GridLedger.Rows[i].Cells[j].ReadOnly = Convert.ToBoolean(dt.Rows[i].ItemArray[8].ToString());

                            if (Convert.ToInt32(dt.Rows[i].ItemArray[4]) == 1)
                            {
                                if (GroupTypeCode == 1)
                                    GridLedger.Rows[i].Cells[j].Value = Math.Abs(Convert.ToInt32(dt.Rows[i].ItemArray[j]));
                                else if (GroupTypeCode == 2)
                                    GridLedger.Rows[i].Cells[j].Value = Convert.ToInt32(dt.Rows[i].ItemArray[j]) * -1;
                            }
                            else if (Convert.ToInt32(dt.Rows[i].ItemArray[4]) == 2)
                            {
                                if (GroupTypeCode == 1)
                                    GridLedger.Rows[i].Cells[j].Value = Convert.ToInt32(dt.Rows[i].ItemArray[j]) * -1;
                                else if (GroupTypeCode == 2)
                                    GridLedger.Rows[i].Cells[j].Value = Math.Abs(Convert.ToInt32(dt.Rows[i].ItemArray[j]));
                            }
                        }

                    }
                }
                if (dt.Rows.Count <= 0)
                {
                    btnApplyChange.Enabled = false;
                    GridLedger.Visible = true;
                }
                else
                {
                    if (GroupTypeCode == 1 || GroupTypeCode == 2)
                    {
                        GridLedger.Columns[2].Visible = false;
                        GridLedger.Columns[4].Visible = false;
                    }


                    btnApplyChange.Enabled = true;
                    GridLedger.Visible = true;
                    TotalLedger.Visible = true;
                    txtTotal.Visible = true;
                    if (rdAllLedger.Checked == true)
                    {
                        GridLedger.Focus();
                        GridLedger.CurrentCell = GridLedger[3, 0];
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void GridLedger_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                bool flag = true;
                for (int i = 0; i < GridLedger.Rows.Count; i++)
                {
                    if (e.ColumnIndex == 3)
                    {
                        if (GridLedger.Rows[i].Cells[3].Value != null)
                        {
                            GridLedger.Rows[i].Cells[3].ErrorText = "";
                            if (ObjFunction.CheckValidAmount(GridLedger.Rows[i].Cells[3].Value.ToString()) == false)
                            {
                                GridLedger.Rows[i].Cells[3].ErrorText = "Please Enter Valid Amount";
                                flag = false;
                                break;
                            }
                            else
                            {
                                if (Convert.ToDouble(GridLedger.Rows[i].Cells[3].Value.ToString()) != Convert.ToDouble(GridLedger.Rows[i].Cells[7].Value.ToString()))
                                    GridLedger.Rows[i].Cells[6].Value = true;
                                else
                                    GridLedger.Rows[i].Cells[6].Value = false;
                                flag = true;
                            }
                        }
                        else
                        {
                            GridLedger.Rows[i].Cells[3].ErrorText = "Please Enter  Amount";
                            flag = false;
                            break;
                        }
                    }
                }
                if (flag == true)
                    btnApplyChange.Enabled = true;
                else
                    btnApplyChange.Enabled = false;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void GridLedger_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btnApplyChange.Focus();
            }
        }

        private void GridLedger_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                e.Value = e.RowIndex + 1;
            }
            if (e.ColumnIndex == 0)
            {
                if (GroupTypeCode == 1 || GroupTypeCode == 2)
                {
                    if (GridLedger.Rows[e.RowIndex].Cells[3].ReadOnly == true)
                        GridLedger.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.CornflowerBlue;
                    else
                        GridLedger.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                }
            }

        }

        #endregion

        #region Button And Text Box Related Methods

        private void txtBoxSearch_TextChanged(object sender, EventArgs e)
        {
            BindGrid();
        }

        private void txtBoxSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                if (GridLedger.Rows.Count > 0)
                {
                    GridLedger.Focus();
                    GridLedger.CurrentCell = GridLedger[3, 0];
                }
            }
        }

        private void DisplayMessage(string str)
        {
            lblMsg.Visible = true;
            lblMsg.Text = str;
            Application.DoEvents();
            System.Threading.Thread.Sleep(1200);
            lblMsg.Visible = false;
        }

        private void btnApplyChange_Click(object sender, EventArgs e)
        {
            try
            {
                int cnt = 0;
                if (OMMessageBox.Show("Are you sure want to change Ledger opening balances?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                {
                    for (int i = 0; i < GridLedger.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(GridLedger.Rows[i].Cells[6].FormattedValue) == true)
                        {
                            cnt = +1;
                            mLedger = new MLedger();
                            mLedger.LedgerNo = Convert.ToInt64(GridLedger.Rows[i].Cells[5].Value);
                            if (Convert.ToDouble(GridLedger.Rows[i].Cells[3].Value) > 0)
                                mLedger.SignCode = GroupTypeCode == 2 ? 2 : 1;//Convert.ToInt64(GridLedger.Rows[i].Cells[4].Value);
                            else
                                mLedger.SignCode = GroupTypeCode == 2 ? 1 : 2;// 2;//Convert.ToInt64(GridLedger.Rows[i].Cells[4].Value);
                            mLedger.OpeningBalance = Math.Abs(Convert.ToDouble(GridLedger.Rows[i].Cells[3].Value));
                            dbmledger.UpdateLedgerBalance(mLedger);
                        }
                    }
                    if (cnt != 0)
                    {
                        if (dbmledger.ExecuteNonQueryStatements() == true)
                        {
                            DisplayMessage("Opening Balance Saved Successfully");
                            BindGrid();
                            txtBoxSearch.Text = "";
                        }
                    }
                    else
                    {
                        OMMessageBox.Show("Please select atleast one ledger...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        BindGrid();
                        if (rdLedgerwise.Checked == true)
                            txtBoxSearch.Focus();
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        private void GridLedger_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (GridLedger.CurrentCell.ColumnIndex == 3)
            {
                TextBox txtOP = (TextBox)e.Control;
                txtOP.TextChanged += new EventHandler(txtOP_TextChanged);
            }
        }

        private void txtOP_TextChanged(object sender, EventArgs e)
        {
            if (GridLedger.CurrentCell.ColumnIndex == 3)
            {
                ObjFunction.SetMasked((TextBox)sender, 2, 9, JitFunctions.MaskedType.PositiveNegative);
            }
        }
    }
}
