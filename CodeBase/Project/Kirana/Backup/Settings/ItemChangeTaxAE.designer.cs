﻿namespace Kirana.Settings
{
    partial class ItemChangeTaxAE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new JitControls.OMBPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.cmbGroupNo2 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbDepartmentName = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbCategoryName = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pnlrb = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.rdAssigned = new System.Windows.Forms.RadioButton();
            this.rdNotAssigned = new System.Windows.Forms.RadioButton();
            this.rdBoth = new System.Windows.Forms.RadioButton();
            this.chkSelectAll = new System.Windows.Forms.CheckBox();
            this.dgvTaxItem = new System.Windows.Forms.DataGridView();
            this.Chechk = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BarCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SVAT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PVAT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SalesLedgNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PurLedgNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HSNCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EP = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnSetTax = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblMsg = new JitControls.OMLabel();
            this.lblStar10 = new System.Windows.Forms.Label();
            this.lblStar9 = new System.Windows.Forms.Label();
            this.cmbVatPurchase = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.cmbVatSales = new System.Windows.Forms.ComboBox();
            this.pnlTAx = new JitControls.OMBPanel();
            this.cmbHSNCode = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPurVat = new System.Windows.Forms.Button();
            this.btnSalesVat = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtHSNCode = new System.Windows.Forms.TextBox();
            this.BtnAddGST = new System.Windows.Forms.Button();
            this.lblChkHelp2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.pnlrb.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTaxItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EP)).BeginInit();
            this.pnlTAx.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BorderColor = System.Drawing.Color.Gray;
            this.panel1.BorderRadius = 3;
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.txtBarcode);
            this.panel1.Controls.Add(this.cmbGroupNo2);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.cmbDepartmentName);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.cmbCategoryName);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.pnlrb);
            this.panel1.Location = new System.Drawing.Point(9, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1006, 64);
            this.panel1.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(407, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 511;
            this.label7.Text = "Barcode :";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(512, 5);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(200, 20);
            this.txtBarcode.TabIndex = 1;
            this.txtBarcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarcode_KeyDown);
            // 
            // cmbGroupNo2
            // 
            this.cmbGroupNo2.FormattingEnabled = true;
            this.cmbGroupNo2.Location = new System.Drawing.Point(108, 5);
            this.cmbGroupNo2.Name = "cmbGroupNo2";
            this.cmbGroupNo2.Size = new System.Drawing.Size(200, 21);
            this.cmbGroupNo2.TabIndex = 0;
            this.cmbGroupNo2.Leave += new System.EventHandler(this.cmbGroupNo2_Leave);
            this.cmbGroupNo2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbGroupNo2_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 505;
            this.label5.Text = "Department :";
            // 
            // cmbDepartmentName
            // 
            this.cmbDepartmentName.FormattingEnabled = true;
            this.cmbDepartmentName.Location = new System.Drawing.Point(108, 32);
            this.cmbDepartmentName.Name = "cmbDepartmentName";
            this.cmbDepartmentName.Size = new System.Drawing.Size(200, 21);
            this.cmbDepartmentName.TabIndex = 2;
            this.cmbDepartmentName.Leave += new System.EventHandler(this.cmbDepartmentName_Leave);
            this.cmbDepartmentName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbDepartmentName_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(408, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 504;
            this.label4.Text = "Category :";
            // 
            // cmbCategoryName
            // 
            this.cmbCategoryName.FormattingEnabled = true;
            this.cmbCategoryName.Location = new System.Drawing.Point(512, 34);
            this.cmbCategoryName.Name = "cmbCategoryName";
            this.cmbCategoryName.Size = new System.Drawing.Size(200, 21);
            this.cmbCategoryName.TabIndex = 3;
            this.cmbCategoryName.Leave += new System.EventHandler(this.cmbCategoryName_Leave);
            this.cmbCategoryName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbCategoryName_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 503;
            this.label3.Text = "Brand Name :";
            // 
            // pnlrb
            // 
            this.pnlrb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlrb.BackColor = System.Drawing.Color.Transparent;
            this.pnlrb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlrb.Controls.Add(this.label2);
            this.pnlrb.Controls.Add(this.rdAssigned);
            this.pnlrb.Controls.Add(this.rdNotAssigned);
            this.pnlrb.Controls.Add(this.rdBoth);
            this.pnlrb.Location = new System.Drawing.Point(733, 10);
            this.pnlrb.Name = "pnlrb";
            this.pnlrb.Size = new System.Drawing.Size(258, 43);
            this.pnlrb.TabIndex = 510;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(110, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 10009;
            this.label2.Text = "HSN Code";
            // 
            // rdAssigned
            // 
            this.rdAssigned.AutoSize = true;
            this.rdAssigned.BackColor = System.Drawing.Color.Transparent;
            this.rdAssigned.Location = new System.Drawing.Point(15, 20);
            this.rdAssigned.Name = "rdAssigned";
            this.rdAssigned.Size = new System.Drawing.Size(68, 17);
            this.rdAssigned.TabIndex = 502;
            this.rdAssigned.TabStop = true;
            this.rdAssigned.Text = "Assigned";
            this.rdAssigned.UseVisualStyleBackColor = false;
            this.rdAssigned.Click += new System.EventHandler(this.rd_Click);
            // 
            // rdNotAssigned
            // 
            this.rdNotAssigned.AutoSize = true;
            this.rdNotAssigned.BackColor = System.Drawing.Color.Transparent;
            this.rdNotAssigned.Location = new System.Drawing.Point(91, 20);
            this.rdNotAssigned.Name = "rdNotAssigned";
            this.rdNotAssigned.Size = new System.Drawing.Size(88, 17);
            this.rdNotAssigned.TabIndex = 508;
            this.rdNotAssigned.TabStop = true;
            this.rdNotAssigned.Text = "Not Assigned";
            this.rdNotAssigned.UseVisualStyleBackColor = false;
            this.rdNotAssigned.Click += new System.EventHandler(this.rd_Click);
            // 
            // rdBoth
            // 
            this.rdBoth.AutoSize = true;
            this.rdBoth.BackColor = System.Drawing.Color.Transparent;
            this.rdBoth.Location = new System.Drawing.Point(185, 20);
            this.rdBoth.Name = "rdBoth";
            this.rdBoth.Size = new System.Drawing.Size(47, 17);
            this.rdBoth.TabIndex = 509;
            this.rdBoth.TabStop = true;
            this.rdBoth.Text = "Both";
            this.rdBoth.UseVisualStyleBackColor = false;
            this.rdBoth.Click += new System.EventHandler(this.rd_Click);
            // 
            // chkSelectAll
            // 
            this.chkSelectAll.AutoSize = true;
            this.chkSelectAll.BackColor = System.Drawing.Color.Transparent;
            this.chkSelectAll.Location = new System.Drawing.Point(26, 83);
            this.chkSelectAll.Name = "chkSelectAll";
            this.chkSelectAll.Size = new System.Drawing.Size(91, 17);
            this.chkSelectAll.TabIndex = 11;
            this.chkSelectAll.Text = "Select All (F2)";
            this.chkSelectAll.UseVisualStyleBackColor = false;
            this.chkSelectAll.CheckedChanged += new System.EventHandler(this.chkSelectAll_CheckedChanged);
            // 
            // dgvTaxItem
            // 
            this.dgvTaxItem.AllowUserToAddRows = false;
            this.dgvTaxItem.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvTaxItem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Chechk,
            this.dataGridViewTextBoxColumn1,
            this.BarCode,
            this.dataGridViewTextBoxColumn3,
            this.SVAT,
            this.PVAT,
            this.SalesLedgNo,
            this.PurLedgNo,
            this.ItemNo,
            this.HSNCode});
            this.dgvTaxItem.Location = new System.Drawing.Point(9, 105);
            this.dgvTaxItem.Name = "dgvTaxItem";
            this.dgvTaxItem.Size = new System.Drawing.Size(1006, 294);
            this.dgvTaxItem.TabIndex = 10;
            this.dgvTaxItem.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvTaxItem_CellFormatting);
            this.dgvTaxItem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvTaxItem_KeyDown);
            // 
            // Chechk
            // 
            this.Chechk.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Chechk.HeaderText = "";
            this.Chechk.Name = "Chechk";
            this.Chechk.Width = 50;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "SrNo";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridViewTextBoxColumn1.HeaderText = "SrNo";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 50;
            // 
            // BarCode
            // 
            this.BarCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.BarCode.DataPropertyName = "Barcode";
            this.BarCode.HeaderText = "BarCode";
            this.BarCode.Name = "BarCode";
            this.BarCode.ReadOnly = true;
            this.BarCode.Width = 180;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "ItemName";
            this.dataGridViewTextBoxColumn3.HeaderText = "Item Name";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // SVAT
            // 
            this.SVAT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.SVAT.DataPropertyName = "SVat";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.SVAT.DefaultCellStyle = dataGridViewCellStyle17;
            this.SVAT.HeaderText = "GST %";
            this.SVAT.Name = "SVAT";
            this.SVAT.ReadOnly = true;
            // 
            // PVAT
            // 
            this.PVAT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.PVAT.DataPropertyName = "PVat";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PVAT.DefaultCellStyle = dataGridViewCellStyle18;
            this.PVAT.HeaderText = "Purchase %";
            this.PVAT.Name = "PVAT";
            this.PVAT.ReadOnly = true;
            this.PVAT.Visible = false;
            // 
            // SalesLedgNo
            // 
            this.SalesLedgNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.SalesLedgNo.DataPropertyName = "SalesLedgerNo";
            this.SalesLedgNo.HeaderText = "SalesLedgNo";
            this.SalesLedgNo.Name = "SalesLedgNo";
            this.SalesLedgNo.ReadOnly = true;
            this.SalesLedgNo.Visible = false;
            // 
            // PurLedgNo
            // 
            this.PurLedgNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.PurLedgNo.DataPropertyName = "PurLedgerNo";
            this.PurLedgNo.HeaderText = "PurLedgNo";
            this.PurLedgNo.Name = "PurLedgNo";
            this.PurLedgNo.ReadOnly = true;
            this.PurLedgNo.Visible = false;
            // 
            // ItemNo
            // 
            this.ItemNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ItemNo.DataPropertyName = "ItemNo";
            this.ItemNo.HeaderText = "ItemNo";
            this.ItemNo.Name = "ItemNo";
            this.ItemNo.ReadOnly = true;
            this.ItemNo.Visible = false;
            // 
            // HSNCode
            // 
            this.HSNCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.HSNCode.DataPropertyName = "HSNCode";
            this.HSNCode.HeaderText = "HSN Code";
            this.HSNCode.Name = "HSNCode";
            this.HSNCode.ReadOnly = true;
            this.HSNCode.Width = 150;
            // 
            // EP
            // 
            this.EP.ContainerControl = this;
            // 
            // btnSetTax
            // 
            this.btnSetTax.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSetTax.Location = new System.Drawing.Point(9, 465);
            this.btnSetTax.Name = "btnSetTax";
            this.btnSetTax.Size = new System.Drawing.Size(80, 60);
            this.btnSetTax.TabIndex = 12;
            this.btnSetTax.Text = "Save";
            this.btnSetTax.UseVisualStyleBackColor = true;
            this.btnSetTax.Click += new System.EventHandler(this.btnSetTax_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.Location = new System.Drawing.Point(107, 465);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 60);
            this.btnCancel.TabIndex = 54;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClose.Location = new System.Drawing.Point(203, 465);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(80, 60);
            this.btnClose.TabIndex = 55;
            this.btnClose.Text = "E&xit";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblMsg
            // 
            this.lblMsg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lblMsg.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMsg.CornerRadius = 3;
            this.lblMsg.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg.ForeColor = System.Drawing.Color.Maroon;
            this.lblMsg.GradientBottom = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lblMsg.GradientMiddle = System.Drawing.Color.White;
            this.lblMsg.GradientShow = true;
            this.lblMsg.GradientTop = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lblMsg.Location = new System.Drawing.Point(270, 249);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(481, 52);
            this.lblMsg.TabIndex = 506;
            this.lblMsg.Text = "label4";
            this.lblMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMsg.Visible = false;
            // 
            // lblStar10
            // 
            this.lblStar10.AutoSize = true;
            this.lblStar10.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar10.Location = new System.Drawing.Point(894, 492);
            this.lblStar10.Name = "lblStar10";
            this.lblStar10.Size = new System.Drawing.Size(11, 13);
            this.lblStar10.TabIndex = 190018;
            this.lblStar10.Text = "*";
            this.lblStar10.Visible = false;
            // 
            // lblStar9
            // 
            this.lblStar9.AutoSize = true;
            this.lblStar9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblStar9.Location = new System.Drawing.Point(683, 490);
            this.lblStar9.Name = "lblStar9";
            this.lblStar9.Size = new System.Drawing.Size(11, 13);
            this.lblStar9.TabIndex = 190017;
            this.lblStar9.Text = "*";
            this.lblStar9.Visible = false;
            // 
            // cmbVatPurchase
            // 
            this.cmbVatPurchase.FormattingEnabled = true;
            this.cmbVatPurchase.Location = new System.Drawing.Point(770, 485);
            this.cmbVatPurchase.MaxLength = 25;
            this.cmbVatPurchase.Name = "cmbVatPurchase";
            this.cmbVatPurchase.Size = new System.Drawing.Size(121, 21);
            this.cmbVatPurchase.TabIndex = 19;
            this.cmbVatPurchase.Visible = false;
            this.cmbVatPurchase.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbVatPurchase_KeyDown);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(797, 468);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(52, 13);
            this.label26.TabIndex = 10009;
            this.label26.Text = "Purchase";
            this.label26.Visible = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(207, 25);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(40, 13);
            this.label24.TabIndex = 10007;
            this.label24.Text = "GST %";
            // 
            // cmbVatSales
            // 
            this.cmbVatSales.FormattingEnabled = true;
            this.cmbVatSales.Location = new System.Drawing.Point(261, 21);
            this.cmbVatSales.Name = "cmbVatSales";
            this.cmbVatSales.Size = new System.Drawing.Size(491, 21);
            this.cmbVatSales.TabIndex = 18;
            this.cmbVatSales.SelectedValueChanged += new System.EventHandler(this.cmbVatSales_SelectedValueChanged);
            this.cmbVatSales.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbVatSales_KeyDown);
            // 
            // pnlTAx
            // 
            this.pnlTAx.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlTAx.BackColor = System.Drawing.Color.Transparent;
            this.pnlTAx.BorderColor = System.Drawing.Color.Gray;
            this.pnlTAx.BorderRadius = 3;
            this.pnlTAx.Controls.Add(this.lblChkHelp2);
            this.pnlTAx.Controls.Add(this.BtnAddGST);
            this.pnlTAx.Controls.Add(this.txtHSNCode);
            this.pnlTAx.Controls.Add(this.label1);
            this.pnlTAx.Controls.Add(this.cmbVatSales);
            this.pnlTAx.Controls.Add(this.label24);
            this.pnlTAx.Location = new System.Drawing.Point(9, 405);
            this.pnlTAx.Name = "pnlTAx";
            this.pnlTAx.Size = new System.Drawing.Size(1006, 54);
            this.pnlTAx.TabIndex = 190019;
            this.pnlTAx.Visible = false;
            // 
            // cmbHSNCode
            // 
            this.cmbHSNCode.FormattingEnabled = true;
            this.cmbHSNCode.Location = new System.Drawing.Point(369, 482);
            this.cmbHSNCode.Name = "cmbHSNCode";
            this.cmbHSNCode.Size = new System.Drawing.Size(188, 21);
            this.cmbHSNCode.TabIndex = 190022;
            this.cmbHSNCode.Visible = false;
            this.cmbHSNCode.SelectedValueChanged += new System.EventHandler(this.cmbHSNCode_SelectedValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 190023;
            this.label1.Text = "HSN Code :";
            // 
            // btnPurVat
            // 
            this.btnPurVat.Location = new System.Drawing.Point(908, 483);
            this.btnPurVat.Name = "btnPurVat";
            this.btnPurVat.Size = new System.Drawing.Size(55, 24);
            this.btnPurVat.TabIndex = 190021;
            this.btnPurVat.Text = "Add";
            this.btnPurVat.UseVisualStyleBackColor = true;
            this.btnPurVat.Visible = false;
            this.btnPurVat.Click += new System.EventHandler(this.btnPurVat_Click);
            // 
            // btnSalesVat
            // 
            this.btnSalesVat.Location = new System.Drawing.Point(700, 483);
            this.btnSalesVat.Name = "btnSalesVat";
            this.btnSalesVat.Size = new System.Drawing.Size(55, 24);
            this.btnSalesVat.TabIndex = 190020;
            this.btnSalesVat.Text = "Add";
            this.btnSalesVat.UseVisualStyleBackColor = true;
            this.btnSalesVat.Visible = false;
            this.btnSalesVat.Click += new System.EventHandler(this.btnSalesVat_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(603, 468);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(33, 13);
            this.label25.TabIndex = 10008;
            this.label25.Text = "Sales";
            this.label25.Visible = false;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewCheckBoxColumn1.HeaderText = "";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Width = 50;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Barcode";
            this.dataGridViewTextBoxColumn2.HeaderText = "BarCode";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 180;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "SVat";
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle19;
            this.dataGridViewTextBoxColumn4.HeaderText = "GST %";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "PVat";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridViewTextBoxColumn5.HeaderText = "Purchase %";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "SalesLedgerNo";
            this.dataGridViewTextBoxColumn6.HeaderText = "SalesLedgNo";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "PurLedgerNo";
            this.dataGridViewTextBoxColumn7.HeaderText = "PurLedgNo";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Visible = false;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn8.DataPropertyName = "ItemNo";
            this.dataGridViewTextBoxColumn8.HeaderText = "ItemNo";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Visible = false;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn9.DataPropertyName = "HSNCode";
            this.dataGridViewTextBoxColumn9.HeaderText = "HSN Code";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 150;
            // 
            // txtHSNCode
            // 
            this.txtHSNCode.Location = new System.Drawing.Point(86, 21);
            this.txtHSNCode.Name = "txtHSNCode";
            this.txtHSNCode.Size = new System.Drawing.Size(117, 20);
            this.txtHSNCode.TabIndex = 190023;
            this.txtHSNCode.TextChanged += new System.EventHandler(this.txtHSNCode_TextChanged);
            // 
            // BtnAddGST
            // 
            this.BtnAddGST.Location = new System.Drawing.Point(761, 19);
            this.BtnAddGST.Name = "BtnAddGST";
            this.BtnAddGST.Size = new System.Drawing.Size(55, 24);
            this.BtnAddGST.TabIndex = 190024;
            this.BtnAddGST.Text = "Add";
            this.BtnAddGST.UseVisualStyleBackColor = true;
            this.BtnAddGST.Click += new System.EventHandler(this.BtnAddGST_Click);
            // 
            // lblChkHelp2
            // 
            this.lblChkHelp2.AutoSize = true;
            this.lblChkHelp2.Location = new System.Drawing.Point(263, 5);
            this.lblChkHelp2.Name = "lblChkHelp2";
            this.lblChkHelp2.Size = new System.Drawing.Size(175, 13);
            this.lblChkHelp2.TabIndex = 16000032;
            this.lblChkHelp2.Text = "IGST/CGST/SGST/UTGST/CESS";
            // 
            // ItemChangeTaxAE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1027, 539);
            this.Controls.Add(this.cmbHSNCode);
            this.Controls.Add(this.pnlTAx);
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.btnPurVat);
            this.Controls.Add(this.btnSalesVat);
            this.Controls.Add(this.chkSelectAll);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblStar10);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSetTax);
            this.Controls.Add(this.lblStar9);
            this.Controls.Add(this.dgvTaxItem);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.cmbVatPurchase);
            this.Name = "ItemChangeTaxAE";
            this.Text = "MultiItemsTaxSettings";
            this.Load += new System.EventHandler(this.MultiItemsTaxSettings_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlrb.ResumeLayout(false);
            this.pnlrb.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTaxItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EP)).EndInit();
            this.pnlTAx.ResumeLayout(false);
            this.pnlTAx.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private JitControls.OMBPanel panel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbDepartmentName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbCategoryName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgvTaxItem;
        private System.Windows.Forms.ErrorProvider EP;
        private System.Windows.Forms.CheckBox chkSelectAll;
        private System.Windows.Forms.Button btnSetTax;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.ComboBox cmbGroupNo2;
        private System.Windows.Forms.RadioButton rdBoth;
        private System.Windows.Forms.RadioButton rdNotAssigned;
        private System.Windows.Forms.RadioButton rdAssigned;
        private JitControls.OMLabel lblMsg;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel pnlrb;
        private System.Windows.Forms.Label lblStar10;
        private System.Windows.Forms.Label lblStar9;
        private System.Windows.Forms.ComboBox cmbVatPurchase;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox cmbVatSales;
        private JitControls.OMBPanel pnlTAx;
        private System.Windows.Forms.Button btnSalesVat;
        private System.Windows.Forms.Button btnPurVat;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Chechk;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn BarCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn SVAT;
        private System.Windows.Forms.DataGridViewTextBoxColumn PVAT;
        private System.Windows.Forms.DataGridViewTextBoxColumn SalesLedgNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PurLedgNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn HSNCode;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.ComboBox cmbHSNCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button BtnAddGST;
        private System.Windows.Forms.TextBox txtHSNCode;
        private System.Windows.Forms.Label lblChkHelp2;
    }
}