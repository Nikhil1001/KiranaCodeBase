﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Vouchers
{
    /// <summary>
    /// This class used for Scheme Info
    /// </summary>
    public partial class GetSchemeInfo : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        DataTable dtDeleteReward = new DataTable();
        
        DataGridView dgBill = null;
        //DialogResult DS = DialogResult.OK;
        double GTotal = 0;
        //bool ChkFlag = false;
        internal long InsSchemeNo = 0, InsA = 0, InsN = 0, SchemeType = 0, LedgerNo = 0, MTDSchDetailsNo = 0;
        double Dper = 0, DAmt = 0, schemeAmt = 0;
        long TypeNo, ID;
        internal Form FrmTemp;
        DateTime BillDate;

        DataTable dtInst = new DataTable();
        DataTable dtTsku = new DataTable();
        DataTable dtTskuOther = new DataTable();

        internal DataTable dtTRewardDtls;

        internal DataTable dtTRewardToFrom;

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public GetSchemeInfo()
        {
            InitializeComponent();
        }

        internal void Scheme_Load(object sender, EventArgs e)
        {
            try
            {
                GetSchemeInformation();
                //dgInsTSKUOther.Visible = true;
                //InitTable();
                //InitDelTable();
                //if (TypeNo == 1)
                //    CheckSchemeValidation();
                //else if (TypeNo == 2)
                //    CheckSchemeValidationDisplay();
                //dgInsTSKU.Columns[ColIndexGrid.Qty].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                //dgInsTSKU.Columns[ColIndexGrid.BillQty].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                //if (dtTRewardDtls.Rows.Count != 0)
                //    FillControls();



                //dgInsTSKU.Columns[ColIndexGrid.Select].Visible = ChkFlag;
                //chkSelect.Visible = ChkFlag;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            //DS = DialogResult.Cancel;
            this.Close();
        }

        private void GetSchemeInformation()
        {
            DataTable dtScheme = ObjFunction.GetDataView("Select * From MScheme Where SchemeNo in(24,27)").Table;
            long SchNo = 0,SchDNo;
            string strScheme, strSchDetails = "", strFrom, strTo;
            strScheme = "<style type='text/css'>.AllFont{font-family:verdana;font-size: 7pt;border-spacing:0px;color:blue} </style>";

            strScheme += "<body><Table class='AllFont' cellspacing='0' cellpadding='0' width='100%'  >";
            for (int iSch = 0; iSch < dtScheme.Rows.Count; iSch++)
            {
                strScheme += "<Tr><Th align='left'>Scheme Name :" + dtScheme.Rows[iSch].ItemArray[2].ToString()+
                    "   (Period From:" + Convert.ToDateTime( dtScheme.Rows[iSch].ItemArray[5]).ToString(Format.DDMMMYY) +
                    " To " + Convert.ToDateTime(dtScheme.Rows[iSch].ItemArray[6]).ToString(Format.DDMMMYY) + ")</Th></tr>";
                SchNo = Convert.ToInt64(dtScheme.Rows[iSch].ItemArray[0].ToString());
                DataTable dtSchemeDetails = ObjFunction.GetDataView("Select * From MSchemeDetails Where SchemeNo=" + SchNo + "").Table;
                strSchDetails = "<table cellspacing='0' cellpadding='0' class='AllFont' width='100%' ><Tr>";
                for (int iSchD = 0; iSchD < dtSchemeDetails.Rows.Count; iSchD++)
                {
                    string SchType = "";
                    if (dtScheme.Rows[iSch].ItemArray[12].ToString() == "1") SchType = "Percent";
                    else if (dtScheme.Rows[iSch].ItemArray[12].ToString() == "2") SchType = "Rupees";
                    else if (dtScheme.Rows[iSch].ItemArray[12].ToString() == "3") SchType = "Product";
                    strSchDetails += "<td colspan='2'>Scheme Type:" + SchType+"   (Percentage:" + dtSchemeDetails.Rows[iSchD].ItemArray[2].ToString() + ")</td>";
                    SchDNo = Convert.ToInt64(dtSchemeDetails.Rows[iSchD].ItemArray[0].ToString());
                    DataTable dtSchemeFrom = ObjFunction.GetDataView("SELECT 0 As SrNo, MStockGroup.StockGroupName+' '+MStockItems.ItemName AS 'Item Name', MSchemeFromDetails.Quantity, " +
                        " MSchemeFromDetails.MRP, MSchemeFromDetails.Amount FROM MSchemeFromDetails INNER JOIN " +
                        " MStockItems ON MSchemeFromDetails.ItemNo = MStockItems.ItemNo INNER JOIN "+
                        " MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo Where MSchemeFromDetails.SchemeDetailsNo=" + SchDNo + "").Table;
                    strFrom = "<Table border='1' cellspacing='0' cellpadding='0' class='AllFont' width='100%' style='border:1px solid black;border-collapse: collapse;'><tr style='background:#f4f5f7'>";
                    for (int iSchColFrom = 0; iSchColFrom < dtSchemeFrom.Columns.Count; iSchColFrom++)
                    {
                        strFrom += "<th>" + dtSchemeFrom.Columns[iSchColFrom].ColumnName + "</th>";
                    }
                    strFrom += "</tr>";
                    for (int iSchFrom = 0; iSchFrom < dtSchemeFrom.Rows.Count; iSchFrom++)
                    {
                        strFrom += "<tr>";
                        for (int iSchColFrom = 0; iSchColFrom < dtSchemeFrom.Columns.Count; iSchColFrom++)
                        {
                            strFrom += "<td>" + dtSchemeFrom.Rows[iSchFrom].ItemArray[iSchColFrom].ToString() + "</td>";
                        }
                        strFrom += "</tr>";
                    }
                    strFrom += "</Table>";

                    DataTable dtSchemeTo = ObjFunction.GetDataView("SELECT 0 AS SrNo,MStockGroup.StockGroupName+' '+MStockItems.ItemName AS 'Item Name', MSchemeToDetails.Quantity, " +
                        " MSchemeToDetails.MRP, MSchemeToDetails.Amount,MSchemeToDetails.DiscPercentage FROM MSchemeToDetails INNER JOIN " +
                        " MStockItems ON MSchemeToDetails.ItemNo = MStockItems.ItemNo INNER JOIN " +
                        " MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo Where MSchemeToDetails.SchemeDetailsNo=" + SchDNo + "").Table;
                    if (dtSchemeTo.Rows.Count > 0)
                    {
                        strTo = "<Table border='1' cellspacing='0' cellpadding='0' class='AllFont' width='100%' style='border:1px solid black;border-collapse: collapse;'><tr>";
                        for (int iSchColTo = 0; iSchColTo < dtSchemeTo.Columns.Count; iSchColTo++)
                        {
                            strTo += "<th>" + dtSchemeTo.Columns[iSchColTo].ColumnName + "</th>";
                        }
                        strTo += "</tr>";
                        for (int iSchTo = 0; iSchTo < dtSchemeTo.Rows.Count; iSchTo++)
                        {
                            strTo += "<tr>";
                            for (int iSchColTo = 0; iSchColTo < dtSchemeTo.Columns.Count; iSchColTo++)
                            {
                                strTo += "<td>" + dtSchemeTo.Rows[iSchTo].ItemArray[iSchColTo].ToString() + "</td>";
                            }
                            strTo += "</tr>";
                        }
                        strTo += "</Table>";
                    }
                    else
                    {
                        strTo = "<p width='100%' style='font-width:bold;text-align:center;'>Not Available</p>";
                    }
                    strSchDetails += "</tr><tr><td valign='top' width='50%'>" + strFrom + "</td><td valign='top' width='50%'>" + strTo + "</td>";
                }
                strSchDetails += "</Tr></Table>";
                strScheme += "<tr><td>" + strSchDetails + "<td></Tr>";
                strScheme += "<tr><td>.<td></Tr>";
            }
            strScheme += "</Table></body>";
            WBScheme.DocumentText = strScheme;
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }

}
