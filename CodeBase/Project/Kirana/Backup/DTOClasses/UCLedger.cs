﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;

namespace DTOClasses
{
    public class UCMLedger
    {
        public long ledgerno { get; set; }
        public string ledgername { get; set; }
        public string address { get; set; }
        public string mobileno { get; set; }
        public bool isactive { get; set; }
        public long userid { get; set; }
        public DateTime userdate { get; set; }
    }
}
