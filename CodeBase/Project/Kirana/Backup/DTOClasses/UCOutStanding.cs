﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTOClasses
{
    class UCOutStanding
    {
        public string LedgerName { get; set; }
        public string BillNo { get; set; }
        public DateTime VoucherDate { get; set; }
        public long FkVoucherNo { get; set; }
        public long LedgerNo { get; set; }
        public double Amount { get; set; }
        public double RecAmt { get; set; }
        public double NetBalAmt { get; set; }
    }
}
