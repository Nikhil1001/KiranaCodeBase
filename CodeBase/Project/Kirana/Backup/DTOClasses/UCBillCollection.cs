﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Collections.Generic;

namespace DTOClasses
{
    public class UCBillCollection
    {
        public long pkreftrnno { get; set; }
        public long fkvoucherno { get; set; }
        public long ledgerno { get; set; }
        public long refno { get; set; }
        public long typeofref { get; set; }
        public double amount { get; set; }
        public long discamount { get; set; }
        public double balamount { get; set; }
        [Newtonsoft.Json.JsonConverter(typeof(OM.IsoDateTimeConverterWithMicroSeconds))]
        public DateTime paydate { get; set; }
        public string paytype { get; set; }
        public string cardno { get; set; }
        public string remark { get; set; }
        public string salebillno { get; set; }
        public DateTime voucherdate { get; set; }
        public double billamount { get; set; }

       
    }


}

