﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using JitControls;

namespace OM
{
    class DBMManufacturerCompany
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        CommandCollection commandcollection = new CommandCollection();
        public static string strerrormsg;

        public bool AddMManufacturerCompany(MManufacturerCompany mmanufacturercompany)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMManufacturerCompany";

            cmd.Parameters.AddWithValue("@MfgCompNo", mmanufacturercompany.MfgCompNo);

            cmd.Parameters.AddWithValue("@MfgCompName", mmanufacturercompany.MfgCompName);

            cmd.Parameters.AddWithValue("@MfgCompShortCode", mmanufacturercompany.MfgCompShortCode);

            //cmd.Parameters.AddWithValue("@MfgCompGlobalCode", mmanufacturercompany.MfgCompGlobalCode);

            cmd.Parameters.AddWithValue("@IsActive", mmanufacturercompany.IsActive);

            //cmd.Parameters.AddWithValue("@StatusNo", mmanufacturercompany.StatusNo);

            cmd.Parameters.AddWithValue("@CompanyNo", mmanufacturercompany.CompanyNo);

            cmd.Parameters.AddWithValue("@UserID", mmanufacturercompany.UserID);

            cmd.Parameters.AddWithValue("@UserDate", mmanufacturercompany.UserDate);

            // cmd.Parameters.AddWithValue("@ModifiedBy", mmanufacturercompany.ModifiedBy);
            if (ObjTrans.ExecuteNonQuery(cmd, CommonFunctions.ConStr) == true)
            {
                return true;
            }
            else
            {
                mmanufacturercompany.msg = ObjTrans.ErrorMessage;
                return false;
            }
        }

        public bool AddMManufacturerCompany1(MManufacturerCompany mmanufacturercompany)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMManufacturerCompany1";

            cmd.Parameters.AddWithValue("@MfgCompNo", mmanufacturercompany.MfgCompNo);

            cmd.Parameters.AddWithValue("@MfgCompName", mmanufacturercompany.MfgCompName);

            cmd.Parameters.AddWithValue("@MfgCompShortCode", mmanufacturercompany.MfgCompShortCode);

            cmd.Parameters.AddWithValue("@MfgCompGlobalCode", mmanufacturercompany.MfgCompGlobalCode);

            cmd.Parameters.AddWithValue("@IsActive", mmanufacturercompany.IsActive);

            //cmd.Parameters.AddWithValue("@StatusNo", mmanufacturercompany.StatusNo);

            cmd.Parameters.AddWithValue("@CompanyNo", mmanufacturercompany.CompanyNo);

            cmd.Parameters.AddWithValue("@UserID", mmanufacturercompany.UserID);

            cmd.Parameters.AddWithValue("@UserDate", mmanufacturercompany.UserDate);

            // cmd.Parameters.AddWithValue("@ModifiedBy", mmanufacturercompany.ModifiedBy);
            if (ObjTrans.ExecuteNonQuery(cmd, CommonFunctions.ConStr) == true)
            {
                return true;
            }
            else
            {
                mmanufacturercompany.msg = ObjTrans.ErrorMessage;
                return false;
            }
        }

        public bool DeleteMManufacturerCompany(MManufacturerCompany mmanufacturercompany)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteMManufacturerCompany";

            cmd.Parameters.AddWithValue("@MfgCompNo", mmanufacturercompany.MfgCompNo);
            if (ObjTrans.ExecuteNonQuery(cmd, CommonFunctions.ConStr) == true)
            {
                return true;
            }
            else
            {
                mmanufacturercompany.msg = ObjTrans.ErrorMessage;
                return false;
            }
        }

        public MManufacturerCompany ModifyMManufacturerCompanyByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql;
            SqlCommand cmd;
            sql = "Select * from MManufacturerCompany where MfgCompNo =" + ID;
            cmd = new SqlCommand(sql, Con);
            cmd.Connection = Con;
            Con.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (dr.HasRows)
            {
                MManufacturerCompany MM = new MManufacturerCompany();
                while (dr.Read())
                {
                    MM.MfgCompNo = Convert.ToInt32(dr["MfgCompNo"]);
                    if (!Convert.IsDBNull(dr["MfgCompName"])) MM.MfgCompName = Convert.ToString(dr["MfgCompName"]);
                    if (!Convert.IsDBNull(dr["MfgCompShortCode"])) MM.MfgCompShortCode = Convert.ToString(dr["MfgCompShortCode"]);
                    //if (!Convert.IsDBNull(dr["MfgCompGlobalCode"])) MM.MfgCompGlobalCode = Convert.ToInt64(dr["MfgCompGlobalCode"]);
                    if (!Convert.IsDBNull(dr["IsActive"])) MM.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    //if (!Convert.IsDBNull(dr["StatusNo"])) MM.StatusNo = Convert.ToInt32(dr["StatusNo"]);
                    if (!Convert.IsDBNull(dr["CompanyNo"])) MM.CompanyNo = Convert.ToInt64(dr["CompanyNo"]);
                    if (!Convert.IsDBNull(dr["UserID"])) MM.UserID = Convert.ToInt64(dr["UserID"]);
                    if (!Convert.IsDBNull(dr["UserDate"])) MM.UserDate = Convert.ToDateTime(dr["UserDate"]);
                    if (!Convert.IsDBNull(dr["ModifiedBy"])) MM.ModifiedBy = Convert.ToString(dr["ModifiedBy"]);
                }
                dr.Close();
                return MM;
            }
            else
                dr.Close();
            return new MManufacturerCompany();
        }

        public DataView GetAllMManufacturerCompany()
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from MManufacturerCompany order by MfgCompNo";
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public DataView GetMManufacturerCompanyByID(int ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from MManufacturerCompany where MfgCompNo =" + ID;
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public DataView GetBySearch(string Column, string Value)
        {

            string sql = null;
            switch (Column)
            {
                case "0":
                    sql = "Select MfgCompNo,MfgCompShortCode AS 'Short Name',MfgCompName AS 'Manufacturer Company Name',Case when (IsActive = 'True') Then 'True' Else 'False' End As 'Status' from MManufacturerCompany order by MfgCompName ";
                    break;
                case "MfgCompName":
                    sql = "Select MfgCompNo,MfgCompShortCode AS 'Short Name',MfgCompName AS 'Manufacturer Company Name',Case when (IsActive = 'True') Then 'True' Else 'False' End As 'Status' from MManufacturerCompany where " + Column + " like '" + Value.Trim().Replace("'", "''") + "' + '%' order by MfgCompName";
                    break;
                case "MfgCompShortCode":
                    sql = "Select MfgCompNo,MfgCompShortCode AS 'Short Name',MfgCompName AS 'Manufacturer Company Name',Case when (IsActive = 'True') Then 'True' Else 'False' End As 'Status' from MManufacturerCompany where " + Column + " like '" + Value.Trim().Replace("'", "''") + "' + '%' order by MfgCompName";
                    break;
            }
            DataSet ds = new DataSet();
            try
            {
                ds = ObjDset.FillDset("New", sql, CommonFunctions.ConStr);
            }
            catch (SqlException e)
            {
                CommonFunctions.ErrorMessge = e.Message;
            }
            return ds.Tables[0].DefaultView;
        }


        public bool AddMManufacturerDetails(MManufacturerDetails mmanufacturerdetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMManufacturerDetails";

            cmd.Parameters.AddWithValue("@PkSrNo", mmanufacturerdetails.PkSrNo);

            cmd.Parameters.AddWithValue("@LedgerNo", mmanufacturerdetails.LedgerNo);

            cmd.Parameters.AddWithValue("@ManufacturerNo", mmanufacturerdetails.ManufacturerNo);

            cmd.Parameters.AddWithValue("@CompanyNo", mmanufacturerdetails.CompanyNo);

            cmd.Parameters.AddWithValue("@UserID", mmanufacturerdetails.UserID);

            cmd.Parameters.AddWithValue("@UserDate", mmanufacturerdetails.UserDate);

            commandcollection.Add(cmd);
            return true;
        }

        public MManufacturerDetails ModifyMManufacturerDetailsByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql;
            SqlCommand cmd;
            sql = "Select * from MManufacturerDetails where PkSrNo =" + ID;
            cmd = new SqlCommand(sql, Con);
            cmd.Connection = Con;
            Con.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (dr.HasRows)
            {
                MManufacturerDetails MM = new MManufacturerDetails();
                while (dr.Read())
                {
                    MM.PkSrNo = Convert.ToInt32(dr["PkSrNo"]);
                    if (!Convert.IsDBNull(dr["LedgerNo"])) MM.LedgerNo = Convert.ToInt64(dr["LedgerNo"]);
                    if (!Convert.IsDBNull(dr["ManufacturerNo"])) MM.ManufacturerNo = Convert.ToInt64(dr["ManufacturerNo"]);
                    if (!Convert.IsDBNull(dr["CompanyNo"])) MM.CompanyNo = Convert.ToInt64(dr["CompanyNo"]);
                    if (!Convert.IsDBNull(dr["UserID"])) MM.UserID = Convert.ToInt64(dr["UserID"]);
                    if (!Convert.IsDBNull(dr["UserDate"])) MM.UserDate = Convert.ToDateTime(dr["UserDate"]);
                }
                dr.Close();
                return MM;
            }
            else
                dr.Close();
            return new MManufacturerDetails();
        }

        public DataView GetAllMManufacturerDetails()
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from MManufacturerDetails order by PkSrNo";
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public bool DeleteMManufacturerDetails(MManufacturerDetails mmanufacturerdetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteMManufacturerDetails";

            cmd.Parameters.AddWithValue("@PkSrNo", mmanufacturerdetails.PkSrNo);

            commandcollection.Add(cmd);
            return true;
        }

        public DataView GetBySearchSupplier(string Column, string Value)
        {

            string sql = null;
            switch (Column)
            {
                case "0":
                    sql = "Select LedgerNo,LedgerName from MLedger Where GroupNo=" + GroupType.SundryCreditors + " And LedgerNo Not In(" + Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_PartyAC)) + ") And IsActive='True' order by LedgerName";
                    break;
                case "1":
                    sql = "Select LedgerNo,LedgerName from MLedger Where GroupNo=" + GroupType.SundryCreditors + " And LedgerNo Not In(" + Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_PartyAC)) + ") And IsActive='True' And   LedgerName like '" + Value.Trim().Replace("'", "''") + "' + '%' order by LedgerName";
                    break;
            }
            DataSet ds = new DataSet();
            try
            {
                ds = ObjDset.FillDset("New", sql, CommonFunctions.ConStr);
            }
            catch (SqlException e)
            {
                CommonFunctions.ErrorMessge = e.Message;
            }
            return ds.Tables[0].DefaultView;
        }

        public bool ExecuteNonQueryStatements()
        {

            SqlConnection cn = null;
            cn = new SqlConnection(CommonFunctions.ConStr);
            cn.Open();

            SqlTransaction myTrans;
            myTrans = cn.BeginTransaction();
            //cmd.Transaction = myTrans;
            try
            {
                for (int i = 0; (i < this.commandcollection.Count); i = (i + 1))
                {
                    if ((this.commandcollection[i] != null))
                    {
                        commandcollection[i].Connection = cn;
                        commandcollection[i].Transaction = myTrans;

                        if (commandcollection[i] != null)
                            commandcollection[i].ExecuteNonQuery();
                    }
                }

                myTrans.Commit();
                return true;
            }
            catch (Exception e)
            {
                myTrans.Rollback();

                if (e.GetBaseException().Message == "")
                {
                    strerrormsg = e.Message;
                }
                else
                {
                    strerrormsg = e.GetBaseException().Message;
                }
                return false;
            }
            finally
            {
                cn.Close();
            }
            //________________________________________________________________________________________________________________________________________________________________________________________________________________________
        }
    }

    /// <summary>
    /// This Class use for MManufacturerCompany
    /// </summary>
    public class MManufacturerCompany
    {
        private long mMfgCompNo;
        private string mMfgCompName;
        private string mMfgCompShortCode;
        private long mMfgCompGlobalCode;
        private bool mIsActive;
        private int mStatusNo;
        private long mCompanyNo;
        private long mUserID;
        private DateTime mUserDate;
        private string mModifiedBy;
        private string Mmsg;

        /// <summary>
        /// This Properties use for MfgCompNo
        /// </summary>
        public long MfgCompNo
        {
            get { return mMfgCompNo; }
            set { mMfgCompNo = value; }
        }
        /// <summary>
        /// This Properties use for MfgCompName
        /// </summary>
        public string MfgCompName
        {
            get { return mMfgCompName; }
            set { mMfgCompName = value; }
        }
        /// <summary>
        /// This Properties use for MfgCompShortCode
        /// </summary>
        public string MfgCompShortCode
        {
            get { return mMfgCompShortCode; }
            set { mMfgCompShortCode = value; }
        }
        /// <summary>
        /// This Properties use for MfgCompGlobalCode
        /// </summary>
        public long MfgCompGlobalCode
        {
            get { return mMfgCompGlobalCode; }
            set { mMfgCompGlobalCode = value; }
        }
        /// <summary>
        /// This Properties use for IsActive
        /// </summary>
        public bool IsActive
        {
            get { return mIsActive; }
            set { mIsActive = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public int StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for UserID
        /// </summary>
        public long UserID
        {
            get { return mUserID; }
            set { mUserID = value; }
        }
        /// <summary>
        /// This Properties use for UserDate
        /// </summary>
        public DateTime UserDate
        {
            get { return mUserDate; }
            set { mUserDate = value; }
        }
        /// <summary>
        /// This Properties use for ModifiedBy
        /// </summary>
        public string ModifiedBy
        {
            get { return mModifiedBy; }
            set { mModifiedBy = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }

    /// <summary>
    /// This Class use for MManufacturerDetails
    /// </summary>
    public class MManufacturerDetails
    {
        private long mPkSrNo;
        private long mLedgerNo;
        private long mManufacturerNo;
        private long mCompanyNo;
        private long mStatusNo;
        private long mUserID;
        private DateTime mUserDate;
        private string Mmsg;

        /// <summary>
        /// This Properties use for PkSrNo
        /// </summary>
        public long PkSrNo
        {
            get { return mPkSrNo; }
            set { mPkSrNo = value; }
        }
        /// <summary>
        /// This Properties use for LedgerNo
        /// </summary>
        public long LedgerNo
        {
            get { return mLedgerNo; }
            set { mLedgerNo = value; }
        }
        /// <summary>
        /// This Properties use for ManufacturerNo
        /// </summary>
        public long ManufacturerNo
        {
            get { return mManufacturerNo; }
            set { mManufacturerNo = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public long StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for UserID
        /// </summary>
        public long UserID
        {
            get { return mUserID; }
            set { mUserID = value; }
        }
        /// <summary>
        /// This Properties use for UserDate
        /// </summary>
        public DateTime UserDate
        {
            get { return mUserDate; }
            set { mUserDate = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }
}
