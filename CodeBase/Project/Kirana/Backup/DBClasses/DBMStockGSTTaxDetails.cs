﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JitControls;
using System.Data;
using System.Data.SqlClient;

namespace OM
{
    public class DBMStockGSTTaxDetails
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        public CommandCollection commandcollection = new CommandCollection();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        public static string strerrormsg;

        public DataView GetBySearch(string Column, string Value)
        {
            string sql = null;
            switch (Column)
            {
                case "0":
                    sql = "Select DISTINCT TD.HSNNo, TD.HSNCode as 'HSN', HC.HSNDescription AS 'Description', TD.TotalPercent AS 'GST %', " +
                        " TD.CGSTPercent AS 'CGST %', TD.SGSTPercent as 'SGST %', TD.IGSTPercent as 'IGST %', TD.UTGSTPercent as 'UTGST %', " +
                        " TD.CessPercent as 'Cess %', Case when (TD.IsActive = 'True') Then 'True' Else 'False' End As 'Status' " +
                        " FROM MStockHSNCode As HC " +
                        " INNER JOIN MStockGSTTaxDetails As TD ON TD.HSNNo = HC.HSNNo" +
                        " Order By TD.HSNCode";
                    break;
                case "TD.HSNCode":
                case "HC.HSNDescription":
                    sql = "Select DISTINCT TD.HSNNo, TD.HSNCode as 'HSN', HC.HSNDescription AS 'Description', TD.TotalPercent AS 'GST %', " +
                        " TD.CGSTPercent AS 'CGST %', TD.SGSTPercent as 'SGST %', TD.IGSTPercent as 'IGST %', TD.UTGSTPercent as 'UTGST %', " +
                        " TD.CessPercent as 'Cess %', Case when (TD.IsActive = 'True') Then 'True' Else 'False' End As 'Status' " +
                        " FROM MStockHSNCode As HC " +
                        " INNER JOIN MStockGSTTaxDetails As TD ON TD.HSNNo = HC.HSNNo" +
                        " WHERE " + Column + " like '" + Value.Trim().Replace("'", "''") + "%' " +
                            " OR " + Column + " like '% " + Value.Trim().Replace("'", "''") + "%' " +
                        " Order By TD.HSNCode";
                    break;
            }
            DataSet ds = new DataSet();
            try
            {
                ds = ObjDset.FillDset("New", sql, CommonFunctions.ConStr);
            }
            catch (SqlException e)
            {
                CommonFunctions.ErrorMessge = e.Message;
            }
            return ds.Tables[0].DefaultView;
        }

        public bool AddMStockHSNCode(MStockHSNCode mstockhsncode)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMStockHSNCode";

            cmd.Parameters.AddWithValue("@HSNNo", mstockhsncode.HSNNo);

            cmd.Parameters.AddWithValue("@HSNCode", mstockhsncode.HSNCode);

            cmd.Parameters.AddWithValue("@HSNDescription", mstockhsncode.HSNDescription);

            cmd.Parameters.AddWithValue("@IsActive", mstockhsncode.IsActive);


            SqlParameter outParameter = new SqlParameter();
            outParameter.ParameterName = "@ReturnID";
            outParameter.Direction = ParameterDirection.Output;
            outParameter.DbType = DbType.Int32;
            cmd.Parameters.Add(outParameter);

            commandcollection.Add(cmd);
            return true;
        }

        public bool AddMStockGstTaxDetails(MStockGSTTaxDetails mstockgsttaxdetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMStockGstTaxDetails";

            cmd.Parameters.AddWithValue("@TaxDetailsNo", mstockgsttaxdetails.TaxDetailsNo);

            cmd.Parameters.AddWithValue("@StateCode", mstockgsttaxdetails.StateCode);

            cmd.Parameters.AddWithValue("@HSNCode", mstockgsttaxdetails.HSNCode);

            //  cmd.Parameters.AddWithValue("@HSNNo", mstockgsttaxdetails.HSNNo);

            cmd.Parameters.AddWithValue("@TotalPercent", mstockgsttaxdetails.TotalPercent);

            cmd.Parameters.AddWithValue("@IGSTPercent", mstockgsttaxdetails.IGSTPercent);

            cmd.Parameters.AddWithValue("@CGSTPercent", mstockgsttaxdetails.CGSTPercent);

            cmd.Parameters.AddWithValue("@SGSTPercent", mstockgsttaxdetails.SGSTPercent);

            cmd.Parameters.AddWithValue("@UTGSTPercent", mstockgsttaxdetails.UTGSTPercent);

            cmd.Parameters.AddWithValue("@CessPercent", mstockgsttaxdetails.CessPercent);

            cmd.Parameters.AddWithValue("@FkTaxSettingNo", mstockgsttaxdetails.FkTaxSettingNo);

            cmd.Parameters.AddWithValue("@FromDate", mstockgsttaxdetails.FromDate);

            cmd.Parameters.AddWithValue("@IsActive", mstockgsttaxdetails.IsActive);

            cmd.Parameters.AddWithValue("@UserID", mstockgsttaxdetails.UserID);

            cmd.Parameters.AddWithValue("@UserDate", mstockgsttaxdetails.UserDate);

            commandcollection.Add(cmd);
            return true;
        }

        public bool AddMStockGstTaxDetails1(MStockGSTTaxDetails mstockgsttaxdetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMStockGstTaxDetails";

            cmd.Parameters.AddWithValue("@TaxDetailsNo", mstockgsttaxdetails.TaxDetailsNo);

            cmd.Parameters.AddWithValue("@StateCode", mstockgsttaxdetails.StateCode);

            cmd.Parameters.AddWithValue("@HSNCode", mstockgsttaxdetails.HSNCode);

            cmd.Parameters.AddWithValue("@HSNNo", mstockgsttaxdetails.HSNNo);

            cmd.Parameters.AddWithValue("@TotalPercent", mstockgsttaxdetails.TotalPercent);

            cmd.Parameters.AddWithValue("@IGSTPercent", mstockgsttaxdetails.IGSTPercent);

            cmd.Parameters.AddWithValue("@CGSTPercent", mstockgsttaxdetails.CGSTPercent);

            cmd.Parameters.AddWithValue("@SGSTPercent", mstockgsttaxdetails.SGSTPercent);

            cmd.Parameters.AddWithValue("@UTGSTPercent", mstockgsttaxdetails.UTGSTPercent);

            cmd.Parameters.AddWithValue("@CessPercent", mstockgsttaxdetails.CessPercent);

            cmd.Parameters.AddWithValue("@FkTaxSettingNo", mstockgsttaxdetails.FkTaxSettingNo);

            cmd.Parameters.AddWithValue("@FromDate", mstockgsttaxdetails.FromDate);

            cmd.Parameters.AddWithValue("@IsActive", mstockgsttaxdetails.IsActive);

            cmd.Parameters.AddWithValue("@UserID", mstockgsttaxdetails.UserID);

            cmd.Parameters.AddWithValue("@UserDate", mstockgsttaxdetails.UserDate);

            commandcollection.Add(cmd);

            if (ObjTrans.ExecuteNonQuery(cmd, CommonFunctions.ConStr) == true)
            {
                return true;
            }
            else
            {
                DBMStockGSTTaxDetails.strerrormsg = ObjTrans.ErrorMessage;
                return false;
            }
        }

        public long getHSNNo(string HSNCode, string HSNDescription)
        {
            long HSNNo = ObjQry.ReturnLong("Select HSNNo FROM MStockHSNCode " +
                " Where HSNCode = '" + HSNCode + "' And IsActive = 'True'", CommonFunctions.ConStr);

            if (HSNNo <= 0)
            {
                string sql = "INSERT INTO MStockHSNCode (HSNCode, HSNDescription, IsActive) " +
                    " Values ('" + HSNCode + "','" + HSNDescription.Replace("'", "''") + "', 'True')";
                ObjTrans = new Transaction.Transactions();
                ObjTrans.ExecuteQuery(sql, CommonFunctions.ConStr);

                if (string.IsNullOrEmpty(ObjTrans.ErrorMessage))
                {
                    HSNNo = ObjQry.ReturnLong("Select HSNNo FROM MStockHSNCode " +
                        " Where HSNCode = '" + HSNCode + "' And IsActive = 'True'", CommonFunctions.ConStr);
                    if (HSNNo <= 0)
                    {
                        throw new Exception("Unable to add HSNCode: Unknown Error");
                    }
                }
                else
                {
                    throw new Exception("Unable to add HSNCode: Err : " + ObjTrans.ErrorMessage);
                }
            }

            return HSNNo;
        }

        public bool ExecuteNonQueryStatements()
        {

            SqlConnection cn = null;
            cn = new SqlConnection(CommonFunctions.ConStr);
            cn.Open();
            //long ItemNoTemp = 0, BarcodeNoTemp = 0;
            int cntref = 0;

            SqlTransaction myTrans;
            myTrans = cn.BeginTransaction();
            //cmd.Transaction = myTrans;

            try
            {
                for (int i = 0; (i < this.commandcollection.Count); i = (i + 1))
                {
                    if ((this.commandcollection[i] != null))
                    {
                        commandcollection[i].Connection = cn;
                        commandcollection[i].Transaction = myTrans;
                        if (commandcollection[i].CommandText == "AddMStockGstTaxDetails")
                        {
                            commandcollection[i].Parameters.AddWithValue("@HSNNo", commandcollection[0].Parameters["@ReturnID"].Value);
                            cntref = i;
                        }

                        if (commandcollection[i] != null)
                            commandcollection[i].ExecuteNonQuery();
                    }
                }

                myTrans.Commit();
                return true;
            }
            catch (Exception e)
            {
                myTrans.Rollback();

                if (e.GetBaseException().Message == "")
                {
                    strerrormsg = e.Message;
                }
                else
                {
                    strerrormsg = e.GetBaseException().Message;
                }
                return false;
            }
            finally
            {
                cn.Close();
            }
            //________________________________________________________________________________________________________________________________________________________________________________________________________________________
        }
    }

    public class MStockHSNCode
    {
        private long mHSNNo;
        private string mHSNCode;
        private string mHSNDescription;
        private bool mIsActive;
        private string Mmsg;
        public long HSNNo
        {
            get { return mHSNNo; }
            set { mHSNNo = value; }
        }
        public string HSNCode
        {
            get { return mHSNCode; }
            set { mHSNCode = value; }
        }
        public string HSNDescription
        {
            get { return mHSNDescription; }
            set { mHSNDescription = value; }
        }
        public bool IsActive
        {
            get { return mIsActive; }
            set { mIsActive = value; }
        }
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }

    public class MStockGSTTaxDetails
    {
        private long mTaxDetailsNo;
        private string mStateCode;
        private string mHSNCode;
        private long mHSNNo;
        private double mTotalPercent;
        private double mIGSTPercent;
        private double mCGSTPercent;
        private double mSGSTPercent;
        private double mUTGSTPercent;
        private double mCessPercent;
        private long mFkTaxSettingNo;
        private DateTime mFromDate;
        private bool mIsActive;
        private long mUserID;
        private DateTime mUserDate;
        private string Mmsg;
        public long TaxDetailsNo
        {
            get { return mTaxDetailsNo; }
            set { mTaxDetailsNo = value; }
        }
        public string StateCode
        {
            get { return mStateCode; }
            set { mStateCode = value; }
        }
        public string HSNCode
        {
            get { return mHSNCode; }
            set { mHSNCode = value; }
        }
        public long HSNNo
        {
            get { return mHSNNo; }
            set { mHSNNo = value; }
        }
        public double TotalPercent
        {
            get { return mTotalPercent; }
            set { mTotalPercent = value; }
        }
        public double IGSTPercent
        {
            get { return mIGSTPercent; }
            set { mIGSTPercent = value; }
        }
        public double CGSTPercent
        {
            get { return mCGSTPercent; }
            set { mCGSTPercent = value; }
        }
        public double SGSTPercent
        {
            get { return mSGSTPercent; }
            set { mSGSTPercent = value; }
        }
        public double UTGSTPercent
        {
            get { return mUTGSTPercent; }
            set { mUTGSTPercent = value; }
        }
        public double CessPercent
        {
            get { return mCessPercent; }
            set { mCessPercent = value; }
        }
        public long FkTaxSettingNo
        {
            get { return mFkTaxSettingNo; }
            set { mFkTaxSettingNo = value; }
        }
        public DateTime FromDate
        {
            get { return mFromDate; }
            set { mFromDate = value; }
        }
        public bool IsActive
        {
            get { return mIsActive; }
            set { mIsActive = value; }
        }
        public long UserID
        {
            get { return mUserID; }
            set { mUserID = value; }
        }
        public DateTime UserDate
        {
            get { return mUserDate; }
            set { mUserDate = value; }
        }
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }
}
