﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using OM;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using System.Drawing.Printing;

namespace OM
{
    class DBReportGenerate
    {
        public static string TitleName, ReportName;
        /* 
         * Added new Variables to access the database Setting in MReportPrinterSettings table
         */
        CommonFunctions ObjFunction = new CommonFunctions();
        public string[] ReportSession;
        ReportDocument report = new ReportDocument();
        ConnectionInfo LIF = new ConnectionInfo();
        TableLogOnInfo logoninfo = new TableLogOnInfo();
        public static ParameterField paramField = new ParameterField();
        public static ParameterDiscreteValue paramDiscreteValue = new ParameterDiscreteValue();
        public static ParameterValues paramValues = new ParameterValues();
        public string OwnPrinterName = "";
        public int PrintCount = 1;

        public DBReportGenerate(object rpt, string[] ReportSession)
        {
            report = (ReportDocument)rpt;
            this.ReportSession = ReportSession;
            PrintCount = 1;
        }

        public bool PrintReport()
        {
            try
            {
                TitleName = "";

                int i, cnt = 0;
                if (report.FileName == "")
                {
                    for (i = report.ToString().Length - 1; i >= 0; i--)
                    {
                        if (Convert.ToChar(report.ToString()[i]) == 46) break;
                        cnt++;
                    }
                    ReportName = report.ToString().Substring(report.ToString().Length - cnt, cnt);
                }
                else
                {
                    for (i = report.FileName.Length - 1; i >= 0; i--)
                    {
                        if (Convert.ToChar(report.FileName[i]) == '\\') break;
                        cnt++;
                    }
                    ReportName = report.FileName.Substring(report.FileName.Length - cnt, cnt).Replace(".rpt", "");
                }
                LIF.ServerName = CommonFunctions.ServerName;

                LIF.IntegratedSecurity = true;
                LIF.DatabaseName = DBGetVal.DBName;
                LIF.UserID = "OM96";
                LIF.Password = "OM96";
                LIF.IntegratedSecurity = false;

                foreach (CrystalDecisions.CrystalReports.Engine.Table table in report.Database.Tables)
                {
                    logoninfo = table.LogOnInfo;
                    logoninfo.ConnectionInfo = LIF;
                    table.ApplyLogOnInfo(logoninfo);
                }


                foreach (Section sec in report.ReportDefinition.Sections)
                {
                    ReportObjects myReportObjects = sec.ReportObjects;
                    foreach (ReportObject myReportObject in myReportObjects)
                    {
                        if (myReportObject.Kind == ReportObjectKind.SubreportObject)
                        {
                            SubreportObject mySubreportObject = (SubreportObject)myReportObject;
                            ReportDocument subReportDocument = mySubreportObject.OpenSubreport(mySubreportObject.SubreportName);
                            foreach (Table tab in subReportDocument.Database.Tables)
                            {
                                // Get the TableLogOnInfo object.
                                logoninfo = tab.LogOnInfo;
                                logoninfo.ConnectionInfo = LIF;
                                tab.ApplyLogOnInfo(logoninfo);
                            }
                        }
                    }
                }

                ReportObject reportObject = report.ReportDefinition.ReportObjects["txtRegName"];
                if (reportObject != null)
                {
                    TextObject textObject = (TextObject)reportObject;
                    textObject.Text = DBGetVal.RegCompName;
                }


                //To set parameters for report
                GetParaMeters();
                System.Drawing.Printing.PrinterSettings objPrint = new System.Drawing.Printing.PrinterSettings();
                report.PrintOptions.PrinterName = objPrint.PrinterName;

                if (PrintCount <= 0) PrintCount = 1;

                report.PrintToPrinter(PrintCount, true, 1, 1);
                report.PrintToPrinter(PrintCount, true, 2, 2);
                report.PrintToPrinter(PrintCount, true, 3, 3);
                report.PrintToPrinter(PrintCount, true, 4, 0);
                /*13-Apr-2014
                * Error Solved - To many Instances Open
                */
                report.Close();
                return true;
            }
            catch (Exception e)
            {
                /*13-Apr-2014
                * Error Solved - To many Instances Open
                */
                report.Close();
                /*13-Apr-2014
                * Exception Handling Purpose.
                */
                CommonFunctions.ErrorMessge = e.Message;
                return false;
            }
        }

        public bool PrintReport(string PrinterName)
        {
            try
            {
                TitleName = "";

                int i, cnt = 0;
                if (report.FileName == "")
                {
                    for (i = report.ToString().Length - 1; i >= 0; i--)
                    {
                        if (Convert.ToChar(report.ToString()[i]) == 46) break;
                        cnt++;
                    }
                    ReportName = report.ToString().Substring(report.ToString().Length - cnt, cnt);
                }
                else
                {
                    for (i = report.FileName.Length - 1; i >= 0; i--)
                    {
                        if (Convert.ToChar(report.FileName[i]) == '\\') break;
                        cnt++;
                    }
                    ReportName = report.FileName.Substring(report.FileName.Length - cnt, cnt).Replace(".rpt", "");
                }
                LIF.ServerName = CommonFunctions.ServerName;

                LIF.IntegratedSecurity = true;
                LIF.DatabaseName = DBGetVal.DBName;
                LIF.UserID = "OM96";
                LIF.Password = "OM96";
                LIF.IntegratedSecurity = false;

                foreach (CrystalDecisions.CrystalReports.Engine.Table table in report.Database.Tables)
                {
                    logoninfo = table.LogOnInfo;
                    logoninfo.ConnectionInfo = LIF;
                    table.ApplyLogOnInfo(logoninfo);
                }


                foreach (Section sec in report.ReportDefinition.Sections)
                {
                    ReportObjects myReportObjects = sec.ReportObjects;
                    foreach (ReportObject myReportObject in myReportObjects)
                    {
                        if (myReportObject.Kind == ReportObjectKind.SubreportObject)
                        {
                            SubreportObject mySubreportObject = (SubreportObject)myReportObject;
                            ReportDocument subReportDocument = mySubreportObject.OpenSubreport(mySubreportObject.SubreportName);
                            foreach (Table tab in subReportDocument.Database.Tables)
                            {
                                // Get the TableLogOnInfo object.
                                logoninfo = tab.LogOnInfo;
                                logoninfo.ConnectionInfo = LIF;
                                tab.ApplyLogOnInfo(logoninfo);
                            }
                        }
                    }
                }

                ReportObject reportObject = report.ReportDefinition.ReportObjects["txtRegName"];
                if (reportObject != null)
                {
                    TextObject textObject = (TextObject)reportObject;
                    textObject.Text = DBGetVal.RegCompName;
                }


                //To set parameters for report
                GetParaMeters();
                report.PrintOptions.PrinterName = PrinterName;

                report.PrintToPrinter(1, true, 1, 1);
                report.PrintToPrinter(1, true, 2, 2);
                report.PrintToPrinter(1, true, 3, 3);
                report.PrintToPrinter(1, true, 4, 0);
                report.Close();
                return true;
            }
            catch (Exception e)
            {
                report.Close();
                CommonFunctions.ErrorMessge = e.Message;
                return false;

            }
        }


        public void PrintReportXLS(string StrXlsFilePath)
        {
            try
            {
                TitleName = "";

                int i, cnt = 0;
                if (report.FileName == "")
                {
                    for (i = report.ToString().Length - 1; i >= 0; i--)
                    {
                        if (Convert.ToChar(report.ToString()[i]) == 46) break;
                        cnt++;
                    }
                    ReportName = report.ToString().Substring(report.ToString().Length - cnt, cnt);
                }
                else
                {
                    for (i = report.FileName.Length - 1; i >= 0; i--)
                    {
                        if (Convert.ToChar(report.FileName[i]) == '\\') break;
                        cnt++;
                    }
                    ReportName = report.FileName.Substring(report.FileName.Length - cnt, cnt).Replace(".rpt", "");
                }
                LIF.ServerName = CommonFunctions.ServerName;

                LIF.IntegratedSecurity = true;
                LIF.DatabaseName = DBGetVal.DBName;
                LIF.UserID = "OM96";
                LIF.Password = "OM96";
                LIF.IntegratedSecurity = false;

                foreach (CrystalDecisions.CrystalReports.Engine.Table table in report.Database.Tables)
                {
                    logoninfo = table.LogOnInfo;
                    logoninfo.ConnectionInfo = LIF;
                    table.ApplyLogOnInfo(logoninfo);
                }


                foreach (Section sec in report.ReportDefinition.Sections)
                {
                    ReportObjects myReportObjects = sec.ReportObjects;
                    foreach (ReportObject myReportObject in myReportObjects)
                    {
                        if (myReportObject.Kind == ReportObjectKind.SubreportObject)
                        {
                            SubreportObject mySubreportObject = (SubreportObject)myReportObject;
                            ReportDocument subReportDocument = mySubreportObject.OpenSubreport(mySubreportObject.SubreportName);
                            foreach (Table tab in subReportDocument.Database.Tables)
                            {
                                // Get the TableLogOnInfo object.
                                logoninfo = tab.LogOnInfo;
                                logoninfo.ConnectionInfo = LIF;
                                tab.ApplyLogOnInfo(logoninfo);
                            }
                        }
                    }
                }

                ReportObject reportObject = report.ReportDefinition.ReportObjects["txtRegName"];
                if (reportObject != null)
                {
                    TextObject textObject = (TextObject)reportObject;
                    textObject.Text = DBGetVal.RegCompName;
                }


                //To set parameters for report
                GetParaMeters();

                ExportOptions CrExportOptions;

                DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                ExcelFormatOptions CrFormatTypeOptions = new ExcelFormatOptions();
                CrFormatTypeOptions.ExcelUseConstantColumnWidth = false;
                CrDiskFileDestinationOptions.DiskFileName = StrXlsFilePath;
                CrExportOptions = report.ExportOptions;
                CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                CrExportOptions.ExportFormatType = ExportFormatType.Excel;
                CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                CrExportOptions.FormatOptions = CrFormatTypeOptions;
                report.Export();
                report.Close();
                            }
            catch (Exception e)
            {
                if (e.Message.StartsWith("The process cannot access the file"))
                {
                    throw new Exception("The file is open, please close it and try exporing again.");
                }
                else
                {
                    throw new Exception("Can't Export data to excel.", e);
                }
            }
        }

        private void GetParaMeters()
        {
            int ParaCount = 0;
            paramField = new ParameterField();
            paramDiscreteValue = new ParameterDiscreteValue();
            paramValues = new ParameterValues();

            /*
            * Exculde report names to which Company Name parameter is not required.
            */
            if (ReportName != "RPTVoucherDetailsPrint" && ReportName != "BarCodePrint"
                && ReportName != "BarCodePrintBig" && ReportName != "Label50x50" && ReportName != "Label50x25"
               && ReportName != "Label34x22" && !ReportName.StartsWith("GetTaxDetailsGST"))
            {
                paramField = report.ParameterFields[ParaCount];
                paramDiscreteValue.Value = DBGetVal.CompanyName;
                paramValues.Add(paramDiscreteValue);
                paramField.CurrentValues = paramValues;
                report.DataDefinition.ParameterFields[ParaCount].ApplyCurrentValues(paramValues);
                ParaCount++;
            }

            /*
            * Exclude report names to which Company Name,From date and To date parameters are not required.
            */
            if (ReportName != "GetBill" && ReportName != "GetBillLang"
                && ReportName != "RPTVoucherDetailsPrint" && ReportName != "BarCodePrint"
                && ReportName != "BarCodePrintBig" && ReportName != "Label50x50" && ReportName != "Label50x25"
                && !ReportName.StartsWith("GetPrintGodown") && ReportName != "Label34x22" && ReportName != "GetBigBill" && ReportName != "GetBill2" && ReportName != "GetBillLang2"
              && !ReportName.StartsWith("GetBill") && ReportName.IndexOf("GetReturnBill") == -1 && !ReportName.StartsWith("GetTaxDetailsGST"))
            {
                paramField = new ParameterField();
                paramDiscreteValue = new ParameterDiscreteValue();
                paramValues = new ParameterValues();

                paramField = report.ParameterFields[ParaCount];
                paramDiscreteValue.Value = DBGetVal.FromDate;
                paramValues.Add(paramDiscreteValue);
                paramField.CurrentValues = paramValues;
                report.DataDefinition.ParameterFields[ParaCount].ApplyCurrentValues(paramValues);
                ParaCount++;

                paramField = new ParameterField();
                paramDiscreteValue = new ParameterDiscreteValue();
                paramValues = new ParameterValues();

                paramField = report.ParameterFields[ParaCount];
                paramDiscreteValue.Value = DBGetVal.ToDate;
                paramValues.Add(paramDiscreteValue);
                paramField.CurrentValues = paramValues;
                report.DataDefinition.ParameterFields[ParaCount].ApplyCurrentValues(paramValues);
                ParaCount++;
            }

            for (int i = 0; i < ReportSession.Length; i++)
            {
                paramField = new ParameterField();
                paramDiscreteValue = new ParameterDiscreteValue();
                paramValues = new ParameterValues();

                paramField = report.ParameterFields[ParaCount];
                paramDiscreteValue.Value = ReportSession[i];
                paramValues.Add(paramDiscreteValue);
                paramField.CurrentValues = paramValues;
                report.DataDefinition.ParameterFields[ParaCount].ApplyCurrentValues(paramValues);

                ParaCount += 1;
            }
        }

        private void SetPrinterSize()
        {
            System.Drawing.Printing.PrinterSettings objPrint = new System.Drawing.Printing.PrinterSettings();

            DataTable dtPrinter = ObjFunction.GetDataView("SELECT MPrinter.PrinterName, MReportPrinterSettings.PageSizeName FROM MReportPrinterSettings INNER JOIN MPrinter ON MReportPrinterSettings.PrinterNo = MPrinter.PrinterNo " +
                   " WHERE     (MReportPrinterSettings.ReportClassName = '" + ReportName + "')").Table;
            string PrinterName = "";
            if (dtPrinter.Rows.Count > 0)
            {
                int RawKind = 0;
                PrinterName = dtPrinter.Rows[0].ItemArray[0].ToString();
                string PageSize = dtPrinter.Rows[0].ItemArray[1].ToString();
                objPrint.PrinterName = PrinterName;
                for (int a11 = 0; a11 < objPrint.PaperSizes.Count; a11++)
                {
                    if (objPrint.PaperSizes[a11].PaperName == PageSize)
                    {
                        RawKind = (int)objPrint.PaperSizes[a11].RawKind;
                        report.PrintOptions.PaperSize = (CrystalDecisions.Shared.PaperSize)objPrint.PaperSizes[a11].RawKind;
                        break;
                    }
                    else
                        RawKind = 0;
                }
            }
            if (PrinterName.Length > 0)
                report.PrintOptions.PrinterName = PrinterName;
            else
                report.PrintOptions.PrinterName = objPrint.PrinterName;
        }
    }
}
