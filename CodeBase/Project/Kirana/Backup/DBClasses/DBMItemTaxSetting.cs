﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using JitControls;

namespace OM
{
    class DBMItemTaxSetting
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();

        public bool AddMItemTaxSetting(MItemTaxSetting mitemtaxsetting)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMItemTaxSetting";

            cmd.Parameters.AddWithValue("@PkSrNo", mitemtaxsetting.PkSrNo);

            cmd.Parameters.AddWithValue("@TaxSettingName", mitemtaxsetting.TaxSettingName);

            cmd.Parameters.AddWithValue("@TaxLedgerNo", mitemtaxsetting.TaxLedgerNo);

            cmd.Parameters.AddWithValue("@SalesLedgerNo", mitemtaxsetting.SalesLedgerNo);

            cmd.Parameters.AddWithValue("@CalculationMethod", mitemtaxsetting.CalculationMethod);

            cmd.Parameters.AddWithValue("@Percentage", mitemtaxsetting.Percentage);

            cmd.Parameters.AddWithValue("@IsActive", mitemtaxsetting.IsActive);

            cmd.Parameters.AddWithValue("@UserID", mitemtaxsetting.UserID);

            cmd.Parameters.AddWithValue("@UserDate", mitemtaxsetting.UserDate);

            cmd.Parameters.AddWithValue("@TaxTypeNo", mitemtaxsetting.TaxTypeNo);

            cmd.Parameters.AddWithValue("@TransactionTypeNo", mitemtaxsetting.TransactionTypeNo);

            if (ObjTrans.ExecuteNonQuery(cmd, CommonFunctions.ConStr) == true)
            {
                return true;
            }
            else
            {
                mitemtaxsetting.msg = ObjTrans.ErrorMessage;
                return false;
            }
        }

        public bool DeleteMItemTaxSetting(MItemTaxSetting mitemtaxsetting)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteMItemTaxSetting";

            cmd.Parameters.AddWithValue("@PkSrNo", mitemtaxsetting.PkSrNo);
            if (ObjTrans.ExecuteNonQuery(cmd, CommonFunctions.ConStr) == true)
            {
                return true;
            }
            else
            {
                mitemtaxsetting.msg = ObjTrans.ErrorMessage;
                return false;
            }
        }

        public DataView GetAllMItemTaxSetting()
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from MItemTaxSetting order by PkSrNo";
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public DataView GetMItemTaxSettingByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from MItemTaxSetting where PkSrNo =" + ID;
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public MItemTaxSetting ModifyMItemTaxSettingByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql;
            SqlCommand cmd;
            sql = "Select * from MItemTaxSetting where PkSrNo =" + ID;
            cmd = new SqlCommand(sql, Con);
            cmd.Connection = Con;
            Con.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (dr.HasRows)
            {
                MItemTaxSetting MM = new MItemTaxSetting();
                while (dr.Read())
                {
                    MM.PkSrNo = Convert.ToInt32(dr["PkSrNo"]);
                    if (!Convert.IsDBNull(dr["TaxSettingName"])) MM.TaxSettingName = Convert.ToString(dr["TaxSettingName"]);
                    if (!Convert.IsDBNull(dr["TaxLedgerNo"])) MM.TaxLedgerNo = Convert.ToInt64(dr["TaxLedgerNo"]);
                    if (!Convert.IsDBNull(dr["SalesLedgerNo"])) MM.SalesLedgerNo = Convert.ToInt64(dr["SalesLedgerNo"]);
                    if (!Convert.IsDBNull(dr["CalculationMethod"])) MM.CalculationMethod = Convert.ToString(dr["CalculationMethod"]);
                    if (!Convert.IsDBNull(dr["Percentage"])) MM.Percentage = Convert.ToDouble(dr["Percentage"]);
                    if (!Convert.IsDBNull(dr["IsActive"])) MM.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    if (!Convert.IsDBNull(dr["UserID"])) MM.UserID = Convert.ToInt64(dr["UserID"]);
                    if (!Convert.IsDBNull(dr["TaxTypeNo"])) MM.TaxTypeNo = Convert.ToInt64(dr["TaxTypeNo"]);
                    if (!Convert.IsDBNull(dr["TransactionTypeNo"])) MM.TransactionTypeNo = Convert.ToInt64(dr["TransactionTypeNo"]);
                    if (!Convert.IsDBNull(dr["UserDate"])) MM.UserDate = Convert.ToDateTime(dr["UserDate"]);
                    if (!Convert.IsDBNull(dr["ModifiedBy"])) MM.ModifiedBy = Convert.ToString(dr["ModifiedBy"]);
                }
                dr.Close();
                return MM;
            }
            else
                dr.Close();
            return new MItemTaxSetting();
        }

        public DataView GetBySearch(string Column, string Value)
        {

            string sql = null;
            switch (Column)
            {
                case "0":
                    sql = "Select PkSrNo,TaxSettingName AS 'Tax Setting Name',Case when (IsActive = 'True') Then 'True' Else 'False' End As 'Status' from MItemTaxSetting order by TaxSettingName";
                    break;
                case "TaxSettingName":
                    /* 25-Apr-2014
                    *  Bug fixed for SQL Injection
                    */
                    sql = "Select PkSrNo,TaxSettingName AS 'Tax Setting Name',Case when (IsActive = 'True') Then 'True' Else 'False' End As 'Status' from MItemTaxSetting where " + Column + " like '" + Value.Trim().Replace("'", "''") + "' + '%'order by TaxSettingName";
                    break;
            }
            DataSet ds = new DataSet();
            try
            {
                ds = ObjDset.FillDset("New", sql, CommonFunctions.ConStr);
            }
            catch (SqlException e)
            {
                /*13-Apr-2014
                * Exception Handling Purpose.
                */
                CommonFunctions.ErrorMessge = e.Message;
            }
            return ds.Tables[0].DefaultView;
        }

    }

    public class MItemTaxSetting
    {
        private long mPkSrNo;
        private string mTaxSettingName;
        private long mTaxLedgerNo;
        private long mSalesLedgerNo;
        private string mCalculationMethod;
        private double mPercentage;
        private bool mIsActive;
        private long mUserID;
        private DateTime mUserDate;
        private string mModifiedBy;
        private string Mmsg;
        private long mTaxTypeNo;
        private long mTransactionTypeNo;
        private double mIGSTPercent;
        private double mCGSTPercent;
        private double mSGSTPercent;
        private double mUTGSTPercent;
        private double mCessPercent;


        public long PkSrNo
        {
            get { return mPkSrNo; }
            set { mPkSrNo = value; }
        }
        public string TaxSettingName
        {
            get { return mTaxSettingName; }
            set { mTaxSettingName = value; }
        }
        public long TaxLedgerNo
        {
            get { return mTaxLedgerNo; }
            set { mTaxLedgerNo = value; }
        }
        public long SalesLedgerNo
        {
            get { return mSalesLedgerNo; }
            set { mSalesLedgerNo = value; }
        }
        public string CalculationMethod
        {
            get { return mCalculationMethod; }
            set { mCalculationMethod = value; }
        }
        public double Percentage
        {
            get { return mPercentage; }
            set { mPercentage = value; }
        }
        public bool IsActive
        {
            get { return mIsActive; }
            set { mIsActive = value; }
        }
        public long UserID
        {
            get { return mUserID; }
            set { mUserID = value; }
        }
        public DateTime UserDate
        {
            get { return mUserDate; }
            set { mUserDate = value; }
        }
        public string ModifiedBy
        {
            get { return mModifiedBy; }
            set { mModifiedBy = value; }
        }
        public long TaxTypeNo
        {
            get { return mTaxTypeNo; }
            set { mTaxTypeNo = value; }
        }
        public long TransactionTypeNo
        {
            get { return mTransactionTypeNo; }
            set { mTransactionTypeNo = value; }
        }
        public double IGSTPercent
        {
            get { return mIGSTPercent; }
            set { mIGSTPercent = value; }
        }
        public double CGSTPercent
        {
            get { return mCGSTPercent; }
            set { mCGSTPercent = value; }
        }
        public double SGSTPercent
        {
            get { return mSGSTPercent; }
            set { mSGSTPercent = value; }
        }
        public double UTGSTPercent
        {
            get { return mUTGSTPercent; }
            set { mUTGSTPercent = value; }
        }
        public double CessPercent
        {
            get { return mCessPercent; }
            set { mCessPercent = value; }
        }

        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }
}
