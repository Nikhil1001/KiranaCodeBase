﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using JitControls;

namespace OM
{
    class DBMStockItems
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();
        CommandCollection commandcollection = new CommandCollection();
        public static string strerrormsg;

        public bool AddMStockItems(MStockItems mstockitems)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMStockItems";

            cmd.Parameters.AddWithValue("@ItemNo", mstockitems.ItemNo);

            cmd.Parameters.AddWithValue("@ItemName", mstockitems.ItemName);

            cmd.Parameters.AddWithValue("@ItemShortCode", mstockitems.ItemShortCode);

            cmd.Parameters.AddWithValue("@GroupNo", mstockitems.GroupNo);

            cmd.Parameters.AddWithValue("@UOMPrimary", mstockitems.UOMPrimary);

            cmd.Parameters.AddWithValue("@UOMDefault", mstockitems.UOMDefault);

            cmd.Parameters.AddWithValue("@CompanyNo", mstockitems.CompanyNo);

            cmd.Parameters.AddWithValue("@GroupNo1", mstockitems.GroupNo1);

            cmd.Parameters.AddWithValue("@FKStockDeptNo", mstockitems.FKStockDeptNo);

            cmd.Parameters.AddWithValue("@FkDepartmentNo", mstockitems.FkDepartmentNo);

            cmd.Parameters.AddWithValue("@FkCategoryNo", mstockitems.FkCategoryNo);

            cmd.Parameters.AddWithValue("@FKStockLocationNo", mstockitems.FKStockLocationNo);

            cmd.Parameters.AddWithValue("@IsActive", mstockitems.IsActive);

            cmd.Parameters.AddWithValue("@IsFixedBarcode", mstockitems.IsFixedBarcode);

            //cmd.Parameters.AddWithValue("@ReOrderLevelQty", mstockitems.ReOrderLevelQty);

            cmd.Parameters.AddWithValue("@MinLevel", mstockitems.MinLevel);

            cmd.Parameters.AddWithValue("@MaxLevel", mstockitems.MaxLevel);

            cmd.Parameters.AddWithValue("@LangFullDesc", mstockitems.LangFullDesc);

            cmd.Parameters.AddWithValue("@LangShortDesc", mstockitems.LangShortDesc);

            cmd.Parameters.AddWithValue("@UserId", mstockitems.UserId);

            cmd.Parameters.AddWithValue("@UserDate", mstockitems.UserDate);

            cmd.Parameters.AddWithValue("@ShortCode", mstockitems.ShortCode);

            cmd.Parameters.AddWithValue("@FkStockGroupTypeNo", mstockitems.FkStockGroupTypeNo);

            cmd.Parameters.AddWithValue("@ControlUnder", mstockitems.ControlUnder);

            cmd.Parameters.AddWithValue("@FactorVal", mstockitems.FactorVal);

            cmd.Parameters.AddWithValue("@Margin", mstockitems.Margin);

            cmd.Parameters.AddWithValue("@MKTQty", mstockitems.MKTQty);

            cmd.Parameters.AddWithValue("@DiscountType", mstockitems.DiscountType);

            cmd.Parameters.AddWithValue("@GodownNo", mstockitems.GodownNo);

            cmd.Parameters.AddWithValue("@HamaliInKg", mstockitems.HamaliInKg);

            cmd.Parameters.AddWithValue("@IsQtyRead", mstockitems.IsQtyRead);

            SqlParameter outParameter = new SqlParameter();
            outParameter.ParameterName = "@ReturnID";
            outParameter.Direction = ParameterDirection.Output;
            outParameter.DbType = DbType.Int32;
            cmd.Parameters.Add(outParameter);

            commandcollection.Add(cmd);

            return true;
        }

        public bool AddMStockItems1(MStockItems mstockitems)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMStockItems1";

            cmd.Parameters.AddWithValue("@ItemNo", mstockitems.ItemNo);

            cmd.Parameters.AddWithValue("@ItemName", mstockitems.ItemName);

            cmd.Parameters.AddWithValue("@ItemShortCode", mstockitems.ItemShortCode);

            cmd.Parameters.AddWithValue("@GroupNo", mstockitems.GroupNo);

            cmd.Parameters.AddWithValue("@UOMPrimary", mstockitems.UOMPrimary);

            cmd.Parameters.AddWithValue("@UOMDefault", mstockitems.UOMDefault);

            cmd.Parameters.AddWithValue("@CompanyNo", mstockitems.CompanyNo);

            cmd.Parameters.AddWithValue("@GroupNo1", mstockitems.GroupNo1);

            cmd.Parameters.AddWithValue("@FKStockDeptNo", mstockitems.FKStockDeptNo);

            cmd.Parameters.AddWithValue("@FkDepartmentNo", mstockitems.FkDepartmentNo);

            cmd.Parameters.AddWithValue("@FkCategoryNo", mstockitems.FkCategoryNo);

            cmd.Parameters.AddWithValue("@FKStockLocationNo", mstockitems.FKStockLocationNo);

            cmd.Parameters.AddWithValue("@IsActive", mstockitems.IsActive);

            cmd.Parameters.AddWithValue("@IsFixedBarcode", mstockitems.IsFixedBarcode);

            //cmd.Parameters.AddWithValue("@ReOrderLevelQty", mstockitems.ReOrderLevelQty);

            cmd.Parameters.AddWithValue("@MinLevel", mstockitems.MinLevel);

            cmd.Parameters.AddWithValue("@MaxLevel", mstockitems.MaxLevel);

            cmd.Parameters.AddWithValue("@LangFullDesc", mstockitems.LangFullDesc);

            cmd.Parameters.AddWithValue("@LangShortDesc", mstockitems.LangShortDesc);

            cmd.Parameters.AddWithValue("@UserId", mstockitems.UserId);

            cmd.Parameters.AddWithValue("@UserDate", mstockitems.UserDate);

            cmd.Parameters.AddWithValue("@ShortCode", mstockitems.ShortCode);

            cmd.Parameters.AddWithValue("@GlobalCode", mstockitems.GlobalCode);

            cmd.Parameters.AddWithValue("@FkStockGroupTypeNo", mstockitems.FkStockGroupTypeNo);

            cmd.Parameters.AddWithValue("@ControlUnder", mstockitems.ControlUnder);

            cmd.Parameters.AddWithValue("@FactorVal", mstockitems.FactorVal);

            SqlParameter outParameter = new SqlParameter();
            outParameter.ParameterName = "@ReturnID";
            outParameter.Direction = ParameterDirection.Output;
            outParameter.DbType = DbType.Int32;
            cmd.Parameters.Add(outParameter);

            commandcollection.Add(cmd);

            return true;
        }

        public bool UpdateMStockItems(long ItemNo , double MarginValue)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Update MStockItems set Margin =@margin,StatusNo=2 where ItemNo =@itemNo";

            cmd.Parameters.AddWithValue("@itemNo", ItemNo);
            cmd.Parameters.AddWithValue("@margin", MarginValue);

            commandcollection.Add(cmd);
            return true;
  
        }

        public bool UpdateMCategory(long CategoryNo, double MarginValue)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Update MStockGroup set Margin =@margin,StatusNo=2 where StockGroupNo =@StockGroupNo";

            cmd.Parameters.AddWithValue("@StockGroupNo", CategoryNo);
            cmd.Parameters.AddWithValue("@margin", MarginValue);

            commandcollection.Add(cmd);
            return true;
        }

        public bool UpdateMStockItemsZeroValued(long CategoryNo, double MarginValue)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Update MStockItems set Margin =@margin,StatusNo=2 where GroupNo1 =@CategoryNo and (Margin=0 or margin is null)";

            cmd.Parameters.AddWithValue("@CategoryNo", CategoryNo);
            cmd.Parameters.AddWithValue("@margin", MarginValue);

            commandcollection.Add(cmd);
            return true;
        }

        public bool UpdateMStockItemsAll(long CategoryNo, double MarginValue)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Update MStockItems set Margin =@margin,StatusNo=2 where GroupNo1 =@CategoryNo ";

            cmd.Parameters.AddWithValue("@CategoryNo", CategoryNo);
            cmd.Parameters.AddWithValue("@margin", MarginValue);

            commandcollection.Add(cmd);
            return true;
        }

        public bool DeleteMStockItems(MStockItems mstockitems)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteMStockItems";

            cmd.Parameters.AddWithValue("@ItemNo", mstockitems.ItemNo);
            if (ObjTrans.ExecuteNonQuery(cmd, CommonFunctions.ConStr) == true)
            {
                return true;
            }
            else
            {
                mstockitems.msg = ObjTrans.ErrorMessage;
                return false;
            }
        }

        public DataView GetAllMStockItems()
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from MStockItems order by ItemNo";
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public DataView GetMStockItemsByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from MStockItems where ItemNo =" + ID;
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public MStockItems ModifyMStockItemsByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql;
            SqlCommand cmd;
            sql = "Select * from MStockItems where ItemNo =" + ID;
            cmd = new SqlCommand(sql, Con);
            cmd.Connection = Con;
            Con.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (dr.HasRows)
            {
                MStockItems MM = new MStockItems();
                while (dr.Read())
                {
                    MM.ItemNo = Convert.ToInt32(dr["ItemNo"]);
                    if (!Convert.IsDBNull(dr["ItemName"])) MM.ItemName = Convert.ToString(dr["ItemName"]);
                    if (!Convert.IsDBNull(dr["ItemShortCode"])) MM.ItemShortCode = Convert.ToString(dr["ItemShortCode"]);
                    if (!Convert.IsDBNull(dr["GroupNo"])) MM.GroupNo = Convert.ToInt64(dr["GroupNo"]);
                    if (!Convert.IsDBNull(dr["UOMPrimary"])) MM.UOMPrimary = Convert.ToInt64(dr["UOMPrimary"]);
                    if (!Convert.IsDBNull(dr["UOMDefault"])) MM.UOMDefault = Convert.ToInt64(dr["UOMDefault"]);
                    if (!Convert.IsDBNull(dr["CompanyNo"])) MM.CompanyNo = Convert.ToInt64(dr["CompanyNo"]);
                    if (!Convert.IsDBNull(dr["GroupNo1"])) MM.GroupNo1 = Convert.ToInt64(dr["GroupNo1"]);
                    if (!Convert.IsDBNull(dr["FKStockDeptNo"])) MM.FKStockDeptNo = Convert.ToInt64(dr["FKStockDeptNo"]);
                    if (!Convert.IsDBNull(dr["FkDepartmentNo"])) MM.FkDepartmentNo = Convert.ToInt64(dr["FkDepartmentNo"]);
                    if (!Convert.IsDBNull(dr["FkCategoryNo"])) MM.FkCategoryNo = Convert.ToInt64(dr["FkCategoryNo"]);
                    if (!Convert.IsDBNull(dr["FKStockLocationNo"])) MM.FKStockLocationNo = Convert.ToInt64(dr["FKStockLocationNo"]);
                    if (!Convert.IsDBNull(dr["IsActive"])) MM.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    if (!Convert.IsDBNull(dr["IsFixedBarcode"])) MM.IsFixedBarcode = Convert.ToBoolean(dr["IsFixedBarcode"]);
                    //if (!Convert.IsDBNull(dr["ReOrderLevelQty"])) MM.ReOrderLevelQty = Convert.ToDouble(dr["ReOrderLevelQty"]);
                    if (!Convert.IsDBNull(dr["MinLevel"])) MM.MinLevel = Convert.ToDouble(dr["MinLevel"]);
                    if (!Convert.IsDBNull(dr["MaxLevel"])) MM.MaxLevel = Convert.ToDouble(dr["MaxLevel"]);
                    if (!Convert.IsDBNull(dr["LangFullDesc"])) MM.LangFullDesc = Convert.ToString(dr["LangFullDesc"]);
                    if (!Convert.IsDBNull(dr["LangShortDesc"])) MM.LangShortDesc = Convert.ToString(dr["LangShortDesc"]);
                    if (!Convert.IsDBNull(dr["UserId"])) MM.UserId = Convert.ToInt64(dr["UserId"]);
                    if (!Convert.IsDBNull(dr["UserDate"])) MM.UserDate = Convert.ToDateTime(dr["UserDate"]);
                    if (!Convert.IsDBNull(dr["ShortCode"])) MM.ShortCode = Convert.ToString(dr["ShortCode"]);
                    if (!Convert.IsDBNull(dr["ModifiedBy"])) MM.ModifiedBy = Convert.ToString(dr["ModifiedBy"]);
                    if (!Convert.IsDBNull(dr["FkStockGroupTypeNo"])) MM.FkStockGroupTypeNo = Convert.ToInt64(dr["FkStockGroupTypeNo"]);
                    if (!Convert.IsDBNull(dr["ControlUnder"])) MM.ControlUnder = Convert.ToInt64(dr["ControlUnder"]);
                    if (!Convert.IsDBNull(dr["FactorVal"])) MM.FactorVal = Convert.ToDouble(dr["FactorVal"]);
                    if (!Convert.IsDBNull(dr["Margin"])) MM.Margin = Convert.ToDouble(dr["Margin"]);
                    if (!Convert.IsDBNull(dr["MKTQty"])) MM.MKTQty = Convert.ToDouble(dr["MKTQty"]);
                    if (!Convert.IsDBNull(dr["GodownNo"])) MM.GodownNo = Convert.ToInt64(dr["GodownNo"]);
                    if (!Convert.IsDBNull(dr["DiscountType"])) MM.DiscountType = Convert.ToInt64(dr["DiscountType"]);
                    if (!Convert.IsDBNull(dr["HamaliInKg"])) MM.HamaliInKg = Convert.ToDouble(dr["HamaliInKg"]);
                    if (!Convert.IsDBNull(dr["IsQtyRead"])) MM.IsQtyRead = Convert.ToBoolean(dr["IsQtyRead"]);
                    if (!Convert.IsDBNull(dr["ShowItemName"])) MM.ShowItemName = Convert.ToString(dr["ShowItemName"]);
                }
                dr.Close();
                return MM;
            }
            else
                dr.Close();
            return new MStockItems();
        }

        public DataView GetBySearch(string Column, string Value)
        {

            //" SELECT MStockItems.ItemNo, MStockGroup.StockGroupName + '-' + MStockItems.ItemShortCode  AS 'Short Desc.' , MStockGroup.StockGroupName + '-' + MStockItems.ItemName  AS 'Item Desc.' " +
            //" FROM MStockItems INNER JOIN MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo Where ItemNo<>1 order by ItemName ";

            string sql = null;
            switch (Column)
            {
                case "0":
                    //sql = "Select ItemNo,ItemShortCode AS 'Short Desc.',ItemName AS 'Item Desc.' from MStockItems_V(null,null,null,null,null,null,null) Where ItemNo<>1 order by ItemName";
                    sql = " SELECT MStockItems.ItemNo, MStockGroup.StockGroupName + '-' + MStockItems.ItemShortCode  AS 'Short Desc.' , MStockGroup.StockGroupName + '-' + MStockItems.ItemName  AS 'Item Desc.',Case when (MStockItems.IsActive = 'True') Then 'True' Else 'False' End As 'Status' " +
                         " FROM MStockItems INNER JOIN MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo Where ItemNo<>1 order by MStockGroup.StockGroupName + '-' + MStockItems.ItemName ";
                    break;
                case "ItemName":
                    sql = " SELECT MStockItems.ItemNo, MStockGroup.StockGroupName + '-' + MStockItems.ItemShortCode  AS 'Short Desc.' , MStockGroup.StockGroupName + '-' + MStockItems.ItemName  AS 'Item Desc.',Case when (MStockItems.IsActive = 'True') Then 'True' Else 'False' End As 'Status' " +
                         " FROM MStockItems INNER JOIN MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo where ItemNo<>1 AND MStockGroup.StockGroupName + '-' + MStockItems.ItemName like '%" + Value.Trim().Replace("'","''") + "' + '%' order by MStockGroup.StockGroupName + '-' + MStockItems.ItemName";
                    //sql = "Select ItemNo,ItemShortCode AS 'Short Desc.',ItemName AS 'Item Desc.' from MStockItems_V(null,null,null,null,null,null,null) where ItemNo<>1 AND " + Column + " like '%" + Value.Trim().Replace("'","''") + "' + '%' order by ItemName";
                    break;
                case "ItemShortCode":
                    sql = " SELECT MStockItems.ItemNo, MStockGroup.StockGroupName + '-' + MStockItems.ItemShortCode  AS 'Short Desc.' , MStockGroup.StockGroupName + '-' + MStockItems.ItemName  AS 'Item Desc.',Case when (MStockItems.IsActive = 'True') Then 'True' Else 'False' End As 'Status' " +
                           " FROM MStockItems INNER JOIN MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo where ItemNo<>1 AND MStockGroup.StockGroupName + '-' + MStockItems.ItemShortCode like '%" + Value.Trim().Replace("'","''") + "' + '%' order by MStockGroup.StockGroupName + '-' + MStockItems.ItemName";
                    //sql = "Select ItemNo,ItemShortCode AS 'Short Desc.',ItemName AS 'Item Desc.' from MStockItems_V(null,null,null,null,null,null,null) where ItemNo<>1 AND " + Column + " like '%" + Value.Trim().Replace("'","''") + "' + '%' order by ItemName";
                    break;
            }
            DataSet ds = new DataSet();
            try
            {
                ds = ObjDset.FillDset("New", sql, CommonFunctions.ConStr);
            }
            catch (SqlException e)
            {
                CommonFunctions.ErrorMessge = e.Message;
            }
            return ds.Tables[0].DefaultView;
        }

        public bool AddMStockBarcode(MStockBarcode mstockbarcode)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMStockBarcode";

            cmd.Parameters.AddWithValue("@PkStockBarcodeNo", mstockbarcode.PkStockBarcodeNo);

            //cmd.Parameters.AddWithValue("@ItemNo", mstockbarcode.ItemNo);

            cmd.Parameters.AddWithValue("@Barcode", mstockbarcode.Barcode);

            cmd.Parameters.AddWithValue("@IsActive", mstockbarcode.IsActive);

            cmd.Parameters.AddWithValue("@UserID", mstockbarcode.UserID);

            cmd.Parameters.AddWithValue("@UserDate", mstockbarcode.UserDate);

            cmd.Parameters.AddWithValue("@CompanyNo", DBGetVal.CompanyNo);

            SqlParameter outParameter = new SqlParameter();
            outParameter.ParameterName = "@ReturnID";
            outParameter.Direction = ParameterDirection.Output;
            outParameter.DbType = DbType.Int32;
            cmd.Parameters.Add(outParameter);

            commandcollection.Add(cmd);

            return true;



        }

        public bool AddMStockBarcode1(MStockBarcode mstockbarcode)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMStockBarcode1";

            cmd.Parameters.AddWithValue("@PkStockBarcodeNo", mstockbarcode.PkStockBarcodeNo);

            cmd.Parameters.AddWithValue("@ItemNo", mstockbarcode.ItemNo);

            cmd.Parameters.AddWithValue("@Barcode", mstockbarcode.Barcode);

            cmd.Parameters.AddWithValue("@IsActive", mstockbarcode.IsActive);

            cmd.Parameters.AddWithValue("@UserID", mstockbarcode.UserID);

            cmd.Parameters.AddWithValue("@UserDate", mstockbarcode.UserDate);

            cmd.Parameters.AddWithValue("@CompanyNo", DBGetVal.CompanyNo);

            SqlParameter outParameter = new SqlParameter();
            outParameter.ParameterName = "@ReturnID";
            outParameter.Direction = ParameterDirection.Output;
            outParameter.DbType = DbType.Int32;
            cmd.Parameters.Add(outParameter);

            commandcollection.Add(cmd);

            return true;
            //if (ObjTrans.ExecuteNonQuery(cmd, CommonFunctions.ConStr) == true)
            //{
            //    return true;
            //}
            //else
            //{


            //    mstockbarcode.msg = ObjTrans.ErrorMessage;
            //    return false;
            //}

        }

        public bool DeleteMStockBarcode(MStockBarcode mstockbarcode)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteMStockBarcode";

            cmd.Parameters.AddWithValue("@PkStockBarcodeNo", mstockbarcode.PkStockBarcodeNo);
            if (ObjTrans.ExecuteNonQuery(cmd, CommonFunctions.ConStr) == true)
            {
                return true;
            }
            else
            {
                mstockbarcode.msg = ObjTrans.ErrorMessage;
                return false;
            }
        }

        public DataView GetAllMStockBarcode()
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from MStockBarcode order by PkStockBarcodeNo";
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public DataView GetMStockBarcodeByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from MStockBarcode where PkStockBarcodeNo =" + ID;
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public MStockBarcode ModifyMStockBarcodeByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql;
            SqlCommand cmd;
            sql = "Select * from MStockBarcode where PkStockBarcodeNo =" + ID;
            cmd = new SqlCommand(sql, Con);
            cmd.Connection = Con;
            Con.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (dr.HasRows)
            {
                MStockBarcode MM = new MStockBarcode();
                while (dr.Read())
                {
                    MM.PkStockBarcodeNo = Convert.ToInt32(dr["PkStockBarcodeNo"]);
                    if (!Convert.IsDBNull(dr["ItemNo"])) MM.ItemNo = Convert.ToInt64(dr["ItemNo"]);
                    if (!Convert.IsDBNull(dr["Barcode"])) MM.Barcode = Convert.ToString(dr["Barcode"]);
                    if (!Convert.IsDBNull(dr["IsActive"])) MM.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    if (!Convert.IsDBNull(dr["UserID"])) MM.UserID = Convert.ToInt64(dr["UserID"]);
                    if (!Convert.IsDBNull(dr["UserDate"])) MM.UserDate = Convert.ToDateTime(dr["UserDate"]);
                    if (!Convert.IsDBNull(dr["ModifiedBy"])) MM.ModifiedBy = Convert.ToString(dr["ModifiedBy"]);
                }
                dr.Close();
                return MM;
            }
            else
                dr.Close();
            return new MStockBarcode();
        }

        public bool AddMRateSetting(MRateSetting mratesetting)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMRateSetting";

            cmd.Parameters.AddWithValue("@PkSrNo", mratesetting.PkSrNo);

            //cmd.Parameters.AddWithValue("@FkBcdSrNo", mratesetting.FkBcdSrNo);

            cmd.Parameters.AddWithValue("@ItemNo", mratesetting.ItemNo);

            cmd.Parameters.AddWithValue("@FromDate", mratesetting.FromDate);

            cmd.Parameters.AddWithValue("@PurRate", mratesetting.PurRate);

            cmd.Parameters.AddWithValue("@UOMNo", mratesetting.UOMNo);

            cmd.Parameters.AddWithValue("@ASaleRate", mratesetting.ASaleRate);

            cmd.Parameters.AddWithValue("@BSaleRate", mratesetting.BSaleRate);

            cmd.Parameters.AddWithValue("@CSaleRate", mratesetting.CSaleRate);

            cmd.Parameters.AddWithValue("@DSaleRate", mratesetting.DSaleRate);

            cmd.Parameters.AddWithValue("@ESaleRate", mratesetting.ESaleRate);

            cmd.Parameters.AddWithValue("@StockConversion", mratesetting.StockConversion);

            cmd.Parameters.AddWithValue("@PerOfRateVariation", mratesetting.PerOfRateVariation);

            cmd.Parameters.AddWithValue("@MKTQty", mratesetting.MKTQty);

            cmd.Parameters.AddWithValue("@MRP", mratesetting.MRP);

            cmd.Parameters.AddWithValue("@IsActive", mratesetting.IsActive);

            cmd.Parameters.AddWithValue("@UserID", mratesetting.UserID);

            cmd.Parameters.AddWithValue("@UserDate", mratesetting.UserDate);

            cmd.Parameters.AddWithValue("@CompanyNo", mratesetting.CompanyNo);

            commandcollection.Add(cmd);

            return true;
        }

        public bool DeleteMRateSetting(MRateSetting mratesetting)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteMRateSetting";

            cmd.Parameters.AddWithValue("@PkSrNo", mratesetting.PkSrNo);
            if (ObjTrans.ExecuteNonQuery(cmd, CommonFunctions.ConStr) == true)
            {
                return true;
            }
            else
            {
                mratesetting.msg = ObjTrans.ErrorMessage;
                return false;
            }
        }

        public DataView GetAllMRateSetting()
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from MRateSetting order by PkSrNo";
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public bool AddMRateSetting1(MRateSetting mratesetting)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMRateSetting";

            cmd.Parameters.AddWithValue("@PkSrNo", mratesetting.PkSrNo);

            cmd.Parameters.AddWithValue("@FkBcdSrNo", mratesetting.FkBcdSrNo);

            cmd.Parameters.AddWithValue("@ItemNo", mratesetting.ItemNo);

            cmd.Parameters.AddWithValue("@FromDate", mratesetting.FromDate);

            cmd.Parameters.AddWithValue("@PurRate", mratesetting.PurRate);

            cmd.Parameters.AddWithValue("@MRP", mratesetting.MRP);

            cmd.Parameters.AddWithValue("@UOMNo", mratesetting.UOMNo);

            cmd.Parameters.AddWithValue("@ASaleRate", mratesetting.ASaleRate);

            cmd.Parameters.AddWithValue("@BSaleRate", mratesetting.BSaleRate);

            cmd.Parameters.AddWithValue("@CSaleRate", mratesetting.CSaleRate);

            cmd.Parameters.AddWithValue("@DSaleRate", mratesetting.DSaleRate);

            cmd.Parameters.AddWithValue("@ESaleRate", mratesetting.ESaleRate);

            cmd.Parameters.AddWithValue("@StockConversion", mratesetting.StockConversion);

            cmd.Parameters.AddWithValue("@PerOfRateVariation", mratesetting.PerOfRateVariation);

            cmd.Parameters.AddWithValue("@MKTQty", mratesetting.MKTQty);

            cmd.Parameters.AddWithValue("@IsActive", mratesetting.IsActive);

            cmd.Parameters.AddWithValue("@UserID", mratesetting.UserID);

            cmd.Parameters.AddWithValue("@UserDate", mratesetting.UserDate);

            cmd.Parameters.AddWithValue("@CompanyNo", mratesetting.CompanyNo);
            if (ObjTrans.ExecuteNonQuery(cmd, CommonFunctions.ConStr) == true)
            {
                return true;
            }
            else
            {
                mratesetting.msg = ObjTrans.ErrorMessage;
                return false;
            }
        }

        public bool AddMRateSetting2(MRateSetting mratesetting)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMRateSetting2";

            cmd.Parameters.AddWithValue("@PkSrNo", mratesetting.PkSrNo);

            //cmd.Parameters.AddWithValue("@FkBcdSrNo", mratesetting.FkBcdSrNo);

            // cmd.Parameters.AddWithValue("@ItemNo", mratesetting.ItemNo);

            cmd.Parameters.AddWithValue("@FromDate", mratesetting.FromDate);

            cmd.Parameters.AddWithValue("@PurRate", mratesetting.PurRate);

            cmd.Parameters.AddWithValue("@MRP", mratesetting.MRP);

            cmd.Parameters.AddWithValue("@UOMNo", mratesetting.UOMNo);

            cmd.Parameters.AddWithValue("@ASaleRate", mratesetting.ASaleRate);

            cmd.Parameters.AddWithValue("@BSaleRate", mratesetting.BSaleRate);

            cmd.Parameters.AddWithValue("@CSaleRate", mratesetting.CSaleRate);

            cmd.Parameters.AddWithValue("@DSaleRate", mratesetting.DSaleRate);

            cmd.Parameters.AddWithValue("@ESaleRate", mratesetting.ESaleRate);

            cmd.Parameters.AddWithValue("@StockConversion", mratesetting.StockConversion);

            cmd.Parameters.AddWithValue("@PerOfRateVariation", mratesetting.PerOfRateVariation);

            cmd.Parameters.AddWithValue("@MKTQty", mratesetting.MKTQty);

            cmd.Parameters.AddWithValue("@IsActive", mratesetting.IsActive);

            cmd.Parameters.AddWithValue("@UserID", mratesetting.UserID);

            cmd.Parameters.AddWithValue("@UserDate", mratesetting.UserDate);

            cmd.Parameters.AddWithValue("@CompanyNo", mratesetting.CompanyNo);

            commandcollection.Add(cmd);

            return true;
        }

        public DataView GetMRateSettingByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from MRateSetting where PkSrNo =" + ID;
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public MRateSetting ModifyMRateSettingByID(int ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql;
            SqlCommand cmd;
            sql = "Select * from MRateSetting where PkSrNo =" + ID;
            cmd = new SqlCommand(sql, Con);
            cmd.Connection = Con;
            Con.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (dr.HasRows)
            {
                MRateSetting MM = new MRateSetting();
                while (dr.Read())
                {
                    MM.PkSrNo = Convert.ToInt32(dr["PkSrNo"]);
                    if (!Convert.IsDBNull(dr["FkBcdSrNo"])) MM.FkBcdSrNo = Convert.ToInt64(dr["FkBcdSrNo"]);
                    if (!Convert.IsDBNull(dr["ItemNo"])) MM.ItemNo = Convert.ToInt64(dr["ItemNo"]);
                    if (!Convert.IsDBNull(dr["FromDate"])) MM.FromDate = Convert.ToDateTime(dr["FromDate"]);
                    if (!Convert.IsDBNull(dr["PurRate"])) MM.PurRate = Convert.ToDouble(dr["PurRate"]);
                    if (!Convert.IsDBNull(dr["MRP"])) MM.PurRate = Convert.ToDouble(dr["MRP"]);
                    if (!Convert.IsDBNull(dr["UOMNo"])) MM.UOMNo = Convert.ToInt64(dr["UOMNo"]);
                    if (!Convert.IsDBNull(dr["ASaleRate"])) MM.ASaleRate = Convert.ToDouble(dr["ASaleRate"]);
                    if (!Convert.IsDBNull(dr["BSaleRate"])) MM.BSaleRate = Convert.ToDouble(dr["BSaleRate"]);
                    if (!Convert.IsDBNull(dr["CSaleRate"])) MM.CSaleRate = Convert.ToDouble(dr["CSaleRate"]);
                    if (!Convert.IsDBNull(dr["DSaleRate"])) MM.DSaleRate = Convert.ToDouble(dr["DSaleRate"]);
                    if (!Convert.IsDBNull(dr["ESaleRate"])) MM.ESaleRate = Convert.ToDouble(dr["ESaleRate"]);
                    if (!Convert.IsDBNull(dr["StockConversion"])) MM.StockConversion = Convert.ToDouble(dr["StockConversion"]);
                    if (!Convert.IsDBNull(dr["PerOfRateVariation"])) MM.PerOfRateVariation = Convert.ToDouble(dr["PerOfRateVariation"]);
                    if (!Convert.IsDBNull(dr["MKTQty"])) MM.MKTQty = Convert.ToInt64(dr["MKTQty"]);
                    if (!Convert.IsDBNull(dr["IsActive"])) MM.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    if (!Convert.IsDBNull(dr["UserID"])) MM.UserID = Convert.ToInt64(dr["UserID"]);
                    if (!Convert.IsDBNull(dr["UserDate"])) MM.UserDate = Convert.ToDateTime(dr["UserDate"]);
                    if (!Convert.IsDBNull(dr["ModifiedBy"])) MM.ModifiedBy = Convert.ToString(dr["ModifiedBy"]);
                }
                dr.Close();
                return MM;
            }
            else
                dr.Close();
            return new MRateSetting();
        }

        public bool AddMItemTaxInfo(MItemTaxInfo mitemtaxinfo)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMItemTaxInfo";

            cmd.Parameters.AddWithValue("@PkSrNo", mitemtaxinfo.PkSrNo);

            // cmd.Parameters.AddWithValue("@ItemNo", mitemtaxinfo.ItemNo);

            cmd.Parameters.AddWithValue("@TaxLedgerNo", mitemtaxinfo.TaxLedgerNo);

            cmd.Parameters.AddWithValue("@SalesLedgerNo", mitemtaxinfo.SalesLedgerNo);

            cmd.Parameters.AddWithValue("@FromDate", mitemtaxinfo.FromDate);

            cmd.Parameters.AddWithValue("@CalculationMethod", mitemtaxinfo.CalculationMethod);

            cmd.Parameters.AddWithValue("@Percentage", mitemtaxinfo.Percentage);

            cmd.Parameters.AddWithValue("@FKTaxSettingNo", mitemtaxinfo.FKTaxSettingNo);

            cmd.Parameters.AddWithValue("@UserID", mitemtaxinfo.UserID);

            cmd.Parameters.AddWithValue("@UserDate", mitemtaxinfo.UserDate);

            cmd.Parameters.AddWithValue("@TaxTypeNo", mitemtaxinfo.TaxTypeNo);

            cmd.Parameters.AddWithValue("@TransactionTypeNo", mitemtaxinfo.TransactionTypeNo);

            cmd.Parameters.AddWithValue("@IsActive", mitemtaxinfo.IsActive);

            cmd.Parameters.AddWithValue("@HSNCode", mitemtaxinfo.HSNCode);

            cmd.Parameters.AddWithValue("@HSNNo", mitemtaxinfo.HSNNo);

            cmd.Parameters.AddWithValue("@IGSTPercent", mitemtaxinfo.IGSTPercent);

            cmd.Parameters.AddWithValue("@CGSTPercent", mitemtaxinfo.CGSTPercent);

            cmd.Parameters.AddWithValue("@SGSTPercent", mitemtaxinfo.SGSTPercent);

            cmd.Parameters.AddWithValue("@UTGSTPercent", mitemtaxinfo.UTGSTPercent);

            cmd.Parameters.AddWithValue("@CessPercent", mitemtaxinfo.CessPercent);

            commandcollection.Add(cmd);

            return true;


        }

        public bool ExecuteNonQueryStatements()
        {

            SqlConnection cn = null;
            cn = new SqlConnection(CommonFunctions.ConStr);
            cn.Open();
            //long ItemNoTemp = 0, BarcodeNoTemp = 0;
            int cntref = 0;

            SqlTransaction myTrans;
            myTrans = cn.BeginTransaction();
            //cmd.Transaction = myTrans;

            try
            {
                for (int i = 0; (i < this.commandcollection.Count); i = (i + 1))
                {
                    if ((this.commandcollection[i] != null))
                    {
                        commandcollection[i].Connection = cn;
                        commandcollection[i].Transaction = myTrans;
                        if (commandcollection[i].CommandText == "AddMStockBarcode")
                        {
                            commandcollection[i].Parameters.AddWithValue("@ItemNo", commandcollection[0].Parameters["@ReturnID"].Value);
                            cntref = i;
                        }
                        else if (commandcollection[i].CommandText == "AddMStockBarcode1")
                        {
                            commandcollection[i].CommandText = "AddMStockBarcode";
                        }


                        if (commandcollection[i].CommandText == "AddMRateSetting")
                        {
                            commandcollection[i].Parameters.AddWithValue("@FkBcdSrNo", commandcollection[0].Parameters["@ReturnID"].Value);

                        }
                        if (commandcollection[i].CommandText == "AddMRateSetting2")
                        {
                            commandcollection[i].Parameters.AddWithValue("@ItemNo", commandcollection[0].Parameters["@ReturnID"].Value);

                            commandcollection[i].Parameters.AddWithValue("@FkBcdSrNo", commandcollection[cntref].Parameters["@ReturnID"].Value);
                        }
                        if (commandcollection[i].CommandText == "AddMItemTaxInfo")
                        {
                            commandcollection[i].Parameters.AddWithValue("@ItemNo", commandcollection[0].Parameters["@ReturnID"].Value);

                        }
                        if (commandcollection[i] != null)
                            commandcollection[i].ExecuteNonQuery();
                    }
                }

                myTrans.Commit();
                return true;
            }
            catch (Exception e)
            {
                myTrans.Rollback();

                if (e.GetBaseException().Message == "")
                {
                    strerrormsg = e.Message;
                }
                else
                {
                    strerrormsg = e.GetBaseException().Message;
                }
                return false;
            }
            finally
            {
                cn.Close();
            }
            //________________________________________________________________________________________________________________________________________________________________________________________________________________________
        }

        public bool ExecuteNonQueryStatements_ImportItemMaster()
        {

            SqlConnection cn = null;
            cn = new SqlConnection(CommonFunctions.ConStr);
            cn.Open();
            int cntItem = -1;
            long ItemNo = 0;
            int cntBarcode = -1;
            long BarcodeNo = 0;

            SqlTransaction myTrans;
            myTrans = cn.BeginTransaction();
            try
            {
                for (int i = 0; (i < this.commandcollection.Count); i = (i + 1))
                {
                    if ((this.commandcollection[i] != null))
                    {
                        commandcollection[i].Connection = cn;
                        commandcollection[i].Transaction = myTrans;

                        if (commandcollection[i].CommandText == "AddMStockItems")
                        {
                            cntItem = i;
                        }
                        else if (commandcollection[i].CommandText == "AddMStockBarcode")
                        {
                            commandcollection[i].Parameters.AddWithValue("@ItemNo", ItemNo);
                            cntBarcode = i;
                        }
                        else if (commandcollection[i].CommandText == "AddMRateSetting2")
                        {
                            commandcollection[i].Parameters.AddWithValue("@ItemNo", ItemNo);
                            commandcollection[i].Parameters.AddWithValue("@FkBcdSrNo", BarcodeNo);
                        }
                        else if (commandcollection[i].CommandText == "AddMItemTaxInfo")
                        {
                            commandcollection[i].Parameters.AddWithValue("@ItemNo", ItemNo);
                        }
                        else if (commandcollection[i].CommandText == "AddMItemTaxInfo1")
                        {
                            commandcollection[i].CommandText = "AddMItemTaxInfo";
                        }
                        else if (commandcollection[i].CommandText == "AddMStockItemManufacturer")
                        {
                            commandcollection[i].Parameters.AddWithValue("@ItemNo", ItemNo);
                        }
                        else if (commandcollection[i].CommandText.StartsWith("Update MStockItemsImport SET ItemNo", StringComparison.CurrentCulture))
                        {
                            commandcollection[i].Parameters.AddWithValue("@ItemNo", ItemNo);
                        }

                        if (commandcollection[i] != null)
                            commandcollection[i].ExecuteNonQuery();

                        if (i == cntItem)
                            ItemNo = Convert.ToInt64(commandcollection[cntItem].Parameters["@ReturnID"].Value.ToString());
                        else if (i == cntBarcode)
                            BarcodeNo = Convert.ToInt64(commandcollection[cntBarcode].Parameters["@ReturnID"].Value.ToString());
                    }
                }

                myTrans.Commit();
                return true;
            }
            catch (Exception e)
            {
                myTrans.Rollback();

                strerrormsg = e.ToString();

                return false;
            }
            finally
            {
                cn.Close();
            }
            //________________________________________________________________________________________________________________________________________________________________________________________________________________________
        }


        public bool UpdateStockIsActiveItems(MStockItems mStockItems)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Update MStockItems set MStockItems.IsActive=@IsActive,StatusNo=2 where MStockItems.ItemNo=@ItemNo";

            cmd.Parameters.AddWithValue("@ItemNo", mStockItems.ItemNo);

            cmd.Parameters.AddWithValue("@IsActive", mStockItems.IsActive);

            commandcollection.Add(cmd);
            return true;
        }

        public bool UpdateStockItemNameAndDesc(MStockItems mStockItems)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Update MStockItems set MStockItems.ItemName=@ItemName,MStockItems.LangFullDesc=@LangFullDesc,StatusNo=2 where MStockItems.ItemNo=@ItemNo";

            cmd.Parameters.AddWithValue("@ItemNo", mStockItems.ItemNo);

            cmd.Parameters.AddWithValue("@ItemName", mStockItems.ItemName);

            cmd.Parameters.AddWithValue("@LangFullDesc", mStockItems.LangFullDesc);

            commandcollection.Add(cmd);
            return true;
        }

        public bool UpdateStockItems(MStockItems mStockItems)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Update MStockItems set MStockItems.ItemName=@ItemName,MStockItems.UOMDefault=@UOMDefault,MStockItems.UOMPrimary=@UOMPrimary,MStockItems.IsActive=@IsActive,MStockItems.IsFixedBarcode=@IsFixedBarcode,GroupNo=@GroupNo,GroupNo1=@GroupNo1,MStockItems.FkCategoryNo=@FkCategoryNo,MStockItems.FKStockDeptNo=@FKStockDeptNo,MStockItems.LangFullDesc=@LangFullDesc,StatusNo=2 where MStockItems.ItemNo=@ItemNo";

            cmd.Parameters.AddWithValue("@ItemNo", mStockItems.ItemNo);

            cmd.Parameters.AddWithValue("@ItemName", mStockItems.ItemName);

            cmd.Parameters.AddWithValue("@UOMPrimary", mStockItems.UOMPrimary);

            cmd.Parameters.AddWithValue("@UOMDefault", mStockItems.UOMDefault);

            cmd.Parameters.AddWithValue("@IsActive", mStockItems.IsActive);

            cmd.Parameters.AddWithValue("@IsFixedBarcode", mStockItems.IsFixedBarcode);

            cmd.Parameters.AddWithValue("@GroupNo", mStockItems.GroupNo);

            cmd.Parameters.AddWithValue("@GroupNo1", mStockItems.GroupNo1);

            cmd.Parameters.AddWithValue("@FkCategoryNo", mStockItems.FkCategoryNo);

            cmd.Parameters.AddWithValue("@FKStockDeptNo", mStockItems.FKStockDeptNo);

            cmd.Parameters.AddWithValue("@LangFullDesc", mStockItems.LangFullDesc);

            cmd.Parameters.AddWithValue("@LangShortDesc", mStockItems.LangShortDesc);

            commandcollection.Add(cmd);
            return true;
        }

        public bool UpdateStockBarcode(MStockBarcode mStockBarcode)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Update MStockBarcode set MStockBarcode.Barcode=@Barcode,StatusNo=2 where MStockBarcode.PkStockBarcodeNo=@PkStockBarcodeNo";

            cmd.Parameters.AddWithValue("@PkStockBarcodeNo", mStockBarcode.PkStockBarcodeNo);
            cmd.Parameters.AddWithValue("@Barcode", mStockBarcode.Barcode);

            commandcollection.Add(cmd);
            return true;
        }

        public bool UpdateMRateSetting(MRateSetting mRateSetting)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Update MRateSetting set IsActive=@IsActive,StatusNo=2 where PkSrNo=@PksrNo";

            cmd.Parameters.AddWithValue("@PksrNo", mRateSetting.PkSrNo);

            cmd.Parameters.AddWithValue("IsActive", mRateSetting.IsActive);

            commandcollection.Add(cmd);
            return true;
        }

        public bool UpdateMRateSettingUom(MRateSetting mRateSetting)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Update MRateSetting set UomNo=@UomNo,StatusNo=2 where ItemNo=@ItemNo";

            cmd.Parameters.AddWithValue("@ItemNo", mRateSetting.ItemNo);

            cmd.Parameters.AddWithValue("@UomNo", mRateSetting.UOMNo);

            commandcollection.Add(cmd);
            return true;
        }

        public bool UpdateStockItemsDeptCate(MStockItems mStockItems)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Update MStockItems set MStockItems.IsActive=@IsActive,GroupNo1=@GroupNo1,MStockItems.FKStockDeptNo=@FKStockDeptNo,StatusNo=2 where MStockItems.ItemNo=@ItemNo";

            cmd.Parameters.AddWithValue("@ItemNo", mStockItems.ItemNo);

            cmd.Parameters.AddWithValue("@IsActive", mStockItems.IsActive);

            cmd.Parameters.AddWithValue("@GroupNo1", mStockItems.GroupNo1);

            cmd.Parameters.AddWithValue("@FKStockDeptNo", mStockItems.FKStockDeptNo);

            commandcollection.Add(cmd);
            return true;
        }

        public bool UpdateGroupNo(bool flag,long Groupno)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Update MStockGroup set IsActive=@IsActive,StatusNo=2 Where StockGroupNo=@GroupNo And IsActive='false'";

            cmd.Parameters.AddWithValue("@GroupNo", Groupno);
            cmd.Parameters.AddWithValue("@IsActive", flag);

            commandcollection.Add(cmd);
            return true;

        }

        public bool UpdateGlobalStockItems(long ItemGlobalNo)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Update MGlobalStockItems set IsActive='true' Where ItemNo=@ItemNo";
            cmd.Parameters.AddWithValue("@ItemNo", ItemGlobalNo);

            commandcollection.Add(cmd);
            return true;

        }


        public DataView GetMStockItems_V(long TypeNo, long ItemNo, long PGroupNo, long PGroupNo1, long PFkDepartmentNo, long PFkCategoryNo, long PCompanyNo)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            DataView dv = new DataView();
            string str = "", TItemNo, TGroupNo, TGroupNo1, TFkDepartmentNo, TFkCategoryNo, TCompanyNo;
            if (ItemNo == 0) TItemNo = "MStockItems.ItemNo"; else TItemNo = ItemNo.ToString();
            if (PGroupNo == 0) TGroupNo = "MStockItems.GroupNo"; else TGroupNo = PGroupNo.ToString();
            if (PGroupNo1 == 0) TGroupNo1 = "MStockItems.GroupNo1"; else TGroupNo1 = PGroupNo1.ToString();
            if (PFkDepartmentNo == 0) TFkDepartmentNo = "MStockItems.FkDepartmentNo"; else TFkDepartmentNo = PFkDepartmentNo.ToString();
            if (PFkCategoryNo == 0) TFkCategoryNo = "MStockItems.FkCategoryNo"; else TFkCategoryNo = PFkCategoryNo.ToString();
            if (PCompanyNo == 0) TCompanyNo = "MStockItems.CompanyNo"; else TCompanyNo = PCompanyNo.ToString();
            if (TypeNo == 0)
            {
                TypeNo = ObjQry.ReturnLong(" select Cast(SettingValue as numeric(10)) from MSettings Where PKSettingNo=29", CommonFunctions.ConStr);
            }
            if (TypeNo == 1)
            {
                str = "  Select  ItemNo,ItemName ,ItemShortCode , GroupNo,UOMPrimary,UOMDefault,CompanyNo,GroupNo1," +
                     " FkDepartmentNo,FkCategoryNo,IsActive ,IsFixedBarcode ,MinLevel,MaxLevel,LangFullDesc,LangShortDesc,UserId,UserDate,ModifiedBy " +
                     " From MStockItems " +
                     " WHERE MStockItems.ItemNo=" + TItemNo + " " +
                     " And MStockItems.GroupNo=" + TGroupNo + "  " +
                     " And MStockItems.GroupNo1= " + TGroupNo1 + "  " +
                     " And MStockItems.FkCategoryNo=" + TFkCategoryNo + "  " +
                      " And MStockItems.CompanyNo= " + TCompanyNo + "  " +
                      "And MStockItems.FkDepartmentNo= " + TFkDepartmentNo + "  ";
            }
            else if (TypeNo == 2)
            {
                str = " SELECT MStockItems.ItemNo, MStockGroup.StockGroupName + ' ' + MStockItems.ItemName AS ItemName, MStockItems.ItemShortCode, MStockItems.GroupNo, " +
                        " MStockItems.UOMPrimary, MStockItems.UOMDefault, MStockItems.CompanyNo, MStockItems.GroupNo1, MStockItems.FkDepartmentNo, " +
                        " MStockItems.FkCategoryNo, MStockItems.IsActive, MStockItems.IsFixedBarcode, MStockItems.MinLevel, MStockItems.MaxLevel,MStockItems.LangFullDesc,MStockItems.LangShortDesc, MStockItems.UserId, " +
                        " MStockItems.UserDate, MStockItems.ModifiedBy" +
                         " FROM MStockItems " +
                         " INNER JOIN MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo" +
                         " WHERE MStockItems.ItemNo=" + TItemNo + " " +
                         " And MStockItems.GroupNo=" + TGroupNo + "  " +
                         " And MStockItems.GroupNo1= " + TGroupNo1 + "  " +
                         " And MStockItems.FkCategoryNo=" + TFkCategoryNo + "  " +
                         " And MStockItems.CompanyNo= " + TCompanyNo + "  ";
            }
            else if (TypeNo == 3)
            {
                str = " SELECT MStockItems.ItemNo, MStockGroup.StockGroupName + ' ' + MStockItems.ItemName AS ItemName, MStockItems.ItemShortCode, MStockItems.GroupNo, " +
                       " MStockItems.UOMPrimary, MStockItems.UOMDefault, MStockItems.CompanyNo, MStockItems.GroupNo1, MStockItems.FkDepartmentNo, " +
                       " MStockItems.FkCategoryNo, MStockItems.IsActive, MStockItems.IsFixedBarcode, MStockItems.MinLevel, MStockItems.MaxLevel,MStockItems.LangFullDesc,MStockItems.LangShortDesc, MStockItems.UserId, " +
                        " MStockItems.UserDate, MStockItems.ModifiedBy" +
                        " FROM MStockItems " +
                        " INNER JOIN MStockGroup ON MStockItems.GroupNo1 = MStockGroup.StockGroupNo" +
                        " WHERE MStockItems.ItemNo=" + TItemNo + " " +
                        " And MStockItems.GroupNo=" + TGroupNo + "  " +
                        " And MStockItems.GroupNo1=" + TGroupNo1 + "  " +
                        " And MStockItems.FkCategoryNo=" + TFkCategoryNo + "  " +
                        " And MStockItems.CompanyNo=" + TCompanyNo + "  ";
            }
            else if (TypeNo == 4)
            {
                str = " SELECT MStockItems.ItemNo, MStockGroup.StockGroupName + ' ' + MStockGroup2.StockGroupName + ' ' + MStockItems.ItemName AS ItemName, MStockItems.ItemShortCode, MStockItems.GroupNo, " +
                       " MStockItems.UOMPrimary, MStockItems.UOMDefault, MStockItems.CompanyNo, MStockItems.GroupNo1, MStockItems.FkDepartmentNo, " +
                       " MStockItems.FkCategoryNo, MStockItems.IsActive, MStockItems.IsFixedBarcode, MStockItems.MinLevel, MStockItems.MaxLevel,MStockItems.LangFullDesc,MStockItems.LangShortDesc, MStockItems.UserId, " +
                       " MStockItems.UserDate, MStockItems.ModifiedBy" +
                        " FROM MStockItems " +
                        " INNER JOIN MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo" +
                        " INNER JOIN MStockGroup AS MStockGroup2 ON MStockItems.GroupNo1 = MStockGroup2.StockGroupNo" +
                        " WHERE MStockItems.ItemNo=" + TItemNo + " " +
                        " And MStockItems.GroupNo=" + TGroupNo + "  " +
                        " And MStockItems.GroupNo1=" + TGroupNo1 + "  " +
                        " And MStockItems.FkCategoryNo=" + TFkCategoryNo + "  " +
                        " And MStockItems.CompanyNo=" + TCompanyNo + "  ";
            }
            else if (TypeNo == 5)
            {
                str = "SELECT MStockItems.ItemNo, MStockGroup2.StockGroupName + ' ' + MStockGroup.StockGroupName + ' ' + MStockItems.ItemName  AS ItemName, MStockItems.ItemShortCode, MStockItems.GroupNo,  " +
                        " MStockItems.UOMPrimary, MStockItems.UOMDefault, MStockItems.CompanyNo, MStockItems.GroupNo1, MStockItems.FkDepartmentNo, " +
                        " MStockItems.FkCategoryNo, MStockItems.IsActive, MStockItems.IsFixedBarcode, MStockItems.MinLevel, MStockItems.MaxLevel,MStockItems.LangFullDesc,MStockItems.LangShortDesc, MStockItems.UserId, " +
                        " MStockItems.UserDate, MStockItems.ModifiedBy" +
                        " FROM MStockItems " +
                        " INNER JOIN MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo" +
                        " INNER JOIN MStockGroup AS MStockGroup2 ON MStockItems.GroupNo1 = MStockGroup2.StockGroupNo" +
                        " WHERE MStockItems.ItemNo=" + TItemNo + " " +
                        " And MStockItems.GroupNo=" + TGroupNo + "  " +
                        " And MStockItems.GroupNo1=" + TGroupNo1 + "  " +
                        " And MStockItems.FkCategoryNo=" + TFkCategoryNo + "  " +
                        " And MStockItems.CompanyNo=" + TCompanyNo + "  ";
            }
            else if (TypeNo == 6)
            {
                str = " SELECT     MStockItems.ItemNo, MStockGroup.StockGroupName + ' ' + MStockItems.ItemName + ' ' + MStockGroup2.StockGroupName AS ItemName, " +
                      " MStockItems.ItemShortCode, MStockItems.GroupNo, MStockItems.UOMPrimary, MStockItems.UOMDefault, MStockItems.CompanyNo, " +
                      " MStockItems.GroupNo1, MStockItems.FkDepartmentNo, MStockItems.FkCategoryNo, MStockItems.IsActive, MStockItems.IsFixedBarcode, " +
                      " MStockItems.MinLevel, MStockItems.MaxLevel,MStockItems.LangFullDesc,MStockItems.LangShortDesc, MStockItems.UserId, MStockItems.UserDate, MStockItems.ModifiedBy" +
                      " FROM         MStockItems INNER JOIN" +
                      " MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo INNER JOIN" +
                      " MStockGroup AS MStockGroup2 ON MStockItems.GroupNo1 = MStockGroup2.StockGroupNo" +
                      " WHERE MStockItems.ItemNo=" + TItemNo + " " +
                      " And MStockItems.GroupNo=" + TGroupNo + "  " +
                      " And MStockItems.GroupNo1=" + TGroupNo1 + "  " +
                      " And MStockItems.FkCategoryNo=" + TFkCategoryNo + "  " +
                      " And MStockItems.CompanyNo=" + TCompanyNo + "  ";
            }
            dv = ObjFunction.GetDataView(str);
            return dv;


        }

    }

    /// <summary>
    /// This Class use for MStockItems
    /// </summary>
    public class MStockItems
    {
        private long mItemNo;
        private string mItemName;
        private string mItemShortCode;
        private long mGroupNo;
        private long mUOMPrimary;
        private long mUOMDefault;
        private long mCompanyNo;
        private long mGroupNo1;
        private long mFKStockDeptNo;
        private long mFkDepartmentNo;
        private long mFkCategoryNo;
        private long mFKStockLocationNo;
        private bool mIsActive;
        private bool mIsFixedBarcode;
        private double mMinLevel;
        private double mMaxLevel;
        private double mReOrderLevelQty;
        private string mLangFullDesc;
        private string mLangShortDesc;
        private long mUserId;
        private DateTime mUserDate;
        private string mModifiedBy;
        private int mStatusNo;
        private string mShortCode;
        private long mGlobalCode;
        private long mFkStockGroupTypeNo;
        private long mControlUnder;
        private double mFactorVal;
        private double mMargin;
        private double mMKTQty;
        private long mGodownNo;
        private long mDiscountType;
        private double mHamaliInKg;
        private bool mIsQtyRead;
        private string mShowItemName;
        private string Mmsg;

        /// <summary>
        /// This Properties use for ItemNo
        /// </summary>
        public long ItemNo
        {
            get { return mItemNo; }
            set { mItemNo = value; }
        }
        /// <summary>
        /// This Properties use for ItemName
        /// </summary>
        public string ItemName
        {
            get { return mItemName; }
            set { mItemName = value; }
        }
        /// <summary>
        /// This Properties use for ItemShortCode
        /// </summary>
        public string ItemShortCode
        {
            get { return mItemShortCode; }
            set { mItemShortCode = value; }
        }
        /// <summary>
        /// This Properties use for GroupNo
        /// </summary>
        public long GroupNo
        {
            get { return mGroupNo; }
            set { mGroupNo = value; }
        }
        /// <summary>
        /// This Properties use for UOMPrimary
        /// </summary>
        public long UOMPrimary
        {
            get { return mUOMPrimary; }
            set { mUOMPrimary = value; }
        }
        /// <summary>
        /// This Properties use for UOMDefault
        /// </summary>
        public long UOMDefault
        {
            get { return mUOMDefault; }
            set { mUOMDefault = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for GroupNo1
        /// </summary>
        public long GroupNo1
        {
            get { return mGroupNo1; }
            set { mGroupNo1 = value; }
        }
        /// <summary>
        /// This Properties use for FKStockDeptNo
        /// </summary>
        public long FKStockDeptNo
        {
            get { return mFKStockDeptNo; }
            set { mFKStockDeptNo = value; }
        }
        /// <summary>
        /// This Properties use for FkDepartmentNo
        /// </summary>
        public long FkDepartmentNo
        {
            get { return mFkDepartmentNo; }
            set { mFkDepartmentNo = value; }
        }
        /// <summary>
        /// This Properties use for FkCategoryNo
        /// </summary>
        public long FkCategoryNo
        {
            get { return mFkCategoryNo; }
            set { mFkCategoryNo = value; }
        }
        /// <summary>
        /// This Properties use for FKStockLocationNo
        /// </summary>
        public long FKStockLocationNo
        {
            get { return mFKStockLocationNo; }
            set { mFKStockLocationNo = value; }
        }
        /// <summary>
        /// This Properties use for IsActive
        /// </summary>
        public bool IsActive
        {
            get { return mIsActive; }
            set { mIsActive = value; }
        }
        /// <summary>
        /// This Properties use for IsFixedBarcode
        /// </summary>
        public bool IsFixedBarcode
        {
            get { return mIsFixedBarcode; }
            set { mIsFixedBarcode = value; }
        }
        /// <summary>
        /// This Properties use for MinLevel
        /// </summary>
        public double MinLevel
        {
            get { return mMinLevel; }
            set { mMinLevel = value; }
        }
        /// <summary>
        /// This Properties use for MaxLevel
        /// </summary>
        public double MaxLevel
        {
            get { return mMaxLevel; }
            set { mMaxLevel = value; }
        }
        /// <summary>
        /// This Properties use for ReOrderLevelQty
        /// </summary>
        public double ReOrderLevelQty
        {
            get { return mReOrderLevelQty; }
            set { mReOrderLevelQty = value; }
        }
        /// <summary>
        /// This Properties use for LangFullDesc
        /// </summary>
        public string LangFullDesc
        {
            get { return mLangFullDesc; }
            set { mLangFullDesc = value; }
        }
        /// <summary>
        /// This Properties use for LangShortDesc
        /// </summary>
        public string LangShortDesc
        {
            get { return mLangShortDesc; }
            set { mLangShortDesc = value; }
        }
        /// <summary>
        /// This Properties use for UserId
        /// </summary>
        public long UserId
        {
            get { return mUserId; }
            set { mUserId = value; }
        }
        /// <summary>
        /// This Properties use for UserDate
        /// </summary>
        public DateTime UserDate
        {
            get { return mUserDate; }
            set { mUserDate = value; }
        }
        /// <summary>
        /// This Properties use for ModifiedBy
        /// </summary>
        public string ModifiedBy
        {
            get { return mModifiedBy; }
            set { mModifiedBy = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public int StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for ShortCode
        /// </summary>
        public string ShortCode
        {
            get { return mShortCode; }
            set { mShortCode = value; }
        }
        /// <summary>
        /// This Properties use for GlobalCode
        /// </summary>
        public long GlobalCode
        {
            get { return mGlobalCode; }
            set { mGlobalCode = value; }
        }
        /// <summary>
        /// This Properties use for FkStockGroupTypeNo
        /// </summary>
        public long FkStockGroupTypeNo
        {
            get { return mFkStockGroupTypeNo; }
            set { mFkStockGroupTypeNo = value; }
        }
        /// <summary>
        /// This Properties use for ControlUnder
        /// </summary>
        public long ControlUnder
        {
            get { return mControlUnder; }
            set { mControlUnder = value; }
        }
        /// <summary>
        /// This Properties use for FactorVal
        /// </summary>
        public double FactorVal
        {
            get { return mFactorVal; }
            set { mFactorVal = value; }
        }
        /// <summary>
        /// This Properties use for Margin
        /// </summary>
        public double Margin
        {
            get { return mMargin; }
            set { mMargin = value; }
        }

        public double MKTQty
        {
            get { return mMKTQty; }
            set { mMKTQty = value; }
        }

        public long GodownNo
        {
            get { return mGodownNo; }
            set { mGodownNo = value; }
        }

        public long DiscountType	
        {
            get { return mDiscountType; }
            set { mDiscountType = value; }
        }
        public double HamaliInKg
        {
            get { return mHamaliInKg; }
            set { mHamaliInKg = value; }
        }
        public bool IsQtyRead
        {
            get { return mIsQtyRead; }
            set { mIsQtyRead = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        /// 
        public string ShowItemName
        {
            get { return mShowItemName; }
            set { mShowItemName = value; }
        }


        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }


    /// <summary>
    /// This Class use for MStockBarcode
    /// </summary>
    public class MStockBarcode
    {
        private long mPkStockBarcodeNo;
        private long mItemNo;
        private string mBarcode;
        private bool mIsActive;
        private long mUserID;
        private DateTime mUserDate;
        private string mModifiedBy;
        private int mStatusNo;
        private long mCompanyNo;
        private string Mmsg;

        /// <summary>
        /// This Properties use for PkStockBarcodeNo
        /// </summary>
        public long PkStockBarcodeNo
        {
            get { return mPkStockBarcodeNo; }
            set { mPkStockBarcodeNo = value; }
        }
        /// <summary>
        /// This Properties use for ItemNo
        /// </summary>
        public long ItemNo
        {
            get { return mItemNo; }
            set { mItemNo = value; }
        }
        /// <summary>
        /// This Properties use for Barcode
        /// </summary>
        public string Barcode
        {
            get { return mBarcode; }
            set { mBarcode = value; }
        }
        /// <summary>
        /// This Properties use for IsActive
        /// </summary>
        public bool IsActive
        {
            get { return mIsActive; }
            set { mIsActive = value; }
        }
        /// <summary>
        /// This Properties use for UserID
        /// </summary>
        public long UserID
        {
            get { return mUserID; }
            set { mUserID = value; }
        }
        /// <summary>
        /// This Properties use for UserDate
        /// </summary>
        public DateTime UserDate
        {
            get { return mUserDate; }
            set { mUserDate = value; }
        }
        /// <summary>
        /// This Properties use for ModifiedBy
        /// </summary>
        public string ModifiedBy
        {
            get { return mModifiedBy; }
            set { mModifiedBy = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public int StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }

        
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }

    /// <summary>
    /// This Class use for MRateSetting
    /// </summary>
    public class MRateSetting
    {
        private long mPkSrNo;
        private long mFkBcdSrNo;
        private long mItemNo;
        private DateTime mFromDate;
        private double mPurRate;
        private double mMRP;
        private long mUOMNo;
        private double mASaleRate;
        private double mBSaleRate;
        private double mCSaleRate;
        private double mDSaleRate;
        private double mESaleRate;
        private double mStockConversion;
        private double mPerOfRateVariation;
        private long mMKTQty;
        private bool mIsActive;
        private long mUserID;
        private DateTime mUserDate;
        private string mModifiedBy;
        private long mCompanyNo;
        private int mStatusNo;
        private string Mmsg;

        /// <summary>
        /// This Properties use for PkSrNo
        /// </summary>
        public long PkSrNo
        {
            get { return mPkSrNo; }
            set { mPkSrNo = value; }
        }
        /// <summary>
        /// This Properties use for FkBcdSrNo
        /// </summary>
        public long FkBcdSrNo
        {
            get { return mFkBcdSrNo; }
            set { mFkBcdSrNo = value; }
        }
        /// <summary>
        /// This Properties use for ItemNo
        /// </summary>
        public long ItemNo
        {
            get { return mItemNo; }
            set { mItemNo = value; }
        }
        /// <summary>
        /// This Properties use for FromDate
        /// </summary>
        public DateTime FromDate
        {
            get { return mFromDate; }
            set { mFromDate = value; }
        }
        /// <summary>
        /// This Properties use for PurRate
        /// </summary>
        public double PurRate
        {
            get { return mPurRate; }
            set { mPurRate = value; }
        }
        /// <summary>
        /// This Properties use for MRP
        /// </summary>
        public double MRP
        {
            get { return mMRP; }
            set { mMRP = value; }
        }
        /// <summary>
        /// This Properties use for UOMNo
        /// </summary>
        public long UOMNo
        {
            get { return mUOMNo; }
            set { mUOMNo = value; }
        }
        /// <summary>
        /// This Properties use for ASaleRate
        /// </summary>
        public double ASaleRate
        {
            get { return mASaleRate; }
            set { mASaleRate = value; }
        }
        /// <summary>
        /// This Properties use for BSaleRate
        /// </summary>
        public double BSaleRate
        {
            get { return mBSaleRate; }
            set { mBSaleRate = value; }
        }
        /// <summary>
        /// This Properties use for CSaleRate
        /// </summary>
        public double CSaleRate
        {
            get { return mCSaleRate; }
            set { mCSaleRate = value; }
        }
        /// <summary>
        /// This Properties use for DSaleRate
        /// </summary>
        public double DSaleRate
        {
            get { return mDSaleRate; }
            set { mDSaleRate = value; }
        }
        /// <summary>
        /// This Properties use for ESaleRate
        /// </summary>
        public double ESaleRate
        {
            get { return mESaleRate; }
            set { mESaleRate = value; }
        }
        /// <summary>
        /// This Properties use for StockConversion
        /// </summary>
        public double StockConversion
        {
            get { return mStockConversion; }
            set { mStockConversion = value; }
        }
        /// <summary>
        /// This Properties use for PerOfRateVariation
        /// </summary>
        public double PerOfRateVariation
        {
            get { return mPerOfRateVariation; }
            set { mPerOfRateVariation = value; }
        }
        /// <summary>
        /// This Properties use for MKTQty
        /// </summary>
        public long MKTQty
        {
            get { return mMKTQty; }
            set { mMKTQty = value; }
        }
        /// <summary>
        /// This Properties use for IsActive
        /// </summary>
        public bool IsActive
        {
            get { return mIsActive; }
            set { mIsActive = value; }
        }
        /// <summary>
        /// This Properties use for UserID
        /// </summary>
        public long UserID
        {
            get { return mUserID; }
            set { mUserID = value; }
        }
        /// <summary>
        /// This Properties use for UserDate
        /// </summary>
        public DateTime UserDate
        {
            get { return mUserDate; }
            set { mUserDate = value; }
        }
        /// <summary>
        /// This Properties use for ModifiedBy
        /// </summary>
        public string ModifiedBy
        {
            get { return mModifiedBy; }
            set { mModifiedBy = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public int StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }
}
