﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Xml.Linq;
using System.Data;
using JitControls;

namespace OM
{
    class DBMLedger
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        CommandCollection commandcollection = new CommandCollection();
        public static string strerrormsg;

        public bool AddMLedger(MLedger mledger)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMLedger";

            cmd.Parameters.AddWithValue("@LedgerNo", mledger.LedgerNo);

            cmd.Parameters.AddWithValue("@LedgerUserNo", mledger.LedgerUserNo);

            cmd.Parameters.AddWithValue("@LedgerName", mledger.LedgerName);

            cmd.Parameters.AddWithValue("@GroupNo", mledger.GroupNo);

            cmd.Parameters.AddWithValue("@OpeningBalance", mledger.OpeningBalance);

            cmd.Parameters.AddWithValue("@SignCode", mledger.SignCode);

            cmd.Parameters.AddWithValue("@InvFlag", mledger.InvFlag);

            cmd.Parameters.AddWithValue("@MaintainBillByBill", mledger.MaintainBillByBill);

            cmd.Parameters.AddWithValue("@IsActive", mledger.IsActive);

            cmd.Parameters.AddWithValue("@ContactPerson", mledger.ContactPerson);

            cmd.Parameters.AddWithValue("@CompanyNo", mledger.CompanyNo);

            cmd.Parameters.AddWithValue("@LedgerStatus", mledger.LedgerStatus);

            cmd.Parameters.AddWithValue("@IsEnroll", mledger.IsEnroll);

            cmd.Parameters.AddWithValue("@IsSendSMS", mledger.IsSendSMS);

            cmd.Parameters.AddWithValue("@UserID", mledger.UserID);

            cmd.Parameters.AddWithValue("@UserDate", mledger.UserDate);

            cmd.Parameters.AddWithValue("@LangLedgerName", mledger.LangLedgerName);

            SqlParameter outParameter = new SqlParameter();
            outParameter.ParameterName = "@ReturnID";
            outParameter.Direction = ParameterDirection.Output;
            outParameter.DbType = DbType.Int32;
            cmd.Parameters.Add(outParameter);

            commandcollection.Add(cmd);
            return true;
        }

        public bool DeleteMLedger(MLedger mledger)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteMLedger";

            cmd.Parameters.AddWithValue("@LedgerNo", mledger.LedgerNo);
            if (ObjTrans.ExecuteNonQuery(cmd, CommonFunctions.ConStr) == true)
            {
                return true;
            }
            else
            {
                mledger.msg = ObjTrans.ErrorMessage;
                return false;
            }
        }

        public DataView GetAllMLedger(int GroupNo)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from MLedger where GroupNo=" + GroupNo + " order by LedgerNo";
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public DataView GetMLedgerByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from MLedger where LedgerNo =" + ID;
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public MLedger ModifyMLedgerByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql;
            SqlCommand cmd;
            sql = "Select * from MLedger where LedgerNo =" + ID;
            cmd = new SqlCommand(sql, Con);
            cmd.Connection = Con;
            Con.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (dr.HasRows)
            {
                MLedger MM = new MLedger();
                while (dr.Read())
                {
                    MM.LedgerNo = Convert.ToInt32(dr["LedgerNo"]);
                    if (!Convert.IsDBNull(dr["LedgerUserNo"])) MM.LedgerUserNo = Convert.ToString(dr["LedgerUserNo"]);
                    if (!Convert.IsDBNull(dr["LedgerName"])) MM.LedgerName = Convert.ToString(dr["LedgerName"]);
                    if (!Convert.IsDBNull(dr["GroupNo"])) MM.GroupNo = Convert.ToInt64(dr["GroupNo"]);
                    if (!Convert.IsDBNull(dr["OpeningBalance"])) MM.OpeningBalance = Convert.ToDouble(dr["OpeningBalance"]);
                    if (!Convert.IsDBNull(dr["SignCode"])) MM.SignCode = Convert.ToInt64(dr["SignCode"]);
                    if (!Convert.IsDBNull(dr["InvFlag"])) MM.InvFlag = Convert.ToBoolean(dr["InvFlag"]);
                    if (!Convert.IsDBNull(dr["MaintainBillByBill"])) MM.MaintainBillByBill = Convert.ToBoolean(dr["MaintainBillByBill"]);
                    if (!Convert.IsDBNull(dr["IsActive"])) MM.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    if (!Convert.IsDBNull(dr["ContactPerson"])) MM.ContactPerson = Convert.ToString(dr["ContactPerson"]);
                    if (!Convert.IsDBNull(dr["CompanyNo"])) MM.CompanyNo = Convert.ToInt64(dr["CompanyNo"]);
                    if (!Convert.IsDBNull(dr["IsEnroll"])) MM.IsEnroll = Convert.ToBoolean(dr["IsEnroll"]);
                    if (!Convert.IsDBNull(dr["IsSendSMS"])) MM.IsSendSMS = Convert.ToBoolean(dr["IsSendSMS"]);
                    if (!Convert.IsDBNull(dr["UserID"])) MM.UserID = Convert.ToInt64(dr["UserID"]);
                    if (!Convert.IsDBNull(dr["UserDate"])) MM.UserDate = Convert.ToDateTime(dr["UserDate"]);
                    if (!Convert.IsDBNull(dr["LangLedgerName"])) MM.LangLedgerName = Convert.ToString(dr["LangLedgerName"]);
                    if (!Convert.IsDBNull(dr["ModifiedBy"])) MM.ModifiedBy = Convert.ToString(dr["ModifiedBy"]);
                }
                dr.Close();
                return MM;
            }
            else
                dr.Close();
            return new MLedger();
        }

        public DataView GetBySearch(string Column, string Value, long GroupNo)
        {
            Value = Value.Replace("'", "''");
            string sql = null;
            switch (Column)
            {
                case "0":
                    sql = "Select LedgerNo,LedgerName AS 'Name',ContactPerson AS " + (GroupNo == GroupType.BankAccounts ? "'Contact Number'" : "'Contact Person'") + ",Case when (IsActive = 'True') Then 'True' Else 'False' End As 'Status' from MLedger where LedgerNo >15 AND GroupNo=" + GroupNo + " order by LedgerName";
                    break;
                case "LedgerName":
                    sql = "Select LedgerNo,LedgerName AS 'Name',ContactPerson AS " + (GroupNo == GroupType.BankAccounts ? "'Contact Number'" : "'Contact Person'") + ",Case when (IsActive = 'True') Then 'True' Else 'False' End As 'Status' from MLedger where LedgerNo >15 AND  GroupNo=" + GroupNo + " and " + Column + " like '%" + Value.Trim() + "%' order by LedgerName";
                    break;
                case "ContactPerson":
                    sql = "Select LedgerNo,LedgerName AS 'Name',ContactPerson AS " + (GroupNo == GroupType.BankAccounts ? "'Contact Number'" : "'Contact Person'") + ",Case when (IsActive = 'True') Then 'True' Else 'False' End As 'Status' from MLedger where LedgerNo >15 AND  GroupNo=" + GroupNo + " and " + Column + " like '%" + Value.Trim() + "%' order by LedgerName";
                    break;

            }
            DataSet ds = new DataSet();
            try
            {
                ds = ObjDset.FillDset("New", sql, CommonFunctions.ConStr);
            }
            catch (SqlException e)
            {
                CommonFunctions.ErrorMessge = e.Message;
            }
            return ds.Tables[0].DefaultView;
        }

        public DataView GetBySearchAcc(string Column, string Value)
        {

            string sql = null;
            switch (Column)
            {
                case "0":
                    sql = "Select LedgerNo,LedgerName AS 'Ledger Name',Case when (IsActive = 'True') Then 'True' Else 'False' End As 'Status' from MLedger where LedgerNo >15 AND GroupNo not in(" + GroupType.SundryCreditors + "," + GroupType.SundryDebtors + "," + GroupType.DutiesAndTaxes + "," + GroupType.BankAccounts + "," + GroupType.Primary + ") order by LedgerName";
                    break;
                case "LedgerName":
                    sql = "Select LedgerNo,LedgerName AS 'Ledger Name',Case when (IsActive = 'True') Then 'True' Else 'False' End As 'Status' from MLedger where LedgerNo >15 AND GroupNo not in(" + GroupType.SundryCreditors + "," + GroupType.SundryDebtors + "," + GroupType.DutiesAndTaxes + "," + GroupType.BankAccounts + "," + GroupType.Primary + ") and " + Column + " like '" + Value.Trim().Replace("'", "''") + "' + '%' order by LedgerName";
                    break;

            }
            DataSet ds = new DataSet();
            try
            {
                ds = ObjDset.FillDset("New", sql, CommonFunctions.ConStr);
            }
            catch (SqlException e)
            {
                CommonFunctions.ErrorMessge = e.Message;
            }
            return ds.Tables[0].DefaultView;
        }

        public DataView GetBySearchTaxAcc(string Column, string Value)
        {
            string sql = null;
            switch (Column)
            {
                case "0":
                    sql = "Select LedgerNo,LedgerName AS 'Ledger Name',MGroup.GroupName As 'ControlGroup',Case when (MLedger.IsActive = 'True') Then 'True' Else 'False' End As 'Status' from MLedger " +
                          "INNER JOIN  MGroup ON MLedger.GroupNo = MGroup.GroupNo " +
                          "where (MLedger.LedgerNo >15) AND (MLedger.GroupNo in(" + GroupType.DutiesAndTaxes + ")) or MLedger.GroupNo in(Select G.GroupNo From MGroup G Where G.ControlGroup in(" + GroupType.DutiesAndTaxes + ")) order by LedgerName";
                    break;
                case "LedgerName":
                    sql = "Select LedgerNo,LedgerName AS 'Ledger Name', MGroup.GroupName As 'ControlGroup',Case when (MLedger.IsActive = 'True') Then 'True' Else 'False' End As 'Status' from MLedger " +
                          "INNER JOIN  MGroup ON MLedger.GroupNo = MGroup.GroupNo " +
                          "where (MLedger.LedgerNo >15) AND (MLedger.GroupNo in(" + GroupType.DutiesAndTaxes + ") or MLedger.GroupNo in(Select G.GroupNo From MGroup G Where G.ControlGroup in(" + GroupType.DutiesAndTaxes + "))) and " + Column + " like '" + Value.Trim().Replace("'", "''") + "' + '%'  order by LedgerName";
                    break;

            }
            DataSet ds = new DataSet();
            try
            {
                ds = ObjDset.FillDset("New", sql, CommonFunctions.ConStr);
            }
            catch (SqlException e)
            {
                CommonFunctions.ErrorMessge = e.Message;
            }
            return ds.Tables[0].DefaultView;
        }

        public bool AddMLedgerDetails(MLedgerDetails mledgerdetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMLedgerDetails";

            cmd.Parameters.AddWithValue("@LedgerDetailsNo", mledgerdetails.LedgerDetailsNo);

            //cmd.Parameters.AddWithValue("@LedgerNo", mledgerdetails.LedgerNo);

            cmd.Parameters.AddWithValue("@Address", mledgerdetails.Address);

            cmd.Parameters.AddWithValue("@StateNo", mledgerdetails.StateNo);

            cmd.Parameters.AddWithValue("@CityNo", mledgerdetails.CityNo);

            cmd.Parameters.AddWithValue("@PinCode", mledgerdetails.PinCode);

            cmd.Parameters.AddWithValue("@PhNo1", mledgerdetails.PhNo1);

            cmd.Parameters.AddWithValue("@PhNo2", mledgerdetails.PhNo2);

            cmd.Parameters.AddWithValue("@MobileNo1", mledgerdetails.MobileNo1);

            cmd.Parameters.AddWithValue("@MobileNo2", mledgerdetails.MobileNo2);

            cmd.Parameters.AddWithValue("@EmailID", mledgerdetails.EmailID);

            cmd.Parameters.AddWithValue("@DOB", mledgerdetails.DOB);

            cmd.Parameters.AddWithValue("@QualificationNo", mledgerdetails.QualificationNo);

            cmd.Parameters.AddWithValue("@OccupationNo", mledgerdetails.OccupationNo);

            cmd.Parameters.AddWithValue("@CustomerType", mledgerdetails.CustomerType);

            cmd.Parameters.AddWithValue("@CreditLimit", mledgerdetails.CreditLimit);

            cmd.Parameters.AddWithValue("@PANNo", mledgerdetails.PANNo);

            cmd.Parameters.AddWithValue("@VATNo", mledgerdetails.VATNo);

            cmd.Parameters.AddWithValue("@CSTNo", mledgerdetails.CSTNo);

            cmd.Parameters.AddWithValue("@UserID", mledgerdetails.UserID);

            cmd.Parameters.AddWithValue("@UserDate", mledgerdetails.UserDate);

            cmd.Parameters.AddWithValue("@Gender", mledgerdetails.Gender);

            cmd.Parameters.AddWithValue("@CompanyNo", DBGetVal.CompanyNo);

            cmd.Parameters.AddWithValue("@LangAddress", mledgerdetails.LangAddress);

            cmd.Parameters.AddWithValue("@AreaNo", mledgerdetails.AreaNo);

            commandcollection.Add(cmd);
            return true;
        }

        public bool DeleteMLedgerDetails(MLedgerDetails mledgerdetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteMLedgerDetails";

            cmd.Parameters.AddWithValue("@LedgerDetailsNo", mledgerdetails.LedgerDetailsNo);
            if (ObjTrans.ExecuteNonQuery(cmd, CommonFunctions.ConStr) == true)
            {
                return true;
            }
            else
            {
                mledgerdetails.msg = ObjTrans.ErrorMessage;
                return false;
            }
        }

        public DataView GetAllMAddress()
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from MLedgerDetails order by LedgerDetailsNo";
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public DataView GetMAddressByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from MLedgerDetails where LedgerDetailsNo =" + ID;
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public MLedgerDetails ModifyMLedgerDetailsByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql;
            SqlCommand cmd;
            sql = "Select * from MLedgerDetails where LedgerNo =" + ID;
            cmd = new SqlCommand(sql, Con);
            cmd.Connection = Con;
            Con.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (dr.HasRows)
            {
                MLedgerDetails MM = new MLedgerDetails();
                while (dr.Read())
                {
                    MM.LedgerDetailsNo = Convert.ToInt32(dr["LedgerDetailsNo"]);
                    if (!Convert.IsDBNull(dr["LedgerNo"])) MM.LedgerNo = Convert.ToInt64(dr["LedgerNo"]);
                    if (!Convert.IsDBNull(dr["Address"])) MM.Address = Convert.ToString(dr["Address"]);
                    if (!Convert.IsDBNull(dr["StateNo"])) MM.StateNo = Convert.ToInt64(dr["StateNo"]);
                    if (!Convert.IsDBNull(dr["CityNo"])) MM.CityNo = Convert.ToInt64(dr["CityNo"]);
                    if (!Convert.IsDBNull(dr["PinCode"])) MM.PinCode = Convert.ToString(dr["PinCode"]);
                    if (!Convert.IsDBNull(dr["PhNo1"])) MM.PhNo1 = Convert.ToString(dr["PhNo1"]);
                    if (!Convert.IsDBNull(dr["PhNo2"])) MM.PhNo2 = Convert.ToString(dr["PhNo2"]);
                    if (!Convert.IsDBNull(dr["MobileNo1"])) MM.MobileNo1 = Convert.ToString(dr["MobileNo1"]);
                    if (!Convert.IsDBNull(dr["MobileNo2"])) MM.MobileNo2 = Convert.ToString(dr["MobileNo2"]);
                    if (!Convert.IsDBNull(dr["EmailID"])) MM.EmailID = Convert.ToString(dr["EmailID"]);
                    if (!Convert.IsDBNull(dr["DOB"])) MM.DOB = Convert.ToDateTime(dr["DOB"]);
                    if (!Convert.IsDBNull(dr["QualificationNo"])) MM.QualificationNo = Convert.ToInt64(dr["QualificationNo"]);
                    if (!Convert.IsDBNull(dr["OccupationNo"])) MM.OccupationNo = Convert.ToInt64(dr["OccupationNo"]);
                    if (!Convert.IsDBNull(dr["CustomerType"])) MM.CustomerType = Convert.ToInt64(dr["CustomerType"]);
                    if (!Convert.IsDBNull(dr["CreditLimit"])) MM.CreditLimit = Convert.ToDouble(dr["CreditLimit"]);
                    if (!Convert.IsDBNull(dr["PANNo"])) MM.PANNo = Convert.ToString(dr["PANNo"]);
                    if (!Convert.IsDBNull(dr["VATNo"])) MM.VATNo = Convert.ToString(dr["VATNo"]);
                    if (!Convert.IsDBNull(dr["CSTNo"])) MM.CSTNo = Convert.ToString(dr["CSTNo"]);
                    if (!Convert.IsDBNull(dr["UserID"])) MM.UserID = Convert.ToInt64(dr["UserID"]);
                    if (!Convert.IsDBNull(dr["UserDate"])) MM.UserDate = Convert.ToDateTime(dr["UserDate"]);
                    if (!Convert.IsDBNull(dr["Gender"])) MM.Gender = Convert.ToInt64(dr["Gender"]);
                    if (!Convert.IsDBNull(dr["ModifiedBy"])) MM.ModifiedBy = Convert.ToString(dr["ModifiedBy"]);
                    if (!Convert.IsDBNull(dr["ConsentDate"])) MM.ConsentDate = Convert.ToDateTime(dr["ConsentDate"]);
                    if (!Convert.IsDBNull(dr["LangAddress"])) MM.LangAddress = Convert.ToString(dr["LangAddress"]);
                    if (!Convert.IsDBNull(dr["AreaNo"])) MM.AreaNo = Convert.ToInt64(dr["AreaNo"]);
                }
                dr.Close();
                return MM;
            }
            else
                dr.Close();
            return new MLedgerDetails();
        }

        public bool UpdateLedgerBalance(MLedger mledger)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Update MLedger set OpeningBalance=@OpeningBalance,SignCode=@SignCode,StatusNo=2 where LedgerNo=@LedgerNo";

            cmd.Parameters.AddWithValue("@LedgerNo", mledger.LedgerNo);

            cmd.Parameters.AddWithValue("@OpeningBalance", mledger.OpeningBalance);

            cmd.Parameters.AddWithValue("@SignCode", mledger.SignCode);

            commandcollection.Add(cmd);
            return true;
        }

        public bool ExecuteNonQueryStatements()
        {

            SqlConnection cn = null;
            cn = new SqlConnection(CommonFunctions.ConStr);
            cn.Open();

            SqlTransaction myTrans;
            myTrans = cn.BeginTransaction();
            //cmd.Transaction = myTrans;
            int TaxLedgNo = 0, SaleLedgNo = 0;

            try
            {
                for (int i = 0; (i < this.commandcollection.Count); i = (i + 1))
                {
                    if ((this.commandcollection[i] != null))
                    {
                        commandcollection[i].Connection = cn;
                        commandcollection[i].Transaction = myTrans;
                        if (commandcollection[i].CommandText == "AddMLedger")
                        {
                            if (i == 0)
                                TaxLedgNo = i;
                            else
                                SaleLedgNo = i;
                        }
                        if (commandcollection[i].CommandText == "AddMLedgerDetails")
                        {
                            commandcollection[i].Parameters.AddWithValue("@LedgerNo", commandcollection[0].Parameters["@ReturnID"].Value);
                        }
                        if (commandcollection[i].CommandText == "AddMLedgerDistDetails")
                        {
                            // commandcollection[i].Parameters.AddWithValue("@LedgerNo", commandcollection[0].Parameters["@ReturnID"].Value);
                        }
                        if (commandcollection[i].CommandText == "AddMItemTaxSetting")
                        {
                            commandcollection[i].Parameters.AddWithValue("@TaxLedgerNo", commandcollection[TaxLedgNo].Parameters["@ReturnID"].Value);
                            commandcollection[i].Parameters.AddWithValue("@SalesLedgerNo", commandcollection[SaleLedgNo].Parameters["@ReturnID"].Value);
                        }
                        if (commandcollection[i] != null)
                            commandcollection[i].ExecuteNonQuery();
                    }
                }

                myTrans.Commit();
                return true;
            }
            catch (Exception e)
            {
                myTrans.Rollback();

                if (e.GetBaseException().Message == "")
                {
                    strerrormsg = e.Message;
                }
                else
                {
                    strerrormsg = e.GetBaseException().Message;
                }
                return false;
            }
            finally
            {
                cn.Close();
            }
            //________________________________________________________________________________________________________________________________________________________________________________________________________________________
        }

        public bool AddMLedgerDistDetails(MLedgerDistDetails mledgerdistdetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMLedgerDistDetails";

            cmd.Parameters.AddWithValue("@PkSrNo", mledgerdistdetails.PkSrNo);

            cmd.Parameters.AddWithValue("@LedgerNo", mledgerdistdetails.LedgerNo);

            cmd.Parameters.AddWithValue("@DistAgentName", mledgerdistdetails.DistAgentName);

            cmd.Parameters.AddWithValue("@MobileNo", mledgerdistdetails.MobileNo);

            cmd.Parameters.AddWithValue("@Remark", mledgerdistdetails.Remark);

            cmd.Parameters.AddWithValue("@CompanyNo", DBGetVal.CompanyNo);

            commandcollection.Add(cmd);
            return true;
        }

        public bool ExecuteNonQueryStatementsServer()
        {

            SqlConnection cn = null;
            cn = new SqlConnection(CommonFunctions.ConStrServer);
            cn.Open();

            SqlTransaction myTrans;
            myTrans = cn.BeginTransaction();
            //cmd.Transaction = myTrans;
            //int cntLedg = 0;
            try
            {
                for (int i = 0; (i < this.commandcollection.Count); i = (i + 1))
                {
                    if ((this.commandcollection[i] != null))
                    {
                        commandcollection[i].Connection = cn;
                        commandcollection[i].Transaction = myTrans;
                        //if (commandcollection[i].CommandText == "AddMLedger1")
                        //{
                        //    cntLedg = i;
                        //}
                        //if (commandcollection[i].CommandText == "AddMLedgerDetails")
                        //{
                        //    commandcollection[i].Parameters.AddWithValue("@LedgerNo", commandcollection[cntLedg].Parameters["@ReturnID"].Value);
                        //}
                        //if (commandcollection[i].CommandText == "AddMLedgerDistDetails")
                        //{
                        //   // commandcollection[i].Parameters.AddWithValue("@LedgerNo", commandcollection[0].Parameters["@ReturnID"].Value);
                        //}
                        if (commandcollection[i] != null)
                            commandcollection[i].ExecuteNonQuery();
                    }
                }

                myTrans.Commit();
                return true;
            }
            catch (Exception e)
            {
                myTrans.Rollback();

                if (e.GetBaseException().Message == "")
                {
                    strerrormsg = e.Message;
                }
                else
                {
                    strerrormsg = e.GetBaseException().Message;
                }
                return false;
            }
            finally
            {
                cn.Close();
            }
            //________________________________________________________________________________________________________________________________________________________________________________________________________________________
        }

        public bool AddMItemTaxSetting(MItemTaxSetting mitemtaxsetting)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMItemTaxSetting";

            cmd.Parameters.AddWithValue("@PkSrNo", mitemtaxsetting.PkSrNo);

            cmd.Parameters.AddWithValue("@TaxSettingName", mitemtaxsetting.TaxSettingName);

            //cmd.Parameters.AddWithValue("@TaxLedgerNo", mitemtaxsetting.TaxLedgerNo);

            //cmd.Parameters.AddWithValue("@SalesLedgerNo", mitemtaxsetting.SalesLedgerNo);

            cmd.Parameters.AddWithValue("@CalculationMethod", mitemtaxsetting.CalculationMethod);

            cmd.Parameters.AddWithValue("@Percentage", mitemtaxsetting.Percentage);

            cmd.Parameters.AddWithValue("@IsActive", mitemtaxsetting.IsActive);

            cmd.Parameters.AddWithValue("@UserID", mitemtaxsetting.UserID);

            cmd.Parameters.AddWithValue("@UserDate", mitemtaxsetting.UserDate);

            cmd.Parameters.AddWithValue("@TaxTypeNo", mitemtaxsetting.TaxTypeNo);

            cmd.Parameters.AddWithValue("@TransactionTypeNo", mitemtaxsetting.TransactionTypeNo);

            cmd.Parameters.AddWithValue("@IGSTPercent", mitemtaxsetting.IGSTPercent);

            cmd.Parameters.AddWithValue("@CGSTPercent", mitemtaxsetting.CGSTPercent);

            cmd.Parameters.AddWithValue("@SGSTPercent", mitemtaxsetting.SGSTPercent);

            cmd.Parameters.AddWithValue("@UTGSTPercent", mitemtaxsetting.UTGSTPercent);

            cmd.Parameters.AddWithValue("@CessPercent", mitemtaxsetting.CessPercent);


            commandcollection.Add(cmd);
            return true;
        }
    }

    /// <summary>
    /// This Class use for MLedger
    /// </summary>
    public class MLedger
    {
        private long mLedgerNo;
        private string mLedgerUserNo;
        private string mLedgerName;
        private long mGroupNo;
        private double mOpeningBalance;
        private long mSignCode;
        private bool mInvFlag;
        private bool mMaintainBillByBill;
        private bool mIsActive;
        private string mContactPerson;
        private long mCompanyNo;
        private int mLedgerStatus;
        private long mUserID;
        private DateTime mUserDate;
        private string mModifiedBy;
        private int mStatusNo;
        private bool mIsEnroll;
        private bool mIsSendSMS;
        private string mLangLedgerName = "";
        private string Mmsg;

        /// <summary>
        /// This Properties use for LedgerNo
        /// </summary>
        public long LedgerNo
        {
            get { return mLedgerNo; }
            set { mLedgerNo = value; }
        }
        /// <summary>
        /// This Properties use for LedgerUserNo
        /// </summary>
        public string LedgerUserNo
        {
            get { return mLedgerUserNo; }
            set { mLedgerUserNo = value; }
        }
        /// <summary>
        /// This Properties use for LedgerName
        /// </summary>
        public string LedgerName
        {
            get { return mLedgerName; }
            set { mLedgerName = value; }
        }
        /// <summary>
        /// This Properties use for GroupNo
        /// </summary>
        public long GroupNo
        {
            get { return mGroupNo; }
            set { mGroupNo = value; }
        }
        /// <summary>
        /// This Properties use for OpeningBalance
        /// </summary>
        public double OpeningBalance
        {
            get { return mOpeningBalance; }
            set { mOpeningBalance = value; }
        }
        /// <summary>
        /// This Properties use for SignCode
        /// </summary>
        public long SignCode
        {
            get { return mSignCode; }
            set { mSignCode = value; }
        }
        /// <summary>
        /// This Properties use for InvFlag
        /// </summary>
        public bool InvFlag
        {
            get { return mInvFlag; }
            set { mInvFlag = value; }
        }
        /// <summary>
        /// This Properties use for MaintainBillByBill
        /// </summary>
        public bool MaintainBillByBill
        {
            get { return mMaintainBillByBill; }
            set { mMaintainBillByBill = value; }
        }
        /// <summary>
        /// This Properties use for IsActive
        /// </summary>
        public bool IsActive
        {
            get { return mIsActive; }
            set { mIsActive = value; }
        }
        /// <summary>
        /// This Properties use for ContactPerson
        /// </summary>
        public string ContactPerson
        {
            get { return mContactPerson; }
            set { mContactPerson = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for LedgerStatus
        /// </summary>
        public int LedgerStatus
        {
            get { return mLedgerStatus; }
            set { mLedgerStatus = value; }
        }
        /// <summary>
        /// This Properties use for UserID
        /// </summary>
        public long UserID
        {
            get { return mUserID; }
            set { mUserID = value; }
        }
        /// <summary>
        /// This Properties use for UserDate
        /// </summary>
        public DateTime UserDate
        {
            get { return mUserDate; }
            set { mUserDate = value; }
        }
        /// <summary>
        /// This Properties use for ModifiedBy
        /// </summary>
        public string ModifiedBy
        {
            get { return mModifiedBy; }
            set { mModifiedBy = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public int StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for IsEnroll
        /// </summary>
        public bool IsEnroll
        {
            get { return mIsEnroll; }
            set { mIsEnroll = value; }
        }
        /// <summary>
        /// This Properties use for IsSendSMS
        /// </summary>
        public bool IsSendSMS
        {
            get { return mIsSendSMS; }
            set { mIsSendSMS = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }

        public string LangLedgerName
        {
            get { return mLangLedgerName; }
            set { mLangLedgerName = value; }
        }
    }

    /// <summary>
    /// This Class use for MLedgerDetails
    /// </summary>
    public class MLedgerDetails
    {
        private long mLedgerDetailsNo;
        private long mLedgerNo;
        private string mAddress;
        private long mStateNo;
        private long mCityNo;
        private string mPinCode;
        private string mPhNo1;
        private string mPhNo2;
        private string mMobileNo1;
        private string mMobileNo2;
        private string mEmailID;
        private DateTime mDOB;
        private long mQualificationNo;
        private long mOccupationNo;
        private long mCustomerType;
        private double mCreditLimit;
        private string mPANNo;
        private string mVATNo;
        private string mCSTNo;
        private long mUserID;
        private DateTime mUserDate;
        private string mModifiedBy;
        private long mGender;
        private int mStatusNo;
        private long mCompanyNo;
        private DateTime mConsentDate;
        private string mLangAddress = "";
        private long mAreaNo;
        private string Mmsg;

        /// <summary>
        /// This Properties use for LedgerDetailsNo
        /// </summary>
        public long LedgerDetailsNo
        {
            get { return mLedgerDetailsNo; }
            set { mLedgerDetailsNo = value; }
        }
        /// <summary>
        /// This Properties use for LedgerNo
        /// </summary>
        public long LedgerNo
        {
            get { return mLedgerNo; }
            set { mLedgerNo = value; }
        }
        /// <summary>
        /// This Properties use for Address
        /// </summary>
        public string Address
        {
            get { return mAddress; }
            set { mAddress = value; }
        }
        /// <summary>
        /// This Properties use for StateNo
        /// </summary>
        public long StateNo
        {
            get { return mStateNo; }
            set { mStateNo = value; }
        }
        /// <summary>
        /// This Properties use for CityNo
        /// </summary>
        public long CityNo
        {
            get { return mCityNo; }
            set { mCityNo = value; }
        }
        /// <summary>
        /// This Properties use for PinCode
        /// </summary>
        public string PinCode
        {
            get { return mPinCode; }
            set { mPinCode = value; }
        }
        /// <summary>
        /// This Properties use for PhNo1
        /// </summary>
        public string PhNo1
        {
            get { return mPhNo1; }
            set { mPhNo1 = value; }
        }
        /// <summary>
        /// This Properties use for PhNo2
        /// </summary>
        public string PhNo2
        {
            get { return mPhNo2; }
            set { mPhNo2 = value; }
        }
        /// <summary>
        /// This Properties use for MobileNo1
        /// </summary>
        public string MobileNo1
        {
            get { return mMobileNo1; }
            set { mMobileNo1 = value; }
        }
        /// <summary>
        /// This Properties use for MobileNo2
        /// </summary>
        public string MobileNo2
        {
            get { return mMobileNo2; }
            set { mMobileNo2 = value; }
        }
        /// <summary>
        /// This Properties use for EmailID
        /// </summary>
        public string EmailID
        {
            get { return mEmailID; }
            set { mEmailID = value; }
        }
        /// <summary>
        /// This Properties use for DOB
        /// </summary>
        public DateTime DOB
        {
            get { return mDOB; }
            set { mDOB = value; }
        }
        /// <summary>
        /// This Properties use for QualificationNo
        /// </summary>
        public long QualificationNo
        {
            get { return mQualificationNo; }
            set { mQualificationNo = value; }
        }
        /// <summary>
        /// This Properties use for OccupationNo
        /// </summary>
        public long OccupationNo
        {
            get { return mOccupationNo; }
            set { mOccupationNo = value; }
        }
        /// <summary>
        /// This Properties use for CustomerType
        /// </summary>
        public long CustomerType
        {
            get { return mCustomerType; }
            set { mCustomerType = value; }
        }
        /// <summary>
        /// This Properties use for CreditLimit
        /// </summary>
        public double CreditLimit
        {
            get { return mCreditLimit; }
            set { mCreditLimit = value; }
        }
        /// <summary>
        /// This Properties use for PANNo
        /// </summary>
        public string PANNo
        {
            get { return mPANNo; }
            set { mPANNo = value; }
        }
        /// <summary>
        /// This Properties use for VATNo
        /// </summary>
        public string VATNo
        {
            get { return mVATNo; }
            set { mVATNo = value; }
        }
        /// <summary>
        /// This Properties use for CSTNo
        /// </summary>
        public string CSTNo
        {
            get { return mCSTNo; }
            set { mCSTNo = value; }
        }
        /// <summary>
        /// This Properties use for UserID
        /// </summary>
        public long UserID
        {
            get { return mUserID; }
            set { mUserID = value; }
        }
        /// <summary>
        /// This Properties use for UserDate
        /// </summary>
        public DateTime UserDate
        {
            get { return mUserDate; }
            set { mUserDate = value; }
        }
        /// <summary>
        /// This Properties use for ModifiedBy
        /// </summary>
        public string ModifiedBy
        {
            get { return mModifiedBy; }
            set { mModifiedBy = value; }
        }
        /// <summary>
        /// This Properties use for Gender
        /// </summary>
        public long Gender
        {
            get { return mGender; }
            set { mGender = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public int StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for ConsentDate
        /// </summary>
        public DateTime ConsentDate
        {
            get { return mConsentDate; }
            set { mConsentDate = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string LangAddress
        {
            get { return mLangAddress; }
            set { mLangAddress = value; }
        }

        public long AreaNo
        {
            get { return mAreaNo; }
            set { mAreaNo = value; }
        }
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }


    /// <summary>
    /// This Class use for MLedgerDistDetails
    /// </summary>
    public class MLedgerDistDetails
    {
        private long mPkSrNo;
        private long mLedgerNo;
        private string mDistAgentName;
        private string mMobileNo;
        private string mRemark;
        private int mStatusNo;
        private long mCompanyNo;
        private string Mmsg;

        /// <summary>
        /// This Properties use for PkSrNo
        /// </summary>
        public long PkSrNo
        {
            get { return mPkSrNo; }
            set { mPkSrNo = value; }
        }
        /// <summary>
        /// This Properties use for LedgerNo
        /// </summary>
        public long LedgerNo
        {
            get { return mLedgerNo; }
            set { mLedgerNo = value; }
        }
        /// <summary>
        /// This Properties use for DistAgentName
        /// </summary>
        public string DistAgentName
        {
            get { return mDistAgentName; }
            set { mDistAgentName = value; }
        }
        /// <summary>
        /// This Properties use for MobileNo
        /// </summary>
        public string MobileNo
        {
            get { return mMobileNo; }
            set { mMobileNo = value; }
        }
        /// <summary>
        /// This Properties use for Remark
        /// </summary>
        public string Remark
        {
            get { return mRemark; }
            set { mRemark = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public int StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }

}
