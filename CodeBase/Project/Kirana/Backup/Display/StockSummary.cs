﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Display
{
    /// <summary>
    /// This class is used for Stock Summary.
    /// </summary>
    public partial class StockSummary : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();
        DataSet dsVd = new DataSet();
        DBProgressBar PB;

        long ItNo, MNo, BItemNo;
        string ItNm, strItemNo = "";

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public StockSummary()
        {
            InitializeComponent();
        }

        private void StockSummary_Load(object sender, EventArgs e)
        {
            label8.Text = "";
            DTPFromDate.Text = "01-" + DBGetVal.ServerTime.ToString("MMM-yyyy");
            DTToDate.Text = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
            DTToDate.MinDate = DTPFromDate.Value;
            tabControl1.Visible = false;
            btnBarShow.Visible = false;
            KeyDownFormat(this.Controls);
            new GridSearch(gvItem, 1, 2);
        }

        private void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is CheckBox)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
            }
        }

        private void BtnShow_Click(object sender, EventArgs e)
        {
            try
            {
                string[] ReportSession;
                Form NewF = null;
                strItemNo = "";
                btnPrint.Visible = false;
                DataGridView1.DataSource = null;

                for (int i = 0; i < gvItem.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(gvItem.Rows[i].Cells[2].FormattedValue) == true)
                    {
                        if (strItemNo == "")
                            strItemNo = gvItem.Rows[i].Cells[0].Value.ToString();
                        else
                            strItemNo = strItemNo + "," + gvItem.Rows[i].Cells[0].Value.ToString();
                    }
                }
                if (strItemNo != "")
                {
                    if (rbNone.Checked == true)
                    {
                        panel1.Visible = false;
                        PB = new DBProgressBar(this);
                        PB.TimerStart();
                        PB.Ctrl = tabControl1;
                        tabControl1.SelectedTab = tabPage1;
                        //ShowFlage = true;

                        dsVd = ObjDset.FillDset("New", "Exec GetStockAllItemQty " + DBGetVal.CompanyNo + ",'" + DTPFromDate.Text + "','" + DTToDate.Text + "', '" + strItemNo + "'", CommonFunctions.ConStr);
                        DataTable dt = dsVd.Tables[0];
                        DataRow dr = dt.NewRow();
                        dsVd.Tables[0].Rows.Add(dr);
                        DataGridView1.DataSource = dsVd.Tables[0].DefaultView;
                        GetCount();
                        if (DataGridView1.Rows.Count > 1)
                            btnPrint.Visible = true;
                        else btnPrint.Visible = false;

                    }
                    else if (rbItemWise.Checked == true)
                    {
                        ReportSession = new string[4];

                        ReportSession[0] = DBGetVal.CompanyNo.ToString();
                        ReportSession[1] = Convert.ToDateTime(DTPFromDate.Text).ToString("dd-MMM-yyyy");
                        ReportSession[2] = Convert.ToDateTime(DTToDate.Text).ToString("dd-MMM-yyyy");
                        if (chkBarcode.Checked == true)
                        {
                            ReportSession[3] = BItemNo.ToString();
                        }
                        else
                            ReportSession[3] = strItemNo;
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            NewF = new Display.ReportViewSource(new Reports.ViewStockSummaryNew(), ReportSession);
                        else
                            NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("ViewStockSummaryNew.rpt", CommonFunctions.ReportPath), ReportSession);
                        ObjFunction.OpenForm(NewF, DBGetVal.MainForm);

                    }
                    else if (rbItemSummaryDtls.Checked == true)
                    {
                        ReportSession = new string[8];
                        ReportSession[0] = "Stock Summary Details";
                        ReportSession[1] = DBGetVal.CompanyNo.ToString();
                        ReportSession[2] = Convert.ToDateTime(DTPFromDate.Text).ToString("dd-MMM-yyyy");
                        ReportSession[3] = Convert.ToDateTime(DTToDate.Text).ToString("dd-MMM-yyyy");
                        ReportSession[4] = strItemNo;
                        ReportSession[5] = "0";//Type
                        ReportSession[6] = "0";//No
                        ReportSession[7] = 1.ToString();
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            NewF = new Display.ReportViewSource(new Reports.ViewStockSummDtlsByMonthlyRType(), ReportSession);
                        else
                            NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("ViewStockSummDtlsByMonthlyRType.rpt", CommonFunctions.ReportPath), ReportSession);
                        ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                    }
                    else if (rbItemDailyDetails.Checked == true)
                    {
                        ReportSession = new string[5];
                        ReportSession[0] = MNo.ToString();
                        ReportSession[1] = DBGetVal.CompanyNo.ToString();
                        ReportSession[2] = Convert.ToDateTime(DTPFromDate.Text).ToString("dd-MMM-yyyy");
                        ReportSession[3] = Convert.ToDateTime(DTToDate.Text).ToString("dd-MMM-yyyy");
                        ReportSession[4] = strItemNo;


                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            NewF = new Display.ReportViewSource(new Reports.ViewGetItemClosingStockByDateRType(), ReportSession);
                        else
                            NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("ViewGetItemClosingStockByDateRType.rpt", CommonFunctions.ReportPath), ReportSession);
                        ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                    }
                }
                else
                    OMMessageBox.Show("Select Atleast one Item ", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);



                if (DataGridView1.Rows.Count >= 1)
                {
                    btnPrint.Visible = true;
                    DataGridView1.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    DataGridView1.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    DataGridView1.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    DataGridView1.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                }
                else btnPrint.Visible = false;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
            
        }

        private void BindGrid()
        {
            try
            {
                tabControl1.Visible = false;
                panel1.Visible = false;
                pnlPB.Visible = true;
                PBBar.Minimum = 1;
                PBBar.Value = 5;
                DataTable dt = new DataTable();
                //string str = " SELECT DISTINCT MStockItems.ItemNo, MStockItems.ItemName,'false' as chk FROM MStockItems_V(NULL,NULL,NULL,NULL,NULL,NULL,NULL) as MStockItems INNER JOIN " +
                //               " TStock INNER JOIN TVoucherDetails INNER JOIN TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo ON TStock.FkVoucherTrnNo = TVoucherDetails.PkVoucherTrnNo ON " +
                //               " MStockItems.ItemNo = TStock.ItemNo ";//where TVoucherEntry.VoucherDate >='" + Convert.ToDateTime(DTPFromDate.Value).ToString("dd-MMM-yyyy") + "' and TVoucherEntry.VoucherDate<='" + Convert.ToDateTime(DTToDate.Value).ToString("dd-MMM-yyyy") + "'";

                //string str = "SELECT DISTINCT MStockItems.ItemNo, MStockItems.ItemName, 'false' AS chk FROM TVoucherDetails INNER JOIN TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo RIGHT OUTER JOIN " +
                          //"TStock INNER JOIN dbo.MStockItems_V(NULL, NULL, NULL, NULL, NULL, NULL, NULL) AS MStockItems ON TStock.ItemNo = MStockItems.ItemNo ON  TVoucherDetails.PkVoucherTrnNo = TStock.FkVoucherTrnNo order by MStockItems.ItemName";
                string strWhere = "";
                if (rdActive.Checked == true)
                    strWhere = " Where MStockItems.IsActive='true' ";
                else if (rdDeActive.Checked == true)
                    strWhere = " Where MStockItems.IsActive='false' ";
                string str = "SELECT DISTINCT  MStockItems.ItemNo,   (SELECT  MStockGroup.StockGroupName + ' ' + CASE WHEN (ItemShortCode <> '') THEN ItemShortCode ELSE ItemName END FROM  MStockItems AS MStockItems_1 INNER JOIN MStockGroup ON MStockItems_1.GroupNo = MStockGroup.StockGroupNo " +
                    " WHERE (MStockItems_1.ItemNo = MStockItems.ItemNo)) AS ItemName, 'false' AS chk " +
                    " FROM TStock INNER JOIN MStockItems ON TStock.ItemNo = MStockItems.ItemNo " +
                    strWhere +
                    " ORDER BY ItemName";
                dt = ObjFunction.GetDataView(str).Table;
                PBBar.Maximum = dt.Rows.Count + 5;


                gvItem.Rows.Clear();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    gvItem.Rows.Add();
                    Application.DoEvents();
                    PBBar.Value += 1;
                    for (int j = 0; j < gvItem.Columns.Count; j++)
                        gvItem.Rows[i].Cells[j].Value = dt.Rows[i].ItemArray[j];

                }
                gvItem.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                if (gvItem.Rows.Count > 0)
                {
                    gvItem.Focus();
                    gvItem.CurrentCell = gvItem[2, 0];
                }
                //PBBar.Value = 0;
                pnlPB.Visible = false;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }


        }

        private void DataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if ((DataGridView1.CurrentRow.Cells[1].Value.ToString()) != "")
                {
                    tabControl1.SelectedTab = tabPage2;
                    ItNo = Convert.ToInt64(DataGridView1.CurrentRow.Cells[1].Value);
                    ItNm = Convert.ToString(DataGridView1.CurrentRow.Cells[2].Value);
                    label7.Font = ObjFunction.GetFont();
                    label7.Text = ItNm;
                    dsVd = ObjDset.FillDset("New", "Select * From GetItemClosingStockMonthly (" + DBGetVal.CompanyNo + ",'" + DTPFromDate.Text + "','" + DTToDate.Text + "', " + ItNo + ", 0, 0 )", CommonFunctions.ConStr);

                    DataGridView2.DataSource = dsVd.Tables[0].DefaultView;
                    for (int i = 2; i < 9; i++)
                    {
                        DataGridView2.Columns[i].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;

                    }
                    for (int i = 2; i < 9; i++)
                    {
                        DataGridView2.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                    }
                    DataGridView2.Columns[0].Visible = false;
                    DataGridView2.Columns[4].Visible = false;
                    DataGridView2.Columns[6].Visible = false;
                    DataGridView2.Columns[8].Visible = false;
                    if (DataGridView2.Rows.Count >= 1)
                        btnPrint.Visible = true;
                    else
                        btnPrint.Visible = false;
                }
                else
                {
                    //tabControl1.SelectedTab = tabPage1;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }


        }

        private void GetCount()
        {
            for (int i = 0; i < DataGridView1.Rows.Count - 1; i = i + 1)
            {
                if (DataGridView1.Rows[i].Index != DataGridView1.Rows.Count - 1)
                {
                    if (Convert.IsDBNull(DataGridView1.Rows[DataGridView1.Rows.Count - 1].Cells[6].Value) != false)
                        DataGridView1.Rows[DataGridView1.Rows.Count - 1].Cells[6].Value = 0;


                    //DataGridView1.Rows[DataGridView1.Rows.Count - 1].Cells[0].Value = 100;
                    DataGridView1.Rows[DataGridView1.Rows.Count - 1].Cells[6].Value = Convert.ToDouble(DataGridView1.Rows[DataGridView1.Rows.Count - 1].Cells[6].Value) + Convert.ToDouble(DataGridView1.Rows[i].Cells[6].Value);
                }
            }

            //===========Total At footer===========

            DataGridView1.Rows[DataGridView1.Rows.Count - 1].DefaultCellStyle.BackColor = System.Drawing.Color.SkyBlue;
            //DataGridView1.Rows[DataGridView1.Rows.Count - 1].DefaultCellStyle.Font =ObjFunction.GetFont() ;
            DataGridView1.Rows[DataGridView1.Rows.Count - 1].Cells[2].Value = "Total";
        }

        private void GetCountVoucherDtls()
        {
            for (int i = 0; i < GridViewDaily.Rows.Count - 1; i = i + 1)
            {
                if (GridViewDaily.Rows[i].Index != GridViewDaily.Rows.Count - 1)
                {
                    if (Convert.IsDBNull(GridViewDaily.Rows[GridViewDaily.Rows.Count - 1].Cells[5].Value) != false)
                        GridViewDaily.Rows[GridViewDaily.Rows.Count - 1].Cells[5].Value = 0;

                    if (Convert.IsDBNull(GridViewDaily.Rows[GridViewDaily.Rows.Count - 1].Cells[6].Value) != false)
                        GridViewDaily.Rows[GridViewDaily.Rows.Count - 1].Cells[6].Value = 0;

                    //GridViewDaily.Rows[GridViewDaily.Rows.Count - 1].Cells[0].Value = 100;
                    GridViewDaily.Rows[GridViewDaily.Rows.Count - 1].Cells[5].Value = Convert.ToDouble(GridViewDaily.Rows[GridViewDaily.Rows.Count - 1].Cells[5].Value) + Convert.ToDouble(GridViewDaily.Rows[i].Cells[5].Value);
                    GridViewDaily.Rows[GridViewDaily.Rows.Count - 1].Cells[6].Value = Convert.ToDouble(GridViewDaily.Rows[GridViewDaily.Rows.Count - 1].Cells[6].Value) + Convert.ToDouble(GridViewDaily.Rows[i].Cells[6].Value);
                }

            }

            //===========Total At footer===========

            GridViewDaily.Rows[GridViewDaily.Rows.Count - 1].DefaultCellStyle.BackColor = System.Drawing.Color.SkyBlue;
            //GridViewDaily.Rows[GridViewDaily.Rows.Count - 1].DefaultCellStyle.Font = ObjFunction.GetFont();
            GridViewDaily.Rows[GridViewDaily.Rows.Count - 1].Cells[3].Value = "Total";
        }

        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                chkSelectAll.Checked = !chkSelectAll.Checked;

                for (int i = 0; i < gvItem.Rows.Count; i++)
                {
                    gvItem.Rows[i].Cells[2].Value = chkSelectAll.Checked;
                }
                BtnShow.Focus();
            }
        }

        private void DataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    tabControl1.SelectedTab = tabPage3;
                    MNo = Convert.ToInt64(DataGridView2.CurrentRow.Cells[0].Value);
                    label8.Text = "(" + Convert.ToString(DataGridView2.CurrentRow.Cells[1].Value) + ")";
                    lblDatewise.Font = ObjFunction.GetFont();
                    lblDatewise.Text = ItNm;
                    dsVd = ObjDset.FillDset("New", "Exec GetItemClosingStockByDate  " + MNo + "," + DBGetVal.CompanyNo + ",'" + DTPFromDate.Text + "','" + DTToDate.Text + "', " + ItNo + "", CommonFunctions.ConStr);

                    DataTable dt = dsVd.Tables[0];
                    DataRow dr = dt.NewRow();
                    dsVd.Tables[0].Rows.Add(dr);

                    GridViewDaily.DataSource = dsVd.Tables[0].DefaultView;

                    GridViewDaily.Columns[1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    GridViewDaily.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    GridViewDaily.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;


                    if (GridViewDaily.Rows.Count >= 1)
                        btnPrint.Visible = true;
                    else
                        btnPrint.Visible = false;
                    GetCountVoucherDtls();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                string[] ReportSession;
                Form NewF = null;
                if (tabControl1.SelectedIndex == 0)
                {
                    ReportSession = new string[4];

                    ReportSession[0] = DBGetVal.CompanyNo.ToString();
                    ReportSession[1] = Convert.ToDateTime(DTPFromDate.Text).ToString("dd-MMM-yyyy");
                    ReportSession[2] = Convert.ToDateTime(DTToDate.Text).ToString("dd-MMM-yyyy");
                    if (chkBarcode.Checked == true)
                    {
                        ReportSession[3] = BItemNo.ToString();
                    }
                    else
                        ReportSession[3] = strItemNo;
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                        NewF = new Display.ReportViewSource(new Reports.ViewStockSummaryNew(), ReportSession);
                    else
                        NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("ViewStockSummaryNew.rpt", CommonFunctions.ReportPath), ReportSession);
                    ObjFunction.OpenForm(NewF, DBGetVal.MainForm);

                }
                else if (tabControl1.SelectedIndex == 1)
                {
                    ReportSession = new string[8];

                    ReportSession[0] = DBGetVal.CompanyNo.ToString();
                    ReportSession[1] = Convert.ToDateTime(DTPFromDate.Text).ToString("dd-MMM-yyyy");
                    ReportSession[2] = Convert.ToDateTime(DTToDate.Text).ToString("dd-MMM-yyyy");
                    ReportSession[3] = ItNo.ToString();
                    ReportSession[4] = "0";//Type
                    ReportSession[5] = "0";//No
                    ReportSession[6] = "Stock Summary Details";
                    ReportSession[7] = ItNm;
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                        NewF = new Display.ReportViewSource(new Reports.ViewStockSummDtlsByMonthly(), ReportSession);
                    else
                        NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("ViewStockSummDtlsByMonthly.rpt", CommonFunctions.ReportPath), ReportSession);
                    ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                }
                else if (tabControl1.SelectedIndex == 2)
                {
                    ReportSession = new string[6];
                    ReportSession[0] = MNo.ToString();
                    ReportSession[1] = DBGetVal.CompanyNo.ToString();
                    ReportSession[2] = Convert.ToDateTime(DTPFromDate.Text).ToString("dd-MMM-yyyy");
                    ReportSession[3] = Convert.ToDateTime(DTToDate.Text).ToString("dd-MMM-yyyy");
                    ReportSession[4] = ItNo.ToString();
                    ReportSession[5] = ItNm;
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                        NewF = new Display.ReportViewSource(new Reports.ViewGetItemClosingStockByDate(), ReportSession);
                    else
                        NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("ViewGetItemClosingStockByDate.rpt", CommonFunctions.ReportPath), ReportSession);
                    ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 0)
            {
                if (DataGridView1.Rows.Count > 0)
                    btnPrint.Visible = true;
                else
                    btnPrint.Visible = false;
            }
            else if (tabControl1.SelectedIndex == 1)
            {
                if (DataGridView2.Rows.Count > 0)
                    btnPrint.Visible = true;
                else
                    btnPrint.Visible = false;
            }
            else if (tabControl1.SelectedIndex == 2)
            {
                if (GridViewDaily.Rows.Count > 0)
                    btnPrint.Visible = true;
                else
                    btnPrint.Visible = false;
            }
        }

        private void DTPFromDate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt16(e.KeyChar) == 13)
            {
                DTToDate.Focus();
            }
        }

        private void DTToDate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt16(e.KeyChar) == 13)
            {
                BtnItmShow.Focus();

            }
        }

        private void GridNull()
        {
            DataGridView1.DataSource = null;
            DataGridView2.DataSource = null;
            GridViewDaily.DataSource = null;
            tabControl1.SelectedTab = tabPage1;
            btnPrint.Visible = false;

        }

        private void chkBarcode_CheckedChanged(object sender, EventArgs e)
        {
            GridNull();

            if (chkBarcode.Checked == true)
            {
                panel2.Visible = true;
                BtnItmShow.Visible = false;
                txtBarCode.Focus();
                panel1.Visible = false;
                tabControl1.Visible = false;
                btnBarShow.Visible = true;


            }
            else
            {
                panel2.Visible = false;
                txtBarCode.Text = "";
                BtnItmShow.Visible = true;
                btnBarShow.Visible = false;


            }
        }

        private void BtnItmShow_Click(object sender, EventArgs e)
        {
            btnPrint.Visible = false;
            BindGrid();
            //panel1.Visible = false;
            panel1.Visible = true;
            panel2.Visible = false;
            tabControl1.Visible = false;
            btnBarShow.Visible = false;
            if (gvItem.Rows.Count > 0)
            {
                gvItem.Focus();
                gvItem.CurrentCell = gvItem[2, 0];
            }
            chkSelectAll.Checked = false;
        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < gvItem.Rows.Count; i++)
            {
                gvItem.Rows[i].Cells[2].Value = chkSelectAll.Checked;
            }
        }

        private void btnBarShow_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtBarCode.Text != "")
                {

                    btnPrint.Visible = false;
                    tabControl1.Visible = false;
                    PB = new DBProgressBar(this);
                    PB.TimerStart();
                    PB.Ctrl = tabControl1;
                    tabControl1.SelectedTab = tabPage1;
                    if (DataGridView1.Rows.Count > 0)
                        btnPrint.Visible = true;
                    else btnPrint.Visible = false;

                    txtBarCode.Text = txtBarCode.Text.Replace("\r", "").Replace("\n", "");
                    //BItemNo = ObjQry.ReturnLong("SELECT MStockItems.ItemNo FROM MStockItems_V(1,NULL) as MStockItems  WHERE (MStockBarcode.Barcode = '" + txtBarCode.Text + "') ", CommonFunctions.ConStr);
                    BItemNo = ObjQry.ReturnLong("SELECT ItemNo FROM MStockBarcode WHERE (Barcode = '" + txtBarCode.Text + "') ", CommonFunctions.ConStr);
                    dsVd = ObjDset.FillDset("New", "Exec GetStockAllItemQty " + DBGetVal.CompanyNo + ",'" + DTPFromDate.Text + "','" + DTToDate.Text + "', '" + BItemNo.ToString() + "'", CommonFunctions.ConStr);
                    DataTable dt = dsVd.Tables[0];
                    DataRow dr = dt.NewRow();
                    dsVd.Tables[0].Rows.Add(dr);
                    DataGridView1.DataSource = dsVd.Tables[0].DefaultView;
                    GetCount();
                }
                else
                    OMMessageBox.Show("Enter Barcode", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);

                if (DataGridView1.Rows.Count >= 1)
                {
                    btnPrint.Visible = true;
                    DataGridView1.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    DataGridView1.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    DataGridView1.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    DataGridView1.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                }
                else btnPrint.Visible = false;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DataGridView2_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            //if (e.ColumnIndex == 1)
            //{
            //    if (e.Value == null)
            //    {
            //      e.Value= Color.PeachPuff;
            //    }
            //}
        }

        private void rbType_Click(object sender, EventArgs e)
        {
            btnPrint.Visible = false;
            tabControl1.Visible = false;
            panel1.Visible = false;
        }

        private void rb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                BtnItmShow.Focus();
            }
        }

        private void txtBarCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnBarShow.Focus();
        }

        private void DTPFromDate_ValueChanged(object sender, EventArgs e)
        {
            DTToDate.MinDate = DTPFromDate.Value;
        }
    }
}
