﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Display
{
    public partial class GroupWiseSaleReports : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();
        DataSet dsVd = new DataSet();
        DataTable dtParty = new DataTable();
        DBProgressBar PB;

        public long CompNo, ItNo, MNo, Type1, No, ItNo1, BItemNo, VoucherTypeNo;
        public string ItName, RptTitle, ItNm;
        string strItemNo = "", strLedgerNo = "";
        
        public GroupWiseSaleReports()
        {
            InitializeComponent();
        }

        public GroupWiseSaleReports(long VchType)
        {
            InitializeComponent();
            VoucherTypeNo = VchType;
        }

        private void StockSummary_Load(object sender, EventArgs e)
        {
            CompNo = DBGetVal.CompanyNo;
            DTPFromDate.Text = "01-" + DBGetVal.ServerTime.ToString("MMM-yyyy");
            DTToDate.Text = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
            DTToDate.MinDate = DTPFromDate.Value;
            KeyDownFormat(this.Controls);

        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            /*13-Apr-2014
            * Exception Handling Purpose.
            * */
            try
            {
                this.Cursor = Cursors.WaitCursor;

                strItemNo = "";
                for (int i = 0; i < gvItem.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(gvItem.Rows[i].Cells[2].FormattedValue) == true)
                    {
                        if (strItemNo == "")
                            strItemNo = gvItem.Rows[i].Cells[0].Value.ToString();
                        else
                            strItemNo = strItemNo + "," + gvItem.Rows[i].Cells[0].Value.ToString();
                    }
                }
                if (strItemNo != "")
                {
                    string[] ReportSession;

                    ReportSession = new string[4];

                    ReportSession[0] = Convert.ToDateTime(DTPFromDate.Text).ToString("dd-MMM-yyyy");
                    ReportSession[1] = Convert.ToDateTime(DTToDate.Text).ToString("dd-MMM-yyyy");
                    ReportSession[2] = VoucherTypeNo.ToString();
                    ReportSession[3] = strItemNo;
                    

                    Form NewF = null;
                    if (rdDetails.Checked == true)
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            NewF = new Display.ReportViewSource(new Reports.GetStockDepartmentWise(), ReportSession);
                        else
                            NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("GetStockDepartmentWise.rpt", CommonFunctions.ReportPath), ReportSession);
                    }
                    //else
                    //{
                    //    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                    //        NewF = new Display.ReportViewSource(new Reports.RptLedgerItemWiseSalesSumm(), ReportSession);
                    //    else
                    //        NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("RptLedgerItemWiseSalesSumm.rpt", CommonFunctions.ReportPath), ReportSession);
                    //}
                    ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                }
                else
                    OMMessageBox.Show("Select Atleast one Item ", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);//For Common Error Displayed Purpose
            }
            this.Cursor = Cursors.Default;
        }

        public void BindGridItem()
        {
            /*13-Apr-2014
            * Exception Handling Purpose.
            * */
            try
            {
                DataTable dt = new DataTable();
                string str = " SELECT StockGroupNo, " +
                    " StockGroupName , " +
                    " 'false' as chk " +
                    " FROM MStockGroup Where ControlGroup=4 ";


                dt = ObjFunction.GetDataView(str).Table;
                gvItem.Rows.Clear();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    gvItem.Rows.Add();
                    for (int j = 0; j < gvItem.Columns.Count; j++)
                        gvItem.Rows[i].Cells[j].Value = dt.Rows[i].ItemArray[j];

                }
                gvItem.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                if (gvItem.Rows.Count > 0)
                {
                    gvItem.Focus();
                    gvItem.CurrentCell = gvItem[2, 0];
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);//For Common Error Displayed Purpose
            }
        }

        private void DTPFromDate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt16(e.KeyChar) == 13)
            {
                DTToDate.Focus();
            }
        }

        private void DTToDate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt16(e.KeyChar) == 13)
            {
                BtnShow.Focus();

            }
        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < gvItem.Rows.Count; i++)
            {
                gvItem.Rows[i].Cells[2].Value = chkSelectAll.Checked;
            }
        }

        #region KeyDown Events
        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                if (pnlItemDetails.Visible == true)
                {
                    chkSelectAll.Checked = !chkSelectAll.Checked;

                    for (int i = 0; i < gvItem.Rows.Count; i++)
                    {
                        gvItem.Rows[i].Cells[2].Value = chkSelectAll.Checked;
                    }
                    BtnPrint.Focus();
                }

            }
        }

        public void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else
                    KeyDownFormat(ctrl.Controls);
            }
        }
        #endregion

       

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            BtnShow.Focus();
            strLedgerNo = "";
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DTPFromDate_ValueChanged(object sender, EventArgs e)
        {
            DTToDate.MinDate = DTPFromDate.Value;
        }

        private void BtnShow_Click(object sender, EventArgs e)
        {
            BindGridItem();
        }

       

       
    }
}
