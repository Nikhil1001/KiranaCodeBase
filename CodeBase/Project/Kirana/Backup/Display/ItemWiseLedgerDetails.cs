﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Display
{
    /// <summary>
    /// This class is used for Item wise Sales Details.
    /// </summary>
    public partial class ItemWiseLedgerDetails : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();
        DataSet dsVd = new DataSet();
        DBProgressBar PB;

        long VchCode = 0;
        string strItemNo = "";
        long RateTypeNo = 0;
        string strGodown = "";

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public ItemWiseLedgerDetails()
        {
            InitializeComponent();
        }

        /// <summary>
        /// This is class of parameterised Constructor
        /// </summary>
        public ItemWiseLedgerDetails(long VchNo)
        {
            VchCode = VchNo;
            InitializeComponent();
            if (VchCode == VchType.Sales)
                this.Text = "ItemWise SalesDetails";
            else if (VchCode == VchType.Purchase)
                this.Text = "ItemWise PurchaseDetails";
        }

        private void StockSummary_Load(object sender, EventArgs e)
        {
            DTPFromDate.Text = "01-" + DBGetVal.ServerTime.ToString("MMM-yyyy");
            DTToDate.Text = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
            DTToDate.MinDate = DTPFromDate.Value;


            pnlItemDetails.Visible = false;
            pnlLocation.Visible = false;

            DTPFromDate.Focus();

            KeyDownFormat(this.Controls);

        }

        private void BtnShow_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                strItemNo = "";
                for (int i = 0; i < gvItem.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(gvItem.Rows[i].Cells[2].FormattedValue) == true)
                    {
                        if (strItemNo == "")
                            strItemNo = gvItem.Rows[i].Cells[0].Value.ToString();
                        else
                            strItemNo = strItemNo + "," + gvItem.Rows[i].Cells[0].Value.ToString();
                    }
                }
                if (strItemNo != "")
                {
                    string[] ReportSession;

                    ReportSession = new string[6];

                    ReportSession[0] = Convert.ToDateTime(DTPFromDate.Text).ToString(Format.DDMMMYYYY);
                    ReportSession[1] = Convert.ToDateTime(DTToDate.Text).ToString("dd-MMM-yyyy");
                    ReportSession[2] = "true";// (IsSuperMode() == true) ? "true" : "false";
                    ReportSession[3] = strItemNo;
                    ReportSession[4] = VchCode.ToString();
                    ReportSession[5] = strGodown;


                    Form NewF = null;

                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                        NewF = new Display.ReportViewSource(new Reports.RptItemWiseLedgerDetails(), ReportSession);
                    else
                        NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("RptItemWiseLedgerDetails.rpt", CommonFunctions.ReportPath), ReportSession);


                    ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                }
                else
                    OMMessageBox.Show("Select Atleast one Item ", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
            this.Cursor = Cursors.Default;
        }

        private void BindGrid()
        {
            try
            {

                strGodown = "";
                for (int k = 0; k < dgLocation.Rows.Count; k++)
                {

                    if (Convert.ToBoolean(dgLocation.Rows[k].Cells[1].FormattedValue) == true)
                    {

                        if (strGodown == "")
                        {
                            strGodown = "" + Convert.ToInt64(dgLocation.Rows[k].Cells[2].Value) + "";
                        }
                        else
                        {
                            strGodown = strGodown + "," + Convert.ToInt64(dgLocation.Rows[k].Cells[2].Value) + "";
                        }

                    }
                }
                if (strGodown != "")
                {

                    DataTable dt = new DataTable();
                    string str = " SELECT DISTINCT MStockItems.ItemNo, MStockGroup.StockGroupName + MStockItems.ItemName AS ItemName, 'false' AS chk " +
                                " FROM MStockItems INNER JOIN TStock INNER JOIN TVoucherDetails INNER JOIN " +
                                " TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo ON TStock.FkVoucherTrnNo = TVoucherDetails.PkVoucherTrnNo ON  " +
                                " MStockItems.ItemNo = TStock.ItemNo INNER JOIN " +
                                " MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo  INNER JOIN " +
                                "  TStockGodown ON TStock.PkStockTrnNo = TStockGodown.FKStockTrnNo INNER JOIN " +
                                "  MGodown ON TStockGodown.GodownNo = MGodown.GodownNo " +
                                " where TVoucherEntry.VoucherDate >='" + Convert.ToDateTime(DTPFromDate.Value).ToString("dd-MMM-yyyy") + "' and TVoucherEntry.VoucherDate<='" + Convert.ToDateTime(DTToDate.Value).ToString("dd-MMM-yyyy") + "' AND TVoucherEntry.VoucherTypeCode in(" + VchCode + "," + ((VchCode == VchType.Sales) ? 12 : 13) + ")  " +
                                " and  MGodown.GodownNo In (" + strGodown + ") " +
                                " order by MStockGroup.StockGroupName  + MStockItems.ItemName ";
                               


                    dt = ObjFunction.GetDataView(str).Table;
                    gvItem.Rows.Clear();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        gvItem.Rows.Add();
                        for (int j = 0; j < gvItem.Columns.Count; j++)
                            gvItem.Rows[i].Cells[j].Value = dt.Rows[i].ItemArray[j];

                    }
                    gvItem.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    pnlItemDetails.Visible = true;
                    pnlLocation.Visible = false;
                    if (gvItem.Rows.Count > 0)
                    {
                        
                        gvItem.Focus();
                        gvItem.CurrentCell = gvItem[2, 0];
                    }
                    new GridSearch(gvItem, 1);
                }
                else
                {
                    pnlLocation.Visible = true;
                    pnlItemDetails.Visible = false;
                    OMMessageBox.Show("Please Select Atleast One Location", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);

                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                string[] ReportSession;

                ReportSession = new string[6];

                ReportSession[0] = DBGetVal.CompanyNo.ToString();
                ReportSession[1] = Convert.ToDateTime(DTPFromDate.Text).ToString("dd-MMM-yyyy");
                ReportSession[2] = Convert.ToDateTime(DTToDate.Text).ToString("dd-MMM-yyyy");
                ReportSession[3] = "true";// (IsSuperMode() == true) ? "true" : "false";
                ReportSession[4] = strItemNo;
                ReportSession[5] = strGodown;

                Form NewF = new Display.ReportViewSource(new Reports.ViewStockSummaryNew(), ReportSession);
                ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void DTPFromDate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt16(e.KeyChar) == 13)
            {
                DTToDate.Focus();
            }
        }

        private void DTToDate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt16(e.KeyChar) == 13)
            {
                BtnItmShow.Focus();

            }
        }

        private void BtnItmShow_Click(object sender, EventArgs e)
        {
            while (dgLocation.Rows.Count > 0)
                dgLocation.Rows.RemoveAt(0);

            pnlItemDetails.Visible = false;
            string sqlQuery = "Select GodownName,'false' as chkCheck,GodownNo From MGodown Where GodownNo<>1  order By GodownName";
            pnlLocation.Visible = true;
            DataTable dtLocation = ObjFunction.GetDataView(sqlQuery).Table;
            for (int j = 0; j < dtLocation.Rows.Count; j++)
            {
                dgLocation.Rows.Add();
                for (int i = 0; i < dgLocation.Columns.Count; i++)
                {
                    if (dtLocation.Rows.Count > 0)
                        dgLocation.Rows[j].Cells[i].Value = dtLocation.Rows[j].ItemArray[i].ToString();

                }
            }

        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < gvItem.Rows.Count; i++)
            {
                gvItem.Rows[i].Cells[2].Value = chkSelectAll.Checked;
            }
        }

        #region KeyDown Events
        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                chkSelectAll.Checked = !chkSelectAll.Checked;

                for (int i = 0; i < gvItem.Rows.Count; i++)
                {
                    gvItem.Rows[i].Cells[2].Value = chkSelectAll.Checked;
                }
                BtnShow.Focus();
            }
            //else if (e.KeyCode == Keys.O && e.Control)
            //{
            //    if (DBGetVal.IsAdmin == true)
            //    {
            //        txtRateTypePassword.Enabled = true;
            //        pnlRateType.Visible = true;
            //        txtRateTypePassword.Text = "";
            //        txtRateTypePassword.Focus();
            //    }
            //}

        }

        private void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else
                    KeyDownFormat(ctrl.Controls);
            }
        }
        #endregion

        private void gvItem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                BtnShow.Focus();
            }
        }

        private bool IsSuperMode()
        {
            bool flag = false;
            long RTNo = RateTypeNo;
            if (RTNo == 1)
            {
                flag = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ARateSuperMode));
            }
            else if (RTNo == 2)
            {
                flag = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.BRateSuperMode));
            }
            else if (RTNo == 3)
            {
                flag = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.CRateSuperMode));
            }
            else if (RTNo == 4)
            {
                flag = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.DRateSuperMode));
            }
            else if (RTNo == 5)
            {
                flag = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ERateSuperMode));
            }
            return flag;
        }

        #region Rate Type Password related Methods

        private void txtRateTypePassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                btnRateTypeOK_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Escape)
            {
                pnlRateType.Visible = false;
                BtnShow.Focus();
            }
        }

        private void btnRateTypeOK_Click(object sender, EventArgs e)
        {
            bool flag = false;
            string[,] arr = new string[5, 2];
            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ARateIsActive)) == true)
            {
                arr[0, 0] = ObjFunction.GetAppSettings(AppSettings.ARatePassword);
                arr[0, 1] = "ASaleRate";
            }

            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.BRateIsActive)) == true)
            {
                arr[1, 0] = ObjFunction.GetAppSettings(AppSettings.BRatePassword);
                arr[1, 1] = "BSaleRate";
            }

            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.CRateIsActive)) == true)
            {
                arr[2, 0] = ObjFunction.GetAppSettings(AppSettings.CRatePassword);
                arr[2, 1] = "CSaleRate";
            }

            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.DRateIsActive)) == true)
            {
                arr[3, 0] = ObjFunction.GetAppSettings(AppSettings.DRatePassword);
                arr[3, 1] = "DSaleRate";
            }

            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ERateIsActive)) == true)
            {
                arr[4, 0] = ObjFunction.GetAppSettings(AppSettings.ERatePassword);
                arr[4, 1] = "ESaleRate";
            }

            for (int i = 0; i < 5; i++)
            {
                if (arr[i, 0] != null)
                {
                    if (txtRateTypePassword.Text == arr[i, 0].ToString())
                    {

                        RateTypeNo = i + 1;

                        flag = true;
                        break;
                    }
                }
            }
            if (flag == false)
            {
                OMMessageBox.Show("Please enter valid password", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                txtRateTypePassword.Focus();
            }
            else
            {
                pnlRateType.Visible = false;
            }
        }

        private void btnRateTypeCancel_Click(object sender, EventArgs e)
        {
            txtRateTypePassword.Text = "";
            pnlRateType.Visible = false;
        }

        #endregion



        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DTPFromDate_ValueChanged(object sender, EventArgs e)
        {
            DTToDate.MinDate = DTPFromDate.Value;
        }

        private void btnLocationOk_Click(object sender, EventArgs e)
        {
            pnlItemDetails.Visible = false;
           
            BindGrid();
           
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < dgLocation.Rows.Count; i++)
            {
                dgLocation.Rows[i].Cells[1].Value = checkBox1.Checked;
            }
        }


    }
}
