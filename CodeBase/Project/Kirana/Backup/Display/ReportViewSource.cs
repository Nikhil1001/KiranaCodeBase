﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using System.Drawing.Printing;

namespace Kirana.Display
{
    /// <summary>
    /// This class is used for Report View Source.
    /// </summary>
    public partial class ReportViewSource : Form
    {
        internal static string TitleName, ReportName;

        internal string[] ReportSession;
        ReportDocument report = new ReportDocument();
        ConnectionInfo LIF = new ConnectionInfo();
        TableLogOnInfo logoninfo = new TableLogOnInfo();
        ParameterField paramField = new ParameterField();
        ParameterDiscreteValue paramDiscreteValue = new ParameterDiscreteValue();
        ParameterValues paramValues = new ParameterValues();
        private bool IsMySession = false;
        private string PrinterName = "";
        /// <summary>
        /// This is class of (Object,string[]) Parameterised Constructor
        /// </summary>
        public ReportViewSource(object rpt, string[] ReportSession)
        {
            InitializeComponent();
            report = (ReportDocument)rpt;
            this.ReportSession = ReportSession;
        }
        public ReportViewSource(object rpt, string[] ReportSession, bool IsMySession)
        {
            InitializeComponent();
            report = (ReportDocument)rpt;
            this.ReportSession = ReportSession;
            this.IsMySession = IsMySession;
        }
       
        /// <summary>
        /// This is class of (object) Parameterised Constructor
        /// </summary>
        public ReportViewSource(object rpt)
        {
            InitializeComponent();
            report = (ReportDocument)rpt;

        }

        private void ReportViewSource_Load(object sender, EventArgs e)
        {
            TitleName = "";

            int i, cnt = 0;
            if (report.FileName == "")
            {
                for (i = report.ToString().Length - 1; i >= 0; i--)
                {
                    if (Convert.ToChar(report.ToString()[i]) == 46) break;
                    cnt++;
                }
                ReportName = report.ToString().Substring(report.ToString().Length - cnt, cnt);
            }
            else
            {
                for (i = report.FileName.Length - 1; i >= 0; i--)
                {
                    if (Convert.ToChar(report.FileName[i]) == '\\') break;
                    cnt++;
                }
                ReportName = report.FileName.Substring(report.FileName.Length - cnt, cnt).Replace(".rpt", "");
            }


            LIF.ServerName = CommonFunctions.ServerName;

            LIF.IntegratedSecurity = true;
            LIF.DatabaseName = DBGetVal.DBName;
            LIF.UserID = "OM96";
            LIF.Password = "OM96";
            LIF.IntegratedSecurity = false;

            foreach (CrystalDecisions.CrystalReports.Engine.Table table in report.Database.Tables)
            {
                logoninfo = table.LogOnInfo;
                logoninfo.ConnectionInfo = LIF;
                table.ApplyLogOnInfo(logoninfo);
            }


            foreach (Section sec in report.ReportDefinition.Sections)
            {
                ReportObjects myReportObjects = sec.ReportObjects;
                foreach (ReportObject myReportObject in myReportObjects)
                {
                    if (myReportObject.Kind == ReportObjectKind.SubreportObject)
                    {
                        SubreportObject mySubreportObject = (SubreportObject)myReportObject;
                        ReportDocument subReportDocument = mySubreportObject.OpenSubreport(mySubreportObject.SubreportName);
                        foreach (Table tab in subReportDocument.Database.Tables)
                        {
                            // Get the TableLogOnInfo object.
                            logoninfo = tab.LogOnInfo;
                            logoninfo.ConnectionInfo = LIF;
                            tab.ApplyLogOnInfo(logoninfo);
                        }
                    }
                }
            }

            ReportObject reportObject = report.ReportDefinition.ReportObjects["txtRegName"];
            if (reportObject != null)
            {
                TextObject textObject = (TextObject)reportObject;
                textObject.Text = DBGetVal.RegCompName;
            }

            //To set parameters for report
            GetParaMeters();
            //To set report to ReportViewer
            crystalReportViewer1.ReportSource = report;


            System.Drawing.Printing.PrinterSettings objPrint = new System.Drawing.Printing.PrinterSettings();
            objPrint.PrinterName = report.PrintOptions.PrinterName;

            string strPrinters = null;
            foreach (string strItem in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                CmbPrinterName.Items.Add(strPrinters + strItem);
            }
            CmbPrinterName.SelectedText = objPrint.PrinterName;
            CmbPrinterName.SelectedIndex = 0;
            BtnPrint.Focus();
        }

        private void GetParaMeters()
        {
            int ParaCount = 0;
            paramField = new ParameterField();
            paramDiscreteValue = new ParameterDiscreteValue();
            paramValues = new ParameterValues();

            if (ReportName != "BarCodePrint" && ReportName != "BarCodePrintBig" && ReportName != "BarCodePrintBig50x50" && ReportName != "BarCodePrintMedium50x25" && ReportName != "BarCodePrintSmall34x22")
            {
                paramField = report.ParameterFields[ParaCount];
                paramDiscreteValue.Value = DBGetVal.CompanyName;
                paramValues.Add(paramDiscreteValue);
                paramField.CurrentValues = paramValues;
                report.DataDefinition.ParameterFields[ParaCount].ApplyCurrentValues(paramValues);
                ParaCount++;
            }


            if (IsMySession == false && ReportName != "RptGetBill" && !ReportName.StartsWith("GetPrintGodown") && !ReportName.StartsWith("GetBill") && ReportName != "BarCodePrint" && ReportName != "BarCodePrintBig" && ReportName != "GetBigBill" && ReportName != "BarCodePrintBig50x50" && ReportName != "BarCodePrintMedium50x25" && ReportName != "BarCodePrintSmall34x22")
            {
                paramField = new ParameterField();
                paramDiscreteValue = new ParameterDiscreteValue();
                paramValues = new ParameterValues();

                paramField = report.ParameterFields[ParaCount];
                paramDiscreteValue.Value = DBGetVal.FromDate;
                paramValues.Add(paramDiscreteValue);
                paramField.CurrentValues = paramValues;
                report.DataDefinition.ParameterFields[ParaCount].ApplyCurrentValues(paramValues);
                ParaCount++;

                paramField = new ParameterField();
                paramDiscreteValue = new ParameterDiscreteValue();
                paramValues = new ParameterValues();

                paramField = report.ParameterFields[ParaCount];
                paramDiscreteValue.Value = DBGetVal.ToDate;
                paramValues.Add(paramDiscreteValue);
                paramField.CurrentValues = paramValues;
                report.DataDefinition.ParameterFields[ParaCount].ApplyCurrentValues(paramValues);
                ParaCount++;
            }
            for (int i = 0; i < ReportSession.Length; i++)
            {
                paramField = new ParameterField();
                paramDiscreteValue = new ParameterDiscreteValue();
                paramValues = new ParameterValues();

                paramField = report.ParameterFields[ParaCount];
                paramDiscreteValue.Value = ReportSession[i];
                paramValues.Add(paramDiscreteValue);
                paramField.CurrentValues = paramValues;
                report.DataDefinition.ParameterFields[ParaCount].ApplyCurrentValues(paramValues);

                ParaCount += 1;
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            report.PrintOptions.PrinterName = CmbPrinterName.SelectedItem.ToString();

            report.PrintToPrinter(1, true, 0, 0);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void chkToolBar_CheckedChanged(object sender, EventArgs e)
        {
            crystalReportViewer1.DisplayToolbar = chkToolBar.Checked;
        }

        private void chkStatusBar_CheckedChanged(object sender, EventArgs e)
        {
            crystalReportViewer1.DisplayStatusBar = chkStatusBar.Checked;
        }

        private void ReportViewSource_FormClosing(object sender, FormClosingEventArgs e)
        {
            report.Close();
        }
    }
}

