﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Display
{
    /// <summary>
    /// This class used for Outstanding
    /// </summary>
    public partial class OutStanding : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        
        long VoucherType;

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public OutStanding()
        {
            InitializeComponent();
        }

        /// <summary>
        /// This is class of parameterised Constructor
        /// </summary>
        public OutStanding(long _VchType)
        {
            InitializeComponent();
            if (_VchType == VchType.Sales)
            {
                this.Name = "Sales Outstanding";
                this.Text = "Sales Outstanding";
                VoucherType = _VchType;
            }
            else if (_VchType == VchType.Purchase)
            {
                this.Name = "Purchase OutStanding";
                this.Text = "Purchase OutStanding";
                VoucherType=_VchType;
            }
            
            
        }

        private void OutStanding_Load(object sender, EventArgs e)
        {
            DTPFromDate.Text = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
            DTPToDate.Text = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
            KeyDownFormat(this.Controls);
        }

        private void BindGrid()
        {
            try
            {
                gvParty.Rows.Clear();
                string str = "";
                if (VoucherType == VchType.Sales)
                    //str = "Select LedgerNo,LedgerName ,'false' as Chk From MLedger Where GroupNo in (" + GroupType.SundryDebtors + ")  order by LedgerName";
                    str = " SELECT Distinct MLedger.LedgerNo, MLedger.LedgerName,'false' as Chk " +
                        " FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN " +
                        " TVoucherRefDetails ON TVoucherDetails.PkVoucherTrnNo = TVoucherRefDetails.FkVoucherTrnNo INNER JOIN " +
                        " MLedger ON TVoucherRefDetails.LedgerNo = MLedger.LedgerNo WHERE "+//(TVoucherEntry.VoucherTypeCode = " + VoucherType + ") AND 
                            " (TVoucherRefDetails.TypeOfRef = 3) AND (TVoucherEntry.CompanyNo = " + DBGetVal.CompanyNo + ") " +
                        " and (TVoucherEntry.VoucherDate<='" + DTPToDate.Value.Date.ToString(Format.DDMMMYYYY) + "') and TVoucherEntry.IsCancel='false' and  MLedger.GroupNo in (" + GroupType.SundryDebtors + ")" +

                        " Union All " +

                        " Select DISTINCT LedgerNo,LedgerName ,'false' AS Chk from MLedger Where LedgerNo Not in(Select LedgerNo from TVoucherRefDetails) and OpeningBalance>0 and GroupNo IN (26) " +
                        " ORDER BY LedgerName ";
                else
                    //str = "Select LedgerNo,LedgerName ,'false' as Chk From MLedger Where GroupNo in (" + GroupType.SundryCreditors + ")  order by LedgerName";
                    str = " SELECT Distinct MLedger.LedgerNo, MLedger.LedgerName ,'false' as Chk" +
                       " FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN " +
                       " TVoucherRefDetails ON TVoucherDetails.PkVoucherTrnNo = TVoucherRefDetails.FkVoucherTrnNo INNER JOIN " +
                       " MLedger ON TVoucherRefDetails.LedgerNo = MLedger.LedgerNo WHERE (TVoucherEntry.VoucherTypeCode = " + VoucherType + ") AND (TVoucherRefDetails.TypeOfRef = 3) AND (TVoucherEntry.CompanyNo = " + DBGetVal.CompanyNo + ") " +
                       " and (TVoucherEntry.VoucherDate<='" + DTPToDate.Value.Date.ToString(Format.DDMMMYYYY) + "') and TVoucherEntry.IsCancel='false' and  MLedger.GroupNo in (" + GroupType.SundryCreditors + ")" +

                       " Union All " +

                        " Select DISTINCT LedgerNo,LedgerName ,'false' AS Chk from MLedger Where LedgerNo Not in(Select LedgerNo from TVoucherRefDetails) and OpeningBalance>0 and GroupNo IN (" + GroupType.SundryCreditors + ") " +
                        " ORDER BY LedgerName ";

                       //" ORDER BY MLedger.LedgerName ";
                DataTable dt = ObjFunction.GetDataView(str).Table;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    gvParty.Rows.Add();
                    for (int j = 0; j < gvParty.Columns.Count; j++)
                    {
                        gvParty.Rows[i].Cells[j].Value = dt.Rows[i].ItemArray[j];
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }
        
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                string strParty = "";
                for (int i = 0; i < gvParty.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(gvParty.Rows[i].Cells[2].FormattedValue) == true)
                    {
                        if (strParty == "")
                            strParty = gvParty.Rows[i].Cells[0].Value.ToString();
                        else
                            strParty = strParty + "," + gvParty.Rows[i].Cells[0].Value.ToString();
                    }
                }
                if (strParty != "")
                {
                    if (rbSummary.Checked == true)
                    {
                        string[] ReportSession = new string[4];
                        ReportSession[0] = VoucherType.ToString();
                        ReportSession[1] = DBGetVal.CompanyNo.ToString();
                        ReportSession[2] = Convert.ToDateTime(DTPToDate.Text).ToString("dd-MMM-yyyy");
                        ReportSession[3] = strParty;

                        Form NewF = null;
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            NewF = new Display.ReportViewSource(new Reports.GetOutStanding(), ReportSession);
                        else
                            NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("GetOutStanding.rpt", CommonFunctions.ReportPath), ReportSession);
                        ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                    }
                    else if (rbDetails.Checked == true)
                    {
                        string[] ReportSession = new string[4];
                        ReportSession[0] = VoucherType.ToString();
                        ReportSession[1] = DBGetVal.CompanyNo.ToString();
                        ReportSession[2] = Convert.ToDateTime(DTPToDate.Text).ToString("dd-MMM-yyyy");
                        ReportSession[3] = strParty;

                        Form NewF = null;
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            NewF = new Display.ReportViewSource(new Reports.GetOutstandingDetails(), ReportSession);
                        else
                            NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("GetOutstandingDetails.rpt", CommonFunctions.ReportPath), ReportSession);
                        ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                    }
                }
                else
                    OMMessageBox.Show("Select Atleast one Party ", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void BtnPartyShow_Click(object sender, EventArgs e)
        {
            BindGrid();
            pnlParty.Visible = true;
        }

        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                chkSelectAll.Checked = !chkSelectAll.Checked;

                for (int i = 0; i < gvParty.Rows.Count; i++)
                {
                    gvParty.Rows[i].Cells[2].Value = chkSelectAll.Checked;
                }
                btnShow.Focus();
            }
        }

        private void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is CheckBox)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
            }
        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < gvParty.Rows.Count; i++)
            {
                gvParty.Rows[i].Cells[2].Value = chkSelectAll.Checked;
            }
        }
    }
}
