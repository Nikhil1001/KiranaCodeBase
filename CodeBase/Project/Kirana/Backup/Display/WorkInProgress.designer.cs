﻿namespace Kirana.Display
{
    partial class WorkInProgress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblheader = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.PB = new System.Windows.Forms.ProgressBar();
            this.bgWorker = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // lblheader
            // 
            this.lblheader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblheader.BackColor = System.Drawing.Color.Coral;
            this.lblheader.ForeColor = System.Drawing.Color.White;
            this.lblheader.Location = new System.Drawing.Point(2, 2);
            this.lblheader.Name = "lblheader";
            this.lblheader.Size = new System.Drawing.Size(470, 23);
            this.lblheader.TabIndex = 908;
            this.lblheader.Text = "Work In Progress";
            this.lblheader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblStatus
            // 
            this.lblStatus.BackColor = System.Drawing.Color.Transparent;
            this.lblStatus.ForeColor = System.Drawing.Color.Black;
            this.lblStatus.Location = new System.Drawing.Point(5, 31);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(467, 23);
            this.lblStatus.TabIndex = 909;
            this.lblStatus.Text = "Please Wait";
            // 
            // PB
            // 
            this.PB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.PB.Location = new System.Drawing.Point(5, 57);
            this.PB.Name = "PB";
            this.PB.Size = new System.Drawing.Size(467, 23);
            this.PB.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.PB.TabIndex = 910;
            // 
            // bgWorker
            // 
            this.bgWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorker_RunWorkerCompleted);
            // 
            // WorkInProgress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 107);
            this.Controls.Add(this.PB);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.lblheader);
            this.Name = "WorkInProgress";
            this.Text = "WorkInProgress";
            this.Shown += new System.EventHandler(this.WorkInProgress_Shown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblheader;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.ProgressBar PB;
        public System.ComponentModel.BackgroundWorker bgWorker;
    }
}