﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using OM;
using JitControls;

namespace Kirana.UI
{
    public partial class FirmAccountDialog : Form
    {
        OMCommonClass cc = new OMCommonClass();
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        Security secure = new Security();

        public string ReturnDataBaseName = "";
        public string ReturnServerName = "";

        public FirmAccountDialog()
        {
            InitializeComponent();
        }

        private void FirmAccountDialog_Shown(object sender, EventArgs e)
        {
            dgDetails.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            BinGrid();
            dgDetails.Focus();
        }

        public void BinGrid()
        {
            string sql = " SELECT 0 As SrNo,ComanyName, DataBaseName,ServerName " +
                         ", (Case When (DataBaseName='" + CommonFunctions.DatabaseName + "') then 1 else 0 End ) As SelectType  " +
                            " FROM MFirmAccount Where IsActive='true' ";

            DataTable dt = ObjFunction.GetDataView(sql, "Data Source=" + CommonFunctions.ServerName + ";Initial Catalog=" + CommonFunctions.DefaultDatabaseName + ";User ID=OM96;Password=OM96").Table;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dgDetails.Rows.Add();
                for (int j = 0; j < dt.Columns.Count - 1; j++)
                {
                    dgDetails.Rows[i].Cells[j].Value = dt.Rows[i].ItemArray[j].ToString();
                }
                dgDetails.Rows[i].Cells[0].Value = i + 1;
                if (dt.Rows[i].ItemArray[3].ToString().Trim() == "1")
                {
                    dgDetails.Rows[i].DefaultCellStyle.BackColor = Color.Green;
                    dgDetails.Rows[i].DefaultCellStyle.SelectionBackColor = Color.Green;
                }
            }
        }

        private void dgDetails_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                if (OMMessageBox.Show("Are you sure want to select "+dgDetails[1,dgDetails.CurrentCell.RowIndex].Value.ToString()+" Company?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ReturnDataBaseName = dgDetails[2, dgDetails.CurrentRow.Index].Value.ToString();

                    ReturnServerName = dgDetails[3, dgDetails.CurrentRow.Index].Value.ToString();
                    if (ReturnServerName.Trim() == "")
                        ReturnServerName = CommonFunctions.DefaultServerName;
                    this.Close();
                }
            }
            else if (e.KeyCode == Keys.Escape)
            {
                ReturnDataBaseName = "";
                ReturnServerName = "";
                this.Close();
            }
            else if (e.KeyCode == Keys.Control && e.KeyCode == Keys.D)
            {
                ReturnDataBaseName = CommonFunctions.DefaultDatabaseName;
                ReturnServerName = CommonFunctions.DefaultServerName;
                this.Close();
            }
        }
    }
}
