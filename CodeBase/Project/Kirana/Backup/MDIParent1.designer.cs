﻿namespace Kirana
{
    partial class MDIParent1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MDIParent1));
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printPreviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolBarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusBarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.newWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cascadeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tileVerticalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tileHorizontalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.arrangeIconsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.contentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.TSSDateTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.TSCmpName = new System.Windows.Forms.ToolStripStatusLabel();
            this.TSUserName = new System.Windows.Forms.ToolStripStatusLabel();
            this.TSAppVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.TSCompName = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblCompName = new System.Windows.Forms.Label();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.pnlActivate = new System.Windows.Forms.Panel();
            this.toolBar1 = new System.Windows.Forms.ToolBar();
            this.toolBarButton1 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton2 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton3 = new System.Windows.Forms.ToolBarButton();
            this.lblMainTitle = new System.Windows.Forms.Label();
            this.pnlInfo = new System.Windows.Forms.Panel();
            this.lblInfo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.pnlMainMenu = new System.Windows.Forms.Panel();
            this.pnlSystemLock = new System.Windows.Forms.Panel();
            this.btnRateTypeCancel = new System.Windows.Forms.Button();
            this.btnRateTypeOK = new System.Windows.Forms.Button();
            this.txtRateTypePassword = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.pnlSubMenu = new System.Windows.Forms.Panel();
            this.btnRetailerTools = new System.Windows.Forms.Button();
            this.btnEOD = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnPurchase = new System.Windows.Forms.Button();
            this.btnItemChangeRate = new System.Windows.Forms.Button();
            this.btnBilling = new System.Windows.Forms.Button();
            this.btnSalesReturn = new System.Windows.Forms.Button();
            this.btnTopItemSales = new System.Windows.Forms.Button();
            this.btnWeeklySales = new System.Windows.Forms.Button();
            this.btnSalesRegister = new System.Windows.Forms.Button();
            this.btnPurchaseReturn = new System.Windows.Forms.Button();
            this.btnSaleSummary = new System.Windows.Forms.Button();
            this.btnTopBrandSales = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.lblDispTaskbar = new System.Windows.Forms.Label();
            this.btnMinimize = new System.Windows.Forms.Button();
            this.statusStrip.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.pnlActivate.SuspendLayout();
            this.pnlInfo.SuspendLayout();
            this.pnlMainMenu.SuspendLayout();
            this.pnlSystemLock.SuspendLayout();
            this.pnlSubMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // fileMenu
            // 
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(32, 19);
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 6);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 6);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // printPreviewToolStripMenuItem
            // 
            this.printPreviewToolStripMenuItem.Name = "printPreviewToolStripMenuItem";
            this.printPreviewToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // printSetupToolStripMenuItem
            // 
            this.printSetupToolStripMenuItem.Name = "printSetupToolStripMenuItem";
            this.printSetupToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // editMenu
            // 
            this.editMenu.Name = "editMenu";
            this.editMenu.Size = new System.Drawing.Size(32, 19);
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 6);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 6);
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // viewMenu
            // 
            this.viewMenu.Name = "viewMenu";
            this.viewMenu.Size = new System.Drawing.Size(32, 19);
            // 
            // toolBarToolStripMenuItem
            // 
            this.toolBarToolStripMenuItem.Name = "toolBarToolStripMenuItem";
            this.toolBarToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // statusBarToolStripMenuItem
            // 
            this.statusBarToolStripMenuItem.Name = "statusBarToolStripMenuItem";
            this.statusBarToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // toolsMenu
            // 
            this.toolsMenu.Name = "toolsMenu";
            this.toolsMenu.Size = new System.Drawing.Size(32, 19);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // windowsMenu
            // 
            this.windowsMenu.Name = "windowsMenu";
            this.windowsMenu.Size = new System.Drawing.Size(32, 19);
            // 
            // newWindowToolStripMenuItem
            // 
            this.newWindowToolStripMenuItem.Name = "newWindowToolStripMenuItem";
            this.newWindowToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // cascadeToolStripMenuItem
            // 
            this.cascadeToolStripMenuItem.Name = "cascadeToolStripMenuItem";
            this.cascadeToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // tileVerticalToolStripMenuItem
            // 
            this.tileVerticalToolStripMenuItem.Name = "tileVerticalToolStripMenuItem";
            this.tileVerticalToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // tileHorizontalToolStripMenuItem
            // 
            this.tileHorizontalToolStripMenuItem.Name = "tileHorizontalToolStripMenuItem";
            this.tileHorizontalToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // closeAllToolStripMenuItem
            // 
            this.closeAllToolStripMenuItem.Name = "closeAllToolStripMenuItem";
            this.closeAllToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // arrangeIconsToolStripMenuItem
            // 
            this.arrangeIconsToolStripMenuItem.Name = "arrangeIconsToolStripMenuItem";
            this.arrangeIconsToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // helpMenu
            // 
            this.helpMenu.Name = "helpMenu";
            this.helpMenu.Size = new System.Drawing.Size(32, 19);
            // 
            // contentsToolStripMenuItem
            // 
            this.contentsToolStripMenuItem.Name = "contentsToolStripMenuItem";
            this.contentsToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // indexToolStripMenuItem
            // 
            this.indexToolStripMenuItem.Name = "indexToolStripMenuItem";
            this.indexToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(23, 23);
            // 
            // toolTip
            // 
            this.toolTip.BackColor = OM.ThemeColor.Body_Header_Color;
            // 
            // statusStrip
            // 
            this.statusStrip.BackColor = OM.ThemeColor.Body_Header_Color;
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSSDateTime,
            this.TSCmpName,
            this.TSUserName,
            this.TSAppVersion,
            this.TSCompName,
            this.toolStripProgressBar1});
            this.statusStrip.Location = new System.Drawing.Point(0, 508);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1024, 27);
            this.statusStrip.TabIndex = 0;
            // 
            // TSSDateTime
            // 
            this.TSSDateTime.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.TSSDateTime.ForeColor = System.Drawing.Color.White;
            this.TSSDateTime.Name = "TSSDateTime";
            this.TSSDateTime.Size = new System.Drawing.Size(4, 22);
            // 
            // TSCmpName
            // 
            this.TSCmpName.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.TSCmpName.ForeColor = System.Drawing.Color.White;
            this.TSCmpName.Name = "TSCmpName";
            this.TSCmpName.Size = new System.Drawing.Size(76, 22);
            this.TSCmpName.Text = "Powered By ";
            // 
            // TSUserName
            // 
            this.TSUserName.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.TSUserName.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TSUserName.ForeColor = System.Drawing.Color.White;
            this.TSUserName.Name = "TSUserName";
            this.TSUserName.Size = new System.Drawing.Size(113, 22);
            this.TSUserName.Text = "toolStripStatusLabel2";
            // 
            // TSAppVersion
            // 
            this.TSAppVersion.AutoSize = false;
            this.TSAppVersion.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.TSAppVersion.ForeColor = System.Drawing.Color.White;
            this.TSAppVersion.Name = "TSAppVersion";
            this.TSAppVersion.Size = new System.Drawing.Size(150, 22);
            this.TSAppVersion.Text = "toolStripStatusLabel1";
            // 
            // TSCompName
            // 
            this.TSCompName.AutoSize = false;
            this.TSCompName.ForeColor = System.Drawing.Color.White;
            this.TSCompName.Name = "TSCompName";
            this.TSCompName.Size = new System.Drawing.Size(250, 22);
            this.TSCompName.Text = "toolStripStatusLabel1";
            this.TSCompName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 21);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblCompName
            // 
            this.lblCompName.BackColor = OM.ThemeColor.Body_Header_Color;
            this.lblCompName.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblCompName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblCompName.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompName.ForeColor = System.Drawing.Color.White;
            this.lblCompName.Location = new System.Drawing.Point(0, 24);
            this.lblCompName.Name = "lblCompName";
            this.lblCompName.Size = new System.Drawing.Size(1024, 27);
            this.lblCompName.TabIndex = 3;
            this.lblCompName.Text = "label1";
            this.lblCompName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = OM.ThemeColor.Body_Header_Color;
            this.menuStrip.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.menuStrip.Font = new System.Drawing.Font("Verdana", 9F);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1024, 24);
            this.menuStrip.TabIndex = 1;
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.testToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // testToolStripMenuItem
            // 
            this.testToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.testToolStripMenuItem.Name = "testToolStripMenuItem";
            this.testToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.testToolStripMenuItem.Text = "Test";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Dot.gif");
            // 
            // pnlActivate
            // 
            this.pnlActivate.BackColor = OM.ThemeColor.Body_Header_Color;
            this.pnlActivate.Controls.Add(this.toolBar1);
            this.pnlActivate.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlActivate.Location = new System.Drawing.Point(0, 478);
            this.pnlActivate.Name = "pnlActivate";
            this.pnlActivate.Size = new System.Drawing.Size(1024, 30);
            this.pnlActivate.TabIndex = 9;
            // 
            // toolBar1
            // 
            this.toolBar1.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
            this.toolBar1.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.toolBarButton1,
            this.toolBarButton2,
            this.toolBarButton3});
            this.toolBar1.ButtonSize = new System.Drawing.Size(24, 20);
            this.toolBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolBar1.DropDownArrows = true;
            this.toolBar1.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolBar1.ImageList = this.imageList1;
            this.toolBar1.Location = new System.Drawing.Point(0, 0);
            this.toolBar1.Margin = new System.Windows.Forms.Padding(0);
            this.toolBar1.Name = "toolBar1";
            this.toolBar1.ShowToolTips = true;
            this.toolBar1.Size = new System.Drawing.Size(1024, 30);
            this.toolBar1.TabIndex = 6;
            this.toolBar1.TextAlign = System.Windows.Forms.ToolBarTextAlign.Right;
            this.toolBar1.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.toolBar1_ButtonClick);
            // 
            // toolBarButton1
            // 
            this.toolBarButton1.ImageIndex = 0;
            this.toolBarButton1.Name = "toolBarButton1";
            this.toolBarButton1.Text = "SalesEntry";
            // 
            // toolBarButton2
            // 
            this.toolBarButton2.ImageIndex = 0;
            this.toolBarButton2.Name = "toolBarButton2";
            this.toolBarButton2.Text = "Login";
            // 
            // toolBarButton3
            // 
            this.toolBarButton3.ImageIndex = 0;
            this.toolBarButton3.Name = "toolBarButton3";
            this.toolBarButton3.Text = "Exit";
            // 
            // lblMainTitle
            // 
            this.lblMainTitle.AutoSize = true;
            this.lblMainTitle.BackColor = OM.ThemeColor.Body_Header_Color;
            this.lblMainTitle.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMainTitle.ForeColor = System.Drawing.Color.White;
            this.lblMainTitle.Location = new System.Drawing.Point(7, 29);
            this.lblMainTitle.Name = "lblMainTitle";
            this.lblMainTitle.Size = new System.Drawing.Size(42, 17);
            this.lblMainTitle.TabIndex = 11;
            this.lblMainTitle.Text = "label1";
            // 
            // pnlInfo
            // 
            this.pnlInfo.Controls.Add(this.lblInfo);
            this.pnlInfo.Controls.Add(this.label1);
            this.pnlInfo.Location = new System.Drawing.Point(809, 386);
            this.pnlInfo.Name = "pnlInfo";
            this.pnlInfo.Size = new System.Drawing.Size(116, 71);
            this.pnlInfo.TabIndex = 13;
            this.pnlInfo.Visible = false;
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Location = new System.Drawing.Point(29, 36);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(0, 13);
            this.lblInfo.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.LightGray;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "HELP Details";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // pnlMainMenu
            // 
            this.pnlMainMenu.BackColor = OM.ThemeColor.Body_Back_Color;
            this.pnlMainMenu.Controls.Add(this.pnlSystemLock);
            this.pnlMainMenu.Controls.Add(this.pnlInfo);
            this.pnlMainMenu.Controls.Add(this.pnlSubMenu);
            this.pnlMainMenu.Controls.Add(this.btnSalesReturn);
            this.pnlMainMenu.Controls.Add(this.btnTopItemSales);
            this.pnlMainMenu.Controls.Add(this.btnWeeklySales);
            this.pnlMainMenu.Controls.Add(this.btnSalesRegister);
            this.pnlMainMenu.Controls.Add(this.btnPurchaseReturn);
            this.pnlMainMenu.Controls.Add(this.btnSaleSummary);
            this.pnlMainMenu.Controls.Add(this.btnTopBrandSales);
            this.pnlMainMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMainMenu.Location = new System.Drawing.Point(0, 24);
            this.pnlMainMenu.Name = "pnlMainMenu";
            this.pnlMainMenu.Size = new System.Drawing.Size(1024, 484);
            this.pnlMainMenu.TabIndex = 15;
            // 
            // pnlSystemLock
            // 
            this.pnlSystemLock.BackColor = System.Drawing.Color.Transparent;
            this.pnlSystemLock.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSystemLock.Controls.Add(this.btnRateTypeCancel);
            this.pnlSystemLock.Controls.Add(this.btnRateTypeOK);
            this.pnlSystemLock.Controls.Add(this.txtRateTypePassword);
            this.pnlSystemLock.Controls.Add(this.label40);
            this.pnlSystemLock.Controls.Add(this.label44);
            this.pnlSystemLock.Location = new System.Drawing.Point(299, 348);
            this.pnlSystemLock.Name = "pnlSystemLock";
            this.pnlSystemLock.Size = new System.Drawing.Size(409, 93);
            this.pnlSystemLock.TabIndex = 5553;
            this.pnlSystemLock.Visible = false;
            // 
            // btnRateTypeCancel
            // 
            this.btnRateTypeCancel.Location = new System.Drawing.Point(209, 57);
            this.btnRateTypeCancel.Name = "btnRateTypeCancel";
            this.btnRateTypeCancel.Size = new System.Drawing.Size(80, 32);
            this.btnRateTypeCancel.TabIndex = 709;
            this.btnRateTypeCancel.Text = "Cancel";
            this.btnRateTypeCancel.UseVisualStyleBackColor = true;
            this.btnRateTypeCancel.Click += new System.EventHandler(this.btnRateTypeCancel_Click);
            // 
            // btnRateTypeOK
            // 
            this.btnRateTypeOK.Location = new System.Drawing.Point(110, 58);
            this.btnRateTypeOK.Name = "btnRateTypeOK";
            this.btnRateTypeOK.Size = new System.Drawing.Size(75, 30);
            this.btnRateTypeOK.TabIndex = 706;
            this.btnRateTypeOK.Text = "OK";
            this.btnRateTypeOK.UseVisualStyleBackColor = true;
            this.btnRateTypeOK.Click += new System.EventHandler(this.btnRateTypeOK_Click);
            // 
            // txtRateTypePassword
            // 
            this.txtRateTypePassword.Location = new System.Drawing.Point(137, 30);
            this.txtRateTypePassword.MaxLength = 6;
            this.txtRateTypePassword.Name = "txtRateTypePassword";
            this.txtRateTypePassword.PasswordChar = '*';
            this.txtRateTypePassword.Size = new System.Drawing.Size(187, 20);
            this.txtRateTypePassword.TabIndex = 703;
            this.txtRateTypePassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRateTypePassword_KeyDown);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(34, 31);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(53, 13);
            this.label40.TabIndex = 3;
            this.label40.Text = "Password";
            // 
            // label44
            // 
            this.label44.BackColor = OM.ThemeColor.Body_Header_Color;
            this.label44.Dock = System.Windows.Forms.DockStyle.Top;
            this.label44.Location = new System.Drawing.Point(0, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(407, 21);
            this.label44.TabIndex = 0;
            this.label44.Text = "Type Password";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlSubMenu
            // 
            this.pnlSubMenu.BackColor = System.Drawing.Color.Transparent;
            this.pnlSubMenu.Controls.Add(this.btnRetailerTools);
            this.pnlSubMenu.Controls.Add(this.btnEOD);
            this.pnlSubMenu.Controls.Add(this.btnExit);
            this.pnlSubMenu.Controls.Add(this.btnPurchase);
            this.pnlSubMenu.Controls.Add(this.btnItemChangeRate);
            this.pnlSubMenu.Controls.Add(this.btnBilling);
            this.pnlSubMenu.Location = new System.Drawing.Point(63, 114);
            this.pnlSubMenu.Name = "pnlSubMenu";
            this.pnlSubMenu.Size = new System.Drawing.Size(811, 310);
            this.pnlSubMenu.TabIndex = 1;
            // 
            // btnRetailerTools
            // 
            this.btnRetailerTools.Location = new System.Drawing.Point(654, 109);
            this.btnRetailerTools.Name = "btnRetailerTools";
            this.btnRetailerTools.Size = new System.Drawing.Size(121, 30);
            this.btnRetailerTools.TabIndex = 16;
            this.btnRetailerTools.Text = "Tools";
            this.btnRetailerTools.UseVisualStyleBackColor = true;
            this.btnRetailerTools.Visible = false;
            this.btnRetailerTools.Click += new System.EventHandler(this.btnRetailerTools_Click);
            // 
            // btnEOD
            // 
            this.btnEOD.Image = global::Kirana.Properties.Resources.ClosingStock;
            this.btnEOD.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnEOD.Location = new System.Drawing.Point(613, 12);
            this.btnEOD.Name = "btnEOD";
            this.btnEOD.Size = new System.Drawing.Size(172, 91);
            this.btnEOD.TabIndex = 15;
            this.btnEOD.Text = "EOD (F12)";
            this.btnEOD.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnEOD.UseVisualStyleBackColor = true;
            this.btnEOD.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnExit
            // 
            this.btnExit.Image = global::Kirana.Properties.Resources.AppTurnOff;
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnExit.Location = new System.Drawing.Point(313, 135);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(237, 91);
            this.btnExit.TabIndex = 14;
            this.btnExit.Text = "Shut Down\r\n(F11)";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnPurchase
            // 
            this.btnPurchase.Image = global::Kirana.Properties.Resources.Sales;
            this.btnPurchase.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnPurchase.Location = new System.Drawing.Point(415, 12);
            this.btnPurchase.Name = "btnPurchase";
            this.btnPurchase.Size = new System.Drawing.Size(172, 91);
            this.btnPurchase.TabIndex = 3;
            this.btnPurchase.Text = "Purchase\r\n(F4)";
            this.btnPurchase.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnPurchase.UseVisualStyleBackColor = true;
            this.btnPurchase.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnItemChangeRate
            // 
            this.btnItemChangeRate.Image = global::Kirana.Properties.Resources.ItemRateChange;
            this.btnItemChangeRate.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnItemChangeRate.Location = new System.Drawing.Point(216, 12);
            this.btnItemChangeRate.Name = "btnItemChangeRate";
            this.btnItemChangeRate.Size = new System.Drawing.Size(172, 91);
            this.btnItemChangeRate.TabIndex = 1;
            this.btnItemChangeRate.Text = "Item Rate Change\r\n(F2)";
            this.btnItemChangeRate.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnItemChangeRate.UseVisualStyleBackColor = true;
            this.btnItemChangeRate.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnBilling
            // 
            this.btnBilling.Image = global::Kirana.Properties.Resources.Sales;
            this.btnBilling.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBilling.Location = new System.Drawing.Point(12, 12);
            this.btnBilling.Name = "btnBilling";
            this.btnBilling.Size = new System.Drawing.Size(172, 91);
            this.btnBilling.TabIndex = 0;
            this.btnBilling.Text = "Billing\r\n(F1)";
            this.btnBilling.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBilling.UseVisualStyleBackColor = true;
            this.btnBilling.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnSalesReturn
            // 
            this.btnSalesReturn.Location = new System.Drawing.Point(142, 41);
            this.btnSalesReturn.Name = "btnSalesReturn";
            this.btnSalesReturn.Size = new System.Drawing.Size(121, 91);
            this.btnSalesReturn.TabIndex = 2;
            this.btnSalesReturn.Text = "Sales Return\r\n(F3)";
            this.btnSalesReturn.UseVisualStyleBackColor = true;
            this.btnSalesReturn.Visible = false;
            this.btnSalesReturn.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnTopItemSales
            // 
            this.btnTopItemSales.Location = new System.Drawing.Point(812, 138);
            this.btnTopItemSales.Name = "btnTopItemSales";
            this.btnTopItemSales.Size = new System.Drawing.Size(121, 91);
            this.btnTopItemSales.TabIndex = 6;
            this.btnTopItemSales.Text = "Top Selling Items\r\n(F9)";
            this.btnTopItemSales.UseVisualStyleBackColor = true;
            this.btnTopItemSales.Visible = false;
            this.btnTopItemSales.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnWeeklySales
            // 
            this.btnWeeklySales.Location = new System.Drawing.Point(666, 41);
            this.btnWeeklySales.Name = "btnWeeklySales";
            this.btnWeeklySales.Size = new System.Drawing.Size(121, 91);
            this.btnWeeklySales.TabIndex = 8;
            this.btnWeeklySales.Text = "Weekly Sales Summary\r\n(F7)";
            this.btnWeeklySales.UseVisualStyleBackColor = true;
            this.btnWeeklySales.Visible = false;
            this.btnWeeklySales.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnSalesRegister
            // 
            this.btnSalesRegister.Location = new System.Drawing.Point(809, 41);
            this.btnSalesRegister.Name = "btnSalesRegister";
            this.btnSalesRegister.Size = new System.Drawing.Size(121, 91);
            this.btnSalesRegister.TabIndex = 7;
            this.btnSalesRegister.Text = "Today\'s Partywise Sales Summary\r\n(F8)";
            this.btnSalesRegister.UseVisualStyleBackColor = true;
            this.btnSalesRegister.Visible = false;
            this.btnSalesRegister.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnPurchaseReturn
            // 
            this.btnPurchaseReturn.Location = new System.Drawing.Point(269, 41);
            this.btnPurchaseReturn.Name = "btnPurchaseReturn";
            this.btnPurchaseReturn.Size = new System.Drawing.Size(121, 91);
            this.btnPurchaseReturn.TabIndex = 4;
            this.btnPurchaseReturn.Text = "Purchase Return\r\n(F5)";
            this.btnPurchaseReturn.UseVisualStyleBackColor = true;
            this.btnPurchaseReturn.Visible = false;
            this.btnPurchaseReturn.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnSaleSummary
            // 
            this.btnSaleSummary.Location = new System.Drawing.Point(538, 41);
            this.btnSaleSummary.Name = "btnSaleSummary";
            this.btnSaleSummary.Size = new System.Drawing.Size(121, 91);
            this.btnSaleSummary.TabIndex = 9;
            this.btnSaleSummary.Text = "Todays Sales Summary\r\n(F6)";
            this.btnSaleSummary.UseVisualStyleBackColor = true;
            this.btnSaleSummary.Visible = false;
            this.btnSaleSummary.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnTopBrandSales
            // 
            this.btnTopBrandSales.Location = new System.Drawing.Point(396, 41);
            this.btnTopBrandSales.Name = "btnTopBrandSales";
            this.btnTopBrandSales.Size = new System.Drawing.Size(121, 91);
            this.btnTopBrandSales.TabIndex = 5;
            this.btnTopBrandSales.Text = "Top Selling Brands\r\n(F10)";
            this.btnTopBrandSales.UseVisualStyleBackColor = true;
            this.btnTopBrandSales.Visible = false;
            this.btnTopBrandSales.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnHome
            // 
            this.btnHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnHome.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.Location = new System.Drawing.Point(841, 3);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(84, 28);
            this.btnHome.TabIndex = 17;
            this.btnHome.Text = "Home";
            this.btnHome.UseVisualStyleBackColor = true;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // lblDispTaskbar
            // 
            this.lblDispTaskbar.AutoSize = true;
            this.lblDispTaskbar.BackColor = OM.ThemeColor.Body_Header_Color;
            this.lblDispTaskbar.ForeColor = System.Drawing.Color.White;
            this.lblDispTaskbar.Location = new System.Drawing.Point(226, 8);
            this.lblDispTaskbar.Name = "lblDispTaskbar";
            this.lblDispTaskbar.Size = new System.Drawing.Size(143, 13);
            this.lblDispTaskbar.TabIndex = 19;
            this.lblDispTaskbar.Text = "Show Taskbar (Ctrl + Space)";
            // 
            // btnMinimize
            // 
            this.btnMinimize.BackgroundImage = global::Kirana.Properties.Resources.Minimize;
            this.btnMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMinimize.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinimize.Location = new System.Drawing.Point(951, 0);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(28, 28);
            this.btnMinimize.TabIndex = 14;
            this.btnMinimize.UseVisualStyleBackColor = true;
            this.btnMinimize.Click += new System.EventHandler(this.btnMinimize_Click);
            // 
            // MDIParent1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(1024, 535);
            this.ControlBox = false;
            this.Controls.Add(this.lblDispTaskbar);
            this.Controls.Add(this.btnHome);
            this.Controls.Add(this.btnMinimize);
            this.Controls.Add(this.lblMainTitle);
            this.Controls.Add(this.pnlActivate);
            this.Controls.Add(this.lblCompName);
            this.Controls.Add(this.pnlMainMenu);
            this.Controls.Add(this.menuStrip);
            this.Controls.Add(this.statusStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MDIParent1";
            this.Text = "Kirana System";
            this.TransparencyKey = System.Drawing.Color.Silver;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MDIParent1_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MDIParent1_FormClosing);
            this.Resize += new System.EventHandler(this.MDIParent1_Resize);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.pnlActivate.ResumeLayout(false);
            this.pnlActivate.PerformLayout();
            this.pnlInfo.ResumeLayout(false);
            this.pnlInfo.PerformLayout();
            this.pnlMainMenu.ResumeLayout(false);
            this.pnlSystemLock.ResumeLayout(false);
            this.pnlSystemLock.PerformLayout();
            this.pnlSubMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem printSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tileHorizontalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printPreviewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editMenu;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewMenu;
        private System.Windows.Forms.ToolStripMenuItem toolBarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statusBarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsMenu;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windowsMenu;
        private System.Windows.Forms.ToolStripMenuItem newWindowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cascadeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tileVerticalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem arrangeIconsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpMenu;
        private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indexToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel TSSDateTime;
        private System.Windows.Forms.ToolStripStatusLabel TSCmpName;
        private System.Windows.Forms.Timer timer1;
        internal System.Windows.Forms.ToolStripStatusLabel TSUserName;
        private System.Windows.Forms.Label lblCompName;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Panel pnlActivate;
        internal System.Windows.Forms.Label lblMainTitle;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel TSAppVersion;
        private System.Windows.Forms.Panel pnlInfo;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer2;
        internal System.Windows.Forms.ToolBar toolBar1;
        private System.Windows.Forms.ToolBarButton toolBarButton1;
        private System.Windows.Forms.ToolBarButton toolBarButton2;
        private System.Windows.Forms.ToolBarButton toolBarButton3;
        private System.Windows.Forms.Panel pnlMainMenu;
        private System.Windows.Forms.Panel pnlSubMenu;
        private System.Windows.Forms.Button btnSalesRegister;
        private System.Windows.Forms.Button btnTopBrandSales;
        private System.Windows.Forms.Button btnTopItemSales;
        private System.Windows.Forms.Button btnPurchase;
        private System.Windows.Forms.Button btnItemChangeRate;
        private System.Windows.Forms.Button btnBilling;
        private System.Windows.Forms.Button btnPurchaseReturn;
        private System.Windows.Forms.Button btnWeeklySales;
        private System.Windows.Forms.Button btnSaleSummary;
        private System.Windows.Forms.Button btnSalesReturn;
        private System.Windows.Forms.Button btnMinimize;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Button btnEOD;
        private System.Windows.Forms.ToolStripStatusLabel TSCompName;
        internal System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        internal System.Windows.Forms.Label lblDispTaskbar;
        private System.Windows.Forms.Button btnRetailerTools;
        private System.Windows.Forms.Panel pnlSystemLock;
        private System.Windows.Forms.Button btnRateTypeCancel;
        private System.Windows.Forms.Button btnRateTypeOK;
        private System.Windows.Forms.TextBox txtRateTypePassword;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
    }
}



