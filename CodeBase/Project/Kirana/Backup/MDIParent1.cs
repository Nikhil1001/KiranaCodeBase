﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using OM;
using JitControls;
using System.Threading;
using System.Net;
using Newtonsoft.Json;
using System.Diagnostics;

namespace Kirana
{
    /// <summary>
    /// This class used for MDI Parent
    /// </summary>
    public partial class MDIParent1 : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        int procID = 0;
        //static string DailyItm;
        bool Logout = false;
        internal static long AutoTimer, CounterTimer, CounterDate, CounterExport, CounterZip;
        string[] strNode; int cntRow = 0, cntCol = 0;
        Security secure = new Security();
        DBMUser dbUser = new DBMUser();
        DataTable dtMenu = new DataTable();
        internal static long ScheduleTime, ScheduleZipTime;
        internal static bool CloseFlag = false;
        delegate void Vch(); bool SpaceFlag = false;

        public string[] TableName = { "TVoucherEntry", "TVoucherDetails", "TStock", "TStockGodown ", "TVoucherREfDetails" ,
                             "TVoucherEntryCompany","TVoucherChqCreditDetails","TVoucherPayTypeDetails","TParkingBill","TParkingBillDetails" };

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public MDIParent1()
        {
            InitializeComponent();
            AutoTimer = 360000;
            CounterTimer = 0;
            CounterDate = 0;
            CounterExport = 0;
        }

        private void RefreshMainForm()
        {
            //toolBar1.Buttons.Clear();
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
            pnlMainMenu.Visible = true;
        }

        private void MDIParent1_Load(object sender, EventArgs e)
        {
            try
            {

                this.Text = CommonFunctions.SystemName + " System";
                dtMenu = ObjFunction.GetDataView("Select * from MMenuMaster order by SrNo").Table;
                CommonFunctions.dtHelp = ObjFunction.GetDataView("SELECT  MHelpFormDtls.FormName, MHelp.HelpInfo FROM MHelpFormDtls INNER JOIN MHelp ON MHelpFormDtls.FKHelpInfo = MHelp.PkSrNo").Table;
                ObjFunction.FormatControl(this.Controls);
                ObjFunction.KeyPressFormat(this.Controls);

                ObjFunction.GetVoucherLock();
                lblCompName.Text = CommonFunctions.CompanyName;
                CommonFunctions.mMain = this;
                //btnBilling.Enabled = false; btnItemChangeRate.Enabled = false; btnSalesReturn.Enabled = false;
                //btnPurchase.Enabled = false; btnPurchaseReturn.Enabled = false; btnEOD.Enabled = false;
                SetMenus();

                // DisableMainControl();
                toolBar1.Buttons.Clear();
                toolBar1.ButtonSize = new System.Drawing.Size(24, 24);
                pnlActivate.Visible = false; lblMainTitle.Text = "";
                string imagePath = ObjQry.ReturnString("Select LogoPath From Mcompany", CommonFunctions.ConStr);
                if (System.IO.File.Exists(imagePath) == true)
                    this.BackgroundImage = System.Drawing.Image.FromFile(imagePath);
                TSUserName.Text = "User Name:" + DBGetVal.UserName;
                TSAppVersion.Text = "Version: " + new DBAssemblyInfo().AssemblyVersion;
                if (CommonFunctions.ServerName.ToUpper().Replace("\\SQLEXPRESS", "") == System.Net.Dns.GetHostName().ToUpper())
                    TSCompName.Text = "" + System.Net.Dns.GetHostName().ToUpper() + " (Server)";
                else
                    TSCompName.Text = "" + System.Net.Dns.GetHostName().ToUpper() + " (Client)";

                btnMinimize.Top = 0;
                KeyDownFormat(this.Controls);
                if (this.FormBorderStyle == FormBorderStyle.None)
                {
                    btnMinimize.Location = new Point(this.Width - btnMinimize.Width, 0);
                }
                else
                    btnMinimize.Location = new Point(this.Width - btnMinimize.Width - 20, 0);
                btnHome.Top = 0;
                btnHome.Location = new Point(this.Width - (btnMinimize.Width + btnHome.Width + 50), 0);
                lblDispTaskbar.Location = new Point(this.Width - lblDispTaskbar.Width - 10, this.Height - lblDispTaskbar.Height - 10);

                btnMinimize.BringToFront();
                btnMinimize.Visible = true;
                TSCmpName.Text = "Powered by " + new DBAssemblyInfo().AssemblyCompany;
                ScheduleTime = (Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.AutoUploadHrs)) * 360000) + (Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.AutoUploadMins)) * 60000);
                ScheduleZipTime = (Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.ZipUploadHrs)) * 360000) + (Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.ZipUploadMins)) * 60000);
                if (Convert.ToDateTime(ObjFunction.GetAppSettings(AppSettings.O_SOD)) < DBGetVal.ServerTime.Date)
                {
                    //if (CommonFunctions.ServerName.ToUpper().Replace("\\SQLEXPRESS", "") == System.Net.Dns.GetHostName().ToUpper())
                    //{
                    Form NewF = new Master.StartDay();
                    ObjFunction.OpenForm(NewF);
                    //}
                    //else
                    //{
                    //    CloseFlag = true;
                    //    OMMessageBox.Show("SOD is not done on Server", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    //    Application.Exit();
                    //}
                }

                SetStockGroup();
                btnHome.Focus();
                CloseFlag = false;
                //AlertNotification();

                toolStripProgressBar1.Visible = false;
              //  DBTVaucherEntry dbt = new DBTVaucherEntry();
               // dbt.SaleBill_Upload();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            //TSSDateTime.Text = DateTime.Now.ToString("dd-MMM-yyyy") + "  " + DateTime.Now.ToLongTimeString();
            if (CounterDate == 0 || CounterDate >= 30)
            {
                CounterDate = 0;
                DBGetVal.ServerTime = CommonFunctions.GetTime();
            }
            CounterDate++;
            TSSDateTime.Text = DBGetVal.ServerTime.ToString("dd-MMM-yyyy") + "  " + DBGetVal.ServerTime.ToShortTimeString();
            if (CounterTimer == 0)
            {
                if (CommonFunctions.ServerName.Replace("\\SQLEXPRESS", "").ToUpper() == System.Net.Dns.GetHostName().ToUpper())
                {
                    DBAutoBackup dbAuto = new DBAutoBackup();
                    System.Threading.Thread tr = new System.Threading.Thread(dbAuto.backupOne);
                    tr.Start();
                }
            }
            if (CounterTimer == AutoTimer)
            {
                CounterTimer = 0;
                if (CommonFunctions.ServerName.Replace("\\SQLEXPRESS", "").ToUpper() == System.Net.Dns.GetHostName().ToUpper())
                {
                    DBAutoBackup dbAuto = new DBAutoBackup();
                    System.Threading.Thread tr = new System.Threading.Thread(dbAuto.backup);
                    tr.Start();
                }
            }
            CounterTimer++;
            if (CounterTimer == 360000) CounterTimer = 0;
            if (CounterExport == 0) TransferData(0);
            if (ScheduleTime > 0) { if (CounterExport >= (ScheduleTime / 1000)) { CounterExport = 0; TransferData(0); } } else CounterExport = 1;
            CounterExport++;
            if (CounterZip == 0) TransferData(1);
            if (ScheduleZipTime > 0) { if (CounterZip >= (ScheduleZipTime / 1000)) { CounterZip = 0; TransferData(1); } } else CounterZip = 1;
            CounterZip++;
        }

        private void MDIParent1_Resize(object sender, EventArgs e)
        {
            try
            {
                int WD = this.Width;
                TSUserName.Width = Convert.ToInt32(WD * 0.60);
                pnlSubMenu.Location = new Point(pnlMainMenu.Width / 2 - pnlSubMenu.Width / 2, pnlMainMenu.Height / 2 - pnlSubMenu.Height / 2);
                lblDispTaskbar.Location = new Point(this.Width - lblDispTaskbar.Width - 10, this.Height - lblDispTaskbar.Height - 10);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void MDIParent1_FormClosing(object sender, FormClosingEventArgs e1)
        {
            SetDateFormat();
            DBAssemblyInfo dbAss = new DBAssemblyInfo();
            if (CloseFlag == false)
            {
                CloseFlag = true;
                if (OMMessageBox.Show("Do You Want To Quit ? ", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                {
                    SetDateFormat();
                    Application.Exit();
                }
                else
                {
                    e1.Cancel = true;
                    CloseFlag = false;
                }
            }
            else if (CloseFlag == true && Logout == true)
            {
                Logout = false;
                if (OMMessageBox.Show("Do You Want To LogOut ? ", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Application.OpenForms["Login"].Show();
                }
                else
                {
                    e1.Cancel = true;
                    CloseFlag = false;
                }
            }
        }

        public void SetDateFormat()
        {
            try
            {
                Microsoft.Win32.RegistryKey rkey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Control Panel\International", true);
                // Set value for short Date Format
                if (rkey.GetValue("sShortDate").ToString() != "dd/MM/yyyy")
                    rkey.SetValue("sShortDate", "dd/MM/yyyy");
                // Set value for Long Date Format
                if (rkey.GetValue("sLongDate").ToString() != "dd/MM/yyyy")
                    rkey.SetValue("sLongDate", "dd/MM/yyyy");
                if (Environment.OSVersion.Version.Major >= 6)
                {
                    if (rkey.GetValue("sShortTime").ToString() != "h:mm tt")
                        rkey.SetValue("sShortTime", "h:mm tt");
                }
                if (rkey.GetValue("sTimeFormat").ToString() != "h:mm:ss tt")
                    rkey.SetValue("sTimeFormat", "h:mm:ss tt");
            }
            catch (Exception exc)
            {
                OMMessageBox.Show("Error while setting Date time format :: " + exc.Message);//For Common Error Displayed Purpose
            }
        }

        private ToolStripMenuItem DisplayMenu(ToolStripMenuItem TD, long Srno)
        {
            ToolStripMenuItem tdtemp;
            DataTable dtTree = dbUser.GetNodesByNodeID(Srno).Table;// ObjFunction.GetDataView("Select SrNo,MenuID,MenuName From MMenuMaster Where ControlMenu=" + Srno + "").Table;
            if (dtTree.Rows.Count > 0)
            {

                for (int i = 0; i < dtTree.Rows.Count; i++)
                {
                    if (dtTree.Rows[i].ItemArray[0].ToString() != "8")
                    {
                        tdtemp = new ToolStripMenuItem(dtTree.Rows[i].ItemArray[2].ToString());
                        tdtemp.Click += new EventHandler(ClickMenus);
                        tdtemp.Name = dtTree.Rows[i].ItemArray[0].ToString();
                        if (Srno == 0)
                        {
                            cntCol = 0;
                            if (cntCol < strNode[cntRow].Length)
                                if (strNode[cntRow][cntCol].ToString() == "1") tdtemp.Checked = true;
                            cntCol++;
                        }
                        else
                        {
                            if (cntCol < strNode[cntRow].Length)
                                if (strNode[cntRow][cntCol].ToString() == "1") tdtemp.Checked = true;
                            cntCol++;
                        }

                        tdtemp = DisplayMenu(tdtemp, Convert.ToInt64(dtTree.Rows[i].ItemArray[0].ToString()));
                        if (Convert.ToInt64(dtTree.Rows[i].ItemArray[0].ToString()) == 14)
                            setShortKey(tdtemp, Keys.S);
                        else if (Convert.ToInt64(dtTree.Rows[i].ItemArray[0].ToString()) == 15)
                            setShortKey(tdtemp, Keys.P);
                        if (dtTree.Rows[i].ItemArray[3].ToString() != "") setShortKey(tdtemp, dtTree.Rows[i].ItemArray[3].ToString());
                        if (Srno == 0)
                        {
                            if (tdtemp.Checked == true)
                            {
                                tdtemp.Checked = false;
                                if (tdtemp.Name == "1")
                                {
                                    //ToolStripSeparator ts = new ToolStripSeparator();
                                    //tdtemp.DropDownItems.Add(ts);
                                    //ToolStripMenuItem t = new ToolStripMenuItem("Refresh"); t.Name = "Refresh";
                                    //t.Click += new EventHandler(ClickMenus);
                                    //tdtemp.DropDownItems.Add(t);
                                    //t = new ToolStripMenuItem("Logout"); t.Name = "Logout";
                                    //t.Click += new EventHandler(ClickMenus);
                                    //tdtemp.DropDownItems.Add(t);
                                    //t = new ToolStripMenuItem("Exit"); t.Name = "Exit";
                                    //t.Click += new EventHandler(ClickMenus);
                                    //tdtemp.DropDownItems.Add(t);
                                }
                                tdtemp.ForeColor = Color.White;
                                tdtemp.MouseHover += new EventHandler(tdtemp_MouseHover);
                                tdtemp.MouseLeave += new EventHandler(tdtemp_MouseLeave);
                                // DisableMainControl(Convert.ToInt64(tdtemp.Name));
                                menuStrip.Items.Add(tdtemp);
                            } cntRow++;
                        }
                        else
                        {
                            if (tdtemp.Checked == true)
                            {
                                //DisableMainControl(Convert.ToInt64(tdtemp.Name));
                                tdtemp.Checked = false;
                                TD.DropDownItems.Add(tdtemp);
                            }
                        }
                    }
                }

            }
            return TD;
        }

        private void tdtemp_MouseLeave(object sender, EventArgs e)
        {
            ((ToolStripMenuItem)sender).ForeColor = Color.White;
            ((ToolStripMenuItem)sender).BackColor = Color.Transparent;
        }
        private void tdtemp_MouseHover(object sender, EventArgs e)
        {
            ((ToolStripMenuItem)sender).ForeColor = Color.Black;
            ((ToolStripMenuItem)sender).BackColor = Color.Yellow;
        }

        private void SetMenus()
        {
            try
            {
                DataTable dtTree = ObjFunction.GetDataView("SELECT * FROM MUserMenuMaster where FKUserId =" + DBGetVal.UserID + "").Table;
                strNode = new string[dtTree.Columns.Count - 4];
                for (int i = 0; i < dtTree.Columns.Count - 4; i++)
                {
                    strNode[i] = secure.psDecrypt(dtTree.Rows[0].ItemArray[i + 2].ToString());
                }
                menuStrip.Items.Clear();
                cntRow = 0; cntCol = 0;
                DisplayMenu(new ToolStripMenuItem(), 0);
                DisplayMainMenu();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        public void DisplayMainMenu()
        {
            ToolStripMenuItem tdtemp = new ToolStripMenuItem("Help");
            tdtemp.Click += new EventHandler(ClickMenus);
            tdtemp.Name = "Help";
            tdtemp.Checked = false;
            ToolStripMenuItem t = new ToolStripMenuItem("Refresh"); t.Name = "Refresh";
            t.Click += new EventHandler(ClickMenus);
            tdtemp.DropDownItems.Add(t);
            ToolStripSeparator ts = new ToolStripSeparator();
            tdtemp.DropDownItems.Add(ts);
            t = new ToolStripMenuItem("Logout"); t.Name = "Logout";
            t.Click += new EventHandler(ClickMenus);
            tdtemp.DropDownItems.Add(t);
            ts = new ToolStripSeparator();
            tdtemp.DropDownItems.Add(ts);
            t = new ToolStripMenuItem("Backup"); t.Name = "Backup";
            t.Click += new EventHandler(ClickMenus);
            tdtemp.DropDownItems.Add(t);
            t = new ToolStripMenuItem("Check For Update"); t.Name = "CheckForUpdate";
            t.Click += new EventHandler(ClickMenus);
            tdtemp.DropDownItems.Add(t);
            t = new ToolStripMenuItem("Exit"); t.Name = "Exit";
            t.Click += new EventHandler(ClickMenus);
            tdtemp.DropDownItems.Add(t);

            tdtemp.ForeColor = Color.Black;
            tdtemp.MouseHover += new EventHandler(tdtemp_MouseHover);
            tdtemp.MouseLeave += new EventHandler(tdtemp_MouseLeave);
            menuStrip.Items.Add(tdtemp);
        }

        private void setShortKey(ToolStripMenuItem TD, Keys keys)
        {
            TD.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | keys)));
        }

        private void setShortKey(ToolStripMenuItem TD, string strShort)
        {
            TD.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | (Keys)((int)strShort[0]))));
        }

        private void ClickMenus(object sender, EventArgs e)
        {
            try
            {
                Form childForm = new Form();
                ToolStripMenuItem TSM = (ToolStripMenuItem)sender;
                if (ObjFunction.CheckNumeric(TSM.Name) == true)
                {
                    DataRow[] dr = dtMenu.Select("SrNo=" + TSM.Name + "");
                    if (dr.Length > 0)
                    {
                        childForm = ObjFunction.GetFormObject(dr[0][4].ToString(), dr);
                        if (childForm != null)
                        {
                            pnlMainMenu.Visible = false;
                            ObjFunction.OpenForm(childForm, this);
                        }
                    }
                }
                else
                {
                    switch (TSM.Name)
                    {
                        case "Exit": this.Close();
                            break;
                        case "Backup":
                            if (CommonFunctions.ServerName.Replace("\\SQLEXPRESS", "").ToUpper() == System.Net.Dns.GetHostName().ToUpper())
                            {
                                DBAutoBackup dbAuto = new DBAutoBackup();
                                System.Threading.Thread tr = new System.Threading.Thread(dbAuto.backup);
                                tr.Start();
                            }
                            else OMMessageBox.Show("Backup allowed only server package", CommonFunctions.ConStr, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            break;
                        case "Refresh": RefreshMainForm();
                            break;
                        case "CheckForUpdate": CheckForUpdate();
                            break;
                        case "Logout": CloseFlag = true; Logout = true;
                            this.Close();
                            break;
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void RunHandHeld()
        {
            try
            {
                System.Diagnostics.Process newProc1 = null;
                newProc1 = System.Diagnostics.Process.Start(DBGetVal.DirectoryPath + "PrjPDAComm");
                procID = newProc1.Id;
                newProc1.WaitForExit();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void TeamViwer()
        {
            try
            {
                System.Diagnostics.Process newProc1 = null;
                newProc1 = System.Diagnostics.Process.Start(DBGetVal.DirectoryPath + "TeamViewerQS_en");
                procID = newProc1.Id;
                newProc1.WaitForExit();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }
        
        private void setToolBarControl()
        {
            //toolBar1.Buttons.Clear();
            int cnt = 0;
            int row = 0, MaxWidth = 0;
            bool[] arrFlag = new bool[toolBar1.Buttons.Count];

            foreach (ToolBarButton tbl in toolBar1.Buttons)
            {
                cnt = 0; if (tbl.Rectangle.Width > MaxWidth) MaxWidth = tbl.Rectangle.Width;
                foreach (Form frm in Application.OpenForms)
                {
                    if (frm.Name == tbl.Name)
                    {
                        cnt = 1;
                        break;
                    }
                }
                if (cnt == 0)
                    arrFlag[row] = true;
                else
                    arrFlag[row] = false;

                row++;
            }

            for (int i = 0; i < arrFlag.Length; i++)
            {
                if (arrFlag[i] == true)
                {
                    if (toolBar1.Buttons.Count > i)
                    {
                        toolBar1.Buttons.RemoveAt(i);
                        if (toolBar1.Buttons.Count == 0)
                        {
                            CloseFlag = false;
                            lblDispTaskbar.Visible = true;
                        }
                        else
                            lblDispTaskbar.Visible = false;
                    }
                }
            }
            if (toolBar1.Buttons.Count == 0) { pnlActivate.Visible = false; lblMainTitle.Text = ""; pnlMainMenu.Visible = true; }
            //pnlActivate.Width = MaxWidth;
            foreach (Form frm in Application.OpenForms)
            {
                //frm.Left = 0;
                //frm.Top = 0;
                //frm.Dock = DockStyle.Fill;

            }

        }

        internal void toolBar1_ButtonClick(object sender, ToolBarButtonClickEventArgs e)
        {
            try
            {
                if (pnlMainMenu.Visible == true) pnlMainMenu.Visible = false;
                foreach (Form frm in Application.OpenForms)
                {
                    if (frm.Name == e.Button.Name)
                    {
                        frm.Focus();
                        if (frm.WindowState == FormWindowState.Minimized)
                            frm.WindowState = FormWindowState.Maximized;
                        lblMainTitle.Text = e.Button.Text;
                        break;
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void SetHelpInfo()
        {
            string str = "Close Form: Esc.";
            str += "\nMenu Open: Ctrl + A";
            lblInfo.Text = str;
        }

        #region KeyDown Events
        private void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is GroupBox)
                    KeyDownFormat(ctrl.Controls);
            }
        }


        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.H && e.Alt && e.Control)
            {
                pnlInfo.Visible = true;
                pnlInfo.Dock = DockStyle.Fill;
                SetHelpInfo();
            }
            else if (e.KeyCode == Keys.X && e.Alt && e.Control)
            {
                pnlInfo.Visible = false;
            }
            else if (e.Alt && e.Control)
            {
                if (DBGetVal.UserID == 1)
                {
                    if (menuStrip.Items.Count == 7)
                        SetHiddenMenus();
                    else
                        menuStrip.Items.RemoveAt(menuStrip.Items.Count - 1);
                }
            }
            else if (e.KeyCode == Keys.H && e.Control)
            {
                About frm = new About();
                frm.ShowDialog();
            }
            else if (e.KeyCode == Keys.M && e.Control)
            {
                menuStrip.Visible = !menuStrip.Visible;
            }
            else if (e.KeyCode == Keys.F1)
                Button_Click(this.btnBilling, new EventArgs());
            else if (e.KeyCode == Keys.F2)
                Button_Click(this.btnItemChangeRate, new EventArgs());
            else if (e.KeyCode == Keys.F3)
                Button_Click(this.btnSalesReturn, new EventArgs());
            else if (e.KeyCode == Keys.F4)
                Button_Click(this.btnPurchase, new EventArgs());
            else if (e.KeyCode == Keys.F5)
                Button_Click(this.btnPurchaseReturn, new EventArgs());
            else if (e.KeyCode == Keys.F6)
                Button_Click(this.btnSaleSummary, new EventArgs());
            else if (e.KeyCode == Keys.F7)
                Button_Click(this.btnWeeklySales, new EventArgs());
            else if (e.KeyCode == Keys.F8)
                Button_Click(this.btnSalesRegister, new EventArgs());
            else if (e.KeyCode == Keys.F9)
                Button_Click(this.btnTopItemSales, new EventArgs());
            else if (e.KeyCode == Keys.F10)
                Button_Click(this.btnTopBrandSales, new EventArgs());
            else if (e.KeyCode == Keys.F11)
                btnExit_Click(sender, new EventArgs());
            else if (e.KeyCode == Keys.F12)
                Button_Click(this.btnEOD, new EventArgs());
            else if (e.KeyCode == Keys.Space && e.Control)
            {
                if (SpaceFlag == false)
                {
                    this.WindowState = FormWindowState.Normal;
                    this.Location = new Point(0, 0);
                    this.Height = Screen.PrimaryScreen.WorkingArea.Height;
                    this.Width = Screen.PrimaryScreen.WorkingArea.Width;
                    SpaceFlag = true;
                    lblDispTaskbar.Text = "Hide Taskbar (Ctrl + Space)";
                }
                else
                {
                    this.WindowState = FormWindowState.Maximized;
                    SpaceFlag = false;
                    lblDispTaskbar.Text = "Show Taskbar (Ctrl + Space)";
                }
            }
            else if (e.KeyCode == Keys.D && e.Control)
            {
                pnlSystemLock.Visible = !pnlSystemLock.Visible;
                txtRateTypePassword.Text = "";
                txtRateTypePassword.Focus();
            }
            else if (e.Control && e.Alt && e.KeyCode == Keys.C)
            {
                e.SuppressKeyPress = true;
                Form frmChild = new UI.FirmAccountDialog();
                ObjFunction.OpenForm(frmChild);
                if (((UI.FirmAccountDialog)frmChild).ReturnDataBaseName != "")
                {
                    SetDetails(((UI.FirmAccountDialog)frmChild).ReturnDataBaseName);
                }
            }
            else if (e.KeyCode == Keys.S && e.Control)
            {
                Form frmChild = new Vouchers.ShowLiveBillDetails();
                ObjFunction.OpenForm(frmChild);
            }
        }

        #endregion

        private void timer2_Tick(object sender, EventArgs e)
        {
            setToolBarControl();
        }

        #region HiddenMenu
        internal void SetHiddenMenus()
        {
            DataTable dtTree = ObjFunction.GetDataView("SELECT Hidden FROM MUserMenuMaster where FKUserId =" + DBGetVal.UserID + "").Table;
            strNode = new string[1];
            for (int i = 0; i < 1; i++)
            {
                strNode[i] = secure.psDecrypt(dtTree.Rows[0].ItemArray[i].ToString());
            }
            cntRow = 0; cntCol = 0;

            DisplayHiddenMenu(new ToolStripMenuItem(), 0);
        }



        internal ToolStripMenuItem DisplayHiddenMenu(ToolStripMenuItem TD, long Srno)
        {
            ToolStripMenuItem tdtemp;
            DataTable dtTree = dbUser.GetNodesByNodeID(Srno).Table;// ObjFunction.GetDataView("Select SrNo,MenuID,MenuName From MMenuMaster Where ControlMenu=" + Srno + "").Table;
            if (dtTree.Rows.Count > 0)
            {

                for (int i = 0; i < dtTree.Rows.Count; i++)
                {
                    if (dtTree.Rows[i].ItemArray[0].ToString() == "8")
                    {
                        tdtemp = new ToolStripMenuItem(dtTree.Rows[i].ItemArray[2].ToString());
                        tdtemp.Click += new EventHandler(ClickMenus);
                        tdtemp.Name = dtTree.Rows[i].ItemArray[0].ToString();
                        if (Srno == 0)
                        {
                            cntCol = 0;
                            if (cntCol < strNode[cntRow].Length)
                                if (strNode[cntRow][cntCol].ToString() == "1") tdtemp.Checked = true;
                            cntCol++;
                        }
                        else
                        {
                            if (cntCol < strNode[cntRow].Length)
                                if (strNode[cntRow][cntCol].ToString() == "1") tdtemp.Checked = true;
                            cntCol++;
                        }

                        tdtemp = DisplayHiddenMenu(tdtemp, Convert.ToInt64(dtTree.Rows[i].ItemArray[0].ToString()));

                        if (dtTree.Rows[i].ItemArray[3].ToString() != "") setShortKey(tdtemp, dtTree.Rows[i].ItemArray[3].ToString());

                        if (Srno == 0)
                        {
                            if (tdtemp.Checked == true)
                            {
                                tdtemp.Checked = false;
                                tdtemp.ForeColor = Color.White;
                                tdtemp.MouseHover += new EventHandler(tdtemp_MouseHover);
                                tdtemp.MouseLeave += new EventHandler(tdtemp_MouseLeave);
                                menuStrip.Items.Add(tdtemp);
                            } cntRow++;
                        }
                        else
                        {
                            if (tdtemp.Checked == true)
                            {
                                tdtemp.Checked = false;
                                TD.DropDownItems.Add(tdtemp);
                            }
                        }
                    }
                }

            }
            return TD;
        }
        #endregion

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Button_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                if (btn.Enabled == false) return;
                pnlMainMenu.Visible = false;
                if (btn.Name == "btnBilling")
                {
                    if (ObjFunction.CheckAllowMenu(41) == false) return;
                    Form frmChild = new Vouchers.SalesAE();
                    ObjFunction.OpenForm(frmChild, this);
                }
                else if (btn.Name == "btnItemChangeRate")
                {
                    if (ObjFunction.CheckAllowMenu(115) == false) return;
                    Form frmChild = new Utilities.ItemRateChange();
                    ObjFunction.OpenForm(frmChild, this);
                }
                else if (btn.Name == "btnSalesReturn")
                {
                    if (ObjFunction.CheckAllowMenu(151) == false) return;
                    Form frmChild = new Vouchers.SalesReturnAE();
                    ObjFunction.OpenForm(frmChild, this);
                    //OMMessageBox.Show("!!!!!  Work Processing  !!!!!", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information); 
                }
                else if (btn.Name == "btnPurchase")
                {
                    if (ObjFunction.CheckAllowMenu(43) == false) return;
                    Form frmChild = new Vouchers.PurchaseAE();
                    ObjFunction.OpenForm(frmChild, this);
                }
                else if (btn.Name == "btnPurchaseReturn")
                {
                    if (ObjFunction.CheckAllowMenu(44) == false) return;
                    Form frmChild = new Vouchers.PurchaseReturnAE();
                    ObjFunction.OpenForm(frmChild, this);
                }
                else if (btn.Name == "btnTopItemSales")
                {
                    //    string[] ReportSession;
                    //    ReportSession = new string[4];
                    //    ReportSession[0] = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                    //    ReportSession[1] = ObjFunction.GetAppSettings(AppSettings.O_TopSalesValue).ToString();
                    //    ReportSession[2] = "0";
                    //    ReportSession[3] = DBGetVal.CompanyNo.ToString();


                    string[] ReportSession;
                    ReportSession = new string[24];
                    ReportSession[0] = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                    ReportSession[1] = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                    ReportSession[2] = ObjFunction.GetAppSettings(AppSettings.O_TopSalesValue).ToString();
                    ReportSession[3] = "0";
                    ReportSession[4] = DBGetVal.CompanyNo.ToString();
                    ReportSession[5] = "1";
                    ReportSession[6] = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                    ReportSession[7] = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                    ReportSession[8] = ObjFunction.GetAppSettings(AppSettings.O_TopSalesValue).ToString();
                    ReportSession[9] = "0";
                    ReportSession[10] = DBGetVal.CompanyNo.ToString();
                    ReportSession[11] = "1";
                    ReportSession[12] = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                    ReportSession[13] = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                    ReportSession[14] = "0";//ObjFunction.GetAppSettings(AppSettings.O_TopSalesValue).ToString();
                    ReportSession[15] = "0";
                    ReportSession[16] = DBGetVal.CompanyNo.ToString();
                    ReportSession[17] = "2";
                    ReportSession[18] = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                    ReportSession[19] = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                    ReportSession[20] = "0";//ObjFunction.GetAppSettings(AppSettings.O_TopSalesValue).ToString();
                    ReportSession[21] = "0";
                    ReportSession[22] = DBGetVal.CompanyNo.ToString();
                    ReportSession[23] = "2";



                    //if (OMMessageBox.Show("Do you want to Preview the report", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                    //{
                    Form NewF = null;
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                        NewF = new Display.ReportViewSource(new Reports.RptTopBrandwiseItemwiseValue(), ReportSession);
                    else
                        NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("RptTopBrandwiseItemwiseValue.rpt", CommonFunctions.ReportPath), ReportSession);
                    ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                    //}
                    //else
                    //{
                    //    CrystalDecisions.CrystalReports.Engine.ReportDocument childForm;
                    //    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                    //        childForm = ObjFunction.GetReportObject("Reports.RptTopSales");
                    //    else
                    //        childForm = ObjFunction.LoadReportObject("RptTopSales.rpt");

                    //    DBReportGenerate objRpt = new DBReportGenerate(childForm, ReportSession);
                    //    objRpt.PrintReport();
                    //}
                }
                else if (btn.Name == "btnTopBrandSales")
                {

                    string[] ReportSession;
                    ReportSession = new string[24];
                    ReportSession[0] = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                    ReportSession[1] = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                    ReportSession[2] = ObjFunction.GetAppSettings(AppSettings.O_TopSalesValue).ToString();
                    ReportSession[3] = "1";
                    ReportSession[4] = DBGetVal.CompanyNo.ToString();
                    ReportSession[5] = "1";
                    ReportSession[6] = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                    ReportSession[7] = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                    ReportSession[8] = ObjFunction.GetAppSettings(AppSettings.O_TopSalesValue).ToString();
                    ReportSession[9] = "1";
                    ReportSession[10] = DBGetVal.CompanyNo.ToString();
                    ReportSession[11] = "1";
                    ReportSession[12] = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                    ReportSession[13] = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                    ReportSession[14] = "0";//ObjFunction.GetAppSettings(AppSettings.O_TopSalesValue).ToString();
                    ReportSession[15] = "1";
                    ReportSession[16] = DBGetVal.CompanyNo.ToString();
                    ReportSession[17] = "2";
                    ReportSession[18] = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                    ReportSession[19] = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                    ReportSession[20] = "0";//ObjFunction.GetAppSettings(AppSettings.O_TopSalesValue).ToString();
                    ReportSession[21] = "1";
                    ReportSession[22] = DBGetVal.CompanyNo.ToString();
                    ReportSession[23] = "2";



                    //if (OMMessageBox.Show("Do you want to Preview the report", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                    //{
                    Form NewF = null;
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                        NewF = new Display.ReportViewSource(new Reports.RptTopBrandwiseItemwiseValue(), ReportSession);
                    else
                        NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("RptTopBrandwiseItemwiseValue.rpt", CommonFunctions.ReportPath), ReportSession);
                    ObjFunction.OpenForm(NewF, DBGetVal.MainForm);

                    //string[] ReportSession;
                    //ReportSession = new string[4];
                    //ReportSession[0] = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                    //ReportSession[1] = ObjFunction.GetAppSettings(AppSettings.O_TopSalesValue).ToString();
                    //ReportSession[2] = "1";
                    //ReportSession[3] = DBGetVal.CompanyNo.ToString();

                    ////if (OMMessageBox.Show("Do you want to Preview the report", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                    ////{

                    //Form NewF = null;
                    //if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                    //    NewF = new Display.ReportViewSource(new Reports.RptTopSales(), ReportSession);
                    //else
                    //    NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("RptTopSales.rpt", CommonFunctions.ReportPath), ReportSession);
                    //ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                    ////}
                    ////else
                    ////{
                    ////    CrystalDecisions.CrystalReports.Engine.ReportDocument childForm;
                    ////    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                    ////        childForm = ObjFunction.GetReportObject("Reports.RptTopSales");
                    ////    else
                    ////        childForm = ObjFunction.LoadReportObject("RptTopSales.rpt");

                    ////    DBReportGenerate objRpt = new DBReportGenerate(childForm, ReportSession);
                    ////    objRpt.PrintReport();
                    ////}
                }
                else if (btn.Name == "btnSalesRegister")
                {
                    string[] ReportSession;
                    Form NewF = null;

                    ReportSession = new string[6];
                    ReportSession[0] = VchType.Sales.ToString();
                    ReportSession[1] = DBGetVal.CompanyNo.ToString();
                    ReportSession[2] = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                    ReportSession[3] = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                    ReportSession[4] = "1";
                    ReportSession[5] = "0";

                    //if (OMMessageBox.Show("Do you want to Preview the report", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                    //{
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                        NewF = new Display.ReportViewSource(new Reports.RPTSalesRegister(), ReportSession);
                    else
                        NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("RPTSalesRegister.rpt", CommonFunctions.ReportPath), ReportSession);
                    ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                    //}
                    //else
                    //{
                    //    CrystalDecisions.CrystalReports.Engine.ReportDocument childForm;
                    //    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                    //        childForm = ObjFunction.GetReportObject("Reports.RPTSalesRegister");
                    //    else
                    //        childForm = ObjFunction.LoadReportObject("RPTSalesRegister.rpt");

                    //    DBReportGenerate objRpt = new DBReportGenerate(childForm, ReportSession);
                    //    objRpt.PrintReport();
                    //}
                }
                else if (btn.Name == "btnSaleSummary")
                {
                    string[] ReportSession;
                    Form NewF = null;

                    ReportSession = new string[7];
                    ReportSession[0] = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                    ReportSession[1] = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                    ReportSession[2] = VchType.Sales.ToString();
                    ReportSession[3] = "0";
                    ReportSession[4] = DBGetVal.CompanyNo.ToString();
                    ReportSession[5] = ObjQry.ReturnLong("Select Count(*) From TVoucherEntry Where VoucherTypeCode=" + VchType.Sales + " AND VoucherDate='" + DBGetVal.ServerTime.ToString(Format.DDMMMYYYY) + "' ANd IsCancel='false'", CommonFunctions.ConStr).ToString();
                    ReportSession[6] = ObjQry.ReturnLong("Select Count(*) From TVoucherEntry Where VoucherTypeCode=" + VchType.RejectionIn + " AND VoucherDate='" + DBGetVal.ServerTime.ToString(Format.DDMMMYYYY) + "' ANd IsCancel='false'", CommonFunctions.ConStr).ToString();
                    //if (OMMessageBox.Show("Do you want to Preview the report", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                    //{
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                        NewF = new Display.ReportViewSource(new Reports.RPTSalesSummary(), ReportSession);
                    else
                        NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("RPTSalesSummary.rpt", CommonFunctions.ReportPath), ReportSession);
                    ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                    //}
                    //else
                    //{
                    //    CrystalDecisions.CrystalReports.Engine.ReportDocument childForm;
                    //    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                    //        childForm = ObjFunction.GetReportObject("Reports.RPTSalesSummary");
                    //    else
                    //        childForm = ObjFunction.LoadReportObject("RPTSalesSummary.rpt");

                    //    DBReportGenerate objRpt = new DBReportGenerate(childForm, ReportSession);
                    //    objRpt.PrintReport();
                    //}
                }
                else if (btn.Name == "btnWeeklySales")
                {
                    string[] ReportSession;
                    Form NewF = null;

                    ReportSession = new string[7];
                    ReportSession[0] = ObjFunction.GetStartWeekDays(DBGetVal.ServerTime).ToString("dd-MMM-yyyy");
                    ReportSession[1] = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                    ReportSession[2] = VchType.Sales.ToString();
                    ReportSession[3] = "0";
                    ReportSession[4] = DBGetVal.CompanyNo.ToString();
                    ReportSession[5] = ObjQry.ReturnLong("Select Count(*) From TVoucherEntry Where VoucherTypeCode=" + VchType.Sales + " AND VoucherDate>='" + ObjFunction.GetStartWeekDays(DBGetVal.ServerTime).ToString("dd-MMM-yyyy") + "' AND VoucherDate<='" + DBGetVal.ServerTime.ToString(Format.DDMMMYYYY) + "' ANd IsCancel='false'", CommonFunctions.ConStr).ToString();
                    ReportSession[6] = ObjQry.ReturnLong("Select Count(*) From TVoucherEntry Where VoucherTypeCode=" + VchType.RejectionIn + " AND VoucherDate>='" + ObjFunction.GetStartWeekDays(DBGetVal.ServerTime).ToString("dd-MMM-yyyy") + "' AND VoucherDate<='" + DBGetVal.ServerTime.ToString(Format.DDMMMYYYY) + "' ANd IsCancel='false'", CommonFunctions.ConStr).ToString();
                    //if (OMMessageBox.Show("Do you want to Preview the report", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                    //{
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                        NewF = new Display.ReportViewSource(new Reports.RPTSalesSummary(), ReportSession);
                    else
                        NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("RPTSalesSummary.rpt", CommonFunctions.ReportPath), ReportSession);
                    ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                    //}
                    //else
                    //{
                    //    CrystalDecisions.CrystalReports.Engine.ReportDocument childForm;
                    //    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                    //        childForm = ObjFunction.GetReportObject("Reports.RPTSalesSummary");
                    //    else
                    //        childForm = ObjFunction.LoadReportObject("RPTSalesSummary.rpt");

                    //    DBReportGenerate objRpt = new DBReportGenerate(childForm, ReportSession);
                    //    objRpt.PrintReport();
                    //}
                }
                else if (btn.Name == "btnEOD")
                {
                    if (ObjFunction.CheckAllowMenu(157) == false) return;
                    Form frmChild = new Master.EndDay();
                    ObjFunction.OpenForm(frmChild, this);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {

            DBAssemblyInfo dbAss = new DBAssemblyInfo();
            if (CloseFlag == false)
            {
                CloseFlag = true;
                if (OMMessageBox.Show("Do You Want To Quit ? ", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //DBAutoBackup dbAuto = new DBAutoBackup();
                    //dbAuto.ExportBackUp();
                    Application.Exit();
                }
                else
                {

                    CloseFlag = false;
                }
            }
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.WindowState = FormWindowState.Minimized;
            }
            pnlMainMenu.Visible = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Form NewForm = new Utilities.StockUpdateUtilities();
            //ObjFunction.OpenForm(NewForm, this);
        }

        #region Upload Data
        private void TransferData(int ZipType)
        {
            timer1.Enabled = false;//Deepak
            try
            {
                bool flag = true;
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.AutoUploadData)) == true)
                {
                    if (System.IO.File.Exists(Application.StartupPath + "\\RetailerAuto.exe") == true)
                    {
                        foreach (System.Diagnostics.Process clsProcess in System.Diagnostics.Process.GetProcesses())
                        {
                            if (clsProcess.ProcessName.ToString() == "RetailerAuto")
                            {
                                flag = false;
                            }
                        }
                        if (flag == true)
                        {
                            try
                            {
                                System.Diagnostics.ProcessStartInfo pr = new System.Diagnostics.ProcessStartInfo(Application.StartupPath + "\\RetailerAuto.exe", CommonFunctions.ConStr.Replace(" ", "") + " " + CommonFunctions.ConStrServer.Replace(" ", "") + " " + ZipType);
                                pr.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                System.Diagnostics.Process newProc1 = null;
                                newProc1 = System.Diagnostics.Process.Start(pr);
                            }
                            catch (Exception e)
                            {
                                System.IO.StreamWriter sw = new System.IO.StreamWriter(Application.StartupPath + "\\Log.txt", true);
                                sw.WriteLine(e.Message);
                                sw.Close();
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                CommonFunctions.ErrorMessge = e.Message;
            }
            timer1.Enabled = true;//Deepak
            //Thread thread = new Thread(new ThreadStart(run));
            //thread.IsBackground = true;
            //thread.Start();
        }

        #endregion

        private void SetStockGroup()
        {
            try
            {
                if (ObjQry.ReturnInteger("Select ControlGroup FRom MStockGroup Where StockGroupNo=8", CommonFunctions.ConStr) == 1)
                {
                    ObjTrans.Execute("Update MStockGroup set ControlGroup=2 , ControlSubGroup=10 Where StockGroupNo=8 ", CommonFunctions.ConStr);
                    ObjTrans.Execute("Update MStockGroup set ControlGroup=3 Where StockGroupNo=9", CommonFunctions.ConStr);
                    ObjTrans.Execute("Update MStockGroup set ControlGroup=4 Where StockGroupNo=10", CommonFunctions.ConStr);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void DisableMainControl(long MenuNo)
        {
            if (MenuNo == 41)
                btnBilling.Enabled = true;
            else if (MenuNo == 115)
                btnItemChangeRate.Enabled = true;
            else if (MenuNo == 151)
                btnSalesReturn.Enabled = true;
            else if (MenuNo == 3)
                btnPurchase.Enabled = true;
            else if (MenuNo == 44)
                btnPurchaseReturn.Enabled = true;
            else if (MenuNo == 157)
                btnEOD.Enabled = true;
        }

        private void btnRetailerTools_Click(object sender, EventArgs e)
        {
            try
            {
                string SQLPath = Application.StartupPath + "\\Tools\\RetailerTools.exe";
                System.Diagnostics.ProcessStartInfo pr = new System.Diagnostics.ProcessStartInfo(SQLPath);
                pr.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal;
                pr.UseShellExecute = true;
                pr.Verb = "runas";
                pr.Arguments = "/user:Administrator cmd /K " + SQLPath;
                System.Diagnostics.Process newProc1 = null;
                newProc1 = System.Diagnostics.Process.Start(pr);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay("Kirana Tools system doesn't exist..." + Environment.NewLine + exc.Message);
            }
        }

        public void GetCreateSqlFile()
        {
            toolStripProgressBar1.Visible = true;
            toolStripProgressBar1.Value = 0;
            toolStripProgressBar1.Maximum = TableName.Length - 1;


            Application.DoEvents();
            StringBuilder sb = new StringBuilder();
            string path = Convert.ToString(ObjFunction.GetAppSettings(AppSettings.O_UpDownLoadPath)) + "\\SqlData.txt";
            for (int i = 0; i < TableName.Length; i++)
            {
                toolStripProgressBar1.Value = i;
                Application.DoEvents();
                sb.Append(GetSelectQuery(TableName[i].ToString()));
            }

            if (sb.ToString().Trim() != "")
            {
                try
                {
                    System.IO.StreamWriter sw = System.IO.File.CreateText(path);
                    sw.Write(sb.ToString());
                    sw.Close();
                    ObjTrans.Execute("Update MSettings Set SettingValue='True' Where PkSettingNo=" + (int)AppSettings.O_SystemLock + "", CommonFunctions.ConStr);
                    ObjTrans.Execute("Delete From  TParkingBill Delete From TParkingBillDetails Delete From TVoucherPayTypeDetails Delete From TVoucherChqCreditDetails Delete From TVoucherEntryCompany  Delete From TVoucherREfDetails Delete From TStockGodown Delete From TItemLevelDiscountDetails Delete From TFooterDiscountDetails Delete From TStock  Delete From TVoucherDetails Delete From TVoucherEntry", CommonFunctions.ConStr);
                    ObjFunction.SetAppSettings();
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    toolStripProgressBar1.Visible = false;
                }
            }
        }

        public void RestoreSqlFile()
        {
            try
            {
                Application.DoEvents();
                pnlSystemLock.Visible = false;
                string path = Convert.ToString(ObjFunction.GetAppSettings(AppSettings.O_UpDownLoadPath)) + "\\SqlData.txt"; ;
                System.IO.StreamReader sr = new System.IO.StreamReader(path);
                string strData = sr.ReadToEnd();
                sr.Close();

                if (ObjTrans.Execute(strData.Replace("\n\r", " ").Replace("\n", " ").Replace("GO", " "), CommonFunctions.ConStr))
                {
                    System.IO.File.Delete(path);
                    ObjTrans.Execute("Update MSettings Set SettingValue='False' Where PkSettingNo=" + (int)AppSettings.O_SystemLock + "", CommonFunctions.ConStr);
                    ObjFunction.SetAppSettings();
                }
            }
            catch (Exception ex)
            {
                ObjFunction.ExceptionDisplay(ex.Message);
            }

        }

        private void btnRateTypeCancel_Click(object sender, EventArgs e)
        {
            txtRateTypePassword.Text = "";
            pnlSystemLock.Visible = false;
        }

        private void btnRateTypeOK_Click(object sender, EventArgs e)
        {

            if (txtRateTypePassword.Text.Trim() == "123")
            {

                Display.WorkInProgress wip = new Kirana.Display.WorkInProgress();
                wip.StrStatus = "Work in process, please wait.";
                wip.bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
                wip.bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                ObjFunction.OpenForm(wip);

            }
            txtRateTypePassword.Text = "";
        }

        private void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            pnlSystemLock.Visible = false;
        }
        
        private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            string sql = "Exec DeleteAllStock '" + DBGetVal.ServerTime.Date + "'";
            ObjTrans.Execute(sql, CommonFunctions.ConStr);
        }

        private void txtRateTypePassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnRateTypeOK_Click(sender, new EventArgs());
            }
            else if (e.KeyCode == Keys.Escape)
            {
                pnlSystemLock.Visible = false;
            }
        }

        public string GetSelectQuery(string StrTableName)
        {
            string strData = "";
            string strMain = "";
            strMain = "\n\rGO \n\r SET IDENTITY_INSERT [dbo].[" + StrTableName + "] ON \n\r";

            DataTable dtData = ObjFunction.GetDataView(ObjQry.ReturnString("Exec GetSqlBackup '" + StrTableName + "'", CommonFunctions.ConStr)).Table;
            for (int i = 0; i < dtData.Rows.Count; i++)
            {
                if (strData == "")
                    strData = dtData.Rows[i].ItemArray[0].ToString();
                else
                    strData += Environment.NewLine + dtData.Rows[i].ItemArray[0].ToString();
                //if (i != 0)
                //{
                //    if (i % 300 == 0)
                //        strData += Environment.NewLine + " Go " + Environment.NewLine;
                //}
            }

            if (strData == "")
                strMain = "";
            else
                strMain = strMain + strData + "\n\r SET IDENTITY_INSERT [dbo].[" + StrTableName + "] OFF";
            return strMain;

        }
        
        private void SetDetails(string DBName)
        {
            OMCommonClass cc = new OMCommonClass();
            CommonFunctions.DatabaseName = DBName;
            CommonFunctions.ConStr = "Data Source=" + CommonFunctions.ServerName + ";Initial Catalog=" + CommonFunctions.DatabaseName + ";User ID=OM96;Password=OM96";


            DBGetVal.DBName = ObjFunction.GetDatabaseName(CommonFunctions.ConStr);
            CommonFunctions.ConStrTools = @"Data Source=.\SQLEXPRESS;Initial Catalog=RetailerTools0001;Integrated security=true;";
            CommonFunctions.CompanyName = ObjQry.ReturnString("Select CompanyName From MCompany", CommonFunctions.ConStr);
            DBGetVal.FromDate = ObjQry.ReturnDate("Select BooksBeginFrom From MCompany", CommonFunctions.ConStr);
            DBGetVal.ToDate = ObjQry.ReturnDate("Select BooksEndOn From MCompany", CommonFunctions.ConStr);
            DBGetVal.CompanyAddress = ObjQry.ReturnString("Select AddressCode From MCompany", CommonFunctions.ConStr);
            DBGetVal.CompanyNo = ObjQry.ReturnInteger("Select CompanyNo From MCompany Order by CompanyNo desc", CommonFunctions.ConStr);
            DBGetVal.ServerTime = CommonFunctions.GetTime();

            DBGetVal.MacID = cc.GetMacName();
            DBGetVal.MachineIPAddress = Dns.GetHostByName(Dns.GetHostName().ToString()).AddressList[0];
            DBGetVal.HostName = Dns.GetHostName();

            ObjFunction.SetAppSettings();
            ObjFunction.SetReportPath();
            ObjFunction.GetTimeUserPasswords();
            string strServer = ObjQry.ReturnString("Select DatabaseServer From MServerSettings", CommonFunctions.ConStr);
            CommonFunctions.ConStrServer = (strServer != "") ? secure.psDecrypt(strServer) : "";

            cc.WriteSecurityFile(DBGetVal.ServerTime, new DBAssemblyInfo().AssemblyTitle, OMCommonClass.InstallationType.None, 0, 0, 0, false);
            if (cc.ReadSecurity(DateTime.Now, new DBAssemblyInfo().AssemblyTitle) == false)
            {
                cc.WriteLockFile(new DBAssemblyInfo().AssemblyTitle);
                OMMessageBox.Show("Software is expired. Please Contact to System Administrator.", CommonFunctions.ErrorTitle);
                Application.Exit();
            }
            if (ObjQry.ReturnLong("Select CompanyNo From MStockBarCode", CommonFunctions.ConStr) == 0)
            {
                DBGetVal.CompanyNo = ObjQry.ReturnInteger("Select CompanyNo From MCompany Order by CompanyNo desc", CommonFunctions.ConStr);
                ObjTrans.Execute("Alter Table MStockBarCode Add CompanyNo numeric(18,0);", CommonFunctions.ConStr);
                ObjTrans.Execute("Update MStockBarCode set CompanyNo =" + DBGetVal.CompanyNo + "", CommonFunctions.ConStr);
            }
            if (ObjQry.ReturnLong("Select Count( Distinct LoginDate) From MLogin", CommonFunctions.ConStr) > Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.O_LoginCount)))
            {
                string Str = "There is One of the serious problem. Please Contact to software team. Thank You ....";
                OMMessageBox.Show(Str, "Error System.", OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                Application.Exit();
            }
            ObjFunction.ExecuteScript();
            ObjFunction.CheckVersion();
        }

        private void CheckForUpdate()
        {
            try
            {
                string result = ObjFunction.getResponseJSON_MVC("/setup/LatestVersion", "GET", null).strResponseBody;
                if (result.Equals("FAILED"))
                {
                    throw new Exception("Can't fetch version details from server.");
                }
                DTOClasses.UCCheckVersion ucVersion = JsonConvert.DeserializeObject<DTOClasses.UCCheckVersion>(result);
                Version CurAppVerion = new Version(new DBAssemblyInfo().AssemblyVersion);
                Version NewAppVerion = new Version(ucVersion.appVersion);
                int verResult = NewAppVerion.CompareTo(CurAppVerion);
                if (verResult <= 0)
                {
                    throw new Exception("You are already using the latest version");
                }

                string strSetupFilePath = "D:\\JitComputer\\Setup";
                string FileName = "Kirana_V_" + ucVersion.appVersion + ".exe";
                string FilePath = strSetupFilePath + "\\" + FileName;
                if (System.IO.File.Exists(FilePath) == false)
                {
                    if (System.IO.Directory.Exists(strSetupFilePath) == false)
                        System.IO.Directory.CreateDirectory(strSetupFilePath);

                    if (OMMessageBox.Show("Are you sure you want to download and install the setup?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question, OMMessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        if (download_Setup(ucVersion.appVersion, FilePath) == true)
                        {
                            RunNewExecFile(FilePath);
                        }
                    }

                }
                else
                {
                    if (OMMessageBox.Show("Are you sure you want to install the setup?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question, OMMessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        RunNewExecFile(FilePath);
                    }
                }
            }
            catch (Exception ex)
            {
                try
                {
                    ObjFunction.SendMail("Check For Update Error", ex.StackTrace, ex.Message + ":" + ex.StackTrace, 0);
                }
                catch (Exception e)
                { 

                }

                OMMessageBox.Show(ex.Message);
            }
        }

        private void RunNewExecFile(string FilePath)
        {
            System.Threading.Thread.Sleep(2000);
            Process p1 = new Process();
            p1.StartInfo.FileName = FilePath;
            p1.Start();
        }

        private void bgWorker_DoWork_Download(object sender, DoWorkEventArgs e)
        {
            try
            {
                List<object> genericlist = e.Argument as List<object>;
                using (WebClient myWebClient = new WebClient())
                {
                    string remoteUri = @"http://www.rshopnow.com/api/setup";
                    myWebClient.Headers.Add("VersionNumber", genericlist[0].ToString());
                    if (remoteUri.StartsWith("https", StringComparison.CurrentCultureIgnoreCase))
                    {
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
                    }

                    myWebClient.DownloadFile(new Uri(remoteUri), genericlist[1].ToString());//"D:\\Dv_V_1.0.1.29.exe");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        private void bgWorker_RunWorkerCompleted_Download(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                throw new Exception(e.Error.Message + ":" + e.Error.StackTrace);
            }
        }
        
        private bool download_Setup(string AppVersion, string DescFileName)
        {
            List<object> arguments = new List<object>();
            arguments.Add(AppVersion);
            arguments.Add(DescFileName);

            Display.WorkInProgress wip = new Kirana.Display.WorkInProgress();
            wip.StrStatus = "Starting download, please wait ...";
            wip.argument = arguments;
            wip.bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork_Download);
            wip.bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted_Download);
            ObjFunction.OpenForm(wip);
            return true;
        }
    }
}
