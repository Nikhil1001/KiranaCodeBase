﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Vouchers
{
    /// <summary>
    /// This Class use for Collection Entry
    /// </summary>
    public partial class CollectionEntryAE : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        DBTVaucherEntry dbTVoucherEntry = new DBTVaucherEntry();
        TVoucherEntry tVoucherEntry = new TVoucherEntry();
        TVoucherDetails tVoucherDetails = new TVoucherDetails();
        TVoucherChqCreditDetails tVchChqCredit = new TVoucherChqCreditDetails();
        TVoucherRefDetails tVchRefDtls = new TVoucherRefDetails();
        DataTable dtSearch = new DataTable();
        DataTable dtPayTypeLedger = new DataTable();
        int rowCount;//VoucherNo
        //double drTotal, crTotal;
        long VoucherType = 0, grpNo, LedgNo = 0;
        double AdvAdjAmt = 0, TolAdvadjAmt = 0, Amt = 0;
        double OpBalPendingAmt = 0, OpBalAdjustedAmt = 0;
        bool Advflag = false;
        public string StrRBillNo = "";
        /// <summary>
        /// This is Class of Constructor
        /// </summary>
        public CollectionEntryAE()
        {
            InitializeComponent();
            this.VoucherType = OM.VchType.Sales;
        }
        /// <summary>
        /// This is Class of parameterised Constructor
        /// </summary>
        public CollectionEntryAE(long VoucherType)
        {
            InitializeComponent();
            this.VoucherType = VoucherType;
            if (VoucherType == VchType.Sales)
            {
                this.Text = "Sales Collection Entry";
                grpNo = GroupType.SundryDebtors;
                rbAdjSalesReturn.Text = "Adjust Sales Return";
            }
            else if (VoucherType == VchType.Purchase)
            {
                this.Text = "Purchase Payment Entry";
                grpNo = GroupType.SundryCreditors;
                rbAdjSalesReturn.Text = "Adj. Purchase Return";
            }
        }

        private void SalesVoucherAE_Load(object sender, EventArgs e)
        {
            try
            {
                KeyDownFormat(this.Controls);
                dtpVoucherDate.Value = DBGetVal.ServerTime;
                ObjFunction.FillCombo(cmbPartyName, "Select LedgerNo,LedgerName From MLedger Where GroupNo =" + grpNo + " order by LedgerName");
                dtPayTypeLedger = ObjFunction.GetDataView("SELECT     MPayTypeLedger.PayTypeNo, MPayTypeLedger.LedgerNo, MPayType.PayTypeName " +
                                                           " FROM MPayTypeLedger INNER JOIN MPayType ON MPayTypeLedger.PayTypeNo = MPayType.PKPayTypeNo " +
                                                            "  where MPayTypeLedger.CompanyNo=" + DBGetVal.CompanyNo + " and MPayType.PKPayTypeNo not in (1,3) order by PayTypeName").Table;


                if (VoucherType == VchType.Sales)
                    ObjFunction.FillCombo(cmbBank, "Select BankNo,BankName From MOtherBank where IsActive='true' order by BankName");
                else
                    ObjFunction.FillCombo(cmbBank, "Select LedgerNo,LedgerName From MLedger Where GroupNo=" + GroupType.BankAccounts + " And IsActive='true' order by LedgerName");

                ObjFunction.FillCombo(cmbBranch, "Select BranchNo,BranchName From MBranch  Where isActive='true' order by BranchName");


                if (VoucherType == VchType.Sales)
                {
                    ObjFunction.FillCombo(cmbCrBank, "Select BankNo,BankName From MOtherBank where IsActive='true' order by BankName");
                    ObjFunction.FillList(lstPayType, "Select PKPayTypeNo,PayTypeName from MPayType where ControlUnder not in(1,3)");
                    ObjFunction.FillComb(cmbPayType, "Select PKPayTypeNo,PayTypeName from MPayType where ControlUnder not in(1,3)");
                    //ObjFunction.FillList(lstPayType, "Select PKPayTypeNo,PayTypeName from MPayType where PayTypeNo not in(1,3)");
                    //ObjFunction.FillComb(cmbPayType, "Select PKPayTypeNo,PayTypeName from MPayType where PKPayTypeNo not in(1,3)");
                }
                else
                {
                    ObjFunction.FillCombo(cmbCrBank, "Select LedgerNo,LedgerName From MLedger Where GroupNo=" + GroupType.BankAccounts + "  And IsActive='true' order by LedgerName");
                    ObjFunction.FillList(lstPayType, "Select PKPayTypeNo,PayTypeName from MPayType where PKPayTypeNo in(2,4,5)");
                    ObjFunction.FillComb(cmbPayType, "Select PKPayTypeNo,PayTypeName from MPayType where PKPayTypeNo in(2,4,5)");
                }

                ObjFunction.FillCombo(cmbCrBranch, "Select BranchNo,BranchName From MBranch Where isActive='true' order by BranchName");

                cmbPartyName.Focus();
                //lblNetBal.Font = new Font("Verdana", 11, FontStyle.Bold);
                lblC.Font = new Font("Verdana", 11, FontStyle.Bold);
                lbld.Font = new Font("Verdana", 11, FontStyle.Bold);
                lblDrBal.Font = new Font("Verdana", 11, FontStyle.Bold);
                lblNetB.Font = new Font("Verdana", 11, FontStyle.Bold);
                lblCrNet.Font = new Font("Verdana", 11, FontStyle.Bold);
                lblN.Font = new Font("Verdana", 11, FontStyle.Bold);
                lblop.Font = new Font("Verdana", 11, FontStyle.Bold);
                lblOpBal.Font = new Font("Verdana", 11, FontStyle.Bold);
                lblCp.Font = new Font("Verdana", 11, FontStyle.Bold);
                lbldp.Font = new Font("Verdana", 11, FontStyle.Bold);
                lblDrBalp.Font = new Font("Verdana", 11, FontStyle.Bold);
                lblNetBp.Font = new Font("Verdana", 11, FontStyle.Bold);
                lblCrNetp.Font = new Font("Verdana", 11, FontStyle.Bold);
                lblNp.Font = new Font("Verdana", 11, FontStyle.Bold);
                lblopp.Font = new Font("Verdana", 11, FontStyle.Bold);
                lblOpBalp.Font = new Font("Verdana", 11, FontStyle.Bold);
                label15.Font = new Font("Verdana", 11, FontStyle.Bold);

                label5.Visible = false;


                if (VoucherType == VchType.Sales)
                {
                    label12.Visible = true;
                    cmbBranch.Visible = true;
                    label2.Visible = true;
                    cmbCrBranch.Visible = true;
                }
                else
                {
                    label12.Visible = false;
                    cmbBranch.Visible = false;
                    cmbBank.Width = 300;
                    if (cmbBranch.Items.Count > 1)
                        cmbBranch.SelectedIndex = 1;

                    label2.Visible = false;
                    cmbCrBranch.Visible = false;
                    cmbCrBank.Width = 200;
                    if (cmbCrBranch.Items.Count > 1)
                        cmbCrBranch.SelectedIndex = 1;

                }
                rbCheck_Changed();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void BindGrid()
        {
            try
            {
                if (cmbPartyName.SelectedIndex != 0)
                {
                    GridView.Rows.Clear();
                    dgBillSelection.Rows.Clear();

                    double NetBal = 0;
                    if (VoucherType == VchType.Sales)
                        pnlDetails.Visible = true;
                    else
                        pnlDetailsPur.Visible = true;
                    DataTable dt = ObjFunction.GetDataView("Exec GetCollectionDetails " + ObjFunction.GetComboValue(cmbPartyName) + "," + VoucherType + "," + DBGetVal.CompanyNo + "").Table;

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        GridView.Rows.Add();
                        dgBillSelection.Rows.Add();

                        for (int j = 0; j < dt.Columns.Count; j++)
                        {
                            GridView.Rows[i].Cells[j].Value = dt.Rows[i].ItemArray[j];
                            dgBillSelection.Rows[i].Cells[j].Value = dt.Rows[i].ItemArray[j];

                        }
                        NetBal = NetBal + Convert.ToDouble(dt.Rows[i].ItemArray[4].ToString());
                    }
                    double OpBalance = ObjQry.ReturnDouble("Select case when signCode=2 then isnull(OpeningBalance,0) else isNull(OpeningBalance,0)*-1 end From MLedger Where LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + "", CommonFunctions.ConStr);
                    //DataTable dtDetails = ObjFunction.GetDataView("SELECT isnull(SUM(TVoucherDetails.Debit),0.00) AS 'Dr Bal',isnull( SUM(TVoucherDetails.Credit),0.00) AS 'Cr Bal',isnull( ABS(MLedger.OpeningBalance),0.00) AS 'Op Bal' FROM         TVoucherDetails INNER JOIN   MLedger ON TVoucherDetails.LedgerNo = MLedger.LedgerNo  INNER JOIN TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo WHERE (TVoucherDetails.LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + ")  AND (TVoucherEntry.IsCancel = 0) GROUP BY MLedger.OpeningBalance").Table;
                    DataTable dtDetails = ObjFunction.GetDataView("SELECT isnull(SUM(TVoucherDetails.Debit),0.00) AS 'Dr Bal',isnull( SUM(TVoucherDetails.Credit),0.00) AS 'Cr Bal',case when MLedger.signCode=2 then isnull(OpeningBalance,0) else isNull(OpeningBalance,0)*-1 end AS 'Op Bal' FROM         TVoucherDetails INNER JOIN   MLedger ON TVoucherDetails.LedgerNo = MLedger.LedgerNo  INNER JOIN TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo WHERE (TVoucherDetails.LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + ")  AND (TVoucherEntry.IsCancel = 0) GROUP BY MLedger.OpeningBalance,MLedger.signCode").Table;
                    //   string str = "\r\n Op Bal : " + dtDetails.Rows[0].ItemArray[0].ToString() + "\r\n Dr Bal : " + dtDetails.Rows[0].ItemArray[0].ToString() + "\r\n Cr Bal : " + dtDetails.Rows[0].ItemArray[1].ToString() + "\r\n Net Bal : " + NetBal.ToString("0.00");
                    //  lblNetBal.Text = str;
                    lblDrBal.Text = "0.00";
                    lblDrBalp.Text = "0.00";
                    lblCrNet.Text = "0.00";
                    lblCrNetp.Text = "0.00";
                    lblOpBal.Text = "0.00";
                    lblNetB.Text = "0.00";
                    lblNetBp.Text = "0.00";
                    lblOpBal.Text = Math.Abs(OpBalance).ToString("0.00");
                    if (Convert.ToDouble(OpBalance) < 0)
                        lblOpStatus.Text = "To Receive";
                    else if (Convert.ToDouble(OpBalance) > 0)
                        lblOpStatus.Text = "To Pay";
                    else
                        lblOpStatus.Text = "";

                    lblOpBalp.Text = Math.Abs(OpBalance).ToString("0.00");
                    if (Convert.ToDouble(OpBalance) < 0)
                        lblOpStatusPur.Text = "To Receive";
                    else if (Convert.ToDouble(OpBalance) > 0)
                        lblOpStatusPur.Text = "To Pay";
                    else
                        lblOpStatusPur.Text = "";


                    if (dtDetails.Rows.Count > 0)
                    {
                        lblAmount.Text = NetBal.ToString("0.00");

                        //lblOpBal.Text = dtDetails.Rows[0].ItemArray[2].ToString();
                        lblDrBal.Text = dtDetails.Rows[0].ItemArray[0].ToString();
                        lblDrBalp.Text = dtDetails.Rows[0].ItemArray[0].ToString();
                        lblCrNet.Text = dtDetails.Rows[0].ItemArray[1].ToString();
                        lblCrNetp.Text = dtDetails.Rows[0].ItemArray[1].ToString();
                        if (VoucherType == VchType.Sales)
                        {
                            lblNetB.Text = (Convert.ToDouble(dtDetails.Rows[0].ItemArray[1].ToString()) + Convert.ToDouble(dtDetails.Rows[0].ItemArray[2].ToString()) - Convert.ToDouble(dtDetails.Rows[0].ItemArray[0].ToString())).ToString("0.00");
                            if (Convert.ToDouble(lblNetB.Text) > 0)
                                lblPayStatus.Text = "To Pay";
                            else if (Convert.ToDouble(lblNetB.Text) < 0)
                                lblPayStatus.Text = "To Receive";
                            else
                                lblPayStatus.Text = "";
                            lblNetB.Text = Math.Abs(Convert.ToDouble(lblNetB.Text)).ToString("0.00");

                        }
                        else
                        {
                            lblNetBp.Text = ((Convert.ToDouble(dtDetails.Rows[0].ItemArray[1].ToString()) + Convert.ToDouble(dtDetails.Rows[0].ItemArray[2].ToString())) - Convert.ToDouble(dtDetails.Rows[0].ItemArray[0].ToString())).ToString("0.00");
                            if (Convert.ToDouble(lblNetBp.Text) > 0)
                                lblPayStatusPur.Text = "To Pay";
                            else if (Convert.ToDouble(lblNetBp.Text) < 0)
                                lblPayStatusPur.Text = "To Receive";
                            else
                                lblPayStatusPur.Text = "";
                            lblNetBp.Text = Math.Abs(Convert.ToDouble(lblNetBp.Text)).ToString("0.00");
                        }

                    }
                    else
                    {
                        if (VoucherType == VchType.Sales)
                        {
                            lblNetB.Text = OpBalance.ToString("0.00");
                            if (Convert.ToDouble(lblNetB.Text) < 0)
                                lblPayStatus.Text = "To Receive";
                            else if (Convert.ToDouble(lblNetB.Text) > 0)
                                lblPayStatus.Text = "To Pay";
                            else
                                lblPayStatus.Text = "";
                            lblNetB.Text = Math.Abs(OpBalance).ToString("0.00");
                            //lblNetB.Text = (Convert.ToDouble(dtDetails.Rows[0].ItemArray[0].ToString()) - Convert.ToDouble(dtDetails.Rows[0].ItemArray[1].ToString()) + Convert.ToDouble(dtDetails.Rows[0].ItemArray[2].ToString())).ToString("0.00") + "  " + lblPayStatus.Text;
                        }
                        else
                        {
                            lblNetBp.Text = OpBalance.ToString("0.00");
                            if (Convert.ToDouble(lblNetBp.Text) < 0)
                                lblPayStatusPur.Text = "To Receive";
                            else if (Convert.ToDouble(lblNetBp.Text) > 0)
                                lblPayStatusPur.Text = "To Pay";
                            else
                                lblPayStatusPur.Text = "";
                            lblNetBp.Text = Math.Abs(OpBalance).ToString("0.00");
                            //lblNetBp.Text = ((Convert.ToDouble(dtDetails.Rows[0].ItemArray[1].ToString()) + Convert.ToDouble(dtDetails.Rows[0].ItemArray[2].ToString())) - Convert.ToDouble(dtDetails.Rows[0].ItemArray[0].ToString())).ToString("0.00") + "  " + lblPayStatusPur.Text; 
                        }
                    }
                    dgAdvance.Rows.Clear();
                    DataTable dtAdv = new DataTable();
                    if (VoucherType == VchType.Sales)
                    {
                        if (rbAgainstAdv.Checked == true)
                            dtAdv = ObjFunction.GetDataView("Exec GetAdvanceCollectionDetails " + ObjFunction.GetComboValue(cmbPartyName) + "," + VchType.SalesReceipt + "," + DBGetVal.CompanyNo + ",1").Table;
                        else if (rbAdjSalesReturn.Checked == true)
                            dtAdv = ObjFunction.GetDataView("Exec GetAdvanceCollectionDetails " + ObjFunction.GetComboValue(cmbPartyName) + "," + VchType.RejectionIn + "," + DBGetVal.CompanyNo + ",3").Table;

                    }
                    else if (VoucherType == VchType.Purchase)
                    {
                        if (rbAgainstAdv.Checked == true)
                            dtAdv = ObjFunction.GetDataView("Exec GetAdvanceCollectionDetails " + ObjFunction.GetComboValue(cmbPartyName) + "," + VchType.PurchasePayment + "," + DBGetVal.CompanyNo + ",1").Table;
                        else if (rbAdjSalesReturn.Checked == true)
                            dtAdv = ObjFunction.GetDataView("Exec GetAdvanceCollectionDetails " + ObjFunction.GetComboValue(cmbPartyName) + "," + VchType.RejectionOut + "," + DBGetVal.CompanyNo + ",3").Table;
                    }

                    for (int i = 0; i < dtAdv.Rows.Count; i++)
                    {
                        dgAdvance.Rows.Add();
                        for (int j = 0; j < dgAdvance.Columns.Count - 1; j++)
                        {
                            dgAdvance.Rows[i].Cells[j].Value = dtAdv.Rows[i].ItemArray[j];
                        }
                    }

                    double OpBal = 0;
                    OpBal = ObjQry.ReturnDouble("Select case when (signcode=2) then isNull(OpeningBalance,0) else isNull(OpeningBalance,0)*-1 end as OpeningBalance From MLedger Where LedgerNo =" + ObjFunction.GetComboValue(cmbPartyName) + "", CommonFunctions.ConStr);
                    double TotBal = 0;
                    //In below query we used reverse logic of signcode as this is adjsted entry's values
                    TotBal = OpBal + ObjQry.ReturnDouble("Select isNull(Sum(case when (signcode=1) then isNull(Amount,0) else isNull(Amount,0)*-1 end),0) from TVoucherRefDetails where LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " and TypeOfRef=5 AND REFNO <> 0", CommonFunctions.ConStr);
                    //In below query we used forward logic of signcode as this is direct entry's values
                    TotBal = TotBal + ObjQry.ReturnDouble("Select isNull(Sum(case when (signcode=2) then isNull(Amount,0) else isNull(Amount,0)*-1 end),0) from TVoucherRefDetails where LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " and TypeOfRef=5 AND REFNO = 0", CommonFunctions.ConStr);

                    OpBalPendingAmt = Math.Abs(TotBal);
                    OpBalAdjustedAmt = 0;
                    if (OpBal <= 0 && TotBal >= 0)
                    {
                        rbAdjustment.Enabled = false;
                    }
                    else if (OpBal >= 0 && TotBal <= 0)
                    {
                        rbAdjustment.Enabled = false;
                    }
                    else
                    {
                        rbAdjustment.Enabled = true;
                    }

                    BindGridAdv();

                    GridView.Columns[ColIndex.Amount].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    GridView.Columns[ColIndex.BillAmt].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    GridView.Columns[ColIndex.NetBal].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    GridView.Columns[ColIndex.TotRec].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;

                    dgBillSelection.Columns[ColIndex.Amount].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgBillSelection.Columns[ColIndex.BillAmt].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgBillSelection.Columns[ColIndex.NetBal].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgBillSelection.Columns[ColIndex.TotRec].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgBillSelection.Columns[ColIndex.Chk].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                    dgBill.Columns[ColIndex.Amount].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgBill.Columns[ColIndex.BillAmt].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgBill.Columns[ColIndex.NetBal].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgBill.Columns[ColIndex.TotRec].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgBill.Columns[ColIndex.Chk].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                    dgAdvance.Columns[8].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dgAdvance.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgAdvance.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgAdvance.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                }
                else
                    DataClrscr();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private bool Validations()
        {
            bool flag = false;
            try
            {
                if (GridView.Rows.Count < 0)
                {
                    OMMessageBox.Show("Sorry No Data to Save......... ", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                }
                else
                {
                    for (int i = 0; i < GridView.Rows.Count; i++)
                    {
                        if (GridView.Rows[i].Cells[5].ErrorText == "")
                        {
                            flag = true;
                        }
                        else
                        {
                            flag = false;
                            OMMessageBox.Show("Please Enter Valid Amount......... ", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            GridView.CurrentCell = GridView.Rows[i].Cells[5];
                            GridView.Focus();
                            return false;
                        }
                    }

                }

                flag = true;

                return flag;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return false;
            }
        }

        private bool ValidationChqNCreditCard()
        {
            bool flag = true;
            long ControlUnder = ObjQry.ReturnLong("Select ControlUnder From MPayType Where PKPayTypeNo=" + ObjFunction.GetComboValue(cmbPayType) + "", CommonFunctions.ConStr);
            if (ControlUnder == 4)//cmbPayType.Text == "Cheque"
            {
                if (txtChqNo.Text == "")
                {
                    flag = false;
                    OMMessageBox.Show("Enter Cheque No.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                }
                else if (ObjFunction.GetComboValue(cmbBank) == 0)
                {
                    flag = false;
                    OMMessageBox.Show("Select Bank Name.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                }
                else if (ObjFunction.GetComboValue(cmbBranch) == 0 && VoucherType == VchType.Sales)
                {
                    flag = false;
                    OMMessageBox.Show("Select Branch Name.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                }
                else
                    flag = true;
            }
            else if (ControlUnder == 5)//cmbPayType.Text == "Credit Card"
            {
                if (txtCrCardNo.Text == "")
                {
                    flag = false;
                    OMMessageBox.Show("Enter Credit Card No.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                }
                else if (ObjFunction.GetComboValue(cmbCrBank) == 0)
                {
                    flag = false;
                    OMMessageBox.Show("Select Bank Name.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                }
                else if (ObjFunction.GetComboValue(cmbCrBranch) == 0 && VoucherType == VchType.Sales)
                {
                    flag = false;
                    OMMessageBox.Show("Select Branch Name.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                }
                else
                    flag = true;
            }
            return flag;
        }

        private bool ValidationsDGBill()
        {
            bool flag = false;
            try
            {
                if (dgBillSelection.Rows.Count < 0)
                {
                    OMMessageBox.Show("Sorry No Data to Save......... ", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                }
                else
                {
                    for (int i = 0; i < dgBillSelection.Rows.Count; i++)
                    {
                        if (dgBillSelection.Rows[i].Cells[ColIndex.Amount].ErrorText == "")
                        {
                            flag = true;
                        }
                        else
                        {
                            flag = false;
                            OMMessageBox.Show("Please Enter Valid Amount......... ", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            dgBillSelection.CurrentCell = dgBillSelection.Rows[i].Cells[ColIndex.Amount];
                            dgBillSelection.Focus();
                            return false;
                        }
                    }

                }

                flag = true;

                return flag;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return false;
            }
        }

        private bool ValidationdgBillAdv()
        {
            bool flag = false, flagAdv = false; ;
            try
            {
                if (dgBill.Rows.Count <= 0)
                {
                    OMMessageBox.Show("Sorry No Data to Save......... ", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                }
                else
                {
                    for (int i = 0; i < dgBill.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(dgBill.Rows[i].Cells[ColIndex.Chk].FormattedValue) == true)
                        {
                            flagAdv = true;
                            break;
                        }

                    }

                    if (flagAdv == true)
                        flag = true;
                    else
                        flag = false;

                }
                return flag;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return false;
            }
        }

        private bool ValidationdgAdjustOpening()
        {
            bool flag = false, flagAdv = false; ;
            try
            {
                if (dgAdjustOpening.Rows.Count <= 0)
                {
                    OMMessageBox.Show("Sorry No Data to Save......... ", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                }
                else
                {
                    for (int i = 0; i < dgAdjustOpening.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(dgAdjustOpening.Rows[i].Cells[ColIndex.Chk].FormattedValue) == true)
                        {
                            flagAdv = true;
                            break;
                        }
                    }

                    if (flagAdv == true)
                        flag = true;
                    else
                        flag = false;
                }
                return flag;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return false;
            }
        }

        private void setValues()
        {
            try
            {
                if (cmbPartyName.SelectedIndex != 0)
                {
                    bool flag = false, flagAdjs = false;
                    DataGridView dg = null;
                    string StrBillNo = "";
                    StrRBillNo = "";
                    double TotAmt = 0;
                    if (Validations() == true)
                    {
                        #region rbBillwise
                        if (rbBillwise.Checked == true)
                        {
                            for (int j = 0; j < GridView.RowCount; j++)
                            {
                                if (Convert.ToDouble(GridView.Rows[j].Cells[ColIndex.Amount].Value.ToString()) != 0)
                                {
                                    long ControlUnder = ObjQry.ReturnLong("Select ControlUnder From MPayType Where PKPayTypeNo=" + Convert.ToInt64(GridView.Rows[j].Cells[ColIndex.PayTypeNo].Value.ToString()) + "", CommonFunctions.ConStr);
                                    dbTVoucherEntry = new DBTVaucherEntry();
                                    tVoucherEntry = new TVoucherEntry();
                                    tVoucherEntry.PkVoucherNo = 0;
                                    if (VoucherType == VchType.Sales)
                                        tVoucherEntry.VoucherTypeCode = VchType.SalesReceipt;
                                    else
                                        tVoucherEntry.VoucherTypeCode = VchType.PurchasePayment;
                                    tVoucherEntry.VoucherUserNo = 0;
                                    tVoucherEntry.VoucherDate = Convert.ToDateTime(dtpVoucherDate.Text);
                                    tVoucherEntry.VoucherTime = Convert.ToDateTime("01-Jan-1900");
                                    tVoucherEntry.Reference = "";

                                    tVoucherEntry.CompanyNo = DBGetVal.CompanyNo;
                                    tVoucherEntry.BilledAmount = Convert.ToDouble(GridView.Rows[j].Cells[ColIndex.Amount].Value);
                                    tVoucherEntry.ChallanNo = "";
                                    tVoucherEntry.Remark = "";
                                    if (ControlUnder == 4)
                                        tVoucherEntry.ChequeNo = Convert.ToInt64(GridView.Rows[j].Cells[ColIndex.ChqNo].Value);
                                    else
                                        tVoucherEntry.ChequeNo = 0;
                                    tVoucherEntry.ClearingDate = Convert.ToDateTime(GridView.Rows[j].Cells[ColIndex.ChqDate].Value);
                                    tVoucherEntry.Narration = "";
                                    tVoucherEntry.UserID = DBGetVal.UserID;
                                    tVoucherEntry.UserDate = DBGetVal.ServerTime.Date;
                                    tVoucherEntry.OrderType = 1;
                                    tVoucherEntry.PayTypeNo = Convert.ToInt64(GridView.Rows[j].Cells[ColIndex.PayTypeNo].Value);
                                    dbTVoucherEntry.AddTVoucherEntry(tVoucherEntry);

                                    if (StrRBillNo == "")
                                        StrRBillNo = "(" + GridView.Rows[j].Cells[ColIndex.BillNo].Value.ToString() + "," + tVoucherEntry.BilledAmount + "," + Convert.ToDateTime(GridView.Rows[j].Cells[ColIndex.BillDate].Value.ToString()).ToString("dd-MMM-yyyy") + ")";
                                    else
                                        StrRBillNo = StrRBillNo + Environment.NewLine + "(" + GridView.Rows[j].Cells[ColIndex.BillNo].Value.ToString() + "," + tVoucherEntry.BilledAmount + "," + Convert.ToDateTime(GridView.Rows[j].Cells[ColIndex.BillDate].Value.ToString()).ToString("dd-MMM-yyyy") + ")";


                                    if (VoucherType == VchType.Sales)
                                    {
                                        tVoucherDetails = new TVoucherDetails();
                                        tVoucherDetails.PkVoucherTrnNo = 0;
                                        tVoucherDetails.VoucherSrNo = 1;
                                        tVoucherDetails.SignCode = 2;
                                        tVoucherDetails.LedgerNo = ObjFunction.GetComboValue(cmbPartyName);
                                        tVoucherDetails.Debit = 0;
                                        tVoucherDetails.Credit = Convert.ToDouble(GridView.Rows[j].Cells[ColIndex.Amount].Value);
                                        tVoucherDetails.SrNo = Others.Party;
                                        tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                                        tVoucherDetails.Narration = "";
                                        dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                                    }
                                    else if (VoucherType == VchType.Purchase)
                                    {
                                        tVoucherDetails = new TVoucherDetails();
                                        tVoucherDetails.PkVoucherTrnNo = 0;
                                        tVoucherDetails.VoucherSrNo = 1;
                                        tVoucherDetails.SignCode = 1;
                                        tVoucherDetails.LedgerNo = ObjFunction.GetComboValue(cmbPartyName);
                                        tVoucherDetails.Debit = Convert.ToDouble(GridView.Rows[j].Cells[ColIndex.Amount].Value);
                                        tVoucherDetails.Credit = 0;//Convert.ToDouble(GridView.Rows[j].Cells[ColIndex.Amount].Value);
                                        tVoucherDetails.SrNo = Others.Party;
                                        tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                                        tVoucherDetails.Narration = "";
                                        dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                                    }

                                    tVchRefDtls = new TVoucherRefDetails();
                                    tVchRefDtls.PkRefTrnNo = 0;
                                    tVchRefDtls.FkVoucherSrNo = tVoucherDetails.VoucherSrNo;
                                    tVchRefDtls.LedgerNo = tVoucherDetails.LedgerNo;
                                    tVchRefDtls.TypeOfRef = 2;
                                    tVchRefDtls.RefNo = Convert.ToInt64(GridView.Rows[j].Cells[ColIndex.RefNo].Value);
                                    tVchRefDtls.DueDays = 0;
                                    tVchRefDtls.DueDate = DBGetVal.ServerTime;
                                    tVchRefDtls.Amount = tVoucherEntry.BilledAmount;
                                    if (VoucherType == VchType.Sales)
                                        tVchRefDtls.SignCode = 2;
                                    else
                                        tVchRefDtls.SignCode = 1;

                                    tVchRefDtls.UserID = DBGetVal.UserID;
                                    tVchRefDtls.UserDate = DBGetVal.ServerTime.Date;
                                    tVchRefDtls.CompanyNo = DBGetVal.CompanyNo;
                                    dbTVoucherEntry.AddTVoucherRefDetails(tVchRefDtls);

                                    if (ControlUnder == 4)//GridView.Rows[j].Cells[ColIndex.PayType].Value.ToString() == "Cheque"
                                    {
                                        tVchChqCredit.PkSrNo = 0;
                                        tVchChqCredit.ChequeNo = Convert.ToString(GridView.Rows[j].Cells[ColIndex.ChqNo].Value);
                                        tVchChqCredit.ChequeDate = Convert.ToDateTime(GridView.Rows[j].Cells[ColIndex.ChqDate].Value);
                                        tVchChqCredit.BankNo = Convert.ToInt64(GridView.Rows[j].Cells[ColIndex.BankNo].Value);
                                        tVchChqCredit.BranchNo = Convert.ToInt64(GridView.Rows[j].Cells[ColIndex.BranchNo].Value);
                                        tVchChqCredit.CreditCardNo = "";
                                        tVchChqCredit.Amount = Convert.ToDouble(GridView.Rows[j].Cells[ColIndex.Amount].Value);
                                        tVchChqCredit.PostFkVoucherNo = 0;
                                        tVchChqCredit.PostFkVoucherTrnNo = 0;
                                        tVchChqCredit.CompanyNo = DBGetVal.CompanyNo;
                                        dbTVoucherEntry.AddTVoucherChqCreditDetails(tVchChqCredit);
                                    }
                                    else if (ControlUnder == 5)//GridView.Rows[j].Cells[ColIndex.PayType].Value.ToString() == "Credit Card"
                                    {
                                        tVchChqCredit.PkSrNo = 0;
                                        tVchChqCredit.ChequeNo = "";
                                        tVchChqCredit.ChequeDate = Convert.ToDateTime(GridView.Rows[j].Cells[ColIndex.ChqDate].Value);
                                        tVchChqCredit.BankNo = Convert.ToInt64(GridView.Rows[j].Cells[ColIndex.BankNo].Value);
                                        tVchChqCredit.BranchNo = Convert.ToInt64(GridView.Rows[j].Cells[ColIndex.BranchNo].Value);
                                        tVchChqCredit.CreditCardNo = Convert.ToString(GridView.Rows[j].Cells[ColIndex.ChqNo].Value);
                                        tVchChqCredit.Amount = Convert.ToDouble(GridView.Rows[j].Cells[ColIndex.Amount].Value);
                                        tVchChqCredit.PostFkVoucherNo = 0;
                                        tVchChqCredit.PostFkVoucherTrnNo = 0;
                                        tVchChqCredit.CompanyNo = DBGetVal.CompanyNo;
                                        dbTVoucherEntry.AddTVoucherChqCreditDetails(tVchChqCredit);
                                    }

                                    if (VoucherType == VchType.Sales)
                                    {
                                        tVoucherDetails = new TVoucherDetails();
                                        tVoucherDetails.PkVoucherTrnNo = 0;
                                        tVoucherDetails.VoucherSrNo = 2;
                                        tVoucherDetails.SignCode = 1;
                                        tVoucherDetails.LedgerNo = Convert.ToInt64(GridView.Rows[j].Cells[ColIndex.LedgerNo].Value);
                                        tVoucherDetails.Debit = Convert.ToDouble(GridView.Rows[j].Cells[ColIndex.Amount].Value);
                                        tVoucherDetails.Credit = 0;
                                        tVoucherDetails.SrNo = 0;
                                        tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                                        tVoucherDetails.Narration = "";
                                        dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                                    }
                                    else if (VoucherType == VchType.Purchase)
                                    {
                                        tVoucherDetails = new TVoucherDetails();
                                        tVoucherDetails.PkVoucherTrnNo = 0;
                                        tVoucherDetails.VoucherSrNo = 2;
                                        tVoucherDetails.SignCode = 2;
                                        tVoucherDetails.LedgerNo = Convert.ToInt64(GridView.Rows[j].Cells[ColIndex.LedgerNo].Value);
                                        tVoucherDetails.Debit = 0;//Convert.ToDouble(GridView.Rows[j].Cells[ColIndex.Amount].Value);
                                        tVoucherDetails.Credit = Convert.ToDouble(GridView.Rows[j].Cells[ColIndex.Amount].Value); //0;
                                        tVoucherDetails.SrNo = 0;
                                        tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                                        tVoucherDetails.Narration = "";
                                        dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                                    }

                                    long tempId = dbTVoucherEntry.ExecuteNonQueryStatements();
                                    if (tempId != 0)
                                    {
                                        if (StrBillNo == "")
                                            StrBillNo = tempId.ToString();
                                        else
                                            StrBillNo = StrBillNo + "," + tempId.ToString();


                                        flag = true;
                                        flagAdjs = true;
                                    }
                                    else
                                    {
                                        flag = false;
                                        flagAdjs = true;
                                        break;
                                    }

                                }

                            }
                        }
                        #endregion

                        #region rbBillSelection
                        else if (rbBillSelection.Checked == true)
                        {

                            if ((OMMessageBox.Show("Do u want to Adjustment ?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question)) == DialogResult.Yes)
                            {
                                flagAdjs = true;
                                if (ValidationsDGBill() == true)
                                {
                                    bool flagetotal = true;
                                    int cnt = 0;
                                    dg = dgBillSelection;
                                    for (int i = 0; i < dgBillSelection.Rows.Count; i++)
                                    {
                                        if ((Convert.ToDouble(dgBillSelection.Rows[i].Cells[ColIndex.Amount].Value.ToString()) != 0))// && (Convert.ToBoolean(dgBillSelection.Rows[i].Cells[ColIndex.Chk].FormattedValue) == true))
                                        {
                                            TotAmt = TotAmt + Convert.ToDouble(dgBillSelection.Rows[i].Cells[ColIndex.Amount].Value.ToString());
                                        }
                                    }

                                    if (txtAmount.Text.Trim() != "")
                                    {
                                        if (TotAmt != Convert.ToDouble(txtAmount.Text))
                                        {
                                            flagetotal = false;
                                            OMMessageBox.Show("Amount does not match please check amount.......", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                                            txtAmount.Focus();
                                            return;
                                        }
                                    }
                                    if (flagetotal == true)
                                    {
                                        flagetotal = ValidationChqNCreditCard();
                                        if (flagetotal == false)
                                        {
                                            cmbPayType_KeyDown(new object(), new KeyEventArgs(Keys.Enter));
                                            return;
                                        }
                                    }
                                    if (flagetotal == true)
                                    {
                                        long ControlUnder = ObjQry.ReturnLong("Select ControlUnder From MPayType Where PKPayTypeNo=" + ObjFunction.GetComboValue(cmbPayType) + "", CommonFunctions.ConStr);
                                        dbTVoucherEntry = new DBTVaucherEntry();
                                        tVoucherEntry = new TVoucherEntry();
                                        tVoucherEntry.PkVoucherNo = 0;
                                        if (VoucherType == VchType.Sales)
                                            tVoucherEntry.VoucherTypeCode = VchType.SalesReceipt;
                                        else
                                            tVoucherEntry.VoucherTypeCode = VchType.PurchasePayment;
                                        tVoucherEntry.VoucherUserNo = 0;
                                        tVoucherEntry.VoucherDate = Convert.ToDateTime(dtpVoucherDate.Text);
                                        tVoucherEntry.VoucherTime = Convert.ToDateTime("01-Jan-1900");
                                        tVoucherEntry.Reference = "";

                                        tVoucherEntry.CompanyNo = DBGetVal.CompanyNo;
                                        tVoucherEntry.BilledAmount = TotAmt;
                                        tVoucherEntry.ChallanNo = "";
                                        tVoucherEntry.Remark = "";
                                        if (ControlUnder == 4)//cmbPayType.Text == "Cheque"
                                            tVoucherEntry.ChequeNo = Convert.ToInt64(txtChqNo.Text.Trim());
                                        else
                                            tVoucherEntry.ChequeNo = 0;
                                        tVoucherEntry.ClearingDate = Convert.ToDateTime("01-01-1900");//Convert.ToDateTime(GridView.Rows[j].Cells[ColIndex.ChqDate].Value);
                                        tVoucherEntry.Narration = "";
                                        tVoucherEntry.UserID = DBGetVal.UserID;
                                        tVoucherEntry.UserDate = DBGetVal.ServerTime.Date;
                                        tVoucherEntry.OrderType = 1;
                                        tVoucherEntry.PayTypeNo = ObjFunction.GetComboValue(cmbPayType);//Convert.ToInt64(GridView.Rows[j].Cells[ColIndex.PayTypeNo].Value);
                                        dbTVoucherEntry.AddTVoucherEntry(tVoucherEntry);

                                        if (VoucherType == VchType.Sales)
                                        {
                                            tVoucherDetails = new TVoucherDetails();
                                            tVoucherDetails.PkVoucherTrnNo = 0;
                                            tVoucherDetails.VoucherSrNo = 1;
                                            tVoucherDetails.SignCode = 2;
                                            tVoucherDetails.LedgerNo = ObjFunction.GetComboValue(cmbPartyName);
                                            tVoucherDetails.Debit = 0;
                                            tVoucherDetails.Credit = TotAmt;
                                            tVoucherDetails.SrNo = Others.Party;
                                            tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                                            tVoucherDetails.Narration = "";
                                            dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                                        }
                                        else if (VoucherType == VchType.Purchase)
                                        {
                                            tVoucherDetails = new TVoucherDetails();
                                            tVoucherDetails.PkVoucherTrnNo = 0;
                                            tVoucherDetails.VoucherSrNo = 1;
                                            tVoucherDetails.SignCode = 1;
                                            tVoucherDetails.LedgerNo = ObjFunction.GetComboValue(cmbPartyName);
                                            tVoucherDetails.Debit = TotAmt;
                                            tVoucherDetails.Credit = 0;//Convert.ToDouble(GridView.Rows[j].Cells[ColIndex.Amount].Value);
                                            tVoucherDetails.SrNo = Others.Party;
                                            tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                                            tVoucherDetails.Narration = "";
                                            dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                                        }


                                        for (int j = 0; j < dg.RowCount; j++)
                                        {
                                            if (Convert.ToDouble(dg.Rows[j].Cells[ColIndex.Amount].Value.ToString()) != 0)
                                            {
                                                cnt++;
                                                tVchRefDtls = new TVoucherRefDetails();
                                                tVchRefDtls.PkRefTrnNo = 0;
                                                tVchRefDtls.FkVoucherSrNo = tVoucherDetails.VoucherSrNo;
                                                tVchRefDtls.LedgerNo = tVoucherDetails.LedgerNo;
                                                tVchRefDtls.TypeOfRef = 2;
                                                tVchRefDtls.RefNo = Convert.ToInt64(dg.Rows[j].Cells[ColIndex.RefNo].Value);
                                                tVchRefDtls.DueDays = 0;
                                                tVchRefDtls.DueDate = DBGetVal.ServerTime;
                                                tVchRefDtls.Amount = Convert.ToDouble(dg.Rows[j].Cells[ColIndex.Amount].Value);
                                                if (VoucherType == VchType.Sales)
                                                    tVchRefDtls.SignCode = 2;
                                                else
                                                    tVchRefDtls.SignCode = 1;

                                                tVchRefDtls.UserID = DBGetVal.UserID;
                                                tVchRefDtls.UserDate = DBGetVal.ServerTime.Date;
                                                tVchRefDtls.CompanyNo = DBGetVal.CompanyNo;
                                                dbTVoucherEntry.AddTVoucherRefDetails(tVchRefDtls);

                                                if (StrRBillNo == "")
                                                    StrRBillNo = "(" + dg.Rows[j].Cells[ColIndex.BillNo].Value.ToString() + "," + tVchRefDtls.Amount + "," + Convert.ToDateTime(dg.Rows[j].Cells[ColIndex.BillDate].Value.ToString()).ToString("dd-MMM-yyyy") + ")";
                                                else
                                                    StrRBillNo = StrRBillNo + Environment.NewLine + "(" + dg.Rows[j].Cells[ColIndex.BillNo].Value.ToString() + "," + tVchRefDtls.Amount + "," + Convert.ToDateTime(dg.Rows[j].Cells[ColIndex.BillDate].Value.ToString()).ToString("dd-MMM-yyyy") + ")";

                                            }
                                        }
                                        if (ControlUnder == 4)//cmbPayType.Text == "Cheque"
                                        {
                                            tVchChqCredit.PkSrNo = 0;
                                            tVchChqCredit.ChequeNo = Convert.ToString(txtChqNo.Text.Trim());
                                            tVchChqCredit.ChequeDate = Convert.ToDateTime(dtpChqDate.Text.Trim());
                                            tVchChqCredit.BankNo = ObjFunction.GetComboValue(cmbBank);
                                            tVchChqCredit.BranchNo = ObjFunction.GetComboValue(cmbBranch);
                                            tVchChqCredit.CreditCardNo = "";
                                            tVchChqCredit.Amount = TotAmt;
                                            tVchChqCredit.PostFkVoucherNo = 0;
                                            tVchChqCredit.PostFkVoucherTrnNo = 0;
                                            tVchChqCredit.CompanyNo = DBGetVal.CompanyNo;
                                            dbTVoucherEntry.AddTVoucherChqCreditDetails(tVchChqCredit);
                                        }
                                        else if (ControlUnder == 5)//cmbPayType.Text == "Credit Card"
                                        {
                                            tVchChqCredit.PkSrNo = 0;
                                            tVchChqCredit.ChequeNo = "";
                                            tVchChqCredit.ChequeDate = Convert.ToDateTime("01-01-1900");
                                            tVchChqCredit.BankNo = ObjFunction.GetComboValue(cmbCrBank);
                                            tVchChqCredit.BranchNo = ObjFunction.GetComboValue(cmbCrBranch);
                                            tVchChqCredit.CreditCardNo = Convert.ToString(txtCrCardNo.Text.Trim());
                                            tVchChqCredit.Amount = TotAmt;
                                            tVchChqCredit.PostFkVoucherNo = 0;
                                            tVchChqCredit.PostFkVoucherTrnNo = 0;
                                            tVchChqCredit.CompanyNo = DBGetVal.CompanyNo;
                                            dbTVoucherEntry.AddTVoucherChqCreditDetails(tVchChqCredit);
                                        }
                                        if (VoucherType == VchType.Sales)
                                        {
                                            tVoucherDetails = new TVoucherDetails();
                                            tVoucherDetails.PkVoucherTrnNo = 0;
                                            tVoucherDetails.VoucherSrNo = 2;
                                            tVoucherDetails.SignCode = 1;
                                            tVoucherDetails.LedgerNo = LedgNo;
                                            tVoucherDetails.Debit = TotAmt;
                                            tVoucherDetails.Credit = 0;
                                            tVoucherDetails.SrNo = 0;
                                            tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                                            tVoucherDetails.Narration = "";
                                            dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                                        }
                                        else if (VoucherType == VchType.Purchase)
                                        {
                                            tVoucherDetails = new TVoucherDetails();
                                            tVoucherDetails.PkVoucherTrnNo = 0;
                                            tVoucherDetails.VoucherSrNo = 2;
                                            tVoucherDetails.SignCode = 2;
                                            tVoucherDetails.LedgerNo = LedgNo;
                                            tVoucherDetails.Debit = 0;//Convert.ToDouble(GridView.Rows[j].Cells[ColIndex.Amount].Value);
                                            tVoucherDetails.Credit = TotAmt;
                                            tVoucherDetails.SrNo = 0;
                                            tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                                            tVoucherDetails.Narration = "";
                                            dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                                        }
                                        if (cnt > 0)
                                        {
                                            long tempId = dbTVoucherEntry.ExecuteNonQueryStatements();
                                            if (tempId != 0)
                                            {
                                                if (StrBillNo == "")
                                                    StrBillNo = tempId.ToString();
                                                else
                                                    StrBillNo = StrBillNo + "," + tempId.ToString();

                                                flag = true;
                                                flagAdjs = true;
                                                txtAmount.Text = "";
                                            }
                                            else
                                            {
                                                flag = false;
                                                flagAdjs = true;

                                            }
                                        }
                                        else
                                        {
                                            OMMessageBox.Show("Select Atleast One Bill.......", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                                            return;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                dgBillSelection.CurrentCell = dgBillSelection[ColIndex.Amount, 0];
                                dgBillSelection.Focus();
                                flagAdjs = false;
                            }
                        }
                        #endregion

                        #region rbAdjustment
                        else if (rbAdjustment.Checked == true)
                        {
                            if (ValidationChqNCreditCard() == false)
                            {
                                cmbPayType_KeyDown(new object(), new KeyEventArgs(Keys.Enter));
                                return;
                            }

                            if (txtAdjustment.Visible && txtAdjustment.Text != "")
                            {
                                long ControlUnder = ObjQry.ReturnLong("Select ControlUnder From MPayType Where PKPayTypeNo=" + ObjFunction.GetComboValue(cmbPayType) + "", CommonFunctions.ConStr);
                                //int cnt = 0;

                                dbTVoucherEntry = new DBTVaucherEntry();
                                tVoucherEntry = new TVoucherEntry();
                                tVoucherEntry.PkVoucherNo = 0;
                                if (VoucherType == VchType.Sales)
                                    tVoucherEntry.VoucherTypeCode = VchType.SalesReceipt;
                                else
                                    tVoucherEntry.VoucherTypeCode = VchType.PurchasePayment;
                                tVoucherEntry.VoucherUserNo = 0;
                                tVoucherEntry.VoucherDate = Convert.ToDateTime(dtpVoucherDate.Text);
                                tVoucherEntry.VoucherTime = Convert.ToDateTime("01-Jan-1900");
                                tVoucherEntry.Reference = "";

                                tVoucherEntry.CompanyNo = DBGetVal.CompanyNo;
                                tVoucherEntry.BilledAmount = Convert.ToDouble(txtAdjustment.Text.Replace("-", ""));
                                tVoucherEntry.ChallanNo = "";
                                tVoucherEntry.Remark = "";
                                if (ControlUnder == 4)//cmbPayType.Text == "Cheque"
                                    tVoucherEntry.ChequeNo = Convert.ToInt64(txtChqNo.Text.Trim());
                                else
                                    tVoucherEntry.ChequeNo = 0;
                                tVoucherEntry.ClearingDate = Convert.ToDateTime("01-01-1900");//Convert.ToDateTime(GridView.Rows[j].Cells[ColIndex.ChqDate].Value);
                                tVoucherEntry.Narration = "";
                                tVoucherEntry.UserID = DBGetVal.UserID;
                                tVoucherEntry.UserDate = DBGetVal.ServerTime.Date;
                                tVoucherEntry.OrderType = 1;
                                tVoucherEntry.PayTypeNo = ObjFunction.GetComboValue(cmbPayType);//Convert.ToInt64(GridView.Rows[j].Cells[ColIndex.PayTypeNo].Value);
                                dbTVoucherEntry.AddTVoucherEntry(tVoucherEntry);

                                if (VoucherType == VchType.Sales)
                                {
                                    tVoucherDetails = new TVoucherDetails();
                                    tVoucherDetails.PkVoucherTrnNo = 0;
                                    tVoucherDetails.VoucherSrNo = 1;
                                    tVoucherDetails.SignCode = 2;
                                    tVoucherDetails.LedgerNo = ObjFunction.GetComboValue(cmbPartyName);
                                    tVoucherDetails.Debit = 0;
                                    tVoucherDetails.Credit = Convert.ToDouble(txtAdjustment.Text.Replace("-", ""));
                                    tVoucherDetails.SrNo = Others.Party;
                                    tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                                    tVoucherDetails.Narration = "";
                                    dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                                }
                                else if (VoucherType == VchType.Purchase)
                                {
                                    tVoucherDetails = new TVoucherDetails();
                                    tVoucherDetails.PkVoucherTrnNo = 0;
                                    tVoucherDetails.VoucherSrNo = 1;
                                    tVoucherDetails.SignCode = 1;
                                    tVoucherDetails.LedgerNo = ObjFunction.GetComboValue(cmbPartyName);
                                    tVoucherDetails.Debit = Convert.ToDouble(txtAdjustment.Text.Replace("-", ""));
                                    tVoucherDetails.Credit = 0;//Convert.ToDouble(GridView.Rows[j].Cells[ColIndex.Amount].Value);
                                    tVoucherDetails.SrNo = Others.Party;
                                    tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                                    tVoucherDetails.Narration = "";
                                    dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                                }

                                tVchRefDtls = new TVoucherRefDetails();
                                tVchRefDtls.PkRefTrnNo = 0;
                                tVchRefDtls.FkVoucherSrNo = tVoucherDetails.VoucherSrNo;
                                tVchRefDtls.LedgerNo = tVoucherDetails.LedgerNo;
                                tVchRefDtls.TypeOfRef = 5;
                                tVchRefDtls.RefNo = 0;
                                tVchRefDtls.DueDays = 0;
                                tVchRefDtls.DueDate = DBGetVal.ServerTime;
                                tVchRefDtls.Amount = Convert.ToDouble(txtAdjustment.Text);
                                if (VoucherType == VchType.Sales)
                                    tVchRefDtls.SignCode = 2;
                                else
                                    tVchRefDtls.SignCode = 1;

                                tVchRefDtls.UserID = DBGetVal.UserID;
                                tVchRefDtls.UserDate = DBGetVal.ServerTime.Date;
                                tVchRefDtls.CompanyNo = DBGetVal.CompanyNo;
                                dbTVoucherEntry.AddTVoucherRefDetails(tVchRefDtls);

                                if (ControlUnder == 4)//cmbPayType.Text == "Cheque"
                                {
                                    tVchChqCredit.PkSrNo = 0;
                                    tVchChqCredit.ChequeNo = Convert.ToString(txtChqNo.Text.Trim());
                                    tVchChqCredit.ChequeDate = Convert.ToDateTime(dtpChqDate.Text.Trim());
                                    tVchChqCredit.BankNo = ObjFunction.GetComboValue(cmbBank);
                                    tVchChqCredit.BranchNo = ObjFunction.GetComboValue(cmbBranch);
                                    tVchChqCredit.CreditCardNo = "";
                                    tVchChqCredit.Amount = Convert.ToDouble(txtAdjustment.Text.Replace("-", ""));
                                    tVchChqCredit.PostFkVoucherNo = 0;
                                    tVchChqCredit.PostFkVoucherTrnNo = 0;
                                    tVchChqCredit.CompanyNo = DBGetVal.CompanyNo;
                                    dbTVoucherEntry.AddTVoucherChqCreditDetails(tVchChqCredit);
                                }
                                else if (ControlUnder == 5)//cmbPayType.Text == "Credit Card"
                                {
                                    tVchChqCredit.PkSrNo = 0;
                                    tVchChqCredit.ChequeNo = "";
                                    tVchChqCredit.ChequeDate = Convert.ToDateTime("01-01-1900");
                                    tVchChqCredit.BankNo = ObjFunction.GetComboValue(cmbCrBank);
                                    tVchChqCredit.BranchNo = ObjFunction.GetComboValue(cmbCrBranch);
                                    tVchChqCredit.CreditCardNo = Convert.ToString(txtCrCardNo.Text.Trim());
                                    tVchChqCredit.Amount = Convert.ToDouble(txtAdjustment.Text.Replace("-", ""));
                                    tVchChqCredit.PostFkVoucherNo = 0;
                                    tVchChqCredit.PostFkVoucherTrnNo = 0;
                                    tVchChqCredit.CompanyNo = DBGetVal.CompanyNo;
                                    dbTVoucherEntry.AddTVoucherChqCreditDetails(tVchChqCredit);
                                }
                                if (VoucherType == VchType.Sales)
                                {
                                    tVoucherDetails = new TVoucherDetails();
                                    tVoucherDetails.PkVoucherTrnNo = 0;
                                    tVoucherDetails.VoucherSrNo = 2;
                                    tVoucherDetails.SignCode = 1;
                                    tVoucherDetails.LedgerNo = ObjQry.ReturnLong("Select LedgerNo From MPayTypeLedger Where PayTypeNo=" + ObjFunction.GetComboValue(cmbPayType) + " AND CompanyNo=" + DBGetVal.CompanyNo + "", CommonFunctions.ConStr);
                                    tVoucherDetails.Debit = Convert.ToDouble(txtAdjustment.Text.Replace("-", ""));
                                    tVoucherDetails.Credit = 0;
                                    tVoucherDetails.SrNo = 0;
                                    tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                                    tVoucherDetails.Narration = "";
                                    dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                                }
                                else if (VoucherType == VchType.Purchase)
                                {
                                    tVoucherDetails = new TVoucherDetails();
                                    tVoucherDetails.PkVoucherTrnNo = 0;
                                    tVoucherDetails.VoucherSrNo = 2;
                                    tVoucherDetails.SignCode = 2;

                                    if (ControlUnder == 2)//cmbPayType.Text == "Cash"
                                        tVoucherDetails.LedgerNo = ObjQry.ReturnLong("Select LedgerNo From MPayTypeLedger Where PayTypeNo=" + ObjFunction.GetComboValue(cmbPayType) + " AND CompanyNo=" + DBGetVal.CompanyNo + "", CommonFunctions.ConStr);
                                    else
                                        tVoucherDetails.LedgerNo = ((ControlUnder == 5) ? ObjFunction.GetComboValue(cmbCrBank) : ObjFunction.GetComboValue(cmbBank));
                                    tVoucherDetails.Debit = 0;//Convert.ToDouble(GridView.Rows[j].Cells[ColIndex.Amount].Value);
                                    tVoucherDetails.Credit = Convert.ToDouble(txtAdjustment.Text.Replace("-", ""));
                                    tVoucherDetails.SrNo = 0;
                                    tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                                    tVoucherDetails.Narration = "";
                                    dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                                }
                                long tempId = dbTVoucherEntry.ExecuteNonQueryStatements();
                                if (tempId != 0)
                                {
                                    if (StrBillNo == "")
                                        StrBillNo = tempId.ToString();
                                    else
                                        StrBillNo = StrBillNo + "," + tempId.ToString();

                                    flag = true;
                                    flagAdjs = true;
                                }
                                else
                                {
                                    flag = false;
                                    flagAdjs = true;

                                }
                            }
                            else if (dgAdjustOpening.Visible)
                            {
                                long pkBill = 0;
                                //double Amount = 0;

                                if (ValidationdgAdjustOpening() == true)
                                {
                                    dbTVoucherEntry = new DBTVaucherEntry();
                                    for (int i = 0; i < dgAdjustOpening.Rows.Count; i++)
                                    {
                                        if (Convert.ToBoolean(dgAdjustOpening.Rows[i].Cells[ColIndex.Chk].FormattedValue) == true)
                                        {
                                            pkBill = Convert.ToInt64(dgAdjustOpening.Rows[i].Cells[ColIndex.PayTypeNo].Value);
                                            DataTable dtRefBill = ObjFunction.GetDataView("SELECT FkVoucherTrnNo, FkVoucherSrNo, RefNo, CompanyNo FROM TVoucherRefDetails where PkRefTrnNo=" + pkBill + "").Table;

                                            #region adjust bill's reference
                                            tVchRefDtls = new TVoucherRefDetails();
                                            tVchRefDtls.PkRefTrnNo = 0;
                                            tVchRefDtls.FkVoucherSrNo = Convert.ToInt64(dtRefBill.Rows[0].ItemArray[1].ToString());
                                            tVchRefDtls.FkVoucherTrnNo = Convert.ToInt64(dtRefBill.Rows[0].ItemArray[0].ToString());
                                            tVchRefDtls.LedgerNo = ObjFunction.GetComboValue(cmbPartyName);
                                            tVchRefDtls.TypeOfRef = 5;//tVchRefDtls.TypeOfRef = 2;
                                            tVchRefDtls.RefNo = Convert.ToInt64(dtRefBill.Rows[0].ItemArray[2].ToString());
                                            tVchRefDtls.DueDays = 0;
                                            tVchRefDtls.DueDate = DBGetVal.ServerTime;
                                            tVchRefDtls.Amount = Convert.ToDouble(dgAdjustOpening.Rows[i].Cells[ColIndex.Amount].Value);
                                            if (VoucherType == VchType.Sales)
                                                tVchRefDtls.SignCode = 2;
                                            else
                                                tVchRefDtls.SignCode = 1;
                                            tVchRefDtls.UserID = DBGetVal.UserID;
                                            tVchRefDtls.UserDate = DBGetVal.ServerTime.Date;
                                            tVchRefDtls.CompanyNo = Convert.ToInt64(dtRefBill.Rows[0].ItemArray[3].ToString());
                                            dbTVoucherEntry.AddTVoucherRefDetails1(tVchRefDtls);
                                            #endregion

                                            //#region adjust Opening balance's reference
                                            //tVchRefDtls = new TVoucherRefDetails();
                                            //tVchRefDtls.PkRefTrnNo = 0;
                                            //tVchRefDtls.FkVoucherSrNo = Convert.ToInt64(dtRefBill.Rows[0].ItemArray[1].ToString());
                                            //tVchRefDtls.FkVoucherTrnNo = Convert.ToInt64(dtRefBill.Rows[0].ItemArray[0].ToString());
                                            //tVchRefDtls.LedgerNo = ObjFunction.GetComboValue(cmbPartyName);
                                            //tVchRefDtls.TypeOfRef = 5;
                                            //tVchRefDtls.RefNo = 0;
                                            //tVchRefDtls.DueDays = 0;
                                            //tVchRefDtls.DueDate = DBGetVal.ServerTime;
                                            //tVchRefDtls.Amount = Convert.ToDouble(dgAdjustOpening.Rows[i].Cells[ColIndex.Amount].Value); ;
                                            ////Note : Here we are adjusting the source refence thus the sign codes are reversed....
                                            //if (VoucherType == VchType.Sales)
                                            //    tVchRefDtls.SignCode = 1;
                                            //else
                                            //    tVchRefDtls.SignCode = 2;
                                            //tVchRefDtls.UserID = DBGetVal.UserID;
                                            //tVchRefDtls.UserDate = DBGetVal.ServerTime.Date;
                                            //tVchRefDtls.CompanyNo = Convert.ToInt64(dtRefBill.Rows[0].ItemArray[3].ToString());
                                            //dbTVoucherEntry.AddTVoucherRefDetails1(tVchRefDtls);
                                            //#endregion
                                        }
                                    }



                                    if (dbTVoucherEntry.ExecuteNonQueryStatements() != 0)
                                    {
                                        flag = true;
                                        flagAdjs = true;
                                    }
                                    else
                                    {
                                        flag = true;
                                        flagAdjs = true;

                                    }
                                }
                            }

                        }
                        #endregion

                        #region rbAdvance
                        else if (rbAdvance.Checked == true)
                        {
                            if (txtAdvAmt.Text != "")
                            {
                                if (ValidationChqNCreditCard() == false)
                                {
                                    cmbPayType_KeyDown(new object(), new KeyEventArgs(Keys.Enter));
                                    return;
                                }
                                long ControlUnder = ObjQry.ReturnLong("Select ControlUnder From MPayType Where PKPayTypeNo=" + ObjFunction.GetComboValue(cmbPayType) + "", CommonFunctions.ConStr);
                                //int cnt = 0;

                                dbTVoucherEntry = new DBTVaucherEntry();
                                tVoucherEntry = new TVoucherEntry();
                                tVoucherEntry.PkVoucherNo = 0;
                                if (VoucherType == VchType.Sales)
                                    tVoucherEntry.VoucherTypeCode = VchType.SalesReceipt;
                                else
                                    tVoucherEntry.VoucherTypeCode = VchType.PurchasePayment;
                                tVoucherEntry.VoucherUserNo = 0;
                                tVoucherEntry.VoucherDate = Convert.ToDateTime(dtpVoucherDate.Text);
                                tVoucherEntry.VoucherTime = Convert.ToDateTime("01-Jan-1900");
                                tVoucherEntry.Reference = "";

                                tVoucherEntry.CompanyNo = DBGetVal.CompanyNo;
                                tVoucherEntry.BilledAmount = Convert.ToDouble(txtAdvAmt.Text.Replace("-", ""));
                                tVoucherEntry.ChallanNo = "";
                                tVoucherEntry.Remark = txtRemark.Text;
                                if (ControlUnder == 4)//cmbPayType.Text == "Cheque"
                                    tVoucherEntry.ChequeNo = Convert.ToInt64(txtChqNo.Text.Trim());
                                else
                                    tVoucherEntry.ChequeNo = 0;
                                tVoucherEntry.ClearingDate = Convert.ToDateTime("01-01-1900");//Convert.ToDateTime(GridView.Rows[j].Cells[ColIndex.ChqDate].Value);
                                tVoucherEntry.Narration = "";
                                tVoucherEntry.UserID = DBGetVal.UserID;
                                tVoucherEntry.UserDate = DBGetVal.ServerTime.Date;
                                tVoucherEntry.OrderType = 1;
                                tVoucherEntry.PayTypeNo = ObjFunction.GetComboValue(cmbPayType);//Convert.ToInt64(GridView.Rows[j].Cells[ColIndex.PayTypeNo].Value);
                                dbTVoucherEntry.AddTVoucherEntry(tVoucherEntry);

                                if (VoucherType == VchType.Sales)
                                {
                                    tVoucherDetails = new TVoucherDetails();
                                    tVoucherDetails.PkVoucherTrnNo = 0;
                                    tVoucherDetails.VoucherSrNo = 1;
                                    tVoucherDetails.SignCode = 2;
                                    tVoucherDetails.LedgerNo = ObjFunction.GetComboValue(cmbPartyName);
                                    tVoucherDetails.Debit = 0;
                                    tVoucherDetails.Credit = Convert.ToDouble(txtAdvAmt.Text.Replace("-", ""));
                                    tVoucherDetails.SrNo = Others.Party;
                                    tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                                    tVoucherDetails.Narration = "";
                                    dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                                }
                                else if (VoucherType == VchType.Purchase)
                                {
                                    tVoucherDetails = new TVoucherDetails();
                                    tVoucherDetails.PkVoucherTrnNo = 0;
                                    tVoucherDetails.VoucherSrNo = 1;
                                    tVoucherDetails.SignCode = 1;
                                    tVoucherDetails.LedgerNo = ObjFunction.GetComboValue(cmbPartyName);
                                    tVoucherDetails.Debit = Convert.ToDouble(txtAdvAmt.Text.Replace("-", ""));
                                    tVoucherDetails.Credit = 0;//Convert.ToDouble(GridView.Rows[j].Cells[ColIndex.Amount].Value);
                                    tVoucherDetails.SrNo = Others.Party;
                                    tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                                    tVoucherDetails.Narration = "";
                                    dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                                }

                                tVchRefDtls = new TVoucherRefDetails();
                                tVchRefDtls.PkRefTrnNo = 0;
                                tVchRefDtls.FkVoucherSrNo = tVoucherDetails.VoucherSrNo;
                                tVchRefDtls.LedgerNo = tVoucherDetails.LedgerNo;
                                tVchRefDtls.TypeOfRef = 1;
                                tVchRefDtls.RefNo = 0;
                                tVchRefDtls.DueDays = 0;
                                tVchRefDtls.DueDate = DBGetVal.ServerTime;
                                tVchRefDtls.Amount = Convert.ToDouble(txtAdvAmt.Text);
                                if (VoucherType == VchType.Sales)
                                    tVchRefDtls.SignCode = 2;
                                else
                                    tVchRefDtls.SignCode = 1;
                                tVchRefDtls.UserID = DBGetVal.UserID;
                                tVchRefDtls.UserDate = DBGetVal.ServerTime.Date;
                                tVchRefDtls.CompanyNo = DBGetVal.CompanyNo;
                                dbTVoucherEntry.AddTVoucherRefDetails(tVchRefDtls);

                                if (ControlUnder == 4)//cmbPayType.Text == "Cheque"
                                {
                                    tVchChqCredit.PkSrNo = 0;
                                    tVchChqCredit.ChequeNo = Convert.ToString(txtChqNo.Text.Trim());
                                    tVchChqCredit.ChequeDate = Convert.ToDateTime(dtpChqDate.Text.Trim());
                                    tVchChqCredit.BankNo = ObjFunction.GetComboValue(cmbBank);
                                    tVchChqCredit.BranchNo = ObjFunction.GetComboValue(cmbBranch);
                                    tVchChqCredit.CreditCardNo = "";
                                    tVchChqCredit.Amount = Convert.ToDouble(txtAdvAmt.Text.Replace("-", ""));
                                    tVchChqCredit.PostFkVoucherNo = 0;
                                    tVchChqCredit.PostFkVoucherTrnNo = 0;
                                    tVchChqCredit.CompanyNo = DBGetVal.CompanyNo;
                                    dbTVoucherEntry.AddTVoucherChqCreditDetails(tVchChqCredit);
                                }
                                else if (ControlUnder == 5)//cmbPayType.Text == "Credit Card"
                                {
                                    tVchChqCredit.PkSrNo = 0;
                                    tVchChqCredit.ChequeNo = "";
                                    tVchChqCredit.ChequeDate = Convert.ToDateTime("01-01-1900");
                                    tVchChqCredit.BankNo = ObjFunction.GetComboValue(cmbCrBank);
                                    tVchChqCredit.BranchNo = ObjFunction.GetComboValue(cmbCrBranch);
                                    tVchChqCredit.CreditCardNo = Convert.ToString(txtCrCardNo.Text.Trim());
                                    tVchChqCredit.Amount = Convert.ToDouble(txtAdvAmt.Text.Replace("-", ""));
                                    tVchChqCredit.PostFkVoucherNo = 0;
                                    tVchChqCredit.PostFkVoucherTrnNo = 0;
                                    tVchChqCredit.CompanyNo = DBGetVal.CompanyNo;
                                    dbTVoucherEntry.AddTVoucherChqCreditDetails(tVchChqCredit);
                                }
                                if (VoucherType == VchType.Sales)
                                {
                                    tVoucherDetails = new TVoucherDetails();
                                    tVoucherDetails.PkVoucherTrnNo = 0;
                                    tVoucherDetails.VoucherSrNo = 2;
                                    tVoucherDetails.SignCode = 1;
                                    tVoucherDetails.LedgerNo = ObjQry.ReturnLong("Select LedgerNo From MPayTypeLedger Where PayTypeNo=" + ObjFunction.GetComboValue(cmbPayType) + " AND CompanyNo=" + DBGetVal.CompanyNo + "", CommonFunctions.ConStr);
                                    tVoucherDetails.Debit = Convert.ToDouble(txtAdvAmt.Text.Replace("-", ""));
                                    tVoucherDetails.Credit = 0;
                                    tVoucherDetails.SrNo = 0;
                                    tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                                    tVoucherDetails.Narration = "";
                                    dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                                }
                                else if (VoucherType == VchType.Purchase)
                                {
                                    tVoucherDetails = new TVoucherDetails();
                                    tVoucherDetails.PkVoucherTrnNo = 0;
                                    tVoucherDetails.VoucherSrNo = 2;
                                    tVoucherDetails.SignCode = 2;

                                    if (ControlUnder == 2)//cmbPayType.Text == "Cash"
                                        tVoucherDetails.LedgerNo = ObjQry.ReturnLong("Select LedgerNo From MPayTypeLedger Where PayTypeNo=" + ObjFunction.GetComboValue(cmbPayType) + " AND CompanyNo=" + DBGetVal.CompanyNo + "", CommonFunctions.ConStr);
                                    else
                                        tVoucherDetails.LedgerNo = ((ControlUnder == 5) ? ObjFunction.GetComboValue(cmbCrBank) : ObjFunction.GetComboValue(cmbBank));

                                    tVoucherDetails.Debit = 0;//Convert.ToDouble(GridView.Rows[j].Cells[ColIndex.Amount].Value);
                                    tVoucherDetails.Credit = Convert.ToDouble(txtAdvAmt.Text.Replace("-", ""));
                                    tVoucherDetails.SrNo = 0;
                                    tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                                    tVoucherDetails.Narration = "";
                                    dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                                }
                                long tempId = dbTVoucherEntry.ExecuteNonQueryStatements();
                                if (tempId != 0)
                                {
                                    if (StrBillNo == "")
                                        StrBillNo = tempId.ToString();
                                    else
                                        StrBillNo = StrBillNo + "," + tempId.ToString();

                                    flag = true;
                                    flagAdjs = true;
                                }
                                else
                                {
                                    flag = false;
                                    flagAdjs = true;

                                }
                            }
                        }
                        #endregion

                        #region rbAgainstAdv && rbAdjSalesReturn
                        else if (rbAgainstAdv.Checked == true || rbAdjSalesReturn.Checked == true)
                        {
                            long pkBill = 0; long pkAdvance = 0;
                            //double Amount = 0;

                            if (ValidationdgBillAdv() == true)
                            {
                                DataTable dtRefAdvance = new DataTable();
                                for (int i = 0; i < dgAdvance.Rows.Count; i++)
                                {
                                    if (Convert.ToBoolean(dgAdvance.Rows[i].Cells[8].FormattedValue) == true)
                                    {
                                        pkAdvance = Convert.ToInt64(dgAdvance.Rows[i].Cells[7].Value);
                                        dtRefAdvance = ObjFunction.GetDataView("SELECT FkVoucherTrnNo, FkVoucherSrNo, RefNo, CompanyNo FROM TVoucherRefDetails where PkRefTrnNo=" + pkAdvance + "").Table;
                                        break;
                                    }
                                }

                                dbTVoucherEntry = new DBTVaucherEntry();
                                for (int i = 0; i < dgBill.Rows.Count; i++)
                                {
                                    if (Convert.ToBoolean(dgBill.Rows[i].Cells[ColIndex.Chk].FormattedValue) == true)
                                    {
                                        pkBill = Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.PayTypeNo].Value);
                                        DataTable dtRefBill = ObjFunction.GetDataView("SELECT FkVoucherTrnNo, FkVoucherSrNo, RefNo, CompanyNo FROM TVoucherRefDetails where PkRefTrnNo=" + pkBill + "").Table;

                                        #region adjust bill's reference
                                        tVchRefDtls = new TVoucherRefDetails();
                                        tVchRefDtls.PkRefTrnNo = 0;
                                        tVchRefDtls.FkVoucherSrNo = Convert.ToInt64(dtRefAdvance.Rows[0].ItemArray[1].ToString());
                                        tVchRefDtls.FkVoucherTrnNo = Convert.ToInt64(dtRefAdvance.Rows[0].ItemArray[0].ToString());
                                        tVchRefDtls.LedgerNo = ObjFunction.GetComboValue(cmbPartyName);
                                        tVchRefDtls.TypeOfRef = 2;
                                        tVchRefDtls.RefNo = Convert.ToInt64(dtRefBill.Rows[0].ItemArray[2].ToString());
                                        tVchRefDtls.DueDays = 0;
                                        tVchRefDtls.DueDate = DBGetVal.ServerTime;
                                        tVchRefDtls.Amount = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Amount].Value);
                                        if (VoucherType == VchType.Sales)
                                            tVchRefDtls.SignCode = 2;
                                        else
                                            tVchRefDtls.SignCode = 1;
                                        tVchRefDtls.UserID = DBGetVal.UserID;
                                        tVchRefDtls.UserDate = DBGetVal.ServerTime.Date;
                                        tVchRefDtls.CompanyNo = Convert.ToInt64(dtRefBill.Rows[0].ItemArray[3].ToString());
                                        dbTVoucherEntry.AddTVoucherRefDetails1(tVchRefDtls);
                                        #endregion

                                        #region adjust Advance's reference
                                        tVchRefDtls = new TVoucherRefDetails();
                                        tVchRefDtls.PkRefTrnNo = 0;
                                        tVchRefDtls.FkVoucherSrNo = Convert.ToInt64(dtRefBill.Rows[0].ItemArray[1].ToString());
                                        tVchRefDtls.FkVoucherTrnNo = Convert.ToInt64(dtRefBill.Rows[0].ItemArray[0].ToString());
                                        tVchRefDtls.LedgerNo = ObjFunction.GetComboValue(cmbPartyName);
                                        if (rbAdjSalesReturn.Checked == true)
                                            tVchRefDtls.TypeOfRef = 2;
                                        else if (rbAgainstAdv.Checked == true)
                                            tVchRefDtls.TypeOfRef = 6;
                                        tVchRefDtls.RefNo = Convert.ToInt64(dtRefAdvance.Rows[0].ItemArray[2].ToString());
                                        tVchRefDtls.DueDays = 0;
                                        tVchRefDtls.DueDate = DBGetVal.ServerTime;
                                        tVchRefDtls.Amount = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Amount].Value);
                                        //Note : Here we are adjusting the source refence thus the sign codes are reversed....
                                        if (VoucherType == VchType.Sales)
                                            tVchRefDtls.SignCode = 1;
                                        else
                                            tVchRefDtls.SignCode = 2;
                                        tVchRefDtls.UserID = DBGetVal.UserID;
                                        tVchRefDtls.UserDate = DBGetVal.ServerTime.Date;
                                        tVchRefDtls.CompanyNo = Convert.ToInt64(dtRefAdvance.Rows[0].ItemArray[3].ToString());
                                        dbTVoucherEntry.AddTVoucherRefDetails1(tVchRefDtls);
                                        #endregion
                                    }
                                }
                                long tempId = dbTVoucherEntry.ExecuteNonQueryStatements();
                                if (tempId != 0)
                                {
                                    if (StrBillNo == "")
                                        StrBillNo = tempId.ToString();
                                    else
                                        StrBillNo = StrBillNo + "," + tempId.ToString();
                                    flag = true;
                                    flagAdjs = true;
                                }
                                else
                                {
                                    flag = true;
                                    flagAdjs = true;
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        OMMessageBox.Show("Please Enter Amount......... ", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                    }

                    if (flag == true && flagAdjs == true)
                    {
                        OMMessageBox.Show("Voucher Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);

                        BindGrid();

                        #region Print Recipet Method

                        if (rbBillwise.Checked == true || rbBillSelection.Checked == true || rbAdvance.Checked == true || rbAdjustment.Checked == true)
                        {
                            if (VoucherType == VchType.Sales)
                            {
                                if (OMMessageBox.Show("Are you sure you want to Print " + ((VoucherType == 15) ? "Receipt" : "Payment") + " ?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question, OMMessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                {
                                    PrintBill(StrBillNo, 0);
                                }

                                //    DialogResult ds = OMMessageBox.Show("Are you sure you want to Print " + ((VoucherType == 15) ? "Receipt" : "Payment") + " ?", CommonFunctions.ErrorTitle, OMMessageBoxIcon.Question, OMMessageBoxDefaultButton.Button1, "Preview");
                                //    if (ds == DialogResult.Yes)
                                //    {
                                //        PrintBill(StrBillNo, 0);
                                //    }
                                //    //else if (ds == DialogResult.Cancel)
                                //    //{
                                //    //    PrintBill(StrBillNo, 1);
                                //    //}
                            }
                            else if (VoucherType == VchType.Purchase)
                            {

                                if (OMMessageBox.Show("Are you sure you want to Print " + ((VoucherType == 9) ? "Receipt" : "Payment") + " ?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question, OMMessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                {
                                    PrintBill(StrBillNo, 0);
                                }
                                //DialogResult ds = OMMessageBox.Show("Are you sure you want to Print " + ((VoucherType == 9) ? "Receipt" : "Payment") + " ?", CommonFunctions.ErrorTitle, OMMessageBoxIcon.Question, OMMessageBoxDefaultButton.Button1, "Preview");
                                //if (ds == DialogResult.Yes)
                                //{
                                //    PrintBill(StrBillNo, 0);
                                //}
                                //else if (ds == DialogResult.Cancel)
                                //{
                                //    PrintBill(StrBillNo, 1);
                                //}
                            }
                        }


                        #endregion


                        if (rbAdjustment.Checked == true)
                        {
                            rbBillwise.Checked = true;
                            rbCheck_Changed();
                        }
                        else if (rbAdvance.Checked == true)
                        {
                            rbBillwise.Checked = true;
                            rbCheck_Changed();
                        }
                        else if (rbAgainstAdv.Checked == true || rbAdjSalesReturn.Checked == true)
                        {
                            rbBillwise.Checked = true;
                            rbCheck_Changed();
                            BindGridAdv();
                        }
                    }
                    else if (flagAdjs == true)
                        OMMessageBox.Show("Voucher Not Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);

                }
                else
                {
                    OMMessageBox.Show("Please Select Party Name ", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        public void PrintBill(string PkVoucherNo, int PType)
        {
            string[] ReportSession;
            DataTable dtPrint = ObjFunction.GetDataView("SELECT VoucherDate, BilledAmount, Remark,IsNull((Select (Case when (Debit>0) then Debit Else Credit end) From TVoucherDetails Where SrNo=501 And FkVoucherNo=PkVoucherNo),0) as PartyAmount ,IsNull( (Select (Case when (Debit>0) then Debit Else Credit end) From TVoucherDetails Where SrNo=502 And FkVoucherNo=PkVoucherNo),0) as DiscAmt  FROM TVoucherEntry Where PkVoucherNo In(" + PkVoucherNo + ") ").Table;
            for (int i = 0; i < dtPrint.Rows.Count; i++)
            {

                ReportSession = new string[11];
                ReportSession[0] = DBGetVal.CompanyName;
                ReportSession[1] = ((VoucherType == 15) ? "Receipt" : "Payment");
                ReportSession[2] =Convert.ToDateTime( dtPrint.Rows[i].ItemArray[0].ToString()).ToString("dd-MMM-yyyy");
                ReportSession[3] = (Convert.ToDouble(dtPrint.Rows[i].ItemArray[3].ToString()) - Convert.ToDouble(dtPrint.Rows[i].ItemArray[4].ToString())).ToString();
                ReportSession[4] = NumberToWordsIndian.getWords(ReportSession[3].ToString());
                ReportSession[5] = dtPrint.Rows[i].ItemArray[2].ToString();
                ReportSession[6] = cmbPartyName.Text;
                ReportSession[7] = StrRBillNo;
                ReportSession[8] = dtPrint.Rows[i].ItemArray[4].ToString();
                if (VoucherType == VchType.Sales)
                    ReportSession[9] = "Total Dues:" + lblNetB.Text + "  " + lblPayStatus.Text;
                else
                    ReportSession[9] = "Total Dues:" + lblNetBp.Text + "  " + lblPayStatusPur.Text;
                ReportSession[10] = ObjQry.ReturnString("Select LangLedgerName From MLedger Where LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName), CommonFunctions.ConStr);

                if (PType == 0)
                {
                    CrystalDecisions.CrystalReports.Engine.ReportDocument childForm;
                    childForm = null;
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                        childForm = ObjFunction.GetReportObject("Reports.GetCollectionPrint");
                    else
                        childForm = ObjFunction.LoadReportObject("GetCollectionPrint.rpt", CommonFunctions.ReportPath);
                    if (childForm != null)
                    {
                        DBReportGenerate objRpt = new DBReportGenerate(childForm, ReportSession);
                        if (objRpt.PrintReport() == true)
                        {
                            DisplayMessage("" + ((VoucherType == 15) ? "Receipt" : "Payment") + " Print Successfully!!!");
                        }
                        else
                        {
                            DisplayMessage("" + ((VoucherType == 9) ? "Receipt" : "Payment") + " not Print !!!");
                        }
                    }
                    else
                    {
                        DisplayMessage("Bill Report not exist !!!");
                    }
                }
                else
                {
                    Form NewF = null;

                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                        NewF = new Display.ReportViewSource(new Reports.GetCollectionPrint(), ReportSession);
                    else
                        NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("GetCollectionPrint.rpt", CommonFunctions.ReportPath), ReportSession);
                    ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                }
            }
        }

        private void setValues1()
        {
            try
            {
                bool flag = false;
                if (Validations() == true)
                {
                    for (int j = 0; j < GridView.RowCount; j++)
                    {
                        if (Convert.ToDouble(GridView.Rows[j].Cells[ColIndex.Amount].Value.ToString()) != 0)
                        {
                            dbTVoucherEntry = new DBTVaucherEntry();
                            tVoucherEntry = new TVoucherEntry();
                            tVoucherEntry.PkVoucherNo = 0;
                            if (VoucherType == VchType.Sales)
                                tVoucherEntry.VoucherTypeCode = VchType.SalesReceipt;
                            else
                                tVoucherEntry.VoucherTypeCode = VchType.PurchasePayment;
                            tVoucherEntry.VoucherUserNo = 0;
                            tVoucherEntry.VoucherDate = Convert.ToDateTime(dtpVoucherDate.Text);
                            tVoucherEntry.VoucherTime = Convert.ToDateTime("01-Jan-1900");
                            tVoucherEntry.Reference = "";
                            tVoucherEntry.CompanyNo = DBGetVal.CompanyNo;
                            tVoucherEntry.BilledAmount = Convert.ToDouble(GridView.Rows[j].Cells[ColIndex.Amount].Value);
                            tVoucherEntry.ChallanNo = "";
                            tVoucherEntry.Remark = "";
                            if (GridView.Rows[j].Cells[ColIndex.PayType].Value.ToString() == "Cheque")
                                tVoucherEntry.ChequeNo = Convert.ToInt64(GridView.Rows[j].Cells[ColIndex.ChqNo].Value);
                            else
                                tVoucherEntry.ChequeNo = 0;
                            tVoucherEntry.ClearingDate = Convert.ToDateTime(GridView.Rows[j].Cells[ColIndex.ChqDate].Value);
                            tVoucherEntry.Narration = "";
                            tVoucherEntry.UserID = DBGetVal.UserID;
                            tVoucherEntry.UserDate = DBGetVal.ServerTime.Date;
                            tVoucherEntry.OrderType = 1;
                            tVoucherEntry.PayTypeNo = Convert.ToInt64(GridView.Rows[j].Cells[ColIndex.PayTypeNo].Value);
                            dbTVoucherEntry.AddTVoucherEntry(tVoucherEntry);

                            if (VoucherType == VchType.Sales)
                            {

                                tVoucherDetails = new TVoucherDetails();
                                tVoucherDetails.PkVoucherTrnNo = 0;
                                tVoucherDetails.VoucherSrNo = 1;
                                tVoucherDetails.SignCode = 2;
                                tVoucherDetails.LedgerNo = ObjFunction.GetComboValue(cmbPartyName);
                                tVoucherDetails.Debit = 0;
                                tVoucherDetails.Credit = Convert.ToDouble(GridView.Rows[j].Cells[ColIndex.Amount].Value);
                                tVoucherDetails.SrNo = Others.Party;
                                tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                                tVoucherDetails.Narration = "";
                                dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                            }
                            else if (VoucherType == VchType.Purchase)
                            {
                                tVoucherDetails = new TVoucherDetails();
                                tVoucherDetails.PkVoucherTrnNo = 0;
                                tVoucherDetails.VoucherSrNo = 1;
                                tVoucherDetails.SignCode = 1;
                                tVoucherDetails.LedgerNo = ObjFunction.GetComboValue(cmbPartyName);
                                tVoucherDetails.Debit = Convert.ToDouble(GridView.Rows[j].Cells[ColIndex.Amount].Value);
                                tVoucherDetails.Credit = 0;//Convert.ToDouble(GridView.Rows[j].Cells[ColIndex.Amount].Value);
                                tVoucherDetails.SrNo = Others.Party;
                                tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                                tVoucherDetails.Narration = "";
                                dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                            }

                            tVchRefDtls = new TVoucherRefDetails();
                            tVchRefDtls.PkRefTrnNo = 0;
                            tVchRefDtls.FkVoucherSrNo = tVoucherDetails.VoucherSrNo;
                            tVchRefDtls.LedgerNo = tVoucherDetails.LedgerNo;
                            tVchRefDtls.TypeOfRef = 2;
                            tVchRefDtls.RefNo = Convert.ToInt64(GridView.Rows[j].Cells[ColIndex.RefNo].Value);
                            tVchRefDtls.DueDays = 0;
                            tVchRefDtls.DueDate = DBGetVal.ServerTime;
                            tVchRefDtls.Amount = tVoucherEntry.BilledAmount;
                            tVchRefDtls.SignCode = 1;
                            tVchRefDtls.UserID = DBGetVal.UserID;
                            tVchRefDtls.UserDate = DBGetVal.ServerTime.Date;
                            tVchRefDtls.CompanyNo = DBGetVal.CompanyNo;
                            dbTVoucherEntry.AddTVoucherRefDetails(tVchRefDtls);

                            if (GridView.Rows[j].Cells[ColIndex.PayType].Value.ToString() == "Cheque")
                            {

                                tVchChqCredit.PkSrNo = 0;
                                tVchChqCredit.ChequeNo = Convert.ToString(GridView.Rows[j].Cells[ColIndex.ChqNo].Value);
                                tVchChqCredit.ChequeDate = Convert.ToDateTime(GridView.Rows[j].Cells[ColIndex.ChqDate].Value);
                                tVchChqCredit.BankNo = Convert.ToInt64(GridView.Rows[j].Cells[ColIndex.BankNo].Value);
                                tVchChqCredit.BranchNo = Convert.ToInt64(GridView.Rows[j].Cells[ColIndex.BranchNo].Value);
                                tVchChqCredit.CreditCardNo = "";
                                tVchChqCredit.Amount = Convert.ToDouble(GridView.Rows[j].Cells[ColIndex.Amount].Value);
                                tVchChqCredit.PostFkVoucherNo = 0;
                                tVchChqCredit.PostFkVoucherTrnNo = 0;
                                tVchChqCredit.CompanyNo = DBGetVal.CompanyNo;
                                dbTVoucherEntry.AddTVoucherChqCreditDetails(tVchChqCredit);
                            }
                            else if (GridView.Rows[j].Cells[ColIndex.PayType].Value.ToString() == "Credit Card")
                            {
                                tVchChqCredit.PkSrNo = 0;
                                tVchChqCredit.ChequeNo = "";
                                tVchChqCredit.ChequeDate = Convert.ToDateTime(GridView.Rows[j].Cells[ColIndex.ChqDate].Value);
                                tVchChqCredit.BankNo = Convert.ToInt64(GridView.Rows[j].Cells[ColIndex.BankNo].Value);
                                tVchChqCredit.BranchNo = Convert.ToInt64(GridView.Rows[j].Cells[ColIndex.BranchNo].Value);
                                tVchChqCredit.CreditCardNo = Convert.ToString(GridView.Rows[j].Cells[ColIndex.ChqNo].Value);
                                tVchChqCredit.Amount = Convert.ToDouble(GridView.Rows[j].Cells[ColIndex.Amount].Value);
                                tVchChqCredit.PostFkVoucherNo = 0;
                                tVchChqCredit.PostFkVoucherTrnNo = 0;
                                tVchChqCredit.CompanyNo = DBGetVal.CompanyNo;
                                dbTVoucherEntry.AddTVoucherChqCreditDetails(tVchChqCredit);
                            }

                            if (VoucherType == VchType.Sales)
                            {
                                tVoucherDetails = new TVoucherDetails();
                                tVoucherDetails.PkVoucherTrnNo = 0;
                                tVoucherDetails.VoucherSrNo = 2;
                                tVoucherDetails.SignCode = 1;
                                tVoucherDetails.LedgerNo = Convert.ToInt64(GridView.Rows[j].Cells[ColIndex.LedgerNo].Value);
                                tVoucherDetails.Debit = Convert.ToDouble(GridView.Rows[j].Cells[ColIndex.Amount].Value);
                                tVoucherDetails.Credit = 0;
                                tVoucherDetails.SrNo = 0;
                                tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                                tVoucherDetails.Narration = "";
                                dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                            }
                            else if (VoucherType == VchType.Purchase)
                            {
                                tVoucherDetails = new TVoucherDetails();
                                tVoucherDetails.PkVoucherTrnNo = 0;
                                tVoucherDetails.VoucherSrNo = 2;
                                tVoucherDetails.SignCode = 2;
                                tVoucherDetails.LedgerNo = Convert.ToInt64(GridView.Rows[j].Cells[ColIndex.LedgerNo].Value);
                                tVoucherDetails.Debit = 0;//Convert.ToDouble(GridView.Rows[j].Cells[ColIndex.Amount].Value);
                                tVoucherDetails.Credit = Convert.ToDouble(GridView.Rows[j].Cells[ColIndex.Amount].Value); //0;
                                tVoucherDetails.SrNo = 0;
                                tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                                tVoucherDetails.Narration = "";
                                dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                            }

                            if (dbTVoucherEntry.ExecuteNonQueryStatements() != 0)
                            {
                                flag = true;
                            }
                            else
                            {
                                flag = false;
                                break;
                            }

                        }

                    }

                    if (flag == true)
                    {
                        OMMessageBox.Show("Voucher Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        BindGrid();


                    }
                    else
                        OMMessageBox.Show("Voucher Not Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            setValues();
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region KeyDown Events
        private void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is GroupBox)
                    KeyDownFormat(ctrl.Controls);
            }
        }

        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Alt && e.KeyCode == Keys.F2)
            {
                btnAdvanceSearch_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F2)
            {
                BtnSave_Click(sender, e);
            }

        }
        #endregion

        private void dtpVoucherDate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == 13)
            {
                cmbPartyName.Focus();
            }
        }

        private void GridView_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    if (GridView.Rows.Count != 0)
                    {
                        if (GridView.CurrentCell.ColumnIndex == ColIndex.PayType)
                        {
                            pnlPaytype.Visible = true;
                            lstPayType.Focus();
                        }
                        else if (GridView.CurrentCell.ColumnIndex == ColIndex.Amount)
                        {
                            if (GridView.CurrentCell.Value != null)
                            {
                                if (ObjFunction.CheckValidAmount(GridView.CurrentCell.Value.ToString()) == false)
                                    GridView.CurrentCell.ErrorText = "Please Enter Valid Amount";
                                else
                                    GridView.CurrentCell = GridView[ColIndex.PayType, GridView.CurrentCell.RowIndex];
                            }
                        }
                    }
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    e.SuppressKeyPress = true;
                    if (pnlRecord.Visible == true)
                    {
                        pnlRecord.Visible = false;
                        GridView.CurrentCell = GridView[ColIndex.TotRec, GridView.CurrentCell.RowIndex];
                    }
                    else
                        BtnSave.Focus();
                }
                else if (e.KeyCode == Keys.F4)
                {
                    e.SuppressKeyPress = true;
                    BindRecords();
                    dgDataRecord.Focus();
                }

            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private static class ColIndex
        {
            public static int BillNo = 0;
            public static int BillDate = 1;
            public static int BillAmt = 2;
            public static int TotRec = 3;
            public static int NetBal = 4;
            public static int PayType = 6;
            public static int Amount = 5;
            public static int LedgerNo = 7;
            public static int ChqNo = 8;
            public static int ChqDate = 9;
            public static int BankNo = 10;
            public static int BranchNo = 11;
            public static int PayTypeNo = 12;
            public static int RefNo = 13;
            public static int Chk = 14;
        }

        private void cmbPartyName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                processPartyNameChange();
            }
        }

        private void processPartyNameChange()
        {
            BindGrid();
            btnPrint.Visible = false;
            pnlPrint.Visible = false;
            if (GridView.Rows.Count > 0)
            {
                GridView.Focus();
                GridView.CurrentCell = GridView[ColIndex.Amount, 0];
                label5.Visible = true;
                //btnPrint.Visible = true;

            }
            rbCheck_Changed();
        }

        private void lstPayType_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    Panel p = new Panel();
                    if (pnlBillselection.Visible == true)
                        p = pnlBillselection;
                    else if (pnlAdjustment.Visible == true)
                        p = pnlAdjustment;
                    else
                        p = pnlBillwise;

                    e.SuppressKeyPress = true;
                    txtChqNo.Text = "";
                    txtCrCardNo.Text = "";
                    cmbBank.SelectedIndex = 0;
                    cmbBranch.SelectedIndex = 0;
                    cmbCrBank.SelectedIndex = 0;
                    cmbCrBranch.SelectedIndex = 0;

                    // GridView.Rows[GridView.CurrentCell.RowIndex].Cells[ColIndex.PayType].Value = lstPayType.Text;
                    //manali
                    long ControlUnder = ObjQry.ReturnLong("Select ControlUnder From MPayType Where PKPayTypeNo=" + lstPayType.SelectedValue + "", CommonFunctions.ConStr);
                    if (ControlUnder == 1 || ControlUnder == 4 || ControlUnder == 5)
                    {
                        if (ControlUnder == 4 || ControlUnder == 5)
                        {
                            if (ControlUnder == 4)
                            {
                                int y = pnlMain.Location.Y + p.Location.Y + GridView.Location.Y;
                                y = y + ((p.Height - GridView.Height) / 2);
                                int x = (GridView.Width - pnlchq.Width) / 2;
                                pnlchq.Location = new Point(GridView.Location.X + x, y + 10);

                                pnlPaytype.Visible = false;
                                pnlchq.Visible = true;
                                BtnSave.Enabled = false;
                                txtChqNo.Focus();
                            }
                            else if (ControlUnder == 5)
                            {
                                int y = pnlMain.Location.Y + p.Location.Y + GridView.Location.Y;
                                y = y + ((p.Height - GridView.Height) / 2);
                                int x = (GridView.Width - pnlCredit.Width) / 2;
                                pnlCredit.Location = new Point(GridView.Location.X + x, y + 10);
                                BtnSave.Enabled = false;
                                pnlPaytype.Visible = false;
                                pnlCredit.Visible = true;
                                txtCrCardNo.Focus();
                            }
                        }
                        else
                        {
                            GridView.Rows[GridView.CurrentCell.RowIndex].Cells[ColIndex.PayType].Value = lstPayType.Text;
                            GridView.Rows[GridView.CurrentCell.RowIndex].Cells[ColIndex.LedgerNo].Value = dtPayTypeLedger.Rows[0].ItemArray[1].ToString();
                            GridView.Rows[GridView.CurrentCell.RowIndex].Cells[ColIndex.PayTypeNo].Value = lstPayType.SelectedValue;
                            pnlPaytype.Visible = false;
                            BtnSave.Enabled = true;
                            GridView.Focus();
                            if (GridView.CurrentCell.RowIndex < GridView.Rows.Count - 1)
                                GridView.CurrentCell = GridView[ColIndex.Amount, GridView.CurrentCell.RowIndex + 1];
                        }

                    }
                    else
                    {
                        GridView.Rows[GridView.CurrentCell.RowIndex].Cells[ColIndex.PayType].Value = lstPayType.Text;
                        GridView.Rows[GridView.CurrentCell.RowIndex].Cells[ColIndex.LedgerNo].Value = dtPayTypeLedger.Rows[0].ItemArray[1].ToString();
                        GridView.Rows[GridView.CurrentCell.RowIndex].Cells[ColIndex.PayTypeNo].Value = lstPayType.SelectedValue;
                        pnlPaytype.Visible = false;
                        BtnSave.Enabled = true;
                        GridView.Focus();
                        if (GridView.CurrentCell.RowIndex < GridView.Rows.Count - 1)
                            GridView.CurrentCell = GridView[ColIndex.Amount, GridView.CurrentCell.RowIndex + 1];
                    }

                    //manaliend
                    //if (lstPayType.SelectedValue.ToString() == "4")
                    //{
                    //    int y = pnlMain.Location.Y + p.Location.Y + GridView.Location.Y;
                    //    y = y + ((p.Height - GridView.Height) / 2);
                    //    int x = (GridView.Width - pnlchq.Width) / 2;
                    //    pnlchq.Location = new Point(GridView.Location.X + x, y + 10);

                    //    pnlPaytype.Visible = false;
                    //    pnlchq.Visible = true;
                    //    BtnSave.Enabled = false;
                    //    txtChqNo.Focus();
                    //}
                    //else if (lstPayType.SelectedValue.ToString() == "5")
                    //{
                    //    int y = pnlMain.Location.Y + p.Location.Y + GridView.Location.Y;
                    //    y = y + ((p.Height - GridView.Height) / 2);
                    //    int x = (GridView.Width - pnlCredit.Width) / 2;
                    //    pnlCredit.Location = new Point(GridView.Location.X + x, y + 10);
                    //    BtnSave.Enabled = false;
                    //    pnlPaytype.Visible = false;
                    //    pnlCredit.Visible = true;
                    //    txtCrCardNo.Focus();
                    //}
                    //else
                    //{
                    //    GridView.Rows[GridView.CurrentCell.RowIndex].Cells[ColIndex.PayType].Value = lstPayType.Text;
                    //    GridView.Rows[GridView.CurrentCell.RowIndex].Cells[ColIndex.LedgerNo].Value = dtPayTypeLedger.Rows[0].ItemArray[1].ToString();
                    //    GridView.Rows[GridView.CurrentCell.RowIndex].Cells[ColIndex.PayTypeNo].Value = lstPayType.SelectedValue;
                    //    pnlPaytype.Visible = false;
                    //    BtnSave.Enabled = true;
                    //    GridView.Focus();
                    //    if (GridView.CurrentCell.RowIndex < GridView.Rows.Count - 1)
                    //        GridView.CurrentCell = GridView[ColIndex.Amount, GridView.CurrentCell.RowIndex + 1];
                    //}
                }
                else if (e.KeyCode == Keys.Escape || e.KeyCode == Keys.Space)
                {
                    pnlPaytype.Visible = false;
                    GridView.Focus();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnchqOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (ChqValidations() == true)
                {
                    BtnSave.Enabled = true;
                    if (rbBillwise.Checked == true)
                    {
                        GridView.Rows[GridView.CurrentCell.RowIndex].Cells[ColIndex.ChqNo].Value = txtChqNo.Text;
                        GridView.Rows[GridView.CurrentCell.RowIndex].Cells[ColIndex.ChqDate].Value = dtpChqDate.Value;
                        GridView.Rows[GridView.CurrentCell.RowIndex].Cells[ColIndex.BankNo].Value = cmbBank.SelectedValue;
                        GridView.Rows[GridView.CurrentCell.RowIndex].Cells[ColIndex.BranchNo].Value = cmbBranch.SelectedValue;
                        GridView.Rows[GridView.CurrentCell.RowIndex].Cells[ColIndex.PayType].Value = lstPayType.Text;
                        if (VoucherType == VchType.Sales)
                            GridView.Rows[GridView.CurrentCell.RowIndex].Cells[ColIndex.LedgerNo].Value = dtPayTypeLedger.Rows[1].ItemArray[1].ToString();
                        else
                            GridView.Rows[GridView.CurrentCell.RowIndex].Cells[ColIndex.LedgerNo].Value = ObjFunction.GetComboValue(cmbBank);

                        GridView.Rows[GridView.CurrentCell.RowIndex].Cells[ColIndex.PayTypeNo].Value = lstPayType.SelectedValue;

                        pnlchq.Visible = false;
                        GridView.Focus();
                        if (GridView.CurrentCell.RowIndex < GridView.Rows.Count - 1)
                            GridView.CurrentCell = GridView[ColIndex.Amount, GridView.CurrentCell.RowIndex + 1];
                    }
                    else if (rbBillSelection.Checked == true)
                    {
                        dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.ChqNo].Value = txtChqNo.Text;
                        dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.ChqDate].Value = dtpChqDate.Value;
                        dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.BankNo].Value = cmbBank.SelectedValue;
                        dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.BranchNo].Value = cmbBranch.SelectedValue;
                        dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.PayType].Value = cmbPayType.Text;
                        if (VoucherType == VchType.Sales)
                        {
                            dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.LedgerNo].Value = dtPayTypeLedger.Rows[1].ItemArray[1].ToString();
                            LedgNo = Convert.ToInt64(dtPayTypeLedger.Rows[1].ItemArray[1].ToString());
                        }
                        else
                        {
                            dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.LedgerNo].Value = ObjFunction.GetComboValue(cmbBank);
                            LedgNo = ObjFunction.GetComboValue(cmbBank);

                        }

                        dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.PayTypeNo].Value = ObjFunction.GetComboValue(cmbPayType);
                        pnlchq.Visible = false;
                        dgBillSelection.Focus();
                        if (dgBillSelection.CurrentCell.RowIndex < dgBillSelection.Rows.Count - 1)
                            dgBillSelection.CurrentCell = dgBillSelection[ColIndex.Amount, dgBillSelection.CurrentCell.RowIndex + 1];
                        BtnSave.Focus();
                    }
                    else if (rbAdjustment.Checked == true)
                    {
                        BtnSave.Focus();
                        pnlchq.Visible = false;
                    }
                    else if (rbAdvance.Checked == true)
                    {
                        BtnSave.Focus();
                        pnlchq.Visible = false;
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnchqCancel_Click(object sender, EventArgs e)
        {
            pnlchq.Visible = false;
            BtnSave.Enabled = true;
            txtChqNo.Text = "";
            cmbBank.SelectedIndex = 0;
            cmbBranch.SelectedIndex = 0;
            EP.SetError(txtChqNo, "");
            if (rbBillwise.Checked == true)
            {
                GridView.Focus();
                if (GridView.CurrentCell.RowIndex < GridView.Rows.Count - 1)
                    GridView.CurrentCell = GridView[ColIndex.Amount, GridView.CurrentCell.RowIndex + 1];
                else
                    GridView.CurrentCell = GridView[ColIndex.Amount, GridView.Rows.Count - 1];
            }
            else
            {
                cmbPayType.Focus();
            }
        }

        private bool ChqValidations()
        {
            bool tempflag = false;
            try
            {
                EP.SetError(txtChqNo, "");
                EP.SetError(cmbBank, "");
                EP.SetError(cmbBranch, "");

                if (txtChqNo.Text.Trim() == "")
                {
                    EP.SetError(txtChqNo, "Enter Cheque No");
                    EP.SetIconAlignment(txtChqNo, ErrorIconAlignment.MiddleRight);
                    txtChqNo.Focus();
                }
                else if (Convert.ToInt64(cmbBank.SelectedValue) == 0)
                {
                    EP.SetError(cmbBank, "Select Bank");
                    EP.SetIconAlignment(cmbBank, ErrorIconAlignment.MiddleRight);
                    cmbBank.Focus();
                }
                else if (Convert.ToInt64(cmbBranch.SelectedValue) == 0)
                {
                    if (VoucherType == VchType.Sales)
                    {
                        EP.SetError(cmbBranch, "Select Branch");
                        EP.SetIconAlignment(cmbBranch, ErrorIconAlignment.MiddleRight);
                        cmbBranch.Focus();
                    }
                    else if (VoucherType == VchType.Purchase)
                        tempflag = true;
                }
                else
                    tempflag = true;
                return tempflag;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return false;
            }
        }

        private void GridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == ColIndex.BillDate)
            {
                e.Value = Convert.ToDateTime(e.Value).ToString("dd-MMM-yy");

            }
        }

        private void GridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                rowCount = GridView.CurrentCell.RowIndex;
                GridView.CurrentCell.ErrorText = "";

                if (GridView.CurrentCell.ColumnIndex == ColIndex.Amount)
                {
                    if (GridView.CurrentCell.Value != null)
                    {
                        if (ObjFunction.CheckValidAmount(GridView.CurrentCell.Value.ToString()) == false)
                            GridView.CurrentCell.ErrorText = "Please Enter Valid Amount";
                        else if (Convert.ToDouble(GridView.Rows[rowCount].Cells[ColIndex.NetBal].Value) < Convert.ToDouble(GridView.Rows[rowCount].Cells[ColIndex.Amount].Value))
                            GridView.CurrentCell.ErrorText = "Please Enter Valid Amount";
                        else
                        {
                            MovetoNext move2n = new MovetoNext(m2n);
                            BeginInvoke(move2n, new object[] { rowCount, ColIndex.PayType, GridView });
                            //GridView.CurrentCell = GridView[ColIndex.PayType, rowCount];
                        }

                    }
                    else
                    {
                        GridView.CurrentCell.Value = "0.00";
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { rowCount, ColIndex.PayType, GridView });
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private delegate void MovetoNext(int RowIndex, int ColIndex, DataGridView dg);

        private void m2n(int RowIndex, int ColIndex, DataGridView dg)
        {
            dg.CurrentCell = dg.Rows[RowIndex].Cells[ColIndex];
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            cmbPartyName.SelectedValue = "0";
            DataClrscr();
            cmbPartyName.Focus();
            pnlPrint.Visible = false;
            btnPrint.Visible = false;
        }

        private void btnCrOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (CreditValidations() == true)
                {
                    BtnSave.Enabled = true;
                    if (rbBillwise.Checked == true)
                    {
                        GridView.Rows[GridView.CurrentCell.RowIndex].Cells[ColIndex.ChqNo].Value = txtCrCardNo.Text;
                        GridView.Rows[GridView.CurrentCell.RowIndex].Cells[ColIndex.ChqDate].Value = Convert.ToDateTime("01-01-1900");
                        GridView.Rows[GridView.CurrentCell.RowIndex].Cells[ColIndex.BankNo].Value = cmbCrBank.SelectedValue;
                        GridView.Rows[GridView.CurrentCell.RowIndex].Cells[ColIndex.BranchNo].Value = cmbCrBranch.SelectedValue;

                        if (VoucherType == VchType.Sales)
                            GridView.Rows[GridView.CurrentCell.RowIndex].Cells[ColIndex.LedgerNo].Value = dtPayTypeLedger.Rows[2].ItemArray[1].ToString();
                        else
                            GridView.Rows[GridView.CurrentCell.RowIndex].Cells[ColIndex.LedgerNo].Value = ObjFunction.GetComboValue(cmbCrBank);

                        GridView.Rows[GridView.CurrentCell.RowIndex].Cells[ColIndex.PayTypeNo].Value = lstPayType.SelectedValue;
                        GridView.Rows[GridView.CurrentCell.RowIndex].Cells[ColIndex.PayType].Value = lstPayType.Text;
                        pnlCredit.Visible = false;
                        GridView.Focus();
                        if (GridView.CurrentCell.RowIndex < GridView.Rows.Count - 1)
                            GridView.CurrentCell = GridView[ColIndex.Amount, GridView.CurrentCell.RowIndex + 1];
                    }
                    else if (rbBillSelection.Checked == true)
                    {
                        dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.ChqNo].Value = txtCrCardNo.Text;
                        dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.ChqDate].Value = Convert.ToDateTime("01-01-1900");
                        dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.BankNo].Value = cmbCrBank.SelectedValue;
                        dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.BranchNo].Value = cmbCrBranch.SelectedValue;
                        if (VoucherType == VchType.Sales)
                        {
                            dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.LedgerNo].Value = dtPayTypeLedger.Rows[2].ItemArray[1].ToString();
                            LedgNo = Convert.ToInt64(dtPayTypeLedger.Rows[2].ItemArray[1].ToString());
                        }
                        else
                        {
                            dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.LedgerNo].Value = ObjFunction.GetComboValue(cmbCrBank);
                            LedgNo = ObjFunction.GetComboValue(cmbCrBank);
                        }
                        dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.PayTypeNo].Value = ObjFunction.GetComboValue(cmbPayType);
                        dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.PayType].Value = cmbPayType.Text;
                        pnlCredit.Visible = false;
                        dgBillSelection.Focus();
                        if (dgBillSelection.CurrentCell.RowIndex < dgBillSelection.Rows.Count - 1)
                            dgBillSelection.CurrentCell = dgBillSelection[ColIndex.Amount, dgBillSelection.CurrentCell.RowIndex + 1];
                        BtnSave.Focus();
                    }
                    else if (rbAdjustment.Checked == true)
                    {
                        pnlCredit.Visible = false;
                        BtnSave.Focus();
                    }
                    else if (rbAdvance.Checked == true)
                    {
                        pnlCredit.Visible = false;
                        BtnSave.Focus();
                    }

                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnCrCancel_Click(object sender, EventArgs e)
        {
            pnlCredit.Visible = false;
            txtCrCardNo.Text = "";
            cmbCrBank.SelectedIndex = 0;
            cmbCrBranch.SelectedIndex = 0;
            EP.SetError(txtCrCardNo, "");
            //GridView.Focus();
            if (rbBillwise.Checked == true)
            {
                GridView.Focus();
                if (GridView.CurrentCell.RowIndex < GridView.Rows.Count - 1)
                    GridView.CurrentCell = GridView[ColIndex.Amount, GridView.CurrentCell.RowIndex + 1];
                else
                    GridView.CurrentCell = GridView[ColIndex.Amount, GridView.Rows.Count - 1];
            }
            else
            {
                cmbPayType.Focus();
            }
        }

        private bool CreditValidations()
        {
            bool tempflag = false;
            try
            {
                EP.SetError(txtCrCardNo, "");
                EP.SetError(cmbCrBank, "");
                EP.SetError(cmbCrBranch, "");

                if (txtCrCardNo.Text.Trim() == "")
                {
                    EP.SetError(txtCrCardNo, "Enter CreditCardNo");
                    EP.SetIconAlignment(txtCrCardNo, ErrorIconAlignment.MiddleRight);
                    txtCrCardNo.Focus();
                }
                else if (Convert.ToInt64(cmbCrBank.SelectedValue) == 0)
                {
                    EP.SetError(cmbCrBank, "Select Bank");
                    EP.SetIconAlignment(cmbCrBank, ErrorIconAlignment.MiddleRight);
                    cmbCrBank.Focus();
                }
                else if (Convert.ToInt64(cmbCrBranch.SelectedValue) == 0)
                {
                    if (VoucherType == VchType.Sales)
                    {
                        EP.SetError(cmbCrBranch, "Select Branch");
                        EP.SetIconAlignment(cmbCrBranch, ErrorIconAlignment.MiddleRight);
                        cmbCrBranch.Focus();
                    }
                    else if (VoucherType == VchType.Purchase)
                        tempflag = true;
                }
                else
                    tempflag = true;
                return tempflag;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return false;
            }
        }

        private void BindRecords()
        {
            try
            {
                while (dgDataRecord.Rows.Count > 0)
                    dgDataRecord.Rows.RemoveAt(0);
                pnlRecord.Visible = false;
                DataTable dt = new DataTable();
                DataGridView gv = null;
                if (rbBillSelection.Checked == true)
                    gv = dgBillSelection;
                else if (rbBillwise.Checked == true)
                    gv = GridView;
                if (gv.Rows.Count > 0)
                {
                    if (VoucherType == VchType.Sales)
                    {
                        //DataTable dt = ObjFunction.GetDataView("Exec GetCollectionDetails " + ObjFunction.GetComboValue(cmbPartyName) + "," + VoucherType + "," + DBGetVal.CompanyNo + "").Table;
                        dt = ObjFunction.GetDataView("SELECT TVoucherEntry.VoucherDate, TVoucherRefDetails.Amount,  Case When(TVoucherEntry.VoucherTypeCode<>" + VchType.RejectionIn + ")Then MPayType.PayTypeName Else 'Against Sales Return' End AS PayTypeName , TVoucherChqCreditDetails.ChequeNo, TVoucherChqCreditDetails.ChequeDate, " +
                              " TVoucherChqCreditDetails.CreditCardNo,TVoucherEntry.PKVoucherNo,TVoucherRefDetails.PkRefTrnNo,TVoucherChqCreditDetails.PkSrNo " +
                              " FROM         TVoucherEntry INNER JOIN " +
                              " TVoucherDetails ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN " +
                              " TVoucherRefDetails ON TVoucherDetails.PkVoucherTrnNo = TVoucherRefDetails.FkVoucherTrnNo LEFT OUTER JOIN " +
                              " TVoucherChqCreditDetails ON TVoucherEntry.PkVoucherNo = TVoucherChqCreditDetails.FKVoucherNo INNER JOIN " +
                              " MPayType ON TVoucherEntry.PayTypeNo = MPayType.PKPayTypeNo " +
                        " WHERE (TVoucherDetails.LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + ") AND (TVoucherEntry.VoucherTypeCode in( " + VchType.SalesReceipt + "," + VchType.RejectionIn + "))" +
                        "AND (TVoucherRefDetails.RefNo = " + gv.CurrentRow.Cells[ColIndex.RefNo].Value + ") AND " +
                        "(TVoucherEntry.CompanyNo = " + DBGetVal.CompanyNo + ")  " +//ORDER BY TVoucherEntry.VoucherDate
                        " UNION " +
                        " SELECT TVoucherRefDetails.UserDate, TVoucherRefDetails.Amount,  'Against Op Balance' , '', '',  '',0,0,0 " +
                        " FROM TVoucherRefDetails   WHERE (TVoucherRefDetails.LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + ") AND TVoucherRefDetails.TypeOfRef=5 AND (TVoucherRefDetails.RefNo = " + gv.CurrentRow.Cells[ColIndex.RefNo].Value + ") " +
                        " AND (TVoucherRefDetails.CompanyNo = " + DBGetVal.CompanyNo + ")  ORDER BY TVoucherRefDetails.PkRefTrnNo").Table;
                    }
                    else if (VoucherType == VchType.Purchase)
                    {
                        dt = ObjFunction.GetDataView("SELECT TVoucherEntry.VoucherDate, TVoucherRefDetails.Amount,  Case When(TVoucherEntry.VoucherTypeCode<>" + VchType.RejectionOut + ")Then MPayType.PayTypeName Else 'Against Purchase Return' End AS PayTypeName , TVoucherChqCreditDetails.ChequeNo, TVoucherChqCreditDetails.ChequeDate, " +
                              " TVoucherChqCreditDetails.CreditCardNo,TVoucherEntry.PKVoucherNo,TVoucherRefDetails.PkRefTrnNo,TVoucherChqCreditDetails.PkSrNo " +
                              " FROM         TVoucherEntry INNER JOIN " +
                              " TVoucherDetails ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN " +
                              " TVoucherRefDetails ON TVoucherDetails.PkVoucherTrnNo = TVoucherRefDetails.FkVoucherTrnNo LEFT OUTER JOIN " +
                              " TVoucherChqCreditDetails ON TVoucherEntry.PkVoucherNo = TVoucherChqCreditDetails.FKVoucherNo INNER JOIN " +
                              " MPayType ON TVoucherEntry.PayTypeNo = MPayType.PKPayTypeNo " +
                        " WHERE (TVoucherDetails.LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + ") AND (TVoucherEntry.VoucherTypeCode in (" + VchType.PurchasePayment + "," + VchType.RejectionOut + "))" +
                        "AND (TVoucherRefDetails.RefNo = " + gv.CurrentRow.Cells[ColIndex.RefNo].Value + ") AND " +
                        "(TVoucherEntry.CompanyNo = " + DBGetVal.CompanyNo + ") " +//ORDER BY TVoucherEntry.VoucherDate
                        " UNION " +
                        " SELECT TVoucherRefDetails.UserDate, TVoucherRefDetails.Amount,  'Against Op Balance' , '', '',  '',0,0,0 " +
                        " FROM TVoucherRefDetails   WHERE (TVoucherRefDetails.LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + ") AND TVoucherRefDetails.TypeOfRef=5 AND (TVoucherRefDetails.RefNo = " + gv.CurrentRow.Cells[ColIndex.RefNo].Value + ") " +
                        " AND (TVoucherRefDetails.CompanyNo = " + DBGetVal.CompanyNo + ")  ORDER BY TVoucherRefDetails.PkRefTrnNo").Table;
                    }
                }
                if (dt.Rows.Count == 0)
                {
                    if (VoucherType == VchType.Sales)
                        DisplayMessage("No Collection Entry Found");
                    else
                        DisplayMessage("No Payments Entry Found");
                }
                else
                {

                    pnlRecord.Visible = true;
                    dgDataRecord.DataSource = dt.DefaultView;
                    pnlRecord.Location = new System.Drawing.Point(10, 110);
                    dgDataRecord.Columns[1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgDataRecord.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgDataRecord.Columns[6].Visible = false;
                    dgDataRecord.Columns[7].Visible = false;
                    dgDataRecord.Columns[8].Visible = false;
                    dgDataRecord.Columns[0].Width = 94;
                    dgDataRecord.Columns[1].Width = 67;
                    dgDataRecord.Columns[2].Width = 103;
                    dgDataRecord.Columns[3].Width = 79;
                    dgDataRecord.Columns[4].Width = 92;
                    dgDataRecord.Columns[5].Width = 100;

                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void DisplayMessage(string str)
        {
            lblMsg.Visible = true;
            lblMsg.Text = str;
            Application.DoEvents();
            System.Threading.Thread.Sleep(700);
            lblMsg.Visible = false;
        }

        private void DataRecord_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 0 || e.ColumnIndex == 4)
            {
                if (Convert.IsDBNull(e.Value) == false)
                {
                    e.Value = Convert.ToDateTime(e.Value).ToString("dd-MMM-yyyy");
                    if (e.Value.ToString() == "01-Jan-1900")
                        e.Value = "";
                }
            }
        }

        private void DataRecord_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    e.SuppressKeyPress = true;
                    if (pnlRecord.Visible == true)
                    {
                        pnlRecord.Visible = false;
                        if (pnlBillwise.Visible == true)
                        {
                            GridView.CurrentCell = GridView[ColIndex.TotRec, GridView.CurrentCell.RowIndex];
                            GridView.Focus();
                        }
                        else if (pnlBillselection.Visible == true)
                        {
                            dgBillSelection.CurrentCell = dgBillSelection[ColIndex.TotRec, dgBillSelection.CurrentCell.RowIndex];
                            dgBillSelection.Focus();
                        }
                    }
                }
                else if (e.KeyCode == Keys.Delete)
                {
                    if (dgDataRecord.CurrentRow.Index < 0)
                        return;
                    if (OMMessageBox.Show("Are you sure want to delete this record ?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        dbTVoucherEntry = new DBTVaucherEntry();
                        tVoucherEntry = new TVoucherEntry();
                        tVoucherEntry.PkVoucherNo = Convert.ToInt64(dgDataRecord.CurrentRow.Cells[6].Value);
                        dbTVoucherEntry.DeleteAllVoucherEntry(tVoucherEntry);
                        OMMessageBox.Show("Record deleted successfully...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);

                        if (pnlRecord.Visible == true)
                        {
                            pnlRecord.Visible = false;
                            if (pnlBillwise.Visible == true)
                            {
                                GridView.CurrentCell = GridView[ColIndex.TotRec, GridView.CurrentCell.RowIndex];
                                GridView.Focus();
                            }
                            else if (pnlBillselection.Visible == true)
                            {
                                dgBillSelection.CurrentCell = dgBillSelection[ColIndex.TotRec, dgBillSelection.CurrentCell.RowIndex];
                                dgBillSelection.Focus();
                            }
                        }
                        cmbPartyName_Leave(sender, new EventArgs());
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void GridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (GridView.CurrentCell.ColumnIndex == ColIndex.Amount)
            {
                TextBox txtAmt = (TextBox)e.Control;
                txtAmt.TextChanged += new EventHandler(txtAmt_TextChanged);
            }
        }

        private void txtAmt_TextChanged(object sender, EventArgs e)
        {
            //ObjFunction.SetMasked((TextBox)sender, 2);
            if (GridView.CurrentCell.ColumnIndex == ColIndex.Amount)
            {
                ObjFunction.SetMasked((TextBox)sender, 2, 9, JitFunctions.MaskedType.NotNegative);
            }
        }

        private void btnAdvanceSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Master.AdvancedSearch Adsch = new Kirana.Master.AdvancedSearch(grpNo);
                ObjFunction.OpenForm(Adsch);
                if (Adsch.LedgerNo != 0)
                {
                    cmbPartyName.SelectedValue = Adsch.LedgerNo;
                    cmbPartyName.Focus();
                    Adsch.Close();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void rbCheck_Changed()
        {
            try
            {
                txtAmount.Visible = true;
                label14.Visible = true;
                if (rbBillwise.Checked == true)
                {
                    cmbPayType.Enabled = true;
                    label5.Visible = true;
                    pnlBillwise.Visible = true;
                    txtAmount.Visible = false;
                    label14.Visible = false;
                    pnlBillselection.Visible = false;
                    pnlAdjustment.Visible = false;
                    PnlAgainstAdv.Visible = false;
                    pnlBillwise.Location = new System.Drawing.Point(panel1.Location.X, 110);
                    BtnSave.Enabled = true;
                    pnlPayT.Visible = false;
                    txtAmount.Text = "";
                    for (int i = 0; i < GridView.Rows.Count; i++)
                    {
                        GridView.Rows[i].Cells[5].Value = "0.00";
                    }
                    cmbPayType.SelectedIndex = 0;
                    if (GridView.Rows.Count > 0)
                        GridView.CurrentCell = GridView.Rows[0].Cells[5];
                    GridView.Focus();
                }
                else if (rbBillSelection.Checked == true)
                {
                    cmbPayType.Enabled = true;
                    label5.Visible = true;
                    pnlBillselection.Visible = true;
                    pnlBillwise.Visible = false;
                    pnlAdjustment.Visible = false;
                    PnlAgainstAdv.Visible = false;
                    pnlBillselection.Location = new System.Drawing.Point(panel1.Location.X, 110);
                    BtnSave.Enabled = false;
                    pnlPayT.Visible = true;
                    if (VoucherType == VchType.Sales)
                        ObjFunction.FillComb(cmbPayType, "Select PKPayTypeNo,PayTypeName from MPayType where ControlUnder Not in(1,3)");
                    else
                        ObjFunction.FillComb(cmbPayType, "Select PKPayTypeNo,PayTypeName from MPayType where PKPayTypeNo in(2,4,5)");
                    txtAmount.Text = "";
                    for (int i = 0; i < dgBillSelection.Rows.Count; i++)
                    {
                        dgBillSelection.Rows[i].Cells[ColIndex.Amount].Value = "0.00";
                    }
                    cmbPayType.SelectedIndex = 0;
                    if (dgBillSelection.Rows.Count > 0)
                        cmbPayType_KeyDown(cmbPayType, new KeyEventArgs(Keys.Enter));
                    if (dgBillSelection.Rows.Count > 0)
                        dgBillSelection.CurrentCell = dgBillSelection.Rows[0].Cells[ColIndex.Amount];
                    dgBillSelection.Focus();
                }
                else if (rbAdjustment.Checked == true)
                {
                    cmbPayType.Enabled = false;
                    label5.Visible = false;
                    cmbPayType.SelectedIndex = 0;
                    pnlBillselection.Visible = false;
                    PnlAgainstAdv.Visible = false;
                    pnlBillwise.Visible = false;
                    lblAdvBal.Visible = false;
                    lblAdvlbl.Visible = false;
                    lblAdvOpBal.Visible = false;
                    txtAdvAmt.Visible = false;
                    lblOpeningBal.Visible = true;
                    lblBalance.Visible = true;
                    pnlAdjustment.Visible = true;
                    dgAdjustOpening.Visible = false;
                    txtAmount.Visible = false;
                    label14.Visible = false;
                    pnlAdjustment.Location = new System.Drawing.Point(12, 110);
                    BtnSave.Enabled = false;
                    txtAdjustment.Text = "";
                    pnlPayT.Visible = true;
                    pnlchq.Visible = false;
                    pnlCredit.Visible = false;
                    txtRemark.Visible = false;
                    lblRemark.Visible = false;
                    if (VoucherType == VchType.Sales)
                        ObjFunction.FillComb(cmbPayType, "Select PKPayTypeNo,PayTypeName from MPayType where ControlUnder not in(1,3)");
                    else
                        ObjFunction.FillComb(cmbPayType, "Select PKPayTypeNo,PayTypeName from MPayType where PKPayTypeNo in(2,4,5)");

                    double OpBal = 0;
                    OpBal = ObjQry.ReturnDouble("Select case when (signcode=2) then isNull(OpeningBalance,0) else isNull(OpeningBalance,0)*-1 end as OpeningBalance From MLedger Where LedgerNo =" + ObjFunction.GetComboValue(cmbPartyName) + "", CommonFunctions.ConStr);
                    double TotBal = 0;
                    //In below query we used reverse logic of signcode as this is adjsted entry's values
                    TotBal = OpBal + ObjQry.ReturnDouble("Select isNull(Sum(case when (signcode=1) then isNull(Amount,0) else isNull(Amount,0)*-1 end),0) from TVoucherRefDetails where LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " and TypeOfRef=5 AND REFNO <> 0", CommonFunctions.ConStr);
                    //In below query we used forward logic of signcode as this is direct entry's values
                    TotBal = TotBal + ObjQry.ReturnDouble("Select isNull(Sum(case when (signcode=2) then isNull(Amount,0) else isNull(Amount,0)*-1 end),0) from TVoucherRefDetails where LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " and TypeOfRef=5 AND REFNO = 0", CommonFunctions.ConStr);

                    OpBalPendingAmt = Math.Abs(TotBal);
                    OpBalAdjustedAmt = 0;

                    if (OpBal < 0 && VoucherType == VchType.Sales)
                    {
                        lblOpeningBal.Text = "Opening Balance  :    " + Math.Abs(OpBal).ToString("0.00") + "   To Receive";
                        lblAdjlbl.Visible = true;
                        txtAdjustment.Visible = true;
                        dgAdjustOpening.Visible = false;
                        BtnSave.Enabled = false;
                    }
                    else if (OpBal > 0 && VoucherType == VchType.Sales)
                    {
                        lblOpeningBal.Text = "Opening Balance  :    " + Math.Abs(OpBal).ToString("0.00") + "   To Pay";
                        lblAdjlbl.Visible = false;
                        txtAdjustment.Visible = false;
                        dgAdjustOpening.Visible = true;
                        BtnSave.Enabled = true;
                    }
                    else if (OpBal > 0 && VoucherType == VchType.Purchase)
                    {
                        lblOpeningBal.Text = "Opening Balance  :    " + Math.Abs(OpBal).ToString("0.00") + "   To Pay";
                        lblAdjlbl.Visible = true;
                        txtAdjustment.Visible = true;
                        dgAdjustOpening.Visible = false;
                        BtnSave.Enabled = false;
                    }
                    else if (OpBal < 0 && VoucherType == VchType.Purchase)
                    {
                        lblOpeningBal.Text = "Opening Balance  :    " + Math.Abs(OpBal).ToString("0.00") + "   To Receive";

                        lblAdjlbl.Visible = false;
                        txtAdjustment.Visible = false;
                        dgAdjustOpening.Visible = true;
                        BtnSave.Enabled = true;
                    }
                    else
                        lblOpeningBal.Text = "Opening Balance  :    " + Math.Abs(OpBal).ToString("0.00");
                    lblBalance.Text = "Balance :    " + Math.Abs(TotBal).ToString("0.00");

                    txtAdjustment.Focus();
                    txtAdjustment.Focus();
                }
                else if (rbAdvance.Checked == true)
                {
                    cmbPayType.Enabled = false;
                    label5.Visible = false;
                    cmbPayType.SelectedIndex = 0;
                    PnlAgainstAdv.Visible = false;
                    pnlBillselection.Visible = false;
                    pnlBillwise.Visible = false;
                    pnlAdjustment.Visible = true;
                    dgAdjustOpening.Visible = false;
                    lblAdvBal.Visible = true;
                    lblAdvlbl.Visible = true;
                    txtRemark.Visible = true;
                    txtRemark.Text = "";
                    lblRemark.Visible = true;
                    lblAdvOpBal.Visible = true;
                    txtAdvAmt.Visible = true;
                    lblOpeningBal.Visible = false;
                    lblBalance.Visible = false;
                    lblAdjlbl.Visible = false;
                    txtAdjustment.Visible = false;
                    txtAmount.Visible = false;
                    label14.Visible = false;
                    pnlAdjustment.Location = new System.Drawing.Point(12, 110);
                    BtnSave.Enabled = false;
                    txtAdvAmt.Text = "";
                    pnlPayT.Visible = true;
                    pnlchq.Visible = false;
                    pnlCredit.Visible = false;
                    if (VoucherType == VchType.Sales)
                        ObjFunction.FillComb(cmbPayType, "Select PKPayTypeNo,PayTypeName from MPayType where ControlUnder not in(1,3)");
                    else
                        ObjFunction.FillComb(cmbPayType, "Select PKPayTypeNo,PayTypeName from MPayType where PKPayTypeNo in(2,4,5)");
                    double OpBal = ObjQry.ReturnDouble("Select IsNull(Sum(Amount),0) From TVoucherRefDetails Where LedgerNo =" + ObjFunction.GetComboValue(cmbPartyName) + " and TypeOfRef=1", CommonFunctions.ConStr);
                    double totRecAdv = ObjQry.ReturnDouble("Select IsNull(Sum(Amount),0) From TVoucherRefDetails where LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " and TypeOfRef=6", CommonFunctions.ConStr);

                    lblAdvOpBal.Text = "Total Advance         : " + OpBal.ToString("0.00");
                    lblAdvBal.Text = "Balance :    " + (OpBal - totRecAdv).ToString("0.00");
                    txtAdvAmt.Focus();

                }
                else if (rbAgainstAdv.Checked == true || rbAdjSalesReturn.Checked == true)
                {
                    cmbPayType.Enabled = true;
                    label5.Visible = false;
                    pnlBillwise.Visible = false;
                    txtAmount.Visible = false;
                    label14.Visible = false;
                    pnlBillselection.Visible = false;
                    pnlAdjustment.Visible = false;
                    PnlAgainstAdv.Visible = true;
                    PnlAgainstAdv.Location = new System.Drawing.Point(panel1.Location.X, 110);
                    BtnSave.Enabled = true;
                    pnlPayT.Visible = false;
                    dgBill.Visible = false;
                    txtAmount.Text = "";


                    if (dgAdvance.Rows.Count > 0)
                        dgAdvance.CurrentCell = dgAdvance[8, 0];
                    //dgAdvance.CurrentCell = dgAdvance.Rows[0].Cells[ColIndex.Amount];
                    dgAdvance.Focus();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

        }

        private void cmbPayType_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    BtnSave.Enabled = true;
                    e.SuppressKeyPress = true;
                    Panel p = new Panel();
                    if (pnlBillselection.Visible == true)
                        p = pnlBillselection;
                    else if (pnlAdjustment.Visible == true)
                        p = pnlAdjustment;
                    else
                        p = pnlBillwise;
                    txtChqNo.Text = "";
                    txtCrCardNo.Text = "";
                    cmbBank.SelectedIndex = 0;
                    cmbBranch.SelectedIndex = 0;
                    cmbCrBank.SelectedIndex = 0;
                    cmbCrBranch.SelectedIndex = 0;
                    pnlCredit.Visible = false;
                    pnlchq.Visible = false;
                    // GridView.Rows[GridView.CurrentCell.RowIndex].Cells[ColIndex.PayType].Value = lstPayType.Text;
                    //manali
                    long ControlUnder = ObjQry.ReturnLong("Select ControlUnder From MPayType Where PKPayTypeNo=" + ObjFunction.GetComboValue(cmbPayType) + "", CommonFunctions.ConStr);
                    if (ControlUnder == 1 || ControlUnder == 4 || ControlUnder == 5)
                    {
                        if (ControlUnder == 4 || ControlUnder == 5)
                        {
                            if (ControlUnder == 4)
                            {
                                if (rbAdjustment.Checked == true || rbAdvance.Checked == true)
                                {
                                    pnlchq.Location = new Point(pnlAdjustment.Location.X + 10, pnlAdjustment.Location.Y + txtAdjustment.Location.Y + 40);
                                    pnlchq.Visible = true;
                                    pnlchq.BringToFront();
                                    txtChqNo.Focus();
                                }
                                else
                                {
                                    int y = pnlMain.Location.Y + p.Location.Y + dgBillSelection.Location.Y;
                                    y = y + ((p.Height - dgBillSelection.Height) / 2);
                                    int x = (dgBillSelection.Width - pnlchq.Width) / 2;
                                    pnlchq.Location = new Point(dgBillSelection.Location.X + x, y + 10);
                                    pnlchq.Visible = true;
                                    pnlchq.BringToFront();
                                    txtChqNo.Focus();
                                }
                            }
                            else if (ControlUnder == 5)
                            {
                                if (rbAdvance.Checked == true || rbAdjustment.Checked == true)
                                {
                                    pnlCredit.Location = new Point(pnlAdjustment.Location.X + 10, pnlAdjustment.Location.Y + txtAdjustment.Location.Y + 40);
                                    pnlCredit.Visible = true;
                                    pnlCredit.BringToFront();
                                    txtCrCardNo.Focus();
                                }
                                else
                                {
                                    int y = pnlMain.Location.Y + p.Location.Y + dgBillSelection.Location.Y;
                                    y = y + ((p.Height - dgBillSelection.Height) / 2);
                                    int x = (dgBillSelection.Width - pnlCredit.Width) / 2;
                                    pnlCredit.Location = new Point(dgBillSelection.Location.X + x, y + 10);
                                    pnlCredit.Visible = true;
                                    pnlCredit.BringToFront();
                                    txtCrCardNo.Focus();
                                }
                            }
                        }
                        {
                            if (rbBillSelection.Checked == true)
                            {
                                dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.PayType].Value = cmbPayType.Text;
                                dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.LedgerNo].Value = dtPayTypeLedger.Rows[0].ItemArray[1].ToString();
                                LedgNo = Convert.ToInt64(dtPayTypeLedger.Rows[0].ItemArray[1].ToString());
                                dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.PayTypeNo].Value = ObjFunction.GetComboValue(cmbPayType);
                                //pnlPayT.Visible = false;

                                //if (dgBillSelection.CurrentCell.RowIndex < dgBillSelection.Rows.Count - 1)
                                //{
                                //    dgBillSelection.Focus();
                                //    dgBillSelection.CurrentCell = dgBillSelection[ColIndex.Amount, dgBillSelection.CurrentCell.RowIndex + 1];
                                //}
                                //else
                                //{
                                //    BtnSave.Focus();
                                //}
                            }
                            //BtnSave.Focus();
                        }
                    }
                    else
                    {
                        if (rbBillSelection.Checked == true)
                        {
                            dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.PayType].Value = cmbPayType.Text;
                            dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.LedgerNo].Value = dtPayTypeLedger.Rows[0].ItemArray[1].ToString();
                            LedgNo = Convert.ToInt64(dtPayTypeLedger.Rows[0].ItemArray[1].ToString());
                            dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.PayTypeNo].Value = ObjFunction.GetComboValue(cmbPayType);
                            //pnlPayT.Visible = false;

                            if (dgBillSelection.CurrentCell.RowIndex < dgBillSelection.Rows.Count - 1)
                            {
                                dgBillSelection.Focus();
                                dgBillSelection.CurrentCell = dgBillSelection[ColIndex.Amount, dgBillSelection.CurrentCell.RowIndex + 1];
                            }
                            else
                            {
                                BtnSave.Focus();
                            }
                        }
                        BtnSave.Focus();
                    }
                    //manali end
                    //if (ObjFunction.GetComboValue(cmbPayType) == 4)//For Cheque
                    //{
                    //    //pnlPayT.Visible = false;
                    //    if (rbAdjustment.Checked == true || rbAdvance.Checked == true)
                    //    {
                    //        //int y = pnlMain.Location.Y + p.Location.Y + txtAdjustment.Location.Y;
                    //        //y = y + ((p.Height - txtAdjustment.Height) / 2);
                    //        //int x = (pnlAdjustment.) / 2;
                    //        pnlchq.Location = new Point(pnlAdjustment.Location.X + 10, pnlAdjustment.Location.Y + txtAdjustment.Location.Y + 40);
                    //        pnlchq.Visible = true;
                    //        pnlchq.BringToFront();
                    //        //pnlchq.Location = new System.Drawing.Point(2, 110);
                    //        txtChqNo.Focus();
                    //    }
                    //    else
                    //    {
                    //        int y = pnlMain.Location.Y + p.Location.Y + dgBillSelection.Location.Y;
                    //        y = y + ((p.Height - dgBillSelection.Height) / 2);
                    //        int x = (dgBillSelection.Width - pnlchq.Width) / 2;
                    //        pnlchq.Location = new Point(dgBillSelection.Location.X + x, y + 10);
                    //        pnlchq.Visible = true;
                    //        pnlchq.BringToFront();
                    //        //pnlchq.Location = new System.Drawing.Point(2, 110);
                    //        txtChqNo.Focus();
                    //    }
                    //}
                    //else if (ObjFunction.GetComboValue(cmbPayType) == 5)//credit Card
                    //{
                    //    if (rbAdvance.Checked == true || rbAdjustment.Checked == true)
                    //    {
                    //        pnlCredit.Location = new Point(pnlAdjustment.Location.X + 10, pnlAdjustment.Location.Y + txtAdjustment.Location.Y + 40);
                    //        pnlCredit.Visible = true;
                    //        pnlCredit.BringToFront();
                    //        txtCrCardNo.Focus();
                    //    }
                    //    else
                    //    {
                    //        int y = pnlMain.Location.Y + p.Location.Y + dgBillSelection.Location.Y;
                    //        y = y + ((p.Height - dgBillSelection.Height) / 2);
                    //        int x = (dgBillSelection.Width - pnlCredit.Width) / 2;
                    //        pnlCredit.Location = new Point(dgBillSelection.Location.X + x, y + 10);
                    //        //pnlPayTypeBill.Visible = false;
                    //        pnlCredit.Visible = true;
                    //        pnlCredit.BringToFront();
                    //        //pnlCredit.Location = new System.Drawing.Point(3, 110);
                    //        txtCrCardNo.Focus();
                    //    }
                    //}
                    //else
                    //{
                    //    if (rbBillSelection.Checked == true)
                    //    {
                    //        dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.PayType].Value = cmbPayType.Text;
                    //        dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.LedgerNo].Value = dtPayTypeLedger.Rows[0].ItemArray[1].ToString();
                    //        LedgNo = Convert.ToInt64(dtPayTypeLedger.Rows[0].ItemArray[1].ToString());
                    //        dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.PayTypeNo].Value = ObjFunction.GetComboValue(cmbPayType);
                    //        //pnlPayT.Visible = false;

                    //        if (dgBillSelection.CurrentCell.RowIndex < dgBillSelection.Rows.Count - 1)
                    //        {
                    //            dgBillSelection.Focus();
                    //            dgBillSelection.CurrentCell = dgBillSelection[ColIndex.Amount, dgBillSelection.CurrentCell.RowIndex + 1];
                    //        }
                    //        else
                    //        {
                    //            BtnSave.Focus();
                    //        }
                    //    }
                    //    BtnSave.Focus();
                    //}

                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void rbBillwise_CheckedChanged(object sender, EventArgs e)
        {
            rbCheck_Changed();
        }

        private void rbBillSelection_CheckedChanged(object sender, EventArgs e)
        {
            rbCheck_Changed();
        }

        private void dgBillSelection_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F4)
                {
                    e.SuppressKeyPress = true;
                    BindRecords();
                    dgDataRecord.Focus();
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    e.SuppressKeyPress = true;
                    cmbPayType.Focus();
                }
                else if (e.KeyCode == Keys.Space)
                {
                    if (dgBillSelection.CurrentCell == dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.Chk])
                    {
                        if (Convert.ToBoolean(dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.Chk].FormattedValue) == true)
                        {
                            dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.Chk].Value = false;
                            dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.Amount].Value = "0.00";
                        }
                        else
                        {
                            dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.Chk].Value = true;
                            dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.Amount].Value = dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.NetBal].Value;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgBillSelection_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == ColIndex.BillDate)
            {
                e.Value = Convert.ToDateTime(e.Value).ToString("dd-MMM-yy");
            }
        }

        private void txtAmt_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                DataGridView gv = null;
                if (rbBillwise.Checked == true) gv = GridView;
                else if (rbBillSelection.Checked == true) gv = dgBillSelection;
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    if (ObjFunction.CheckValidAmount(txtAmount.Text.Replace("-", "")) == true)
                    {
                        if (Convert.ToDouble(txtAmount.Text.Trim()) <= Convert.ToDouble(lblAmount.Text))
                        {
                            double Amount = Convert.ToDouble(txtAmount.Text.Trim());
                            for (int i = 0; i < gv.Rows.Count; i++)
                            {
                                if (Convert.ToDouble(gv.Rows[i].Cells[ColIndex.NetBal].Value) <= Amount)
                                {
                                    gv.Rows[i].Cells[ColIndex.Amount].Value = gv.Rows[i].Cells[ColIndex.NetBal].Value;
                                    gv.Rows[i].Cells[ColIndex.Chk].Value = "True";
                                    Amount = Amount - Convert.ToDouble(gv.Rows[i].Cells[ColIndex.NetBal].Value);
                                }
                                else if (Amount != 0)
                                {
                                    if (Convert.ToDouble(gv.Rows[i].Cells[ColIndex.NetBal].Value) <= Amount)
                                    {
                                        gv.Rows[i].Cells[ColIndex.Amount].Value = Convert.ToDouble(gv.Rows[i].Cells[ColIndex.NetBal].Value);
                                        Amount = Amount - Convert.ToDouble(gv.Rows[i].Cells[ColIndex.Amount].Value);
                                        gv.Rows[i].Cells[ColIndex.Chk].Value = "True";
                                    }
                                    else
                                    {
                                        gv.Rows[i].Cells[ColIndex.Amount].Value = Amount.ToString("0.00");
                                        gv.Rows[i].Cells[ColIndex.Chk].Value = "True";
                                        Amount = 0;
                                    }
                                }
                                else
                                {
                                    gv.Rows[i].Cells[ColIndex.Amount].Value = "0.00";
                                    gv.Rows[i].Cells[ColIndex.Chk].Value = false;
                                }
                            }
                            gv.CurrentCell = gv[ColIndex.Amount, 0];
                            gv.Focus();
                        }
                        else
                        {
                            MessageBox.Show("Enter Amount less than Total Amount", CommonFunctions.ErrorTitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                        MessageBox.Show("Enter Valid Amount", CommonFunctions.ErrorTitle, MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void rbAdjustment_CheckedChanged(object sender, EventArgs e)
        {
            rbCheck_Changed();
        }

        private void txtAdjustment_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    cmbPayType.Enabled = false;
                    if (txtAdjustment.Text != "")
                    {
                        if (Convert.ToDouble(txtAdjustment.Text.Replace("-", "")) < 0)
                        {
                            OMMessageBox.Show("Enter Valid Amount", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            txtAdjustment.Focus();
                        }
                        else if (Convert.ToDouble(txtAdjustment.Text) == 0)
                        {
                            OMMessageBox.Show("Enter Amount", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            txtAdjustment.Focus();
                        }
                        else if (Convert.ToDouble(lblBalance.Text.Replace("Balance :    ", "").Replace("-", "")) < Convert.ToDouble(txtAdjustment.Text.Replace("-", "")))
                        {
                            OMMessageBox.Show("Enter Amount less than Balance Amount", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            txtAdjustment.Focus();
                        }
                        else
                        {
                            cmbPayType.Enabled = true;
                            cmbPayType.Focus();
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void txtAdjustment_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(((TextBox)sender), 2, 10, JitFunctions.MaskedType.NotNegative);

        }

        private void cmbPartyName_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //if (cmbPartyName.SelectedIndex != 0)
            //{
            //    processPartyNameChange();
            //}
        }

        private void cmbPartyName_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (cmbPartyName.SelectedIndex != 0)
            //{
            //    processPartyNameChange();
            //}
        }

        private bool isProcessingPartyNameChange = false;
        private void cmbPartyName_Leave(object sender, EventArgs e)
        {
            if (isProcessingPartyNameChange == false)
            {
                isProcessingPartyNameChange = true;
                if (cmbPartyName.SelectedIndex != 0)
                {
                    processPartyNameChange();
                }
                else DataClrscr();
                isProcessingPartyNameChange = false;
            }
        }

        private void dgBillSelection_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            TextBox txtAmt = (TextBox)e.Control;
            txtAmt.TextChanged += new EventHandler(txtAmt_TextChanged);
        }

        private void txtAmount_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(txtAmount, 2, 9, JitFunctions.MaskedType.NotNegative);
        }

        private void txtChqNo_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMaskedNumeric(txtChqNo);
        }

        private void cmbPartyName_MouseClick(object sender, MouseEventArgs e)
        {
            // if (ObjFunction.GetComboValue(cmbPartyName)>0)
            //   cmbPartyName_KeyDown(cmbPartyName, new KeyEventArgs(Keys.Enter));
        }

        private void txtCrCardNo_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMaskedNumeric(txtCrCardNo);
        }

        private void rbAdvance_CheckedChanged(object sender, EventArgs e)
        {
            rbCheck_Changed();
        }

        private void txtAdvAmt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                cmbPayType.Enabled = false;
                if (txtAdvAmt.Text != "")
                {
                    if (Convert.ToDouble(txtAdvAmt.Text.Replace("-", "")) < 0)
                    {
                        OMMessageBox.Show("Enter Valid Amount", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        txtAdvAmt.Focus();
                    }
                    else if (Convert.ToDouble(txtAdvAmt.Text) == 0)
                    {
                        OMMessageBox.Show("Enter Amount", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        txtAdvAmt.Focus();
                    }
                    //else if (Convert.ToDouble(lblAdvBal.Text.Replace("Balance :    ", "").Replace("-", "")) < Convert.ToDouble(txtAdvAmt.Text.Replace("-", "")))
                    //{
                    //    OMMessageBox.Show("Enter Amount less than Balance Amount", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                    //    txtAdvAmt.Focus();
                    //}
                    else
                    {
                        txtRemark.Focus();
                        //cmbPayType.Enabled = true;
                        //cmbPayType.Focus();
                    }
                }
            }
        }

        private void txtAdvAmt_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(((TextBox)sender), 2, 10, JitFunctions.MaskedType.NotNegative);
        }

        private void rbAgainstAdv_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                rbCheck_Changed();
                label15.Visible = true;
                dgAdvance.Rows.Clear();
                DataTable dtAdv = new DataTable();
                if (VoucherType == VchType.Sales)
                {
                    if (rbAgainstAdv.Checked == true)
                        dtAdv = ObjFunction.GetDataView("Exec GetAdvanceCollectionDetails " + ObjFunction.GetComboValue(cmbPartyName) + "," + VchType.SalesReceipt + "," + DBGetVal.CompanyNo + ",1").Table;
                    else if (rbAdjSalesReturn.Checked == true)
                        dtAdv = ObjFunction.GetDataView("Exec GetAdvanceCollectionDetails " + ObjFunction.GetComboValue(cmbPartyName) + "," + VchType.RejectionIn + "," + DBGetVal.CompanyNo + ",3").Table;

                }
                else if (VoucherType == VchType.Purchase)
                {
                    if (rbAgainstAdv.Checked == true)
                        dtAdv = ObjFunction.GetDataView("Exec GetAdvanceCollectionDetails " + ObjFunction.GetComboValue(cmbPartyName) + "," + VchType.PurchasePayment + "," + DBGetVal.CompanyNo + ",1").Table;
                    else if (rbAdjSalesReturn.Checked == true)
                        dtAdv = ObjFunction.GetDataView("Exec GetAdvanceCollectionDetails " + ObjFunction.GetComboValue(cmbPartyName) + "," + VchType.RejectionOut + "," + DBGetVal.CompanyNo + ",3").Table;
                }

                for (int i = 0; i < dtAdv.Rows.Count; i++)
                {
                    dgAdvance.Rows.Add();
                    for (int j = 0; j < dgAdvance.Columns.Count - 1; j++)
                    {
                        dgAdvance.Rows[i].Cells[j].Value = dtAdv.Rows[i].ItemArray[j];
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgAdvance_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 0)
                e.Value = e.RowIndex + 1;
            if (e.ColumnIndex == 1)
            {
                if (e.Value != null)
                    e.Value = Convert.ToDateTime(e.Value).ToString("dd-MMM-yyyy");
            }
        }

        private void dgAdvance_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            //if (dgAdvance.CurrentCell.ColumnIndex == 8)
            //{
            //    CheckBox Chk = (CheckBox)e.Control;
            //    Chk.CheckedChanged += new EventHandler(Chk_CheckedChanged);
            //}
        }

        private void Chk_CheckedChanged(object sender, EventArgs e)
        {
            if (dgAdvance.CurrentCell.ColumnIndex == 8)
            {
                if (Convert.ToBoolean(dgAdvance.Rows[dgAdvance.CurrentCell.RowIndex].Cells[8].FormattedValue) == true)
                {
                    for (int i = 0; i < dgAdvance.Rows.Count; i++)
                    {
                        if (i != dgAdvance.CurrentCell.ColumnIndex)
                            dgAdvance.Rows[i].Cells[8].Value = false;
                    }
                }

            }
        }

        private void dgAdvance_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //if (e.ColumnIndex == 8)
            //{
            //    int rowIndex = e.RowIndex;
            //    if (Convert.ToBoolean(dgAdvance.Rows[rowIndex].Cells[8].FormattedValue) == true)
            //    {
            //        for (int i = 0; i < dgAdvance.Rows.Count; i++)
            //        {
            //            if (i != e.RowIndex)
            //                dgAdvance.Rows[i].Cells[8].Value = false;
            //        }
            //        TolAdvadjAmt = Convert.ToDouble(dgAdvance.Rows[rowIndex].Cells[5].Value);
            //        AdvAdjAmt = 0;
            //        Amt = 0;
            //        Advflag = false;
            //        BindGridAdv();
            //        dgBill.Visible = true;
            //    }
            //    else
            //    {
            //        TolAdvadjAmt = 0;
            //        AdvAdjAmt = 0;
            //        Amt = 0;
            //        Advflag = false;
            //        dgBill.Visible = false;
            //    }
            //}
        }

        private void dgBill_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == ColIndex.BillDate)
            {
                e.Value = Convert.ToDateTime(e.Value).ToString("dd-MMM-yy");
            }
        }

        private void dgAdvance_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

            //if (e.ColumnIndex == 8 && dgAdvance.Rows.Count>0)
            //{
            //    if (Convert.ToBoolean(dgAdvance.Rows[e.RowIndex].Cells[8].FormattedValue) == true)
            //    {
            //        for (int i = 0; i < dgAdvance.Rows.Count; i++)
            //        {
            //            if (i != e.RowIndex)
            //                dgAdvance.Rows[i].Cells[8].Value = false;
            //        }
            //        dgBill.Visible = true;
            //    }
            //    else
            //        dgBill.Visible = false;
            //}
        }

        private void dgBill_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //if (e.ColumnIndex == ColIndex.Chk)
            //{
            //    int rowIndex = e.RowIndex;
            //    if (Convert.ToBoolean(dgBill.Rows[rowIndex].Cells[ColIndex.Chk].FormattedValue) == true)
            //    {
            //        if (AdvAdjAmt <= 0 && (Amt != TolAdvadjAmt))
            //            AdvAdjAmt = Convert.ToDouble(dgBill.Rows[rowIndex].Cells[ColIndex.NetBal].Value);
            //        if (AdvAdjAmt <= TolAdvadjAmt && Advflag == false && (Amt != TolAdvadjAmt))
            //        {
            //            if (AdvAdjAmt <= Convert.ToDouble(dgBill.Rows[rowIndex].Cells[ColIndex.NetBal].Value))
            //            {
            //                dgBill.Rows[rowIndex].Cells[ColIndex.Amount].Value = AdvAdjAmt.ToString("0.00");
            //                Amt = Amt + Convert.ToDouble(dgBill.Rows[rowIndex].Cells[ColIndex.Amount].Value);
            //            }
            //            else
            //            {
            //                dgBill.Rows[rowIndex].Cells[ColIndex.Amount].Value = dgBill.Rows[rowIndex].Cells[ColIndex.NetBal].Value;
            //                Amt = Amt + Convert.ToDouble(dgBill.Rows[rowIndex].Cells[ColIndex.Amount].Value);
            //            }
            //            AdvAdjAmt = TolAdvadjAmt - Amt;
            //        }
            //        else if (AdvAdjAmt > TolAdvadjAmt)
            //        {
            //            dgBill.Rows[rowIndex].Cells[ColIndex.Amount].Value = TolAdvadjAmt.ToString("0.00");
            //            AdvAdjAmt = TolAdvadjAmt;
            //            Advflag = true;
            //        }
            //        else
            //            dgBill.Rows[rowIndex].Cells[ColIndex.Chk].Value = false;
            //    }
            //    else
            //    {
            //        AdvAdjAmt = AdvAdjAmt - Convert.ToDouble(dgBill.Rows[rowIndex].Cells[ColIndex.Amount].Value);
            //        Amt = Amt - Convert.ToDouble(dgBill.Rows[rowIndex].Cells[ColIndex.Amount].Value);
            //        dgBill.Rows[rowIndex].Cells[ColIndex.Amount].Value = "0.00";
            //    }
            //}
        }

        private void BindGridAdv()
        {
            try
            {
                dgBill.Rows.Clear();
                DataTable dt = ObjFunction.GetDataView("Exec GetAdvanceCollectionBillDetails " + ObjFunction.GetComboValue(cmbPartyName) + "," + VoucherType + "," + DBGetVal.CompanyNo + "").Table;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dgBill.Rows.Add();
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        dgBill.Rows[i].Cells[j].Value = dt.Rows[i].ItemArray[j];
                    }
                    dgBill.Rows[i].Cells[ColIndex.Chk].Value = false;
                }

                dgBill.Columns[ColIndex.Amount].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgBill.Columns[ColIndex.BillAmt].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgBill.Columns[ColIndex.NetBal].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgBill.Columns[ColIndex.TotRec].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgBill.Columns[ColIndex.Chk].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                #region For OpeningBalance Adjustment
                dgAdjustOpening.Rows.Clear();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dgAdjustOpening.Rows.Add();
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        dgAdjustOpening.Rows[i].Cells[j].Value = dt.Rows[i].ItemArray[j];
                    }
                    dgAdjustOpening.Rows[i].Cells[ColIndex.Chk].Value = false;
                }

                dgAdjustOpening.Columns[ColIndex.Amount].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgAdjustOpening.Columns[ColIndex.BillAmt].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgAdjustOpening.Columns[ColIndex.NetBal].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgAdjustOpening.Columns[ColIndex.TotRec].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgAdjustOpening.Columns[ColIndex.Chk].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                #endregion
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void rbAdjSalesReturn_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                rbCheck_Changed();
                label15.Visible = true;


                dgAdvance.Rows.Clear();
                DataTable dtAdv = new DataTable();
                if (VoucherType == VchType.Sales)
                {
                    if (rbAgainstAdv.Checked == true)
                        dtAdv = ObjFunction.GetDataView("Exec GetAdvanceCollectionDetails " + ObjFunction.GetComboValue(cmbPartyName) + "," + VchType.SalesReceipt + "," + DBGetVal.CompanyNo + ",1").Table;
                    else if (rbAdjSalesReturn.Checked == true)
                        dtAdv = ObjFunction.GetDataView("Exec GetAdvanceCollectionDetails " + ObjFunction.GetComboValue(cmbPartyName) + "," + VchType.RejectionIn + "," + DBGetVal.CompanyNo + ",3").Table;

                    label15.Text = "Select the Bill to be Adjust Sales Return";
                }
                else if (VoucherType == VchType.Purchase)
                {
                    if (rbAgainstAdv.Checked == true)
                        dtAdv = ObjFunction.GetDataView("Exec GetAdvanceCollectionDetails " + ObjFunction.GetComboValue(cmbPartyName) + "," + VchType.PurchasePayment + "," + DBGetVal.CompanyNo + ",1").Table;
                    else if (rbAdjSalesReturn.Checked == true)
                        dtAdv = ObjFunction.GetDataView("Exec GetAdvanceCollectionDetails " + ObjFunction.GetComboValue(cmbPartyName) + "," + VchType.RejectionOut + "," + DBGetVal.CompanyNo + ",3").Table;
                    label15.Text = "Select the Bill to be Adjust Purchase Return";
                }

                for (int i = 0; i < dtAdv.Rows.Count; i++)
                {
                    dgAdvance.Rows.Add();
                    for (int j = 0; j < dgAdvance.Columns.Count - 1; j++)
                    {
                        dgAdvance.Rows[i].Cells[j].Value = dtAdv.Rows[i].ItemArray[j];
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        //private void dgAdjustOpening_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        //{
        //    try
        //    {
        //        double AdjustAmt = 0;
        //        if (e.ColumnIndex == ColIndex.Chk)
        //        {
        //            int rowIndex = e.RowIndex;
        //            if (Convert.ToBoolean(dgAdjustOpening.Rows[rowIndex].Cells[ColIndex.Chk].FormattedValue) == true)
        //            {
        //                if (OpBalAdjustedAmt < OpBalPendingAmt)
        //                {
        //                    AdjustAmt = Convert.ToDouble(dgAdjustOpening.Rows[rowIndex].Cells[ColIndex.NetBal].Value);

        //                    if (AdjustAmt <= (OpBalPendingAmt - OpBalAdjustedAmt))
        //                    {
        //                        dgAdjustOpening.Rows[rowIndex].Cells[ColIndex.Amount].Value = AdjustAmt.ToString("0.00");
        //                        OpBalAdjustedAmt = OpBalAdjustedAmt + AdjustAmt;
        //                    }
        //                    else if ((OpBalPendingAmt - OpBalAdjustedAmt) > 0)
        //                    {
        //                        dgAdjustOpening.Rows[rowIndex].Cells[ColIndex.Amount].Value = (OpBalPendingAmt - OpBalAdjustedAmt).ToString("0.00");
        //                        OpBalAdjustedAmt = OpBalAdjustedAmt + (OpBalPendingAmt - OpBalAdjustedAmt);
        //                    }
        //                }
        //                else
        //                    dgAdjustOpening.Rows[rowIndex].Cells[ColIndex.Chk].Value = false;
        //            }
        //            else
        //            {
        //                OpBalAdjustedAmt = OpBalAdjustedAmt - Convert.ToDouble(dgAdjustOpening.Rows[rowIndex].Cells[ColIndex.Amount].Value);
        //                dgAdjustOpening.Rows[rowIndex].Cells[ColIndex.Amount].Value = "0.00";
        //            }
        //        }
        //    }
        //    catch (Exception exc)
        //    {
        //        ObjFunction.ExceptionDisplay(exc.Message);
        //    }
        //}

        private void dgAdjustOpening_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Space)
                {
                    double AdjustAmt = 0;
                    int rowIndex = dgAdjustOpening.CurrentRow.Index;
                    if (dgAdjustOpening.CurrentCell.ColumnIndex == ColIndex.Chk)
                    {
                        if (Convert.ToBoolean(dgAdjustOpening.Rows[rowIndex].Cells[ColIndex.Chk].FormattedValue) == false)
                        {
                            dgAdjustOpening.Rows[rowIndex].Cells[ColIndex.Chk].Value = true;
                            if (OpBalAdjustedAmt < OpBalPendingAmt)
                            {
                                AdjustAmt = Convert.ToDouble(dgAdjustOpening.Rows[rowIndex].Cells[ColIndex.NetBal].Value);

                                if (AdjustAmt <= (OpBalPendingAmt - OpBalAdjustedAmt))
                                {
                                    dgAdjustOpening.Rows[rowIndex].Cells[ColIndex.Amount].Value = AdjustAmt.ToString("0.00");
                                    OpBalAdjustedAmt = OpBalAdjustedAmt + AdjustAmt;
                                }
                                else if ((OpBalPendingAmt - OpBalAdjustedAmt) > 0)
                                {
                                    dgAdjustOpening.Rows[rowIndex].Cells[ColIndex.Amount].Value = (OpBalPendingAmt - OpBalAdjustedAmt).ToString("0.00");
                                    OpBalAdjustedAmt = OpBalAdjustedAmt + (OpBalPendingAmt - OpBalAdjustedAmt);
                                }
                            }
                            else
                                dgAdjustOpening.Rows[rowIndex].Cells[ColIndex.Chk].Value = false;
                        }
                        else
                        {
                            dgAdjustOpening.Rows[rowIndex].Cells[ColIndex.Chk].Value = false;
                            OpBalAdjustedAmt = OpBalAdjustedAmt - Convert.ToDouble(dgAdjustOpening.Rows[rowIndex].Cells[ColIndex.Amount].Value);
                            dgAdjustOpening.Rows[rowIndex].Cells[ColIndex.Amount].Value = "0.00";
                        }
                    }
                }

            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgAdjustOpening_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                double AdjustAmt = 0;
                if (e.ColumnIndex == ColIndex.Chk)
                {
                    int rowIndex = e.RowIndex;
                    if (Convert.ToBoolean(dgAdjustOpening.Rows[rowIndex].Cells[ColIndex.Chk].FormattedValue) == false)
                    {
                        dgAdjustOpening.Rows[rowIndex].Cells[ColIndex.Chk].Value = true;
                        if (OpBalAdjustedAmt < OpBalPendingAmt)
                        {
                            AdjustAmt = Convert.ToDouble(dgAdjustOpening.Rows[rowIndex].Cells[ColIndex.NetBal].Value);

                            if (AdjustAmt <= (OpBalPendingAmt - OpBalAdjustedAmt))
                            {
                                dgAdjustOpening.Rows[rowIndex].Cells[ColIndex.Amount].Value = AdjustAmt.ToString("0.00");
                                OpBalAdjustedAmt = OpBalAdjustedAmt + AdjustAmt;
                            }
                            else if ((OpBalPendingAmt - OpBalAdjustedAmt) > 0)
                            {
                                dgAdjustOpening.Rows[rowIndex].Cells[ColIndex.Amount].Value = (OpBalPendingAmt - OpBalAdjustedAmt).ToString("0.00");
                                OpBalAdjustedAmt = OpBalAdjustedAmt + (OpBalPendingAmt - OpBalAdjustedAmt);
                            }
                        }
                        else
                            dgAdjustOpening.Rows[rowIndex].Cells[ColIndex.Chk].Value = false;
                    }
                    else
                    {
                        dgAdjustOpening.Rows[rowIndex].Cells[ColIndex.Chk].Value = false;
                        OpBalAdjustedAmt = OpBalAdjustedAmt - Convert.ToDouble(dgAdjustOpening.Rows[rowIndex].Cells[ColIndex.Amount].Value);
                        dgAdjustOpening.Rows[rowIndex].Cells[ColIndex.Amount].Value = "0.00";
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgAdjustOpening_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == ColIndex.BillDate)
            {
                e.Value = Convert.ToDateTime(e.Value).ToString("dd-MMM-yy");
            }
        }

        private void dgAdjustOpening_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == ColIndex.Chk)
            {
                int rowIndex = e.RowIndex;
            }
        }

        private void cmbBank_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (VoucherType == VchType.Purchase)
                {
                    btnchqOk.Focus();
                }
                else
                    cmbBranch.Focus();
            }
        }

        private void cmbCrBank_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (VoucherType == VchType.Purchase)
                {
                    btnCrOk.Focus();
                }
                else
                    cmbCrBranch.Focus();
            }
        }

        private void txtRemark_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                cmbPayType.Enabled = true;
                cmbPayType.Focus();
            }
        }

        private void dgAdvance_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 8)
                {
                    int rowIndex = e.RowIndex;
                    if (Convert.ToBoolean(dgAdvance.Rows[rowIndex].Cells[8].Value) == false)
                    {
                        dgAdvance.Rows[rowIndex].Cells[8].Value = true;
                        for (int i = 0; i < dgAdvance.Rows.Count; i++)
                        {
                            if (i != e.RowIndex)
                                dgAdvance.Rows[i].Cells[8].Value = false;
                        }
                        TolAdvadjAmt = Convert.ToDouble(dgAdvance.Rows[rowIndex].Cells[5].Value);
                        AdvAdjAmt = 0;
                        Amt = 0;
                        Advflag = false;
                        BindGridAdv();
                        dgBill.Visible = true;
                    }
                    else
                    {
                        dgAdvance.Rows[rowIndex].Cells[8].Value = false;
                        TolAdvadjAmt = 0;
                        AdvAdjAmt = 0;
                        Amt = 0;
                        Advflag = false;
                        dgBill.Visible = false;
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgBill_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == ColIndex.Chk)
                {
                    int rowIndex = e.RowIndex;
                    if (Convert.ToBoolean(dgBill.Rows[rowIndex].Cells[ColIndex.Chk].EditedFormattedValue) == false)
                    {
                        dgBill.Rows[rowIndex].Cells[ColIndex.Chk].Value = true;
                        if (AdvAdjAmt <= 0 && (Amt != TolAdvadjAmt))
                            AdvAdjAmt = Convert.ToDouble(dgBill.Rows[rowIndex].Cells[ColIndex.NetBal].Value);
                        if (AdvAdjAmt <= TolAdvadjAmt && Advflag == false && (Amt != TolAdvadjAmt))
                        {
                            if (AdvAdjAmt <= Convert.ToDouble(dgBill.Rows[rowIndex].Cells[ColIndex.NetBal].Value))
                            {
                                dgBill.Rows[rowIndex].Cells[ColIndex.Amount].Value = AdvAdjAmt.ToString("0.00");
                                Amt = Amt + Convert.ToDouble(dgBill.Rows[rowIndex].Cells[ColIndex.Amount].Value);
                            }
                            else
                            {
                                dgBill.Rows[rowIndex].Cells[ColIndex.Amount].Value = dgBill.Rows[rowIndex].Cells[ColIndex.NetBal].Value;
                                Amt = Amt + Convert.ToDouble(dgBill.Rows[rowIndex].Cells[ColIndex.Amount].Value);
                            }
                            AdvAdjAmt = TolAdvadjAmt - Amt;
                        }
                        else if (AdvAdjAmt > TolAdvadjAmt)
                        {
                            dgBill.Rows[rowIndex].Cells[ColIndex.Amount].Value = TolAdvadjAmt.ToString("0.00");
                            AdvAdjAmt = TolAdvadjAmt;
                            Advflag = true;
                        }
                        else
                            dgBill.Rows[rowIndex].Cells[ColIndex.Chk].Value = false;
                    }
                    else
                    {
                        dgBill.Rows[rowIndex].Cells[ColIndex.Chk].Value = false;
                        AdvAdjAmt = AdvAdjAmt - Convert.ToDouble(dgBill.Rows[rowIndex].Cells[ColIndex.Amount].Value);
                        Amt = Amt - Convert.ToDouble(dgBill.Rows[rowIndex].Cells[ColIndex.Amount].Value);
                        dgBill.Rows[rowIndex].Cells[ColIndex.Amount].Value = "0.00";
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgBillSelection_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                rowCount = dgBillSelection.CurrentCell.RowIndex;
                dgBillSelection.CurrentCell.ErrorText = "";

                if (dgBillSelection.CurrentCell.ColumnIndex == ColIndex.Amount)
                {
                    if (dgBillSelection.CurrentCell.Value != null)
                    {
                        if (ObjFunction.CheckValidAmount(dgBillSelection.CurrentCell.EditedFormattedValue.ToString()) == false)
                            dgBillSelection.CurrentCell.ErrorText = "Please Enter Valid Amount";
                        else if (Convert.ToDouble(dgBillSelection.Rows[rowCount].Cells[ColIndex.NetBal].Value) < Convert.ToDouble(dgBillSelection.Rows[rowCount].Cells[ColIndex.Amount].Value))
                            dgBillSelection.CurrentCell.ErrorText = "Please Enter Valid Amount";
                        else if (Convert.ToDouble(dgBillSelection.Rows[rowCount].Cells[ColIndex.Amount].Value) == 0)
                            dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.Chk].Value = false;
                        else
                        {
                            dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.Chk].Value = true;
                            MovetoNext move2n = new MovetoNext(m2n);
                            if (dgBillSelection.Rows.Count - 1 == rowCount)
                                BeginInvoke(move2n, new object[] { rowCount, ColIndex.Amount, dgBillSelection });
                            else if (dgBillSelection.Rows.Count - 1 > rowCount)
                                BeginInvoke(move2n, new object[] { rowCount + 1, ColIndex.Amount, dgBillSelection });
                            //GridView.CurrentCell = GridView[ColIndex.PayType, rowCount];
                        }
                    }
                    //else if (dgBillSelection.CurrentCell.ColumnIndex == ColIndex.Chk)
                    //{

                    //    if (Convert.ToBoolean(dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.Chk].EditedFormattedValue) == true)
                    //    {
                    //        dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.Chk].Value = false;
                    //        dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.Amount].Value = "0.00";
                    //    }
                    //    else
                    //    {
                    //        dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.Chk].Value = true;
                    //        dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.Amount].Value = dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.NetBal].Value;
                    //    }
                    //}
                    else
                    {
                        dgBillSelection.CurrentCell.Value = "0.00";
                        dgBillSelection.CurrentRow.Cells[ColIndex.Chk].Value = false;
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { rowCount, ColIndex.Amount, dgBillSelection });

                    }
                }
                else if (e.ColumnIndex == ColIndex.Chk)
                {
                    if (Convert.ToBoolean(dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.Chk].FormattedValue) == true)
                    {
                        dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.Chk].Value = false;
                        dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.Amount].Value = "0.00";
                    }
                    else
                    {
                        dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.Chk].Value = true;
                        dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.Amount].Value = dgBillSelection.Rows[dgBillSelection.CurrentCell.RowIndex].Cells[ColIndex.NetBal].Value;
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void DataClrscr()
        {
            cmbPartyName.SelectedValue = "0";
            lblNetBal.Text = "";
            lblAmount.Text = "";
            lblOpBal.Text = "0.00";
            lblNetB.Text = "0.00";
            lblDrBal.Text = "0.00";
            lblCrNet.Text = "0.00";
            lblOpBalp.Text = "0.00";
            lblNetBp.Text = "0.00";
            lblDrBalp.Text = "0.00";
            lblCrNetp.Text = "0.00";

            GridView.Rows.Clear();
            dgBillSelection.Rows.Clear();
            dgBill.Rows.Clear();
            dgAdvance.Rows.Clear();
            pnlchq.Visible = false;
            pnlCredit.Visible = false;
            pnlPaytype.Visible = false;
            pnlRecord.Visible = false;
            pnlDetails.Visible = false;
            pnlDetailsPur.Visible = false;
            BtnSave.Enabled = false;
            label5.Visible = false;
            txtAmount.Text = "";
            txtAdjustment.Text = "";
            txtAdvAmt.Text = "";

            lblOpeningBal.Text = "Opening Balance:    0.00";
            lblBalance.Text = "Balance:    0.00";
            lblAdvOpBal.Text = "Total Advance:    0.00";
            lblAdvBal.Text = "Balance:    0.00";

        }

        private void dgBill_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                e.SuppressKeyPress = true;
                BtnSave.Focus();
            }
            else if (e.KeyCode == Keys.Space)
            {
                try
                {
                    int rowIndex = dgBill.CurrentCell.RowIndex;
                    if (dgBill.CurrentCell.ColumnIndex == ColIndex.Chk)
                    {
                        if (Convert.ToBoolean(dgBill.Rows[rowIndex].Cells[ColIndex.Chk].EditedFormattedValue) == false)
                        {
                            dgBill.Rows[rowIndex].Cells[ColIndex.Chk].Value = true;
                            if (AdvAdjAmt <= 0 && (Amt != TolAdvadjAmt))
                                AdvAdjAmt = Convert.ToDouble(dgBill.Rows[rowIndex].Cells[ColIndex.NetBal].Value);
                            if (AdvAdjAmt <= TolAdvadjAmt && Advflag == false && (Amt != TolAdvadjAmt))
                            {
                                if (AdvAdjAmt <= Convert.ToDouble(dgBill.Rows[rowIndex].Cells[ColIndex.NetBal].Value))
                                {
                                    dgBill.Rows[rowIndex].Cells[ColIndex.Amount].Value = AdvAdjAmt.ToString("0.00");
                                    Amt = Amt + Convert.ToDouble(dgBill.Rows[rowIndex].Cells[ColIndex.Amount].Value);
                                }
                                else
                                {
                                    dgBill.Rows[rowIndex].Cells[ColIndex.Amount].Value = dgBill.Rows[rowIndex].Cells[ColIndex.NetBal].Value;
                                    Amt = Amt + Convert.ToDouble(dgBill.Rows[rowIndex].Cells[ColIndex.Amount].Value);
                                }
                                AdvAdjAmt = TolAdvadjAmt - Amt;
                            }
                            else if (AdvAdjAmt > TolAdvadjAmt)
                            {
                                dgBill.Rows[rowIndex].Cells[ColIndex.Amount].Value = TolAdvadjAmt.ToString("0.00");
                                AdvAdjAmt = TolAdvadjAmt;
                                Advflag = true;
                            }
                            else
                                dgBill.Rows[rowIndex].Cells[ColIndex.Chk].Value = false;
                        }
                        else
                        {
                            dgBill.Rows[rowIndex].Cells[ColIndex.Chk].Value = false;
                            AdvAdjAmt = AdvAdjAmt - Convert.ToDouble(dgBill.Rows[rowIndex].Cells[ColIndex.Amount].Value);
                            Amt = Amt - Convert.ToDouble(dgBill.Rows[rowIndex].Cells[ColIndex.Amount].Value);
                            dgBill.Rows[rowIndex].Cells[ColIndex.Amount].Value = "0.00";
                        }
                    }
                }
                catch (Exception exc)
                {
                    ObjFunction.ExceptionDisplay(exc.Message);
                }
            }
        }

        private void dgAdvance_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Space && dgAdvance.CurrentCell.ColumnIndex == 8)
                {
                    int rowIndex = dgAdvance.CurrentCell.RowIndex;
                    if (Convert.ToBoolean(dgAdvance.Rows[rowIndex].Cells[8].Value) == false)
                    {
                        dgAdvance.Rows[rowIndex].Cells[8].Value = true;
                        for (int i = 0; i < dgAdvance.Rows.Count; i++)
                        {
                            if (i != rowIndex)
                                dgAdvance.Rows[i].Cells[8].Value = false;
                        }
                        TolAdvadjAmt = Convert.ToDouble(dgAdvance.Rows[rowIndex].Cells[5].Value);
                        AdvAdjAmt = 0;
                        Amt = 0;
                        Advflag = false;
                        BindGridAdv();
                        dgBill.Visible = true;
                    }
                    else
                    {
                        dgAdvance.Rows[rowIndex].Cells[8].Value = false;
                        TolAdvadjAmt = 0;
                        AdvAdjAmt = 0;
                        Amt = 0;
                        Advflag = false;
                        dgBill.Visible = false;
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            pnlPrint.Visible = !pnlPrint.Visible;
            rbSummary.Focus();
        }

        private void pnlPrint_VisibleChanged(object sender, EventArgs e)
        {
            if (pnlPrint.Visible == true)
            {

            }
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            if (GridView.Rows.Count > 0)
            {
                try
                {

                    if (rbSummary.Checked == true)
                    {
                        string[] ReportSession = new string[4];
                        ReportSession[0] = VoucherType.ToString();
                        ReportSession[1] = DBGetVal.CompanyNo.ToString();
                        ReportSession[2] = Convert.ToDateTime(dtpVoucherDate.Text).ToString("dd-MMM-yyyy");
                        ReportSession[3] = ObjFunction.GetComboValue(cmbPartyName).ToString();

                        Form NewF = null;
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            NewF = new Display.ReportViewSource(new Reports.GetOutStanding(), ReportSession);
                        else
                            NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("GetOutStanding.rpt", CommonFunctions.ReportPath), ReportSession);
                        ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                    }
                    else if (rbDetails.Checked == true)
                    {
                        string[] ReportSession = new string[4];
                        ReportSession[0] = VoucherType.ToString();
                        ReportSession[1] = DBGetVal.CompanyNo.ToString();
                        ReportSession[2] = Convert.ToDateTime(dtpVoucherDate.Text).ToString("dd-MMM-yyyy");
                        ReportSession[3] = ObjFunction.GetComboValue(cmbPartyName).ToString();

                        Form NewF = null;
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            NewF = new Display.ReportViewSource(new Reports.GetOutstandingDetails(), ReportSession);
                        else
                            NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("GetOutstandingDetails.rpt", CommonFunctions.ReportPath), ReportSession);
                        ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                    }
                }
                catch (Exception exc)
                {
                    ObjFunction.ExceptionDisplay(exc.Message);
                }
                finally
                {
                    pnlPrint.Visible = false;
                }
            }
        }

        private void rbSummary_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                e.SuppressKeyPress = true;
                pnlPrint.Visible = false;
                btnCancel.Focus();
            }
        }

        private void btnPrintBillWise_Click(object sender, EventArgs e)
        {
            string[] ReportSession;
            Form NewF = null;


            ReportSession = new string[5];

            ReportSession[0] = Convert.ToDateTime(dtpVoucherDate.Text).ToString("dd-MMM-yyyy");
            ReportSession[1] = Convert.ToDateTime(dtpVoucherDate.Text).ToString("dd-MMM-yyyy");
            ReportSession[2] = VoucherType.ToString();
            ReportSession[3] = DBGetVal.CompanyNo.ToString();
            ReportSession[4] = ObjFunction.GetComboValue(cmbPartyName).ToString();
            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                NewF = new Display.ReportViewSource(new Reports.RptLederBookBillwise(), ReportSession);
            else
                NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("RptLederBookBillwise.rpt", CommonFunctions.ReportPath), ReportSession);
            ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
        }
    }
}


