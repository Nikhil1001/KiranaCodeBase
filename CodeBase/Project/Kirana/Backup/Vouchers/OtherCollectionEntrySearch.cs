﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Vouchers
{
    public partial class OtherCollectionEntrySearch : Form
    {

        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        private long VoucherType = VchType.CashPayment;
        public DialogResult drUserResponse = DialogResult.Cancel;
        public long PkVoucherNo = 0;
        private DateTime dtSelectedDate = DateTime.Now.Date;
        private bool isDatewiseSearch = false;

        public OtherCollectionEntrySearch()
        {
            InitializeComponent();
            initComponent();
        }

        private void initComponent()
        {
            if (VoucherType == VchType.CashPayment)
            {
                lblTitle.Text = "Cash Payments";
            }
            else if (VoucherType == VchType.CashReceipt)
            {
                lblTitle.Text = "Cash Receipts";
            }
        }

        public OtherCollectionEntrySearch(long voucherType)
        {
            InitializeComponent();
            this.VoucherType = voucherType;
            initComponent();
        }

        public OtherCollectionEntrySearch(long voucherType, DateTime dtSearchDate)
        {
            InitializeComponent();
            this.VoucherType = voucherType;
            this.dtSelectedDate = dtSearchDate;
            isDatewiseSearch = true;
            initComponent();
        }

        private void OtherCollectionEntry_Load(object sender, EventArgs e)
        {
            
            ObjFunction.FillCombo(cmbPartyNameSearch, "Select LedgerNo,LedgerName From MLedger "  +
                " Where GroupNo NOT IN (" + GroupType.SundryDebtors + "," + GroupType.SundryCreditors +
                    "," + GroupType.CashInhand + "," + GroupType.DutiesAndTaxes + "," + GroupType.PurchaseAccount +
                    "," + GroupType.SalesAccount + ") " + 
                    " ORDER BY LedgerName");
            dgInvSearch.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            if (isDatewiseSearch)
            {
                rbDate.Checked = true;
            }
            rbType(true);
            if (isDatewiseSearch)
            {
                dtpSearchDate.Value = dtSelectedDate;
                dtpSearchDate_KeyDown(null, new KeyEventArgs(Keys.Enter));
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void rbType_CheckedChanged(object sender, EventArgs e)
        {
            rbType(true);
        }

        public void rbType(bool IsSetFocus)
        {
            /*13-Apr-2014
            * Exception Handling Purpose.
            * */
            try
            {
                rbInvNo.Enabled = true;
                rbPartyName.Enabled = true;
                rbDate.Enabled = true;
                pnlInvSearch.Visible = false;
                dgInvSearch.DataSource = null;
                txtSearch.Text = "";
                cmbPartyNameSearch.SelectedIndex = 0;


                dtpSearchDate.Visible = false;
                if (rbInvNo.Checked == true)
                {
                    if (IsSetFocus)
                    {
                        lblLable.Visible = true;
                        lblLable.Text = "Inv No :";
                        txtSearch.Width = 162;
                        txtSearch.Location = new System.Drawing.Point(90, 39);
                        txtSearch.Visible = true;
                        cmbPartyNameSearch.Visible = false;
                        btnPartyName.Visible = false;
                        dtpSearchDate.Visible = false;
                        txtSearch.Focus();
                    }
                }
                else if (rbPartyName.Checked == true)
                {
                    if (IsSetFocus)
                    {
                        btnPartyName.Enabled = true;
                        cmbPartyNameSearch.Enabled = true;
                        btnPartyName.Visible = true;
                        lblLable.Visible = true;
                        lblLable.Text = "Party Name :";
                        cmbPartyNameSearch.Width = 250;
                        cmbPartyNameSearch.Location = new System.Drawing.Point(90, 39);
                        btnPartyName.Location = new System.Drawing.Point((90 + 250 + 5), 39);
                        cmbPartyNameSearch.Visible = true;
                        txtSearch.Visible = false;
                        dtpSearchDate.Visible = false;
                        cmbPartyNameSearch.Focus();
                    }
                }
                else if (rbDate.Checked == true)
                {
                    if (IsSetFocus)
                    {
                        dtpSearchDate.Enabled = true;
                        cmbPartyNameSearch.Visible = false;
                        btnPartyName.Visible = false;
                        lblLable.Visible = true;
                        lblLable.Text = "Date :";
                        dtpSearchDate.Location = new System.Drawing.Point(90, 39);
                        txtSearch.Visible = false;
                        dtpSearchDate.Visible = true;
                        dtpSearchDate.Focus();
                    }
                }

            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);;//For Common Error Displayed Purpose
            }
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            /*13-Apr-2014
            * Exception Handling Purpose.
			* Actual data searching by Invoice No
            * */
            try
            {
                long tempNo;
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    if (rbInvNo.Checked)
                    {
                        if (ObjQry.ReturnLong("Select Count(*) From TVoucherEntry Where VoucherUserNo=" + txtSearch.Text + " and VoucherTypeCode=" + VoucherType + "", CommonFunctions.ConStr) > 1)
                        {
                            pnlInvSearch.Visible = true;

                            string str = "SELECT    0 as [#], TVoucherEntry.VoucherUserNo AS [Doc #], TVoucherEntry.VoucherDate AS 'Date', TVoucherEntry.BilledAmount AS 'Amount'," +
                                         "TVoucherEntry.PkVoucherNo FROM TVoucherEntry WHERE (TVoucherEntry.VoucherTypeCode IN (" + VoucherType + ")) " +
                                         "And TVoucherEntry.VoucherUserNo=" + txtSearch.Text + " " +
                                         "Order By  TVoucherEntry.VoucherUserNo desc,TVoucherEntry.VoucherDate desc, TVoucherEntry.Reference desc";
                            dgInvSearch.DataSource = ObjFunction.GetDataView(str).Table.DefaultView;
                            dgInvSearch.Columns[0].Width = 50;
                            dgInvSearch.Columns[1].Width = 150;
                            dgInvSearch.Columns[2].Width = 80;
                            dgInvSearch.Columns[3].Width = 110;
                            dgInvSearch.Columns[3].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
                            dgInvSearch.Columns[3].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
                            dgInvSearch.Columns[4].Visible = false;
                            if (dgInvSearch.RowCount > 0)
                            {
                                pnlInvSearch.Focus();
                                dgInvSearch.Focus();
                                dgInvSearch.CurrentCell = dgInvSearch[0, dgInvSearch.CurrentRow.Index];
                            }
                            txtSearch.Text = "";
                            cmbPartyNameSearch.SelectedIndex = 0;
                            return;
                        }
                        tempNo = ObjQry.ReturnLong("Select PKVoucherNo From TVoucherEntry Where VoucherUserNo=" + txtSearch.Text + " and VoucherTypeCode=" + VoucherType + "", CommonFunctions.ConStr);
                        if (tempNo > 0)
                        {
                            drUserResponse = DialogResult.OK;
                            PkVoucherNo = tempNo;
                            this.Close();
                        }
                        else
                        {
                            //pnlSearch.Visible = false;
                            DisplayMessage("Bill Not Found");
                            Application.DoEvents();
                            pnlSearch.Visible = true;
                            //cmbPartyNameSearch.SelectedIndex = 0;
                            rbInvNo.Focus();
                            txtSearch.Focus();
                            txtSearch.Text = "";
                        }
                    }

                }
                else if (e.KeyCode == Keys.Escape)
                {
                    e.SuppressKeyPress = true;

                }
                else if (e.KeyCode == Keys.Right)
                {
                    if (rbInvNo.Checked)
                    {
                        e.SuppressKeyPress = true;
                        rbPartyName.Checked = true;
                        rbType_CheckedChanged(rbPartyName, null);
                    }
                }

            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);;//For Common Error Displayed Purpose
            }
        }

        public void DisplayMessage(string str)
        {
            lblMsg.Visible = true;
            lblMsg.Text = str;
            Application.DoEvents();
            System.Threading.Thread.Sleep(700);
            lblMsg.Visible = false;
        }

        private void cmbPartyNameSearch_KeyDown(object sender, KeyEventArgs e)
        {
            /*13-Apr-2014
            * Exception Handling Purpose.
			* Party Name wise searching the field
            * */
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    dgInvSearch.DataSource = null;

                    string str = "SELECT    0 as [#], TVoucherEntry.VoucherUserNo AS [Doc #], TVoucherEntry.VoucherDate AS 'Date', TVoucherEntry.BilledAmount AS 'Amount'," +
                                 "TVoucherEntry.PkVoucherNo FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo WHERE (TVoucherEntry.VoucherTypeCode = " + VoucherType + ") AND (TVoucherDetails.VoucherSrNo = 1) " +
                                 "And TVoucherDetails.LedgerNo=" + ObjFunction.GetComboValue(cmbPartyNameSearch) + " " +
                                 "Order By  TVoucherEntry.VoucherUserNo desc,TVoucherEntry.VoucherDate desc, TVoucherEntry.Reference desc";
                    dgInvSearch.DataSource = ObjFunction.GetDataView(str).Table.DefaultView;
                    dgInvSearch.Columns[0].Width = 50;
                    dgInvSearch.Columns[1].Width = 150;
                    dgInvSearch.Columns[2].Width = 80;
                    dgInvSearch.Columns[3].Width = 110;
                    dgInvSearch.Columns[3].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
                    dgInvSearch.Columns[3].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
                    dgInvSearch.Columns[4].Visible = false;
                    if (dgInvSearch.RowCount > 0)
                    {
                        pnlInvSearch.Focus();
                        pnlInvSearch.Visible = true;
                        e.SuppressKeyPress = true;
                        dgInvSearch.Focus();
                        dgInvSearch.CurrentCell = dgInvSearch[0, dgInvSearch.CurrentRow.Index];
                        lblSearchName.Text = "Party Name: " + cmbPartyNameSearch.Text;
                        lblSearchName.Font = new Font("Verdana", 11, FontStyle.Bold);

                    }
                    else
                    {

                        DisplayMessage("Bill Not Found");
                        pnlSearch.Visible = true;
                        rbPartyName.Focus();
                        rbType_CheckedChanged(rbPartyName, null);
                    }


                }
                else if (e.KeyCode == Keys.Escape)
                {
                    e.SuppressKeyPress = true;

                }
                else if (e.KeyCode == Keys.Left)
                {
                    e.SuppressKeyPress = true;
                    rbInvNo.Checked = true;
                    rbType_CheckedChanged(rbInvNo, null);
                }
                else if (e.KeyCode == Keys.Right)
                {
                    e.SuppressKeyPress = true;
                    rbDate.Checked = true;
                    rbType_CheckedChanged(rbDate, null);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);;//For Common Error Displayed Purpose
            }
        }

        private void dgInvSearch_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                e.Value = e.RowIndex + 1;

            }
            else if (e.ColumnIndex == 2)
            {
                e.Value = Convert.ToDateTime(e.Value).ToString("dd-MMM-yy");
            }
        }

        private void dtpSearchDate_KeyDown(object sender, KeyEventArgs e)
        {
            /*13-Apr-2014
            * Exception Handling Purpose.
			* Date wise searching billing data
            * */
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    pnlInvSearch.Visible = true;
                    lblSearchName.Text = "";
                    string str = "SELECT    0 as [#], TVoucherEntry.VoucherUserNo AS [Doc #], TVoucherEntry.VoucherDate AS 'Date', TVoucherEntry.BilledAmount AS 'Amount'," +
                                 "TVoucherEntry.PkVoucherNo FROM TVoucherEntry WHERE (TVoucherEntry.VoucherTypeCode =" + VoucherType + ") " +
                                 "And TVoucherEntry.VoucherDate='" + dtpSearchDate.Text + "' " +
                                 "Order By  TVoucherEntry.VoucherUserNo desc,TVoucherEntry.VoucherDate desc, TVoucherEntry.Reference desc";
                    dgInvSearch.DataSource = ObjFunction.GetDataView(str).Table.DefaultView;
                    dgInvSearch.Columns[0].Width = 50;
                    dgInvSearch.Columns[1].Width = 150;
                    dgInvSearch.Columns[2].Width = 80;
                    dgInvSearch.Columns[3].Width = 110;
                    dgInvSearch.Columns[3].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
                    dgInvSearch.Columns[3].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
                    dgInvSearch.Columns[4].Visible = false;
                    if (dgInvSearch.RowCount > 0)
                    {
                        pnlInvSearch.Focus();
                        pnlInvSearch.Visible = true;
                        e.SuppressKeyPress = true;
                        dgInvSearch.Focus();
                        dgInvSearch.CurrentCell = dgInvSearch[0, dgInvSearch.CurrentRow.Index];
                    }
                    else
                    {
                        pnlInvSearch.Visible = false;
                        DisplayMessage("Bill Not Found");
                        Application.DoEvents();
                        rbDate.Focus();
                        dtpSearchDate.Focus();
                    }
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    e.SuppressKeyPress = true;


                }
                else if (e.KeyCode == Keys.Left)
                {
                    e.SuppressKeyPress = true;
                    rbPartyName.Checked = true;
                    rbType_CheckedChanged(rbPartyName, null);
                }

            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);;//For Common Error Displayed Purpose
            }
        }

        private void dgInvSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                drUserResponse = DialogResult.OK;
                PkVoucherNo = Convert.ToInt64(dgInvSearch[4, dgInvSearch.CurrentRow.Index].EditedFormattedValue);
                this.Close();

            }
        }

    }
}
