﻿namespace Kirana.Vouchers
{
    partial class SalesReturnAE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.txtInvNo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpBillDate = new System.Windows.Forms.DateTimePicker();
            this.dtpBillTime = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.pnlTotalAmt = new System.Windows.Forms.Panel();
            this.txtTotalItemDisc = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtTotalAnotherDisc = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtRoundOff = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtTotalChrgs = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtGrandTotal = new System.Windows.Forms.TextBox();
            this.txtTotalTax = new System.Windows.Forms.TextBox();
            this.txtTotalDisc = new System.Windows.Forms.TextBox();
            this.txtSubTotal = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblCompName = new System.Windows.Forms.Label();
            this.txtChrgRupees1 = new System.Windows.Forms.TextBox();
            this.txtDiscRupees1 = new System.Windows.Forms.TextBox();
            this.txtRemark = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.lblExchange = new System.Windows.Forms.Label();
            this.txtDiscount1 = new System.Windows.Forms.TextBox();
            this.cmbPaymentType = new System.Windows.Forms.ComboBox();
            this.lblChrg1 = new System.Windows.Forms.Label();
            this.lblDisc1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblBilExchangeItem = new System.Windows.Forms.Label();
            this.lblBillItem = new System.Windows.Forms.Label();
            this.txtRefNo = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.btndelete = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrev = new System.Windows.Forms.Button();
            this.btnLast = new System.Windows.Forms.Button();
            this.btnFirst = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.pnlFooterInfo = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label37 = new System.Windows.Forms.Label();
            this.txtlastBillQty = new System.Windows.Forms.TextBox();
            this.txtLastPayment = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txtLastBillAmt = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.dgBill = new System.Windows.Forms.DataGridView();
            this.btnSearch = new System.Windows.Forms.Button();
            this.lblMsg = new JitControls.OMLabel();
            this.pnlPartial = new System.Windows.Forms.Panel();
            this.label38 = new System.Windows.Forms.Label();
            this.dgPayCreditCardDetails = new System.Windows.Forms.DataGridView();
            this.CreditCardNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlBranch = new System.Windows.Forms.Panel();
            this.lstBranch = new System.Windows.Forms.ListBox();
            this.pnlBank = new System.Windows.Forms.Panel();
            this.lstBank = new System.Windows.Forms.ListBox();
            this.dtpChqDate = new System.Windows.Forms.MonthCalendar();
            this.dgPayChqDetails = new System.Windows.Forms.DataGridView();
            this.ChequeNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ChequeDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BankName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BranchName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AmountChq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkSrNoChq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BankNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BranchNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblPayTypeBal = new System.Windows.Forms.Label();
            this.dgPayType = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PKVoucherPayTypeNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnOk = new System.Windows.Forms.Button();
            this.txtTotalAmt = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.dgParkingBills = new System.Windows.Forms.DataGridView();
            this.BillNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BillAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlParking = new System.Windows.Forms.Panel();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.pnlSearch = new JitControls.OMBPanel();
            this.rbPartyName = new System.Windows.Forms.RadioButton();
            this.rbDocNo = new System.Windows.Forms.RadioButton();
            this.rbInvNo = new System.Windows.Forms.RadioButton();
            this.cmbPartyNameSearch = new System.Windows.Forms.ComboBox();
            this.txtInvNoSearch = new System.Windows.Forms.TextBox();
            this.btnCancelSearch = new System.Windows.Forms.Button();
            this.lblLable = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.pnlItemName = new System.Windows.Forms.Panel();
            this.dgItemList = new System.Windows.Forms.DataGridView();
            this.iItemNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemNameLang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iUOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iMRP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iStock = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iUOMStk = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iSaleTax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iPurTax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iCompany = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iBarcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iRateSettingNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMDefault = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PurRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lstUOM = new System.Windows.Forms.ListBox();
            this.lstRate = new System.Windows.Forms.ListBox();
            this.lstGroup1 = new System.Windows.Forms.ListBox();
            this.lstGroup2 = new System.Windows.Forms.ListBox();
            this.label33 = new System.Windows.Forms.Label();
            this.cmbPartyName = new System.Windows.Forms.ComboBox();
            this.cmbTaxType = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblRateType = new System.Windows.Forms.Label();
            this.cmbRateType = new System.Windows.Forms.ComboBox();
            this.pnlGroup1 = new System.Windows.Forms.Panel();
            this.lstGroup1Lang = new System.Windows.Forms.ListBox();
            this.pnlGroup2 = new System.Windows.Forms.Panel();
            this.pnlUOM = new System.Windows.Forms.Panel();
            this.pnlRate = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pnlSalePurHistory = new System.Windows.Forms.Panel();
            this.dgPurHistory = new System.Windows.Forms.DataGridView();
            this.dgSaleHistory = new System.Windows.Forms.DataGridView();
            this.pnlRateType = new System.Windows.Forms.Panel();
            this.btnRateTypeCancel = new System.Windows.Forms.Button();
            this.btnRateTypeOK = new System.Windows.Forms.Button();
            this.txtRateTypePassword = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.pnlBarCodePrint = new JitControls.OMPanel();
            this.rbSmallMode = new System.Windows.Forms.RadioButton();
            this.rbBigMod = new System.Windows.Forms.RadioButton();
            this.txtStartNo = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.btnCancelPrintBarcode = new System.Windows.Forms.Button();
            this.btnOKPrintBarCode = new System.Windows.Forms.Button();
            this.txtNoOfPrint = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.dgCompany = new System.Windows.Forms.DataGridView();
            this.CompanyNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SRCompanyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlCompany = new System.Windows.Forms.Panel();
            this.btnAllBarCodePrint = new System.Windows.Forms.Button();
            this.pnlAllPrintBarCode = new JitControls.OMPanel();
            this.dgPrintBarCode = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BarCodeQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chkPrint = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.FKRateSettingNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rdAllSmallMode = new System.Windows.Forms.RadioButton();
            this.rdAllBigMode = new System.Windows.Forms.RadioButton();
            this.txtAllStartNo = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.btnAllBarcodeCancel = new System.Windows.Forms.Button();
            this.btnAllBarcodeOK = new System.Windows.Forms.Button();
            this.txtOtherDisc = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.pnlPartySearch = new JitControls.OMGPanel();
            this.dgPartySearch = new System.Windows.Forms.DataGridView();
            this.btnAdvanceSearch = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnShortcut = new System.Windows.Forms.Button();
            this.btnNewCustomer = new System.Windows.Forms.Button();
            this.ombPanel1 = new JitControls.OMBPanel();
            this.lblLastPayment = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblLastBillAmt = new System.Windows.Forms.Label();
            this.lblLastBillQty = new System.Windows.Forms.Label();
            this.lblGrandTotal = new System.Windows.Forms.Label();
            this.btnShowDetails = new System.Windows.Forms.Button();
            this.btnShowBill = new System.Windows.Forms.Button();
            this.pnlInvSearch = new JitControls.OMBPanel();
            this.dgInvSearch = new System.Windows.Forms.DataGridView();
            this.label52 = new System.Windows.Forms.Label();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NetRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscPerce = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscRupees = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscPercentage2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscAmount2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NetAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxPerce = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscRupees2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Barcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkStockTrnNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkSrNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkVoucherNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxLedgerNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SalesLedgerNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkRateSettingNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkItemTaxInfo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StockFactor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActualQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MKTQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SalesVchNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxVchNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StockCompanyNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BarCodePrinting = new System.Windows.Forms.DataGridViewButtonColumn();
            this.HSNCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IGSTPercent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IGSTAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CGSTPercent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CGSTAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SGSTPercent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SGSTAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UTGSTPercent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UTGSTAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CessPercent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CessAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MRP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Godown = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlTotalAmt.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnlFooterInfo.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgBill)).BeginInit();
            this.pnlPartial.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPayCreditCardDetails)).BeginInit();
            this.pnlBranch.SuspendLayout();
            this.pnlBank.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPayChqDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgPayType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgParkingBills)).BeginInit();
            this.pnlParking.SuspendLayout();
            this.pnlSearch.SuspendLayout();
            this.pnlItemName.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgItemList)).BeginInit();
            this.pnlGroup1.SuspendLayout();
            this.pnlGroup2.SuspendLayout();
            this.pnlUOM.SuspendLayout();
            this.pnlRate.SuspendLayout();
            this.pnlSalePurHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPurHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgSaleHistory)).BeginInit();
            this.pnlRateType.SuspendLayout();
            this.pnlBarCodePrint.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCompany)).BeginInit();
            this.pnlCompany.SuspendLayout();
            this.pnlAllPrintBarCode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPrintBarCode)).BeginInit();
            this.pnlPartySearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPartySearch)).BeginInit();
            this.ombPanel1.SuspendLayout();
            this.pnlInvSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgInvSearch)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(10, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 501;
            this.label1.Text = "Doc No :";
            // 
            // txtInvNo
            // 
            this.txtInvNo.BackColor = System.Drawing.Color.White;
            this.txtInvNo.Location = new System.Drawing.Point(66, 5);
            this.txtInvNo.Name = "txtInvNo";
            this.txtInvNo.ReadOnly = true;
            this.txtInvNo.Size = new System.Drawing.Size(84, 20);
            this.txtInvNo.TabIndex = 0;
            this.txtInvNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(152, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 502;
            this.label2.Text = "Date :";
            // 
            // dtpBillDate
            // 
            this.dtpBillDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpBillDate.Location = new System.Drawing.Point(197, 5);
            this.dtpBillDate.Name = "dtpBillDate";
            this.dtpBillDate.Size = new System.Drawing.Size(90, 20);
            this.dtpBillDate.TabIndex = 1;
            this.dtpBillDate.Leave += new System.EventHandler(this.dtpBillDate_Leave);
            this.dtpBillDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpBillDate_KeyDown);
            // 
            // dtpBillTime
            // 
            this.dtpBillTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpBillTime.Location = new System.Drawing.Point(374, 24);
            this.dtpBillTime.Name = "dtpBillTime";
            this.dtpBillTime.Size = new System.Drawing.Size(94, 20);
            this.dtpBillTime.TabIndex = 552;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(328, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 503;
            this.label3.Text = "Time :";
            // 
            // pnlTotalAmt
            // 
            this.pnlTotalAmt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlTotalAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTotalAmt.Controls.Add(this.txtTotalItemDisc);
            this.pnlTotalAmt.Controls.Add(this.label18);
            this.pnlTotalAmt.Controls.Add(this.txtTotalAnotherDisc);
            this.pnlTotalAmt.Controls.Add(this.label36);
            this.pnlTotalAmt.Controls.Add(this.txtRoundOff);
            this.pnlTotalAmt.Controls.Add(this.label35);
            this.pnlTotalAmt.Controls.Add(this.txtTotalChrgs);
            this.pnlTotalAmt.Controls.Add(this.label34);
            this.pnlTotalAmt.Controls.Add(this.txtGrandTotal);
            this.pnlTotalAmt.Controls.Add(this.txtTotalTax);
            this.pnlTotalAmt.Controls.Add(this.txtTotalDisc);
            this.pnlTotalAmt.Controls.Add(this.txtSubTotal);
            this.pnlTotalAmt.Controls.Add(this.label8);
            this.pnlTotalAmt.Controls.Add(this.label7);
            this.pnlTotalAmt.Controls.Add(this.label6);
            this.pnlTotalAmt.Controls.Add(this.label5);
            this.pnlTotalAmt.Location = new System.Drawing.Point(775, 306);
            this.pnlTotalAmt.Name = "pnlTotalAmt";
            this.pnlTotalAmt.Size = new System.Drawing.Size(230, 211);
            this.pnlTotalAmt.TabIndex = 558;
            this.pnlTotalAmt.Visible = false;
            // 
            // txtTotalItemDisc
            // 
            this.txtTotalItemDisc.BackColor = System.Drawing.Color.Coral;
            this.txtTotalItemDisc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalItemDisc.Location = new System.Drawing.Point(102, 25);
            this.txtTotalItemDisc.Name = "txtTotalItemDisc";
            this.txtTotalItemDisc.ReadOnly = true;
            this.txtTotalItemDisc.Size = new System.Drawing.Size(123, 20);
            this.txtTotalItemDisc.TabIndex = 751;
            this.txtTotalItemDisc.Text = "0.00";
            this.txtTotalItemDisc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(1, 29);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(57, 13);
            this.label18.TabIndex = 752;
            this.label18.Text = "Item Disc. ";
            // 
            // txtTotalAnotherDisc
            // 
            this.txtTotalAnotherDisc.BackColor = System.Drawing.Color.Coral;
            this.txtTotalAnotherDisc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAnotherDisc.Location = new System.Drawing.Point(102, 121);
            this.txtTotalAnotherDisc.Name = "txtTotalAnotherDisc";
            this.txtTotalAnotherDisc.ReadOnly = true;
            this.txtTotalAnotherDisc.Size = new System.Drawing.Size(123, 20);
            this.txtTotalAnotherDisc.TabIndex = 519;
            this.txtTotalAnotherDisc.Text = "0.00";
            this.txtTotalAnotherDisc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(1, 125);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(49, 13);
            this.label36.TabIndex = 620;
            this.label36.Text = "Discount";
            // 
            // txtRoundOff
            // 
            this.txtRoundOff.BackColor = System.Drawing.Color.Coral;
            this.txtRoundOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRoundOff.Location = new System.Drawing.Point(102, 145);
            this.txtRoundOff.Name = "txtRoundOff";
            this.txtRoundOff.ReadOnly = true;
            this.txtRoundOff.Size = new System.Drawing.Size(123, 20);
            this.txtRoundOff.TabIndex = 517;
            this.txtRoundOff.Text = "0.00";
            this.txtRoundOff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(1, 149);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(56, 13);
            this.label35.TabIndex = 618;
            this.label35.Text = "Round Off";
            // 
            // txtTotalChrgs
            // 
            this.txtTotalChrgs.BackColor = System.Drawing.Color.Coral;
            this.txtTotalChrgs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalChrgs.Location = new System.Drawing.Point(102, 97);
            this.txtTotalChrgs.Name = "txtTotalChrgs";
            this.txtTotalChrgs.ReadOnly = true;
            this.txtTotalChrgs.Size = new System.Drawing.Size(123, 20);
            this.txtTotalChrgs.TabIndex = 515;
            this.txtTotalChrgs.Text = "0.00";
            this.txtTotalChrgs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(1, 101);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(73, 13);
            this.label34.TabIndex = 516;
            this.label34.Text = "Total Charges";
            // 
            // txtGrandTotal
            // 
            this.txtGrandTotal.BackColor = System.Drawing.Color.Coral;
            this.txtGrandTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGrandTotal.Location = new System.Drawing.Point(89, 169);
            this.txtGrandTotal.Name = "txtGrandTotal";
            this.txtGrandTotal.ReadOnly = true;
            this.txtGrandTotal.Size = new System.Drawing.Size(136, 20);
            this.txtGrandTotal.TabIndex = 750;
            this.txtGrandTotal.Text = "0.00";
            this.txtGrandTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGrandTotal.TextChanged += new System.EventHandler(this.txtGrandTotal_TextChanged);
            // 
            // txtTotalTax
            // 
            this.txtTotalTax.BackColor = System.Drawing.Color.Coral;
            this.txtTotalTax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalTax.Location = new System.Drawing.Point(102, 48);
            this.txtTotalTax.Name = "txtTotalTax";
            this.txtTotalTax.ReadOnly = true;
            this.txtTotalTax.Size = new System.Drawing.Size(123, 20);
            this.txtTotalTax.TabIndex = 11;
            this.txtTotalTax.Text = "0.00";
            this.txtTotalTax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotalDisc
            // 
            this.txtTotalDisc.BackColor = System.Drawing.Color.Coral;
            this.txtTotalDisc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalDisc.Location = new System.Drawing.Point(102, 72);
            this.txtTotalDisc.Name = "txtTotalDisc";
            this.txtTotalDisc.ReadOnly = true;
            this.txtTotalDisc.Size = new System.Drawing.Size(123, 20);
            this.txtTotalDisc.TabIndex = 10;
            this.txtTotalDisc.Text = "0.00";
            this.txtTotalDisc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSubTotal
            // 
            this.txtSubTotal.BackColor = System.Drawing.Color.Coral;
            this.txtSubTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSubTotal.Location = new System.Drawing.Point(102, 2);
            this.txtSubTotal.Name = "txtSubTotal";
            this.txtSubTotal.ReadOnly = true;
            this.txtSubTotal.Size = new System.Drawing.Size(123, 20);
            this.txtSubTotal.TabIndex = 92;
            this.txtSubTotal.Text = "0.00";
            this.txtSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(1, 173);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 514;
            this.label8.Text = "Grand Total";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 513;
            this.label7.Text = "Total Tax";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 512;
            this.label6.Text = "Total Disc.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 511;
            this.label5.Text = "Sub Total ";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblCompName);
            this.panel1.Controls.Add(this.txtChrgRupees1);
            this.panel1.Controls.Add(this.txtDiscRupees1);
            this.panel1.Controls.Add(this.txtRemark);
            this.panel1.Controls.Add(this.label32);
            this.panel1.Controls.Add(this.lblExchange);
            this.panel1.Controls.Add(this.txtDiscount1);
            this.panel1.Controls.Add(this.cmbPaymentType);
            this.panel1.Controls.Add(this.lblChrg1);
            this.panel1.Controls.Add(this.lblDisc1);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.lblBilExchangeItem);
            this.panel1.Controls.Add(this.lblBillItem);
            this.panel1.Location = new System.Drawing.Point(15, 528);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(394, 107);
            this.panel1.TabIndex = 91;
            // 
            // lblCompName
            // 
            this.lblCompName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblCompName.AutoSize = true;
            this.lblCompName.Location = new System.Drawing.Point(157, 21);
            this.lblCompName.Name = "lblCompName";
            this.lblCompName.Size = new System.Drawing.Size(40, 13);
            this.lblCompName.TabIndex = 5554;
            this.lblCompName.Text = "Party : ";
            // 
            // txtChrgRupees1
            // 
            this.txtChrgRupees1.Location = new System.Drawing.Point(62, 31);
            this.txtChrgRupees1.MaxLength = 6;
            this.txtChrgRupees1.Name = "txtChrgRupees1";
            this.txtChrgRupees1.Size = new System.Drawing.Size(81, 20);
            this.txtChrgRupees1.TabIndex = 11;
            this.txtChrgRupees1.Text = "0.00";
            this.txtChrgRupees1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtChrgRupees1.TextChanged += new System.EventHandler(this.txtChrgRupees1_TextChanged);
            this.txtChrgRupees1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtChrgRupees1_KeyDown);
            this.txtChrgRupees1.Leave += new System.EventHandler(this.Control_Leave);
            // 
            // txtDiscRupees1
            // 
            this.txtDiscRupees1.Location = new System.Drawing.Point(101, 6);
            this.txtDiscRupees1.Name = "txtDiscRupees1";
            this.txtDiscRupees1.Size = new System.Drawing.Size(42, 20);
            this.txtDiscRupees1.TabIndex = 520;
            this.txtDiscRupees1.Text = "0.00";
            this.txtDiscRupees1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDiscRupees1.TextChanged += new System.EventHandler(this.txtDiscRupees1_TextChanged);
            this.txtDiscRupees1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDiscRupees1_KeyDown);
            this.txtDiscRupees1.Leave += new System.EventHandler(this.Control_Leave);
            // 
            // txtRemark
            // 
            this.txtRemark.Location = new System.Drawing.Point(62, 80);
            this.txtRemark.Name = "txtRemark";
            this.txtRemark.Size = new System.Drawing.Size(316, 20);
            this.txtRemark.TabIndex = 13;
            this.txtRemark.Leave += new System.EventHandler(this.txtRemark_Leave);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(3, 83);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(50, 13);
            this.label32.TabIndex = 519;
            this.label32.Text = "Remark :";
            // 
            // lblExchange
            // 
            this.lblExchange.AutoSize = true;
            this.lblExchange.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExchange.ForeColor = System.Drawing.Color.Maroon;
            this.lblExchange.Location = new System.Drawing.Point(268, 34);
            this.lblExchange.Name = "lblExchange";
            this.lblExchange.Size = new System.Drawing.Size(122, 13);
            this.lblExchange.TabIndex = 510;
            this.lblExchange.Text = "-- Exchange Mode --";
            this.lblExchange.Visible = false;
            // 
            // txtDiscount1
            // 
            this.txtDiscount1.Location = new System.Drawing.Point(62, 6);
            this.txtDiscount1.Name = "txtDiscount1";
            this.txtDiscount1.Size = new System.Drawing.Size(34, 20);
            this.txtDiscount1.TabIndex = 7;
            this.txtDiscount1.Text = "0.00";
            this.txtDiscount1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDiscount1.TextChanged += new System.EventHandler(this.txtDiscount1_TextChanged);
            this.txtDiscount1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDiscount1_KeyDown);
            this.txtDiscount1.Leave += new System.EventHandler(this.Control_Leave);
            // 
            // cmbPaymentType
            // 
            this.cmbPaymentType.FormattingEnabled = true;
            this.cmbPaymentType.Items.AddRange(new object[] {
            "Cash",
            "Credit Card",
            "PA",
            "PP"});
            this.cmbPaymentType.Location = new System.Drawing.Point(62, 56);
            this.cmbPaymentType.Name = "cmbPaymentType";
            this.cmbPaymentType.Size = new System.Drawing.Size(172, 21);
            this.cmbPaymentType.TabIndex = 560;
            this.cmbPaymentType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbPaymentType_KeyDown);
            // 
            // lblChrg1
            // 
            this.lblChrg1.AutoSize = true;
            this.lblChrg1.Location = new System.Drawing.Point(0, 35);
            this.lblChrg1.Name = "lblChrg1";
            this.lblChrg1.Size = new System.Drawing.Size(53, 13);
            this.lblChrg1.TabIndex = 508;
            this.lblChrg1.Text = "Chrg 1.  : ";
            // 
            // lblDisc1
            // 
            this.lblDisc1.AutoSize = true;
            this.lblDisc1.Location = new System.Drawing.Point(0, 9);
            this.lblDisc1.Name = "lblDisc1";
            this.lblDisc1.Size = new System.Drawing.Size(57, 13);
            this.lblDisc1.TabIndex = 507;
            this.lblDisc1.Text = "Disc 1. % :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(1, 59);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 13);
            this.label13.TabIndex = 509;
            this.label13.Text = "Payment :";
            // 
            // lblBilExchangeItem
            // 
            this.lblBilExchangeItem.BackColor = System.Drawing.Color.Coral;
            this.lblBilExchangeItem.Location = new System.Drawing.Point(215, 34);
            this.lblBilExchangeItem.Name = "lblBilExchangeItem";
            this.lblBilExchangeItem.Size = new System.Drawing.Size(50, 13);
            this.lblBilExchangeItem.TabIndex = 506;
            // 
            // lblBillItem
            // 
            this.lblBillItem.BackColor = System.Drawing.Color.Coral;
            this.lblBillItem.Location = new System.Drawing.Point(159, 34);
            this.lblBillItem.Name = "lblBillItem";
            this.lblBillItem.Size = new System.Drawing.Size(50, 13);
            this.lblBillItem.TabIndex = 505;
            // 
            // txtRefNo
            // 
            this.txtRefNo.Location = new System.Drawing.Point(360, 5);
            this.txtRefNo.MaxLength = 20;
            this.txtRefNo.Name = "txtRefNo";
            this.txtRefNo.Size = new System.Drawing.Size(57, 20);
            this.txtRefNo.TabIndex = 2;
            this.txtRefNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRefNo.TextChanged += new System.EventHandler(this.txtRefNo_TextChanged);
            this.txtRefNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRefNo_KeyDown);
            this.txtRefNo.Leave += new System.EventHandler(this.txtRefNo_Leave);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Location = new System.Drawing.Point(308, 8);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(42, 13);
            this.label25.TabIndex = 5555;
            this.label25.Text = "Inv No:";
            // 
            // btndelete
            // 
            this.btndelete.Location = new System.Drawing.Point(901, 37);
            this.btndelete.Name = "btndelete";
            this.btndelete.Size = new System.Drawing.Size(69, 60);
            this.btndelete.TabIndex = 19;
            this.btndelete.Text = "Delete";
            this.btndelete.UseVisualStyleBackColor = true;
            this.btndelete.Visible = false;
            this.btndelete.Click += new System.EventHandler(this.btndelete_Click);
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnNext.Location = new System.Drawing.Point(519, 609);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(37, 27);
            this.btnNext.TabIndex = 23;
            this.btnNext.Text = ">";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPrev.Location = new System.Drawing.Point(467, 609);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(37, 27);
            this.btnPrev.TabIndex = 22;
            this.btnPrev.Text = "<";
            this.btnPrev.UseVisualStyleBackColor = true;
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // btnLast
            // 
            this.btnLast.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLast.Location = new System.Drawing.Point(569, 609);
            this.btnLast.Name = "btnLast";
            this.btnLast.Size = new System.Drawing.Size(37, 27);
            this.btnLast.TabIndex = 24;
            this.btnLast.Text = ">|";
            this.btnLast.UseVisualStyleBackColor = true;
            this.btnLast.Click += new System.EventHandler(this.btnLast_Click);
            // 
            // btnFirst
            // 
            this.btnFirst.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnFirst.Location = new System.Drawing.Point(415, 609);
            this.btnFirst.Name = "btnFirst";
            this.btnFirst.Size = new System.Drawing.Size(37, 27);
            this.btnFirst.TabIndex = 21;
            this.btnFirst.Text = "|<";
            this.btnFirst.UseVisualStyleBackColor = true;
            this.btnFirst.Click += new System.EventHandler(this.btnFirst_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(622, 258);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(69, 60);
            this.btnExit.TabIndex = 18;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(621, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(69, 60);
            this.btnSave.TabIndex = 14;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // pnlFooterInfo
            // 
            this.pnlFooterInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlFooterInfo.BackColor = System.Drawing.Color.Olive;
            this.pnlFooterInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlFooterInfo.Controls.Add(this.label12);
            this.pnlFooterInfo.Controls.Add(this.lblStatus);
            this.pnlFooterInfo.Controls.Add(this.label15);
            this.pnlFooterInfo.Controls.Add(this.label14);
            this.pnlFooterInfo.Location = new System.Drawing.Point(821, 576);
            this.pnlFooterInfo.Name = "pnlFooterInfo";
            this.pnlFooterInfo.Size = new System.Drawing.Size(393, 26);
            this.pnlFooterInfo.TabIndex = 71;
            this.pnlFooterInfo.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(258, 4);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(111, 13);
            this.label12.TabIndex = 524;
            this.label12.Text = "Ctrl + Q :New Item";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.ForeColor = System.Drawing.Color.Maroon;
            this.lblStatus.Location = new System.Drawing.Point(7, 54);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(11, 13);
            this.lblStatus.TabIndex = 523;
            this.lblStatus.Text = ".";
            this.lblStatus.Click += new System.EventHandler(this.lblStatus_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(125, 5);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(113, 13);
            this.label15.TabIndex = 516;
            this.label15.Text = "F2 : Give Discount";
            this.label15.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(3, 4);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(99, 13);
            this.label14.TabIndex = 515;
            this.label14.Text = "F7 : Qty Change";
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel3.BackColor = System.Drawing.Color.Coral;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label37);
            this.panel3.Controls.Add(this.txtlastBillQty);
            this.panel3.Controls.Add(this.txtLastPayment);
            this.panel3.Controls.Add(this.label23);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Controls.Add(this.txtLastBillAmt);
            this.panel3.Controls.Add(this.label24);
            this.panel3.Location = new System.Drawing.Point(775, 528);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(801, 29);
            this.panel3.TabIndex = 72;
            this.panel3.Visible = false;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Maroon;
            this.label37.Location = new System.Drawing.Point(689, 7);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(83, 13);
            this.label37.TabIndex = 530;
            this.label37.Text = "Ctrl + P: Print";
            // 
            // txtlastBillQty
            // 
            this.txtlastBillQty.BackColor = System.Drawing.Color.White;
            this.txtlastBillQty.Enabled = false;
            this.txtlastBillQty.Location = new System.Drawing.Point(331, 3);
            this.txtlastBillQty.Name = "txtlastBillQty";
            this.txtlastBillQty.ReadOnly = true;
            this.txtlastBillQty.Size = new System.Drawing.Size(107, 20);
            this.txtlastBillQty.TabIndex = 526;
            this.txtlastBillQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtLastPayment
            // 
            this.txtLastPayment.BackColor = System.Drawing.Color.White;
            this.txtLastPayment.Enabled = false;
            this.txtLastPayment.Location = new System.Drawing.Point(569, 3);
            this.txtLastPayment.Name = "txtLastPayment";
            this.txtLastPayment.ReadOnly = true;
            this.txtLastPayment.Size = new System.Drawing.Size(107, 20);
            this.txtLastPayment.TabIndex = 527;
            this.txtLastPayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Maroon;
            this.label23.Location = new System.Drawing.Point(227, 7);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(75, 13);
            this.label23.TabIndex = 528;
            this.label23.Text = "Last Bill Qty";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Maroon;
            this.label22.Location = new System.Drawing.Point(463, 7);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(55, 13);
            this.label22.TabIndex = 529;
            this.label22.Text = "Payment";
            // 
            // txtLastBillAmt
            // 
            this.txtLastBillAmt.BackColor = System.Drawing.Color.White;
            this.txtLastBillAmt.Enabled = false;
            this.txtLastBillAmt.Location = new System.Drawing.Point(107, 3);
            this.txtLastBillAmt.Name = "txtLastBillAmt";
            this.txtLastBillAmt.ReadOnly = true;
            this.txtLastBillAmt.Size = new System.Drawing.Size(107, 20);
            this.txtLastBillAmt.TabIndex = 13;
            this.txtLastBillAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Maroon;
            this.label24.Location = new System.Drawing.Point(4, 7);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(98, 13);
            this.label24.TabIndex = 523;
            this.label24.Text = "Last Bill Amount";
            // 
            // dgBill
            // 
            this.dgBill.AllowUserToAddRows = false;
            this.dgBill.AllowUserToDeleteRows = false;
            this.dgBill.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgBill.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.ItemName,
            this.Quantity,
            this.UOM,
            this.Rate,
            this.NetRate,
            this.DiscPerce,
            this.DiscAmt,
            this.DiscRupees,
            this.DiscPercentage2,
            this.DiscAmount2,
            this.NetAmt,
            this.TaxPerce,
            this.TaxAmount,
            this.DiscRupees2,
            this.Amount,
            this.Barcode,
            this.PkStockTrnNo,
            this.PkSrNo,
            this.PkVoucherNo,
            this.ItemNo,
            this.UOMNo,
            this.TaxLedgerNo,
            this.SalesLedgerNo,
            this.PkRateSettingNo,
            this.PkItemTaxInfo,
            this.StockFactor,
            this.ActualQty,
            this.MKTQuantity,
            this.SalesVchNo,
            this.TaxVchNo,
            this.StockCompanyNo,
            this.BarCodePrinting,
            this.HSNCode,
            this.IGSTPercent,
            this.IGSTAmount,
            this.CGSTPercent,
            this.CGSTAmount,
            this.SGSTPercent,
            this.SGSTAmount,
            this.UTGSTPercent,
            this.UTGSTAmount,
            this.CessPercent,
            this.CessAmount,
            this.MRP,
            this.Godown});
            this.dgBill.Location = new System.Drawing.Point(13, 67);
            this.dgBill.Name = "dgBill";
            this.dgBill.Size = new System.Drawing.Size(593, 458);
            this.dgBill.TabIndex = 6;
            this.dgBill.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgBill_CellValueChanged);
            this.dgBill.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgBill_CellFormatting);
            this.dgBill.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgBill_CellEndEdit);
            this.dgBill.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgBill_CellClick);
            this.dgBill.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgBill_EditingControlShowing);
            this.dgBill.CurrentCellChanged += new System.EventHandler(this.dgBill_CurrentCellChanged);
            this.dgBill.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgBill_KeyDown);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(621, 131);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(69, 60);
            this.btnSearch.TabIndex = 20;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // lblMsg
            // 
            this.lblMsg.BackColor = System.Drawing.Color.Coral;
            this.lblMsg.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMsg.CornerRadius = 3;
            this.lblMsg.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg.ForeColor = System.Drawing.Color.Maroon;
            this.lblMsg.GradientBottom = System.Drawing.Color.Coral;
            this.lblMsg.GradientMiddle = System.Drawing.Color.White;
            this.lblMsg.GradientShow = true;
            this.lblMsg.GradientTop = System.Drawing.Color.Coral;
            this.lblMsg.Location = new System.Drawing.Point(121, 254);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(481, 52);
            this.lblMsg.TabIndex = 505;
            this.lblMsg.Text = "label4";
            this.lblMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMsg.Visible = false;
            // 
            // pnlPartial
            // 
            this.pnlPartial.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlPartial.Controls.Add(this.label38);
            this.pnlPartial.Controls.Add(this.dgPayCreditCardDetails);
            this.pnlPartial.Controls.Add(this.pnlBranch);
            this.pnlPartial.Controls.Add(this.pnlBank);
            this.pnlPartial.Controls.Add(this.dtpChqDate);
            this.pnlPartial.Controls.Add(this.dgPayChqDetails);
            this.pnlPartial.Controls.Add(this.lblPayTypeBal);
            this.pnlPartial.Controls.Add(this.dgPayType);
            this.pnlPartial.Controls.Add(this.btnOk);
            this.pnlPartial.Controls.Add(this.txtTotalAmt);
            this.pnlPartial.Controls.Add(this.label26);
            this.pnlPartial.Location = new System.Drawing.Point(200, 123);
            this.pnlPartial.Name = "pnlPartial";
            this.pnlPartial.Size = new System.Drawing.Size(305, 221);
            this.pnlPartial.TabIndex = 506;
            this.pnlPartial.Visible = false;
            // 
            // label38
            // 
            this.label38.Location = new System.Drawing.Point(6, 131);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(288, 37);
            this.label38.TabIndex = 5548;
            this.label38.Text = "(Ctrl + D) Display Details (For Cheque and Credit Card.";
            // 
            // dgPayCreditCardDetails
            // 
            this.dgPayCreditCardDetails.AllowUserToAddRows = false;
            this.dgPayCreditCardDetails.AllowUserToDeleteRows = false;
            this.dgPayCreditCardDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPayCreditCardDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CreditCardNo,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12});
            this.dgPayCreditCardDetails.Location = new System.Drawing.Point(304, 3);
            this.dgPayCreditCardDetails.Name = "dgPayCreditCardDetails";
            this.dgPayCreditCardDetails.Size = new System.Drawing.Size(464, 169);
            this.dgPayCreditCardDetails.TabIndex = 5547;
            this.dgPayCreditCardDetails.Visible = false;
            this.dgPayCreditCardDetails.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPayCreditCardDetails_CellEndEdit);
            this.dgPayCreditCardDetails.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgPayCreditCardDetails_KeyDown);
            // 
            // CreditCardNo
            // 
            this.CreditCardNo.DataPropertyName = "CreditCardNo";
            this.CreditCardNo.HeaderText = "Cr. Card No";
            this.CreditCardNo.MaxInputLength = 20;
            this.CreditCardNo.Name = "CreditCardNo";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "BankName";
            this.dataGridViewTextBoxColumn7.HeaderText = "Bank Name";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 120;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "BranchName";
            this.dataGridViewTextBoxColumn8.HeaderText = "BranchName";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 120;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "Amount";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewTextBoxColumn9.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "PkSrNo";
            this.dataGridViewTextBoxColumn10.HeaderText = "PkSrNo";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Visible = false;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "BankNo";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.Visible = false;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "BranchNo";
            this.dataGridViewTextBoxColumn12.HeaderText = "BranchNo";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.Visible = false;
            // 
            // pnlBranch
            // 
            this.pnlBranch.BackColor = System.Drawing.Color.Coral;
            this.pnlBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBranch.Controls.Add(this.lstBranch);
            this.pnlBranch.Location = new System.Drawing.Point(538, 39);
            this.pnlBranch.Name = "pnlBranch";
            this.pnlBranch.Size = new System.Drawing.Size(164, 117);
            this.pnlBranch.TabIndex = 5544;
            this.pnlBranch.Visible = false;
            // 
            // lstBranch
            // 
            this.lstBranch.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstBranch.FormattingEnabled = true;
            this.lstBranch.Location = new System.Drawing.Point(8, 8);
            this.lstBranch.Name = "lstBranch";
            this.lstBranch.Size = new System.Drawing.Size(147, 93);
            this.lstBranch.TabIndex = 516;
            this.lstBranch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstBranch_KeyDown);
            // 
            // pnlBank
            // 
            this.pnlBank.BackColor = System.Drawing.Color.Coral;
            this.pnlBank.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBank.Controls.Add(this.lstBank);
            this.pnlBank.Location = new System.Drawing.Point(352, 38);
            this.pnlBank.Name = "pnlBank";
            this.pnlBank.Size = new System.Drawing.Size(176, 118);
            this.pnlBank.TabIndex = 5545;
            this.pnlBank.Visible = false;
            // 
            // lstBank
            // 
            this.lstBank.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstBank.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstBank.FormattingEnabled = true;
            this.lstBank.Location = new System.Drawing.Point(8, 10);
            this.lstBank.Name = "lstBank";
            this.lstBank.Size = new System.Drawing.Size(159, 93);
            this.lstBank.TabIndex = 516;
            this.lstBank.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstBank_KeyDown);
            // 
            // dtpChqDate
            // 
            this.dtpChqDate.Location = new System.Drawing.Point(471, 45);
            this.dtpChqDate.Name = "dtpChqDate";
            this.dtpChqDate.TabIndex = 5546;
            this.dtpChqDate.Visible = false;
            this.dtpChqDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpChqDate_KeyDown);
            // 
            // dgPayChqDetails
            // 
            this.dgPayChqDetails.AllowUserToAddRows = false;
            this.dgPayChqDetails.AllowUserToDeleteRows = false;
            this.dgPayChqDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPayChqDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ChequeNo,
            this.ChequeDate,
            this.BankName,
            this.BranchName,
            this.AmountChq,
            this.PkSrNoChq,
            this.BankNo,
            this.BranchNo});
            this.dgPayChqDetails.Location = new System.Drawing.Point(304, 3);
            this.dgPayChqDetails.Name = "dgPayChqDetails";
            this.dgPayChqDetails.Size = new System.Drawing.Size(464, 169);
            this.dgPayChqDetails.TabIndex = 659;
            this.dgPayChqDetails.Visible = false;
            this.dgPayChqDetails.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPayChqDetails_CellEndEdit);
            this.dgPayChqDetails.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgPayChqDetails_KeyDown);
            // 
            // ChequeNo
            // 
            this.ChequeNo.DataPropertyName = "ChequeNo";
            this.ChequeNo.HeaderText = "Chq. No";
            this.ChequeNo.MaxInputLength = 10;
            this.ChequeNo.Name = "ChequeNo";
            this.ChequeNo.Width = 75;
            // 
            // ChequeDate
            // 
            this.ChequeDate.DataPropertyName = "ChequeDate";
            this.ChequeDate.HeaderText = "Date";
            this.ChequeDate.Name = "ChequeDate";
            // 
            // BankName
            // 
            this.BankName.DataPropertyName = "BankName";
            this.BankName.HeaderText = "Bank Name";
            this.BankName.Name = "BankName";
            this.BankName.ReadOnly = true;
            this.BankName.Width = 120;
            // 
            // BranchName
            // 
            this.BranchName.DataPropertyName = "BranchName";
            this.BranchName.HeaderText = "BranchName";
            this.BranchName.Name = "BranchName";
            this.BranchName.ReadOnly = true;
            this.BranchName.Width = 120;
            // 
            // AmountChq
            // 
            this.AmountChq.DataPropertyName = "Amount";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.AmountChq.DefaultCellStyle = dataGridViewCellStyle15;
            this.AmountChq.HeaderText = "Amount";
            this.AmountChq.Name = "AmountChq";
            // 
            // PkSrNoChq
            // 
            this.PkSrNoChq.DataPropertyName = "PkSrNo";
            this.PkSrNoChq.HeaderText = "PkSrNo";
            this.PkSrNoChq.Name = "PkSrNoChq";
            this.PkSrNoChq.Visible = false;
            // 
            // BankNo
            // 
            this.BankNo.HeaderText = "BankNo";
            this.BankNo.Name = "BankNo";
            this.BankNo.Visible = false;
            // 
            // BranchNo
            // 
            this.BranchNo.DataPropertyName = "BranchNo";
            this.BranchNo.HeaderText = "BranchNo";
            this.BranchNo.Name = "BranchNo";
            this.BranchNo.Visible = false;
            // 
            // lblPayTypeBal
            // 
            this.lblPayTypeBal.AutoSize = true;
            this.lblPayTypeBal.Location = new System.Drawing.Point(86, 185);
            this.lblPayTypeBal.Name = "lblPayTypeBal";
            this.lblPayTypeBal.Size = new System.Drawing.Size(0, 13);
            this.lblPayTypeBal.TabIndex = 658;
            // 
            // dgPayType
            // 
            this.dgPayType.AllowUserToAddRows = false;
            this.dgPayType.AllowUserToDeleteRows = false;
            this.dgPayType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPayType.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn5,
            this.PKVoucherPayTypeNo});
            this.dgPayType.Location = new System.Drawing.Point(2, 3);
            this.dgPayType.Name = "dgPayType";
            this.dgPayType.Size = new System.Drawing.Size(295, 169);
            this.dgPayType.TabIndex = 657;
            this.dgPayType.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPayType_CellEndEdit);
            this.dgPayType.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgPayType_EditingControlShowing);
            this.dgPayType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgPayType_KeyDown);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "PayTypeName";
            this.dataGridViewTextBoxColumn1.HeaderText = "Pay Type";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "PKPayTypeNo";
            this.dataGridViewTextBoxColumn4.HeaderText = "PayTypeNo";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Amount";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridViewTextBoxColumn2.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "LedgerNo";
            this.dataGridViewTextBoxColumn5.HeaderText = "LedgerNo";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // PKVoucherPayTypeNo
            // 
            this.PKVoucherPayTypeNo.DataPropertyName = "PKVoucherPayTypeNo";
            this.PKVoucherPayTypeNo.HeaderText = "PKVoucherPayTypeNo";
            this.PKVoucherPayTypeNo.Name = "PKVoucherPayTypeNo";
            this.PKVoucherPayTypeNo.Visible = false;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(3, 178);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // txtTotalAmt
            // 
            this.txtTotalAmt.BackColor = System.Drawing.Color.White;
            this.txtTotalAmt.Enabled = false;
            this.txtTotalAmt.Location = new System.Drawing.Point(222, 178);
            this.txtTotalAmt.Name = "txtTotalAmt";
            this.txtTotalAmt.ReadOnly = true;
            this.txtTotalAmt.Size = new System.Drawing.Size(72, 20);
            this.txtTotalAmt.TabIndex = 656;
            this.txtTotalAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(171, 183);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(37, 13);
            this.label26.TabIndex = 502;
            this.label26.Text = "Total :";
            // 
            // dgParkingBills
            // 
            this.dgParkingBills.AllowUserToAddRows = false;
            this.dgParkingBills.AllowUserToDeleteRows = false;
            this.dgParkingBills.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgParkingBills.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BillNo,
            this.BillAmount});
            this.dgParkingBills.Location = new System.Drawing.Point(5, 4);
            this.dgParkingBills.Name = "dgParkingBills";
            this.dgParkingBills.ReadOnly = true;
            this.dgParkingBills.Size = new System.Drawing.Size(193, 132);
            this.dgParkingBills.TabIndex = 0;
            // 
            // BillNo
            // 
            this.BillNo.DataPropertyName = "BillNo";
            this.BillNo.HeaderText = "BillNo";
            this.BillNo.Name = "BillNo";
            this.BillNo.ReadOnly = true;
            this.BillNo.Width = 50;
            // 
            // BillAmount
            // 
            this.BillAmount.DataPropertyName = "Amount";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.BillAmount.DefaultCellStyle = dataGridViewCellStyle17;
            this.BillAmount.HeaderText = "Amount";
            this.BillAmount.Name = "BillAmount";
            this.BillAmount.ReadOnly = true;
            // 
            // pnlParking
            // 
            this.pnlParking.Controls.Add(this.dgParkingBills);
            this.pnlParking.Location = new System.Drawing.Point(281, 174);
            this.pnlParking.Name = "pnlParking";
            this.pnlParking.Size = new System.Drawing.Size(203, 141);
            this.pnlParking.TabIndex = 508;
            this.pnlParking.Visible = false;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(621, 67);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(69, 60);
            this.btnUpdate.TabIndex = 16;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(621, 3);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(69, 60);
            this.btnNew.TabIndex = 15;
            this.btnNew.Text = "New";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(622, 194);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(69, 60);
            this.btnCancel.TabIndex = 17;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // pnlSearch
            // 
            this.pnlSearch.BorderColor = System.Drawing.Color.Empty;
            this.pnlSearch.BorderRadius = 3;
            this.pnlSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSearch.Controls.Add(this.rbPartyName);
            this.pnlSearch.Controls.Add(this.rbDocNo);
            this.pnlSearch.Controls.Add(this.rbInvNo);
            this.pnlSearch.Controls.Add(this.cmbPartyNameSearch);
            this.pnlSearch.Controls.Add(this.txtInvNoSearch);
            this.pnlSearch.Controls.Add(this.btnCancelSearch);
            this.pnlSearch.Controls.Add(this.lblLable);
            this.pnlSearch.Controls.Add(this.txtSearch);
            this.pnlSearch.Location = new System.Drawing.Point(164, 269);
            this.pnlSearch.Name = "pnlSearch";
            this.pnlSearch.Size = new System.Drawing.Size(424, 72);
            this.pnlSearch.TabIndex = 509;
            this.pnlSearch.Visible = false;
            // 
            // rbPartyName
            // 
            this.rbPartyName.AutoSize = true;
            this.rbPartyName.BackColor = System.Drawing.Color.Transparent;
            this.rbPartyName.Location = new System.Drawing.Point(203, 9);
            this.rbPartyName.Name = "rbPartyName";
            this.rbPartyName.Size = new System.Drawing.Size(80, 17);
            this.rbPartyName.TabIndex = 2;
            this.rbPartyName.TabStop = true;
            this.rbPartyName.Text = "Party Name";
            this.rbPartyName.UseVisualStyleBackColor = false;
            this.rbPartyName.CheckedChanged += new System.EventHandler(this.rbType_CheckedChanged);
            this.rbPartyName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rbType_KeyDown);
            // 
            // rbDocNo
            // 
            this.rbDocNo.AutoSize = true;
            this.rbDocNo.BackColor = System.Drawing.Color.Transparent;
            this.rbDocNo.Location = new System.Drawing.Point(107, 9);
            this.rbDocNo.Name = "rbDocNo";
            this.rbDocNo.Size = new System.Drawing.Size(62, 17);
            this.rbDocNo.TabIndex = 1;
            this.rbDocNo.TabStop = true;
            this.rbDocNo.Text = "Doc No";
            this.rbDocNo.UseVisualStyleBackColor = false;
            this.rbDocNo.CheckedChanged += new System.EventHandler(this.rbType_CheckedChanged);
            this.rbDocNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rbType_KeyDown);
            // 
            // rbInvNo
            // 
            this.rbInvNo.AutoSize = true;
            this.rbInvNo.BackColor = System.Drawing.Color.Transparent;
            this.rbInvNo.Checked = true;
            this.rbInvNo.Location = new System.Drawing.Point(8, 9);
            this.rbInvNo.Name = "rbInvNo";
            this.rbInvNo.Size = new System.Drawing.Size(57, 17);
            this.rbInvNo.TabIndex = 0;
            this.rbInvNo.TabStop = true;
            this.rbInvNo.Text = "Inv No";
            this.rbInvNo.UseVisualStyleBackColor = false;
            this.rbInvNo.CheckedChanged += new System.EventHandler(this.rbType_CheckedChanged);
            this.rbInvNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rbType_KeyDown);
            // 
            // cmbPartyNameSearch
            // 
            this.cmbPartyNameSearch.FormattingEnabled = true;
            this.cmbPartyNameSearch.Location = new System.Drawing.Point(236, 38);
            this.cmbPartyNameSearch.Name = "cmbPartyNameSearch";
            this.cmbPartyNameSearch.Size = new System.Drawing.Size(139, 21);
            this.cmbPartyNameSearch.TabIndex = 500000000;
            this.cmbPartyNameSearch.Visible = false;
            this.cmbPartyNameSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbPartyNameSearch_KeyDown);
            // 
            // txtInvNoSearch
            // 
            this.txtInvNoSearch.Location = new System.Drawing.Point(153, 39);
            this.txtInvNoSearch.Name = "txtInvNoSearch";
            this.txtInvNoSearch.Size = new System.Drawing.Size(77, 20);
            this.txtInvNoSearch.TabIndex = 50000000;
            this.txtInvNoSearch.Visible = false;
            this.txtInvNoSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtInvNoSearch_KeyDown);
            // 
            // btnCancelSearch
            // 
            this.btnCancelSearch.Location = new System.Drawing.Point(296, 7);
            this.btnCancelSearch.Name = "btnCancelSearch";
            this.btnCancelSearch.Size = new System.Drawing.Size(79, 23);
            this.btnCancelSearch.TabIndex = 4;
            this.btnCancelSearch.Text = "Cancel";
            this.btnCancelSearch.UseVisualStyleBackColor = true;
            this.btnCancelSearch.Click += new System.EventHandler(this.btnCancelSearch_Click);
            // 
            // lblLable
            // 
            this.lblLable.AutoSize = true;
            this.lblLable.BackColor = System.Drawing.Color.Transparent;
            this.lblLable.Location = new System.Drawing.Point(6, 42);
            this.lblLable.Name = "lblLable";
            this.lblLable.Size = new System.Drawing.Size(45, 13);
            this.lblLable.TabIndex = 502;
            this.lblLable.Text = "Inv No :";
            this.lblLable.Visible = false;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(70, 39);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(77, 20);
            this.txtSearch.TabIndex = 500000;
            this.txtSearch.Visible = false;
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            // 
            // pnlItemName
            // 
            this.pnlItemName.BackColor = System.Drawing.Color.Coral;
            this.pnlItemName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlItemName.Controls.Add(this.dgItemList);
            this.pnlItemName.Location = new System.Drawing.Point(15, 88);
            this.pnlItemName.Name = "pnlItemName";
            this.pnlItemName.Size = new System.Drawing.Size(79, 48);
            this.pnlItemName.TabIndex = 514;
            this.pnlItemName.Visible = false;
            this.pnlItemName.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            // 
            // dgItemList
            // 
            this.dgItemList.AllowUserToAddRows = false;
            this.dgItemList.AllowUserToDeleteRows = false;
            this.dgItemList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgItemList.BackgroundColor = System.Drawing.Color.Coral;
            this.dgItemList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgItemList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iItemNo,
            this.iItemName,
            this.ItemNameLang,
            this.iRate,
            this.iUOM,
            this.iMRP,
            this.iStock,
            this.iUOMStk,
            this.iSaleTax,
            this.iPurTax,
            this.iCompany,
            this.iBarcode,
            this.iRateSettingNo,
            this.UOMDefault,
            this.PurRate});
            this.dgItemList.Location = new System.Drawing.Point(8, 8);
            this.dgItemList.Name = "dgItemList";
            this.dgItemList.Size = new System.Drawing.Size(730, 25);
            this.dgItemList.TabIndex = 515;
            this.dgItemList.CurrentCellChanged += new System.EventHandler(this.dgItemList_CurrentCellChanged);
            this.dgItemList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgItemList_KeyDown);
            this.dgItemList.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            // 
            // iItemNo
            // 
            this.iItemNo.DataPropertyName = "ItemNo";
            this.iItemNo.HeaderText = "ItemNo";
            this.iItemNo.Name = "iItemNo";
            this.iItemNo.ReadOnly = true;
            this.iItemNo.ToolTipText = "ItemNo";
            this.iItemNo.Visible = false;
            // 
            // iItemName
            // 
            this.iItemName.DataPropertyName = "ItemName";
            this.iItemName.HeaderText = "Item Description";
            this.iItemName.Name = "iItemName";
            this.iItemName.ReadOnly = true;
            this.iItemName.ToolTipText = "Product Name";
            this.iItemName.Width = 200;
            // 
            // ItemNameLang
            // 
            this.ItemNameLang.DataPropertyName = "ItemNameLang";
            this.ItemNameLang.HeaderText = "Item Name";
            this.ItemNameLang.Name = "ItemNameLang";
            // 
            // iRate
            // 
            this.iRate.DataPropertyName = "SaleRate";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle18.Format = "N2";
            dataGridViewCellStyle18.NullValue = null;
            this.iRate.DefaultCellStyle = dataGridViewCellStyle18;
            this.iRate.HeaderText = "Rate";
            this.iRate.Name = "iRate";
            this.iRate.ReadOnly = true;
            this.iRate.ToolTipText = "Sales Rate";
            this.iRate.Width = 60;
            // 
            // iUOM
            // 
            this.iUOM.DataPropertyName = "UOMName";
            this.iUOM.HeaderText = "UOM";
            this.iUOM.Name = "iUOM";
            this.iUOM.ReadOnly = true;
            this.iUOM.ToolTipText = "UOM";
            this.iUOM.Width = 60;
            // 
            // iMRP
            // 
            this.iMRP.DataPropertyName = "MRP";
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle19.Format = "N2";
            this.iMRP.DefaultCellStyle = dataGridViewCellStyle19;
            this.iMRP.HeaderText = "MRP";
            this.iMRP.Name = "iMRP";
            this.iMRP.ReadOnly = true;
            this.iMRP.ToolTipText = "MRP";
            this.iMRP.Width = 60;
            // 
            // iStock
            // 
            this.iStock.DataPropertyName = "Stock";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle20.Format = "N2";
            this.iStock.DefaultCellStyle = dataGridViewCellStyle20;
            this.iStock.HeaderText = "Stock";
            this.iStock.Name = "iStock";
            this.iStock.ReadOnly = true;
            this.iStock.ToolTipText = "Stock QTY";
            this.iStock.Width = 60;
            // 
            // iUOMStk
            // 
            this.iUOMStk.DataPropertyName = "stkUOM";
            this.iUOMStk.HeaderText = "UOM";
            this.iUOMStk.Name = "iUOMStk";
            this.iUOMStk.ReadOnly = true;
            this.iUOMStk.ToolTipText = "Stock UOM";
            this.iUOMStk.Visible = false;
            this.iUOMStk.Width = 50;
            // 
            // iSaleTax
            // 
            this.iSaleTax.DataPropertyName = "SaleTax";
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle21.Format = "N2";
            dataGridViewCellStyle21.NullValue = null;
            this.iSaleTax.DefaultCellStyle = dataGridViewCellStyle21;
            this.iSaleTax.HeaderText = "S.Tax";
            this.iSaleTax.Name = "iSaleTax";
            this.iSaleTax.ReadOnly = true;
            this.iSaleTax.ToolTipText = "Sales Tax Percent";
            this.iSaleTax.Visible = false;
            this.iSaleTax.Width = 50;
            // 
            // iPurTax
            // 
            this.iPurTax.DataPropertyName = "PurTax";
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle22.Format = "N2";
            dataGridViewCellStyle22.NullValue = null;
            this.iPurTax.DefaultCellStyle = dataGridViewCellStyle22;
            this.iPurTax.HeaderText = "P.Tax";
            this.iPurTax.Name = "iPurTax";
            this.iPurTax.ReadOnly = true;
            this.iPurTax.ToolTipText = "Purchase Tax Percent";
            this.iPurTax.Visible = false;
            this.iPurTax.Width = 50;
            // 
            // iCompany
            // 
            this.iCompany.DataPropertyName = "CompanyNo";
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle23.Format = "N0";
            dataGridViewCellStyle23.NullValue = null;
            this.iCompany.DefaultCellStyle = dataGridViewCellStyle23;
            this.iCompany.HeaderText = "CNo";
            this.iCompany.Name = "iCompany";
            this.iCompany.ReadOnly = true;
            this.iCompany.ToolTipText = "Company No";
            this.iCompany.Visible = false;
            this.iCompany.Width = 30;
            // 
            // iBarcode
            // 
            this.iBarcode.DataPropertyName = "Barcode";
            this.iBarcode.HeaderText = "Barcode";
            this.iBarcode.Name = "iBarcode";
            this.iBarcode.ReadOnly = true;
            this.iBarcode.ToolTipText = "Barcode";
            this.iBarcode.Width = 125;
            // 
            // iRateSettingNo
            // 
            this.iRateSettingNo.DataPropertyName = "RateSettingNo";
            this.iRateSettingNo.HeaderText = "RateSettingNo";
            this.iRateSettingNo.Name = "iRateSettingNo";
            this.iRateSettingNo.ReadOnly = true;
            this.iRateSettingNo.Visible = false;
            // 
            // UOMDefault
            // 
            this.UOMDefault.DataPropertyName = "UOMDefault";
            this.UOMDefault.HeaderText = "UOMDefault";
            this.UOMDefault.Name = "UOMDefault";
            this.UOMDefault.ReadOnly = true;
            this.UOMDefault.Visible = false;
            // 
            // PurRate
            // 
            this.PurRate.DataPropertyName = "PurRate";
            this.PurRate.HeaderText = "PurRate";
            this.PurRate.Name = "PurRate";
            this.PurRate.ReadOnly = true;
            this.PurRate.Visible = false;
            // 
            // lstUOM
            // 
            this.lstUOM.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstUOM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstUOM.FormattingEnabled = true;
            this.lstUOM.Location = new System.Drawing.Point(8, 8);
            this.lstUOM.Name = "lstUOM";
            this.lstUOM.Size = new System.Drawing.Size(101, 28);
            this.lstUOM.TabIndex = 516;
            this.lstUOM.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            this.lstUOM.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lstUOM_KeyPress);
            // 
            // lstRate
            // 
            this.lstRate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstRate.FormattingEnabled = true;
            this.lstRate.Location = new System.Drawing.Point(8, 8);
            this.lstRate.Name = "lstRate";
            this.lstRate.Size = new System.Drawing.Size(101, 41);
            this.lstRate.TabIndex = 517;
            this.lstRate.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            this.lstRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lstRate_KeyPress);
            // 
            // lstGroup1
            // 
            this.lstGroup1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstGroup1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstGroup1.FormattingEnabled = true;
            this.lstGroup1.Location = new System.Drawing.Point(8, 8);
            this.lstGroup1.Name = "lstGroup1";
            this.lstGroup1.Size = new System.Drawing.Size(101, 28);
            this.lstGroup1.TabIndex = 516;
            this.lstGroup1.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            this.lstGroup1.SelectedIndexChanged += new System.EventHandler(this.lstGroup1_SelectedIndexChanged);
            this.lstGroup1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lstGroup1_KeyPress);
            this.lstGroup1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstGroup1_KeyDown);
            // 
            // lstGroup2
            // 
            this.lstGroup2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstGroup2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstGroup2.FormattingEnabled = true;
            this.lstGroup2.Location = new System.Drawing.Point(8, 8);
            this.lstGroup2.Name = "lstGroup2";
            this.lstGroup2.Size = new System.Drawing.Size(101, 28);
            this.lstGroup2.TabIndex = 517;
            this.lstGroup2.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            this.lstGroup2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lstGroup2_KeyPress);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Location = new System.Drawing.Point(10, 38);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(49, 13);
            this.label33.TabIndex = 551;
            this.label33.Text = "Party    : ";
            // 
            // cmbPartyName
            // 
            this.cmbPartyName.FormattingEnabled = true;
            this.cmbPartyName.Location = new System.Drawing.Point(66, 37);
            this.cmbPartyName.Name = "cmbPartyName";
            this.cmbPartyName.Size = new System.Drawing.Size(344, 21);
            this.cmbPartyName.TabIndex = 4;
            this.cmbPartyName.Leave += new System.EventHandler(this.cmbPartyName_Leave);
            this.cmbPartyName.SelectedValueChanged += new System.EventHandler(this.cmbPartyName_SelectedValueChanged);
            this.cmbPartyName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbPartyName_KeyDown);
            // 
            // cmbTaxType
            // 
            this.cmbTaxType.FormattingEnabled = true;
            this.cmbTaxType.Location = new System.Drawing.Point(517, 37);
            this.cmbTaxType.Name = "cmbTaxType";
            this.cmbTaxType.Size = new System.Drawing.Size(89, 21);
            this.cmbTaxType.TabIndex = 5;
            this.cmbTaxType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbTaxType_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(451, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 5542;
            this.label4.Text = "Tax        :";
            // 
            // lblRateType
            // 
            this.lblRateType.AutoSize = true;
            this.lblRateType.BackColor = System.Drawing.Color.Transparent;
            this.lblRateType.Location = new System.Drawing.Point(443, 10);
            this.lblRateType.Name = "lblRateType";
            this.lblRateType.Size = new System.Drawing.Size(57, 13);
            this.lblRateType.TabIndex = 556;
            this.lblRateType.Text = "Rate Type";
            // 
            // cmbRateType
            // 
            this.cmbRateType.FormattingEnabled = true;
            this.cmbRateType.Location = new System.Drawing.Point(517, 6);
            this.cmbRateType.Name = "cmbRateType";
            this.cmbRateType.Size = new System.Drawing.Size(90, 21);
            this.cmbRateType.TabIndex = 553;
            this.cmbRateType.SelectedIndexChanged += new System.EventHandler(this.cmbRateType_SelectedIndexChanged);
            this.cmbRateType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbRateType_KeyDown);
            // 
            // pnlGroup1
            // 
            this.pnlGroup1.BackColor = System.Drawing.Color.Coral;
            this.pnlGroup1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlGroup1.Controls.Add(this.lstGroup1Lang);
            this.pnlGroup1.Controls.Add(this.lstGroup1);
            this.pnlGroup1.Location = new System.Drawing.Point(18, 154);
            this.pnlGroup1.Name = "pnlGroup1";
            this.pnlGroup1.Size = new System.Drawing.Size(118, 46);
            this.pnlGroup1.TabIndex = 5543;
            this.pnlGroup1.Visible = false;
            this.pnlGroup1.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            // 
            // lstGroup1Lang
            // 
            this.lstGroup1Lang.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstGroup1Lang.FormattingEnabled = true;
            this.lstGroup1Lang.Location = new System.Drawing.Point(286, 7);
            this.lstGroup1Lang.Name = "lstGroup1Lang";
            this.lstGroup1Lang.Size = new System.Drawing.Size(279, 262);
            this.lstGroup1Lang.TabIndex = 517;
            this.lstGroup1Lang.SelectedIndexChanged += new System.EventHandler(this.lstGroup1Lang_SelectedIndexChanged);
            this.lstGroup1Lang.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lstGroup1_KeyPress);
            // 
            // pnlGroup2
            // 
            this.pnlGroup2.BackColor = System.Drawing.Color.Coral;
            this.pnlGroup2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlGroup2.Controls.Add(this.lstGroup2);
            this.pnlGroup2.Location = new System.Drawing.Point(19, 203);
            this.pnlGroup2.Name = "pnlGroup2";
            this.pnlGroup2.Size = new System.Drawing.Size(118, 47);
            this.pnlGroup2.TabIndex = 5544;
            this.pnlGroup2.Visible = false;
            this.pnlGroup2.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            // 
            // pnlUOM
            // 
            this.pnlUOM.BackColor = System.Drawing.Color.Coral;
            this.pnlUOM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlUOM.Controls.Add(this.lstUOM);
            this.pnlUOM.Location = new System.Drawing.Point(18, 256);
            this.pnlUOM.Name = "pnlUOM";
            this.pnlUOM.Size = new System.Drawing.Size(118, 50);
            this.pnlUOM.TabIndex = 5545;
            this.pnlUOM.Visible = false;
            this.pnlUOM.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            // 
            // pnlRate
            // 
            this.pnlRate.BackColor = System.Drawing.Color.Coral;
            this.pnlRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRate.Controls.Add(this.lstRate);
            this.pnlRate.Location = new System.Drawing.Point(170, 88);
            this.pnlRate.Name = "pnlRate";
            this.pnlRate.Size = new System.Drawing.Size(118, 70);
            this.pnlRate.TabIndex = 5546;
            this.pnlRate.Visible = false;
            this.pnlRate.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Coral;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Location = new System.Drawing.Point(170, 177);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(118, 47);
            this.panel4.TabIndex = 5547;
            this.panel4.Visible = false;
            // 
            // pnlSalePurHistory
            // 
            this.pnlSalePurHistory.BackColor = System.Drawing.Color.Coral;
            this.pnlSalePurHistory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSalePurHistory.Controls.Add(this.dgPurHistory);
            this.pnlSalePurHistory.Controls.Add(this.dgSaleHistory);
            this.pnlSalePurHistory.Location = new System.Drawing.Point(958, 227);
            this.pnlSalePurHistory.Name = "pnlSalePurHistory";
            this.pnlSalePurHistory.Size = new System.Drawing.Size(23, 20);
            this.pnlSalePurHistory.TabIndex = 5548;
            this.pnlSalePurHistory.Visible = false;
            // 
            // dgPurHistory
            // 
            this.dgPurHistory.AllowUserToAddRows = false;
            this.dgPurHistory.AllowUserToDeleteRows = false;
            this.dgPurHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgPurHistory.BackgroundColor = System.Drawing.Color.Coral;
            this.dgPurHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPurHistory.Location = new System.Drawing.Point(337, 8);
            this.dgPurHistory.Name = "dgPurHistory";
            this.dgPurHistory.Size = new System.Drawing.Size(0, 3);
            this.dgPurHistory.TabIndex = 516;
            // 
            // dgSaleHistory
            // 
            this.dgSaleHistory.AllowUserToAddRows = false;
            this.dgSaleHistory.AllowUserToDeleteRows = false;
            this.dgSaleHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgSaleHistory.BackgroundColor = System.Drawing.Color.Coral;
            this.dgSaleHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSaleHistory.Location = new System.Drawing.Point(8, 8);
            this.dgSaleHistory.Name = "dgSaleHistory";
            this.dgSaleHistory.Size = new System.Drawing.Size(3, 3);
            this.dgSaleHistory.TabIndex = 515;
            // 
            // pnlRateType
            // 
            this.pnlRateType.BackColor = System.Drawing.Color.Transparent;
            this.pnlRateType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRateType.Controls.Add(this.btnRateTypeCancel);
            this.pnlRateType.Controls.Add(this.btnRateTypeOK);
            this.pnlRateType.Controls.Add(this.txtRateTypePassword);
            this.pnlRateType.Controls.Add(this.label40);
            this.pnlRateType.Controls.Add(this.label44);
            this.pnlRateType.Location = new System.Drawing.Point(194, 262);
            this.pnlRateType.Name = "pnlRateType";
            this.pnlRateType.Size = new System.Drawing.Size(409, 104);
            this.pnlRateType.TabIndex = 5552;
            this.pnlRateType.Visible = false;
            // 
            // btnRateTypeCancel
            // 
            this.btnRateTypeCancel.Location = new System.Drawing.Point(209, 64);
            this.btnRateTypeCancel.Name = "btnRateTypeCancel";
            this.btnRateTypeCancel.Size = new System.Drawing.Size(75, 23);
            this.btnRateTypeCancel.TabIndex = 709;
            this.btnRateTypeCancel.Text = "Cancel";
            this.btnRateTypeCancel.UseVisualStyleBackColor = true;
            this.btnRateTypeCancel.Click += new System.EventHandler(this.btnRateTypeCancel_Click);
            // 
            // btnRateTypeOK
            // 
            this.btnRateTypeOK.Location = new System.Drawing.Point(110, 64);
            this.btnRateTypeOK.Name = "btnRateTypeOK";
            this.btnRateTypeOK.Size = new System.Drawing.Size(75, 23);
            this.btnRateTypeOK.TabIndex = 706;
            this.btnRateTypeOK.Text = "OK";
            this.btnRateTypeOK.UseVisualStyleBackColor = true;
            this.btnRateTypeOK.Click += new System.EventHandler(this.btnRateTypeOK_Click);
            // 
            // txtRateTypePassword
            // 
            this.txtRateTypePassword.Location = new System.Drawing.Point(137, 30);
            this.txtRateTypePassword.MaxLength = 6;
            this.txtRateTypePassword.Name = "txtRateTypePassword";
            this.txtRateTypePassword.PasswordChar = '*';
            this.txtRateTypePassword.Size = new System.Drawing.Size(187, 20);
            this.txtRateTypePassword.TabIndex = 703;
            this.txtRateTypePassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRateTypePassword_KeyDown);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(34, 31);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(53, 13);
            this.label40.TabIndex = 3;
            this.label40.Text = "Password";
            // 
            // label44
            // 
            this.label44.BackColor = System.Drawing.Color.Coral;
            this.label44.Dock = System.Windows.Forms.DockStyle.Top;
            this.label44.Location = new System.Drawing.Point(0, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(407, 13);
            this.label44.TabIndex = 0;
            this.label44.Text = "Rate Type Password Details";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlBarCodePrint
            // 
            this.pnlBarCodePrint.BorderRadius = 3;
            this.pnlBarCodePrint.Controls.Add(this.rbSmallMode);
            this.pnlBarCodePrint.Controls.Add(this.rbBigMod);
            this.pnlBarCodePrint.Controls.Add(this.txtStartNo);
            this.pnlBarCodePrint.Controls.Add(this.label16);
            this.pnlBarCodePrint.Controls.Add(this.btnCancelPrintBarcode);
            this.pnlBarCodePrint.Controls.Add(this.btnOKPrintBarCode);
            this.pnlBarCodePrint.Controls.Add(this.txtNoOfPrint);
            this.pnlBarCodePrint.Controls.Add(this.label21);
            this.pnlBarCodePrint.CornerRadius = 3;
            this.pnlBarCodePrint.GradientBottom = System.Drawing.Color.LightGray;
            this.pnlBarCodePrint.GradientMiddle = System.Drawing.Color.White;
            this.pnlBarCodePrint.GradientShow = true;
            this.pnlBarCodePrint.GradientTop = System.Drawing.Color.Silver;
            this.pnlBarCodePrint.Location = new System.Drawing.Point(290, 167);
            this.pnlBarCodePrint.Name = "pnlBarCodePrint";
            this.pnlBarCodePrint.Size = new System.Drawing.Size(242, 126);
            this.pnlBarCodePrint.TabIndex = 5553;
            this.pnlBarCodePrint.Visible = false;
            // 
            // rbSmallMode
            // 
            this.rbSmallMode.AutoSize = true;
            this.rbSmallMode.BackColor = System.Drawing.Color.Transparent;
            this.rbSmallMode.Location = new System.Drawing.Point(141, 71);
            this.rbSmallMode.Name = "rbSmallMode";
            this.rbSmallMode.Size = new System.Drawing.Size(80, 17);
            this.rbSmallMode.TabIndex = 10006;
            this.rbSmallMode.Text = "Small Mode";
            this.rbSmallMode.UseVisualStyleBackColor = false;
            // 
            // rbBigMod
            // 
            this.rbBigMod.AutoSize = true;
            this.rbBigMod.BackColor = System.Drawing.Color.Transparent;
            this.rbBigMod.Checked = true;
            this.rbBigMod.Location = new System.Drawing.Point(50, 72);
            this.rbBigMod.Name = "rbBigMod";
            this.rbBigMod.Size = new System.Drawing.Size(70, 17);
            this.rbBigMod.TabIndex = 10005;
            this.rbBigMod.TabStop = true;
            this.rbBigMod.Text = "Big Mode";
            this.rbBigMod.UseVisualStyleBackColor = false;
            // 
            // txtStartNo
            // 
            this.txtStartNo.Location = new System.Drawing.Point(114, 43);
            this.txtStartNo.MaxLength = 5;
            this.txtStartNo.Name = "txtStartNo";
            this.txtStartNo.Size = new System.Drawing.Size(100, 20);
            this.txtStartNo.TabIndex = 10004;
            this.txtStartNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Location = new System.Drawing.Point(7, 46);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(52, 13);
            this.label16.TabIndex = 1000701;
            this.label16.Text = "Start No :";
            // 
            // btnCancelPrintBarcode
            // 
            this.btnCancelPrintBarcode.Location = new System.Drawing.Point(115, 94);
            this.btnCancelPrintBarcode.Name = "btnCancelPrintBarcode";
            this.btnCancelPrintBarcode.Size = new System.Drawing.Size(73, 24);
            this.btnCancelPrintBarcode.TabIndex = 10008;
            this.btnCancelPrintBarcode.Text = "Cancel";
            this.btnCancelPrintBarcode.UseVisualStyleBackColor = true;
            this.btnCancelPrintBarcode.Click += new System.EventHandler(this.btnCancelPrintBarcode_Click);
            // 
            // btnOKPrintBarCode
            // 
            this.btnOKPrintBarCode.Location = new System.Drawing.Point(37, 95);
            this.btnOKPrintBarCode.Name = "btnOKPrintBarCode";
            this.btnOKPrintBarCode.Size = new System.Drawing.Size(62, 24);
            this.btnOKPrintBarCode.TabIndex = 10007;
            this.btnOKPrintBarCode.Text = "OK";
            this.btnOKPrintBarCode.UseVisualStyleBackColor = true;
            this.btnOKPrintBarCode.Click += new System.EventHandler(this.btnOKPrintBarCode_Click);
            // 
            // txtNoOfPrint
            // 
            this.txtNoOfPrint.Location = new System.Drawing.Point(115, 16);
            this.txtNoOfPrint.MaxLength = 5;
            this.txtNoOfPrint.Name = "txtNoOfPrint";
            this.txtNoOfPrint.Size = new System.Drawing.Size(100, 20);
            this.txtNoOfPrint.TabIndex = 10003;
            this.txtNoOfPrint.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Location = new System.Drawing.Point(8, 19);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(65, 13);
            this.label21.TabIndex = 1000201;
            this.label21.Text = "No Of Print :";
            // 
            // label19
            // 
            this.label19.BackColor = System.Drawing.Color.Coral;
            this.label19.Dock = System.Windows.Forms.DockStyle.Top;
            this.label19.Location = new System.Drawing.Point(0, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(352, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Company Name Selection";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dgCompany
            // 
            this.dgCompany.AllowUserToAddRows = false;
            this.dgCompany.AllowUserToDeleteRows = false;
            this.dgCompany.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgCompany.BackgroundColor = System.Drawing.Color.Coral;
            this.dgCompany.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgCompany.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CompanyNo,
            this.SRCompanyName});
            this.dgCompany.Location = new System.Drawing.Point(6, 17);
            this.dgCompany.Name = "dgCompany";
            this.dgCompany.Size = new System.Drawing.Size(341, 83);
            this.dgCompany.TabIndex = 516;
            this.dgCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgCompany_KeyDown);
            // 
            // CompanyNo
            // 
            this.CompanyNo.DataPropertyName = "CompanyNo";
            this.CompanyNo.HeaderText = "CompanyNo";
            this.CompanyNo.Name = "CompanyNo";
            this.CompanyNo.Visible = false;
            // 
            // SRCompanyName
            // 
            this.SRCompanyName.DataPropertyName = "CompanyName";
            this.SRCompanyName.HeaderText = "Name";
            this.SRCompanyName.Name = "SRCompanyName";
            this.SRCompanyName.Width = 295;
            // 
            // pnlCompany
            // 
            this.pnlCompany.BackColor = System.Drawing.Color.Transparent;
            this.pnlCompany.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlCompany.Controls.Add(this.dgCompany);
            this.pnlCompany.Controls.Add(this.label19);
            this.pnlCompany.Location = new System.Drawing.Point(157, 210);
            this.pnlCompany.Name = "pnlCompany";
            this.pnlCompany.Size = new System.Drawing.Size(354, 104);
            this.pnlCompany.TabIndex = 5553;
            this.pnlCompany.Visible = false;
            // 
            // btnAllBarCodePrint
            // 
            this.btnAllBarCodePrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAllBarCodePrint.Location = new System.Drawing.Point(622, 321);
            this.btnAllBarCodePrint.Name = "btnAllBarCodePrint";
            this.btnAllBarCodePrint.Size = new System.Drawing.Size(69, 60);
            this.btnAllBarCodePrint.TabIndex = 25;
            this.btnAllBarCodePrint.Text = ".";
            this.btnAllBarCodePrint.UseVisualStyleBackColor = true;
            // 
            // pnlAllPrintBarCode
            // 
            this.pnlAllPrintBarCode.BorderRadius = 3;
            this.pnlAllPrintBarCode.Controls.Add(this.dgPrintBarCode);
            this.pnlAllPrintBarCode.Controls.Add(this.rdAllSmallMode);
            this.pnlAllPrintBarCode.Controls.Add(this.rdAllBigMode);
            this.pnlAllPrintBarCode.Controls.Add(this.txtAllStartNo);
            this.pnlAllPrintBarCode.Controls.Add(this.label20);
            this.pnlAllPrintBarCode.Controls.Add(this.btnAllBarcodeCancel);
            this.pnlAllPrintBarCode.Controls.Add(this.btnAllBarcodeOK);
            this.pnlAllPrintBarCode.CornerRadius = 3;
            this.pnlAllPrintBarCode.GradientBottom = System.Drawing.Color.LightGray;
            this.pnlAllPrintBarCode.GradientMiddle = System.Drawing.Color.White;
            this.pnlAllPrintBarCode.GradientShow = true;
            this.pnlAllPrintBarCode.GradientTop = System.Drawing.Color.Silver;
            this.pnlAllPrintBarCode.Location = new System.Drawing.Point(110, 98);
            this.pnlAllPrintBarCode.Name = "pnlAllPrintBarCode";
            this.pnlAllPrintBarCode.Size = new System.Drawing.Size(48, 38);
            this.pnlAllPrintBarCode.TabIndex = 5556;
            this.pnlAllPrintBarCode.Visible = false;
            // 
            // dgPrintBarCode
            // 
            this.dgPrintBarCode.AllowUserToAddRows = false;
            this.dgPrintBarCode.AllowUserToDeleteRows = false;
            this.dgPrintBarCode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgPrintBarCode.BackgroundColor = System.Drawing.Color.Coral;
            this.dgPrintBarCode.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPrintBarCode.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn6,
            this.BarCodeQty,
            this.chkPrint,
            this.FKRateSettingNo});
            this.dgPrintBarCode.Location = new System.Drawing.Point(13, 9);
            this.dgPrintBarCode.Name = "dgPrintBarCode";
            this.dgPrintBarCode.Size = new System.Drawing.Size(330, 267);
            this.dgPrintBarCode.TabIndex = 1000702;
            this.dgPrintBarCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgPrintBarCode_KeyDown);
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "ItemNo";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ToolTipText = "ItemNo";
            this.dataGridViewTextBoxColumn3.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Item Description";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.ToolTipText = "Product Name";
            this.dataGridViewTextBoxColumn6.Width = 200;
            // 
            // BarCodeQty
            // 
            this.BarCodeQty.HeaderText = "Quantity";
            this.BarCodeQty.Name = "BarCodeQty";
            this.BarCodeQty.Width = 50;
            // 
            // chkPrint
            // 
            this.chkPrint.HeaderText = "";
            this.chkPrint.Name = "chkPrint";
            this.chkPrint.Width = 50;
            // 
            // FKRateSettingNo
            // 
            this.FKRateSettingNo.HeaderText = "FKRateSettingNo";
            this.FKRateSettingNo.Name = "FKRateSettingNo";
            this.FKRateSettingNo.Visible = false;
            // 
            // rdAllSmallMode
            // 
            this.rdAllSmallMode.AutoSize = true;
            this.rdAllSmallMode.BackColor = System.Drawing.Color.Transparent;
            this.rdAllSmallMode.Location = new System.Drawing.Point(443, 88);
            this.rdAllSmallMode.Name = "rdAllSmallMode";
            this.rdAllSmallMode.Size = new System.Drawing.Size(80, 17);
            this.rdAllSmallMode.TabIndex = 10008;
            this.rdAllSmallMode.Text = "Small Mode";
            this.rdAllSmallMode.UseVisualStyleBackColor = false;
            // 
            // rdAllBigMode
            // 
            this.rdAllBigMode.AutoSize = true;
            this.rdAllBigMode.BackColor = System.Drawing.Color.Transparent;
            this.rdAllBigMode.Checked = true;
            this.rdAllBigMode.Location = new System.Drawing.Point(352, 89);
            this.rdAllBigMode.Name = "rdAllBigMode";
            this.rdAllBigMode.Size = new System.Drawing.Size(70, 17);
            this.rdAllBigMode.TabIndex = 10007;
            this.rdAllBigMode.TabStop = true;
            this.rdAllBigMode.Text = "Big Mode";
            this.rdAllBigMode.UseVisualStyleBackColor = false;
            // 
            // txtAllStartNo
            // 
            this.txtAllStartNo.Location = new System.Drawing.Point(411, 9);
            this.txtAllStartNo.MaxLength = 5;
            this.txtAllStartNo.Name = "txtAllStartNo";
            this.txtAllStartNo.Size = new System.Drawing.Size(100, 20);
            this.txtAllStartNo.TabIndex = 10004;
            this.txtAllStartNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Location = new System.Drawing.Point(353, 12);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(52, 13);
            this.label20.TabIndex = 1000701;
            this.label20.Text = "Start No :";
            // 
            // btnAllBarcodeCancel
            // 
            this.btnAllBarcodeCancel.Location = new System.Drawing.Point(438, 53);
            this.btnAllBarcodeCancel.Name = "btnAllBarcodeCancel";
            this.btnAllBarcodeCancel.Size = new System.Drawing.Size(73, 24);
            this.btnAllBarcodeCancel.TabIndex = 10006;
            this.btnAllBarcodeCancel.Text = "Cancel";
            this.btnAllBarcodeCancel.UseVisualStyleBackColor = true;
            this.btnAllBarcodeCancel.Click += new System.EventHandler(this.btnAllBarcodeCancel_Click);
            // 
            // btnAllBarcodeOK
            // 
            this.btnAllBarcodeOK.Location = new System.Drawing.Point(360, 54);
            this.btnAllBarcodeOK.Name = "btnAllBarcodeOK";
            this.btnAllBarcodeOK.Size = new System.Drawing.Size(62, 24);
            this.btnAllBarcodeOK.TabIndex = 10005;
            this.btnAllBarcodeOK.Text = "OK";
            this.btnAllBarcodeOK.UseVisualStyleBackColor = true;
            this.btnAllBarcodeOK.Click += new System.EventHandler(this.btnAllBarcodeOK_Click);
            // 
            // txtOtherDisc
            // 
            this.txtOtherDisc.Location = new System.Drawing.Point(861, 12);
            this.txtOtherDisc.MaxLength = 5;
            this.txtOtherDisc.Name = "txtOtherDisc";
            this.txtOtherDisc.Size = new System.Drawing.Size(77, 20);
            this.txtOtherDisc.TabIndex = 3;
            this.txtOtherDisc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOtherDisc.Visible = false;
            this.txtOtherDisc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtOtherDisc_KeyDown);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.Location = new System.Drawing.Point(789, 16);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(54, 13);
            this.label39.TabIndex = 5558;
            this.label39.Text = "Disc %    :";
            this.label39.Visible = false;
            // 
            // pnlPartySearch
            // 
            this.pnlPartySearch.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.pnlPartySearch.BorderColor = System.Drawing.Color.Empty;
            this.pnlPartySearch.Controls.Add(this.dgPartySearch);
            this.pnlPartySearch.DropShadow = false;
            this.pnlPartySearch.Glossy = false;
            this.pnlPartySearch.LightingColor = System.Drawing.Color.FromArgb(((int)(((byte)(176)))), ((int)(((byte)(186)))), ((int)(((byte)(196)))));
            this.pnlPartySearch.Location = new System.Drawing.Point(131, 119);
            this.pnlPartySearch.Name = "pnlPartySearch";
            this.pnlPartySearch.Radius = 7;
            this.pnlPartySearch.Size = new System.Drawing.Size(475, 398);
            this.pnlPartySearch.TabIndex = 5001;
            this.pnlPartySearch.Visible = false;
            // 
            // dgPartySearch
            // 
            this.dgPartySearch.AllowUserToAddRows = false;
            this.dgPartySearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPartySearch.Location = new System.Drawing.Point(5, 4);
            this.dgPartySearch.Name = "dgPartySearch";
            this.dgPartySearch.ReadOnly = true;
            this.dgPartySearch.Size = new System.Drawing.Size(465, 456);
            this.dgPartySearch.TabIndex = 5002;
            this.dgPartySearch.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgPartySearch_CellFormatting);
            this.dgPartySearch.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPartySearch_CellClick);
            this.dgPartySearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgPartySearch_KeyDown);
            // 
            // btnAdvanceSearch
            // 
            this.btnAdvanceSearch.Location = new System.Drawing.Point(421, 37);
            this.btnAdvanceSearch.Name = "btnAdvanceSearch";
            this.btnAdvanceSearch.Size = new System.Drawing.Size(21, 21);
            this.btnAdvanceSearch.TabIndex = 5559;
            this.btnAdvanceSearch.Text = "..";
            this.btnAdvanceSearch.UseVisualStyleBackColor = true;
            this.btnAdvanceSearch.Click += new System.EventHandler(this.btnAdvanceSearch_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(696, 131);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(69, 60);
            this.button3.TabIndex = 5567;
            this.button3.Text = ".";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(696, 67);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(69, 60);
            this.button2.TabIndex = 5566;
            this.button2.Text = ".";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(696, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(69, 60);
            this.button1.TabIndex = 5565;
            this.button1.Text = ".";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // btnShortcut
            // 
            this.btnShortcut.Location = new System.Drawing.Point(696, 257);
            this.btnShortcut.Name = "btnShortcut";
            this.btnShortcut.Size = new System.Drawing.Size(69, 60);
            this.btnShortcut.TabIndex = 5569;
            this.btnShortcut.Text = "( F1 )   Help";
            this.btnShortcut.UseVisualStyleBackColor = true;
            this.btnShortcut.Click += new System.EventHandler(this.btnShortcut_Click);
            // 
            // btnNewCustomer
            // 
            this.btnNewCustomer.Location = new System.Drawing.Point(696, 194);
            this.btnNewCustomer.Name = "btnNewCustomer";
            this.btnNewCustomer.Size = new System.Drawing.Size(69, 60);
            this.btnNewCustomer.TabIndex = 5568;
            this.btnNewCustomer.Text = ".";
            this.btnNewCustomer.UseVisualStyleBackColor = true;
            // 
            // ombPanel1
            // 
            this.ombPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ombPanel1.BackColor = System.Drawing.Color.Olive;
            this.ombPanel1.BorderColor = System.Drawing.Color.Gray;
            this.ombPanel1.BorderRadius = 3;
            this.ombPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ombPanel1.Controls.Add(this.lblLastPayment);
            this.ombPanel1.Controls.Add(this.label11);
            this.ombPanel1.Controls.Add(this.label9);
            this.ombPanel1.Controls.Add(this.label10);
            this.ombPanel1.Controls.Add(this.lblLastBillAmt);
            this.ombPanel1.Controls.Add(this.lblLastBillQty);
            this.ombPanel1.Location = new System.Drawing.Point(612, 528);
            this.ombPanel1.Name = "ombPanel1";
            this.ombPanel1.Size = new System.Drawing.Size(140, 107);
            this.ombPanel1.TabIndex = 5571;
            // 
            // lblLastPayment
            // 
            this.lblLastPayment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLastPayment.AutoSize = true;
            this.lblLastPayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastPayment.ForeColor = System.Drawing.Color.White;
            this.lblLastPayment.Location = new System.Drawing.Point(6, 63);
            this.lblLastPayment.Name = "lblLastPayment";
            this.lblLastPayment.Size = new System.Drawing.Size(55, 13);
            this.lblLastPayment.TabIndex = 5574;
            this.lblLastPayment.Text = "Payment";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(5, 3);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 13);
            this.label11.TabIndex = 530;
            this.label11.Text = "Last Bill";
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Coral;
            this.label9.Location = new System.Drawing.Point(465, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 13);
            this.label9.TabIndex = 506;
            this.label9.Visible = false;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Coral;
            this.label10.Location = new System.Drawing.Point(409, 54);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(50, 13);
            this.label10.TabIndex = 505;
            this.label10.Visible = false;
            // 
            // lblLastBillAmt
            // 
            this.lblLastBillAmt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLastBillAmt.AutoSize = true;
            this.lblLastBillAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastBillAmt.ForeColor = System.Drawing.Color.White;
            this.lblLastBillAmt.Location = new System.Drawing.Point(5, 22);
            this.lblLastBillAmt.Name = "lblLastBillAmt";
            this.lblLastBillAmt.Size = new System.Drawing.Size(98, 13);
            this.lblLastBillAmt.TabIndex = 523;
            this.lblLastBillAmt.Text = "Last Bill Amount";
            // 
            // lblLastBillQty
            // 
            this.lblLastBillQty.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLastBillQty.AutoSize = true;
            this.lblLastBillQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastBillQty.ForeColor = System.Drawing.Color.White;
            this.lblLastBillQty.Location = new System.Drawing.Point(5, 41);
            this.lblLastBillQty.Name = "lblLastBillQty";
            this.lblLastBillQty.Size = new System.Drawing.Size(26, 13);
            this.lblLastBillQty.TabIndex = 528;
            this.lblLastBillQty.Text = "Qty";
            // 
            // lblGrandTotal
            // 
            this.lblGrandTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblGrandTotal.BackColor = System.Drawing.Color.Olive;
            this.lblGrandTotal.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrandTotal.ForeColor = System.Drawing.Color.White;
            this.lblGrandTotal.Location = new System.Drawing.Point(415, 528);
            this.lblGrandTotal.Name = "lblGrandTotal";
            this.lblGrandTotal.Size = new System.Drawing.Size(192, 78);
            this.lblGrandTotal.TabIndex = 5572;
            this.lblGrandTotal.Text = "0.00";
            this.lblGrandTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnShowDetails
            // 
            this.btnShowDetails.Location = new System.Drawing.Point(696, 320);
            this.btnShowDetails.Name = "btnShowDetails";
            this.btnShowDetails.Size = new System.Drawing.Size(69, 60);
            this.btnShowDetails.TabIndex = 5573;
            this.btnShowDetails.Text = "Show\r\nDetail";
            this.btnShowDetails.UseVisualStyleBackColor = true;
            this.btnShowDetails.Click += new System.EventHandler(this.btnShowDetails_Click);
            // 
            // btnShowBill
            // 
            this.btnShowBill.Location = new System.Drawing.Point(422, 6);
            this.btnShowBill.Name = "btnShowBill";
            this.btnShowBill.Size = new System.Drawing.Size(21, 21);
            this.btnShowBill.TabIndex = 5574;
            this.btnShowBill.Text = "..";
            this.btnShowBill.UseVisualStyleBackColor = true;
            this.btnShowBill.Click += new System.EventHandler(this.btnShowBill_Click);
            this.btnShowBill.MouseHover += new System.EventHandler(this.btnShowBill_MouseHover);
            // 
            // pnlInvSearch
            // 
            this.pnlInvSearch.BorderColor = System.Drawing.Color.Empty;
            this.pnlInvSearch.BorderRadius = 3;
            this.pnlInvSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlInvSearch.Controls.Add(this.dgInvSearch);
            this.pnlInvSearch.Controls.Add(this.label52);
            this.pnlInvSearch.Location = new System.Drawing.Point(320, 149);
            this.pnlInvSearch.Name = "pnlInvSearch";
            this.pnlInvSearch.Size = new System.Drawing.Size(389, 342);
            this.pnlInvSearch.TabIndex = 123462;
            this.pnlInvSearch.Visible = false;
            this.pnlInvSearch.VisibleChanged += new System.EventHandler(this.pnlInvSearch_VisibleChanged);
            // 
            // dgInvSearch
            // 
            this.dgInvSearch.AllowUserToAddRows = false;
            this.dgInvSearch.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgInvSearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgInvSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgInvSearch.Location = new System.Drawing.Point(0, 23);
            this.dgInvSearch.Name = "dgInvSearch";
            this.dgInvSearch.ReadOnly = true;
            this.dgInvSearch.Size = new System.Drawing.Size(387, 317);
            this.dgInvSearch.TabIndex = 123455;
            this.dgInvSearch.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgInvSearch_CellFormatting);
            this.dgInvSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgInvSearch_KeyDown);
            // 
            // label52
            // 
            this.label52.BackColor = System.Drawing.Color.SteelBlue;
            this.label52.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label52.Dock = System.Windows.Forms.DockStyle.Top;
            this.label52.ForeColor = System.Drawing.Color.White;
            this.label52.Location = new System.Drawing.Point(0, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(387, 23);
            this.label52.TabIndex = 123456;
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Sr";
            this.Column1.HeaderText = "Sr";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 30;
            // 
            // ItemName
            // 
            this.ItemName.DataPropertyName = "ItemName";
            this.ItemName.HeaderText = "Description";
            this.ItemName.Name = "ItemName";
            this.ItemName.Width = 235;
            // 
            // Quantity
            // 
            this.Quantity.DataPropertyName = "Quantity";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Quantity.DefaultCellStyle = dataGridViewCellStyle1;
            this.Quantity.HeaderText = "Qty";
            this.Quantity.Name = "Quantity";
            this.Quantity.Width = 75;
            // 
            // UOM
            // 
            this.UOM.DataPropertyName = "UOMName";
            this.UOM.HeaderText = "UOM";
            this.UOM.Name = "UOM";
            this.UOM.ReadOnly = true;
            this.UOM.Width = 40;
            // 
            // Rate
            // 
            this.Rate.DataPropertyName = "Rate";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Rate.DefaultCellStyle = dataGridViewCellStyle2;
            this.Rate.HeaderText = "Rate";
            this.Rate.Name = "Rate";
            this.Rate.Width = 60;
            // 
            // NetRate
            // 
            this.NetRate.DataPropertyName = "NetRate";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.NetRate.DefaultCellStyle = dataGridViewCellStyle3;
            this.NetRate.HeaderText = "NetRate";
            this.NetRate.Name = "NetRate";
            this.NetRate.ReadOnly = true;
            this.NetRate.Visible = false;
            this.NetRate.Width = 65;
            // 
            // DiscPerce
            // 
            this.DiscPerce.DataPropertyName = "DiscPercentage";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DiscPerce.DefaultCellStyle = dataGridViewCellStyle4;
            this.DiscPerce.HeaderText = "Disc%";
            this.DiscPerce.Name = "DiscPerce";
            this.DiscPerce.Visible = false;
            this.DiscPerce.Width = 45;
            // 
            // DiscAmt
            // 
            this.DiscAmt.DataPropertyName = "DiscAmount";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DiscAmt.DefaultCellStyle = dataGridViewCellStyle5;
            this.DiscAmt.HeaderText = "DiscAmt";
            this.DiscAmt.Name = "DiscAmt";
            this.DiscAmt.ReadOnly = true;
            this.DiscAmt.Visible = false;
            this.DiscAmt.Width = 65;
            // 
            // DiscRupees
            // 
            this.DiscRupees.DataPropertyName = "DiscRupees";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DiscRupees.DefaultCellStyle = dataGridViewCellStyle6;
            this.DiscRupees.HeaderText = "DiscRs.";
            this.DiscRupees.Name = "DiscRupees";
            this.DiscRupees.Width = 55;
            // 
            // DiscPercentage2
            // 
            this.DiscPercentage2.DataPropertyName = "DiscPercentage2";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DiscPercentage2.DefaultCellStyle = dataGridViewCellStyle7;
            this.DiscPercentage2.HeaderText = "Disc% 2";
            this.DiscPercentage2.Name = "DiscPercentage2";
            this.DiscPercentage2.Visible = false;
            this.DiscPercentage2.Width = 45;
            // 
            // DiscAmount2
            // 
            this.DiscAmount2.DataPropertyName = "DiscAmount2";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DiscAmount2.DefaultCellStyle = dataGridViewCellStyle8;
            this.DiscAmount2.HeaderText = "DiscAmt2";
            this.DiscAmount2.Name = "DiscAmount2";
            this.DiscAmount2.ReadOnly = true;
            this.DiscAmount2.Visible = false;
            this.DiscAmount2.Width = 65;
            // 
            // NetAmt
            // 
            this.NetAmt.DataPropertyName = "NetAmt";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.NetAmt.DefaultCellStyle = dataGridViewCellStyle9;
            this.NetAmt.HeaderText = "NetAmt";
            this.NetAmt.Name = "NetAmt";
            this.NetAmt.ReadOnly = true;
            this.NetAmt.Visible = false;
            this.NetAmt.Width = 70;
            // 
            // TaxPerce
            // 
            this.TaxPerce.DataPropertyName = "TaxPercentage";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.TaxPerce.DefaultCellStyle = dataGridViewCellStyle10;
            this.TaxPerce.HeaderText = "Tax%";
            this.TaxPerce.Name = "TaxPerce";
            this.TaxPerce.Visible = false;
            this.TaxPerce.Width = 45;
            // 
            // TaxAmount
            // 
            this.TaxAmount.DataPropertyName = "TaxAmount";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.TaxAmount.DefaultCellStyle = dataGridViewCellStyle11;
            this.TaxAmount.HeaderText = "TaxAmt";
            this.TaxAmount.Name = "TaxAmount";
            this.TaxAmount.Visible = false;
            this.TaxAmount.Width = 60;
            // 
            // DiscRupees2
            // 
            this.DiscRupees2.DataPropertyName = "DiscRupees2";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DiscRupees2.DefaultCellStyle = dataGridViewCellStyle12;
            this.DiscRupees2.HeaderText = "DiscRs2";
            this.DiscRupees2.Name = "DiscRupees2";
            this.DiscRupees2.Visible = false;
            this.DiscRupees2.Width = 65;
            // 
            // Amount
            // 
            this.Amount.DataPropertyName = "Amount";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Amount.DefaultCellStyle = dataGridViewCellStyle13;
            this.Amount.HeaderText = "Amount";
            this.Amount.Name = "Amount";
            this.Amount.ReadOnly = true;
            this.Amount.Width = 75;
            // 
            // Barcode
            // 
            this.Barcode.DataPropertyName = "Barcode";
            this.Barcode.HeaderText = "Barcode";
            this.Barcode.Name = "Barcode";
            this.Barcode.Visible = false;
            // 
            // PkStockTrnNo
            // 
            this.PkStockTrnNo.DataPropertyName = "PkStockTrnNo";
            this.PkStockTrnNo.HeaderText = "PkStockTrnNo";
            this.PkStockTrnNo.Name = "PkStockTrnNo";
            this.PkStockTrnNo.Visible = false;
            // 
            // PkSrNo
            // 
            this.PkSrNo.DataPropertyName = "PkStockBarcodeNo";
            this.PkSrNo.HeaderText = "PkBarcodeNo";
            this.PkSrNo.Name = "PkSrNo";
            this.PkSrNo.Visible = false;
            // 
            // PkVoucherNo
            // 
            this.PkVoucherNo.DataPropertyName = "PkVoucherNo";
            this.PkVoucherNo.HeaderText = "PkVoucherNo";
            this.PkVoucherNo.Name = "PkVoucherNo";
            this.PkVoucherNo.Visible = false;
            // 
            // ItemNo
            // 
            this.ItemNo.DataPropertyName = "ItemNo";
            this.ItemNo.HeaderText = "ItemNo";
            this.ItemNo.Name = "ItemNo";
            this.ItemNo.Visible = false;
            // 
            // UOMNo
            // 
            this.UOMNo.DataPropertyName = "UOMNo";
            this.UOMNo.HeaderText = "UOMNo";
            this.UOMNo.Name = "UOMNo";
            this.UOMNo.Visible = false;
            // 
            // TaxLedgerNo
            // 
            this.TaxLedgerNo.HeaderText = "TaxLedgerNo";
            this.TaxLedgerNo.Name = "TaxLedgerNo";
            this.TaxLedgerNo.Visible = false;
            // 
            // SalesLedgerNo
            // 
            this.SalesLedgerNo.HeaderText = "SalesLedgerNo";
            this.SalesLedgerNo.Name = "SalesLedgerNo";
            this.SalesLedgerNo.Visible = false;
            // 
            // PkRateSettingNo
            // 
            this.PkRateSettingNo.HeaderText = "RateSettingNo";
            this.PkRateSettingNo.Name = "PkRateSettingNo";
            this.PkRateSettingNo.Visible = false;
            // 
            // PkItemTaxInfo
            // 
            this.PkItemTaxInfo.HeaderText = "ItemTaxInfoNo";
            this.PkItemTaxInfo.Name = "PkItemTaxInfo";
            this.PkItemTaxInfo.Visible = false;
            // 
            // StockFactor
            // 
            this.StockFactor.DataPropertyName = "StockConversion";
            this.StockFactor.HeaderText = "StockFactor";
            this.StockFactor.Name = "StockFactor";
            this.StockFactor.Visible = false;
            // 
            // ActualQty
            // 
            this.ActualQty.HeaderText = "ActualQty";
            this.ActualQty.Name = "ActualQty";
            this.ActualQty.Visible = false;
            // 
            // MKTQuantity
            // 
            this.MKTQuantity.DataPropertyName = "MKTQty";
            this.MKTQuantity.HeaderText = "MKTQuantity";
            this.MKTQuantity.Name = "MKTQuantity";
            this.MKTQuantity.Visible = false;
            // 
            // SalesVchNo
            // 
            this.SalesVchNo.HeaderText = "SalesVchNo";
            this.SalesVchNo.Name = "SalesVchNo";
            this.SalesVchNo.Visible = false;
            // 
            // TaxVchNo
            // 
            this.TaxVchNo.HeaderText = "TaxVchNo";
            this.TaxVchNo.Name = "TaxVchNo";
            this.TaxVchNo.Visible = false;
            // 
            // StockCompanyNo
            // 
            this.StockCompanyNo.HeaderText = "StockCompanyNo";
            this.StockCompanyNo.Name = "StockCompanyNo";
            this.StockCompanyNo.Visible = false;
            // 
            // BarCodePrinting
            // 
            this.BarCodePrinting.HeaderText = "Print";
            this.BarCodePrinting.Name = "BarCodePrinting";
            this.BarCodePrinting.Visible = false;
            this.BarCodePrinting.Width = 60;
            // 
            // HSNCode
            // 
            this.HSNCode.HeaderText = "HSNCode";
            this.HSNCode.Name = "HSNCode";
            this.HSNCode.Visible = false;
            // 
            // IGSTPercent
            // 
            this.IGSTPercent.HeaderText = "IGSTPercent";
            this.IGSTPercent.Name = "IGSTPercent";
            this.IGSTPercent.Visible = false;
            // 
            // IGSTAmount
            // 
            this.IGSTAmount.HeaderText = "IGSTAmount";
            this.IGSTAmount.Name = "IGSTAmount";
            this.IGSTAmount.Visible = false;
            // 
            // CGSTPercent
            // 
            this.CGSTPercent.HeaderText = "CGSTPercent";
            this.CGSTPercent.Name = "CGSTPercent";
            this.CGSTPercent.Visible = false;
            // 
            // CGSTAmount
            // 
            this.CGSTAmount.HeaderText = "CGSTAmount";
            this.CGSTAmount.Name = "CGSTAmount";
            this.CGSTAmount.Visible = false;
            // 
            // SGSTPercent
            // 
            this.SGSTPercent.HeaderText = "SGSTPercent";
            this.SGSTPercent.Name = "SGSTPercent";
            this.SGSTPercent.Visible = false;
            // 
            // SGSTAmount
            // 
            this.SGSTAmount.HeaderText = "SGSTAmount";
            this.SGSTAmount.Name = "SGSTAmount";
            this.SGSTAmount.Visible = false;
            // 
            // UTGSTPercent
            // 
            this.UTGSTPercent.HeaderText = "UTGSTPercent";
            this.UTGSTPercent.Name = "UTGSTPercent";
            this.UTGSTPercent.Visible = false;
            // 
            // UTGSTAmount
            // 
            this.UTGSTAmount.HeaderText = "UTGSTAmount";
            this.UTGSTAmount.Name = "UTGSTAmount";
            this.UTGSTAmount.Visible = false;
            // 
            // CessPercent
            // 
            this.CessPercent.HeaderText = "CessPercent";
            this.CessPercent.Name = "CessPercent";
            this.CessPercent.Visible = false;
            // 
            // CessAmount
            // 
            this.CessAmount.HeaderText = "CessAmount";
            this.CessAmount.Name = "CessAmount";
            this.CessAmount.Visible = false;
            // 
            // MRP
            // 
            this.MRP.HeaderText = "MRP";
            this.MRP.Name = "MRP";
            this.MRP.Visible = false;
            // 
            // Godown
            // 
            this.Godown.HeaderText = "GodownNo";
            this.Godown.Name = "Godown";
            // 
            // SalesReturnAE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 641);
            this.Controls.Add(this.pnlInvSearch);
            this.Controls.Add(this.btnShowBill);
            this.Controls.Add(this.pnlItemName);
            this.Controls.Add(this.btnShowDetails);
            this.Controls.Add(this.lblGrandTotal);
            this.Controls.Add(this.ombPanel1);
            this.Controls.Add(this.btnShortcut);
            this.Controls.Add(this.btnNewCustomer);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.pnlRate);
            this.Controls.Add(this.btnAdvanceSearch);
            this.Controls.Add(this.pnlPartySearch);
            this.Controls.Add(this.pnlSearch);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.txtRefNo);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.txtOtherDisc);
            this.Controls.Add(this.pnlAllPrintBarCode);
            this.Controls.Add(this.btnAllBarCodePrint);
            this.Controls.Add(this.pnlCompany);
            this.Controls.Add(this.pnlBarCodePrint);
            this.Controls.Add(this.pnlRateType);
            this.Controls.Add(this.pnlPartial);
            this.Controls.Add(this.pnlSalePurHistory);
            this.Controls.Add(this.pnlUOM);
            this.Controls.Add(this.pnlGroup2);
            this.Controls.Add(this.pnlGroup1);
            this.Controls.Add(this.lblRateType);
            this.Controls.Add(this.cmbRateType);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cmbTaxType);
            this.Controls.Add(this.cmbPartyName);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.pnlParking);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.pnlFooterInfo);
            this.Controls.Add(this.btndelete);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnPrev);
            this.Controls.Add(this.btnLast);
            this.Controls.Add(this.btnFirst);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlTotalAmt);
            this.Controls.Add(this.dgBill);
            this.Controls.Add(this.dtpBillTime);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtpBillDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtInvNo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnUpdate);
            this.Name = "SalesReturnAE";
            this.Text = "Sales Return";
            this.Deactivate += new System.EventHandler(this.SalesReturnAE_Deactivate);
            this.Load += new System.EventHandler(this.PurchaseAE_Load);
            this.Activated += new System.EventHandler(this.SalesReturnAE_Activated);
            this.pnlTotalAmt.ResumeLayout(false);
            this.pnlTotalAmt.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlFooterInfo.ResumeLayout(false);
            this.pnlFooterInfo.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgBill)).EndInit();
            this.pnlPartial.ResumeLayout(false);
            this.pnlPartial.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPayCreditCardDetails)).EndInit();
            this.pnlBranch.ResumeLayout(false);
            this.pnlBank.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgPayChqDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgPayType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgParkingBills)).EndInit();
            this.pnlParking.ResumeLayout(false);
            this.pnlSearch.ResumeLayout(false);
            this.pnlSearch.PerformLayout();
            this.pnlItemName.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgItemList)).EndInit();
            this.pnlGroup1.ResumeLayout(false);
            this.pnlGroup2.ResumeLayout(false);
            this.pnlUOM.ResumeLayout(false);
            this.pnlRate.ResumeLayout(false);
            this.pnlSalePurHistory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgPurHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgSaleHistory)).EndInit();
            this.pnlRateType.ResumeLayout(false);
            this.pnlRateType.PerformLayout();
            this.pnlBarCodePrint.ResumeLayout(false);
            this.pnlBarCodePrint.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCompany)).EndInit();
            this.pnlCompany.ResumeLayout(false);
            this.pnlAllPrintBarCode.ResumeLayout(false);
            this.pnlAllPrintBarCode.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPrintBarCode)).EndInit();
            this.pnlPartySearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgPartySearch)).EndInit();
            this.ombPanel1.ResumeLayout(false);
            this.ombPanel1.PerformLayout();
            this.pnlInvSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgInvSearch)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtInvNo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpBillDate;
        private System.Windows.Forms.DateTimePicker dtpBillTime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel pnlTotalAmt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtGrandTotal;
        private System.Windows.Forms.TextBox txtTotalTax;
        private System.Windows.Forms.TextBox txtTotalDisc;
        private System.Windows.Forms.TextBox txtSubTotal;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblBillItem;
        private System.Windows.Forms.Label lblBilExchangeItem;
        private System.Windows.Forms.TextBox txtDiscount1;
        private System.Windows.Forms.Label lblChrg1;
        private System.Windows.Forms.Label lblDisc1;
        private System.Windows.Forms.ComboBox cmbPaymentType;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btndelete;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.Button btnLast;
        private System.Windows.Forms.Button btnFirst;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Panel pnlFooterInfo;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtLastBillAmt;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label lblExchange;
        private System.Windows.Forms.DataGridView dgBill;
        private System.Windows.Forms.Button btnSearch;
        private JitControls.OMLabel lblMsg;
        private System.Windows.Forms.Panel pnlPartial;
        private System.Windows.Forms.TextBox txtTotalAmt;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.DataGridView dgParkingBills;
        private System.Windows.Forms.DataGridViewTextBoxColumn BillNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn BillAmount;
        private System.Windows.Forms.Panel pnlParking;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnCancel;
        private JitControls.OMBPanel pnlSearch;
        private System.Windows.Forms.Button btnCancelSearch;
        private System.Windows.Forms.Label lblLable;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.TextBox txtRemark;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Panel pnlItemName;
        private System.Windows.Forms.DataGridView dgItemList;
        private System.Windows.Forms.ListBox lstUOM;
        private System.Windows.Forms.ListBox lstRate;
        private System.Windows.Forms.ListBox lstGroup1;
        private System.Windows.Forms.ListBox lstGroup2;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.ComboBox cmbPartyName;
        private System.Windows.Forms.TextBox txtDiscRupees1;
        private System.Windows.Forms.TextBox txtRoundOff;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtTotalChrgs;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtChrgRupees1;
        private System.Windows.Forms.TextBox txtTotalAnotherDisc;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtlastBillQty;
        private System.Windows.Forms.TextBox txtLastPayment;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.DataGridView dgPayType;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn PKVoucherPayTypeNo;
        private System.Windows.Forms.Label lblPayTypeBal;
        private System.Windows.Forms.ComboBox cmbTaxType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblRateType;
        private System.Windows.Forms.ComboBox cmbRateType;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Panel pnlGroup1;
        private System.Windows.Forms.Panel pnlGroup2;
        private System.Windows.Forms.Panel pnlUOM;
        private System.Windows.Forms.Panel pnlRate;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel pnlSalePurHistory;
        private System.Windows.Forms.DataGridView dgSaleHistory;
        private System.Windows.Forms.DataGridView dgPurHistory;
        private System.Windows.Forms.DataGridView dgPayChqDetails;
        private System.Windows.Forms.Panel pnlBank;
        private System.Windows.Forms.ListBox lstBank;
        private System.Windows.Forms.Panel pnlBranch;
        private System.Windows.Forms.ListBox lstBranch;
        private System.Windows.Forms.MonthCalendar dtpChqDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ChequeNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ChequeDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn BankName;
        private System.Windows.Forms.DataGridViewTextBoxColumn BranchName;
        private System.Windows.Forms.DataGridViewTextBoxColumn AmountChq;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkSrNoChq;
        private System.Windows.Forms.DataGridViewTextBoxColumn BankNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn BranchNo;
        private System.Windows.Forms.DataGridView dgPayCreditCardDetails;
        private System.Windows.Forms.DataGridViewTextBoxColumn CreditCardNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Panel pnlRateType;
        private System.Windows.Forms.Button btnRateTypeCancel;
        private System.Windows.Forms.Button btnRateTypeOK;
        private System.Windows.Forms.TextBox txtRateTypePassword;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label44;
        private JitControls.OMPanel pnlBarCodePrint;
        private System.Windows.Forms.TextBox txtStartNo;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btnCancelPrintBarcode;
        private System.Windows.Forms.Button btnOKPrintBarCode;
        private System.Windows.Forms.TextBox txtNoOfPrint;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.RadioButton rbSmallMode;
        private System.Windows.Forms.RadioButton rbBigMod;
        private System.Windows.Forms.Label lblCompName;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.DataGridView dgCompany;
        private System.Windows.Forms.Panel pnlCompany;
        private System.Windows.Forms.TextBox txtTotalItemDisc;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnAllBarCodePrint;
        private JitControls.OMPanel pnlAllPrintBarCode;
        private System.Windows.Forms.RadioButton rdAllSmallMode;
        private System.Windows.Forms.RadioButton rdAllBigMode;
        private System.Windows.Forms.TextBox txtAllStartNo;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button btnAllBarcodeCancel;
        private System.Windows.Forms.Button btnAllBarcodeOK;
        private System.Windows.Forms.DataGridView dgPrintBarCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn BarCodeQty;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chkPrint;
        private System.Windows.Forms.DataGridViewTextBoxColumn FKRateSettingNo;
        private System.Windows.Forms.TextBox txtOtherDisc;
        private System.Windows.Forms.TextBox txtRefNo;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox cmbPartyNameSearch;
        private System.Windows.Forms.TextBox txtInvNoSearch;
        private JitControls.OMGPanel pnlPartySearch;
        private System.Windows.Forms.DataGridView dgPartySearch;
        private System.Windows.Forms.RadioButton rbPartyName;
        private System.Windows.Forms.RadioButton rbDocNo;
        private System.Windows.Forms.RadioButton rbInvNo;
        private System.Windows.Forms.Button btnAdvanceSearch;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnShortcut;
        private System.Windows.Forms.Button btnNewCustomer;
        private JitControls.OMBPanel ombPanel1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblLastBillAmt;
        private System.Windows.Forms.Label lblLastBillQty;
        private System.Windows.Forms.Label lblGrandTotal;
        private System.Windows.Forms.Button btnShowDetails;
        private System.Windows.Forms.Label lblLastPayment;
        private System.Windows.Forms.ListBox lstGroup1Lang;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridViewTextBoxColumn iItemNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn iItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemNameLang;
        private System.Windows.Forms.DataGridViewTextBoxColumn iRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn iUOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn iMRP;
        private System.Windows.Forms.DataGridViewTextBoxColumn iStock;
        private System.Windows.Forms.DataGridViewTextBoxColumn iUOMStk;
        private System.Windows.Forms.DataGridViewTextBoxColumn iSaleTax;
        private System.Windows.Forms.DataGridViewTextBoxColumn iPurTax;
        private System.Windows.Forms.DataGridViewTextBoxColumn iCompany;
        private System.Windows.Forms.DataGridViewTextBoxColumn iBarcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn iRateSettingNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMDefault;
        private System.Windows.Forms.DataGridViewTextBoxColumn PurRate;
        private System.Windows.Forms.Button btnShowBill;
        private JitControls.OMBPanel pnlInvSearch;
        private System.Windows.Forms.DataGridView dgInvSearch;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn SRCompanyName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rate;
        private System.Windows.Forms.DataGridViewTextBoxColumn NetRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscPerce;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscAmt;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscRupees;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscPercentage2;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscAmount2;
        private System.Windows.Forms.DataGridViewTextBoxColumn NetAmt;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaxPerce;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaxAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscRupees2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Barcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkStockTrnNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkSrNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkVoucherNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaxLedgerNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn SalesLedgerNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkRateSettingNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkItemTaxInfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn StockFactor;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActualQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn MKTQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn SalesVchNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaxVchNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn StockCompanyNo;
        private System.Windows.Forms.DataGridViewButtonColumn BarCodePrinting;
        private System.Windows.Forms.DataGridViewTextBoxColumn HSNCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn IGSTPercent;
        private System.Windows.Forms.DataGridViewTextBoxColumn IGSTAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn CGSTPercent;
        private System.Windows.Forms.DataGridViewTextBoxColumn CGSTAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn SGSTPercent;
        private System.Windows.Forms.DataGridViewTextBoxColumn SGSTAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn UTGSTPercent;
        private System.Windows.Forms.DataGridViewTextBoxColumn UTGSTAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn CessPercent;
        private System.Windows.Forms.DataGridViewTextBoxColumn CessAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn MRP;
        private System.Windows.Forms.DataGridViewTextBoxColumn Godown;
    }
}