﻿namespace Kirana.Vouchers
{
    partial class OtherCollectionEntrySearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ombPanel1 = new JitControls.OMBPanel();
            this.lblMsg = new System.Windows.Forms.Label();
            this.pnlInvSearch = new JitControls.OMBPanel();
            this.dgInvSearch = new System.Windows.Forms.DataGridView();
            this.lblSearchName = new System.Windows.Forms.Label();
            this.pnlSearch = new System.Windows.Forms.Panel();
            this.btnExit = new System.Windows.Forms.Button();
            this.dtpSearchDate = new System.Windows.Forms.DateTimePicker();
            this.rbDate = new System.Windows.Forms.RadioButton();
            this.btnPartyName = new System.Windows.Forms.Button();
            this.cmbPartyNameSearch = new System.Windows.Forms.ComboBox();
            this.rbPartyName = new System.Windows.Forms.RadioButton();
            this.rbInvNo = new System.Windows.Forms.RadioButton();
            this.btnCancelSearch = new System.Windows.Forms.Button();
            this.lblLable = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.lblTitle = new System.Windows.Forms.Label();
            this.ombPanel1.SuspendLayout();
            this.pnlInvSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgInvSearch)).BeginInit();
            this.pnlSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // ombPanel1
            // 
            this.ombPanel1.BorderColor = System.Drawing.Color.Gray;
            this.ombPanel1.BorderRadius = 3;
            this.ombPanel1.Controls.Add(this.lblTitle);
            this.ombPanel1.Controls.Add(this.lblMsg);
            this.ombPanel1.Controls.Add(this.pnlInvSearch);
            this.ombPanel1.Controls.Add(this.pnlSearch);
            this.ombPanel1.Location = new System.Drawing.Point(12, 12);
            this.ombPanel1.Name = "ombPanel1";
            this.ombPanel1.Size = new System.Drawing.Size(542, 402);
            this.ombPanel1.TabIndex = 0;
            // 
            // lblMsg
            // 
            this.lblMsg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(73)))), ((int)(((byte)(83)))));
            this.lblMsg.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMsg.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg.ForeColor = System.Drawing.Color.White;
            this.lblMsg.Location = new System.Drawing.Point(3, 187);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(538, 45);
            this.lblMsg.TabIndex = 123459;
            this.lblMsg.Text = "label4";
            this.lblMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMsg.Visible = false;
            // 
            // pnlInvSearch
            // 
            this.pnlInvSearch.BorderColor = System.Drawing.Color.Empty;
            this.pnlInvSearch.BorderRadius = 3;
            this.pnlInvSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlInvSearch.Controls.Add(this.dgInvSearch);
            this.pnlInvSearch.Controls.Add(this.lblSearchName);
            this.pnlInvSearch.Location = new System.Drawing.Point(14, 115);
            this.pnlInvSearch.Name = "pnlInvSearch";
            this.pnlInvSearch.Size = new System.Drawing.Size(509, 274);
            this.pnlInvSearch.TabIndex = 123462;
            this.pnlInvSearch.Visible = false;
            // 
            // dgInvSearch
            // 
            this.dgInvSearch.AllowUserToAddRows = false;
            this.dgInvSearch.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgInvSearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgInvSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgInvSearch.Location = new System.Drawing.Point(0, 23);
            this.dgInvSearch.Name = "dgInvSearch";
            this.dgInvSearch.ReadOnly = true;
            this.dgInvSearch.Size = new System.Drawing.Size(507, 249);
            this.dgInvSearch.TabIndex = 123455;
            this.dgInvSearch.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgInvSearch_CellFormatting);
            this.dgInvSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgInvSearch_KeyDown);
            // 
            // lblSearchName
            // 
            this.lblSearchName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(73)))), ((int)(((byte)(83)))));
            this.lblSearchName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSearchName.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblSearchName.ForeColor = System.Drawing.Color.White;
            this.lblSearchName.Location = new System.Drawing.Point(0, 0);
            this.lblSearchName.Name = "lblSearchName";
            this.lblSearchName.Size = new System.Drawing.Size(507, 23);
            this.lblSearchName.TabIndex = 123456;
            this.lblSearchName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlSearch
            // 
            this.pnlSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSearch.Controls.Add(this.btnExit);
            this.pnlSearch.Controls.Add(this.dtpSearchDate);
            this.pnlSearch.Controls.Add(this.rbDate);
            this.pnlSearch.Controls.Add(this.btnPartyName);
            this.pnlSearch.Controls.Add(this.cmbPartyNameSearch);
            this.pnlSearch.Controls.Add(this.rbPartyName);
            this.pnlSearch.Controls.Add(this.rbInvNo);
            this.pnlSearch.Controls.Add(this.btnCancelSearch);
            this.pnlSearch.Controls.Add(this.lblLable);
            this.pnlSearch.Controls.Add(this.txtSearch);
            this.pnlSearch.Location = new System.Drawing.Point(14, 39);
            this.pnlSearch.Name = "pnlSearch";
            this.pnlSearch.Size = new System.Drawing.Size(509, 70);
            this.pnlSearch.TabIndex = 0;
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(410, 9);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 51);
            this.btnExit.TabIndex = 123458;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // dtpSearchDate
            // 
            this.dtpSearchDate.Location = new System.Drawing.Point(197, 32);
            this.dtpSearchDate.Name = "dtpSearchDate";
            this.dtpSearchDate.Size = new System.Drawing.Size(137, 20);
            this.dtpSearchDate.TabIndex = 123457;
            this.dtpSearchDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpSearchDate_KeyDown);
            // 
            // rbDate
            // 
            this.rbDate.AutoSize = true;
            this.rbDate.BackColor = System.Drawing.Color.Transparent;
            this.rbDate.Location = new System.Drawing.Point(191, 9);
            this.rbDate.Name = "rbDate";
            this.rbDate.Size = new System.Drawing.Size(48, 17);
            this.rbDate.TabIndex = 123456;
            this.rbDate.TabStop = true;
            this.rbDate.Text = "Date";
            this.rbDate.UseVisualStyleBackColor = false;
            this.rbDate.CheckedChanged += new System.EventHandler(this.rbType_CheckedChanged);
            // 
            // btnPartyName
            // 
            this.btnPartyName.Location = new System.Drawing.Point(368, 37);
            this.btnPartyName.Name = "btnPartyName";
            this.btnPartyName.Size = new System.Drawing.Size(21, 21);
            this.btnPartyName.TabIndex = 123455;
            this.btnPartyName.Text = "..";
            this.btnPartyName.UseVisualStyleBackColor = true;
            // 
            // cmbPartyNameSearch
            // 
            this.cmbPartyNameSearch.FormattingEnabled = true;
            this.cmbPartyNameSearch.Location = new System.Drawing.Point(186, 37);
            this.cmbPartyNameSearch.Name = "cmbPartyNameSearch";
            this.cmbPartyNameSearch.Size = new System.Drawing.Size(176, 21);
            this.cmbPartyNameSearch.TabIndex = 123453;
            this.cmbPartyNameSearch.Visible = false;
            this.cmbPartyNameSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbPartyNameSearch_KeyDown);
            // 
            // rbPartyName
            // 
            this.rbPartyName.AutoSize = true;
            this.rbPartyName.BackColor = System.Drawing.Color.Transparent;
            this.rbPartyName.Location = new System.Drawing.Point(94, 9);
            this.rbPartyName.Name = "rbPartyName";
            this.rbPartyName.Size = new System.Drawing.Size(80, 17);
            this.rbPartyName.TabIndex = 123451;
            this.rbPartyName.TabStop = true;
            this.rbPartyName.Text = "Party Name";
            this.rbPartyName.UseVisualStyleBackColor = false;
            this.rbPartyName.CheckedChanged += new System.EventHandler(this.rbType_CheckedChanged);
            // 
            // rbInvNo
            // 
            this.rbInvNo.AutoSize = true;
            this.rbInvNo.BackColor = System.Drawing.Color.Transparent;
            this.rbInvNo.Checked = true;
            this.rbInvNo.Location = new System.Drawing.Point(17, 9);
            this.rbInvNo.Name = "rbInvNo";
            this.rbInvNo.Size = new System.Drawing.Size(57, 17);
            this.rbInvNo.TabIndex = 0;
            this.rbInvNo.TabStop = true;
            this.rbInvNo.Text = "Inv No";
            this.rbInvNo.UseVisualStyleBackColor = false;
            this.rbInvNo.CheckedChanged += new System.EventHandler(this.rbType_CheckedChanged);
            // 
            // btnCancelSearch
            // 
            this.btnCancelSearch.CausesValidation = false;
            this.btnCancelSearch.Location = new System.Drawing.Point(310, 69);
            this.btnCancelSearch.Name = "btnCancelSearch";
            this.btnCancelSearch.Size = new System.Drawing.Size(79, 23);
            this.btnCancelSearch.TabIndex = 123454;
            this.btnCancelSearch.Text = "Cancel";
            this.btnCancelSearch.UseVisualStyleBackColor = true;
            // 
            // lblLable
            // 
            this.lblLable.AutoSize = true;
            this.lblLable.Location = new System.Drawing.Point(6, 41);
            this.lblLable.Name = "lblLable";
            this.lblLable.Size = new System.Drawing.Size(45, 13);
            this.lblLable.TabIndex = 502;
            this.lblLable.Text = "Inv No :";
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(63, 37);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(114, 20);
            this.txtSearch.TabIndex = 0;
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            // 
            // lblTitle
            // 
            this.lblTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(73)))), ((int)(((byte)(83)))));
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(542, 25);
            this.lblTitle.TabIndex = 123463;
            this.lblTitle.Text = "Search - Cash Payment";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // OtherCollectionEntrySearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(566, 442);
            this.Controls.Add(this.ombPanel1);
            this.Name = "OtherCollectionEntrySearch";
            this.Text = "OtherCollectionEntrySearch";
            this.Load += new System.EventHandler(this.OtherCollectionEntry_Load);
            this.ombPanel1.ResumeLayout(false);
            this.pnlInvSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgInvSearch)).EndInit();
            this.pnlSearch.ResumeLayout(false);
            this.pnlSearch.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private JitControls.OMBPanel ombPanel1;
        private System.Windows.Forms.Panel pnlSearch;
        private System.Windows.Forms.DateTimePicker dtpSearchDate;
        private System.Windows.Forms.RadioButton rbDate;
        private System.Windows.Forms.Button btnPartyName;
        private System.Windows.Forms.ComboBox cmbPartyNameSearch;
        private System.Windows.Forms.RadioButton rbPartyName;
        private System.Windows.Forms.RadioButton rbInvNo;
        private System.Windows.Forms.Button btnCancelSearch;
        private System.Windows.Forms.Label lblLable;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Button btnExit;
        private JitControls.OMBPanel pnlInvSearch;
        private System.Windows.Forms.DataGridView dgInvSearch;
        private System.Windows.Forms.Label lblSearchName;
        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.Label lblTitle;
    }
}