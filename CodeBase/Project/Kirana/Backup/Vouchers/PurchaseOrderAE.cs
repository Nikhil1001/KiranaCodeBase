﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Vouchers
{
    /// <summary>
    /// This class used for Purchase Order AE
    /// </summary>
    public partial class PurchaseOrderAE : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();


        TOtherStock tOtherStock = new TOtherStock();
        TOtherVoucherEntry tOtherVoucherEntry = new TOtherVoucherEntry();
        DBTOtherVoucherEntry dbtOtherVoucherEntry = new DBTOtherVoucherEntry();

        //DataTable dtCompRatio = new DataTable();
        DataTable dtVchPrev = new DataTable();
        DataTable dtDelete = new DataTable();
        DataTable dtSearch = new DataTable();
        DataTable dtUOMTemp = new DataTable();
        DataTable dtVchMainDetails = new DataTable();
        DataTable dtPayLedger = new DataTable();
        DataTable dtGodown = null;
        //DataTable dtCompRatio = new DataTable();
        Color clrColorRow = Color.FromArgb(255, 224, 192);
        int cntRow, BillingMode, rowQtyIndex;//, tempindex;
        long LastBillNo = 0;
        bool Spaceflag = true, BillSizeFlag = false;
        long ItemNameType = 0, RateTypeNo, PartyNo, PayType;/*bcdno,*/
        int iItemNameStartIndex = 3, ItemType = 0;
        string strUom, Param1Value = "", Param2Value = "";
        string[] strItemQuery, strItemQuery_last;
        DateTime tempDate; long tempPartyNo = 0;

        DataTable dt = new DataTable();
        DataTablesCollection dtBillCollect = new DataTablesCollection();
        long VoucherUserNo;
        bool isDisc1PercentChanged = false, isDisc2PercentChanged = false;
        bool StopOnQty = false, StopOnRate = false;
        bool isDoProcess = false, MixModeFlag = false, ExistFormFlag = false;
        long Pno;

        DateTime dtFrom, dtTo;

        long ID, VoucherType, ItemNumber = 0;

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public PurchaseOrderAE()
        {
            InitializeComponent();
        }

        /// <summary>
        /// This is class of Parameterised Constructor
        /// </summary>
        public PurchaseOrderAE(long ID)
        {
            InitializeComponent();
            this.ID = ID;
        }

        private void dgBill_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                e.Value = (e.RowIndex + 1).ToString();

            }
            if (e.ColumnIndex == ColIndex.ItemName)
            {
                if (dgBill.Rows[e.RowIndex].Cells[ColIndex.PkBarCodeNo].Value != null && dgBill.Rows[e.RowIndex].Cells[ColIndex.PkBarCodeNo].Value.ToString() != "")
                {
                    if (dgBill.Rows[e.RowIndex].Cells[ColIndex.PkBarCodeNo].Value.ToString() != "0")
                        dgBill.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly = true;
                }
            }
            //if (e.ColumnIndex == ColIndex.FreeQty)
            //{
            //    if (e.Value == null || e.Value=="")
            //        e.Value = "0";
            //}

            //dgBill.CurrentRow.Selected = true;
        }

        private void PurchaseAE_Load(object sender, EventArgs e)
        {
            try
            {
                ObjFunction.LockButtons(true, this.Controls);
                ObjFunction.LockControls(false, this.Controls);
                dgBill.Enabled = false;
                VoucherType = 1;
                RateTypeNo = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_RateType));
                BindCompany();
                lblCompName.Text = GetCompanyName(DBGetVal.CompanyNo);
                SetSchemeStatus(0);
                InitDelTable();

                ItemNameType = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_ItemNameType)); //deepak
                initItemQuery();

                ObjFunction.FillComb(cmbPaymentType, "Select PKPayTypeNo,PayTypeName From MPayType where PKPayTypeNo in (2,3,4,5)order by PayTypeName");
                ObjFunction.FillCombo(cmbLocation, "SELECT GodownNo, GodownName FROM MGodown WHERE (IsActive = 'true') And GodownNo<>1 ");

                ObjFunction.FillList(lstBank, "Select LedgerNo,LedgerName From MLedger Where GroupNo=" + GroupType.BankAccounts + " And IsActive='true' order by LedgerName");
                ObjFunction.FillList(lstBranch, "Select BranchNo,BranchName From MBranch order by BranchName");
                ObjFunction.FillComb(cmbPartyName, "Select LedgerNo,LedgerName From MLedger Where GroupNo in " +
                    "(" + GroupType.SundryCreditors + ") and IsActive='true' order by LedgerName");//deepak
                ObjFunction.FillComb(cmbTaxType, "SELECT GroupNo, GroupName FROM MGroup WHERE (ControlGroup = " + GroupType.DutiesAndTaxes + " ) AND IsActive = 'True' ORDER BY GroupName");

                ObjFunction.FillCombo(cmbPartyNameSearch, " SELECT DISTINCT MLedger.LedgerNo, MLedger.LedgerName FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo  AND  " +
                             " TVoucherEntry.VoucherTypeCode = " + VchType.Purchase + " AND  TVoucherDetails.VoucherSrNo = 1 INNER JOIN MLedger ON MLedger.LedgerNo = TVoucherDetails.LedgerNo ORDER BY MLedger.LedgerName ");
                dtGodown = ObjFunction.GetDataView("Select GodownNo,GodownName From MGodown Where GodownNo<>1").Table;
                cmbLocation.SelectedValue = ObjFunction.GetAppSettings(AppSettings.P_OutwardLocation);

                FillRateType();
                cmbTaxType.SelectedValue = ObjFunction.GetAppSettings(AppSettings.P_TaxType);
                cmbTaxType.Enabled = false;
                lblSchemeStatus.Font = new Font("Verdana", 14, FontStyle.Bold);
                cmbPartyName.Enabled = false;//deepak
                txtInvNo.Enabled = false;

                dtpBillTime.Visible = false;
                label3.Visible = false;

                dtpBillDate.Enabled = false;
                dtpBillTime.Enabled = false;
                dtpBillTime.Format = DateTimePickerFormat.Time;
                InitControls();
                StopOnQty = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnQty));// Convert.ToBoolean(dtSalesSetting.Rows[0].ItemArray[14].ToString());
                StopOnRate = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnRate)); //Convert.ToBoolean(dtSalesSetting.Rows[0].ItemArray[13].ToString());

                //dtSearch = ObjFunction.GetDataView("Select PkVoucherNo from TVoucherEntry Where VoucherTypeCode=" + VoucherType + " AND CompanyNo=" + DBGetVal.CompanyNo + "").Table;

                dtSearch = ObjFunction.GetDataView("Select PkOtherVoucherNo  from TOtherVoucherEntry " +
                   " Where VoucherTypeCode=" + VoucherType + " AND CompanyNo=" + DBGetVal.CompanyNo +
                   " Order by VoucherDate, VoucherUserNo ").Table;

                if (dtSearch.Rows.Count > 0 && ID == 0)
                {
                    ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    FillControls();
                    SetNavigation();
                    setDisplay(true);
                }
                else
                {
                    if (ID != 0)
                    {
                        FillControls();
                        SetNavigation();
                        btnFirst.Visible = false; btnPrev.Visible = false; btnNext.Visible = false; btnLast.Visible = false;
                        btnNew.Visible = false; btnUpdate.Visible = false; btnCancel.Visible = false; btnSave.Visible = false;
                        btnSearch.Visible = false; btnShortcut.Visible = false; btnNewCustomer.Visible = false; btnMixMode.Visible = false;
                        btnAdvanceSearch.Visible = false; btnAllBarCodePrint.Visible = false;
                        btnDelete.Visible = false; btnOrderClosed.Visible = false;
                        ExistFormFlag = true;
                        dgBill.Enabled = true;
                        for (int i = 0; i < dgBill.ColumnCount; i++)
                        {
                            dgBill.Columns[i].ReadOnly = true;
                        }
                        dgBill.KeyDown -= new KeyEventHandler(dgBill_KeyDown);
                    }
                    else setDisplay(true);
                }

                btnNew.Focus();
                KeyDownFormat(this.Controls);

                DataTable dtSettings = ObjFunction.GetDataView("Select PKSettingNo From MSettings Where SettingTypeNo=5").Table;

                for (int i = 0; i < dtSettings.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(Convert.ToInt32(dtSettings.Rows[i].ItemArray[0].ToString()))) == true)
                    {
                        if (i >= ColIndex.MRP && i < dtSettings.Rows.Count)
                            dgBill.Columns[i + 1].Visible = true;
                        else
                            dgBill.Columns[i].Visible = true;
                    }
                    else
                    {
                        if (i >= ColIndex.MRP && i < dtSettings.Rows.Count - 1)
                            dgBill.Columns[i + 1].Visible = false;
                        else
                            dgBill.Columns[i].Visible = false;
                    }

                }
                dgBill.Columns[ColIndex.BarcodePrint].Visible = false;
                //dgBill.Columns[ColIndex.LandedRate].Visible = false;
                txtGrandTotal.Font = new Font("Verdana", 18, FontStyle.Bold);
                txtGrandTotal.ForeColor = Color.Maroon;

                lblGrandTotal.Font = new Font("Verdana", 18, FontStyle.Bold);
                lblGrandTotal.ForeColor = Color.White;

                new GridSearch(dgItemList, 1);
                formatPics();
                DisplayChargANDDisc();
                DisplayRateType();
                btnAllBarCodePrint.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_AllBarCodePrint));
                //dgPurHistory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)
                //            | System.Windows.Forms.AnchorStyles.Left)));
                //for (int i = 0; i < dgBill.Columns.Count; i++) dgBill.Columns[i].Visible = true;

                //btnShowDetails.BackColor = Color.FromArgb(255, 128, 0);
                button1.BackColor = Color.FromArgb(255, 128, 0);
                //button2.BackColor = Color.FromArgb(255, 128, 0);
                //button3.BackColor = Color.FromArgb(255, 128, 0);
                btnAllBarCodePrint.BackColor = Color.FromArgb(255, 128, 0);
                // btnShortcut.BackColor = Color.FromArgb(255, 128, 0);
                // btnNewCustomer.BackColor = Color.FromArgb(255, 128, 0);
                btnAllBarCodePrint.Font = new Font("Arial", 8, FontStyle.Bold);

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_Bilingual)) == false)
                {
                    dgItemList.Columns[2].Visible = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void DisplayChargANDDisc()
        {
            try
            {
                lblChrg1.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_Charges1Display));
                txtChrgRupees1.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_Charges1Display));
                if (ObjFunction.GetAppSettings(AppSettings.P_Charges2Display) != "")
                    lblOtherTax.Text = ObjFunction.GetAppSettings(AppSettings.P_Charges2Display);

                lblDisc1.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_Discount1Display));
                //txtDiscount1.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_Discount1Display));
                txtDiscRupees1.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_Discount1Display));
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

        }

        private void DisplayRateType()
        {
            //Rate Type related.
            //if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_IsDisplayRateType)) == true)
            //{
            //    lblRateType.Visible = true;
            //    cmbRateType.Visible = true;
            //}
            //else
            //{
            lblRateType.Visible = false;
            cmbRateType.Visible = false;
            //}

            //if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_RateTypeAskPassword)) == true)
            //    cmbRateType.Enabled = false;
            //else cmbRateType.Enabled = true;
        }

        private void formatPics()
        {
            try
            {
                pnlItemName.Width = 1000;
                pnlItemName.Height = 300;
                pnlItemName.Top = 88;
                pnlItemName.Left = 62;

                pnlGroup1.Top = 88;
                //pnlGroup1.Left = 200;
                //pnlGroup1.Width = 300;
                //pnlGroup1.Height = 220;
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_Bilingual)) == true)
                {
                    pnlGroup1.Left = 50;//150;
                    pnlGroup1.Width = 575;
                    lstGroup1.Font = ObjFunction.GetFont(FontStyle.Regular, 11);
                    lstGroup1Lang.Font = ObjFunction.GetLangFont();
                    dgItemList.RowTemplate.DefaultCellStyle.Font = null;
                    dgItemList.Columns[2].DefaultCellStyle.Font = ObjFunction.GetLangFont();
                }
                else
                {
                    pnlGroup1.Left = 150;
                    pnlGroup1.Width = 300;
                }
                pnlGroup1.Height = 275;

                pnlGroup2.Top = 88;
                pnlGroup2.Left = 100;
                pnlGroup2.Width = 300;
                pnlGroup2.Height = 220;

                pnlUOM.Top = 88;
                pnlUOM.Left = 372;
                pnlUOM.Width = 120;
                pnlUOM.Height = 80;

                pnlRate.Top = 88;
                pnlRate.Left = 430;
                pnlRate.Width = 120;
                pnlRate.Height = 80;

                pnlSalePurHistory.Width = 720;
                pnlSalePurHistory.Height = 235;
                pnlSalePurHistory.Top = pnlItemName.Height + 88;
                pnlSalePurHistory.Left = pnlItemName.Left;

                //btnMixMode.Left = btnExit.Left+btnExit.Width+5;
                //btnMixMode.Top = btnExit.Top;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void initItemQuery()
        {
            try
            {
                DataTable dtItemQuery = new DataTable();
                dtItemQuery = ObjFunction.GetDataView("SELECT * from MItemNameDisplayType WHERE ItemNameTypeNo = " + ItemNameType).Table;

                if (dtItemQuery.Rows.Count == 1)
                {
                    int qCount = 0;
                    for (int i = 1; i < 4; i++)
                    {
                        if (dtItemQuery.Rows[0]["Query" + i] != null && dtItemQuery.Rows[0]["Query" + i].ToString().Trim().Length > 0)
                        {
                            qCount++;
                        }
                    }

                    strItemQuery = new string[qCount];
                    strItemQuery_last = new string[qCount];
                    for (int i = 0; i < strItemQuery_last.Length; i++)
                    {
                        strItemQuery_last[i] = "";
                    }
                    qCount = 0;
                    for (int i = 1; i < 4; i++)
                    {
                        if (dtItemQuery.Rows[0]["Query" + i] != null && dtItemQuery.Rows[0]["Query" + i].ToString().Trim().Length > 0)
                        {
                            strItemQuery[qCount] = dtItemQuery.Rows[0]["Query" + i].ToString().Trim();
                            qCount++;
                        }
                    }

                    iItemNameStartIndex = Convert.ToInt32(dtItemQuery.Rows[0]["StartIndex"].ToString());
                    Param1Value = dtItemQuery.Rows[0]["Param1Value"].ToString();
                    Param2Value = dtItemQuery.Rows[0]["Param2Value"].ToString();
                }
                else
                {
                    OMMessageBox.Show("Please Select Valid Item Name display type in Purchse Setting Form ...");
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void InitControls()
        {
            try
            {
                VoucherUserNo = 0;
                SetRateType(RateTypeNo);
                dtBillCollect = new DataTablesCollection();
                cmbPaymentType.SelectedIndex = 0;
                dtpBillDate.Value = DBGetVal.ServerTime;
                dtpBillTime.Value = DBGetVal.ServerTime;
                MixModeFlag = false;
                while (dgBill.Rows.Count > 0)
                {
                    dgBill.Rows.RemoveAt(0);
                }
                //for (int i = 0; i < 20; i++)
                //{
                //    dgBill.Rows.Add();
                //}
                dgBill.Rows.Add();
                CalculateTotal();
                ObjFunction.GetFinancialYear(dtpBillDate.Value, out dtFrom, out dtTo);
                SetSchemeStatus(0);
                txtInvNo.Text = (ObjQry.ReturnLong("Select max(VoucherUserNo) from TOtherVoucherEntry Where VoucherTypeCode=" + VoucherType + " AND VoucherDate>='" + dtFrom.Date + "' AND VoucherDate<='" + dtTo.Date + "'", CommonFunctions.ConStr) + 1).ToString();

                string sqlQuery = " SELECT 0 AS Sr, MStockItems.ItemName, TStock.Quantity, MUOM.UOMName, TStock.Rate, TStock.Amount,MStockBarcode.Barcode, " +
                                 " TStock.PkStockTrnNo, MStockBarcode.PkStockBarcodeNo,TVoucherEntry.PkVoucherNo, MStockItems.ItemNo, MUOM.UOMNo " +
                                 " FROM TVoucherDetails INNER JOIN TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo " +
                                 " INNER JOIN TStock INNER JOIN MStockItems ON TStock.ItemNo = MStockItems.ItemNo ON " +
                                 " TVoucherDetails.PkVoucherTrnNo = TStock.FkVoucherTrnNo INNER JOIN " +
                                 " MStockBarcode ON MStockItems.ItemNo = MStockBarcode.ItemNo INNER JOIN " +
                                 " MUOM ON MStockItems.UOMDefault = MUOM.UOMNo " +
                                 " WHERE (MStockBarcode.Barcode = '') AND (TVoucherDetails.VoucherSrNo = 1) AND " +
                                 " (TVoucherEntry.VoucherTypeCode IN (9, 21)) AND MStockItems.CompanyNo=" + DBGetVal.CompanyNo + "";
                dt = ObjFunction.GetDataView(sqlQuery).Table;

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnDate)) == true) dtpBillDate.Focus();
                else if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnParty)) == true) cmbPartyName.Focus();
                else if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnHeaderDisc)) == true) txtOtherDisc.Focus();
                else if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnTaxType)) == true) cmbTaxType.Focus();
                else txtRefNo.Focus();
                //dgBill.Focus();
                dgBill.CurrentCell = dgBill[ColIndex.ItemName, 0];
                lblExchange.Visible = false;
                //ExchangeMode = false; 
                lblExchange.Visible = false;
                cmbPartyName.SelectedValue = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_DefaultPartyAC));
                //txtDiscount1.Text = "0.00"; 
                txtDiscRupees1.Text = "0.00";
                txtChrgRupees1.Text = "0.00";
                txtOtherTax.Text = "0.00";
                txtVisibility.Text = "0.00";
                txtReturnAmt.Text = "0.00";
                BindGridPayType(0);
                BindPayChequeDetails(0);
                BindPayCreditDetails(0);
                pnlBarCodePrint.Visible = false;
                btnSave.Enabled = true;
                tempDate = dtpBillDate.Value.Date;
                tempPartyNo = ObjFunction.GetComboValue(cmbPartyName);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillControls()
        {
            try
            {
                tOtherVoucherEntry = dbtOtherVoucherEntry.ModifyTOtherVoucherEntryByID(ID);
                VoucherUserNo = Convert.ToInt32(tOtherVoucherEntry.VoucherUserNo);
                txtInvNo.Text = tOtherVoucherEntry.VoucherUserNo.ToString();
                dtpBillDate.Value = tOtherVoucherEntry.VoucherDate;
                dtpBillTime.Value = tOtherVoucherEntry.VoucherTime;
                cmbPaymentType.SelectedValue = tOtherVoucherEntry.PayTypeNo.ToString();
                cmbTaxType.SelectedValue = tOtherVoucherEntry.TaxTypeNo.ToString();
                SetRateType(tOtherVoucherEntry.RateTypeNo);
                cmbTaxType.Enabled = false;
                cmbRateType.Enabled = false;
                MixModeFlag = false;
                PayType = tOtherVoucherEntry.PayTypeNo;
                // PurCompNo = tVoucherEntry.CompanyNo;
                lblCompName.Text = GetCompanyName(tOtherVoucherEntry.CompanyNo);
                txtRefNo.Text = tOtherVoucherEntry.Reference;

                txtSubTotal.Text = "0.00";
                txtTotalDisc.Text = "0.00"; txtTotalItemDisc.Text = "0.00";
                //txtDiscount1.Text = "0.00";
                txtDiscRupees1.Text = "0.00";
                txtTotalTax.Text = "0.00";
                txtReturnAmt.Text = "0.00";
                txtVisibility.Text = "0.00";
                //manali

                txtChrgRupees1.Text = "0.00";
                txtOtherTax.Text = "0.00";

                txtRemark.Text = tOtherVoucherEntry.Remark;
                txtReturnAmt.Text = tOtherVoucherEntry.ReturnAmount.ToString("0.00");
                txtVisibility.Text = tOtherVoucherEntry.Visibility.ToString("0.00");
                txtTotalDisc.Text = tOtherVoucherEntry.SchemeDisc.ToString("0.00");
                txtDistDisc.Text = tOtherVoucherEntry.DistDisc.ToString("0.00");
                txtDiscRupees1.Text = tOtherVoucherEntry.CashDisc.ToString("0.00");
                txtChrgRupees1.Text = tOtherVoucherEntry.Charges.ToString("0.00");
                txtSubTotal.Text = tOtherVoucherEntry.SubTotal.ToString("0.00");
                txtTotalTax.Text = tOtherVoucherEntry.TotalTax.ToString("0.00");
                txtRoundOff.Text = tOtherVoucherEntry.RoundOff.ToString("0.00");
                txtOtherTax.Text = tOtherVoucherEntry.OtherTax.ToString("0.00");
                cmbPartyName.SelectedValue = tOtherVoucherEntry.LedgerNo.ToString();
                Pno = tOtherVoucherEntry.LedgerNo;
                //end
                tempDate = dtpBillDate.Value.Date;

                //DataTable dt = ObjFunction.GetDataView("Select Case When Debit<>0 then Debit Else Credit End,LedgerNo,SrNo From TVoucherDetails Where FKVoucherNo=" + ID + " order by VoucherSrNo").Table;
                //double subTot = ObjQry.ReturnDouble("Select sum(Debit) from TVoucherDetails  Where FKVoucherNo=" + ID + " and LedgerNo!=1 ",CommonFunctions.ConStr);

                //Pno = ObjQry.ReturnLong("Select LedgerNo from TVoucherDetails  Where FKVoucherNo=" + ID + " AND VoucherSrNo=1", CommonFunctions.ConStr);
                ObjFunction.FillComb(cmbPartyName, "Select LedgerNo,LedgerName From MLedger Where GroupNo in " +
                    "(" + GroupType.SundryCreditors + ") and IsActive='true' or LedgerNo=" + Pno + " order by LedgerName");
                cmbPartyName.SelectedValue = Pno;

                if (ObjQry.ReturnLong("Select Count(*) From MManufacturerDetails Where LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + "", CommonFunctions.ConStr) > 0)
                {
                    chkFilterItem.Checked = true;
                    chkFilterItem.Enabled = false;
                }
                else
                {
                    chkFilterItem.Checked = false;
                    chkFilterItem.Enabled = false;
                }
                PartyNo = ObjFunction.GetComboValue(cmbPartyName);
                tempPartyNo = PartyNo;



                txtGrandTotal.Text = ((Convert.ToDouble(txtSubTotal.Text) - (Convert.ToDouble(txtTotalDisc.Text) + Convert.ToDouble(txtTotalItemDisc.Text))) + Convert.ToDouble(txtTotalTax.Text)).ToString("0.00");
                //cmbPaymentType.SelectedValue = ObjQry.ReturnLong("Select LedgerNo From TVoucherDetails Where VoucherSrNo=2 AND FKVoucherNo=" + ID + "", CommonFunctions.ConStr).ToString();

                FillGrid();




                long LastBillNo = ObjQry.ReturnLong("Select IsNull(Max(PkOtherVoucherNo),0) From TOtherVoucherEntry Where VoucherTypeCode=" + VoucherType + " AND  PkVoucherNo<" + ID + "", CommonFunctions.ConStr);
                //dt = ObjFunction.GetDataView("SELECT IsNull(SUM(TStock.Quantity),0) AS Quantity, IsNull(SUM(TStock.Amount),0) AS Amount FROM TVoucherDetails INNER JOIN TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN "+
                //    " TStock ON TVoucherDetails.PkVoucherTrnNo = TStock.FkVoucherTrnNo WHERE (TVoucherDetails.VoucherSrNo = 1) AND (TVoucherEntry.VoucherTypeCode = "+ VoucherType +") AND (TVoucherEntry.PkVoucherNo = "+ LastBillNo +")").Table;

                dt = ObjFunction.GetDataView("SELECT IsNull(SUM(TOtherStock.Quantity),0) AS Quantity, IsNull(SUM(TOtherStock.Amount),0) AS Amount from TOtherVoucherEntry INNER JOIN " +
                    " TOtherStock ON TOtherVoucherEntry.PkOtherVoucherNo = TOtherStock.FKVoucherNo  WHERE (TOtherVoucherEntry.VoucherTypeCode = " + VoucherType + ") AND (TOtherVoucherEntry.PkOtherVoucherNo = " + LastBillNo + ")").Table;

                if (dt.Rows.Count > 0)
                {
                    lblLastBillAmt.Text = "Amount : " + Convert.ToDouble(dt.Rows[0].ItemArray[1].ToString()).ToString("0.00");
                    txtLastBillAmt.Text = Convert.ToDouble(dt.Rows[0].ItemArray[1].ToString()).ToString("0.00");
                    lblLastBillQty.Text = "Qty: " + dt.Rows[0].ItemArray[0].ToString();
                    txtlastBillQty.Text = dt.Rows[0].ItemArray[0].ToString();
                    lblLastPayment.Text = "" + ObjQry.ReturnString("SELECT MPayType.PayTypeName FROM MPayType INNER JOIN TOtherVoucherEntry ON MPayType.PKPayTypeNo = TOtherVoucherEntry.PayTypeNo WHERE (TOtherVoucherEntry.PkOtherVoucherNo = " + LastBillNo + ")", CommonFunctions.ConStr);
                    txtLastPayment.Text = ObjQry.ReturnString("SELECT MPayType.PayTypeName FROM MPayType INNER JOIN TOtherVoucherEntry ON MPayType.PKPayTypeNo = TOtherVoucherEntry.PayTypeNo WHERE (TOtherVoucherEntry.PkOtherVoucherNo = " + LastBillNo + ")", CommonFunctions.ConStr);
                }

                SetSchemeStatus(tOtherVoucherEntry.VoucherStatus);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillGrid()
        {
            try
            {
                dgBill.Rows.Clear();

                //string sqlQuery = "SELECT 0 AS Sr, MStockItems.ItemName, TStock.Quantity, MUOM.UOMName, TStock.Rate, TStock.NetRate AS NetRate, " +
                //    " TStock.DiscPercentage AS DiscPercentage, TStock.DiscAmount AS DiscAmount,TStock.DiscRupees,TStock.DiscPercentage2, " +
                //    " TStock.DiscAmount2, TStock.NetAmount AS NetAmt, TStock.TaxPercentage AS TaxPercentage, TStock.TaxAmount AS TaxAmount, " +
                //    " TStock.DiscRupees2, TStock.Amount, MStockBarcode.Barcode,  " +
                //    " TStock.PkStockTrnNo, MStockBarcode.PkStockBarcodeNo, TVoucherDetails.PkVoucherTrnNo, MStockItems.ItemNo, " +
                //    " MUOM.UOMNo,  MItemTaxInfo.TaxLedgerNo, MItemTaxInfo.SalesLedgerNo, TStock.FkRateSettingNo, MItemTaxInfo.PkSrNo,  " +
                //    " MRateSetting.StockConversion AS StockConversion, TStock.Quantity * MRateSetting.StockConversion AS ActualQty,  MRateSetting.MKTQty AS MKTQuantity, " +
                //    " (SELECT PkVoucherTrnNo FROM TVoucherDetails AS SV WHERE SV.CompanyNo=TVoucherDetails.CompanyNo AND (LedgerNo = MItemTaxInfo.SalesLedgerNo) AND (FkVoucherNo = TVoucherDetails.FkVoucherNo)) AS SalesVchNo, " +
                //    " (SELECT PkVoucherTrnNo FROM TVoucherDetails AS TXV WHERE TXV.CompanyNo=TVoucherDetails.CompanyNo AND (LedgerNo = MItemTaxInfo.TaxLedgerNo) AND (FkVoucherNo = TVoucherDetails.FkVoucherNo)) AS TaxVchNo,MStockItems.CompanyNo,'Print' As BarcodePrinting " +
                //    " FROM dbo.MStockItems_V(2,NULL,NULL,NULL,NULL,NULL,NULL) AS MStockItems INNER JOIN TStock ON MStockItems.ItemNo = TStock.ItemNo INNER JOIN TVoucherDetails ON TStock.FkVoucherTrnNo = TVoucherDetails.PkVoucherTrnNo INNER JOIN " +
                //    " MItemTaxInfo ON TStock.FkItemTaxInfo = MItemTaxInfo.PkSrNo INNER JOIN MStockBarcode ON TStock.FkStockBarCodeNo = MStockBarcode.PkStockBarcodeNo INNER JOIN MUOM ON TStock.FkUomNo = MUOM.UOMNo INNER JOIN MRateSetting ON TStock.FkRateSettingNo = MRateSetting.PkSrNo " +
                //    " WHERE     (TVoucherDetails.Credit <> 0 ) AND (TVoucherDetails.FkVoucherNo = " + ID + ") AND MStockItems.CompanyNo=" + DBGetVal.CompanyNo + " ORDER BY TStock.PkStockTrnNo";

                //string sqlQuery = "SELECT 0 AS Sr, (Select ItemName from dbo.MStockItems_V(null, Tstock.ItemNo,NULL,NULL,NULL,NULL,NULL)) AS ItemName, TStock.Quantity, MUOM.UOMName, TStock.Rate, Cast(MRateSetting.MRP as numeric(18,2)) AS MRP,TStock.NetRate AS NetRate , TStock.FreeQty, MUOMFree.UOMName AS FreeUOM," +
                //    " TStock.DiscPercentage AS DiscPercentage, TStock.DiscAmount AS DiscAmount,TStock.DiscRupees,TStock.DiscPercentage2, " +
                //    " TStock.DiscAmount2, TStock.NetAmount AS NetAmt, TStock.TaxPercentage AS TaxPercentage, TStock.TaxAmount AS TaxAmount, " +
                //    " TStock.DiscRupees2, TStock.Amount, MStockBarcode.Barcode,  " +
                //    " TStock.PkStockTrnNo, MStockBarcode.PkStockBarcodeNo, TVoucherDetails.PkVoucherTrnNo, MStockItems.ItemNo, " +
                //    " MUOM.UOMNo,  MItemTaxInfo.TaxLedgerNo, MItemTaxInfo.SalesLedgerNo, TStock.FkRateSettingNo, MItemTaxInfo.PkSrNo,  " +
                //    " MRateSetting.StockConversion AS StockConversion, TStock.Quantity * MRateSetting.StockConversion AS ActualQty,  MRateSetting.MKTQty AS MKTQuantity, " +
                //    " IsNULL((SELECT PkVoucherTrnNo FROM TVoucherDetails AS SV WHERE SV.CompanyNo=TVoucherDetails.CompanyNo AND (LedgerNo = MItemTaxInfo.SalesLedgerNo) AND (FkVoucherNo = TVoucherDetails.FkVoucherNo)),0) AS SalesVchNo, " +
                //    " IsNull((SELECT PkVoucherTrnNo FROM TVoucherDetails AS TXV WHERE TXV.CompanyNo=TVoucherDetails.CompanyNo AND (LedgerNo = MItemTaxInfo.TaxLedgerNo) AND (FkVoucherNo = TVoucherDetails.FkVoucherNo)),0) AS TaxVchNo,MStockItems.CompanyNo,'Print' As BarcodePrinting,TStock.FreeUOMNo,Cast(MRateSetting.MRP as numeric(18,2)) AS TempMRP,TStock.LandedRate " +//,TStock.LandedRate
                //    " FROM MStockItems INNER JOIN TStock ON MStockItems.ItemNo = TStock.ItemNo INNER JOIN TVoucherDetails ON TStock.FkVoucherTrnNo = TVoucherDetails.PkVoucherTrnNo INNER JOIN " +
                //    " MItemTaxInfo ON TStock.FkItemTaxInfo = MItemTaxInfo.PkSrNo INNER JOIN MStockBarcode ON TStock.FkStockBarCodeNo = MStockBarcode.PkStockBarcodeNo INNER JOIN MUOM ON TStock.FkUomNo = MUOM.UOMNo INNER JOIN MRateSetting ON TStock.FkRateSettingNo = MRateSetting.PkSrNo INNER JOIN " +
                //    " MUOM AS MUOMFree ON TStock.FreeUOMNo = MUOMFree.UOMNo " +
                //    " WHERE  (TVoucherDetails.FkVoucherNo = " + ID + ") " + /*" AND MStockItems.CompanyNo=" + DBGetVal.CompanyNo +*/ " ORDER BY TStock.PkStockTrnNo";//(TVoucherDetails.Credit <> 0 ) AND
                string sqlQuery = " SELECT     0 AS Sr, " +
                          " (SELECT     ItemName " +
                            " FROM          dbo.MStockItems_V(NULL, TOtherStock.ItemNo, NULL, NULL, NULL, NULL, NULL) AS MStockItems_V_1) AS ItemName, TOtherStock.Quantity, " +
                            " MUOM.UOMName, TOtherStock.Rate, CAST(MRateSetting.MRP AS numeric(18, 2)) AS MRP, TOtherStock.NetRate, TOtherStock.FreeQty,  " +
                            " MUOMFree.UOMName AS FreeUOM, TOtherStock.DiscPercentage, TOtherStock.DiscAmount, TOtherStock.DiscRupees, TOtherStock.DiscPercentage2, " +
                            " TOtherStock.DiscAmount2, TOtherStock.NetAmount AS NetAmt, TOtherStock.TaxPercentage, TOtherStock.TaxAmount, TOtherStock.DiscRupees2, TOtherStock.Amount, " +
                            " MStockBarcode.Barcode, TOtherStock.PkOtherStockTrnNo, MStockBarcode.PkStockBarcodeNo, 0 as PkVoucherTrnNo,MStockItems.ItemNo, MUOM.UOMNo, MItemTaxInfo.TaxLedgerNo, " +
                            " MItemTaxInfo.SalesLedgerNo, TOtherStock.FkRateSettingNo, MItemTaxInfo.PkSrNo, MRateSetting.StockConversion,  " +
                            " TOtherStock.Quantity * MRateSetting.StockConversion AS ActualQty, MRateSetting.MKTQty AS MKTQuantity, 0 AS SalesVchNo, 0 AS TaxVchNo,  " +
                            " MStockItems.CompanyNo, 'Print' AS BarcodePrinting, TOtherStock.FreeUOMNo, CAST(MRateSetting.MRP AS numeric(18, 2)) AS TempMRP, " +
                            " TOtherStock.LandedRate,TOtherStock.BalanceQty FROM MStockItems INNER JOIN " +
                            " TOtherStock ON MStockItems.ItemNo = TOtherStock.ItemNo INNER JOIN " +
                            " MItemTaxInfo ON TOtherStock.FkItemTaxInfo = MItemTaxInfo.PkSrNo INNER JOIN " +
                            " MStockBarcode ON TOtherStock.FkStockBarCodeNo = MStockBarcode.PkStockBarcodeNo INNER JOIN " +
                            " MUOM ON TOtherStock.FkUomNo = MUOM.UOMNo INNER JOIN " +
                            " MRateSetting ON TOtherStock.FkRateSettingNo = MRateSetting.PkSrNo INNER JOIN " +
                            " MUOM AS MUOMFree ON TOtherStock.FreeUOMNo = MUOMFree.UOMNo INNER JOIN " +
                            " TOtherVoucherEntry ON TOtherStock.FKVoucherNo = TOtherVoucherEntry.PkOtherVoucherNo " +
                            " Where TOtherVoucherEntry.PkOtherVoucherNo=" + ID + "" +
                            " ORDER BY TOtherStock.PkOtherStockTrnNo ";

                dt = ObjFunction.GetDataView(sqlQuery).Table;

                dtBillCollect = new DataTablesCollection();
                lblBillItem.Text = "0"; lblBilExchangeItem.Text = "0";
                string strStkNo = "";

                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    dgBill.Rows.Add();
                    for (int i = 0; i < dt.Columns.Count; i++)//LandedRate
                    {
                        //if (dt.Rows.Count > 0)
                        //{
                        dgBill.Rows[j].Cells[i].Value = dt.Rows[j].ItemArray[i].ToString();
                        //dgBill.Rows[j].Cells[i].ReadOnly = true;
                        //}
                    }

                    if (Convert.ToDouble(dgBill.Rows[j].Cells[ColIndex.Quantity].Value) >= 0)
                        lblBillItem.Text = (Convert.ToDouble(lblBillItem.Text) + Convert.ToDouble(dgBill.Rows[j].Cells[ColIndex.Quantity].Value)).ToString();
                    else
                        lblBilExchangeItem.Text = (Convert.ToInt64(lblBilExchangeItem.Text) + Math.Abs(Convert.ToDouble(dgBill.Rows[j].Cells[ColIndex.Quantity].Value))).ToString();
                    //DEEPAK
                    strStkNo = dgBill.Rows[j].Cells[ColIndex.PkStockTrnNo].Value.ToString();
                    FillGodownDetails(strStkNo);
                    if (j == 0)
                    {
                        long GdNo = ObjQry.ReturnLong("Select GodownNo From TStockGodown WHERE (FKStockTrnNo in (" + strStkNo + "))", CommonFunctions.ConStr);
                        ObjFunction.FillCombo(cmbLocation, "SELECT GodownNo, GodownName FROM MGodown WHERE ((IsActive = 'true') Or (GodownNo=" + GdNo + ")) And GodownNo<>1 ");
                        cmbLocation.SelectedValue = GdNo.ToString();
                    }
                    //DEEPAK
                }

                #region old code
                //for (int i = 0; i < dgBill.Rows.Count; i++)
                //{

                //    /*
                //     * if (i == 0) strStkNo = dgBill.Rows[i].Cells[ColIndex.PkStockTrnNo].Value.ToString();
                //     * else strStkNo += "," + dgBill.Rows[i].Cells[ColIndex.PkStockTrnNo].Value.ToString();
                //     */


                //    //dtBillCollect.Add(ObjFunction.GetDataView("Exec GetStockGodownDetails " + dgBill.Rows[i].Cells[ColIndex.PkStockTrnNo].Value.ToString() + "," + dgBill.Rows[i].Cells[ColIndex.ItemNo].Value + "").Table);
                //}
                ////DataSet ds = ObjDset.FillDset("New", "Exec GetStockGodownDetailsTemp '" + strStkNo + "'", CommonFunctions.ConStr);
                ////for (int i = 0; i < ds.Tables.Count; i++)
                ////    dtBillCollect.Add(ds.Tables[i]);
                #endregion

                dgBill.Rows.Add();
                dgBill.CurrentCell = dgBill[1, dgBill.Rows.Count - 1];
                CalculateTotal();
                BindGridPayType(ID);
                BindPayChequeDetails(ID);
                BindPayCreditDetails(ID);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillGodownDetails(string strStkNo)
        {
            try
            {
                string strQuery = "SELECT ISNULL(MGodown.GodownNo, 0) , ISNULL(MGodown.GodownName, '') , ISNULL(TStockGodown.Qty, 0), " +
                       " ISNULL(TStockGodown.ActualQty, 0) , ISNULL(TStockGodown.PKStockGodownNo, 0)   " +
                       " FROM MGodown LEFT OUTER JOIN (SELECT * FROM TStockGodown " +
                       " WHERE (TStockGodown.FKStockTrnNo in (" + strStkNo + ")) ) As TStockGodown  " +
                       " ON TStockGodown.GodownNo = MGodown.GodownNo " +
                       "WHERE (MGodown.GodownNo <> 1) ";

                DataTable dt = ObjFunction.GetDataView(strQuery).Table;
                dtBillCollect.Add(dt);
                #region old code
                //long tempStkNo = 0;
                //if (dt.Rows.Count > 0)
                //{
                //    //tempStkNo = Convert.ToInt64(dt.Rows[0].ItemArray[4].ToString());
                //    DataTable dtGodStock = dt.Clone();
                //    for (int i = 0; i < dt.Rows.Count; i++)
                //    {

                //        DataRow dr = dtGodStock.NewRow();
                //        for (int j = 0; j < dt.Columns.Count; j++)
                //            dr[j] = dt.Rows[i].ItemArray[j].ToString();
                //        dtGodStock.Rows.Add(dr);
                //        dtGodStock.AcceptChanges();
                //        #region old code
                //        /*
                //        for (int k = 0; k < dtGodown.Rows.Count; k++)
                //        {
                //            if (dtGodown.Rows[k].ItemArray[0].ToString() == dt.Rows[k].ItemArray[0].ToString())
                //            {
                //                if (Convert.ToInt64(dt.Rows[i].ItemArray[4].ToString()) != tempStkNo)
                //                {
                //                    dtBillCollect.Add(dtGodStock);
                //                    dtGodStock = new DataTable();
                //                    dtGodStock = dt.Clone();
                //                    tempStkNo = Convert.ToInt64(dt.Rows[i].ItemArray[4].ToString());

                //                }

                //                DataRow dr = dtGodStock.NewRow();
                //                for (int j = 0; j < dt.Columns.Count; j++)
                //                    dr[j] = dt.Rows[i].ItemArray[j].ToString();
                //                dtGodStock.Rows.Add(dr);

                //            }
                //            else
                //            {
                //                DataRow dr = dtGodStock.NewRow();
                //                dr[0] = dtGodown.Rows[k].ItemArray[0].ToString();
                //                dr[1] = dtGodown.Rows[k].ItemArray[1].ToString();
                //                for (int j = 2; j < 5; j++)
                //                    dr[j] = "0";
                //                dtGodStock.Rows.Add(dr);
                //            }
                //        }
                //        */
                //        #endregion
                //    }
                //    dtBillCollect.Add(dtGodStock);
                //}
                #endregion
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void CalculateTotal()
        {
            try
            {
                double AmtGrTotal = (txtGrandTotal.Text == "") ? 0 : Convert.ToDouble(txtGrandTotal.Text);
                txtSubTotal.Text = "0.00";
                lblBillItem.Text = "0";
                lblBilExchangeItem.Text = "0";
                txtGrandTotal.Text = "0.00";
                if (txtTotalDisc.Text == null || txtTotalDisc.Text == "")
                    txtTotalDisc.Text = "0.00";
                if (txtDistDisc.Text == null || txtDistDisc.Text == "")
                    txtDistDisc.Text = "0.00";
                txtTotalItemDisc.Text = "0.00";
                txtTotalTax.Text = "0.00";
                //txtVisibility.Text = "0.00";
                //txtReturnAmt.Text = "0.00";
                double subTotal = 0, TotalDiscBeforeTax = 0, TotalDiscAfterTax = 0, totalChrg = 0, totalTax = 0, TotFinal = 0, TotSchemeDisc = 0, TotDistDisc = 0;
                if (Validations() == true)
                {
                    for (int i = 0; i < dgBill.Rows.Count; i++)
                    {
                        if (dgBill.Rows[i].Cells[ColIndex.ItemNo].Value != null && dgBill.Rows[i].Cells[ColIndex.ItemNo].Value.ToString() != "")
                        {

                            #region check & init Default values
                            if (dgBill.Rows[i].Cells[ColIndex.Quantity].Value == null) dgBill.Rows[i].Cells[ColIndex.Quantity].Value = Convert.ToDouble(1).ToString("0.00");

                            if (dgBill.Rows[i].Cells[ColIndex.MKTQuantity].Value == null) dgBill.Rows[i].Cells[ColIndex.MKTQuantity].Value = 1;
                            if (dgBill.Rows[i].Cells[ColIndex.StockFactor].Value == null) dgBill.Rows[i].Cells[ColIndex.StockFactor].Value = 1;
                            if (dgBill.Rows[i].Cells[ColIndex.Rate].Value == null) dgBill.Rows[i].Cells[ColIndex.Rate].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.DiscRupees].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscRupees].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.DiscRupees2].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscRupees2].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.FreeQty].Value == null) dgBill.Rows[i].Cells[ColIndex.FreeQty].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.LandedRate].Value == null) dgBill.Rows[i].Cells[ColIndex.LandedRate].Value = 0;
                            //if (dgBill.Rows[i].Cells[ColIndex.FreeQty].Value == "") dgBill.Rows[i].Cells[ColIndex.FreeQty].Value = 0;
                            #endregion

                            #region fetch basic values
                            double Qty = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value);
                            double Rate = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Rate].Value);
                            double TaxPerce = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.TaxPercentage].Value);
                            double MktQty = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.MKTQuantity].Value);

                            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_IsReverseRateCalc)) == true)
                            {
                                Rate = Convert.ToDouble(((Rate * 100) / (100 + TaxPerce)).ToString("0.00")); //reverse rate
                            }

                            double Amount = Convert.ToDouble((((Qty) * (Rate)) / (MktQty)).ToString("0.0000"));
                            #endregion

                            #region Before tax discount calculation
                            //disc1 %
                            double Disc1 = 0;
                            if (isDisc1PercentChanged == true && i == dgBill.CurrentRow.Index)
                            {
                                dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value).ToString(Format.DoubleFloating);
                                dgBill.Rows[i].Cells[ColIndex.DiscAmount].Value = Convert.ToDouble(((Amount * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value)) / 100).ToString("0.00"));
                                isDisc1PercentChanged = false;
                                //isDisc2PercentChanged = true;
                                Disc1 = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscAmount].Value);
                            }
                            else if (i == dgBill.CurrentRow.Index)
                            {
                                //Disc1 = Convert.ToDouble(((Amount * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value)) / 100).ToString("0.0000"));
                                Disc1 = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscAmount].Value);
                                dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value = (Amount == 0) ? "0" : ((Disc1 * 100) / Amount).ToString("0.00");
                            }
                            Disc1 = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscAmount].Value);
                            //disc 1 rs
                            double DiscAmt1 = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscRupees].Value);
                            double DiscBeforeTax = Disc1 + DiscAmt1;// before disc 2%
                            double tAmount = Amount - DiscBeforeTax; // before disc 2%
                            //disc 2 %
                            double Disc2 = 0;
                            if (isDisc2PercentChanged == true && i == dgBill.CurrentRow.Index)
                            {
                                dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value).ToString(Format.DoubleFloating);
                                dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value = Convert.ToDouble(((tAmount * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value)) / 100).ToString("0.00"));
                                isDisc2PercentChanged = false;
                                Disc2 = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value);
                            }
                            else if (i == dgBill.CurrentRow.Index)
                            {
                                //Disc2 = Convert.ToDouble(((tAmount * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value)) / 100).ToString("0.0000"));
                                Disc2 = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value);
                                dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value = (tAmount == 0) ? "0" : ((Disc2 * 100) / tAmount).ToString("0.00");
                            }
                            Disc2 = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value);
                            // Total disc before tax
                            DiscBeforeTax = 0;
                            DiscBeforeTax += Disc2;
                            //Net Amt Before Tax - for sub total
                            tAmount -= Disc2;
                            //Net Rate (after 1st disc %, 1st Rs, 2nd % OR Before Tax)
                            double ttRate = 0, LandedRate = 0, LandedGrandTotal = 0;
                            #region Tax Calculation
                            double TaxAmt = Convert.ToDouble(((tAmount * TaxPerce) / (100)).ToString("0.00"));
                            #endregion

                            #region Charges Calculation
                            tAmount += Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscRupees2].Value);
                            #endregion
                            double DiscAfterTax = 0;
                            dgBill.Rows[i].Cells[ColIndex.Amount].Value = (tAmount + TaxAmt - DiscAfterTax).ToString("0.00");

                            for (int rw = 0; rw < dgBill.Rows.Count - 1; rw++)
                            {
                                LandedGrandTotal += Convert.ToDouble(dgBill.Rows[rw].Cells[ColIndex.Amount].Value);
                            }

                            if (tAmount != 0 || Qty != 0)
                            {
                                ttRate = (tAmount) / Qty;
                                LandedGrandTotal = Convert.ToDouble(Convert.ToDouble((Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Amount].Value) * (Convert.ToDouble(txtOtherTax.Text) + Convert.ToDouble(txtChrgRupees1.Text))) / LandedGrandTotal).ToString(Format.DoubleFloating));
                                LandedRate = (tAmount + TaxAmt + LandedGrandTotal) / (Qty + Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.FreeQty].Value));
                            }
                            #endregion



                            //#region After tax discount calculation
                            //double DiscAmt2 = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscRupees2].Value);
                            //double DiscAfterTax = DiscAmt2;
                            //#endregion
                            //double DiscAfterTax = 0;

                            #region Put values in Grid
                            //dgBill.Rows[i].Cells[ColIndex.Amount].Value = (tAmount + TaxAmt - DiscAfterTax).ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.DiscAmount].Value = Disc1.ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value = Disc2.ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.TaxAmount].Value = TaxAmt.ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.NetRate].Value = ttRate.ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.NetAmt].Value = tAmount.ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.ActualQty].Value = (((Qty) + Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.FreeQty].Value)) * (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.StockFactor].Value)));
                            dgBill.Rows[i].Cells[ColIndex.LandedRate].Value = LandedRate.ToString("0.00");
                            #endregion

                            #region Cumulative sum for footer calc usage
                            TotalDiscBeforeTax += DiscBeforeTax;
                            TotalDiscAfterTax += DiscAfterTax;
                            subTotal += tAmount;
                            totalTax += TaxAmt;
                            #endregion

                            #region Calculate total Sale & Exchange Qty
                            if (Qty >= 0)
                                lblBillItem.Text = (Convert.ToDouble(lblBillItem.Text) + Qty).ToString();
                            else
                                lblBilExchangeItem.Text = (Convert.ToDouble(lblBilExchangeItem.Text) + Math.Abs(Qty)).ToString();
                            #endregion

                            TotSchemeDisc = TotSchemeDisc + Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscAmount].Value);
                            TotDistDisc = TotDistDisc + Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value);
                        }
                    }



                    double TotalAmt = 0.0;
                    if (Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_BTaxItemDisc)) != 0)
                    {
                        //subTotal = Convert.ToDouble((subTotal + TotalDiscBeforeTax).ToString("0.00"));// Math.Round(subTotal, 00);
                        subTotal = Convert.ToDouble((subTotal).ToString("0.00"));// Math.Round(subTotal, 00);
                        //TotalAmt = Convert.ToDouble((subTotal - TotalDiscBeforeTax + totalTax - TotalDiscAfterTax).ToString("0.00"));
                        TotalAmt = Convert.ToDouble((subTotal + totalTax - TotalDiscAfterTax).ToString("0.00"));
                    }
                    else
                    {
                        subTotal = Convert.ToDouble(subTotal.ToString("0.00"));// Math.Round(subTotal, 00);
                        TotalAmt = Convert.ToDouble((subTotal + totalTax - TotalDiscAfterTax).ToString("0.00"));
                    }


                    #region footer discount & Charges calculation
                    //txtDiscRupees1.Text = Convert.ToDouble((TotalAmt * Convert.ToDouble(txtDiscount1.Text)) / 100).ToString("0.00");
                    TotalAmt -= Convert.ToDouble(txtDiscRupees1.Text);



                    double TotalAnotherDisc = Convert.ToDouble(txtDiscRupees1.Text);
                    totalChrg = Convert.ToDouble(txtChrgRupees1.Text) + Convert.ToDouble(txtOtherTax.Text);

                    #endregion

                    #region Put Values in Footer TextFields
                    txtSubTotal.Text = subTotal.ToString("0.00");

                    if (Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_BTaxItemDisc)) != 0)
                    {
                        txtTotalItemDisc.Text = TotalDiscBeforeTax.ToString("0.00");
                    }
                    else
                    {
                        txtTotalItemDisc.Text = "0.00";
                        TotalDiscBeforeTax = 0;
                    }
                    // txtTotalDisc.Text = TotalDiscAfterTax.ToString("0.00");
                    txtTotalDisc.Text = TotSchemeDisc.ToString("0.00");
                    txtDistDisc.Text = TotDistDisc.ToString("0.00");
                    txtTotalTax.Text = totalTax.ToString("0.00");

                    txtTotalAnotherDisc.Text = TotalAnotherDisc.ToString("0.00");
                    txtTotalChrgs.Text = totalChrg.ToString("0.00");

                    totalTax = Convert.ToDouble(totalTax.ToString("0.00"));
                    txtGrandTotal.Text = ((subTotal + totalTax + totalChrg) - (TotalDiscAfterTax + TotalAnotherDisc)).ToString("0.00");
                    //txtGrandTotal.Text = ((subTotal + totalTax + totalChrg) - (TotalDiscAfterTax + TotalDiscBeforeTax + TotalAnotherDisc)).ToString("0.00");
                    TotFinal = Math.Round(Convert.ToDouble(txtGrandTotal.Text), MidpointRounding.AwayFromZero);
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_IsBillRoundOff)) == true)
                    {
                        txtRoundOff.Text = (TotFinal - Convert.ToDouble(txtGrandTotal.Text)).ToString("0.00");
                        // txtGrandTotal.Text = ((subTotal + totalTax + totalChrg + Convert.ToDouble(txtRoundOff.Text)) - (TotalDiscAfterTax + TotalDiscBeforeTax + TotalAnotherDisc)).ToString("0.00");
                        txtGrandTotal.Text = ((subTotal + totalTax + totalChrg + Convert.ToDouble(txtRoundOff.Text)) - (TotalDiscAfterTax + TotalAnotherDisc)).ToString("0.00");
                    }
                    else
                        txtRoundOff.Text = "0.00";
                    #endregion

                    long ControlUnder = ObjQry.ReturnLong("Select ControlUnder From MPayType Where PKPayTypeNo=" + ObjFunction.GetComboValue(cmbPaymentType) + "", CommonFunctions.ConStr);
                    if (ControlUnder == 4)
                    {
                        if (dgPayChqDetails.Rows.Count > 0)
                        {
                            dgPayChqDetails.Rows[0].Cells[4].Value = txtGrandTotal.Text;
                        }
                    }
                    else if (ControlUnder == 5)
                    {
                        if (dgPayCreditCardDetails.Rows.Count > 0)
                        {
                            dgPayCreditCardDetails.Rows[0].Cells[3].Value = txtGrandTotal.Text;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void CalculateTotalCompanywise()
        {
            try
            {
                for (int i = 0; i < dgBill.Rows.Count; i++)
                {
                    if (dgBill.Rows[i].Cells[ColIndex.ItemNo].Value != null && dgBill.Rows[i].Cells[ColIndex.ItemNo].Value.ToString() != "")
                    {
                    }
                }

                double subTotal = 0, totalDisc = 0, totalChrg = 0, totalTax = 0, TotFinal = 0;
                if (Validations() == true)
                {
                    for (int i = 0; i < dgBill.Rows.Count; i++)
                    {
                        if (dgBill.Rows[i].Cells[ColIndex.ItemNo].Value != null && dgBill.Rows[i].Cells[ColIndex.ItemNo].Value.ToString() != "")
                        {
                            if (dgBill.Rows[i].Cells[ColIndex.Quantity].Value == null) dgBill.Rows[i].Cells[ColIndex.Quantity].Value = Convert.ToDouble(1).ToString("0.00");
                            if (dgBill.Rows[i].Cells[ColIndex.MKTQuantity].Value == null) dgBill.Rows[i].Cells[ColIndex.MKTQuantity].Value = 1;
                            if (dgBill.Rows[i].Cells[ColIndex.StockFactor].Value == null) dgBill.Rows[i].Cells[ColIndex.StockFactor].Value = 1;
                            if (dgBill.Rows[i].Cells[ColIndex.Rate].Value == null) dgBill.Rows[i].Cells[ColIndex.Rate].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.DiscRupees].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscRupees].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.DiscRupees2].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscRupees2].Value = 0;

                            double Amount = Convert.ToDouble((((Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value)) * (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Rate].Value))) / (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.MKTQuantity].Value))).ToString("0.0000"));
                            double Disc1 = Convert.ToDouble(((Amount * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value)) / 100).ToString("0.0000"));
                            double Disc2 = Convert.ToDouble(((Amount * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value)) / 100).ToString("0.0000"));

                            double DiscAmt = (Disc1 + Disc2); //Convert.ToDouble(((Amount * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value)) / 100).ToString("0.0000"));
                            //DiscAmt += Convert.ToDouble((((Amount - DiscAmt) * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value)) / 100).ToString("0.0000"));
                            DiscAmt += Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscRupees].Value);// +Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscRupees2].Value);

                            DiscAmt = 0;//Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscRupees2].Value);
                            double tAmount = Amount - DiscAmt;
                            double TaxPerce = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.TaxPercentage].Value);
                            double TaxAmt = Convert.ToDouble(((tAmount * TaxPerce) / (100 + TaxPerce)).ToString("0.0000"));
                            totalTax += TaxAmt;
                            double ttRate = (tAmount - TaxAmt) / Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value);


                            dgBill.Rows[i].Cells[ColIndex.Amount].Value = tAmount.ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.DiscAmount].Value = Disc1.ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.DiscAmount].Value = Disc2.ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.TaxAmount].Value = TaxAmt.ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.NetRate].Value = ttRate.ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.NetAmt].Value = (ttRate * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value)).ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.ActualQty].Value = (((Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value)) + (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.FreeQty].Value))) * (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.StockFactor].Value)));

                            subTotal = subTotal + Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.NetAmt].Value);
                            totalDisc = totalDisc + DiscAmt;// Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscAmount].Value);

                        }
                    }
                    subTotal = Math.Round(subTotal, 00);
                    txtSubTotal.Text = (subTotal + totalDisc).ToString("0.00");
                    txtTotalDisc.Text = totalDisc.ToString("0.00");

                    double TotalAmt = Convert.ToDouble(((Convert.ToDouble(txtSubTotal.Text) + totalTax) - Convert.ToDouble(txtTotalDisc.Text) - Convert.ToDouble(txtDistDisc.Text)).ToString("0.00"));
                    // txtDiscRupees1.Text = Convert.ToDouble((TotalAmt * Convert.ToDouble(txtDiscount1.Text)) / 100).ToString("0.00");

                    TotalAmt -= Convert.ToDouble(txtDiscRupees1.Text);


                    totalDisc = Convert.ToDouble(txtDiscRupees1.Text);
                    totalChrg = Convert.ToDouble(txtChrgRupees1.Text) + Convert.ToDouble(txtOtherTax.Text);
                    txtTotalAnotherDisc.Text = totalDisc.ToString("0.00");
                    txtTotalChrgs.Text = totalChrg.ToString("0.00");
                    txtTotalTax.Text = totalTax.ToString("0.00");
                    totalTax = Math.Round(totalTax, 00);
                    txtGrandTotal.Text = ((subTotal + totalTax + totalChrg) - totalDisc).ToString("0.00");
                    TotFinal = Math.Round(Convert.ToDouble(txtGrandTotal.Text), MidpointRounding.AwayFromZero);
                    txtRoundOff.Text = (TotFinal - Convert.ToDouble(txtGrandTotal.Text)).ToString("0.00");
                    txtGrandTotal.Text = ((subTotal + totalTax + totalChrg + Convert.ToDouble(txtRoundOff.Text)) - totalDisc).ToString("0.00");
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void txtDiscount1_Leave(object sender, EventArgs e)
        {
            txtDiscRupees1.Focus();
            //txtDiscRupees.Focus();
            CalculateTotal();
        }

        private void txtChrg1_Leave(object sender, EventArgs e)
        {
            txtChrgRupees1.Focus();
            CalculateTotal();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SetValue();

        }




        private void SetValue()
        {
            try
            {
                //double debit = 0;
                //long temp = 0;
                if (ValidationsMain() == false) return;
                if (Convert.ToDouble(txtGrandTotal.Text) < 0)
                {
                    OMMessageBox.Show("Negative Bill amount not allowed.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    return;
                }
                for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                {
                    if (dgBill.Rows.Count <= 1)
                    {
                        OMMessageBox.Show("Atleast one item required.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        dgBill.CurrentCell = dgBill[ColIndex.ItemName, dgBill.Rows.Count - 1];
                        dgBill.Focus();
                        return;
                    }
                    else if (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value) == 0 && Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.FreeQty].Value) == 0)
                    {
                        DeleteZeroQty(i);
                        i--;
                    }
                }
                if (dgBill.Rows.Count <= 1)
                {
                    OMMessageBox.Show("Atleast one item required.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    dgBill.CurrentCell = dgBill[ColIndex.ItemName, dgBill.Rows.Count - 1];
                    dgBill.Focus();
                    return;
                }
                if (ID != 0)
                {
                    if (Convert.ToDouble(txtDiscRupees1.Text) == 0) DeleteDtls(2, GetVoucherPK("SrNo=" + Others.Discount1));

                    if (Convert.ToDouble(txtChrgRupees1.Text) == 0) DeleteDtls(2, GetVoucherPK("SrNo=" + Others.Charges1));
                    if (Convert.ToDouble(txtOtherTax.Text) == 0) DeleteDtls(2, GetVoucherPK("SrNo=" + Others.Charges2));

                    if (Convert.ToDouble(txtRoundOff.Text) == 0) DeleteDtls(2, GetVoucherPK("SrNo=" + Others.RoundOff));
                    if (Convert.ToDouble(txtTotalDisc.Text) == 0) DeleteDtls(2, GetVoucherPK("SrNo=" + Others.ItemDisc));
                    if (Convert.ToDouble(txtTotalItemDisc.Text) == 0) DeleteDtls(2, GetVoucherPK("SrNo=" + Others.BTaxItemDisc));
                }
                CalculateTotal();
                dbtOtherVoucherEntry = new DBTOtherVoucherEntry();

                DeleteValues();//Delete Old Values

                int VoucherSrNo = 1;
                //Voucher Header Entry 
                tOtherVoucherEntry = new TOtherVoucherEntry();
                tOtherVoucherEntry.PkOtherVoucherNo = ID;
                tOtherVoucherEntry.VoucherTypeCode = 1;
                tOtherVoucherEntry.VoucherUserNo = VoucherUserNo;
                tOtherVoucherEntry.VoucherDate = Convert.ToDateTime(dtpBillDate.Text);
                tOtherVoucherEntry.VoucherTime = dtpBillTime.Value;
                tOtherVoucherEntry.Narration = "Purchase Bill";
                tOtherVoucherEntry.Reference = txtRefNo.Text;
                tOtherVoucherEntry.ChequeNo = 0;
                tOtherVoucherEntry.ClearingDate = dtpBillDate.Value;
                tOtherVoucherEntry.CompanyNo = DBGetVal.CompanyNo;
                tOtherVoucherEntry.BilledAmount = Convert.ToDouble(txtGrandTotal.Text);
                tOtherVoucherEntry.ChallanNo = "";
                tOtherVoucherEntry.OrderType = 1;
                tOtherVoucherEntry.Remark = txtRemark.Text.Trim();
                tOtherVoucherEntry.MacNo = DBGetVal.MacNo;
                tOtherVoucherEntry.PayTypeNo = ObjFunction.GetComboValue(cmbPaymentType);
                tOtherVoucherEntry.RateTypeNo = GetRateType();
                tOtherVoucherEntry.TaxTypeNo = ObjFunction.GetComboValue(cmbTaxType);
                tOtherVoucherEntry.ReturnAmount = Convert.ToDouble(txtReturnAmt.Text.Trim());
                tOtherVoucherEntry.Visibility = Convert.ToDouble(txtVisibility.Text.Trim());
                tOtherVoucherEntry.UserID = DBGetVal.UserID;
                tOtherVoucherEntry.UserDate = DBGetVal.ServerTime.Date;
                tOtherVoucherEntry.SchemeDisc = Convert.ToDouble(txtTotalDisc.Text);
                tOtherVoucherEntry.DistDisc = Convert.ToDouble(txtDistDisc.Text);
                tOtherVoucherEntry.CashDisc = Convert.ToDouble(txtDiscRupees1.Text);
                tOtherVoucherEntry.Charges = Convert.ToDouble(txtChrgRupees1.Text);
                tOtherVoucherEntry.SubTotal = Convert.ToDouble(txtSubTotal.Text);
                tOtherVoucherEntry.TotalTax = Convert.ToDouble(txtTotalTax.Text);
                tOtherVoucherEntry.RoundOff = Convert.ToDouble(txtRoundOff.Text);
                tOtherVoucherEntry.OtherTax = Convert.ToDouble(txtOtherTax.Text);
                tOtherVoucherEntry.LedgerNo = ObjFunction.GetComboValue(cmbPartyName);
                dbtOtherVoucherEntry.AddTOtherVoucherEntry(tOtherVoucherEntry);




                for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                {

                    // Add record in RateSetting Table if MRP Changed
                    if (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.MRP].Value) != Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.TempMRP].Value))
                    {
                        MRateSetting3 mRateSetting = new MRateSetting3();
                        DataTable dtRateSetting = ObjFunction.GetDataView("select * from MRateSetting where PkSrNo=" + Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.PkRateSettingNo].Value) + "").Table;
                        mRateSetting.PkSrNo = 0;
                        mRateSetting.FkBcdSrNo = Convert.ToInt64(dtRateSetting.Rows[0].ItemArray[1].ToString());
                        mRateSetting.ItemNo = Convert.ToInt64(dtRateSetting.Rows[0].ItemArray[2].ToString());
                        mRateSetting.FromDate = DBGetVal.ServerTime;
                        mRateSetting.PurRate = Convert.ToDouble(dtRateSetting.Rows[0].ItemArray[4].ToString());
                        mRateSetting.MRP = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.MRP].Value);
                        mRateSetting.UOMNo = Convert.ToInt64(dtRateSetting.Rows[0].ItemArray[6].ToString());
                        mRateSetting.ASaleRate = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.MRP].Value);//Convert.ToDouble(dtRateSetting.Rows[0].ItemArray[7].ToString());
                        mRateSetting.BSaleRate = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.MRP].Value);//Convert.ToDouble(dtRateSetting.Rows[0].ItemArray[8].ToString());
                        mRateSetting.CSaleRate = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.MRP].Value);//Convert.ToDouble(dtRateSetting.Rows[0].ItemArray[9].ToString());
                        mRateSetting.DSaleRate = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.MRP].Value);//Convert.ToDouble(dtRateSetting.Rows[0].ItemArray[10].ToString());
                        mRateSetting.ESaleRate = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.MRP].Value);//Convert.ToDouble(dtRateSetting.Rows[0].ItemArray[11].ToString());
                        mRateSetting.StockConversion = Convert.ToDouble(dtRateSetting.Rows[0].ItemArray[12].ToString());
                        mRateSetting.PerOfRateVariation = Convert.ToDouble(dtRateSetting.Rows[0].ItemArray[13].ToString());
                        mRateSetting.MKTQty = Convert.ToInt64(dtRateSetting.Rows[0].ItemArray[14].ToString());
                        mRateSetting.IsActive = true;
                        mRateSetting.UserID = DBGetVal.UserID;
                        mRateSetting.UserDate = DBGetVal.ServerTime.Date;
                        mRateSetting.CompanyNo = Convert.ToInt64(dtRateSetting.Rows[0].ItemArray[19].ToString());
                        dbtOtherVoucherEntry.AddMRateSetting3(mRateSetting);
                    }

                    // Add record in TStock Table
                    tOtherStock = new TOtherStock();
                    if (Convert.ToInt64(dgBill[ColIndex.PkStockTrnNo, i].Value) == 0)
                    {
                        tOtherStock.PkOtherStockTrnNo = 0;
                    }
                    else
                    {
                        tOtherStock.PkOtherStockTrnNo = Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.PkStockTrnNo].Value);
                    }

                    tOtherStock.GroupNo = GroupType.CapitalAccount;
                    tOtherStock.ItemNo = Convert.ToInt64(dgBill[ColIndex.ItemNo, i].Value.ToString());
                    tOtherStock.FkVoucherSrNo = VoucherSrNo;
                    tOtherStock.TrnCode = 1;
                    tOtherStock.Quantity = Convert.ToDouble(dgBill[ColIndex.Quantity, i].Value.ToString());
                    tOtherStock.BilledQuantity = Convert.ToDouble(dgBill[ColIndex.ActualQty, i].Value.ToString());
                    tOtherStock.Rate = Convert.ToDouble(dgBill[ColIndex.Rate, i].Value.ToString());
                    tOtherStock.Amount = Convert.ToDouble(dgBill[ColIndex.Amount, i].Value.ToString());
                    tOtherStock.TaxPercentage = Convert.ToDouble(dgBill[ColIndex.TaxPercentage, i].Value.ToString());
                    tOtherStock.TaxAmount = Convert.ToDouble(dgBill[ColIndex.TaxAmount, i].Value.ToString());
                    tOtherStock.DiscPercentage = Convert.ToDouble(dgBill[ColIndex.DiscPercentage, i].Value.ToString());
                    tOtherStock.DiscAmount = Convert.ToDouble(dgBill[ColIndex.DiscAmount, i].Value.ToString());
                    tOtherStock.DiscRupees = Convert.ToDouble(dgBill[ColIndex.DiscRupees, i].Value.ToString());
                    tOtherStock.DiscPercentage2 = Convert.ToDouble(dgBill[ColIndex.DiscPercentage2, i].Value.ToString());
                    tOtherStock.DiscAmount2 = Convert.ToDouble(dgBill[ColIndex.DiscAmount2, i].Value.ToString());
                    tOtherStock.DiscRupees2 = Convert.ToDouble(dgBill[ColIndex.DiscRupees2, i].Value.ToString());
                    tOtherStock.NetRate = Convert.ToDouble(dgBill[ColIndex.NetRate, i].Value.ToString());
                    tOtherStock.NetAmount = Convert.ToDouble(dgBill[ColIndex.NetAmt, i].Value.ToString());
                    tOtherStock.FkStockBarCodeNo = Convert.ToInt64(dgBill[ColIndex.PkBarCodeNo, i].Value.ToString());
                    tOtherStock.FkUomNo = Convert.ToInt64(dgBill[ColIndex.UOMNo, i].Value.ToString());
                    tOtherStock.FkRateSettingNo = Convert.ToInt64(dgBill[ColIndex.PkRateSettingNo, i].Value.ToString());
                    tOtherStock.FkItemTaxInfo = Convert.ToInt64(dgBill[ColIndex.PkItemTaxInfo, i].Value.ToString());
                    tOtherStock.FreeQty = Convert.ToDouble(dgBill[ColIndex.FreeQty, i].Value.ToString());
                    tOtherStock.FreeUOMNo = 1;//Convert.ToInt64(dgBill[ColIndex.FreeUomNo, i].Value.ToString());
                    tOtherStock.UserID = DBGetVal.UserID;
                    tOtherStock.UserDate = DBGetVal.ServerTime.Date;
                    tOtherStock.CompanyNo = Convert.ToInt64(dgBill[ColIndex.StockCompanyNo, i].Value.ToString());
                    tOtherStock.LandedRate = Convert.ToDouble(dgBill[ColIndex.LandedRate, i].Value.ToString());
                    tOtherStock.BalanceQty = Convert.ToDouble(dgBill[ColIndex.BalanceQty, i].Value.ToString());
                    if (ID == 0)
                        tOtherStock.BalanceQty = tOtherStock.Quantity;
                    else
                    {
                        if (tOtherStock.PkOtherStockTrnNo == 0)
                            tOtherStock.BalanceQty = tOtherStock.Quantity;
                        else
                        {
                            double ActUseQty = ObjQry.ReturnDouble("Select sum(Quantity) From TStockDetails Where FKOtherStockTrnNo=" + tOtherStock.PkOtherStockTrnNo + "", CommonFunctions.ConStr);
                            double BalQty = tOtherStock.Quantity - (tOtherStock.BalanceQty + ActUseQty);
                            tOtherStock.BalanceQty = tOtherStock.BalanceQty + BalQty;
                        }
                    }
                    dbtOtherVoucherEntry.AddTOtherStock(tOtherStock);
                }

                long tempID = dbtOtherVoucherEntry.ExecuteNonQueryStatements();
                if (tempID != 0)
                {
                    if (Convert.ToDouble(dgPayType.Rows[1].Cells[2].Value) > 0 || Convert.ToDouble(dgPayType.Rows[3].Cells[2].Value) > 0 || Convert.ToDouble(dgPayType.Rows[4].Cells[2].Value) > 0)
                    {

                    }

                    string strVChNo = ObjQry.ReturnLong("Select VoucherUserNo From TOtherVoucherEntry Where PKOtherVoucherNo=" + tempID + "", CommonFunctions.ConStr).ToString();
                    OMMessageBox.Show("Bill No " + strVChNo + " Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                    if (ID == 0)
                    {
                        //dtSearch = ObjFunction.GetDataView("Select PkVoucherNo from TVoucherEntry Where VoucherTypeCode=" + VoucherType + " AND CompanyNo=" + DBGetVal.CompanyNo + "").Table;
                        DataRow drSearch = dtSearch.NewRow();
                        drSearch[0] = tempID;
                        dtSearch.Rows.Add(drSearch);
                        ID = tempID;
                        if (cmbPaymentType.SelectedValue.ToString() == "3" && MixModeFlag == true)
                        {
                            btnMixMode_Click(null, null);
                        }
                        PartyNo = 0; PayType = 0;
                        SetNavigation();
                        FillControls();
                    }
                    else
                    {
                        FillControls();
                    }
                    ObjFunction.FillCombo(cmbPartyNameSearch, " SELECT DISTINCT MLedger.LedgerNo, MLedger.LedgerName FROM  TOtherVoucherEntry INNER JOIN  MLedger ON TOtherVoucherEntry.LedgerNo = MLedger.LedgerNo  " +
                            " where TOtherVoucherEntry.VoucherTypeCode = " + VoucherType + " ORDER BY MLedger.LedgerName ");
                    setDisplay(true);
                    ObjFunction.LockButtons(true, this.Controls);
                    ObjFunction.LockControls(false, this.Controls);
                    dgBill.Enabled = false;
                    MixModeFlag = false;
                    btnNew.Focus();
                }
                else
                {
                    OMMessageBox.Show("Bill Not Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }




        private bool ValidationsMain()
        {
            bool flag = false;
            try
            {
                ObjFunction.GetFinancialYear(dtpBillDate.Value, out dtFrom, out dtTo);

                if (txtRefNo.Text.Trim() == "")
                {
                    OMMessageBox.Show("Enter Invoice No.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    txtRefNo.Focus();
                }
                else if (ObjFunction.GetComboValue(cmbPartyName) <= 0)
                {
                    OMMessageBox.Show("Please Select Party Name", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    cmbPartyName.Focus();
                }
                else if (ObjFunction.GetComboValueString(cmbRateType) == "")
                {
                    OMMessageBox.Show("Please Select Rate Type", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    cmbRateType.Focus();
                }
                else if (ObjFunction.GetComboValue(cmbTaxType) <= 0)
                {
                    OMMessageBox.Show("Please Select Tax Type", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    cmbTaxType.Focus();
                }
                else if (ObjFunction.GetComboValue(cmbPaymentType) <= 0)
                {
                    OMMessageBox.Show("Please Select Payment Type", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    cmbTaxType.Focus();
                }
                else if (ObjQry.ReturnLong(" Select Count(*) FROM  TOtherVoucherEntry " +
                                            " where  TOtherVoucherEntry.Reference ='" + txtRefNo.Text.Trim() + "' and TOtherVoucherEntry.VoucherTypeCode=" + VoucherType + " " +
                                            " and TOtherVoucherEntry.LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " " +
                                            " AND TOtherVoucherEntry.VoucherDate>='" + dtFrom.Date + "' AND TOtherVoucherEntry.VoucherDate<='" + dtTo.Date + "' " +
                                            " and TOtherVoucherEntry.PkOtherVoucherNo!=" + ID + "", CommonFunctions.ConStr) > 0)//TVoucherEntry.VoucherDate='" + Convert.ToDateTime(dtpBillDate.Text).ToString("
                {
                    OMMessageBox.Show("This Invoice is already exist ....", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    cmbPartyName.Focus();
                }
                else flag = true;

                if (flag == true)
                {
                    for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                    {
                        DataGridViewRow dr = dgBill.Rows[i];
                        if (dr.Cells[ColIndex.PkBarCodeNo].Value == null && dr.Cells[ColIndex.ItemNo].Value == null && dr.Cells[ColIndex.TaxLedgerNo].Value == null && dr.Cells[ColIndex.SalesLedgerNo].Value == null && dr.Cells[ColIndex.PkRateSettingNo].Value == null && dr.Cells[ColIndex.PkItemTaxInfo].Value == null)
                        {
                            flag = false;
                            OMMessageBox.Show("Please Fill properly Sr No. " + (i + 1) + " of item..", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            break;
                        }
                        else if (dgBill.Rows[i].Cells[ColIndex.PkBarCodeNo].Value.ToString() == "0")
                        {
                            flag = false;
                            OMMessageBox.Show("Please Fill properly Sr No. " + (i + 1) + " of item..", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            break;
                        }
                    }
                }

                return flag;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return false;
            }
        }

        private bool Validations()
        {
            bool flag = true;
            if (txtDiscRupees1.Text == "")
            {
                txtDiscRupees1.Text = "0.00";
                flag = true;
            }
            else if (ObjFunction.CheckValidAmount(txtDiscRupees1.Text) == false)
            {
                txtDiscRupees1.Text = "0.00";
                flag = true;
            }
            if (txtChrgRupees1.Text == "")
            {
                txtChrgRupees1.Text = "0.00";
                flag = true;
            }
            if (txtOtherTax.Text == "")
            {
                txtOtherTax.Text = "0.00";
                flag = true;
            }
            if (txtReturnAmt.Text == "")
            {
                txtReturnAmt.Text = "0.00";
                flag = true;
            }
            if (txtVisibility.Text == "")
            {
                txtVisibility.Text = "0.00";
                flag = true;
            }
            else if (ObjFunction.CheckValidAmount(txtChrgRupees1.Text) == false)
            {
                txtChrgRupees1.Text = "0.00";
                flag = true;
            }
            else if (ObjFunction.CheckValidAmount(txtOtherTax.Text) == false)
            {
                txtOtherTax.Text = "0.00";
                flag = true;
            }
            if (txtRemark.Text.Trim() == "")
            {
                txtRemark.Text = "Purchase Order";
            }

            return flag;
        }

        private void Control_Leave(object sender, EventArgs e)
        {
            try
            {
                double TotalAmt = 0;
                TotalAmt = ((Convert.ToDouble(txtSubTotal.Text) + Convert.ToDouble(txtTotalTax.Text)) - (Convert.ToDouble(txtTotalDisc.Text) + Convert.ToDouble(txtTotalItemDisc.Text)));
                //if (((TextBox)sender).Name == "txtDiscount1")
                //{
                //    if (ObjFunction.CheckValidAmount(((TextBox)sender).Text) == false)
                //    {
                //        OMMessageBox.Show("Enter Discount Value.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                //        ((TextBox)sender).Focus();
                //    }
                //    else
                //    {
                //        txtChrgRupees1.Focus();
                //        CalculateTotal();
                //    }
                //}
                if (((TextBox)sender).Name == "txtDiscRupees1")
                {
                    if (ObjFunction.CheckValidAmount(((TextBox)sender).Text) == false)
                    {
                        OMMessageBox.Show("Enter Discount Value.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        ((TextBox)sender).Focus();
                    }
                    else
                    {
                        //txtDiscount1.Text = Convert.ToDouble((100 * Convert.ToDouble(txtDiscRupees1.Text)) / TotalAmt).ToString("0.00");
                        //txtChrgRupees1.Focus();
                        CalculateTotal();
                    }
                }

                else if (((TextBox)sender).Name == "txtChrgRupees1")
                {
                    if (ObjFunction.CheckValidAmount(((TextBox)sender).Text) == false)
                    {
                        OMMessageBox.Show("Enter Charges Value.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        ((TextBox)sender).Focus();
                    }
                    else
                    {
                        //txtReturnAmt.Focus();
                        CalculateTotal();
                    }
                }
                else if (((TextBox)sender).Name == "txtReturnAmt")
                {
                    if (ObjFunction.CheckValidAmount(((TextBox)sender).Text) == false)
                    {
                        OMMessageBox.Show("Enter Valid Return Amount.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        ((TextBox)sender).Focus();
                    }
                    else
                    {
                        //txtVisibility.Focus();
                        //CalculateTotal();
                    }
                }
                else if (((TextBox)sender).Name == "txtVisibility")
                {
                    if (ObjFunction.CheckValidAmount(((TextBox)sender).Text) == false)
                    {
                        OMMessageBox.Show("Enter Valid Visility Amount.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        ((TextBox)sender).Focus();
                    }
                    else
                    {
                        //txtRemark.Focus();
                        //CalculateTotal();
                    }
                }
                else if (((TextBox)sender).Name == "txtOtherTax")
                {
                    if (ObjFunction.CheckValidAmount(((TextBox)sender).Text) == false)
                    {
                        OMMessageBox.Show("Enter Other Tax Value.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        ((TextBox)sender).Focus();
                    }
                    else
                    {
                        CalculateTotal();
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }


        }

        #region dgBill Methods and Events
        private delegate void MovetoNext(int RowIndex, int ColIndex, DataGridView dg);

        private void m2n(int RowIndex, int ColIndex, DataGridView dg)
        {
            dg.CurrentCell = dg.Rows[RowIndex].Cells[ColIndex];
        }

        private void Desc_Start()
        {
            try
            {
                if (dgBill.CurrentCell.Value == null || Convert.ToString(dgBill.CurrentCell.Value) == "")
                {
                    ItemType = 1;
                    FillItemList(0, ItemType); //FillItemList();
                }
                else
                {
                    ItemType = 2;
                    long[] BarcodeNo = null; long[] ItemNo = null;

                    switch (dgBill.CurrentCell.Value.ToString().Trim())
                    {
                        case "SV":
                            {
                                if (btnSave.Visible)
                                {
                                    btnSave_Click(btnSave, null);
                                    return;
                                }
                                break;
                            }
                        default:
                            {
                                SearchBarcode(dgBill.CurrentCell.Value.ToString().Trim(), out ItemNo, out BarcodeNo);
                                dgBill.CurrentRow.Cells[ColIndex.PkRateSettingNo].Value = 0;
                                break;
                            }
                    }

                    if (ItemNo.Length == 0 || BarcodeNo.Length == 0)
                    {
                        string strB = dgBill.CurrentCell.FormattedValue.ToString();
                        dgBill.CurrentCell.Value = null;
                        dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkBarCodeNo].Value = "0";
                        //DisplayMessage("Barcode Not Found");
                        if (OMMessageBox.Show("Barcode Not Found.\nPRESS ESCAPE TO CONTINUE....", "Information", OMMessageBoxButton.OK, OMMessageBoxIcon.Information) == DialogResult.OK)
                        {
                            MovetoNext move2n = new MovetoNext(m2n);
                            BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex, ColIndex.ItemName, dgBill });
                        }
                        else
                            NewItemAdd(strB);
                    }
                    else
                    {
                        if (ItemNo.Length > 1)
                        {
                            ItemType = 3;
                            FillItemList(0, ItemType);//FillItemList();
                        }
                        else
                        {
                            dgBill.CurrentRow.Cells[ColIndex.Barcode].Value = dgBill.CurrentCell.Value;
                            Desc_MoveNext(ItemNo[0], BarcodeNo[0]);
                        }
                    }
                    //BindGrid();
                    //CalculateTotal();
                }

                //from key_down
                //ItemType = 1;
                //FillItemList();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void SearchBarcode(String strBarcode, out long[] ItemNo, out long[] BarcodeNo)
        {
            string sql = "";
            DataTable dt = new DataTable();
            if (chkFilterItem.Checked == false)
                sql = "SELECT     MStockBarcode.PkStockBarcodeNo, MStockBarcode.ItemNo,MStockBarcode.Barcode FROM MStockBarcode INNER JOIN MStockItems ON MStockBarcode.ItemNo = MStockItems.ItemNo " +
                        " INNER JOIN MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo " +
                        " where (MStockBarcode.Barcode = '" + strBarcode + "' or MStockItems.ShortCode = '" + strBarcode + "') And MStockItems.IsActive ='true' AND MStockItems.CompanyNo=" + DBGetVal.CompanyNo + " AND (MRateSetting.IsActive='true') and (MStockItems.FkStockGroupTypeNo<>3)";
            else
                sql = "SELECT     MStockBarcode.PkStockBarcodeNo, MStockBarcode.ItemNo,MStockBarcode.Barcode FROM MStockBarcode " +
                        " INNER JOIN MStockItems ON MStockBarcode.ItemNo = MStockItems.ItemNo " +
                        " INNER JOIN MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo " +
                        " INNER JOIN MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo " +
                        " INNER JOIN MManufacturerDetails ON MStockGroup.MfgCompNo = MManufacturerDetails.ManufacturerNo AND MManufacturerDetails.LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + " " +
                        " where (MStockBarcode.Barcode = '" + strBarcode + "' or MStockItems.ShortCode = '" + strBarcode + "') And MStockItems.IsActive ='true' " +
                        " AND MStockItems.CompanyNo=" + DBGetVal.CompanyNo + " AND (MRateSetting.IsActive='true') and (MStockItems.FkStockGroupTypeNo<>3)";
            //sql = "SELECT     MStockBarcode.PkStockBarcodeNo, MStockBarcode.ItemNo,MStockBarcode.Barcode FROM MStockBarcode INNER JOIN MStockItems ON MStockBarcode.ItemNo = MStockItems.ItemNo " +
            //" INNER JOIN MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo " +
            //    " where (MStockBarcode.Barcode = '" + strBarcode + "' or MStockItems.ShortCode = '" + strBarcode + "') And MStockItems.IsActive ='true' AND MStockItems.CompanyNo=" + DBGetVal.CompanyNo + " AND (MRateSetting.IsActive='true') and (MStockItems.FkStockGroupTypeNo<>3)" +
            //    " AND MStockItems.GroupNo in (SELECT MStockGroup.StockGroupNo FROM MManufacturerDetails INNER JOIN MStockGroup ON MManufacturerDetails.ManufacturerNo = MStockGroup.MfgCompNo WHERE (MManufacturerDetails.LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + ") AND (MStockGroup.ControlGroup = 3))";

            dt = ObjFunction.GetDataView(sql).Table;
            BarcodeNo = new long[dt.Rows.Count];
            ItemNo = new long[dt.Rows.Count];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    BarcodeNo[i] = Convert.ToInt64(dt.Rows[i].ItemArray[0].ToString());
                    ItemNo[i] = Convert.ToInt64(dt.Rows[i].ItemArray[1].ToString());
                    dgBill.CurrentCell.Value = dt.Rows[i].ItemArray[2].ToString();
                }
            }
            else
            {
                //sql = "SELECT     MStockBarcode.PkStockBarcodeNo, MStockBarcode.ItemNo,MStockBarcode.Barcode FROM MStockBarcode INNER JOIN MStockItems ON MStockBarcode.ItemNo = MStockItems.ItemNo " +
                //                    " where MStockItems.ShortCode = '" + strBarcode + "' And MStockItems.IsActive ='true' AND MStockItems.CompanyNo=" + DBGetVal.CompanyNo + "";
                //dt = ObjFunction.GetDataView(sql).Table;
                //BarcodeNo = new long[dt.Rows.Count];
                //ItemNo = new long[dt.Rows.Count];
                //if (dt.Rows.Count > 0)
                //{
                //    for (int i = 0; i < dt.Rows.Count; i++)
                //    {
                //        BarcodeNo[i] = Convert.ToInt64(dt.Rows[i].ItemArray[0].ToString());
                //        ItemNo[i] = Convert.ToInt64(dt.Rows[i].ItemArray[1].ToString());
                //        dgBill.CurrentCell.Value = dt.Rows[i].ItemArray[2].ToString();
                //    }
                //}
                //ItemNo[0] = 0;
                //BarcodeNo[0] = 0;
            }
        }

        private void Desc_MoveNext(long ItemNo, long BarcodeNo)
        {
            try
            {
                dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemNo].Value = ItemNo;
                dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkBarCodeNo].Value = BarcodeNo;

                DataTable dtItem = ObjFunction.GetDataView("Select ItemName,CompanyNo from MStockItems_V(NULL,NULL,NULL,NULL,NULL,NULL,NULL) where ItemNo = " + ItemNo + " AND IsActive='true'").Table;
                dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemName].Value = dtItem.Rows[0].ItemArray[0].ToString();
                dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.StockCompanyNo].Value = dtItem.Rows[0].ItemArray[1].ToString();
                //dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemName].Value = ObjQry.ReturnString("Select ItemName from MStockItems_V(NULL,NULL) where ItemNo = " + ItemNo,CommonFunctions.ConStr);

                if (ItemType == 2)
                    dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemName].Value += " - " + dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.Barcode].Value.ToString();

                if (StopOnQty == true)
                {
                    if (dgBill[2, dgBill.CurrentCell.RowIndex].Value == null)
                    {
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex, ColIndex.Quantity, dgBill });
                        //   dgBill.CurrentCell = dgBill[2, dgBill.CurrentCell.RowIndex];
                        dgBill.Focus();
                    }
                    else
                        Qty_MoveNext();
                }
                else
                {
                    dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.Quantity].Value = Convert.ToDouble(1).ToString("0.00");
                    Qty_MoveNext();
                }

                //BindGrid();
                //CalculateTotal();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void Qty_MoveNext()
        {
            try
            {
                rowQtyIndex = dgBill.CurrentCell.RowIndex;

                MovetoNext move2n = new MovetoNext(m2n);
                BeginInvoke(move2n, new object[] { rowQtyIndex, 3, dgBill });

                UOM_Start();

                //CalculateTotal();//temp

                // dgBill.Rows[rowQtyIndex].Cells[2].ReadOnly = true;

                //found in the dgBill_keydown
                //if (dgBill.CurrentCell.ColumnIndex == 2)
                //{
                //    if (dgBill.Rows.Count > 1)
                //    {
                //        row = (dgBill.CurrentCell.RowIndex == 0) ? 0 : dgBill.CurrentCell.RowIndex;
                //        if (Convert.ToString(dgBill.Rows[row].Cells[2].Value) != "")
                //        {
                //            dgBill.CurrentCell = dgBill[3, row];
                //            dgBill.CurrentCell.ReadOnly = false;
                //        }
                //    }
                //}
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

        }

        private void UOM_Start()
        {
            try
            {
                int row = 0;
                if (dgBill.CurrentCell.RowIndex == 0)
                    row = dgBill.CurrentCell.RowIndex;
                else
                    row = dgBill.CurrentCell.RowIndex;
                dgBill.CurrentCell = dgBill[3, row];

                //dgBill.CurrentCell.ReadOnly = false;
                FillUOMList(row);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void UOM_MoveNext()
        {
            try
            {
                int Row = dgBill.CurrentCell.RowIndex;

                if (dgBill.CurrentRow.Cells[ColIndex.UOMNo].Value != null &&
                    dgBill.CurrentRow.Cells[ColIndex.UOMNo].Value.ToString() != lstUOM.SelectedValue.ToString())
                {
                    dgBill.CurrentRow.Cells[ColIndex.Rate].Value = "0.00";//lstRate.Text;
                    dgBill.CurrentRow.Cells[ColIndex.PkRateSettingNo].Value = 0;//lstRate.SelectedValue;
                }

                dgBill.CurrentRow.Cells[ColIndex.UOM].Value = lstUOM.Text;
                dgBill.CurrentRow.Cells[ColIndex.UOMNo].Value = Convert.ToInt64(lstUOM.SelectedValue);
                pnlUOM.Visible = false;

                Rate_Start();
                //CalculateTotal();//temp
                //CalculateGridValues(Row);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void Rate_Start()
        {
            try
            {
                string str;
                //CalculateGridValues();
                int RowIndex = dgBill.CurrentCell.RowIndex;
                long ItemNo = Convert.ToInt64(dgBill.Rows[RowIndex].Cells[ColIndex.ItemNo].Value);
                long BarcodeNo = Convert.ToInt64(dgBill.Rows[RowIndex].Cells[ColIndex.PkBarCodeNo].Value);
                long UOMNo = Convert.ToInt64(dgBill.Rows[RowIndex].Cells[ColIndex.UOMNo].Value);
                double Qty = Convert.ToDouble(dgBill.Rows[RowIndex].Cells[ColIndex.Quantity].Value);
                dgBill.Rows[RowIndex].Cells[ColIndex.BarcodePrint].Value = "Print";
                if (dgBill.Rows[RowIndex].Cells[ColIndex.PkRateSettingNo].Value == null ||
                    Convert.ToInt64(dgBill.Rows[RowIndex].Cells[ColIndex.PkRateSettingNo].Value) == 0)
                {
                    ObjFunction.FillList(lstRate, "pksrno", ObjFunction.GetComboValueString(cmbRateType));
                    if (ItemType == 2)
                    {
                        str = "select pksrno," + ObjFunction.GetComboValueString(cmbRateType) +
                            " from GetItemRateAll(" + ItemNo + "," + BarcodeNo + "," + UOMNo + ",null ,null,null)";
                    }
                    else
                    {
                        str = "select pksrno," + ObjFunction.GetComboValueString(cmbRateType) +
                            " from GetItemRateAll(" + ItemNo + ",null," + UOMNo + ",null ,null,null)";
                    }
                    str = str.Replace("PurRate", "Isnull((Select Rate FRom Tstock Where PKStockTrnNo in (Select Max(PKStockTrnNo) FRom Tstock,TVoucherEntry Where TVoucherEntry.PKVoucherNo=TStock.FKVoucherNo AND TVoucherEntry.VoucherTypeCode=" + VchType.Purchase + " AND FKRateSettingNo in(PkSrNo))),0) AS PurRate");
                    ObjFunction.FillList(lstRate, str);

                    if (lstRate.Items.Count == 1)
                    {
                        lstRate.SelectedIndex = 0;
                        dgBill.Rows[RowIndex].Cells[ColIndex.Rate].Value = lstRate.Text;
                        dgBill.Rows[RowIndex].Cells[ColIndex.PkRateSettingNo].Value = lstRate.SelectedValue;

                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { RowIndex, ColIndex.Rate, dgBill });
                        // dgBill.CurrentCell = dgBill[ColIndex.Rate, RowIndex];
                        dgBill.Focus();
                        //BindGrid(dgBill.CurrentRow.Index);

                        if (StopOnRate == false)
                        {
                            dgBill.CurrentCell = dgBill[ColIndex.Rate, RowIndex];
                            Rate_MoveNext();
                        }
                        else
                        {
                            BindGrid(dgBill.CurrentRow.Index);
                        }
                    }
                    else if (lstRate.Items.Count > 1)
                    {
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { RowIndex, ColIndex.Rate, dgBill });
                        dgBill.CurrentCell = dgBill[ColIndex.Rate, RowIndex];
                        dgBill.Focus();

                        CalculateTotal();
                        pnlRate.Visible = true;
                        lstRate.Focus();
                    }
                    else
                    {
                        //error invalid Qty or UOM
                    }
                }
                else
                {
                    MovetoNext move2n = new MovetoNext(m2n);
                    BeginInvoke(move2n, new object[] { RowIndex, ColIndex.Rate, dgBill });
                    // dgBill.CurrentCell = dgBill[ColIndex.Rate, RowIndex];
                    dgBill.Focus();
                    //BindGrid(dgBill.CurrentRow.Index);

                    if (StopOnRate == false)
                    {
                        dgBill.CurrentCell = dgBill[ColIndex.Rate, RowIndex];
                        Rate_MoveNext();
                    }
                    else
                    {
                        BindGrid(dgBill.CurrentRow.Index);
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FreeQty_MoveNext()
        {
            try
            {
                if (dgBill.CurrentCell.Value != null && dgBill.CurrentCell.Value.ToString() != "")
                {
                    if (ObjFunction.CheckValidAmount(dgBill.CurrentCell.Value.ToString()) == true)
                    {

                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex, ColIndex.DiscPercentage, dgBill });
                        //dgBill.CurrentCell = dgBill[1, dgBill.Rows.Count - 1];
                        dgBill.Focus();

                        CalculateTotal();
                    }
                    else
                    {
                        dgBill.CurrentCell.ErrorText = "Please Enter Valid Free Quantity...";
                    }
                }
                else
                {
                    dgBill.CurrentCell.Value = "0";
                }
            }

            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void Rate_MoveNext()
        {
            try
            {
                if (dgBill.CurrentCell.Value != null)
                {
                    if (ObjFunction.CheckValidAmount(dgBill.CurrentCell.Value.ToString()) == true)
                    {
                        //dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.Amount].Value = (Convert.ToDouble(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.Rate].Value) * Convert.ToDouble(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[2].Value)) / Convert.ToDouble(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.MKTQuantity].Value);
                        //dgBill.CurrentCell.ReadOnly = true;
                        BindGrid(dgBill.CurrentCell.RowIndex);

                        if (dgBill.Columns[ColIndex.FreeQty].Visible == true)
                        {
                            MovetoNext move2n = new MovetoNext(m2n);
                            BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex, ColIndex.FreeQty, dgBill });
                            dgBill.CurrentCell = dgBill[ColIndex.FreeQty, dgBill.CurrentCell.RowIndex];
                        }
                        else
                        {
                            MovetoNext move2n = new MovetoNext(m2n);
                            BeginInvoke(move2n, new object[] { dgBill.Rows.Count - 1, 1, dgBill });
                            dgBill.CurrentCell = dgBill[1, dgBill.Rows.Count - 1];
                        }
                        dgBill.Focus();

                        //CalculateTotal();
                    }
                    else
                    {
                        dgBill.CurrentCell.ErrorText = "Please Enter valid rate...";
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void Disc_MoveNext()
        {
            //Rate_MoveNext();
            CalculateTotal();
        }

        private void delete_row()
        {
            try
            {
                bool flag;
                if (dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkStockTrnNo].Value != null)
                {
                    if (OMMessageBox.Show("Are you sure want to delete this item ?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question, OMMessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        long PKStockTrnNo = Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkStockTrnNo].Value);
                        if (PKStockTrnNo != 0)
                        {
                            DeleteDtls(1, PKStockTrnNo);
                            DataTable dtB = dtBillCollect[dgBill.CurrentCell.RowIndex];
                            for (int row = 0; row < dtB.Rows.Count; row++)
                            {
                                dtB.Rows[0][2] = "0";
                                dtB.AcceptChanges();
                            }
                            ////For Purchase LedgerNo
                            //flag = false;
                            //for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                            //{
                            //    if (dgBill.CurrentCell.RowIndex != i)
                            //    {
                            //        if (Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.StockCompanyNo].Value) == Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.StockCompanyNo].Value))
                            //        { flag = true; break; }
                            //    }
                            //}
                            //if (flag == false)
                            //    DeleteDtls(3, Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.StockCompanyNo].Value));

                            //For Party LedgerNo
                            flag = false;
                            for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                            {
                                if (dgBill.CurrentCell.RowIndex != i)
                                {
                                    if (Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkVoucherNo].Value) == Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.PkVoucherNo].Value))
                                    { flag = true; break; }
                                }
                            }
                            if (flag == false)
                                DeleteDtls(2, Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkVoucherNo].Value));

                            //For Purchase LedgerNo
                            flag = false;
                            for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                            {
                                if (dgBill.CurrentCell.RowIndex != i)
                                {
                                    if (Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.SalesVchNo].Value) == Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.SalesVchNo].Value))
                                    { flag = true; break; }
                                }
                            }
                            if (flag == false)
                                DeleteDtls(2, Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.SalesVchNo].Value));

                            //FOr TaxLedgerNo
                            flag = false;
                            for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                            {
                                if (dgBill.CurrentCell.RowIndex != i)
                                {
                                    if (Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.TaxVchNo].Value) == Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.TaxVchNo].Value))
                                    { flag = true; break; }
                                }
                            }
                            if (flag == false)
                                DeleteDtls(2, Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.TaxVchNo].Value));

                            DataTable dt = dtBillCollect[dgBill.CurrentCell.RowIndex];
                            for (int row = 0; row < dt.Rows.Count; row++)
                            {
                                if (Convert.ToInt64(dt.Rows[row].ItemArray[4].ToString()) != 0 && Convert.ToDouble(dt.Rows[row].ItemArray[2].ToString()) == 0)
                                {
                                    DeleteDtls(4, Convert.ToInt64(dt.Rows[row].ItemArray[4].ToString()));
                                }
                            }
                        }

                        if (dgBill.Rows.Count - 1 == dgBill.CurrentCell.RowIndex)
                        {
                            dgBill.Rows.RemoveAt(dgBill.CurrentCell.RowIndex);
                            dgBill.Rows.Add();
                        }
                        else
                            dgBill.Rows.RemoveAt(dgBill.CurrentCell.RowIndex);
                        dtBillCollect.RemoveAt(dgBill.CurrentCell.RowIndex);

                        CalculateTotal();

                        dgBill.CurrentCell = dgBill[2, dgBill.Rows.Count - 1];
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgBill_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (Spaceflag == false) { Spaceflag = true; return; }
                if (dgBill.CurrentCell.ColumnIndex == ColIndex.ItemName)
                {
                    isDisc1PercentChanged = true;
                    isDisc2PercentChanged = true;
                    dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemNo].Value = 0;
                    dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkRateSettingNo].Value = 0;

                    Desc_Start();
                    if (dgBill.CurrentCell.Value == null)
                    {
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { e.RowIndex, ColIndex.ItemName, dgBill });
                    }
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.Quantity)
                {
                    isDisc1PercentChanged = true;
                    isDisc2PercentChanged = true;
                    Qty_MoveNext();
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.FreeQty)
                {
                    isDisc1PercentChanged = true;
                    isDisc2PercentChanged = true;
                    FreeQty_MoveNext();
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.UOM)
                {
                    isDisc1PercentChanged = true;
                    isDisc2PercentChanged = true;
                    UOM_Start();
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.Rate)
                {
                    isDisc1PercentChanged = true;
                    isDisc2PercentChanged = true;
                    Rate_MoveNext();
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscPercentage)
                {
                    isDisc1PercentChanged = true;
                    isDisc2PercentChanged = true;
                    Disc_MoveNext();
                    if (dgBill.Columns[ColIndex.DiscAmount].Visible == true)
                    {
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { e.RowIndex, ColIndex.DiscAmount, dgBill });
                    }
                    else
                    {
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { e.RowIndex, ColIndex.DiscPercentage2, dgBill });
                    }
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscAmount)
                {
                    isDisc2PercentChanged = true;
                    //double amt = Convert.ToDouble((Convert.ToDouble(dgBill.CurrentCell.Value) * 100) / (Convert.ToDouble(dgBill.Rows[e.RowIndex].Cells[ColIndex.Quantity].Value) * Convert.ToDouble(dgBill.Rows[e.RowIndex].Cells[ColIndex.Rate].Value)));
                    //dgBill.Rows[e.RowIndex].Cells[ColIndex.DiscPercentage].Value = amt;
                    ////dgBill.Rows[e.RowIndex].Cells[ColIndex.Amount].Value = Convert.ToDouble(dgBill.Rows[e.RowIndex].Cells[ColIndex.Amount].Value) - Convert.ToDouble(dgBill.Rows[e.RowIndex].Cells[ColIndex.DiscAmount].Value);
                    CalculateTotal();
                    MovetoNext move2n = new MovetoNext(m2n);
                    BeginInvoke(move2n, new object[] { e.RowIndex, ColIndex.DiscPercentage2, dgBill });
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscRupees)
                {
                    Disc_MoveNext();
                    if (dgBill.Columns[ColIndex.DiscPercentage2].Visible == true)
                    {
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { e.RowIndex, ColIndex.DiscPercentage2, dgBill });
                    }
                    else
                    {
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { e.RowIndex + 1, ColIndex.ItemName, dgBill });
                    }
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscPercentage2)
                {
                    isDisc1PercentChanged = true;
                    isDisc2PercentChanged = true;
                    Disc_MoveNext();
                    if (dgBill.Columns[ColIndex.DiscAmount2].Visible == true)
                    {
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { e.RowIndex, ColIndex.DiscAmount2, dgBill });
                    }
                    else
                    {

                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { e.RowIndex + 1, ColIndex.ItemName, dgBill });
                    }
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscRupees2)
                {
                    CalculateTotal();
                    MovetoNext move2n = new MovetoNext(m2n);
                    BeginInvoke(move2n, new object[] { e.RowIndex + 1, ColIndex.ItemName, dgBill });
                    //Disc_MoveNext();
                    //MovetoNext move2n = new MovetoNext(m2n);
                    //BeginInvoke(move2n, new object[] { e.RowIndex, ColIndex.Amount, dgBill });
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscAmount2)
                {
                    //isDisc1PercentChanged = true;
                    //double amt = Convert.ToDouble((Convert.ToDouble(dgBill.CurrentCell.Value) * 100) / Convert.ToDouble(dgBill.Rows[e.RowIndex].Cells[ColIndex.Amount].Value));
                    //dgBill.Rows[e.RowIndex].Cells[ColIndex.DiscPercentage2].Value = amt;
                    //dgBill.Rows[e.RowIndex].Cells[ColIndex.Amount].Value = Convert.ToDouble(dgBill.Rows[e.RowIndex].Cells[ColIndex.Amount].Value) - Convert.ToDouble(dgBill.Rows[e.RowIndex].Cells[ColIndex.DiscAmount2].Value);
                    CalculateTotal();
                    MovetoNext move2n = new MovetoNext(m2n);
                    if (dgBill.Columns[ColIndex.DiscRupees2].Visible == true)
                        BeginInvoke(move2n, new object[] { e.RowIndex, ColIndex.DiscRupees2, dgBill });
                    else
                        BeginInvoke(move2n, new object[] { e.RowIndex + 1, ColIndex.ItemName, dgBill });
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.MRP)
                {
                    if (dgBill.CurrentCell.Value != null && dgBill.CurrentCell.Value.ToString() != "")
                    {
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { e.RowIndex, ColIndex.FreeQty, dgBill });

                        if (ObjFunction.CheckValidAmount(dgBill.CurrentCell.Value.ToString()) == true)
                        {

                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgBill_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    delete_row();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    dgBill.Focus();
                    if (dgBill.CurrentCell.ColumnIndex == ColIndex.SrNo)
                    {
                        e.SuppressKeyPress = true;
                        dgBill.CurrentCell = dgBill[ColIndex.ItemName, dgBill.CurrentCell.RowIndex];
                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.ItemName)
                    {
                        e.SuppressKeyPress = true;
                        dgBill.CurrentCell.Value = "";
                        Desc_Start();
                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.Quantity)
                    {
                        e.SuppressKeyPress = true;
                        if (dgBill.CurrentCell.Value == null) dgBill.CurrentCell.Value = "1";
                        Qty_MoveNext();
                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.FreeQty)
                    {
                        e.SuppressKeyPress = true;
                        if (dgBill.CurrentCell.Value == null && dgBill.CurrentCell.Value.ToString() == "") dgBill.CurrentCell.Value = "1";
                        FreeQty_MoveNext();
                    }
                    if (dgBill.CurrentCell.ColumnIndex == ColIndex.MRP)
                    {
                        e.SuppressKeyPress = true;
                        dgBill.CurrentCell = dgBill[ColIndex.FreeQty, dgBill.CurrentCell.RowIndex];
                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscPercentage)
                    {
                        e.SuppressKeyPress = true;
                        if (dgBill.CurrentCell.Value == null) dgBill.CurrentCell.Value = "0";
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex, ColIndex.DiscAmount, dgBill });

                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscPercentage2)
                    {
                        e.SuppressKeyPress = true;
                        if (dgBill.CurrentCell.Value == null) dgBill.CurrentCell.Value = "0";
                        if (dgBill.CurrentCell.RowIndex < dgBill.Rows.Count)
                        {
                            if (dgBill.Rows.Count - 1 == dgBill.CurrentCell.RowIndex)
                            {
                                MovetoNext move2n = new MovetoNext(m2n);
                                BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex, ColIndex.Rate, dgBill });
                            }
                            else
                            {
                                MovetoNext move2n = new MovetoNext(m2n);
                                BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex + 1, ColIndex.ItemName, dgBill });
                            }
                        }
                        else
                        {
                            MovetoNext move2n = new MovetoNext(m2n);
                            BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex, ColIndex.ItemName, dgBill });
                        }

                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.UOM)
                    {
                        e.SuppressKeyPress = true;
                        if (dgBill.CurrentRow.Cells[ColIndex.ItemNo].Value != null && dgBill.CurrentRow.Cells[ColIndex.ItemNo].Value.ToString() != "")
                        {
                            UOM_Start();
                        }
                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.Rate)
                    {
                        e.SuppressKeyPress = true;
                        Rate_MoveNext();
                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscAmount)
                    {
                        e.SuppressKeyPress = true;
                        if (dgBill.CurrentCell.Value == null) dgBill.CurrentCell.Value = "0";
                        MovetoNext move2n = new MovetoNext(m2n);
                        if (dgBill.Rows.Count - 1 != dgBill.CurrentCell.RowIndex)
                            BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex, ColIndex.DiscPercentage2, dgBill });

                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscAmount2)
                    {
                        e.SuppressKeyPress = true;
                        if (dgBill.CurrentCell.Value == null) dgBill.CurrentCell.Value = "0";
                        if (dgBill.Columns[ColIndex.DiscRupees2].Visible == true)
                        {
                            MovetoNext move2n = new MovetoNext(m2n);
                            BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex, ColIndex.DiscRupees2, dgBill });
                        }
                        else
                        {
                            if (dgBill.CurrentCell.RowIndex < dgBill.Rows.Count)
                            {
                                MovetoNext move2n = new MovetoNext(m2n);
                                if (dgBill.Rows.Count - 1 != dgBill.CurrentCell.RowIndex)
                                    BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex + 1, ColIndex.ItemName, dgBill });
                            }
                            else
                            {
                                MovetoNext move2n = new MovetoNext(m2n);
                                if (dgBill.Rows.Count - 1 != dgBill.CurrentCell.RowIndex)
                                    BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex, ColIndex.Amount, dgBill });
                            }
                        }

                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscRupees2)
                    {
                        e.SuppressKeyPress = true;
                        if (dgBill.CurrentCell.Value == null) dgBill.CurrentCell.Value = "0";
                        if (dgBill.CurrentCell.RowIndex < dgBill.Rows.Count)
                        {
                            MovetoNext move2n = new MovetoNext(m2n);
                            if (dgBill.Rows.Count - 1 != dgBill.CurrentCell.RowIndex)
                                BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex + 1, ColIndex.ItemName, dgBill });
                        }
                        else
                        {
                            MovetoNext move2n = new MovetoNext(m2n);
                            if (dgBill.Rows.Count - 1 != dgBill.CurrentCell.RowIndex)
                                BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex, ColIndex.Amount, dgBill });
                        }

                    }
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    //if (txtDistDisc.Visible == true) txtDiscount1.Focus();
                    //else if (txtDiscRupees1.Visible == true) 
                    txtDiscRupees1.Focus();
                }
                else if (e.KeyCode == Keys.F8)
                {
                    if (dgBill.CurrentCell.Value != null)
                        dgBill.CurrentCell = dgBill[ColIndex.Rate, dgBill.CurrentCell.RowIndex];
                    else
                    {
                        if (dgBill.CurrentCell.RowIndex == 0)
                            dgBill.CurrentCell = dgBill[ColIndex.Rate, dgBill.CurrentCell.RowIndex];
                        else
                            dgBill.CurrentCell = dgBill[ColIndex.Rate, dgBill.CurrentCell.RowIndex - 1];
                    }
                }
                else if (e.KeyCode == Keys.F7)
                {
                    //if (btnSave.Visible == true)
                    //{
                    //    DisplayStockGodown();
                    //}
                    if (btnSave.Visible == true)
                    {
                        //DisplayStockGodown();
                        if (dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemNo].Value != null)
                            dgBill.CurrentCell = dgBill[ColIndex.Quantity, dgBill.CurrentCell.RowIndex];
                        else
                            dgBill.CurrentCell = dgBill[ColIndex.Quantity, dgBill.CurrentCell.RowIndex - 1];
                    }

                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgBill_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int row, col;
                if (dgBill.CurrentCell != null)
                { col = dgBill.CurrentCell.ColumnIndex; row = dgBill.CurrentCell.RowIndex; }
                else { col = e.ColumnIndex; row = e.RowIndex; }
                if (dgBill.Rows.Count > 0)
                    dgBill.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";
                if (col == ColIndex.Quantity && row >= 0)
                {
                    //if (dgBill.Rows.Count == dgBill.CurrentCell.RowIndex + 1) AddRows = true;
                    //    if (flagParking == true) return;
                    dgBill.CurrentCell.ErrorText = "";
                    if (dgBill.CurrentCell.Value != null)
                    {
                        if (dgBill.CurrentCell.Value.ToString() != "" && dgBill.CurrentCell.Value.ToString() != "0")
                        {
                            if (ObjFunction.CheckNumeric(dgBill.CurrentCell.Value.ToString()) == true)
                            {
                                int rowIndex = dgBill.CurrentCell.RowIndex;
                                if (dgBill[ColIndex.Rate, dgBill.CurrentCell.RowIndex].Value == null || Convert.ToString(dgBill[ColIndex.Rate, dgBill.CurrentCell.RowIndex].Value) == "")
                                    dgBill[ColIndex.Amount, dgBill.CurrentCell.RowIndex].Value = "0.00";
                                //else
                                //  CalculateTotal();
                                dgBill.Focus();

                                dgBill.CurrentCell = dgBill[2, row];


                            }

                        }
                    }
                }
                else if (col == ColIndex.Rate && row >= 0)
                {
                    //    if (flagParking == true) return;
                    dgBill.CurrentCell.ErrorText = "";
                    if (dgBill.CurrentCell.Value != null)
                    {
                        if (dgBill.CurrentCell.Value.ToString() != "" && dgBill.CurrentCell.Value.ToString() != "0")
                        {
                            if (ObjFunction.CheckNumeric(dgBill.CurrentCell.Value.ToString()) == true)
                            {

                                //dgBill[5, dgBill.CurrentCell.RowIndex].Value = Convert.ToDouble(dgBill[4, dgBill.CurrentCell.RowIndex].Value) * Convert.ToDouble(dgBill[2, dgBill.CurrentCell.RowIndex].Value);
                                dgBill.CurrentCell = dgBill[ColIndex.Rate, dgBill.CurrentCell.RowIndex];
                                dgBill.Rows[dgBill.CurrentCell.RowIndex].Selected = true;
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

        }

        private void dgBill_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            TextBox txt = (TextBox)e.Control;
            txt.KeyDown += new KeyEventHandler(txtSpace_KeyDown);
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.Quantity)
            {
                TextBox txt1 = (TextBox)e.Control;
                txt1.TextChanged += new EventHandler(txtQuantity_TextChanged);
                //txt1.TextChanged -= new EventHandler(txtQuantity_TextChanged);
            }
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.FreeQty)
            {
                TextBox txt1 = (TextBox)e.Control;
                txt1.TextChanged += new EventHandler(txtFreeQuantity_TextChanged);
                //txt1.TextChanged -= new EventHandler(txtQuantity_TextChanged);
            }
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.Rate)
            {
                TextBox txtrate = (TextBox)e.Control;
                txtrate.TextChanged += new EventHandler(txtRate_TextChanged);
            }
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.MRP)
            {
                TextBox txtMRP = (TextBox)e.Control;
                txtMRP.TextChanged += new EventHandler(txtMRP_TextChanged);
            }
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscPercentage)
            {
                TextBox txtSchemeDisc = (TextBox)e.Control;
                txtSchemeDisc.TextChanged += new EventHandler(txtSchemeDisc_TextChanged);
            }
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscPercentage2)
            {
                TextBox txtDistDisc = (TextBox)e.Control;
                txtDistDisc.TextChanged += new EventHandler(txtDistDisc_TextChanged);
            }
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscAmount2)
            {
                TextBox txtDistDisc = (TextBox)e.Control;
                txtDistDisc.TextChanged += new EventHandler(txtSchemeRupess_TextChanged);
            }
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscAmount)
            {
                TextBox txtDistDisc = (TextBox)e.Control;
                txtDistDisc.TextChanged += new EventHandler(txtSchemeRupess_TextChanged);
            }
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscRupees2)
            {
                TextBox txtDiscRs2 = (TextBox)e.Control;
                txtDiscRs2.TextChanged += new EventHandler(txtDiscRs2_TextChanged);
            }
        }

        private void txtDistDisc_TextChanged(object sender, EventArgs e)
        {
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscPercentage2)
            {
                ObjFunction.SetMasked((TextBox)sender, 2, 2, JitFunctions.MaskedType.NotNegative);
            }
        }

        private void txtSchemeRupess_TextChanged(object sender, EventArgs e)
        {
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscAmount2 || dgBill.CurrentCell.ColumnIndex == ColIndex.DiscAmount)
            {
                ObjFunction.SetMasked((TextBox)sender, 2, 5, JitFunctions.MaskedType.NotNegative);
            }
        }

        private void txtDiscRs2_TextChanged(object sender, EventArgs e)
        {
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscRupees2)
            {
                ObjFunction.SetMasked((TextBox)sender, 2, 5, JitFunctions.MaskedType.NotNegative);
            }
        }

        private void txtMRP_TextChanged(object sender, EventArgs e)
        {
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.MRP)
            {
                ObjFunction.SetMasked((TextBox)sender, 2, 9, JitFunctions.MaskedType.NotNegative);
            }
        }

        private void txtSchemeDisc_TextChanged(object sender, EventArgs e)
        {
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscPercentage)
            {
                ObjFunction.SetMasked((TextBox)sender, 2, 2, JitFunctions.MaskedType.NotNegative);
            }
        }

        private void txtRate_TextChanged(object sender, EventArgs e)
        {
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.Rate)
            {
                ObjFunction.SetMasked((TextBox)sender, 2, 5, JitFunctions.MaskedType.NotNegative);
            }
        }

        public void txtQuantity_TextChanged(object sender, EventArgs e)
        {
            //if (dgItemList.CurrentCell.ColumnIndex == 3)
            //{
            //    ObjFunction.SetMasked((TextBox)sender, -1, 6, JitFunctions.MaskedType.NotNegative);
            //}
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.Quantity)
            {
                //if (((TextBox)sender).Text.Length > 6)
                //    ((TextBox)sender).Text = "1";
                ObjFunction.SetMasked((TextBox)sender, 2, 6, JitFunctions.MaskedType.NotNegative);
            }
        }

        private void txtQuantity1_TextChanged(object sender, EventArgs e)
        {
            if (dgItemList.CurrentCell.ColumnIndex == 3)
            {
                ObjFunction.SetMasked((TextBox)sender, -1, 6, JitFunctions.MaskedType.NotNegative);
            }
        }

        private void txtFreeQuantity_TextChanged(object sender, EventArgs e)
        {
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.FreeQty)
            {
                ObjFunction.SetMasked((TextBox)sender, 2, 6, JitFunctions.MaskedType.NotNegative);
            }
        }

        private void txtSpace_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                Spaceflag = false;
                if (dgBill.CurrentCell.RowIndex == 0)
                {
                    if (dgBill.CurrentCell.ColumnIndex != 0)
                    {
                        int col = dgBill.CurrentCell.ColumnIndex;
                        while (true)
                        {
                            if (col == 0) break;
                            if (dgBill[col - 1, dgBill.CurrentCell.RowIndex].Visible == true)
                            {
                                dgBill.CurrentCell = dgBill[col - 1, dgBill.CurrentCell.RowIndex];
                                break;
                            }
                            else
                            {
                                col = col - 1;
                            }
                        }
                    }
                }
                else
                {

                    if (dgBill.CurrentCell.ColumnIndex == 1)
                        dgBill.CurrentCell = dgBill[4, dgBill.CurrentCell.RowIndex - 1];
                    else if (dgBill.CurrentCell.ColumnIndex != 0)
                        dgBill.CurrentCell = dgBill[dgBill.CurrentCell.ColumnIndex - 1, dgBill.CurrentCell.RowIndex];
                }
            }
            TextBox txt = (TextBox)sender;
            txt.KeyDown -= new KeyEventHandler(txtSpace_KeyDown);
        }

        private void BindGrid(int RowIndex)
        {
            try
            {
                long ItemNo, RateSettingNo, BarcodeNo;
                double StockConFactor;
                DataTable dtLedger = new DataTable();

                RateSettingNo = Convert.ToInt64(dgBill.Rows[RowIndex].Cells[ColIndex.PkRateSettingNo].Value);
                ItemNo = Convert.ToInt64(dgBill.Rows[RowIndex].Cells[ColIndex.ItemNo].Value);
                BarcodeNo = Convert.ToInt64(dgBill.Rows[RowIndex].Cells[ColIndex.PkBarCodeNo].Value);

                string str = "SELECT r.FkBcdSrNo, r.MKTQty, r.StockConversion, t.TaxLedgerNo, t.SalesLedgerNo, " +
                    " t.PkSrNo,t.Percentage, r.MRP FROM MRateSetting As r,GetItemTaxAll(" + ItemNo + ", NULL, " + GroupType.PurchaseAccount + "," + ObjFunction.GetComboValue(cmbTaxType) + ",NULL) As t " +
                    " WHERE r.PkSrNo = " + RateSettingNo + " AND r.ItemNo = t.ItemNo";

                DataTable dt = ObjFunction.GetDataView("SELECT r.FkBcdSrNo, r.MKTQty, r.StockConversion, t.TaxLedgerNo, t.SalesLedgerNo, " +
                    " t.PkSrNo,t.Percentage, r.MRP FROM MRateSetting As r,GetItemTaxAll(" + ItemNo + ", NULL, " + GroupType.PurchaseAccount + "," + ObjFunction.GetComboValue(cmbTaxType) + ",NULL) As t " +
                    " WHERE r.PkSrNo = " + RateSettingNo + " AND r.ItemNo = t.ItemNo").Table;


                if (dt.Rows.Count > 0)
                {
                    if (BarcodeNo == 0)
                    {
                        BarcodeNo = Convert.ToInt64(dt.Rows[0][0].ToString());
                        dgBill.Rows[RowIndex].Cells[ColIndex.PkBarCodeNo].Value = BarcodeNo;
                    }

                    dgBill.Rows[RowIndex].Cells[ColIndex.MKTQuantity].Value = Convert.ToInt64(dt.Rows[0][1].ToString());

                    if (txtOtherDisc.Text.Trim() == "" || ObjFunction.CheckValidAmount(txtOtherDisc.Text) == false)
                    {
                        txtOtherDisc.Text = "0";
                    }

                    //dgBill.Rows[RowIndex].Cells[ColIndex.DiscAmount].Value = Convert.ToDouble(txtTotalDisc.Text);

                    StockConFactor = Convert.ToDouble(dt.Rows[0][2].ToString());
                    dgBill.Rows[RowIndex].Cells[ColIndex.StockFactor].Value = StockConFactor;

                    dgBill.Rows[RowIndex].Cells[ColIndex.TaxLedgerNo].Value = Convert.ToInt64(dt.Rows[0][3].ToString());
                    dgBill.Rows[RowIndex].Cells[ColIndex.SalesLedgerNo].Value = Convert.ToInt64(dt.Rows[0][4].ToString());
                    dgBill.Rows[RowIndex].Cells[ColIndex.PkItemTaxInfo].Value = Convert.ToInt64(dt.Rows[0][5].ToString());
                    if (dgBill.Rows[RowIndex].Cells[ColIndex.PkStockTrnNo].Value == null) dgBill.Rows[RowIndex].Cells[ColIndex.PkStockTrnNo].Value = 0;
                    dgBill.Rows[RowIndex].Cells[ColIndex.PkVoucherNo].Value = 0;
                    dgBill.Rows[RowIndex].Cells[ColIndex.TaxPercentage].Value = Convert.ToDouble(dt.Rows[0][6].ToString());
                    if (dgBill.Rows[RowIndex].Cells[ColIndex.BalanceQty].Value == null) dgBill.Rows[RowIndex].Cells[ColIndex.BalanceQty].Value = 0;
                    if (Convert.ToDouble(dgBill.Rows[RowIndex].Cells[ColIndex.MRP].Value) == Convert.ToDouble(dgBill.Rows[RowIndex].Cells[ColIndex.TempMRP].Value))
                    {
                        dgBill.Rows[RowIndex].Cells[ColIndex.MRP].Value = Convert.ToDouble(dt.Rows[0][7].ToString()).ToString("0.00");
                        dgBill.Rows[RowIndex].Cells[ColIndex.TempMRP].Value = Convert.ToDouble(dt.Rows[0][7].ToString()).ToString("0.00");
                    }

                    if (dgBill.Rows.Count == dgBill.CurrentRow.Index + 1 && (dgBill.CurrentCell.ColumnIndex == 4 || dgBill.CurrentCell.ColumnIndex == 3))
                    {
                        dgBill.Rows.Add();
                    }

                    CalculateTotal();

                    DataTable dtStk;
                    DataRow dr = null;
                    if (dtBillCollect.Count == RowIndex)
                    {
                        dtStk = ObjFunction.GetDataView("Exec GetStockGodownDetails 0,0").Table;
                        for (int row = 0; row < dtStk.Rows.Count; row++)
                        {
                            if (Convert.ToInt64(dtStk.Rows[row].ItemArray[0].ToString()) == ObjFunction.GetComboValue(cmbLocation))
                            {
                                dr = dtStk.Rows[row];
                                dr[2] = dgBill.Rows[RowIndex].Cells[ColIndex.Quantity].Value;
                                dr[3] = dgBill.Rows[RowIndex].Cells[ColIndex.ActualQty].Value;
                            }
                        }
                        dtBillCollect.Add(dtStk);
                    }
                    else
                    {
                        dtStk = dtBillCollect[RowIndex];
                        for (int row = 0; row < dtStk.Rows.Count; row++)
                        {
                            if (Convert.ToInt64(dtStk.Rows[row].ItemArray[0].ToString()) == ObjFunction.GetComboValue(cmbLocation))
                            {
                                dr = dtStk.Rows[row];
                                dr[2] = dgBill.Rows[RowIndex].Cells[ColIndex.Quantity].Value;
                                dr[3] = dgBill.Rows[RowIndex].Cells[ColIndex.ActualQty].Value;
                            }
                            else
                            {
                                dr = dtStk.Rows[row];
                                dr[2] = 0;
                                dr[3] = 0;
                            }
                        }
                        dtBillCollect.RemoveAt(RowIndex);
                        dtBillCollect.Insert(RowIndex, dtStk);
                    }
                }
                else
                {
                    for (int i = 1; i < dgBill.Columns.Count; i++)
                    {
                        dgBill.Rows[RowIndex].Cells[i].Value = null;
                    }
                    DisplayMessage("Items Tax Details Not Found.....");
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #endregion

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                //Purchase.RequestSalesNo = 0;
                //Form NewF = new Purchase();
                //this.Close();
                //ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                ObjFunction.FillCombo(cmbPartyNameSearch, " SELECT DISTINCT MLedger.LedgerNo, MLedger.LedgerName FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo  AND  " +
                             " TVoucherEntry.VoucherTypeCode = " + VchType.Purchase + " AND  TVoucherDetails.VoucherSrNo = 1 INNER JOIN MLedger ON MLedger.LedgerNo = TVoucherDetails.LedgerNo ORDER BY MLedger.LedgerName ");
                pnlPartySearch.Visible = false;
                pnlSearch.Visible = true;
                txtSearch.Text = ""; txtSearch.Enabled = true;

                btnNew.Enabled = false;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false; btnOrderClosed.Visible = false;
                txtInvNoSearch.Enabled = true;
                cmbPartyNameSearch.Enabled = true;
                txtInvNoSearch.Text = "";
                txtSearch.Text = "";
                //cmbPartyName.SelectedIndex = 0;
                rbInvNo.Checked = true;
                rbType_CheckedChanged(rbInvNo, null);
                txtSearch.Focus();
                dgInvSearch.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                dgPartySearch.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #region Navigation Methods
        private void NavigationDisplay(int type)
        {
            try
            {
                long No = 0;
                if (type == 5)
                {
                    if (dtSearch.Rows.Count > 0)
                    {
                        No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                        ID = No;
                    }
                }
                else if (type == 1)
                {
                    No = Convert.ToInt64(dtSearch.Rows[0].ItemArray[0].ToString());
                    cntRow = 0;
                    ID = No;
                }
                else if (type == 2)
                {
                    No = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    cntRow = dtSearch.Rows.Count - 1;
                    ID = No;
                }
                else
                {
                    if (type == 3)
                    {
                        cntRow = cntRow + 1;
                    }
                    else if (type == 4)
                    {
                        cntRow = cntRow - 1;
                    }

                    if (cntRow < 0)
                    {
                        OMMessageBox.Show("This is First Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow + 1;
                    }
                    else if (cntRow > dtSearch.Rows.Count - 1)
                    {
                        OMMessageBox.Show("This is Last Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow - 1;
                    }
                    else
                    {
                        No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                        ID = No;
                    }

                }

                if (ID > 0)
                    FillControls();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

        }

        private void SetNavigation()
        {
            try
            {
                cntRow = 0;
                for (int i = 0; i < dtSearch.Rows.Count; i++)
                {
                    if (Convert.ToInt64(dtSearch.Rows[i].ItemArray[0].ToString()) == ID)
                    {
                        cntRow = i;
                        break;
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void setDisplay(bool flag)
        {
            btnFirst.Visible = flag;
            btnPrev.Visible = flag;
            btnNext.Visible = flag;
            btnLast.Visible = flag;
            btnPrint.Visible = flag;
            //btnDelete.Visible = flag;
            //GridRange.Height = 25;
            if (dtSearch.Rows.Count == 0)
            {
                btnFirst.Visible = false;
                btnPrev.Visible = false;
                btnNext.Visible = false;
                btnLast.Visible = false;
                btnPrint.Visible = false;
            }
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            NavigationDisplay(1);
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            NavigationDisplay(4);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            NavigationDisplay(3);
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            NavigationDisplay(2);
        }

        #endregion

        #region KeyDown Events
        private void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is GroupBox)
                    KeyDownFormat(ctrl.Controls);
            }
        }

        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left && e.Control)
            {
                if (btnPrev.Enabled) btnPrev_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Up && e.Control)
            {
                if (btnFirst.Enabled) btnFirst_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Right && e.Control)
            {
                if (btnNext.Enabled) btnNext_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Down && e.Control)
            {
                if (btnLast.Enabled) btnLast_Click(sender, e);
            }

            else if (e.KeyCode == Keys.F12)
            {
                if (btnSave.Visible && ID == 0)
                {
                    cmbPaymentType.SelectedValue = "2";
                    btnSave_Click(sender, e);
                }
            }
            //else if (e.KeyCode == Keys.F11)
            //{
            //    if (btnSave.Visible && ID == 0)
            //    {
            //        cmbPaymentType.SelectedValue = "3";
            //        btnSave_Click(sender, e);
            //    }
            //}
            else if (e.KeyCode == Keys.F7)
            {
                if (dgBill.Focused == true)
                {
                    if (dgBill.Rows.Count > 0)
                    {
                        if (dgBill.CurrentCell.ColumnIndex == 2)
                        {
                            dgBill.CurrentCell.ReadOnly = false;
                            //AddRows = false;
                            //FlagRate = false;
                            //defaultQty = true;
                        }
                    }
                }
            }
            else if (e.KeyCode == Keys.Escape)
            {
                //btnExit_Click(sender, e);
            }
            //else if (e.KeyCode == Keys.F2)
            //{
            //    if (txtDiscount1.Enabled) txtDiscount1.Focus();
            //}
            else if (e.KeyCode == Keys.F4)
            {
                //if (ID == 0 && btnSave.Visible == true)
                //    cmbPaymentType.Focus();
                //if (ID == 0)
                //{
                //    if (ExchangeMode == false)
                //    {
                //        lblExchange.Visible = true;
                //        ExchangeMode = true;
                //    }
                //    else
                //    {
                //        ExchangeMode = false;
                //        lblExchange.Visible = false;
                //    }
                //}
            }
            else
                if (e.KeyCode == Keys.F3)
                {
                    if (dgCompany.Rows.Count > 1 && btnNew.Visible)
                    {
                        pnlCompany.Visible = true;
                        dgCompany.Focus();
                        dgCompany.CurrentCell = dgCompany[1, 0];
                    }


                }
            if (e.KeyCode == Keys.P && e.Control)
            {
                PrintBill();
            }

            else if (e.KeyCode == Keys.F1)
            {
                e.SuppressKeyPress = true;
                if (pnlFooterInfo.Visible == false)
                {
                    pnlFooterInfo.Dock = DockStyle.Bottom;
                    //pnlFooterInfo.Height = 30;
                    pnlFooterInfo.BorderStyle = BorderStyle.None;
                    pnlFooterInfo.BringToFront();
                    pnlFooterInfo.Visible = true;
                }
                else
                {
                    pnlFooterInfo.Visible = false;
                }
            }
            else if (e.KeyCode == Keys.F5)
            {
                if (pnlItemName.Visible == true)
                    btnOk1_Click(sender, new EventArgs());
            }
            //else if (e.KeyCode == Keys.F6)
            //{
            //    if (ID == 0)
            //        ShowParkingBill();
            //}
            //else if (e.KeyCode == Keys.F4)
            //    ValidationsMain();
            else if (e.KeyCode == Keys.L && e.Control)
            {
                if (btnSave.Visible)
                {
                    dgBill.Columns[ColIndex.LandedRate].Visible = !dgBill.Columns[ColIndex.LandedRate].Visible;
                    if (dgBill.Columns[ColIndex.LandedRate].Visible == true)
                    {
                        if (dgBill.CurrentCell != null)
                            dgBill.CurrentCell = dgBill[ColIndex.LandedRate, dgBill.CurrentRow.Index];
                        else
                            dgBill.CurrentCell = dgBill[ColIndex.LandedRate, 0];
                    }
                    else
                    {
                        if (dgBill.CurrentCell != null)
                            dgBill.CurrentCell = dgBill[ColIndex.ItemName, dgBill.CurrentRow.Index];
                        else
                            dgBill.CurrentCell = dgBill[ColIndex.ItemName, 0];
                    }

                }
            }
            else if (e.KeyCode == Keys.Q && e.Control)
            {
                if (btnSave.Visible == true)
                {
                    if (ObjFunction.CheckAllowMenu(10) == false) return;
                    Form NewF = new Master.StockItemSAE(-1);
                    ObjFunction.OpenForm(NewF);

                    if (((Master.StockItemSAE)NewF).ShortID != 0)
                    {
                        string barcode = ObjQry.ReturnString("Select BarCode From MStockBarCode where ItemNo=" + ((Master.StockItemSAE)NewF).ShortID + "", CommonFunctions.ConStr);
                        int rwindex = dgBill.CurrentCell.RowIndex;
                        dgBill.CurrentRow.Cells[ColIndex.ItemName].Value = barcode;
                        dgBill_CellEndEdit(dgBill, new DataGridViewCellEventArgs(ColIndex.ItemName, rwindex));
                    }
                }
            }
            else if (e.KeyCode == Keys.W && e.Control)
            {
                if (btnSave.Visible == false)
                {
                    MixModeFlag = true;
                    cmbPaymentType.SelectedValue = "3";
                    btnSave_Click(sender, e);

                }
            }
            else if (e.KeyCode == Keys.M && e.Control)
            {
                if (cmbPaymentType.SelectedValue.ToString() == "3" && btnMixMode.Visible == true)
                {
                    //btnMixMode_Click(sender, e);

                }
            }
            else if (e.KeyCode == Keys.E && e.Control)
            {
                if (btnSave.Visible && chkFilterItem.Enabled)
                    chkFilterItem.Checked = !chkFilterItem.Checked;
            }
            else if (e.Alt && e.KeyCode == Keys.F2)
            {
                if (btnNew.Visible == false)
                {
                    if (btnAdvanceSearch.Enabled) btnAdvanceSearch_Click(sender, e);
                }
            }
        }
        #endregion

        #region Delete code
        private void InitDelTable()
        {
            dtDelete.Columns.Add();
            dtDelete.Columns.Add();
        }

        private void DeleteDtls(int Type, long PkNo)
        {
            DataRow dr = null;
            dr = dtDelete.NewRow();
            dr[0] = Type;
            dr[1] = PkNo;
            dtDelete.Rows.Add(dr);
        }

        private void DeleteValues()
        {

            if (dtDelete != null)
            {

                for (int i = 0; i < dtDelete.Rows.Count; i++)
                {
                    if (Convert.ToInt32(dtDelete.Rows[i].ItemArray[0]) == 1)
                    {
                        tOtherStock.PkOtherStockTrnNo = Convert.ToInt64(dtDelete.Rows[i].ItemArray[1]);
                        dbtOtherVoucherEntry.DeleteTOtherStock(tOtherStock);
                    }
                }
                dtDelete.Rows.Clear();


            }
        }


        #endregion

        #region Parial Payment Methods

        private void Partial_Leave(object sender, EventArgs e)
        {
            if (((TextBox)sender).Text == "")
            {
                OMMessageBox.Show("Please Enter amount", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                ((TextBox)sender).Focus();
            }
            else if (ObjFunction.CheckValidAmount(((TextBox)sender).Text) == false)
            {
                OMMessageBox.Show("Please Enter valid amount", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                ((TextBox)sender).Focus();
            }
            else
            {
                //double cash, credit;
                //cash = (txtCash.Text == "") ? 0 : Convert.ToDouble(txtCash.Text); txtCash.Text = cash.ToString("0.00");
                //credit = (txtCredit.Text == "") ? 0 : Convert.ToDouble(txtCredit.Text); txtCredit.Text = credit.ToString("0.00");
                //txtBalance.Text = (Convert.ToDouble(txtGrandTotal.Text) - (cash + credit)).ToString("0.00");
            }
        }
        private void Partial_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Partial_Leave(sender, e);
            }
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                bool flag = true;
                if (Convert.ToDouble(dgPayType.Rows[3].Cells[2].Value) > 0)
                {
                    if (dgPayChqDetails.Rows[0].Cells[0].Value == null || dgPayChqDetails.Rows[0].Cells[1].FormattedValue.ToString().Trim() == "" || dgPayChqDetails.Rows[0].Cells[2].FormattedValue.ToString().Trim() == "")
                    {
                        OMMessageBox.Show("Please Fill Cheque Details.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        flag = false;
                    }
                    else flag = true;
                }
                if (flag == true)
                {
                    if (Convert.ToDouble(dgPayType.Rows[4].Cells[2].Value) > 0)
                    {
                        if (dgPayCreditCardDetails.Rows[0].Cells[0].FormattedValue.ToString().Trim() == "" || dgPayCreditCardDetails.Rows[0].Cells[1].FormattedValue.ToString().Trim() == "")
                        {
                            OMMessageBox.Show("Please Fill Credit Card Details.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            flag = false;
                        }
                        else flag = true;
                    }
                }
                if (flag == true)
                {
                    if (Convert.ToDouble(txtTotalAmt.Text) != Convert.ToDouble(txtGrandTotal.Text))
                    {
                        OMMessageBox.Show("TOTAL AMOUNT EXCEEDS TO BILL AMOUNT.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                    else
                    {
                        btnSave.Enabled = true;
                        btnSave.Focus();
                        pnlPartial.Visible = false;
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }
        #endregion

        private void DisplayMessage(string str)
        {
            lblMsg.Visible = true;
            lblMsg.Text = str;
            Application.DoEvents();
            System.Threading.Thread.Sleep(700);
            lblMsg.Visible = false;
        }

        private void lblStatus_Click(object sender, EventArgs e)
        {
            try
            {
                if (BillingMode == 0)
                {
                    BillingMode = 1;
                    //VoucherType = VchType.TempSales;
                    lblStatus.ForeColor = Color.Green;
                }
                else
                {
                    BillingMode = 0;
                    //VoucherType = VchType.Purchase;
                    lblStatus.ForeColor = Color.Red;
                }

                ObjFunction.LockButtons(true, this.Controls);
                ObjFunction.LockControls(false, this.Controls);

                InitDelTable();
                txtInvNo.Enabled = false;
                InitControls();
                dtSearch = ObjFunction.GetDataView("Select PkVoucherNo from TVoucherEntry Where VoucherTypeCode=" + VoucherType + "").Table;

                if (dtSearch.Rows.Count > 0)
                {
                    ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    FillControls();
                    SetNavigation();
                }

                setDisplay(true);
                btnNew.Focus();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                ID = 0;
                ObjFunction.InitialiseControl(this.Controls);
                ObjFunction.LockButtons(false, this.Controls);
                ObjFunction.LockControls(true, this.Controls);
                dgBill.Enabled = true;
                InitControls();
                ObjFunction.FillComb(cmbPartyName, "Select LedgerNo,LedgerName From MLedger Where GroupNo in " +
                    "(" + GroupType.SundryCreditors + ") and IsActive='true' order by LedgerName");
                cmbPartyName.SelectedValue = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_DefaultPartyAC));
                LastBillNo = ObjQry.ReturnLong("Select IsNull(Max(PkOtherVoucherNo),0) From TOtherVoucherEntry Where VoucherTypeCode=" + VoucherType + " ", CommonFunctions.ConStr);
                dt = ObjFunction.GetDataView("SELECT IsNull(SUM(TOtherStock.Quantity),0) AS Quantity, IsNull(SUM(TOtherStock.Amount),0) AS Amount FROM TOtherVoucherEntry INNER JOIN " +
                    " TOtherStock ON TOtherVoucherEntry.PkOtherVoucherNo = TOtherStock.FKVoucherNo WHERE (TOtherVoucherEntry.VoucherTypeCode = " + VoucherType + ") AND (TOtherVoucherEntry.PkOtherVoucherNo = " + LastBillNo + ")").Table;

                if (dt.Rows.Count > 0)
                {
                    txtLastBillAmt.Text = Convert.ToDouble(dt.Rows[0].ItemArray[1].ToString()).ToString("0.00");
                    txtlastBillQty.Text = dt.Rows[0].ItemArray[0].ToString();
                    txtLastPayment.Text = ObjQry.ReturnString("SELECT MPayType.PayTypeName FROM MPayType INNER JOIN TOtherVoucherEntry ON MPayType.PKPayTypeNo = TOtherVoucherEntry.PayTypeNo WHERE (TOtherVoucherEntry.PkOtherVoucherNo = " + LastBillNo + ")", CommonFunctions.ConStr);
                    txtLastBillAmt.Enabled = false;
                    txtlastBillQty.Enabled = false;
                    txtLastPayment.Enabled = false;
                }
                btnMixMode.Visible = false;
                cmbPaymentType.SelectedValue = "2";
                cmbPaymentType.Enabled = false;
                cmbTaxType.SelectedValue = ObjFunction.GetAppSettings(AppSettings.P_TaxType);
                cmbTaxType.Enabled = false;
                cmbLocation.SelectedValue = ObjFunction.GetAppSettings(AppSettings.P_OutwardLocation);

                if (ObjQry.ReturnLong("Select Count(*) From MManufacturerDetails Where LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + "", CommonFunctions.ConStr) > 0)
                {
                    chkFilterItem.Checked = true;
                    chkFilterItem.Enabled = true;
                }
                else
                {
                    chkFilterItem.Checked = false;
                    chkFilterItem.Enabled = false;
                }
                tempDate = dtpBillDate.Value.Date;
                tempPartyNo = ObjFunction.GetComboValue(cmbPartyName);
                btnDelete.Visible = false; btnOrderClosed.Visible = false;
                dtpBillDate.Focus();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo in ( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + ")", CommonFunctions.ConStr) > 1)
                {
                    btnUpdate.Visible = false;
                    btnMixMode.Visible = false;
                    btnDelete.Visible = false;
                    OMMessageBox.Show("Already this bill Payment is done", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    return;
                }
                else
                {
                    if (ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo in ( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + " and TR.TypeOfRef in(6))", CommonFunctions.ConStr) > 1)
                    {
                        btnUpdate.Visible = false;
                        btnMixMode.Visible = false;
                        btnDelete.Visible = false;
                        OMMessageBox.Show("Already this bill Payment is done", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        return;
                    }
                }

                ObjFunction.LockButtons(false, this.Controls);
                ObjFunction.LockControls(true, this.Controls);
                dgBill.Enabled = true;
                dgBill.Focus();
                dgBill.CurrentCell = dgBill[1, dgBill.Rows.Count - 1];
                btnMixMode.Visible = false;
                if (ObjFunction.GetComboValue(cmbPartyName) == Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_PartyAC)))// && (ObjFunction.GetComboValue(cmbPaymentType) == 3))
                {
                    cmbPaymentType.SelectedValue = 2;
                    cmbPaymentType.Enabled = false;
                }
                else
                    cmbPaymentType.Enabled = true;
                if (ObjQry.ReturnLong("Select Count(*) From MManufacturerDetails Where LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + "", CommonFunctions.ConStr) > 0)
                {
                    chkFilterItem.Checked = true;
                    chkFilterItem.Enabled = true;
                }
                else
                {
                    chkFilterItem.Checked = false;
                    chkFilterItem.Enabled = false;
                }
                if (ID != 0)
                {
                    // cmbPaymentType.Enabled = false;
                    //dgPayType.Enabled = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ObjFunction.LockButtons(true, this.Controls);
            ObjFunction.LockControls(false, this.Controls);
            NavigationDisplay(5);
            DisplayList(false);
            btnSave.Enabled = true;
            dgBill.Enabled = false;
            MixModeFlag = false;
            btnNew.Focus();
        }

        private void DisplayList(bool flag)
        {
            pnlItemName.Visible = flag;
            pnlGroup1.Visible = flag;
            pnlGroup2.Visible = flag;
            pnlUOM.Visible = flag;
            pnlRate.Visible = flag;
            pnlCompany.Visible = flag;
        }

        private void cmbPaymentType_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                int cntCompany = 0;
                if (e.KeyCode == Keys.Enter)
                {
                    for (int i = 0; i < dgPayType.Rows.Count; i++)
                    {
                        dgPayType.Rows[i].Cells[2].Value = "0.00";
                    }

                    if (ObjFunction.GetComboValue(cmbPaymentType) == 1 || ObjFunction.GetComboValue(cmbPaymentType) == 4 || ObjFunction.GetComboValue(cmbPaymentType) == 5)
                    {
                        cmbPaymentType.TabIndex = 656;
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsAllowSingleFirmChq)) == true)
                        {
                            for (int i = 1; i < dgBill.Rows.Count - 1; i++)
                                if (Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.StockCompanyNo].Value) != Convert.ToInt64(dgBill.Rows[i - 1].Cells[ColIndex.StockCompanyNo].Value))
                                    cntCompany++;

                        }
                        if (cntCompany == 0 || ObjFunction.GetComboValue(cmbPaymentType) == 4 || ObjFunction.GetComboValue(cmbPaymentType) == 5)
                        {
                            //dgPayType.CurrentCell = dgPayType[2, 1];
                            pnlPartial.Visible = true;
                            if (ObjFunction.GetComboValue(cmbPaymentType) == 4 || ObjFunction.GetComboValue(cmbPaymentType) == 5)
                            {
                                if (ObjFunction.GetComboValue(cmbPaymentType) == 4)
                                {
                                    pnlPartial.Size = new Size(475, 214);
                                    pnlPartial.Location = new Point(75, 123);
                                    dgPayChqDetails.Location = dgPayType.Location;
                                    dgPayChqDetails.Visible = true;
                                    dgPayChqDetails.BringToFront();
                                    dgPayChqDetails.Columns[3].Visible = false;
                                    dgPayChqDetails.Focus();
                                    dgPayCreditCardDetails.Visible = false;
                                    dgPayChqDetails.Enabled = true;
                                    if (dgPayChqDetails.Rows.Count == 0)
                                    {
                                        dgPayChqDetails.Rows.Add();
                                        dgPayChqDetails.CurrentCell = dgPayChqDetails[0, 0];
                                    }
                                }
                                else if (ObjFunction.GetComboValue(cmbPaymentType) == 5)
                                {
                                    pnlPartial.Size = new Size(475, 214);
                                    pnlPartial.Location = new Point(75, 123);
                                    dgPayCreditCardDetails.Location = dgPayType.Location;
                                    dgPayCreditCardDetails.Visible = true;
                                    ((DataGridViewTextBoxColumn)dgPayCreditCardDetails.Columns[0]).MaxInputLength = Convert.ToInt32(ObjFunction.GetAppSettings(AppSettings.S_CreditCardDigitLimit));
                                    dgPayCreditCardDetails.Focus();
                                    dgPayCreditCardDetails.BringToFront();
                                    dgPayChqDetails.Visible = false;
                                    dgPayCreditCardDetails.Enabled = true;
                                    dgPayCreditCardDetails.Columns[2].Visible = false;
                                    if (dgPayCreditCardDetails.Rows.Count == 0)
                                    {
                                        dgPayCreditCardDetails.Rows.Add();
                                        dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[0, 0];
                                    }
                                }
                                dgPayType.Rows[Convert.ToInt32(ObjFunction.GetComboValue(cmbPaymentType)) - 1].Cells[2].Value = txtGrandTotal.Text;
                                // dgPayType.CurrentCell = dgPayType[2, Convert.ToInt32(ObjFunction.GetComboValue(cmbPaymentType)) - 1];
                                CaluculatePayType();
                            }
                            else
                            {
                                dgPayCreditCardDetails.Visible = false;
                                dgPayChqDetails.Visible = false;
                                pnlPartial.Location = new Point(200, 123);
                                pnlPartial.Size = new Size(305, 221);
                                dgPayType.CurrentCell = dgPayType[2, 1];
                                dgPayType.Focus();
                            }
                            btnSave.Enabled = false;
                        }
                    }
                    else if (ObjFunction.GetComboValue(cmbPaymentType) == 3)
                    {
                        e.SuppressKeyPress = true;
                        btnSave.Enabled = true;
                        btnSave.Focus();
                    }
                    else
                    {
                        pnlPartial.Visible = false;
                        cmbPaymentType.TabIndex = 10;
                        btnSave.Enabled = true;
                        txtRemark.Focus(); ;
                    }
                    e.SuppressKeyPress = true;
                    //if (ObjFunction.GetComboValue(cmbPaymentType) == 1 || ObjFunction.GetComboValue(cmbPaymentType) == 4 || ObjFunction.GetComboValue(cmbPaymentType) == 5)
                    //{
                    //    cmbPaymentType.TabIndex = 656;
                    //    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_IsAllowSingleFirmChq)) == true)
                    //    {
                    //        for (int i = 1; i < dgBill.Rows.Count - 1; i++)
                    //            if (Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.StockCompanyNo].Value) != Convert.ToInt64(dgBill.Rows[i - 1].Cells[ColIndex.StockCompanyNo].Value))
                    //                cntCompany++;

                    //    }
                    //    if (cntCompany == 0 || ObjFunction.GetComboValue(cmbPaymentType) == 4 || ObjFunction.GetComboValue(cmbPaymentType) == 5)
                    //    {
                    //        dgPayType.CurrentCell = dgPayType[2, 1];
                    //        if (ObjFunction.GetComboValue(cmbPaymentType) == 4 || ObjFunction.GetComboValue(cmbPaymentType) == 5)
                    //        {
                    //            dgPayType.Rows[Convert.ToInt32(ObjFunction.GetComboValue(cmbPaymentType)) - 1].Cells[2].Value = txtGrandTotal.Text;
                    //            dgPayType.CurrentCell = dgPayType[2, Convert.ToInt32(ObjFunction.GetComboValue(cmbPaymentType)) - 1];
                    //            CaluculatePayType();
                    //        }
                    //        dgPayType.Focus();
                    //        btnSave.Enabled = false;
                    //        pnlPartial.Visible = true;
                    //    }
                    //}
                    //else if (ObjFunction.GetComboValue(cmbPaymentType) == 3)
                    //{
                    //    e.SuppressKeyPress = true;
                    //    cmbPartyName.Focus();
                    //}
                    //else
                    //{
                    //    pnlPartial.Visible = false;
                    //    cmbPaymentType.TabIndex = 10;
                    //    txtRemark.Focus(); ;
                    //}
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (ID == 0) return;
                if (OMMessageBox.Show("Are you sure you want to delete the record ?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                {

                    dbtOtherVoucherEntry = new DBTOtherVoucherEntry();
                    tOtherVoucherEntry = new TOtherVoucherEntry();
                    tOtherVoucherEntry.PkOtherVoucherNo = ID;
                    dbtOtherVoucherEntry.DeleteTOtherVoucherEntry(tOtherVoucherEntry);


                    OMMessageBox.Show("Record deleted successfully.....", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);


                    //dtSearch = ObjFunction.GetDataView("Select PkVoucherNo from TVoucherEntry Where VoucherTypeCode=" + VoucherType + "").Table;
                    //ID = ObjQry.ReturnLong("Select Max(PkVoucherNo) FRom TVoucherEntry Where VoucherTypeCode=" + VoucherType + "", CommonFunctions.ConStr);
                    //SetNavigation();
                    setDisplay(true);
                    ObjFunction.LockButtons(true, this.Controls);
                    ObjFunction.LockControls(false, this.Controls);
                    FillControls();
                    dgBill.Enabled = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnCancelSearch_Click(object sender, EventArgs e)
        {
            pnlSearch.Visible = false;
            btnNew.Enabled = true;
            btnUpdate.Enabled = true;
            btnDelete.Enabled = true;
            btnOrderClosed.Enabled = true;
        }

        private void txtChrg2_Leave(object sender, EventArgs e)
        {
            cmbPaymentType.Focus();
            CalculateTotal();
        }

        private void txtRemark_Leave(object sender, EventArgs e)
        {
            if (txtRemark.Text.Trim() == "")
            {
                txtRemark.Text = "Purchase Order";
                btnSave.Focus();
            }
        }

        #region Without Barcode Methods

        private void FillItemList(int qNo, int iType)
        {
            try
            {
                switch (iType)
                {
                    case 1:
                        FillItemList(qNo);
                        break;
                    case 2:
                        break;
                    case 3:
                        //string ItemList = " SELECT MStockItems.ItemNo, MStockItems.ItemName, MRateSetting." + ObjFunction.GetComboValueString(cmbRateType) + " AS SaleRate, MUOM.UOMName, MRateSetting.MRP, " +
                        //    " 0 AS Stock, '' AS stkUOM, 0 AS SaleTax, 0 AS PurTax, " + //MItemTaxInfo_Sale.Percentage AS SaleTax , MItemTaxInfo_Pur.Percentage
                        //    " MStockItems.CompanyNo, MStockBarcode.Barcode, MRateSetting.PkSrNo As RateSettingNo, MStockItems.UOMDefault " +
                        //    " FROM MStockItems_V(NULL,NULL,NULL,NULL,NULL,NULL,NULL) AS MStockItems INNER JOIN " +
                        //    " dbo.GetItemRateAll(NULL, NULL, NULL, NULL, NULL,NULL) AS MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo AND  " +
                        //    " MStockItems.UOMDefault = MRateSetting.UOMNo INNER JOIN " +
                        //    " MStockBarcode ON MRateSetting.FkBcdSrNo = MStockBarcode.PkStockBarcodeNo INNER JOIN " +
                        //    " MUOM ON MStockItems.UOMDefault = MUOM.UOMNo  Where MStockItems.ItemNo in " +
                        //    "(Select ItemNo from MStockBarcode where Barcode = '" + dgBill.CurrentCell.Value + "' And IsActive ='true') AND MStockItems.CompanyNo=" + DBGetVal.CompanyNo + " " +
                        //    " ORDER BY MStockItems.ItemName";
                        DataTable dtBarCodeItemNo = ObjFunction.GetDataView("Select ItemNo from MStockBarcode where Barcode = '" + dgBill.CurrentCell.Value + "'").Table;
                        string ItemList = "";
                        for (int i = 0; i < dtBarCodeItemNo.Rows.Count; i++)
                        {
                            if (i != 0)
                            {
                                ItemList += " Union all ";
                            }

                            //ItemList += " SELECT MStockItems.ItemNo, MStockItems.ItemName,MStockItems.ItemNameLang, MRateSetting." + ObjFunction.GetComboValueString(cmbRateType) + " AS SaleRate, MUOM.UOMName, MRateSetting.MRP, " +
                            //    " (SELECT  ISNULL(SUM(CASE WHEN trncode = 1 THEN quantity ELSE - quantity END), 0) +  ISNULL(SUM(CASE WHEN trncode = 1 THEN FreeQty ELSE - FreeQty END), 0)   FROM TStock  INNER JOIN TStockGodown ON TSTock.PKStockTrnNo=TStockGodown.FKStockTrnNo   WHERE  TStockGodown.GodownNo=" + ObjFunction.GetComboValue(cmbLocation) + " AND    (FkRateSettingNo IN  (SELECT PkSrNo  FROM MRateSetting AS MRateSetting_1  WHERE (TStock.ItemNo = MStockItems.ItemNo) AND (MRP = MRateSetting.MRP))) AND (TStock.ItemNo = MStockItems.ItemNo) And PkStockTrnNo Not In (SELECT TStock.PkStockTrnNo FROM TVoucherEntry INNER JOIN  TStock ON TVoucherEntry.PkVoucherNo = TStock.FKVoucherNo WHERE (TVoucherEntry.IsCancel = 'True') AND (TStock.ItemNo = MStockItems.ItemNo))) AS Stock, '' AS stkUOM, 0 AS SaleTax, 0 AS PurTax, " + //MItemTaxInfo_Sale.Percentage AS SaleTax , MItemTaxInfo_Pur.Percentage
                            //    " MStockItems.CompanyNo, MStockBarcode.Barcode, MRateSetting.PkSrNo As RateSettingNo, MStockItems.UOMDefault,MRateSetting.PurRate " +
                            //    " FROM MStockItems_V(NULL," + dtBarCodeItemNo.Rows[i].ItemArray[0].ToString() + ",NULL,NULL,NULL,NULL,NULL) AS MStockItems INNER JOIN " +
                            //    " dbo.GetItemRateAll(" + dtBarCodeItemNo.Rows[i].ItemArray[0].ToString() + ", NULL, NULL, NULL, '" + dtpBillDate.Value.ToString("dd-MMM-yyyy") + " " + DBGetVal.ServerTime.ToLongTimeString() + "',NULL) AS MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo AND  " +
                            //    " MStockItems.UOMDefault = MRateSetting.UOMNo INNER JOIN " +
                            //    " MStockBarcode ON MRateSetting.FkBcdSrNo = MStockBarcode.PkStockBarcodeNo INNER JOIN " +
                            //    " MUOM ON MStockItems.UOMDefault = MUOM.UOMNo  Where  " +
                            //    " MStockItems.IsActive='true' and MStockItems.FkStockGroupTypeNo<>3 ";

                            ItemList += " SELECT MStockItems.ItemNo, MStockItems.ItemName,MStockItems.ItemNameLang, MRateSetting." + ObjFunction.GetComboValueString(cmbRateType) + " AS SaleRate, MUOM.UOMName, MRateSetting.MRP, " +
                                " IsNull(MSB.CurrentStock,0) AS Stock, '' AS stkUOM, 0 AS SaleTax, 0 AS PurTax, " + //MItemTaxInfo_Sale.Percentage AS SaleTax , MItemTaxInfo_Pur.Percentage
                                " MStockItems.CompanyNo, MStockBarcode.Barcode, MRateSetting.PkSrNo As RateSettingNo, MStockItems.UOMDefault,MRateSetting.PurRate " +
                                " FROM MStockItems_V(NULL," + dtBarCodeItemNo.Rows[i].ItemArray[0].ToString() + ",NULL,NULL,NULL,NULL,NULL) AS MStockItems INNER JOIN " +
                                " dbo.GetItemRateAll(" + dtBarCodeItemNo.Rows[i].ItemArray[0].ToString() + ", NULL, NULL, NULL, '" + dtpBillDate.Value.ToString("dd-MMM-yyyy") + " " + DBGetVal.ServerTime.ToLongTimeString() + "',NULL) AS MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo AND  " +
                                " MStockItems.UOMDefault = MRateSetting.UOMNo INNER JOIN " +
                                " MStockBarcode ON MRateSetting.FkBcdSrNo = MStockBarcode.PkStockBarcodeNo INNER JOIN " +
                                " MUOM ON MStockItems.UOMDefault = MUOM.UOMNo LEFT OUTER JOIN MStockItemBalance MSB ON MSB.ItemNo = MStockItems.ItemNo AND MSB.MRP = MRateSetting.MRP AND MSB.GodownNo = " + ObjFunction.GetComboValue(cmbLocation) + "  Where  " +
                                " MStockItems.IsActive='true' and MStockItems.FkStockGroupTypeNo<>3 ";
                        }
                        //if (rdBtnMin.Checked == true)
                        //{
                        //    ItemList += " Where Stock < 0 or Stock < MStockItems.MinLevel";
                        //}
                        ItemList += " ORDER BY MStockItems.ItemName";
                        DataTable dtItemList = ObjFunction.GetDataView(ItemList).Table;
                        if (dtItemList.Rows.Count > 0)
                        {
                            dgItemList.DataSource = dtItemList.DefaultView;
                            pnlItemName.Visible = true;
                            dgItemList.CurrentCell = dgItemList[1, 0];
                            dgItemList.Focus();
                        }
                        else
                        {
                            DisplayMessage("Items Not Found......");
                            dgBill.CurrentCell = dgBill[dgBill.CurrentCell.ColumnIndex, dgBill.CurrentCell.RowIndex];
                            dgBill.Focus();
                        }
                        break;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillItemList(int qNo)
        {
            try
            {
                if (qNo == 0)
                {
                    qNo = iItemNameStartIndex;
                }

                string ItemList = strItemQuery[qNo - 1];
                if (qNo == 2)
                {
                    ItemList = "SELECT MStockItems.ItemNo, Case When(MStockItems.ItemShortCode<>'') then MStockItems.ItemShortCode  else MStockItems.ItemName end AS ItemName, 0 As Quantity, MRateSetting.@cmbRateType@ AS SaleRate, MUOM.UOMName, MRateSetting.MRP,(SELECT  ISNULL(SUM(CASE WHEN trncode = 1 THEN quantity ELSE - quantity END), 0) +  ISNULL(SUM(CASE WHEN trncode = 1 THEN FreeQty ELSE - FreeQty END), 0)   FROM TStock  INNER JOIN TStockGodown ON TSTock.PKStockTrnNo=TStockGodown.FKStockTrnNo   WHERE  TStockGodown.GodownNo=2 AND    (FkRateSettingNo IN  (SELECT PkSrNo  FROM MRateSetting AS MRateSetting_1  WHERE (TStock.ItemNo = MStockItems.ItemNo) AND (MStockItems.FKStockGroupTypeNo <>3) AND (MRP = MRateSetting.MRP))) AND (TStock.ItemNo = MStockItems.ItemNo) And PkStockTrnNo Not In (SELECT TStock.PkStockTrnNo FROM TVoucherEntry INNER JOIN  TStock ON TVoucherEntry.PkVoucherNo = TStock.FKVoucherNo WHERE (TVoucherEntry.IsCancel = 'True') AND (TStock.ItemNo = MStockItems.ItemNo))) - abs((SELECT  ISNULL(SUM(CASE WHEN trncode = 1 THEN quantity ELSE - quantity END * Case When MST.Factorval= 0 Then 1 Else MST.Factorval End), 0) +  ISNULL(SUM(CASE WHEN trncode = 1 THEN FreeQty ELSE - FreeQty END), 0)   FROM TStock  INNER JOIN TStockGodown ON TSTock.PKStockTrnNo=TStockGodown.FKStockTrnNo INNER  JOIN MStockItems MST ON MST.ItemNo=TStock.ItemNo   WHERE  TStockGodown.GodownNo=2 AND    (FkRateSettingNo IN  (SELECT PkSrNo  FROM MRateSetting AS MRateSetting_1  WHERE (MRateSetting_1.ItemNo in ( Select MS.ItemNo from MStockItems MS Where MS.ControlUnder=MStockItems.ItemNo)) )) And PkStockTrnNo Not In (SELECT TStock.PkStockTrnNo FROM TVoucherEntry INNER JOIN  TStock ON TVoucherEntry.PkVoucherNo = TStock.FKVoucherNo WHERE (TVoucherEntry.IsCancel = 'True') AND (TStock.ItemNo in ( Select MS.ItemNo from MStockItems MS Where MS.ControlUnder=MStockItems.ItemNo))))) AS Stock ,MstockItems.MinLevel as MinLevel,MstockItems.MaxLevel as MaxLevel, '' AS stkUOM, 0 AS SaleTax, 0 AS PurTax,  MStockItems.CompanyNo, MStockBarcode.Barcode, MRateSetting.PkSrNo As RateSettingNo, MStockItems.UOMDefault,MRateSetting.PurRate,0.00 AS SuggMin,0.00 AS SuggMax FROM MStockItems INNER JOIN  dbo.GetItemRateAll(NULL, NULL, NULL, NULL, NULL,@Param1NULL@) AS MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo AND   MStockItems.UOMDefault = MRateSetting.UOMNo INNER JOIN  MStockBarcode ON MRateSetting.FkBcdSrNo = MStockBarcode.PkStockBarcodeNo INNER JOIN  MUOM ON MStockItems.UOMDefault = MUOM.UOMNo  Where MStockItems.ItemNo <> 1  And MStockItems.IsActive='true'  AND MStockItems.GroupNo = @Param1@ AND MStockItems.CompanyNo=@CompNo@  ORDER BY ItemName";
                }
                ItemList = ItemList.Replace("@cmbRateType@", "" + ObjFunction.GetComboValueString(cmbRateType));
                ItemList = ItemList.Replace("ORDER BY ItemName", " and MStockItems.FkStockGroupTypeNo<>3 ORDER BY ItemName");
                switch (qNo)
                {
                    case 1:
                        break;
                    case 2:
                        switch (strItemQuery.Length)
                        {
                            case 2:
                                ItemList = ItemList.Replace("@Param1@", "" + (Convert.ToInt64(lstGroup1.SelectedValue) != 0 ? lstGroup1.SelectedValue.ToString() : Param1Value));
                                ItemList = ItemList.Replace("@Param1NULL@", "" + (Convert.ToInt64(lstGroup1.SelectedValue) != 0 ? lstGroup1.SelectedValue.ToString() : "NULL"));
                                break;
                            case 3:
                                ItemList = ItemList.Replace("@Param2@", "" + (Convert.ToInt64(lstGroup2.SelectedValue) != 0 ? lstGroup2.SelectedValue.ToString() : Param2Value));
                                ItemList = ItemList.Replace("@Param2NULL@", "" + (Convert.ToInt64(lstGroup2.SelectedValue) != 0 ? lstGroup2.SelectedValue.ToString() : "NULL"));
                                break;
                        }
                        break;
                    case 3:
                        ItemList = ItemList.Replace("@Param1@", "" + (Convert.ToInt64(lstGroup1.SelectedValue) != 0 ? lstGroup1.SelectedValue.ToString() : Param1Value));
                        ItemList = ItemList.Replace("@Param2@", "" + (Convert.ToInt64(lstGroup2.SelectedValue) != 0 ? lstGroup2.SelectedValue.ToString() : Param2Value));
                        ItemList = ItemList.Replace("@Param1NULL@", "" + (Convert.ToInt64(lstGroup1.SelectedValue) != 0 ? lstGroup1.SelectedValue.ToString() : "NULL"));
                        ItemList = ItemList.Replace("@Param2NULL@", "" + (Convert.ToInt64(lstGroup2.SelectedValue) != 0 ? lstGroup2.SelectedValue.ToString() : "NULL"));
                        break;
                }
                ItemList = ItemList.Replace("@CompNo@", "" + DBGetVal.CompanyNo);
                ItemList = ItemList.Replace("MRateSetting.PurRate AS SaleRate", "Isnull((Select Rate FRom Tstock Where PKStockTrnNo in (Select Max(PKStockTrnNo) FRom Tstock,TVoucherEntry Where TVoucherEntry.PKVoucherNo=TStock.FKVoucherNo AND TVoucherEntry.VoucherTypeCode=" + VchType.Purchase + " AND FKRateSettingNo in(MRateSetting.PkSrNo))),0) AS SaleRate");
                //ItemList = ItemList.Replace("TStockGodown.GodownNo=2", "TStockGodown.GodownNo=" + ObjFunction.GetComboValue(cmbLocation) + "");
                ItemList = ItemList.Replace("TStockGodown.GodownNo=2", "TStockGodown.GodownNo=TStockGodown.GodownNo");
                if (chkFilterItem.Checked == true)
                {
                    ItemList = ItemList.Replace("order by StockGroupName", "" +
                    "AND StockGroupNo in (SELECT MStockGroup.StockGroupNo FROM MManufacturerDetails INNER JOIN MStockGroup ON MManufacturerDetails.ManufacturerNo = MStockGroup.MfgCompNo " +
                    "WHERE (MManufacturerDetails.LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + ") AND (MStockGroup.ControlGroup = 3))");
                }
                if (rdBtnMin.Checked == true && qNo == 2)
                {
                    ItemList = ItemList.Replace("ORDER BY ItemName", " AND ((SELECT  ISNULL(SUM(CASE WHEN trncode = 1 THEN quantity ELSE - quantity END), 0) +  ISNULL(SUM(CASE WHEN trncode = 1 THEN FreeQty ELSE - FreeQty END), 0)   FROM TStock  INNER JOIN TStockGodown ON TSTock.PKStockTrnNo=TStockGodown.FKStockTrnNo   WHERE  TStockGodown.GodownNo=2 AND    (FkRateSettingNo IN  (SELECT PkSrNo  FROM MRateSetting AS MRateSetting_1  WHERE (TStock.ItemNo = MStockItems.ItemNo) AND (MStockItems.FKStockGroupTypeNo <>3) AND (MRP = MRateSetting.MRP))) AND (TStock.ItemNo = MStockItems.ItemNo) And PkStockTrnNo Not In (SELECT TStock.PkStockTrnNo FROM TVoucherEntry INNER JOIN  TStock ON TVoucherEntry.PkVoucherNo = TStock.FKVoucherNo WHERE (TVoucherEntry.IsCancel = 'True') AND (TStock.ItemNo = MStockItems.ItemNo))) - abs((SELECT  ISNULL(SUM(CASE WHEN trncode = 1 THEN quantity ELSE - quantity END * Case When MST.Factorval= 0 Then 1 Else MST.Factorval End), 0) +  ISNULL(SUM(CASE WHEN trncode = 1 THEN FreeQty ELSE - FreeQty END), 0)   FROM TStock  INNER JOIN TStockGodown ON TSTock.PKStockTrnNo=TStockGodown.FKStockTrnNo INNER  JOIN MStockItems MST ON MST.ItemNo=TStock.ItemNo   WHERE  TStockGodown.GodownNo=2 AND    (FkRateSettingNo IN  (SELECT PkSrNo  FROM MRateSetting AS MRateSetting_1  WHERE (MRateSetting_1.ItemNo in ( Select MS.ItemNo from MStockItems MS Where MS.ControlUnder=MStockItems.ItemNo)) )) And PkStockTrnNo Not In (SELECT TStock.PkStockTrnNo FROM TVoucherEntry INNER JOIN  TStock ON TVoucherEntry.PkVoucherNo = TStock.FKVoucherNo WHERE (TVoucherEntry.IsCancel = 'True') AND (TStock.ItemNo in ( Select MS.ItemNo from MStockItems MS Where MS.ControlUnder=MStockItems.ItemNo)))))) < MStockItems.MinLevel ORDER BY ItemName");
                    //                    ItemList += " Where Stock < 0 or Stock < MStockItems.MinLevel";
                }
                switch (strItemQuery.Length - qNo)
                {
                    case 0:
                        if (!ItemList.Equals(strItemQuery_last[qNo - 1], StringComparison.CurrentCultureIgnoreCase))
                        {
                            ItemList = ItemList.Replace("AS ItemName,", "AS ItemName,Case When(MStockItems.LangShortDesc<>'') then MStockItems.LangShortDesc else MStockItems.LangFullDesc end AS ItemNameLang,");
                            DataTable dtItemList = ObjFunction.GetDataView(ItemList).Table;
                            if (dtItemList.Rows.Count > 0)
                            {
                                dgItemList.DataSource = dtItemList.DefaultView;
                                pnlItemName.Visible = true;
                                if (dgItemList.Rows.Count > 0)
                                {
                                    for (int k = 0; k < dgItemList.Rows.Count; k++)
                                    {
                                        dgItemList.Rows[k].Cells[18].Value = Convert.ToDouble(Convert.ToDouble(dgItemList.Rows[k].Cells[7].Value) - Convert.ToDouble(dgItemList.Rows[k].Cells[8].Value)).ToString(Format.DoubleFloating);
                                        dgItemList.Rows[k].Cells[19].Value = Convert.ToDouble(Convert.ToDouble(dgItemList.Rows[k].Cells[7].Value) - Convert.ToDouble(dgItemList.Rows[k].Cells[9].Value)).ToString(Format.DoubleFloating);
                                    }
                                }
                                dgItemList.CurrentCell = dgItemList[3, 0];
                                dgItemList.Focus();
                            }
                            else
                            {
                                DisplayMessage("Items Not Found......");
                                dgBill.CurrentCell = dgBill[dgBill.CurrentCell.ColumnIndex, dgBill.CurrentCell.RowIndex];
                                dgBill.Focus();
                            }
                        }
                        else
                        {
                            pnlItemName.Visible = true;
                            dgItemList.CurrentCell = dgItemList[1, 0];
                            dgItemList.Focus();
                        }
                        break;
                    case 1:
                        //if (!ItemList.Equals(strItemQuery_last[qNo - 1], StringComparison.CurrentCultureIgnoreCase))
                        //{

                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_Bilingual)) == true)
                            ObjFunction.FillList(lstGroup1Lang, ItemList.Replace("StockGroupName from", "LanguageName from"));

                        ObjFunction.FillList(lstGroup1, ItemList);
                        strItemQuery_last[qNo - 1] = ItemList;
                        //ObjFunction.FillList(lstGroup1, ItemList);
                        //strItemQuery_last[qNo - 1] = ItemList;
                        //}
                        if (lstGroup1.Items.Count > 0)
                        {
                            pnlGroup1.Visible = true;
                            lstGroup1.Focus();
                        }
                        else
                        {
                            DisplayMessage("Brands Not Found......");
                            dgBill.CurrentCell = dgBill[dgBill.CurrentCell.ColumnIndex, dgBill.CurrentCell.RowIndex];
                            dgBill.Focus();
                        }
                        break;
                    case 2:
                        if (!ItemList.Equals(strItemQuery_last[qNo - 1], StringComparison.CurrentCultureIgnoreCase))
                        {
                            ObjFunction.FillList(lstGroup2, ItemList);
                            strItemQuery_last[qNo - 1] = ItemList;
                        }
                        pnlGroup2.Visible = true;
                        lstGroup2.Focus();
                        break;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillItemList()
        {
            try
            {
                string ItemList = "";
                if (ItemType == 3)
                {
                    ItemList = " SELECT MStockItems.ItemNo, MStockItems.ItemName,MStockItems.ItemNameLang, MRateSetting." + ObjFunction.GetComboValueString(cmbRateType) + " AS SaleRate, MUOM.UOMName, MRateSetting.MRP, " +
                        " (SELECT  ISNULL(SUM(CASE WHEN trncode = 1 THEN quantity ELSE - quantity END), 0) +  ISNULL(SUM(CASE WHEN trncode = 1 THEN FreeQty ELSE - FreeQty END), 0)   FROM TStock  INNER JOIN TStockGodown ON TSTock.PKStockTrnNo=TStockGodown.FKStockTrnNo   WHERE  TStockGodown.GodownNo=" + ObjFunction.GetComboValue(cmbLocation) + " AND    (FkRateSettingNo IN  (SELECT PkSrNo  FROM MRateSetting AS MRateSetting_1  WHERE (TStock.ItemNo = MStockItems.ItemNo) AND (MRP = MRateSetting.MRP))) AND (TStock.ItemNo = MStockItems.ItemNo) And PkStockTrnNo Not In (SELECT TStock.PkStockTrnNo FROM TVoucherEntry INNER JOIN  TStock ON TVoucherEntry.PkVoucherNo = TStock.FKVoucherNo WHERE (TVoucherEntry.IsCancel = 'True') AND (TStock.ItemNo = MStockItems.ItemNo))) AS Stock, '' AS stkUOM, 0 AS SaleTax, 0 AS PurTax, " + //MItemTaxInfo_Sale.Percentage AS SaleTax , MItemTaxInfo_Pur.Percentage
                        " MStockItems.CompanyNo, MStockBarcode.Barcode, MRateSetting.PkSrNo As RateSettingNo, MStockItems.UOMDefault,MRateSetting.PurRate " +
                        " FROM MStockItems_V(NULL,NULL,NULL,NULL,NULL,NULL,NULL) AS MStockItems INNER JOIN " +
                        " dbo.GetItemRateAll(NULL, NULL, NULL, NULL, NULL,NULL) AS MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo AND  " +
                        " MStockItems.UOMDefault = MRateSetting.UOMNo INNER JOIN " +
                        " MStockBarcode ON MRateSetting.FkBcdSrNo = MStockBarcode.PkStockBarcodeNo INNER JOIN " +
                        // " dbo.GetItemTaxAll(NULL, NULL, 10," + ObjFunction.GetComboValue(cmbTaxType) + ",NULL) AS MItemTaxInfo_Sale ON MStockItems.ItemNo = MItemTaxInfo_Sale.ItemNo INNER JOIN " +
                        // " dbo.GetItemTaxAll(NULL, NULL, 11," + ObjFunction.GetComboValue(cmbTaxType) + ",NULL) AS MItemTaxInfo_Pur ON MStockItems.ItemNo = MItemTaxInfo_Pur.ItemNo INNER JOIN " +
                        " MUOM ON MStockItems.UOMDefault = MUOM.UOMNo  Where MStockItems.ItemNo in " +
                        "(Select ItemNo from MStockBarcode where Barcode = '" + dgBill.CurrentCell.Value + "' And IsActive ='true') AND MStockItems.CompanyNo=" + DBGetVal.CompanyNo + " AND MStockItems.IsActive='true' and MStockItems.FkStockGroupTypeNo<>3" +
                        " ORDER BY MStockItems.ItemName";
                    DataTable dtItemList = ObjFunction.GetDataView(ItemList).Table;
                    if (dtItemList.Rows.Count > 0)
                    {
                        dgItemList.DataSource = dtItemList.DefaultView;
                        pnlItemName.Visible = true;
                        dgItemList.CurrentCell = dgItemList[1, 0];
                        dgItemList.Focus();
                    }
                    else
                    {
                        DisplayMessage("Items Not Found......");
                        dgBill.CurrentCell = dgBill[dgBill.CurrentCell.ColumnIndex, dgBill.CurrentCell.RowIndex];
                        dgBill.Focus();
                    }
                }
                else if (ItemNameType == 1)
                {
                    ItemList = " SELECT MStockItems.ItemNo, MStockItems.ItemName,MStockItems.ItemNameLang, MRateSetting." + ObjFunction.GetComboValueString(cmbRateType) + " AS SaleRate, MUOM.UOMName, MRateSetting.MRP, " +
                            " (SELECT  ISNULL(SUM(CASE WHEN trncode = 1 THEN quantity ELSE - quantity END), 0) +  ISNULL(SUM(CASE WHEN trncode = 1 THEN FreeQty ELSE - FreeQty END), 0)   FROM TStock  INNER JOIN TStockGodown ON TSTock.PKStockTrnNo=TStockGodown.FKStockTrnNo   WHERE  TStockGodown.GodownNo=" + ObjFunction.GetComboValue(cmbLocation) + " AND    (FkRateSettingNo IN  (SELECT PkSrNo  FROM MRateSetting AS MRateSetting_1  WHERE (TStock.ItemNo = MStockItems.ItemNo) AND (MRP = MRateSetting.MRP))) AND (TStock.ItemNo = MStockItems.ItemNo) And PkStockTrnNo Not In (SELECT TStock.PkStockTrnNo FROM TVoucherEntry INNER JOIN  TStock ON TVoucherEntry.PkVoucherNo = TStock.FKVoucherNo WHERE (TVoucherEntry.IsCancel = 'True') AND (TStock.ItemNo = MStockItems.ItemNo))) AS Stock, '' AS stkUOM, 0 AS SaleTax, 0 AS PurTax, " +//MItemTaxInfo_Sale.Percentage,MItemTaxInfo_Pur.Percentage
                            " MStockItems.CompanyNo, MStockBarcode.Barcode, MRateSetting.PkSrNo As RateSettingNo, MStockItems.UOMDefault,MRateSetting.PurRate " +
                            " FROM MStockItems INNER JOIN " +
                            " dbo.GetItemRateAll(NULL, NULL, NULL, NULL, NULL, NULL) AS MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo AND  " +
                            " MStockItems.UOMDefault = MRateSetting.UOMNo INNER JOIN " +
                            " MStockBarcode ON MRateSetting.FkBcdSrNo = MStockBarcode.PkStockBarcodeNo INNER JOIN " +
                        //" dbo.GetItemTaxAll(NULL, NULL, 10," + ObjFunction.GetComboValue(cmbTaxType) + ", NULL) AS MItemTaxInfo_Sale ON MStockItems.ItemNo = MItemTaxInfo_Sale.ItemNo INNER JOIN " +
                        //" dbo.GetItemTaxAll(NULL, NULL, 11," + ObjFunction.GetComboValue(cmbTaxType) + ", NULL) AS MItemTaxInfo_Pur ON MStockItems.ItemNo = MItemTaxInfo_Pur.ItemNo INNER JOIN " +
                            " MUOM ON MStockItems.UOMDefault = MUOM.UOMNo Where MStockItems.ItemNo <> 1 AND MStockItems.CompanyNo=" + DBGetVal.CompanyNo + " and MStockItems.FkStockGroupTypeNo<>3 " +
                            " ORDER BY MStockItems.ItemName";
                    DataTable dtItemList = ObjFunction.GetDataView(ItemList).Table;
                    if (dtItemList.Rows.Count > 0)
                    {
                        dgItemList.DataSource = dtItemList.DefaultView;
                        pnlItemName.Visible = true;
                        dgItemList.CurrentCell = dgItemList[1, 0];
                        dgItemList.Focus();
                    }
                    else
                    {
                        DisplayMessage("Items Not Found......");
                        dgBill.CurrentCell = dgBill[dgBill.CurrentCell.ColumnIndex, dgBill.CurrentCell.RowIndex];
                        dgBill.Focus();
                    }

                }
                else if (ItemNameType == 2)
                {
                    ItemList = "Select StockGroupNo,StockGroupName from MStockGroup where StockGroupNo in (select Distinct GroupNo from MStockItems WHERE ItemNo<>1 AND CompanyNo=" + DBGetVal.CompanyNo + ") order by StockGroupName";
                    ObjFunction.FillList(lstGroup1, ItemList);
                    pnlGroup1.Visible = true;
                    lstGroup1.Focus();
                    //lstGroup1.Location = new Point(dgBill.Left + dgBill.CurrentCell.ContentBounds.Left, dgBill.Top + dgBill.CurrentRow.Height + dgBill.CurrentCell.ContentBounds.Top + dgBill.ColumnHeadersHeight);
                }
                else if (ItemNameType == 3)
                {
                    ItemList = "Select StockGroupNo,StockGroupName from MStockGroup where StockGroupNo in (select Distinct GroupNo1 from MStockItems WHERE ItemNo<>1 AND CompanyNo=" + DBGetVal.CompanyNo + ") order by StockGroupName";
                    ObjFunction.FillList(lstGroup2, ItemList);
                    pnlGroup2.Visible = true;
                    lstGroup2.Focus();
                }
                else if (ItemNameType == 4)
                {
                    ItemList = "Select StockGroupNo,StockGroupName from MStockGroup where StockGroupNo in (select Distinct GroupNo from MStockItems WHERE ItemNo<>1 AND CompanyNo=" + DBGetVal.CompanyNo + ") order by StockGroupName";
                    ObjFunction.FillList(lstGroup1, ItemList);
                    pnlGroup1.Visible = true;
                    lstGroup1.Focus();
                }
                dgBill.Enabled = false;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void lstUOM_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                UOM_MoveNext();
            }
            else if (e.KeyChar == ' ')
            {
                dgBill.Focus();
                pnlUOM.Visible = false;
            }
        }

        private void lstGroup1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //string ItemListStr = "";
            //if (e.KeyChar == 13)
            //{
            //    pnlGroup1.Visible = false;

            //    FillItemList(strItemQuery.Length);
            //}
            //else if (e.KeyChar == ' ')
            //{
            //    dgBill.Focus();
            //    pnlGroup1.Visible = false;
            //}
        }

        private void lstGroup2_KeyPress(object sender, KeyPressEventArgs e)
        {
            //string ItemListStr = "";
            //if (e.KeyChar == 13)
            //{
            //    pnlGroup2.Visible = false;

            //    FillItemList(strItemQuery.Length - 1);
            //}
            //else if (e.KeyChar == ' ')
            //{
            //    dgBill.Focus();
            //    pnlGroup2.Visible = false;
            //}
        }

        private void lstRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                dgBill.CurrentRow.Cells[ColIndex.Rate].Value = lstRate.Text;
                dgBill.CurrentRow.Cells[ColIndex.PkRateSettingNo].Value = lstRate.SelectedValue;
                Rate_MoveNext();
                pnlRate.Visible = false;
            }
            else if (e.KeyChar == ' ')
            {
                dgBill.Focus();
                pnlRate.Visible = false;
            }
        }

        private void FillUOMList(int RowIndex)
        {
            try
            {
                ObjFunction.FillList(lstUOM, "UomNo", "UomName");

                if (ItemType == 2)
                    strUom = " Select * From GetUomList ('" + Convert.ToString(dgBill.Rows[RowIndex].Cells[ColIndex.Barcode].Value) + "',0," + Convert.ToDouble(dgBill.Rows[RowIndex].Cells[ColIndex.Quantity].Value) + ")";
                else
                    strUom = " Select * From GetUomList (''," + Convert.ToInt64(dgBill.Rows[RowIndex].Cells[ColIndex.ItemNo].Value) + "," + Convert.ToDouble(dgBill.Rows[RowIndex].Cells[ColIndex.Quantity].Value) + ")";

                ObjFunction.FillList(lstUOM, strUom);

                if (lstUOM.Items.Count <= 1)
                {
                    //dgBill.Rows[RowIndex].Cells[3].Value = lstUOM.Text;
                    //dgBill.Rows[RowIndex].Cells[ColIndex.UOMNo].Value = Convert.ToInt64(lstUOM.SelectedValue);
                    if (lstUOM.Items.Count == 1)
                    {
                        lstUOM.SelectedIndex = 0;
                        UOM_MoveNext();
                    }
                }
                else
                {
                    CalculateTotal();
                    pnlUOM.Visible = true;
                    lstUOM.Focus();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #endregion

        private void lst_VisibleChanged(object sender, EventArgs e)
        {
            if (((System.Windows.Forms.Control)sender).Visible == true)
                dgBill.Enabled = false;
            else
            {
                dgBill.Enabled = true;
                dgBill.Focus();
            }
            if (((System.Windows.Forms.Control)sender).Name == "pnlItemName")
            {
                if (((System.Windows.Forms.Control)sender).Visible == false)
                    pnlSalePurHistory.Visible = ((System.Windows.Forms.Control)sender).Visible;
            }
        }

        private void dgBill_CurrentCellChanged(object sender, EventArgs e)
        {
            if (dgBill.CurrentCell != null)
            {
                for (int i = 0; i < dgBill.Rows.Count; i++)
                {
                    dgBill.Rows[i].DefaultCellStyle.BackColor = Color.White;
                }
                dgBill.Rows[dgBill.CurrentCell.RowIndex].DefaultCellStyle.BackColor = clrColorRow;
                dgBill.CurrentCell.Style.SelectionBackColor = Color.LightCyan;
            }
        }

        #region ColumnIndex
        private static class ColIndex
        {
            public static int SrNo = 0;
            public static int ItemName = 1;
            public static int Quantity = 2;
            public static int UOM = 3;
            public static int Rate = 4;
            public static int MRP = 5;
            public static int NetRate = 6;
            public static int FreeQty = 7;
            public static int FreeUOM = 8;
            public static int DiscPercentage = 9;
            public static int DiscAmount = 10;
            public static int DiscRupees = 11;
            public static int DiscPercentage2 = 12;
            public static int DiscAmount2 = 13;
            public static int NetAmt = 14;
            public static int TaxPercentage = 15;
            public static int TaxAmount = 16;
            public static int DiscRupees2 = 17;
            public static int Amount = 18;
            public static int Barcode = 19;
            public static int PkStockTrnNo = 20;
            public static int PkBarCodeNo = 21;
            public static int PkVoucherNo = 22;
            public static int ItemNo = 23;
            public static int UOMNo = 24;
            public static int TaxLedgerNo = 25;
            public static int SalesLedgerNo = 26;
            public static int PkRateSettingNo = 27;
            public static int PkItemTaxInfo = 28;
            public static int StockFactor = 29;
            public static int ActualQty = 30;
            public static int MKTQuantity = 31;
            public static int SalesVchNo = 32;
            public static int TaxVchNo = 33;
            public static int StockCompanyNo = 34;
            public static int BarcodePrint = 35;
            public static int FreeUomNo = 36;
            public static int TempMRP = 37;
            public static int LandedRate = 38;
            public static int BalanceQty = 39;
        }
        #endregion

        private long GetVoucherPK(string expression)
        {
            long strVal = 0;
            try
            {
                if (dtVchMainDetails.Rows.Count > 0)
                {
                    DataRow[] result = dtVchMainDetails.Select(expression);
                    strVal = Convert.ToInt64(result[0].ItemArray[0].ToString());
                }
            }
            catch (Exception e)
            {
                strVal = 0;
                CommonFunctions.ErrorMessge = e.Message;
            }
            return strVal;
        }

        #region Receipt Grid Methods
        private void BindGridPayType(long ID)
        {
            DataTable dtPayType = new DataTable();
            dtPayLedger = ObjFunction.GetDataView("Select * From MPayTypeLedger Where PayTypeNo in(2,3,4,5)").Table;
            string sqlQuery = "";
            if (ID == 0)
                sqlQuery = "SELECT PayTypeName, PKPayTypeNo, Cast(0.00 as varchar) AS Amount,0 As LedgerNo, 0 AS PKVoucherPayTypeNo FROM MPayType ORDER BY PKPayTypeNo";
            else
                sqlQuery = "SELECT PayTypeName, PKPayTypeNo,Cast( IsNull((SELECT SUM(Amount) FROM TVoucherPayTypeDetails WHERE (FKSalesVoucherNo = " + ID + ") AND (FKPayTypeNo = PKPayTypeNo)),0) AS varchar) AS Amount,0 As LedgerNo, 0 AS PKVoucherPayTypeNo FROM MPayType  ORDER BY PKPayTypeNo";

            dtPayType = ObjFunction.GetDataView(sqlQuery).Table;
            while (dgPayType.Columns.Count > 0)
                dgPayType.Columns.RemoveAt(0);
            dgPayType.DataSource = dtPayType.DefaultView;
            for (int i = 0; i < dgPayType.Columns.Count; i++)
                dgPayType.Columns[i].Visible = false;
            dgPayType.Columns[0].Visible = true;
            dgPayType.Columns[2].Visible = true;
            dgPayType.Rows[0].Visible = false;
            dgPayType.Columns[0].Width = 150;
            dgPayType.Columns[2].Width = 100;
            dgPayType.Columns[0].ReadOnly = true;
            dgPayType.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgPayType.Rows[0].Visible = false;
            if (ID != 0)
            {
                if (ObjFunction.GetComboValue(cmbPaymentType) == 1 || ObjFunction.GetComboValue(cmbPaymentType) == 4 || ObjFunction.GetComboValue(cmbPaymentType) == 5)
                {
                    dgPayType.Rows[3].Cells[2].Value = ObjQry.ReturnDouble("Select Sum(Amount) From TVoucherChqCreditDetails Where FKVoucherNo=" + ID + "  AND ChequeNo <>''", CommonFunctions.ConStr).ToString("0.00");
                    dgPayType.Rows[4].Cells[2].Value = ObjQry.ReturnDouble("Select Sum(Amount) From TVoucherChqCreditDetails Where FKVoucherNo=" + ID + "  AND CreditCardNo <>''", CommonFunctions.ConStr).ToString("0.00");
                }

                double RefAmt = ObjQry.ReturnDouble("Select Sum(Amount) From TVoucherRefDetails Where FKVucherTrnNo in (Select PKVoucherTrnNo From TVoucherDetails Where FkVoucherNo=" + ID + ")", CommonFunctions.ConStr);
                if (RefAmt > 0)
                {
                    dgPayType.Rows[2].Cells[2].Value = RefAmt;
                }
            }
            CaluculatePayType();
            pnlPartial.Visible = false;

        }

        private void dgPayType_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                if (dgPayType.CurrentCell.Value == null) dgPayType.CurrentCell.Value = "0";
                if (ObjFunction.CheckValidAmount(dgPayType.CurrentCell.Value.ToString()) == false)
                {
                    dgPayType.CurrentCell.Value = "0.00";
                    OMMessageBox.Show("Please Enter valid amount..", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    MovetoNext move2n = new MovetoNext(m2n);
                    BeginInvoke(move2n, new object[] { dgPayType.CurrentCell.RowIndex, 2, dgPayType });
                }
                else
                {
                    dgPayType.CurrentCell.ErrorText = "";
                    dgPayType.CurrentCell.Value = Convert.ToDouble(dgPayType.CurrentCell.Value).ToString("0.00");
                    CaluculatePayType();

                    if (dgPayType.CurrentCell.RowIndex == 3)
                    {
                        if (Convert.ToDouble(dgPayType.Rows[3].Cells[2].Value) <= 0)
                        {
                            dgPayChqDetails.Rows.Clear();
                            dgPayChqDetails.Rows.Add();
                        }
                        else
                        {
                            pnlPartial.Size = new Size(775, 214);
                            pnlPartial.Location = new Point(20, 123);
                            dgPayChqDetails.Visible = true;
                            dgPayChqDetails.BringToFront();
                            dgPayChqDetails.Focus();
                            dgPayCreditCardDetails.Visible = false;
                            if (dgPayChqDetails.Rows.Count == 0)
                            {
                                dgPayChqDetails.Rows.Add();
                                dgPayChqDetails.CurrentCell = dgPayChqDetails[0, 0];
                            }
                        }
                    }
                    if (dgPayType.CurrentCell.RowIndex == 4)
                    {
                        if (Convert.ToDouble(dgPayType.Rows[4].Cells[2].Value) <= 0)
                        {
                            dgPayCreditCardDetails.Rows.Clear();
                            dgPayCreditCardDetails.Rows.Add();
                        }
                        else
                        {
                            pnlPartial.Size = new Size(775, 214);
                            pnlPartial.Location = new Point(20, 123);
                            dgPayCreditCardDetails.Visible = true;
                            dgPayCreditCardDetails.Focus();
                            dgPayCreditCardDetails.BringToFront();
                            dgPayChqDetails.Visible = false;
                            if (dgPayCreditCardDetails.Rows.Count == 0)
                            {
                                dgPayCreditCardDetails.Rows.Add();
                                dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[0, 0];
                            }
                        }
                    }
                }
            }
        }

        private void dgPayType_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (dgPayType.CurrentCell.ColumnIndex == 2)
            {
                TextBox txt = (TextBox)e.Control;
                txt.KeyDown += new KeyEventHandler(txtAmt_KeyDown);
            }
        }

        private void txtAmt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                if (ObjFunction.CheckValidAmount(dgPayType.CurrentCell.Value.ToString()) == false)
                {
                    OMMessageBox.Show("Please Enter valid amount..", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    dgPayType.CurrentCell = dgPayType[2, dgPayType.CurrentCell.RowIndex];
                }
            }
        }

        private void CaluculatePayType()
        {
            double TotAmt = 0;
            for (int i = 0; i < dgPayType.Rows.Count; i++)
            {
                if (dgPayType.Rows[i].Cells[2].Value == null) dgPayType.Rows[i].Cells[2].Value = "0";
                TotAmt += Convert.ToDouble(dgPayType.Rows[i].Cells[2].Value);
            }
            txtTotalAmt.Text = TotAmt.ToString("0.00");
            if (txtGrandTotal.Text != "")
                lblPayTypeBal.Text = (Convert.ToDouble(txtGrandTotal.Text) - TotAmt).ToString("0.00");
            else
                lblPayTypeBal.Text = "0.00";
        }

        private void FillPayType()
        {
            int cntflag = 0;
            for (int i = 0; i < dgPayType.Rows.Count; i++)
            {
                if (Convert.ToDouble(dgPayType.Rows[i].Cells[2].Value) > 0)
                    cntflag += 1;
                if (cntflag > 1) break;
            }
            if (cntflag > 1) cmbPaymentType.SelectedValue = "1";

            long PayType = ObjFunction.GetComboValue(cmbPaymentType);
            //for (int i = 0; i < dtCompRatio.Rows.Count; i++)
            //{
            //    dgPayType.Columns.Add(dtCompRatio.Rows[i].ItemArray[0].ToString(), dtCompRatio.Rows[i].ItemArray[0].ToString());
            //    dgPayType.Columns[dgPayType.Columns.Count - 1].Visible = false;
            //}

            if (PayType != 1)
            {
                for (int i = 0; i < dgPayType.Rows.Count; i++)
                {
                    if (PayType == Convert.ToInt64(dgPayType.Rows[i].Cells[1].Value))
                    {
                        dgPayType.Rows[i].Cells[2].Value = txtGrandTotal.Text;
                        // for (int j = 0; j < dtCompRatio.Rows.Count; j++)
                        //{
                        dgPayType.Rows[i].Cells[4].Value = Convert.ToDouble(txtGrandTotal.Text);// * Convert.ToDouble(dtCompRatio.Rows[j].ItemArray[1].ToString())) / 10;
                        //}
                    }
                }
            }
            else
            {
                for (int i = 0; i < dgPayType.Rows.Count; i++)
                {
                    // for (int j = 0; j < dtCompRatio.Rows.Count; j++)
                    //{
                    dgPayType.Rows[i].Cells[4].Value = Convert.ToDouble(dgPayType.Rows[i].Cells[2].Value);// * Convert.ToDouble(dtCompRatio.Rows[j].ItemArray[1].ToString())) / 10;
                    //}
                }
            }
        }
        //public void FillPayType()
        //{
        //    //int cntflag = 0;
        //    //for (int i = 0; i < dgPayType.Rows.Count; i++)
        //    //{
        //    //    if (Convert.ToDouble(dgPayType.Rows[i].Cells[2].Value) > 0)
        //    //        cntflag += 1;
        //    //    if (cntflag > 1) break;
        //    //}
        //    //if (cntflag > 1) cmbPaymentType.SelectedValue = "1";

        //    //long PayType = ObjFunction.GetComboValue(cmbPaymentType);
        //    //for (int i = 0; i < dtCompRatio.Rows.Count; i++)
        //    //{
        //    //    dgPayType.Columns.Add(dtCompRatio.Rows[i].ItemArray[0].ToString(), dtCompRatio.Rows[i].ItemArray[0].ToString());
        //    //    dgPayType.Columns[dgPayType.Columns.Count - 1].Visible = false;
        //    //}

        //    //if (PayType != 1)
        //    //{
        //    //    for (int i = 0; i < dgPayType.Rows.Count; i++)
        //    //    {
        //    //        if (PayType == Convert.ToInt64(dgPayType.Rows[i].Cells[1].Value))
        //    //        {
        //    //            dgPayType.Rows[i].Cells[2].Value = txtGrandTotal.Text;
        //    //            for (int j = 0; j < dtCompRatio.Rows.Count; j++)
        //    //            {
        //    //                dgPayType.Rows[i].Cells[4 + (j + 1)].Value = (Convert.ToDouble(txtGrandTotal.Text) * Convert.ToDouble(dtCompRatio.Rows[j].ItemArray[1].ToString())) / 10;
        //    //            }
        //    //        }
        //    //    }
        //    //}
        //    //else
        //    //{
        //    //    for (int i = 0; i < dgPayType.Rows.Count; i++)
        //    //    {
        //    //        for (int j = 0; j < dtCompRatio.Rows.Count; j++)
        //    //        {
        //    //            dgPayType.Rows[i].Cells[4 + (j + 1)].Value = (Convert.ToDouble(dgPayType.Rows[i].Cells[2].Value) * Convert.ToDouble(dtCompRatio.Rows[j].ItemArray[1].ToString())) / 10;
        //    //        }
        //    //    }
        //    //}
        //}

        private void dgPayType_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btnOk_Click(sender, new EventArgs());
            }
            else if (e.KeyCode == Keys.D && e.Control)
            {
                if (dgPayType.CurrentCell.RowIndex == 3)
                {
                    if (Convert.ToDouble(dgPayType.Rows[dgPayType.CurrentCell.RowIndex].Cells[2].Value) > 0)
                    {
                        pnlPartial.Size = new Size(775, 214);
                        pnlPartial.Location = new Point(20, 123);
                        dgPayChqDetails.Visible = true;
                        dgPayChqDetails.BringToFront();
                        dgPayChqDetails.Focus();
                        dgPayCreditCardDetails.Visible = false;
                        if (dgPayChqDetails.Rows.Count == 0)
                        {
                            dgPayChqDetails.Rows.Add();
                            dgPayChqDetails.CurrentCell = dgPayChqDetails[0, 0];
                        }
                    }
                }
                else if (dgPayType.CurrentCell.RowIndex == 4)
                {
                    if (Convert.ToDouble(dgPayType.Rows[dgPayType.CurrentCell.RowIndex].Cells[2].Value) > 0)
                    {
                        pnlPartial.Size = new Size(775, 214);
                        pnlPartial.Location = new Point(20, 123);
                        dgPayCreditCardDetails.Visible = true;
                        dgPayCreditCardDetails.Focus();
                        dgPayCreditCardDetails.BringToFront();
                        dgPayChqDetails.Visible = false;
                        if (dgPayCreditCardDetails.Rows.Count == 0)
                        {
                            dgPayCreditCardDetails.Rows.Add();
                            dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[0, 0];
                        }
                    }
                }
            }
        }

        #endregion

        #region Datagrid ItemList Methods

        private void dgItemList_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    long i = Convert.ToInt64(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[0].Value);

                    dgBill.CurrentRow.Cells[ColIndex.UOMNo].Value = Convert.ToInt64(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[16].Value);
                    dgBill.CurrentRow.Cells[ColIndex.Rate].Value = Convert.ToDouble(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[4].Value).ToString("0.00");//lstRate.Text;
                    dgBill.CurrentRow.Cells[ColIndex.PkRateSettingNo].Value = Convert.ToInt64(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[15].Value);//lstRate.SelectedValue;
                    dgBill.CurrentRow.Cells[ColIndex.MRP].Value = Convert.ToDouble(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[6].Value).ToString("0.00");
                    dgBill.CurrentRow.Cells[ColIndex.TempMRP].Value = Convert.ToDouble(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[6].Value).ToString("0.00");
                    dgBill.CurrentRow.Cells[ColIndex.UOM].Value = dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[5].Value;
                    dgBill.CurrentRow.Cells[ColIndex.Quantity].Value = dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[3].Value;

                    pnlItemName.Visible = false;
                    Desc_MoveNext(i, 0);
                    if (dgItemList.CurrentCell.ColumnIndex == ColIndex.Quantity)
                    {
                        if (dgItemList.CurrentCell.RowIndex < dgItemList.Rows.Count - 1)
                        {
                            dgItemList.CurrentCell = dgItemList.Rows[dgItemList.CurrentCell.RowIndex + 1].Cells[ColIndex.Quantity];
                        }
                        else
                        {
                            btnOk1.Focus();
                        }
                    }
                }
                else if (e.KeyCode == Keys.Space)
                {
                    pnlItemName.Visible = false;
                    if (strItemQuery.Length > 1)
                    {
                        pnlGroup1.Visible = true;
                        lstGroup1.Focus();
                    }
                    else
                    {
                        dgBill.Focus();
                    }
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    e.SuppressKeyPress = true;
                    btnOk1.Focus();
                }
                else if (e.KeyCode == Keys.F6)
                {

                    pnlSalePurHistory.Visible = !pnlSalePurHistory.Visible;
                    if (pnlSalePurHistory.Visible == true)
                    {

                        pnlSalePurHistory.Location = new Point(88, 235 + 88);
                        pnlSalePurHistory.Size = new Size(692, 287);
                        DataSet ds = ObjDset.FillDset("New", "Exec GetLastSalePurchaseDetails " + ObjFunction.GetComboValue(cmbPartyName) + "," + dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[0].Value.ToString() + " ", CommonFunctions.ConStr);

                        if (ds.Tables.Count > 0)
                        {
                            dgSaleHistory.DataSource = ds.Tables[0].DefaultView;
                            dgPurHistory.DataSource = ds.Tables[1].DefaultView;

                            if (dgSaleHistory.Rows.Count > 0)
                            {
                                dgSaleHistory.Location = new Point(8, 8);
                                dgSaleHistory.Size = new Size(323, 150);
                                dgSaleHistory.Columns[0].Width = 70;
                                dgSaleHistory.Columns[1].Width = 45;
                                dgSaleHistory.Columns[2].Width = 70; dgSaleHistory.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                                dgSaleHistory.Columns[3].Width = 50; dgSaleHistory.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                                dgSaleHistory.Columns[4].Width = 85;
                            }

                            if (dgPurHistory.Rows.Count > 0)
                            {
                                dgPurHistory.Location = new Point(337, 8);
                                dgPurHistory.Size = new Size(339, 150);
                                dgPurHistory.Columns[0].Width = 70;
                                dgPurHistory.Columns[1].Width = 45;
                                dgPurHistory.Columns[2].Width = 70; dgPurHistory.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                                dgPurHistory.Columns[3].Width = 50; dgPurHistory.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                                dgPurHistory.Columns[4].Width = 85;
                            }

                        }
                        else
                        {
                            dgSaleHistory.Rows.Clear();
                            dgPurHistory.Rows.Clear();
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgItemList_CurrentCellChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgItemList.CurrentCell != null)
                {
                    for (int i = 0; i < dgItemList.Rows.Count; i++)
                    {
                        dgItemList.Rows[i].DefaultCellStyle.BackColor = Color.White;
                    }
                    dgItemList.Rows[dgItemList.CurrentCell.RowIndex].DefaultCellStyle.BackColor = clrColorRow;
                    dgItemList.CurrentCell.Style.SelectionBackColor = Color.LightCyan;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }
        #endregion

        #region Rate Type Realted Methods and Functions
        private void FillRateType()
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("RateType");
                dt.Columns.Add("RateTypeName");
                DataRow dr = null;

                //dr = dt.NewRow();
                //dr[0] = "----Select----";
                //dr[1] = "0";
                //dt.Rows.Add(dr);

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ARateIsActive)) == true)
                {
                    dr = dt.NewRow();
                    dr[1] = ObjFunction.GetAppSettings(AppSettings.ARateLabel);
                    dr[0] = "ASaleRate";
                    dt.Rows.Add(dr);
                }

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.BRateIsActive)) == true)
                {
                    dr = dt.NewRow();
                    dr[1] = ObjFunction.GetAppSettings(AppSettings.BRateLabel);
                    dr[0] = "BSaleRate";
                    dt.Rows.Add(dr);
                }

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.CRateIsActive)) == true)
                {
                    dr = dt.NewRow();
                    dr[1] = ObjFunction.GetAppSettings(AppSettings.CRateLabel);
                    dr[0] = "CSaleRate";
                    dt.Rows.Add(dr);
                }

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.DRateIsActive)) == true)
                {
                    dr = dt.NewRow();
                    dr[1] = ObjFunction.GetAppSettings(AppSettings.DRateLabel);
                    dr[0] = "DSaleRate";
                    dt.Rows.Add(dr);
                }

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ERateIsActive)) == true)
                {
                    dr = dt.NewRow();
                    dr[1] = ObjFunction.GetAppSettings(AppSettings.ERateLabel);
                    dr[0] = "ESaleRate";
                    dt.Rows.Add(dr);
                }

                //For Purchase Rate
                dr = dt.NewRow();
                dr[1] = "Purchase Rate";
                dr[0] = "PurRate";
                dt.Rows.Add(dr);

                cmbRateType.DataSource = dt.DefaultView;
                cmbRateType.DisplayMember = dt.Columns[1].ColumnName;
                cmbRateType.ValueMember = dt.Columns[0].ColumnName;
                SetRateType(Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_RateType).ToString()));
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private int GetRateType()
        {
            string str = ObjFunction.GetComboValueString(cmbRateType);
            int val = 0;
            if (str == "ASaleRate") val = 1;
            else if (str == "BSaleRate") val = 2;
            else if (str == "CSaleRate") val = 3;
            else if (str == "DSaleRate") val = 4;
            else if (str == "ESaleRate") val = 5;
            else if (str == "PurRate") val = 6;
            return val;
        }

        private void SetRateType(long RateTypeNo)
        {
            if (RateTypeNo == 1) cmbRateType.SelectedValue = "ASaleRate";
            else if (RateTypeNo == 2) cmbRateType.SelectedValue = "BSaleRate";
            else if (RateTypeNo == 3) cmbRateType.SelectedValue = "CSaleRate";
            else if (RateTypeNo == 4) cmbRateType.SelectedValue = "DSaleRate";
            else if (RateTypeNo == 5) cmbRateType.SelectedValue = "ESaleRate";
            else if (RateTypeNo == 6) cmbRateType.SelectedValue = "PurRate";
        }
        #endregion

        private void PrintBill()
        {
            //string[] ReportSession;

            //ReportSession = new string[2];
            //ReportSession[0] = ID.ToString();
            //ReportSession[1] = "";//ObjQry.ReturnLong("Select Max(PkVoucherNo) FRom TVoucherEntry Where VoucherTypeCode=" + ((flagPP == true) ? VchType.Purchase : VoucherType) + "", CommonFunctions.ConStr).ToString();
            //CrystalDecisions.CrystalReports.Engine.ReportClass childForm = ObjFunction.GetReportObject("Reports.GetBill");
            //if (childForm != null)
            //{
            //    DBReportGenerate objRpt = new DBReportGenerate(childForm, ReportSession);
            //    if (objRpt.PrintReport() == true)
            //    {
            //        DisplayMessage("Bill Print Successfully!!!");
            //    }
            //    else
            //    {
            //        DisplayMessage("Bill not Print !!!");
            //    }
            //}
            //else
            //{
            //    DisplayMessage("Bill Report not exist !!!");
            //}
        }

        private void dtpBillDate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnParty)) == true) cmbPartyName.Focus();
                //else if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnHeaderDisc)) == true) txtOtherDisc.Focus();
                //else if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnTaxType)) == true) cmbTaxType.Focus();
                //else dgBill.Focus();
                txtRefNo.Focus();
                e.SuppressKeyPress = true;
            }
        }

        private void cmbPartyName_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (ObjFunction.GetComboValue(cmbPartyName) == 0)
                    {
                        DisplayMessage("Select Party Name");
                        cmbPartyName.Focus();
                        //cmbPartyName.SelectedValue = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_PartyAC));
                    }
                    else
                    {
                        if (ObjFunction.GetComboValue(cmbPartyName) == Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_PartyAC)))
                        {
                            if (ID == 0)
                            {
                                cmbPaymentType.SelectedValue = 2;
                                cmbPaymentType.Enabled = false;
                            }
                        }
                        else
                            cmbPaymentType.Enabled = true;
                        ObjFunction.GetFinancialYear(dtpBillDate.Value, out dtFrom, out dtTo);
                        if (ObjQry.ReturnLong(" Select Count(*) FROM  TOtherVoucherEntry " +
                                             " where TOtherVoucherEntry.Reference ='" + txtRefNo.Text.Trim() + "' " +
                                             " and TOtherVoucherEntry.LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " " +
                                             " AND TOtherVoucherEntry.VoucherDate>='" + dtFrom.Date + "' AND TOtherVoucherEntry.VoucherDate<='" + dtTo.Date + "' " +
                                             " and TOtherVoucherEntry.PkOtherVoucherNo!=" + ID + "", CommonFunctions.ConStr) > 0)
                        {

                            OMMessageBox.Show("This Invoice is already exist ....", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            cmbPartyName.Focus();
                        }
                        else
                        {
                            //rdBtnAll.Focus();
                            dgBill.Focus();
                            dgBill.CurrentCell = dgBill[ColIndex.ItemName, dgBill.Rows.Count - 1];
                        }

                    }
                    //if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnTaxType)) == true) cmbTaxType.Focus();
                    //else 
                    //dgBill.Focus();
                    //dgBill.CurrentCell = dgBill[1, 0];
                    e.SuppressKeyPress = true;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbRateType_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnTaxType)) == true) cmbTaxType.Focus();
                else dgBill.Focus();

                e.SuppressKeyPress = true;
            }
        }

        private void cmbTaxType_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                dgBill.Focus();
                dgBill.CurrentCell = dgBill[1, 0];
                e.SuppressKeyPress = true;
            }
        }

        private void lstGroup1_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (lstGroup1.Items.Count <= 0)
                {
                    dgBill.Focus();
                    pnlGroup1.Visible = false;
                }
                if (e.KeyCode == Keys.F9)
                {
                    pnlGroup1.Visible = false;
                    FillItemList(1);
                }
                //string ItemListStr = "";
                if (e.KeyCode == Keys.Enter)
                {

                    e.SuppressKeyPress = true;
                    pnlGroup1.Visible = false;

                    FillItemList(strItemQuery.Length);
                }
                else if (e.KeyCode == Keys.Space)
                {
                    dgBill.Focus();
                    pnlGroup1.Visible = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #region Stock Godown related Methods
        int StockGodwnIndex;
        private void DisplayStockGodown()
        {
            StockGodwnIndex = dgBill.CurrentCell.RowIndex;
            if (pnlStockGodown.Visible == false)
            {
                if (dgBill.Rows[StockGodwnIndex].Cells[ColIndex.ItemNo].Value != null)
                {
                    pnlStockGodown.Visible = true;

                    dgStockGodown.Rows.Clear();
                    DataTable dt = dtBillCollect[dgBill.CurrentCell.RowIndex];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dgStockGodown.Rows.Add();
                        for (int j = 0; j < dt.Columns.Count; j++)
                        {
                            dgStockGodown.Rows[i].Cells[j].Value = dt.Rows[i].ItemArray[j].ToString();
                        }
                    }
                    //dgStockGodown.DataSource = dtBillCollect[dgBill.CurrentCell.RowIndex];
                    if (dgStockGodown.Rows.Count > 0)
                    {
                        CalculateGodownQty();
                        dgStockGodown.Columns[0].Visible = false;
                        dgStockGodown.Columns[1].ReadOnly = true;
                        dgStockGodown.Columns[1].Width = 145;
                        dgStockGodown.Columns[2].Width = 68;
                        dgStockGodown.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                        dgStockGodown.Columns[3].Width = 78;
                        dgStockGodown.Columns[3].ReadOnly = true;
                        dgStockGodown.Columns[4].Visible = false;


                        dgStockGodown.CurrentCell = dgStockGodown[2, 0];
                        dgStockGodown.Focus();
                    }
                }
                else
                    DisplayMessage("Please fill item details...");
            }
            else
            {
                pnlStockGodown.Visible = false;
            }
        }

        private void CalculateGodownQty()
        {
            double Qty = 0;
            for (int i = 0; i < dgStockGodown.Rows.Count; i++)
            {
                Qty += Convert.ToDouble(dgStockGodown.Rows[i].Cells[2].Value);
            }
            txtStockGodwnQty.Text = Qty.ToString("0.00");
        }

        private void dgStockGodown_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                if (dgStockGodown.CurrentCell.Value != null)
                {
                    if (ObjFunction.CheckValidAmount(dgStockGodown.CurrentCell.Value.ToString()) == true)
                    {
                        dgStockGodown.Rows[e.RowIndex].Cells[3].Value = Convert.ToDouble(dgStockGodown.Rows[e.RowIndex].Cells[2].Value) * Convert.ToDouble(dgBill.Rows[StockGodwnIndex].Cells[ColIndex.StockFactor].Value);
                        dgStockGodown.CurrentCell.ErrorText = "";
                    }
                    else
                    {
                        //dgStockGodown.CurrentCell.ErrorText = "Enter Valid Amount";
                        dgStockGodown.CurrentCell.Value = "0";
                        dgStockGodown.Rows[e.RowIndex].Cells[3].Value = "0";
                        dgStockGodown.CurrentCell = dgStockGodown[dgStockGodown.CurrentCell.ColumnIndex, dgStockGodown.CurrentCell.RowIndex];
                    }
                    CalculateGodownQty();
                }
                else
                {
                    dgStockGodown.CurrentCell.Value = 0;
                }
            }
        }

        private void btnStkGodownOk_Click(object sender, EventArgs e)
        {
            if (Convert.ToDouble(txtStockGodwnQty.Text) == Convert.ToDouble(dgBill.Rows[StockGodwnIndex].Cells[ColIndex.Quantity].Value))
            {

                DataTable dt = dtBillCollect[StockGodwnIndex].Clone();
                for (int i = 0; i < dgStockGodown.Rows.Count; i++)
                {
                    DataRow dr = dt.NewRow();
                    for (int col = 0; col < dgStockGodown.Columns.Count; col++)
                        dr[col] = dgStockGodown.Rows[i].Cells[col].Value;
                    dt.Rows.Add(dr);
                }
                if (dtBillCollect.Count > 0)
                    dtBillCollect.RemoveAt(StockGodwnIndex);
                dtBillCollect.Insert(StockGodwnIndex, dt);
                pnlStockGodown.Visible = false;
                dgBill.CurrentCell = dgBill[ColIndex.Quantity, StockGodwnIndex];
                dgBill.Focus();
            }
            else
            {
                OMMessageBox.Show("Item Bill Quantity and Stock Godown Quantity doesn't match.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                btnStkGodownOk.Focus();
            }
        }

        private void dgStockGodown_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnStkGodownOk.Focus();
            }
        }

        private void DeleteStockGodown()
        {
            DataTable dt;
            for (int i = 0; i < dtBillCollect.Count; i++)
            {
                dt = dtBillCollect[i];
                for (int row = 0; row < dt.Rows.Count; row++)
                {
                    if (Convert.ToInt64(dt.Rows[row].ItemArray[4].ToString()) != 0 && Convert.ToDouble(dt.Rows[row].ItemArray[2].ToString()) == 0)
                    {
                        //DeleteDtls(4, Convert.ToInt64(dt.Rows[row].ItemArray[4].ToString()));
                    }
                }
            }
        }
        #endregion

        private void cmbRateType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeBillRate();
        }

        private void ChangeBillRate()
        {
            try
            {
                for (int i = 0; i < dgBill.Rows.Count; i++)
                {
                    if (dgBill.Rows[i].Cells[ColIndex.PkRateSettingNo].Value != null)
                    {
                        dgBill.Rows[i].Cells[ColIndex.Rate].Value = ObjQry.ReturnDouble("Select " + ObjFunction.GetComboValueString(cmbRateType) + " From MRateSetting Where PkSrNo=" + dgBill.Rows[i].Cells[ColIndex.PkRateSettingNo].Value + "", CommonFunctions.ConStr);
                    }
                }
                CalculateTotal();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #region Cheque AND Credit grid Related Methods
        private void BindPayChequeDetails(long VchNo)
        {
            try
            {
                //string strQuery = "SELECT  TVoucherChqCreditDetails.ChequeNo, TVoucherChqCreditDetails.ChequeDate, IsNull(MOtherBank.BankName,'') AS BankName, IsNull(MBranch.BranchName,'') AS BranchName,  " +
                //    " TVoucherChqCreditDetails.Amount, TVoucherChqCreditDetails.PkSrNo, TVoucherChqCreditDetails.BankNo, TVoucherChqCreditDetails.BranchNo FROM TVoucherChqCreditDetails LEFT OUTER JOIN " +
                //    " MBranch ON TVoucherChqCreditDetails.BranchNo = MBranch.BranchNo LEFT OUTER JOIN " +
                //    " MOtherBank ON TVoucherChqCreditDetails.BankNo = MOtherBank.BankNo Where TVoucherChqCreditDetails.ChequeNo <>'' AND TVoucherChqCreditDetails.FKVoucherNo=" + VchNo + "";
                string strQuery = " SELECT TVoucherChqCreditDetails.ChequeNo, TVoucherChqCreditDetails.ChequeDate, isNull(MLedger.LedgerName,'') AS BankName,''  AS BranchName, TVoucherChqCreditDetails.Amount,  " +
                                " TVoucherChqCreditDetails.PkSrNo, TVoucherChqCreditDetails.BankNo, TVoucherChqCreditDetails.BranchNo " +
                                " FROM TVoucherChqCreditDetails LEFT OUTER JOIN MLedger ON TVoucherChqCreditDetails.BankNo = MLedger.LedgerNo " +
                                " WHERE (TVoucherChqCreditDetails.ChequeNo <> '') AND (TVoucherChqCreditDetails.FKVoucherNo = " + VchNo + ")";
                dgPayChqDetails.Rows.Clear();
                DataTable dt = ObjFunction.GetDataView(strQuery).Table;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dgPayChqDetails.Rows.Add();
                    for (int k = 0; k < dt.Columns.Count; k++)
                    {
                        if (k == 1)
                            dgPayChqDetails.Rows[i].Cells[k].Value = Convert.ToDateTime(dt.Rows[i].ItemArray[k]).ToString("dd-MMM-yyyy");
                        else
                            dgPayChqDetails.Rows[i].Cells[k].Value = dt.Rows[i].ItemArray[k];
                    }
                }
                dgPayChqDetails.Rows.Add();
                dgPayChqDetails.Columns[0].Width = 69;
                dgPayChqDetails.Columns[1].Width = 83;
                dgPayChqDetails.Columns[2].Width = 240;
                //dgPayChqDetails.Columns[3].Width = 114;
                dgPayChqDetails.Columns[4].Width = 75;
                //dgPayChqDetails.Focus();
                //dgPayChqDetails.CurrentCell = dgPayChqDetails[0, dgPayChqDetails.Rows.Count - 1];
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgPayChqDetails_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    if (dgPayChqDetails.CurrentCell.ColumnIndex == 1)
                    {
                        dtpChqDate.Visible = true;
                        dtpChqDate.BringToFront();
                        dtpChqDate.Focus();
                    }
                    else if (dgPayChqDetails.CurrentCell.ColumnIndex == 2)
                    {

                        pnlBank.Visible = true;
                        pnlBank.BringToFront();
                        lstBank.Focus();
                    }
                    else if (dgPayChqDetails.CurrentCell.ColumnIndex == 3)
                    {
                        pnlBranch.Visible = true;
                        pnlBranch.BringToFront();
                        lstBranch.Focus();
                    }
                    else if (dgPayChqDetails.CurrentCell.ColumnIndex == 4)
                    {
                        double TotAmt = 0;
                        for (int i = 0; i < dgPayChqDetails.Rows.Count; i++)
                        {
                            if (dgPayChqDetails.Rows[0].Cells[4].Value != null)
                            {
                                TotAmt = TotAmt + Convert.ToDouble(dgPayChqDetails.Rows[i].Cells[4].Value);
                            }
                        }
                        txtTotalAmt.Text = TotAmt.ToString();
                        btnOk.Focus();
                    }
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    double Amt = 0;
                    if (dgPayChqDetails.Rows[0].Cells[0].EditedFormattedValue.ToString().Trim() != "")
                    {
                        if (dgPayChqDetails.Rows[0].Cells[1].FormattedValue.ToString().Trim() == "" || dgPayChqDetails.Rows[0].Cells[2].FormattedValue.ToString().Trim() == "")
                        {
                            OMMessageBox.Show("Please Fill Cheque Details.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            return;
                        }
                        for (int i = 0; i < dgPayChqDetails.Rows.Count; i++)
                        {
                            Amt += (dgPayChqDetails.Rows[i].Cells[4].Value == null) ? 0 : Convert.ToDouble(dgPayChqDetails.Rows[i].Cells[4].Value);
                        }

                        if (Convert.ToDouble(dgPayType.Rows[3].Cells[2].Value) != Amt)
                        {
                            OMMessageBox.Show("Please enter Cheque amount and Cheque Details amount are not same...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            dgPayChqDetails.Focus();
                        }
                        else
                        {
                            if (ObjFunction.GetComboValue(cmbPaymentType) == 1)
                            {
                                pnlPartial.Size = new Size(305, 214);
                                pnlPartial.Location = new Point(200, 123);
                                dgPayType.CurrentCell = dgPayType[2, 3];
                                dgPayType.Focus();
                            }
                            else
                            {
                                btnSave.Enabled = true;
                                btnSave.Focus();
                                pnlPartial.Visible = false;
                            }
                            //btnOk.Focus();
                        }
                        //double Amt = 0;
                        //if (dgPayChqDetails.Rows[0].Cells[0].Value != null)
                        //{
                        //    for (int i = 0; i < dgPayChqDetails.Rows.Count; i++)
                        //    {
                        //        Amt += (dgPayChqDetails.Rows[i].Cells[4].Value == null) ? 0 : Convert.ToDouble(dgPayChqDetails.Rows[i].Cells[4].Value);
                        //    }

                        //    if (Convert.ToDouble(dgPayType.Rows[3].Cells[2].Value) != Amt)
                        //    {
                        //        OMMessageBox.Show("Please enter Cheque amount and Cheque Details amount are not same...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        //        dgPayChqDetails.Focus();
                        //    }
                        //    else
                        //    {
                        //        pnlPartial.Size = new Size(305, 214);
                        //        pnlPartial.Location = new Point(200, 123);
                        //        dgPayType.CurrentCell = dgPayType[2, 3];
                        //        dgPayType.Focus();
                        //        //btnOk.Focus();
                        //    }
                    }
                    else
                    {
                        if (ObjFunction.GetComboValue(cmbPaymentType) == 1)
                        {
                            pnlPartial.Size = new Size(305, 214);
                            pnlPartial.Location = new Point(200, 123);
                            dgPayType.CurrentCell = dgPayType[2, 3];
                            dgPayType.Focus();
                        }
                        else
                        {
                            //btnSave.Enabled = true;
                            //btnSave.Focus();
                            cmbPaymentType.Focus();
                            pnlPartial.Visible = false;
                        }
                    }
                }
                else if (e.KeyCode == Keys.Delete)
                {
                    //if (dgPayChqDetails.CurrentCell.RowIndex != dgPayChqDetails.Rows.Count - 1)
                    //{
                    if (Convert.ToInt64(dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[5].Value) != 0)
                    {
                        DeleteDtls(5, Convert.ToInt64(dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[5].Value));
                    }
                    dgPayChqDetails.Rows.RemoveAt(dgPayChqDetails.CurrentCell.RowIndex);
                    if (dgPayChqDetails.Rows.Count == 0)
                        dgPayChqDetails.Rows.Add();
                    dgPayChqDetails.CurrentCell = dgPayChqDetails[0, dgPayChqDetails.Rows.Count - 1];
                    // }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dtpChqDate_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;

                    dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[1].Value = dtpChqDate.SelectionStart.ToString("dd-MMM-yy");
                    dtpChqDate.Visible = false;
                    if (dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[2].Value == null)
                    {

                        pnlBank.Location = new Point(dgPayChqDetails.CurrentCell.ContentBounds.X + 150 + dgPayChqDetails.Location.X, dgPayChqDetails.CurrentCell.ContentBounds.Y + 40);
                        pnlBank.Visible = true;
                        pnlBank.BringToFront();
                        lstBank.Focus();
                    }
                    else
                    {
                        dgPayChqDetails.Focus();
                        dgPayChqDetails.CurrentCell = dgPayChqDetails[2, dgPayChqDetails.CurrentCell.RowIndex];
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void lstBank_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    if (dgPayChqDetails.Visible == true)
                    {
                        dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[2].Value = lstBank.Text;
                        dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[6].Value = lstBank.SelectedValue;
                        pnlBank.Visible = false;
                        //if (dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[3].Value == null)
                        //{
                        //    pnlBranch.Visible = true;
                        //    pnlBranch.BringToFront();
                        //    lstBranch.Focus();
                        //}
                        //else
                        //{
                        dgPayChqDetails.Focus();
                        dgPayChqDetails.CurrentCell = dgPayChqDetails[4, dgPayChqDetails.CurrentCell.RowIndex];
                        //}
                    }
                    else if (dgPayCreditCardDetails.Visible == true)
                    {
                        dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[1].Value = lstBank.Text;
                        dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[5].Value = lstBank.SelectedValue;
                        pnlBank.Visible = false;
                        //if (dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[2].Value == null)
                        //{
                        //    pnlBranch.Location = new Point(dgPayCreditCardDetails.CurrentCell.ContentBounds.X + 235 + dgPayCreditCardDetails.Location.X, dgPayCreditCardDetails.CurrentCell.ContentBounds.Y + 40);
                        //    pnlBranch.Visible = true;
                        //    pnlBranch.BringToFront();
                        //    lstBranch.Focus();
                        //}
                        //else
                        //{
                        dgPayCreditCardDetails.Focus();
                        dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[3, dgPayCreditCardDetails.CurrentCell.RowIndex];
                        //}
                    }
                }
                else if (e.KeyCode == Keys.Space)
                {
                    if (dgPayChqDetails.Visible == true)
                    {
                        dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[2].Value = "";
                        dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[6].Value = 0;
                        pnlBank.Visible = false;
                        if (dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[3].Value == null)
                        {
                            pnlBranch.Visible = true;
                            pnlBranch.BringToFront();
                            lstBranch.Focus();
                        }
                        else
                        {
                            dgPayChqDetails.Focus();
                            dgPayChqDetails.CurrentCell = dgPayChqDetails[3, dgPayChqDetails.CurrentCell.RowIndex];
                        }
                    }
                    else if (dgPayCreditCardDetails.Visible == true)
                    {
                        dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[1].Value = "";
                        dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[5].Value = 0;
                        pnlBank.Visible = false;
                        if (dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[2].Value == null)
                        {
                            pnlBranch.Visible = true;
                            pnlBranch.BringToFront();
                            lstBranch.Focus();
                        }
                        else
                        {
                            dgPayCreditCardDetails.Focus();
                            dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[3, dgPayCreditCardDetails.CurrentCell.RowIndex];
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void lstBranch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    if (dgPayChqDetails.Visible == true)
                    {
                        dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[3].Value = lstBranch.Text;
                        dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[7].Value = lstBranch.SelectedValue;
                        pnlBranch.Visible = false;
                        dgPayChqDetails.Focus();
                        dgPayChqDetails.CurrentCell = dgPayChqDetails[4, dgPayChqDetails.CurrentCell.RowIndex];
                    }
                    else if (dgPayCreditCardDetails.Visible == true)
                    {
                        dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[2].Value = lstBranch.Text;
                        dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[6].Value = lstBranch.SelectedValue;
                        pnlBranch.Visible = false;
                        dgPayCreditCardDetails.Focus();
                        dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[3, dgPayCreditCardDetails.CurrentCell.RowIndex];
                    }
                }
                else if (e.KeyCode == Keys.Space)
                {
                    if (dgPayChqDetails.Visible == true)
                    {
                        dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[3].Value = "";
                        dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[7].Value = 0;
                        pnlBranch.Visible = false;
                        dgPayChqDetails.Focus();
                        dgPayChqDetails.CurrentCell = dgPayChqDetails[4, dgPayChqDetails.CurrentCell.RowIndex];
                    }
                    else if (dgPayCreditCardDetails.Visible == true)
                    {
                        dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[3].Value = "";
                        dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[6].Value = 0;
                        pnlBranch.Visible = false;
                        dgPayCreditCardDetails.Focus();
                        dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[3, dgPayCreditCardDetails.CurrentCell.RowIndex];
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgPayChqDetails_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 0)
                {
                    if (dgPayChqDetails.Rows[e.RowIndex].Cells[1].Value == null)
                    {
                        if (e.RowIndex == 0) dgPayChqDetails.Rows[e.RowIndex].Cells[4].Value = dgPayType.Rows[3].Cells[2].Value;
                        dtpChqDate.Location = new Point(dgPayChqDetails.CurrentCell.ContentBounds.X + 72 + dgPayChqDetails.Location.X, dgPayChqDetails.CurrentCell.ContentBounds.Y + 40);
                        dtpChqDate.Visible = true;
                        dtpChqDate.BringToFront();
                        dtpChqDate.Focus();
                    }
                    else
                    {
                        dgPayChqDetails.Focus();
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { dgPayChqDetails.CurrentCell.RowIndex, 1, dgPayChqDetails });
                    }
                }
                if (e.ColumnIndex == 4)
                {
                    if (dgPayChqDetails.Rows[e.RowIndex].Cells[4].Value != null)
                    {
                        dgPayChqDetails.Rows[e.RowIndex].Cells[4].ErrorText = "";
                        if (ObjFunction.CheckValidAmount(dgPayChqDetails.Rows[e.RowIndex].Cells[4].Value.ToString()) == false)
                        {
                            dgPayChqDetails.Rows[e.RowIndex].Cells[4].ErrorText = "Please Enter Valid Amount";
                        }
                        else
                        {
                            double TotAmt = 0;
                            dgPayChqDetails.Rows[e.RowIndex].Cells[4].ErrorText = "";
                            if (e.RowIndex == dgPayChqDetails.Rows.Count - 1 && dgPayChqDetails.Rows[e.RowIndex].Cells[1].Value != null)
                            {
                                for (int i = 0; i < dgPayChqDetails.Rows.Count; i++)
                                {
                                    if (dgPayChqDetails.Rows[e.RowIndex].Cells[4].Value != null)
                                    {
                                        TotAmt = TotAmt + Convert.ToDouble(dgPayChqDetails.Rows[i].Cells[4].Value);
                                    }
                                }
                                txtTotalAmt.Text = TotAmt.ToString();
                                dgPayChqDetails.Rows.Add();
                                dgPayChqDetails.Focus();
                                dgPayChqDetails.CurrentCell = dgPayChqDetails[0, dgPayChqDetails.Rows.Count - 1];
                            }
                            else
                            {
                                for (int i = 0; i < dgPayChqDetails.Rows.Count; i++)
                                {
                                    if (dgPayChqDetails.Rows[e.RowIndex].Cells[4].Value != null)
                                    {
                                        TotAmt = TotAmt + Convert.ToDouble(dgPayChqDetails.Rows[i].Cells[4].Value);
                                    }
                                }
                                txtTotalAmt.Text = TotAmt.ToString();
                                btnOk.Focus();
                            }
                        }
                    }
                    else
                    {
                        dgPayChqDetails.Rows[e.RowIndex].Cells[4].ErrorText = "Please Enter  Amount";
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void BindPayCreditDetails(long VchNo)
        {
            try
            {
                //string strQuery = "SELECT  TVoucherChqCreditDetails.CreditCardNo, IsNull(MOtherBank.BankName,'') AS BankName, IsNull(MBranch.BranchName,'') AS BranchName,  " +
                //    " TVoucherChqCreditDetails.Amount, TVoucherChqCreditDetails.PkSrNo, TVoucherChqCreditDetails.BankNo, TVoucherChqCreditDetails.BranchNo FROM TVoucherChqCreditDetails LEFT OUTER JOIN " +
                //    " MBranch ON TVoucherChqCreditDetails.BranchNo = MBranch.BranchNo LEFT OUTER JOIN " +
                //    " MOtherBank ON TVoucherChqCreditDetails.BankNo = MOtherBank.BankNo Where TVoucherChqCreditDetails.CreditCardNo <>'' AND TVoucherChqCreditDetails.FKVoucherNo=" + VchNo + "";
                string strQuery = " SELECT  TVoucherChqCreditDetails.CreditCardNo, isNull(MLedger.LedgerName,'') AS BankName,''  AS BranchName, TVoucherChqCreditDetails.Amount,  " +
                                " TVoucherChqCreditDetails.PkSrNo, TVoucherChqCreditDetails.BankNo, TVoucherChqCreditDetails.BranchNo " +
                                " FROM TVoucherChqCreditDetails LEFT OUTER JOIN MLedger ON TVoucherChqCreditDetails.BankNo = MLedger.LedgerNo " +
                                " WHERE     (TVoucherChqCreditDetails.CreditCardNo <> '') AND (TVoucherChqCreditDetails.FKVoucherNo = " + VchNo + ")";
                dgPayCreditCardDetails.Rows.Clear();
                DataTable dt = ObjFunction.GetDataView(strQuery).Table;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dgPayCreditCardDetails.Rows.Add();
                    for (int k = 0; k < dt.Columns.Count; k++)
                        dgPayCreditCardDetails.Rows[i].Cells[k].Value = dt.Rows[i].ItemArray[k];
                }
                dgPayCreditCardDetails.Rows.Add();
                dgPayCreditCardDetails.Columns[0].Width = 69;
                dgPayCreditCardDetails.Columns[1].Width = 240;
                //dgPayCreditCardDetails.Columns[2].Width = 114;
                dgPayCreditCardDetails.Columns[3].Width = 75;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgPayCreditCardDetails_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    if (dgPayCreditCardDetails.CurrentCell.ColumnIndex == 1)
                    {

                        pnlBank.Location = new Point(dgPayCreditCardDetails.CurrentCell.ContentBounds.X + 72 + dgPayCreditCardDetails.Location.X, dgPayCreditCardDetails.CurrentCell.ContentBounds.Y + 50);
                        pnlBank.Visible = true;
                        pnlBank.BringToFront();
                        lstBank.Focus();
                    }
                    else if (dgPayCreditCardDetails.CurrentCell.ColumnIndex == 2)
                    {
                        pnlBranch.Location = new Point(dgPayCreditCardDetails.CurrentCell.ContentBounds.X + 200 + dgPayCreditCardDetails.Location.X, dgPayCreditCardDetails.CurrentCell.ContentBounds.Y + 50);
                        pnlBranch.Visible = true;
                        pnlBranch.BringToFront();
                        lstBranch.Focus();
                    }
                    else if (dgPayCreditCardDetails.CurrentCell.ColumnIndex == 3)
                    {
                        double TotAmt = 0;
                        for (int i = 0; i < dgPayCreditCardDetails.Rows.Count; i++)
                        {
                            if (dgPayCreditCardDetails.Rows[i].Cells[3].Value != null)
                            {
                                TotAmt = TotAmt + Convert.ToDouble(dgPayCreditCardDetails.Rows[i].Cells[3].Value);
                            }
                        }
                        txtTotalAmt.Text = TotAmt.ToString();
                        btnOk.Focus();
                    }
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    double Amt = 0;
                    if (dgPayCreditCardDetails.Rows[0].Cells[0].Value != null)
                    {
                        if (dgPayCreditCardDetails.Rows[0].Cells[0].FormattedValue.ToString() == "" || dgPayCreditCardDetails.Rows[0].Cells[1].FormattedValue.ToString() == "")
                        {
                            OMMessageBox.Show("Please Fill Credit Card Details.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            return;
                        }
                        for (int i = 0; i < dgPayCreditCardDetails.Rows.Count; i++)
                        {
                            Amt += (dgPayCreditCardDetails.Rows[i].Cells[3].Value == null) ? 0 : Convert.ToDouble(dgPayCreditCardDetails.Rows[i].Cells[3].Value);
                        }
                        if (Convert.ToDouble(dgPayType.Rows[4].Cells[2].Value) != Amt)
                        {
                            OMMessageBox.Show("Please enter CrediCard amount and CreditCard Details amount are not same...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            dgPayCreditCardDetails.Focus();
                        }
                        else
                        {
                            if (ObjFunction.GetComboValue(cmbPaymentType) == 1)
                            {
                                pnlPartial.Size = new Size(305, 214);
                                pnlPartial.Location = new Point(200, 123);
                                dgPayType.CurrentCell = dgPayType[2, 4];
                                dgPayType.Focus();
                            }
                            else
                            {
                                btnSave.Enabled = true;
                                btnSave.Focus();
                                pnlPartial.Visible = false;
                            }
                        }
                    }
                    else
                    {
                        if (ObjFunction.GetComboValue(cmbPaymentType) == 1)
                        {
                            pnlPartial.Size = new Size(305, 214);
                            pnlPartial.Location = new Point(200, 123);
                            dgPayType.CurrentCell = dgPayType[2, 4];
                            dgPayType.Focus();
                        }
                        else
                        {
                            cmbPaymentType.Focus();
                            pnlPartial.Visible = false;
                        }
                    }
                }
                else if (e.KeyCode == Keys.Delete)
                {
                    //if (dgPayCreditCardDetails.CurrentCell.RowIndex != dgPayCreditCardDetails.Rows.Count - 1)
                    //{
                    if (Convert.ToInt64(dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[4].Value) != 0)
                    {
                        DeleteDtls(5, Convert.ToInt64(dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[4].Value));
                    }
                    dgPayCreditCardDetails.Rows.RemoveAt(dgPayCreditCardDetails.CurrentCell.RowIndex);
                    if (dgPayCreditCardDetails.Rows.Count == 0)
                        dgPayCreditCardDetails.Rows.Add();
                    dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[0, dgPayCreditCardDetails.Rows.Count - 1];
                    //}
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgPayCreditCardDetails_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 0)
                {
                    if (dgPayCreditCardDetails.Rows[e.RowIndex].Cells[1].Value == null)
                    {
                        if (e.RowIndex == 0) dgPayCreditCardDetails.Rows[e.RowIndex].Cells[3].Value = dgPayType.Rows[4].Cells[2].Value;
                        pnlBank.Location = new Point(dgPayCreditCardDetails.CurrentCell.ContentBounds.X + 72 + dgPayCreditCardDetails.Location.X, dgPayCreditCardDetails.CurrentCell.ContentBounds.Y + 50);
                        pnlBank.Visible = true;
                        pnlBank.BringToFront();
                        lstBank.Focus();
                    }
                    else
                    {
                        dgPayCreditCardDetails.Focus();
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { dgPayCreditCardDetails.CurrentCell.RowIndex, 1, dgPayCreditCardDetails });
                    }
                }
                if (e.ColumnIndex == 3)
                {
                    if (dgPayCreditCardDetails.Rows[e.RowIndex].Cells[3].Value != null)
                    {
                        dgPayCreditCardDetails.Rows[e.RowIndex].Cells[3].ErrorText = "";
                        if (ObjFunction.CheckValidAmount(dgPayCreditCardDetails.Rows[e.RowIndex].Cells[3].Value.ToString()) == false)
                        {
                            dgPayCreditCardDetails.Rows[e.RowIndex].Cells[3].ErrorText = "Please Enter Valid Amount";
                        }
                        else
                        {
                            double TotAmt = 0;
                            dgPayCreditCardDetails.Rows[e.RowIndex].Cells[3].ErrorText = "";
                            if (e.RowIndex == dgPayCreditCardDetails.Rows.Count - 1 && dgPayCreditCardDetails.Rows[e.RowIndex].Cells[1].Value != null)
                            {
                                dgPayCreditCardDetails.Rows.Add();
                                dgPayCreditCardDetails.Focus();
                                dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[0, dgPayCreditCardDetails.Rows.Count - 1];
                            }

                            for (int i = 0; i < dgPayCreditCardDetails.Rows.Count; i++)
                            {
                                if (dgPayCreditCardDetails.Rows[i].Cells[3].Value != null)
                                {
                                    TotAmt = TotAmt + Convert.ToDouble(dgPayCreditCardDetails.Rows[i].Cells[3].Value);
                                }
                            }
                            txtTotalAmt.Text = TotAmt.ToString();
                        }
                    }
                    else
                    {
                        dgPayCreditCardDetails.Rows[e.RowIndex].Cells[3].ErrorText = "Please Enter  Amount";
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }
        #endregion

        #region Rate Type Password related Methods

        private void txtRateTypePassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                btnRateTypeOK_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Escape)
            {
                pnlRateType.Visible = false;
                btnNew.Focus();
            }
        }

        private void btnRateTypeOK_Click(object sender, EventArgs e)
        {
            try
            {
                bool flag = false;
                string[,] arr = new string[5, 2];
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ARateIsActive)) == true)
                {
                    arr[0, 0] = ObjFunction.GetAppSettings(AppSettings.ARatePassword);
                    arr[0, 1] = "ASaleRate";
                }

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.BRateIsActive)) == true)
                {
                    arr[1, 0] = ObjFunction.GetAppSettings(AppSettings.BRatePassword);
                    arr[1, 1] = "BSaleRate";
                }

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.CRateIsActive)) == true)
                {
                    arr[2, 0] = ObjFunction.GetAppSettings(AppSettings.CRatePassword);
                    arr[2, 1] = "CSaleRate";
                }

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.DRateIsActive)) == true)
                {
                    arr[3, 0] = ObjFunction.GetAppSettings(AppSettings.DRatePassword);
                    arr[3, 1] = "DSaleRate";
                }

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ERateIsActive)) == true)
                {
                    arr[4, 0] = ObjFunction.GetAppSettings(AppSettings.ERatePassword);
                    arr[4, 1] = "ESaleRate";
                }

                for (int i = 0; i < 5; i++)
                {
                    if (arr[i, 0] != null)
                    {
                        if (txtRateTypePassword.Text == arr[i, 0].ToString())
                        {
                            cmbRateType.SelectedValue = arr[i, 1].ToString();
                            cmbRateType_SelectedIndexChanged(sender, new EventArgs());
                            RateTypeNo = i + 1;
                            DBMSettings dbMSettings = new DBMSettings();
                            if (arr[i, 1].ToString() == "ASaleRate")
                            {
                                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ARateDBEffect)) == true)
                                    dbMSettings.AddAppSettings(AppSettings.P_RateType, "1");
                            }
                            else if (arr[i, 1].ToString() == "BSaleRate")
                            {
                                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.BRateDBEffect)) == true)
                                    dbMSettings.AddAppSettings(AppSettings.P_RateType, "2");
                            }
                            else if (arr[i, 1].ToString() == "CSaleRate")
                            {
                                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.CRateDBEffect)) == true)
                                    dbMSettings.AddAppSettings(AppSettings.P_RateType, "3");
                            }
                            else if (arr[i, 1].ToString() == "DSaleRate")
                            {
                                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.DRateDBEffect)) == true)
                                    dbMSettings.AddAppSettings(AppSettings.P_RateType, "4");
                            }
                            else if (arr[i, 1].ToString() == "ESaleRate")
                            {
                                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ERateDBEffect)) == true)
                                    dbMSettings.AddAppSettings(AppSettings.P_RateType, "5");
                            }
                            dbMSettings.ExecuteNonQueryStatements();
                            ObjFunction.SetAppSettings();
                            flag = true;
                            break;
                        }
                    }
                }
                if (flag == false)
                {
                    OMMessageBox.Show("Please enter valid password", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    txtRateTypePassword.Focus();
                }
                else
                {
                    pnlRateType.Visible = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnRateTypeCancel_Click(object sender, EventArgs e)
        {
            txtRateTypePassword.Text = "";
            pnlRateType.Visible = false;
        }

        #endregion

        private bool IsSuperMode()
        {
            try
            {
                bool flag = false;
                long RTNo = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_RateType));
                if (RTNo == 1)
                {
                    flag = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ARateSuperMode));
                }
                else if (RTNo == 2)
                {
                    flag = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.BRateSuperMode));
                }
                else if (RTNo == 3)
                {
                    flag = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.CRateSuperMode));
                }
                else if (RTNo == 4)
                {
                    flag = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.DRateSuperMode));
                }
                else if (RTNo == 5)
                {
                    flag = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ERateSuperMode));
                }
                return flag;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return false;
            }
        }

        private void btnOKPrintBarCode_Click(object sender, EventArgs e)
        {
            try
            {
                DBTBarCodePrint dbBarCodePrint = new DBTBarCodePrint();
                TBarCodePrint tBarCodePrint = new TBarCodePrint();
                tBarCodePrint.MacNo = DBGetVal.MacNo;
                tBarCodePrint.UserID = DBGetVal.UserID;
                dbBarCodePrint.DeleteTBarCodePrint(tBarCodePrint);

                tBarCodePrint = new TBarCodePrint();
                tBarCodePrint.PkSrNo = 0;
                tBarCodePrint.ItemNo = ItemNumber;
                tBarCodePrint.Quantity = Convert.ToInt64(txtNoOfPrint.Text);
                tBarCodePrint.FKRateSettingNo = Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkRateSettingNo].Value);
                tBarCodePrint.MacNo = DBGetVal.MacNo;
                tBarCodePrint.UserID = DBGetVal.UserID;
                dbBarCodePrint.AddTBarCodePrint(tBarCodePrint);
                dbBarCodePrint.ExecuteNonQueryStatements();

                string[] ReportSession;
                ReportSession = new string[4];
                //ReportSession[0] = ItemNumber.ToString();
                //ReportSession[1] = txtNoOfPrint.Text;
                ReportSession[0] = "1";
                ReportSession[1] = txtStartNo.Text;
                ReportSession[2] = DBGetVal.MacNo.ToString();
                ReportSession[3] = DBGetVal.UserID.ToString();

                if (OMMessageBox.Show("Do you want Preview of barcode?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Form NewF;
                    if (rbBigMod.Checked == true)
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            NewF = new Display.ReportViewSource(new Reports.BarCodePrintBig(), ReportSession);
                        else
                            NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("BarCodePrintBig.rpt", CommonFunctions.ReportPath), ReportSession);
                        ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                    }
                    else if (rbSmallMode.Checked == true)
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            NewF = new Display.ReportViewSource(new Reports.BarCodePrint(), ReportSession);
                        else
                            NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("BarCodePrint.rpt", CommonFunctions.ReportPath), ReportSession);
                        ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                    }
                }
                else
                {
                    CrystalDecisions.CrystalReports.Engine.ReportDocument childForm = null;
                    if (rbBigMod.Checked == true)
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            childForm = new Reports.BarCodePrintBig();
                        else
                            childForm = ObjFunction.LoadReportObject("BarCodePrintBig.rpt", CommonFunctions.ReportPath);
                    }
                    else if (rbSmallMode.Checked == true)
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            childForm = new Reports.BarCodePrint();
                        else

                            childForm = ObjFunction.LoadReportObject("BarCodePrint.rpt", CommonFunctions.ReportPath);
                    }

                    if (childForm != null)
                    {
                        DBReportGenerate objRpt = new DBReportGenerate(childForm, ReportSession);
                        if (objRpt.PrintReport() == true)
                        {
                            OMMessageBox.Show("Printing barCode sucessfully...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        }
                        else
                        {
                            OMMessageBox.Show("Barcode not Print...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        OMMessageBox.Show("Barcode Print report not exist...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                }

                pnlBarCodePrint.Visible = false;
                btnNew.Focus();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnCancelPrintBarcode_Click(object sender, EventArgs e)
        {
            pnlBarCodePrint.Visible = false;
            btnNew.Focus();
        }

        private void dgBill_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == ColIndex.BarcodePrint)
            {
                if (dgBill.Rows[e.RowIndex].Cells[ColIndex.ItemNo].Value != null)
                {
                    txtNoOfPrint.Text = "";
                    ItemNumber = Convert.ToInt64(dgBill.Rows[e.RowIndex].Cells[ColIndex.ItemNo].Value);
                    txtNoOfPrint.Text = dgBill.Rows[e.RowIndex].Cells[ColIndex.ActualQty].Value.ToString();
                    pnlBarCodePrint.Visible = true;
                    txtStartNo.Text = "0";
                    txtNoOfPrint.Enabled = true;
                    txtStartNo.Enabled = true;
                    txtNoOfPrint.Focus();
                }
            }
        }

        #region Company Panel related Methods

        private void BindCompany()
        {
            try
            {
                DataTable dtPayType = new DataTable();
                string sqlQuery = "";
                sqlQuery = "SELECT CompanyNo, CompanyName From MCompany Order by CompanyName";

                DataTable dt = ObjFunction.GetDataView(sqlQuery).Table;

                for (int row = 0; row < dt.Rows.Count; row++)
                {
                    dgCompany.Rows.Add();
                    for (int col = 0; col < dt.Columns.Count; col++)
                    {
                        dgCompany[col, row].Value = dt.Rows[row].ItemArray[col].ToString();
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgCompany_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    //PurCompNo = Convert.ToInt64(dgCompany.CurrentRow.Cells[0].Value);
                    lblCompName.Text = dgCompany.CurrentRow.Cells[1].Value.ToString();
                    pnlCompany.Visible = false;

                    ID = 0;
                    ObjFunction.InitialiseControl(this.Controls);
                    ObjFunction.LockButtons(true, this.Controls);
                    ObjFunction.LockControls(false, this.Controls);
                    dgBill.Enabled = false;
                    DisplayList(false);
                    InitControls();

                    dtSearch = ObjFunction.GetDataView("Select PkVoucherNo from TVoucherEntry Where VoucherTypeCode=" + VoucherType + " AND CompanyNo=" + DBGetVal.CompanyNo + "").Table;

                    if (dtSearch.Rows.Count > 0)
                    {
                        ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                        FillControls();
                        SetNavigation();
                    }

                    setDisplay(true);
                    btnNew.Focus();
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    pnlCompany.Visible = false;
                    btnNew.Focus();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private string GetCompanyName(long CNO)
        {
            string strName = "";
            for (int i = 0; i < dgCompany.Rows.Count; i++)
            {
                if (CNO == Convert.ToInt64(dgCompany.Rows[i].Cells[0].Value))
                {
                    strName = dgCompany.Rows[i].Cells[1].Value.ToString();
                    break;
                }
            }
            return strName;
        }
        #endregion

        #region All Print BarCode
        private void btnAllBarCodePrint_Click(object sender, EventArgs e)
        {
            try
            {
                pnlAllPrintBarCode.Visible = true;
                pnlAllPrintBarCode.Location = new Point(110, 98);
                pnlAllPrintBarCode.Size = new Size(540, 291);
                dgPrintBarCode.Height = 267;
                while (dgPrintBarCode.Rows.Count > 0)
                    dgPrintBarCode.Rows.RemoveAt(0);
                for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                {
                    dgPrintBarCode.Rows.Add();
                    int index = dgPrintBarCode.Rows.Count - 1;
                    dgPrintBarCode.Rows[index].Cells[0].Value = dgBill.Rows[index].Cells[ColIndex.ItemNo].Value;
                    dgPrintBarCode.Rows[index].Cells[1].Value = dgBill.Rows[index].Cells[ColIndex.ItemName].Value;
                    dgPrintBarCode.Rows[index].Cells[2].Value = dgBill.Rows[index].Cells[ColIndex.Quantity].Value;
                    dgPrintBarCode.Rows[index].Cells[3].Value = true;
                    dgPrintBarCode.Rows[index].Cells[4].Value = dgBill.Rows[index].Cells[ColIndex.PkRateSettingNo].Value;
                }
                txtAllStartNo.Enabled = true;
                txtAllStartNo.Text = "0";
                txtAllStartNo.Focus();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgPrintBarCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btnAllBarcodeOK.Focus();
            }
        }

        private void btnAllBarcodeOK_Click(object sender, EventArgs e)
        {
            try
            {
                DBTBarCodePrint dbBarCodePrint = new DBTBarCodePrint();
                TBarCodePrint tBarCodePrint = new TBarCodePrint();
                tBarCodePrint.MacNo = DBGetVal.MacNo;
                tBarCodePrint.UserID = DBGetVal.UserID;
                dbBarCodePrint.DeleteTBarCodePrint(tBarCodePrint);

                for (int i = 0; i < dgPrintBarCode.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(dgPrintBarCode.Rows[i].Cells[3].Value) == true)
                    {
                        tBarCodePrint = new TBarCodePrint();
                        tBarCodePrint.PkSrNo = 0;
                        tBarCodePrint.ItemNo = Convert.ToInt64(dgPrintBarCode.Rows[i].Cells[0].Value.ToString());
                        tBarCodePrint.Quantity = Convert.ToInt64(dgPrintBarCode.Rows[i].Cells[2].Value.ToString());
                        tBarCodePrint.FKRateSettingNo = Convert.ToInt64(dgPrintBarCode.Rows[i].Cells[4].Value.ToString());
                        tBarCodePrint.MacNo = DBGetVal.MacNo;
                        tBarCodePrint.UserID = DBGetVal.UserID;
                        dbBarCodePrint.AddTBarCodePrint(tBarCodePrint);
                    }
                }
                dbBarCodePrint.ExecuteNonQueryStatements();

                string[] ReportSession;
                ReportSession = new string[4];
                //ReportSession[0] = ItemNumber.ToString();
                //ReportSession[1] = txtNoOfPrint.Text;
                ReportSession[0] = "1";
                ReportSession[1] = txtAllStartNo.Text;
                ReportSession[2] = DBGetVal.MacNo.ToString();
                ReportSession[3] = DBGetVal.UserID.ToString();

                if (OMMessageBox.Show("Do you want Preview of barcode?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Form NewF;
                    if (rdAllBigMode.Checked == true)
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            NewF = new Display.ReportViewSource(new Reports.BarCodePrintBig(), ReportSession);
                        else
                            NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("BarCodePrintBig.rpt", CommonFunctions.ReportPath), ReportSession);
                        ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                    }
                    else if (rdAllSmallMode.Checked == true)
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            NewF = new Display.ReportViewSource(new Reports.BarCodePrint(), ReportSession);
                        else
                            NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("BarCodePrint.rpt", CommonFunctions.ReportPath), ReportSession);
                        ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                    }
                }
                else
                {
                    CrystalDecisions.CrystalReports.Engine.ReportDocument childForm = null;
                    if (rbBigMod.Checked == true)
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            childForm = new Reports.BarCodePrintBig();
                        else
                            childForm = ObjFunction.LoadReportObject("BarCodePrintBig.rpt", CommonFunctions.ReportPath);
                    }
                    else if (rbSmallMode.Checked == true)
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            childForm = new Reports.BarCodePrint();
                        else

                            childForm = ObjFunction.LoadReportObject("BarCodePrint.rpt", CommonFunctions.ReportPath);
                    }

                    if (childForm != null)
                    {
                        DBReportGenerate objRpt = new DBReportGenerate(childForm, ReportSession);
                        if (objRpt.PrintReport() == true)
                        {
                            OMMessageBox.Show("Printing barCode sucessfully...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        }
                        else
                        {
                            OMMessageBox.Show("Barcode not Print...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        OMMessageBox.Show("Barcode Print report not exist...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                }
                //dbBarCodePrint.DeleteTBarCodePrint(tBarCodePrint);
                pnlAllPrintBarCode.Visible = false;
                btnNew.Focus();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnAllBarcodeCancel_Click(object sender, EventArgs e)
        {
            pnlAllPrintBarCode.Visible = false;
            if (btnNew.Visible)
                btnNew.Focus();
            else
                btnSave.Focus();
        }
        #endregion

        private void txtOtherDisc_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Escape)
                {
                    if (txtOtherDisc.Text.Trim() != "")
                    {
                        if (ObjFunction.CheckValidAmount(txtOtherDisc.Text) == false)
                        {
                            OMMessageBox.Show("Enter Discount Value.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            txtOtherDisc.Focus();
                        }
                        else
                        {
                            for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                            {
                                dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value = txtOtherDisc.Text;
                                CalculateTotal();
                            }
                            if (e.KeyCode == Keys.Escape)

                                btnSave.Focus();
                            else if (e.KeyCode == Keys.Enter)
                            {
                                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnParty)) == true) cmbPartyName.Focus();
                                else if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnTaxType)) == true) cmbTaxType.Focus();
                                else dgBill.Focus();
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                        {
                            dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value = 0;
                            CalculateTotal();
                        }
                        if (e.KeyCode == Keys.Escape)

                            btnSave.Focus();
                        else if (e.KeyCode == Keys.Enter)
                        {
                            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnParty)) == true) cmbPartyName.Focus();
                            else if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnTaxType)) == true) cmbTaxType.Focus();
                            else dgBill.Focus();
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void txtRefNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtRefNo_Leave(txtRefNo, new EventArgs());
                e.SuppressKeyPress = true;
            }
        }

        private void txtRefNo_Leave(object sender, EventArgs e)
        {
            if (txtRefNo.Text == "")
            {
                OMMessageBox.Show("Enter PO. No:.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                txtRefNo.Focus();
            }
            else
            {
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnHeaderDisc)) == true) txtOtherDisc.Focus();
                else if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnParty)) == true) cmbPartyName.Focus();
                else if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnTaxType)) == true) cmbTaxType.Focus();
                else dgBill.Focus();
            }
        }

        #region All Search Code

        private void txtInvNoSearch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                long tempNo;
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    if (ObjQry.ReturnLong("Select Count(*) From TOtherVoucherEntry Where Reference='" + txtInvNoSearch.Text + "' and VoucherTypeCode=" + VoucherType + "", CommonFunctions.ConStr) > 1)
                    {
                        pnlInvSearch.Visible = true;

                        int x = dgBill.GetCellDisplayRectangle(0, 0, true).X + 200;//(Screen.PrimaryScreen.WorkingArea.Width) / 2;
                        int y = dgBill.GetCellDisplayRectangle(0, 0, true).Y + 100;
                        //pnlPartySearch.SetBounds(x, y, dgPartySearch.Width + 10, dgPartySearch.Height + 10);
                        pnlInvSearch.Location = new Point(x, y);
                        string str = "SELECT    0 as [#], TOtherVoucherEntry.VoucherUserNo AS [Doc #], TOtherVoucherEntry.Reference AS 'Inv No', TOtherVoucherEntry.VoucherDate AS 'Date', TOtherVoucherEntry.BilledAmount AS 'Amount'," +
                                     "TOtherVoucherEntry.PkOtherVoucherNo, MLedger.LedgerName as 'Party' FROM TOtherVoucherEntry INNER JOIN MLedger ON TOtherVoucherEntry.LedgerNo = MLedger.LedgerNo " +
                                     " WHERE (TOtherVoucherEntry.VoucherTypeCode IN (" + VoucherType + "," + VchType.PurchaseOrder + ")) AND (TOtherVoucherEntry.CompanyNo = " + DBGetVal.CompanyNo + ")   " +
                                     "And TOtherVoucherEntry.Reference='" + txtInvNoSearch.Text + "' " +
                                     "Order By  TOtherVoucherEntry.VoucherUserNo desc,TOtherVoucherEntry.VoucherDate desc, TOtherVoucherEntry.Reference desc";
                        dgInvSearch.DataSource = ObjFunction.GetDataView(str).Table.DefaultView;

                        dgInvSearch.Columns[0].Width = 40;
                        dgInvSearch.Columns[1].Width = 50;
                        dgInvSearch.Columns[2].Width = 60;
                        dgInvSearch.Columns[3].Width = 70;
                        dgInvSearch.Columns[4].Width = 80;
                        dgInvSearch.Columns[5].Visible = false;
                        dgInvSearch.Columns[4].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
                        dgInvSearch.Columns[4].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
                        dgInvSearch.Columns[6].Width = 150;
                        if (dgInvSearch.RowCount > 0)
                        {
                            pnlInvSearch.Focus();
                            SearchVisible(false);
                            pnlSearch.Visible = false;
                            e.SuppressKeyPress = true;
                            dgInvSearch.Focus();
                            dgInvSearch.CurrentCell = dgInvSearch[0, dgInvSearch.CurrentRow.Index];

                        }
                        txtInvNoSearch.Text = "";
                        cmbPartyNameSearch.SelectedIndex = 0;
                        return;
                    }

                    tempNo = ObjQry.ReturnLong("Select PKOtherVoucherNo From TOtherVoucherEntry Where Reference='" + txtInvNoSearch.Text + "' and VoucherTypeCode=" + VoucherType + " AND CompanyNo=" + DBGetVal.CompanyNo + "", CommonFunctions.ConStr);
                    if (tempNo > 0)
                    {
                        ID = tempNo;
                        SetNavigation();
                        FillControls();

                        pnlSearch.Visible = false;
                        btnNew.Enabled = true;
                        btnUpdate.Enabled = true;
                        btnDelete.Enabled = true;
                        btnOrderClosed.Enabled = true;
                        SearchVisible(false);
                    }
                    else
                    {
                        pnlSearch.Visible = false;
                        DisplayMessage("Bill Not Found");
                        pnlSearch.Visible = true;
                        rbInvNo.Focus();
                        rbType(true);
                        txtInvNoSearch.Text = "";
                        txtSearch.Text = "";
                        cmbPartyName.SelectedIndex = 0;
                        //SearchVisible(false);
                    }
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    e.SuppressKeyPress = true;
                    pnlSearch.Visible = false;
                    pnlPartySearch.Visible = false;
                    btnCancelSearch_Click(sender, new EventArgs());
                    btnNew.Focus();
                }
                else if (e.KeyCode == Keys.Right)
                {
                    e.SuppressKeyPress = true;
                    rbDocNo.Checked = true;
                    rbType_CheckedChanged(rbPartyName, null);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbPartyNameSearch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (pnlPartySearch.Visible == true)
                    {
                        pnlPartySearch.Visible = false;
                        pnlSearch.Visible = true;
                    }
                    else
                    {

                        pnlPartySearch.Visible = true;
                        int x = dgBill.GetCellDisplayRectangle(0, 0, true).X + 200;//(Screen.PrimaryScreen.WorkingArea.Width) / 2;
                        int y = dgBill.GetCellDisplayRectangle(0, 0, true).Y + 100;
                        pnlPartySearch.SetBounds(x, y, dgPartySearch.Width + 10, dgPartySearch.Height + 10);
                        string str = "SELECT    0 as [#], TOtherVoucherEntry.VoucherUserNo AS [Doc #], TOtherVoucherEntry.Reference AS 'Invoice No', TOtherVoucherEntry.VoucherDate AS 'Date', TOtherVoucherEntry.BilledAmount AS 'Amount'," +
                                     "TOtherVoucherEntry.PkOtherVoucherNo FROM TOtherVoucherEntry INNER JOIN    MLedger ON TOtherVoucherEntry.LedgerNo = MLedger.LedgerNo WHERE (TOtherVoucherEntry.VoucherTypeCode IN (" + VoucherType + "," + VchType.PurchaseOrder + "))  AND (TOtherVoucherEntry.CompanyNo = " + DBGetVal.CompanyNo + ")" +
                                     "and TOtherVoucherEntry.LedgerNo=" + ObjFunction.GetComboValue(cmbPartyNameSearch) + " " +
                                     "Order By  TOtherVoucherEntry.VoucherUserNo desc,TOtherVoucherEntry.VoucherDate desc, TOtherVoucherEntry.Reference desc";
                        dgPartySearch.DataSource = ObjFunction.GetDataView(str).Table.DefaultView;
                        dgPartySearch.Columns[0].Width = 50;
                        dgPartySearch.Columns[1].Width = 50;
                        dgPartySearch.Columns[2].Width = 150;
                        dgPartySearch.Columns[3].Width = 80;
                        dgPartySearch.Columns[4].Width = 110;
                        dgPartySearch.Columns[5].Visible = false;
                        if (dgPartySearch.RowCount > 0)
                        {

                            pnlSearch.Visible = false;
                            e.SuppressKeyPress = true;
                            pnlPartySearch.Focus();
                            dgPartySearch.Focus();
                            dgPartySearch.CurrentCell = dgPartySearch[0, dgPartySearch.CurrentRow.Index];
                            SearchVisible(false);
                        }
                        else
                        {
                            pnlPartySearch.Visible = false;
                            pnlSearch.Visible = false;
                            DisplayMessage("Bill Not Found");
                            pnlSearch.Visible = true;
                            rbPartyName.Focus();
                            //SearchVisible(false);
                        }
                        txtInvNoSearch.Text = "";
                        txtSearch.Text = "";
                        cmbPartyNameSearch.SelectedIndex = 0;

                    }
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    e.SuppressKeyPress = true;
                    pnlSearch.Visible = false;
                    pnlPartySearch.Visible = false;
                    btnCancelSearch_Click(sender, new EventArgs());
                    btnNew.Focus();
                }
                else if (e.KeyCode == Keys.Left)
                {
                    e.SuppressKeyPress = true;
                    rbDocNo.Checked = true;
                    rbType_CheckedChanged(rbPartyName, null);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgPartySearch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    long tempNo;
                    e.SuppressKeyPress = true;
                    tempNo = ObjQry.ReturnLong("Select PKOtherVoucherNo From TOtherVoucherEntry Where PkOtherVoucherNo=" + Convert.ToInt64(dgPartySearch.Rows[dgPartySearch.CurrentRow.Index].Cells[5].Value) + " and VoucherTypeCode=" + VoucherType + " AND CompanyNo=" + DBGetVal.CompanyNo + "", CommonFunctions.ConStr);
                    if (tempNo > 0)
                    {
                        ID = tempNo;
                        SetNavigation();
                        FillControls();
                        btnNew.Enabled = true;
                        btnUpdate.Enabled = true;
                        btnDelete.Enabled = true;
                        btnOrderClosed.Enabled = true;
                        pnlPartySearch.Visible = false;
                        btnNew.Focus();
                        SearchVisible(false);
                    }
                    else
                    {
                        txtInvNoSearch.Text = "";
                        txtSearch.Text = "";
                        cmbPartyName.SelectedIndex = 0;
                        DisplayMessage("Bill Not Found");
                        txtSearch.Focus();
                        //SearchVisible(false);
                    }

                }
                else if (e.KeyCode == Keys.Escape)
                {
                    e.SuppressKeyPress = true;
                    pnlPartySearch.Visible = false;
                    pnlSearch.Visible = true;
                    txtSearch.Focus();
                    rbType_CheckedChanged(sender, new EventArgs());
                }
                txtInvNoSearch.Text = "";
                txtSearch.Text = "";
                cmbPartyNameSearch.SelectedIndex = 0;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgPartySearch_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                e.Value = e.RowIndex + 1;

            }
            else if (e.ColumnIndex == 3)
            {
                e.Value = Convert.ToDateTime(e.Value).ToString("dd-MMM-yy");
            }
        }

        private void rbType_CheckedChanged(object sender, EventArgs e)
        {
            rbType(true);

        }

        private void rbType(bool IsSetFocus)
        {
            try
            {
                rbInvNo.Enabled = true;
                rbDocNo.Enabled = true;
                rbPartyName.Enabled = true;
                if (rbInvNo.Checked == true)
                {
                    if (IsSetFocus)
                    {
                        lblLable.Visible = true;
                        lblLable.Text = "Inv No :";
                        txtInvNoSearch.Width = 162;
                        txtInvNoSearch.Location = new System.Drawing.Point(90, 39);
                        txtInvNoSearch.Visible = true;
                        txtSearch.Visible = false;
                        cmbPartyNameSearch.Visible = false;
                        txtInvNoSearch.Focus();
                    }
                }
                else if (rbDocNo.Checked == true)
                {
                    if (IsSetFocus)
                    {
                        lblLable.Visible = true;
                        lblLable.Text = "Doc No :";
                        txtSearch.Width = 162;
                        txtSearch.Location = new System.Drawing.Point(90, 39);
                        txtSearch.Visible = true;
                        txtInvNoSearch.Visible = false;
                        cmbPartyNameSearch.Visible = false;
                        txtSearch.Focus();
                    }
                }
                else if (rbPartyName.Checked == true)
                {
                    if (IsSetFocus)
                    {
                        lblLable.Visible = true;
                        lblLable.Text = "Party Name :";
                        cmbPartyNameSearch.Width = 330;
                        cmbPartyNameSearch.Location = new System.Drawing.Point(90, 39);
                        cmbPartyNameSearch.Visible = true;
                        txtSearch.Visible = false;
                        txtInvNoSearch.Visible = false;
                        cmbPartyNameSearch.Focus();
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void rbType_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                rbType(true);
            }
            else if (e.KeyCode == Keys.Escape)
            {
                e.SuppressKeyPress = true;
                pnlSearch.Visible = false;
                pnlPartySearch.Visible = false;
                btnSave.Focus();
                SearchVisible(false);
            }
        }

        private void SearchVisible(bool flag)
        {
            txtSearch.Visible = flag;
            cmbPartyNameSearch.Visible = flag;
            txtInvNoSearch.Visible = flag;
            lblLable.Visible = flag;
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                long tempNo;
                if (e.KeyCode == Keys.Enter)
                {


                    tempNo = ObjQry.ReturnLong("Select PKOtherVoucherNo From TOtherVoucherEntry Where VoucherUserNo=" + txtSearch.Text + " and VoucherTypeCode=" + VoucherType + " AND CompanyNo=" + DBGetVal.CompanyNo + "", CommonFunctions.ConStr);
                    if (tempNo > 0)
                    {
                        ID = tempNo;
                        SetNavigation();
                        FillControls();

                        pnlSearch.Visible = false;
                        btnNew.Enabled = true;
                        btnUpdate.Enabled = true;
                        btnDelete.Enabled = true;
                        btnOrderClosed.Enabled = false;
                        SearchVisible(false);
                    }
                    else
                    {
                        pnlSearch.Visible = false;
                        DisplayMessage("Bill Not Found");
                        pnlSearch.Visible = true;
                        rbDocNo.Focus();
                        txtSearch.Focus();
                        txtSearch.Text = "";
                        //SearchVisible(false);
                    }
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    e.SuppressKeyPress = true;
                    pnlSearch.Visible = false;
                    pnlPartySearch.Visible = false;
                    btnCancelSearch_Click(sender, new EventArgs());
                    btnNew.Focus();
                }
                else if (e.KeyCode == Keys.Right)
                {
                    e.SuppressKeyPress = true;
                    rbPartyName.Checked = true;
                    rbType_CheckedChanged(rbPartyName, null);
                }
                else if (e.KeyCode == Keys.Left)
                {
                    e.SuppressKeyPress = true;
                    rbInvNo.Checked = true;
                    rbType_CheckedChanged(rbPartyName, null);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #endregion

        private void txtDiscount1_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            //    e.SuppressKeyPress = true;
            //    Control_Leave((object)txtDiscount1, new EventArgs());
            //}
        }

        private void txtDiscRupees1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                Control_Leave((object)txtDiscRupees1, new EventArgs());
                txtChrgRupees1.Focus();
            }
        }

        private void txtChrgRupees1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                Control_Leave((object)txtChrgRupees1, new EventArgs());
                txtReturnAmt.Focus();
            }
        }

        private void btnAdvanceSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Master.AdvancedSearch Adsch = new Kirana.Master.AdvancedSearch(GroupType.SundryCreditors);
                ObjFunction.OpenForm(Adsch);
                if (Adsch.LedgerNo != 0)
                {
                    cmbPartyName.SelectedValue = Adsch.LedgerNo;
                    cmbPartyName.Focus();
                    Adsch.Close();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void txtGrandTotal_TextChanged(object sender, EventArgs e)
        {
            lblGrandTotal.Text = txtGrandTotal.Text;
        }

        private void btnShowDetails_Click(object sender, EventArgs e)
        {
            try
            {
                if (BillSizeFlag == false)
                {
                    dgBill.Height = dgBill.Height - (pnlTotalAmt.Height + 10);
                    pnlTotalAmt.Location = new Point(dgBill.Height + 10, pnlTotalAmt.Location.Y);
                    pnlTotalAmt.Location = new Point(dgBill.Width - pnlTotalAmt.Width + 10, dgBill.Location.Y + dgBill.Height + 10);
                    pnlTotalAmt.Visible = true;
                    BillSizeFlag = true;
                }
                else
                {
                    BillSizeFlag = false;
                    pnlTotalAmt.Visible = false;
                    dgBill.Height = dgBill.Height + (pnlTotalAmt.Height + 10);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnShortcut_Click(object sender, EventArgs e)
        {
            if (pnlFooterInfo.Visible == false)
            {
                pnlFooterInfo.Dock = DockStyle.Bottom;
                //pnlFooterInfo.Height = 30;
                pnlFooterInfo.BorderStyle = BorderStyle.None;
                pnlFooterInfo.BringToFront();
                pnlFooterInfo.Visible = true;
            }
            else
            {
                pnlFooterInfo.Visible = false;
            }
        }

        private void btnNewCustomer_Click(object sender, EventArgs e)
        {
            try
            {
                long PartyNo = ObjFunction.GetComboValue(cmbPartyName);
                if (ObjFunction.CheckAllowMenu(24) == false) return;
                Form NewF = new Master.SupplierAE(-1);
                ObjFunction.OpenForm(NewF);
                if (((Master.SupplierAE)NewF).ShortID != 0)
                {
                    ObjFunction.FillCombo(cmbPartyName, "Select LedgerNo,LedgerName From MLedger Where GroupNo in (" + GroupType.SundryCreditors + ")  order by LedgerName", "New Entry");
                    if (((Master.SupplierAE)NewF).ShortID > 0)
                        cmbPartyName.SelectedValue = ((Master.SupplierAE)NewF).ShortID;
                    else
                        cmbPartyName.SelectedValue = PartyNo;
                    cmbPartyName.Focus();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgPartySearch_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtDiscRupees1_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(txtDiscRupees1, 2, 5, JitFunctions.MaskedType.NotNegative);
        }

        private void txtChrgRupees1_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(txtChrgRupees1, 2, 5, JitFunctions.MaskedType.NotNegative);
        }

        private void txtReturnAmt_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(txtReturnAmt, 2, 5, JitFunctions.MaskedType.NotNegative);
        }

        private void txtVisibility_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(txtVisibility, 2, 5, JitFunctions.MaskedType.NotNegative);
        }

        private void txtReturnAmt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                Control_Leave((object)txtReturnAmt, new EventArgs());
                txtVisibility.Focus();
            }
        }

        private void txtVisibility_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                Control_Leave((object)txtVisibility, new EventArgs());
                if (cmbPaymentType.Enabled == true)
                    cmbPaymentType.Focus();
                else
                    txtRemark.Focus();
            }
        }

        private void lstGroup2_KeyDown(object sender, KeyEventArgs e)
        {
            //string ItemListStr = "";
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                pnlGroup2.Visible = false;

                FillItemList(strItemQuery.Length - 1);
            }
            else if (e.KeyCode == Keys.Space)
            {
                dgBill.Focus();
                pnlGroup2.Visible = false;
            }
        }

        private void lstGroup1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_Bilingual)) == true)
                lstGroup1Lang.SelectedIndex = lstGroup1.SelectedIndex;
        }

        private void lstGroup1Lang_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_Bilingual)) == true)
            {
                if (lstGroup1.Items.Count > 0)
                    lstGroup1.SelectedIndex = lstGroup1Lang.SelectedIndex;
            }
        }

        private void PurchaseAE_Activated(object sender, EventArgs e)
        {
            try
            {
                if (btnNew.Visible == true && dtSearch.Rows.Count > 0)
                    FillControls();
                if (ExistFormFlag == true) return;
                if (isDoProcess)
                {
                    long Pid = ObjFunction.GetComboValue(cmbPartyName);
                    ObjFunction.FillComb(cmbPartyName, "Select LedgerNo,LedgerName From MLedger Where GroupNo in " +
                    "(" + GroupType.SundryCreditors + ") and IsActive='true' or LedgerNo=" + Pno + " order by LedgerName");
                    cmbPartyName.SelectedValue = Pid;
                }
                isDoProcess = false;
                if (lblSchemeStatus.Text == "Issued")
                {
                    if (ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo=( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + ")", CommonFunctions.ConStr) > 1)
                        btnUpdate.Visible = false;
                    else if (ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo in ( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + " and TR.TypeOfRef in(6))", CommonFunctions.ConStr) > 1)
                        btnUpdate.Visible = false;
                    else
                        btnUpdate.Visible = true;
                }
                else
                    btnUpdate.Visible = false;
                if ((ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo=( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + ")", CommonFunctions.ConStr) > 1) && PayType == 3)
                {
                    btnMixMode.Visible = false;
                }
                else if (PayType == 3 && tOtherVoucherEntry.IsVoucherLock == false && tOtherVoucherEntry.IsCancel == false)
                {
                    // btnMixMode.Visible = true;
                }
                else
                    btnMixMode.Visible = false;

            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void PurchaseAE_Deactivate(object sender, EventArgs e)
        {
            isDoProcess = true;
        }

        private void DeleteZeroQty(int rowindex)
        {
            bool flag;
            try
            {
                long PKStockTrnNo = Convert.ToInt64(dgBill.Rows[rowindex].Cells[ColIndex.PkStockTrnNo].Value);
                if (PKStockTrnNo != 0)
                {
                    DeleteDtls(1, PKStockTrnNo);
                    DataTable dtB = dtBillCollect[rowindex];
                    for (int row = 0; row < dtB.Rows.Count; row++)
                    {
                        dtB.Rows[0][2] = "0";
                        dtB.AcceptChanges();
                    }
                    flag = false;
                    for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                    {
                        if (rowindex != i)
                        {
                            if (Convert.ToInt64(dgBill.Rows[rowindex].Cells[ColIndex.PkVoucherNo].Value) == Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.PkVoucherNo].Value))
                            { flag = true; break; }
                        }
                    }
                    if (flag == false)
                        DeleteDtls(2, Convert.ToInt64(dgBill.Rows[rowindex].Cells[ColIndex.PkVoucherNo].Value));

                    //For Purchase LedgerNo
                    flag = false;
                    for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                    {
                        if (rowindex != i)
                        {
                            if (Convert.ToInt64(dgBill.Rows[rowindex].Cells[ColIndex.SalesVchNo].Value) == Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.SalesVchNo].Value))
                            { flag = true; break; }
                        }
                    }
                    if (flag == false)
                        DeleteDtls(2, Convert.ToInt64(dgBill.Rows[rowindex].Cells[ColIndex.SalesVchNo].Value));

                    //FOr TaxLedgerNo
                    flag = false;
                    for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                    {
                        if (rowindex != i)
                        {
                            if (Convert.ToInt64(dgBill.Rows[rowindex].Cells[ColIndex.TaxVchNo].Value) == Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.TaxVchNo].Value))
                            { flag = true; break; }
                        }
                    }
                    if (flag == false)
                        DeleteDtls(2, Convert.ToInt64(dgBill.Rows[rowindex].Cells[ColIndex.TaxVchNo].Value));

                    DataTable dt = dtBillCollect[rowindex];
                    for (int row = 0; row < dt.Rows.Count; row++)
                    {
                        if (Convert.ToInt64(dt.Rows[row].ItemArray[4].ToString()) != 0 && Convert.ToDouble(dt.Rows[row].ItemArray[2].ToString()) == 0)
                        {
                            DeleteDtls(4, Convert.ToInt64(dt.Rows[row].ItemArray[4].ToString()));
                        }
                    }
                }

                if (dgBill.Rows.Count - 1 == rowindex)
                {
                    dgBill.Rows.RemoveAt(rowindex);
                    dgBill.Rows.Add();
                }
                else
                    dgBill.Rows.RemoveAt(rowindex);
                dtBillCollect.RemoveAt(rowindex);

                CalculateTotal();

                dgBill.CurrentCell = dgBill[2, dgBill.Rows.Count - 1];
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnMixMode_Click(object sender, EventArgs e)
        {
            try
            {
                if (ID != 0)
                {
                    if (ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo in ( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + ")", CommonFunctions.ConStr) > 1)
                    {
                        btnUpdate.Visible = false;
                        btnMixMode.Visible = false;
                        btnDelete.Visible = false;
                        OMMessageBox.Show("Already this bill Payment is done", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        return;
                    }
                    else
                    {
                        if (ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo in ( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + " and TR.TypeOfRef in(6))", CommonFunctions.ConStr) > 1)
                        {
                            btnUpdate.Visible = false;
                            btnMixMode.Visible = false;
                            btnDelete.Visible = false;
                            OMMessageBox.Show("Already this bill Payment is done", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            return;
                        }
                    }

                    Form NewF = new Vouchers.PartialPurchasePayment(ID, Convert.ToDouble(txtGrandTotal.Text.Trim()), ObjFunction.GetComboValue(cmbPaymentType), ObjFunction.GetComboValue(cmbPartyName));
                    ObjFunction.OpenForm(NewF);
                    NewF.Left = 15;
                    if (((PartialPurchasePayment)NewF).DS == DialogResult.Cancel) return;
                    else if (((PartialPurchasePayment)NewF).DS == DialogResult.OK) FillControls();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbPartyName_Leave(object sender, EventArgs e)
        {
            try
            {
                if (ObjFunction.GetComboValue(cmbPartyName) == Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_PartyAC)))// && (ObjFunction.GetComboValue(cmbPaymentType) == 3))
                {
                    if (ID == 0)
                    {
                        cmbPaymentType.SelectedValue = 2;
                        cmbPaymentType.Enabled = false;
                    }
                }
                else
                {
                    if (ID == 0)
                    {
                        cmbPaymentType.SelectedValue = 3;
                        cmbPaymentType.Enabled = true;
                    }
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_AutoMFGMapping)) == true)
                        CheckMFGCompany();
                }
                if (ObjQry.ReturnLong("Select Count(*) From MManufacturerDetails Where LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + "", CommonFunctions.ConStr) > 0)
                {
                    chkFilterItem.Checked = true;
                    chkFilterItem.Enabled = true;
                }
                else
                {
                    chkFilterItem.Checked = false;
                    chkFilterItem.Enabled = false;
                }
                //if (ObjQry.ReturnLong(" Select Count(*) FROM  TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo " +
                //                            " where TVoucherEntry.VoucherDate='" + Convert.ToDateTime(dtpBillDate.Text).ToString("dd-MMM-yyyy") + "' and " +
                //                            " TVoucherEntry.Reference ='" + txtRefNo.Text.Trim() + "' and TVoucherEntry.VoucherTypeCode=" + VchType.Purchase + " " +
                //                            " and TVoucherDetails.LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " and TVoucherDetails.SrNo=501" +
                //                            " and TVoucherEntry.PkVoucherNo!=" + ID + "", CommonFunctions.ConStr) > 0)
                //{
                //    OMMessageBox.Show("Inv No Already Exist For the Party..", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                //    cmbPartyName.Focus();
                //}
                if (ID == 0) tempPartyNo = ObjFunction.GetComboValue(cmbPartyName);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbPaymentType_Leave(object sender, EventArgs e)
        {
            cmbPaymentType_KeyDown(sender, new KeyEventArgs(Keys.Enter));
        }

        private void cmbLocation_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                SetGodownValue();
                dgBill.Focus();
                dgBill.CurrentCell = dgBill[ColIndex.ItemName, dgBill.Rows.Count - 1];
            }
        }

        private void SetGodownValue()
        {
            DataRow dr = null;
            for (int i = 0; i < dtBillCollect.Count; i++)
            {
                DataTable dtStk = dtBillCollect[i];
                long pkStk = 0;
                for (int row = 0; row < dtStk.Rows.Count; row++)
                {
                    if (Convert.ToDouble(dtStk.Rows[row].ItemArray[4].ToString()) > 0)
                    {
                        pkStk = Convert.ToInt64(dtStk.Rows[row].ItemArray[4].ToString());
                        break;
                    }
                }
                for (int row = 0; row < dtStk.Rows.Count; row++)
                {
                    if (Convert.ToInt64(dtStk.Rows[row].ItemArray[0].ToString()) == ObjFunction.GetComboValue(cmbLocation))
                    {
                        dr = dtStk.Rows[row];
                        dr[2] = dgBill.Rows[i].Cells[ColIndex.Quantity].Value;
                        dr[3] = dgBill.Rows[i].Cells[ColIndex.ActualQty].Value;
                        dr[4] = pkStk;
                    }
                    else
                    {
                        dr = dtStk.Rows[row];
                        dr[2] = 0;
                        dr[3] = 0;
                        dr[4] = 0;
                    }
                }
                dtBillCollect.RemoveAt(i);
                dtBillCollect.Insert(i, dtStk);
            }
        }

        private void cmbLocation_Leave(object sender, EventArgs e)
        {
            cmbLocation_KeyDown(sender, new KeyEventArgs(Keys.Enter));
        }

        private void chkFilterItem_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFilterItem.Checked == true)
                chkFilterItem.Text = "Selected Mfg. Company(Ctrl+E)";
            else
                chkFilterItem.Text = "All Mfg. Company(Ctrl+E)";
        }

        public void CheckMFGCompany()
        {
            if (ObjFunction.GetComboValue(cmbPartyName) != 0)
            {
                bool flag = false;
                DBMManufacturerCompany dbMManufacturerCompany = new DBMManufacturerCompany();
                MManufacturerDetails mManufactuerDetails = new MManufacturerDetails();

                //string sql = " Select * From  (Select ManufacturerNo From MManufacturerDetails  Where LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + "  ) As Table1 " +
                //           " EXCEPT " +
                //           " Select * From (SELECT DISTINCT MManufacturerCompany.MfgCompNo FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo INNER JOIN TStock ON TVoucherEntry.PkVoucherNo = TStock.FKVoucherNo INNER JOIN " +
                //           " MStockItems ON TStock.ItemNo = MStockItems.ItemNo INNER JOIN MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo INNER JOIN MManufacturerCompany ON MStockGroup.MfgCompNo = MManufacturerCompany.MfgCompNo " +
                //           " WHERE (TVoucherEntry.VoucherTypeCode = " + VchType.Purchase + ") AND (TVoucherEntry.VoucherDate ='" + DBGetVal.ServerTime.Date + "' ) AND (TVoucherDetails.VoucherSrNo = 1) AND  (TVoucherDetails.LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + ")) AS Table2 " +
                //           " Union " +
                //           " Select * From (SELECT DISTINCT MManufacturerCompany.MfgCompNo FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo INNER JOIN TStock ON TVoucherEntry.PkVoucherNo = TStock.FKVoucherNo INNER JOIN " +
                //           " MStockItems ON TStock.ItemNo = MStockItems.ItemNo INNER JOIN MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo INNER JOIN MManufacturerCompany ON MStockGroup.MfgCompNo = MManufacturerCompany.MfgCompNo " +
                //           " WHERE (TVoucherEntry.VoucherTypeCode = " + VchType.Purchase + ") AND (TVoucherEntry.VoucherDate = '" + DBGetVal.ServerTime.Date + "') AND (TVoucherDetails.VoucherSrNo = 1) AND (TVoucherDetails.LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + ")) AS Table2 " +
                //           " EXCEPT " +
                //           " Select * From  (Select ManufacturerNo From MManufacturerDetails  Where LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + "  ) As Table1 ";

                string sql = "SELECT DISTINCT MManufacturerCompany.MfgCompNo FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo INNER JOIN TStock ON TVoucherEntry.PkVoucherNo = TStock.FKVoucherNo INNER JOIN " +
                           " MStockItems ON TStock.ItemNo = MStockItems.ItemNo INNER JOIN MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo INNER JOIN MManufacturerCompany ON MStockGroup.MfgCompNo = MManufacturerCompany.MfgCompNo " +
                           " WHERE (TVoucherEntry.VoucherTypeCode = " + VchType.Purchase + ") AND (TVoucherEntry.VoucherDate ='" + DBGetVal.ServerTime.Date + "' ) AND (TVoucherDetails.VoucherSrNo = 1) AND  (TVoucherDetails.LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + ") " +
                           " And MManufacturerCompany.MfgCompNo Not In(Select ManufacturerNo From MManufacturerDetails  Where LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + ") ";


                DataTable dt = new DataTable();
                dt = ObjFunction.GetDataView(sql).Table;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    mManufactuerDetails = new MManufacturerDetails();
                    mManufactuerDetails.LedgerNo = ObjFunction.GetComboValue(cmbPartyName);
                    mManufactuerDetails.PkSrNo = 0;
                    mManufactuerDetails.ManufacturerNo = Convert.ToInt64(dt.Rows[i].ItemArray[0].ToString());
                    mManufactuerDetails.UserID = DBGetVal.UserID;
                    mManufactuerDetails.CompanyNo = DBGetVal.CompanyNo;
                    mManufactuerDetails.UserID = DBGetVal.UserID;
                    mManufactuerDetails.UserDate = DBGetVal.ServerTime.Date;
                    dbMManufacturerCompany.AddMManufacturerDetails(mManufactuerDetails);
                    flag = true;
                }
                if (flag == true)
                {
                    dbMManufacturerCompany.ExecuteNonQueryStatements();
                }
            }
        }

        private void NewItemAdd(string BarCode)
        {
            if (ObjFunction.CheckAllowMenu(10) == false) return;
            Form NewF = new Master.StockItemSAE(-1, BarCode);
            ObjFunction.OpenForm(NewF);

            if (((Master.StockItemSAE)NewF).ShortID != 0)
            {
                string barcode = ObjQry.ReturnString("Select BarCode From MStockBarCode where ItemNo=" + ((Master.StockItemSAE)NewF).ShortID + "", CommonFunctions.ConStr);
                int rwindex = dgBill.CurrentCell.RowIndex;
                dgBill.CurrentRow.Cells[ColIndex.ItemName].Value = barcode;
                dgBill_CellEndEdit(dgBill, new DataGridViewCellEventArgs(ColIndex.ItemName, rwindex));
            }
        }

        private void dtpBillDate_Leave(object sender, EventArgs e)
        {
            if (ID == 0)
            {
                tempDate = dtpBillDate.Value.Date;
                ObjFunction.GetFinancialYear(dtpBillDate.Value, out dtFrom, out dtTo);
                txtInvNo.Text = (ObjQry.ReturnLong("Select max(VoucherUserNo) from TOtherVoucherEntry Where VoucherTypeCode=" + VoucherType + " AND VoucherDate>='" + dtFrom.Date + "' AND VoucherDate<='" + dtTo.Date + "'", CommonFunctions.ConStr) + 1).ToString();
            }
        }

        private void dgInvSearch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    long tempNo;
                    e.SuppressKeyPress = true;
                    tempNo = Convert.ToInt64(dgInvSearch.Rows[dgInvSearch.CurrentRow.Index].Cells[5].Value);
                    if (tempNo > 0)
                    {
                        ID = tempNo;
                        SetNavigation();
                        FillControls();
                        btnNew.Enabled = true;
                        btnUpdate.Enabled = true;
                        pnlPartySearch.Visible = false;
                        pnlInvSearch.Visible = false;
                        btnNew.Focus();
                        SearchVisible(false);
                    }
                    else
                    {
                        txtInvNoSearch.Text = "";
                        txtSearch.Text = "";
                        cmbPartyName.SelectedIndex = 0;
                        DisplayMessage("Bill Not Found");
                        txtSearch.Focus();
                        //SearchVisible(false);
                    }

                }
                else if (e.KeyCode == Keys.Escape)
                {
                    e.SuppressKeyPress = true;
                    pnlInvSearch.Visible = false;
                    pnlSearch.Visible = true;
                    txtSearch.Focus();
                    rbType_CheckedChanged(sender, new EventArgs());

                }
                txtInvNoSearch.Text = "";
                txtSearch.Text = "";
                cmbPartyNameSearch.SelectedIndex = 0;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgInvSearch_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                e.Value = e.RowIndex + 1;

            }
            else if (e.ColumnIndex == 3)
            {
                e.Value = Convert.ToDateTime(e.Value).ToString("dd-MMM-yy");
            }
        }

        private void txtOtherTax_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(txtOtherTax, 2, 5, JitFunctions.MaskedType.NotNegative);
        }

        private void txtOtherTax_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                Control_Leave((object)txtOtherTax, new EventArgs());
                txtReturnAmt.Focus();
            }
        }

        private void btnAdvanceItemSelection_Click(object sender, EventArgs e)
        {
            Form NewF = new Vouchers.AdvanceItemSelection();
            ObjFunction.OpenForm(NewF);
            NewF.Left = 15;

        }

        private void btnOk1_Click(object sender, EventArgs e)
        {
            int rowcnt = 0;
            int rowindx = dgBill.CurrentCell.RowIndex;

            for (int i = 0; i < dgItemList.Rows.Count; i++)
            {
                
                if (i >= dgBill.Rows.Count - 1 - rowindx)
                {
                    if (dgBill.Rows[dgBill.Rows.Count - 1].Cells[ColIndex.ItemName].Value != null)
                        if (dgBill.Rows[dgBill.Rows.Count - 1].Cells[ColIndex.ItemName].Value.ToString() != "")
                            dgBill.Rows.Add();

                    rowcnt = dgBill.Rows.Count - 1;
                }
                else
                    rowcnt = rowindx;
                if (Convert.IsDBNull(dgItemList.Rows[i].Cells[3].Value) == true) dgItemList.Rows[i].Cells[3].Value = "0";
                dgBill.CurrentCell = dgBill.Rows[rowcnt].Cells[ColIndex.ItemName];

                if (Convert.ToInt16(dgItemList.Rows[i].Cells[3].Value) > 0)
                {
                    long ino = Convert.ToInt64(dgItemList.Rows[i].Cells[0].Value);

                    dgBill.Rows[rowcnt].Cells[ColIndex.UOMNo].Value = Convert.ToInt64(dgItemList.Rows[i].Cells[16].Value);
                    dgBill.Rows[rowcnt].Cells[ColIndex.Rate].Value = Convert.ToDouble(dgItemList.Rows[i].Cells[4].Value).ToString("0.00");//lstRate.Text;
                    dgBill.Rows[rowcnt].Cells[ColIndex.PkRateSettingNo].Value = Convert.ToInt64(dgItemList.Rows[i].Cells[15].Value);//lstRate.SelectedValue;
                    dgBill.Rows[rowcnt].Cells[ColIndex.MRP].Value = Convert.ToDouble(dgItemList.Rows[i].Cells[6].Value).ToString("0.00");
                    dgBill.Rows[rowcnt].Cells[ColIndex.TempMRP].Value = Convert.ToDouble(dgItemList.Rows[i].Cells[6].Value).ToString("0.00");
                    dgBill.Rows[rowcnt].Cells[ColIndex.UOM].Value = dgItemList.Rows[i].Cells[5].Value;
                    dgBill.Rows[rowcnt].Cells[ColIndex.Quantity].Value = dgItemList.Rows[i].Cells[3].Value;

                    pnlItemName.Visible = false;
                    Desc_MoveNext(ino, 0);
                    //dgBill.Rows[rowcnt].Cells[ColIndex.ItemNo].Value = ino;
                    //dgBill.Rows[rowcnt].Cells[ColIndex.PkBarCodeNo].Value = 0;

                    //DataTable dtItem = ObjFunction.GetDataView("Select ItemName,CompanyNo from MStockItems_V(NULL,NULL,NULL,NULL,NULL,NULL,NULL) where ItemNo = " + ino + " AND IsActive='true'").Table;
                    //dgBill.Rows[rowcnt].Cells[ColIndex.ItemName].Value = dtItem.Rows[0].ItemArray[0].ToString();
                    //dgBill.Rows[rowcnt].Cells[ColIndex.StockCompanyNo].Value = dtItem.Rows[0].ItemArray[1].ToString();
                    ////dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemName].Value = ObjQry.ReturnString("Select ItemName from MStockItems_V(NULL,NULL) where ItemNo = " + ItemNo,CommonFunctions.ConStr);

                    //if (ItemType == 2)
                    //    dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemName].Value += " - " + dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.Barcode].Value.ToString();

                    //dgBill.Rows[i].Cells[ColIndex.Quantity].Value = Convert.ToInt16(dgItemList.Rows[i].Cells[3].Value.ToString());
                }
            }
            pnlItemName.Visible = false;
        }


        private void dgItemList_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            TextBox txt = (TextBox)e.Control;
            if (dgItemList.CurrentCell.ColumnIndex == 3)
            {
                TextBox txt1 = (TextBox)e.Control;
                txt1.TextChanged += new EventHandler(txtQuantity1_TextChanged);
            }
        }

        private void SetSchemeStatus(int StatusCode)
        {
            if (StatusCode == 0)
            {
                lblSchemeStatus.Text = "Issued";
                lblSchemeStatus.ForeColor = Color.Blue;
                btnUpdate.Visible = true;
                btnDelete.Visible = true;
                btnOrderClosed.Visible = true;
            }
            else if (StatusCode == 1)
            {
                lblSchemeStatus.Text = "Partly Fulfilled";
                lblSchemeStatus.ForeColor = Color.Green;
                btnUpdate.Visible = false;
                btnDelete.Visible = true;
                btnOrderClosed.Visible = true;
            }
            else if (StatusCode == 2)
            {
                lblSchemeStatus.Text = "Completed";
                lblSchemeStatus.ForeColor = Color.Red;
                btnUpdate.Visible = false;
                btnDelete.Visible = false;
                btnOrderClosed.Visible = false;
            }
            else if (StatusCode == 3)
            {
                lblSchemeStatus.Text = "Cancelled";
                lblSchemeStatus.ForeColor = Color.Red;
                btnUpdate.Visible = false;
                btnDelete.Visible = false;
                btnOrderClosed.Visible = false;
            }
            else if (StatusCode == 4)
            {
                lblSchemeStatus.Text = "Order Closed";
                lblSchemeStatus.ForeColor = Color.Red;
                btnUpdate.Visible = false;
                btnDelete.Visible = false;
                btnOrderClosed.Visible = false;
            }
        }

        private void btnOrderClosed_Click(object sender, EventArgs e)
        {
            try
            {
                if (ID == 0) return;
                if (OMMessageBox.Show("Are you sure you want to close the order ?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                {
                    dbtOtherVoucherEntry = new DBTOtherVoucherEntry();
                    tOtherVoucherEntry = new TOtherVoucherEntry();
                    tOtherVoucherEntry.PkOtherVoucherNo = ID;
                    tOtherVoucherEntry.VoucherStatus = 4;
                    dbtOtherVoucherEntry.UpdateStatusOVoucherEntry(tOtherVoucherEntry);

                    OMMessageBox.Show("Order Closed successfully.....", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                    setDisplay(true);
                    ObjFunction.LockButtons(true, this.Controls);
                    ObjFunction.LockControls(false, this.Controls);
                    FillControls();
                    dgBill.Enabled = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (ID != 0)
                {
                    string[] ReportSession;

                    ReportSession = new string[1];

                    ReportSession[0] = ID.ToString();
                    CrystalDecisions.CrystalReports.Engine.ReportDocument childForm;
                    childForm = null;
                    childForm = ObjFunction.LoadReportObject("GetPurchaseOrderBill.rpt", CommonFunctions.ReportPath);
                    
                    if (childForm != null)
                    {
                        DBReportGenerate objRpt = new DBReportGenerate(childForm, ReportSession);
                        if (objRpt.PrintReport() == true)
                        {
                            DisplayMessage("Bill Print Successfully!!!");
                        }
                        else
                        {
                            DisplayMessage("Bill not Print !!!");
                        }
                    }
                    else
                    {
                        DisplayMessage("Bill Report not exist !!!");
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

    }
}
