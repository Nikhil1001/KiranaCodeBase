﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;


namespace Kirana.Vouchers
{
    /// <summary>
    /// This class is used for Physical Stock Entry AE
    /// </summary>
    public partial class PhysicalStockEntryAE : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        DBTVaucherEntry dbTVoucherEntry = new DBTVaucherEntry();
        TVoucherEntry tVoucherEntry = new TVoucherEntry();
        TStock tStock = new TStock();
        TStockGodown tStockGodown = new TStockGodown();
        DBMStockItems dbStockItems = new DBMStockItems();
        long ID = 0, VoucherUserNo = 0;
        DataTable dtSearch = new DataTable();
        DataTable dtMain;//, dtFill;
        int cntRow, Rtype = 0;
        int iItemNameStartIndex = 3, ItemType = 0, rowQtyIndex;
        //bool BarFlag = true;
        string[] strItemQuery, strItemQuery_last;
        long ItemNameType = 0;
        string Param1Value = "", Param2Value = "", str = "";//strUom

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public PhysicalStockEntryAE()
        {
            InitializeComponent();
        }

        /// <summary>
        /// This is class of parameterised Constructor
        /// </summary>
        public PhysicalStockEntryAE(DataTable dt, int Rtype)
        {
            InitializeComponent();
            this.Rtype = Rtype;
            dtMain = dt;
        }

        private void PhysicalStockEntryAE_Load(object sender, EventArgs e)
        {
            try
            {
                ObjTrans.Execute("exec StockUpdateAll", CommonFunctions.ConStr);
                ItemNameType = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_ItemNameType));
                initItemQuery();
                ObjFunction.FillCombo(cmbGodown, "SELECT GodownNo, GodownName FROM MGodown WHERE (IsActive = 'true') And GodownNo<>1 ");
                cmbGodown.SelectedValue = "0";
                //dtSearch = ObjFunction.GetDataView("Select PkVoucherNo from TVoucherEntry Where VoucherTypeCode=" + VchType.PhysicalStock + "").Table;
                dtSearch = ObjFunction.GetDataView("SELECT DISTINCT TVoucherEntry.PkVoucherNo, ISNULL(TStockGodown.GodownNo, 0) AS GodownNo "+
                                                   " FROM  TStockGodown INNER JOIN "+
                                                   " TStock ON TStockGodown.FKStockTrnNo = TStock.PkStockTrnNo RIGHT OUTER JOIN "+
                                                   " TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo "+
                                                   " WHERE   (TVoucherEntry.VoucherTypeCode = " + VchType.PhysicalStock + ") AND TVoucherEntry.VoucherDate<='" + Convert.ToDateTime(dtpBillDate.Text).ToString(Format.DDMMMYYYY) + "' " +
                                                   " ORDER BY TVoucherEntry.PkVoucherNo").Table;
                    //"SELECT DISTINCT TVoucherEntry.PkVoucherNo, TStockGodown.GodownNo "+
                    //                               " FROM  TVoucherEntry INNER JOIN "+
                    //                               " TStock ON TVoucherEntry.PkVoucherNo = TStock.FKVoucherNo INNER JOIN "+
                    //                               " TStockGodown ON TStock.PkStockTrnNo = TStockGodown.FKStockTrnNo "+
                    //                               " WHERE  (TVoucherEntry.VoucherTypeCode =" + VchType.PhysicalStock + ") "+
                    //                               " ORDER BY TVoucherEntry.PkVoucherNo").Table;
                dtpBillDate.Value = DBGetVal.ServerTime.Date;

                if (ObjQry.ReturnLong("Select isNull(PkVoucherNo,0) from TVoucherEntry where VoucherTypeCode=" + VchType.PhysicalStock + " and VoucherDate='" + Convert.ToDateTime(dtpBillDate.Text) + "'", CommonFunctions.ConStr) == 0)
                {
                    ID = EntryTvoucherEntry();
                    DataRow dr;
                    dr = dtSearch.NewRow();
                    dr[0] = ID;//ObjQry.ReturnLong("Select isNull(PkVoucherNo,0) from TVoucherEntry where VoucherTypeCode=" + VchType.PhysicalStock + " and VoucherDate='" + Convert.ToDateTime(dtpBillDate.Text) + "'", CommonFunctions.ConStr);
                    dr[1] = 0;
                    dtSearch.Rows.Add(dr);
                }
                else
                    ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                
                SetNavigation();
                if (Rtype != 0)
                {
                    btnLast.Visible = false;
                    btnPrev.Visible = false;
                    btnNext.Visible = false;
                    btnFirst.Visible = false;
                    cmbGodown.Enabled = false;
                    if (cmbGodown.Items.Count > 1)
                    {
                        cmbGodown.SelectedIndex = 1;
                        FillGrid();
                    }
                }
                else
                    setDisplay(false);
                new GridSearch(dgItemList, 1);
                formatPics();
                cmbGodown.Focus();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private long EntryTvoucherEntry()
        {
            try
            {
                dbTVoucherEntry = new DBTVaucherEntry();
                //DeleteStockGodown();

                //int VoucherSrNo = 1;
                //Voucher Header Entry 
                tVoucherEntry = new TVoucherEntry();
                tVoucherEntry.PkVoucherNo = ID;
                tVoucherEntry.VoucherTypeCode = VchType.PhysicalStock; ;
                tVoucherEntry.VoucherUserNo = VoucherUserNo;
                tVoucherEntry.VoucherDate = Convert.ToDateTime(dtpBillDate.Text);
                tVoucherEntry.VoucherTime = DBGetVal.ServerTime;
                tVoucherEntry.Narration = "Physical Stock Entry";
                tVoucherEntry.RateTypeNo = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_RateType));
                tVoucherEntry.Reference = "";
                tVoucherEntry.ChequeNo = 0;
                tVoucherEntry.ClearingDate = DBGetVal.ServerTime.Date;
                tVoucherEntry.CompanyNo = DBGetVal.CompanyNo;
                tVoucherEntry.BilledAmount = 0;
                tVoucherEntry.ChallanNo = "";
                tVoucherEntry.Remark = "";
                tVoucherEntry.MacNo = DBGetVal.MacNo;
                tVoucherEntry.PayTypeNo = 0;
                tVoucherEntry.TaxTypeNo = 0;


                tVoucherEntry.UserID = DBGetVal.UserID;
                tVoucherEntry.UserDate = DBGetVal.ServerTime.Date;
                dbTVoucherEntry.AddTVoucherEntry(tVoucherEntry);
                long tempID = dbTVoucherEntry.ExecuteNonQueryStatements();
                return tempID;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return 0;
            }
        }

        private void initItemQuery()
        {
            try
            {
                DataTable dtItemQuery = new DataTable();
                dtItemQuery = ObjFunction.GetDataView("SELECT * from MItemNameDisplayType WHERE ItemNameTypeNo = " + ItemNameType).Table;

                if (dtItemQuery.Rows.Count == 1)
                {
                    int qCount = 0;
                    for (int i = 1; i < 4; i++)
                    {
                        if (dtItemQuery.Rows[0]["Query" + i] != null && dtItemQuery.Rows[0]["Query" + i].ToString().Trim().Length > 0)
                        {
                            qCount++;
                        }
                    }

                    strItemQuery = new string[qCount];
                    strItemQuery_last = new string[qCount];
                    for (int i = 0; i < strItemQuery_last.Length; i++)
                    {
                        strItemQuery_last[i] = "";
                    }
                    qCount = 0;
                    for (int i = 1; i < 4; i++)
                    {
                        if (dtItemQuery.Rows[0]["Query" + i] != null && dtItemQuery.Rows[0]["Query" + i].ToString().Trim().Length > 0)
                        {
                            strItemQuery[qCount] = dtItemQuery.Rows[0]["Query" + i].ToString().Trim();
                            qCount++;
                        }
                    }

                    iItemNameStartIndex = Convert.ToInt32(dtItemQuery.Rows[0]["StartIndex"].ToString());
                    Param1Value = dtItemQuery.Rows[0]["Param1Value"].ToString();
                    Param2Value = dtItemQuery.Rows[0]["Param2Value"].ToString();
                }
                else
                {
                    OMMessageBox.Show("Please Select Valid Item Name display type in Sales Setting Form ...");
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillGrid()
        {

            try
            {
                DataTable dtFill = new DataTable();
                DataTable dtStock;
                if (Rtype == 0)
                {
                    dtStock = ObjFunction.GetDataView("Exec GetInventoryCountSchedule '" + dtpBillDate.Value.ToString("dd-MMM-yyyy") + "'").Table;
                }
                else
                    dtStock = dtMain;
                str = "";
                for (int i = 0; i < dtStock.Rows.Count; i++)
                {
                    if (str == "")
                        str += dtStock.Rows[i].ItemArray[0].ToString();
                    else
                        str += "," + dtStock.Rows[i].ItemArray[0].ToString();
                }

               //if (str != "")
                {
                    string sqlQuery = "";
                    dgBill.Rows.Clear();
                    str = str.Equals("") ? "0" : str;

                    sqlQuery = "Delete from MStockItemBalanceTemp where UserID = " + DBGetVal.UserID + "";
                    ObjTrans.Execute(sqlQuery, CommonFunctions.ConStr);

                    sqlQuery =" INSERT INTO MStockItemBalanceTemp (ItemNo, MRP,GodownNo,UserID,PrevStock,CurrentStock) "+
                                " Select TStock.ItemNo, MRateSetting.MRP, TStockGodown.GodownNo, "+DBGetVal.UserID+" as UserID, "+
                                " SUM(CASE WHEN TStock.FKVoucherNo < " + ID + " THEN (CASE WHEN TStock.TrnCode = 1 THEN (TStock.BilledQuantity) ELSE (TStock.BilledQuantity)*-1 END) else 0 end) as PrevStock, " +
                                " SUM(CASE WHEN TStock.TrnCode = 1 THEN (TStock.BilledQuantity) ELSE (TStock.BilledQuantity)*-1 END) as CurrentStock "+
                                " from TStock INNER JOIN "+
                                " TVoucherEntry ON TStock.FKVoucherNo = TVoucherEntry.PkVoucherNo AND TVoucherEntry.IsCancel = 'False'  INNER JOIN "+
                                " MRateSetting ON TStock.FkRateSettingNo = MRateSetting.PkSrNo INNER JOIN "+
                                " TStockGodown ON TStock.PkStockTrnNo = TStockGodown.FKStockTrnNo "+
                                " group by TStock.ItemNo, MRateSetting.MRP, TStockGodown.GodownNo";
                    ObjTrans.Execute(sqlQuery, CommonFunctions.ConStr);

                    sqlQuery = " SELECT DISTINCT 0 AS SrNo,MStockGroup.StockGroupName+' '+MStockItems.ItemName as ItemName, " +
                               //" (SELECT  ItemName FROM    dbo.MStockItems_V(NULL, Tab1.ItemNo, NULL, NULL, NULL, NULL, NULL) AS MStockItems_V_1) AS ItemName,"+
                               " Tab1.MRP, MUOM.UOMName, " +

                               " MSB.PrevStock AS Stock, " +
                               //
                               " MSB.CurrentStock AS StockNow, " + 
                               //
                               " 0 AS ActualStock, 0 AS Quantity, " +
                               " Tab1.ASaleRate AS Rate, Tab1.PurRate, 0 AS NetRate, 0 AS DiscPercentage, 0 AS DiscAmount, 0 AS DiscRupees, 0 AS DiscPercentage2, 0 AS DiscAmount2, " +
                               " 0 AS DiscRupees2, 0 AS NetAmount, 0 AS Amount, MStockBarcode.Barcode, 0 AS PkStockTrnNo, MStockBarcode.PkStockBarcodeNo, 0 AS PkVoucherNo, " +
                               " Tab1.ItemNo, MStockItems.UOMPrimary AS UOMNo, Tab1.PkSrNo AS PkRateSettingNo, 0 AS PkItemTaxInfo, Tab1.StockConversion AS StockFactor, " +
                               " 0 AS ActualQty, Tab1.MKTQty AS MKTQuantity, 0 AS TaxPercentage, 0 AS TaxAmount, 0 AS FkStockGodownNo, MStockItems.CompanyNo, 1 AS Type " +
                               //From
                               " FROM  " +
                               //Tab1
                               " (SELECT DISTINCT " +
                               " TStock_1.ItemNo, GetItemRateAll_1.MRP, GetItemRateAll_1.PkSrNo, GetItemRateAll_1.UOMNo, GetItemRateAll_1.ASaleRate, GetItemRateAll_1.PurRate, " +
                               " GetItemRateAll_1.StockConversion, GetItemRateAll_1.MKTQty " +
                               " FROM    TStock AS TStock_1 INNER JOIN " +
                               " MRateSetting AS GetItemRateAll_1 ON TStock_1.FkRateSettingNo = GetItemRateAll_1.PkSrNo INNER JOIN " +
                               " TStockGodown AS TStockGodown_3 ON TStock_1.PkStockTrnNo = TStockGodown_3.FKStockTrnNo " +
                               " WHERE      (TStock_1.FKVoucherNo = " + ID + ") AND (TStockGodown_3.GodownNo = " + ObjFunction.GetComboValue(cmbGodown) + ") " +
                               " UNION " +
                               " SELECT DISTINCT " +
                               " MStockCountSchedule.ItemNo, GetItemRateAll_1.MRP, GetItemRateAll_1.PkSrNo, GetItemRateAll_1.UOMNo, GetItemRateAll_1.ASaleRate, " +
                               " GetItemRateAll_1.PurRate, GetItemRateAll_1.StockConversion, GetItemRateAll_1.MKTQty " +
                               " FROM         MStockCountSchedule INNER JOIN " +
                               " dbo.GetItemRateAll(NULL, NULL, NULL, NULL, NULL, NULL) AS GetItemRateAll_1 ON MStockCountSchedule.ItemNo = GetItemRateAll_1.ItemNo " +
                               " WHERE     (MStockCountSchedule.ItemNo IN (" + str + "))) " +
                               " AS Tab1 INNER JOIN MUOM " + 
                               //tab1
                               " ON MUOM.UOMNo = Tab1.UOMNo INNER JOIN "+
                               " MStockItems INNER JOIN "+
                               " MStockBarcode ON MStockItems.ItemNo = MStockBarcode.ItemNo ON Tab1.ItemNo = MStockItems.ItemNo "+
                               " Inner Join MStockGroup ON  MStockGroup.StockGRoupNo=MStockItems.Groupno1 " +
                               " INNER JOIN MStockItemBalanceTemp MSB ON MSB.ItemNO = Tab1.ItemNo AND MSB.MRP = Tab1.MRP " + 
                               " AND MSB.GodownNo = " + ObjFunction.GetComboValue(cmbGodown) + " AND MSB.UserID = " + DBGetVal.UserID + " " +
                               " ORDER BY ItemName";
                    //if (ID != 0)
                    //{
                    //    sqlQuery = " SELECT   SrNo, ItemName, MRP, UOMName,Stock, StockNow, ActualStock, Quantity, Rate, PurRate, NetRate, DiscPercentage, DiscAmount, DiscRupees, DiscPercentage2, " +
                    //               " DiscAmount2, DiscRupees2, NetAmount, Amount, Barcode, PkStockTrnNo, PkStockBarcodeNo, PkVoucherNo, ItemNo, UOMNo, PkRateSettingNo, PkItemTaxInfo, " +
                    //               " StockFactor, ActualQty, MKTQuantity, TaxPercentage, TaxAmount, FkStockGodownNo, CompanyNo, Type " +
                    //               " FROM   (SELECT DISTINCT 0 AS SrNo, " +
                    //               " (SELECT     ItemName " +
                    //               " FROM    dbo.MStockItems_V(NULL, MStockItems_1.ItemNo, NULL, NULL, NULL, NULL, NULL) AS MStockItems_V_1) AS ItemName, MRateSetting.MRP, " +
                    //               " MUOM_1.UOMName, ISNULL " +
                    //               " ((SELECT     ISNULL(SUM(CASE WHEN trncode = 1 THEN quantity ELSE - quantity END), 0) " +
                    //               " + ISNULL(SUM(CASE WHEN trncode = 1 THEN FreeQty ELSE - FreeQty END), 0) AS Expr1 " +
                    //               " FROM   TStock INNER JOIN " +
                    //               " TStockGodown ON TStock.PkStockTrnNo = TStockGodown.FKStockTrnNo " +
                    //               " WHERE     (TStockGodown.GodownNo = " + ObjFunction.GetComboValue(cmbGodown) + ") AND (TStock.FkRateSettingNo IN " +
                    //               " (SELECT     PkSrNo " +
                    //               " FROM   MRateSetting AS MRateSetting_1 " +
                    //               " WHERE   (ItemNo = MStockItems_1.ItemNo) AND (MRP = MRateSetting.MRP))) " +//(TStock.FkRateSettingNo = MRateSetting.PkSrNo))) "+
                    //               " AND (TStock.ItemNo = MStockItems_1.ItemNo) " +
                    //               " AND (TStock.PkStockTrnNo IN " +
                    //               " (SELECT     TStock_4.PkStockTrnNo " +
                    //               " FROM    TVoucherEntry INNER JOIN " +
                    //               " TStock AS TStock_4 ON TVoucherEntry.PkVoucherNo = TStock_4.FKVoucherNo " +
                    //               " WHERE      (TStock_4.ItemNo = MStockItems_1.ItemNo) AND (TStock_4.FKVoucherNo < " + ID + ")  AND (TVoucherEntry.IsCancel = 'false')))), 0) AS Stock, ISNULL " +
                    //               " ((SELECT     ISNULL(SUM(CASE WHEN trncode = 1 THEN quantity ELSE - quantity END), 0) " +
                    //               " + ISNULL(SUM(CASE WHEN trncode = 1 THEN FreeQty ELSE - FreeQty END), 0) AS Expr1 " +
                    //               " FROM    TStock AS TStock_5 INNER JOIN " +
                    //               " TStockGodown AS TStockGodown_2 ON TStock_5.PkStockTrnNo = TStockGodown_2.FKStockTrnNo " +
                    //               " WHERE     (TStockGodown_2.GodownNo = " + ObjFunction.GetComboValue(cmbGodown) + ") AND (TStock_5.FkRateSettingNo IN " +
                    //               " (SELECT     PkSrNo " +
                    //               " FROM    MRateSetting AS MRateSetting_1 " +
                    //               " WHERE    (TStock_5.ItemNo = MStockItems_1.ItemNo) AND (MRP = MRateSetting.MRP))) AND (TStock_5.ItemNo = MStockItems_1.ItemNo) " +
                    //               " AND (TStock_5.PkStockTrnNo NOT IN " +
                    //               " (SELECT     TStock_1.PkStockTrnNo " +
                    //               " FROM    TVoucherEntry AS TVoucherEntry_2 INNER JOIN " +
                    //               " TStock AS TStock_1 ON TVoucherEntry_2.PkVoucherNo = TStock_1.FKVoucherNo " +
                    //               " WHERE   (TVoucherEntry_2.IsCancel = 'True') AND (TStock_1.ItemNo = MStockItems_1.ItemNo)))), 0) AS StockNow, 0 AS ActualStock, " +
                    //               " 0 AS Quantity, MRateSetting.ASaleRate AS Rate, MRateSetting.PurRate, 0 AS NetRate, 0 AS DiscPercentage, 0 AS DiscAmount, 0 AS DiscRupees, " +
                    //               " 0 AS DiscPercentage2, 0 AS DiscAmount2, 0 AS DiscRupees2, 0 AS NetAmount, 0 AS Amount, MStockBarcode_1.Barcode, 0 AS PkStockTrnNo, " +
                    //               " MStockBarcode_1.PkStockBarcodeNo, 0 AS PkVoucherNo, MStockItems_1.ItemNo, MStockItems_1.UOMPrimary AS UOMNo, " +
                    //               " MRateSetting.PkSrNo AS PkRateSettingNo, 0 AS PkItemTaxInfo, MRateSetting.StockConversion AS StockFactor, 0 AS ActualQty, " +
                    //               " MRateSetting.MKTQty AS MKTQuantity, 0 AS TaxPercentage, 0 AS TaxAmount, 0 AS FkStockGodownNo, " +
                    //               " MStockItems_1.CompanyNo, 1 AS Type " +
                    //               " FROM   TVoucherEntry AS TVoucherEntry_1 INNER JOIN " +
                    //               " TStock AS TStock_2 ON TVoucherEntry_1.PkVoucherNo = TStock_2.FKVoucherNo INNER JOIN " +
                    //               " TStockGodown AS TStockGodown_1 ON TStock_2.PkStockTrnNo = TStockGodown_1.FKStockTrnNo INNER JOIN " +
                    //               " MStockItems AS MStockItems_1 INNER JOIN " +
                    //               " MStockBarcode AS MStockBarcode_1 ON MStockItems_1.ItemNo = MStockBarcode_1.ItemNo ON TStock_2.ItemNo = MStockItems_1.ItemNo INNER JOIN " +
                    //               " dbo.GetItemRateAll(NULL, NULL, NULL, NULL, NULL, NULL) AS MRateSetting INNER JOIN " +
                    //               " MUOM AS MUOM_1 ON MRateSetting.UOMNo = MUOM_1.UOMNo ON TStock_2.FkRateSettingNo = MRateSetting.PkSrNo " +
                    //               " WHERE     (TStock_2.FKVoucherNo = " + ID + ") AND (TStockGodown_1.GodownNo = " + ObjFunction.GetComboValue(cmbGodown) + ")) AS tab1 ";
                    //        //" FROM   dbo.GetItemRateAll(NULL, NULL, NULL, NULL, NULL, NULL) AS MRateSetting INNER JOIN " +
                    //        //" MUOM AS MUOM_1 ON MRateSetting.UOMNo = MUOM_1.UOMNo INNER JOIN " +
                    //        //" MStockItems AS MStockItems_1 INNER JOIN " +
                    //        //" MStockBarcode AS MStockBarcode_1 ON MStockItems_1.ItemNo = MStockBarcode_1.ItemNo ON TStock_2.FkRateSettingNo = MRateSetting.PkSrNo " +//ON MRateSetting.ItemNo = MStockItems_1.ItemNo INNER JOIN " +
                    //        //" TVoucherEntry AS TVoucherEntry_1 INNER JOIN " +
                    //        //" TStock AS TStock_2 ON TVoucherEntry_1.PkVoucherNo = TStock_2.FKVoucherNo INNER JOIN " +
                    //        //" TStockGodown AS TStockGodown_1 ON TStock_2.PkStockTrnNo = TStockGodown_1.FKStockTrnNo ON MStockItems_1.ItemNo = TStock_2.ItemNo " +
                    //        //" WHERE  (TStock_2.FKVoucherNo = " + ID + ") AND (TStockGodown_1.GodownNo = " + ObjFunction.GetComboValue(cmbGodown) + ")) AS tab1 " +
                    //               //" UNION ALL ";
                    //}
                    //if (str != "")
                    //    sqlQuery = sqlQuery + " UNION ALL  SELECT     Tab1.SrNo, Tab1.ItemName, Tab1.MRP, Tab1.UOMName, Tab1.Stock, Tab1.StockNow, Tab1.ActualStock, Tab1.Quantity, Tab1.Rate, Tab1.PurRate, Tab1.NetRate, " +
                    //                          " Tab1.DiscPercentage, Tab1.DiscAmount, Tab1.DiscRupees, Tab1.DiscPercentage2, Tab1.DiscAmount2, Tab1.DiscRupees2, Tab1.NetAmount, Tab1.Amount, " +
                    //                          " Tab1.Barcode, Tab1.PkStockTrnNo, Tab1.PkStockBarcodeNo, Tab1.PkVoucherNo, Tab1.ItemNo, Tab1.UOMNo, Tab1.PkRateSettingNo, Tab1.PkItemTaxInfo, " +
                    //                          " Tab1.StockFactor, Tab1.ActualQty, Tab1.MKTQuantity, Tab1.TaxPercentage, Tab1.TaxAmount, Tab1.FkStockGodownNo, Tab1.CompanyNo, Tab1.Type " +
                    //                          " FROM   (SELECT     TStock.ItemNo, FkRateSettingNo " +
                    //                          " FROM   TStock   INNER JOIN " +
                    //                          " TStockGodown ON TStock.PkStockTrnNo = TStockGodown.FKStockTrnNo " +
                    //                          " WHERE  (TStock.ItemNo IN (" + str + ")) AND (TStock.FKVoucherNo = " + ID + ") AND (TStockGodown.GodownNo =  " + ObjFunction.GetComboValue(cmbGodown) + ")) AS tab RIGHT OUTER JOIN " +
                    //                          " (SELECT     SrNo, ItemName, MRP, UOMName, Stock, Stock AS StockNow, ActualStock, Quantity, Rate, PurRate, NetRate, DiscPercentage, DiscAmount, DiscRupees, " +
                    //                          " DiscPercentage2, DiscAmount2, DiscRupees2, NetAmount, Amount, Barcode, PkStockTrnNo, PkStockBarcodeNo, PkVoucherNo, ItemNo, UOMNo, " +
                    //                          " PkRateSettingNo, PkItemTaxInfo, StockFactor, ActualQty, MKTQuantity, TaxPercentage, TaxAmount, FkStockGodownNo, CompanyNo, Type " +
                    //                          " FROM   (SELECT DISTINCT 0 AS SrNo, " +
                    //                          " (SELECT     ItemName " +
                    //                          " FROM   dbo.MStockItems_V(NULL, MStockItems.ItemNo, NULL, NULL, NULL, NULL, NULL) AS MStockItems_V_1) AS ItemName, " +
                    //                          " MRateSetting.MRP, MUOM.UOMName, ISNULL " +
                    //                          " ((SELECT     ISNULL(SUM(CASE WHEN trncode = 1 THEN quantity ELSE - quantity END), 0)  " +
                    //                          " + ISNULL(SUM(CASE WHEN trncode = 1 THEN FreeQty ELSE - FreeQty END), 0) AS Expr1 " +
                    //                          " FROM   TStock AS TStock_3 INNER JOIN " +
                    //                          " TStockGodown AS TStockGodown_3 ON TStock_3.PkStockTrnNo = TStockGodown_3.FKStockTrnNo " +
                    //                          " WHERE     (TStockGodown_3.GodownNo = " + ObjFunction.GetComboValue(cmbGodown) + ") AND (TStock_3.FkRateSettingNo IN " +
                    //                          " (SELECT     PkSrNo " +
                    //                          " FROM          MRateSetting AS MRateSetting_1 " +
                    //                          " WHERE      (TStock_3.ItemNo = MStockItems.ItemNo) AND (MRP = MRateSetting.MRP))) AND " +
                    //                          " (TStock_3.ItemNo = MStockItems.ItemNo) AND (TStock_3.PkStockTrnNo NOT IN " +
                    //                          " (SELECT     TStock_1.PkStockTrnNo " +
                    //                          " FROM          TVoucherEntry AS TVoucherEntry_3 INNER JOIN " +
                    //                          " TStock AS TStock_1 ON TVoucherEntry_3.PkVoucherNo = TStock_1.FKVoucherNo " +
                    //                          " WHERE      (TVoucherEntry_3.IsCancel = 'True') AND (TStock_1.ItemNo = MStockItems.ItemNo)))), 0) AS Stock, 0 AS StockNow, " +
                    //                          " 0 AS ActualStock, 0 AS Quantity, MRateSetting.ASaleRate AS Rate, MRateSetting.PurRate, 0 AS NetRate, 0 AS DiscPercentage, " +
                    //                          " 0 AS DiscAmount, 0 AS DiscRupees, 0 AS DiscPercentage2, 0 AS DiscAmount2, 0 AS DiscRupees2, 0 AS NetAmount, 0 AS Amount, " +
                    //                          " MStockBarcode.Barcode, 0 AS PkStockTrnNo, MStockBarcode.PkStockBarcodeNo, 0 AS PkVoucherNo, MStockItems.ItemNo, " +
                    //                          " MStockItems.UOMPrimary AS UOMNo, MRateSetting.PkSrNo AS PkRateSettingNo, 0 AS PkItemTaxInfo, " +
                    //                          " MRateSetting.StockConversion AS StockFactor, 0 AS ActualQty, MRateSetting.MKTQty AS MKTQuantity, 0 AS TaxPercentage, 0 AS TaxAmount, " +
                    //                          " 0 AS FkStockGodownNo, MStockItems.CompanyNo, 1 AS Type " +
                    //                          " FROM          dbo.GetItemRateAll(NULL, NULL, NULL, NULL, NULL, NULL) AS MRateSetting INNER JOIN " +
                    //                          " MUOM ON MRateSetting.UOMNo = MUOM.UOMNo INNER JOIN " +
                    //                          " MStockItems INNER JOIN " +
                    //                          " MStockBarcode ON MStockItems.ItemNo = MStockBarcode.ItemNo ON MRateSetting.ItemNo = MStockItems.ItemNo " +
                    //                          " WHERE      (MStockItems.ItemNo IN (" + str + "))) AS Tab1_1) AS Tab1 ON Tab1.ItemNo = tab.ItemNo AND " +
                    //                          " Tab1.PkRateSettingNo = tab.FkRateSettingNo " +
                    //                          " WHERE  (tab.ItemNo IS NULL) AND (tab.FkRateSettingNo IS NULL) ORDER BY ItemName";
                    //" SELECT DISTINCT 0 AS SrNo,(SELECT ItemName FROM dbo.MStockItems_V(NULL, MStockItems.ItemNo, NULL, NULL, NULL, NULL, NULL) AS MStockItems_V_1) AS ItemName, MRateSetting.MRP,  MUOM.UOMName, ISNULL ((SELECT ISNULL(SUM(CASE WHEN trncode = 1 THEN quantity ELSE - quantity END), 0)  + ISNULL(SUM(CASE WHEN trncode = 1 THEN FreeQty ELSE - FreeQty END), 0) AS Expr1 FROM TStock Inner join TStockGodown on TStock.PkStockTrnNo=TStockGodown.fkStockTrnNo WHERE TStockGodown.GodownNo=(Case When(" + ObjFunction.GetComboValue(cmbGodown) + "=0) Then 2 Else " + ObjFunction.GetComboValue(cmbGodown) + " End) and  (FkRateSettingNo IN (SELECT PkSrNo FROM  MRateSetting AS MRateSetting_1 WHERE (TStock.ItemNo = MStockItems.ItemNo) AND (MRP = MRateSetting.MRP))) AND (TStock.ItemNo = MStockItems.ItemNo) AND (PkStockTrnNo NOT IN (SELECT TStock_1.PkStockTrnNo FROM " +
                    //     " TVoucherEntry INNER JOIN TStock AS TStock_1 ON TVoucherEntry.PkVoucherNo = TStock_1.FKVoucherNo WHERE (TVoucherEntry.IsCancel = 'True') AND (TStock_1.ItemNo = MStockItems.ItemNo)))), 0) AS Stock, 0 AS ActualStock, 0 AS Quantity, MRateSetting.ASaleRate AS Rate, MRateSetting.PurRate, 0 AS NetRate, 0 AS DiscPercentage, 0 AS DiscAmount, 0 AS DiscRupees, 0 AS DiscPercentage2, 0 AS DiscAmount2, 0 AS DiscRupees2, 0 AS NetAmount, 0 AS Amount, MStockBarcode.Barcode, 0 AS PkStockTrnNo, MStockBarcode.PkStockBarcodeNo,  0 AS PkVoucherNo, MStockItems.ItemNo, MStockItems.UOMPrimary AS UOMNo, MRateSetting.PkSrNo AS PkRateSettingNo, 0 AS PkItemTaxInfo,  MRateSetting.StockConversion AS StockFactor, 0 AS ActualQty, MRateSetting.MKTQty AS MKTQuantity, 0 AS TaxPercentage, 0 AS TaxAmount, 0 AS FkStockGodownNo, MStockItems.CompanyNo " +
                    //     " FROM dbo.GetItemRateAll(NULL, NULL, NULL, NULL, NULL, NULL) AS MRateSetting INNER JOIN MUOM ON MRateSetting.UOMNo = MUOM.UOMNo INNER JOIN MStockItems INNER JOIN MStockBarcode ON MStockItems.ItemNo = MStockBarcode.ItemNo ON MRateSetting.ItemNo = MStockItems.ItemNo " +
                    //     " WHERE (MStockItems.ItemNo IN (" + str + ")) ";
                    //if (ID != 0)
                    //{
                        //sqlQuery = sqlQuery + " and (TVoucherEntry_1.PkVoucherNo = " + ID + ") and VoucherTypeCode=" + VchType.PhysicalStock + "";
                    //}
                    // sqlQuery = sqlQuery + " ) AS Tab1";
                    dgBill.Rows.Clear();
                    dtFill = ObjFunction.GetDataView(sqlQuery).Table;


                    for (int j = 0; j < dtFill.Rows.Count; j++)
                    {
                        dgBill.Rows.Add();
                        for (int i = 0; i < dgBill.Columns.Count - 3; i++)
                        {
                            if (dtFill.Rows.Count > 0)
                            {
                                dgBill.Rows[j].Cells[i].Value = dtFill.Rows[j].ItemArray[i].ToString();
                                dgBill.Rows[j].Cells[ColIndex.MRP].Value = Convert.ToDouble(dtFill.Rows[j].ItemArray[ColIndex.MRP].ToString()).ToString("0.00");
                                dgBill.Rows[j].Cells[ColIndex.Rate].Value = Convert.ToDouble(dtFill.Rows[j].ItemArray[ColIndex.Rate].ToString()).ToString("0.00");
                                dgBill.Rows[j].Cells[ColIndex.PurRate].Value = Convert.ToDouble(dtFill.Rows[j].ItemArray[ColIndex.PurRate].ToString()).ToString("0.00");
                                if (i == ColIndex.Quantity)
                                {
                                    if (Convert.ToDouble(dgBill.Rows[j].Cells[ColIndex.Quantity].Value) != 0)
                                        dgBill.Rows[j].Cells[ColIndex.ActualStock].Value = Convert.ToDouble(dgBill.Rows[j].Cells[ColIndex.Stock].Value) - Convert.ToDouble(dgBill.Rows[j].Cells[ColIndex.Quantity].Value);
                                    //if (Convert.ToDouble(dgBill.Rows[j].Cells[ColIndex.ActualStock].Value) == 0)
                                    //    dgBill.Rows[j].Cells[ColIndex.Quantity].Value = "0.00";

                                }
                            }
                        }
                        dgBill.Rows[j].Cells[ColIndex.ItemName].ReadOnly = true;
                    }

                    dgBill.Columns[ColIndex.ActualStock].Visible = true;

                    dgBill.Rows.Add();
                    dgBill.Rows[dgBill.Rows.Count - 1].Cells[ColIndex.ItemName].ReadOnly = false;
                    dgBill.Rows[dgBill.Rows.Count - 1].Cells[ColIndex.ActualStock].ReadOnly = true;
                    if (dgBill.Rows.Count > 1)
                        dgBill.CurrentCell = dgBill[ColIndex.ActualStock, 0];
                    else
                        dgBill.CurrentCell = dgBill[ColIndex.ItemName, 0];
                    dgBill.Focus();

                }
                //else
                //{
                //    dgBill.Rows.Clear();
                //    dgBill.Rows.Add();
                //    dgBill.Rows[dgBill.Rows.Count - 1].Cells[ColIndex.ItemName].ReadOnly = false;
                //    dgBill.Rows[dgBill.Rows.Count - 1].Cells[ColIndex.ActualStock].ReadOnly = true;
                //    dgBill.CurrentCell = dgBill[ColIndex.ItemName, 0];
                //    dgBill.Focus();
                //}
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #region ColumnIndex
        private static class ColIndex
        {
            public static int SrNo = 0;
            public static int ItemName = 1;
            public static int MRP = 2;
            public static int UOM = 3;
            public static int Stock = 4;
            public static int StockNow = 5;
            public static int ActualStock = 6;
            public static int Quantity = 7;
            public static int Rate = 8;
            public static int PurRate = 9;
            public static int NetRate = 10;
            public static int DiscPercentage = 11;
            public static int DiscAmount = 12;
            public static int DiscRupees = 13;
            public static int DiscPercentage2 = 14;
            public static int DiscAmount2 = 15;
            public static int DiscRupees2 = 16;
            public static int NetAmt = 17;
            public static int Amount = 18;
            public static int Barcode = 19;
            public static int PkStockTrnNo = 20;
            public static int PkBarCodeNo = 21;
            public static int PkVoucherNo = 22;
            public static int ItemNo = 23;
            public static int UOMNo = 24;
            public static int PkRateSettingNo = 25;
            public static int PkItemTaxInfo = 26;
            public static int StockFactor = 27;
            public static int ActualQty = 28;
            public static int MKTQuantity = 29;
            public static int TaxPercentage = 30;
            public static int TaxAmount = 31;
            public static int FkStockGodownNo = 32;
            public static int StockCompanyNo = 33;
            public static int Type = 34;
            public static int StockAmt = 35;
            public static int DiffAmt = 36;
            public static int ActualAmt = 37;
            
        }
        #endregion

        private bool Validation()
        {
            try
            {
                bool flag = true;
                if (ObjFunction.GetComboValue(cmbGodown) > 0)
                {
                    for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                    {
                        dgBill.Rows[i].Cells[ColIndex.ActualStock].ErrorText = "";
                        if (dgBill.Rows[i].Cells[ColIndex.ActualStock].FormattedValue.ToString() == "")
                        {
                            flag = false;
                            dgBill.Rows[i].Cells[ColIndex.ActualStock].ErrorText = "Enter Valid Qty";
                            dgBill.CurrentCell = dgBill.Rows[i].Cells[ColIndex.ActualStock];
                            dgBill.Focus();
                            break;
                        }
                    }
                }
                else
                    flag = false;
                return flag;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return false;
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int cnt = 0;
                if (dgBill.Rows.Count <= 1)
                {
                    OMMessageBox.Show("Atleast one item required.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    return;
                }
                if (Validation() == true)
                {
                    if (ID != 0)
                    {
                        VoucherUserNo = ObjQry.ReturnLong("Select VoucherUserNo From TVoucherEntry Where PKVoucherNo=" + ID + "", CommonFunctions.ConStr);
                    }
                    else
                        VoucherUserNo = 0;
                    dbTVoucherEntry = new DBTVaucherEntry();
                    //DeleteStockGodown();

                    int VoucherSrNo = 1;
                    //Voucher Header Entry 
                    tVoucherEntry = new TVoucherEntry();
                    tVoucherEntry.PkVoucherNo = ID;
                    tVoucherEntry.VoucherTypeCode = VchType.PhysicalStock; ;
                    tVoucherEntry.VoucherUserNo = VoucherUserNo;
                    tVoucherEntry.VoucherDate = Convert.ToDateTime(dtpBillDate.Text);
                    tVoucherEntry.VoucherTime = DBGetVal.ServerTime;
                    tVoucherEntry.Narration = "Physical Stock Entry";
                    tVoucherEntry.RateTypeNo = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_RateType));
                    tVoucherEntry.Reference = "";
                    tVoucherEntry.ChequeNo = 0;
                    tVoucherEntry.ClearingDate = DBGetVal.ServerTime.Date;
                    tVoucherEntry.CompanyNo = DBGetVal.CompanyNo;
                    tVoucherEntry.BilledAmount = 0;
                    tVoucherEntry.ChallanNo = "";
                    tVoucherEntry.Remark = "";
                    tVoucherEntry.MacNo = DBGetVal.MacNo;
                    tVoucherEntry.PayTypeNo = 0;
                    tVoucherEntry.TaxTypeNo = 0;
                    tVoucherEntry.IsCancel = false;

                    tVoucherEntry.UserID = DBGetVal.UserID;
                    tVoucherEntry.UserDate = DBGetVal.ServerTime.Date;
                    dbTVoucherEntry.AddTVoucherEntry(tVoucherEntry);

                    for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                    {
                        if (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value) != 0)
                        {
                            cnt++;
                            tStock = new TStock();
                            if (Convert.ToInt64(dgBill[ColIndex.PkStockTrnNo, i].Value) == 0)
                            {
                                tStock.PkStockTrnNo = 0;
                            }
                            else
                            {
                                tStock.PkStockTrnNo = Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.PkStockTrnNo].Value);
                            }

                            tStock.GroupNo = GroupType.CapitalAccount;
                            tStock.ItemNo = Convert.ToInt64(dgBill[ColIndex.ItemNo, i].Value.ToString());
                            tStock.FkVoucherSrNo = VoucherSrNo;
                            if (Convert.ToDouble(dgBill[ColIndex.Quantity, i].Value.ToString()) > 0)
                                tStock.TrnCode = 2;
                            else
                                tStock.TrnCode = 1;

                            tStock.Quantity = Math.Abs(Convert.ToDouble(dgBill[ColIndex.Quantity, i].Value.ToString()));
                            tStock.BilledQuantity = Math.Abs(Convert.ToDouble(dgBill[ColIndex.Quantity, i].Value.ToString())) * Math.Abs(Convert.ToDouble(dgBill[ColIndex.StockFactor, i].Value.ToString()));
                            tStock.Rate = Convert.ToDouble(dgBill[ColIndex.Rate, i].Value.ToString());
                            tStock.Amount = Convert.ToDouble(dgBill[ColIndex.Amount, i].Value.ToString());
                            tStock.TaxPercentage = Convert.ToDouble(dgBill[ColIndex.TaxPercentage, i].Value.ToString());
                            tStock.TaxAmount = Convert.ToDouble(dgBill[ColIndex.TaxAmount, i].Value.ToString());
                            tStock.DiscPercentage = Convert.ToDouble(dgBill[ColIndex.DiscPercentage, i].Value.ToString());
                            tStock.DiscAmount = Convert.ToDouble(dgBill[ColIndex.DiscAmount, i].Value.ToString());
                            tStock.DiscRupees = Convert.ToDouble(dgBill[ColIndex.DiscRupees, i].Value.ToString());
                            tStock.DiscPercentage2 = Convert.ToDouble(dgBill[ColIndex.DiscPercentage2, i].Value.ToString());
                            tStock.DiscAmount2 = Convert.ToDouble(dgBill[ColIndex.DiscAmount2, i].Value.ToString());
                            tStock.DiscRupees2 = Convert.ToDouble(dgBill[ColIndex.DiscRupees2, i].Value.ToString());
                            tStock.NetRate = Convert.ToDouble(dgBill[ColIndex.NetRate, i].Value.ToString());
                            tStock.NetAmount = Convert.ToDouble(dgBill[ColIndex.NetAmt, i].Value.ToString());
                            tStock.FkStockBarCodeNo = Convert.ToInt64(dgBill[ColIndex.PkBarCodeNo, i].Value.ToString());
                            tStock.FkUomNo = Convert.ToInt64(dgBill[ColIndex.UOMNo, i].Value.ToString());
                            tStock.FkRateSettingNo = Convert.ToInt64(dgBill[ColIndex.PkRateSettingNo, i].Value.ToString());
                            tStock.FkItemTaxInfo = Convert.ToInt64(dgBill[ColIndex.PkItemTaxInfo, i].Value.ToString());
                            tStock.UserID = DBGetVal.UserID;
                            tStock.UserDate = DBGetVal.ServerTime.Date;
                            tStock.FreeQty = 0; 
                            tStock.FreeUOMNo = 1;
                            tStock.CompanyNo = Convert.ToInt64(dgBill[ColIndex.StockCompanyNo, i].Value.ToString());
                            dbTVoucherEntry.AddTStock(tStock);


                            //if (Convert.ToDouble(dgBill[ColIndex.Quantity, i].Value.ToString()) > 0)
                            //{
                            tStockGodown = new TStockGodown();
                            tStockGodown.PKStockGodownNo = Convert.ToInt64(dgBill[ColIndex.FkStockGodownNo, i].Value.ToString());
                            tStockGodown.ItemNo = Convert.ToInt64(dgBill[ColIndex.ItemNo, i].Value.ToString());
                            tStockGodown.GodownNo = ObjFunction.GetComboValue(cmbGodown);
                            tStockGodown.Qty = Math.Abs(Convert.ToDouble(dgBill[ColIndex.Quantity, i].Value.ToString()));
                            tStockGodown.ActualQty = Math.Abs(Convert.ToDouble(dgBill[ColIndex.Quantity, i].Value.ToString())) * Math.Abs(Convert.ToDouble(dgBill[ColIndex.StockFactor, i].Value.ToString()));//Math.Abs(Convert.ToDouble(dgBill[ColIndex.Quantity, i].Value.ToString()));
                            tStockGodown.UserID = DBGetVal.UserID;
                            tStockGodown.UserDate = DBGetVal.ServerTime.Date;
                            tStockGodown.CompanyNo = Convert.ToInt64(dgBill[ColIndex.StockCompanyNo, i].Value.ToString());
                            dbTVoucherEntry.AddTStockGodown(tStockGodown);
                            //}

                        }
                    }
                    if (cnt > 0)
                    {
                        long tempID = dbTVoucherEntry.ExecuteNonQueryStatements();
                        if (tempID != 0)
                        {
                            ObjTrans.Execute("exec StockUpdateAll", CommonFunctions.ConStr);
                            string strVChNo = ObjQry.ReturnLong("Select VoucherUserNo From TVoucherEntry Where PKVoucherNo=" + tempID + "", CommonFunctions.ConStr).ToString();
                            //if (ID == 0)
                            OMMessageBox.Show("Physical Stock Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            //else
                            //    OMMessageBox.Show("Physical Stock Updated Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);

                            //dtSearch = ObjFunction.GetDataView("Select PkVoucherNo from TVoucherEntry Where VoucherTypeCode=" + VoucherType + "").Table;
                            ID = tempID;
                            //dtSearch = ObjFunction.GetDataView("Select PkVoucherNo from TVoucherEntry Where VoucherTypeCode=" + VchType.PhysicalStock + "").Table;
                            dtSearch = ObjFunction.GetDataView("SELECT DISTINCT TVoucherEntry.PkVoucherNo, TStockGodown.GodownNo " +
                                                   " FROM  TVoucherEntry INNER JOIN " +
                                                   " TStock ON TVoucherEntry.PkVoucherNo = TStock.FKVoucherNo INNER JOIN " +
                                                   " TStockGodown ON TStock.PkStockTrnNo = TStockGodown.FKStockTrnNo " +
                                                   " WHERE  (TVoucherEntry.VoucherTypeCode =" + VchType.PhysicalStock + ") " +
                                                   " ORDER BY TVoucherEntry.PkVoucherNo").Table;
                            FillGrid();
                            SetNavigation();
                            if (Rtype == 0)
                            {
                                setDisplay(false);
                            }



                        }
                        else
                        {
                            OMMessageBox.Show("Physical Stock Not Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        OMMessageBox.Show("Enter Atleast One Items Physical Stock ", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgBill_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (ObjFunction.GetComboValue(cmbGodown) > 0)
                {
                    if (dgBill.CurrentCell.ColumnIndex == ColIndex.ItemName)
                    {
                        dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemNo].Value = 0;
                        dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkRateSettingNo].Value = 0;

                        Desc_Start();
                        //if (dgBill.CurrentCell.Value == null)
                        //{
                        //    MovetoNext move2n = new MovetoNext(m2n);
                        //    BeginInvoke(move2n, new object[] { e.RowIndex, ColIndex.ItemName });
                        //}
                        //if (cmbGodown.Enabled == true && dgBill.Rows.Count > 0 && dgBill.Rows[0].Cells[ColIndex.ItemName].Value != "" && dgBill.Rows[0].Cells[ColIndex.ItemName].Value != null)
                        //{
                        //    cmbGodown.Enabled = false;
                        //}
                    }
                    else if (e.ColumnIndex == ColIndex.ActualStock)
                    {
                        if (dgBill.Rows[e.RowIndex].Cells[e.ColumnIndex].Value == null || dgBill.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "")
                        {
                            dgBill.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "0";
                            //dgBill.Rows[e.RowIndex].Cells[ColIndex.Quantity].Value = Convert.ToDouble(dgBill.Rows[e.RowIndex].Cells[ColIndex.StockNow].Value) - Convert.ToDouble(dgBill.Rows[e.RowIndex].Cells[ColIndex.ActualStock].Value);
                        }
                        else
                        {
                            if (Convert.ToDouble(dgBill.Rows[e.RowIndex].Cells[ColIndex.ActualStock].Value) == 0)
                            {
                                if (OMMessageBox.Show("Are you sure you want to set Zero", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                                {
                                    dgBill.Rows[e.RowIndex].Cells[ColIndex.Quantity].Value = Convert.ToDouble(dgBill.Rows[e.RowIndex].Cells[ColIndex.StockNow].Value) - Convert.ToDouble(dgBill.Rows[e.RowIndex].Cells[ColIndex.ActualStock].Value);
                                }
                                else
                                {
                                    dgBill.Rows[e.RowIndex].Cells[ColIndex.ActualStock].Value = 0;
                                    dgBill.Rows[e.RowIndex].Cells[ColIndex.Quantity].Value = 0;
                                }
                            }
                            else
                            {
                                if (Convert.ToDouble(dgBill.Rows[e.RowIndex].Cells[ColIndex.ActualStock].Value) != 0)
                                    dgBill.Rows[e.RowIndex].Cells[ColIndex.Quantity].Value = Convert.ToDouble(dgBill.Rows[e.RowIndex].Cells[ColIndex.StockNow].Value) - Convert.ToDouble(dgBill.Rows[e.RowIndex].Cells[ColIndex.ActualStock].Value);
                                else
                                    dgBill.Rows[e.RowIndex].Cells[ColIndex.Quantity].Value = 0;
                            }
                        }
                        if (e.RowIndex < dgBill.Rows.Count - 2)
                        {
                            MovetoNext move2n = new MovetoNext(m2n);
                            BeginInvoke(move2n, new object[] { e.RowIndex + 1, 6 });
                            //dgBill.CurrentCell = dgBill[ColIndex.ActualStock, dgBill.Rows.Count - 1];
                        }
                        else if (e.RowIndex == dgBill.Rows.Count - 2)
                        {
                            MovetoNext move2n = new MovetoNext(m2n);
                            BeginInvoke(move2n, new object[] { e.RowIndex + 1, ColIndex.ItemName });
                            //dgBill.CurrentCell = dgBill[ColIndex.ItemName, dgBill.Rows.Count - 1];
                        }
                    }
                }
                else
                {
                    dgBill.CurrentCell.Value = "";
                    OMMessageBox.Show("Please Select Godown Name", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    cmbGodown.Focus();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgBill_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                if (dgBill.CurrentCell.ColumnIndex == ColIndex.ActualStock)
                {
                    TextBox txt1 = (TextBox)e.Control;
                    txt1.TextChanged += new EventHandler(txtQuantity_TextChanged);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void txtQuantity_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgBill.CurrentCell.ColumnIndex == ColIndex.ActualStock)
                {
                    ObjFunction.SetMasked((TextBox)sender, 2, 9, JitFunctions.MaskedType.NotNegative);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgBill_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 0)
                {
                    e.Value = (e.RowIndex + 1).ToString();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dtpBillDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ID = ObjQry.ReturnLong("Select isNull(PkVoucherNo,0) from TVoucherEntry where VoucherTypeCode=" + VchType.PhysicalStock + " and VoucherDate='" + Convert.ToDateTime(dtpBillDate.Text) + "'", CommonFunctions.ConStr);
                //FillGrid();
                if (dtpBillDate.Text == DBGetVal.ServerTime.Date.ToString("dd-MMM-yyyy"))
                    DisplayALL(true);
                else
                    DisplayALL(false);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void DisplayALL(bool flag)
        {
            BtnSave.Enabled = flag;
            btnCancel.Enabled = flag;
            btnPrint.Enabled = flag;
            cmbGodown.Enabled = flag;
        }

        private void FillControls()
        {
            try
            {
                if (ID != 0)
                {
                    tVoucherEntry = dbTVoucherEntry.ModifyTVoucherEntryByID(ID);
                    dtpBillDate.Value = tVoucherEntry.VoucherDate;
                }
                else
                {
                    dtpBillDate.Value = DBGetVal.ServerTime.Date;
                }
                FillGrid();
                if (dtpBillDate.Text == DBGetVal.ServerTime.Date.ToString("dd-MMM-yyyy"))
                    DisplayALL(true);
                else
                    DisplayALL(false);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #region Navigation Methods
        private void NavigationDisplay(int type)
        {
            try
            {
                long No = 0;
                if (type == 5)
                {
                    No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                    ID = No;
                }
                else if (type == 1)
                {
                    No = Convert.ToInt64(dtSearch.Rows[0].ItemArray[0].ToString());
                    cntRow = 0;
                    ID = No;
                }
                else if (type == 2)
                {
                    No = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    cntRow = dtSearch.Rows.Count - 1;
                    ID = No;
                }
                else
                {
                    if (type == 3)
                    {
                        cntRow = cntRow + 1;
                    }
                    else if (type == 4)
                    {
                        cntRow = cntRow - 1;
                    }

                    if (cntRow < 0)
                    {
                        OMMessageBox.Show("This is First Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow + 1;
                    }
                    else if (cntRow > dtSearch.Rows.Count - 1)
                    {
                        OMMessageBox.Show("This is Last Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow - 1;
                    }
                    else
                    {
                        No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                        ID = No;
                    }

                }

                DataRow[] dr = dtSearch.Select("GodownNo=" + dtSearch.Rows[cntRow].ItemArray[1].ToString());
                long godown = Convert.ToInt64(dr[0].ItemArray[1]);
                cmbGodown.SelectedValue = godown;
                FillControls();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void SetNavigation()
        {
            try
            {
                cntRow = 0;
                for (int i = 0; i < dtSearch.Rows.Count; i++)
                {
                    if (Convert.ToInt64(dtSearch.Rows[i].ItemArray[0].ToString()) == ID)
                    {
                        cntRow = i;
                        break;
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }



        private void setDisplay(bool flag)
        {
            try
            {
                btnFirst.Visible = flag;
                btnPrev.Visible = flag;
                btnNext.Visible = flag;
                btnLast.Visible = flag;
                //Btndelete.Visible = flag;
                //GridRange.Height = 25;
                if (dtSearch.Rows.Count == 0)
                {
                    btnFirst.Visible = false;
                    btnPrev.Visible = false;
                    btnNext.Visible = false;
                    btnLast.Visible = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            NavigationDisplay(1);
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            NavigationDisplay(4);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            NavigationDisplay(3);
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            NavigationDisplay(2);
        }

        #endregion

        private void delete_row()
        {
            try
            {
                //bool flag;
                if (dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.Type].EditedFormattedValue.ToString().Trim() == "" )
                {
                    if (OMMessageBox.Show("Are you sure want to delete this item ?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question, OMMessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        //if (dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkStockTrnNo].EditedFormattedValue.ToString().Trim() != "")
                        //{
                        //    long PKStockTrnNo = Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkStockTrnNo].Value);
                        //    long StockGodownNo = Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.FkStockGodownNo].Value);
                        //    //if (PKStockTrnNo != 0)
                        //    //{
                        //    //    DeleteDtls(1, PKStockTrnNo, StockGodownNo);
                        //    //    dgBill.Rows.RemoveAt(dgBill.CurrentCell.RowIndex);
                        //    //    dgBill.CurrentCell = dgBill[ColIndex.ItemName, dgBill.Rows.Count - 1];
                        //    //}

                        //}
                        if (dgBill.Rows.Count - 1 == dgBill.CurrentCell.RowIndex)
                        {
                            dgBill.Rows.RemoveAt(dgBill.CurrentCell.RowIndex);
                            dgBill.Rows.Add();
                        }
                        else
                            dgBill.Rows.RemoveAt(dgBill.CurrentCell.RowIndex);
                        //CalculateTotal();
                        dgBill.CurrentCell = dgBill[ColIndex.ItemName, dgBill.Rows.Count - 1];
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgBill_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    e.SuppressKeyPress = true;
                    BtnSave.Focus();
                }
                if (e.KeyCode == Keys.Delete)
                {
                    delete_row();
                    if (dgBill.Rows.Count == 1 && (dgBill.Rows[0].Cells[ColIndex.ItemName].Value == null || dgBill.Rows[0].Cells[ColIndex.ItemName].Value.ToString() == ""))
                        cmbGodown.Enabled = true;
                    else if (dgBill.Rows.Count == 0)
                        cmbGodown.Focus();

                }
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    dgBill.Focus();
                    if (ObjFunction.GetComboValue(cmbGodown) > 0)
                    {
                        if (dgBill.CurrentRow.Index == dgBill.Rows.Count - 1)
                        {
                            if (dgBill.CurrentCell.ColumnIndex == ColIndex.ItemName)
                            {
                                e.SuppressKeyPress = true;

                                if (dgBill.CurrentRow.Cells[ColIndex.ItemName].Value == null || dgBill.CurrentRow.Cells[ColIndex.ItemName].Value.ToString() == "")
                                {
                                    dgBill.CurrentCell.Value = "";
                                    Desc_Start();
                                }
                                //else if (Convert.ToInt64(dgBill.CurrentRow.Cells[ColIndex.PkRateSettingNo].Value) == 0)
                                //{
                                //    dgBill.CurrentCell.Value = "";
                                //    Desc_Start();
                                //}
                            }
                        }
                        else if (dgBill.CurrentCell.ColumnIndex == ColIndex.ActualStock)
                        {
                            e.SuppressKeyPress = true;
                            if (dgBill.CurrentRow.Cells[ColIndex.ActualStock].Value == null || dgBill.CurrentRow.Cells[ColIndex.ActualStock].Value.ToString() == "")
                            {
                                dgBill.CurrentRow.Cells[ColIndex.ActualStock].Value = "0";
                                dgBill.CurrentRow.Cells[ColIndex.Quantity].Value = "0";
                            }
                            if (dgBill.CurrentRow.Index < dgBill.Rows.Count - 2)
                            {
                                MovetoNext move2n = new MovetoNext(m2n);
                                BeginInvoke(move2n, new object[] { dgBill.CurrentRow.Index + 1, 6 });
                            }
                            else if (dgBill.CurrentRow.Index == dgBill.Rows.Count - 2)
                            {
                                MovetoNext move2n = new MovetoNext(m2n);
                                BeginInvoke(move2n, new object[] { dgBill.CurrentRow.Index + 1, ColIndex.ItemName });
                            }
                        }
                    }
                    else
                    {
                        OMMessageBox.Show("Please Select Godown Name", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        cmbGodown.Focus();
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbGodown_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    if (ObjFunction.GetComboValue(cmbGodown) > 0)
                    {
                        dgBill.Focus();
                    }
                    else
                    {
                        OMMessageBox.Show("Please Select Location", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cmbGodown.Focus();
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbGodown_Leave(object sender, EventArgs e)
        {
            try
            {
                if (ObjFunction.GetComboValue(cmbGodown) > 0)
                {
                    SelectGodown();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void SelectGodown()
        {
            try
            {
                FillGrid();
                cmbGodown.Enabled = false;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                cmbGodown.Enabled = true;
                cmbGodown.SelectedValue = "0";
                while (dgBill.Rows.Count > 0)
                    dgBill.Rows.RemoveAt(0);
                pnlGroup1.Visible = false;
                pnlItemName.Visible = false;
                pnlRate.Visible = false;
                pnlUOM.Visible = false;
                pnlGroup2.Visible = false;
                cmbGodown.Focus();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void formatPics()
        {
            try
            {
                pnlItemName.Width = 610;
                pnlItemName.Height = 235;
                pnlItemName.Top = 130;
                pnlItemName.Left = 42;

                pnlGroup1.Top = 140;
                pnlGroup1.Left = 200;
                pnlGroup1.Width = 300;
                pnlGroup1.Height = 220;

                pnlGroup2.Top = 140;
                pnlGroup2.Left = 100;
                pnlGroup2.Width = 300;
                pnlGroup2.Height = 220;

                pnlUOM.Top = 140;
                pnlUOM.Left = 372;
                pnlUOM.Width = 120;
                pnlUOM.Height = 80;

                pnlRate.Top = 140;
                pnlRate.Left = 430;
                pnlRate.Width = 120;
                pnlRate.Height = 80;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void Desc_Start()
        {
            try
            {
                if (dgBill.Rows.Count > 1)
                {
                    cmbGodown.Enabled = false;
                }
                if (dgBill.CurrentCell.Value == null || Convert.ToString(dgBill.CurrentCell.Value) == "")
                {
                    ItemType = 1;
                    FillItemList(0, ItemType); //FillItemList();
                }
                else
                {
                    ItemType = 2;
                    long[] BarcodeNo; long[] ItemNo;
                    SearchBarcode(dgBill.CurrentCell.Value.ToString().Trim(), out ItemNo, out BarcodeNo);

                    if (ItemNo.Length == 0 || BarcodeNo.Length == 0)
                    {
                        dgBill.CurrentCell.Value = null;
                        DisplayMessage("Barcode Not Found");
                    }
                    else
                    {
                        if (ItemNo.Length > 1)
                        {
                            ItemType = 3;
                            FillItemList(0, ItemType);//FillItemList();
                        }
                        else
                        {
                            //dgBill.CurrentRow.Cells[ColIndex.Barcode].Value = dgBill.CurrentCell.Value;
                            //Desc_MoveNext(ItemNo[0], BarcodeNo[0]);

                            int rowIndex = dgBill.CurrentCell.RowIndex;
                            int tempRowindex = dgBill.CurrentCell.RowIndex;
                            dgBill.CurrentRow.Cells[ColIndex.Barcode].Value = dgBill.CurrentCell.Value;
                            //Desc_MoveNext(ItemNo[0], BarcodeNo[0]);
                            //UOM_Start();
                            ItemType = 3;
                            FillItemList(0, ItemType);
                            //if (dgBill.Rows.Count - 1 > rowIndex)
                            //{
                            //    if (dgBill.Rows[dgBill.Rows.Count - 1].Cells[ColIndex.ItemName].Value == null || dgBill.Rows[dgBill.Rows.Count - 1].Cells[ColIndex.ItemName].Value == "")
                            //        dgBill.Rows.RemoveAt(dgBill.Rows.Count - 1);

                            //}
                            //if (ItemExist(Convert.ToInt64(dgBill.Rows[rowIndex].Cells[ColIndex.ItemNo].Value), Convert.ToInt64(dgBill.Rows[rowIndex].Cells[ColIndex.PkRateSettingNo].Value), out rowIndex) == true)
                            //{
                            //    //if (tempRowindex != rowIndex)
                            //    //{
                            //        pnlItemName.Visible = false;
                            //        OMMessageBox.Show("Item Already Exist.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            //        dgBill.CurrentCell.Value = "";
                            //        dgBill.CurrentCell = dgBill[dgBill.CurrentCell.ColumnIndex, dgBill.CurrentRow.Index];
                            //        //dgBill.Rows[rowIndex].Cells[ColIndex.Quantity].Value = Convert.ToDouble(dgBill.Rows[rowIndex].Cells[ColIndex.Quantity].Value) + 1;
                            //        ////dgBill.Rows[rowIndex].Cells[ColIndex.ItemName].Value = "";
                            //        //for (int i = 1; i < dgBill.Columns.Count; i++)
                            //        //    dgBill.CurrentRow.Cells[i].Value = null;
                            //        //dgBill.CurrentCell = dgBill[ColIndex.Quantity, rowIndex];
                            //        //dgBill_CellValueChanged(dgBill, new DataGridViewCellEventArgs(ColIndex.Quantity, rowIndex));
                            //        //dgBill_KeyDown(dgBill, new KeyEventArgs(Keys.Enter));
                            //    //}


                            //}
                            //else
                            //{
                            //    rowIndex = dgBill.CurrentCell.RowIndex;
                            //    DataTable dt = ObjFunction.GetDataView(" Select TStock.FkUomNo,TStock.Rate,TStock.FkRateSettingNo,MRateSetting.ASaleRate as SaleRate,MRateSetting.PurRate,TStock.PkStockTrnNo,TStock.Quantity from MRateSetting INNER JOIN " +
                            //                                   "   TStock ON MRateSetting.PkSrNo = TStock.FkRateSettingNo  INNER JOIN  TStockGodown ON TStock.PkStockTrnNo = TStockGodown.FKStockTrnNo " +
                            //                                   " Where TStock.FkVoucherNo=" + ID + " and TStock.ItemNo=" + Convert.ToInt64(dgBill.Rows[rowIndex].Cells[ColIndex.ItemNo].Value) + " AND MRateSetting.PkSrNo=" + Convert.ToInt64(dgBill.Rows[rowIndex].Cells[ColIndex.PkRateSettingNo].Value) + "" +
                            //                                   " and TStockGodown.GodownNo=" + ObjFunction.GetComboValue(cmbGodown) + "").Table;
                            //    if (dt.Rows.Count > 0)
                            //    {
                            //        dgBill.Rows[rowIndex].Cells[ColIndex.UOMNo].Value = Convert.ToInt64(dt.Rows[0].ItemArray[0].ToString());
                            //        dgBill.Rows[rowIndex].Cells[ColIndex.Rate].Value = Convert.ToDouble(dt.Rows[0].ItemArray[1].ToString());//lstRate.Text;
                            //        dgBill.Rows[rowIndex].Cells[ColIndex.PkRateSettingNo].Value = Convert.ToInt64(dt.Rows[0].ItemArray[2].ToString());//lstRate.SelectedValue;
                            //        dgBill.Rows[rowIndex].Cells[ColIndex.Rate].Value = Convert.ToDouble(dt.Rows[0].ItemArray[3].ToString());
                            //        dgBill.Rows[rowIndex].Cells[ColIndex.PurRate].Value = Convert.ToDouble(dt.Rows[0].ItemArray[4].ToString());
                            //        dgBill.Rows[rowIndex].Cells[ColIndex.PkStockTrnNo].Value = Convert.ToInt64(dt.Rows[0].ItemArray[5].ToString());
                            //        dgBill.Rows[rowIndex].Cells[ColIndex.Quantity].Value = Convert.ToDouble(dt.Rows[0].ItemArray[6].ToString());
                            //        MovetoNext move2n = new MovetoNext(m2n);
                            //        BeginInvoke(move2n, new object[] { rowIndex, ColIndex.Quantity });
                            //        //Desc_MoveNext(Convert.ToInt64(dgBill.Rows[rowIndex].Cells[ColIndex.ItemNo].Value), Convert.ToInt64(dgBill.Rows[rowIndex].Cells[ColIndex.PkBarCodeNo].Value));
                            //    }
                            //}


                        }

                    }
                    //BindGrid();
                    //CalculateTotal();
                }

                //from key_down
                //ItemType = 1;
                //FillItemList();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillItemList(int qNo, int iType)
        {
            try
            {
                switch (iType)
                {
                    case 1:
                        FillItemList(qNo);
                        break;
                    case 2:
                        break;
                    case 3:
                        // ItemList = " SELECT MStockItems.ItemNo, MStockItems.ItemName, MRateSetting.ASaleRate AS SaleRate, MUOM.UOMName, MRateSetting.MRP, " +
                        //    " 0 AS Stock, '' AS stkUOM, 0 AS SaleTax, 0 AS PurTax, " + //MItemTaxInfo_Sale.Percentage AS SaleTax , MItemTaxInfo_Pur.Percentage
                        //    " MStockItems.CompanyNo, MStockBarcode.Barcode, MRateSetting.PkSrNo As RateSettingNo, MStockItems.UOMDefault " +
                        //    " FROM MStockItems_V(NULL,NULL,NULL,NULL,NULL,NULL,NULL) AS MStockItems INNER JOIN " +
                        //    " dbo.GetItemRateAll(NULL, NULL, NULL, NULL, NULL,NULL) AS MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo AND  " +
                        //    " MStockItems.UOMDefault = MRateSetting.UOMNo INNER JOIN " +
                        //    " MStockBarcode ON MRateSetting.FkBcdSrNo = MStockBarcode.PkStockBarcodeNo INNER JOIN " +
                        //    " MUOM ON MStockItems.UOMDefault = MUOM.UOMNo  Where MStockItems.ItemNo in " +
                        //    "(Select ItemNo from MStockBarcode where Barcode = '" + dgBill.CurrentCell.Value + "' And IsActive ='true') AND MStockItems.IsActive='true' " +
                        //    " ORDER BY MStockItems.ItemName";
                        DataTable dtBarCodeItemNo = ObjFunction.GetDataView("Select ItemNo from MStockBarcode where Barcode = '" + dgBill.CurrentCell.Value + "'").Table;
                        string ItemList = "";
                        for (int i = 0; i < dtBarCodeItemNo.Rows.Count; i++)
                        {
                            if (i != 0)
                            {
                                ItemList += " Union all ";
                            }
                            //ItemList = " SELECT MStockItems.ItemNo, MStockItems.ItemName, MRateSetting.ASaleRate AS SaleRate, MUOM.UOMName, MRateSetting.MRP, " +
                            //    " (SELECT  ISNULL(SUM(CASE WHEN trncode = 1 THEN quantity ELSE - quantity END), 0) +  ISNULL(SUM(CASE WHEN trncode = 1 THEN FreeQty ELSE - FreeQty END), 0)   FROM TStock  INNER JOIN TStockGodown ON TSTock.PKStockTrnNo=TStockGodown.FKStockTrnNo   WHERE  TStockGodown.GodownNo=" + ObjFunction.GetComboValue(cmbGodown) + " and (FkRateSettingNo IN  (SELECT PkSrNo  FROM MRateSetting AS MRateSetting_1  WHERE (TStock.ItemNo = MStockItems.ItemNo) AND (MRP = MRateSetting.MRP))) AND (TStock.ItemNo = MStockItems.ItemNo) And PkStockTrnNo Not In (SELECT TStock.PkStockTrnNo FROM TVoucherEntry INNER JOIN  TStock ON TVoucherEntry.PkVoucherNo = TStock.FKVoucherNo WHERE (TVoucherEntry.IsCancel = 'True') AND (TStock.ItemNo = MStockItems.ItemNo))) AS Stock, '' AS stkUOM, 0 AS SaleTax, 0 AS PurTax, " + //MItemTaxInfo_Sale.Percentage AS SaleTax , MItemTaxInfo_Pur.Percentage
                            //    " MStockItems.CompanyNo, MStockBarcode.Barcode, MRateSetting.PkSrNo As RateSettingNo, MStockItems.UOMDefault,MRateSetting.PurRate " +
                            //    " FROM MStockItems_V(NULL," + dtBarCodeItemNo.Rows[i].ItemArray[0].ToString() + ",NULL,NULL,NULL,NULL,NULL) AS MStockItems INNER JOIN " +
                            //    " dbo.GetItemRateAll(" + dtBarCodeItemNo.Rows[i].ItemArray[0].ToString() + ", NULL, NULL, NULL, '" + DBGetVal.ServerTime.Date.ToString("dd-MMM-yyyy") + " " + DBGetVal.ServerTime.ToLongTimeString() + "',NULL) AS MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo AND  " +
                            //    " MStockItems.UOMDefault = MRateSetting.UOMNo INNER JOIN " +
                            //    " MStockBarcode ON MRateSetting.FkBcdSrNo = MStockBarcode.PkStockBarcodeNo INNER JOIN " +
                            //    " MUOM ON MStockItems.UOMDefault = MUOM.UOMNo  Where MStockItems.IsActive='true' and MStockItems.FkStockGroupTypeNo<>3 ";
                            ////" MStockItems.ItemNo in (Select ItemNo from MStockBarcode where Barcode = '" + dgBill.CurrentCell.Value + "' And IsActive ='true') AND MStockItems.IsActive='true' ";

                            ItemList = " SELECT MStockItems.ItemNo, MStockItems.ItemName, MRateSetting.ASaleRate AS SaleRate, MUOM.UOMName, MRateSetting.MRP, " +
                                " IsNull(MSB.CurrentStock,0) AS Stock, '' AS stkUOM, 0 AS SaleTax, 0 AS PurTax, " + //MItemTaxInfo_Sale.Percentage AS SaleTax , MItemTaxInfo_Pur.Percentage
                                " MStockItems.CompanyNo, MStockBarcode.Barcode, MRateSetting.PkSrNo As RateSettingNo, MStockItems.UOMDefault,MRateSetting.PurRate " +
                                " FROM MStockItems_V(NULL," + dtBarCodeItemNo.Rows[i].ItemArray[0].ToString() + ",NULL,NULL,NULL,NULL,NULL) AS MStockItems INNER JOIN " +
                                " dbo.GetItemRateAll(" + dtBarCodeItemNo.Rows[i].ItemArray[0].ToString() + ", NULL, NULL, NULL, '" + DBGetVal.ServerTime.Date.ToString("dd-MMM-yyyy") + " " + DBGetVal.ServerTime.ToLongTimeString() + "',NULL) AS MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo AND  " +
                                " MStockItems.UOMDefault = MRateSetting.UOMNo INNER JOIN " +
                                " MStockBarcode ON MRateSetting.FkBcdSrNo = MStockBarcode.PkStockBarcodeNo INNER JOIN " +
                                " MUOM ON MStockItems.UOMDefault = MUOM.UOMNo LEFT OUTER JOIN MStockItemBalance MSB ON MSB.ItemNo = MStockItems.ItemNo AND MSB.MRP = MRateSetting.MRP AND MSB.GodownNo = " + ObjFunction.GetComboValue(cmbGodown) + 
                                " Where MStockItems.IsActive='true' and MStockItems.FkStockGroupTypeNo<>3 ";
                            //" MStockItems.ItemNo in (Select ItemNo from MStockBarcode where Barcode = '" + dgBill.CurrentCell.Value + "' And IsActive ='true') AND MStockItems.IsActive='true' ";

                        }
                        DataTable dtItemList = ObjFunction.GetDataView(ItemList).Table;
                        if (dtItemList.Rows.Count > 0)
                        {
                            dgItemList.DataSource = dtItemList.DefaultView;
                            pnlItemName.Visible = true;
                            dgItemList.CurrentCell = dgItemList[2, 0];
                            dgItemList.Focus();
                        }
                        else
                        {
                            DisplayMessage("Items Not Found......");
                        }
                        break;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillItemList(int qNo)
        {
            try
            {
                if (qNo == 0)
                {
                    qNo = iItemNameStartIndex;
                }

                string ItemList = strItemQuery[qNo - 1];

                ItemList = ItemList.Replace("@cmbRateType@", "ASaleRate");// + ObjFunction.GetComboValueString(cmbRateType));
                ItemList = ItemList.Replace("ORDER BY ItemName", " and MStockItems.FkStockGroupTypeNo<>3 ORDER BY ItemName");


                switch (qNo)
                {
                    case 1:
                        break;
                    case 2:
                        switch (strItemQuery.Length)
                        {
                            case 2:
                                ItemList = ItemList.Replace("@Param1@", "" + (Convert.ToInt64(lstGroup1.SelectedValue) != 0 ? lstGroup1.SelectedValue.ToString() : Param1Value));
                                ItemList = ItemList.Replace("@Param1NULL@", "" + (Convert.ToInt64(lstGroup1.SelectedValue) != 0 ? lstGroup1.SelectedValue.ToString() : "NULL"));
                                break;
                            case 3:
                                ItemList = ItemList.Replace("@Param2@", "" + (Convert.ToInt64(lstGroup2.SelectedValue) != 0 ? lstGroup2.SelectedValue.ToString() : Param2Value));
                                ItemList = ItemList.Replace("@Param2NULL@", "" + (Convert.ToInt64(lstGroup2.SelectedValue) != 0 ? lstGroup2.SelectedValue.ToString() : "NULL"));
                                break;
                        }
                        break;
                    case 3:
                        ItemList = ItemList.Replace("@Param1@", "" + (Convert.ToInt64(lstGroup1.SelectedValue) != 0 ? lstGroup1.SelectedValue.ToString() : Param1Value));
                        ItemList = ItemList.Replace("@Param2@", "" + (Convert.ToInt64(lstGroup2.SelectedValue) != 0 ? lstGroup2.SelectedValue.ToString() : Param2Value));
                        ItemList = ItemList.Replace("@Param1NULL@", "" + (Convert.ToInt64(lstGroup1.SelectedValue) != 0 ? lstGroup1.SelectedValue.ToString() : "NULL"));
                        ItemList = ItemList.Replace("@Param2NULL@", "" + (Convert.ToInt64(lstGroup2.SelectedValue) != 0 ? lstGroup2.SelectedValue.ToString() : "NULL"));
                        break;
                }
                ItemList = ItemList.Replace("@CompNo@", "MStockItems.CompanyNo");
                if (ObjFunction.GetComboValue(cmbGodown) != 0)
                    ItemList = ItemList.Replace("@GodownNo@", "" + ObjFunction.GetComboValue(cmbGodown) + "");
                    //ItemList = ItemList.Replace("MSB.GodownNo = 2", "MSB.GodownNo = " + ObjFunction.GetComboValue(cmbGodown));

                switch (strItemQuery.Length - qNo)
                {
                    case 0:
                        if (!ItemList.Equals(strItemQuery_last[qNo - 1], StringComparison.CurrentCultureIgnoreCase))
                        {
                            DataTable dtItemList = ObjFunction.GetDataView(ItemList).Table;
                            if (dtItemList.Rows.Count > 0)
                            {
                                dgItemList.DataSource = dtItemList.DefaultView;
                                pnlItemName.Visible = true;
                                dgItemList.CurrentCell = dgItemList[1, 0];
                                dgItemList.Focus();
                            }
                            else
                            {
                                DisplayMessage("Items Not Found......");
                            }
                        }
                        else
                        {
                            pnlItemName.Visible = true;
                            dgItemList.CurrentCell = dgItemList[1, 0];
                            dgItemList.Focus();
                        }
                        break;
                    case 1:
                        if (!ItemList.Equals(strItemQuery_last[qNo - 1], StringComparison.CurrentCultureIgnoreCase))
                        {
                            ObjFunction.FillList(lstGroup1, ItemList);
                            strItemQuery_last[qNo - 1] = ItemList;
                        }
                        pnlGroup1.Visible = true;
                        lstGroup1.Focus();
                        break;
                    case 2:
                        if (!ItemList.Equals(strItemQuery_last[qNo - 1], StringComparison.CurrentCultureIgnoreCase))
                        {
                            ObjFunction.FillList(lstGroup2, ItemList);
                            strItemQuery_last[qNo - 1] = ItemList;
                        }
                        pnlGroup2.Visible = true;
                        lstGroup2.Focus();
                        break;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void SearchBarcode(String strBarcode, out long[] ItemNo, out long[] BarcodeNo)
        {
            //string sql = "Select PkStockBarcodeNo, ItemNo from MStockBarcode where Barcode = '" + strBarcode + "' And IsActive ='true'";
            string sql = "SELECT     MStockBarcode.PkStockBarcodeNo, MStockBarcode.ItemNo,MStockBarcode.Barcode FROM MStockBarcode INNER JOIN MStockItems ON MStockBarcode.ItemNo = MStockItems.ItemNo " +
                " INNER JOIN MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo " +
                " WHERE ((MStockBarcode.Barcode = '" + strBarcode + "') or (MStockItems.ShortCode='" + strBarcode + "')) AND (MStockItems.IsActive = 'true') and (MStockItems.FkStockGroupTypeNo<>3) AND (MRateSetting.IsActive='true')";
            DataTable dt = ObjFunction.GetDataView(sql).Table;
            BarcodeNo = new long[dt.Rows.Count];
            ItemNo = new long[dt.Rows.Count];
            try
            {
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        BarcodeNo[i] = Convert.ToInt64(dt.Rows[i].ItemArray[0].ToString());
                        ItemNo[i] = Convert.ToInt64(dt.Rows[i].ItemArray[1].ToString());
                        dgBill.CurrentCell.Value = dt.Rows[i].ItemArray[2].ToString();
                    }
                }
                else
                {
                    //ItemNo[0] = 0;
                    //BarcodeNo[0] = 0;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void DisplayMessage(string str)
        {
            try
            {
                lblMsg.Visible = true;
                lblMsg.Text = str;
                Application.DoEvents();
                System.Threading.Thread.Sleep(700);
                lblMsg.Visible = false;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void Desc_MoveNext(long ItemNo, long BarcodeNo)
        {
            try
            {
                dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemNo].Value = ItemNo;
                dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkBarCodeNo].Value = BarcodeNo;

                DataTable dtItem = ObjFunction.GetDataView("Select ItemName,CompanyNo from MStockItems_V(NULL,NULL,NULL,NULL,NULL,NULL,NULL) where ItemNo = " + ItemNo + " AND IsActive='true' ").Table;
                dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemName].Value = dtItem.Rows[0].ItemArray[0].ToString();
                dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.StockCompanyNo].Value = dtItem.Rows[0].ItemArray[1].ToString();
                //dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemName].Value = ObjQry.ReturnString("Select ItemName from MStockItems_V(NULL,NULL) where ItemNo = " + ItemNo,CommonFunctions.ConStr);

                if (ItemType == 2)
                    dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemName].Value += " - " + dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.Barcode].Value.ToString();


                //if (StopOnQty == true)
                //{
                //    if (dgBill[2, dgBill.CurrentCell.RowIndex].Value == null || dgBill[2, dgBill.CurrentCell.RowIndex].EditedFormattedValue.ToString().Trim() == "")
                //    {
                //        dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.Quantity].Value = "1";
                //        dgBill.CurrentCell = dgBill[2, dgBill.CurrentCell.RowIndex];
                //        dgBill.Focus();
                //    }
                //    else
                //        Qty_MoveNext();
                //}
                //else
                //{
                //    dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.Quantity].Value = "1";
                //    Qty_MoveNext();
                //}
                //BindGrid();
                //CalculateTotal();
                ActualStock_MoveNext();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void ActualStock_MoveNext()
        {
            try
            {
                rowQtyIndex = dgBill.CurrentCell.RowIndex;
                dgBill.Rows[dgBill.CurrentRow.Index].Cells[ColIndex.ItemName].ReadOnly = true;
                dgBill.Rows[dgBill.CurrentRow.Index].Cells[ColIndex.ActualStock].ReadOnly = false;
                MovetoNext move2n = new MovetoNext(m2n);
                BeginInvoke(move2n, new object[] { rowQtyIndex, ColIndex.ActualStock });
                dgBill.Rows.Add();
                dgBill.Rows[dgBill.Rows.Count - 1].Cells[ColIndex.ItemName].ReadOnly = false;
                dgBill.Rows[dgBill.Rows.Count - 1].Cells[ColIndex.ActualStock].ReadOnly = true;
                //UOM_Start();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private delegate void MovetoNext(int RowIndex, int ColIndex);

        private void m2n(int RowIndex, int ColIndex)
        {
            try
            {
                dgBill.CurrentCell = dgBill.Rows[RowIndex].Cells[ColIndex];
            }
            catch (Exception)
            {
            }
        }

        private void lstGroup1_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F9)
                {
                    pnlGroup1.Visible = false;
                    FillItemList(1);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void lstGroup1_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                //string ItemListStr = "";
                if (e.KeyChar == 13)
                {
                    pnlGroup1.Visible = false;

                    FillItemList(strItemQuery.Length);
                }
                else if (e.KeyChar == ' ')
                {
                    dgBill.Focus();
                    pnlGroup1.Visible = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgItemList_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    //if (Convert.ToDouble(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[5].Value) > 0)
                    //{
                        long i = Convert.ToInt64(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[0].Value);
                        int rwindex = 0;
                        if (ItemExist(i, Convert.ToInt64(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[11].Value), out rwindex) == true)
                        {
                            pnlItemName.Visible = false;
                            OMMessageBox.Show("Item Already Exist.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            dgBill.CurrentCell.Value = "";
                            dgBill.CurrentCell = dgBill[dgBill.CurrentCell.ColumnIndex, dgBill.CurrentRow.Index];

                            //if (Convert.ToDouble(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[5].Value) > ((dgBill.Rows[rwindex].Cells[ColIndex.Quantity].EditedFormattedValue == "") ? 0 : Convert.ToDouble(dgBill.Rows[rwindex].Cells[ColIndex.Quantity].EditedFormattedValue)))
                            //{
                            //    pnlItemName.Visible = false;
                            //    dgBill.Rows[rwindex].Cells[ColIndex.Quantity].Value = Convert.ToDouble(dgBill.Rows[rwindex].Cells[ColIndex.Quantity].Value) + 1;
                            //    //dgBill.Rows[rwindex].Cells[ColIndex.TempQty].Value = Convert.ToDouble(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[5].Value).ToString("0.00");
                            //    if (rwindex != dgBill.CurrentRow.Index)
                            //    {
                            //        for (int j = 0; j < dgBill.Columns.Count; j++)
                            //            dgBill.CurrentRow.Cells[j].Value = "";
                            //    }
                            //    dgBill.CurrentCell = dgBill[ColIndex.Quantity, rwindex];
                            //    dgBill_CellValueChanged(dgBill, new DataGridViewCellEventArgs(ColIndex.Quantity, rwindex));
                            //    dgBill_KeyDown(dgBill, new KeyEventArgs(Keys.Enter));
                            //}
                            //else
                            //{
                            //    OMMessageBox.Show("Stock For This Item Is insufficient", CommonFunctions.ErrorTitle, OMMessageBoxButton.Escape, OMMessageBoxIcon.Error);
                            //}
                        }
                        else
                        {
                            dgBill.CurrentRow.Cells[ColIndex.UOMNo].Value = Convert.ToInt64(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[12].Value);
                            dgBill.CurrentRow.Cells[ColIndex.Rate].Value = Convert.ToDouble(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[2].Value).ToString("0.00");//lstRate.Text;
                            dgBill.CurrentRow.Cells[ColIndex.PkRateSettingNo].Value = Convert.ToInt64(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[11].Value);//lstRate.SelectedValue;
                            dgBill.CurrentRow.Cells[ColIndex.MRP].Value = Convert.ToDouble(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[4].Value).ToString("0.00");
                            //dgBill.CurrentRow.Cells[ColIndex.TempMRP].Value = Convert.ToDouble(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[4].Value).ToString("0.00");
                            //dgBill.CurrentRow.Cells[ColIndex.]
                            dgBill.CurrentRow.Cells[ColIndex.StockFactor].Value = ObjQry.ReturnDouble("Select StockConversion FRom MRateSetting Where PkSrNo=" + dgBill.CurrentRow.Cells[ColIndex.PkRateSettingNo].Value + "", CommonFunctions.ConStr).ToString();
                            dgBill.CurrentRow.Cells[ColIndex.PurRate].Value = Convert.ToDouble(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[13].Value).ToString("0.00");
                            dgBill.CurrentRow.Cells[ColIndex.UOM].Value = dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[3].Value;
                            dgBill.CurrentRow.Cells[ColIndex.PkStockTrnNo].Value = ((dgBill.CurrentRow.Cells[ColIndex.PkStockTrnNo].EditedFormattedValue.ToString().Trim() == "") ? "0" : dgBill.CurrentRow.Cells[ColIndex.PkStockTrnNo].EditedFormattedValue.ToString().Trim());
                            dgBill.CurrentRow.Cells[ColIndex.FkStockGodownNo].Value = ((dgBill.CurrentRow.Cells[ColIndex.FkStockGodownNo].EditedFormattedValue.ToString().Trim() == "") ? "0" : dgBill.CurrentRow.Cells[ColIndex.FkStockGodownNo].EditedFormattedValue.ToString().Trim());
                            dgBill.CurrentRow.Cells[ColIndex.Stock].Value = Convert.ToInt64(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[5].Value).ToString("0.00");
                            dgBill.CurrentRow.Cells[ColIndex.StockNow].Value = Convert.ToInt64(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[5].Value).ToString("0.00");
                            dgBill.CurrentRow.Cells[ColIndex.ActualStock].Value = "0";
                            dgBill.CurrentRow.Cells[ColIndex.Quantity].Value = "0";
                            dgBill.CurrentRow.Cells[ColIndex.NetRate].Value = "0";
                            dgBill.CurrentRow.Cells[ColIndex.DiscPercentage].Value = "0";
                            dgBill.CurrentRow.Cells[ColIndex.DiscAmount].Value = "0";
                            dgBill.CurrentRow.Cells[ColIndex.DiscRupees].Value = "0";
                            dgBill.CurrentRow.Cells[ColIndex.DiscPercentage2].Value = "0";
                            dgBill.CurrentRow.Cells[ColIndex.DiscAmount2].Value = "0";
                            dgBill.CurrentRow.Cells[ColIndex.DiscRupees2].Value = "0";
                            dgBill.CurrentRow.Cells[ColIndex.NetAmt].Value = "0";
                            dgBill.CurrentRow.Cells[ColIndex.Amount].Value = "0";
                            dgBill.CurrentRow.Cells[ColIndex.Barcode].Value = dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[10].Value;
                            dgBill.CurrentRow.Cells[ColIndex.PkStockTrnNo].Value = "0";
                            dgBill.CurrentRow.Cells[ColIndex.PkBarCodeNo].Value = ObjQry.ReturnLong("select PkStockBarcodeNo From MStockBarcode Where Barcode='" + dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[10].Value + "'", CommonFunctions.ConStr);
                            dgBill.CurrentRow.Cells[ColIndex.PkVoucherNo].Value = "0";
                            dgBill.CurrentRow.Cells[ColIndex.ItemNo].Value = Convert.ToInt64(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[0].Value);
                            dgBill.CurrentRow.Cells[ColIndex.PkItemTaxInfo].Value = "0";
                            //dgBill.CurrentRow.Cells[ColIndex.StockFactor].Value = "0";
                            dgBill.CurrentRow.Cells[ColIndex.ActualQty].Value = "0";
                            dgBill.CurrentRow.Cells[ColIndex.MKTQuantity].Value = "0";
                            dgBill.CurrentRow.Cells[ColIndex.TaxPercentage].Value = "0";
                            dgBill.CurrentRow.Cells[ColIndex.TaxAmount].Value = "0";
                            //dgBill.CurrentRow.Cells[ColIndex.TempQty].Value = Convert.ToDouble(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[5].Value).ToString("0.00");
                            pnlItemName.Visible = false;
                            //dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemNo].Value = ItemNo;
                            //dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkBarCodeNo].Value = dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[10].Value;

                            DataTable dtItem = ObjFunction.GetDataView("Select ItemName,CompanyNo from MStockItems_V(NULL,NULL,NULL,NULL,NULL,NULL,NULL) where ItemNo = " + Convert.ToInt64(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[0].Value) + " AND IsActive='true' ").Table;
                            dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemName].Value = dtItem.Rows[0].ItemArray[0].ToString();
                            dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.StockCompanyNo].Value = dtItem.Rows[0].ItemArray[1].ToString();
                            //Desc_MoveNext(i, 0);
                            ActualStock_MoveNext();
                        }
                        if (cmbGodown.Enabled == true && dgBill.Rows.Count > 0 && dgBill.Rows[0].Cells[ColIndex.ItemName].Value != null && dgBill.Rows[0].Cells[ColIndex.ItemName].Value.ToString() != "")
                        {
                            cmbGodown.Enabled = false;
                        }
                    
                }
                else if (e.KeyCode == Keys.Space)
                {
                    pnlItemName.Visible = false;
                    if (strItemQuery.Length > 1)
                    {
                        pnlGroup1.Visible = true;
                        lstGroup1.Focus();
                    }
                    else
                    {
                        dgBill.Focus();
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private bool ItemExist(long ItNo, out int rowIndex)
        {
            rowIndex = -1;
            try
            {
                bool flag = false;
                for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                {
                    if (ItNo == Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.ItemNo].Value))
                    {
                        rowIndex = i;
                        flag = true;
                        break;
                    }
                }
                return flag;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return false;
            }
        }
        private bool ItemExist(long ItNo, long FKRateSettingNo, out int rowIndex)
        {
            rowIndex = -1;
            try
            {
                bool flag = false;
                for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                {
                    if (ItNo == Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.ItemNo].Value) && FKRateSettingNo == Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.PkRateSettingNo].Value))
                    {
                        rowIndex = i;
                        flag = true;
                        break;
                    }
                }
                return flag;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return false;
            }
        }

        private void dgBill_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int row, col;
                if (dgBill.CurrentCell != null)
                { col = dgBill.CurrentCell.ColumnIndex; row = dgBill.CurrentCell.RowIndex; }
                else { col = e.ColumnIndex; row = e.RowIndex; }
                if (dgBill.Rows.Count > 0)
                    dgBill.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";
                if (col == ColIndex.Quantity && row >= 0)
                {
                    //if (dgBill.Rows.Count == dgBill.CurrentCell.RowIndex + 1) AddRows = true;
                    //    if (flagParking == true) return;
                    dgBill.CurrentCell.ErrorText = "";
                    if (dgBill.CurrentCell.Value != null)
                    {
                        if (dgBill.CurrentCell.Value.ToString() != "" && dgBill.CurrentCell.Value.ToString() != "0")
                        {
                            if (ObjFunction.CheckNumeric(dgBill.CurrentCell.Value.ToString()) == true)
                            {
                                int rowIndex = dgBill.CurrentCell.RowIndex;
                                if (dgBill[ColIndex.Rate, dgBill.CurrentCell.RowIndex].Value == null || Convert.ToString(dgBill[ColIndex.Rate, dgBill.CurrentCell.RowIndex].Value) == "")
                                    dgBill[ColIndex.Amount, dgBill.CurrentCell.RowIndex].Value = "0.00";
                                //else
                                //    CalculateTotal();
                                dgBill.Focus();

                                dgBill.CurrentCell = dgBill[2, row];


                            }

                        }
                    }
                }
                else if (col == ColIndex.Rate && row >= 0)
                {

                    //    if (flagParking == true) return;
                    dgBill.CurrentCell.ErrorText = "";
                    if (dgBill.CurrentCell.Value != null)
                    {
                        if (dgBill.CurrentCell.Value.ToString() != "" && dgBill.CurrentCell.Value.ToString() != "0")
                        {
                            if (ObjFunction.CheckNumeric(dgBill.CurrentCell.Value.ToString()) == true)
                            {

                                //dgBill[5, dgBill.CurrentCell.RowIndex].Value = Convert.ToDouble(dgBill[4, dgBill.CurrentCell.RowIndex].Value) * Convert.ToDouble(dgBill[2, dgBill.CurrentCell.RowIndex].Value);
                                dgBill.CurrentCell = dgBill[ColIndex.Rate, dgBill.CurrentCell.RowIndex];
                                dgBill.Rows[dgBill.CurrentCell.RowIndex].Selected = true;
                            }
                        }
                    }
                }

            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void lst_VisibleChanged(object sender, EventArgs e)
        {
            try
            {
                if (((System.Windows.Forms.Control)sender).Visible == true)
                    dgBill.Enabled = false;
                else
                {
                    dgBill.Enabled = true;
                    dgBill.Focus();
                }
                if (((System.Windows.Forms.Control)sender).Name == "pnlItemName")
                {
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void pnlGroup2_VisibleChanged(object sender, EventArgs e)
        {

        }

        private void pnlUOM_VisibleChanged(object sender, EventArgs e)
        {

        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (ObjFunction.GetComboValue(cmbGodown) > 0)
                {
                    string[] ReportSession;
                    ReportSession = new string[5];

                    ReportSession[0] = DBGetVal.CompanyNo.ToString();
                    ReportSession[1] = Convert.ToDateTime(dtpBillDate.Text).ToString("dd-MMM-yyyy");
                    ReportSession[2] = ID.ToString();
                    ReportSession[3] = ObjFunction.GetComboValue(cmbGodown).ToString();
                    ReportSession[4] = cmbGodown.Text;
                    Form NewF = null;
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                        NewF = new Display.ReportViewSource(new Reports.RptPhysicalStock(), ReportSession);
                    else
                        NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("RptPhysicalStock.rpt", CommonFunctions.ReportPath), ReportSession);
                    ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                }
                else
                {
                    OMMessageBox.Show("Please Select Godown Name", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    cmbGodown.Focus();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }
    }
}
