﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using JitControls;
using OM;
using System.IO.Ports;

namespace Kirana.Vouchers
{
    public partial class QtyReadDialog : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        private delegate void AddDataDelegate(string myString);
        private AddDataDelegate myDelegate;
        private string DispString = "";

        private string Sign;

        public static string ScanQty;

        public QtyReadDialog()
        {
            InitializeComponent();
        }

        private void QtyReadDialog_Shown(object sender, EventArgs e)
        {
            try
            {
                TxtQty.Font = ObjFunction.GetFont(FontStyle.Bold, 26);
                this.myDelegate = new AddDataDelegate(AddDataMethod);
                serialPort1.PortName = "Com9";
                serialPort1.BaudRate = 2400;
                serialPort1.Parity = Parity.None;
                serialPort1.StopBits = StopBits.One;
                serialPort1.Handshake = Handshake.None;
                serialPort1.DataBits = 8;


                serialPort1.ReadTimeout = 200;
                if (serialPort1.IsOpen)
                {
                    DispString = "";
                    TxtQty.Text = "";
                }
                else
                {
                    serialPort1.Open();
                }
                serialPort1.DataReceived += new SerialDataReceivedEventHandler(serialPort1_DataReceived);
            }
            catch (Exception ex)
            {
                ObjFunction.ExceptionDisplay(ex.Message);
               // string Str = "Please make sure Weighing Scales connected properly and switched on.\n\rOR\n\rCheck Weighing Scales Settings.\n\rError Description :";
               // OMMessageBox.Show(Str + ex.Message, CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information, 150);
            }
        }

        private void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            string TempString = serialPort1.ReadExisting();
            DispString += TempString;

            TxtQty.Invoke(this.myDelegate, new Object[] { DispString });
        }

        private void AddDataMethod(string myString)
        {
            try
            {
                if (myString.Length >= 15 && myString.EndsWith("\r\n"))
                {
                    Sign = myString[0].ToString();

                    if (Sign == "-")
                        TxtQty.Text = Sign + Convert.ToDouble(myString.Substring(3, 7).Trim()).ToString("0.00");
                    else
                        TxtQty.Text = Convert.ToDouble(myString.Substring(3, 7).Trim()).ToString("0.00");

                }
            }
            catch (Exception ex)
            {
                ObjFunction.ExceptionDisplay(ex.Message);
                OMMessageBox.Show(ex.Message, CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
            }
        }

        private Parity GetParity(string StrParity)
        {
            if (StrParity == "None")
                return Parity.None;
            else if (StrParity == "Even")
                return Parity.Even;
            else if (StrParity == "Mark")
                return Parity.Mark;
            else if (StrParity == "Odd")
                return Parity.Odd;
            else if (StrParity == "Space")
                return Parity.Space;
            else
                return Parity.None;
        }

        private StopBits GetStopBits(double strStopBits)
        {
            if (1 == strStopBits)
                return StopBits.One;
            else if (2 == strStopBits)
                return StopBits.Two;
            if (1.5 == strStopBits)
                return StopBits.OnePointFive;
            else
                return StopBits.None;
        }

        private void BtnScan_Click(object sender, EventArgs e)
        {
            EP.SetError(TxtQty, "");
            try
            {
                if (serialPort1.IsOpen == false)
                    serialPort1.Open();

                serialPort1.Write("W");
                DispString = "";
            }
            catch (Exception ex)
            {
                ObjFunction.ExceptionDisplay(ex.Message);
              //  string Str = "Please make sure Weighing Scales connected properly and switched on.\n\rOR\n\rCheck Weighing Scales Settings.\n\rError Description :";
               // OMMessageBox.Show(Str + ex.Message, CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information, 150);
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            EP.SetError(TxtQty, "");
         
            if (TxtQty.Text.Trim() != "")
            {
                if (Sign != "-")
                {
                    if (serialPort1.IsOpen == true)
                        serialPort1.Close();
         
                    ScanQty = TxtQty.Text.Trim();
                    this.Close();
                }
                else
                {
                    EP.SetError(TxtQty, "InValid Weighing Scales Input.");
                    EP.SetIconAlignment(TxtQty, ErrorIconAlignment.MiddleRight);
                    TxtQty.Focus();
                }
            }
            else
            {
                EP.SetError(TxtQty, "Please Read Weight.");
                EP.SetIconAlignment(TxtQty, ErrorIconAlignment.MiddleRight);
                TxtQty.Focus();
            }

        }

        private void QtyReadDialog_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                BtnScan_Click(sender, new EventArgs());
            }
            else if (e.KeyCode == Keys.F3)
            {
                BtnOk_Click(sender, new EventArgs());
            }
            else if (e.KeyCode == Keys.Escape)
            {
                BtnCancel_Click(sender, new EventArgs());
            }
            else if (e.KeyCode == Keys.F5)
            {
                btnManual_Click(sender, new EventArgs());
            }
        }

        private void TxtQty_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked((TextBox)sender, 3, 6, JitFunctions.MaskedType.PositiveNegative);
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen == true)
                serialPort1.Close();
          
            ScanQty = "Cancel";
            this.Close();
        }

        private void btnManual_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen == true)
                serialPort1.Close();
            ScanQty = "Manual";
            this.Close();
        }

    }
}
