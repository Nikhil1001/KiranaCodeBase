﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using JitControls;
using Newtonsoft.Json;
using OM;


namespace Kirana.Vouchers
{
    public partial class ShowLiveBillDetails : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();


        List<DTOClasses.UCSaleBills> saleBillList;
        List<DTOClasses.UCBillCollection> saleBill_Coll_List;
        double TotalNetBal = 0;

        public long VoucherType;
        public ShowLiveBillDetails()
        {
            InitializeComponent();
        }
        public ShowLiveBillDetails(long vchType)
        {
            InitializeComponent();
            VoucherType = vchType;
        }

        private void ShowLiveBillDetails_Load(object sender, EventArgs e)
        {
            DTPFromDate.Value = DBGetVal.ServerTime;
            dtpVoucherDate.Value = DBGetVal.ServerTime;
            ObjFunction.FillCombo(cmbLedgerName, "Select LedgerNo,LedgerName From MLedger Where GroupNo =" + GroupType.SundryDebtors + " order by LedgerName");
            ObjFunction.FillCombo(cmbPartyName, "Select LedgerNo,LedgerName From MLedger Where GroupNo =" + GroupType.SundryDebtors + " order by LedgerName");
            KeyDownFormat(this.Controls);


            lblC.Font = new Font("Verdana", 11, FontStyle.Bold);
            lbld.Font = new Font("Verdana", 11, FontStyle.Bold);
            lblDrBal.Font = new Font("Verdana", 11, FontStyle.Bold);
            lblNetB.Font = new Font("Verdana", 11, FontStyle.Bold);
            lblCrNet.Font = new Font("Verdana", 11, FontStyle.Bold);
            lblN.Font = new Font("Verdana", 11, FontStyle.Bold);

        }

        #region KeyDown Events
        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                chkSelectAll.Checked = !chkSelectAll.Checked;

                for (int i = 0; i < dgDetailes.Rows.Count; i++)
                {
                    dgDetailes.Rows[i].Cells[6].Value = chkSelectAll.Checked;
                }
                btnDelete.Focus();
            }


        }

        public void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else
                    KeyDownFormat(ctrl.Controls);
            }
        }
        #endregion

        private void btnShow_Click(object sender, EventArgs e)
        {
            DTOClasses.UCTVoucherEntry tvs = new DTOClasses.UCTVoucherEntry();
            tvs.voucherdate = Convert.ToDateTime(DTPFromDate.Text);
            if (txtInvNo.Text.Trim() != "")
            {
                tvs.salebillno = txtInvNo.Text;
            }
            if (ObjFunction.GetComboValue(cmbLedgerName) != 0)
            {
                tvs.ledgerno = ObjFunction.GetComboValue(cmbLedgerName);
            }
            string strJson = JsonConvert.SerializeObject(tvs, Newtonsoft.Json.Formatting.None,
                           new JsonSerializerSettings
                           {
                               NullValueHandling = NullValueHandling.Ignore
                           });

            Display.WorkInProgress wip = new Kirana.Display.WorkInProgress();
            wip.StrStatus = "Search Bill in process, please wait.";
            wip.argument = strJson;
            wip.bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
            wip.bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
            ObjFunction.OpenForm(wip);
        }

        private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (e.Argument.ToString() != "")
            {
                try
                {
                    string result = ObjFunction.getResponseJSON("/GetBill", "POST", string.Format("StrBillDetails={0}", e.Argument.ToString()));
                    saleBillList = JsonConvert.DeserializeObject<List<DTOClasses.UCSaleBills>>(result);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
        }

        private void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                OMMessageBox.Show(e.Error.Message);
            }
            else
            {
                UpdateGridView();
            }
        }

        private void UpdateGridView()
        {
            while (dgDetailes.Rows.Count > 0)
                dgDetailes.Rows.RemoveAt(0);

            for (int i = 0; i < saleBillList.Count; i++)
            {
                dgDetailes.Rows.Add();
                DTOClasses.UCTVoucherEntry items = saleBillList[i].tvoucherentry;
                dgDetailes[0, i].Value = i + 1;
                dgDetailes[1, i].Value = items.salebillno;
                dgDetailes[2, i].Value = Convert.ToDateTime(items.voucherdate).ToString("dd-MMM-yyyy");
                dgDetailes[3, i].Value = saleBillList[i].mledger.ledgername;
                dgDetailes[4, i].Value = items.paymenttype;
                dgDetailes[5, i].Value = items.billamount;
                dgDetailes[6, i].Value = "Print";
                dgDetailes[7, i].Value = 0;
                dgDetailes[8, i].Value = i;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {


        }

        private void dgDetailes_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                e.Value = e.RowIndex + 1;
            }
        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < dgDetailes.Rows.Count; i++)
            {
                dgDetailes.Rows[i].Cells[6].Value = chkSelectAll.Checked;
            }
        }

        private void dgDetailes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            if (senderGrid.Columns[6] is DataGridViewButtonColumn &&
    e.RowIndex >= 0)
            {
                if (e.ColumnIndex == 6)
                {

                    string[] ReportSession;
                    Form NewF = null;

                    ReportSession = new string[7];
                    ReportSession[0] = "";
                    ReportSession[1] = saleBillList[e.RowIndex].tvoucherentry.paymenttype;
                    ReportSession[2] = saleBillList[e.RowIndex].tvoucherentry.billdiscamount.ToString("0.00");
                    ReportSession[3] = saleBillList[e.RowIndex].tvoucherentry.chargamount.ToString("0.00");
                    ReportSession[4] = saleBillList[e.RowIndex].tvoucherentry.salebillno.ToString();
                    ReportSession[5] = saleBillList[e.RowIndex].tvoucherentry.voucherdate.ToString(Format.DDMMMYYYY);
                    ReportSession[6] = saleBillList[e.RowIndex].mledger.ledgername;


                    Reports.GetBillMRP_Live GC = new Reports.GetBillMRP_Live();
                    GC.SetDataSource(saleBillList[e.RowIndex].tstock);
                    //GC. = "GetBillMRP";
                    NewF = new Display.ReportViewSource(GC, ReportSession, true);
                    ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                }


            }


        }

        private void btnShowCollection_Click(object sender, EventArgs e)
        {
            CallCollectionDetails();
        }

        public void CallCollectionDetails()
        {
            Display.WorkInProgress wip = new Kirana.Display.WorkInProgress();
            wip.StrStatus = "Search Bill Collection in process, please wait.";
            wip.argument = ObjFunction.GetComboValue(cmbPartyName).ToString();
            wip.bgWorker.DoWork += new DoWorkEventHandler(bgWorker_Bill_Coll_DoWork);
            wip.bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_Bill_Coll_RunWorkerCompleted);
            ObjFunction.OpenForm(wip);
        }

        private void bgWorker_Bill_Coll_DoWork(object sender, DoWorkEventArgs e)
        {
            if (e.Argument.ToString() != "")
            {
                try
                {
                    string result = ObjFunction.getResponseJSON("/GetBillCollection", "POST", string.Format("LedgerNo={0}", e.Argument.ToString()));
                    saleBill_Coll_List = JsonConvert.DeserializeObject<List<DTOClasses.UCBillCollection>>(result);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
        }

        private void bgWorker_Bill_Coll_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                OMMessageBox.Show(e.Error.Message);
            }
            else
            {
                UpdateGridView_BillColl();
            }
        }

        private void UpdateGridView_BillColl()
        {
            while (GridView.Rows.Count > 0)
                GridView.Rows.RemoveAt(0);

            for (int i = 0; i < saleBill_Coll_List.Count; i++)
            {
                GridView.Rows.Add();
                DTOClasses.UCBillCollection items = saleBill_Coll_List[i];
                GridView[0, i].Value = items.salebillno;
                GridView[1, i].Value = items.voucherdate.ToString(Format.DDMMMYYYY);
                GridView[2, i].Value = items.billamount.ToString("0.00");
                GridView[3, i].Value = items.balamount.ToString("0.00"); ;
                GridView[4, i].Value = (items.billamount - items.balamount).ToString("0.00");
                GridView[6, i].Value = items.remark;
            }
            CalculateTotal();

        }

        private void CalculateTotal()
        {
            double Saleamt = 0, Recamt = 0;
            foreach (DTOClasses.UCBillCollection items in saleBill_Coll_List)
            {
                Saleamt = Saleamt + items.billamount;
                Recamt = Recamt + items.balamount;
            }

            lblDrBal.Text = Saleamt.ToString("0.00");
            lblCrNet.Text = Recamt.ToString("0.00");
            lblNetB.Text = (Saleamt - Recamt).ToString("0.00");
            TotalNetBal = (Saleamt - Recamt);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            while (GridView.Rows.Count > 0)
                GridView.Rows.RemoveAt(0);
            saleBill_Coll_List = null;
            TotalNetBal = 0;
            cmbLedgerName.Focus();
        }

        private void GridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (GridView.CurrentCell.ColumnIndex == 5)
            {
                TextBox txtAmt = (TextBox)e.Control;
                txtAmt.TextChanged += new EventHandler(txtAmt_TextChanged);
            }
        }

        private void txtAmt_TextChanged(object sender, EventArgs e)
        {
            //ObjFunction.SetMasked((TextBox)sender, 2);
            if (GridView.CurrentCell.ColumnIndex == 5)
            {
                ObjFunction.SetMasked((TextBox)sender, 2, 9, JitFunctions.MaskedType.NotNegative);
            }
        }

        private void GridView_KeyDown(object sender, KeyEventArgs e)
        {

            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    if (GridView.Rows.Count != 0)
                    {
                        if (GridView.CurrentCell.ColumnIndex == 5)
                        {
                            if (GridView.CurrentCell.Value != null)
                            {
                                if (ObjFunction.CheckValidAmount(GridView.CurrentCell.Value.ToString()) == false)
                                    GridView.CurrentCell.ErrorText = "Please Enter Valid Amount";
                                else
                                    GridView.CurrentCell = GridView[6, GridView.CurrentCell.RowIndex];
                            }
                        }
                    }
                }
                else if (e.KeyCode == Keys.Escape)
                {

                    BtnSave.Focus();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private delegate void MovetoNext(int RowIndex, int ColIndex, DataGridView dg);

        private void m2n(int RowIndex, int ColIndex, DataGridView dg)
        {
            dg.CurrentCell = dg.Rows[RowIndex].Cells[ColIndex];
        }

        private void GridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int rowCount = GridView.CurrentCell.RowIndex;
                GridView.CurrentCell.ErrorText = "";

                if (GridView.CurrentCell.ColumnIndex == 5)
                {
                    if (GridView.CurrentCell.Value != null)
                    {
                        if (ObjFunction.CheckValidAmount(GridView.CurrentCell.Value.ToString()) == false)
                            GridView.CurrentCell.ErrorText = "Please Enter Valid Amount";
                        else if (Convert.ToDouble(GridView.Rows[rowCount].Cells[4].Value) < Convert.ToDouble(GridView.Rows[rowCount].Cells[5].Value))
                            GridView.CurrentCell.ErrorText = "Please Enter Valid Amount";
                        else
                        {
                            MovetoNext move2n = new MovetoNext(m2n);
                            BeginInvoke(move2n, new object[] { rowCount, 5, GridView });
                            //GridView.CurrentCell = GridView[ColIndex.PayType, rowCount];
                        }

                    }
                    else
                    {
                        GridView.CurrentCell.Value = "0.00";
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { rowCount, 6, GridView });
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private bool Validations()
        {
            bool flag = false;
            try
            {
                if (GridView.Rows.Count < 0)
                {
                    OMMessageBox.Show("Sorry No Data to Save......... ", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                }
                else
                {
                    for (int i = 0; i < GridView.Rows.Count; i++)
                    {
                        if (GridView.Rows[i].Cells[5].ErrorText == "")
                        {
                            flag = true;
                        }
                        else
                        {
                            flag = false;
                            OMMessageBox.Show("Please Enter Valid Amount......... ", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            GridView.CurrentCell = GridView.Rows[i].Cells[5];
                            GridView.Focus();
                            return false;
                        }
                    }

                }

                flag = true;

                return flag;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return false;
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (Validations() == false)
                return;
            List<DTOClasses.UCBillCollection> lst_coll = new List<DTOClasses.UCBillCollection>();

            for (int i = 0; i < GridView.Rows.Count; i++)
            {
                DTOClasses.UCBillCollection uc = new DTOClasses.UCBillCollection();
                if (GridView.Rows[i].Cells[5].EditedFormattedValue.ToString() != "" && Convert.ToDouble(GridView.Rows[i].Cells[5].Value) != 0)
                {
                    uc.pkreftrnno = saleBill_Coll_List[i].pkreftrnno;
                    uc.ledgerno = ObjFunction.GetComboValue(cmbPartyName);
                    uc.fkvoucherno = saleBill_Coll_List[i].fkvoucherno;
                    uc.refno = saleBill_Coll_List[i].refno;
                    uc.amount = Convert.ToDouble(GridView.Rows[i].Cells[5].Value);
                    uc.remark = GridView.Rows[i].Cells[6].Value.ToString();
                    uc.voucherdate = Convert.ToDateTime(dtpVoucherDate.Text);
                    lst_coll.Add(uc);
                }
            }

            if (lst_coll != null && lst_coll.Count != 0)
            {
                string strJson = JsonConvert.SerializeObject(lst_coll, Newtonsoft.Json.Formatting.None,
                           new JsonSerializerSettings
                           {
                               NullValueHandling = NullValueHandling.Ignore
                           });

                Display.WorkInProgress wip = new Kirana.Display.WorkInProgress();
                wip.StrStatus = "Save Bill Collection in process, please wait.";
                wip.argument = strJson;
                wip.bgWorker.DoWork += new DoWorkEventHandler(bgWorker_Save_Coll_DoWork);
                wip.bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_Bill_Coll_Save_RunWorkerCompleted);
                ObjFunction.OpenForm(wip);
            }
        }

        private void bgWorker_Save_Coll_DoWork(object sender, DoWorkEventArgs e)
        {
            if (e.Argument.ToString() != "")
            {
                try
                {
                    string result = ObjFunction.getResponseJSON("/SaveCollection", "POST", string.Format("strDetails={0}", e.Argument.ToString()));
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
        }

        private void bgWorker_Bill_Coll_Save_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                OMMessageBox.Show(e.Error.Message);
            }
            else
            {
                while (GridView.Rows.Count > 0)
                    GridView.Rows.RemoveAt(0);
                saleBill_Coll_List = null;

            }
        }

        private void txtAmount_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(txtAmount, 2, 9, JitFunctions.MaskedType.NotNegative);
        }

        private void txtAmount_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                DataGridView gv = GridView;
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    if (ObjFunction.CheckValidAmount(txtAmount.Text.Replace("-", "")) == true)
                    {
                        if (Convert.ToDouble(txtAmount.Text.Trim()) > TotalNetBal)
                        {
                            if (OMMessageBox.Show("Enter Amount less than Total Amount \nExceed Amount consider as advance amount. want to continue, click on Yes or No", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Information) == DialogResult.No)
                            {
                                txtAmount.Focus();
                                return;
                            }
                        }
                        double Amount = Convert.ToDouble(txtAmount.Text.Trim());
                        for (int i = 0; i < gv.Rows.Count; i++)
                        {
                            if (Convert.ToDouble(gv.Rows[i].Cells[4].Value) <= Amount)
                            {
                                gv.Rows[i].Cells[5].Value = gv.Rows[i].Cells[4].Value;
                                Amount = Amount - Convert.ToDouble(gv.Rows[i].Cells[4].Value);
                            }
                            else if (Amount != 0)
                            {
                                if (Convert.ToDouble(gv.Rows[i].Cells[4].Value) <= Amount)
                                {
                                    gv.Rows[i].Cells[5].Value = Convert.ToDouble(gv.Rows[i].Cells[4].Value);
                                    Amount = Amount - Convert.ToDouble(gv.Rows[i].Cells[5].Value);
                                }
                                else
                                {
                                    gv.Rows[i].Cells[5].Value = Amount.ToString("0.00");
                                    Amount = 0;
                                }
                            }
                            else
                            {
                                gv.Rows[i].Cells[5].Value = "0.00";
                            }
                        }
                        gv.CurrentCell = gv[5, 0];
                        gv.Focus();
                    }
                    else
                        MessageBox.Show("Enter Valid Amount", CommonFunctions.ErrorTitle, MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, CommonFunctions.ErrorTitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {

            Display.WorkInProgress wip = new Kirana.Display.WorkInProgress();
            wip.StrStatus = "Search Details in process, please wait.";
            wip.argument = "";
            wip.bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork_Print);
            wip.bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted_Print);
            ObjFunction.OpenForm(wip);



           
        }


        private void bgWorker_DoWork_Print(object sender, DoWorkEventArgs e)
        {
            
                try
                {
                    string result = ObjFunction.getResponseJSON("/GetOutStandingDetails", "GET", null);
                    e.Result = result;

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            
        }

        private void bgWorker_RunWorkerCompleted_Print(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                OMMessageBox.Show(e.Error.Message);
            }
            else
            {
                List<DTOClasses.UCOutStanding> ucDetails = JsonConvert.DeserializeObject<List<DTOClasses.UCOutStanding>>(e.Result.ToString());
                string[] ReportSession;
                ReportSession = new string[0];
                Form NewF = null;
                Reports.GetBillOutSandingDetails GC = new Reports.GetBillOutSandingDetails();
                GC.SetDataSource(ucDetails);
                NewF = new Display.ReportViewSource(GC, ReportSession, true);
                ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
            }
        }


    }

}