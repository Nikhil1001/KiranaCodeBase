﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Vouchers
{
    public partial class OtherCollectionEntryAE : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        DBTVaucherEntry dbTVaucherEntry = new DBTVaucherEntry();
        TVoucherEntry tVoucherEntry = new TVoucherEntry();
        TVoucherDetails tVoucherDetails = new TVoucherDetails();



        DataTable dtSearch = new DataTable();
        int cntRow;
        public long ID, voucherType;
        bool isAllowSingleDate = false;
        public DateTime vchDate = DateTime.Now.Date;
        UiMode uiModeForm = UiMode.None;

        //public OtherCollectionEntryAE()
        //{
        //    InitializeComponent();
        //}

        public OtherCollectionEntryAE(long voucherType)
        {
            InitializeComponent();
            this.voucherType = voucherType;
            initComponent();
        }

        public OtherCollectionEntryAE(long voucherType, long ID, UiMode uiModeForm)
        {
            InitializeComponent();
            this.voucherType = voucherType;
            this.ID = ID;
            this.uiModeForm = uiModeForm;
            initComponent();
        }


        public OtherCollectionEntryAE(long voucherType, DateTime vchDate, UiMode uiModeForm)
        {
            InitializeComponent();
            this.voucherType = voucherType;
            this.vchDate = vchDate;
            this.uiModeForm = uiModeForm;
            isAllowSingleDate = true;
            initComponent();
        }

        private void initComponent()
        {
            if (voucherType == VchType.CashPayment)
            {
                Text = "Cash Payment";
                label5.Text = "Paid From : ";
            }
            else if (voucherType == VchType.CashReceipt)
            {
                Text = "Cash Receipt";
                label5.Text = "Received By : ";
            }
            lblTitle.Text = Text;
        }

        private void OtherCollectionEntryAE_Load(object sender, EventArgs e)
        {
            /*13-Apr-2014
            * Exception Handling Purpose.
            * */
            try
            {
                ObjFunction.LockButtons(true, this.Controls);
                ObjFunction.LockControls(false, this.Controls);

                if (uiModeForm != UiMode.Add)
                {
                    if (isAllowSingleDate)
                    {
                        dtSearch = ObjFunction.GetDataView("Select PkVoucherNo from TVoucherEntry Where VoucherTypeCode=" + voucherType +
                             " AND IsVoucherLock='false' AND VoucherDate = '" + vchDate.ToString(Format.DDMMMYYYY) + "' Order by VoucherDate, VoucherUserNo ").Table;
                    }
                    else if (ID > 0)
                    {
                        dtSearch = ObjFunction.GetDataView("Select PkVoucherNo from TVoucherEntry Where VoucherTypeCode=" + voucherType +
                             " AND IsVoucherLock='false' AND PkVoucherNo = " + ID + " Order by VoucherDate, VoucherUserNo ").Table;
                    }
                    else
                    {
                        dtSearch = ObjFunction.GetDataView("Select PkVoucherNo from TVoucherEntry Where VoucherTypeCode=" + voucherType +
                             " AND IsVoucherLock='false' Order by VoucherDate, VoucherUserNo ").Table;
                    }
                }

                ObjFunction.FillComb(cmbParty, "Select LedgerNo,LedgerName From MLedger " +
                    " Where GroupNo Not in (" + GroupType.SundryDebtors + "," + GroupType.SundryCreditors +
                    "," + GroupType.CashInhand + "," + GroupType.DutiesAndTaxes + "," + GroupType.PurchaseAccount +
                    "," + GroupType.SalesAccount + ","+GroupType.GST+",32,33,36) " +
                    " and IsActive='true' order by LedgerName");

                if (voucherType == VchType.CashPayment || voucherType == VchType.CashReceipt)
                {
                    ObjFunction.FillComb(cmbFromAcount, "Select LedgerNo,LedgerName From MLedger " +
                        " Where GroupNo in (" + GroupType.CashInhand + ") " +
                        " and IsActive='true' order by LedgerName");
                }
                else if (voucherType == VchType.BankPayment || voucherType == VchType.BankReceipt)
                {
                    ObjFunction.FillComb(cmbFromAcount, "Select LedgerNo,LedgerName From MLedger " +
                        " Where GroupNo in (" + GroupType.BankAccounts + "," + GroupType.BankODAccount + ") " +
                        " and IsActive='true' order by LedgerName");
                }

                if (uiModeForm == UiMode.None)
                {
                    if (dtSearch.Rows.Count > 0)
                    {

                        ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                        FillControls();
                        SetNavigation();
                    }
                    setDisplay(true);

                    btnNew.Focus();
                }
                else if (uiModeForm == UiMode.Add)
                {
                    setDisplay(false);
                    dtpVoucherDate.Value = vchDate;

                    btnNew_Click(sender, e);
                    dtpVoucherDate.Enabled = false;
                }
                else if (uiModeForm == UiMode.Search)
                {
                    if (dtSearch.Rows.Count > 0)
                    {

                        ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                        FillControls();
                        SetNavigation();
                    }
                    setDisplay(true);

                    btnSearch_Click(sender, e);
                    uiModeForm = UiMode.None;
                }
                else if (ID > 0)
                {
                    FillControls();
                    SetNavigation();
                    if (uiModeForm == UiMode.Edit)
                    {
                        btnUpdate_Click(sender, e);
                    }
                    else if (uiModeForm == UiMode.View)
                    {
                        btnNew.Enabled = false;
                        btnUpdate.Enabled = false;
                        btnSearch.Enabled = false;
                        setDisplay(false);
                    }
                }

                KeyDownFormat(this.Controls);
            }
            catch (Exception exc)
            {
                //ObjFunction.ExceptionDisplay(exc.Message);;//For Common Error Displayed Purpose
            }
        }

        public void setDisplay(bool flag)
        {
            btnFirst.Visible = flag;
            btnPrev.Visible = flag;
            btnNext.Visible = flag;
            btnLast.Visible = flag;
            btnDelete.Visible = flag;
        }

        private void FillControls()
        {
            /*13-Apr-2014
            * Exception Handling Purpose.
            * */
            try
            {
                EP.SetError(txtAmount, "");

                dtpVoucherDate.MinDate = Convert.ToDateTime("01-01-1900");
                tVoucherEntry = dbTVaucherEntry.ModifyTVoucherEntryByID(ID);

                txtDocNo.Text = tVoucherEntry.VoucherUserNo.ToString();
                dtpVoucherDate.Value = tVoucherEntry.VoucherDate;
                txtAmount.Text = tVoucherEntry.BilledAmount.ToString("0.00");
                long partyNo = ObjQry.ReturnLong("Select LedgerNo from TVoucherDetails  Where FKVoucherNo=" + ID + " AND VoucherSrNo=1", CommonFunctions.ConStr);
                cmbParty.SelectedValue = partyNo.ToString();
                partyNo = ObjQry.ReturnLong("Select LedgerNo from TVoucherDetails  Where FKVoucherNo=" + ID + " AND VoucherSrNo=2", CommonFunctions.ConStr);
                cmbFromAcount.SelectedValue = partyNo.ToString();
                txtRemark.Text = tVoucherEntry.Remark;

            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);//For Common Error Displayed Purpose
            }
        }

        public void SetValue()
        {
            /*13-Apr-2014
            * Exception Handling Purpose.
            * */
            try
            {
                if (Validations() == true)
                {
                    dbTVaucherEntry = new DBTVaucherEntry();
                    tVoucherEntry = new TVoucherEntry();
                    int VoucherSrNo = 1;

                    tVoucherEntry.PkVoucherNo = ID;

                    tVoucherEntry.VoucherTypeCode = voucherType;
                    tVoucherEntry.VoucherUserNo = Convert.ToInt64(txtDocNo.Text);
                    tVoucherEntry.VoucherDate = Convert.ToDateTime(dtpVoucherDate.Text);
                    tVoucherEntry.VoucherTime = Convert.ToDateTime("01-Jan-1900");
                    tVoucherEntry.Reference = "";

                    tVoucherEntry.BilledAmount = Convert.ToDouble(txtAmount.Text);
                    tVoucherEntry.ChallanNo = "";
                    tVoucherEntry.Remark = txtRemark.Text;

                    tVoucherEntry.ChequeNo = 0;
                    tVoucherEntry.ClearingDate = Convert.ToDateTime("01-Jan-1900");
                    tVoucherEntry.Narration = "";
                    tVoucherEntry.UserID = DBGetVal.UserID;
                    tVoucherEntry.UserDate = DBGetVal.ServerTime;
                    tVoucherEntry.OrderType = 1;
                    tVoucherEntry.PayTypeNo = 0;

                    dbTVaucherEntry.AddTVoucherEntry(tVoucherEntry);

                    DataTable dtVoucherDetails = new DataTable();

                    if (ID != 0)
                    {
                        dtVoucherDetails = ObjFunction.GetDataView("Select PkVoucherTrnNo,LedgerNo,0 AS StatusNo From TVoucherDetails Where FkVoucherNo=" + ID + " order by VoucherSrNo").Table;
                    }

                    //Main Party Entry
                    tVoucherDetails = new TVoucherDetails();
                    tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                    tVoucherDetails.VoucherSrNo = VoucherSrNo; VoucherSrNo += 1;
                    tVoucherDetails.LedgerNo = ObjFunction.GetComboValue(cmbParty);
                    if (voucherType == VchType.CashReceipt)
                    {
                        tVoucherDetails.SignCode = 2;
                        tVoucherDetails.Debit = 0;
                        tVoucherDetails.Credit = Convert.ToDouble(txtAmount.Text);
                    }
                    else if (voucherType == VchType.CashPayment)
                    {
                        tVoucherDetails.SignCode = 1;
                        tVoucherDetails.Debit = Convert.ToDouble(txtAmount.Text);
                        tVoucherDetails.Credit = 0;
                    }
                    tVoucherDetails.SrNo = Others.Party;
                    tVoucherDetails.Narration = "";
                    dbTVaucherEntry.AddTVoucherDetails(tVoucherDetails);

                    //Cash Acount Entry
                    tVoucherDetails = new TVoucherDetails();
                    tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                    tVoucherDetails.VoucherSrNo = VoucherSrNo; VoucherSrNo += 1;
                    tVoucherDetails.SignCode = 1;
                    tVoucherDetails.LedgerNo = ObjFunction.GetComboValue(cmbFromAcount);
                    if (voucherType == VchType.CashReceipt)
                    {
                        tVoucherDetails.SignCode = 1;
                        tVoucherDetails.Debit = Convert.ToDouble(txtAmount.Text);
                        tVoucherDetails.Credit = 0;
                    }
                    else if (voucherType == VchType.CashPayment)
                    {
                        tVoucherDetails.SignCode = 2;
                        tVoucherDetails.Debit = 0;
                        tVoucherDetails.Credit = Convert.ToDouble(txtAmount.Text);
                    }
                    tVoucherDetails.SrNo = 0;
                    tVoucherDetails.Narration = "";
                    dbTVaucherEntry.AddTVoucherDetails(tVoucherDetails);

                    long tempID = dbTVaucherEntry.ExecuteNonQueryStatements();
                    if (tempID != 0)
                    {
                        if (ID == 0)
                        {
                            OMMessageBox.Show("Voucher Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            ID = tempID;
                            if (uiModeForm == UiMode.Add)
                            {
                                this.Close();
                                return;
                            }
                            else
                            {
                                DataRow drSearch = dtSearch.NewRow();
                                drSearch[0] = tempID;
                                dtSearch.Rows.Add(drSearch);
                                SetNavigation();
                                FillControls();
                            }
                        }
                        else
                        {
                            OMMessageBox.Show("Voucher Updated Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            FillControls();
                            if (uiModeForm == UiMode.Edit)
                            {
                                this.Close();
                                return;
                            }
                        }

                        ObjFunction.LockButtons(true, this.Controls);
                        ObjFunction.LockControls(false, this.Controls);
                        btnNew.Focus();
                    }
                    else
                    {
                        OMMessageBox.Show("Other Collection not saved", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);;//For Common Error Displayed Purpose
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            SetValue();
        }

        public bool Validations()
        {
            bool flag = false;
            EP.SetError(txtAmount, "");
            if (ObjFunction.GetComboValue(cmbParty) <= 0)
            {

                EP.SetError(cmbParty, "Please Select Party Name");
                EP.SetIconAlignment(cmbParty, ErrorIconAlignment.MiddleRight);
                cmbParty.Focus();
            }
            else if (txtAmount.Text.Trim() == "")
            {

                EP.SetError(txtAmount, "Enter Amount");
                EP.SetIconAlignment(txtAmount, ErrorIconAlignment.MiddleRight);
                txtAmount.Focus();
            }
            else
                flag = true;
            return flag;
        }

        private void CountryAE_FormClosing(object sender, FormClosingEventArgs e)
        {

        }



        #region Navigation Methods
        private void NavigationDisplay(int type)
        {
            /*13-Apr-2014
            * Exception Handling Purpose.
            * */
            try
            {
                long No = 0;
                if (type == 5)
                {
                    No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                    ID = No;
                }

                if (type == 1)
                {
                    No = Convert.ToInt64(dtSearch.Rows[0].ItemArray[0].ToString());
                    cntRow = 0;
                    ID = No;
                }
                else if (type == 2)
                {
                    No = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    cntRow = dtSearch.Rows.Count - 1;
                    ID = No;
                }
                else
                {
                    if (type == 3)
                    {
                        cntRow = cntRow + 1;
                    }
                    else if (type == 4)
                    {
                        cntRow = cntRow - 1;
                    }

                    if (cntRow < 0)
                    {
                        OMMessageBox.Show("This is First Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow + 1;
                    }
                    else if (cntRow > dtSearch.Rows.Count - 1)
                    {
                        OMMessageBox.Show("This is Last Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow - 1;
                    }
                    else
                    {
                        No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                        ID = No;
                    }

                }


                FillControls();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);;//For Common Error Displayed Purpose
            }

        }

        private void SetNavigation()
        {
            cntRow = 0;
            for (int i = 0; i < dtSearch.Rows.Count; i++)
            {
                if (Convert.ToInt64(dtSearch.Rows[i].ItemArray[0].ToString()) == ID)
                {
                    cntRow = i;
                    break;
                }
            }
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            NavigationDisplay(1);
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            NavigationDisplay(4);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            NavigationDisplay(3);
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            NavigationDisplay(2);
        }

        #endregion

        #region KeyDown Events
        public void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is GroupBox)
                    KeyDownFormat(ctrl.Controls);
            }
        }

        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left && e.Control)
            {
                if (btnPrev.Enabled) btnPrev_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Up && e.Control)
            {
                if (btnFirst.Enabled) btnFirst_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Right && e.Control)
            {
                if (btnNext.Enabled) btnNext_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Down && e.Control)
            {
                if (btnLast.Enabled) btnLast_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F2)
            {
                if (BtnSave.Visible) BtnSave_Click(sender, e);
            }
        }
        #endregion

        private void btnNew_Click(object sender, EventArgs e)
        {
            /*13-Apr-2014
            * Exception Handling Purpose.
            * */
            try
            {
                ID = 0;

                ObjFunction.InitialiseControl(this.Controls);
                DateTime dtFrom, dtTo;
                ObjFunction.LockButtons(false, this.Controls);
                ObjFunction.LockControls(true, this.Controls);
                ObjFunction.GetFinancialYear(dtpVoucherDate.Value, out dtFrom, out dtTo);
                txtDocNo.Text = (ObjQry.ReturnLong("Select max(VoucherUserNo) from TVoucherEntry Where VoucherTypeCode=" + voucherType + " AND VoucherDate>='" + dtFrom.Date + "' AND VoucherDate<='" + dtTo.Date + "'", CommonFunctions.ConStr) + 1).ToString();
                dtpVoucherDate.Value = DateTime.Now;
                cmbParty.Focus();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);;//For Common Error Displayed Purpose
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {

            ObjFunction.LockButtons(false, this.Controls);
            ObjFunction.LockControls(true, this.Controls);
            cmbParty.Focus();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (uiModeForm == UiMode.Add || uiModeForm == UiMode.Edit)
            {
                this.Close();
            }
            else
            {
                NavigationDisplay(5);
                ObjFunction.LockButtons(true, this.Controls);
                ObjFunction.LockControls(false, this.Controls);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            OtherCollectionEntrySearch frmChild;
            if (uiModeForm == UiMode.Search)
            {
                frmChild = new OtherCollectionEntrySearch(voucherType, vchDate);
            }
            else
            {
                frmChild = new OtherCollectionEntrySearch(voucherType);
            }

            ObjFunction.OpenForm(frmChild as Form);

            if (frmChild.drUserResponse == DialogResult.OK)
            {
                ID = frmChild.PkVoucherNo;
                FillControls();
            }
            else if(uiModeForm == UiMode.Search)
            {
                this.Close();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            /*13-Apr-2014
            * Exception Handling Purpose.
            * */
            try
            {
                dbTVaucherEntry = new DBTVaucherEntry();
                tVoucherEntry = new TVoucherEntry();

                tVoucherEntry.PkVoucherNo = ID;
                if (OMMessageBox.Show("Are you sure want to delete this record?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (dbTVaucherEntry.DeleteAllVoucherEntry(tVoucherEntry) == true)
                    {
                        OMMessageBox.Show("Other Collection Entry Deleted Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        dtSearch = ObjFunction.GetDataView("Select PkVoucherNo from TVoucherEntry Where VoucherTypeCode=" + voucherType +
                     " AND IsVoucherLock='false' Order by VoucherDate, VoucherUserNo ").Table;
                        if (dtSearch.Rows.Count > 0)
                        {

                            ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                            FillControls();
                        }
                        else
                        {
                            ID = 0;
                            ObjFunction.InitialiseControl(this.Controls);
                            btnNew.Focus();
                        }
                    }
                    else
                    {
                        OMMessageBox.Show("Other Collection Entry not Deleted", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);;//For Common Error Displayed Purpose
            }
        }

        private void txtAmount_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(txtAmount, 2, 9, JitFunctions.MaskedType.NotNegative);
        }
    }
}
