﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Vouchers
{
    public partial class ParkingBillAE : Form
    {

        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        DBTVaucherEntry dbTVoucherEntry = new DBTVaucherEntry();
        private long VoucherTypeCode;
        public long ParkBillNo = -1;

        public ParkingBillAE()
        {
            InitializeComponent();
        }

        public ParkingBillAE(long VoucherTypeCode)
        {
            InitializeComponent();
           this.VoucherTypeCode = VoucherTypeCode;
        }

        private void ParkingBillAE_Shown(object sender, EventArgs e)
        {
            dgParkingBills.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            ShowParkingBill();
        }


        #region Parking Bill Related Methods

        private void ShowParkingBill()
        {
            try
            {
                while (dgParkingBills.Rows.Count > 0)
                    dgParkingBills.Rows.RemoveAt(0);
                new GridSearch(dgParkingBills, 1);
                DataTable dtParking = ObjFunction.GetDataView("SELECT (Case When(VoucherTypeCode=15) Then Cast( BillNo AS Varchar) Else InvNo End) AS BillNo, PersonName AS Name " +
                    ",(SELECT SUM(Qty)FROM TParkingBillDetails  WHERE (ParkingBillNo = TParkingBill.ParkingBillNo)) AS ItemQty,BillDate, BillTime,ParkingBillNo ,Remark" +
                    
                                                              " FROM TParkingBill Where IsBill='false' And VoucherTypeCode="+VoucherTypeCode+" and IsCancel='false' "+
                                                              " Order by PersonName,BillDate desc").Table;
                if (dtParking.Rows.Count > 0)
                {
                    
                    dgParkingBills.DataSource = dtParking.DefaultView;
                    dgParkingBills.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                    dgParkingBills.Focus();
                    dgParkingBills.CurrentCell = dgParkingBills[0, dgParkingBills.CurrentRow.Index];

                }
                else
                    DisplayMessage("Parking Bills not Available...");
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgParkingBills_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //ParkBillNo = 0;
                if (e.KeyCode == Keys.Enter)
                {
                    ParkBillNo = Convert.ToInt64(dgParkingBills.CurrentRow.Cells[5].Value.ToString());
                    this.Close();
                }
                else if (e.KeyCode == Keys.Delete)
                {
                    Application.DoEvents();
                    if (OMMessageBox.Show("Are you sure want to delete Parking Bill...?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        TParkingBill tParkingBill = new TParkingBill();
                        tParkingBill.ParkingBillNo = Convert.ToInt64(dgParkingBills.CurrentRow.Cells[5].Value.ToString());
                        tParkingBill.FKVoucherNo = 0;
                        if (dbTVoucherEntry.DeleteTParkingBill(tParkingBill) == true)
                        {
                            DisplayMessage("Parking Bill Deleted Successfully..");
                            ShowParkingBill();
                        }
                    }
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    this.Close();
                }
                else if (e.KeyCode == Keys.C && e.Control && e.Alt)
                {
                    if (OMMessageBox.Show("Are you sure want cancel this parking bill..", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question, OMMessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        ObjTrans.Execute("Update TParkingBill set IsCancel='true' where ParkingBillNo=" + Convert.ToInt64(dgParkingBills.CurrentRow.Cells[5].Value.ToString()) + "", CommonFunctions.ConStr);
                        Application.DoEvents();
                        DisplayMessage(dgParkingBills.CurrentRow.Cells[0].Value.ToString() + "Parking Bill Cancel Successfully....");
                        ShowParkingBill();
                    }
                    else
                    {
                        dgParkingBills.Focus();
                        dgParkingBills.CurrentCell = dgParkingBills[0, dgParkingBills.CurrentRow.Index];
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void DisplayMessage(string str)
        {
            lblMsg.Visible = true;
            lblMsg.Text = str;
            Application.DoEvents();
            System.Threading.Thread.Sleep(700);
            lblMsg.Visible = false;
        }

        private void dgParkingBills_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                e.Value = Convert.ToDateTime(e.Value).ToString("dd-MMM-yyyy");
            }
            else if (e.ColumnIndex == 4)
            {
                e.Value = Convert.ToDateTime(e.Value).ToShortTimeString();
            }

        }
        #endregion


       
    }
}
