﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Management;
using System.Net.NetworkInformation;
using System.Security.AccessControl;
using OM;
using JitControls;

namespace Kirana.Settings
{
    /// <summary>
    /// This class used for Sales Settings
    /// </summary>
    public partial class SalesSetting : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();
        Security secure = new Security();
        DBMSettings dbMSettings = new DBMSettings();
        bool IsChange = false;
        string strPsw;

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public SalesSetting()
        {
            InitializeComponent(); 
        }

        private void SalesSettingAE_Load(object sender, EventArgs e)
        {
            cmbItemNameType.Enabled = false;
            ObjFunction.FillCombo(cmbParty, "Select LedgerNo,LedgerName From MLedger Where GroupNo in (" + GroupType.SundryDebtors + ")");
            ObjFunction.FillCombo(cmbDefaultParty, "Select LedgerNo,LedgerName From MLedger Where GroupNo in (" + GroupType.SundryDebtors + ") AND IsActive='true'");
            //ObjFunction.FillCombo(cmbTaxType, "Select LedgerNo,LedgerName From MLedger Where GroupNo=" + GroupType.DutiesAndTaxes + "");
            ObjFunction.FillCombo(cmbTaxType, "SELECT GroupNo, GroupName FROM MGroup WHERE (ControlGroup = " + GroupType.DutiesAndTaxes + " ) AND IsActive = 'True' ORDER BY GroupName");
            ObjFunction.FillCombo(cmbDiscount1, "Select LedgerNo,LedgerName From MLedger Where GroupNo in (" + GroupType.DirectExpenses + "," + GroupType.DirectIncome + ")");
            ObjFunction.FillCombo(cmbSchemeDisc, "Select LedgerNo,LedgerName From MLedger Where GroupNo in (" + GroupType.DirectExpenses + "," + GroupType.DirectIncome + ")");
            ObjFunction.FillCombo(cmbOtherDisc, "Select LedgerNo,LedgerName From MLedger Where GroupNo in (" + GroupType.DirectExpenses + "," + GroupType.DirectIncome + ")");
            
            ObjFunction.FillCombo(cmbItemDisc, "Select LedgerNo,LedgerName From MLedger Where GroupNo in (" + GroupType.DirectExpenses + "," + GroupType.DirectIncome + ")");
            ObjFunction.FillCombo(cmbCharge1, "Select LedgerNo,LedgerName From MLedger Where GroupNo in (" + GroupType.InDirectExpenses + "," + GroupType.IndirectIncome + ")");
            
            ObjFunction.FillCombo(cmbRoundOfAccount, "Select LedgerNo,LedgerName From MLedger Where GroupNo in (" + GroupType.InDirectExpenses + "," + GroupType.IndirectIncome + "," + GroupType.DirectExpenses + "," + GroupType.DirectIncome + ")");
            ObjFunction.FillCombo(cmbItemNameType, "SELECT ItemNameTypeNo,ItemNameType FROM MItemNameDisplayType ORDER BY ItemNameTypeNo");
            ObjFunction.FillCombo(cmbOutwardLoc, "SELECT GodownNo,GodownName FROM MGodown where GodownNo<>1 ORDER BY GodownNo ");
            ObjFunction.FillCombo(cmbOrderType, "Select OrderTypeNo,OrderTypeName from MOrderType");

            ObjFunction.FillCombo(cmbGodownNo, " Select GodownNo,GodownName From MGodown Where GodownNo<>1 And IsActive='true'");
            FillDiscType(cmbDiscountType);

            FillRateType();

            FillControls();
            FillGrid();
            tabSalesSetting.SelectedIndex = 0;
            dgSettings.Visible = false;
            KeyDownFormat(this.Controls);

            lblWithin.Visible = false;
            numSeconds.Visible = false;
            lblSeconds.Visible = false;
        }

        internal void FillDiscType(ComboBox cmb)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ID", Type.GetType("System.Int32")); dt.Columns.Add("Desc");
            DataRow dr = dt.NewRow();
            //dr[0] = "0";
            //dr[1] = " ------ Select ------ ";
            //dt.Rows.Add(dr);

            //dr = dt.NewRow();
            dr[0] = 1;
            dr[1] = "Percent";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr[0] = 2;
            dr[1] = "Rupees";
            dt.Rows.Add(dr);

            cmb.DisplayMember = dt.Columns[1].ColumnName;
            cmb.ValueMember = dt.Columns[0].ColumnName;
            cmb.DataSource = dt;
            cmb.SelectedIndex = 0;
        }

        private void FillGrid()
        {
            DataTable dt = new DataTable();
            dt = ObjFunction.GetDataView("Select 0 AS SrNo,SettingKeyCode,SettingValue,PkSettingNo From MSettings Where SettingTypeNo=4").Table;
            dgSettings.DataSource = dt.DefaultView;

        }

        private void FillControls()
        {

            ObjFunction.SetAppSettings();
            SetRateType(Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_RateType)));
            cmbParty.SelectedValue = ObjFunction.GetAppSettings(AppSettings.S_PartyAC);
            cmbDefaultParty.SelectedValue = ObjFunction.GetAppSettings(AppSettings.S_DefaultPartyAC);
            cmbTaxType.SelectedValue = ObjFunction.GetAppSettings(AppSettings.S_TaxType);
            //cmbTransporter.SelectedValue = ObjFunction.GetAppSettings(AppSettings.S_Transporter);
            cmbOutwardLoc.SelectedValue = ObjFunction.GetAppSettings(AppSettings.S_OutwardLocation);
            chkDisc1.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_Discount1Display));
            
            chkChrg1.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_Charges1Display));
            

            cmbCharge1.SelectedValue = ObjFunction.GetAppSettings(AppSettings.S_Charges1);
           
            cmbItemDisc.SelectedValue = ObjFunction.GetAppSettings(AppSettings.S_ItemDisc);
            cmbDiscount1.SelectedValue = ObjFunction.GetAppSettings(AppSettings.S_Discount1);
            cmbSchemeDisc.SelectedValue = ObjFunction.GetAppSettings(AppSettings.S_Discount2);
            cmbOtherDisc.SelectedValue = ObjFunction.GetAppSettings(AppSettings.S_Discount3);
            
            cmbRoundOfAccount.SelectedValue = ObjFunction.GetAppSettings(AppSettings.S_RoundOfAcc);

            chkStopOnRate.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_StopOnRate));
            chkStopOnQty.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_StopOnQty));
            chkIsBarcodeEnabled.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsBarcodeEnabled));
            chkIsAllowsDuplicatesItemsInSameBill.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsAllowsDuplicatesItemsInSameBill));
            chkIsReverseRateCalc.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsReverseRateCalc));

            cmbItemNameType.SelectedValue = ObjFunction.GetAppSettings(AppSettings.S_ItemNameType);

            
            chkStopOnDate.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_StopOnDate));
            chkStopOnParty.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_StopOnParty));
            chkStopOnRateType.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_StopOnRateType));
            chkStopOnTaxType.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_StopOnTaxType));
            chkStopOnGrid.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_StopOnGrid));
            chkSingleFirm.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsAllowSingleFirmChq));
            chkMultipleFirm.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsAllowMultipleChq));

            chkDisplayRateType.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsDisplayRateType));
            chkRatePassword.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_RateTypeAskPassword));
            chkBillPrint.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsBillPrint));
            ChkPayableAmt.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_AskPayableAmount));
            ChkManualBill.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsManualBillNo));
            txtChargeLabel.Text = ObjFunction.GetAppSettings(AppSettings.S_ChargeLabelName).ToString();
            txtCreditCardDigits.Text = ObjFunction.GetAppSettings(AppSettings.S_CreditCardDigitLimit).ToString();
            chkIsUseLastSaleRateEnabled.Enabled = false;
            chkIsStopOnSaleHistoryListEnabled.Enabled = false;
            txtSavingValue.Text = ObjFunction.GetAppSettings(AppSettings.S_SettingValue).ToString();
            txtFooterValue.Text = ObjFunction.GetAppSettings(AppSettings.S_FooterValue).ToString();
            txtFooter2Value.Text = ObjFunction.GetAppSettings(AppSettings.S_Footer2Value).ToString();
            cmbOrderType.SelectedValue = ObjFunction.GetAppSettings(AppSettings.S_OrderType);
            chkShowSavingBill.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_ShowSavingBill));
            chkShowOutStanding.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_ShowOutStanding));
            chckShowSchemeDetails.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_ShowSchemeDetails));
            chkBillRoundOff.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsBillRoundOff));
            chkBillWithMRP.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsBillWithMRP));
            chkAddressInBill.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsAddressInBill));
            chkHomeDelivery.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsAddressInBillHomeDelivery));
            chkCounterBill.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsAddressInBillCouterBill));
            txtHamaliRs.Text = ObjFunction.GetAppSettings(AppSettings.S_HamaliRs).ToString();
            cmbDiscountType.SelectedValue = ObjFunction.GetAppSettings(AppSettings.S_DefaultDiscountType);
            cmbGodownNo.SelectedValue = ObjFunction.GetAppSettings(AppSettings.S_DefaultStockLocation);
            chkGodownPrinting.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_GodownPrinting));
            chkStopOnDisc.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_StopOnDisc));

            chkAllBillUpdate.Checked = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsBillUpdate));
            txtBillUpdatePwd.Text = (ObjFunction.GetAppSettings(AppSettings.S_BillUpdatePwd).ToString()) == "" ? "" : secure.psDecrypt(ObjFunction.GetAppSettings(AppSettings.S_BillUpdatePwd).ToString());
            txtBillUpdatePwd.Visible = chkAllBillUpdate.Checked;



            if (chkAddressInBill.Checked == true)
            {
              
                pnlAddressInBill.Visible = true;
            }
            else
            {               
                pnlAddressInBill.Visible = false;
                chkHomeDelivery.Checked = false;
                chkCounterBill.Checked = false;

            }
            FillGrid();

        }

        #region Rate Type Realted Methods and Functions
        private void FillRateType()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("RateType");
            dt.Columns.Add("RateTypeName");
            DataRow dr = null;

            //dr = dt.NewRow();
            //dr[0] = "----Select----";
            //dr[1] = "0";
            //dt.Rows.Add(dr);

            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ARateIsActive)) == true)
            {
                dr = dt.NewRow();
                dr[1] = ObjFunction.GetAppSettings(AppSettings.ARateLabel);
                dr[0] = "ASaleRate";
                dt.Rows.Add(dr);
            }

            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.BRateIsActive)) == true)
            {
                dr = dt.NewRow();
                dr[1] = ObjFunction.GetAppSettings(AppSettings.BRateLabel);
                dr[0] = "BSaleRate";
                dt.Rows.Add(dr);
            }

            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.CRateIsActive)) == true)
            {
                dr = dt.NewRow();
                dr[1] = ObjFunction.GetAppSettings(AppSettings.CRateLabel);
                dr[0] = "CSaleRate";
                dt.Rows.Add(dr);
            }

            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.DRateIsActive)) == true)
            {
                dr = dt.NewRow();
                dr[1] = ObjFunction.GetAppSettings(AppSettings.DRateLabel);
                dr[0] = "DSaleRate";
                dt.Rows.Add(dr);
            }

            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ERateIsActive)) == true)
            {
                dr = dt.NewRow();
                dr[1] = ObjFunction.GetAppSettings(AppSettings.ERateLabel);
                dr[0] = "ESaleRate";
                dt.Rows.Add(dr);
            }
            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_BillWithMRP)) == true)
            {
                dr = dt.NewRow();
                dr[1] = "MRP";
                dr[0] = "MRP";
                dt.Rows.Add(dr);
            }

            cmbRateType.DataSource = dt.DefaultView;
            cmbRateType.DisplayMember = dt.Columns[1].ColumnName;
            cmbRateType.ValueMember = dt.Columns[0].ColumnName;
        }

        private int GetRateType()
        {
            string str = ObjFunction.GetComboValueString(cmbRateType);
            int val = 0;
            if (str == "ASaleRate") val = 1;
            else if (str == "BSaleRate") val = 2;
            else if (str == "CSaleRate") val = 3;
            else if (str == "DSaleRate") val = 4;
            else if (str == "ESaleRate") val = 5;
            else if (str == "MRP") val = 6;
            return val;
        }

        private void SetRateType(long RateTypeNo)
        {
            if (RateTypeNo == 1) cmbRateType.SelectedValue = "ASaleRate";
            else if (RateTypeNo == 2) cmbRateType.SelectedValue = "BSaleRate";
            else if (RateTypeNo == 3) cmbRateType.SelectedValue = "CSaleRate";
            else if (RateTypeNo == 4) cmbRateType.SelectedValue = "DSaleRate";
            else if (RateTypeNo == 5) cmbRateType.SelectedValue = "ESaleRate";
            else if (RateTypeNo == 6) cmbRateType.SelectedValue = "MRP";
        }
        #endregion

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (Validations() == true)
            {
                //panel 1
                dbMSettings.AddAppSettings(AppSettings.S_PartyAC, ObjFunction.GetComboValue(cmbParty).ToString());
                dbMSettings.AddAppSettings(AppSettings.S_DefaultPartyAC, ObjFunction.GetComboValue(cmbDefaultParty).ToString());
                dbMSettings.AddAppSettings(AppSettings.S_RateType, GetRateType().ToString());
                dbMSettings.AddAppSettings(AppSettings.S_ItemNameType, ObjFunction.GetComboValue(cmbItemNameType).ToString());
                dbMSettings.AddAppSettings(AppSettings.S_TaxType, ObjFunction.GetComboValue(cmbTaxType).ToString());
                //dbMSettings.AddAppSettings(AppSettings.S_Transporter, ObjFunction.GetComboValue(cmbTransporter).ToString());
                dbMSettings.AddAppSettings(AppSettings.S_RoundOfAcc, ObjFunction.GetComboValue(cmbRoundOfAccount).ToString());
                dbMSettings.AddAppSettings(AppSettings.S_HideRatePopupAutomatically_Seconds, (numSeconds.Value.ToString()));
                if (ObjFunction.GetComboValue(cmbOutwardLoc) == 0)
                    dbMSettings.AddAppSettings(AppSettings.S_OutwardLocation, "2");
                else
                    dbMSettings.AddAppSettings(AppSettings.S_OutwardLocation, ObjFunction.GetComboValue(cmbOutwardLoc).ToString());

                //panel 2
                dbMSettings.AddAppSettings(AppSettings.S_Charges1, ObjFunction.GetComboValue(cmbCharge1).ToString());
                

                dbMSettings.AddAppSettings(AppSettings.S_Charges1Display, (chkChrg1.Checked ? "True" : "False"));
                
                //panel 3
                dbMSettings.AddAppSettings(AppSettings.S_Discount1, ObjFunction.GetComboValue(cmbDiscount1).ToString());
                dbMSettings.AddAppSettings(AppSettings.S_Discount2, ObjFunction.GetComboValue(cmbSchemeDisc).ToString());
                dbMSettings.AddAppSettings(AppSettings.S_Discount3, ObjFunction.GetComboValue(cmbOtherDisc).ToString());

                dbMSettings.AddAppSettings(AppSettings.S_ItemDisc, ObjFunction.GetComboValue(cmbItemDisc).ToString());

                dbMSettings.AddAppSettings(AppSettings.S_Discount1Display, (chkDisc1.Checked ? "True" : "False"));
                
                //panel 4
                dbMSettings.AddAppSettings(AppSettings.S_StopOnDate, (chkStopOnDate.Checked ? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_StopOnRateType, (chkStopOnRateType.Checked ? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_StopOnParty, (chkStopOnParty.Checked ? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_StopOnTaxType, (chkStopOnTaxType.Checked ? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_StopOnGrid, (chkStopOnGrid.Checked ? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_StopOnQty, (chkStopOnQty.Checked? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_StopOnRate, (chkStopOnRate.Checked? "True" : "False"));
                //panel 5
                dbMSettings.AddAppSettings(AppSettings.S_IsBarcodeEnabled, (chkIsBarcodeEnabled.Checked? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_IsAllowsDuplicatesItemsInSameBill, (chkIsAllowsDuplicatesItemsInSameBill.Checked? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_IsReverseRateCalc, (chkIsReverseRateCalc.Checked ? "True" : "False"));
       
                dbMSettings.AddAppSettings(AppSettings.S_IsShowSalesHistoryEnabled, (chkIsShowSalesHistoryEnabled.Checked ? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_IsShowPurchaseHistoryEnabled, (chkIsShowPurchaseHistoryEnabled.Checked ? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_IsUseLastSaleRateEnabled, (chkIsUseLastSaleRateEnabled.Checked ? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_IsStopOnSaleHistoryListEnabled, (chkIsStopOnSaleHistoryListEnabled.Checked ? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_ShowRateHistoryAutomatically,(chkShowRateHistoryAutomatically.Checked ? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_HideRatePopupAutomatically,(chkHideRatePopupAutomatically.Checked ? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_HideRatePopupAutomatically_Seconds,(numSeconds.Value.ToString()));
                dbMSettings.AddAppSettings(AppSettings.S_IsAllowMultipleChq, (chkMultipleFirm.Checked ? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_IsAllowSingleFirmChq, (chkSingleFirm.Checked ? "True" : "False"));

                dbMSettings.AddAppSettings(AppSettings.S_IsDisplayRateType, (chkDisplayRateType.Checked ? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_RateTypeAskPassword, (chkRatePassword.Checked ? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_IsBillPrint, (chkBillPrint.Checked ? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_AskPayableAmount, (ChkPayableAmt.Checked ? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_IsManualBillNo, (ChkManualBill.Checked ? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_ChargeLabelName, ((txtChargeLabel.Text.Trim() == "") ? "Charges" : txtChargeLabel.Text.Trim()));
                dbMSettings.AddAppSettings(AppSettings.S_CreditCardDigitLimit, txtCreditCardDigits.Text.Trim());
                dbMSettings.AddAppSettings(AppSettings.S_SettingValue,txtSavingValue.Text.Trim());
                dbMSettings.AddAppSettings(AppSettings.S_FooterValue,txtFooterValue.Text.Trim());
                dbMSettings.AddAppSettings(AppSettings.S_Footer2Value, txtFooter2Value.Text.Trim());
                dbMSettings.AddAppSettings(AppSettings.S_OrderType, ObjFunction.GetComboValue(cmbOrderType).ToString());
                dbMSettings.AddAppSettings(AppSettings.S_ShowSavingBill, (chkShowSavingBill.Checked ? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_ShowOutStanding, (chkShowOutStanding.Checked ? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_IsBillRoundOff, (chkBillRoundOff.Checked ? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_IsBillWithMRP, (chkBillWithMRP.Checked ? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_IsAddressInBill, (chkAddressInBill.Checked ? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_IsAddressInBillHomeDelivery, (chkHomeDelivery.Checked ? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_IsAddressInBillCouterBill, (chkCounterBill.Checked ? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_ShowSchemeDetails, (chckShowSchemeDetails.Checked ? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_HamaliRs, txtHamaliRs.Text.Trim());

                dbMSettings.AddAppSettings(AppSettings.S_GodownPrinting, (chkGodownPrinting.Checked ? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_DefaultStockLocation, ObjFunction.GetComboValue(cmbGodownNo).ToString());
                dbMSettings.AddAppSettings(AppSettings.S_DefaultDiscountType, ObjFunction.GetComboValue(cmbDiscountType).ToString());
                dbMSettings.AddAppSettings(AppSettings.S_StopOnDisc, (chkStopOnDisc.Checked ? "True" : "False"));


                dbMSettings.AddAppSettings(AppSettings.S_IsBillUpdate, (chkAllBillUpdate.Checked ? "True" : "False"));
                dbMSettings.AddAppSettings(AppSettings.S_BillUpdatePwd, (txtBillUpdatePwd.Text == "" ? "" : secure.psEncrypt(txtBillUpdatePwd.Text)));

                
                if (IsChange == true)
                {
                    for (int i = 0; i < dgSettings.Rows.Count; i++)
                    {
                        dbMSettings.AddAppSettings(Convert.ToInt32(dgSettings.Rows[i].Cells[3].Value), (Convert.ToBoolean(dgSettings.Rows[i].Cells[2].FormattedValue) ? "True" : "False"));
                    }
                }

                if (dbMSettings.ExecuteNonQueryStatements() == true)
                {
                    ObjFunction.SetAppSettings();
                    OMMessageBox.Show("Sales Setting Saved Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                    ObjFunction.SetAppSettings();
                    FillControls();
                }
                else
                {
                    OMMessageBox.Show("Sales Setting Not Saved", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                    EP.SetError(btnSave, DBMSettings.strerrormsg);
                    EP.SetIconAlignment(btnSave, ErrorIconAlignment.MiddleRight);
                }
            }
        }

        private bool Validations()
        {
            EP.SetError(cmbItemNameType, "");
            EP.SetError(txtCreditCardDigits, "");
            EP.SetError(txtHamaliRs, "");
            bool flag = false;
            if (ObjFunction.GetComboValue(cmbItemNameType) <= 0)
            {
                EP.SetError(cmbItemNameType, "Select Item Name Display Type");
                EP.SetIconAlignment(cmbItemNameType, ErrorIconAlignment.MiddleRight);
                cmbItemNameType.Focus();
                tabSalesSetting.SelectedTab = tabPage1;
                flag = false;
            }
            else if(txtCreditCardDigits.Text.Trim()=="")
            {
                EP.SetError(txtCreditCardDigits, "Enter CreditCard Digits");
                EP.SetIconAlignment(txtCreditCardDigits, ErrorIconAlignment.MiddleRight);
                txtCreditCardDigits.Focus();
                tabSalesSetting.SelectedTab = tabPage4;
                flag = false;
            }
            else if(ObjFunction.CheckNumeric(txtCreditCardDigits.Text.Trim())==false)
            {
                EP.SetError(txtCreditCardDigits, "Enter Valid Digits");
                EP.SetIconAlignment(txtCreditCardDigits, ErrorIconAlignment.MiddleRight);
                txtCreditCardDigits.Focus();
                tabSalesSetting.SelectedTab = tabPage4;
                flag = false;
            }
            else if (ObjFunction.GetComboValue(cmbOrderType) <= 0)
            {
                EP.SetError(cmbOrderType, "Select Order Type");
                EP.SetIconAlignment(cmbOrderType, ErrorIconAlignment.MiddleRight);
                cmbOrderType.Focus();
                tabSalesSetting.SelectedTab = tabPage5;
                flag = false;
            }
            else if (txtHamaliRs.Text.Trim()== "")
            {
                EP.SetError(txtHamaliRs, "Enter Hamali");
                EP.SetIconAlignment(txtHamaliRs, ErrorIconAlignment.MiddleRight);
                txtHamaliRs.Focus();
                flag = false;
            }
            else
                flag = true;
            return flag;
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void chk_CheckedChanged(object sender, EventArgs e)
        {
            if ( ((CheckBox)sender).Checked == true)
            {
                ((CheckBox)sender).Text = "Yes";
            }
            else
            {
                ((CheckBox)sender).Text = "No";
            }
        }

        private void chkChrg_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            
            if (chk.Name == "chkChrg1")
            {
                cmbCharge1.Enabled = chk.Checked;
                cmbCharge1.SelectedIndex = 0;
            }
            

        }

        private void chkDisc_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;

            if (chk.Name == "chkDisc1")
            {
                cmbDiscount1.Enabled = chk.Checked;
                cmbDiscount1.SelectedIndex = 0;
            }
            
        }

        private void dgSettings_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                IsChange = true;
            }
        }

        #region KeyDown Events
        private void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is GroupBox)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is OMTabControl)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is OMTabPage)
                    KeyDownFormat(ctrl.Controls);
            }
        }

        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            
            strPsw += (char)e.KeyValue;
            if (strPsw.ToLower() == "logicall")
            {
                strPsw = "";
                tabSalesSetting.Visible = false;
                dgSettings.Visible = true;
                dgSettings.Width = 736; dgSettings.Height = 239;
                dgSettings.Location = new Point(13, 12);
            }
            else if (e.KeyCode == Keys.F1)
                tabSalesSetting.SelectedIndex = 0;
            else if (e.KeyCode == Keys.F2)
                tabSalesSetting.SelectedIndex = 1;
            else if (e.KeyCode == Keys.F3)
                tabSalesSetting.SelectedIndex = 2;
            else if (e.KeyCode == Keys.F4)
                tabSalesSetting.SelectedIndex = 3;
            else if (e.KeyCode == Keys.F5)
                tabSalesSetting.SelectedIndex = 4;
        }

        #endregion

        private void dgSettings_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                dgSettings.Visible = false;
                tabSalesSetting.Visible = true;
            }
        }

        private void SalesSettingAE_Activated(object sender, EventArgs e)
        {
           
        }

        private void SalesSettingAE_Click(object sender, EventArgs e)
        {
            strPsw = "";
        }

        private void dgSettings_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                e.Value = Convert.ToString(e.RowIndex + 1);
            }
        }

        private void chkIsShowSalesHistoryEnabled_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked == true)
            {
                ((CheckBox)sender).Text = "Yes";
                chkIsUseLastSaleRateEnabled.Enabled = true;
                chkIsStopOnSaleHistoryListEnabled.Enabled = false;
            }
            else
            {
                ((CheckBox)sender).Text = "No";
                chkIsUseLastSaleRateEnabled.Enabled = false;
                chkIsStopOnSaleHistoryListEnabled.Enabled = false;

                chkIsUseLastSaleRateEnabled.Checked = false;
                chkIsStopOnSaleHistoryListEnabled.Checked = false;

            }
        }

        private void chkIsUseLastSaleRateEnabled_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked == true)
            {
                ((CheckBox)sender).Text = "Yes";
                chkIsStopOnSaleHistoryListEnabled.Enabled = true;
            }
            else
            {
                ((CheckBox)sender).Text = "No";
                chkIsStopOnSaleHistoryListEnabled.Enabled = false;
                chkIsStopOnSaleHistoryListEnabled.Checked = false;

            }
        }

        private void chkIsStopOnSaleHistoryListEnabled_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked == true)
            {
                ((CheckBox)sender).Text = "Yes";
            }
            else
            {
                ((CheckBox)sender).Text = "No";
            }
        }

        private void chkIsShowPurchaseHistoryEnabled_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked == true)
            {
                ((CheckBox)sender).Text = "Yes";
            }
            else
            {
                ((CheckBox)sender).Text = "No";
            }
        }

        private void chkShowRateHistoryAutomatically_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked == true)
            {
                ((CheckBox)sender).Text = "Yes";
            }
            else
            {
                ((CheckBox)sender).Text = "No";
            }
        }

        private void chkHideRatePopupAutomatically_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked == true)
            {
                lblWithin.Visible = true;
                numSeconds.Visible = true;
                lblSeconds.Visible = true;                         
            }
            else
            {
                lblWithin.Visible = false;
                numSeconds.Visible = false;
                lblSeconds.Visible = false;                
            }
        }

        private void chkSingleFirm_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked == true)
            {
                ((CheckBox)sender).Text = "Yes";
                chkMultipleFirm.Checked = false;
            }
            else
            {
                ((CheckBox)sender).Text = "No";
            }
        }

        private void chkMultipleFirm_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked == true)
            {
                ((CheckBox)sender).Text = "Yes";
                chkSingleFirm.Checked = false;
            }
            else
            {
                ((CheckBox)sender).Text = "No";
            }
        }

        private void chkBillPrint_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked == true)
            {
                ((CheckBox)sender).Text = "Yes";               
            }
            else
            {
                ((CheckBox)sender).Text = "No";
            }
        }

        private void ChkPayableAmt_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked == true)
            {
                ((CheckBox)sender).Text = "Yes";
            }
            else
            {
                ((CheckBox)sender).Text = "No";
            }
        }

        private void ChkManualBill_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked == true)
            {
                ((CheckBox)sender).Text = "Yes";
            }
            else
            {
                ((CheckBox)sender).Text = "No";
            }
        }

        private void chkFooterLevelDisc_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked == true)
            {
                ((CheckBox)sender).Text = "Yes";
            }
            else
            {
                ((CheckBox)sender).Text = "No";
            }
        }

        private void chkItemLevelDisc_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked == true)
            {
                ((CheckBox)sender).Text = "Yes";
            }
            else
            {
                ((CheckBox)sender).Text = "No";
            }
        }

        private void chkBillWithMRP_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked == true)
            {
                ((CheckBox)sender).Text = "Yes";
            }
            else
            {
                ((CheckBox)sender).Text = "No";
            }
        }

        private void chkAddressInBill_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked == true)
            {
                ((CheckBox)sender).Text = "Yes";
                pnlAddressInBill.Visible = true;
            }
            else
            {
                ((CheckBox)sender).Text = "No";
                pnlAddressInBill.Visible = false;
                    chkHomeDelivery.Checked=false;
                    chkCounterBill.Checked = false;

            }
        }

        private void chkHomeDelivery_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked == true)
            {
                ((CheckBox)sender).Text = "Yes";
            }
            else
            {
                ((CheckBox)sender).Text = "No";
            }
        }

        private void chkCounterBill_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked == true)
            {
                ((CheckBox)sender).Text = "Yes";
            }
            else
            {
                ((CheckBox)sender).Text = "No";
            }
        }

        private void txtHamaliRs_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(((TextBox)sender), 2, 9, JitFunctions.MaskedType.NotNegative);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkAllBillUpdate_CheckedChanged(object sender, EventArgs e)
        {
            txtBillUpdatePwd.Visible = ((CheckBox)sender).Checked;
            if (((CheckBox)sender).Checked == true)
                ((CheckBox)sender).Text = "Yes";
            else
                ((CheckBox)sender).Text = "No";
        }
    }
}
