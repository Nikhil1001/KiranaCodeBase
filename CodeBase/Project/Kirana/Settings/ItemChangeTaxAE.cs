﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Settings
{
    public partial class ItemChangeTaxAE : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();
        DataTable dt = new DataTable();
        DataView dv = new DataView();
        DataTable dt1 = new DataTable();
        DataView dv1 = new DataView();

        public ItemChangeTaxAE()
        {
            InitializeComponent();
        }

        private void MultiItemsTaxSettings_Load(object sender, EventArgs e)
        {
            ObjFunction.FillCombo(cmbGroupNo2, "SELECT DISTINCT MStockGroup.StockGroupNo, MStockGroup.StockGroupName  FROM   MStockGroup INNER JOIN  MStockItems ON MStockGroup.StockGroupNo = MStockItems.GroupNo  WHERE  (MStockGroup.IsActive = 'True') AND (MStockGroup.ControlGroup = 3) ORDER BY MStockGroup.StockGroupName");
            ObjFunction.FillCombo(cmbDepartmentName, "SELECT DISTINCT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=4 order by StockGroupName");
            ObjFunction.FillCombo(cmbCategoryName, "SELECT DISTINCT StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=2 order By StockGroupName");


            ObjFunction.FillCombo(cmbHSNCode, "SELECT MStockHSNCode.HSNNo, MStockHSNCode.HSNCode FROM MStockHSNCode " +
                " WHERE (MStockHSNCode.IsActive = 'True') " +
                " Order by MStockHSNCode.HSNCode");

            ObjFunction.FillCombo(cmbVatSales, "SELECT MItemTaxSetting.PkSrNo, " +
                       " (cast(MItemTaxSetting.IGSTPercent as varchar)+ ' / ')+" +
                        " (cast(MItemTaxSetting.CGSTPercent as varchar)+ ' / ')+" +
                        " (cast(MItemTaxSetting.SGSTPercent as varchar)+ ' / ')+" +
                        " (cast(MItemTaxSetting.UTGSTPercent as varchar)+ ' / ')+" +
                        " (cast(MItemTaxSetting.CessPercent as varchar))" +
                    " as Percentage " +
                    " FROM MItemTaxSetting " +
                    " WHERE (MItemTaxSetting.TransactionTypeNo = " + GroupType.SalesAccount + ") " +
                    " AND (MItemTaxSetting.TaxTypeNo = " + GroupType.GST + " AND MItemTaxSetting.IsActive='True') " +
                    " Order by  MItemTaxSetting.IGSTPercent,MItemTaxSetting.CessPercent ");

            ObjFunction.FillCombo(cmbVatPurchase, "SELECT MItemTaxSetting.PkSrNo, " +
                    " (cast(MItemTaxSetting.Percentage as varchar)+ ' %') as Percentage " +
                    " FROM MItemTaxSetting " +
                    " WHERE (MItemTaxSetting.TransactionTypeNo = " + GroupType.PurchaseAccount + ") " +
                    " AND (MItemTaxSetting.TaxTypeNo = " + GroupType.GST + " AND MItemTaxSetting.IsActive='True') " +
                    " Order by  MItemTaxSetting.IGSTPercent,MItemTaxSetting.CessPercent ");

            dgvTaxItem.ColumnHeadersDefaultCellStyle.Font = GetFont();
            dgvTaxItem.Columns[ColIndex.SrNo].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvTaxItem.Columns[ColIndex.SVat].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvTaxItem.Columns[ColIndex.PVat].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            rdBoth.Checked = true;
            new GridSearch(dgvTaxItem, ColIndex.ItemName, 2);
            KeyDownFormat(this.Controls);
            //pnlrb.Visible = false;
        }

        private Font GetFont()
        {
            return new Font("Microsoft Sans Serif", 8, FontStyle.Regular);
        }


        private void BindGrid1(int recordType)
        {
            dt1.Clear();
            string sql = "";

            sql = " SELECT DISTINCT 0 as SrNo,MStockBarcode.Barcode, MStockGroup.StockGroupName + ' ' + MStockItems.ItemName AS ItemName, " +
                          " ISNULL(MItemTaxInfo_S.Percentage, NULL) AS SVat, " +
                          " ISNULL(MItemTaxInfo_P.Percentage, NULL) AS PVat, " +
                          " ISNULL (MItemTaxInfo_S.PkSrNo, 0) AS SalesLedgerNo, " +
                          " ISNULL (MItemTaxInfo_P.PkSrNo, 0) AS PurLedgerNo, " +
                          " MStockItems.ItemNo, " +
                          " ISNULL (MItemTaxInfo_S.HSNCode, '') AS HSNCode " +
                          " FROM " +
                          " MStockItems INNER JOIN " +
                          " MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo INNER JOIN " +
                          " MStockBarcode ON MStockBarcode.ItemNo = MStockItems.ItemNo LEFT OUTER JOIN " +
                                  " ( SELECT MItemTaxInfo_S.Percentage, MItemTaxInfo_S.PkSrNo, MItemTaxInfo_S.ItemNo, MItemTaxInfo_S.HSNCode FROM " +
                                  " MItemTaxInfo AS MItemTaxInfo_S" +
                                  " WHERE MItemTaxInfo_S.TransactionTypeNo = " + GroupType.SalesAccount + " " +
                                  " AND  MItemTaxInfo_S.TaxTypeNo = " + GroupType.GST + " " +
                          " ) MItemTaxInfo_S ON MItemTaxInfo_S.ItemNo = MStockItems.ItemNo LEFT OUTER JOIN " +
                                  " ( SELECT MItemTaxInfo_P.Percentage, MItemTaxInfo_P.PkSrNo, MItemTaxInfo_P.ItemNo, MItemTaxInfo_P.HSNCode FROM " +
                                  " MItemTaxInfo AS MItemTaxInfo_P " +
                                  " WHERE MItemTaxInfo_P.TransactionTypeNo = " + GroupType.PurchaseAccount + " " +
                                  " AND MItemTaxInfo_P.TaxTypeNo = " + GroupType.GST + " " +
                          " ) MItemTaxInfo_P ON MItemTaxInfo_P.ItemNo = MStockItems.ItemNo " +
                          " WHERE     (MStockItems.ItemNo <> 1) ";


            string StrWhere = "";
            //0==Both
            if (recordType == 1) // Assigned
            {
                StrWhere += " AND MItemTaxInfo_S.HSNCode IS NOT NULL AND LEN(MItemTaxInfo_S.HSNCode) > 0 " +
                            " AND MItemTaxInfo_P.HSNCode IS NOT NULL AND LEN(MItemTaxInfo_P.HSNCode) > 0 ";
            }
            else if (recordType == 2) // Not Assigned
            {
                StrWhere += " AND (MItemTaxInfo_S.HSNCode IS NULL OR LEN(MItemTaxInfo_S.HSNCode) = 0) " +
                            " AND (MItemTaxInfo_P.HSNCode IS NULL OR LEN(MItemTaxInfo_P.HSNCode) = 0) ";
            }

            if (ObjFunction.GetComboValue(cmbGroupNo2) > 0)
            {
                StrWhere += " And MStockItems.GroupNo = " + ObjFunction.GetComboValue(cmbGroupNo2) + " ";
            }
            if (ObjFunction.GetComboValue(cmbCategoryName) > 0)
            {
                StrWhere += " And MStockItems.GroupNo1 = " + ObjFunction.GetComboValue(cmbCategoryName) + " ";
            }
            if (ObjFunction.GetComboValue(cmbDepartmentName) > 0)
            {
                StrWhere += " And MStockItems.FKStockDeptNo = " + ObjFunction.GetComboValue(cmbDepartmentName) + " ";
            }
            if (txtBarcode.Text.Trim() != "")
            {
                StrWhere += " And MStockBarcode.Barcode = '" + txtBarcode.Text.Trim() + "' ";
            }
            sql += StrWhere + " order by ItemName, Barcode ";
            /*13-Apr-2014
            * Exception Handling Purpose.
            * */
            try
            {
                if (StrWhere != "")
                {
                    dt1 = ObjFunction.GetDataView(sql).Table;
                }
                dgvTaxItem.DataSource = dt1.DefaultView;
                if (dt1.Rows.Count > 0)
                    pnlTAx.Visible = true;
                else
                    pnlTAx.Visible = false;
            }
            catch (Exception e)
            {
                CommonFunctions.ErrorMessge = e.Message;
            }
        }

        private void dgvItemTax_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                e.Value = (e.RowIndex + 1).ToString();
            }

        }

        private void cmbGroupNo1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                Validation();

            }
        }

        private void dgvTaxItem_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                e.Value = (e.RowIndex + 1).ToString();
            }
        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvTaxItem.Rows.Count; i++)
            {
                dgvTaxItem.Rows[i].Cells[0].Value = chkSelectAll.Checked;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ClearFields();
            cmbCategoryName.Enabled = true;
            cmbDepartmentName.Enabled = true;
        }

        public void ClearFields()
        {
            dt.Clear();
            dt1.Clear();

            EP.SetError(cmbGroupNo2, "");
            EP.SetError(cmbCategoryName, "");
            EP.SetError(cmbDepartmentName, "");
            chkSelectAll.Checked = false;
            txtBarcode.Text = "";
            ObjFunction.FillCombo(cmbGroupNo2, "SELECT DISTINCT MStockGroup.StockGroupNo, MStockGroup.StockGroupName  FROM   MStockGroup INNER JOIN  MStockItems ON MStockGroup.StockGroupNo = MStockItems.GroupNo  WHERE  (MStockGroup.IsActive = 'True') AND (MStockGroup.ControlGroup = 3) ORDER BY MStockGroup.StockGroupName");
            ObjFunction.FillCombo(cmbDepartmentName, "SELECT Distinct StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=4 order by StockGroupName");
            ObjFunction.FillCombo(cmbCategoryName, "SELECT Distinct StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=2 order By StockGroupName");
            cmbGroupNo2.SelectedIndex = 0;
            cmbCategoryName.SelectedIndex = 0;
            cmbDepartmentName.SelectedIndex = 0;
            cmbVatPurchase.SelectedIndex = 0;
            cmbVatSales.SelectedIndex = 0;

            rdBoth.Checked = true;
            dgvTaxItem.DataSource = dt1.DefaultView;
            txtHSNCode.Text = "";
            //pnlrb.Visible = false;
            pnlTAx.Visible = false;
            cmbGroupNo2.Focus();
        }

        private void btnSetTax_Click(object sender, EventArgs e)
        {
            if (Validations() == true)
            {
                SetValue();
            }
        }

        public bool Validations()
        {
            bool flag = true;
            EP.SetError(cmbVatPurchase, "");
            EP.SetError(cmbVatSales, "");
            if (ObjFunction.GetComboValue(cmbVatSales) <= 0 && ObjFunction.GetComboValue(cmbVatPurchase) <= 0)
            {
                DisplayMessage("Please select HSN Code Or GST Tax %");
                flag = false;
            }

            return flag;
        }

        public void SetValue()
        {
            bool flag = false, SFlag = false;
            DataTable dt = new DataTable();
            DBMDutiesTaxesInfo dbMDutiesTaxesInfo = new DBMDutiesTaxesInfo();
            MItemTaxInfo mItemTaxInfo = new MItemTaxInfo();
            Application.DoEvents();
            /*13-Apr-2014
            * Exception Handling Purpose.
            * */
            try
            {
                for (int i = 0; i < dgvTaxItem.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(dgvTaxItem[0, i].Value) == true)
                    {
                        if (ObjFunction.GetComboValue(cmbVatSales) > 0)
                        {
                            DataTable dtItemTaxSales = ObjFunction.GetDataView("Select " +
                                " TaxLedgerNo,SalesLedgerNo,CalculationMethod,Percentage,TaxTypeNo,TransactionTypeNo, " +
                                " IGSTPercent,CGSTPercent,SGSTPercent,UTGSTPercent,CessPercent " +
                                " From MItemTaxSetting Where PkSrNo = " + ObjFunction.GetComboValue(cmbVatSales) + "").Table;
                            if (dtItemTaxSales.Rows.Count > 0)
                            {
                                dbMDutiesTaxesInfo = new DBMDutiesTaxesInfo();
                                mItemTaxInfo = new MItemTaxInfo();
                                flag = true;
                                mItemTaxInfo.PkSrNo = Convert.ToInt64(dgvTaxItem[ColIndex.SaleLedgPk, i].Value);
                                mItemTaxInfo.ItemNo = Convert.ToInt64(dgvTaxItem[ColIndex.ItemNo, i].Value);
                                mItemTaxInfo.TaxLedgerNo = Convert.ToInt64(dtItemTaxSales.Rows[0].ItemArray[0].ToString());//Convert.ToInt64(dgvItemTax[17, j].Value)
                                mItemTaxInfo.SalesLedgerNo = Convert.ToInt64(dtItemTaxSales.Rows[0].ItemArray[1].ToString());
                                mItemTaxInfo.FromDate = DBGetVal.ServerTime;
                                mItemTaxInfo.Percentage = Convert.ToDouble(dtItemTaxSales.Rows[0].ItemArray[3].ToString());
                                mItemTaxInfo.CalculationMethod = dtItemTaxSales.Rows[0].ItemArray[2].ToString();
                                mItemTaxInfo.FKTaxSettingNo = ObjFunction.GetComboValue(cmbVatSales);
                                mItemTaxInfo.TaxTypeNo = Convert.ToInt64(dtItemTaxSales.Rows[0].ItemArray[4].ToString());
                                mItemTaxInfo.TransactionTypeNo = Convert.ToInt64(dtItemTaxSales.Rows[0].ItemArray[5].ToString());
                                mItemTaxInfo.HSNNo = ObjFunction.GetComboValue(cmbHSNCode);

                                mItemTaxInfo.IGSTPercent = Convert.ToDouble(dtItemTaxSales.Rows[0]["IGSTPercent"].ToString());
                                mItemTaxInfo.CGSTPercent = Convert.ToDouble(dtItemTaxSales.Rows[0]["CGSTPercent"].ToString());
                                mItemTaxInfo.SGSTPercent = Convert.ToDouble(dtItemTaxSales.Rows[0]["SGSTPercent"].ToString());
                                mItemTaxInfo.UTGSTPercent = Convert.ToDouble(dtItemTaxSales.Rows[0]["UTGSTPercent"].ToString());
                                mItemTaxInfo.CessPercent = Convert.ToDouble(dtItemTaxSales.Rows[0]["CessPercent"].ToString());

                                if (mItemTaxInfo.HSNNo > 0)
                                {
                                    mItemTaxInfo.HSNCode = cmbHSNCode.Text;
                                }
                                else
                                {
                                    mItemTaxInfo.HSNCode = txtHSNCode.Text;
                                }
                                mItemTaxInfo.IsActive = true;
                                mItemTaxInfo.UserID = DBGetVal.UserID;
                                mItemTaxInfo.UserDate = DBGetVal.ServerTime.Date;
                                dbMDutiesTaxesInfo.AddMItemTaxInfo1(mItemTaxInfo);
                            }
                        }

                        if (ObjFunction.GetComboValue(cmbVatPurchase) > 0)
                        {
                            DataTable dtItemTaxPur = ObjFunction.GetDataView("Select " +
                                " TaxLedgerNo,SalesLedgerNo,CalculationMethod,Percentage,TaxTypeNo,TransactionTypeNo, " +
                                " IGSTPercent,CGSTPercent,SGSTPercent,UTGSTPercent,CessPercent " +
                                " From MItemTaxSetting Where PkSrNo = " + ObjFunction.GetComboValue(cmbVatPurchase) + "").Table;
                            if (dtItemTaxPur.Rows.Count > 0)
                            {
                                mItemTaxInfo = new MItemTaxInfo();
                                flag = true;
                                mItemTaxInfo.PkSrNo = Convert.ToInt64(dgvTaxItem[ColIndex.PurLedgPk, i].Value);
                                mItemTaxInfo.ItemNo = Convert.ToInt64(dgvTaxItem[ColIndex.ItemNo, i].Value);
                                mItemTaxInfo.TaxLedgerNo = Convert.ToInt64(dtItemTaxPur.Rows[0].ItemArray[0].ToString());//Convert.ToInt64(dgvItemTax[17, j].Value)
                                mItemTaxInfo.SalesLedgerNo = Convert.ToInt64(dtItemTaxPur.Rows[0].ItemArray[1].ToString());
                                mItemTaxInfo.FromDate = DBGetVal.ServerTime;
                                mItemTaxInfo.Percentage = Convert.ToDouble(dtItemTaxPur.Rows[0].ItemArray[3].ToString());
                                mItemTaxInfo.CalculationMethod = dtItemTaxPur.Rows[0].ItemArray[2].ToString();
                                mItemTaxInfo.FKTaxSettingNo = ObjFunction.GetComboValue(cmbVatPurchase);
                                mItemTaxInfo.TaxTypeNo = Convert.ToInt64(dtItemTaxPur.Rows[0].ItemArray[4].ToString());
                                mItemTaxInfo.TransactionTypeNo = Convert.ToInt64(dtItemTaxPur.Rows[0].ItemArray[5].ToString());
                                mItemTaxInfo.HSNNo = ObjFunction.GetComboValue(cmbHSNCode);

                                mItemTaxInfo.IGSTPercent = Convert.ToDouble(dtItemTaxPur.Rows[0]["IGSTPercent"].ToString());
                                mItemTaxInfo.CGSTPercent = Convert.ToDouble(dtItemTaxPur.Rows[0]["CGSTPercent"].ToString());
                                mItemTaxInfo.SGSTPercent = Convert.ToDouble(dtItemTaxPur.Rows[0]["SGSTPercent"].ToString());
                                mItemTaxInfo.UTGSTPercent = Convert.ToDouble(dtItemTaxPur.Rows[0]["UTGSTPercent"].ToString());
                                mItemTaxInfo.CessPercent = Convert.ToDouble(dtItemTaxPur.Rows[0]["CessPercent"].ToString());

                                if (mItemTaxInfo.HSNNo > 0)
                                {
                                    mItemTaxInfo.HSNCode = cmbHSNCode.Text;
                                }
                                else
                                {
                                    mItemTaxInfo.HSNCode = txtHSNCode.Text;
                                }
                                mItemTaxInfo.IsActive = true;
                                mItemTaxInfo.UserID = DBGetVal.UserID;
                                mItemTaxInfo.UserDate = DBGetVal.ServerTime.Date;
                                dbMDutiesTaxesInfo.AddMItemTaxInfo1(mItemTaxInfo);
                            }
                        }
                        if (dbMDutiesTaxesInfo.ExecuteNonQueryStatements() == true && flag == true)
                        {
                            SFlag = true;
                        }
                    }
                }
                if (SFlag == true)
                {
                    OMMessageBox.Show("Tax Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                    rdBoth.Checked = true;
                    ClearFields();
                    cmbCategoryName.Enabled = true;
                    cmbDepartmentName.Enabled = true;
                }
                else
                {
                    if (flag == false)
                        OMMessageBox.Show("Select Atleast One Checkbox", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    else
                        OMMessageBox.Show("Tax not saved", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);//For Common Error Displayed Purpose
            }
        }

        public static class ColIndex
        {
            public static int Select = 0;
            public static int SrNo = 1;
            public static int Barcode = 2;
            public static int ItemName = 3;
            public static int SVat = 4;
            public static int PVat = 5;
            public static int SaleLedgPk = 6;
            public static int PurLedgPk = 7;
            public static int ItemNo = 8;

        }

        private void txtSelectItem_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void rd_Click(object sender, EventArgs e)
        {
            RadioButton rd = (RadioButton)sender;
            if (rd.Checked == true)
            {
                if (rd.Name == "rdBoth")
                {
                    BindGrid1(0);
                }
                else if (rd.Name == "rdAssigned")
                {
                    BindGrid1(1);
                }
                else if (rd.Name == "rdNotAssigned")
                {
                    BindGrid1(2);
                }
            }
        }

        #region KeyDown Events
        public void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is GroupBox)
                    KeyDownFormat(ctrl.Controls);
            }
        }

        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                chkSelectAll.Checked = !chkSelectAll.Checked;
            }
        }
        #endregion

        public void Validation()
        {
            bool flag = false;
            if (ObjFunction.GetComboValue(cmbGroupNo2) > 0)
            {
                flag = true;
            }
            else if (ObjFunction.GetComboValue(cmbCategoryName) > 0)
            {
                flag = true;
            }
            else if (ObjFunction.GetComboValue(cmbDepartmentName) > 0)
            {
                flag = true;
            }

            if (flag == false)
            {
                DisplayMessage("Please Select atleast one group....");
                while (dgvTaxItem.Rows.Count > 0)
                    dgvTaxItem.Rows.RemoveAt(0);
            }
            else
            {
                BindGrid1(0);
            }
        }

        public void DisplayMessage(string str)
        {
            lblMsg.Visible = true;
            lblMsg.Text = str;
            Application.DoEvents();
            System.Threading.Thread.Sleep(700);
            lblMsg.Visible = false;
        }

        private void txtBarcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                EP.SetError(cmbGroupNo2, "");
                EP.SetError(cmbCategoryName, "");
                EP.SetError(cmbDepartmentName, "");
                EP.SetError(txtBarcode, "");
                e.SuppressKeyPress = true;
                if (txtBarcode.Text.Trim() != "")
                {
                    BindGrid1(0);
                    if (dgvTaxItem.Rows.Count > 0)
                    {
                        dgvTaxItem.CurrentCell = dgvTaxItem[0, 0];
                        dgvTaxItem.Focus();
                    }
                }
                else
                {
                    if (cmbDepartmentName.Enabled == true)
                        cmbDepartmentName.Focus();
                    else
                    {
                        if (dgvTaxItem.Rows.Count > 0)
                        {
                            dgvTaxItem.CurrentCell = dgvTaxItem[0, 0];
                            dgvTaxItem.Focus();
                        }
                    }
                }
            }
        }

        private void dgvTaxItem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                cmbVatSales.Focus();
            }
        }

        private void cmbVatSales_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (ObjFunction.GetComboValue(cmbVatSales) > 0)
                    btnSetTax.Focus();
                else
                    cmbVatSales.Focus();
            }
        }

        private void cmbVatPurchase_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (ObjFunction.GetComboValue(cmbVatPurchase) > 0)
                    btnSetTax.Focus();
                else
                    cmbVatPurchase.Focus();
            }
        }

        private void cmbDepartmentName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                cmbCategoryName.Focus();
            }
        }

        private void cmbGroupNo2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (ObjFunction.GetComboValue(cmbGroupNo2) > 0)
                {
                    dgvTaxItem.Focus();
                }
                else
                {
                    txtBarcode.Focus();
                }
            }
        }

        public void DisplayAll(bool flag)
        {
            cmbCategoryName.Enabled = flag;
            cmbDepartmentName.Enabled = flag;
            cmbCategoryName.SelectedIndex = 0;
            cmbDepartmentName.SelectedIndex = 0;
            txtBarcode.Text = "";
        }

        private void cmbCategoryName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                Validation();
                if (dgvTaxItem.Rows.Count > 0)
                {
                    dgvTaxItem.CurrentCell = dgvTaxItem[0, 0];
                    dgvTaxItem.Focus();
                }
            }
        }

        private void cmbGroupNo2_Leave(object sender, EventArgs e)
        {
            if (ObjFunction.GetComboValue(cmbGroupNo2) > 0)
            {
                DisplayAll(false);
                BindGrid1(0);
            }
            else
            {
                DisplayAll(true);
                ObjFunction.FillCombo(cmbDepartmentName, "SELECT Distinct StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=4 order by StockGroupName");
                ObjFunction.FillCombo(cmbCategoryName, "SELECT Distinct StockGroupNo,StockGroupName From MStockGroup WHERE IsActive = 'True' AND ControlGroup=2 order By StockGroupName");
            }
        }

        private void cmbDepartmentName_Leave(object sender, EventArgs e)
        {
            if (cmbDepartmentName.SelectedIndex != 0)
            {
                ObjFunction.FillCombo(cmbCategoryName, "SELECT DISTINCT StockGroupNo, StockGroupName FROM   MStockGroup WHERE   (ControlGroup = 2) AND (ControlSubGroup = " + ObjFunction.GetComboValue(cmbDepartmentName) + ") AND (IsActive = 'true')  ORDER BY StockGroupName");
                BindGrid1(0);
                dgvTaxItem.Focus();
            }
            else
                ObjFunction.FillCombo(cmbCategoryName, "SELECT DISTINCT StockGroupNo, StockGroupName FROM   MStockGroup WHERE   (ControlGroup = 2) AND  (IsActive = 'true')  ORDER BY StockGroupName");
        }

        private void cmbCategoryName_Leave(object sender, EventArgs e)
        {
            Validation();
        }

        private void btnSalesVat_Click(object sender, EventArgs e)
        {
            try
            {
                long SalesVatNo = 0;
                Form NewF = new Master.PercentageVAT(GroupType.SalesAccount, GroupType.GST);
                ObjFunction.OpenForm(NewF);
                if (((Master.PercentageVAT)NewF).DS == DialogResult.OK)
                {
                    SalesVatNo = ObjFunction.GetComboValue(cmbVatSales);
                    ObjFunction.FillCombo(cmbVatSales, "SELECT MItemTaxSetting.PkSrNo, (cast(MItemTaxSetting.Percentage as varchar)+ ' %') as Percentage " +
                        " FROM MItemTaxSetting " +
                        " WHERE     (MItemTaxSetting.TransactionTypeNo = " + GroupType.SalesAccount + ") AND  (MItemTaxSetting.TaxTypeNo = " + GroupType.GST + ") And MItemTaxSetting.IsActive='True'  Order by  MItemTaxSetting.Percentage ");
                    cmbVatSales.SelectedValue = SalesVatNo;
                }
                cmbVatSales.Focus();

            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);;//For Common Error Displayed Purpose
            }
        }

        private void btnPurVat_Click(object sender, EventArgs e)
        {
            try
            {
                long PurVatNo = 0;
                Form NewF = new Master.PercentageVAT(GroupType.PurchaseAccount, GroupType.GST);
                ObjFunction.OpenForm(NewF);
                if (((Master.PercentageVAT)NewF).DS == DialogResult.OK)
                {
                    PurVatNo = ObjFunction.GetComboValue(cmbVatPurchase);
                    ObjFunction.FillCombo(cmbVatPurchase, "SELECT MItemTaxSetting.PkSrNo, (cast(MItemTaxSetting.Percentage as varchar)+ ' %') as Percentage " +
                        " FROM MItemTaxSetting " +
                       " WHERE (MItemTaxSetting.TransactionTypeNo = " + GroupType.PurchaseAccount + ") AND  (MItemTaxSetting.TaxTypeNo = " + GroupType.GST + ") And MItemTaxSetting.IsActive='True' Order by  MItemTaxSetting.Percentage ");
                    cmbVatPurchase.SelectedValue = PurVatNo;
                }
                cmbVatPurchase.Focus();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message); //For Common Error Displayed Purpose
            }

        }

        bool isHSNValueChanged = false;

        private void cmbHSNCode_SelectedValueChanged(object sender, EventArgs e)
        {
            isHSNValueChanged = true;
            string sql = "Select TOP 1 GTD.FkTaxSettingNo " +
                        " From MStockGSTTaxDetails As GTD " +
                        " INNER JOIN MItemTaxSetting As MTS ON MTS.PkSrNo = GTD.FkTaxSettingNo " +
                                " AND MTS.TransactionTypeNo = " + GroupType.SalesAccount + " " +
                        " WHERE GTD.HSNNo = " + ObjFunction.GetComboValue(cmbHSNCode) + " " +
                                " AND GTD.IsActive = 'True' " +
                        " ORDER BY GTD.FromDate";

            long GstTaxDetailNo = ObjQry.ReturnLong(sql, CommonFunctions.ConStr);

            if (GstTaxDetailNo > 0)
            {
                cmbVatSales.SelectedValue = GstTaxDetailNo;
                sql = "Select TOP 1 GTD.FkTaxSettingNo " +
                            " From MStockGSTTaxDetails As GTD " +
                            " INNER JOIN MItemTaxSetting As MTS ON MTS.PkSrNo = GTD.FkTaxSettingNo " +
                                    " AND MTS.TransactionTypeNo = " + GroupType.PurchaseAccount + " " +
                            " WHERE GTD.HSNNo = " + ObjFunction.GetComboValue(cmbHSNCode) + " " +
                                    " AND GTD.IsActive = 'True' " +
                            " ORDER BY GTD.FromDate";

                GstTaxDetailNo = ObjQry.ReturnLong(sql, CommonFunctions.ConStr);
                if (GstTaxDetailNo > 0)
                {
                    cmbVatPurchase.SelectedValue = GstTaxDetailNo;
                }
                else
                {
                    throw new Exception("GST Tax details for purchase not found !!!");
                }
            }
            else
            {
                cmbVatSales.Enabled = true;
            }
            isHSNValueChanged = false;
        }

        private void cmbVatSales_SelectedValueChanged(object sender, EventArgs e)
        {
            if (!isHSNValueChanged && cmbVatPurchase.Items.Count > cmbVatSales.SelectedIndex)
            {
                cmbVatPurchase.SelectedIndex = cmbVatSales.SelectedIndex;
            }
        }

        private void txtHSNCode_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(((TextBox)sender), -1, 8, JitFunctions.MaskedType.NotNegative);
            if (txtHSNCode.TextLength >= 8)
            {
                cmbHSNCode.SelectedIndex = cmbHSNCode.FindStringExact(txtHSNCode.Text);
            }
            else
            {
                if (cmbHSNCode.SelectedIndex != -1)
                {
                    cmbHSNCode.SelectedIndex = 0;
                }
            }
        }

        private void BtnAddGST_Click(object sender, EventArgs e)
        {
            Master.PercentageGST PGST = new Kirana.Master.PercentageGST();
            DialogResult dr = ObjFunction.OpenDialog(PGST, this);
            if (dr == DialogResult.OK)
            {
                long iselected = 0;

                iselected = ObjFunction.GetComboValue(cmbVatSales);
                ObjFunction.FillCombo(cmbVatSales, "SELECT MItemTaxSetting.PkSrNo, " +
                        " (cast(MItemTaxSetting.IGSTPercent as varchar)+ ' / ')+" +
                        " (cast(MItemTaxSetting.CGSTPercent as varchar)+ ' / ')+" +
                        " (cast(MItemTaxSetting.SGSTPercent as varchar)+ ' / ')+" +
                        " (cast(MItemTaxSetting.UTGSTPercent as varchar)+ ' / ')+" +
                        " (cast(MItemTaxSetting.CessPercent as varchar))" +
                          " as Percentage " +
                    " FROM MItemTaxSetting " +
                    " WHERE (MItemTaxSetting.TransactionTypeNo = " + GroupType.SalesAccount + ") " +
                    " AND (MItemTaxSetting.TaxTypeNo = " + GroupType.GST + " AND MItemTaxSetting.IsActive='True') " +
                    " Order by  MItemTaxSetting.IGSTPercent,MItemTaxSetting.CessPercent ");
                cmbVatSales.SelectedValue = iselected;
                cmbVatSales.DroppedDown = true;
            }
        }
    }
}
