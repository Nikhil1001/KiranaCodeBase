﻿namespace Kirana.Vouchers
{
    partial class GetSchemeInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnOk = new System.Windows.Forms.Button();
            this.pnlMain = new JitControls.OMBPanel();
            this.WBScheme = new System.Windows.Forms.WebBrowser();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(384, 463);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 37);
            this.BtnOk.TabIndex = 0;
            this.BtnOk.Text = "Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // pnlMain
            // 
            this.pnlMain.BorderColor = System.Drawing.Color.Gray;
            this.pnlMain.BorderRadius = 3;
            this.pnlMain.Controls.Add(this.WBScheme);
            this.pnlMain.Controls.Add(this.BtnOk);
            this.pnlMain.Location = new System.Drawing.Point(2, 3);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(866, 505);
            this.pnlMain.TabIndex = 101;
            // 
            // WBScheme
            // 
            this.WBScheme.Location = new System.Drawing.Point(6, 6);
            this.WBScheme.MinimumSize = new System.Drawing.Size(20, 20);
            this.WBScheme.Name = "WBScheme";
            this.WBScheme.Size = new System.Drawing.Size(857, 451);
            this.WBScheme.TabIndex = 9;
            // 
            // GetSchemeInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 512);
            this.Controls.Add(this.pnlMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GetSchemeInfo";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Scheme";
            this.Load += new System.EventHandler(this.Scheme_Load);
            this.pnlMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BtnOk;
        private JitControls.OMBPanel pnlMain;
        private System.Windows.Forms.WebBrowser WBScheme;
    }
}