﻿namespace Kirana.Settings
{
    partial class OtherSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnSave = new System.Windows.Forms.Button();
            this.BtnClose = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.chkMultiRates = new System.Windows.Forms.CheckBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtUpDownLink = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.chkBillWithMRP = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdGodex = new System.Windows.Forms.RadioButton();
            this.rdTSC = new System.Windows.Forms.RadioButton();
            this.chkIsException = new System.Windows.Forms.CheckBox();
            this.label19 = new System.Windows.Forms.Label();
            this.btnPath = new System.Windows.Forms.Button();
            this.txtBackUpPath = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtUpDownDays = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtUpDownLoadPath = new System.Windows.Forms.TextBox();
            this.UpDownLoadPath = new System.Windows.Forms.Label();
            this.cmbDefaultBillPrint = new System.Windows.Forms.ComboBox();
            this.lblDefaultBillPrint = new System.Windows.Forms.Label();
            this.cmbLanguage = new System.Windows.Forms.ComboBox();
            this.lblLanguage = new System.Windows.Forms.Label();
            this.chkBilingual = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.ChkShowLastBill = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.chkIsBrand = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtTopSales = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.chkRptExcel = new System.Windows.Forms.CheckBox();
            this.lblRptExcel = new System.Windows.Forms.Label();
            this.chkStockItemPrintBarCode = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.ChkReportDisplay = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.chkTaxTypeGrid = new System.Windows.Forms.CheckBox();
            this.chkESupermode = new System.Windows.Forms.CheckBox();
            this.chkDSupermode = new System.Windows.Forms.CheckBox();
            this.chkCSupermode = new System.Windows.Forms.CheckBox();
            this.chkBSupermode = new System.Windows.Forms.CheckBox();
            this.chkASupermode = new System.Windows.Forms.CheckBox();
            this.chkRate5DBEffect = new System.Windows.Forms.CheckBox();
            this.chkRate4DBEffect = new System.Windows.Forms.CheckBox();
            this.chkRate3DBEffect = new System.Windows.Forms.CheckBox();
            this.chkRate2DBEffect = new System.Windows.Forms.CheckBox();
            this.chkRate1DBEffect = new System.Windows.Forms.CheckBox();
            this.txtRate5Password = new System.Windows.Forms.TextBox();
            this.txtRate4Password = new System.Windows.Forms.TextBox();
            this.txtRate3Password = new System.Windows.Forms.TextBox();
            this.txtRate2Password = new System.Windows.Forms.TextBox();
            this.txtRate1Password = new System.Windows.Forms.TextBox();
            this.ChkStockLocation = new System.Windows.Forms.CheckBox();
            this.ChkBarCodeDis = new System.Windows.Forms.CheckBox();
            this.chkCategoryDis = new System.Windows.Forms.CheckBox();
            this.ChkDepartmentDis = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtLabel5 = new System.Windows.Forms.TextBox();
            this.chkRate5 = new System.Windows.Forms.CheckBox();
            this.txtLabel4 = new System.Windows.Forms.TextBox();
            this.chkRate4 = new System.Windows.Forms.CheckBox();
            this.txtLabel3 = new System.Windows.Forms.TextBox();
            this.chkRate3 = new System.Windows.Forms.CheckBox();
            this.txtLabel2 = new System.Windows.Forms.TextBox();
            this.chkRate2 = new System.Windows.Forms.CheckBox();
            this.txtLabel1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.chkRate1 = new System.Windows.Forms.CheckBox();
            this.EP = new System.Windows.Forms.ErrorProvider(this.components);
            this.chkAddvanceSearch = new System.Windows.Forms.CheckBox();
            this.label46 = new System.Windows.Forms.Label();
            this.panel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EP)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(14, 532);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(80, 60);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // BtnClose
            // 
            this.BtnClose.Location = new System.Drawing.Point(103, 533);
            this.BtnClose.Name = "BtnClose";
            this.BtnClose.Size = new System.Drawing.Size(80, 60);
            this.BtnClose.TabIndex = 13;
            this.BtnClose.Text = "E&xit";
            this.BtnClose.UseVisualStyleBackColor = true;
            this.BtnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.chkAddvanceSearch);
            this.panel3.Controls.Add(this.label46);
            this.panel3.Controls.Add(this.chkMultiRates);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Controls.Add(this.txtUpDownLink);
            this.panel3.Controls.Add(this.label21);
            this.panel3.Controls.Add(this.chkBillWithMRP);
            this.panel3.Controls.Add(this.label20);
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Controls.Add(this.chkIsException);
            this.panel3.Controls.Add(this.label19);
            this.panel3.Controls.Add(this.btnPath);
            this.panel3.Controls.Add(this.txtBackUpPath);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.txtUpDownDays);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.btnBrowse);
            this.panel3.Controls.Add(this.txtUpDownLoadPath);
            this.panel3.Controls.Add(this.UpDownLoadPath);
            this.panel3.Controls.Add(this.cmbDefaultBillPrint);
            this.panel3.Controls.Add(this.lblDefaultBillPrint);
            this.panel3.Controls.Add(this.cmbLanguage);
            this.panel3.Controls.Add(this.lblLanguage);
            this.panel3.Controls.Add(this.chkBilingual);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.ChkShowLastBill);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.chkIsBrand);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.txtTopSales);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.chkRptExcel);
            this.panel3.Controls.Add(this.lblRptExcel);
            this.panel3.Controls.Add(this.chkStockItemPrintBarCode);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.ChkReportDisplay);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.chkTaxTypeGrid);
            this.panel3.Controls.Add(this.chkESupermode);
            this.panel3.Controls.Add(this.chkDSupermode);
            this.panel3.Controls.Add(this.chkCSupermode);
            this.panel3.Controls.Add(this.chkBSupermode);
            this.panel3.Controls.Add(this.chkASupermode);
            this.panel3.Controls.Add(this.chkRate5DBEffect);
            this.panel3.Controls.Add(this.chkRate4DBEffect);
            this.panel3.Controls.Add(this.chkRate3DBEffect);
            this.panel3.Controls.Add(this.chkRate2DBEffect);
            this.panel3.Controls.Add(this.chkRate1DBEffect);
            this.panel3.Controls.Add(this.txtRate5Password);
            this.panel3.Controls.Add(this.txtRate4Password);
            this.panel3.Controls.Add(this.txtRate3Password);
            this.panel3.Controls.Add(this.txtRate2Password);
            this.panel3.Controls.Add(this.txtRate1Password);
            this.panel3.Controls.Add(this.ChkStockLocation);
            this.panel3.Controls.Add(this.ChkBarCodeDis);
            this.panel3.Controls.Add(this.chkCategoryDis);
            this.panel3.Controls.Add(this.ChkDepartmentDis);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.txtLabel5);
            this.panel3.Controls.Add(this.chkRate5);
            this.panel3.Controls.Add(this.txtLabel4);
            this.panel3.Controls.Add(this.chkRate4);
            this.panel3.Controls.Add(this.txtLabel3);
            this.panel3.Controls.Add(this.chkRate3);
            this.panel3.Controls.Add(this.txtLabel2);
            this.panel3.Controls.Add(this.chkRate2);
            this.panel3.Controls.Add(this.txtLabel1);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.chkRate1);
            this.panel3.Location = new System.Drawing.Point(14, 12);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(748, 512);
            this.panel3.TabIndex = 526;
            // 
            // chkMultiRates
            // 
            this.chkMultiRates.AutoSize = true;
            this.chkMultiRates.Location = new System.Drawing.Point(700, 329);
            this.chkMultiRates.Name = "chkMultiRates";
            this.chkMultiRates.Size = new System.Drawing.Size(40, 17);
            this.chkMultiRates.TabIndex = 595;
            this.chkMultiRates.Text = "No";
            this.chkMultiRates.UseVisualStyleBackColor = true;
            this.chkMultiRates.CheckedChanged += new System.EventHandler(this.chkMultiRates_CheckedChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(556, 331);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(60, 13);
            this.label22.TabIndex = 594;
            this.label22.Text = "Multi Rates";
            // 
            // txtUpDownLink
            // 
            this.txtUpDownLink.Location = new System.Drawing.Point(157, 480);
            this.txtUpDownLink.Name = "txtUpDownLink";
            this.txtUpDownLink.Size = new System.Drawing.Size(264, 20);
            this.txtUpDownLink.TabIndex = 593;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(16, 482);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(78, 13);
            this.label21.TabIndex = 592;
            this.label21.Text = "Up Down Link:";
            // 
            // chkBillWithMRP
            // 
            this.chkBillWithMRP.AutoSize = true;
            this.chkBillWithMRP.Location = new System.Drawing.Point(157, 201);
            this.chkBillWithMRP.Name = "chkBillWithMRP";
            this.chkBillWithMRP.Size = new System.Drawing.Size(40, 17);
            this.chkBillWithMRP.TabIndex = 591;
            this.chkBillWithMRP.Text = "No";
            this.chkBillWithMRP.UseVisualStyleBackColor = true;
            this.chkBillWithMRP.CheckedChanged += new System.EventHandler(this.chkBillWithMRP_CheckedChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(13, 201);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(78, 13);
            this.label20.TabIndex = 590;
            this.label20.Text = "Bill With MRP :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdGodex);
            this.groupBox1.Controls.Add(this.rdTSC);
            this.groupBox1.Location = new System.Drawing.Point(559, 416);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(161, 54);
            this.groupBox1.TabIndex = 589;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Printer Type";
            // 
            // rdGodex
            // 
            this.rdGodex.AutoSize = true;
            this.rdGodex.Location = new System.Drawing.Point(84, 19);
            this.rdGodex.Name = "rdGodex";
            this.rdGodex.Size = new System.Drawing.Size(56, 17);
            this.rdGodex.TabIndex = 1;
            this.rdGodex.TabStop = true;
            this.rdGodex.Text = "Godex";
            this.rdGodex.UseVisualStyleBackColor = true;
            // 
            // rdTSC
            // 
            this.rdTSC.AutoSize = true;
            this.rdTSC.Checked = true;
            this.rdTSC.Location = new System.Drawing.Point(20, 19);
            this.rdTSC.Name = "rdTSC";
            this.rdTSC.Size = new System.Drawing.Size(46, 17);
            this.rdTSC.TabIndex = 0;
            this.rdTSC.TabStop = true;
            this.rdTSC.Text = "TSC";
            this.rdTSC.UseVisualStyleBackColor = true;
            // 
            // chkIsException
            // 
            this.chkIsException.AutoSize = true;
            this.chkIsException.Location = new System.Drawing.Point(700, 219);
            this.chkIsException.Name = "chkIsException";
            this.chkIsException.Size = new System.Drawing.Size(40, 17);
            this.chkIsException.TabIndex = 588;
            this.chkIsException.Text = "No";
            this.chkIsException.UseVisualStyleBackColor = true;
            this.chkIsException.CheckedChanged += new System.EventHandler(this.chkIsException_CheckedChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(548, 219);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(91, 13);
            this.label19.TabIndex = 587;
            this.label19.Text = "Exception Display";
            // 
            // btnPath
            // 
            this.btnPath.Location = new System.Drawing.Point(448, 450);
            this.btnPath.Name = "btnPath";
            this.btnPath.Size = new System.Drawing.Size(75, 23);
            this.btnPath.TabIndex = 586;
            this.btnPath.Text = "Browse";
            this.btnPath.UseVisualStyleBackColor = true;
            this.btnPath.Click += new System.EventHandler(this.btnPath_Click);
            // 
            // txtBackUpPath
            // 
            this.txtBackUpPath.Location = new System.Drawing.Point(157, 452);
            this.txtBackUpPath.Name = "txtBackUpPath";
            this.txtBackUpPath.Size = new System.Drawing.Size(264, 20);
            this.txtBackUpPath.TabIndex = 585;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(16, 457);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(80, 13);
            this.label18.TabIndex = 584;
            this.label18.Text = "Back Up Path :";
            // 
            // txtUpDownDays
            // 
            this.txtUpDownDays.Location = new System.Drawing.Point(157, 420);
            this.txtUpDownDays.MaxLength = 3;
            this.txtUpDownDays.Name = "txtUpDownDays";
            this.txtUpDownDays.Size = new System.Drawing.Size(97, 20);
            this.txtUpDownDays.TabIndex = 583;
            this.txtUpDownDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtUpDownDays.TextChanged += new System.EventHandler(this.txtUpDownDays_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(15, 425);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(85, 13);
            this.label17.TabIndex = 582;
            this.label17.Text = "Up Down Days :";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(448, 389);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 581;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtUpDownLoadPath
            // 
            this.txtUpDownLoadPath.Location = new System.Drawing.Point(157, 391);
            this.txtUpDownLoadPath.Name = "txtUpDownLoadPath";
            this.txtUpDownLoadPath.Size = new System.Drawing.Size(264, 20);
            this.txtUpDownLoadPath.TabIndex = 580;
            // 
            // UpDownLoadPath
            // 
            this.UpDownLoadPath.AutoSize = true;
            this.UpDownLoadPath.Location = new System.Drawing.Point(14, 394);
            this.UpDownLoadPath.Name = "UpDownLoadPath";
            this.UpDownLoadPath.Size = new System.Drawing.Size(110, 13);
            this.UpDownLoadPath.TabIndex = 579;
            this.UpDownLoadPath.Text = "Up Down Load Path :";
            // 
            // cmbDefaultBillPrint
            // 
            this.cmbDefaultBillPrint.FormattingEnabled = true;
            this.cmbDefaultBillPrint.Items.AddRange(new object[] {
            "English",
            "Marathi",
            "Hindi"});
            this.cmbDefaultBillPrint.Location = new System.Drawing.Point(157, 362);
            this.cmbDefaultBillPrint.Name = "cmbDefaultBillPrint";
            this.cmbDefaultBillPrint.Size = new System.Drawing.Size(121, 21);
            this.cmbDefaultBillPrint.TabIndex = 578;
            // 
            // lblDefaultBillPrint
            // 
            this.lblDefaultBillPrint.AutoSize = true;
            this.lblDefaultBillPrint.Location = new System.Drawing.Point(13, 365);
            this.lblDefaultBillPrint.Name = "lblDefaultBillPrint";
            this.lblDefaultBillPrint.Size = new System.Drawing.Size(87, 13);
            this.lblDefaultBillPrint.TabIndex = 577;
            this.lblDefaultBillPrint.Text = "Default Bill Print :";
            // 
            // cmbLanguage
            // 
            this.cmbLanguage.FormattingEnabled = true;
            this.cmbLanguage.Items.AddRange(new object[] {
            "Marathi",
            "Hindi"});
            this.cmbLanguage.Location = new System.Drawing.Point(300, 331);
            this.cmbLanguage.Name = "cmbLanguage";
            this.cmbLanguage.Size = new System.Drawing.Size(121, 21);
            this.cmbLanguage.TabIndex = 576;
            this.cmbLanguage.Leave += new System.EventHandler(this.cmbLanguage_Leave);
            this.cmbLanguage.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbLanguage_KeyDown);
            // 
            // lblLanguage
            // 
            this.lblLanguage.AutoSize = true;
            this.lblLanguage.Location = new System.Drawing.Point(225, 333);
            this.lblLanguage.Name = "lblLanguage";
            this.lblLanguage.Size = new System.Drawing.Size(61, 13);
            this.lblLanguage.TabIndex = 575;
            this.lblLanguage.Text = "Language :";
            // 
            // chkBilingual
            // 
            this.chkBilingual.AutoSize = true;
            this.chkBilingual.Location = new System.Drawing.Point(157, 333);
            this.chkBilingual.Name = "chkBilingual";
            this.chkBilingual.Size = new System.Drawing.Size(40, 17);
            this.chkBilingual.TabIndex = 574;
            this.chkBilingual.Text = "No";
            this.chkBilingual.UseVisualStyleBackColor = true;
            this.chkBilingual.CheckedChanged += new System.EventHandler(this.chkBilingual_CheckedChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(13, 335);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(52, 13);
            this.label16.TabIndex = 573;
            this.label16.Text = "Bilingual :";
            // 
            // ChkShowLastBill
            // 
            this.ChkShowLastBill.AutoSize = true;
            this.ChkShowLastBill.Location = new System.Drawing.Point(157, 305);
            this.ChkShowLastBill.Name = "ChkShowLastBill";
            this.ChkShowLastBill.Size = new System.Drawing.Size(40, 17);
            this.ChkShowLastBill.TabIndex = 572;
            this.ChkShowLastBill.Text = "No";
            this.ChkShowLastBill.UseVisualStyleBackColor = true;
            this.ChkShowLastBill.CheckedChanged += new System.EventHandler(this.ChkShowLastBill_CheckedChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(13, 305);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(79, 13);
            this.label15.TabIndex = 571;
            this.label15.Text = "Show Last Bill :";
            // 
            // chkIsBrand
            // 
            this.chkIsBrand.AutoSize = true;
            this.chkIsBrand.Location = new System.Drawing.Point(157, 276);
            this.chkIsBrand.Name = "chkIsBrand";
            this.chkIsBrand.Size = new System.Drawing.Size(40, 17);
            this.chkIsBrand.TabIndex = 570;
            this.chkIsBrand.Text = "No";
            this.chkIsBrand.UseVisualStyleBackColor = true;
            this.chkIsBrand.CheckedChanged += new System.EventHandler(this.chkIsBrand_CheckedChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(13, 278);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(86, 13);
            this.label14.TabIndex = 569;
            this.label14.Text = "IsBrand Filtered :";
            // 
            // txtTopSales
            // 
            this.txtTopSales.Location = new System.Drawing.Point(156, 171);
            this.txtTopSales.Name = "txtTopSales";
            this.txtTopSales.Size = new System.Drawing.Size(144, 20);
            this.txtTopSales.TabIndex = 568;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(13, 176);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(91, 13);
            this.label13.TabIndex = 567;
            this.label13.Text = "Top Sales Value :";
            // 
            // chkRptExcel
            // 
            this.chkRptExcel.AutoSize = true;
            this.chkRptExcel.Location = new System.Drawing.Point(500, 306);
            this.chkRptExcel.Name = "chkRptExcel";
            this.chkRptExcel.Size = new System.Drawing.Size(40, 17);
            this.chkRptExcel.TabIndex = 566;
            this.chkRptExcel.Text = "No";
            this.chkRptExcel.UseVisualStyleBackColor = true;
            this.chkRptExcel.CheckedChanged += new System.EventHandler(this.chkRptExcel_CheckedChanged);
            // 
            // lblRptExcel
            // 
            this.lblRptExcel.AutoSize = true;
            this.lblRptExcel.Location = new System.Drawing.Point(228, 305);
            this.lblRptExcel.Name = "lblRptExcel";
            this.lblRptExcel.Size = new System.Drawing.Size(109, 13);
            this.lblRptExcel.TabIndex = 565;
            this.lblRptExcel.Text = "Report Excel Format :";
            // 
            // chkStockItemPrintBarCode
            // 
            this.chkStockItemPrintBarCode.AccessibleName = "y";
            this.chkStockItemPrintBarCode.AutoSize = true;
            this.chkStockItemPrintBarCode.Location = new System.Drawing.Point(500, 274);
            this.chkStockItemPrintBarCode.Name = "chkStockItemPrintBarCode";
            this.chkStockItemPrintBarCode.Size = new System.Drawing.Size(40, 17);
            this.chkStockItemPrintBarCode.TabIndex = 564;
            this.chkStockItemPrintBarCode.Text = "No";
            this.chkStockItemPrintBarCode.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(225, 278);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(132, 13);
            this.label12.TabIndex = 563;
            this.label12.Text = "Stock Item Print BarCode :";
            // 
            // ChkReportDisplay
            // 
            this.ChkReportDisplay.AutoSize = true;
            this.ChkReportDisplay.Location = new System.Drawing.Point(500, 245);
            this.ChkReportDisplay.Name = "ChkReportDisplay";
            this.ChkReportDisplay.Size = new System.Drawing.Size(40, 17);
            this.ChkReportDisplay.TabIndex = 562;
            this.ChkReportDisplay.Text = "No";
            this.ChkReportDisplay.UseVisualStyleBackColor = true;
            this.ChkReportDisplay.CheckedChanged += new System.EventHandler(this.ChkReportDisplay_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(225, 249);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(148, 13);
            this.label9.TabIndex = 561;
            this.label9.Text = "Report Display From System : ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(225, 223);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(196, 13);
            this.label8.TabIndex = 560;
            this.label8.Text = "Is TaxType Grid Display On Stock Item :";
            // 
            // chkTaxTypeGrid
            // 
            this.chkTaxTypeGrid.AutoSize = true;
            this.chkTaxTypeGrid.Location = new System.Drawing.Point(500, 218);
            this.chkTaxTypeGrid.Name = "chkTaxTypeGrid";
            this.chkTaxTypeGrid.Size = new System.Drawing.Size(40, 17);
            this.chkTaxTypeGrid.TabIndex = 559;
            this.chkTaxTypeGrid.Text = "No";
            this.chkTaxTypeGrid.UseVisualStyleBackColor = true;
            this.chkTaxTypeGrid.CheckedChanged += new System.EventHandler(this.chkTaxTypeGrid_CheckedChanged);
            // 
            // chkESupermode
            // 
            this.chkESupermode.AutoSize = true;
            this.chkESupermode.Location = new System.Drawing.Point(559, 142);
            this.chkESupermode.Name = "chkESupermode";
            this.chkESupermode.Size = new System.Drawing.Size(80, 17);
            this.chkESupermode.TabIndex = 558;
            this.chkESupermode.Text = "Supermode";
            this.chkESupermode.UseVisualStyleBackColor = true;
            // 
            // chkDSupermode
            // 
            this.chkDSupermode.AutoSize = true;
            this.chkDSupermode.Location = new System.Drawing.Point(559, 113);
            this.chkDSupermode.Name = "chkDSupermode";
            this.chkDSupermode.Size = new System.Drawing.Size(80, 17);
            this.chkDSupermode.TabIndex = 557;
            this.chkDSupermode.Text = "Supermode";
            this.chkDSupermode.UseVisualStyleBackColor = true;
            // 
            // chkCSupermode
            // 
            this.chkCSupermode.AutoSize = true;
            this.chkCSupermode.Location = new System.Drawing.Point(559, 75);
            this.chkCSupermode.Name = "chkCSupermode";
            this.chkCSupermode.Size = new System.Drawing.Size(80, 17);
            this.chkCSupermode.TabIndex = 556;
            this.chkCSupermode.Text = "Supermode";
            this.chkCSupermode.UseVisualStyleBackColor = true;
            // 
            // chkBSupermode
            // 
            this.chkBSupermode.AutoSize = true;
            this.chkBSupermode.Location = new System.Drawing.Point(559, 43);
            this.chkBSupermode.Name = "chkBSupermode";
            this.chkBSupermode.Size = new System.Drawing.Size(80, 17);
            this.chkBSupermode.TabIndex = 555;
            this.chkBSupermode.Text = "Supermode";
            this.chkBSupermode.UseVisualStyleBackColor = true;
            // 
            // chkASupermode
            // 
            this.chkASupermode.AutoSize = true;
            this.chkASupermode.Location = new System.Drawing.Point(559, 9);
            this.chkASupermode.Name = "chkASupermode";
            this.chkASupermode.Size = new System.Drawing.Size(80, 17);
            this.chkASupermode.TabIndex = 554;
            this.chkASupermode.Text = "Supermode";
            this.chkASupermode.UseVisualStyleBackColor = true;
            // 
            // chkRate5DBEffect
            // 
            this.chkRate5DBEffect.AutoSize = true;
            this.chkRate5DBEffect.Location = new System.Drawing.Point(480, 142);
            this.chkRate5DBEffect.Name = "chkRate5DBEffect";
            this.chkRate5DBEffect.Size = new System.Drawing.Size(72, 17);
            this.chkRate5DBEffect.TabIndex = 553;
            this.chkRate5DBEffect.Text = "DB Effect";
            this.chkRate5DBEffect.UseVisualStyleBackColor = true;
            // 
            // chkRate4DBEffect
            // 
            this.chkRate4DBEffect.AutoSize = true;
            this.chkRate4DBEffect.Location = new System.Drawing.Point(480, 113);
            this.chkRate4DBEffect.Name = "chkRate4DBEffect";
            this.chkRate4DBEffect.Size = new System.Drawing.Size(72, 17);
            this.chkRate4DBEffect.TabIndex = 552;
            this.chkRate4DBEffect.Text = "DB Effect";
            this.chkRate4DBEffect.UseVisualStyleBackColor = true;
            // 
            // chkRate3DBEffect
            // 
            this.chkRate3DBEffect.AutoSize = true;
            this.chkRate3DBEffect.Location = new System.Drawing.Point(480, 75);
            this.chkRate3DBEffect.Name = "chkRate3DBEffect";
            this.chkRate3DBEffect.Size = new System.Drawing.Size(72, 17);
            this.chkRate3DBEffect.TabIndex = 551;
            this.chkRate3DBEffect.Text = "DB Effect";
            this.chkRate3DBEffect.UseVisualStyleBackColor = true;
            // 
            // chkRate2DBEffect
            // 
            this.chkRate2DBEffect.AutoSize = true;
            this.chkRate2DBEffect.Location = new System.Drawing.Point(480, 43);
            this.chkRate2DBEffect.Name = "chkRate2DBEffect";
            this.chkRate2DBEffect.Size = new System.Drawing.Size(72, 17);
            this.chkRate2DBEffect.TabIndex = 550;
            this.chkRate2DBEffect.Text = "DB Effect";
            this.chkRate2DBEffect.UseVisualStyleBackColor = true;
            // 
            // chkRate1DBEffect
            // 
            this.chkRate1DBEffect.AutoSize = true;
            this.chkRate1DBEffect.Location = new System.Drawing.Point(480, 9);
            this.chkRate1DBEffect.Name = "chkRate1DBEffect";
            this.chkRate1DBEffect.Size = new System.Drawing.Size(72, 17);
            this.chkRate1DBEffect.TabIndex = 549;
            this.chkRate1DBEffect.Text = "DB Effect";
            this.chkRate1DBEffect.UseVisualStyleBackColor = true;
            // 
            // txtRate5Password
            // 
            this.txtRate5Password.Location = new System.Drawing.Point(363, 142);
            this.txtRate5Password.Name = "txtRate5Password";
            this.txtRate5Password.Size = new System.Drawing.Size(108, 20);
            this.txtRate5Password.TabIndex = 548;
            // 
            // txtRate4Password
            // 
            this.txtRate4Password.Location = new System.Drawing.Point(363, 110);
            this.txtRate4Password.Name = "txtRate4Password";
            this.txtRate4Password.Size = new System.Drawing.Size(108, 20);
            this.txtRate4Password.TabIndex = 547;
            // 
            // txtRate3Password
            // 
            this.txtRate3Password.Location = new System.Drawing.Point(363, 74);
            this.txtRate3Password.Name = "txtRate3Password";
            this.txtRate3Password.Size = new System.Drawing.Size(108, 20);
            this.txtRate3Password.TabIndex = 546;
            // 
            // txtRate2Password
            // 
            this.txtRate2Password.Location = new System.Drawing.Point(363, 40);
            this.txtRate2Password.Name = "txtRate2Password";
            this.txtRate2Password.Size = new System.Drawing.Size(108, 20);
            this.txtRate2Password.TabIndex = 545;
            // 
            // txtRate1Password
            // 
            this.txtRate1Password.Location = new System.Drawing.Point(365, 6);
            this.txtRate1Password.Name = "txtRate1Password";
            this.txtRate1Password.Size = new System.Drawing.Size(108, 20);
            this.txtRate1Password.TabIndex = 544;
            // 
            // ChkStockLocation
            // 
            this.ChkStockLocation.AutoSize = true;
            this.ChkStockLocation.Location = new System.Drawing.Point(157, 251);
            this.ChkStockLocation.Name = "ChkStockLocation";
            this.ChkStockLocation.Size = new System.Drawing.Size(40, 17);
            this.ChkStockLocation.TabIndex = 543;
            this.ChkStockLocation.Text = "No";
            this.ChkStockLocation.UseVisualStyleBackColor = true;
            this.ChkStockLocation.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // ChkBarCodeDis
            // 
            this.ChkBarCodeDis.AutoSize = true;
            this.ChkBarCodeDis.Location = new System.Drawing.Point(157, 223);
            this.ChkBarCodeDis.Name = "ChkBarCodeDis";
            this.ChkBarCodeDis.Size = new System.Drawing.Size(40, 17);
            this.ChkBarCodeDis.TabIndex = 542;
            this.ChkBarCodeDis.Text = "No";
            this.ChkBarCodeDis.UseVisualStyleBackColor = true;
            this.ChkBarCodeDis.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chkCategoryDis
            // 
            this.chkCategoryDis.AutoSize = true;
            this.chkCategoryDis.Location = new System.Drawing.Point(841, 144);
            this.chkCategoryDis.Name = "chkCategoryDis";
            this.chkCategoryDis.Size = new System.Drawing.Size(40, 17);
            this.chkCategoryDis.TabIndex = 541;
            this.chkCategoryDis.Text = "No";
            this.chkCategoryDis.UseVisualStyleBackColor = true;
            this.chkCategoryDis.Visible = false;
            this.chkCategoryDis.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // ChkDepartmentDis
            // 
            this.ChkDepartmentDis.AutoSize = true;
            this.ChkDepartmentDis.Location = new System.Drawing.Point(842, 113);
            this.ChkDepartmentDis.Name = "ChkDepartmentDis";
            this.ChkDepartmentDis.Size = new System.Drawing.Size(40, 17);
            this.ChkDepartmentDis.TabIndex = 540;
            this.ChkDepartmentDis.Text = "No";
            this.ChkDepartmentDis.UseVisualStyleBackColor = true;
            this.ChkDepartmentDis.Visible = false;
            this.ChkDepartmentDis.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 251);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 13);
            this.label5.TabIndex = 539;
            this.label5.Text = "Stock Location :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 223);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 13);
            this.label4.TabIndex = 538;
            this.label4.Text = "BarCode Display :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(697, 144);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 537;
            this.label2.Text = "Category Display :";
            this.label2.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(697, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 13);
            this.label1.TabIndex = 536;
            this.label1.Text = "Department Display :";
            this.label1.Visible = false;
            // 
            // txtLabel5
            // 
            this.txtLabel5.Location = new System.Drawing.Point(156, 143);
            this.txtLabel5.Name = "txtLabel5";
            this.txtLabel5.Size = new System.Drawing.Size(145, 20);
            this.txtLabel5.TabIndex = 535;
            // 
            // chkRate5
            // 
            this.chkRate5.AutoSize = true;
            this.chkRate5.Location = new System.Drawing.Point(318, 145);
            this.chkRate5.Name = "chkRate5";
            this.chkRate5.Size = new System.Drawing.Size(40, 17);
            this.chkRate5.TabIndex = 534;
            this.chkRate5.Text = "No";
            this.chkRate5.UseVisualStyleBackColor = true;
            this.chkRate5.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // txtLabel4
            // 
            this.txtLabel4.Location = new System.Drawing.Point(155, 106);
            this.txtLabel4.Name = "txtLabel4";
            this.txtLabel4.Size = new System.Drawing.Size(145, 20);
            this.txtLabel4.TabIndex = 533;
            // 
            // chkRate4
            // 
            this.chkRate4.AutoSize = true;
            this.chkRate4.Location = new System.Drawing.Point(317, 108);
            this.chkRate4.Name = "chkRate4";
            this.chkRate4.Size = new System.Drawing.Size(40, 17);
            this.chkRate4.TabIndex = 532;
            this.chkRate4.Text = "No";
            this.chkRate4.UseVisualStyleBackColor = true;
            this.chkRate4.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // txtLabel3
            // 
            this.txtLabel3.Location = new System.Drawing.Point(154, 73);
            this.txtLabel3.Name = "txtLabel3";
            this.txtLabel3.Size = new System.Drawing.Size(145, 20);
            this.txtLabel3.TabIndex = 531;
            // 
            // chkRate3
            // 
            this.chkRate3.AutoSize = true;
            this.chkRate3.Location = new System.Drawing.Point(316, 75);
            this.chkRate3.Name = "chkRate3";
            this.chkRate3.Size = new System.Drawing.Size(40, 17);
            this.chkRate3.TabIndex = 530;
            this.chkRate3.Text = "No";
            this.chkRate3.UseVisualStyleBackColor = true;
            this.chkRate3.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // txtLabel2
            // 
            this.txtLabel2.Location = new System.Drawing.Point(155, 40);
            this.txtLabel2.Name = "txtLabel2";
            this.txtLabel2.Size = new System.Drawing.Size(145, 20);
            this.txtLabel2.TabIndex = 529;
            // 
            // chkRate2
            // 
            this.chkRate2.AutoSize = true;
            this.chkRate2.Location = new System.Drawing.Point(317, 42);
            this.chkRate2.Name = "chkRate2";
            this.chkRate2.Size = new System.Drawing.Size(40, 17);
            this.chkRate2.TabIndex = 528;
            this.chkRate2.Text = "No";
            this.chkRate2.UseVisualStyleBackColor = true;
            this.chkRate2.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // txtLabel1
            // 
            this.txtLabel1.Location = new System.Drawing.Point(155, 6);
            this.txtLabel1.Name = "txtLabel1";
            this.txtLabel1.Size = new System.Drawing.Size(145, 20);
            this.txtLabel1.TabIndex = 527;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 147);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 507;
            this.label3.Text = "Rate 5 Label :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 503;
            this.label6.Text = "Rate 1 Label :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 44);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 504;
            this.label7.Text = "Rate 2 Label  :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(11, 77);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 505;
            this.label10.Text = "Rate 3 Label :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 110);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 13);
            this.label11.TabIndex = 506;
            this.label11.Text = "Rate 4 Label :";
            // 
            // chkRate1
            // 
            this.chkRate1.AutoSize = true;
            this.chkRate1.Location = new System.Drawing.Point(317, 8);
            this.chkRate1.Name = "chkRate1";
            this.chkRate1.Size = new System.Drawing.Size(40, 17);
            this.chkRate1.TabIndex = 12;
            this.chkRate1.Text = "No";
            this.chkRate1.UseVisualStyleBackColor = true;
            this.chkRate1.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // EP
            // 
            this.EP.ContainerControl = this;
            // 
            // chkAddvanceSearch
            // 
            this.chkAddvanceSearch.AutoSize = true;
            this.chkAddvanceSearch.Location = new System.Drawing.Point(700, 247);
            this.chkAddvanceSearch.Name = "chkAddvanceSearch";
            this.chkAddvanceSearch.Size = new System.Drawing.Size(40, 17);
            this.chkAddvanceSearch.TabIndex = 5241;
            this.chkAddvanceSearch.Text = "No";
            this.chkAddvanceSearch.UseVisualStyleBackColor = true;
            this.chkAddvanceSearch.CheckedChanged += new System.EventHandler(this.chkAddvanceSearch_CheckedChanged);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(548, 246);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(121, 13);
            this.label46.TabIndex = 5240;
            this.label46.Text = "Addvance Serach Items";
            // 
            // OtherSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 601);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.BtnClose);
            this.Controls.Add(this.btnSave);
            this.Name = "OtherSetting";
            this.Text = "Other Settings";
            this.Load += new System.EventHandler(this.SalesSettingAE_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EP)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button BtnClose;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox chkRate1;
        private System.Windows.Forms.ErrorProvider EP;
        private System.Windows.Forms.TextBox txtLabel5;
        private System.Windows.Forms.CheckBox chkRate5;
        private System.Windows.Forms.TextBox txtLabel4;
        private System.Windows.Forms.CheckBox chkRate4;
        private System.Windows.Forms.TextBox txtLabel3;
        private System.Windows.Forms.CheckBox chkRate3;
        private System.Windows.Forms.TextBox txtLabel2;
        private System.Windows.Forms.CheckBox chkRate2;
        private System.Windows.Forms.TextBox txtLabel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox ChkStockLocation;
        private System.Windows.Forms.CheckBox ChkBarCodeDis;
        private System.Windows.Forms.CheckBox chkCategoryDis;
        private System.Windows.Forms.CheckBox ChkDepartmentDis;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkRate5DBEffect;
        private System.Windows.Forms.CheckBox chkRate4DBEffect;
        private System.Windows.Forms.CheckBox chkRate3DBEffect;
        private System.Windows.Forms.CheckBox chkRate2DBEffect;
        private System.Windows.Forms.CheckBox chkRate1DBEffect;
        private System.Windows.Forms.TextBox txtRate5Password;
        private System.Windows.Forms.TextBox txtRate4Password;
        private System.Windows.Forms.TextBox txtRate3Password;
        private System.Windows.Forms.TextBox txtRate2Password;
        private System.Windows.Forms.TextBox txtRate1Password;
        private System.Windows.Forms.CheckBox chkESupermode;
        private System.Windows.Forms.CheckBox chkDSupermode;
        private System.Windows.Forms.CheckBox chkCSupermode;
        private System.Windows.Forms.CheckBox chkBSupermode;
        private System.Windows.Forms.CheckBox chkASupermode;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chkTaxTypeGrid;
        private System.Windows.Forms.CheckBox ChkReportDisplay;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox chkStockItemPrintBarCode;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox chkRptExcel;
        private System.Windows.Forms.Label lblRptExcel;
        private System.Windows.Forms.TextBox txtTopSales;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox chkIsBrand;
        private System.Windows.Forms.CheckBox ChkShowLastBill;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox chkBilingual;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cmbLanguage;
        private System.Windows.Forms.Label lblLanguage;
        private System.Windows.Forms.ComboBox cmbDefaultBillPrint;
        private System.Windows.Forms.Label lblDefaultBillPrint;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox txtUpDownLoadPath;
        private System.Windows.Forms.Label UpDownLoadPath;
        private System.Windows.Forms.TextBox txtUpDownDays;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtBackUpPath;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnPath;
        private System.Windows.Forms.CheckBox chkIsException;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdGodex;
        private System.Windows.Forms.RadioButton rdTSC;
        private System.Windows.Forms.CheckBox chkBillWithMRP;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtUpDownLink;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.CheckBox chkMultiRates;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.CheckBox chkAddvanceSearch;
        private System.Windows.Forms.Label label46;
    }
}