﻿namespace Kirana.Settings
{
    partial class SalesSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbParty = new System.Windows.Forms.ComboBox();
            this.cmbTaxType = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.BtnClose = new System.Windows.Forms.Button();
            this.grpbDefault = new System.Windows.Forms.GroupBox();
            this.cmbDiscountType = new System.Windows.Forms.ComboBox();
            this.cmbGodownNo = new System.Windows.Forms.ComboBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.cmbDefaultParty = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.chkRatePassword = new System.Windows.Forms.CheckBox();
            this.chkDisplayRateType = new System.Windows.Forms.CheckBox();
            this.cmbOutwardLoc = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.cmbRateType = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbRoundOfAccount = new System.Windows.Forms.ComboBox();
            this.cmbItemNameType = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.grpbDiscount = new System.Windows.Forms.GroupBox();
            this.cmbOtherDisc = new System.Windows.Forms.ComboBox();
            this.label34 = new System.Windows.Forms.Label();
            this.cmbSchemeDisc = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.chkDisc1 = new System.Windows.Forms.CheckBox();
            this.cmbItemDisc = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbDiscount1 = new System.Windows.Forms.ComboBox();
            this.grpbCharges = new System.Windows.Forms.GroupBox();
            this.txtChargeLabel = new System.Windows.Forms.TextBox();
            this.chkChrg1 = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbCharge1 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.grpbOthers = new System.Windows.Forms.GroupBox();
            this.chkBillRoundOff = new System.Windows.Forms.CheckBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtCreditCardDigits = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.ChkManualBill = new System.Windows.Forms.CheckBox();
            this.label30 = new System.Windows.Forms.Label();
            this.ChkPayableAmt = new System.Windows.Forms.CheckBox();
            this.label29 = new System.Windows.Forms.Label();
            this.chkBillPrint = new System.Windows.Forms.CheckBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.chkMultipleFirm = new System.Windows.Forms.CheckBox();
            this.label24 = new System.Windows.Forms.Label();
            this.chkSingleFirm = new System.Windows.Forms.CheckBox();
            this.lblSeconds = new System.Windows.Forms.Label();
            this.numSeconds = new System.Windows.Forms.NumericUpDown();
            this.lblWithin = new System.Windows.Forms.Label();
            this.chkHideRatePopupAutomatically = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.chkShowRateHistoryAutomatically = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.chkIsStopOnSaleHistoryListEnabled = new System.Windows.Forms.CheckBox();
            this.chkIsUseLastSaleRateEnabled = new System.Windows.Forms.CheckBox();
            this.chkIsShowPurchaseHistoryEnabled = new System.Windows.Forms.CheckBox();
            this.chkIsShowSalesHistoryEnabled = new System.Windows.Forms.CheckBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.chkIsReverseRateCalc = new System.Windows.Forms.CheckBox();
            this.chkIsAllowsDuplicatesItemsInSameBill = new System.Windows.Forms.CheckBox();
            this.label17 = new System.Windows.Forms.Label();
            this.chkIsBarcodeEnabled = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.EP = new System.Windows.Forms.ErrorProvider(this.components);
            this.grpbStopOnField = new System.Windows.Forms.GroupBox();
            this.chkStopOnDisc = new System.Windows.Forms.CheckBox();
            this.chkStopOnRate = new System.Windows.Forms.CheckBox();
            this.chkStopOnQty = new System.Windows.Forms.CheckBox();
            this.chkStopOnGrid = new System.Windows.Forms.CheckBox();
            this.chkStopOnTaxType = new System.Windows.Forms.CheckBox();
            this.chkStopOnDate = new System.Windows.Forms.CheckBox();
            this.chkStopOnRateType = new System.Windows.Forms.CheckBox();
            this.chkStopOnParty = new System.Windows.Forms.CheckBox();
            this.dgSettings = new System.Windows.Forms.DataGridView();
            this.SrNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SettingKeyCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SettingValue = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PkSettingNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkGodownPrinting = new System.Windows.Forms.CheckBox();
            this.label43 = new System.Windows.Forms.Label();
            this.chckShowSchemeDetails = new System.Windows.Forms.CheckBox();
            this.label39 = new System.Windows.Forms.Label();
            this.pnlAddressInBill = new System.Windows.Forms.Panel();
            this.label37 = new System.Windows.Forms.Label();
            this.chkCounterBill = new System.Windows.Forms.CheckBox();
            this.label38 = new System.Windows.Forms.Label();
            this.chkHomeDelivery = new System.Windows.Forms.CheckBox();
            this.chkAddressInBill = new System.Windows.Forms.CheckBox();
            this.chkBillWithMRP = new System.Windows.Forms.CheckBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.chkShowOutStanding = new System.Windows.Forms.CheckBox();
            this.label31 = new System.Windows.Forms.Label();
            this.chkShowSavingBill = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFooter2Value = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cmbOrderType = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtFooterValue = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtSavingValue = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tabSalesSetting = new JitControls.OMTabControl();
            this.tabPage1 = new JitControls.OMTabPage();
            this.tabPage2 = new JitControls.OMTabPage();
            this.label42 = new System.Windows.Forms.Label();
            this.txtHamaliRs = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.tabPage3 = new JitControls.OMTabPage();
            this.tabPage4 = new JitControls.OMTabPage();
            this.tabPage5 = new JitControls.OMTabPage();
            this.pnlMain = new JitControls.OMBPanel();
            this.txtBillUpdatePwd = new System.Windows.Forms.TextBox();
            this.chkAllBillUpdate = new System.Windows.Forms.CheckBox();
            this.label58 = new System.Windows.Forms.Label();
            this.grpbDefault.SuspendLayout();
            this.grpbDiscount.SuspendLayout();
            this.grpbCharges.SuspendLayout();
            this.grpbOthers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSeconds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EP)).BeginInit();
            this.grpbStopOnField.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgSettings)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.pnlAddressInBill.SuspendLayout();
            this.tabSalesSetting.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 501;
            this.label1.Text = "Party A/C :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(455, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 502;
            this.label2.Text = "Tax Type :";
            // 
            // cmbParty
            // 
            this.cmbParty.Enabled = false;
            this.cmbParty.FormattingEnabled = true;
            this.cmbParty.Location = new System.Drawing.Point(192, 14);
            this.cmbParty.Name = "cmbParty";
            this.cmbParty.Size = new System.Drawing.Size(260, 21);
            this.cmbParty.TabIndex = 0;
            // 
            // cmbTaxType
            // 
            this.cmbTaxType.FormattingEnabled = true;
            this.cmbTaxType.Location = new System.Drawing.Point(586, 41);
            this.cmbTaxType.Name = "cmbTaxType";
            this.cmbTaxType.Size = new System.Drawing.Size(230, 21);
            this.cmbTaxType.TabIndex = 1;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Location = new System.Drawing.Point(17, 264);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(80, 60);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // BtnClose
            // 
            this.BtnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnClose.Location = new System.Drawing.Point(103, 264);
            this.BtnClose.Name = "BtnClose";
            this.BtnClose.Size = new System.Drawing.Size(80, 60);
            this.BtnClose.TabIndex = 13;
            this.BtnClose.Text = "E&xit";
            this.BtnClose.UseVisualStyleBackColor = true;
            this.BtnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // grpbDefault
            // 
            this.grpbDefault.Controls.Add(this.cmbDiscountType);
            this.grpbDefault.Controls.Add(this.cmbGodownNo);
            this.grpbDefault.Controls.Add(this.label45);
            this.grpbDefault.Controls.Add(this.label44);
            this.grpbDefault.Controls.Add(this.cmbDefaultParty);
            this.grpbDefault.Controls.Add(this.label40);
            this.grpbDefault.Controls.Add(this.chkRatePassword);
            this.grpbDefault.Controls.Add(this.chkDisplayRateType);
            this.grpbDefault.Controls.Add(this.cmbOutwardLoc);
            this.grpbDefault.Controls.Add(this.label23);
            this.grpbDefault.Controls.Add(this.cmbRateType);
            this.grpbDefault.Controls.Add(this.label18);
            this.grpbDefault.Controls.Add(this.label8);
            this.grpbDefault.Controls.Add(this.cmbRoundOfAccount);
            this.grpbDefault.Controls.Add(this.cmbItemNameType);
            this.grpbDefault.Controls.Add(this.label16);
            this.grpbDefault.Controls.Add(this.cmbTaxType);
            this.grpbDefault.Controls.Add(this.label2);
            this.grpbDefault.Controls.Add(this.cmbParty);
            this.grpbDefault.Location = new System.Drawing.Point(10, 14);
            this.grpbDefault.Name = "grpbDefault";
            this.grpbDefault.Size = new System.Drawing.Size(827, 171);
            this.grpbDefault.TabIndex = 0;
            this.grpbDefault.TabStop = false;
            this.grpbDefault.Text = "Default Value Settings";
            // 
            // cmbDiscountType
            // 
            this.cmbDiscountType.FormattingEnabled = true;
            this.cmbDiscountType.Location = new System.Drawing.Point(190, 122);
            this.cmbDiscountType.MaxLength = 25;
            this.cmbDiscountType.Name = "cmbDiscountType";
            this.cmbDiscountType.Size = new System.Drawing.Size(262, 21);
            this.cmbDiscountType.TabIndex = 5225;
            // 
            // cmbGodownNo
            // 
            this.cmbGodownNo.FormattingEnabled = true;
            this.cmbGodownNo.Location = new System.Drawing.Point(586, 122);
            this.cmbGodownNo.MaxLength = 25;
            this.cmbGodownNo.Name = "cmbGodownNo";
            this.cmbGodownNo.Size = new System.Drawing.Size(230, 21);
            this.cmbGodownNo.TabIndex = 5224;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(455, 125);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(122, 13);
            this.label45.TabIndex = 5223;
            this.label45.Text = "Default Stock Location :";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(6, 130);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(119, 13);
            this.label44.TabIndex = 5218;
            this.label44.Text = "Default Discount Type :";
            // 
            // cmbDefaultParty
            // 
            this.cmbDefaultParty.FormattingEnabled = true;
            this.cmbDefaultParty.Location = new System.Drawing.Point(190, 96);
            this.cmbDefaultParty.Name = "cmbDefaultParty";
            this.cmbDefaultParty.Size = new System.Drawing.Size(260, 21);
            this.cmbDefaultParty.TabIndex = 5216;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(6, 100);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(92, 13);
            this.label40.TabIndex = 5217;
            this.label40.Text = "Default Party A/c.";
            // 
            // chkRatePassword
            // 
            this.chkRatePassword.AutoSize = true;
            this.chkRatePassword.Location = new System.Drawing.Point(586, 99);
            this.chkRatePassword.Name = "chkRatePassword";
            this.chkRatePassword.Size = new System.Drawing.Size(146, 17);
            this.chkRatePassword.TabIndex = 5215;
            this.chkRatePassword.Text = "Rate Type Password Ask";
            this.chkRatePassword.UseVisualStyleBackColor = true;
            // 
            // chkDisplayRateType
            // 
            this.chkDisplayRateType.AutoSize = true;
            this.chkDisplayRateType.Location = new System.Drawing.Point(458, 98);
            this.chkDisplayRateType.Name = "chkDisplayRateType";
            this.chkDisplayRateType.Size = new System.Drawing.Size(113, 17);
            this.chkDisplayRateType.TabIndex = 5214;
            this.chkDisplayRateType.Text = "Display Rate Type";
            this.chkDisplayRateType.UseVisualStyleBackColor = true;
            // 
            // cmbOutwardLoc
            // 
            this.cmbOutwardLoc.FormattingEnabled = true;
            this.cmbOutwardLoc.Location = new System.Drawing.Point(191, 71);
            this.cmbOutwardLoc.Name = "cmbOutwardLoc";
            this.cmbOutwardLoc.Size = new System.Drawing.Size(258, 21);
            this.cmbOutwardLoc.TabIndex = 5212;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(4, 74);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(97, 13);
            this.label23.TabIndex = 5213;
            this.label23.Text = "Outward Location :";
            // 
            // cmbRateType
            // 
            this.cmbRateType.FormattingEnabled = true;
            this.cmbRateType.Location = new System.Drawing.Point(586, 15);
            this.cmbRateType.Name = "cmbRateType";
            this.cmbRateType.Size = new System.Drawing.Size(230, 21);
            this.cmbRateType.TabIndex = 5211;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(455, 19);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(63, 13);
            this.label18.TabIndex = 5210;
            this.label18.Text = "Rate Type :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(455, 74);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(102, 13);
            this.label8.TabIndex = 5209;
            this.label8.Text = "RoundOff Account :";
            // 
            // cmbRoundOfAccount
            // 
            this.cmbRoundOfAccount.FormattingEnabled = true;
            this.cmbRoundOfAccount.Location = new System.Drawing.Point(586, 70);
            this.cmbRoundOfAccount.Name = "cmbRoundOfAccount";
            this.cmbRoundOfAccount.Size = new System.Drawing.Size(230, 21);
            this.cmbRoundOfAccount.TabIndex = 5208;
            this.cmbRoundOfAccount.Text = "  ";
            // 
            // cmbItemNameType
            // 
            this.cmbItemNameType.FormattingEnabled = true;
            this.cmbItemNameType.Location = new System.Drawing.Point(192, 42);
            this.cmbItemNameType.Name = "cmbItemNameType";
            this.cmbItemNameType.Size = new System.Drawing.Size(259, 21);
            this.cmbItemNameType.TabIndex = 5205;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 46);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(145, 13);
            this.label16.TabIndex = 5204;
            this.label16.Text = "Product Name Display Type :";
            // 
            // grpbDiscount
            // 
            this.grpbDiscount.Controls.Add(this.cmbOtherDisc);
            this.grpbDiscount.Controls.Add(this.label34);
            this.grpbDiscount.Controls.Add(this.cmbSchemeDisc);
            this.grpbDiscount.Controls.Add(this.label32);
            this.grpbDiscount.Controls.Add(this.chkDisc1);
            this.grpbDiscount.Controls.Add(this.cmbItemDisc);
            this.grpbDiscount.Controls.Add(this.label27);
            this.grpbDiscount.Controls.Add(this.label4);
            this.grpbDiscount.Controls.Add(this.cmbDiscount1);
            this.grpbDiscount.Location = new System.Drawing.Point(425, 6);
            this.grpbDiscount.Name = "grpbDiscount";
            this.grpbDiscount.Size = new System.Drawing.Size(412, 125);
            this.grpbDiscount.TabIndex = 527;
            this.grpbDiscount.TabStop = false;
            this.grpbDiscount.Text = "Discount Account Settings";
            // 
            // cmbOtherDisc
            // 
            this.cmbOtherDisc.FormattingEnabled = true;
            this.cmbOtherDisc.Location = new System.Drawing.Point(116, 98);
            this.cmbOtherDisc.Name = "cmbOtherDisc";
            this.cmbOtherDisc.Size = new System.Drawing.Size(252, 21);
            this.cmbOtherDisc.TabIndex = 515;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(15, 101);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(63, 13);
            this.label34.TabIndex = 516;
            this.label34.Text = "Other Disc :";
            // 
            // cmbSchemeDisc
            // 
            this.cmbSchemeDisc.FormattingEnabled = true;
            this.cmbSchemeDisc.Location = new System.Drawing.Point(116, 72);
            this.cmbSchemeDisc.Name = "cmbSchemeDisc";
            this.cmbSchemeDisc.Size = new System.Drawing.Size(252, 21);
            this.cmbSchemeDisc.TabIndex = 513;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(15, 75);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(76, 13);
            this.label32.TabIndex = 514;
            this.label32.Text = "Scheme Disc.:";
            // 
            // chkDisc1
            // 
            this.chkDisc1.AutoSize = true;
            this.chkDisc1.Location = new System.Drawing.Point(383, 22);
            this.chkDisc1.Name = "chkDisc1";
            this.chkDisc1.Size = new System.Drawing.Size(15, 14);
            this.chkDisc1.TabIndex = 511;
            this.chkDisc1.UseVisualStyleBackColor = true;
            this.chkDisc1.CheckedChanged += new System.EventHandler(this.chkDisc_CheckedChanged);
            // 
            // cmbItemDisc
            // 
            this.cmbItemDisc.FormattingEnabled = true;
            this.cmbItemDisc.Location = new System.Drawing.Point(116, 45);
            this.cmbItemDisc.Name = "cmbItemDisc";
            this.cmbItemDisc.Size = new System.Drawing.Size(252, 21);
            this.cmbItemDisc.TabIndex = 511;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(15, 48);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(60, 13);
            this.label27.TabIndex = 512;
            this.label27.Text = "Item Disc. :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 507;
            this.label4.Text = "Discount 1 :";
            // 
            // cmbDiscount1
            // 
            this.cmbDiscount1.FormattingEnabled = true;
            this.cmbDiscount1.Location = new System.Drawing.Point(116, 19);
            this.cmbDiscount1.Name = "cmbDiscount1";
            this.cmbDiscount1.Size = new System.Drawing.Size(252, 21);
            this.cmbDiscount1.TabIndex = 6;
            // 
            // grpbCharges
            // 
            this.grpbCharges.Controls.Add(this.txtChargeLabel);
            this.grpbCharges.Controls.Add(this.chkChrg1);
            this.grpbCharges.Controls.Add(this.label6);
            this.grpbCharges.Controls.Add(this.cmbCharge1);
            this.grpbCharges.Controls.Add(this.label7);
            this.grpbCharges.Location = new System.Drawing.Point(13, 6);
            this.grpbCharges.Name = "grpbCharges";
            this.grpbCharges.Size = new System.Drawing.Size(406, 83);
            this.grpbCharges.TabIndex = 526;
            this.grpbCharges.TabStop = false;
            this.grpbCharges.Text = "Charges Account Settings";
            // 
            // txtChargeLabel
            // 
            this.txtChargeLabel.Location = new System.Drawing.Point(141, 48);
            this.txtChargeLabel.Name = "txtChargeLabel";
            this.txtChargeLabel.Size = new System.Drawing.Size(211, 20);
            this.txtChargeLabel.TabIndex = 508;
            // 
            // chkChrg1
            // 
            this.chkChrg1.AutoSize = true;
            this.chkChrg1.Location = new System.Drawing.Point(369, 25);
            this.chkChrg1.Name = "chkChrg1";
            this.chkChrg1.Size = new System.Drawing.Size(15, 14);
            this.chkChrg1.TabIndex = 507;
            this.chkChrg1.UseVisualStyleBackColor = true;
            this.chkChrg1.CheckedChanged += new System.EventHandler(this.chkChrg_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 503;
            this.label6.Text = "Charge 1 :";
            // 
            // cmbCharge1
            // 
            this.cmbCharge1.FormattingEnabled = true;
            this.cmbCharge1.Location = new System.Drawing.Point(141, 21);
            this.cmbCharge1.Name = "cmbCharge1";
            this.cmbCharge1.Size = new System.Drawing.Size(211, 21);
            this.cmbCharge1.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 13);
            this.label7.TabIndex = 504;
            this.label7.Text = "Charge Lable Name :";
            // 
            // grpbOthers
            // 
            this.grpbOthers.Controls.Add(this.txtBillUpdatePwd);
            this.grpbOthers.Controls.Add(this.chkAllBillUpdate);
            this.grpbOthers.Controls.Add(this.chkBillRoundOff);
            this.grpbOthers.Controls.Add(this.label58);
            this.grpbOthers.Controls.Add(this.label33);
            this.grpbOthers.Controls.Add(this.txtCreditCardDigits);
            this.grpbOthers.Controls.Add(this.label5);
            this.grpbOthers.Controls.Add(this.ChkManualBill);
            this.grpbOthers.Controls.Add(this.label30);
            this.grpbOthers.Controls.Add(this.ChkPayableAmt);
            this.grpbOthers.Controls.Add(this.label29);
            this.grpbOthers.Controls.Add(this.chkBillPrint);
            this.grpbOthers.Controls.Add(this.label28);
            this.grpbOthers.Controls.Add(this.label25);
            this.grpbOthers.Controls.Add(this.chkMultipleFirm);
            this.grpbOthers.Controls.Add(this.label24);
            this.grpbOthers.Controls.Add(this.chkSingleFirm);
            this.grpbOthers.Controls.Add(this.lblSeconds);
            this.grpbOthers.Controls.Add(this.numSeconds);
            this.grpbOthers.Controls.Add(this.lblWithin);
            this.grpbOthers.Controls.Add(this.chkHideRatePopupAutomatically);
            this.grpbOthers.Controls.Add(this.label14);
            this.grpbOthers.Controls.Add(this.chkShowRateHistoryAutomatically);
            this.grpbOthers.Controls.Add(this.label9);
            this.grpbOthers.Controls.Add(this.chkIsStopOnSaleHistoryListEnabled);
            this.grpbOthers.Controls.Add(this.chkIsUseLastSaleRateEnabled);
            this.grpbOthers.Controls.Add(this.chkIsShowPurchaseHistoryEnabled);
            this.grpbOthers.Controls.Add(this.chkIsShowSalesHistoryEnabled);
            this.grpbOthers.Controls.Add(this.label22);
            this.grpbOthers.Controls.Add(this.label21);
            this.grpbOthers.Controls.Add(this.label20);
            this.grpbOthers.Controls.Add(this.label19);
            this.grpbOthers.Controls.Add(this.label26);
            this.grpbOthers.Controls.Add(this.chkIsReverseRateCalc);
            this.grpbOthers.Controls.Add(this.chkIsAllowsDuplicatesItemsInSameBill);
            this.grpbOthers.Controls.Add(this.label17);
            this.grpbOthers.Controls.Add(this.chkIsBarcodeEnabled);
            this.grpbOthers.Controls.Add(this.label15);
            this.grpbOthers.Location = new System.Drawing.Point(12, 9);
            this.grpbOthers.Name = "grpbOthers";
            this.grpbOthers.Size = new System.Drawing.Size(825, 188);
            this.grpbOthers.TabIndex = 535;
            this.grpbOthers.TabStop = false;
            this.grpbOthers.Text = "Other Settings";
            // 
            // chkBillRoundOff
            // 
            this.chkBillRoundOff.AutoSize = true;
            this.chkBillRoundOff.Location = new System.Drawing.Point(215, 164);
            this.chkBillRoundOff.Name = "chkBillRoundOff";
            this.chkBillRoundOff.Size = new System.Drawing.Size(40, 17);
            this.chkBillRoundOff.TabIndex = 5237;
            this.chkBillRoundOff.Text = "No";
            this.chkBillRoundOff.UseVisualStyleBackColor = true;
            this.chkBillRoundOff.CheckedChanged += new System.EventHandler(this.chkFooterLevelDisc_CheckedChanged);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(8, 164);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(72, 13);
            this.label33.TabIndex = 5236;
            this.label33.Text = "Bill Round Off";
            // 
            // txtCreditCardDigits
            // 
            this.txtCreditCardDigits.Location = new System.Drawing.Point(452, 132);
            this.txtCreditCardDigits.Name = "txtCreditCardDigits";
            this.txtCreditCardDigits.Size = new System.Drawing.Size(100, 20);
            this.txtCreditCardDigits.TabIndex = 5235;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(307, 137);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 13);
            this.label5.TabIndex = 5234;
            this.label5.Text = "CreditCardDigits :";
            // 
            // ChkManualBill
            // 
            this.ChkManualBill.AutoSize = true;
            this.ChkManualBill.Location = new System.Drawing.Point(770, 137);
            this.ChkManualBill.Name = "ChkManualBill";
            this.ChkManualBill.Size = new System.Drawing.Size(40, 17);
            this.ChkManualBill.TabIndex = 5233;
            this.ChkManualBill.Text = "No";
            this.ChkManualBill.UseVisualStyleBackColor = true;
            this.ChkManualBill.CheckedChanged += new System.EventHandler(this.ChkManualBill_CheckedChanged);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(593, 138);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(95, 13);
            this.label30.TabIndex = 5232;
            this.label30.Text = "Is Manual Bill No : ";
            // 
            // ChkPayableAmt
            // 
            this.ChkPayableAmt.AutoSize = true;
            this.ChkPayableAmt.Location = new System.Drawing.Point(215, 138);
            this.ChkPayableAmt.Name = "ChkPayableAmt";
            this.ChkPayableAmt.Size = new System.Drawing.Size(40, 17);
            this.ChkPayableAmt.TabIndex = 5231;
            this.ChkPayableAmt.Text = "No";
            this.ChkPayableAmt.UseVisualStyleBackColor = true;
            this.ChkPayableAmt.CheckedChanged += new System.EventHandler(this.ChkPayableAmt_CheckedChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(8, 139);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(114, 13);
            this.label29.TabIndex = 5230;
            this.label29.Text = "Ask Payable Amount : ";
            // 
            // chkBillPrint
            // 
            this.chkBillPrint.AutoSize = true;
            this.chkBillPrint.Location = new System.Drawing.Point(524, 111);
            this.chkBillPrint.Name = "chkBillPrint";
            this.chkBillPrint.Size = new System.Drawing.Size(40, 17);
            this.chkBillPrint.TabIndex = 5229;
            this.chkBillPrint.Text = "No";
            this.chkBillPrint.UseVisualStyleBackColor = true;
            this.chkBillPrint.CheckedChanged += new System.EventHandler(this.chkBillPrint_CheckedChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(409, 111);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(64, 13);
            this.label28.TabIndex = 5228;
            this.label28.Text = "Is Bill Print : ";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(592, 111);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(123, 13);
            this.label25.TabIndex = 5227;
            this.label25.Text = "IsAllowMultipleFirmChq : ";
            // 
            // chkMultipleFirm
            // 
            this.chkMultipleFirm.AutoSize = true;
            this.chkMultipleFirm.Location = new System.Drawing.Point(770, 111);
            this.chkMultipleFirm.Name = "chkMultipleFirm";
            this.chkMultipleFirm.Size = new System.Drawing.Size(40, 17);
            this.chkMultipleFirm.TabIndex = 5226;
            this.chkMultipleFirm.Text = "No";
            this.chkMultipleFirm.UseVisualStyleBackColor = true;
            this.chkMultipleFirm.CheckedChanged += new System.EventHandler(this.chkMultipleFirm_CheckedChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(593, 81);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(116, 13);
            this.label24.TabIndex = 5225;
            this.label24.Text = "IsAllowSingleFirmChq : ";
            // 
            // chkSingleFirm
            // 
            this.chkSingleFirm.AutoSize = true;
            this.chkSingleFirm.Location = new System.Drawing.Point(770, 81);
            this.chkSingleFirm.Name = "chkSingleFirm";
            this.chkSingleFirm.Size = new System.Drawing.Size(40, 17);
            this.chkSingleFirm.TabIndex = 5224;
            this.chkSingleFirm.Text = "No";
            this.chkSingleFirm.UseVisualStyleBackColor = true;
            this.chkSingleFirm.CheckedChanged += new System.EventHandler(this.chkSingleFirm_CheckedChanged);
            // 
            // lblSeconds
            // 
            this.lblSeconds.AutoSize = true;
            this.lblSeconds.Location = new System.Drawing.Point(328, 112);
            this.lblSeconds.Name = "lblSeconds";
            this.lblSeconds.Size = new System.Drawing.Size(49, 13);
            this.lblSeconds.TabIndex = 5223;
            this.lblSeconds.Text = "Seconds";
            // 
            // numSeconds
            // 
            this.numSeconds.Location = new System.Drawing.Point(283, 109);
            this.numSeconds.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numSeconds.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numSeconds.Name = "numSeconds";
            this.numSeconds.Size = new System.Drawing.Size(38, 20);
            this.numSeconds.TabIndex = 5222;
            this.numSeconds.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // lblWithin
            // 
            this.lblWithin.AutoSize = true;
            this.lblWithin.Location = new System.Drawing.Point(232, 113);
            this.lblWithin.Name = "lblWithin";
            this.lblWithin.Size = new System.Drawing.Size(37, 13);
            this.lblWithin.TabIndex = 5220;
            this.lblWithin.Text = "Within";
            // 
            // chkHideRatePopupAutomatically
            // 
            this.chkHideRatePopupAutomatically.AutoSize = true;
            this.chkHideRatePopupAutomatically.Location = new System.Drawing.Point(215, 112);
            this.chkHideRatePopupAutomatically.Name = "chkHideRatePopupAutomatically";
            this.chkHideRatePopupAutomatically.Size = new System.Drawing.Size(15, 14);
            this.chkHideRatePopupAutomatically.TabIndex = 5219;
            this.chkHideRatePopupAutomatically.UseVisualStyleBackColor = true;
            this.chkHideRatePopupAutomatically.CheckedChanged += new System.EventHandler(this.chkHideRatePopupAutomatically_CheckedChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 112);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(160, 13);
            this.label14.TabIndex = 5218;
            this.label14.Text = "Hide Rate Popup Automatically :";
            // 
            // chkShowRateHistoryAutomatically
            // 
            this.chkShowRateHistoryAutomatically.AutoSize = true;
            this.chkShowRateHistoryAutomatically.Location = new System.Drawing.Point(524, 81);
            this.chkShowRateHistoryAutomatically.Name = "chkShowRateHistoryAutomatically";
            this.chkShowRateHistoryAutomatically.Size = new System.Drawing.Size(40, 17);
            this.chkShowRateHistoryAutomatically.TabIndex = 5217;
            this.chkShowRateHistoryAutomatically.Text = "No";
            this.chkShowRateHistoryAutomatically.UseVisualStyleBackColor = true;
            this.chkShowRateHistoryAutomatically.CheckedChanged += new System.EventHandler(this.chkShowRateHistoryAutomatically_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(305, 81);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(166, 13);
            this.label9.TabIndex = 5216;
            this.label9.Text = "Show Rate History Automatically :";
            // 
            // chkIsStopOnSaleHistoryListEnabled
            // 
            this.chkIsStopOnSaleHistoryListEnabled.AutoSize = true;
            this.chkIsStopOnSaleHistoryListEnabled.Location = new System.Drawing.Point(770, 51);
            this.chkIsStopOnSaleHistoryListEnabled.Name = "chkIsStopOnSaleHistoryListEnabled";
            this.chkIsStopOnSaleHistoryListEnabled.Size = new System.Drawing.Size(40, 17);
            this.chkIsStopOnSaleHistoryListEnabled.TabIndex = 5215;
            this.chkIsStopOnSaleHistoryListEnabled.Text = "No";
            this.chkIsStopOnSaleHistoryListEnabled.UseVisualStyleBackColor = true;
            this.chkIsStopOnSaleHistoryListEnabled.CheckedChanged += new System.EventHandler(this.chkIsStopOnSaleHistoryListEnabled_CheckedChanged);
            // 
            // chkIsUseLastSaleRateEnabled
            // 
            this.chkIsUseLastSaleRateEnabled.AutoSize = true;
            this.chkIsUseLastSaleRateEnabled.Location = new System.Drawing.Point(524, 51);
            this.chkIsUseLastSaleRateEnabled.Name = "chkIsUseLastSaleRateEnabled";
            this.chkIsUseLastSaleRateEnabled.Size = new System.Drawing.Size(40, 17);
            this.chkIsUseLastSaleRateEnabled.TabIndex = 5214;
            this.chkIsUseLastSaleRateEnabled.Text = "No";
            this.chkIsUseLastSaleRateEnabled.UseVisualStyleBackColor = true;
            this.chkIsUseLastSaleRateEnabled.CheckedChanged += new System.EventHandler(this.chkIsUseLastSaleRateEnabled_CheckedChanged);
            // 
            // chkIsShowPurchaseHistoryEnabled
            // 
            this.chkIsShowPurchaseHistoryEnabled.AutoSize = true;
            this.chkIsShowPurchaseHistoryEnabled.Location = new System.Drawing.Point(215, 78);
            this.chkIsShowPurchaseHistoryEnabled.Name = "chkIsShowPurchaseHistoryEnabled";
            this.chkIsShowPurchaseHistoryEnabled.Size = new System.Drawing.Size(40, 17);
            this.chkIsShowPurchaseHistoryEnabled.TabIndex = 5213;
            this.chkIsShowPurchaseHistoryEnabled.Text = "No";
            this.chkIsShowPurchaseHistoryEnabled.UseVisualStyleBackColor = true;
            this.chkIsShowPurchaseHistoryEnabled.CheckedChanged += new System.EventHandler(this.chkIsShowPurchaseHistoryEnabled_CheckedChanged);
            // 
            // chkIsShowSalesHistoryEnabled
            // 
            this.chkIsShowSalesHistoryEnabled.AutoSize = true;
            this.chkIsShowSalesHistoryEnabled.Location = new System.Drawing.Point(215, 48);
            this.chkIsShowSalesHistoryEnabled.Name = "chkIsShowSalesHistoryEnabled";
            this.chkIsShowSalesHistoryEnabled.Size = new System.Drawing.Size(40, 17);
            this.chkIsShowSalesHistoryEnabled.TabIndex = 5212;
            this.chkIsShowSalesHistoryEnabled.Text = "No";
            this.chkIsShowSalesHistoryEnabled.UseVisualStyleBackColor = true;
            this.chkIsShowSalesHistoryEnabled.CheckedChanged += new System.EventHandler(this.chkIsShowSalesHistoryEnabled_CheckedChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(593, 54);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(127, 13);
            this.label22.TabIndex = 5211;
            this.label22.Text = "Stop On Sale Hisory List :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(306, 53);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(105, 13);
            this.label21.TabIndex = 5210;
            this.label21.Text = "Use Last Sale Rate :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(7, 81);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(123, 13);
            this.label20.TabIndex = 5209;
            this.label20.Text = "Show Purchase History :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(8, 48);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(104, 13);
            this.label19.TabIndex = 5208;
            this.label19.Text = "Show Sales History :";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(592, 25);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(131, 13);
            this.label26.TabIndex = 5207;
            this.label26.Text = "Reverse Rate Calulation : ";
            // 
            // chkIsReverseRateCalc
            // 
            this.chkIsReverseRateCalc.AutoSize = true;
            this.chkIsReverseRateCalc.Enabled = false;
            this.chkIsReverseRateCalc.Location = new System.Drawing.Point(771, 18);
            this.chkIsReverseRateCalc.Name = "chkIsReverseRateCalc";
            this.chkIsReverseRateCalc.Size = new System.Drawing.Size(40, 17);
            this.chkIsReverseRateCalc.TabIndex = 5206;
            this.chkIsReverseRateCalc.Text = "No";
            this.chkIsReverseRateCalc.UseVisualStyleBackColor = true;
            this.chkIsReverseRateCalc.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chkIsAllowsDuplicatesItemsInSameBill
            // 
            this.chkIsAllowsDuplicatesItemsInSameBill.AutoSize = true;
            this.chkIsAllowsDuplicatesItemsInSameBill.Location = new System.Drawing.Point(524, 20);
            this.chkIsAllowsDuplicatesItemsInSameBill.Name = "chkIsAllowsDuplicatesItemsInSameBill";
            this.chkIsAllowsDuplicatesItemsInSameBill.Size = new System.Drawing.Size(40, 17);
            this.chkIsAllowsDuplicatesItemsInSameBill.TabIndex = 5205;
            this.chkIsAllowsDuplicatesItemsInSameBill.Text = "No";
            this.chkIsAllowsDuplicatesItemsInSameBill.UseVisualStyleBackColor = true;
            this.chkIsAllowsDuplicatesItemsInSameBill.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(307, 22);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(124, 13);
            this.label17.TabIndex = 5204;
            this.label17.Text = "Allows Duplicates Items :";
            // 
            // chkIsBarcodeEnabled
            // 
            this.chkIsBarcodeEnabled.AutoSize = true;
            this.chkIsBarcodeEnabled.Location = new System.Drawing.Point(215, 16);
            this.chkIsBarcodeEnabled.Name = "chkIsBarcodeEnabled";
            this.chkIsBarcodeEnabled.Size = new System.Drawing.Size(40, 17);
            this.chkIsBarcodeEnabled.TabIndex = 14;
            this.chkIsBarcodeEnabled.Text = "No";
            this.chkIsBarcodeEnabled.UseVisualStyleBackColor = true;
            this.chkIsBarcodeEnabled.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(8, 18);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(113, 13);
            this.label15.TabIndex = 5201;
            this.label15.Text = "Use BarCode System :";
            // 
            // EP
            // 
            this.EP.ContainerControl = this;
            // 
            // grpbStopOnField
            // 
            this.grpbStopOnField.Controls.Add(this.chkStopOnDisc);
            this.grpbStopOnField.Controls.Add(this.chkStopOnRate);
            this.grpbStopOnField.Controls.Add(this.chkStopOnQty);
            this.grpbStopOnField.Controls.Add(this.chkStopOnGrid);
            this.grpbStopOnField.Controls.Add(this.chkStopOnTaxType);
            this.grpbStopOnField.Controls.Add(this.chkStopOnDate);
            this.grpbStopOnField.Controls.Add(this.chkStopOnRateType);
            this.grpbStopOnField.Controls.Add(this.chkStopOnParty);
            this.grpbStopOnField.Location = new System.Drawing.Point(14, 13);
            this.grpbStopOnField.Name = "grpbStopOnField";
            this.grpbStopOnField.Size = new System.Drawing.Size(825, 71);
            this.grpbStopOnField.TabIndex = 536;
            this.grpbStopOnField.TabStop = false;
            this.grpbStopOnField.Text = "Stop(Focus) On Field Settings";
            // 
            // chkStopOnDisc
            // 
            this.chkStopOnDisc.AutoSize = true;
            this.chkStopOnDisc.Location = new System.Drawing.Point(671, 48);
            this.chkStopOnDisc.Name = "chkStopOnDisc";
            this.chkStopOnDisc.Size = new System.Drawing.Size(89, 17);
            this.chkStopOnDisc.TabIndex = 5203;
            this.chkStopOnDisc.Text = "Stop On Disc";
            this.chkStopOnDisc.UseVisualStyleBackColor = true;
            // 
            // chkStopOnRate
            // 
            this.chkStopOnRate.AutoSize = true;
            this.chkStopOnRate.Location = new System.Drawing.Point(452, 47);
            this.chkStopOnRate.Name = "chkStopOnRate";
            this.chkStopOnRate.Size = new System.Drawing.Size(91, 17);
            this.chkStopOnRate.TabIndex = 5202;
            this.chkStopOnRate.Text = "Stop On Rate";
            this.chkStopOnRate.UseVisualStyleBackColor = true;
            // 
            // chkStopOnQty
            // 
            this.chkStopOnQty.AutoSize = true;
            this.chkStopOnQty.Location = new System.Drawing.Point(230, 46);
            this.chkStopOnQty.Name = "chkStopOnQty";
            this.chkStopOnQty.Size = new System.Drawing.Size(84, 17);
            this.chkStopOnQty.TabIndex = 5200;
            this.chkStopOnQty.Text = "Stop On Qty";
            this.chkStopOnQty.UseVisualStyleBackColor = true;
            // 
            // chkStopOnGrid
            // 
            this.chkStopOnGrid.AutoSize = true;
            this.chkStopOnGrid.Location = new System.Drawing.Point(26, 47);
            this.chkStopOnGrid.Name = "chkStopOnGrid";
            this.chkStopOnGrid.Size = new System.Drawing.Size(87, 17);
            this.chkStopOnGrid.TabIndex = 17;
            this.chkStopOnGrid.Text = "Stop On Grid";
            this.chkStopOnGrid.UseVisualStyleBackColor = true;
            // 
            // chkStopOnTaxType
            // 
            this.chkStopOnTaxType.AutoSize = true;
            this.chkStopOnTaxType.Location = new System.Drawing.Point(671, 20);
            this.chkStopOnTaxType.Name = "chkStopOnTaxType";
            this.chkStopOnTaxType.Size = new System.Drawing.Size(111, 17);
            this.chkStopOnTaxType.TabIndex = 16;
            this.chkStopOnTaxType.Text = "Stop on Tax Type";
            this.chkStopOnTaxType.UseVisualStyleBackColor = true;
            // 
            // chkStopOnDate
            // 
            this.chkStopOnDate.AutoSize = true;
            this.chkStopOnDate.Location = new System.Drawing.Point(26, 20);
            this.chkStopOnDate.Name = "chkStopOnDate";
            this.chkStopOnDate.Size = new System.Drawing.Size(89, 17);
            this.chkStopOnDate.TabIndex = 13;
            this.chkStopOnDate.Text = "Stop on Date";
            this.chkStopOnDate.UseVisualStyleBackColor = true;
            // 
            // chkStopOnRateType
            // 
            this.chkStopOnRateType.AutoSize = true;
            this.chkStopOnRateType.Location = new System.Drawing.Point(229, 20);
            this.chkStopOnRateType.Name = "chkStopOnRateType";
            this.chkStopOnRateType.Size = new System.Drawing.Size(116, 17);
            this.chkStopOnRateType.TabIndex = 15;
            this.chkStopOnRateType.Text = "Stop on Rate Type";
            this.chkStopOnRateType.UseVisualStyleBackColor = true;
            // 
            // chkStopOnParty
            // 
            this.chkStopOnParty.AutoSize = true;
            this.chkStopOnParty.Location = new System.Drawing.Point(452, 20);
            this.chkStopOnParty.Name = "chkStopOnParty";
            this.chkStopOnParty.Size = new System.Drawing.Size(121, 17);
            this.chkStopOnParty.TabIndex = 14;
            this.chkStopOnParty.Text = "Stop on Party Name";
            this.chkStopOnParty.UseVisualStyleBackColor = true;
            // 
            // dgSettings
            // 
            this.dgSettings.AllowUserToAddRows = false;
            this.dgSettings.AllowUserToDeleteRows = false;
            this.dgSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dgSettings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSettings.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SrNo,
            this.SettingKeyCode,
            this.SettingValue,
            this.PkSettingNo});
            this.dgSettings.Location = new System.Drawing.Point(569, 264);
            this.dgSettings.Name = "dgSettings";
            this.dgSettings.Size = new System.Drawing.Size(302, 55);
            this.dgSettings.TabIndex = 537;
            this.dgSettings.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgSettings_CellFormatting);
            this.dgSettings.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgSettings_CellEndEdit);
            this.dgSettings.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgSettings_KeyDown);
            // 
            // SrNo
            // 
            this.SrNo.DataPropertyName = "SrNo";
            this.SrNo.HeaderText = "SrNo";
            this.SrNo.Name = "SrNo";
            this.SrNo.ReadOnly = true;
            this.SrNo.Width = 50;
            // 
            // SettingKeyCode
            // 
            this.SettingKeyCode.DataPropertyName = "SettingKeyCode";
            this.SettingKeyCode.HeaderText = "Setting Name";
            this.SettingKeyCode.Name = "SettingKeyCode";
            this.SettingKeyCode.ReadOnly = true;
            this.SettingKeyCode.Width = 200;
            // 
            // SettingValue
            // 
            this.SettingValue.DataPropertyName = "SettingValue";
            this.SettingValue.HeaderText = "Value";
            this.SettingValue.Name = "SettingValue";
            this.SettingValue.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SettingValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.SettingValue.Width = 50;
            // 
            // PkSettingNo
            // 
            this.PkSettingNo.DataPropertyName = "PkSettingNo";
            this.PkSettingNo.HeaderText = "PkSettingNo";
            this.PkSettingNo.Name = "PkSettingNo";
            this.PkSettingNo.ReadOnly = true;
            this.PkSettingNo.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkGodownPrinting);
            this.groupBox1.Controls.Add(this.label43);
            this.groupBox1.Controls.Add(this.chckShowSchemeDetails);
            this.groupBox1.Controls.Add(this.label39);
            this.groupBox1.Controls.Add(this.pnlAddressInBill);
            this.groupBox1.Controls.Add(this.chkAddressInBill);
            this.groupBox1.Controls.Add(this.chkBillWithMRP);
            this.groupBox1.Controls.Add(this.label36);
            this.groupBox1.Controls.Add(this.label35);
            this.groupBox1.Controls.Add(this.chkShowOutStanding);
            this.groupBox1.Controls.Add(this.label31);
            this.groupBox1.Controls.Add(this.chkShowSavingBill);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtFooter2Value);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.cmbOrderType);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtFooterValue);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtSavingValue);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Location = new System.Drawing.Point(13, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(825, 168);
            this.groupBox1.TabIndex = 538;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Print Settings";
            // 
            // chkGodownPrinting
            // 
            this.chkGodownPrinting.AutoSize = true;
            this.chkGodownPrinting.Location = new System.Drawing.Point(143, 135);
            this.chkGodownPrinting.Name = "chkGodownPrinting";
            this.chkGodownPrinting.Size = new System.Drawing.Size(40, 17);
            this.chkGodownPrinting.TabIndex = 5221;
            this.chkGodownPrinting.Text = "No";
            this.chkGodownPrinting.UseVisualStyleBackColor = true;
            this.chkGodownPrinting.CheckedChanged += new System.EventHandler(this.chkBillWithMRP_CheckedChanged);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(4, 135);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(91, 13);
            this.label43.TabIndex = 5220;
            this.label43.Text = "Godown Printing :";
            // 
            // chckShowSchemeDetails
            // 
            this.chckShowSchemeDetails.AutoSize = true;
            this.chckShowSchemeDetails.Location = new System.Drawing.Point(770, 92);
            this.chckShowSchemeDetails.Name = "chckShowSchemeDetails";
            this.chckShowSchemeDetails.Size = new System.Drawing.Size(40, 17);
            this.chckShowSchemeDetails.TabIndex = 5219;
            this.chckShowSchemeDetails.Text = "No";
            this.chckShowSchemeDetails.UseVisualStyleBackColor = true;
            this.chckShowSchemeDetails.CheckedChanged += new System.EventHandler(this.ChkManualBill_CheckedChanged);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(574, 93);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(141, 13);
            this.label39.TabIndex = 5218;
            this.label39.Text = "Show Print Scheme Details :";
            // 
            // pnlAddressInBill
            // 
            this.pnlAddressInBill.Controls.Add(this.label37);
            this.pnlAddressInBill.Controls.Add(this.chkCounterBill);
            this.pnlAddressInBill.Controls.Add(this.label38);
            this.pnlAddressInBill.Controls.Add(this.chkHomeDelivery);
            this.pnlAddressInBill.Location = new System.Drawing.Point(516, 120);
            this.pnlAddressInBill.Name = "pnlAddressInBill";
            this.pnlAddressInBill.Size = new System.Drawing.Size(306, 35);
            this.pnlAddressInBill.TabIndex = 540;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(9, 11);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(82, 13);
            this.label37.TabIndex = 5218;
            this.label37.Text = "Home Delivery :";
            // 
            // chkCounterBill
            // 
            this.chkCounterBill.AutoSize = true;
            this.chkCounterBill.Location = new System.Drawing.Point(255, 11);
            this.chkCounterBill.Name = "chkCounterBill";
            this.chkCounterBill.Size = new System.Drawing.Size(40, 17);
            this.chkCounterBill.TabIndex = 5221;
            this.chkCounterBill.Text = "No";
            this.chkCounterBill.UseVisualStyleBackColor = true;
            this.chkCounterBill.CheckedChanged += new System.EventHandler(this.chkCounterBill_CheckedChanged);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(166, 12);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(66, 13);
            this.label38.TabIndex = 5219;
            this.label38.Text = "Counter Bill :";
            // 
            // chkHomeDelivery
            // 
            this.chkHomeDelivery.AutoSize = true;
            this.chkHomeDelivery.Location = new System.Drawing.Point(118, 11);
            this.chkHomeDelivery.Name = "chkHomeDelivery";
            this.chkHomeDelivery.Size = new System.Drawing.Size(40, 17);
            this.chkHomeDelivery.TabIndex = 5220;
            this.chkHomeDelivery.Text = "No";
            this.chkHomeDelivery.UseVisualStyleBackColor = true;
            this.chkHomeDelivery.CheckedChanged += new System.EventHandler(this.chkHomeDelivery_CheckedChanged);
            // 
            // chkAddressInBill
            // 
            this.chkAddressInBill.AutoSize = true;
            this.chkAddressInBill.Location = new System.Drawing.Point(473, 130);
            this.chkAddressInBill.Name = "chkAddressInBill";
            this.chkAddressInBill.Size = new System.Drawing.Size(40, 17);
            this.chkAddressInBill.TabIndex = 5217;
            this.chkAddressInBill.Text = "No";
            this.chkAddressInBill.UseVisualStyleBackColor = true;
            this.chkAddressInBill.CheckedChanged += new System.EventHandler(this.chkAddressInBill_CheckedChanged);
            // 
            // chkBillWithMRP
            // 
            this.chkBillWithMRP.AutoSize = true;
            this.chkBillWithMRP.Location = new System.Drawing.Point(143, 109);
            this.chkBillWithMRP.Name = "chkBillWithMRP";
            this.chkBillWithMRP.Size = new System.Drawing.Size(40, 17);
            this.chkBillWithMRP.TabIndex = 5216;
            this.chkBillWithMRP.Text = "No";
            this.chkBillWithMRP.UseVisualStyleBackColor = true;
            this.chkBillWithMRP.CheckedChanged += new System.EventHandler(this.chkBillWithMRP_CheckedChanged);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(326, 131);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(79, 13);
            this.label36.TabIndex = 5215;
            this.label36.Text = "Address In Bill :";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(4, 110);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(102, 13);
            this.label35.TabIndex = 5214;
            this.label35.Text = "Bill Print With MRP :";
            // 
            // chkShowOutStanding
            // 
            this.chkShowOutStanding.AutoSize = true;
            this.chkShowOutStanding.Location = new System.Drawing.Point(473, 93);
            this.chkShowOutStanding.Name = "chkShowOutStanding";
            this.chkShowOutStanding.Size = new System.Drawing.Size(40, 17);
            this.chkShowOutStanding.TabIndex = 5213;
            this.chkShowOutStanding.Text = "No";
            this.chkShowOutStanding.UseVisualStyleBackColor = true;
            this.chkShowOutStanding.CheckedChanged += new System.EventHandler(this.ChkManualBill_CheckedChanged);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(325, 93);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(100, 13);
            this.label31.TabIndex = 5212;
            this.label31.Text = "Show Outstanding :";
            // 
            // chkShowSavingBill
            // 
            this.chkShowSavingBill.AutoSize = true;
            this.chkShowSavingBill.Location = new System.Drawing.Point(143, 77);
            this.chkShowSavingBill.Name = "chkShowSavingBill";
            this.chkShowSavingBill.Size = new System.Drawing.Size(40, 17);
            this.chkShowSavingBill.TabIndex = 5211;
            this.chkShowSavingBill.Text = "No";
            this.chkShowSavingBill.UseVisualStyleBackColor = true;
            this.chkShowSavingBill.CheckedChanged += new System.EventHandler(this.ChkManualBill_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 5210;
            this.label3.Text = "Show Saving Bill :";
            // 
            // txtFooter2Value
            // 
            this.txtFooter2Value.Location = new System.Drawing.Point(470, 58);
            this.txtFooter2Value.Name = "txtFooter2Value";
            this.txtFooter2Value.Size = new System.Drawing.Size(293, 20);
            this.txtFooter2Value.TabIndex = 5209;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(325, 61);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(82, 13);
            this.label13.TabIndex = 5208;
            this.label13.Text = "Footer 2 Value :";
            // 
            // cmbOrderType
            // 
            this.cmbOrderType.FormattingEnabled = true;
            this.cmbOrderType.Location = new System.Drawing.Point(143, 46);
            this.cmbOrderType.Name = "cmbOrderType";
            this.cmbOrderType.Size = new System.Drawing.Size(180, 21);
            this.cmbOrderType.TabIndex = 5207;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(4, 51);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(106, 13);
            this.label12.TabIndex = 5206;
            this.label12.Text = "Default Order Type : ";
            // 
            // txtFooterValue
            // 
            this.txtFooterValue.Location = new System.Drawing.Point(470, 18);
            this.txtFooterValue.Multiline = true;
            this.txtFooterValue.Name = "txtFooterValue";
            this.txtFooterValue.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtFooterValue.Size = new System.Drawing.Size(293, 34);
            this.txtFooterValue.TabIndex = 5205;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(325, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 13);
            this.label11.TabIndex = 5204;
            this.label11.Text = "Footer Value :";
            // 
            // txtSavingValue
            // 
            this.txtSavingValue.Location = new System.Drawing.Point(143, 19);
            this.txtSavingValue.Name = "txtSavingValue";
            this.txtSavingValue.Size = new System.Drawing.Size(180, 20);
            this.txtSavingValue.TabIndex = 5203;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(4, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 13);
            this.label10.TabIndex = 5202;
            this.label10.Text = "Saving Value :";
            // 
            // tabSalesSetting
            // 
            this.tabSalesSetting.ActiveColor = System.Drawing.SystemColors.Control;
            this.tabSalesSetting.BackColor = System.Drawing.SystemColors.Control;
            this.tabSalesSetting.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.tabSalesSetting.Controls.Add(this.tabPage1);
            this.tabSalesSetting.Controls.Add(this.tabPage2);
            this.tabSalesSetting.Controls.Add(this.tabPage3);
            this.tabSalesSetting.Controls.Add(this.tabPage4);
            this.tabSalesSetting.Controls.Add(this.tabPage5);
            this.tabSalesSetting.ImageIndex = -1;
            this.tabSalesSetting.ImageList = null;
            this.tabSalesSetting.InactiveColor = System.Drawing.SystemColors.Window;
            this.tabSalesSetting.Location = new System.Drawing.Point(13, 12);
            this.tabSalesSetting.Name = "tabSalesSetting";
            this.tabSalesSetting.ScrollButtonStyle = JitControls.OMScrollButtonStyle.Always;
            this.tabSalesSetting.SelectedIndex = 3;
            this.tabSalesSetting.SelectedTab = this.tabPage4;
            this.tabSalesSetting.Size = new System.Drawing.Size(858, 239);
            this.tabSalesSetting.TabDock = System.Windows.Forms.DockStyle.Top;
            this.tabSalesSetting.TabDrawer = null;
            this.tabSalesSetting.TabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabSalesSetting.TabIndex = 539;
            // 
            // tabPage1
            // 
            this.tabPage1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.grpbDefault);
            this.tabPage1.ImageIndex = -1;
            this.tabPage1.Location = new System.Drawing.Point(4, 30);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(850, 205);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Default Value Settings(F1)";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label42);
            this.tabPage2.Controls.Add(this.txtHamaliRs);
            this.tabPage2.Controls.Add(this.label41);
            this.tabPage2.Controls.Add(this.grpbCharges);
            this.tabPage2.Controls.Add(this.grpbDiscount);
            this.tabPage2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPage2.ImageIndex = -1;
            this.tabPage2.Location = new System.Drawing.Point(4, 30);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(850, 205);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Charges Account Settings(F2)";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(172, 107);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(31, 13);
            this.label42.TabIndex = 528;
            this.label42.Text = "1/Kg";
            // 
            // txtHamaliRs
            // 
            this.txtHamaliRs.Location = new System.Drawing.Point(86, 104);
            this.txtHamaliRs.Name = "txtHamaliRs";
            this.txtHamaliRs.Size = new System.Drawing.Size(80, 20);
            this.txtHamaliRs.TabIndex = 510;
            this.txtHamaliRs.TextChanged += new System.EventHandler(this.txtHamaliRs_TextChanged);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(19, 107);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(61, 13);
            this.label41.TabIndex = 509;
            this.label41.Text = "Hamali Rs :";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.grpbStopOnField);
            this.tabPage3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPage3.ImageIndex = -1;
            this.tabPage3.Location = new System.Drawing.Point(4, 30);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(850, 205);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Stop(Focus)(F3)";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.grpbOthers);
            this.tabPage4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPage4.ImageIndex = -1;
            this.tabPage4.Location = new System.Drawing.Point(4, 30);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(850, 205);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Other Settings(F4)";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.groupBox1);
            this.tabPage5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPage5.ImageIndex = -1;
            this.tabPage5.Location = new System.Drawing.Point(4, 30);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(850, 205);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Print Settings(F5)";
            // 
            // pnlMain
            // 
            this.pnlMain.BorderColor = System.Drawing.Color.Gray;
            this.pnlMain.BorderRadius = 3;
            this.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMain.Controls.Add(this.tabSalesSetting);
            this.pnlMain.Controls.Add(this.dgSettings);
            this.pnlMain.Controls.Add(this.BtnClose);
            this.pnlMain.Controls.Add(this.btnSave);
            this.pnlMain.Location = new System.Drawing.Point(12, 12);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(886, 338);
            this.pnlMain.TabIndex = 540;
            // 
            // txtBillUpdatePwd
            // 
            this.txtBillUpdatePwd.Location = new System.Drawing.Point(450, 164);
            this.txtBillUpdatePwd.Name = "txtBillUpdatePwd";
            this.txtBillUpdatePwd.Size = new System.Drawing.Size(68, 20);
            this.txtBillUpdatePwd.TabIndex = 5259;
            // 
            // chkAllBillUpdate
            // 
            this.chkAllBillUpdate.AutoSize = true;
            this.chkAllBillUpdate.Location = new System.Drawing.Point(524, 164);
            this.chkAllBillUpdate.Name = "chkAllBillUpdate";
            this.chkAllBillUpdate.Size = new System.Drawing.Size(40, 17);
            this.chkAllBillUpdate.TabIndex = 5258;
            this.chkAllBillUpdate.Text = "No";
            this.chkAllBillUpdate.UseVisualStyleBackColor = true;
            this.chkAllBillUpdate.CheckedChanged += new System.EventHandler(this.chkAllBillUpdate_CheckedChanged);
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(307, 164);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(72, 13);
            this.label58.TabIndex = 5257;
            this.label58.Text = "All Bill Update";
            // 
            // SalesSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(909, 362);
            this.Controls.Add(this.pnlMain);
            this.Name = "SalesSetting";
            this.Text = "Sales Setting";
            this.Load += new System.EventHandler(this.SalesSettingAE_Load);
            this.Activated += new System.EventHandler(this.SalesSettingAE_Activated);
            this.Click += new System.EventHandler(this.SalesSettingAE_Click);
            this.grpbDefault.ResumeLayout(false);
            this.grpbDefault.PerformLayout();
            this.grpbDiscount.ResumeLayout(false);
            this.grpbDiscount.PerformLayout();
            this.grpbCharges.ResumeLayout(false);
            this.grpbCharges.PerformLayout();
            this.grpbOthers.ResumeLayout(false);
            this.grpbOthers.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSeconds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EP)).EndInit();
            this.grpbStopOnField.ResumeLayout(false);
            this.grpbStopOnField.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgSettings)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.pnlAddressInBill.ResumeLayout(false);
            this.pnlAddressInBill.PerformLayout();
            this.tabSalesSetting.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.pnlMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbParty;
        private System.Windows.Forms.ComboBox cmbTaxType;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button BtnClose;
        private System.Windows.Forms.GroupBox grpbDefault;
        private System.Windows.Forms.GroupBox grpbDiscount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbDiscount1;
        private System.Windows.Forms.GroupBox grpbCharges;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbCharge1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox grpbOthers;
        private System.Windows.Forms.CheckBox chkIsBarcodeEnabled;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox chkIsAllowsDuplicatesItemsInSameBill;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ErrorProvider EP;
        private System.Windows.Forms.GroupBox grpbStopOnField;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.CheckBox chkIsReverseRateCalc;
        private System.Windows.Forms.ComboBox cmbItemDisc;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.CheckBox chkDisc1;
        private System.Windows.Forms.CheckBox chkChrg1;
        private System.Windows.Forms.CheckBox chkStopOnTaxType;
        private System.Windows.Forms.CheckBox chkStopOnDate;
        private System.Windows.Forms.CheckBox chkStopOnRateType;
        private System.Windows.Forms.CheckBox chkStopOnParty;
        private System.Windows.Forms.DataGridView dgSettings;
        private System.Windows.Forms.DataGridViewTextBoxColumn SrNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn SettingKeyCode;
        private System.Windows.Forms.DataGridViewCheckBoxColumn SettingValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkSettingNo;
        private System.Windows.Forms.CheckBox chkStopOnGrid;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbRoundOfAccount;
        private System.Windows.Forms.ComboBox cmbItemNameType;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cmbRateType;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox chkStopOnRate;
        private System.Windows.Forms.CheckBox chkStopOnQty;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.CheckBox chkIsStopOnSaleHistoryListEnabled;
        private System.Windows.Forms.CheckBox chkIsUseLastSaleRateEnabled;
        private System.Windows.Forms.CheckBox chkIsShowPurchaseHistoryEnabled;
        private System.Windows.Forms.CheckBox chkIsShowSalesHistoryEnabled;
        private System.Windows.Forms.CheckBox chkShowRateHistoryAutomatically;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblWithin;
        private System.Windows.Forms.CheckBox chkHideRatePopupAutomatically;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblSeconds;
        private System.Windows.Forms.NumericUpDown numSeconds;
        private System.Windows.Forms.ComboBox cmbOutwardLoc;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.CheckBox chkSingleFirm;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.CheckBox chkMultipleFirm;
        private System.Windows.Forms.CheckBox chkDisplayRateType;
        private System.Windows.Forms.CheckBox chkRatePassword;
        private System.Windows.Forms.CheckBox chkBillPrint;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.CheckBox ChkPayableAmt;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.CheckBox ChkManualBill;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtChargeLabel;
        private System.Windows.Forms.TextBox txtCreditCardDigits;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtFooterValue;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtSavingValue;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbOrderType;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtFooter2Value;
        private System.Windows.Forms.Label label13;
        private JitControls.OMTabPage tabPage1;
        private JitControls.OMTabPage tabPage2;
        private JitControls.OMTabPage tabPage3;
        private JitControls.OMTabPage tabPage4;
        private JitControls.OMTabPage tabPage5;
        private JitControls.OMBPanel pnlMain;
        private JitControls.OMTabControl tabSalesSetting;
        private System.Windows.Forms.CheckBox chkShowOutStanding;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.CheckBox chkShowSavingBill;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbSchemeDisc;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.CheckBox chkAddressInBill;
        private System.Windows.Forms.CheckBox chkBillWithMRP;
        private System.Windows.Forms.CheckBox chkBillRoundOff;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.ComboBox cmbOtherDisc;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Panel pnlAddressInBill;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.CheckBox chkCounterBill;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.CheckBox chkHomeDelivery;
        private System.Windows.Forms.CheckBox chckShowSchemeDetails;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox cmbDefaultParty;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtHamaliRs;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.CheckBox chkGodownPrinting;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.ComboBox cmbGodownNo;
        private System.Windows.Forms.ComboBox cmbDiscountType;
        private System.Windows.Forms.CheckBox chkStopOnDisc;
        private System.Windows.Forms.TextBox txtBillUpdatePwd;
        private System.Windows.Forms.CheckBox chkAllBillUpdate;
        private System.Windows.Forms.Label label58;
    }
}