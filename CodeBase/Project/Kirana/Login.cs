﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using OM;
using JitControls;

namespace Kirana
{
    /// <summary>
    /// This class used for Login
    /// </summary>
    public partial class Login : Form
    {
        OMCommonClass cc = new OMCommonClass();
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();
        Security secure = new Security();

        MLogin log = new MLogin();
        DBMUser user = new DBMUser();

        internal int RegType;

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public Login()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
            ObjFunction.FormatControl(this.Controls);
            ObjFunction.KeyPressFormat(this.Controls);
            Label1.Font = ObjFunction.GetFont(FontStyle.Bold, 15);

            if (ObjFunction.SetConnection("Master") == false)
                Application.Exit();

            else
            {

                try
                {
                    //CommonFunctions.MainPassword = "786";

                    //CommonFunctions.ServerName = "Comp14\\SQLEXPRESS";// ".\\SQLEXPRESS";

                   //CommonFunctions.DatabaseName = "Hi";// "Retailer1213";
                    CommonFunctions.DefaultDatabaseName = CommonFunctions.DBName;
                    CommonFunctions.DefaultServerName = CommonFunctions.ServerName;
                   // CommonFunctions.DatabaseName = "KiranaC";
                    CommonFunctions.ConStr = "Data Source=" + CommonFunctions.ServerName + ";Initial Catalog=" + CommonFunctions.DatabaseName + ";User ID=OM96;Password=OM96";
                    //CommonFunctions.ConStr = @"Data Source=.\PEELWORKS;Initial Catalog=UbuntuPOSP;User ID=Logicall;Password=Logicall";

                    //CommonFunctions.ConStr = "Data Source=" + CommonFunctions.ServerName + ";Initial Catalog=" + CommonFunctions.DatabaseName + ";User ID=Logicall;Password=Log!%3031";
                    DBGetVal.DBName = ObjFunction.GetDatabaseName(CommonFunctions.ConStr);
                //    CommonFunctions.ConStrTools = @"Data Source=.\SQLEXPRESS;Initial Catalog=RetailerTools0001;Integrated security=true;";
                    //CBUser.DataSource = null;
                    //DataSet ds = new DataSet();
                    //ds = ObjDset.FillDset("New", "Select UserCode,UsersUserCode From Muser", CommonFunctions.ConStr);
                    //CBUser.DisplayMember = "UserName";
                    //CBUser.ValueMember = "UserCode";
                    //CBUser.DataSource = ds.Tables[0];
                    //ObjFunction.FillComb(CBUser, "Select UserCode,UsersUserCode From Muser");
                    CommonFunctions.CompanyName = ObjQry.ReturnString("Select CompanyName From MCompany", CommonFunctions.ConStr);
                    DBGetVal.FromDate = ObjQry.ReturnDate("Select BooksBeginFrom From MCompany", CommonFunctions.ConStr);
                    DBGetVal.ToDate = ObjQry.ReturnDate("Select BooksEndOn From MCompany", CommonFunctions.ConStr);
                    DBGetVal.CompanyAddress = ObjQry.ReturnString("Select AddressCode From MCompany", CommonFunctions.ConStr);
                    DBGetVal.CompanyNo = ObjQry.ReturnInteger("Select CompanyNo From MCompany Order by CompanyNo desc", CommonFunctions.ConStr);
                    DBGetVal.GSTNo = ObjQry.ReturnString("Select GSTIN From MCompany", CommonFunctions.ConStr);
                    //DBGetVal.DirectoryPath = @"C:\Finance\";
                    //CommonFunctions.AddDirectorySecurity(DBGetVal.DirectoryPath, System.Security.AccessControl.FileSystemRights.Read, System.Security.AccessControl.AccessControlType.Allow);
                    //DBGetVal.DirectoryPath = @"C:\Finance\";
                    //if (System.IO.Directory.Exists(DBGetVal.DirectoryPath) == false)
                    //{
                    //    System.IO.Directory.CreateDirectory(DBGetVal.DirectoryPath);
                    //}
                    btnLogin.Focus();
                    //txtPass.Focus();
                    cnxtMenu.Items.Add(CommonFunctions.ConStr);
                    DBGetVal.ServerTime = CommonFunctions.GetTime();

                    DBGetVal.MacID = cc.GetMacName();
                    DBGetVal.MachineIPAddress = Dns.GetHostByName(Dns.GetHostName().ToString()).AddressList[0];
                    DBGetVal.HostName = Dns.GetHostName();

                    if (RegType == 1) InsertRegistration();
                    DBGetVal.MacNo = ObjQry.ReturnLong("Select RegNo From MRegistration Where MacID='" + secure.psEncrypt(DBGetVal.MacID) + "'", CommonFunctions.ConStr);
                    if (DBGetVal.MacNo == 0)
                    {
                        InsertRegistration();
                        DBGetVal.MacNo = ObjQry.ReturnLong("Select RegNo From MRegistration Where MacID='" + secure.psEncrypt(DBGetVal.MacID) + "'", CommonFunctions.ConStr);
                    }
                    ObjFunction.SetAppSettings();
                    ObjFunction.SetReportPath();
                    ObjFunction.GetTimeUserPasswords();
                    string strServer = ObjQry.ReturnString("Select DatabaseServer From MServerSettings", CommonFunctions.ConStr);
                    CommonFunctions.ConStrServer = (strServer != "") ? secure.psDecrypt(strServer) : "";

                    cc.WriteSecurityFile(DBGetVal.ServerTime, new DBAssemblyInfo().AssemblyTitle, OMCommonClass.InstallationType.None, 0, 0, 0, false);
                    if (cc.ReadSecurity(DateTime.Now, new DBAssemblyInfo().AssemblyTitle) == false)
                    {
                        cc.WriteLockFile(new DBAssemblyInfo().AssemblyTitle);
                        OMMessageBox.Show("Software is expired. Please Contact to System Administrator.", CommonFunctions.ErrorTitle);
                        Application.Exit();
                    }
                    if (ObjQry.ReturnLong("Select CompanyNo From MStockBarCode", CommonFunctions.ConStr) == 0)
                    {
                        DBGetVal.CompanyNo = ObjQry.ReturnInteger("Select CompanyNo From MCompany Order by CompanyNo desc", CommonFunctions.ConStr);
                        ObjTrans.Execute("Alter Table MStockBarCode Add CompanyNo numeric(18,0);", CommonFunctions.ConStr);
                        ObjTrans.Execute("Update MStockBarCode set CompanyNo =" + DBGetVal.CompanyNo + "", CommonFunctions.ConStr);
                    }
                    if (ObjQry.ReturnLong("Select Count( Distinct LoginDate) From MLogin", CommonFunctions.ConStr) > Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.O_LoginCount)))
                    {
                        string Str = "There is One of the serious problem. Please Contact to software team. Thank You ....";
                        OMMessageBox.Show(Str, "Error System.", OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        Application.Exit();
                    }
                    //ObjFunction.ExecuteScript();
                    ObjFunction.CheckVersion();
                }
                catch (Exception e1)
                {
                    OMMessageBox.Show(e1.Message);
                    Application.Exit();
                }
            }
        }

        private bool Valid()
        {
            bool flag = false;
            DBAssemblyInfo dbAss = new DBAssemblyInfo();
            if (txtUserName.Text == "")
            {
                OMMessageBox.Show("Please Enter User Name.", dbAss.AssemblyTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                txtUserName.Focus();
            }
            else if (txtPass.Text == "")
            {
                OMMessageBox.Show("Please Enter Password.", dbAss.AssemblyTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                txtPass.Focus();
            }
            else flag = true;


            return flag;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (Valid() == true)
                {
                    DBGetVal.UserID = ObjQry.ReturnInteger("Select UserCode From Muser Where UsersUserCode='" + txtUserName.Text + "'", CommonFunctions.ConStr);
                    if (DBGetVal.UserID > 0)
                    {
                        DBGetVal.CompanyNo = ObjQry.ReturnInteger("Select CompanyNo From MCompany Order by CompanyNo desc", CommonFunctions.ConStr);
                        if (CommonFunctions.MainPassword.ToString() == txtPass.Text)
                        {
                            DBGetVal.CompanyName = CommonFunctions.CompanyName;

                            MDIParent1 NewF = new MDIParent1();
                            DBGetVal.MainForm = NewF;
                            DBGetVal.UserName = txtUserName.Text.ToUpper();
                            DBGetVal.IsAdmin = ObjQry.ReturnInteger("Select UserType from Muser where UserCode=" + DBGetVal.UserID + " ", CommonFunctions.ConStr) == 1 ? true : false;
                            NewF.TSUserName.Text = "User Name: " + DBGetVal.UserName;
                            NewF.Show();
                            txtUserName.Text = "";
                            txtPass.Text = "";
                            this.Hide();

                        }
                        else
                        {
                            string pass = ObjQry.ReturnString("Select Password from Muser where UserCode=" + DBGetVal.UserID + " ", CommonFunctions.ConStr);

                            if (txtPass.Text == secure.psDecrypt(pass))
                            {
                                log.LoginNo = 0;
                                log.UserID = DBGetVal.UserID;
                                log.LoginDate = DateTime.Now.Date;// Convert.ToDateTime(DateTime.Now.ToString(Format.DDMMMYYYY));
                                log.LoginTime = DateTime.Now;
                                log.LogoutTime = DateTime.Now;
                                if (user.AddMLogin(log) == true)
                                {
                                    DBGetVal.CompanyName = CommonFunctions.CompanyName;

                                    DBGetVal.IsAdmin = ObjQry.ReturnInteger("Select UserType from Muser where UserCode=" + DBGetVal.UserID + " ", CommonFunctions.ConStr) == 1 ? true : false;
                                    DBGetVal.UserName = txtUserName.Text.ToUpper();
                                    MDIParent1 NewF = new MDIParent1();
                                    DBGetVal.MainForm = NewF;
                                    NewF.TSUserName.Text = "User Name: " + DBGetVal.UserName;
                                    NewF.Show();
                                    txtUserName.Text = "";
                                    txtPass.Text = "";
                                    this.Hide();
                                }
                                else
                                {
                                    OMMessageBox.Show("Please Check System......", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                                }
                            }
                            else
                            {
                                OMMessageBox.Show("Password Incorrect", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                                txtPass.Focus();
                            }
                        }
                    }
                    else
                    {
                        OMMessageBox.Show("User Name is Incorrect", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        txtUserName.Focus();
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Login_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void Login_Activated(object sender, EventArgs e)
        {
            txtUserName.Focus();
        }

        private void Login_Shown(object sender, EventArgs e)
        {
            txtUserName.Text = "";
            txtPass.Text = "";
            txtUserName.Focus();
        }

        private void InsertRegistration()
        {
            DBMRegistration dbReg = new DBMRegistration();
            MRegistration mReg = new MRegistration();
            MRegistrationDetails mRegDtls = new MRegistrationDetails();

            mReg.RegNo = ObjQry.ReturnLong("Select RegNo From MRegistration Where MacID='" + secure.psEncrypt(DBGetVal.MacID) + "'", CommonFunctions.ConStr);
            mReg.MacID = DBGetVal.MacID;
            mReg.MachineIP = DBGetVal.MachineIPAddress.ToString();
            mReg.HostName = DBGetVal.HostName;
            mReg.IsActive = true;
            mReg.IsManual = false;
            mReg.CompanyNo = DBGetVal.CompanyNo;
            dbReg.AddMRegistration(mReg);

            mRegDtls.RegDtlsNo = 0;
            mRegDtls.RegDate = Convert.ToDateTime(DBGetVal.ServerTime.ToString("dd-MMM-yyyy"));
            mRegDtls.RegTime = DBGetVal.ServerTime;
            mRegDtls.CompanyNo = DBGetVal.CompanyNo;
            dbReg.AddMRegistrationDetails(mRegDtls);

            dbReg.ExecuteNonQueryStatements();
        }

        private void txtUserName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtUserName.Focus();
            }
            else if (e.Control && e.Alt && e.KeyCode == Keys.C)
            {
                e.SuppressKeyPress = true;
                Form frmChild = new UI.FirmAccountDialog();
                ObjFunction.OpenForm(frmChild);
                if (((UI.FirmAccountDialog)frmChild).ReturnDataBaseName != "")
                {
                    SetDetails(((UI.FirmAccountDialog)frmChild).ReturnDataBaseName, ((UI.FirmAccountDialog)frmChild).ReturnServerName);
                }
            }
        }

        private void txtPass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnLogin.Focus();
                btnLogin_Click(sender, e);
                e.SuppressKeyPress = true;
            }
        }

        private void Login_Paint(object sender, PaintEventArgs e)
        {
            Rectangle rect = this.ClientRectangle;

            LinearGradientBrush brush = new LinearGradientBrush(rect, Color.AliceBlue, Color.LightSteelBlue, 75);

            e.Graphics.FillRectangle(brush, rect);

            Pen pen = new Pen(Color.Snow, 2);
            pen.Alignment = PenAlignment.Inset;
            e.Graphics.DrawRectangle(pen, rect);
        }

        private void SetDetails(string DBName,string ServerName)
        {
            CommonFunctions.DatabaseName = DBName;
            CommonFunctions.ServerName = ServerName;
            CommonFunctions.ConStr = "Data Source=" + CommonFunctions.ServerName + ";Initial Catalog=" + CommonFunctions.DatabaseName + ";User ID=OM96;Password=OM96";


            DBGetVal.DBName = ObjFunction.GetDatabaseName(CommonFunctions.ConStr);
     //       CommonFunctions.ConStrTools = @"Data Source=.\SQLEXPRESS;Initial Catalog=RetailerTools0001;Integrated security=true;";
            CommonFunctions.CompanyName = ObjQry.ReturnString("Select CompanyName From MCompany", CommonFunctions.ConStr);
            DBGetVal.FromDate = ObjQry.ReturnDate("Select BooksBeginFrom From MCompany", CommonFunctions.ConStr);
            DBGetVal.ToDate = ObjQry.ReturnDate("Select BooksEndOn From MCompany", CommonFunctions.ConStr);
            DBGetVal.CompanyAddress = ObjQry.ReturnString("Select AddressCode From MCompany", CommonFunctions.ConStr);
            DBGetVal.CompanyNo = ObjQry.ReturnInteger("Select CompanyNo From MCompany Order by CompanyNo desc", CommonFunctions.ConStr);
            cnxtMenu.Items.Add(CommonFunctions.ConStr);
            DBGetVal.ServerTime = CommonFunctions.GetTime();

            DBGetVal.MacID = cc.GetMacName();
            DBGetVal.MachineIPAddress = Dns.GetHostByName(Dns.GetHostName().ToString()).AddressList[0];
            DBGetVal.HostName = Dns.GetHostName();

            if (RegType == 1) InsertRegistration();
            DBGetVal.MacNo = ObjQry.ReturnLong("Select RegNo From MRegistration Where MacID='" + secure.psEncrypt(DBGetVal.MacID) + "'", CommonFunctions.ConStr);
            if (DBGetVal.MacNo == 0)
            {
                InsertRegistration();
                DBGetVal.MacNo = ObjQry.ReturnLong("Select RegNo From MRegistration Where MacID='" + secure.psEncrypt(DBGetVal.MacID) + "'", CommonFunctions.ConStr);
            }
            ObjFunction.SetAppSettings();
            ObjFunction.SetReportPath();
            ObjFunction.GetTimeUserPasswords();
            string strServer = ObjQry.ReturnString("Select DatabaseServer From MServerSettings", CommonFunctions.ConStr);
            CommonFunctions.ConStrServer = (strServer != "") ? secure.psDecrypt(strServer) : "";

            cc.WriteSecurityFile(DBGetVal.ServerTime, new DBAssemblyInfo().AssemblyTitle, OMCommonClass.InstallationType.None, 0, 0, 0, false);
            if (cc.ReadSecurity(DateTime.Now, new DBAssemblyInfo().AssemblyTitle) == false)
            {
                cc.WriteLockFile(new DBAssemblyInfo().AssemblyTitle);
                OMMessageBox.Show("Software is expired. Please Contact to System Administrator.", CommonFunctions.ErrorTitle);
                Application.Exit();
            }
            if (ObjQry.ReturnLong("Select CompanyNo From MStockBarCode", CommonFunctions.ConStr) == 0)
            {
                DBGetVal.CompanyNo = ObjQry.ReturnInteger("Select CompanyNo From MCompany Order by CompanyNo desc", CommonFunctions.ConStr);
                ObjTrans.Execute("Alter Table MStockBarCode Add CompanyNo numeric(18,0);", CommonFunctions.ConStr);
                ObjTrans.Execute("Update MStockBarCode set CompanyNo =" + DBGetVal.CompanyNo + "", CommonFunctions.ConStr);
            }
            if (ObjQry.ReturnLong("Select Count( Distinct LoginDate) From MLogin", CommonFunctions.ConStr) > Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.O_LoginCount)))
            {
                string Str = "There is One of the serious problem. Please Contact to software team. Thank You ....";
                OMMessageBox.Show(Str, "Error System.", OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                Application.Exit();
            }
            ObjFunction.ExecuteScript();
            ObjFunction.CheckVersion();
        }

       
    }
}
