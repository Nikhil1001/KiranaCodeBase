﻿namespace Kirana.Vouchers
{
    partial class QtyReadDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblheader = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtQty = new System.Windows.Forms.TextBox();
            this.BtnScan = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.EP = new System.Windows.Forms.ErrorProvider(this.components);
            this.BtnCancel = new System.Windows.Forms.Button();
            this.btnManual = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.EP)).BeginInit();
            this.SuspendLayout();
            // 
            // lblheader
            // 
            this.lblheader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblheader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(73)))), ((int)(((byte)(83)))));
            this.lblheader.ForeColor = System.Drawing.Color.White;
            this.lblheader.Location = new System.Drawing.Point(0, 0);
            this.lblheader.Name = "lblheader";
            this.lblheader.Size = new System.Drawing.Size(406, 23);
            this.lblheader.TabIndex = 911;
            this.lblheader.Text = "Read Weight From Scale";
            this.lblheader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(13, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 912;
            this.label1.Text = "Weight :";
            // 
            // TxtQty
            // 
            this.TxtQty.Location = new System.Drawing.Point(109, 41);
            this.TxtQty.Multiline = true;
            this.TxtQty.Name = "TxtQty";
            this.TxtQty.ReadOnly = true;
            this.TxtQty.Size = new System.Drawing.Size(280, 59);
            this.TxtQty.TabIndex = 913;
            this.TxtQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // BtnScan
            // 
            this.BtnScan.Location = new System.Drawing.Point(18, 125);
            this.BtnScan.Name = "BtnScan";
            this.BtnScan.Size = new System.Drawing.Size(89, 55);
            this.BtnScan.TabIndex = 914;
            this.BtnScan.Text = "&Read\r\nWeight\r\n(F2)";
            this.BtnScan.UseVisualStyleBackColor = true;
            this.BtnScan.Click += new System.EventHandler(this.BtnScan_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(112, 125);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(89, 55);
            this.BtnOk.TabIndex = 915;
            this.BtnOk.Text = "&Ok \r\n(F3)";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // serialPort1
            // 
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
            // 
            // EP
            // 
            this.EP.ContainerControl = this;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(206, 125);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(89, 55);
            this.BtnCancel.TabIndex = 916;
            this.BtnCancel.Text = "&Cancel \r\n(Esc)";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // btnManual
            // 
            this.btnManual.Location = new System.Drawing.Point(300, 125);
            this.btnManual.Name = "btnManual";
            this.btnManual.Size = new System.Drawing.Size(89, 55);
            this.btnManual.TabIndex = 917;
            this.btnManual.Text = "&Manual \r\n(F5)";
            this.btnManual.UseVisualStyleBackColor = true;
            this.btnManual.Click += new System.EventHandler(this.btnManual_Click);
            // 
            // QtyReadDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(406, 195);
            this.Controls.Add(this.btnManual);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.BtnScan);
            this.Controls.Add(this.TxtQty);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblheader);
            this.Name = "QtyReadDialog";
            this.Text = "QtyReadDialog";
            this.Shown += new System.EventHandler(this.QtyReadDialog_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.QtyReadDialog_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.EP)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblheader;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtQty;
        private System.Windows.Forms.Button BtnScan;
        private System.Windows.Forms.Button BtnOk;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.ErrorProvider EP;
        private System.Windows.Forms.Button btnManual;
        private System.Windows.Forms.Button BtnCancel;
    }
}