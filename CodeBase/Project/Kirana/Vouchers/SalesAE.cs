﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;
using System.Net.Sockets;
using System.Net;
using System.IO;


namespace Kirana.Vouchers
{
    /// <summary>
    /// This class used for Sales AE
    /// </summary>
    public partial class SalesAE : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        DBTVaucherEntry dbTVoucherEntry = new DBTVaucherEntry();
        TVoucherEntry tVoucherEntry = new TVoucherEntry();
        TVoucherDetails tVoucherDetails = new TVoucherDetails();
        TVoucherRefDetails tVchRefDtls = new TVoucherRefDetails();
        TVoucherPayTypeDetails tVchPayTypeDetails = new TVoucherPayTypeDetails();
        TVoucherChqCreditDetails tVchChqCredit = new TVoucherChqCreditDetails();
        TStock tStock = new TStock();
        TStockGodown tStockGodown = new TStockGodown();
        TReward tReward = new TReward();
        TRewardDetails tRewardDetails = new TRewardDetails();
        TRewardFrom tRewardFrom = new TRewardFrom();
        TRewardTo tRewardTo = new TRewardTo();
        MSchemeAchieverDetails mSchAchDtls = new MSchemeAchieverDetails();
        TFooterDiscountDetails tFooterDisc = new TFooterDiscountDetails();
        TItemLevelDiscountDetails tItemLevelDisc = new TItemLevelDiscountDetails();

        DataTable dtCurrentStock = new DataTable();
        DataTable dtDelete = new DataTable();
        DataTable dtSearch = new DataTable();
        DataTable dtUOMTemp = new DataTable();
        DataTable dtVchMainDetails = new DataTable();
        DataTable dtCompRatio = new DataTable();
        DataTable dtVchPrev = new DataTable();
        DataTable dtPayLedger = new DataTable();
        DataTable dtItemLevelDisc = new DataTable();
        Color clrColorRow = Color.FromArgb(255, 224, 192);
        DataSet ds_ItemSearch;

        int cntRow, BillingMode, rowQtyIndex;
        long LastBillNo = 0, MTDSchDetailsNo = 0;
        bool Spaceflag = true, BillSizeFlag = false, ShowSchemeFlag = false;
        internal bool RewardDeleteFlag = false, RewardFlag = false, DiscFlag = false;
        long ItemNameType = 0, RateTypeNo, PrintAsk, PartyNo, PayType, ParkBillNo, OrderType = 0, FooterDiscDtlsNo = 0;/*bcdno,*/
        int iItemNameStartIndex = 3, ItemType = 0, MixModeVal = 0;
        string strUom, Param1Value = "", Param2Value = "";
        string[] strItemQuery, strItemQuery_last;
        bool isDoProcess = false;
        //string strBank = "0", strBranch = "0";
        bool EnrFlag = false, MixModeFlag = false, CancelFlag = false, ComboFlag = false;//BillFlag = false
        DateTime tempDate; long tempPartyNo = 0;
        DataTable dtOrderType;
        DataTable dt = new DataTable();
        DateTime dtFrom, dtTo;

        public static string BrandFilter = "";
        long VoucherUserNo;
        bool flagParking = false;
        bool StopOnQty = false, StopOnRate = false;
        internal long ID, VoucherType;
        bool isSavingTransaction = false;//, ExistFormFlag = false;

        DataTable dtTRewardToFrom;
        DataTable dtTRewardDtls;
        DataTable dtPrinterSetting = new DataTable();
        bool MFlag = false;
        PartialPayment partialPay = new PartialPayment();
        long LocalStateNo = 0;
        bool isLocalSale = true, DiscountItemMasterWise = false;
        string strRateType = "";
        long TempCount = 0;
        bool IsSchemeDispaly = false;
        System.Media.SoundPlayer player = new System.Media.SoundPlayer();

        List<OM.Scheme> LstScheme, LstSchemeConsume;
        DBMScheme dbMScheme = new DBMScheme();
        int dgBillHight;

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public SalesAE()
        {
            InitializeComponent();
        }

        /// <summary>
        /// This is class of parameterised Constructor
        /// </summary>
        public SalesAE(long ID)
        {
            InitializeComponent();
            this.ID = ID;
        }

        private void dgBill_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 0)
                {
                    e.Value = (e.RowIndex + 1).ToString();
                }

                if (e.ColumnIndex == ColIndex.ItemName)
                {
                    if (dgBill.Rows[e.RowIndex].Cells[ColIndex.PkBarCodeNo].Value != null &&
                        dgBill.Rows[e.RowIndex].Cells[ColIndex.PkBarCodeNo].Value.ToString() != "")
                    {
                        if (dgBill.Rows[e.RowIndex].Cells[ColIndex.PkBarCodeNo].Value.ToString() != "0")
                        {
                            dgBill.Rows[e.RowIndex].ReadOnly = false;
                            dgBill.Rows[e.RowIndex].Cells[ColIndex.ItemName].ReadOnly = true;
                        }
                    }
                }

                if (e.ColumnIndex == ColIndex.ItemName)
                {
                    if (dgBill.Rows[e.RowIndex].Cells[ColIndex.SchemeDetailsNo].Value != null)
                    {
                        if (dgBill.Rows[e.RowIndex].Cells[ColIndex.SchemeDetailsNo].FormattedValue.ToString() != "" && dgBill.Rows[e.RowIndex].Cells[ColIndex.SchemeDetailsNo].FormattedValue.ToString() != "0")
                        {
                            dgBill.Rows[e.RowIndex].ReadOnly = true;
                        }
                        //else dgBill.Rows[e.RowIndex].ReadOnly = false;
                    }
                    //else dgBill.Rows[e.RowIndex].ReadOnly = false;
                }
                if (e.ColumnIndex == ColIndex.DiscRupees)
                {
                    if (dgBill.Rows[e.RowIndex].Cells[ColIndex.Amount].Value == null)
                        dgBill.Rows[e.RowIndex].Cells[ColIndex.DiscRupees].ReadOnly = true;
                    else dgBill.Rows[e.RowIndex].Cells[ColIndex.DiscRupees].ReadOnly = false;
                }



                //dgBill.CurrentRow.Selected = true;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void SalesAE_Load(object sender, EventArgs e)
        {
            try
            {
                LstScheme = dbMScheme.IntScheme(dtpBillDate.Value.Date);
                player.SoundLocation = Environment.CurrentDirectory + "\\BarcodeNotFound.wav";
                dtPrinterSetting = ObjFunction.GetDataView("Select PrinterNo,PrinterName,MachineName,MachineIP,IsDefault,IsActive,PrintType,IsLangType From MPrinter").Table;
                int width = Screen.PrimaryScreen.Bounds.Width;
                int height = Screen.PrimaryScreen.Bounds.Height;
                ObjFunction.LockButtons(true, this.Controls);
                ObjFunction.LockControls(false, this.Controls);
                btnInsScheme.Visible = true;
                btnInsSchemeInfo.Visible = true;

                rbEnglish.Enabled = true;
                rbMarathi.Enabled = true;
                dgBill.Enabled = false;
                VoucherType = VchType.Sales;
                dtOrderType = ObjFunction.GetDataView("Select OrderTypeNo,OrderTypeName,ColorName From MOrderType").Table;
                RateTypeNo = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_RateType));
                InitDelTable();
                intialCurrentStock();
                ItemNameType = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_ItemNameType)); //deepak
                LocalStateNo = ObjQry.ReturnLong("Select StateNo " +
                               " FROM MCompany WHERE CompanyNo = " + DBGetVal.CompanyNo, CommonFunctions.ConStr);

                initItemQuery();

                txtHRs.Text = ObjFunction.GetAppSettings(AppSettings.S_HamaliRs);

                ObjFunction.FillComb(cmbPaymentType, "Select PKPayTypeNo,PayTypeName From MPayType where PKPayTypeNo!=1 order by PayTypeName");

                ObjFunction.FillList(lstBank, "Select BankNo,BankName From MOtherBank where IsActive='true' order by BankName");
                ObjFunction.FillList(lstBranch, "Select BranchNo,BranchName From MBranch where IsActive='true' order by BranchName");

                ObjFunction.FillComb(cmbPartyName, "Select LedgerNo,LedgerName From MLedger Where GroupNo in (" + GroupType.SundryDebtors + ") and IsActive='true' order by LedgerName");
                ObjFunction.FillComb(cmbTaxType, "SELECT GroupNo, GroupName FROM MGroup WHERE (ControlGroup = " + GroupType.DutiesAndTaxes + " ) AND IsActive = 'True' ORDER BY GroupName");

                ObjFunction.FillCombo(cmbPartyNameSearch, "Select LedgerNo,LedgerName From MLedger Where GroupNo in (" + GroupType.SundryDebtors + ")  order by LedgerName");

                FillRateType();
                cmbTaxType.SelectedValue = ObjFunction.GetAppSettings(AppSettings.S_TaxType);
                MFlag = false;
                cmbPartyName.Enabled = false;//deepak
                txtInvNo.Enabled = false;
                dtpBillDate.Enabled = false;
                dtpBillTime.Enabled = false;
                dtpBillTime.Format = DateTimePickerFormat.Time;
                btnOrderType.BackColor = Color.FromArgb(255, 128, 0);

                InitControls();
                StopOnQty = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_StopOnQty));// Convert.ToBoolean(dtSalesSetting.Rows[0].ItemArray[14].ToString());
                StopOnRate = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_StopOnRate)); //Convert.ToBoolean(dtSalesSetting.Rows[0].ItemArray[13].ToString());

                dtSearch = ObjFunction.GetDataView("Select PkVoucherNo from TVoucherEntry Where VoucherTypeCode=" + VchType.Sales + " AND IsVoucherLock='false' " +
                    " And PkVoucherNo>(Select Max(PkVoucherNo)-100  from TVoucherEntry Where VoucherTypeCode=" + VchType.Sales + " AND IsVoucherLock='false') " +
                    " Order by VoucherDate, VoucherUserNo ").Table;

                if (dtSearch.Rows.Count > 0 && ID == 0)
                {
                    ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    FillControls();
                    SetNavigation();
                    setDisplay(true);
                }
                else
                {
                    if (ID != 0)
                    {
                        FillControls();
                        SetNavigation();
                        btnFirst.Visible = false; btnPrev.Visible = false; btnNext.Visible = false; btnLast.Visible = false;
                        btnNew.Visible = false; btnUpdate.Visible = false; btnCancel.Visible = false; btnSave.Visible = false;
                        btnSearch.Visible = false; btnShortcut.Visible = false; btnNewCustomer.Visible = false; btnMixMode.Visible = false;
                        btnAdvanceSearch.Visible = false;
                        btnBillCancel.Visible = false; btnTodaysSales.Visible = false; btnSalesRegister.Visible = false;
                        btnOrderType.Visible = false; btnPrint.Visible = false;
                        pnlDiscountDetails.Visible = false; btnInsScheme.Visible = true; btnInsSchemeInfo.Visible = true; btnCCSave.Visible = false;
                        btnCashSave.Visible = false; btnCreditSave.Visible = false;
                        btnShowDetails.Location = new Point(btnShowDetails.Location.X, btnShowDetails.Location.Y + btnShowDetails.Height + 7);
                        dgBill.Enabled = true;
                        //for (int i = 0; i < dgBill.ColumnCount; i++)
                        //{
                        //    dgBill.Columns[i].ReadOnly = true;
                        //}
                        dgBill.KeyDown -= new KeyEventHandler(dgBill_KeyDown);
                    }
                    else setDisplay(true);
                }
                ShowLastBill();
                //setDisplay(true);
                btnNew.Focus();
                KeyDownFormat(this.Controls);

                DataTable dtSettings = ObjFunction.GetDataView("Select PKSettingNo,Cast(SettingDescription As Numeric) AS ColIndex  From MSettings Where SettingTypeNo=4 order by Cast(SettingDescription As Numeric)").Table;
                for (int i = 0; i < dtSettings.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(Convert.ToInt32(dtSettings.Rows[i].ItemArray[0].ToString()))) == true)
                        dgBill.Columns[Convert.ToInt32(dtSettings.Rows[i]["ColIndex"].ToString())].Visible = true;
                    else
                        dgBill.Columns[Convert.ToInt32(dtSettings.Rows[i]["ColIndex"].ToString())].Visible = false;
                }
                txtGrandTotal.Font = new Font("Verdana", 18, FontStyle.Bold);
                txtGrandTotal.ForeColor = Color.White;

                lblGrandTotal.Font = new Font("Verdana", 22, FontStyle.Bold);
                lblGrandTotal.ForeColor = Color.White;

                lblCreditLimit.Font = new Font("Verdana", 9, FontStyle.Bold);
                lblLastBillAmt.Font = new Font("Verdana", 9, FontStyle.Bold);
                lblBrandName.Font = new Font("Verdana", 10, FontStyle.Bold);



                new GridSearch(dgItemList, 1, 2);
                new GridSearch(dgParkingBills, 1);
                formatPics();
                DisplayChargANDDisc();
                DisplayRateType();

                //dgPurHistory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)
                //            | System.Windows.Forms.AnchorStyles.Left)));

                dtpBillDate.CustomFormat = "dd-MMM-yy"; dtpBillDate.Width = 90;
                //dtpBillTime.Width = 90;
                dgBill.BackgroundColor = Color.FromArgb(255, 255, 210);
                btnSalesRegister.BackColor = Color.FromArgb(255, 128, 0);

                btnNewCustomer.BackColor = Color.FromArgb(255, 128, 0);
                btnTodaysSales.BackColor = Color.FromArgb(255, 128, 0);
                btnShowDetails.BackColor = Color.FromArgb(255, 128, 0);
                btnShortcut.BackColor = Color.FromArgb(255, 128, 0);

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_Bilingual)) == false)
                {
                    plnPrinting.Visible = false;
                    rbEnglish.Checked = true;
                    dgItemList.Columns[2].Visible = false;
                }
                else
                {
                    plnPrinting.Visible = true;
                    if (Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.O_DefaultBillPrint)) == 1)
                        rbEnglish.Checked = true;
                    else
                        rbMarathi.Checked = true;

                    if (Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.O_Language)) == 2)
                        rbMarathi.Text = "marazI";
                    else if (Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.O_Language)) == 3)
                        rbMarathi.Text = "ihMdI";
                }
                //btnMixMode.Visible = true;
                plnPrinting.Enabled = true;
                rbEnglish.Enabled = true;
                rbMarathi.Enabled = true;
                cmbRateType.Enabled = false;
                txtSchemeDisc.Enabled = false;
                DataTable dtLang = ObjFunction.GetDataView("Select * From MLanguage Where LanguageNo=1").Table;
                lstGroup1.Font = new Font(dtLang.Rows[0].ItemArray[2].ToString(), Convert.ToInt64(dtLang.Rows[0].ItemArray[3].ToString()), (Convert.ToBoolean(dtLang.Rows[0].ItemArray[4].ToString()) == true) ? FontStyle.Bold : FontStyle.Regular);
                //for (int i = 0; i < dgBill.Columns.Count; i++) dgBill.Columns[i].Visible = true;
                DiscountItemMasterWise = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_DiscountItemMasterWise));
                strRateType = ObjFunction.GetComboValueString(cmbRateType);
                bw_ItemSearch.RunWorkerAsync();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

            //  BTSacn.WorkerReportsProgress = true;
            // BTSacn.RunWorkerAsync();
        }

        private void InitItemLeveDiscount()
        {
            dtItemLevelDisc = ObjFunction.GetDataView("SELECT  MItemDiscount.PkSrNo AS ItemDiscNo, MItemBrandDiscount.PkSrNo AS ItemBrandDiscNo, MItemDiscountDetails.PkSrNo AS ItemDiscDetailsNo, MItemDiscountDetails.ItemNo, MItemDiscountDetails.FkRateSettingNo, MItemDiscountDetails.DiscPercentage,MItemDiscountDetails.MRP " +
                    " FROM MItemDiscountDetails INNER JOIN MItemBrandDiscount ON MItemDiscountDetails.ItemBrandDiscNo = MItemBrandDiscount.PkSrNo INNER JOIN MItemDiscount ON MItemBrandDiscount.ItemDiscNo = MItemDiscount.PkSrNo " +
                    " WHERE (MItemDiscount.PeriodFrom <= '" + dtpBillDate.Value.Date + "') AND (MItemDiscount.PeriodTo >= '" + dtpBillDate.Value.Date + "') AND (MItemDiscount.IsActive = 1) AND " +
                    " (MItemDiscountDetails.IsActive = 'true')").Table;
        }

        private void DisplayChargANDDisc()
        {
            try
            {
                lblChrg1.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_Charges1Display));
                lblChrg1.Text = ObjFunction.GetAppSettings(AppSettings.S_ChargeLabelName).ToString() + ":";
                txtChrgRupees1.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_Charges1Display));

                lblDisc1.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_Discount1Display));
                txtDiscount1.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_Discount1Display));
                txtDiscRupees1.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_Discount1Display));
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void DisplayRateType()
        {
            try
            {
                //Rate Type related.
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsDisplayRateType)) == true)
                {
                    lblRateType.Visible = true;
                    cmbRateType.Visible = true;
                }
                else
                {
                    lblRateType.Visible = false;
                    cmbRateType.Visible = false;
                }

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_RateTypeAskPassword)) == true)
                    cmbRateType.Enabled = false;
                else cmbRateType.Enabled = true;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void formatPics()
        {
            try
            {
                txtBrandFilter.BackColor = Color.Yellow;
                panel3.Width = pnlItemName.Width = dgItemList.Width = 1300;//560;
                dgItemList.Width = 1290;
                pnlItemName.Height = 500;
                pnlItemName.Top = 90;
                pnlItemName.Left = 10;

                pnlGroup1.Top = 88;

                dgItemList.RowTemplate.DefaultCellStyle.Font = null;
                dgItemList.Columns[2].DefaultCellStyle.Font = ObjFunction.GetLangFont();
                //dgItemList.Columns[1].DefaultCellStyle.Font = ObjFunction.GetFont(FontStyle.Bold, 16);
                //dgItemList.Columns[4].DefaultCellStyle.Font = ObjFunction.GetFont(FontStyle.Bold, 12);
                //dgItemList.Columns[6].DefaultCellStyle.Font = ObjFunction.GetFont(FontStyle.Bold, 12);
                dgItemList.DefaultCellStyle.Font = ObjFunction.GetFont(FontStyle.Bold, 10);
                dgItemList.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dgItemList.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dgItemList.Columns[3].Width = 100;
                dgItemList.Columns[5].Width = 100;
                dgItemList.Columns[6].Width = 100;
                dgItemList.RowTemplate.Height = 25;

                lstGroup1.Height = 400;
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_Bilingual)) == true)
                {
                    pnlGroup1.Left = 50;//150;
                    pnlGroup1.Width = 815;
                    lstGroup1Lang.Height = lstGroup1.Height;
                    lstGroup1.Font = ObjFunction.GetFont(FontStyle.Regular, 11);
                    lstGroup1Lang.Font = ObjFunction.GetLangFont();

                }
                else
                {
                    pnlGroup1.Left = 150;
                    pnlGroup1.Width = lstGroup1.Width + 10;
                    txtBrandFilter.Width = lstGroup1.Width - 2;
                }
                //  pnlGroup1.Height = 290;//205;// 220;
                pnlGroup1.Height = lstGroup1.Height + 5;
                pnlGroup2.Top = 88;
                pnlGroup2.Left = 100;
                pnlGroup2.Width = 300;
                pnlGroup2.Height = 220;

                pnlUOM.Top = 88;
                pnlUOM.Left = 372;
                pnlUOM.Width = 120;
                pnlUOM.Height = 80;

                pnlRate.Top = 88;
                pnlRate.Left = 430;
                pnlRate.Width = 120;
                pnlRate.Height = 80;

                pnlSalePurHistory.Width = 720;
                pnlSalePurHistory.Height = 235;
                pnlSalePurHistory.Top = pnlItemName.Height + 88;
                pnlSalePurHistory.Left = pnlItemName.Left;

                btnMixMode.Left = btnExit.Left;
                btnMixMode.Top = btnExit.Top + btnExit.Height + 5;
                btnInsScheme.Left = btnShortcut.Left;
                btnInsScheme.Top = btnShortcut.Bottom + 35;
                btnInsSchemeInfo.Left = btnShortcut.Left;
                btnInsSchemeInfo.Top = btnShortcut.Bottom + 5;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void initItemQuery()
        {
            try
            {
                DataTable dtItemQuery = new DataTable();
                dtItemQuery = ObjFunction.GetDataView("SELECT * from MItemNameDisplayType WHERE ItemNameTypeNo = " + ItemNameType).Table;

                if (dtItemQuery.Rows.Count == 1)
                {
                    int qCount = 0;
                    for (int i = 1; i < 4; i++)
                    {
                        if (dtItemQuery.Rows[0]["Query" + i] != null && dtItemQuery.Rows[0]["Query" + i].ToString().Trim().Length > 0)
                        {
                            qCount++;
                        }
                    }

                    strItemQuery = new string[qCount];
                    strItemQuery_last = new string[qCount];
                    for (int i = 0; i < strItemQuery_last.Length; i++)
                    {
                        strItemQuery_last[i] = "";
                    }
                    qCount = 0;
                    for (int i = 1; i < 4; i++)
                    {
                        if (dtItemQuery.Rows[0]["Query" + i] != null && dtItemQuery.Rows[0]["Query" + i].ToString().Trim().Length > 0)
                        {
                            strItemQuery[qCount] = dtItemQuery.Rows[0]["Query" + i].ToString().Trim();
                            qCount++;
                        }
                    }

                    iItemNameStartIndex = Convert.ToInt32(dtItemQuery.Rows[0]["StartIndex"].ToString());
                    Param1Value = dtItemQuery.Rows[0]["Param1Value"].ToString();
                    Param2Value = dtItemQuery.Rows[0]["Param2Value"].ToString();
                }
                else
                {
                    OMMessageBox.Show("Please Select Valid Item Name display type in Sales Setting Form ...");
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void InitControls()
        {
            try
            {
                flagParking = false;
                MixModeFlag = false; MixModeVal = 0;
                PrintAsk = 0;
                VoucherUserNo = 0;
                OrderType = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_OrderType));
                FillRateType();
                SetRateType(RateTypeNo);
                lblCreditLimit.Text = "";
                cmbPaymentType.SelectedIndex = 0;
                txtHRs.Text = "0.00";
                txtKg.Text = "0.00";
                dtpBillDate.Value = DBGetVal.ServerTime;
                dtpBillTime.Value = DBGetVal.ServerTime;
                lblCancelBll.Text = "";
                while (dgBill.Rows.Count > 0)
                {
                    dgBill.Rows.RemoveAt(0);
                }
                //for (int i = 0; i < 20; i++)
                //{
                //    dgBill.Rows.Add();
                //}
                dgBill.Rows.Add();
                ObjFunction.GetFinancialYear(dtpBillDate.Value, out dtFrom, out dtTo);
                txtInvNo.Text = (ObjQry.ReturnLong("Select max(VoucherUserNo) from TVoucherEntry Where VoucherTypeCode=" + VoucherType + " AND VoucherDate>='" + dtFrom.Date + "' AND VoucherDate<='" + dtTo.Date + "'", CommonFunctions.ConStr) + 1).ToString();
                //txtInvNo.Text = (ObjQry.ReturnLong("Select max(VoucherUserNo) from TVoucherEntry Where VoucherTypeCode=" + VoucherType + "", CommonFunctions.ConStr) + 1).ToString();

                string sqlQuery = " SELECT 0 AS Sr, MStockItems.ItemName, TStock.Quantity, MUOM.UOMName, TStock.Rate, TStock.Amount,MStockBarcode.Barcode, " +
                                 " TStock.PkStockTrnNo, MStockBarcode.PkStockBarcodeNo,TVoucherEntry.PkVoucherNo, MStockItems.ItemNo, MUOM.UOMNo " +
                                 " FROM TVoucherDetails INNER JOIN TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo " +
                                 " INNER JOIN TStock INNER JOIN MStockItems ON TStock.ItemNo = MStockItems.ItemNo ON " +
                                 " TVoucherDetails.PkVoucherTrnNo = TStock.FkVoucherTrnNo INNER JOIN " +
                                 " MStockBarcode ON MStockItems.ItemNo = MStockBarcode.ItemNo INNER JOIN " +
                                 " MUOM ON MStockItems.UOMDefault = MUOM.UOMNo " +
                                 " WHERE (MStockBarcode.Barcode = '') AND (TVoucherDetails.VoucherSrNo = 1) AND " +
                                 " (TVoucherEntry.VoucherTypeCode IN (9, 21))";
                dt = ObjFunction.GetDataView(sqlQuery).Table;

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_StopOnDate)) == true) dtpBillDate.Focus();
                else if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_StopOnParty)) == true) cmbPartyName.Focus();
                else if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_StopOnRateType)) == true) cmbRateType.Focus();
                else if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_StopOnTaxType)) == true) cmbTaxType.Focus();
                else dgBill.Focus();
                //dgBill.Focus();
                dgBill.CurrentCell = dgBill[1, 0];
                cmbPartyName.SelectedValue = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_DefaultPartyAC));
                if (ObjFunction.GetComboValue(cmbPartyName) == Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_PartyAC)))// && (ObjFunction.GetComboValue(cmbPaymentType) == 3))
                {
                    // cmbPaymentType.SelectedValue = 2;
                    // cmbPaymentType.Enabled = false;
                }
                else
                    cmbPaymentType.Enabled = true;

                txtDiscount1.Text = "0.00";
                txtDiscRupees1.Text = "0.00";
                txtChrgRupees1.Text = "0.00";
                txtAddFreight.Text = "0.00";
                txtSubTotal.Text = "0.00"; lblBillItem.Text = "0"; lblBilExchangeItem.Text = "0";
                txtGrandTotal.Text = "0.00"; txtTotalDisc.Text = "0.00"; txtTotalTax.Text = "0.00";
                txtSchemeDisc.Text = "0.00"; txtOtherDisc.Text = "0.00"; txtRoundOff.Text = "0.00";
                txtSchemeDisc.Enabled = false;
                chkItemLevelDisc.Checked = true; chkFooterLevelDisc.Checked = true;
                BindGridPayType(0);
                BindPayChequeDetails(0);
                BindPayCreditDetails(0);
                pnlPartial.Visible = false;
                btnMixMode.Text = "Mix\r\nMode\r\n(F3)";
                btnBillCancel.Visible = DBGetVal.IsAdmin;
                ParkBillNo = 0;
                dtTRewardDtls = ObjFunction.GetDataView("SELECT PkSrNo, RewardNo, SchemeNo, SchemeDetailsNo, SchemeType, DiscPer, DiscAmount, SchemeAmount,0 As Status,SchemeAcheiverNo,AchievedNoOfTime,ActualNoOfTime FROM TRewardDetails WHERE (PkSrNo = 0)").Table;
                dtTRewardToFrom = ObjFunction.GetDataView("SELECT 0 AS TypeNo,PkSrNo, RewardNo, RewardDetailsNo,SchemeDetailsNo, SchemeFromNo, FkStockNo,0 As 'ItemNo' FROM TRewardFrom WHERE (PkSrNo = 0)").Table;
                tempDate = dtpBillDate.Value.Date;
                tempPartyNo = ObjFunction.GetComboValue(cmbPartyName);
                ShowHideScheme(false);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillControls()
        {
            try
            {
                dtpBillDate.MinDate = Convert.ToDateTime("01-01-1900");
                tVoucherEntry = dbTVoucherEntry.ModifyTVoucherEntryByID(ID);
                VoucherUserNo = Convert.ToInt32(tVoucherEntry.VoucherUserNo);
                txtInvNo.Text = tVoucherEntry.VoucherUserNo.ToString();
                dtpBillDate.Value = tVoucherEntry.VoucherDate;
                dtpBillTime.Value = tVoucherEntry.VoucherTime;
                cmbPaymentType.SelectedValue = tVoucherEntry.PayTypeNo.ToString();
                PayType = tVoucherEntry.PayTypeNo;
                tempDate = dtpBillDate.Value.Date;

                cmbTaxType.SelectedValue = tVoucherEntry.TaxTypeNo.ToString();
                FillRateType(tVoucherEntry.RateTypeNo);
                SetRateType(tVoucherEntry.RateTypeNo);
                cmbTaxType.Enabled = false;
                cmbRateType.Enabled = false;
                MixModeFlag = false; MixModeVal = tVoucherEntry.MixMode;
                txtSubTotal.Text = "0.00";
                txtTotalDisc.Text = "0.00";
                txtDiscount1.Text = "0.00";
                txtTotalDisc.Text = "0.00";
                txtTotalTax.Text = "0.00";
                txtChrgRupees1.Text = "0.00";
                txtAddFreight.Text = "0.00";
                txtGrandTotal.Text = "0.00";
                txtOtherDisc.Text = "0.00";
                txtSchemeDisc.Text = "0.00";
                //manali
                OrderType = tVoucherEntry.OrderType;
                txtRemark.Text = tVoucherEntry.Remark;
                txtPartyName.Text = tVoucherEntry.Reference;
                //end

                DataTable dt = ObjFunction.GetDataView("Select Case When Debit<>0 then Debit Else Credit End,LedgerNo,SrNo,SignCode From TVoucherDetails Where FKVoucherNo=" + ID + " order by VoucherSrNo").Table;
                //double subTot = ObjQry.ReturnDouble("Select sum(Debit) from TVoucherDetails  Where FKVoucherNo=" + ID + " and LedgerNo!=1 ",CommonFunctions.ConStr);
                double subTot = ObjQry.ReturnDouble("Select sum(Case When(SrNo<>508 AND SrNo<>506)Then Debit else -Credit End) from TVoucherDetails  Where FKVoucherNo=" + ID + " ", CommonFunctions.ConStr);
                txtSubTotal.Text = subTot.ToString();
                long partyNo = ObjQry.ReturnLong("Select LedgerNo from TVoucherDetails  Where FKVoucherNo=" + ID + " AND VoucherSrNo=1", CommonFunctions.ConStr);
                ObjFunction.FillComb(cmbPartyName, "Select LedgerNo,LedgerName From MLedger Where GroupNo in (" + GroupType.SundryDebtors + ") and (IsActive='true' or LedgerNo = " + partyNo + ") order by LedgerName");
                cmbPartyName.SelectedValue = partyNo;
                tempPartyNo = partyNo;

                PartyNo = ObjFunction.GetComboValue(cmbPartyName);
                txtChrgRupees1.Text = "0"; txtAddFreight.Text = "0.00";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //if ((dt.Rows[i].ItemArray[1].ToString() == dtSalesSetting.Rows[0].ItemArray[1].ToString()) || (Convert.ToInt64(dt.Rows[i].ItemArray[1].ToString()) == 0))
                    //if (dt.Rows[i].ItemArray[1].ToString() == dgBill.Rows[i].Cells[13].Value)
                    //txtSubTotal.Text = Convert.ToDouble(dt.Rows[i].ItemArray[0].ToString()).ToString("0.00");
                    //txtSubTotal.Text=subTot.ToString();

                    if (dt.Rows[i].ItemArray[2].ToString() == Others.Discount2.ToString())
                    {
                        //txtDiscount1.Text = ((Convert.ToDouble(dt.Rows[i].ItemArray[0].ToString()) * 100) / subTot).ToString("0.00");
                        if (dt.Rows[i].ItemArray[3].ToString() == "1")
                            txtSchemeDisc.Text = (-Convert.ToDouble(dt.Rows[i].ItemArray[0].ToString())).ToString("0.00");
                        else
                            txtSchemeDisc.Text = Convert.ToDouble(dt.Rows[i].ItemArray[0].ToString()).ToString("0.00");
                        Control_Leave((object)txtSchemeDisc, new EventArgs());
                    }
                    else if (dt.Rows[i].ItemArray[2].ToString() == Others.Discount3.ToString())
                    {
                        //txtDiscount1.Text = ((Convert.ToDouble(dt.Rows[i].ItemArray[0].ToString()) * 100) / subTot).ToString("0.00");
                        txtOtherDisc.Text = Convert.ToDouble(dt.Rows[i].ItemArray[0].ToString()).ToString("0.00");
                        Control_Leave((object)txtOtherDisc, new EventArgs());
                    }
                    else if (dt.Rows[i].ItemArray[2].ToString() == Others.Charges1.ToString())
                    {
                        txtChrgRupees1.Text = Convert.ToDouble(dt.Rows[i].ItemArray[0].ToString()).ToString("0.00");
                    }
                    else if (dt.Rows[i].ItemArray[2].ToString() == Others.Charges2.ToString())
                    {
                        txtAddFreight.Text = Convert.ToDouble(dt.Rows[i].ItemArray[0].ToString()).ToString("0.00");
                    }
                }

                txtDiscount1.Text = tVoucherEntry.DiscPercent.ToString("0.00");
                txtDiscRupees1.Text = tVoucherEntry.DiscAmt.ToString("0.00");
                MixModeVal = tVoucherEntry.MixMode;
                chkItemLevelDisc.Checked = tVoucherEntry.IsItemLevelDisc;
                chkFooterLevelDisc.Checked = tVoucherEntry.IsFooterLevelDisc;
                ShowLastBill();
                FillCreditLimit();
                btnOrderType.Text = dtOrderType.Select("OrderTypeNo=" + OrderType)[0].ItemArray[1].ToString();
                btnOrderType.BackColor = Color.FromName(dtOrderType.Select("OrderTypeNo=" + OrderType)[0].ItemArray[2].ToString());
                txtGrandTotal.Text = ((Convert.ToDouble(txtSubTotal.Text) - Convert.ToDouble(txtTotalDisc.Text)) + Convert.ToDouble(txtTotalTax.Text)).ToString("0.00");
                //cmbPaymentType.SelectedValue = ObjQry.ReturnLong("Select LedgerNo From TVoucherDetails Where VoucherSrNo=2 AND FKVoucherNo=" + ID + "", CommonFunctions.ConStr).ToString();
                txtHRs.Text = tVoucherEntry.HamaliRs.ToString("0.00");
                FillGrid();

                DataTable dtPartial = ObjFunction.GetDataView("Select Credit From TVoucherDetails Where FKVoucherNo=" + ID + " AND VoucherSrNo in (2,3) AND LedgerNo in(1,3) ").Table;
                if (dtPartial.Rows.Count == 2)
                {
                    //txtCash.Text = Convert.ToDouble(dtPartial.Rows[0].ItemArray[0]).ToString("0.00");
                    //txtCredit.Text = Convert.ToDouble(dtPartial.Rows[1].ItemArray[0]).ToString("0.00");
                    txtTotalAmt.Text = "0.00";
                }

                long LastBillNo = ObjQry.ReturnLong("Select IsNull(Max(PkVoucherNo),0) From TVoucherEntry Where VoucherTypeCode=" + VoucherType + " AND  PkVoucherNo<" + ID + "", CommonFunctions.ConStr);
                //dt = ObjFunction.GetDataView("SELECT IsNull(SUM(TStock.Quantity),0) AS Quantity, IsNull(SUM(TStock.Amount),0) AS Amount FROM TVoucherDetails INNER JOIN TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN "+
                //    " TStock ON TVoucherDetails.PkVoucherTrnNo = TStock.FkVoucherTrnNo WHERE (TVoucherDetails.VoucherSrNo = 1) AND (TVoucherEntry.VoucherTypeCode = "+ VoucherType +") AND (TVoucherEntry.PkVoucherNo = "+ LastBillNo +")").Table;

                dt = ObjFunction.GetDataView("SELECT IsNull(SUM(TStock.Quantity),0) AS Quantity, IsNull(SUM(TStock.Amount),0) AS Amount FROM TVoucherDetails INNER JOIN TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN " +
                    " TStock ON TVoucherDetails.PkVoucherTrnNo = TStock.FkVoucherTrnNo WHERE (TVoucherEntry.VoucherTypeCode = " + VoucherType + ") AND (TVoucherEntry.PkVoucherNo = " + LastBillNo + ")").Table;

                if (dt.Rows.Count > 0)
                {
                    lblLastAmt.Text = "Amount : ";
                    lblLastBillAmt.Text = ObjQry.ReturnDouble("Select BilledAmount From TVoucherEntry Where PKVoucherNo=" + LastBillNo + "", CommonFunctions.ConStr).ToString();
                    //Convert.ToDouble(dt.Rows[0].ItemArray[1].ToString()).ToString();
                    //btnLastBillAmt.Text = "Last Bill " + Convert.ToDouble(dt.Rows[0].ItemArray[1].ToString()).ToString("0.00");
                    lblLastBillQty.Text = "Qty: " + dt.Rows[0].ItemArray[0].ToString();
                    // btnLastBillQty.Text = "Qty: " + dt.Rows[0].ItemArray[0].ToString();
                    lblLastPayment.Text = "Payment: " + ObjQry.ReturnString("SELECT MPayType.PayTypeName FROM MPayType INNER JOIN TVoucherEntry ON MPayType.PKPayTypeNo = TVoucherEntry.PayTypeNo WHERE (TVoucherEntry.PkVoucherNo = " + LastBillNo + ")", CommonFunctions.ConStr);
                    // btnPayment.Text = lblLastPayment.Text;
                }
                dtVchMainDetails = ObjFunction.GetDataView("Select * From TVoucherDetails Where FKVoucherNo=" + ID + "").Table;

                lblCancelBll.Text = "";
                btnUpdate.Visible = true; btnBillCancel.Visible = true; lblCancelBll.Text = "";
                if (tVoucherEntry.IsVoucherLock == true)
                {
                    btnUpdate.Visible = !tVoucherEntry.IsVoucherLock;
                    lblCancelBll.Text = "Bill Lock";
                    //BillFlag = false;
                }
                else if (tVoucherEntry.IsCancel == true)
                {
                    btnUpdate.Visible = !tVoucherEntry.IsCancel;
                    btnBillCancel.Visible = !tVoucherEntry.IsCancel;
                    lblCancelBll.Text = "Bill Cancel";
                    //BillFlag = true;
                }
                else if (ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo in ( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + ")", CommonFunctions.ConStr) > 1)
                {
                    btnUpdate.Visible = false;
                    btnBillCancel.Visible = false;
                    //BillFlag = false;
                }
                else if (ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo in ( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + " and TR.TypeOfRef in(6))", CommonFunctions.ConStr) > 1)
                {
                    btnUpdate.Visible = false;
                    btnBillCancel.Visible = false;
                    //BillFlag = false;
                }
                else if (ObjQry.ReturnInteger("SELECT  COUNT(*) FROM  TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo " +
                    " WHERE     (TVoucherEntry.Reference = " + tVoucherEntry.VoucherUserNo + ") AND (TVoucherEntry.VoucherTypeCode = " + VchType.RejectionIn + ") AND (TVoucherDetails.VoucherSrNo = 1) AND (TVoucherDetails.LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + ")", CommonFunctions.ConStr) > 0)
                {
                    btnBillCancel.Visible = false;
                    //BillFlag = false;
                }
                else
                {
                    btnUpdate.Visible = true;
                    btnBillCancel.Visible = true;
                    //BillFlag = false;
                }

                //pnlCollectionDetails.Visible = false;
                dgCollectionDetails.DataSource = null;
                lblTotalCollection.Text = "0.00";
                lblOw.Text = "0.00";
                btnMixMode.Text = "Mix\r\nMode\r\n(F3)";
                dgCollectionDetails.RowsDefaultCellStyle.BackColor = Color.FromArgb(255, 255, 210);
                if ((ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo in ( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + ")", CommonFunctions.ConStr) > 1) && PayType == 3)
                {
                    btnMixMode.Visible = false;

                    DisplayCollectionDetails();
                }
                else if (PayType == 3 && tVoucherEntry.IsVoucherLock == false && tVoucherEntry.IsCancel == false)
                {
                    btnMixMode.Visible = true;
                    btnMixMode.Text = "Receipt\r\nMode";
                    DisplayCollectionDetails();
                }
                else
                    btnMixMode.Visible = false;


                FillTodaysSalesDetails();

                //  FillRewarddetails();//Nikhil
                btnInsScheme.Enabled = false;
                txtSchemeDisc.Enabled = false;


            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillRewarddetails()
        {
            try
            {
                string sql = " SELECT TRewardDetails.PkSrNo, TRewardDetails.RewardNo, TRewardDetails.SchemeNo, TRewardDetails.SchemeDetailsNo, TRewardDetails.SchemeType,TRewardDetails.DiscPer, TRewardDetails.DiscAmount, TRewardDetails.SchemeAmount,0 AS Status,TRewardDetails.SchemeAcheiverNo " +
                           " FROM TRewardDetails INNER JOIN TReward ON TRewardDetails.RewardNo = TReward.RewardNo " +
                           " WHERE (TReward.FkVoucherNo = " + ID + ") ";
                dtTRewardDtls = ObjFunction.GetDataView(sql).Table;
                //Instant Scheme Kamlesh
                btnInsSchemeInfo.Text = "Scheme Applied";
                if (dtTRewardDtls.Rows.Count != 0)
                {
                    //btnInsScheme.Visible = true;
                    btnInsSchemeInfo.Visible = true;

                    DataRow[] drMTD = dtTRewardDtls.Select("SchemeType = 1 or SchemeType = 5");//1=MTD or 5=PSKU
                    if (drMTD.Length > 0)
                    {

                        btnUpdate.Visible = false;
                        btnBillCancel.Visible = false;
                        if (drMTD[0]["SchemeType"].ToString().Equals("1"))
                            lblCancelBll.Text = "MTD Given";
                        else
                            lblCancelBll.Text = "PSKUC Given";
                        //BillFlag = false;
                    }
                }
                else
                {
                    //btnInsScheme.Visible = false;
                    btnInsSchemeInfo.Visible = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillTodaysSalesDetails()
        {
            try
            {
                //DataTable dt = ObjFunction.GetDataView("Exec GetSaleVouchEntryDayDtls " + VchType.Sales + "," + DBGetVal.CompanyNo + ",'" + DBGetVal.ServerTime.ToString("dd-MMM-yyyy") + "','" + DBGetVal.ServerTime.ToString("dd-MMM-yyyy") + "',1,0").Table;
                //lblCash.Text = dt.Compute("Sum(Cash)", "").ToString();
                //lblCheque.Text = dt.Compute("Sum(Cheque)", "").ToString();
                //lblCredit.Text = dt.Compute("Sum(Credit)", "").ToString();
                //lblCc.Text = dt.Compute("Sum(CC)", "").ToString();
                //lblFV.Text = dt.Compute("Sum(FV)", "").ToString();
                //lblTotAmt.Text = dt.Compute("Sum(TotalAmount)", "").ToString();
                //lblBillCnt.Text = dt.Rows.Count.ToString();


                DataTable dtPayType = ObjFunction.GetDataView("Exec GetPayTypeDetails '" + DBGetVal.ServerTime.ToString("dd-MMM-yyyy") + "','" + DBGetVal.ServerTime.ToString("dd-MMM-yyyy") + "'," + VchType.Sales + ",0," + DBGetVal.CompanyNo + "").Table;
                dgSalesDetails.DataSource = dtPayType.DefaultView;
                dgSalesDetails.ColumnHeadersVisible = false;
                dgSalesDetails.Columns[0].Visible = false;
                dgSalesDetails.Columns[1].Visible = false;
                dgSalesDetails.Columns[2].Width = 100;
                dgSalesDetails.Columns[3].Visible = false;
                dgSalesDetails.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgSalesDetails.Columns[4].Width = 112;
                dgSalesDetails.Columns[5].Visible = false;
                dgSalesDetails.RowsDefaultCellStyle.BackColor = Color.FromArgb(255, 255, 210);
                dgSalesDetails.BackgroundColor = Color.FromArgb(255, 255, 210);
                dgSalesDetails.Height = 107;
                lblTotAmt.Text = dtPayType.Compute("Sum(Amount)", "").ToString();
                lblBillCnt.Text = ObjQry.ReturnLong("Select Count(*) From TVoucherEntry Where VoucherDAte='" + DBGetVal.ServerTime.ToString("dd-MMM-yyyy") + "' AND VoucherTypeCode=" + VchType.Sales + " AND IsCancel='false'", CommonFunctions.ConStr).ToString();// dtPayType.Compute("Sum(NoOfBills)", "").ToString();

                lblTodaysSales.Font = new Font("Verdana", 10, FontStyle.Bold);
                lblTotAmt.Font = new Font("Verdana", 8, FontStyle.Bold);
                lblBillCnt.Font = new Font("Verdana", 8, FontStyle.Bold);
                if (lblCash.Text == "") lblCash.Text = "0.00";
                if (lblCheque.Text == "") lblCheque.Text = "0.00";
                if (lblCredit.Text == "") lblCredit.Text = "0.00";
                if (lblCc.Text == "") lblCc.Text = "0.00";
                if (lblFV.Text == "") lblFV.Text = "0.00";
                if (lblTotAmt.Text == "") lblTotAmt.Text = "0.00";
                if (lblBillCnt.Text == "") lblBillCnt.Text = "0.00";
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillCreditLimit()
        {
            try
            {
                lblCreditLimit.Text = "Cr Limit : 0\r\n Cr Bal : 0";
                if (ObjFunction.GetComboValue(cmbPartyName) != Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_PartyAC)))// && (ObjFunction.GetComboValue(cmbPaymentType) == 3))
                {
                    double CreditLimit = ObjQry.ReturnDouble("Select isNull((CreditLimit),0) from MLedgerDetails where LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + "", CommonFunctions.ConStr);
                    lblCreditLimit.Text = "Cr Limit : " + CreditLimit.ToString();
                    double OpBal = ObjQry.ReturnDouble("Select case when signCode=2 then isnull(OpeningBalance,0) else isNull(OpeningBalance,0)*-1 end As OpeningBalance From MLedger Where LedgerNo =" + ObjFunction.GetComboValue(cmbPartyName) + "", CommonFunctions.ConStr);
                    double AvlBal = OpBal + ObjQry.ReturnDouble("Select isNull(Sum(case when signCode=2 then isnull(TVoucherDetails.Credit,0) else isNull(TVoucherDetails.Debit,0)*-1 end),0) FROM TVoucherDetails INNER JOIN " +
                                    " TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo " +
                                    " where TVoucherDetails.LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " and TVoucherEntry.IsCancel='false'", CommonFunctions.ConStr);
                    //double AvlBal = OpBal + ObjQry.ReturnDouble("Select isNull(Sum(Amount),0) FROM TVoucherDetails INNER JOIN " +
                    //                " TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN " +
                    //                " TVoucherRefDetails ON TVoucherDetails.PkVoucherTrnNo = TVoucherRefDetails.FkVoucherTrnNo where TVoucherRefDetails.LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " and TypeOfRef=3 and TVoucherEntry.IsCancel='false'", CommonFunctions.ConStr);
                    //double RecBal = ObjQry.ReturnDouble("Select isNull(Sum(Amount),0) from TVoucherRefDetails where LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " and TypeOfRef in(2,5)", CommonFunctions.ConStr);
                    //lblCreditLimit.Text += "\r\n Cr Bal : " + (CreditLimit - (AvlBal - RecBal)).ToString();
                    AvlBal = ((AvlBal + CreditLimit) * 1);
                    lblCreditLimit.Text += "\r\nCr Bal : " + AvlBal.ToString();
                    if (AvlBal >= 0)
                    {
                        lblCreditLimit.ForeColor = Color.Maroon;
                    }
                    else if (AvlBal < 0)
                    {
                        lblCreditLimit.ForeColor = Color.Red;
                    }
                    btnCreditSave.Enabled = true;
                    if (ID == 0) btnMixMode.Visible = true;
                }
                else
                {
                    btnCreditSave.Enabled = false;
                    if (btnSave.Visible == true)
                        btnMixMode.Visible = true;//btnMixMode.Visible = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void CheckSchemeEnroll()
        {
            try
            {
                EnrFlag = true;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillGrid()
        {
            try
            {
                dgBill.Rows.Clear();
                string sqlQuery = "";

                sqlQuery = "SELECT 0 AS Sr,MStockItems.ShowItemName AS ItemName, TStock.Quantity, MUOM.UOMName,cast(MRateSetting.MRP as Numeric(18,2)) AS MRP, " +
                        " TStock.Rate, TStock.NetRate, TStock.DiscPercentage, TStock.DiscAmount, TStock.DiscRupees, TStock.DiscPercentage2, TStock.DiscAmount2, TStock.DiscRupees2, " +
                        " TStock.NetAmount AS NetAmt, TStock.Amount, MStockBarcode.Barcode, TStock.PkStockTrnNo, MStockBarcode.PkStockBarcodeNo, TVoucherDetails.PkVoucherTrnNo, " +
                        " MStockItems.ItemNo, MUOM.UOMNo, MItemTaxInfo.TaxLedgerNo, MItemTaxInfo.SalesLedgerNo, TStock.FkRateSettingNo, MItemTaxInfo.PkSrNo, " +
                        " MRateSetting.StockConversion, TStock.Quantity * MRateSetting.StockConversion AS ActualQty, MRateSetting.MKTQty AS MKTQuantity, TStock.TaxPercentage, " +
                        " TStock.TaxAmount, ISNULL ((SELECT PkVoucherTrnNo FROM TVoucherDetails AS SV WHERE (CompanyNo = TVoucherDetails.CompanyNo) AND (LedgerNo = MItemTaxInfo.SalesLedgerNo) AND (FkVoucherNo = TVoucherDetails.FkVoucherNo)), 0)  " +
                        " AS SalesVchNo, ISNULL ((SELECT     PkVoucherTrnNo FROM TVoucherDetails AS TXV WHERE (CompanyNo = TVoucherDetails.CompanyNo) AND (LedgerNo = MItemTaxInfo.TaxLedgerNo) AND (FkVoucherNo = TVoucherDetails.FkVoucherNo)), 0) " +
                        " AS TaxVchNo, MStockItems.CompanyNo, " +
                        " (Case When(TStock.SchemeDetailsNo=0) Then '' else (Cast(TStock.SchemeDetailsNo AS Varchar)) End ) AS SchemeDetailsNo ," +
                        " TStock.SchemeFromNo , " +
                        " TStock.SchemeToNo , " +
                        " null AS 'RewardFromNo', null AS 'RewardToNo',NULL  AS 'ItemLevelDiscNo', NULL AS 'FKItemLevelDiscNo' , " +
                        " TStock.GodownNo,IsNull(TStock.DiscountType,0),IsNull(TStock.HamaliInKg ,0) As HamaliInKg," +
                        " TStock.HSNCode, TStock.IGSTPercent, TStock.IGSTAmount, TStock.CGSTPercent, TStock.CGSTAmount, " +
                        " TStock.SGSTPercent, TStock.SGSTAmount, TStock.UTGSTPercent, TStock.UTGSTAmount," +
                        " TStock.CessPercent, TStock.CessAmount,MStockItems.IsQtyRead " +
                        " FROM MStockItems INNER JOIN TStock ON MStockItems.ItemNo = TStock.ItemNo INNER JOIN TVoucherDetails ON TStock.FkVoucherTrnNo = TVoucherDetails.PkVoucherTrnNo INNER JOIN " +
                        " MItemTaxInfo ON TStock.FkItemTaxInfo = MItemTaxInfo.PkSrNo INNER JOIN MStockBarcode ON TStock.FkStockBarCodeNo = MStockBarcode.PkStockBarcodeNo INNER JOIN MUOM ON TStock.FkUomNo = MUOM.UOMNo INNER JOIN MRateSetting ON TStock.FkRateSettingNo = MRateSetting.PkSrNo " +
                        " WHERE      (TVoucherDetails.FkVoucherNo = " + ID + ") ORDER BY TStock.PkStockTrnNo";//(TVoucherDetails.Debit <> 0 ) AND

                dt = ObjFunction.GetDataView(sqlQuery).Table;
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    dgBill.Rows.Add();
                    for (int i = 0; i < dgBill.Columns.Count; i++)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            dgBill.Rows[j].Cells[i].Value = dt.Rows[j].ItemArray[i].ToString();
                            //dgBill.Rows[j].Cells[i].ReadOnly = true;
                        }
                    }
                }
                lblBillItem.Text = "0"; lblBilExchangeItem.Text = "0";
                string strStkNo = "";
                for (int i = 0; i < dgBill.Rows.Count; i++)
                {
                    if (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value) >= 0)
                        lblBillItem.Text = (Convert.ToDouble(lblBillItem.Text) + Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value)).ToString();
                    else
                        lblBilExchangeItem.Text = (Convert.ToInt64(lblBilExchangeItem.Text) + Math.Abs(Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value))).ToString();
                    if (i == 0) strStkNo = dgBill.Rows[i].Cells[ColIndex.PkStockTrnNo].Value.ToString();
                    else strStkNo += "," + dgBill.Rows[i].Cells[ColIndex.PkStockTrnNo].Value.ToString();

                }
                dgBill.Rows.Add();
                dgBill.CurrentCell = dgBill[1, dgBill.Rows.Count - 1];
                CalculateTotal();
                BindGridPayType(ID);
                BindPayChequeDetails(ID);
                BindPayCreditDetails(ID);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void CalculateTotal()
        {
            try
            {
                txtSubTotal.Text = "0.00";
                lblBillItem.Text = "0";
                lblBilExchangeItem.Text = "0";
                txtGrandTotal.Text = "0.00";
                txtTotalDisc.Text = "0.00";
                txtTotalTax.Text = "0.00";
                double subTotal = 0, totalDisc = 0, totalChrg = 0, totalTax = 0, TotFinal = 0;
                double HamaliKg = 0;

                if (Validations() == true)
                {
                    for (int i = 0; i < dgBill.Rows.Count; i++)
                    {
                        if (dgBill.Rows[i].Cells[ColIndex.ItemNo].Value != null && dgBill.Rows[i].Cells[ColIndex.ItemNo].Value.ToString() != "")
                        {
                            if (dgBill.Rows[i].Cells[ColIndex.Quantity].Value == null) dgBill.Rows[i].Cells[ColIndex.Quantity].Value = 1;
                            if (dgBill.Rows[i].Cells[ColIndex.MKTQuantity].Value == null) dgBill.Rows[i].Cells[ColIndex.MKTQuantity].Value = 1;
                            if (dgBill.Rows[i].Cells[ColIndex.StockFactor].Value == null) dgBill.Rows[i].Cells[ColIndex.StockFactor].Value = 1;
                            if (dgBill.Rows[i].Cells[ColIndex.Rate].Value == null) dgBill.Rows[i].Cells[ColIndex.Rate].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.DiscRupees].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscRupees].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.DiscRupees2].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscRupees2].Value = "";
                            if (dgBill.Rows[i].Cells[ColIndex.SchemeDetailsNo].Value == null) dgBill.Rows[i].Cells[ColIndex.SchemeDetailsNo].Value = "";
                            if (dgBill.Rows[i].Cells[ColIndex.SchemeFromNo].Value == null) dgBill.Rows[i].Cells[ColIndex.SchemeFromNo].Value = "";
                            if (dgBill.Rows[i].Cells[ColIndex.SchemeToNo].Value == null) dgBill.Rows[i].Cells[ColIndex.SchemeToNo].Value = "";
                            if (dgBill.Rows[i].Cells[ColIndex.HamaliInKg].EditedFormattedValue.ToString() == "") dgBill.Rows[i].Cells[ColIndex.HamaliInKg].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.HSNCode].Value == null) dgBill.Rows[i].Cells[ColIndex.HSNCode].Value = "";



                            double DiscountValue = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscRupees].Value);

                            if (DiscountItemMasterWise)
                            {
                                if (Convert.ToInt32(dgBill.Rows[i].Cells[ColIndex.DiscountType].EditedFormattedValue) == 1)
                                {
                                    dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value = DiscountValue;
                                }
                            }


                            double Amount = Convert.ToDouble((((Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value)) * (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Rate].Value))) / (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.MKTQuantity].Value))).ToString("0.0000"));
                            double Disc1 = Convert.ToDouble(((Amount * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value)) / 100).ToString("0.0000"));
                            double Disc2 = 0;
                            if (Convert.ToInt32(dgBill.Rows[i].Cells[ColIndex.DiscountType].EditedFormattedValue) == 2)
                                Disc2 = Convert.ToDouble((((Amount - (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscRupees].Value) + Disc1)) * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value)) / 100).ToString("0.0000"));



                            double DiscAmt = (Disc1 + Disc2);// Convert.ToDouble(((Amount * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value)) / 100).ToString("0.0000"));

                            if (Convert.ToInt32(dgBill.Rows[i].Cells[ColIndex.DiscountType].EditedFormattedValue) == 2)
                                DiscAmt += Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscRupees].Value) + Convert.ToDouble((dgBill.Rows[i].Cells[ColIndex.DiscRupees2].Value.ToString() == "") ? "0" : dgBill.Rows[i].Cells[ColIndex.DiscRupees2].Value);

                            double tAmount = Amount - DiscAmt;
                            double TaxPerce = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.TaxPercentage].Value);
                            double TaxAmt = Convert.ToDouble(((tAmount * TaxPerce) / (100 + TaxPerce)).ToString("0.00"));
                            totalTax += TaxAmt;

                            if (TaxPerce > 0 && ObjFunction.GetComboValue(cmbTaxType) == GroupType.GST)// && dgBill.Rows[i].Cells[ColIndex.HSNCode].Value.ToString().Length > 0)
                            {
                                if (isLocalSale)
                                {
                                    dgBill.Rows[i].Cells[ColIndex.IGSTAmount].Value = 0;
                                    double CGSTPercent = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.CGSTPercent].Value);
                                    if (CGSTPercent > 0)
                                    {
                                        double CGSTValue = Convert.ToDouble(((CGSTPercent * TaxAmt) / TaxPerce).ToString("0.000"));
                                        dgBill.Rows[i].Cells[ColIndex.CGSTAmount].Value = CGSTValue;
                                    }

                                    double SGSTPercent = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.SGSTPercent].Value);
                                    if (SGSTPercent > 0)
                                    {
                                        double SGSTValue = Convert.ToDouble(((SGSTPercent * TaxAmt) / TaxPerce).ToString("0.000"));
                                        dgBill.Rows[i].Cells[ColIndex.SGSTAmount].Value = SGSTValue;
                                    }

                                    double CessPercent = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.CessPercent].Value);
                                    if (CessPercent > 0)
                                    {
                                        double CessValue = Convert.ToDouble(((CessPercent * TaxAmt) / TaxPerce).ToString("0.000"));
                                        dgBill.Rows[i].Cells[ColIndex.CessAmount].Value = CessValue;
                                    }
                                }
                                else
                                {
                                    dgBill.Rows[i].Cells[ColIndex.CGSTAmount].Value = 0;
                                    dgBill.Rows[i].Cells[ColIndex.SGSTAmount].Value = 0;
                                    dgBill.Rows[i].Cells[ColIndex.CessAmount].Value = 0;

                                    double IGSTPercent = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.IGSTPercent].Value);
                                    if (IGSTPercent > 0)
                                    {
                                        double IGSTValue = Convert.ToDouble(((IGSTPercent * TaxAmt) / TaxPerce).ToString("0.000"));
                                        dgBill.Rows[i].Cells[ColIndex.IGSTAmount].Value = IGSTValue;
                                    }

                                    double CessPercent = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.CessPercent].Value);
                                    if (CessPercent > 0)
                                    {
                                        double CessValue = Convert.ToDouble(((CessPercent * TaxAmt) / TaxPerce).ToString("0.000"));
                                        dgBill.Rows[i].Cells[ColIndex.CessAmount].Value = CessValue;
                                    }
                                }
                            }

                            double ttRate = (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value) == 0) ? 0 : (tAmount - TaxAmt) / Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value);


                            dgBill.Rows[i].Cells[ColIndex.Amount].Value = tAmount.ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.DiscAmount].Value = Disc1.ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value = Disc2.ToString("0.00");
                            //dgBill.Rows[i].Cells[ColIndex.DiscAmount].Value = Disc3.ToString("0.00");
                            //dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value = "0.00";

                            dgBill.Rows[i].Cells[ColIndex.TaxAmount].Value = TaxAmt.ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.NetRate].Value = ttRate.ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.NetAmt].Value = (ttRate * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value)).ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.ActualQty].Value = ((Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value)) * (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.StockFactor].Value)));

                            subTotal = subTotal + Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.NetAmt].Value);
                            totalDisc = totalDisc + DiscAmt;// Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscAmount].Value);

                            if (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value) >= 0)
                                lblBillItem.Text = (Convert.ToDouble(lblBillItem.Text) + Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value)).ToString();
                            else
                                lblBilExchangeItem.Text = (Convert.ToInt64(lblBilExchangeItem.Text) + Math.Abs(Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value))).ToString();

                            if (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.HamaliInKg].Value) > 0)
                                HamaliKg += (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.HamaliInKg].Value) * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value));

                        }
                    }
                    subTotal = Convert.ToDouble(subTotal.ToString("0.00"));// Math.Round(subTotal, 00);
                    txtSubTotal.Text = (subTotal + totalDisc).ToString("0.00");
                    txtTotalDisc.Text = totalDisc.ToString("0.00");

                    txtKg.Text = HamaliKg.ToString("0.00");
                    if (txtHRs.Text != "")
                        txtChrgRupees1.Text = (HamaliKg * Convert.ToDouble(txtHRs.Text)).ToString("0.00");

                    double TotalAmt = Convert.ToDouble(((Convert.ToDouble(txtSubTotal.Text) + totalTax) - Convert.ToDouble(txtTotalDisc.Text)).ToString("0.00"));
                    //txtDiscRupees1.Text = Convert.ToDouble((TotalAmt * Convert.ToDouble(txtDiscount1.Text)) / 100).ToString("0.00");
                    if (txtSchemeDisc.Text.Trim() == "") txtSchemeDisc.Text = "0.00";
                    if (txtOtherDisc.Text.Trim() == "") txtOtherDisc.Text = "0.00";
                    if (txtDiscRupees1.Text.Trim() == "") txtDiscRupees1.Text = "0.00";
                    TotalAmt -= Convert.ToDouble(txtSchemeDisc.Text) + Convert.ToDouble(txtDiscRupees1.Text);





                    totalDisc = Convert.ToDouble(txtDiscRupees1.Text) + Convert.ToDouble(txtSchemeDisc.Text) + Convert.ToDouble(txtOtherDisc.Text);
                    totalChrg = Convert.ToDouble(txtChrgRupees1.Text) + Convert.ToDouble((txtAddFreight.Text == "") ? "0.00" : txtAddFreight.Text);
                    txtTotalAnotherDisc.Text = totalDisc.ToString("0.00");
                    txtTotalChrgs.Text = totalChrg.ToString("0.00");
                    txtTotalTax.Text = totalTax.ToString("0.00");
                    if ((Convert.ToDouble(txtSubTotal.Text.Trim()) - Convert.ToDouble(txtTotalDisc.Text.Trim()) + Convert.ToDouble(txtTotalTax.Text.Trim())) != 0)
                        txtDiscount1.Text = Convert.ToDouble((100 * Convert.ToDouble(txtDiscRupees1.Text)) / (Convert.ToDouble(txtSubTotal.Text.Trim()) - Convert.ToDouble(txtTotalDisc.Text.Trim()) - (Convert.ToDouble(txtSchemeDisc.Text.Trim())) + Convert.ToDouble(txtTotalTax.Text.Trim()))).ToString("0.00");

                    totalTax = Convert.ToDouble(totalTax.ToString("0.00"));// Math.Round(totalTax, 00);
                    txtGrandTotal.Text = ((subTotal + totalTax + totalChrg) - totalDisc).ToString("0.00");
                    TotFinal = Math.Round(Convert.ToDouble(txtGrandTotal.Text), MidpointRounding.AwayFromZero);
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsBillRoundOff)) == true)
                    {
                        txtRoundOff.Text = (TotFinal - Convert.ToDouble(txtGrandTotal.Text)).ToString("0.00");
                        txtGrandTotal.Text = ((subTotal + totalTax + totalChrg + Convert.ToDouble(txtRoundOff.Text)) - totalDisc).ToString("0.00");
                    }
                    else
                        txtRoundOff.Text = "0.00";
                    long ControlUnder = ObjQry.ReturnLong("Select ControlUnder From MPayType Where PKPayTypeNo=" + ObjFunction.GetComboValue(cmbPaymentType) + "", CommonFunctions.ConStr);
                    if (ControlUnder == 4)
                    {
                        if (dgPayChqDetails.Rows.Count > 0)
                        {
                            dgPayChqDetails.Rows[0].Cells[4].Value = txtGrandTotal.Text;
                        }
                    }
                    else if (ControlUnder == 5)
                    {
                        if (dgPayCreditCardDetails.Rows.Count > 0)
                        {
                            dgPayCreditCardDetails.Rows[0].Cells[3].Value = txtGrandTotal.Text;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        //public void CalculateTotal()
        //{
        //    /*13-Apr-2014
        //    * Exception Handling Purpose.
        //    * */
        //    try
        //    {
        //        txtSubTotal.Text = "0.00";
        //        lblGrandTotal.Text = "0.00";
        //        txtTotalDisc.Text = "0.00";
        //        txtTotalTax.Text = "0.00";
        //        double subTotal = 0, totalDisc = 0, totalChrg = 0, totalTax = 0, TotFinal = 0, MaxPer = 0;
        //        if (Validations() == true)
        //        {
        //            for (int i = 0; i < dgBill.Rows.Count; i++)
        //            {
        //                if (dgBill.Rows[i].Cells[ColIndex.ItemNo].Value != null && dgBill.Rows[i].Cells[ColIndex.ItemNo].Value.ToString() != "")
        //                {
        //                    if (dgBill.Rows[i].Cells[ColIndex.Quantity].Value == null) dgBill.Rows[i].Cells[ColIndex.Quantity].Value = (1).ToString(Format.DoubleFloating);
        //                    if (dgBill.Rows[i].Cells[ColIndex.MKTQuantity].Value == null) dgBill.Rows[i].Cells[ColIndex.MKTQuantity].Value = 1;
        //                    if (dgBill.Rows[i].Cells[ColIndex.StockFactor].Value == null) dgBill.Rows[i].Cells[ColIndex.StockFactor].Value = 1;
        //                    if (dgBill.Rows[i].Cells[ColIndex.Rate].Value == null) dgBill.Rows[i].Cells[ColIndex.Rate].Value = 0;
        //                    if (dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value = 0;
        //                    if (dgBill.Rows[i].Cells[ColIndex.DiscRupees].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscRupees].Value = 0;
        //                    if (dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value = 0;
        //                    if (dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value = 0;
        //                    if (dgBill.Rows[i].Cells[ColIndex.DiscRupees2].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscRupees2].Value = 0;
        //                    if (dgBill.Rows[i].Cells[ColIndex.HSNCode].Value == null) dgBill.Rows[i].Cells[ColIndex.HSNCode].Value = "";

        //                    double Amount = Convert.ToDouble((((Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value)) * (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Rate].Value))) / (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.MKTQuantity].Value))).ToString("0.0000"));
        //                    double Disc1 = Convert.ToDouble(((Amount * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value)) / 100).ToString("0.0000"));
        //                    double Disc2 = Convert.ToDouble((((Amount - (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscRupees].Value) + Disc1)) * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value)) / 100).ToString("0.0000"));
        //                    double DiscAmt = (Disc1 + Disc2);
        //                    DiscAmt += Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscRupees].Value) + Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscRupees2].Value);

        //                    double tAmount = Amount - DiscAmt;
        //                    double TaxPerce = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.TaxPercentage].Value);
        //                    double TaxAmt = 0, ttRate = 0;
        //                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsReverseRateCalc)) == true)
        //                    {
        //                        TaxAmt = Convert.ToDouble(((tAmount * TaxPerce) / (100 + TaxPerce)).ToString("0.00"));
        //                        ttRate = (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value) == 0) ? 0 : (tAmount - TaxAmt) / Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value);
        //                    }
        //                    else
        //                    {
        //                        TaxAmt = Convert.ToDouble(((tAmount * TaxPerce) / (100)).ToString("0.00"));
        //                        ttRate = (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value) == 0) ? 0 : (tAmount) / Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value);
        //                    }
        //                    totalTax += TaxAmt;

        //                    if (TaxPerce > 0 && ObjFunction.GetComboValue(cmbTaxType) == GroupType.GST)// && dgBill.Rows[i].Cells[ColIndex.HSNCode].Value.ToString().Length > 0)
        //                    {
        //                        double CGSTPercent = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.CGSTPercent].Value);
        //                        if (CGSTPercent > 0)
        //                        {
        //                            double CGSTValue = Convert.ToDouble(((CGSTPercent * TaxAmt) / TaxPerce).ToString("0.000"));
        //                            dgBill.Rows[i].Cells[ColIndex.CGSTAmount].Value = CGSTValue;
        //                        }

        //                        double SGSTPercent = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.SGSTPercent].Value);
        //                        if (SGSTPercent > 0)
        //                        {
        //                            double SGSTValue = Convert.ToDouble(((SGSTPercent * TaxAmt) / TaxPerce).ToString("0.000"));
        //                            dgBill.Rows[i].Cells[ColIndex.SGSTAmount].Value = SGSTValue;
        //                        }

        //                        double CessPercent = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.CessPercent].Value);
        //                        if (CessPercent > 0)
        //                        {
        //                            double CessValue = Convert.ToDouble(((CessPercent * TaxAmt) / TaxPerce).ToString("0.000"));
        //                            dgBill.Rows[i].Cells[ColIndex.CessAmount].Value = CessValue;
        //                        }
        //                    }

        //                    dgBill.Rows[i].Cells[ColIndex.Amount].Value = tAmount.ToString("0.00");
        //                    dgBill.Rows[i].Cells[ColIndex.DiscAmount].Value = Disc1.ToString("0.00");
        //                    dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value = Disc2.ToString("0.00");
        //                    dgBill.Rows[i].Cells[ColIndex.TaxAmount].Value = TaxAmt.ToString("0.00");
        //                    dgBill.Rows[i].Cells[ColIndex.NetRate].Value = ttRate.ToString("0.00");
        //                    dgBill.Rows[i].Cells[ColIndex.NetAmt].Value = (ttRate * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value)).ToString("0.00");
        //                    dgBill.Rows[i].Cells[ColIndex.ActualQty].Value = ((Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value)) * (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.StockFactor].Value)));

        //                    subTotal = subTotal + Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.NetAmt].Value);
        //                    totalDisc = totalDisc + DiscAmt;

        //                    MaxPer = Math.Max(Convert.ToDouble((dgBill[ColIndex.TaxPercentage, i].EditedFormattedValue.ToString().Trim() == "") ? "0" : dgBill[ColIndex.TaxPercentage, i].EditedFormattedValue), MaxPer);
        //                }
        //            }
        //            subTotal = Convert.ToDouble(subTotal.ToString("0.00"));
        //            txtSubTotal.Text = (subTotal + totalDisc).ToString("0.00");
        //            txtTotalDisc.Text = totalDisc.ToString("0.00");

        //            double TotalAmt = Convert.ToDouble(((Convert.ToDouble(txtSubTotal.Text) + totalTax) - Convert.ToDouble(txtTotalDisc.Text)).ToString("0.00"));
        //            //to initialise the discount related fields
        //            if (txtSchemeDisc.Text.Trim() == "") txtSchemeDisc.Text = "0.00";
        //            if (txtOtherDisc.Text.Trim() == "") txtOtherDisc.Text = "0.00";
        //            if (txtDiscRupees1.Text.Trim() == "") txtDiscRupees1.Text = "0.00";
        //            TotalAmt -= Convert.ToDouble(txtSchemeDisc.Text) + Convert.ToDouble(txtDiscRupees1.Text) + Convert.ToDouble(txtOtherDisc.Text);


        //            totalDisc = Convert.ToDouble(txtDiscRupees1.Text) + Convert.ToDouble(txtSchemeDisc.Text) + Convert.ToDouble(txtOtherDisc.Text);
        //            txtTotalAnotherDisc.Text = totalDisc.ToString("0.00");
        //            txtTotalChrgs.Text = totalChrg.ToString("0.00");
        //            txtTotalTax.Text = totalTax.ToString("0.00");
        //            if ((Convert.ToDouble(txtSubTotal.Text.Trim()) - Convert.ToDouble(txtTotalDisc.Text.Trim()) + Convert.ToDouble(txtTotalTax.Text.Trim())) != 0)
        //                txtDiscount1.Text = Convert.ToDouble((100 * Convert.ToDouble(txtDiscRupees1.Text)) / (Convert.ToDouble(txtSubTotal.Text.Trim()) - Convert.ToDouble(txtTotalDisc.Text.Trim()) - (Convert.ToDouble(txtSchemeDisc.Text.Trim()) + Convert.ToDouble(txtOtherDisc.Text.Trim())) + Convert.ToDouble(txtTotalTax.Text.Trim()))).ToString("0.00");

        //            totalTax = Convert.ToDouble(totalTax.ToString("0.00"));
        //            lblGrandTotal.Text = ((subTotal + totalTax + totalChrg) - totalDisc).ToString("0.00");
        //            TotFinal = Math.Round(Convert.ToDouble(lblGrandTotal.Text), MidpointRounding.AwayFromZero);
        //            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsBillRoundOff)) == true)
        //            {
        //                txtRoundOff.Text = (TotFinal - Convert.ToDouble(lblGrandTotal.Text)).ToString("0.00");
        //                lblGrandTotal.Text = ((subTotal + totalTax + totalChrg + Convert.ToDouble(txtRoundOff.Text)) - totalDisc).ToString("0.00");
        //            }
        //            else
        //                txtRoundOff.Text = "0.00";
        //        }
        //    }
        //    catch (Exception exc)
        //    {
        //        //ObjFunction.ExceptionDisplay(exc.Message);;//For Common Error Displayed Purpose
        //    }
        //}

        private void CheckSchemeValidation()
        {
            try
            {
                while (dgInsTSKU.Rows.Count > 0)
                    dgInsTSKU.Rows.RemoveAt(0);

                #region Check Scheme Period

                string Str = " Select Count(*) From mScheme Where (SchemePeriodFrom <= '" + dtpBillDate.Text + "') AND (SchemePeriodTo >= '" + dtpBillDate.Text + "') AND (MScheme.IsActive = 1) ";
                if (ObjQry.ReturnLong(Str, CommonFunctions.ConStr) == 0)
                {
                    return;
                }

                #endregion

                double Amt = Convert.ToDouble(txtGrandTotal.Text.Trim());
                double GAmt = Amt;
                string strItm = "";
                DataRow[] dr = null;
                for (int i = 0; i < dgBill.Rows.Count; i++)
                {
                    if (dgBill.Rows[i].Cells[ColIndex.ItemNo].Value != null && dgBill.Rows[i].Cells[ColIndex.ItemNo].Value.ToString() != "")
                    {
                        if (strItm == "")
                            strItm = " (ItemNo = " + dgBill.Rows[i].Cells[ColIndex.ItemNo].Value.ToString() + " AND MRP=" + ObjQry.ReturnLong("Select MRP From MRateSetting Where PkSrNo=" + dgBill.Rows[i].Cells[ColIndex.PkRateSettingNo].Value.ToString() + "", CommonFunctions.ConStr) + ")";
                        else
                            strItm = strItm + " OR " + " (ItemNo = " + dgBill.Rows[i].Cells[ColIndex.ItemNo].Value.ToString() + " AND MRP=" + ObjQry.ReturnLong("Select MRP From MRateSetting Where PkSrNo=" + dgBill.Rows[i].Cells[ColIndex.PkRateSettingNo].Value.ToString() + "", CommonFunctions.ConStr) + ")";
                    }
                }
                DataTable dtTsku = ObjFunction.GetDataView("Exec GetLoyaltyInstantTSKU_C '" + dtpBillDate.Text + "','" + strItm + "'," + ObjFunction.GetComboValue(cmbPartyName) + ",0").Table;
                if (dtTsku.Rows.Count > 0)
                {
                    ShowHideScheme(true);
                    string SchName = "";
                    for (int i = 0; i < dtTsku.Rows.Count; i++)
                    {
                        dgInsTSKU.Rows.Add();

                        for (int j = 0; j < dgInsTSKU.Columns.Count - 6; j++)
                        {
                            dgInsTSKU.Rows[i].Cells[j].Value = dtTsku.Rows[i].ItemArray[j];

                            if (j == 3)
                            {
                                if (SchName != dtTsku.Rows[i].ItemArray[3].ToString())
                                {
                                    dgInsTSKU.Rows[i].Cells[19].Style.BackColor = ChkValue.False;
                                    dgInsTSKU.Rows[i].Cells[19].Value = false;
                                    //dgInsTSKU.Rows[i].Cells[18].Value = chk;
                                    SchName = dtTsku.Rows[i].ItemArray[3].ToString();
                                    dgInsTSKU.Rows[i].Cells[18].Value = "Show";
                                    dgInsTSKU.Rows[i].Cells[19].Style.BackColor = ChkValue.Blank;

                                }
                                else
                                {
                                    dgInsTSKU.Rows[i].Cells[19].Style.BackColor = ChkValue.Blank;
                                    dgInsTSKU.Rows[i].Cells[2].Value = "";
                                    dgInsTSKU.Rows[i].Cells[3].Value = "";
                                    dgInsTSKU.Rows[i].Cells[18].Value = null;
                                }
                            }
                        }
                        dgInsTSKU.Rows[i].Cells[ColIndexGrid.MRP].Value = dtTsku.Rows[i].ItemArray[17];
                        if (dtTsku.Rows[i].ItemArray[0].ToString() != "3")
                        {
                            double Qty = 0, LAmt = 0; string UName = "";
                            for (int k = 0; k < dgBill.Rows.Count; k++)
                            {
                                if (dgBill.Rows[k].Cells[1].Value != "" && dgBill.Rows[k].Cells[1].Value != null)
                                {
                                    double dMRP = 0;
                                    dMRP = ObjQry.ReturnLong("Select MRP From MRateSetting Where PkSrNo=" + dgBill.Rows[k].Cells[ColIndex.PkRateSettingNo].Value.ToString() + "", CommonFunctions.ConStr);
                                    if (dgBill.Rows[k].Cells[ColIndex.ItemNo].Value.ToString() == dtTsku.Rows[i].ItemArray[12].ToString()
                                        && dMRP == Convert.ToDouble(dtTsku.Rows[i].ItemArray[17].ToString()))
                                    {
                                        Qty = Convert.ToDouble(dgBill.Rows[k].Cells[ColIndex.Quantity].Value);
                                        LAmt = Convert.ToDouble(dgBill.Rows[k].Cells[ColIndex.Amount].Value);
                                        UName = dgBill.Rows[k].Cells[ColIndex.UOM].Value.ToString();
                                    }
                                }
                            }
                            //if (dtTsku.Rows[i].ItemArray[0].ToString() != "3")
                            //if (UName != "")
                            dgInsTSKU.Rows[i].Cells[ColIndexGrid.BillQty].Value = Qty; //+ "-" + UName;
                            dgInsTSKU.Rows[i].Cells[ColIndexGrid.BUom].Value = UName;
                            dgInsTSKU.Rows[i].Cells[ColIndexGrid.LoyaltyFactor].Value = Math.Floor(Qty / Convert.ToDouble(dgInsTSKU.Rows[i].Cells[ColIndexGrid.Qty].Value));
                            if (Convert.ToDouble(dgInsTSKU.Rows[i].Cells[ColIndexGrid.DiscAmt].Value) == -1)
                            {
                                dgInsTSKU.Rows[i].Cells[ColIndexGrid.DiscAmt].Value = Convert.ToDouble((LAmt * Convert.ToDouble(dgInsTSKU.Rows[i].Cells[ColIndexGrid.PDiscAmt].Value)) / 100).ToString(Format.DoubleFloating);
                                dgInsTSKU.Rows[i].Cells[ColIndexGrid.PDiscAmt].Value = "-1";
                            }
                            else dgInsTSKU.Rows[i].Cells[ColIndexGrid.PDiscAmt].Value = "0";
                            //else
                            //  dgInsTSKU.Rows[i].Cells[15].Value = Qty;
                        }
                        else
                        {
                            //dgInsTSKU.Rows[i].Cells[18].Value = txt;
                            // dgInsTSKU.Rows[i].Cells.Add(txt);
                            dgInsTSKU.Rows[i].Cells[18].Value = null;
                            dgInsTSKU.Rows[i].Cells[19].ReadOnly = true;
                            dgInsTSKU.Rows[i].Cells[19].Style.BackColor = Color.Gray;
                            dgInsTSKU.Rows[i].DefaultCellStyle.BackColor = Color.Gray;
                        }
                    }



                }
                else
                {
                    ShowHideScheme(false);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        public void ShowHideScheme(bool flag)
        {
            btnShowSchemeInfo.Visible = flag;
            btnApplyScheme.Visible = flag;
            btnRemoveScheme.Visible = flag;
        }

        public static class ChkValue
        {
            public static Color Blank = Color.White;
            public static Color True = Color.Blue;
            public static Color False = Color.WhiteSmoke;

        }

        public static class ColIndexGrid
        {
            public static int rtype = 0;
            public static int SchemeNo = 1;
            public static int SchemeUserNo = 2;
            public static int SchemeName = 3;
            public static int ItemName = 4;
            public static int SchemeTypeNo = 5;
            public static int SchDate = 6;
            public static int SchePerFrom = 7;
            public static int SchPerTo = 8;
            public static int DiscAmt = 9;
            public static int PDiscAmt = 10;
            public static int SchDtlsPksrNo = 11;
            public static int ItemNo = 12;
            public static int Qty = 13;
            public static int Uom = 14;
            public static int UomNo = 15;
            public static int BillQty = 16;
            public static int BUom = 17;
            public static int BtnDetails = 18;
            public static int Select = 19;
            public static int chk = 20;
            public static int MRP = 21;
            public static int LoyaltyFactor = 22;

        }

        //public void CalculateDiscount()
        //{
        //    txtDiscRupees1.Text =

        //    txtTotalDisc.Text = totalDisc.ToString("0.00");

        //    double TotalAmt = Convert.ToDouble(((Convert.ToDouble(txtSubTotal.Text) + totalTax) - Convert.ToDouble(txtTotalDisc.Text)).ToString("0.00"));
        //    txtDiscRupees1.Text = Convert.ToDouble((TotalAmt * Convert.ToDouble(txtDiscount1.Text)) / 100).ToString("0");

        //    TotalAmt -= Convert.ToDouble(txtDiscRupees1.Text);



        //    totalDisc = Convert.ToDouble(txtDiscRupees1.Text);
        //    totalChrg = Convert.ToDouble(txtChrgRupees1.Text);
        //    txtTotalAnotherDisc.Text = totalDisc.ToString("0.00");
        //    txtTotalChrgs.Text = totalChrg.ToString("0.00");
        //    txtTotalTax.Text = totalTax.ToString("0.00");
        //    totalTax = Convert.ToDouble(totalTax.ToString("0.00"));// Math.Round(totalTax, 00);
        //    txtGrandTotal.Text = ((subTotal + totalTax + totalChrg) - totalDisc).ToString("0.00");
        //    TotFinal = Math.Round(Convert.ToDouble(txtGrandTotal.Text), MidpointRounding.AwayFromZero);
        //    txtRoundOff.Text = (TotFinal - Convert.ToDouble(txtGrandTotal.Text)).ToString("0.00");
        //    txtGrandTotal.Text = ((subTotal + totalTax + totalChrg + Convert.ToDouble(txtRoundOff.Text)) - totalDisc).ToString("0.00");
        //}

        //private void CalculateTotalCompanywise()
        //{
        //    for (int i = 0; i < dgBill.Rows.Count; i++)
        //    {
        //        if (dgBill.Rows[i].Cells[ColIndex.ItemNo].Value != null && dgBill.Rows[i].Cells[ColIndex.ItemNo].Value != "")
        //        {
        //        }
        //    }

        //    double subTotal = 0, totalDisc = 0, totalChrg = 0, totalTax = 0, TotFinal = 0;
        //    if (Validations() == true)
        //    {
        //        for (int i = 0; i < dgBill.Rows.Count; i++)
        //        {
        //            if (dgBill.Rows[i].Cells[ColIndex.ItemNo].Value != null && dgBill.Rows[i].Cells[ColIndex.ItemNo].Value != "")
        //            {
        //                if (dgBill.Rows[i].Cells[ColIndex.Quantity].Value == null) dgBill.Rows[i].Cells[ColIndex.Quantity].Value = 1;
        //                if (dgBill.Rows[i].Cells[ColIndex.MKTQuantity].Value == null) dgBill.Rows[i].Cells[ColIndex.MKTQuantity].Value = 1;
        //                if (dgBill.Rows[i].Cells[ColIndex.StockFactor].Value == null) dgBill.Rows[i].Cells[ColIndex.StockFactor].Value = 1;
        //                if (dgBill.Rows[i].Cells[ColIndex.Rate].Value == null) dgBill.Rows[i].Cells[ColIndex.Rate].Value = 0;
        //                if (dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value = 0;
        //                if (dgBill.Rows[i].Cells[ColIndex.DiscRupees].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscRupees].Value = 0;
        //                if (dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value = 0;
        //                if (dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value = 0;
        //                if (dgBill.Rows[i].Cells[ColIndex.DiscRupees2].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscRupees2].Value = 0;

        //                double Amount = Convert.ToDouble((((Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value)) * (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Rate].Value))) / (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.MKTQuantity].Value))).ToString("0.0000"));
        //                double Disc1 = Convert.ToDouble(((Amount * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value)) / 100).ToString("0.0000"));
        //                double Disc2 = Convert.ToDouble(((Amount * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value)) / 100).ToString("0.0000"));

        //                double DiscAmt = (Disc1 + Disc2); //Convert.ToDouble(((Amount * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value)) / 100).ToString("0.0000"));
        //                DiscAmt += Convert.ToDouble((((Amount - DiscAmt) * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value)) / 100).ToString("0.0000"));
        //                DiscAmt += Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscRupees].Value) + Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscRupees2].Value);

        //                double tAmount = Amount - DiscAmt;
        //                double TaxPerce = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.TaxPercentage].Value);
        //                double TaxAmt = Convert.ToDouble(((tAmount * TaxPerce) / (100 + TaxPerce)).ToString("0.0000"));
        //                totalTax += TaxAmt;
        //                double ttRate = (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value) == 0) ? 0 : (tAmount - TaxAmt) / Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value);


        //                dgBill.Rows[i].Cells[ColIndex.Amount].Value = tAmount.ToString("0.00");
        //                dgBill.Rows[i].Cells[ColIndex.DiscAmount].Value = Disc1.ToString("0.00");
        //                dgBill.Rows[i].Cells[ColIndex.DiscAmount].Value = Disc2.ToString("0.00");
        //                dgBill.Rows[i].Cells[ColIndex.TaxAmount].Value = TaxAmt.ToString("0.00");
        //                dgBill.Rows[i].Cells[ColIndex.NetRate].Value = ttRate.ToString("0.00");
        //                dgBill.Rows[i].Cells[ColIndex.NetAmt].Value = (ttRate * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value)).ToString("0.00");
        //                dgBill.Rows[i].Cells[ColIndex.ActualQty].Value = ((Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value)) * (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.StockFactor].Value)));

        //                subTotal = subTotal + Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.NetAmt].Value);
        //                totalDisc = totalDisc + DiscAmt;// Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscAmount].Value);

        //            }
        //        }
        //        subTotal = Math.Round(subTotal, 00);
        //        txtSubTotal.Text = (subTotal + totalDisc).ToString("0.00");
        //        txtTotalDisc.Text = totalDisc.ToString("0.00");

        //        double TotalAmt = Convert.ToDouble(((Convert.ToDouble(txtSubTotal.Text) + totalTax) - Convert.ToDouble(txtTotalDisc.Text)).ToString("0.00"));
        //        txtDiscRupees1.Text = Convert.ToDouble((TotalAmt * Convert.ToDouble(txtDiscount1.Text)) / 100).ToString("0");

        //        TotalAmt -= Convert.ToDouble(txtDiscRupees1.Text);

        //        totalDisc = Convert.ToDouble(txtDiscRupees1.Text);
        //        totalChrg = Convert.ToDouble(txtChrgRupees1.Text);
        //        txtTotalAnotherDisc.Text = totalDisc.ToString("0.00");
        //        txtTotalChrgs.Text = totalChrg.ToString("0.00");
        //        txtTotalTax.Text = totalTax.ToString("0.00");
        //        totalTax = Math.Round(totalTax, 00);
        //        txtGrandTotal.Text = ((subTotal + totalTax + totalChrg) - totalDisc).ToString("0.00");
        //        TotFinal = Math.Round(Convert.ToDouble(txtGrandTotal.Text), MidpointRounding.AwayFromZero);
        //        txtRoundOff.Text = (TotFinal - Convert.ToDouble(txtGrandTotal.Text)).ToString("0.00");
        //        txtGrandTotal.Text = ((subTotal + totalTax + totalChrg + Convert.ToDouble(txtRoundOff.Text)) - totalDisc).ToString("0.00");

        //    }

        //}

        private void txtDiscount1_Leave(object sender, EventArgs e)
        {
            try
            {
                txtChrgRupees1.Focus();
                //txtDiscRupees.Focus();
                CalculateTotal();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void txtChrg1_Leave(object sender, EventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //deepak 23-may-2013
                if (isSavingTransaction)
                    return;
                //deepak 23-may-2013

                //deepak 23-may-2013
                isSavingTransaction = true;
                //deepak 23-may-2013

                double debit = 0;
                //long temp = 0;


                if (btnSave.Enabled == false)
                {
                    isSavingTransaction = false;
                    return;
                }
                btnSave.Enabled = true;
                if (ValidationsMain() == false)
                {
                    isSavingTransaction = false;
                    return;
                }
                if (dgBill.Rows.Count <= 1)
                {
                    OMMessageBox.Show("Atleast one item required.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    isSavingTransaction = false;
                    return;
                }
                if (Convert.ToDouble(txtGrandTotal.Text) < 0)
                {
                    OMMessageBox.Show("Negative Bill amount not allowed.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    isSavingTransaction = false;
                    return;
                }
                if (ID != 0)
                {
                    if (Convert.ToDouble(txtDiscount1.Text) == 0 && GetVoucherPK("SrNo=" + Others.Discount1) != 0) DeleteDtls(2, GetVoucherPK("SrNo=" + Others.Discount1));

                    if (Convert.ToDouble(txtSchemeDisc.Text) == 0 && GetVoucherPK("SrNo=" + Others.Discount2) != 0) DeleteDtls(2, GetVoucherPK("SrNo=" + Others.Discount2));

                    if (Convert.ToDouble(txtOtherDisc.Text) == 0 && GetVoucherPK("SrNo=" + Others.Discount3) != 0)
                    {
                        DeleteDtls(2, GetVoucherPK("SrNo=" + Others.Discount3));
                    }

                    if (Convert.ToDouble(txtChrgRupees1.Text) == 0 && GetVoucherPK("SrNo=" + Others.Charges1) != 0) DeleteDtls(2, GetVoucherPK("SrNo=" + Others.Charges1));
                    if (Convert.ToDouble(txtAddFreight.Text) == 0 && GetVoucherPK("SrNo=" + Others.Charges2) != 0) DeleteDtls(2, GetVoucherPK("SrNo=" + Others.Charges2));
                    if (Convert.ToDouble(txtRoundOff.Text) == 0 && GetVoucherPK("SrNo=" + Others.RoundOff) != 0) DeleteDtls(2, GetVoucherPK("SrNo=" + Others.RoundOff));
                }
                CalculateTotal();
                long ControlUnder = ObjQry.ReturnLong("Select ControlUnder From MPayType Where PKPayTypeNo=" + ObjFunction.GetComboValue(cmbPaymentType) + "", CommonFunctions.ConStr);
                //if (ObjFunction.GetComboValue(cmbPaymentType) == 2)
                if (ControlUnder == 2)
                {
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_AskPayableAmount)) == true)
                    {
                        Form NewF = new Vouchers.BillCalculator(Convert.ToDouble(txtGrandTotal.Text));
                        ObjFunction.OpenForm(NewF);
                        NewF.Left = 15;
                        if (((BillCalculator)NewF).DS == DialogResult.Cancel)
                        {
                            isSavingTransaction = false;
                            return;
                        }
                        //NewF.ShowDialog();
                    }
                }



                dbTVoucherEntry = new DBTVaucherEntry();

                DeleteRefDetails();
                UpdateCurrentStock();
                DeleteValues();//Delete Old Values


                if (ID != 0)
                {
                    dbTVoucherEntry.ReverseStock(ID);
                    dbTVoucherEntry.DeleteTReward_All(ID);
                }

                VoucherUserNo = Convert.ToInt64(txtInvNo.Text.Trim());
                int VoucherSrNo = 1;
                //Voucher Header Entry 
                tVoucherEntry = new TVoucherEntry();
                tVoucherEntry.PkVoucherNo = ID;
                tVoucherEntry.VoucherTypeCode = VoucherType;
                tVoucherEntry.VoucherUserNo = VoucherUserNo;
                tVoucherEntry.VoucherDate = Convert.ToDateTime(dtpBillDate.Text);
                tVoucherEntry.VoucherTime = dtpBillTime.Value;
                tVoucherEntry.Narration = "Sales Bill";
                tVoucherEntry.Reference = txtPartyName.Text;
                tVoucherEntry.ChequeNo = 0;
                tVoucherEntry.ClearingDate = dtpBillDate.Value;
                tVoucherEntry.CompanyNo = DBGetVal.CompanyNo;
                tVoucherEntry.BilledAmount = Convert.ToDouble(txtGrandTotal.Text);
                tVoucherEntry.ChallanNo = "";
                tVoucherEntry.Remark = txtRemark.Text.Trim();
                tVoucherEntry.MacNo = DBGetVal.MacNo;
                tVoucherEntry.PayTypeNo = ObjFunction.GetComboValue(cmbPaymentType);
                tVoucherEntry.RateTypeNo = GetRateType();
                tVoucherEntry.TaxTypeNo = ObjFunction.GetComboValue(cmbTaxType);
                tVoucherEntry.OrderType = OrderType;
                tVoucherEntry.DiscPercent = (txtDiscount1.Text == "") ? 0 : Convert.ToDouble(txtDiscount1.Text);
                tVoucherEntry.DiscAmt = (txtDiscRupees1.Text == "") ? 0 : Convert.ToDouble(txtDiscRupees1.Text);
                tVoucherEntry.MixMode = MixModeVal;
                tVoucherEntry.IsItemLevelDisc = chkItemLevelDisc.Checked;
                tVoucherEntry.IsFooterLevelDisc = chkFooterLevelDisc.Checked;
                tVoucherEntry.HamaliRs = Convert.ToDouble(((txtHRs.Text.Trim() == "") ? "0" : txtHRs.Text));
                tVoucherEntry.UserID = DBGetVal.UserID;
                tVoucherEntry.UserDate = DBGetVal.ServerTime.Date;
                dbTVoucherEntry.AddTVoucherEntry(tVoucherEntry);

                ////Nikhil
                //saveRewardHeader();
                ////Nikhil

                DataTable dtVoucherDetails = new DataTable();
                dtVchPrev = new DataTable();
                if (ID != 0)
                {
                    dtVoucherDetails = ObjFunction.GetDataView("Select PkVoucherTrnNo,LedgerNo,0 AS StatusNo From TVoucherDetails Where FkVoucherNo=" + ID + " order by VoucherSrNo").Table;
                    dtVchPrev = ObjFunction.GetDataView("Select LedgerNo,Debit,CompanyNo From TVoucherDetails Where FkVoucherNo=" + ID + " order by VoucherSrNo").Table;
                }
                setCompanyRatio();
                FillPayType();
                //For Party Ledger

                tVoucherDetails = new TVoucherDetails();
                tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                tVoucherDetails.VoucherSrNo = VoucherSrNo; VoucherSrNo += 1; ObjFunction.SetVouchers(dtVoucherDetails, tVoucherDetails.PkVoucherTrnNo);
                tVoucherDetails.SignCode = 1;
                tVoucherDetails.LedgerNo = ObjFunction.GetComboValue(cmbPartyName);
                tVoucherDetails.Debit = Convert.ToDouble(txtGrandTotal.Text);
                tVoucherDetails.Credit = 0;// -Convert.ToDouble(txtTotalDisc.Text);
                tVoucherDetails.Narration = "";
                tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                tVoucherDetails.SrNo = Others.Party;
                dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                
                    for (int row = 0; row < dgPayChqDetails.Rows.Count; row++)
                    {
                        if (dgPayChqDetails.Rows[row].Cells[0].Value != null && dgPayChqDetails.Rows[row].Cells[0].Value.ToString() != "")
                        {
                            tVchChqCredit.PkSrNo = Convert.ToInt64(dgPayChqDetails.Rows[row].Cells[5].Value);
                            tVchChqCredit.ChequeNo = dgPayChqDetails.Rows[row].Cells[0].Value.ToString();
                            tVchChqCredit.ChequeDate = (dgPayChqDetails.Rows[row].Cells[1].Value == null) ? Convert.ToDateTime("01-Jan-1900") : Convert.ToDateTime(dgPayChqDetails.Rows[row].Cells[1].Value);
                            tVchChqCredit.BankNo = Convert.ToInt64(dgPayChqDetails.Rows[row].Cells[6].Value);
                            tVchChqCredit.BranchNo = Convert.ToInt64(dgPayChqDetails.Rows[row].Cells[7].Value);
                            tVchChqCredit.CreditCardNo = "";
                            tVchChqCredit.Amount = Convert.ToDouble(dgPayChqDetails.Rows[row].Cells[4].Value);
                            tVchChqCredit.PostFkVoucherNo = 0;
                            tVchChqCredit.PostFkVoucherTrnNo = 0;
                            tVchChqCredit.CompanyNo = DBGetVal.CompanyNo;
                            dbTVoucherEntry.AddTVoucherChqCreditDetails(tVchChqCredit);
                        }
                    }

                    for (int row = 0; row < dgPayCreditCardDetails.Rows.Count; row++)
                    {
                        if (dgPayCreditCardDetails.Rows[row].Cells[0].Value != null && dgPayCreditCardDetails.Rows[row].Cells[0].Value.ToString() != "")
                        {
                            tVchChqCredit.PkSrNo = Convert.ToInt64(dgPayCreditCardDetails.Rows[row].Cells[4].Value);
                            tVchChqCredit.CreditCardNo = dgPayCreditCardDetails.Rows[row].Cells[0].Value.ToString();
                            tVchChqCredit.ChequeDate = Convert.ToDateTime("01-Jan-1900");
                            tVchChqCredit.BankNo = Convert.ToInt64(dgPayCreditCardDetails.Rows[row].Cells[5].Value);
                            tVchChqCredit.BranchNo = Convert.ToInt64(dgPayCreditCardDetails.Rows[row].Cells[6].Value);
                            tVchChqCredit.ChequeNo = "";
                            tVchChqCredit.Amount = Convert.ToDouble(dgPayCreditCardDetails.Rows[row].Cells[3].Value);
                            tVchChqCredit.PostFkVoucherNo = 0;
                            tVchChqCredit.PostFkVoucherTrnNo = 0;
                            tVchChqCredit.CompanyNo = DBGetVal.CompanyNo;
                            dbTVoucherEntry.AddTVoucherChqCreditDetails(tVchChqCredit);
                        }
                    }

                    if (Convert.ToDouble(dgPayType.Rows[2].Cells[2].Value) != 0)
                    {
                        tVchRefDtls = new TVoucherRefDetails();
                        tVchRefDtls.PkRefTrnNo = ObjQry.ReturnLong("Select PKRefTrnNo From TVoucherRefDetails Where FKVoucherTrnNo=" + tVoucherDetails.PkVoucherTrnNo + " ", CommonFunctions.ConStr);
                        tVchRefDtls.FkVoucherSrNo = tVoucherDetails.VoucherSrNo;
                        tVchRefDtls.LedgerNo = tVoucherDetails.LedgerNo;
                        tVchRefDtls.TypeOfRef = 3;
                        tVchRefDtls.RefNo = 0;
                        tVchRefDtls.DueDays = 0;
                        tVchRefDtls.DueDate = DBGetVal.ServerTime;
                        tVchRefDtls.Amount = tVoucherEntry.BilledAmount;
                        tVchRefDtls.SignCode = 1;
                        tVchRefDtls.UserID = DBGetVal.UserID;
                        tVchRefDtls.UserDate = DBGetVal.ServerTime.Date;
                        tVchRefDtls.CompanyNo = DBGetVal.CompanyNo;
                        dbTVoucherEntry.AddTVoucherRefDetails(tVchRefDtls);
                    }
                

                int cntreward = 0;
                for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                {
                    tStock = new TStock();
                    if (Convert.ToInt64(dgBill[ColIndex.PkStockTrnNo, i].Value) == 0)
                    {
                        tStock.PkStockTrnNo = 0;
                    }
                    else
                    {
                        tStock.PkStockTrnNo = Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.PkStockTrnNo].Value);
                    }

                    tStock.GroupNo = 0;
                    tStock.ItemNo = Convert.ToInt64(dgBill[ColIndex.ItemNo, i].Value.ToString());
                    tStock.FkVoucherSrNo = VoucherSrNo;
                    tStock.TrnCode = (Convert.ToDouble(dgBill[ColIndex.Quantity, i].Value.ToString()) < 0) ? 1 : 2;
                    tStock.Quantity = Convert.ToDouble(dgBill[ColIndex.Quantity, i].Value.ToString());
                    tStock.BilledQuantity = Convert.ToDouble(dgBill[ColIndex.ActualQty, i].Value.ToString());
                    tStock.Rate = Convert.ToDouble(dgBill[ColIndex.Rate, i].Value.ToString());
                    tStock.Amount = Convert.ToDouble(dgBill[ColIndex.Amount, i].Value.ToString());
                    tStock.TaxPercentage = Convert.ToDouble(dgBill[ColIndex.TaxPercentage, i].Value.ToString());
                    tStock.TaxAmount = Convert.ToDouble(dgBill[ColIndex.TaxAmount, i].Value.ToString());
                    tStock.DiscPercentage = Convert.ToDouble(dgBill[ColIndex.DiscPercentage, i].Value.ToString());
                    tStock.DiscAmount = Convert.ToDouble(dgBill[ColIndex.DiscAmount, i].Value.ToString());
                    tStock.DiscRupees = Convert.ToDouble(dgBill[ColIndex.DiscRupees, i].Value.ToString());
                    tStock.DiscPercentage2 = Convert.ToDouble(dgBill[ColIndex.DiscPercentage2, i].Value.ToString());
                    tStock.DiscAmount2 = Convert.ToDouble(dgBill[ColIndex.DiscAmount2, i].Value.ToString());
                    tStock.DiscRupees2 = Convert.ToDouble((dgBill[ColIndex.DiscRupees2, i].Value.ToString() == "") ? "0" : dgBill[ColIndex.DiscRupees2, i].Value.ToString());
                    tStock.NetRate = Convert.ToDouble(dgBill[ColIndex.NetRate, i].Value.ToString());
                    tStock.NetAmount = Convert.ToDouble(dgBill[ColIndex.NetAmt, i].Value.ToString());
                    tStock.FkStockBarCodeNo = Convert.ToInt64(dgBill[ColIndex.PkBarCodeNo, i].Value.ToString());
                    tStock.FkUomNo = Convert.ToInt64(dgBill[ColIndex.UOMNo, i].Value.ToString());
                    tStock.FkRateSettingNo = Convert.ToInt64(dgBill[ColIndex.PkRateSettingNo, i].Value.ToString());
                    tStock.FkItemTaxInfo = Convert.ToInt64(dgBill[ColIndex.PkItemTaxInfo, i].Value.ToString());
                    tStock.FreeQty = 0;
                    tStock.FreeUOMNo = 1;
                    tStock.UserID = DBGetVal.UserID;
                    tStock.UserDate = DBGetVal.ServerTime.Date;
                    tStock.CompanyNo = Convert.ToInt64(dgBill[ColIndex.StockCompanyNo, i].Value.ToString());
                    tStock.LandedRate = 0;
                    tStock.DiscountType = Convert.ToInt64(dgBill[ColIndex.DiscountType, i].Value.ToString());
                    tStock.HamaliInKg = Convert.ToDouble(dgBill[ColIndex.HamaliInKg, i].Value.ToString());
                    tStock.SchemeDetailsNo = Convert.ToDouble((dgBill[ColIndex.SchemeDetailsNo, i].Value.ToString() == "") ? "0" : dgBill[ColIndex.SchemeDetailsNo, i].Value.ToString());
                    tStock.SchemeFromNo = Convert.ToDouble((dgBill[ColIndex.SchemeFromNo, i].Value.ToString() == "") ? "0" : dgBill[ColIndex.SchemeFromNo, i].Value.ToString());
                    tStock.SchemeToNo = Convert.ToDouble((dgBill[ColIndex.SchemeToNo, i].Value.ToString() == "") ? "0" : dgBill[ColIndex.SchemeToNo, i].Value.ToString());
                    tStock.GodownNo = Convert.ToInt64(dgBill[ColIndex.GodownNo, i].Value.ToString());
                    tStock.MRP = Convert.ToDouble(dgBill[ColIndex.MRP, i].Value.ToString());

                    #region GST Related fields
                    tStock.HSNCode = dgBill[ColIndex.HSNCode, i].EditedFormattedValue.ToString();
                    tStock.IGSTPercent = Convert.ToDouble(dgBill[ColIndex.IGSTPercent, i].EditedFormattedValue.ToString());
                    tStock.IGSTAmount = Convert.ToDouble(dgBill[ColIndex.IGSTAmount, i].EditedFormattedValue.ToString());
                    tStock.CGSTPercent = Convert.ToDouble(dgBill[ColIndex.CGSTPercent, i].EditedFormattedValue.ToString());
                    tStock.CGSTAmount = Convert.ToDouble(dgBill[ColIndex.CGSTAmount, i].EditedFormattedValue.ToString());
                    tStock.SGSTPercent = Convert.ToDouble(dgBill[ColIndex.SGSTPercent, i].EditedFormattedValue.ToString());
                    tStock.SGSTAmount = Convert.ToDouble(dgBill[ColIndex.SGSTAmount, i].EditedFormattedValue.ToString());
                    tStock.UTGSTPercent = Convert.ToDouble(dgBill[ColIndex.UTGSTPercent, i].EditedFormattedValue.ToString());
                    tStock.UTGSTAmount = Convert.ToDouble(dgBill[ColIndex.UTGSTAmount, i].EditedFormattedValue.ToString());
                    tStock.CessPercent = Convert.ToDouble(dgBill[ColIndex.CessPercent, i].EditedFormattedValue.ToString());
                    tStock.CessAmount = Convert.ToDouble(dgBill[ColIndex.CessAmount, i].EditedFormattedValue.ToString());
                    #endregion
                    dbTVoucherEntry.AddTStock(tStock);

                }




                //For Item Discount Entry
                //double DiscAmt = 0;
                //for (int j = 0; j < dgBill.Rows.Count - 1; j++)
                //{
                //    if (Convert.ToDouble(dgBill.Rows[j].Cells[ColIndex.DiscAmount].Value) != 0)
                //    {
                //        DiscAmt = DiscAmt + Convert.ToDouble(dgBill.Rows[j].Cells[ColIndex.DiscAmount].Value);
                //    }
                //}

                //if (DiscAmt != 0)
                //{
                //    tVoucherDetails = new TVoucherDetails();
                //    tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                //    tVoucherDetails.VoucherSrNo = VoucherSrNo; VoucherSrNo += 1;
                //    tVoucherDetails.SignCode = 2;
                //    tVoucherDetails.LedgerNo = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_ItemDisc));
                //    tVoucherDetails.Debit = 0;
                //    tVoucherDetails.Credit = DiscAmt;
                //    dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                //}


                //Item Sales Ledger Details
                DataTable dtSaleLedger = new DataTable();
                bool TempFlag = false;
                dtSaleLedger.Columns.Add();
                dtSaleLedger.Columns.Add();
                DataRow dr = dtSaleLedger.NewRow();
                dr[0] = Convert.ToInt64(dgBill.Rows[0].Cells[ColIndex.SalesLedgerNo].Value);
                dr[1] = Convert.ToInt64(dgBill.Rows[0].Cells[ColIndex.StockCompanyNo].Value);
                dtSaleLedger.Rows.Add(dr);
                for (int k = 1; k < dgBill.Rows.Count - 1; k++)
                {
                    for (int i = 0; i < dtSaleLedger.Rows.Count; i++)
                    {
                        if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.SalesLedgerNo].Value) != Convert.ToInt64(dtSaleLedger.Rows[i].ItemArray[0].ToString()) || Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.StockCompanyNo].Value) != Convert.ToInt64(dtSaleLedger.Rows[i].ItemArray[1].ToString()))
                        {
                            TempFlag = true;

                        }
                        else
                        {
                            TempFlag = false;
                            break;
                        }
                    }
                    if (TempFlag == true)
                    {
                        dr = dtSaleLedger.NewRow();
                        dr[0] = Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.SalesLedgerNo].Value);
                        dr[1] = Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.StockCompanyNo].Value);
                        dtSaleLedger.Rows.Add(dr);
                    }
                }

                int ledgerNo = 0; int cnt = VoucherSrNo - 1, cntLedg = -1, cntTaxLedg = -1;
                for (int k = 0; k < dtSaleLedger.Rows.Count; k++)
                {
                    cntLedg = -1;
                    for (int j = 0; j < dgBill.Rows.Count - 1; j++)
                    {
                        if (Convert.ToInt64(dgBill.Rows[j].Cells[ColIndex.SalesLedgerNo].Value) == Convert.ToInt64(dtSaleLedger.Rows[k].ItemArray[0].ToString()) && Convert.ToInt64(dgBill.Rows[j].Cells[ColIndex.StockCompanyNo].Value) == Convert.ToInt64(dtSaleLedger.Rows[k].ItemArray[1].ToString()))
                        {
                            if (cntLedg == -1) cntLedg = j;
                            debit = debit + Convert.ToDouble(dgBill.Rows[j].Cells[ColIndex.NetAmt].Value);
                            dgBill.Rows[j].Cells[ColIndex.SalesVchNo].Value = dgBill.Rows[cntLedg].Cells[ColIndex.SalesVchNo].Value;
                            ledgerNo = j;
                        }

                    }
                    if (debit > 0)
                    {
                        tVoucherDetails = new TVoucherDetails();
                        if (dtVoucherDetails.Rows.Count > 0)
                        {
                            //if (Convert.ToInt64(dtVoucherDetails.Rows[cnt].ItemArray[1].ToString()) == Convert.ToInt64(dtSaleLedger.Rows[k].ItemArray[0].ToString()))
                            if (Convert.ToInt64(dgBill.Rows[cntLedg].Cells[ColIndex.SalesVchNo].Value) != 0)
                            {
                                tVoucherDetails.PkVoucherTrnNo = Convert.ToInt64(dgBill.Rows[cntLedg].Cells[ColIndex.SalesVchNo].Value);// Convert.ToInt64(dtVoucherDetails.Rows[cnt].ItemArray[0].ToString());
                                cnt++;
                            }
                            else
                            {
                                tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                            }
                            tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                        }
                        else
                        {
                            tVoucherDetails.PkVoucherTrnNo = 0;
                        }
                        tVoucherDetails.VoucherSrNo = VoucherSrNo; VoucherSrNo += 1; ObjFunction.SetVouchers(dtVoucherDetails, tVoucherDetails.PkVoucherTrnNo);
                        tVoucherDetails.SignCode = 2;
                        tVoucherDetails.LedgerNo = Convert.ToInt64(dgBill.Rows[ledgerNo].Cells[ColIndex.SalesLedgerNo].Value);
                        tVoucherDetails.Debit = 0;
                        tVoucherDetails.CompanyNo = Convert.ToInt64(dgBill.Rows[ledgerNo].Cells[ColIndex.StockCompanyNo].Value);
                        tVoucherDetails.Credit = debit;// (GetRatio(tVoucherDetails.CompanyNo) * debit) / 10;
                        tVoucherDetails.Narration = "";
                        dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                        debit = 0;
                    }
                }



                //Item Tax Details
                DataTable dtTAxLedger = new DataTable();
                TempFlag = false;
                dtTAxLedger.Columns.Add();
                dtTAxLedger.Columns.Add();
                dr = dtTAxLedger.NewRow();
                dr[0] = Convert.ToInt64(dgBill.Rows[0].Cells[ColIndex.TaxLedgerNo].Value);
                dr[1] = Convert.ToInt64(dgBill.Rows[0].Cells[ColIndex.StockCompanyNo].Value);
                dtTAxLedger.Rows.Add(dr);
                for (int k = 1; k < dgBill.Rows.Count - 1; k++)
                {
                    TempFlag = false;
                    for (int i = 0; i < dtTAxLedger.Rows.Count; i++)
                    {
                        if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.TaxLedgerNo].Value) != Convert.ToInt64(dtTAxLedger.Rows[i].ItemArray[0].ToString()) || Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.StockCompanyNo].Value) != Convert.ToInt64(dtTAxLedger.Rows[i].ItemArray[1].ToString()))
                        {
                            TempFlag = true;
                        }
                        else
                        {
                            TempFlag = false;
                            break;
                        }
                    }
                    if (TempFlag == true)
                    {
                        dr = dtTAxLedger.NewRow();
                        dr[0] = Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.TaxLedgerNo].Value);
                        dr[1] = Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.StockCompanyNo].Value);
                        dtTAxLedger.Rows.Add(dr);
                    }
                }

                cnt = VoucherSrNo - 1;
                debit = 0;
                ledgerNo = 0;
                for (int k = 0; k < dtTAxLedger.Rows.Count; k++)
                {
                    cntTaxLedg = -1;
                    for (int j = 0; j < dgBill.Rows.Count - 1; j++)
                    {
                        if (Convert.ToInt64(dgBill.Rows[j].Cells[ColIndex.TaxLedgerNo].Value) == Convert.ToInt64(dtTAxLedger.Rows[k].ItemArray[0].ToString()) && Convert.ToInt64(dgBill.Rows[j].Cells[ColIndex.StockCompanyNo].Value) == Convert.ToInt64(dtTAxLedger.Rows[k].ItemArray[1].ToString()))
                        {
                            if (cntTaxLedg == -1) cntTaxLedg = j;
                            debit = debit + Convert.ToDouble(dgBill.Rows[j].Cells[ColIndex.TaxAmount].Value);
                            dgBill.Rows[j].Cells[ColIndex.TaxVchNo].Value = dgBill.Rows[cntTaxLedg].Cells[ColIndex.TaxVchNo].Value;
                            ledgerNo = j;
                        }
                    }
                    if (debit > 0)
                    {
                        tVoucherDetails = new TVoucherDetails();
                        if (dtVoucherDetails.Rows.Count > 0)
                        {
                            if (Convert.ToInt64(dgBill.Rows[cntTaxLedg].Cells[ColIndex.TaxVchNo].Value) != 0)
                            {
                                tVoucherDetails.PkVoucherTrnNo = Convert.ToInt64(dgBill.Rows[cntTaxLedg].Cells[ColIndex.TaxVchNo].Value);// Convert.ToInt64(dtVoucherDetails.Rows[cnt].ItemArray[0].ToString());
                                cnt++;
                            }
                            else
                            {
                                tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                            }
                            tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                        }
                        else
                        {
                            tVoucherDetails.PkVoucherTrnNo = 0;
                        }
                        tVoucherDetails.VoucherSrNo = VoucherSrNo; VoucherSrNo += 1; ObjFunction.SetVouchers(dtVoucherDetails, tVoucherDetails.PkVoucherTrnNo);
                        tVoucherDetails.SignCode = 2;
                        tVoucherDetails.LedgerNo = Convert.ToInt64(dgBill.Rows[ledgerNo].Cells[ColIndex.TaxLedgerNo].Value);
                        tVoucherDetails.Debit = 0;
                        tVoucherDetails.CompanyNo = Convert.ToInt64(dgBill.Rows[ledgerNo].Cells[ColIndex.StockCompanyNo].Value);
                        tVoucherDetails.Credit = debit;// (GetRatio(tVoucherDetails.CompanyNo) * debit) / 10;
                        tVoucherDetails.Narration = "";
                        dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                        debit = 0;
                    }
                }

                //For Scheme Discount
                if (Convert.ToDouble(txtSchemeDisc.Text) != 0)
                {
                    tVoucherDetails = new TVoucherDetails();
                    tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                    tVoucherDetails.VoucherSrNo = VoucherSrNo; VoucherSrNo += 1; ObjFunction.SetVouchers(dtVoucherDetails, tVoucherDetails.PkVoucherTrnNo);
                    tVoucherDetails.SignCode = 1;
                    tVoucherDetails.LedgerNo = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_Discount2));
                    tVoucherDetails.Debit = Convert.ToDouble(txtSchemeDisc.Text);
                    tVoucherDetails.Credit = 0;
                    tVoucherDetails.Narration = "";
                    tVoucherDetails.SrNo = Others.Discount2;
                    tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                    dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                }


                //For Discount Ledger 1 %
                if (Convert.ToDouble(txtDiscRupees1.Text) != 0)
                {

                    tVoucherDetails = new TVoucherDetails();
                    tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                    tVoucherDetails.VoucherSrNo = VoucherSrNo; VoucherSrNo += 1; ObjFunction.SetVouchers(dtVoucherDetails, tVoucherDetails.PkVoucherTrnNo);
                    tVoucherDetails.SignCode = 1;
                    tVoucherDetails.LedgerNo = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_Discount1));
                    tVoucherDetails.Debit = Convert.ToDouble(txtDiscRupees1.Text);
                    tVoucherDetails.Credit = 0;
                    tVoucherDetails.Narration = "";
                    tVoucherDetails.SrNo = Others.Discount1;
                    tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                    dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);

                }

                //For Footer Other  disc
                if (Convert.ToDouble(txtOtherDisc.Text) != 0)
                {
                    tVoucherDetails = new TVoucherDetails();
                    tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                    tVoucherDetails.VoucherSrNo = VoucherSrNo; VoucherSrNo += 1; ObjFunction.SetVouchers(dtVoucherDetails, tVoucherDetails.PkVoucherTrnNo);
                    tVoucherDetails.SignCode = 1;
                    tVoucherDetails.LedgerNo = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_Discount3));
                    tVoucherDetails.Debit = Convert.ToDouble(txtOtherDisc.Text);
                    tVoucherDetails.Credit = 0;
                    tVoucherDetails.Narration = "";
                    tVoucherDetails.SrNo = Others.Discount3;
                    tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                    dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);

                }

                //=========Debit Entrys=========================
                //For Charges Rupees 1
                if (Convert.ToDouble(txtChrgRupees1.Text) != 0)
                {

                    tVoucherDetails = new TVoucherDetails();
                    tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                    tVoucherDetails.VoucherSrNo = VoucherSrNo; VoucherSrNo += 1; ObjFunction.SetVouchers(dtVoucherDetails, tVoucherDetails.PkVoucherTrnNo);
                    tVoucherDetails.SignCode = 2;
                    tVoucherDetails.LedgerNo = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_Charges1));
                    tVoucherDetails.Debit = 0;
                    tVoucherDetails.Credit = Convert.ToDouble(txtChrgRupees1.Text);
                    tVoucherDetails.Narration = "";
                    tVoucherDetails.SrNo = Others.Charges1;
                    tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                    dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);

                }

                if (Convert.ToDouble(txtAddFreight.Text) != 0)
                {

                    tVoucherDetails = new TVoucherDetails();
                    tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                    tVoucherDetails.VoucherSrNo = VoucherSrNo; VoucherSrNo += 1; ObjFunction.SetVouchers(dtVoucherDetails, tVoucherDetails.PkVoucherTrnNo);
                    tVoucherDetails.SignCode = 2;
                    tVoucherDetails.LedgerNo = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_Charges1));
                    tVoucherDetails.Debit = 0;
                    tVoucherDetails.Credit = Convert.ToDouble(txtAddFreight.Text);
                    tVoucherDetails.Narration = "";
                    tVoucherDetails.SrNo = Others.Charges2;
                    tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                    dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);

                }


                //For Round Off Acc Ledger
                if (Convert.ToDouble(txtRoundOff.Text) != 0)
                {
                    tVoucherDetails = new TVoucherDetails();
                    tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                    tVoucherDetails.VoucherSrNo = VoucherSrNo; VoucherSrNo += 1; ObjFunction.SetVouchers(dtVoucherDetails, tVoucherDetails.PkVoucherTrnNo);
                    tVoucherDetails.LedgerNo = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_RoundOfAcc));
                    if (Convert.ToDouble(txtRoundOff.Text) >= 0)
                    {
                        tVoucherDetails.SignCode = 2;
                        tVoucherDetails.Debit = 0;
                        tVoucherDetails.Credit = Convert.ToDouble(txtRoundOff.Text);
                    }
                    else
                    {
                        tVoucherDetails.SignCode = 1;
                        tVoucherDetails.Debit = Math.Abs(Convert.ToDouble(txtRoundOff.Text));
                        tVoucherDetails.Credit = 0;
                    }
                    tVoucherDetails.Narration = "";
                    tVoucherDetails.SrNo = Others.RoundOff;
                    tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                    dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                }
                if (ID != 0)
                {
                    for (int i = 0; i < dtVoucherDetails.Rows.Count; i++)
                    {
                        if (dtVoucherDetails.Rows[i].ItemArray[2].ToString() == "0")
                            DeleteDtls(2, Convert.ToInt64(dtVoucherDetails.Rows[i].ItemArray[0].ToString()));
                    }
                    DeleteValues();
                }

                dbTVoucherEntry.EffectStock();

                long tempID = dbTVoucherEntry.ExecuteNonQueryStatements();
                if (tempID != 0)
                {
                    UpdateSerachStock(tempID);
                    FooterDiscDtlsNo = 0;
                    //if (Convert.ToDouble(dgPayType.Rows[1].Cells[2].Value) > 0 || Convert.ToDouble(dgPayType.Rows[3].Cells[2].Value) > 0 || Convert.ToDouble(dgPayType.Rows[4].Cells[2].Value) > 0)
                    if (Convert.ToDouble(dgPayType.Rows[2].Cells[2].Value) == 0)
                    {
                        if (tempDate.Date == dtpBillDate.Value.Date && tempPartyNo == ObjFunction.GetComboValue(cmbPartyName))
                            SaveReceipt(tempID);
                        else if (tempDate.Date != dtpBillDate.Value.Date || tempPartyNo != ObjFunction.GetComboValue(cmbPartyName))
                        {
                            SaveReceiptNew(tempID);
                            SaveReceiptOld(tempDate, tempPartyNo);
                        }
                    }


                    string strVChNo = ObjQry.ReturnLong("Select VoucherUserNo From TVoucherEntry Where PKVoucherNo=" + tempID + "", CommonFunctions.ConStr).ToString();
                    //OMMessageBox.Show("Bill No " + strVChNo + " Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                    DisplayMessage("Bill No " + strVChNo + " Added Successfully");
                    if (ID == 0)
                    {

                        //dtSearch = ObjFunction.GetDataView("Select PkVoucherNo from TVoucherEntry Where VoucherTypeCode=" + VoucherType + " AND IsVoucherLock='false' Order by VoucherDate, VoucherUserNo").Table;
                        DataRow drSearch = dtSearch.NewRow();
                        drSearch[0] = tempID;
                        dtSearch.Rows.Add(drSearch);
                        ID = tempID;
                        if (cmbPaymentType.SelectedValue.ToString() == "3" && MixModeFlag == true && MFlag == true)
                        {
                            if (partialPay.SaveData(ID) == true)
                            {
                                //OMMessageBox.Show("MixMode Data Saved Successfuly", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                                partialPay = new PartialPayment();
                            }
                            else
                                OMMessageBox.Show("MixMode Data Not Saved Successfuly", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            //btnMixMode_Click(sender, e);
                        }
                        if (PrintAsk == 0)
                        {
                            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsBillPrint)) == true)
                            {
                                //if (MessageBox.Show("Want to print this bill?", CommonFunctions.ErrorTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_BillPrintWisePrinterSetting)) == true)
                                {
                                    PrintBill1();
                                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_GodownPrinting)) == true)
                                        PrintGodownWise1();
                                }
                                else
                                {
                                    if (OMMessageBox.Show("Want to print this bill?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNoCancel, OMMessageBoxIcon.Question, OMMessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                        PrintBill();
                                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_GodownPrinting)) == true)
                                    {
                                        if (OMMessageBox.Show("Want to Print Godown Wise?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNoCancel, OMMessageBoxIcon.Question, OMMessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                            PrintGodownWise();
                                    }
                                }
                            }
                        }
                        else if (PrintAsk == 1)
                        {
                            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_BillPrintWisePrinterSetting)) == true)
                            {
                                PrintBill1();
                                PrintGodownWise1();
                            }
                            else
                            {
                                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsBillPrint)) == true)
                                    PrintBill();
                                if (OMMessageBox.Show("Want to Print Godown Wise?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNoCancel, OMMessageBoxIcon.Question, OMMessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                    PrintGodownWise();
                            }
                        }

                        PartyNo = 0; PayType = 0;
                        SetNavigation();

                        dbTVoucherEntry = new DBTVaucherEntry();
                        TParkingBill tParkingBill = new TParkingBill();
                        tParkingBill.ParkingBillNo = ParkBillNo;
                        tParkingBill.FKVoucherNo = ID;
                        dbTVoucherEntry.DeleteTParkingBill(tParkingBill);

                        btnNew_Click(btnNew, e);
                        ParkBillNo = 0;
                    }
                    else
                    {
                        FillControls();
                        setDisplay(true);
                        ObjFunction.LockButtons(true, this.Controls);
                        ObjFunction.LockControls(false, this.Controls);
                        rbEnglish.Enabled = true;
                        rbMarathi.Enabled = true;
                        btnCashSave.Visible = false;
                        btnCreditSave.Visible = false;
                        //btnCCSave.Visible = false;
                        btnSearch.Visible = true;
                        btnPrint.Visible = true;
                        dgBill.Enabled = false;
                        btnNew.Focus();
                    }
                    FillTodaysSalesDetails();
                    PrintAsk = 0;
                    btnInsScheme.Visible = true; btnInsSchemeInfo.Visible = true;
                    MixModeFlag = false; MixModeVal = 0; MTDSchDetailsNo = 0; MFlag = false;
                }
                else
                {
                    OMMessageBox.Show("Bill Not Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                }

            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
            //deepak 23-may-2013
            isSavingTransaction = false;
            //deepak 23-may-2013
        }

        private void saveRewardHeader()
        {
            try
            {
                //TReward
                if (dtTRewardDtls.Rows.Count != 0)
                {
                    TReward tReward = new TReward();
                    tReward.VoucherUserNo = VoucherUserNo;
                    tReward.RewardNo = Convert.ToInt64(dtTRewardDtls.Rows[0].ItemArray[1].ToString());
                    tReward.LedgerNo = ObjFunction.GetComboValue(cmbPartyName);
                    tReward.TotalBillAmount = Convert.ToDouble(txtGrandTotal.Text) + Convert.ToDouble(txtSchemeDisc.Text);
                    tReward.ToalDiscAmount = Convert.ToDouble(txtSchemeDisc.Text);
                    tReward.FkVoucherNo = 0;
                    tReward.CompanyNo = DBGetVal.CompanyNo;
                    tReward.RedemptionStatusNo = 0;
                    tReward.UserID = DBGetVal.UserID;
                    tReward.UserDate = DBGetVal.ServerTime;
                    dbTVoucherEntry.AddTReward(tReward);
                }
                // T Reward 
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private bool saveRewardDetails(int cntRwdRow, int i, int type)
        {
            bool Rwdflag = false;
            try
            {

                if (type == 1)
                {
                    if (cntRwdRow != -1)
                    {
                        for (int rw = 0; rw < dtTRewardDtls.Rows.Count; rw++)
                        {
                            if (dtTRewardDtls.Rows[rw].ItemArray[3].ToString() == dgBill.Rows[i].Cells[ColIndex.SchemeDetailsNo].FormattedValue.ToString())
                            { cntRwdRow = rw; break; }
                        }
                        DataRow[] drReward = dtTRewardDtls.Select("SchemeDetailsNo='" + ((dgBill.Rows[i].Cells[ColIndex.SchemeDetailsNo].FormattedValue.ToString() == "") ? "0" : dgBill.Rows[i].Cells[ColIndex.SchemeDetailsNo].FormattedValue.ToString()) + "' AND Status='0' ");
                        if (drReward.Length <= 0)
                            return Rwdflag;
                        TRewardDetails tRewardDetails = new TRewardDetails();
                        tRewardDetails.PkSrNo = Convert.ToInt64(drReward[0].ItemArray[0].ToString());
                        tRewardDetails.SchemeNo = Convert.ToInt64(drReward[0].ItemArray[2].ToString());
                        tRewardDetails.SchemeDetailsNo = Convert.ToInt64(drReward[0].ItemArray[3].ToString());
                        tRewardDetails.SchemeType = Convert.ToInt64(drReward[0].ItemArray[4].ToString());
                        if (dgBill.Rows[i].Cells[ColIndex.ItemLevelDiscNo].FormattedValue.ToString() != "" && dgBill.Rows[i].Cells[ColIndex.ItemLevelDiscNo].FormattedValue.ToString() != "0")
                        {
                            tRewardDetails.DiscPer = Convert.ToDouble(drReward[0].ItemArray[5].ToString());
                            tRewardDetails.DiscAmount = Convert.ToDouble(txtSchemeDisc.Text);// Convert.ToDouble(drReward[0].ItemArray[6].ToString());
                        }
                        else
                        {
                            tRewardDetails.DiscPer = Convert.ToDouble(drReward[0].ItemArray[5].ToString());
                            tRewardDetails.DiscAmount = Convert.ToDouble(drReward[0].ItemArray[6].ToString()); //Convert.ToDouble(txtSchemeDisc.Text);
                        }
                        tRewardDetails.SchemeAmount = Convert.ToDouble(drReward[0].ItemArray[7].ToString());
                        tRewardDetails.SchemeAcheiverNo = Convert.ToInt64(drReward[0].ItemArray[9].ToString());
                        tRewardDetails.CompanyNo = DBGetVal.CompanyNo;
                        dtTRewardDtls.Rows[cntRwdRow]["Status"] = 1;
                        dtTRewardDtls.AcceptChanges();
                        tRewardDetails.AchievedNoOfTime = 0;
                        tRewardDetails.ActualNoOfTime = 0;
                        dbTVoucherEntry.AddTRewardDetails(tRewardDetails);

                        //Shceme Achiever Details
                        if (tRewardDetails.SchemeAcheiverNo != 0)
                        {
                            if (tRewardDetails.PkSrNo == 0)
                            {
                                dbTVoucherEntry.UpdateSchemeAchievers(tRewardDetails.SchemeAcheiverNo);

                                mSchAchDtls = new MSchemeAchieverDetails();
                                mSchAchDtls.PkSrNo = 0;
                                mSchAchDtls.SchemeAchDate = DBGetVal.ServerTime.Date;
                                mSchAchDtls.SchemeAchSrNo = 1;
                                mSchAchDtls.LedgerNo = ObjFunction.GetComboValue(cmbPartyName);
                                mSchAchDtls.TypeOfRef = 2;
                                mSchAchDtls.RefNo = ObjQry.ReturnLong("Select RefNo From MSchemeAchieverDetails Where SchemeAchieverNo=" + tRewardDetails.SchemeAcheiverNo + "", CommonFunctions.ConStr);
                                mSchAchDtls.Amount = tRewardDetails.DiscAmount;
                                mSchAchDtls.SignCode = 2;
                                mSchAchDtls.CompanyNo = DBGetVal.CompanyNo;
                                mSchAchDtls.UserID = DBGetVal.UserID;
                                mSchAchDtls.UserDate = DBGetVal.ServerTime;
                                mSchAchDtls.SchemeAchieverNo = tRewardDetails.SchemeAcheiverNo;
                                dbTVoucherEntry.AddMSchemeAchieverDetails(mSchAchDtls);
                            }
                        }

                        Rwdflag = true;
                    }
                }
                else if (type == 2)
                {
                    DataRow[] drReward = dtTRewardDtls.Select("Status='0'");
                    if (drReward.Length <= 0)
                        return Rwdflag;
                    TRewardDetails tRewardDetails = new TRewardDetails();
                    for (int rw = 0; rw < drReward.Length; rw++)
                    {
                        tRewardDetails = new TRewardDetails();
                        tRewardDetails.PkSrNo = Convert.ToInt64(drReward[rw].ItemArray[0].ToString());
                        tRewardDetails.SchemeNo = Convert.ToInt64(drReward[rw].ItemArray[2].ToString());
                        tRewardDetails.SchemeDetailsNo = Convert.ToInt64(drReward[rw].ItemArray[3].ToString());
                        tRewardDetails.SchemeType = Convert.ToInt64(drReward[rw].ItemArray[4].ToString());
                        if (dgBill.Rows[i].Cells[ColIndex.ItemLevelDiscNo].FormattedValue.ToString() != "" && dgBill.Rows[i].Cells[ColIndex.ItemLevelDiscNo].FormattedValue.ToString() != "0")
                        {
                            tRewardDetails.DiscPer = Convert.ToDouble(drReward[rw].ItemArray[5].ToString());
                            tRewardDetails.DiscAmount = Convert.ToDouble(txtSchemeDisc.Text);// Convert.ToDouble(drReward[rw].ItemArray[6].ToString());
                        }
                        else
                        {
                            tRewardDetails.DiscPer = Convert.ToDouble(drReward[rw].ItemArray[5].ToString()); ;
                            tRewardDetails.DiscAmount = Convert.ToDouble(drReward[rw].ItemArray[6].ToString());// Convert.ToDouble(txtSchemeDisc.Text);
                        }
                        tRewardDetails.SchemeAmount = Convert.ToDouble(drReward[rw].ItemArray[7].ToString());
                        tRewardDetails.SchemeAcheiverNo = Convert.ToInt64(drReward[0].ItemArray[9].ToString());
                        tRewardDetails.CompanyNo = DBGetVal.CompanyNo;
                        dtTRewardDtls.Rows[cntRwdRow]["Status"] = 1;
                        dtTRewardDtls.AcceptChanges();
                        tRewardDetails.AchievedNoOfTime = 0;
                        tRewardDetails.ActualNoOfTime = 0;
                        dbTVoucherEntry.AddTRewardDetails(tRewardDetails);

                        //Shceme Ahiever Details
                        if (tRewardDetails.SchemeAcheiverNo != 0)
                        {
                            if (tRewardDetails.PkSrNo == 0)
                            {
                                dbTVoucherEntry.UpdateSchemeAchievers(tRewardDetails.SchemeAcheiverNo);

                                mSchAchDtls = new MSchemeAchieverDetails();
                                mSchAchDtls.PkSrNo = 0;
                                mSchAchDtls.SchemeAchDate = DBGetVal.ServerTime.Date;
                                mSchAchDtls.SchemeAchSrNo = 1;
                                mSchAchDtls.LedgerNo = ObjFunction.GetComboValue(cmbPartyName);
                                mSchAchDtls.TypeOfRef = 2;
                                mSchAchDtls.RefNo = ObjQry.ReturnLong("Select RefNo From MSchemeAchieverDetails Where SchemeAchieverNo=" + tRewardDetails.SchemeAcheiverNo + "", CommonFunctions.ConStr);
                                mSchAchDtls.Amount = tRewardDetails.DiscAmount;
                                mSchAchDtls.SignCode = 2;
                                mSchAchDtls.CompanyNo = DBGetVal.CompanyNo;
                                mSchAchDtls.UserID = DBGetVal.UserID;
                                mSchAchDtls.UserDate = DBGetVal.ServerTime;
                                mSchAchDtls.SchemeAchieverNo = tRewardDetails.SchemeAcheiverNo;
                                dbTVoucherEntry.AddMSchemeAchieverDetails(mSchAchDtls);
                            }
                        }
                    }
                    Rwdflag = true;
                }
                return Rwdflag;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return Rwdflag;
            }
        }

        private void setCompanyRatio()
        {
            try
            {
                DataTable dtTemp = new DataTable();
                bool TempFlag = false;
                dtTemp.Columns.Add();
                DataRow dr = dtTemp.NewRow();
                dr[0] = Convert.ToInt64(dgBill.Rows[0].Cells[ColIndex.StockCompanyNo].Value);
                dtTemp.Rows.Add(dr);
                for (int k = 1; k < dgBill.Rows.Count - 1; k++)
                {
                    for (int i = 0; i < dtTemp.Rows.Count; i++)
                    {
                        if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.StockCompanyNo].Value) != Convert.ToInt64(dtTemp.Rows[i].ItemArray[0].ToString()))
                        {
                            TempFlag = true;
                        }
                        else
                        {
                            TempFlag = false;
                            break;
                        }
                    }
                    if (TempFlag == true)
                    {
                        dr = dtTemp.NewRow();
                        dr[0] = Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.StockCompanyNo].Value);
                        dtTemp.Rows.Add(dr);
                    }
                }

                dtCompRatio = new DataTable();
                dtCompRatio.Columns.Add();
                dtCompRatio.Columns.Add();
                double debit = 0;
                for (int k = 0; k < dtTemp.Rows.Count; k++)
                {
                    for (int j = 0; j < dgBill.Rows.Count - 1; j++)
                    {
                        if (Convert.ToInt64(dgBill.Rows[j].Cells[ColIndex.StockCompanyNo].Value) == Convert.ToInt64(dtTemp.Rows[k].ItemArray[0].ToString()))
                        {
                            debit = debit + Convert.ToDouble(dgBill.Rows[j].Cells[ColIndex.Amount].Value);
                        }
                    }

                    DataRow dr1 = dtCompRatio.NewRow();
                    dr1[0] = Convert.ToInt64(dtTemp.Rows[k].ItemArray[0].ToString());
                    if (debit > 0)
                        dr1[1] = (debit * 10) / ((Convert.ToDouble(txtGrandTotal.Text) + Convert.ToDouble(txtTotalAnotherDisc.Text) - Convert.ToDouble(txtRoundOff.Text)) - Convert.ToDouble(txtTotalChrgs.Text));
                    else dr1[1] = 1;
                    dtCompRatio.Rows.Add(dr1);
                    debit = 0;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private bool ValidationsMain()
        {
            bool flag = false;
            try
            {
                if (ObjFunction.GetComboValue(cmbPartyName) <= 0)
                {
                    OMMessageBox.Show("Please Select Party Name", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    cmbPartyName.Focus();
                }
                else if (ObjFunction.GetComboValueString(cmbRateType) == "")
                {
                    OMMessageBox.Show("Please Select Rate Type", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    cmbRateType.Focus();
                }
                else if (ObjFunction.GetComboValue(cmbTaxType) <= 0)
                {
                    OMMessageBox.Show("Please Select Tax Type", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    cmbTaxType.Focus();
                }
                else if (ObjFunction.GetComboValue(cmbPaymentType) <= 0)
                {
                    OMMessageBox.Show("Please Select Payment Type", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    cmbTaxType.Focus();
                }
                else flag = true;

                if (flag == true)
                {
                    for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                    {
                        DataGridViewRow dr = dgBill.Rows[i];
                        if (dr.Cells[ColIndex.PkBarCodeNo].Value == null && dr.Cells[ColIndex.ItemNo].Value == null && dr.Cells[ColIndex.TaxLedgerNo].Value == null && dr.Cells[ColIndex.SalesLedgerNo].Value == null && dr.Cells[ColIndex.PkRateSettingNo].Value == null && dr.Cells[ColIndex.PkItemTaxInfo].Value == null)
                        {
                            flag = false;
                            OMMessageBox.Show("Please Fill properly Sr No. " + (i + 1) + " of item..", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            break;
                        }
                        else if (dgBill.Rows[i].Cells[ColIndex.PkBarCodeNo].Value.ToString() == "0")
                        {
                            flag = false;
                            OMMessageBox.Show("Please Fill properly Sr No. " + (i + 1) + " of item..", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            break;
                        }
                    }
                }
                //Open Pay Type for Cash Sales.
                //if (flag == true)
                //{
                //    if (ObjFunction.GetComboValue(cmbPartyName) == Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_PartyAC)))
                //    {
                //        long ControlUnder = ObjQry.ReturnLong("Select ControlUnder From MPayType Where PKPayTypeNo=" + ObjFunction.GetComboValue(cmbPaymentType) + "", CommonFunctions.ConStr);
                //        //if (ObjFunction.GetComboValue(cmbPaymentType) != 2)
                //        if (ControlUnder != 2)
                //        {
                //            OMMessageBox.Show("Please Select Proper Party Name.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                //            cmbPaymentType.Focus();
                //            while (dgPayChqDetails.Rows.Count > 0)
                //                dgPayChqDetails.Rows.RemoveAt(0);
                //            while (dgPayCreditCardDetails.Rows.Count > 0)
                //                dgPayCreditCardDetails.Rows.RemoveAt(0);
                //            flag = false;
                //        }
                //    }
                //}
                return flag;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return false;
            }
        }

        private bool Validations()
        {
            bool flag = true;
            if (txtDiscount1.Text == "")
            {
                txtDiscount1.Text = "0.00";
                flag = true;
            }
            else if (ObjFunction.CheckValidAmount(txtDiscount1.Text) == false)
            {
                txtDiscount1.Text = "0.00";
                flag = true;
            }
            if (txtChrgRupees1.Text == "")
            {
                txtChrgRupees1.Text = "0.00";
                flag = true;
            }
            else if (ObjFunction.CheckValidAmount(txtChrgRupees1.Text) == false)
            {
                txtChrgRupees1.Text = "0.00";
                flag = true;
            }
            //if (txtRemark.Text.Trim() == "")
            //{
            //    txtRemark.Text = "Sales Bill";
            //}

            return flag;
        }

        private void Control_Leave(object sender, EventArgs e)
        {
            try
            {
                double TotalAmt = 0;
                TotalAmt = ((Convert.ToDouble(txtSubTotal.Text) + Convert.ToDouble(txtTotalTax.Text)) - Convert.ToDouble(txtTotalDisc.Text));
                if (((TextBox)sender).Name == "txtDiscount1")
                {
                    //if (((TextBox)sender).Text == "") ((TextBox)sender).Text = "0.00";
                    txtOtherDisc.Text = Format.DoubleFloating;
                    txtSchemeDisc.Text = Format.DoubleFloating;
                    if (ObjFunction.CheckValidAmount(((TextBox)sender).Text) == false)
                    {
                        OMMessageBox.Show("Enter Discount Value.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        ((TextBox)sender).Focus();
                    }
                    else
                    {
                        //txtDiscRupees1.Focus();//txtChrgRupees1.Focus();//txtDiscRupees1.Text = Convert.ToDouble((TotalAmt * Convert.ToDouble(txtDiscount1.Text)) / 100).ToString("0.00");
                        CalculateTotal();
                    }
                }
                if (((TextBox)sender).Name == "txtOtherDisc")
                {
                    //if (((TextBox)sender).Text == "") ((TextBox)sender).Text = "0.00";
                    if (ObjFunction.CheckValidAmount(((TextBox)sender).Text) == false)
                    {
                        OMMessageBox.Show("Enter Discount Value.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        ((TextBox)sender).Focus();
                    }
                    else
                    {
                        //txtDiscRupees1.Focus();//txtChrgRupees1.Focus();//txtDiscRupees1.Text = Convert.ToDouble((TotalAmt * Convert.ToDouble(txtDiscount1.Text)) / 100).ToString("0.00");
                        CalculateTotal();
                    }
                }
                else if (((TextBox)sender).Name == "txtDiscRupees1")
                {
                    txtOtherDisc.Text = Format.DoubleFloating;
                    txtSchemeDisc.Text = Format.DoubleFloating;
                    // if (((TextBox)sender).Text == "") ((TextBox)sender).Text = "0.00";
                    if (ObjFunction.CheckValidAmount(((TextBox)sender).Text) == false)
                    {
                        OMMessageBox.Show("Enter Discount Value.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        ((TextBox)sender).Focus();
                    }
                    else
                    {

                        CalculateTotal();
                        // txtDiscount1.Text = Convert.ToDouble((100 * Convert.ToDouble(txtDiscRupees1.Text)) / TotalAmt).ToString("0.00");
                        // txtDiscount1.Text = "0.00";
                        // txtChrgRupees1.Focus();
                    }
                }
                else if (((TextBox)sender).Name == "txtSchemeDisc")
                {
                    CalculateTotal();
                }
                else if (((TextBox)sender).Name == "txtChrgRupees1")
                {
                    if (ObjFunction.CheckValidAmount(((TextBox)sender).Text) == false)
                    {
                        OMMessageBox.Show("Enter Charges Value.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        ((TextBox)sender).Focus();
                    }
                    else
                    {
                        if (cmbPaymentType.Enabled == true)
                            cmbPaymentType.Focus();
                        else
                            txtRemark.Focus();
                        CalculateTotal();
                    }
                }
                else if (((TextBox)sender).Name == "txtAddFreight")
                {
                    if (ObjFunction.CheckValidAmount(((TextBox)sender).Text) == false)
                    {
                        OMMessageBox.Show("Enter Charges Value.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        ((TextBox)sender).Focus();
                    }
                    else
                    {
                        if (cmbPaymentType.Enabled == true)
                            cmbPaymentType.Focus();
                        else
                            txtRemark.Focus();
                        CalculateTotal();
                    }
                }

                else if (((TextBox)sender).Name == "txtInvNo")
                {
                    ObjFunction.GetFinancialYear(dtpBillDate.Value, out dtFrom, out dtTo);
                    if (ObjFunction.CheckNumeric(((TextBox)sender).Text) == false)
                    {
                        OMMessageBox.Show("Enter Valid No.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        ((TextBox)sender).Focus();
                    }
                    else if (ObjQry.ReturnLong("Select VoucherUserNo from TVoucherEntry where VoucherTypeCode=" + VchType.Sales + " and VoucherUserNo=" + Convert.ToInt64(((TextBox)sender).Text) + " AND VoucherDate>='" + dtFrom.Date + "'  And ( PkVoucherNo <> " + ID + " ) AND VoucherDate<='" + dtTo.Date + "' ", CommonFunctions.ConStr) != 0)
                    {
                        OMMessageBox.Show("No Already Exist.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        ((TextBox)sender).Focus();
                    }
                    else
                    {
                        if (Convert.ToBoolean(AppSettings.S_IsManualBillNo) == true)
                            VoucherUserNo = Convert.ToInt64(txtInvNo.Text.Trim());
                        dtpBillDate.Focus();
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #region dgBill Methods and Events
        private delegate void MovetoNext(int RowIndex, int ColIndex, DataGridView dg);

        private void m2n(int RowIndex, int ColIndex, DataGridView dg)
        {
            try
            {
                // if (dg.CurrentCell.Value != null)
                dg.CurrentCell = dg.Rows[RowIndex].Cells[ColIndex];
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }
        private void m2n1(int RowIndex, int ColIndex, DataGridView dg)
        {
            try
            {

                dg.CurrentCell = dg.Rows[RowIndex].Cells[ColIndex];
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }
        private void Desc_Start()
        {
            try
            {
                if (dgBill.CurrentCell.Value == null || Convert.ToString(dgBill.CurrentCell.Value) == "")
                {
                    ItemType = 1;
                    FillItemList(0, ItemType); //FillItemList();
                }
                else
                {
                    ItemType = 2;
                    long[] BarcodeNo = null; long[] ItemNo = null;

                    //new code
                    switch (dgBill.CurrentCell.Value.ToString().Trim().ToUpper())
                    {
                        case "SV":
                            {
                                if (btnSave.Visible)
                                {
                                    //cmbPaymentType.SelectedValue = "2";
                                    PrintAsk = 2;
                                    btnSave_Click(btnSave, null);
                                    return;
                                }
                                break;
                            }
                        case "SVP":
                            {
                                if (btnSave.Visible)
                                {
                                    //cmbPaymentType.SelectedValue = "2";
                                    PrintAsk = 1;
                                    btnSave_Click(btnSave, null);
                                    return;
                                }
                                break;
                            }
                        case "CRP":
                            {
                                if (btnSave.Visible)
                                {
                                    dgBill.CurrentCell.Value = "";
                                    cmbPaymentType.SelectedValue = "3";
                                    for (int i = 0; i < dgPayType.Rows.Count; i++)
                                    {
                                        dgPayType.Rows[i].Cells[2].Value = "0.00";
                                    }
                                    cmbPartyName.Focus();
                                    return;
                                }
                                break;
                            }
                        case "CHQ":
                            {
                                if (btnSave.Visible)
                                {
                                    dgBill.CurrentCell.Value = "";
                                    cmbPaymentType.SelectedValue = "4";
                                    cmbPaymentType_KeyDown(cmbPaymentType, new KeyEventArgs(Keys.Enter));
                                    return;
                                }
                                break;
                            }
                        case "CRD":
                            {
                                if (btnSave.Visible)
                                {
                                    dgBill.CurrentCell.Value = "";
                                    cmbPaymentType.SelectedValue = "5";
                                    cmbPaymentType_KeyDown(cmbPaymentType, new KeyEventArgs(Keys.Enter));
                                    return;
                                }
                                break;
                            }
                        case "B":
                            {
                                if (btnSave.Visible)
                                {
                                    if (ObjFunction.GetComboValue(cmbPartyName) == Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_PartyAC)))// && (ObjFunction.GetComboValue(cmbPaymentType) == 3))
                                    {
                                        if (ObjFunction.GetComboValue(cmbPaymentType) == 3)
                                        {
                                            DisplayMessage("B- Short cut key not working..");
                                            dgBill.CurrentCell.Value = ""; return;
                                        }
                                        cmbPaymentType.SelectedValue = "2";
                                        pnlPartyName.Visible = true;
                                        if (ID == 0)
                                            txtPartyName.Text = "";
                                        txtPartyName.Focus();
                                        return;
                                    }
                                    else
                                    {
                                        DisplayMessage("B- Short cut key not working..");
                                        dgBill.CurrentCell.Value = ""; return;
                                    }
                                }
                                break;
                            }
                        case "N":
                            {
                                if (btnSave.Visible)
                                {
                                    if (ObjFunction.GetComboValue(cmbPaymentType) == 3)
                                    {
                                        DisplayMessage("N- Short cut key not working..");
                                        dgBill.CurrentCell.Value = ""; return;
                                    }
                                    PrintAsk = 2;
                                    cmbPaymentType.SelectedValue = "2";
                                    btnSave_Click(btnSave, new EventArgs());
                                    return;
                                }
                                break;
                            }
                        default:
                            {
                                SearchBarcode(dgBill.CurrentCell.Value.ToString().Trim(), out ItemNo, out BarcodeNo);
                                dgBill.CurrentRow.Cells[ColIndex.PkRateSettingNo].Value = 0;
                                break;
                            }

                    }

                    //old code
                    //SearchBarcode(dgBill.CurrentCell.Value.ToString().Trim(), out ItemNo, out BarcodeNo);

                    if (ItemNo.Length == 0 || BarcodeNo.Length == 0)
                    {
                        string strB = dgBill.CurrentCell.FormattedValue.ToString();
                        dgBill.CurrentCell.Value = null;
                        dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkBarCodeNo].Value = "0";
                        //DisplayMessage("Barcode Not Found");
                        try
                        {
                            player.Play();
                            if (OMMessageBox.Show("Barcode Not Found.\nPRESS ESCAPE TO CONTINUE...." + Environment.NewLine + "Press Ctrl+N New Item..", "Information", OMMessageBoxButton.EscapeButton, OMMessageBoxIcon.Information) == DialogResult.No)
                            //  if (OMMessageBox.Show("Barcode Not Found.\nPRESS ESCAPE TO CONTINUE....", "Information", OMMessageBoxButton.OK, OMMessageBoxIcon.Information) == DialogResult.OK)
                            {
                                player.Stop();
                                MovetoNext move2n = new MovetoNext(m2n);
                                BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex, ColIndex.ItemName, dgBill });
                            }
                            else
                            {
                                player.Stop();
                                NewItemAdd(strB);
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    else
                    {
                        if (ItemNo.Length > 1)
                        {
                            ItemType = 3;
                            FillItemList(0, ItemType);//FillItemList();
                        }
                        else
                        {
                            int pos = -1;
                            for (int s = dgBill.RowCount - 2; s >= 0; s--)
                            {
                                if (Convert.ToInt64(dgBill.Rows[s].Cells[ColIndex.ItemNo].Value) == ItemNo[0])
                                {
                                    pos = s;
                                    break;
                                }
                            }
                            if (pos != -1 && pos != dgBill.RowCount - 2)
                            {
                                int totalRows = dgBill.Rows.Count;
                                DataGridViewRow selectedRow = dgBill.Rows[pos];
                                dgBill.Rows.Remove(selectedRow);
                                dgBill.Rows.Insert(dgBill.Rows.Count - 1, selectedRow);
                            }

                            int rwindex = 0;
                            if (ItemExist(ItemNo[0], out rwindex) == true)
                            {
                                dgBill.Rows[rwindex].Cells[ColIndex.Quantity].Value = Convert.ToDouble(dgBill.Rows[rwindex].Cells[ColIndex.Quantity].Value) + 1;
                                dgBill.CurrentRow.Cells[ColIndex.ItemName].Value = "";
                                dgBill.CurrentCell = dgBill[ColIndex.Quantity, rwindex];
                                dgBill_CellValueChanged(dgBill, new DataGridViewCellEventArgs(ColIndex.Quantity, rwindex));
                                dgBill_KeyDown(dgBill, new KeyEventArgs(Keys.Enter));
                            }
                            else
                            {
                                dgBill.CurrentRow.Cells[ColIndex.Barcode].Value = dgBill.CurrentCell.Value;
                                Desc_MoveNext(ItemNo[0], BarcodeNo[0]);
                            }
                        }
                    }
                    //BindGrid();
                    //CalculateTotal();
                }

                //from key_down
                //ItemType = 1;
                //FillItemList();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void SearchBarcode(String strBarcode, out long[] ItemNo, out long[] BarcodeNo)
        {

            //string sql = "Select PkStockBarcodeNo, ItemNo from MStockBarcode where Barcode = '" + strBarcode + "' And IsActive ='true'";
            string sql = "";
            DataTable dt = new DataTable();
            sql = "SELECT     MStockBarcode.PkStockBarcodeNo, MStockBarcode.ItemNo,MStockBarcode.Barcode FROM MStockBarcode INNER JOIN MStockItems ON MStockBarcode.ItemNo = MStockItems.ItemNo " +
                " INNER JOIN MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo " +
                " WHERE ((MStockBarcode.Barcode = '" + strBarcode + "') or (MStockItems.ShortCode = '" + strBarcode + "')) AND (MStockItems.IsActive = 'true') AND (MRateSetting.IsActive='true')";
            dt = ObjFunction.GetDataView(sql).Table;
            BarcodeNo = new long[dt.Rows.Count];
            ItemNo = new long[dt.Rows.Count];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    BarcodeNo[i] = Convert.ToInt64(dt.Rows[i].ItemArray[0].ToString());
                    ItemNo[i] = Convert.ToInt64(dt.Rows[i].ItemArray[1].ToString());
                    dgBill.CurrentCell.Value = dt.Rows[i].ItemArray[2].ToString();
                }
            }
            else
            {
                //sql = "SELECT     MStockBarcode.PkStockBarcodeNo, MStockBarcode.ItemNo,MStockBarcode.Barcode FROM MStockBarcode INNER JOIN MStockItems ON MStockBarcode.ItemNo = MStockItems.ItemNo " +
                //" WHERE (MStockItems.ShortCode = '" + strBarcode + "') AND (MStockItems.IsActive = 'true')";
                //dt = ObjFunction.GetDataView(sql).Table;
                //BarcodeNo = new long[dt.Rows.Count];
                //ItemNo = new long[dt.Rows.Count];
                //if (dt.Rows.Count > 0)
                //{
                //    for (int i = 0; i < dt.Rows.Count; i++)
                //    {
                //        BarcodeNo[i] = Convert.ToInt64(dt.Rows[i].ItemArray[0].ToString());
                //        ItemNo[i] = Convert.ToInt64(dt.Rows[i].ItemArray[1].ToString());
                //        dgBill.CurrentCell.Value = dt.Rows[i].ItemArray[2].ToString();
                //    }

                //}

            }
        }

        private void Desc_MoveNext(long ItemNo, long BarcodeNo)
        {
            try
            {
                dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemNo].Value = ItemNo;
                dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkBarCodeNo].Value = BarcodeNo;
                DataTable dtItem = ObjFunction.GetDataView("Select ShowItemName as ItemName,CompanyNo,GodownNo,DiscountType,HamaliInKg,IsQtyRead from MStockItems Where  Itemno =" + ItemNo + " And IsActive='true' ").Table;//where ItemNo = " + ItemNo
                dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemName].Value = dtItem.Rows[0]["ItemName"].ToString();
                dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.StockCompanyNo].Value = dtItem.Rows[0]["CompanyNo"].ToString();
                dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.GodownNo].Value = (ObjFunction.GetAppSettings(AppSettings.S_DefaultStockLocation).ToString() == "0") ? dtItem.Rows[0]["GodownNo"].ToString() : ObjFunction.GetAppSettings(AppSettings.S_DefaultStockLocation).ToString();
                dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.DiscountType].Value = dtItem.Rows[0]["DiscountType"].ToString();
                dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.HamaliInKg].Value = dtItem.Rows[0]["HamaliInKg"].ToString();
                dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.IsQtyRead].Value = (dtItem.Rows[0]["IsQtyRead"].ToString().Trim() == "") ? "false" : dtItem.Rows[0]["IsQtyRead"].ToString();

                //dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemName].Value = ObjQry.ReturnString("Select ItemName from MStockItems_V(NULL,NULL,NULL,NULL,NULL,NULL,NULL) where ItemNo = " + ItemNo,CommonFunctions.ConStr);

                if (ItemType == 2)
                    dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemName].Value += " - " + dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.Barcode].Value.ToString();

                if (Convert.ToBoolean(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.IsQtyRead].FormattedValue.ToString()) == true)
                {
                    Form frm = new Vouchers.QtyReadDialog();
                    ObjFunction.OpenForm(frm);
                    if (Vouchers.QtyReadDialog.ScanQty != "")
                    {
                        if (Vouchers.QtyReadDialog.ScanQty == "Cancel")
                        {
                            for (int i = 1; i < dgBill.Columns.Count; i++)
                            {
                                dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[i].Value = null;
                            }
                            dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemName].ReadOnly = false;
                            MovetoNext move2n = new MovetoNext(m2n);
                            BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex, ColIndex.ItemName, dgBill });
                        }
                        else if (Vouchers.QtyReadDialog.ScanQty == "Manual")
                        {
                            dgBill.CurrentCell = dgBill[ColIndex.Quantity, dgBill.CurrentCell.RowIndex];
                            dgBill.Focus();
                        }
                        else
                        {
                            dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.Quantity].Value = Vouchers.QtyReadDialog.ScanQty;
                            Qty_MoveNext();
                        }
                    }
                }
                else
                {
                    if (StopOnQty == true)
                    {
                        if (dgBill[2, dgBill.CurrentCell.RowIndex].Value == null)
                        {
                            dgBill.CurrentCell = dgBill[2, dgBill.CurrentCell.RowIndex];
                            dgBill.Focus();
                        }
                        else
                            Qty_MoveNext();
                    }
                    else
                    {
                        if (dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.Quantity].Value == null)
                            dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.Quantity].Value = "1";
                        Qty_MoveNext();
                    }
                }

                //BindGrid();
                //CalculateTotal();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void Qty_MoveNext()
        {
            try
            {
                rowQtyIndex = dgBill.CurrentCell.RowIndex;

                MovetoNext move2n = new MovetoNext(m2n);
                BeginInvoke(move2n, new object[] { rowQtyIndex, ColIndex.UOM, dgBill });

                UOM_Start();

                //CalculateTotal();//temp

                // dgBill.Rows[rowQtyIndex].Cells[2].ReadOnly = true;

                //found in the dgBill_keydown
                //if (dgBill.CurrentCell.ColumnIndex == 2)
                //{
                //    if (dgBill.Rows.Count > 1)
                //    {
                //        row = (dgBill.CurrentCell.RowIndex == 0) ? 0 : dgBill.CurrentCell.RowIndex;
                //        if (Convert.ToString(dgBill.Rows[row].Cells[2].Value) != "")
                //        {
                //            dgBill.CurrentCell = dgBill[3, row];
                //            dgBill.CurrentCell.ReadOnly = false;
                //        }
                //    }
                //}
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void UOM_Start()
        {
            try
            {
                int row = 0;
                if (dgBill.CurrentCell.RowIndex == 0)
                    row = dgBill.CurrentCell.RowIndex;
                else
                    row = dgBill.CurrentCell.RowIndex;

                dgBill.CurrentCell = dgBill[ColIndex.UOM, row];

                //dgBill.CurrentCell.ReadOnly = false;
                FillUOMList(row);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void UOM_MoveNext()
        {
            try
            {
                int Row = dgBill.CurrentCell.RowIndex;

                if (dgBill.CurrentRow.Cells[ColIndex.UOMNo].Value != null &&
                    dgBill.CurrentRow.Cells[ColIndex.UOMNo].Value.ToString() != lstUOM.SelectedValue.ToString())
                {
                    dgBill.CurrentRow.Cells[ColIndex.Rate].Value = "0.00";//lstRate.Text;
                    dgBill.CurrentRow.Cells[ColIndex.PkRateSettingNo].Value = 0;//lstRate.SelectedValue;
                }

                dgBill.CurrentRow.Cells[ColIndex.UOM].Value = lstUOM.Text;
                dgBill.CurrentRow.Cells[ColIndex.UOMNo].Value = Convert.ToInt64(lstUOM.SelectedValue);
                pnlUOM.Visible = false;

                Rate_Start();
                //CalculateTotal();//temp
                //CalculateGridValues(Row);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void Rate_Start()
        {
            try
            {

                string str;
                //CalculateGridValues();
                int RowIndex = dgBill.CurrentCell.RowIndex;
                long ItemNo = Convert.ToInt64(dgBill.Rows[RowIndex].Cells[ColIndex.ItemNo].Value);
                long BarcodeNo = Convert.ToInt64(dgBill.Rows[RowIndex].Cells[ColIndex.PkBarCodeNo].Value);
                long UOMNo = Convert.ToInt64(dgBill.Rows[RowIndex].Cells[ColIndex.UOMNo].Value);
                double Qty = Convert.ToDouble(dgBill.Rows[RowIndex].Cells[ColIndex.Quantity].Value);

                if (dgBill.Rows[RowIndex].Cells[ColIndex.PkRateSettingNo].Value == null ||
                Convert.ToInt64(dgBill.Rows[RowIndex].Cells[ColIndex.PkRateSettingNo].Value) == 0)
                {
                    ObjFunction.FillList(lstRate, "pksrno", ObjFunction.GetComboValueString(cmbRateType));
                    if (ItemType == 2)
                    {
                        str = "select pksrno," + ObjFunction.GetComboValueString(cmbRateType) +
                            " from GetItemRateAll(" + ItemNo + "," + BarcodeNo + "," + UOMNo + ",null ,'" + dtpBillDate.Value.ToString("dd-MMM-yyyy") + " " + DBGetVal.ServerTime.ToLongTimeString() + "',null)";
                    }
                    else
                    {
                        str = "select pksrno," + ObjFunction.GetComboValueString(cmbRateType) +
                            " from GetItemRateAll(" + ItemNo + ",null," + UOMNo + ",null ,'" + dtpBillDate.Value.ToString("dd-MMM-yyyy") + " " + DBGetVal.ServerTime.ToLongTimeString() + "',null)";
                    }

                    ObjFunction.FillList(lstRate, str);

                    if (lstRate.Items.Count == 1)
                    {
                        lstRate.SelectedIndex = 0;
                        dgBill.Rows[RowIndex].Cells[ColIndex.Rate].Value = lstRate.Text;
                        dgBill.Rows[RowIndex].Cells[ColIndex.PkRateSettingNo].Value = lstRate.SelectedValue;

                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { RowIndex, ColIndex.Rate, dgBill });
                        dgBill.CurrentCell = dgBill[ColIndex.Rate, RowIndex];
                        dgBill.Focus();
                        //BindGrid(dgBill.CurrentRow.Index);

                        if (StopOnRate == false)
                        {
                            Rate_MoveNext();
                        }
                        else
                        {
                            BindGrid(dgBill.CurrentRow.Index);
                        }
                    }
                    else if (lstRate.Items.Count > 1)
                    {
                        if (flagParking == true)
                        {
                            lstRate.Text = dgBill.Rows[RowIndex].Cells[ColIndex.Rate].Value.ToString();
                            dgBill.Rows[RowIndex].Cells[ColIndex.PkRateSettingNo].Value = lstRate.SelectedValue;

                            MovetoNext move2n = new MovetoNext(m2n);
                            BeginInvoke(move2n, new object[] { RowIndex, ColIndex.Rate, dgBill });
                            dgBill.CurrentCell = dgBill[ColIndex.Rate, RowIndex];
                            dgBill.Focus();
                            if (StopOnRate == false)
                                Rate_MoveNext();
                            else
                                BindGrid(dgBill.CurrentRow.Index);
                        }
                        else
                        {
                            MovetoNext move2n = new MovetoNext(m2n);
                            BeginInvoke(move2n, new object[] { RowIndex, ColIndex.Rate, dgBill });
                            dgBill.CurrentCell = dgBill[ColIndex.Rate, RowIndex];
                            dgBill.Focus();
                            CalculateTotal();
                            pnlRate.Visible = true;
                            lstRate.Focus();
                        }
                    }
                    else
                    {
                        //error invalid Qty or UOM
                    }
                }
                else
                {
                    MovetoNext move2n = new MovetoNext(m2n);
                    BeginInvoke(move2n, new object[] { RowIndex, ColIndex.Rate, dgBill });
                    dgBill.CurrentCell = dgBill[ColIndex.Rate, RowIndex];
                    dgBill.Focus();
                    //BindGrid(dgBill.CurrentRow.Index);

                    if (StopOnRate == false)
                    {
                        Rate_MoveNext();
                    }
                    else
                    {
                        BindGrid(dgBill.CurrentRow.Index);
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void Rate_MoveNext()
        {
            try
            {
                if (dgBill.CurrentCell.Value != null)
                {
                    if (ObjFunction.CheckValidAmount(dgBill.CurrentCell.Value.ToString()) == true)
                    {
                        dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.Rate].Value = Convert.ToDouble(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.Rate].Value).ToString("0.00");
                        //dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.Amount].Value = (Convert.ToDouble(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.Rate].Value) * Convert.ToDouble(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[2].Value)) / Convert.ToDouble(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.MKTQuantity].Value);
                        //dgBill.CurrentCell.ReadOnly = true;
                        BindGrid(dgBill.CurrentCell.RowIndex);
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_StopOnDisc)) == false)
                        {
                            MovetoNext move2n = new MovetoNext(m2n);
                            BeginInvoke(move2n, new object[] { dgBill.Rows.Count - 1, ColIndex.ItemName, dgBill });
                            dgBill.CurrentCell = dgBill[1, dgBill.Rows.Count - 1];
                            dgBill.Focus();

                            if (ItemType == 1 && Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_StopOnGrid)) == false)
                            {
                                Desc_Start();
                            }
                        }
                        else
                        {
                            MovetoNext move2n = new MovetoNext(m2n);
                            BeginInvoke(move2n, new object[] { dgBill.CurrentRow.Index, ColIndex.DiscRupees, dgBill });
                            dgBill.CurrentCell = dgBill[ColIndex.DiscRupees, dgBill.CurrentRow.Index];
                            dgBill.Focus();

                        }

                        //CalculateTotal();
                    }
                    else
                    {
                        dgBill.CurrentCell.ErrorText = "Please Enter valid rate...";
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void Disc_MoveNext()
        {
            //Rate_MoveNext();
            CalculateTotal();
        }

        private void delete_row()
        {
            try
            {
                bool flag;// bool ItemLevelDisc = false;
                if (dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkStockTrnNo].Value != null)
                {
                    if (dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.SchemeDetailsNo].FormattedValue.ToString() == "" ||
                        dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.SchemeDetailsNo].FormattedValue.ToString() == "0")
                    {
                        if (OMMessageBox.Show("Are you sure want to delete this item ?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question, OMMessageBoxDefaultButton.Button2) == DialogResult.Yes)
                        {
                            long PKStockTrnNo = Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkStockTrnNo].Value);
                            if (PKStockTrnNo != 0)
                            {
                                DeleteDtls(1, PKStockTrnNo);
                                double MRP = ObjQry.ReturnDouble("Select MRP From MRateSetting Where PkSrNo=" + dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkRateSettingNo].Value + "", CommonFunctions.ConStr);

                                DeleteCurrentStock(Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemNo].Value),
                                    MRP,
                                    Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.GodownNo].Value),
                                    Convert.ToDouble(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.Quantity].Value), 1);

                                if (dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.FKItemLevelDiscNo].FormattedValue.ToString() != "" && dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.FKItemLevelDiscNo].FormattedValue.ToString() != "0")
                                {
                                    DeleteDtls(9, Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.FKItemLevelDiscNo].Value));
                                    dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.DiscRupees].Value = "0.00";
                                    //ItemLevelDisc = true;
                                }
                                //For Party LedgerNo
                                flag = false;
                                for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                                {
                                    if (dgBill.CurrentCell.RowIndex != i)
                                    {
                                        if (Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkVoucherNo].Value) == Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.PkVoucherNo].Value))
                                        { flag = true; break; }
                                    }
                                }
                                if (flag == false)
                                    DeleteDtls(2, Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkVoucherNo].Value));

                                //For Sales LedgerNo
                                flag = false;
                                for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                                {
                                    if (dgBill.CurrentCell.RowIndex != i)
                                    {
                                        if (Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.SalesVchNo].Value) == Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.SalesVchNo].Value))
                                        { flag = true; break; }
                                    }
                                }
                                if (flag == false)
                                    DeleteDtls(2, Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.SalesVchNo].Value));

                                //FOr TaxLedgerNo
                                flag = false;
                                for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                                {
                                    if (dgBill.CurrentCell.RowIndex != i)
                                    {
                                        if (Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.TaxVchNo].Value) == Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.TaxVchNo].Value))
                                        { flag = true; break; }
                                    }
                                }
                                if (flag == false)
                                    DeleteDtls(2, Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.TaxVchNo].Value));
                            }

                            if (dgBill.Rows.Count - 1 == dgBill.CurrentCell.RowIndex)
                            {
                                dgBill.Rows.RemoveAt(dgBill.CurrentCell.RowIndex);
                                dgBill.Rows.Add();
                            }
                            else
                                dgBill.Rows.RemoveAt(dgBill.CurrentCell.RowIndex);

                            txtOtherDisc.Text = Format.DoubleFloating;
                            txtSchemeDisc.Text = Format.DoubleFloating;
                            FooterDiscDtlsNo = 0;
                            CalculateTotal();
                            dgBill.CurrentCell = dgBill[ColIndex.ItemName, dgBill.Rows.Count - 1];
                        }
                    }
                    else
                        OMMessageBox.Show("This item already asssigned to scheme. Not allowed to delete this item.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void delete_rowReward()
        {
            try
            {
                bool flag;
                if (dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkStockTrnNo].Value != null)
                {

                    long PKStockTrnNo = Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkStockTrnNo].Value);
                    if (PKStockTrnNo != 0)
                    {
                        DeleteDtls(1, PKStockTrnNo);
                        flag = false;
                        for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                        {
                            if (dgBill.CurrentCell.RowIndex != i)
                            {
                                if (Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkVoucherNo].Value) == Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.PkVoucherNo].Value))
                                { flag = true; break; }
                            }
                        }
                        if (flag == false)
                            DeleteDtls(2, Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkVoucherNo].Value));

                        //For Sales LedgerNo
                        flag = false;
                        for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                        {
                            if (dgBill.CurrentCell.RowIndex != i)
                            {
                                if (Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.SalesVchNo].Value) == Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.SalesVchNo].Value))
                                { flag = true; break; }
                            }
                        }
                        if (flag == false)
                            DeleteDtls(2, Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.SalesVchNo].Value));

                        //FOr TaxLedgerNo
                        flag = false;
                        for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                        {
                            if (dgBill.CurrentCell.RowIndex != i)
                            {
                                if (Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.TaxVchNo].Value) == Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.TaxVchNo].Value))
                                { flag = true; break; }
                            }
                        }
                        if (flag == false)
                            DeleteDtls(2, Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.TaxVchNo].Value));


                    }

                    if (dgBill.Rows.Count - 1 == dgBill.CurrentCell.RowIndex)
                    {
                        dgBill.Rows.RemoveAt(dgBill.CurrentCell.RowIndex);
                        dgBill.Rows.Add();
                    }
                    else
                        dgBill.Rows.RemoveAt(dgBill.CurrentCell.RowIndex);

                    CalculateTotal();

                    dgBill.CurrentCell = dgBill[ColIndex.ItemName, dgBill.Rows.Count - 1];
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgBill_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (Spaceflag == false) { Spaceflag = true; return; }
                if (dgBill.CurrentCell.ColumnIndex == ColIndex.ItemName)
                {
                    if (dgBill.CurrentCell.Value != null && Convert.ToString(dgBill.CurrentCell.Value) != "")
                    {
                        Desc_Start();
                    }
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.Quantity)
                {
                    if (dgBill.CurrentCell.Value != null)
                    {
                        if (ObjFunction.CheckValidAmount(dgBill.CurrentCell.Value.ToString()) == true)
                        {
                            if (Convert.ToDouble(dgBill.CurrentCell.Value.ToString()) == 0) dgBill.CurrentCell.Value = "1";
                        }
                        else
                            dgBill.CurrentCell.Value = "1";
                    }
                    Qty_MoveNext();
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.UOM)
                {
                    UOM_Start();
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.Rate)
                {
                    Rate_MoveNext();
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscPercentage)
                {
                    Disc_MoveNext();
                    MovetoNext move2n = new MovetoNext(m2n1);
                    int colIndex = -1;
                    for (int i = e.ColumnIndex + 1; i < dgBill.Columns.Count; i++)
                    {
                        if (dgBill.Columns[i].Visible == true)
                        {
                            colIndex = i;
                            break;
                        }
                    }
                    if (colIndex != -1)
                        BeginInvoke(move2n, new object[] { e.RowIndex, colIndex, dgBill });
                    else
                        BeginInvoke(move2n, new object[] { e.RowIndex + 1, colIndex, dgBill });
                    //if (dgBill.Columns[ColIndex.DiscAmount].Visible == true)
                    //{
                    //    MovetoNext move2n = new MovetoNext(m2n);
                    //    BeginInvoke(move2n, new object[] { e.RowIndex, ColIndex.DiscAmount, dgBill });
                    //}
                    //else 
                    //{
                    //    MovetoNext move2n = new MovetoNext(m2n);
                    //    BeginInvoke(move2n, new object[] { e.RowIndex + 1, ColIndex.ItemName, dgBill });
                    //}
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscRupees)
                {
                    Disc_MoveNext();

                    MovetoNext move2n = new MovetoNext(m2n1);
                    int colIndex = -1;
                    for (int i = e.ColumnIndex + 1; i < dgBill.Columns.Count; i++)
                    {
                        if (dgBill.Columns[i].Visible == true)
                        {
                            if (ColIndex.Amount != i)
                            {
                                colIndex = i;
                                break;
                            }
                        }
                    }
                    if (colIndex != -1)
                        BeginInvoke(move2n, new object[] { e.RowIndex + 1, ColIndex.ItemName, dgBill });
                    // BeginInvoke(move2n, new object[] { e.RowIndex, colIndex, dgBill });
                    else
                        BeginInvoke(move2n, new object[] { e.RowIndex + 1, ColIndex.ItemName, dgBill });
                    //if (dgBill.Columns[ColIndex.DiscPercentage2].Visible == true)
                    //{
                    //    MovetoNext move2n = new MovetoNext(m2n);
                    //    BeginInvoke(move2n, new object[] { e.RowIndex, ColIndex.DiscPercentage2, dgBill });
                    //}
                    //else
                    //{
                    //    MovetoNext move2n = new MovetoNext(m2n);
                    //    BeginInvoke(move2n, new object[] { e.RowIndex + 1, ColIndex.Barcode, dgBill });
                    //}
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscPercentage2)
                {
                    Disc_MoveNext();
                    MovetoNext move2n = new MovetoNext(m2n);
                    int colIndex = -1;
                    for (int i = e.ColumnIndex + 1; i < dgBill.Columns.Count; i++)
                    {
                        if (dgBill.Columns[i].Visible == true)
                        {
                            colIndex = i;
                            break;
                        }
                    }
                    if (colIndex != -1)
                        BeginInvoke(move2n, new object[] { e.RowIndex, colIndex, dgBill });
                    else
                        BeginInvoke(move2n, new object[] { e.RowIndex + 1, colIndex, dgBill });
                    //if (dgBill.Columns[ColIndex.DiscAmount2].Visible == true)
                    //{
                    //    MovetoNext move2n = new MovetoNext(m2n);
                    //    BeginInvoke(move2n, new object[] { e.RowIndex, ColIndex.DiscAmount2, dgBill });
                    //}
                    //else
                    //{
                    //    MovetoNext move2n = new MovetoNext(m2n);
                    //    BeginInvoke(move2n, new object[] { e.RowIndex + 1, ColIndex.Barcode, dgBill });
                    //}
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscRupees2)
                {
                    Disc_MoveNext();
                    MovetoNext move2n = new MovetoNext(m2n);
                    int colIndex = -1;
                    for (int i = e.ColumnIndex + 1; i < dgBill.Columns.Count; i++)
                    {
                        if (dgBill.Columns[i].Visible == true)
                        {
                            colIndex = i;
                            break;
                        }
                    }
                    if (colIndex != -1)
                        BeginInvoke(move2n, new object[] { e.RowIndex, colIndex, dgBill });
                    else
                        BeginInvoke(move2n, new object[] { e.RowIndex + 1, colIndex, dgBill });
                    //MovetoNext move2n = new MovetoNext(m2n);
                    //BeginInvoke(move2n, new object[] { e.RowIndex, ColIndex.Amount, dgBill });
                }

            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

        }

        private void dgBill_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    RewardDeleteFlag = false;
                    delete_row();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    dgBill.Focus();
                    if (dgBill.CurrentCell.ColumnIndex == ColIndex.SrNo)
                    {
                        e.SuppressKeyPress = true;
                        dgBill.CurrentCell = dgBill[ColIndex.ItemName, dgBill.CurrentCell.RowIndex];
                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.ItemName)
                    {
                        e.SuppressKeyPress = true; txtBrandFilter.Text = "";
                        if (dgBill.CurrentRow.Cells[ColIndex.PkRateSettingNo].Value == null)
                        {
                            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_AddvanceSearch)) == false)
                            {
                                dgBill.CurrentCell.Value = "";
                                Desc_Start();
                            }
                            else
                            {
                                ItemSerach_Method();
                            }
                        }
                        else if (Convert.ToInt64(dgBill.CurrentRow.Cells[ColIndex.PkRateSettingNo].Value) == 0)
                        {
                            dgBill.CurrentCell.Value = "";
                            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_AddvanceSearch)) == false)
                            {
                                Desc_Start();
                            }
                            else
                            {
                                ItemSerach_Method();
                            }
                        }
                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.Quantity)
                    {
                        e.SuppressKeyPress = true;
                        if (dgBill.CurrentRow.Cells[ColIndex.ItemNo].EditedFormattedValue.ToString().Trim() != "" && dgBill.CurrentRow.Cells[ColIndex.ItemNo].EditedFormattedValue.ToString().Trim() != "0")
                        {
                            if (dgBill.CurrentCell.Value == null) dgBill.CurrentCell.Value = "1";
                            Qty_MoveNext();
                        }
                        else
                        {
                            dgBill.CurrentCell = dgBill.Rows[dgBill.Rows.Count - 1].Cells[ColIndex.ItemName];
                        }
                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.UOM)
                    {
                        e.SuppressKeyPress = true;
                        if (dgBill.CurrentRow.Cells[ColIndex.ItemNo].Value != null && dgBill.CurrentRow.Cells[ColIndex.ItemNo].Value.ToString() != "")
                        {
                            UOM_Start();
                        }
                        else
                        {
                            dgBill.CurrentCell = dgBill.Rows[dgBill.Rows.Count - 1].Cells[ColIndex.ItemName];
                        }
                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.MRP)
                    {
                        e.SuppressKeyPress = true;
                        dgBill.CurrentCell = dgBill.Rows[dgBill.Rows.Count - 1].Cells[ColIndex.ItemName];
                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.Rate)
                    {
                        e.SuppressKeyPress = true;
                        if (dgBill.CurrentRow.Cells[ColIndex.ItemNo].EditedFormattedValue.ToString().Trim() != "" && dgBill.CurrentRow.Cells[ColIndex.ItemNo].EditedFormattedValue.ToString().Trim() != "0")
                        {
                            Rate_MoveNext();
                        }
                        else
                        {
                            dgBill.CurrentCell = dgBill.Rows[dgBill.Rows.Count - 1].Cells[ColIndex.ItemName];
                        }
                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscRupees)
                    {
                        e.SuppressKeyPress = true;
                        if (dgBill.CurrentRow.Cells[ColIndex.ItemNo].EditedFormattedValue.ToString().Trim() != "" && dgBill.CurrentRow.Cells[ColIndex.ItemNo].EditedFormattedValue.ToString().Trim() != "0")
                        {
                            dgBill.CurrentCell = dgBill.Rows[dgBill.Rows.Count - 1].Cells[ColIndex.ItemName];
                        }
                        else
                        {
                            dgBill.CurrentCell = dgBill.Rows[dgBill.Rows.Count - 1].Cells[ColIndex.ItemName];
                        }
                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.Amount)
                    {
                        e.SuppressKeyPress = true;
                        dgBill.CurrentCell = dgBill.Rows[dgBill.Rows.Count - 1].Cells[ColIndex.ItemName];
                    }
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    //txtDiscount1.Focus();
                    cmbPaymentType.Focus();
                }
                else if (e.KeyCode == Keys.F8)
                {
                    if (dgBill.CurrentCell.Value != null)
                        dgBill.CurrentCell = dgBill[ColIndex.Rate, dgBill.CurrentCell.RowIndex];
                    else
                    {
                        if (dgBill.CurrentCell.RowIndex == 0)
                            dgBill.CurrentCell = dgBill[ColIndex.Rate, dgBill.CurrentCell.RowIndex];
                        else
                            dgBill.CurrentCell = dgBill[ColIndex.Rate, dgBill.CurrentCell.RowIndex - 1];
                    }
                }
                else if (e.KeyCode == Keys.F7)
                {
                    if (btnSave.Visible == true)
                    {
                        //DisplayStockGodown();
                        if (dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemNo].Value != null)
                            dgBill.CurrentCell = dgBill[ColIndex.Quantity, dgBill.CurrentCell.RowIndex];
                        else
                            dgBill.CurrentCell = dgBill[ColIndex.Quantity, dgBill.CurrentCell.RowIndex - 1];
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgBill_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //try
            //{
            //    int row, col;
            //    if (dgBill.CurrentCell != null)
            //    { col = dgBill.CurrentCell.ColumnIndex; row = dgBill.CurrentCell.RowIndex; }
            //    else { col = e.ColumnIndex; row = e.RowIndex; }
            //    if (dgBill.Rows.Count > 0)
            //        dgBill.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";
            //    if (col == ColIndex.Quantity && row >= 0)
            //    {
            //        dgBill.CurrentCell.ErrorText = "";
            //        if (dgBill.CurrentCell.Value != null)
            //        {
            //            if (dgBill.CurrentCell.Value.ToString() != "" && dgBill.CurrentCell.Value.ToString() != "0")
            //            {
            //                if (ObjFunction.CheckNumeric(dgBill.CurrentCell.Value.ToString()) == true)
            //                {
            //                    int rowIndex = dgBill.CurrentCell.RowIndex;
            //                    if (dgBill[ColIndex.Rate, dgBill.CurrentCell.RowIndex].Value == null || Convert.ToString(dgBill[ColIndex.Rate, dgBill.CurrentCell.RowIndex].Value) == "")
            //                        dgBill[ColIndex.Amount, dgBill.CurrentCell.RowIndex].Value = "0.00";
            //                    else
            //                        CalculateTotal();
            //                    dgBill.Focus();

            //                    dgBill.CurrentCell = dgBill[2, row];
            //                }
            //            }
            //        }
            //    }
            //    else if (col == ColIndex.Rate && row >= 0)
            //    {
            //        dgBill.CurrentCell.ErrorText = "";
            //        if (dgBill.CurrentCell.Value != null)
            //        {
            //            if (dgBill.CurrentCell.Value.ToString() != "" && dgBill.CurrentCell.Value.ToString() != "0")
            //            {
            //                if (ObjFunction.CheckNumeric(dgBill.CurrentCell.Value.ToString()) == true)
            //                {
            //                    dgBill.CurrentCell = dgBill[ColIndex.Rate, dgBill.CurrentCell.RowIndex];
            //                    dgBill.Rows[dgBill.CurrentCell.RowIndex].Selected = true;
            //                }
            //            }
            //        }
            //    }
            //}
            //catch (Exception exc)
            //{
            //    ObjFunction.ExceptionDisplay(exc.Message);
            //}
        }

        private void dgBill_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                TextBox txt = (TextBox)e.Control;
                txt.KeyDown += new KeyEventHandler(txtSpace_KeyDown);
                if (dgBill.CurrentCell.ColumnIndex == ColIndex.Quantity)
                {
                    TextBox txt1 = (TextBox)e.Control;
                    txt1.TextChanged += new EventHandler(txtQuantity_TextChanged);
                    //txt1.TextChanged -= new EventHandler(txtQuantity_TextChanged);
                }
                if (dgBill.CurrentCell.ColumnIndex == ColIndex.Rate)
                {
                    TextBox txtrate = (TextBox)e.Control;
                    txtrate.TextChanged += new EventHandler(txtRate_TextChanged);
                }
                if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscPercentage)
                {
                    TextBox txtDisc = (TextBox)e.Control;
                    txtDisc.TextChanged += new EventHandler(txtDisc_TextChanged);
                }
                if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscRupees)
                {
                    TextBox txtDisc = (TextBox)e.Control;
                    txtDisc.TextChanged += new EventHandler(txtDisc_TextChanged);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void txtSpace_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Space)
                {
                    Spaceflag = false;
                    if (dgBill.CurrentCell.RowIndex == 0)
                    {
                        if (dgBill.CurrentCell.ColumnIndex != 0)
                        {
                            for (int i = dgBill.CurrentCell.ColumnIndex - 1; i > 0; i--)
                            {

                                if (dgBill.Columns[i].Visible == true)
                                {
                                    dgBill.CurrentCell = dgBill[i, dgBill.CurrentCell.RowIndex];
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {

                        if (dgBill.CurrentCell.ColumnIndex == 1)
                            dgBill.CurrentCell = dgBill[4, dgBill.CurrentCell.RowIndex - 1];
                        else if (dgBill.CurrentCell.ColumnIndex != 0)
                            dgBill.CurrentCell = dgBill[dgBill.CurrentCell.ColumnIndex - 1, dgBill.CurrentCell.RowIndex];
                    }
                }

                TextBox txt = (TextBox)sender;
                txt.KeyDown -= new KeyEventHandler(txtSpace_KeyDown);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void txtRate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //ObjFunction.SetMasked((TextBox)sender, 2);
                if (dgBill.CurrentCell.ColumnIndex == ColIndex.Rate)
                {
                    if (ObjFunction.CheckValidAmount(((TextBox)sender).Text) && Convert.ToDouble(((TextBox)sender).Text) > 99999.99)
                    {
                        OMMessageBox.Show("Please enter valid value on Rate field." + Environment.NewLine + "Press Escape to continue..", CommonFunctions.ErrorTitle, OMMessageBoxButton.Escape, OMMessageBoxIcon.Error);
                        ((TextBox)sender).Text = (DBCellValue.Rate == "") ? "0" : DBCellValue.Rate;
                        //((TextBox)sender).SelectionStart = ((TextBox)sender).Text.Length;
                        //((TextBox)sender).SelectionLength = 0;
                    }
                    ObjFunction.SetMasked((TextBox)sender, 2, 5, JitFunctions.MaskedType.NotNegative);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void txtQuantity_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //ObjFunction.SetMasked((TextBox)sender, 2);
                if (dgBill.CurrentCell.ColumnIndex == ColIndex.Quantity)
                {
                    if (ObjFunction.CheckValidAmount(((TextBox)sender).Text) && Convert.ToDouble(((TextBox)sender).Text) > 999999.99)
                    {
                        OMMessageBox.Show("Please enter valid value on Quantity field." + Environment.NewLine + "Press Escape to continue..", CommonFunctions.ErrorTitle, OMMessageBoxButton.Escape, OMMessageBoxIcon.Error);
                        ((TextBox)sender).Text = (DBCellValue.Quanity == "") ? "0" : DBCellValue.Quanity;
                        //((TextBox)sender).SelectionStart = ((TextBox)sender).Text.Length;
                        //((TextBox)sender).SelectionLength = 0;
                    }
                    ObjFunction.SetMasked((TextBox)sender, 3, 6, JitFunctions.MaskedType.NotNegative);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void txtDisc_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscPercentage)
                {
                    ObjFunction.SetMasked((TextBox)sender, 2, 2, JitFunctions.MaskedType.NotNegative);
                }
                if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscRupees)
                {
                    if (ObjFunction.CheckValidAmount(((TextBox)sender).Text) && Convert.ToDouble(((TextBox)sender).Text) > 99999.99)
                    {
                        OMMessageBox.Show("Please enter valid value on Disc. field." + Environment.NewLine + "Press Escape to continue..", CommonFunctions.ErrorTitle, OMMessageBoxButton.Escape, OMMessageBoxIcon.Error);
                        ((TextBox)sender).Text = (DBCellValue.Disc == "") ? "0" : DBCellValue.Disc;
                        //((TextBox)sender).SelectionStart = ((TextBox)sender).Text.Length;
                        //((TextBox)sender).SelectionLength = 0;
                    }
                    ObjFunction.SetMasked((TextBox)sender, 2, 5, JitFunctions.MaskedType.NotNegative);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void BindGrid(int RowIndex)
        {
            try
            {
                long ItemNo, RateSettingNo, BarcodeNo;
                double StockConFactor;
                DataTable dtLedger = new DataTable();

                RateSettingNo = Convert.ToInt64(dgBill.Rows[RowIndex].Cells[ColIndex.PkRateSettingNo].Value);
                ItemNo = Convert.ToInt64(dgBill.Rows[RowIndex].Cells[ColIndex.ItemNo].Value);
                BarcodeNo = Convert.ToInt64(dgBill.Rows[RowIndex].Cells[ColIndex.PkBarCodeNo].Value);
                string strSQL = "";
                strSQL = "SELECT r.FkBcdSrNo, " +
                        " r.MKTQty, r.StockConversion, t.TaxLedgerNo, t.SalesLedgerNo, " +
                        " t.PkSrNo,t.Percentage, cast(r.MRP as numeric(18,2)) as MRP, " +
                        " t.HSNNo, t.HSNCode, t.FkTaxSettingNo " +
                        ", t.IGSTPercent, " +
                        " t.CGSTPercent, t.SGSTPercent, t.UTGSTPercent, t.CessPercent " +
                        " FROM " +
                        " MRateSetting As r " +
                        " , GetItemTaxAll(" + ItemNo + ", " +
                                " '" + dtpBillDate.Value.ToString("dd-MMM-yyyy") +
                                        " " + DBGetVal.ServerTime.ToLongTimeString() + "', " +
                                GroupType.SalesAccount + ", " +
                                ObjFunction.GetComboValue(cmbTaxType) + ", " +
                                " NULL) As t " +
                        " WHERE r.PkSrNo = " + RateSettingNo + " ";

                DataTable dtTaxDetails = ObjFunction.GetDataView(strSQL).Table;




                if (dtTaxDetails.Rows.Count > 0)
                {
                    if (BarcodeNo == 0)
                    {
                        BarcodeNo = Convert.ToInt64(dtTaxDetails.Rows[0][0].ToString());
                        dgBill.Rows[RowIndex].Cells[ColIndex.PkBarCodeNo].Value = BarcodeNo;
                        dgBill.Rows[RowIndex].Cells[ColIndex.Barcode].Value = ObjQry.ReturnString("Select BarCode From MStockBarCode Where PkStockBarcodeNo=" + BarcodeNo + "", CommonFunctions.ConStr);
                    }

                    dgBill.Rows[RowIndex].Cells[ColIndex.MKTQuantity].Value = Convert.ToInt64(dtTaxDetails.Rows[0][1].ToString());

                    StockConFactor = Convert.ToDouble(dtTaxDetails.Rows[0][2].ToString());
                    dgBill.Rows[RowIndex].Cells[ColIndex.StockFactor].Value = StockConFactor;

                    dgBill.Rows[RowIndex].Cells[ColIndex.TaxLedgerNo].Value = Convert.ToInt64(dtTaxDetails.Rows[0][3].ToString());
                    dgBill.Rows[RowIndex].Cells[ColIndex.SalesLedgerNo].Value = Convert.ToInt64(dtTaxDetails.Rows[0][4].ToString());
                    dgBill.Rows[RowIndex].Cells[ColIndex.PkItemTaxInfo].Value = Convert.ToInt64(dtTaxDetails.Rows[0][5].ToString());
                    if (dgBill.Rows[RowIndex].Cells[ColIndex.PkStockTrnNo].Value == null) dgBill.Rows[RowIndex].Cells[ColIndex.PkStockTrnNo].Value = 0;
                    dgBill.Rows[RowIndex].Cells[ColIndex.PkVoucherNo].Value = 0;
                    dgBill.Rows[RowIndex].Cells[ColIndex.TaxPercentage].Value = Convert.ToDouble(dtTaxDetails.Rows[0][6].ToString());
                    dgBill.Rows[RowIndex].Cells[ColIndex.MRP].Value = Convert.ToDouble(dtTaxDetails.Rows[0][7].ToString()).ToString("0.00");
                    dgBill.Rows[RowIndex].Cells[ColIndex.HSNCode].Value = dtTaxDetails.Rows[0]["HSNCode"].ToString();

                    if (ID != 0)
                    {
                        if (dgBill.Rows[RowIndex].Cells[ColIndex.SalesVchNo].Value == null)
                            dgBill.Rows[RowIndex].Cells[ColIndex.SalesVchNo].Value = ObjQry.ReturnLong("SELECT TVoucherDetails.PkVoucherTrnNo FROM MItemTaxInfo INNER JOIN TVoucherDetails ON MItemTaxInfo.SalesLedgerNo = TVoucherDetails.LedgerNo " +
                                " WHERE (MItemTaxInfo.ItemNo = " + dgBill.Rows[RowIndex].Cells[ColIndex.ItemNo].Value + ") AND (TVoucherDetails.FkVoucherNo = " + ID + ")", CommonFunctions.ConStr);
                        if (dgBill.Rows[RowIndex].Cells[ColIndex.TaxVchNo].Value == null)
                            dgBill.Rows[RowIndex].Cells[ColIndex.TaxVchNo].Value = ObjQry.ReturnLong("SELECT TVoucherDetails.PkVoucherTrnNo FROM MItemTaxInfo INNER JOIN TVoucherDetails ON MItemTaxInfo.TaxLedgerNo = TVoucherDetails.LedgerNo " +
                                " WHERE (MItemTaxInfo.ItemNo = " + dgBill.Rows[RowIndex].Cells[ColIndex.ItemNo].Value + ") AND (TVoucherDetails.FkVoucherNo = " + ID + ")", CommonFunctions.ConStr);
                    }

                    #region Fill TAX Details (Part - 2) Fill GST Bifurcation details

                    #region Old Code
                    //if (VoucherType == VchType.Sales && ObjFunction.GetComboValue(cmbTaxType) == GroupType.GST)
                    //{
                    //    string strHSNCode = dgBill.Rows[RowIndex].Cells[ColIndex.HSNCode].Value.ToString();
                    //    bool isGstTaxDetailsFound = false;
                    //    DataTable dtGSTDetails = null;
                    //    string sql = "";

                    //    if (Convert.ToInt64(dtTaxDetails.Rows[0]["HSNNo"].ToString()) != 0)
                    //    {
                    //        sql = "SELECT * FROM MStockGSTTaxDetails " +
                    //                " WHERE HSNNo = '" + dtTaxDetails.Rows[0]["HSNNo"].ToString() + "' " +
                    //                    " AND FkTaxSettingNo = " + Convert.ToInt64(dtTaxDetails.Rows[0]["FkTaxSettingNo"].ToString()) + " " +
                    //                    " AND IsActive = 'True'";
                    //    }
                    //    else
                    //    {
                    //        sql = "SELECT TOP 1 * FROM MStockGSTTaxDetails " +
                    //                " WHERE FkTaxSettingNo = " + Convert.ToInt64(dtTaxDetails.Rows[0]["FkTaxSettingNo"].ToString()) + " " +
                    //                    " AND IsActive = 'True'";
                    //    }

                    //    dtGSTDetails = ObjFunction.GetDataView(sql).Table;

                    //    if (dtGSTDetails.Rows.Count > 0)
                    //    {
                    //        isGstTaxDetailsFound = true;
                    //    }

                    //    while (!isGstTaxDetailsFound)
                    //    {
                    //        OMMessageBox.Show("Taxation details not specified for the selected SKU." + Environment.NewLine +
                    //                  "Click OK to specify taxation details and continue.",
                    //                  "Information",
                    //                  OMMessageBoxButton.Escape,
                    //                  OMMessageBoxIcon.Information);

                    //        Master.StockItemSAE sItem = new Kirana.Master.StockItemSAE(ItemNo);

                    //        ObjFunction.OpenDialog(sItem, this);
                    //        if (Convert.ToInt64(dtTaxDetails.Rows[0]["HSNNo"].ToString()) != 0)
                    //        {
                    //            sql = "SELECT * FROM MStockGSTTaxDetails " +
                    //                    " WHERE HSNNo = '" + dtTaxDetails.Rows[0]["HSNNo"].ToString() + "' " +
                    //                        " AND FkTaxSettingNo = " + Convert.ToInt64(dtTaxDetails.Rows[0]["FkTaxSettingNo"].ToString()) + " " +
                    //                        " AND IsActive = 'True'";
                    //        }
                    //        else
                    //        {
                    //            sql = "SELECT TOP 1 * FROM MStockGSTTaxDetails " +
                    //                    " WHERE FkTaxSettingNo = " + Convert.ToInt64(dtTaxDetails.Rows[0]["FkTaxSettingNo"].ToString()) + " " +
                    //                        " AND IsActive = 'True'";
                    //        }
                    //        dtGSTDetails = ObjFunction.GetDataView(sql).Table;

                    //        if (dtGSTDetails.Rows.Count > 0)
                    //        {
                    //            isGstTaxDetailsFound = true;
                    //        }
                    //    }

                    //    dgBill.Rows[RowIndex].Cells[ColIndex.IGSTPercent].Value = Convert.ToDouble(dtGSTDetails.Rows[0]["IGSTPercent"].ToString());
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.IGSTAmount].Value = 0;
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.CGSTPercent].Value = Convert.ToDouble(dtGSTDetails.Rows[0]["CGSTPercent"].ToString());
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.CGSTAmount].Value = 0;
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.SGSTPercent].Value = Convert.ToDouble(dtGSTDetails.Rows[0]["SGSTPercent"].ToString());
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.SGSTAmount].Value = 0;
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.UTGSTPercent].Value = Convert.ToDouble(dtGSTDetails.Rows[0]["UTGSTPercent"].ToString());
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.UTGSTAmount].Value = 0;
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.CessPercent].Value = Convert.ToDouble(dtGSTDetails.Rows[0]["CessPercent"].ToString());
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.CessAmount].Value = 0;
                    //}
                    //else
                    //{
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.IGSTPercent].Value = 0;
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.IGSTAmount].Value = 0;
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.CGSTPercent].Value = 0;
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.CGSTAmount].Value = 0;
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.SGSTPercent].Value = 0;
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.SGSTAmount].Value = 0;
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.UTGSTPercent].Value = 0;
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.UTGSTAmount].Value = 0;
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.CessPercent].Value = 0;
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.CessAmount].Value = 0;
                    //}
                    #endregion

                    #region new code
                    dgBill.Rows[RowIndex].Cells[ColIndex.IGSTPercent].Value = Convert.ToDouble(dtTaxDetails.Rows[0]["IGSTPercent"].ToString());
                    dgBill.Rows[RowIndex].Cells[ColIndex.IGSTAmount].Value = 0;
                    dgBill.Rows[RowIndex].Cells[ColIndex.CGSTPercent].Value = Convert.ToDouble(dtTaxDetails.Rows[0]["CGSTPercent"].ToString());
                    dgBill.Rows[RowIndex].Cells[ColIndex.CGSTAmount].Value = 0;
                    dgBill.Rows[RowIndex].Cells[ColIndex.SGSTPercent].Value = Convert.ToDouble(dtTaxDetails.Rows[0]["SGSTPercent"].ToString());
                    dgBill.Rows[RowIndex].Cells[ColIndex.SGSTAmount].Value = 0;
                    dgBill.Rows[RowIndex].Cells[ColIndex.UTGSTPercent].Value = Convert.ToDouble(dtTaxDetails.Rows[0]["UTGSTPercent"].ToString());
                    dgBill.Rows[RowIndex].Cells[ColIndex.UTGSTAmount].Value = 0;
                    dgBill.Rows[RowIndex].Cells[ColIndex.CessPercent].Value = Convert.ToDouble(dtTaxDetails.Rows[0]["CessPercent"].ToString());
                    dgBill.Rows[RowIndex].Cells[ColIndex.CessAmount].Value = 0;
                    #endregion

                    #endregion



                    if (dgBill.Rows.Count == dgBill.CurrentRow.Index + 1 && dgBill.CurrentCell.ColumnIndex == ColIndex.Rate)
                    {
                        dgBill.Rows.Add();
                    }
                    CheckSchemeValidation();
                    CalculateTotal();
                }
                else
                {
                    for (int i = 1; i < dgBill.Columns.Count; i++)
                    {
                        dgBill.Rows[RowIndex].Cells[i].Value = null;
                    }
                    DisplayMessage("Items Tax Details Not Found.....");
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #endregion

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                //Sales.RequestSalesNo = 0;
                //Form NewF = new Sales();
                //this.Close();
                //ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                pnlSearch.Visible = true;
                txtSearch.Text = ""; txtSearch.Enabled = true;
                txtSearch.Focus();
                btnNew.Enabled = false;
                btnUpdate.Enabled = false;
                btnBillCancel.Enabled = false;
                rbInvNo.Checked = true;
                rbType_CheckedChanged(rbInvNo, null);
                dgPartySearch.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #region Navigation Methods
        private void NavigationDisplay(int type)
        {
            try
            {
                long No = 0;
                if (type == 5)
                {
                    if (dtSearch.Rows.Count > 0)
                    {
                        No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                        ID = No;
                    }
                }
                else if (type == 1)
                {
                    No = Convert.ToInt64(dtSearch.Rows[0].ItemArray[0].ToString());
                    cntRow = 0;
                    ID = No;
                }
                else if (type == 2)
                {
                    No = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    cntRow = dtSearch.Rows.Count - 1;
                    ID = No;
                }
                else
                {
                    if (type == 3)
                    {
                        cntRow = cntRow + 1;
                    }
                    else if (type == 4)
                    {
                        cntRow = cntRow - 1;
                    }

                    if (cntRow < 0)
                    {
                        OMMessageBox.Show("This is First Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow + 1;
                    }
                    else if (cntRow > dtSearch.Rows.Count - 1)
                    {
                        OMMessageBox.Show("This is Last Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow - 1;
                    }
                    else
                    {
                        No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                        ID = No;
                    }

                }

                if (ID > 0)
                    FillControls();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void SetNavigation()
        {
            cntRow = 0;

            //new code
            DataRow[] dr = dtSearch.Select("PkVoucherNo=" + ID);
            if (dr.Length > 0)
            {
                cntRow = dtSearch.Rows.IndexOf(dr[0]);
            }
            else
            {
                cntRow = dtSearch.Rows.Count - 1;
            }

            //old code
            //for (int i = 0; i < dtSearch.Rows.Count; i++)
            //{
            //    if (Convert.ToInt64(dtSearch.Rows[i].ItemArray[0].ToString()) == ID)
            //    {
            //        cntRow = i;
            //        break;
            //    }
            //}
        }

        private void setDisplay(bool flag)
        {
            btnFirst.Visible = flag;
            btnPrev.Visible = flag;
            btnNext.Visible = flag;
            btnLast.Visible = flag;
            //btnDelete.Visible = flag;
            //GridRange.Height = 25;
            if (dtSearch.Rows.Count == 0)
            {
                btnFirst.Visible = false;
                btnPrev.Visible = false;
                btnNext.Visible = false;
                btnLast.Visible = false;
            }
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            NavigationDisplay(1);
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            NavigationDisplay(4);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            NavigationDisplay(3);
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            NavigationDisplay(2);
        }

        #endregion

        #region KeyDown Events
        private void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is GroupBox)
                    KeyDownFormat(ctrl.Controls);
            }
        }

        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Left && e.Control)
            {
                if (btnPrev.Enabled) btnPrev_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Up && e.Control)
            {
                if (btnFirst.Enabled) btnFirst_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Right && e.Control)
            {
                if (btnNext.Enabled) btnNext_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Down && e.Control)
            {
                if (btnLast.Enabled) btnLast_Click(sender, e);
            }
            else if (e.KeyCode == Keys.F12)
            {
                if (btnSave.Visible)
                {
                    PrintAsk = 0;
                    cmbPaymentType.SelectedValue = "2";
                    btnSave_Click(sender, e);
                }
            }
            else if (e.KeyCode == Keys.F11)
            {
                if (btnCreditSave.Visible == true && btnCreditSave.Enabled == true)
                    btnCreditSave_Click(sender, new EventArgs());
            }
            else if (e.KeyCode == Keys.H && e.Control)
            {
                e.SuppressKeyPress = true;
                if (pnlFooterInfo.Visible == false)
                {
                    pnlFooterInfo.Dock = DockStyle.Bottom;
                    //pnlFooterInfo.Height = 30;
                    pnlFooterInfo.BorderStyle = BorderStyle.None;
                    pnlFooterInfo.BringToFront();
                    pnlFooterInfo.Visible = true;
                }
                else
                {
                    pnlFooterInfo.Visible = false;
                }
            }
            else if (e.KeyCode == Keys.F1)
            {
                if (dgBill.Rows.Count != 0)
                {

                    ItemSerach_Method();
                }
            }
            else if (e.KeyCode == Keys.F7)
            {
                if (dgBill.Focused == true)
                {
                    if (dgBill.Rows.Count > 0)
                    {
                        if (dgBill.CurrentCell.ColumnIndex == 2)
                        {
                            dgBill.CurrentCell.ReadOnly = false;
                            //AddRows = false;
                            //FlagRate = false;
                            //defaultQty = true;
                        }
                    }
                }
            }
            if (e.KeyCode == Keys.P && e.Control)
            {
                if (ID != 0)
                {
                    PrintBill();
                }
            }
            else if (e.KeyCode == Keys.F5)
            {
                if (ParkBillNo != 0)
                {
                    if (ObjTrans.Execute("Update TParkingBill set IsCancel='true' where ParkingBillNo=" + ParkBillNo + "", CommonFunctions.ConStr) == true)
                        ParkBillNo = 0;
                }
                if (dgBill.Rows.Count <= 1)
                {
                    OMMessageBox.Show("Atleast one item required.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    return;
                }
                else if (ID == 0 && ParkBillNo == 0)
                {
                    pnlMainParking.Visible = true;
                    if (ObjFunction.GetComboValue(cmbPartyName) != Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_PartyAC)))
                        txtPersonName.Text = cmbPartyName.Text;
                    else
                        txtPersonName.Text = "";
                    txtPersonName.Focus();
                }
            }
            else if (e.KeyCode == Keys.F6)
            {
                if (ID == 0)
                {
                    if (dgBill.Rows.Count == 1)
                    {
                        // dgParkingBills.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                        ShowParkingBill();
                    }
                }
            }
            //else if (e.KeyCode == Keys.F4)
            //    ValidationsMain();
            else if (e.KeyCode == Keys.O && e.Control)
            {
                if (DBGetVal.IsAdmin == true)
                {
                    if (btnNew.Visible == true)
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_RateTypeAskPassword)) == true)
                        {
                            txtRateTypePassword.Enabled = true;
                            pnlRateType.Visible = true;
                            txtRateTypePassword.Text = "";
                            txtRateTypePassword.Focus();
                        }
                    }
                }
            }
            else if (e.KeyCode == Keys.D && e.Control)
            {
                if (ID != 0 && btnSave.Visible)
                {
                    long ControlUnder = ObjQry.ReturnLong("Select ControlUnder From MPayType Where PKPayTypeNo=" + ObjFunction.GetComboValue(cmbPaymentType) + "", CommonFunctions.ConStr);
                    //if (ObjFunction.GetComboValue(cmbPaymentType) == 4)
                    if (ControlUnder == 4)
                    {
                        pnlPartial.Visible = !pnlPartial.Visible;
                        pnlPartial.Size = new Size(475, 214);
                        pnlPartial.Location = new Point(75, 123);
                        dgPayChqDetails.Location = dgPayType.Location;
                        dgPayChqDetails.Visible = true;
                        dgPayChqDetails.BringToFront();
                        dgPayChqDetails.Focus();
                        //dgPayChqDetails.Enabled = false;
                        dgPayCreditCardDetails.Visible = false;
                    }
                    //else if (ObjFunction.GetComboValue(cmbPaymentType) == 5)
                    else if (ControlUnder == 5)
                    {
                        pnlPartial.Visible = !pnlPartial.Visible;
                        pnlPartial.Size = new Size(475, 214);
                        pnlPartial.Location = new Point(75, 123);
                        dgPayCreditCardDetails.Location = dgPayType.Location;
                        dgPayCreditCardDetails.Visible = true;
                        ((DataGridViewTextBoxColumn)dgPayCreditCardDetails.Columns[0]).MaxInputLength = Convert.ToInt32(ObjFunction.GetAppSettings(AppSettings.S_CreditCardDigitLimit));
                        dgPayCreditCardDetails.Focus();
                        dgPayCreditCardDetails.BringToFront();
                        //dgPayCreditCardDetails.Enabled = false;
                        dgPayChqDetails.Visible = false;

                    }
                }

            }
            else if (e.KeyCode == Keys.K && e.Control)
            {
                if (ID != 0 && ObjFunction.GetComboValue(cmbPaymentType) == 3 && btnNew.Visible == true && ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo in ( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + ")", CommonFunctions.ConStr) > 1)
                {
                    if (OMMessageBox.Show("Are you sure you want to delete this bill collection ?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question, OMMessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        ObjTrans.Execute("Exec DeleteCollectionDetails " + ID + "", CommonFunctions.ConStr);
                        FillControls();
                    }
                }

            }
            else if (e.KeyCode == Keys.W && e.Control)
            {
                if (btnMixMode.Text == "Mix\r\nMode\r\n(F3)" && btnMixMode.Visible == true && btnMixMode.Enabled == true)
                {
                    btnMixMode_Click(sender, e);
                }
                //if (btnCreditSave.Visible == true && btnCreditSave.Enabled == true)
                //if (Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_PartyAC)) != ObjFunction.GetComboValue(cmbPartyName))
                //{
                //    MixModeVal = 1;
                //    MixModeFlag = true;
                //    btnCreditSave_Click(sender, new EventArgs());
                //}
                //else
                //{
                //    MixModeVal = 0;
                //    MixModeFlag = false;
                //    btnCashSave_Click(sender, new EventArgs());
                //}
            }
            else if (e.KeyCode == Keys.Q && e.Control)
            {
                if (btnSave.Visible == true)
                {
                    if (ObjFunction.CheckAllowMenu(10) == false) return;
                    Form NewF = new Master.StockItemSAE(-1);
                    ObjFunction.OpenForm(NewF);

                    if (((Master.StockItemSAE)NewF).ShortID != 0)
                    {
                        string barcode = ObjQry.ReturnString("Select BarCode From MStockBarCode where ItemNo=" + ((Master.StockItemSAE)NewF).ShortID + "", CommonFunctions.ConStr);
                        int rwindex = dgBill.CurrentCell.RowIndex;
                        dgBill.CurrentRow.Cells[ColIndex.ItemName].Value = barcode;
                        dgBill_CellEndEdit(dgBill, new DataGridViewCellEventArgs(ColIndex.ItemName, rwindex));

                    }
                }
            }
            else if (e.KeyCode == Keys.Y && e.Control)
            {
                if (btnSave.Visible == true)
                {
                    chkItemLevelDisc.Checked = false;
                    chkFooterLevelDisc.Checked = false;
                }
            }
            else if (e.Alt && e.KeyCode == Keys.F2)
            {
                if (btnNew.Visible == false)
                {
                    if (btnAdvanceSearch.Enabled) btnAdvanceSearch_Click(sender, e);
                }
            }
        }
        #endregion

        #region Delete code
        private void InitDelTable()
        {
            dtDelete.Columns.Add();
            dtDelete.Columns.Add();
        }

        private void DeleteDtls(int Type, long PkNo)
        {
            DataRow dr = null;
            dr = dtDelete.NewRow();
            dr[0] = Type;
            dr[1] = PkNo;
            dtDelete.Rows.Add(dr);
        }

        #region Update CurrentStock code
        public void intialCurrentStock()
        {
            dtCurrentStock = ObjFunction.GetDataView("Select ItemNo,MRP,GodownNo,CurrentStock,0 as Type From MStockItemBalance Where Itemno=0").Table;

        }
        private void DeleteCurrentStock(long ItemNo, double MRP, long GodownNo, double CurrentStock, long type)
        {
            DataRow dr = null;
            dr = dtCurrentStock.NewRow();
            dr[0] = ItemNo;
            dr[1] = MRP;
            dr[2] = GodownNo;
            dr[3] = CurrentStock;
            dr[4] = type;
            dtCurrentStock.Rows.Add(dr);
        }
        private void UpdateCurrentStock()
        {
            if (dtCurrentStock != null)
            {
                for (int i = 0; i < dtCurrentStock.Rows.Count; i++)
                {
                    dbTVoucherEntry.UpdateReverStock(Convert.ToInt64(dtCurrentStock.Rows[i].ItemArray[0].ToString()),
                        Convert.ToDouble(dtCurrentStock.Rows[i].ItemArray[1].ToString()), Convert.ToInt64(dtCurrentStock.Rows[i].ItemArray[2].ToString()),
                        Convert.ToDouble(dtCurrentStock.Rows[i].ItemArray[3].ToString()), Convert.ToInt64(dtCurrentStock.Rows[i].ItemArray[4].ToString()));
                }
            }
            dtCurrentStock.Rows.Clear();
        }
        #endregion

        private void DeleteValues()
        {
            if (dtDelete != null)
            {
                for (int i = 0; i < dtDelete.Rows.Count; i++)
                {
                    if (Convert.ToInt32(dtDelete.Rows[i].ItemArray[0]) == 1)
                    {
                        tStock.PkStockTrnNo = Convert.ToInt64(dtDelete.Rows[i].ItemArray[1]);
                        dbTVoucherEntry.DeleteTStock(tStock);
                    }
                    else if (Convert.ToInt32(dtDelete.Rows[i].ItemArray[0]) == 2)
                    {
                        tVoucherDetails.PkVoucherTrnNo = Convert.ToInt64(dtDelete.Rows[i].ItemArray[1]);
                        dbTVoucherEntry.DeleteTVoucherDetails(tVoucherDetails);
                    }
                    else if (Convert.ToInt32(dtDelete.Rows[i].ItemArray[0]) == 3)
                    {
                        tVoucherDetails.CompanyNo = Convert.ToInt64(dtDelete.Rows[i].ItemArray[1]);
                        tVoucherDetails.FkVoucherNo = ID;
                        dbTVoucherEntry.DeleteTVoucherDetailsCompany(tVoucherDetails);
                    }
                    else if (Convert.ToInt32(dtDelete.Rows[i].ItemArray[0]) == 4)
                    {
                        tStockGodown.PKStockGodownNo = Convert.ToInt64(dtDelete.Rows[i].ItemArray[1]);
                        dbTVoucherEntry.DeleteTStockGodown(tStockGodown);
                    }
                    else if (Convert.ToInt32(dtDelete.Rows[i].ItemArray[0]) == 5)
                    {
                        tReward.RewardNo = Convert.ToInt64(dtDelete.Rows[i].ItemArray[1]);
                        dbTVoucherEntry.DeleteTReward(tReward);
                    }
                    else if (Convert.ToInt32(dtDelete.Rows[i].ItemArray[0]) == 6)
                    {
                        tRewardDetails.PkSrNo = Convert.ToInt64(dtDelete.Rows[i].ItemArray[1]);
                        dbTVoucherEntry.DeleteTRewardDetails(tRewardDetails);
                    }
                    else if (Convert.ToInt32(dtDelete.Rows[i].ItemArray[0]) == 7)
                    {
                        tRewardFrom.PkSrNo = Convert.ToInt64(dtDelete.Rows[i].ItemArray[1]);
                        dbTVoucherEntry.DeleteTRewardFrom(tRewardFrom);
                    }
                    else if (Convert.ToInt32(dtDelete.Rows[i].ItemArray[0]) == 8)
                    {
                        tRewardTo.PkSrNo = Convert.ToInt64(dtDelete.Rows[i].ItemArray[1]);
                        dbTVoucherEntry.DeleteTRewardTo(tRewardTo);
                    }
                    else if (Convert.ToInt32(dtDelete.Rows[i].ItemArray[0]) == 9)
                    {
                        tItemLevelDisc.PkSrNo = Convert.ToInt64(dtDelete.Rows[i].ItemArray[1]);
                        dbTVoucherEntry.DeleteTItemLevelDiscountDetails(tItemLevelDisc);
                    }
                    else if (Convert.ToInt32(dtDelete.Rows[i].ItemArray[0]) == 10)
                    {
                        tFooterDisc.PkSrNo = Convert.ToInt64(dtDelete.Rows[i].ItemArray[1]);
                        dbTVoucherEntry.DeleteTFooterDiscountDetails(tFooterDisc);
                    }
                    //else if (Convert.ToInt32(dtDelete.Rows[i].ItemArray[0]) == 2)
                    //{
                    //    dbTVoucherEntry.UpdateTStockBarCode(Convert.ToInt64(dtDelete.Rows[i].ItemArray[1]));
                    //}
                    //else if (Convert.ToInt32(dtDelete.Rows[i].ItemArray[0]) == 3)
                    //{
                    //    tRequire.PkRequireNo = Convert.ToInt64(dtDelete.Rows[i].ItemArray[1]);
                    //    dbTVoucherEntry.DeleteTRequiredQuantity(tRequire);
                    //}
                    //else if (Convert.ToInt32(dtDelete.Rows[i].ItemArray[0]) == 4)
                    //{
                    //    ObjTrans.Execute("Update TStockBarCode set IsSale='False' Where PkSrNo=(Select FKStockBarCode From TParkingBills Where PkSrNo=" + Convert.ToInt64(dtDelete.Rows[i].ItemArray[1]) + ")", CommonFunctions.ConStr);

                    //    TParkingBills tParking = new TParkingBills();
                    //    tParking.PkSrNo = Convert.ToInt64(dtDelete.Rows[i].ItemArray[1]);
                    //    tParking.BillNo = 0;
                    //    dbTVoucherEntry.DeleteTParkingBills(tParking);
                    //}
                    //else if (Convert.ToInt32(dtDelete.Rows[i].ItemArray[0]) == 5)
                    //{
                    //    tExchange.PkSrNo = Convert.ToInt64(dtDelete.Rows[i].ItemArray[1]);
                    //    dbTVoucherEntry.DeleteTExchangeDetails(tExchange);
                    //}
                }
                dtDelete.Rows.Clear();
            }
        }


        #endregion

        #region Parial Payment Methods

        private void Partial_Leave(object sender, EventArgs e)
        {
            if (((TextBox)sender).Text == "")
            {
                OMMessageBox.Show("Please Enter amount", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                ((TextBox)sender).Focus();
            }
            else if (ObjFunction.CheckValidAmount(((TextBox)sender).Text) == false)
            {
                OMMessageBox.Show("Please Enter valid amount", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                ((TextBox)sender).Focus();
            }
            else
            {
                //double cash, credit;
                //cash = (txtCash.Text == "") ? 0 : Convert.ToDouble(txtCash.Text); txtCash.Text = cash.ToString("0.00");
                //credit = (txtCredit.Text == "") ? 0 : Convert.ToDouble(txtCredit.Text); txtCredit.Text = credit.ToString("0.00");
                //txtBalance.Text = (Convert.ToDouble(txtGrandTotal.Text) - (cash + credit)).ToString("0.00");
            }
        }
        private void Partial_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Partial_Leave(sender, e);
            }
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            bool flag = true;
            try
            {
                if (Convert.ToDouble(dgPayType.Rows[3].Cells[2].Value) > 0)
                {
                    if (dgPayChqDetails.Rows[0].Cells[0].Value == null || dgPayChqDetails.Rows[0].Cells[1].FormattedValue.ToString() == "" || dgPayChqDetails.Rows[0].Cells[2].FormattedValue.ToString() == "" || dgPayChqDetails.Rows[0].Cells[3].FormattedValue.ToString() == "")
                    {
                        OMMessageBox.Show("Please Fill Cheque Details.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        flag = false;
                    }
                    else flag = true;
                }
                if (flag == true)
                {
                    if (Convert.ToDouble(dgPayType.Rows[4].Cells[2].Value) > 0)
                    {
                        if (dgPayCreditCardDetails.Rows[0].Cells[0].FormattedValue.ToString() == "" || dgPayCreditCardDetails.Rows[0].Cells[1].FormattedValue.ToString() == "" || dgPayCreditCardDetails.Rows[0].Cells[2].FormattedValue.ToString() == "")
                        {
                            OMMessageBox.Show("Please Fill Credit Card Details.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            flag = false;
                        }
                        else flag = true;
                    }
                }
                if (flag == true)
                {
                    if (Convert.ToDouble(txtTotalAmt.Text) != Convert.ToDouble(txtGrandTotal.Text))
                    {
                        OMMessageBox.Show("TOTAL AMOUNT EXCEEDS TO BILL AMOUNT.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                    else
                    {
                        btnSave.Enabled = true;
                        btnSave.Focus();
                        pnlPartial.Visible = false;
                    }
                }
                if (flag == true)
                {

                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }
        #endregion

        private void DisplayMessage(string str)
        {
            lblMsg.Visible = true;
            lblMsg.Text = str;
            Application.DoEvents();
            System.Threading.Thread.Sleep(700);
            lblMsg.Visible = false;
        }

        private void lblStatus_Click(object sender, EventArgs e)
        {
            try
            {
                if (BillingMode == 0)
                {
                    BillingMode = 1;
                    //VoucherType = VchType.TempSales;
                    lblStatus.ForeColor = Color.Green;
                }
                else
                {
                    BillingMode = 0;
                    //VoucherType = VchType.Sales;
                    lblStatus.ForeColor = Color.Red;
                }

                ObjFunction.LockButtons(true, this.Controls);
                ObjFunction.LockControls(false, this.Controls);
                rbEnglish.Enabled = true;
                rbMarathi.Enabled = true;

                InitDelTable();
                intialCurrentStock();
                txtInvNo.Enabled = false;
                InitControls();
                dtSearch = ObjFunction.GetDataView("Select PkVoucherNo from TVoucherEntry Where VoucherTypeCode=" + VoucherType + " AND IsVoucherLock='false' Order by VoucherDate, VoucherUserNo").Table;

                if (dtSearch.Rows.Count > 0)
                {
                    ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    FillControls();
                    SetNavigation();
                }

                setDisplay(true);
                btnNew.Focus();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                ID = 0;
                partialPay = new PartialPayment();
                FooterDiscDtlsNo = 0; MTDSchDetailsNo = 0;
                ObjFunction.InitialiseControl(this.Controls);
                ObjFunction.LockButtons(false, this.Controls);
                ObjFunction.LockControls(true, this.Controls);
                btnInsScheme.Enabled = true; btnInsSchemeInfo.Enabled = true;
                btnInsScheme.Visible = true; btnInsSchemeInfo.Visible = true;
                ObjFunction.FillList(lstBank, "Select BankNo,BankName From MOtherBank where IsActive='true' order by BankName");
                ObjFunction.FillList(lstBranch, "Select BranchNo,BranchName From MBranch where IsActive='true' order by BranchName");
                btnSearch.Visible = false;
                btnPrint.Visible = false;
                btnCreditSave.Visible = true;
                //btnCCSave.Visible = true;
                btnCashSave.Visible = true;
                dgBill.Enabled = true;
                ObjFunction.FillComb(cmbPartyName, "Select LedgerNo,LedgerName From MLedger Where GroupNo in (" + GroupType.SundryDebtors + ") and (IsActive='true') order by LedgerName");
                InitControls();
                if (cmbPartyName.Focused == true) cmbPartyName.DroppedDown = true;
                btnBillCancel.Visible = false;
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_RateTypeAskPassword)) == true)
                    cmbRateType.Enabled = false;

                LastBillNo = ObjQry.ReturnLong("Select IsNull(Max(PkVoucherNo),0) From TVoucherEntry Where VoucherTypeCode=" + VoucherType + " ", CommonFunctions.ConStr);
                dt = ObjFunction.GetDataView("SELECT IsNull(SUM(TStock.Quantity),0) AS Quantity, IsNull(SUM(TStock.Amount),0) AS Amount FROM TVoucherDetails INNER JOIN TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN " +
                    " TStock ON TVoucherDetails.PkVoucherTrnNo = TStock.FkVoucherTrnNo WHERE (TVoucherDetails.VoucherSrNo = 1) AND (TVoucherEntry.VoucherTypeCode = " + VoucherType + ") AND (TVoucherEntry.PkVoucherNo = " + LastBillNo + ")").Table;

                if (dt.Rows.Count > 0)
                {
                    lblLastAmt.Text = "Amount : ";
                    lblLastBillAmt.Text = Convert.ToDouble(dt.Rows[0].ItemArray[1].ToString()).ToString();
                    //btnLastBillAmt.Text = "Last Bill " + Convert.ToDouble(dt.Rows[0].ItemArray[1].ToString()).ToString("0.00");
                    lblLastBillQty.Text = "Qty: " + dt.Rows[0].ItemArray[0].ToString();
                    // btnLastBillQty.Text = "Qty: " + dt.Rows[0].ItemArray[0].ToString();
                    lblLastPayment.Text = "Payment: " + ObjQry.ReturnString("SELECT MPayType.PayTypeName FROM MPayType INNER JOIN TVoucherEntry ON MPayType.PKPayTypeNo = TVoucherEntry.PayTypeNo WHERE (TVoucherEntry.PkVoucherNo = " + LastBillNo + ")", CommonFunctions.ConStr);
                    // btnPayment.Text = lblLastPayment.Text; 
                }
                cmbTaxType.SelectedValue = ObjFunction.GetAppSettings(AppSettings.S_TaxType);
                dgPayType.Enabled = true;
                PartyNo = 0; PayType = 0;
                ParkBillNo = 0;
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsManualBillNo)) == true)
                {
                    txtInvNo.Enabled = true;
                    txtInvNo.ReadOnly = false;
                }
                btnMixMode.Visible = false;
                btnOrderType.Text = dtOrderType.Select("OrderTypeNo=" + OrderType)[0].ItemArray[1].ToString();
                btnOrderType.BackColor = Color.FromName(dtOrderType.Select("OrderTypeNo=" + OrderType)[0].ItemArray[2].ToString());
                //dtpBillDate.MinDate = Convert.ToDateTime(ObjFunction.GetAppSettings(AppSettings.O_SOD));
                DisplayCollectionDetails();
                txtSchemeDisc.Enabled = false;
                txtSchemeDisc.Text = "0.00";
                //cmbPartyName_KeyDown(cmbPartyName, new KeyEventArgs(Keys.Enter));
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_StopOnParty)) == false) FillCreditLimit();
                else btnMixMode.Visible = true;
                InitItemLeveDiscount();
                tempDate = dtpBillDate.Value.Date;
                tempPartyNo = ObjFunction.GetComboValue(cmbPartyName);
                txtHRs.Text = ObjFunction.GetAppSettings(AppSettings.S_HamaliRs);
                strRateType = ObjFunction.GetComboValueString(cmbRateType);


            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsBillUpdate)) == true)
                {
                    Form frm = new Utilities.PasswordAsk(2);
                    ObjFunction.OpenForm(frm);
                    if (Utilities.PasswordAsk.IsAllow == 0) return;
                }

                if (ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo in ( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + ")", CommonFunctions.ConStr) > 1)
                {
                    btnUpdate.Visible = false;
                    btnBillCancel.Visible = false;
                    btnMixMode.Visible = false;
                    //BillFlag = false;
                    OMMessageBox.Show("Already this bill is amount collected", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    return;
                }
                else
                {
                    if (ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo in ( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + " and TR.TypeOfRef in(6))", CommonFunctions.ConStr) > 1)
                    {
                        btnUpdate.Visible = false;
                        btnBillCancel.Visible = false;
                        btnMixMode.Visible = false;
                        //BillFlag = false;
                        OMMessageBox.Show("Already this bill is amount collected", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        return;
                    }

                }

                btnCashSave.Visible = true;
                btnCreditSave.Visible = true;
                btnInsSchemeInfo.Text = "Scheme Info";
                ObjFunction.LockButtons(false, this.Controls);
                ObjFunction.LockControls(true, this.Controls);
                txtInvNo.Enabled = false;
                btnInsScheme.Enabled = true;
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_RateTypeAskPassword)) == true)
                    cmbRateType.Enabled = false;
                btnSearch.Visible = false;
                btnPrint.Visible = false;
                dgBill.Enabled = true;
                dgBill.Focus();
                btnBillCancel.Visible = false;
                btnMixMode.Visible = false;
                dgBill.CurrentCell = dgBill[1, dgBill.Rows.Count - 1];
                if (ID != 0)
                {
                    //cmbPaymentType.Enabled = false;
                    //dgPayType.Enabled = false;
                }
                if (ObjFunction.GetComboValue(cmbPartyName) == Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_PartyAC)))
                {
                    //cmbPaymentType.Enabled = false;
                }
                else cmbPaymentType.Enabled = true;
                txtSchemeDisc.Enabled = false;

            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                ObjFunction.LockButtons(true, this.Controls);
                btnBillCancel.Visible = true;
                partialPay = new PartialPayment();
                btnSearch.Visible = true;
                btnPrint.Visible = true;
                ParkBillNo = 0;
                MixModeFlag = false;
                txtHRs.Text = "0.00";
                MixModeVal = 0;
                FooterDiscDtlsNo = 0;
                NavigationDisplay(5); MTDSchDetailsNo = 0;
                //if (ParkBillNo != 0)
                //  ObjTrans.Execute("Update TParkingBill set IsCancel='true' where ParkingBillNo=" + ParkBillNo + "", CommonFunctions.ConStr);
                ObjFunction.LockControls(false, this.Controls);
                btnInsScheme.Enabled = false;
                rbEnglish.Enabled = true;
                rbMarathi.Enabled = true;
                DisplayList(false);
                dgBill.Enabled = false;
                btnSave.Enabled = true;
                btnNew.Focus();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void DisplayList(bool flag)
        {
            pnlItemName.Visible = flag;
            pnlGroup1.Visible = flag;
            pnlGroup2.Visible = flag;
            pnlUOM.Visible = flag;
            pnlRate.Visible = flag;
            pnlParking.Visible = flag;
            pnlMainParking.Visible = flag;
            pnlPartial.Visible = flag;
        }

        private void cmbPaymentType_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                int cntCompany = 0;
                if (e.KeyCode == Keys.F12)
                    e.SuppressKeyPress = true;
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    for (int i = 0; i < dgPayType.Rows.Count; i++)
                    {
                        dgPayType.Rows[i].Cells[2].Value = "0.00";
                    }
                    long ControlUnder = ObjQry.ReturnLong("Select ControlUnder From MPayType Where PKPayTypeNo=" + ObjFunction.GetComboValue(cmbPaymentType) + "", CommonFunctions.ConStr);
                    if (ObjFunction.GetComboValue(cmbPartyName) == Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_PartyAC)))
                    {
                        if (ControlUnder == 3)
                        {
                            OMMessageBox.Show("This PayType is Not Valid For Current party .. Select other PayType...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            cmbPaymentType.Focus();
                            return;
                        }
                    }

                    ComboFlag = true;
                    if (EnrFlag == false)
                    {
                    }
                    else
                    {
                        // SetSchemeData(true);
                        CalculateTotal();
                    }
                    ComboFlag = false;

                    //if (ObjFunction.GetComboValue(cmbPaymentType) == 1 || ObjFunction.GetComboValue(cmbPaymentType) == 4 || ObjFunction.GetComboValue(cmbPaymentType) == 5)
                    if (ControlUnder == 1 || ControlUnder == 4 || ControlUnder == 5)
                    {
                        cmbPaymentType.TabIndex = 656;
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsAllowSingleFirmChq)) == true)
                        {
                            for (int i = 1; i < dgBill.Rows.Count - 1; i++)
                                if (Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.StockCompanyNo].Value) != Convert.ToInt64(dgBill.Rows[i - 1].Cells[ColIndex.StockCompanyNo].Value))
                                    cntCompany++;

                        }
                        //if (cntCompany == 0 || ObjFunction.GetComboValue(cmbPaymentType) == 4 || ObjFunction.GetComboValue(cmbPaymentType) == 5)
                        if (cntCompany == 0 || ControlUnder == 4 || ControlUnder == 5)
                        {
                            //dgPayType.CurrentCell = dgPayType[2, 1];
                            pnlPartial.Visible = true;
                            //if (ObjFunction.GetComboValue(cmbPaymentType) == 4 || ObjFunction.GetComboValue(cmbPaymentType) == 5)
                            if (ControlUnder == 4 || ControlUnder == 5)
                            {
                                //if (ObjFunction.GetComboValue(cmbPaymentType) == 4)
                                if (ControlUnder == 4)
                                {
                                    pnlPartial.Size = new Size(475, 214);
                                    pnlPartial.Location = new Point(75, 123);
                                    dgPayChqDetails.Location = dgPayType.Location;
                                    dgPayChqDetails.Visible = true;
                                    dgPayChqDetails.BringToFront();
                                    dgPayChqDetails.Focus();
                                    dgPayCreditCardDetails.Visible = false;
                                    dgPayChqDetails.Enabled = true;
                                    if (dgPayChqDetails.Rows.Count == 0)
                                    {
                                        dgPayChqDetails.Rows.Add();
                                        dgPayChqDetails.CurrentCell = dgPayChqDetails[0, 0];
                                    }
                                }
                                //else if (ObjFunction.GetComboValue(cmbPaymentType) == 5)
                                else if (ControlUnder == 5)
                                {
                                    pnlPartial.Size = new Size(475, 214);
                                    pnlPartial.Location = new Point(75, 123);
                                    dgPayCreditCardDetails.Location = dgPayType.Location;
                                    dgPayCreditCardDetails.Visible = true;
                                    ((DataGridViewTextBoxColumn)dgPayCreditCardDetails.Columns[0]).MaxInputLength = Convert.ToInt32(ObjFunction.GetAppSettings(AppSettings.S_CreditCardDigitLimit));
                                    dgPayCreditCardDetails.Focus();
                                    dgPayCreditCardDetails.BringToFront();
                                    dgPayChqDetails.Visible = false;
                                    dgPayCreditCardDetails.Enabled = true;
                                    if (dgPayCreditCardDetails.Rows.Count == 0)
                                    {
                                        dgPayCreditCardDetails.Rows.Add();
                                        dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[0, 0];
                                    }
                                }
                                for (int i = 0; i < dgPayType.Rows.Count; i++)
                                {
                                    if (dgPayType.Rows[i].Cells[1].Value.ToString() == ObjFunction.GetComboValue(cmbPaymentType).ToString())
                                        dgPayType.Rows[i].Cells[2].Value = txtGrandTotal.Text;
                                }
                                ////dgPayType.Rows[Convert.ToInt32(ObjFunction.GetComboValue(cmbPaymentType)) - 1].Cells[2].Value = txtGrandTotal.Text;
                                // dgPayType.CurrentCell = dgPayType[2, Convert.ToInt32(ObjFunction.GetComboValue(cmbPaymentType)) - 1];
                                CaluculatePayType();
                            }
                            else
                            {
                                dgPayCreditCardDetails.Visible = false;
                                dgPayChqDetails.Visible = false;
                                pnlPartial.Location = new Point(200, 123);
                                pnlPartial.Size = new Size(305, 221);
                                dgPayType.CurrentCell = dgPayType[2, 1];
                                dgPayType.Focus();
                            }
                            btnSave.Enabled = false;
                        }
                    }
                    //else if (ObjFunction.GetComboValue(cmbPaymentType) == 3)
                    else if (ControlUnder == 3)
                    {
                        e.SuppressKeyPress = true;
                        btnSave.Enabled = true;
                        btnSave.Focus();
                    }
                    else
                    {
                        pnlPartial.Visible = false;
                        cmbPaymentType.TabIndex = 10;
                        btnSave.Enabled = true;
                        txtRemark.Focus(); ;
                    }
                    e.SuppressKeyPress = true;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbPaymentType_Leave(object sender, EventArgs e)
        {

            cmbPaymentType_KeyDown(sender, new KeyEventArgs(Keys.Enter));
        }

        private void btnBillCancel_Click(object sender, EventArgs e)
        {
            try
            {
                if (ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo in ( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + ")", CommonFunctions.ConStr) > 1)
                {
                    btnUpdate.Visible = false;
                    btnBillCancel.Visible = false;
                    btnMixMode.Visible = false;
                    //BillFlag = false;
                    OMMessageBox.Show("Already this bill is amount collected", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    return;
                }
                else
                {
                    if (ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo in ( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + " and TR.TypeOfRef in(6))", CommonFunctions.ConStr) > 1)
                    {
                        btnUpdate.Visible = false;
                        btnBillCancel.Visible = false;
                        btnMixMode.Visible = false;
                        //BillFlag = false;
                        OMMessageBox.Show("Already this bill is amount collected", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        return;
                    }
                    else if (ObjQry.ReturnInteger("SELECT  COUNT(*) FROM  TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo " +
                    " WHERE     (TVoucherEntry.Reference = " + txtInvNo.Text + ") AND (TVoucherEntry.VoucherTypeCode = " + VchType.RejectionIn + ") AND (TVoucherDetails.VoucherSrNo = 1) AND (TVoucherDetails.LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + ")", CommonFunctions.ConStr) > 0)
                    {
                        btnBillCancel.Visible = false;
                        //BillFlag = false;
                        OMMessageBox.Show("Already this bill is returned.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        return;
                    }
                }

                if (OMMessageBox.Show("Are you sure you want to cancel this bill ?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question, OMMessageBoxDefaultButton.Button1) == DialogResult.Yes)
                {
                    CancelFlag = true;
                    SaveReceipt(ID);
                    dbTVoucherEntry = new DBTVaucherEntry();
                    tVoucherEntry = new TVoucherEntry();
                    tVoucherEntry.PkVoucherNo = ID;
                    dbTVoucherEntry.CancelTVoucherEntry(tVoucherEntry);


                    OMMessageBox.Show("Bill No. " + txtInvNo.Text + " cancel successfully.....", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);


                    //dtSearch = ObjFunction.GetDataView("Select PkVoucherNo from TVoucherEntry Where VoucherTypeCode=" + VoucherType + " AND IsVoucherLock='false' Order by VoucherDate, VoucherUserNo").Table;
                    //ID = ObjQry.ReturnLong("Select Max(PkVoucherNo) FRom TVoucherEntry Where VoucherTypeCode=" + VoucherType + "", CommonFunctions.ConStr);
                    SetNavigation();

                    setDisplay(true);
                    ObjFunction.LockButtons(true, this.Controls);
                    ObjFunction.LockControls(false, this.Controls);
                    rbEnglish.Enabled = true;
                    rbMarathi.Enabled = true;
                    dgBill.Enabled = false;

                    FillControls();


                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                long tempNo;
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    if (ObjQry.ReturnLong("Select Count(*) From TVoucherEntry Where VoucherUserNo=" + txtSearch.Text + " and VoucherTypeCode=" + VoucherType + "", CommonFunctions.ConStr) > 1)
                    {
                        pnlInvSearch.Visible = true;
                        int x = dgBill.GetCellDisplayRectangle(0, 0, true).X + 200;//(Screen.PrimaryScreen.WorkingArea.Width) / 2;
                        int y = dgBill.GetCellDisplayRectangle(0, 0, true).Y + 100;
                        //pnlPartySearch.SetBounds(x, y, dgPartySearch.Width + 10, dgPartySearch.Height + 10);
                        pnlInvSearch.Location = new Point(x, y);
                        string str = "SELECT    0 as [#], TVoucherEntry.VoucherUserNo AS [Doc #], TVoucherEntry.VoucherDate AS 'Date', TVoucherEntry.BilledAmount AS 'Amount'," +
                                     "TVoucherEntry.PkVoucherNo FROM TVoucherEntry WHERE (TVoucherEntry.VoucherTypeCode IN (" + VchType.Sales + "," + VchType.SalesOrder + ")) AND (TVoucherEntry.CompanyNo = " + DBGetVal.CompanyNo + ")" +
                                     "And TVoucherEntry.VoucherUserNo=" + txtSearch.Text + " " +
                                     "Order By  TVoucherEntry.VoucherUserNo desc,TVoucherEntry.VoucherDate desc, TVoucherEntry.Reference desc";
                        dgInvSearch.DataSource = ObjFunction.GetDataView(str).Table.DefaultView;
                        dgInvSearch.Columns[0].Width = 50;
                        dgInvSearch.Columns[1].Width = 150;
                        dgInvSearch.Columns[2].Width = 80;
                        dgInvSearch.Columns[3].Width = 110;
                        dgInvSearch.Columns[3].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
                        dgInvSearch.Columns[3].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
                        dgInvSearch.Columns[4].Visible = false;
                        if (dgInvSearch.RowCount > 0)
                        {
                            pnlInvSearch.Focus();
                            SearchVisible(false);
                            pnlSearch.Visible = false;
                            e.SuppressKeyPress = true;
                            dgInvSearch.Focus();
                            dgInvSearch.CurrentCell = dgInvSearch[0, dgInvSearch.CurrentRow.Index];
                        }
                        txtSearch.Text = "";
                        cmbPartyNameSearch.SelectedIndex = 0;
                        return;
                    }
                    tempNo = ObjQry.ReturnLong("Select PKVoucherNo From TVoucherEntry Where VoucherUserNo=" + txtSearch.Text + " and VoucherTypeCode=" + VoucherType + "", CommonFunctions.ConStr);
                    if (tempNo > 0)
                    {
                        ID = tempNo;
                        SetNavigation();
                        FillControls();

                        pnlSearch.Visible = false;
                        btnNew.Enabled = true;
                        btnUpdate.Enabled = true;
                        btnBillCancel.Enabled = true;
                        SearchVisible(false);
                    }
                    else
                    {
                        pnlSearch.Visible = false;
                        DisplayMessage("Bill Not Found");
                        Application.DoEvents();
                        pnlSearch.Visible = true;
                        cmbPartyNameSearch.SelectedIndex = 0;
                        rbInvNo.Focus();
                        txtSearch.Focus();
                        txtSearch.Text = "";
                    }
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    e.SuppressKeyPress = true;
                    pnlSearch.Visible = false;
                    pnlPartySearch.Visible = false;
                    pnlInvSearch.Visible = false;
                    btnCancelSearch_Click(sender, new EventArgs());
                    btnNew.Focus();
                }
                else if (e.KeyCode == Keys.Right)
                {
                    e.SuppressKeyPress = true;
                    rbPartyName.Checked = true;
                    rbType_CheckedChanged(rbPartyName, null);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnCancelSearch_Click(object sender, EventArgs e)
        {
            pnlSearch.Visible = false;
            btnNew.Enabled = true;
            btnUpdate.Enabled = true;
            btnBillCancel.Enabled = true;
        }

        private void txtChrg2_Leave(object sender, EventArgs e)
        {
            if (cmbPaymentType.Enabled == true)
                cmbPaymentType.Focus();
            else
                btnSave.Focus();
            CalculateTotal();
        }

        private void txtRemark_Leave(object sender, EventArgs e)
        {
            //if (txtRemark.Text.Trim() == "")
            //{
            //    txtRemark.Text = "Sales Bill";
            //    btnSave.Focus();
            //}
        }

        private void ChkQty_CheckedChanged(object sender, EventArgs e)
        {
            //StopOnQty = ChkQty.Checked;
        }

        private void ChkRate_CheckedChanged(object sender, EventArgs e)
        {
            //StopOnRate = ChkRate.Checked;
        }

        #region Without Barcode Methods

        private void FillItemList(int qNo, int iType)
        {
            try
            {
                switch (iType)
                {
                    case 1:
                        FillItemList(qNo);
                        break;
                    case 2:
                        break;
                    case 3:
                        //string  ItemList = " SELECT MStockItems.ItemNo, MStockItems.ItemName,MStockItems.ItemNameLang, MRateSetting." + ObjFunction.GetComboValueString(cmbRateType) + " AS SaleRate, MUOM.UOMName, MRateSetting.MRP, " +
                        //         " 0 AS Stock, '' AS stkUOM, 0 AS SaleTax, 0 AS PurTax, " + //MItemTaxInfo_Sale.Percentage AS SaleTax , MItemTaxInfo_Pur.Percentage
                        //         " MStockItems.CompanyNo, MStockBarcode.Barcode, MRateSetting.PkSrNo As RateSettingNo, MStockItems.UOMDefault " +
                        //         " FROM MStockItems_V(NULL,NULL,NULL,NULL,NULL,NULL,NULL) AS MStockItems INNER JOIN " +
                        //         " dbo.GetItemRateAll(NULL, NULL, NULL, NULL, '" + dtpBillDate.Value.ToString("dd-MMM-yyyy") + " " + DBGetVal.ServerTime.ToLongTimeString() + "',NULL) AS MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo AND  " +
                        //         " MStockItems.UOMDefault = MRateSetting.UOMNo INNER JOIN " +
                        //         " MStockBarcode ON MRateSetting.FkBcdSrNo = MStockBarcode.PkStockBarcodeNo INNER JOIN " +
                        //         " MUOM ON MStockItems.UOMDefault = MUOM.UOMNo  Where MStockItems.ItemNo in " +
                        //         "(Select ItemNo from MStockBarcode where Barcode = '" + dgBill.CurrentCell.Value + "' And IsActive ='true') And MStockItems.IsActive='true'" +
                        //         " ORDER BY MStockItems.ItemName";
                        DataTable dtBarCodeItemNo = ObjFunction.GetDataView("Select ItemNo from MStockBarcode where Barcode = '" + dgBill.CurrentCell.Value + "'").Table;
                        string ItemList = "";
                        for (int i = 0; i < dtBarCodeItemNo.Rows.Count; i++)
                        {
                            if (i != 0)
                            {
                                ItemList += " Union all ";
                            }

                            //ItemList += " SELECT MStockItems.ItemNo, MStockItems.ItemName,MStockItems.ItemNameLang, MRateSetting." + ObjFunction.GetComboValueString(cmbRateType) + " AS SaleRate, MUOM.UOMName, MRateSetting.MRP, " +
                            //    " (SELECT  ISNULL(SUM(CASE WHEN trncode = 1 THEN quantity ELSE - quantity END), 0) +  ISNULL(SUM(CASE WHEN trncode = 1 THEN FreeQty ELSE - FreeQty END), 0)   FROM TStock  INNER JOIN TStockGodown ON TSTock.PKStockTrnNo=TStockGodown.FKStockTrnNo   WHERE  TStockGodown.GodownNo=" + ObjFunction.GetAppSettings(AppSettings.S_OutwardLocation) + " AND    (FkRateSettingNo IN  (SELECT PkSrNo  FROM MRateSetting AS MRateSetting_1  WHERE (TStock.ItemNo = MStockItems.ItemNo) AND (MRP = MRateSetting.MRP))) AND (TStock.ItemNo = MStockItems.ItemNo) And PkStockTrnNo Not In (SELECT TStock.PkStockTrnNo FROM TVoucherEntry INNER JOIN  TStock ON TVoucherEntry.PkVoucherNo = TStock.FKVoucherNo WHERE (TVoucherEntry.IsCancel = 'True') AND (TStock.ItemNo = MStockItems.ItemNo))) AS Stock , '' AS stkUOM, 0 AS SaleTax, 0 AS PurTax, " + //MItemTaxInfo_Sale.Percentage AS SaleTax , MItemTaxInfo_Pur.Percentage
                            //    " MStockItems.CompanyNo, MStockBarcode.Barcode, MRateSetting.PkSrNo As RateSettingNo, MStockItems.UOMDefault,0 As PurRate " +
                            //    " FROM MStockItems_V(NULL," + dtBarCodeItemNo.Rows[i].ItemArray[0].ToString() + ",NULL,NULL,NULL,NULL,NULL) AS MStockItems INNER JOIN " +
                            //    " dbo.GetItemRateAll(" + dtBarCodeItemNo.Rows[i].ItemArray[0].ToString() + ", NULL, NULL, NULL, '" + dtpBillDate.Value.ToString("dd-MMM-yyyy") + " " + DBGetVal.ServerTime.ToLongTimeString() + "',NULL) AS MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo AND  " +
                            //    " MStockItems.UOMDefault = MRateSetting.UOMNo INNER JOIN " +
                            //    " MStockBarcode ON MRateSetting.FkBcdSrNo = MStockBarcode.PkStockBarcodeNo INNER JOIN " +
                            //    " MUOM ON MStockItems.UOMDefault = MUOM.UOMNo  Where  " +
                            //    " MStockItems.IsActive='true'";

                            ItemList += " SELECT MStockItems.ItemNo, MStockItems.ShowItemName As ItemName,MStockItems.ShowItemName_Lang as ItemNameLang, MRateSetting." + ObjFunction.GetComboValueString(cmbRateType) + " AS SaleRate, MUOM.UOMName, MRateSetting.MRP, " +
                                " IsNull(MSB.CurrentStock,0) AS Stock , '' AS stkUOM, 0 AS SaleTax, 0 AS PurTax, " + //MItemTaxInfo_Sale.Percentage AS SaleTax , MItemTaxInfo_Pur.Percentage
                                " MStockItems.CompanyNo, MStockBarcode.Barcode, MRateSetting.PkSrNo As RateSettingNo, MStockItems.UOMDefault,0 As PurRate " +
                                " FROM MStockItems INNER JOIN " +
                                " dbo.GetItemRateAll(" + dtBarCodeItemNo.Rows[i].ItemArray[0].ToString() + ", NULL, NULL, NULL, '" + dtpBillDate.Value.ToString("dd-MMM-yyyy") + " " + DBGetVal.ServerTime.ToLongTimeString() + "',NULL) AS MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo AND  " +
                                " MStockItems.UOMDefault = MRateSetting.UOMNo INNER JOIN " +
                                " MStockBarcode ON MRateSetting.FkBcdSrNo = MStockBarcode.PkStockBarcodeNo INNER JOIN " +
                                " MUOM ON MStockItems.UOMDefault = MUOM.UOMNo LEFT OUTER JOIN MStockItemBalance MSB ON MSB.ItemNo = MStockItems.ItemNo AND MSB.MRP = MRateSetting.MRP AND MSB.GodownNo = MStockItems.GodownNo Where  " +
                                " MStockItems.IsActive='true'";
                        }
                        ItemList += " ORDER BY MStockItems.ShowItemName";

                        DataTable dtItemList = ObjFunction.GetDataView(ItemList).Table;
                        if (dtItemList.Rows.Count > 0)
                        {
                            dgItemList.DataSource = dtItemList.DefaultView;
                            dgItemList.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                            dgItemList.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                            dgItemList.Columns[14].Visible = false;
                            pnlItemName.Visible = true;
                            //  dgItemList.CurrentCell = dgItemList[2, 0];
                            dgItemList.Focus();
                        }
                        else
                        {
                            //DisplayMessage("Items Not Found......");
                            OMMessageBox.Show("SKU Not Found.\nPRESS ESCAPE TO CONTINUE....", "Information", OMMessageBoxButton.Escape, OMMessageBoxIcon.Information);
                        }
                        break;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillItemList(int qNo)
        {
            try
            {
                if (qNo == 0)
                {
                    qNo = iItemNameStartIndex;
                }

                string ItemList = strItemQuery[qNo - 1];

                ItemList = ItemList.Replace("@cmbRateType@", "" + ObjFunction.GetComboValueString(cmbRateType));

                switch (qNo)
                {
                    case 1:
                        break;
                    case 2:
                        switch (strItemQuery.Length)
                        {
                            case 2:
                                ItemList = ItemList.Replace("@Param1@", "" + (Convert.ToInt64(lstGroup1.SelectedValue) != 0 ? lstGroup1.SelectedValue.ToString() : Param1Value));
                                ItemList = ItemList.Replace("@Param1NULL@", "" + (Convert.ToInt64(lstGroup1.SelectedValue) != 0 ? lstGroup1.SelectedValue.ToString() : "NULL"));
                                break;
                            case 3:
                                ItemList = ItemList.Replace("@Param2@", "" + (Convert.ToInt64(lstGroup2.SelectedValue) != 0 ? lstGroup2.SelectedValue.ToString() : Param2Value));
                                ItemList = ItemList.Replace("@Param2NULL@", "" + (Convert.ToInt64(lstGroup2.SelectedValue) != 0 ? lstGroup2.SelectedValue.ToString() : "NULL"));
                                break;
                        }
                        break;
                    case 3:
                        ItemList = ItemList.Replace("@Param1@", "" + (Convert.ToInt64(lstGroup1.SelectedValue) != 0 ? lstGroup1.SelectedValue.ToString() : Param1Value));
                        ItemList = ItemList.Replace("@Param2@", "" + (Convert.ToInt64(lstGroup2.SelectedValue) != 0 ? lstGroup2.SelectedValue.ToString() : Param2Value));
                        ItemList = ItemList.Replace("@Param1NULL@", "" + (Convert.ToInt64(lstGroup1.SelectedValue) != 0 ? lstGroup1.SelectedValue.ToString() : "NULL"));
                        ItemList = ItemList.Replace("@Param2NULL@", "" + (Convert.ToInt64(lstGroup2.SelectedValue) != 0 ? lstGroup2.SelectedValue.ToString() : "NULL"));
                        break;
                }
                ItemList = ItemList.Replace("@CompNo@", "MStockItems.CompanyNo");
                //ItemList = ItemList.Replace("TStockGodown.GodownNo=2", "TStockGodown.GodownNo=" + ObjFunction.GetAppSettings(AppSettings.S_OutwardLocation) + "");
                ItemList = ItemList.Replace("@GodownNo@", "MStockItems.GodownNo");
                switch (strItemQuery.Length - qNo)
                {
                    case 0:
                        if (!ItemList.Equals(strItemQuery_last[qNo - 1], StringComparison.CurrentCultureIgnoreCase))
                        {
                            ItemList = ItemList.Replace("AS ItemName,", "AS ItemName,Case When(MStockItems.LangShortDesc<>'') then MStockItems.LangShortDesc else MStockItems.LangFullDesc end AS ItemNameLang,");
                            DataTable dtItemList = ObjFunction.GetDataView(ItemList).Table;
                            strItemQuery_last[qNo - 1] = ItemList;
                            if (dtItemList.Rows.Count > 0)
                            {
                                dgItemList.DataSource = dtItemList.DefaultView;
                                dgItemList.Columns[14].Visible = false;
                                pnlItemName.Visible = true;
                                dgItemList.CurrentCell = dgItemList[1, 0];
                                dgItemList.Focus();
                            }
                            else
                            {
                                //DisplayMessage("Items Not Found......");
                                OMMessageBox.Show("SKU Not Found.\nPRESS ESCAPE TO CONTINUE....", "Information", OMMessageBoxButton.Escape, OMMessageBoxIcon.Information);
                            }
                        }
                        else
                        {
                            pnlItemName.Visible = true;
                            dgItemList.CurrentCell = dgItemList[1, 0];
                            dgItemList.Focus();
                        }
                        break;
                    case 1:
                        if (!ItemList.Equals(strItemQuery_last[qNo - 1], StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_Bilingual)) == true)
                                ObjFunction.FillList(lstGroup1Lang, ItemList.Replace("StockGroupName from", "LanguageName from"));
                            //ObjFunction.FillList(lstGroup1Lang, ItemList);
                            BrandFilter = ItemList;
                            ObjFunction.FillList(lstGroup1, ItemList);
                            strItemQuery_last[qNo - 1] = ItemList;
                        }
                        pnlGroup1.Visible = true;
                        txtBrandFilter.Focus();
                        break;
                    case 2:
                        if (!ItemList.Equals(strItemQuery_last[qNo - 1], StringComparison.CurrentCultureIgnoreCase))
                        {
                            ObjFunction.FillList(lstGroup2, ItemList);
                            strItemQuery_last[qNo - 1] = ItemList;
                        }
                        pnlGroup2.Visible = true;
                        lstGroup2.Focus();
                        break;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void lstUOM_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                UOM_MoveNext();
            }
            else if (e.KeyChar == ' ')
            {
                dgBill.Focus();
                pnlUOM.Visible = false;
            }
        }

        private void lstGroup1_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                //string ItemListStr = "";
                if (e.KeyChar == 13)
                {
                    pnlGroup1.Visible = false;

                    FillItemList(strItemQuery.Length);

                    //if (ItemNameType == 2) 
                    //{
                    //    ItemListStr = " SELECT MStockItems.ItemNo, MStockItems.ItemName, MRateSetting." + ObjFunction.GetComboValueString(cmbRateType) + " AS SaleRate, MUOM.UOMName, MRateSetting.MRP, " +
                    //            " 0 AS Stock, '' AS stkUOM, 0 AS SaleTax, 0 AS PurTax, " +//MItemTaxInfo_Sale.Percentage,MItemTaxInfo_Pur.Percentage
                    //            " MStockItems.CompanyNo, MStockBarcode.Barcode, MRateSetting.PkSrNo As RateSettingNo, MStockItems.UOMDefault " +
                    //            " FROM MStockItems_V(NULL,NULL,NULL,NULL,NULL,NULL,NULL) AS MStockItems INNER JOIN " +
                    //            " dbo.GetItemRateAll(NULL, NULL, NULL, NULL, NULL," + Convert.ToInt64(lstGroup1.SelectedValue) + ") AS MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo AND  " +
                    //            " MStockItems.UOMDefault = MRateSetting.UOMNo INNER JOIN " +
                    //            " MStockBarcode ON MRateSetting.FkBcdSrNo = MStockBarcode.PkStockBarcodeNo INNER JOIN " +
                    //            //" dbo.GetItemTaxAll(NULL, NULL, 10," + ObjFunction.GetComboValue(cmbTaxType) + "," + Convert.ToInt64(lstGroup1.SelectedValue) + ") AS MItemTaxInfo_Sale ON MStockItems.ItemNo = MItemTaxInfo_Sale.ItemNo INNER JOIN " +
                    //            //" dbo.GetItemTaxAll(NULL, NULL, 11," + ObjFunction.GetComboValue(cmbTaxType) + "," + Convert.ToInt64(lstGroup1.SelectedValue) + ") AS MItemTaxInfo_Pur ON MStockItems.ItemNo = MItemTaxInfo_Pur.ItemNo INNER JOIN " +
                    //            " MUOM ON MStockItems.UOMDefault = MUOM.UOMNo  Where MStockItems.ItemNo <> 1 " +
                    //            " AND MStockItems.GroupNo = " + Convert.ToInt64(lstGroup1.SelectedValue) + " " +
                    //            " ORDER BY MStockItems.ItemName";
                    //    DataTable dtItemList = ObjFunction.GetDataView(ItemListStr).Table;
                    //    if (dtItemList.Rows.Count > 0)
                    //    {
                    //        dgItemList.DataSource = dtItemList.DefaultView;
                    //        pnlItemName.Visible = true;
                    //        dgItemList.CurrentCell = dgItemList[1, 0];
                    //        dgItemList.Focus();
                    //    }
                    //    else
                    //    {
                    //        DisplayMessage("Items Not Found......");
                    //    }

                    //}
                    //else if (ItemNameType == 4)
                    //{
                    //    ItemListStr = "Select StockGroupNo,StockGroupName from MStockGroup " + 
                    //         " where StockGroupNo in (select Distinct GroupNo1 from MStockItems " + 
                    //         " where GroupNo=" + Convert.ToInt64(lstGroup1.SelectedValue) + ")";
                    //    ObjFunction.FillList(lstGroup2, ItemListStr);
                    //    pnlGroup1.Visible = false;
                    //    pnlGroup2.Visible = true;
                    //    lstGroup2.Focus();
                    //}
                    lblBrandName.Text = lstGroup1.Text;

                }
                else if (e.KeyChar == ' ')
                {
                    dgBill.Focus();
                    pnlGroup1.Visible = false;
                    txtBrandFilter.Text = "";
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void lstGroup2_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                //string ItemListStr = "";
                if (e.KeyChar == 13)
                {
                    pnlGroup2.Visible = false;
                    FillItemList(strItemQuery.Length - 1);
                }
                else if (e.KeyChar == ' ')
                {
                    dgBill.Focus();
                    pnlGroup2.Visible = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void lstRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 13)
                {
                    dgBill.CurrentRow.Cells[ColIndex.Rate].Value = lstRate.Text;
                    dgBill.CurrentRow.Cells[ColIndex.PkRateSettingNo].Value = lstRate.SelectedValue;
                    Rate_MoveNext();
                    pnlRate.Visible = false;
                }
                else if (e.KeyChar == ' ')
                {
                    dgBill.Focus();
                    pnlRate.Visible = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillUOMList(int RowIndex)
        {
            try
            {
                ObjFunction.FillList(lstUOM, "UomNo", "UomName");

                if (ItemType == 2)
                    strUom = " Select * From GetUomList ('" + Convert.ToString(dgBill.Rows[RowIndex].Cells[ColIndex.Barcode].Value) + "',0," + Convert.ToDouble(dgBill.Rows[RowIndex].Cells[ColIndex.Quantity].Value) + ")";
                else
                    strUom = " Select * From GetUomList (''," + Convert.ToInt64(dgBill.Rows[RowIndex].Cells[ColIndex.ItemNo].Value) + "," + Convert.ToDouble(dgBill.Rows[RowIndex].Cells[ColIndex.Quantity].Value) + ")";

                ObjFunction.FillList(lstUOM, strUom);

                if (lstUOM.Items.Count == 0)
                {
                    dgBill.Focus();
                }
                else if (lstUOM.Items.Count == 1)
                {
                    //dgBill.Rows[RowIndex].Cells[3].Value = lstUOM.Text;
                    //dgBill.Rows[RowIndex].Cells[ColIndex.UOMNo].Value = Convert.ToInt64(lstUOM.SelectedValue);
                    lstUOM.SelectedIndex = 0;
                    //lstUOM_KeyPress(lstUOM, new KeyPressEventArgs((char)13));
                    //CalculateGridValues(RowIndex);
                    UOM_MoveNext();
                }
                else
                {
                    ////lstUOM.SelectedIndex = 0;
                    //lstUOM.SelectedValue = dgBill.Rows[RowIndex].Cells[ColIndex.UOMNo].Value;
                    ////lstUOM_KeyPress(lstUOM, new KeyPressEventArgs((char)13));
                    ////CalculateGridValues(RowIndex);
                    //UOM_MoveNext();
                    if (flagParking == true)
                    {
                        lstUOM.SelectedValue = dgBill.Rows[RowIndex].Cells[ColIndex.UOMNo].Value;
                        UOM_MoveNext();
                    }
                    else
                    {
                        CalculateTotal();
                        pnlUOM.Visible = true;
                        lstUOM.Focus();
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #endregion

        private void lst_VisibleChanged(object sender, EventArgs e)
        {
            try
            {
                if (((System.Windows.Forms.Control)sender).Visible == true)
                    dgBill.Enabled = false;
                else
                {
                    dgBill.Enabled = true;
                    dgBill.Focus();
                }
                if (((System.Windows.Forms.Control)sender).Name == "pnlItemName")
                {
                    if (((System.Windows.Forms.Control)sender).Visible == false)
                        pnlSalePurHistory.Visible = ((System.Windows.Forms.Control)sender).Visible;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgBill_CurrentCellChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgBill.CurrentCell != null)
                {
                    for (int i = 0; i < dgBill.Rows.Count; i++)
                    {
                        dgBill.Rows[i].DefaultCellStyle.BackColor = Color.White;
                    }
                    dgBill.Rows[dgBill.CurrentCell.RowIndex].DefaultCellStyle.BackColor = clrColorRow;
                    dgBill.CurrentCell.Style.SelectionBackColor = Color.Yellow;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #region ColumnIndex
        private static class ColIndex
        {
            public static int SrNo = 0;
            public static int ItemName = 1;
            public static int Quantity = 2;
            public static int UOM = 3;
            public const int MRP = 4;
            public static int Rate = 5;
            public static int NetRate = 6;
            public static int DiscPercentage = 7;
            public static int DiscAmount = 8;
            public static int DiscRupees = 9;
            public static int DiscPercentage2 = 10;
            public static int DiscAmount2 = 11;
            public static int DiscRupees2 = 12;
            public static int NetAmt = 13;
            public static int Amount = 14;
            public static int Barcode = 15;
            public static int PkStockTrnNo = 16;
            public static int PkBarCodeNo = 17;
            public static int PkVoucherNo = 18;
            public static int ItemNo = 19;
            public static int UOMNo = 20;
            public static int TaxLedgerNo = 21;
            public static int SalesLedgerNo = 22;
            public static int PkRateSettingNo = 23;
            public static int PkItemTaxInfo = 24;
            public static int StockFactor = 25;
            public static int ActualQty = 26;
            public static int MKTQuantity = 27;
            public static int TaxPercentage = 28;
            public static int TaxAmount = 29;
            public static int SalesVchNo = 30;
            public static int TaxVchNo = 31;
            public static int StockCompanyNo = 32;
            public static int SchemeDetailsNo = 33;
            public static int SchemeFromNo = 34;
            public static int SchemeToNo = 35;
            public static int RewardFromNo = 36;
            public static int RewardToNo = 37;
            public static int ItemLevelDiscNo = 38;
            public static int FKItemLevelDiscNo = 39;
            public static int GodownNo = 40;
            public static int DiscountType = 41;
            public static int HamaliInKg = 42;
            public const int HSNCode = 43;
            public const int IGSTPercent = 44;
            public const int IGSTAmount = 45;
            public const int CGSTPercent = 46;
            public const int CGSTAmount = 47;
            public const int SGSTPercent = 48;
            public const int SGSTAmount = 49;
            public const int UTGSTPercent = 50;
            public const int UTGSTAmount = 51;
            public const int CessPercent = 52;
            public const int CessAmount = 53;
            public const int IsQtyRead = 54;

        }
        #endregion

        private long GetVoucherPK(string expression)
        {
            long strVal = 0;
            try
            {
                if (dtVchMainDetails.Rows.Count > 0)
                {
                    DataRow[] result = dtVchMainDetails.Select(expression);
                    strVal = Convert.ToInt64(result[0].ItemArray[0].ToString());
                }
            }
            catch (Exception e)
            {
                strVal = 0;
                CommonFunctions.ErrorMessge = e.Message;
            }
            return strVal;
        }

        private double GetRatio(long CompNo)
        {
            double ValRatio = 0;
            try
            {
                for (int i = 0; i < dtCompRatio.Rows.Count; i++)
                {
                    if (Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString()) == CompNo)
                    {
                        ValRatio = Convert.ToDouble(dtCompRatio.Rows[i].ItemArray[1].ToString());
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                ValRatio = 0;
                CommonFunctions.ErrorMessge = e.Message;
            }
            return ValRatio;
        }

        #region Receipt Grid Methods
        private void BindGridPayType(long ID)
        {
            try
            {
                DataTable dtPayType = new DataTable();
                dtPayLedger = ObjFunction.GetDataView("Select * From MPayTypeLedger").Table;
                string sqlQuery = "";
                if (ID == 0)
                    sqlQuery = "SELECT PayTypeName, PKPayTypeNo, Cast(0.00 as varchar) AS Amount, 0 AS LedgerNo, 0 AS PKVoucherPayTypeNo, ControlUnder FROM MPayType ORDER BY PKPayTypeNo";
                else
                    sqlQuery = "SELECT PayTypeName, PKPayTypeNo,Cast( IsNull((SELECT SUM(Amount) FROM TVoucherPayTypeDetails WHERE (FKSalesVoucherNo = " + ID + ") AND (FKPayTypeNo = PKPayTypeNo)),0) AS varchar) AS Amount, 0 AS LedgerNo, 0 AS PKVoucherPayTypeNo,ControlUnder FROM MPayType ORDER BY PKPayTypeNo";

                //sqlQuery = "SELECT PT.PayTypeName, PT.PKPayTypeNo, ISNULL(Sum(TD.Amount), 0) AS Amount, PT.LedgerNo, " +
                //    " 0 AS PKVoucherPayTypeNo FROM MPayType AS PT LEFT OUTER JOIN (SELECT PKVoucherPayTypeNo, FKSalesVoucherNo, FKReceiptVoucherNo, FKVoucherTrnNo, FKPayTypeNo, Amount " +
                //    " FROM TVoucherPayTypeDetails WHERE (FKSalesVoucherNo = " + ID + ")) AS TD ON PT.PKPayTypeNo = TD.FKPayTypeNo " +
                //    " GROUP BY PT.PayTypeName, PT.PKPayTypeNo, ISNULL(TD.Amount, 0), PT.LedgerNo ORDER BY PT.PKPayTypeNo";


                dtPayType = ObjFunction.GetDataView(sqlQuery).Table;
                while (dgPayType.Columns.Count > 0)
                    dgPayType.Columns.RemoveAt(0);
                dgPayType.DataSource = dtPayType.DefaultView;
                for (int i = 0; i < dgPayType.Columns.Count; i++)
                    dgPayType.Columns[i].Visible = false;
                dgPayType.Columns[0].Visible = true;
                dgPayType.Columns[2].Visible = true;
                dgPayType.Rows[0].Visible = false;
                dgPayType.Columns[0].Width = 150;
                dgPayType.Columns[2].Width = 100;
                dgPayType.Columns[0].ReadOnly = true;
                dgPayType.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgPayType.Rows[0].Visible = false;
                if (ID != 0)
                {
                    long ControlUnder = ObjQry.ReturnLong("Select ControlUnder From MPayType Where PKPayTypeNo=" + ObjFunction.GetComboValue(cmbPaymentType) + "", CommonFunctions.ConStr);
                    //if (ObjFunction.GetComboValue(cmbPaymentType) == 1 || ObjFunction.GetComboValue(cmbPaymentType) == 4 || ObjFunction.GetComboValue(cmbPaymentType) == 5)
                    if (ControlUnder == 1 || ControlUnder == 4 || ControlUnder == 5)
                    {
                        for (int i = 0; i < dgPayType.Rows.Count; i++)
                        {
                            if (dgPayType.Rows[i].Cells[1].Value.ToString() == ObjFunction.GetComboValue(cmbPaymentType).ToString())
                            {
                                if (ControlUnder == 4)
                                    dgPayType.Rows[i].Cells[2].Value = ObjQry.ReturnDouble("Select Sum(Amount) From TVoucherChqCreditDetails Where FKVoucherNo=" + ID + "  AND ChequeNo <>''", CommonFunctions.ConStr).ToString("0.00");
                                else if (ControlUnder == 5)
                                    dgPayType.Rows[i].Cells[2].Value = ObjQry.ReturnDouble("Select Sum(Amount) From TVoucherChqCreditDetails Where FKVoucherNo=" + ID + "  AND CreditCardNo <>''", CommonFunctions.ConStr).ToString("0.00");
                            }
                        }
                        //dgPayType.Rows[3].Cells[2].Value = ObjQry.ReturnDouble("Select Sum(Amount) From TVoucherChqCreditDetails Where FKVoucherNo=" + ID + "  AND ChequeNo <>''", CommonFunctions.ConStr).ToString("0.00");
                        //dgPayType.Rows[4].Cells[2].Value = ObjQry.ReturnDouble("Select Sum(Amount) From TVoucherChqCreditDetails Where FKVoucherNo=" + ID + "  AND CreditCardNo <>''", CommonFunctions.ConStr).ToString("0.00");
                    }

                    double RefAmt = ObjQry.ReturnDouble("Select Sum(Amount) From TVoucherRefDetails Where FKVucherTrnNo in (Select PKVoucherTrnNo From TVoucherDetails Where FkVoucherNo=" + ID + ")", CommonFunctions.ConStr);
                    if (RefAmt > 0)
                    {
                        dgPayType.Rows[2].Cells[2].Value = RefAmt;
                    }
                }
                CaluculatePayType();
                pnlPartial.Visible = false;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgPayType_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 2)
                {
                    if (dgPayType.CurrentCell.Value == null) dgPayType.CurrentCell.Value = "0";
                    if (ObjFunction.CheckValidAmount(dgPayType.CurrentCell.Value.ToString()) == false)
                    {
                        dgPayType.CurrentCell.Value = "0.00";
                        OMMessageBox.Show("Please Enter valid amount..", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { dgPayType.CurrentCell.RowIndex, 2, dgPayType });
                    }
                    else
                    {
                        dgPayType.CurrentCell.ErrorText = "";
                        dgPayType.CurrentCell.Value = Convert.ToDouble(dgPayType.CurrentCell.Value).ToString("0.00");
                        CaluculatePayType();

                        if (dgPayType.CurrentCell.RowIndex == 3)
                        {
                            if (Convert.ToDouble(dgPayType.Rows[3].Cells[2].Value) <= 0)
                            {
                                dgPayChqDetails.Rows.Clear();
                                dgPayChqDetails.Rows.Add();
                            }
                            else
                            {
                                pnlPartial.Size = new Size(775, 214);
                                pnlPartial.Location = new Point(20, 123);
                                dgPayChqDetails.Location = new Point(304, 3);
                                dgPayChqDetails.Visible = true;
                                dgPayChqDetails.BringToFront();
                                dgPayChqDetails.Focus();
                                dgPayCreditCardDetails.Visible = false;
                                if (dgPayChqDetails.Rows.Count == 0)
                                {
                                    dgPayChqDetails.Rows.Add();
                                    dgPayChqDetails.CurrentCell = dgPayChqDetails[0, 0];
                                }
                            }
                        }
                        if (dgPayType.CurrentCell.RowIndex == 4)
                        {
                            if (Convert.ToDouble(dgPayType.Rows[4].Cells[2].Value) <= 0)
                            {
                                dgPayCreditCardDetails.Rows.Clear();
                                dgPayCreditCardDetails.Rows.Add();
                            }
                            else
                            {
                                pnlPartial.Size = new Size(775, 214);
                                pnlPartial.Location = new Point(20, 123);
                                dgPayCreditCardDetails.Location = new Point(304, 3);
                                dgPayCreditCardDetails.Visible = true;
                                dgPayCreditCardDetails.Focus();
                                dgPayCreditCardDetails.BringToFront();
                                dgPayChqDetails.Visible = false;
                                if (dgPayCreditCardDetails.Rows.Count == 0)
                                {
                                    dgPayCreditCardDetails.Rows.Add();
                                    dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[0, 0];
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgPayType_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (dgPayType.CurrentCell.ColumnIndex == 2)
            {
                TextBox txt = (TextBox)e.Control;
                txt.KeyDown += new KeyEventHandler(txtAmt_KeyDown);
            }
        }

        private void txtAmt_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyValue == 13)
                {
                    if (ObjFunction.CheckValidAmount(dgPayType.CurrentCell.Value.ToString()) == false)
                    {
                        OMMessageBox.Show("Please Enter valid amount..", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        dgPayType.CurrentCell = dgPayType[2, dgPayType.CurrentCell.RowIndex];
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void CaluculatePayType()
        {
            try
            {
                double TotAmt = 0;
                for (int i = 0; i < dgPayType.Rows.Count; i++)
                {
                    if (dgPayType.Rows[i].Cells[2].Value == null) dgPayType.Rows[i].Cells[2].Value = "0";
                    TotAmt += Convert.ToDouble(dgPayType.Rows[i].Cells[2].Value);
                }
                txtTotalAmt.Text = TotAmt.ToString("0.00");
                if (txtGrandTotal.Text != "")
                    lblPayTypeBal.Text = (Convert.ToDouble(txtGrandTotal.Text) - TotAmt).ToString("0.00");
                else
                    lblPayTypeBal.Text = "0.00";
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillPayType()
        {
            try
            {
                int cntflag = 0;
                for (int i = 0; i < dgPayType.Rows.Count; i++)
                {
                    if (Convert.ToDouble(dgPayType.Rows[i].Cells[2].Value) > 0)
                        cntflag += 1;
                    if (cntflag > 1) break;
                }
                if (cntflag > 1) cmbPaymentType.SelectedValue = "1";

                long PayType = ObjFunction.GetComboValue(cmbPaymentType);
                for (int i = 0; i < dtCompRatio.Rows.Count; i++)
                {
                    dgPayType.Columns.Add(dtCompRatio.Rows[i].ItemArray[0].ToString(), dtCompRatio.Rows[i].ItemArray[0].ToString());
                    dgPayType.Columns[dgPayType.Columns.Count - 1].Visible = false;
                }

                if (PayType != 1)
                {
                    for (int i = 0; i < dgPayType.Rows.Count; i++)
                    {
                        if (PayType == Convert.ToInt64(dgPayType.Rows[i].Cells[1].Value))
                        {
                            dgPayType.Rows[i].Cells[2].Value = txtGrandTotal.Text;
                            for (int j = 0; j < dtCompRatio.Rows.Count; j++)
                            {
                                dgPayType.Rows[i].Cells[4 + (j + 1)].Value = (Convert.ToDouble(txtGrandTotal.Text) * Convert.ToDouble(dtCompRatio.Rows[j].ItemArray[1].ToString())) / 10;
                            }
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < dgPayType.Rows.Count; i++)
                    {
                        for (int j = 0; j < dtCompRatio.Rows.Count; j++)
                        {
                            dgPayType.Rows[i].Cells[4 + (j + 1)].Value = (Convert.ToDouble(dgPayType.Rows[i].Cells[2].Value) * Convert.ToDouble(dtCompRatio.Rows[j].ItemArray[1].ToString())) / 10;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgPayType_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    btnOk_Click(sender, new EventArgs());
                    //if (Convert.ToDouble(txtTotalAmt.Text) != Convert.ToDouble(txtGrandTotal.Text))
                    //{
                    //    OMMessageBox.Show("TOTAL AMOUNT EXCEEDS TO BILL AMOUNT.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    //}
                    //else
                    //{
                    //    btnSave.Enabled = true;
                    //    btnSave.Focus();
                    //    pnlPartial.Visible = false;
                    //}
                }
                else if (e.KeyCode == Keys.D && e.Control)
                {
                    if (dgPayType.CurrentCell.RowIndex == 3)
                    {
                        if (Convert.ToDouble(dgPayType.Rows[dgPayType.CurrentCell.RowIndex].Cells[2].Value) > 0)
                        {
                            pnlPartial.Size = new Size(775, 214);
                            pnlPartial.Location = new Point(20, 123);
                            dgPayChqDetails.Location = new Point(304, 3);
                            dgPayChqDetails.Visible = true;
                            dgPayChqDetails.BringToFront();
                            dgPayChqDetails.Focus();
                            dgPayCreditCardDetails.Visible = false;
                            if (dgPayChqDetails.Rows.Count == 0)
                            {
                                dgPayChqDetails.Rows.Add();
                                dgPayChqDetails.CurrentCell = dgPayChqDetails[0, 0];
                            }
                        }
                    }
                    else if (dgPayType.CurrentCell.RowIndex == 4)
                    {
                        if (Convert.ToDouble(dgPayType.Rows[dgPayType.CurrentCell.RowIndex].Cells[2].Value) > 0)
                        {
                            pnlPartial.Size = new Size(775, 214);
                            pnlPartial.Location = new Point(20, 123);
                            dgPayCreditCardDetails.Location = new Point(304, 3);
                            dgPayCreditCardDetails.Visible = true;
                            dgPayCreditCardDetails.Focus();
                            dgPayCreditCardDetails.BringToFront();
                            dgPayChqDetails.Visible = false;
                            if (dgPayCreditCardDetails.Rows.Count == 0)
                            {
                                dgPayCreditCardDetails.Rows.Add();
                                dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[0, 0];
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private bool SaveReceipt(long SalesID)
        {
            DataTable dtPayType = new DataTable();
            int cntPayType = 1;
            //if (ObjFunction.GetComboValue(cmbPaymentType) != 1)
            //{
            //    FillPayType();
            //}
            //FillPayType();
            long tempid = -1, ReceiptID = 0, VoucherUserNo = 0;

            for (int row = 1; row <= dgPayType.Rows.Count - 1; row++) //for (int row = 1; row <= 4; row++)//for (int j = 0; j < dgPayType.Rows.Count; j++)
            {
                if (row == 1 || row == 3 || row == 4 || row > 4)
                {
                    if (Convert.ToDouble(dgPayType.Rows[row].Cells[2].Value) != 0)
                    {
                        if (row == 1 || row == 3 || row == 4 || row > 4)
                            ReceiptID = ObjQry.ReturnLong("SELECT TVoucherDetails.FkVoucherNo FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo " +
                              " WHERE (TVoucherEntry.VoucherTypeCode = " + VchType.Receipt + ") AND (TVoucherEntry.VoucherDate ='" + dtpBillDate.Text + "') AND (TVoucherDetails.LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + ") AND " +
                                " (TVoucherDetails.VoucherSrNo = 1) AND (TVoucherEntry.PayTypeNo=" + Convert.ToInt64(dgPayType.Rows[row].Cells[1].Value) + ") ", CommonFunctions.ConStr);
                        //else if (row == 3 || row == 4)
                        //  ReceiptID = ObjQry.ReturnLong("SELECT FKReceiptVoucherNo FROM TVoucherPayTypeDetails Where FKSalesVoucherNo=" + SalesID + " AND FKPayTypeNo=" + dgPayType.Rows[row].Cells[1].Value + " ", CommonFunctions.ConStr);
                        if (CancelFlag == true)
                        {
                            dtVchPrev = ObjFunction.GetDataView("Select LedgerNo,Debit,CompanyNo From TVoucherDetails Where FkVoucherNo=" + 0 + " order by VoucherSrNo").Table;
                            setCompanyRatio();
                            FillPayType();
                        }
                        DataTable dtReceipt = ObjFunction.GetDataView("SELECT PkVoucherTrnNo,LedgerNo,Debit,Credit FROM TVoucherDetails " +
                            " WHERE (FkVoucherNo = " + ReceiptID + ") order by VoucherSrNo ").Table;
                        VoucherUserNo = ObjQry.ReturnLong("Select IsNull((VoucherUserNo),0) From TVoucherEntry Where PkVoucherNo=" + ReceiptID + "", CommonFunctions.ConStr);
                        long VoucherSrNo = ObjQry.ReturnLong("select IsNull(max(VoucherSrNo),0)+1 from TVoucherDetails where (FkVoucherNo = " + ReceiptID + ")", CommonFunctions.ConStr);
                        double PrevAmt = 0;
                        for (int i = 0; i < dtVchPrev.Rows.Count; i++)
                            PrevAmt += Convert.ToDouble(dtVchPrev.Rows[i].ItemArray[1].ToString());
                        PrevAmt = (ObjQry.ReturnDouble("Select BilledAmount From TVoucherEntry Where  PkVoucherNo=" + ReceiptID + "", CommonFunctions.ConStr) - PrevAmt) + ((CancelFlag == true) ? -Convert.ToDouble(txtGrandTotal.Text) : Convert.ToDouble(txtGrandTotal.Text));
                        //int VoucherSrNo = 1;
                        dbTVoucherEntry = new DBTVaucherEntry();
                        //Voucher Header Entry
                        tVoucherEntry = new TVoucherEntry();
                        tVoucherEntry.PkVoucherNo = ReceiptID;
                        tVoucherEntry.VoucherTypeCode = VchType.Receipt;
                        tVoucherEntry.VoucherUserNo = VoucherUserNo;
                        tVoucherEntry.VoucherDate = Convert.ToDateTime(dtpBillDate.Text);
                        tVoucherEntry.VoucherTime = dtpBillTime.Value;
                        tVoucherEntry.Narration = "Receipt Bill";
                        tVoucherEntry.Reference = "";
                        tVoucherEntry.ChequeNo = 0;
                        tVoucherEntry.ClearingDate = Convert.ToDateTime("01-Jan-1900");
                        tVoucherEntry.CompanyNo = DBGetVal.CompanyNo;
                        tVoucherEntry.BilledAmount = (ReceiptID != 0) ? PrevAmt : Convert.ToDouble(txtGrandTotal.Text);
                        tVoucherEntry.ChallanNo = "";
                        tVoucherEntry.Remark = txtRemark.Text.Trim();
                        tVoucherEntry.MacNo = DBGetVal.MacNo;
                        tVoucherEntry.PayTypeNo = ObjFunction.GetComboValue(cmbPaymentType);
                        tVoucherEntry.RateTypeNo = 0;
                        tVoucherEntry.TaxTypeNo = 0;
                        tVoucherEntry.OrderType = 2;


                        tVoucherEntry.UserID = DBGetVal.UserID;
                        tVoucherEntry.UserDate = DBGetVal.ServerTime.Date;
                        dbTVoucherEntry.AddTVoucherEntry(tVoucherEntry);

                        DataTable dtVoucherDetails = new DataTable();
                        if (ReceiptID != 0)
                        {
                            dtVoucherDetails = ObjFunction.GetDataView("Select PkVoucherTrnNo,LedgerNo,Debit,Credit From TVoucherDetails Where FkVoucherNo=" + ReceiptID + " order by VoucherSrNo").Table;
                            dtPayType = ObjFunction.GetDataView("Select PKVoucherPayTypeNo From TVoucherPayTypeDetails Where FKSalesVoucherNo=" + SalesID + " AND FKPayTypeNo=" + dgPayType.Rows[row].Cells[1].Value + "  order by PKVoucherPayTypeNo").Table;
                        }

                        dtVoucherDetails = ObjFunction.GetDataView("Select PkVoucherTrnNo,LedgerNo,Debit,Credit,VoucherSrNo From TVoucherDetails TD,TVoucherEntry TC Where TC.PKVoucherNo=TD.FKVoucherNo AND TC.PayTypeNo=" + ObjFunction.GetComboValue(cmbPaymentType) + " AND TD.LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " AND  TD.FkVoucherNo=" + ReceiptID + "  order by TD.VoucherSrNo").Table;

                        double totamt = 0, amt = 0;

                        if (Convert.ToDouble(dgPayType.Rows[row].Cells[2].Value) != 0)
                        {
                            amt = Convert.ToDouble(dgPayType.Rows[row].Cells[4 + (1)].Value);
                            totamt = totamt + amt;
                        }
                        amt = 0; if (CancelFlag == true) totamt = -totamt;
                        if (ID != 0 && PartyNo == ObjFunction.GetComboValue(cmbPartyName) && PayType == ObjFunction.GetComboValue(cmbPaymentType) && CancelFlag == false) amt = Convert.ToDouble(dtVchPrev.Select("CompanyNo=" + DBGetVal.CompanyNo + "")[0].ItemArray[1].ToString());//ObjQry.ReturnDouble("Select Sum(Debit) From TVoucherDetails Where LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " AND  CompanyNo=" + Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString()) + " ", CommonFunctions.ConStr);

                        //For Party Ledger
                        tVoucherDetails = new TVoucherDetails();
                        tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > 0) ? Convert.ToInt64(dtVoucherDetails.Rows[0].ItemArray[0].ToString()) : 0;
                        if (tVoucherDetails.PkVoucherTrnNo == 0)
                            tVoucherDetails.VoucherSrNo = VoucherSrNo;
                        else tVoucherDetails.VoucherSrNo = Convert.ToInt64(dtVoucherDetails.Rows[0].ItemArray[4].ToString());
                        tVoucherDetails.SignCode = 2;
                        tVoucherDetails.LedgerNo = ObjFunction.GetComboValue(cmbPartyName);
                        tVoucherDetails.Debit = 0;

                        double Newval = 0;
                        for (int k = 0; k < dgBill.Rows.Count - 1; k++)
                        {
                            if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.StockCompanyNo].Value) == DBGetVal.CompanyNo)//Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString())
                            {
                                if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.PkStockTrnNo].Value) == 0)
                                {
                                    Newval = Newval + Convert.ToDouble(dgBill.Rows[k].Cells[ColIndex.Amount].Value);
                                }
                            }
                        }
                        totamt = totamt - Newval;
                        if (ID == 0)
                            tVoucherDetails.Credit = ((dtVoucherDetails.Rows.Count > 0) ? Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[3].ToString()) : 0) + Convert.ToDouble(dgPayType.Rows[row].Cells[4 + (1)].Value);
                        else
                        {
                            if (dtVoucherDetails.Rows.Count > 0)
                                tVoucherDetails.Credit = Newval + ((Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[3].ToString()) - amt) + totamt);
                            else
                            {
                                if (PayType != 3)
                                    tVoucherDetails.Credit = totamt;
                                else tVoucherDetails.Credit = totamt + Newval;
                            }
                        }
                        if (tVoucherDetails.PkVoucherTrnNo == 0) VoucherSrNo += 1;
                        tVoucherDetails.Narration = "";
                        tVoucherDetails.SrNo = Others.Party;
                        dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);

                        totamt = 0;
                        if (Convert.ToDouble(dgPayType.Rows[row].Cells[2].Value) != 0)
                            totamt += Convert.ToDouble(dgPayType.Rows[row].Cells[4 + (1)].Value);

                        dtVoucherDetails = ObjFunction.GetDataView("Select PkVoucherTrnNo,LedgerNo,Debit,Credit,VoucherSrNo From TVoucherDetails Where LedgerNo=" + Convert.ToInt64(dtPayLedger.Select("PayTypeNo='" + Convert.ToInt64(dgPayType.Rows[row].Cells[1].Value) + "' AND CompanyNo=" + DBGetVal.CompanyNo)[0][2].ToString()) + " AND  FkVoucherNo=" + ReceiptID + "  order by VoucherSrNo").Table;

                        //For PayType Details
                        tVoucherDetails = new TVoucherDetails();
                        tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > 0) ? Convert.ToInt64(dtVoucherDetails.Rows[0].ItemArray[0].ToString()) : 0;
                        if (tVoucherDetails.PkVoucherTrnNo == 0)
                            tVoucherDetails.VoucherSrNo = VoucherSrNo;
                        else tVoucherDetails.VoucherSrNo = Convert.ToInt64(dtVoucherDetails.Rows[0].ItemArray[4].ToString());
                        tVoucherDetails.SignCode = 1;
                        tVoucherDetails.LedgerNo = Convert.ToInt64(dtPayLedger.Select("PayTypeNo=" + Convert.ToInt64(dgPayType.Rows[row].Cells[1].Value) + " AND CompanyNo=" + DBGetVal.CompanyNo)[0][2].ToString());// Convert.ToInt64(dgPayType.Rows[row].Cells[3].Value);//Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString())
                        Newval = 0;
                        for (int k = 0; k < dgBill.Rows.Count - 1; k++)
                        {
                            if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.StockCompanyNo].Value) == DBGetVal.CompanyNo)//Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString())
                            {
                                if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.PkStockTrnNo].Value) == 0)
                                {
                                    Newval = Newval + Convert.ToDouble(dgBill.Rows[k].Cells[ColIndex.Amount].Value);
                                }
                            }
                        }
                        totamt = totamt - Newval;
                        amt = 0; if (CancelFlag == true) totamt = -totamt;
                        if (ID != 0 && PartyNo == ObjFunction.GetComboValue(cmbPartyName) && PayType == ObjFunction.GetComboValue(cmbPaymentType) && CancelFlag == false) amt = Convert.ToDouble(dtVchPrev.Select("CompanyNo=" + DBGetVal.CompanyNo + "")[0].ItemArray[1].ToString());//ObjQry.ReturnDouble("Select Sum(Debit) From TVoucherDetails Where LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " AND  CompanyNo=" + Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString()) + " ", CommonFunctions.ConStr);

                        if (ID == 0)
                            tVoucherDetails.Debit = ((dtVoucherDetails.Rows.Count > 0) ? Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[2].ToString()) : 0) + Convert.ToDouble(dgPayType.Rows[row].Cells[4 + (1)].Value);
                        else
                        {
                            if (dtVoucherDetails.Rows.Count > 0)
                                tVoucherDetails.Debit = Newval + ((Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[2].ToString()) - amt) + totamt);
                            else
                            {
                                if (PayType != 3)
                                    tVoucherDetails.Debit = totamt;
                                else tVoucherDetails.Debit = totamt + Newval;
                            }
                        }

                        tVoucherDetails.Credit = 0;
                        if (tVoucherDetails.PkVoucherTrnNo == 0) VoucherSrNo += 1;
                        tVoucherDetails.Narration = "";
                        dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);

                        tVchPayTypeDetails = new TVoucherPayTypeDetails();
                        tVchPayTypeDetails.PKVoucherPayTypeNo = (dtPayType.Rows.Count > cntPayType - 1) ? Convert.ToInt64(dtPayType.Rows[cntPayType - 1].ItemArray[0].ToString()) : 0; cntPayType += 1;
                        tVchPayTypeDetails.FKSalesVoucherNo = SalesID;
                        tVchPayTypeDetails.FKPayTypeNo = Convert.ToInt64(dgPayType.Rows[row].Cells[1].Value);
                        tVchPayTypeDetails.Amount = Convert.ToDouble(dgPayType.Rows[row].Cells[4 + (1)].Value);
                        dbTVoucherEntry.AddTVoucherPayTypeDetails(tVchPayTypeDetails);

                        if ((PartyNo != ObjFunction.GetComboValue(cmbPartyName) || PayType != ObjFunction.GetComboValue(cmbPaymentType)) && ID != 0)
                        {
                            DataTable dtDelPayType = ObjFunction.GetDataView("Select PKVoucherPayTypeNo," + DBGetVal.CompanyNo + " As CompanyNo,FKReceiptVoucherNo,Amount From TVoucherPayTypeDetails,TVoucherDetails Where TVoucherDetails.PkVoucherTrnNo=TVoucherPayTypeDetails.FKVoucherTrnNo AND FKSalesVoucherNo=" + SalesID + "  order by PKVoucherPayTypeNo").Table;
                            for (int k = 0; k < dtDelPayType.Rows.Count; k++)
                            {
                                if (dtPayType.Rows.Count > 0)
                                {
                                    if (dtPayType.Select("PKVoucherPayTypeNo=" + dtDelPayType.Rows[k].ItemArray[0].ToString())[0][0].ToString() != dtDelPayType.Rows[k].ItemArray[0].ToString())
                                    {
                                        tVchPayTypeDetails = new TVoucherPayTypeDetails();
                                        tVchPayTypeDetails.PKVoucherPayTypeNo = Convert.ToInt64(dtDelPayType.Rows[k].ItemArray[0].ToString());
                                        dbTVoucherEntry.DeleteTVoucherPayTypeDetails(tVchPayTypeDetails);
                                    }
                                }
                                else
                                {
                                    tVchPayTypeDetails = new TVoucherPayTypeDetails();
                                    tVchPayTypeDetails.PKVoucherPayTypeNo = Convert.ToInt64(dtDelPayType.Rows[k].ItemArray[0].ToString());
                                    dbTVoucherEntry.DeleteTVoucherPayTypeDetails(tVchPayTypeDetails);
                                }
                                DataTable dtUpdateVoucher = ObjFunction.GetDataView("Select PKVoucherTrnNo,Debit,Credit From TVoucherDetails Where FKVoucherNo=" + dtDelPayType.Rows[k].ItemArray[2].ToString() + " ").Table;
                                totamt = 0;
                                bool alllowdel = false;
                                for (int m = 0; m < dtUpdateVoucher.Rows.Count; m++)
                                {
                                    double DrAmt = Convert.ToDouble(dtUpdateVoucher.Rows[m].ItemArray[1].ToString());
                                    double CrAmt = Convert.ToDouble(dtUpdateVoucher.Rows[m].ItemArray[2].ToString());
                                    if (DrAmt > 0) DrAmt = DrAmt - Convert.ToDouble(dtDelPayType.Rows[k].ItemArray[3].ToString());
                                    if (CrAmt > 0) CrAmt = CrAmt - Convert.ToDouble(dtDelPayType.Rows[k].ItemArray[3].ToString());
                                    dbTVoucherEntry.UpdateVoucherDetails(DrAmt, CrAmt, Convert.ToInt64(dtUpdateVoucher.Rows[m].ItemArray[0].ToString()));
                                    totamt = totamt + DrAmt + CrAmt;
                                    alllowdel = true;
                                }
                                if (totamt == 0 && alllowdel == true)
                                {
                                    for (int m = 0; m < dtUpdateVoucher.Rows.Count; m++)
                                    {
                                        tVoucherDetails = new TVoucherDetails();
                                        tVoucherDetails.PkVoucherTrnNo = Convert.ToInt64(dtUpdateVoucher.Rows[m].ItemArray[0].ToString());
                                        dbTVoucherEntry.DeleteTVoucherDetails(tVoucherDetails);

                                        if (m == dtUpdateVoucher.Rows.Count - 1)
                                        {
                                            if (ObjQry.ReturnLong("Select Count(*) From TVoucherDetails Where FKVoucherNo=" + Convert.ToInt64(dtDelPayType.Rows[k].ItemArray[2].ToString()) + "", CommonFunctions.ConStr) >= dtUpdateVoucher.Rows.Count)
                                            {
                                                tVoucherEntry = new TVoucherEntry();
                                                tVoucherEntry.PkVoucherNo = Convert.ToInt64(dtDelPayType.Rows[k].ItemArray[2].ToString());
                                                dbTVoucherEntry.DeleteTVoucherEntry1(tVoucherEntry);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        tempid = dbTVoucherEntry.ExecuteNonQueryStatements();

                    }
                }

            }
            CancelFlag = false;

            if (tempid != 0)
                return true;
            else
                return false;
        }

        private bool SaveReceiptNew(long SalesID)
        {
            DataTable dtPayType = new DataTable();
            int cntPayType = 1;
            //if (ObjFunction.GetComboValue(cmbPaymentType) != 1)
            //{
            //    FillPayType();
            //}
            //FillPayType();
            long tempid = -1, ReceiptID = 0, VoucherUserNo = 0;

            for (int row = 1; row <= dgPayType.Rows.Count - 1; row++) //for (int row = 1; row <= 4; row++)//for (int j = 0; j < dgPayType.Rows.Count; j++)
            {
                if (row == 1 || row == 3 || row == 4 || row > 4)
                {
                    if (Convert.ToDouble(dgPayType.Rows[row].Cells[2].Value) != 0)
                    {
                        if (row == 1 || row == 3 || row == 4 || row > 4)
                            ReceiptID = ObjQry.ReturnLong("SELECT TVoucherDetails.FkVoucherNo FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo " +
                              " WHERE (TVoucherEntry.VoucherTypeCode = " + VchType.Receipt + ") AND (TVoucherEntry.VoucherDate ='" + dtpBillDate.Text + "') AND (TVoucherDetails.LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + ") AND " +
                                " (TVoucherDetails.VoucherSrNo = 1) AND (TVoucherEntry.PayTypeNo=" + Convert.ToInt64(dgPayType.Rows[row].Cells[1].Value) + ") ", CommonFunctions.ConStr);
                        //else if (row == 3 || row == 4)
                        //  ReceiptID = ObjQry.ReturnLong("SELECT FKReceiptVoucherNo FROM TVoucherPayTypeDetails Where FKSalesVoucherNo=" + SalesID + " AND FKPayTypeNo=" + dgPayType.Rows[row].Cells[1].Value + " ", CommonFunctions.ConStr);
                        if (CancelFlag == true)
                        {
                            dtVchPrev = ObjFunction.GetDataView("Select LedgerNo,Debit,CompanyNo From TVoucherDetails Where FkVoucherNo=" + 0 + " order by VoucherSrNo").Table;
                            setCompanyRatio();
                            FillPayType();
                        }
                        DataTable dtReceipt = ObjFunction.GetDataView("SELECT PkVoucherTrnNo,LedgerNo,Debit,Credit FROM TVoucherDetails " +
                            " WHERE (FkVoucherNo = " + ReceiptID + ") order by VoucherSrNo ").Table;
                        VoucherUserNo = ObjQry.ReturnLong("Select IsNull((VoucherUserNo),0) From TVoucherEntry Where PkVoucherNo=" + ReceiptID + "", CommonFunctions.ConStr);
                        long VoucherSrNo = ObjQry.ReturnLong("select IsNull(max(VoucherSrNo),0)+1 from TVoucherDetails where (FkVoucherNo = " + ReceiptID + ")", CommonFunctions.ConStr);
                        double PrevAmt = 0;
                        for (int i = 0; i < dtVchPrev.Rows.Count; i++)
                            PrevAmt += Convert.ToDouble(dtVchPrev.Rows[i].ItemArray[1].ToString());
                        if (ReceiptID != 0)
                        {
                            if (tempDate.Date == dtpBillDate.Value.Date)
                                PrevAmt = (ObjQry.ReturnDouble("Select BilledAmount From TVoucherEntry Where  PkVoucherNo=" + ReceiptID + "", CommonFunctions.ConStr) - PrevAmt) + ((CancelFlag == true) ? -Convert.ToDouble(txtGrandTotal.Text) : Convert.ToDouble(txtGrandTotal.Text));
                            else
                                PrevAmt = (ObjQry.ReturnDouble("Select BilledAmount From TVoucherEntry Where  PkVoucherNo=" + ReceiptID + "", CommonFunctions.ConStr)) + ((CancelFlag == true) ? -Convert.ToDouble(txtGrandTotal.Text) : Convert.ToDouble(txtGrandTotal.Text));
                        }
                        else
                            PrevAmt = (ObjQry.ReturnDouble("Select BilledAmount From TVoucherEntry Where  PkVoucherNo=" + ReceiptID + "", CommonFunctions.ConStr)) + ((CancelFlag == true) ? -Convert.ToDouble(txtGrandTotal.Text) : Convert.ToDouble(txtGrandTotal.Text));
                        //int VoucherSrNo = 1;
                        dbTVoucherEntry = new DBTVaucherEntry();
                        //Voucher Header Entry
                        tVoucherEntry = new TVoucherEntry();
                        tVoucherEntry.PkVoucherNo = ReceiptID;
                        tVoucherEntry.VoucherTypeCode = VchType.Receipt;
                        tVoucherEntry.VoucherUserNo = VoucherUserNo;
                        tVoucherEntry.VoucherDate = Convert.ToDateTime(dtpBillDate.Text);
                        tVoucherEntry.VoucherTime = dtpBillTime.Value;
                        tVoucherEntry.Narration = "Receipt Bill";
                        tVoucherEntry.Reference = "";
                        tVoucherEntry.ChequeNo = 0;
                        tVoucherEntry.ClearingDate = Convert.ToDateTime("01-Jan-1900");
                        tVoucherEntry.CompanyNo = DBGetVal.CompanyNo;
                        tVoucherEntry.BilledAmount = PrevAmt;// Convert.ToDouble(txtGrandTotal.Text);
                        tVoucherEntry.ChallanNo = "";
                        tVoucherEntry.Remark = txtRemark.Text.Trim();
                        tVoucherEntry.MacNo = DBGetVal.MacNo;
                        tVoucherEntry.PayTypeNo = ObjFunction.GetComboValue(cmbPaymentType);
                        tVoucherEntry.RateTypeNo = 0;
                        tVoucherEntry.TaxTypeNo = 0;
                        tVoucherEntry.OrderType = 2;


                        tVoucherEntry.UserID = DBGetVal.UserID;
                        tVoucherEntry.UserDate = DBGetVal.ServerTime.Date;
                        dbTVoucherEntry.AddTVoucherEntry(tVoucherEntry);

                        DataTable dtVoucherDetails = new DataTable();
                        if (ReceiptID != 0)
                        {
                            dtVoucherDetails = ObjFunction.GetDataView("Select PkVoucherTrnNo,LedgerNo,Debit,Credit From TVoucherDetails Where FkVoucherNo=" + ReceiptID + " order by VoucherSrNo").Table;
                            dtPayType = ObjFunction.GetDataView("Select PKVoucherPayTypeNo From TVoucherPayTypeDetails Where FKSalesVoucherNo=" + SalesID + " AND FKPayTypeNo=" + dgPayType.Rows[row].Cells[1].Value + "  order by PKVoucherPayTypeNo").Table;
                        }

                        dtVoucherDetails = ObjFunction.GetDataView("Select PkVoucherTrnNo,LedgerNo,Debit,Credit,VoucherSrNo From TVoucherDetails TD,TVoucherEntry TC Where TC.PKVoucherNo=TD.FKVoucherNo AND TC.PayTypeNo=" + ObjFunction.GetComboValue(cmbPaymentType) + " AND TD.LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " AND  TD.FkVoucherNo=" + ReceiptID + "  order by TD.VoucherSrNo").Table;//Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString())

                        double totamt = 0, amt = 0;

                        if (Convert.ToDouble(dgPayType.Rows[row].Cells[2].Value) != 0)
                        {
                            amt = Convert.ToDouble(dgPayType.Rows[row].Cells[4 + (1)].Value);
                            totamt = totamt + amt;
                        }
                        amt = 0; if (CancelFlag == true) totamt = -totamt;
                        if (ID != 0 && PartyNo == ObjFunction.GetComboValue(cmbPartyName) && PayType == ObjFunction.GetComboValue(cmbPaymentType) && CancelFlag == false) amt = Convert.ToDouble(dtVchPrev.Select("CompanyNo=" + DBGetVal.CompanyNo + "")[0].ItemArray[1].ToString());//ObjQry.ReturnDouble("Select Sum(Debit) From TVoucherDetails Where LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " AND  CompanyNo=" + Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString()) + " ", CommonFunctions.ConStr);//Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString())

                        //For Party Ledger
                        tVoucherDetails = new TVoucherDetails();
                        tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > 0) ? Convert.ToInt64(dtVoucherDetails.Rows[0].ItemArray[0].ToString()) : 0;
                        if (tVoucherDetails.PkVoucherTrnNo == 0)
                            tVoucherDetails.VoucherSrNo = VoucherSrNo;
                        else tVoucherDetails.VoucherSrNo = Convert.ToInt64(dtVoucherDetails.Rows[0].ItemArray[4].ToString());
                        tVoucherDetails.SignCode = 2;
                        tVoucherDetails.LedgerNo = ObjFunction.GetComboValue(cmbPartyName);
                        tVoucherDetails.Debit = 0;

                        double Newval = 0;
                        for (int k = 0; k < dgBill.Rows.Count - 1; k++)
                        {
                            if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.StockCompanyNo].Value) == DBGetVal.CompanyNo)
                            {
                                if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.PkStockTrnNo].Value) == 0)
                                {
                                    Newval = Newval + Convert.ToDouble(dgBill.Rows[k].Cells[ColIndex.Amount].Value);
                                }
                            }
                        }
                        totamt = totamt - Newval;
                        if (ID == 0)
                            tVoucherDetails.Credit = ((dtVoucherDetails.Rows.Count > 0) ? Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[3].ToString()) : 0) + Convert.ToDouble(dgPayType.Rows[row].Cells[4 + (1)].Value);
                        else
                        {
                            if (dtVoucherDetails.Rows.Count > 0)
                                tVoucherDetails.Credit = Newval + ((Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[3].ToString())) + totamt);
                            else
                                tVoucherDetails.Credit = totamt;
                        }
                        if (tVoucherDetails.PkVoucherTrnNo == 0) VoucherSrNo += 1;
                        tVoucherDetails.Narration = "";
                        tVoucherDetails.SrNo = Others.Party;
                        dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);

                        totamt = 0;
                        if (Convert.ToDouble(dgPayType.Rows[row].Cells[2].Value) != 0)
                            totamt += Convert.ToDouble(dgPayType.Rows[row].Cells[4 + (1)].Value);

                        dtVoucherDetails = ObjFunction.GetDataView("Select PkVoucherTrnNo,LedgerNo,Debit,Credit,VoucherSrNo From TVoucherDetails Where LedgerNo=" + Convert.ToInt64(dtPayLedger.Select("PayTypeNo=" + Convert.ToInt64(dgPayType.Rows[row].Cells[1].Value) + " AND CompanyNo=" + DBGetVal.CompanyNo)[0][2].ToString()) + " AND  FkVoucherNo=" + ReceiptID + " order by VoucherSrNo").Table;

                        //For PayType Details
                        tVoucherDetails = new TVoucherDetails();
                        tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > 0) ? Convert.ToInt64(dtVoucherDetails.Rows[0].ItemArray[0].ToString()) : 0;
                        if (tVoucherDetails.PkVoucherTrnNo == 0)
                            tVoucherDetails.VoucherSrNo = VoucherSrNo;
                        else tVoucherDetails.VoucherSrNo = Convert.ToInt64(dtVoucherDetails.Rows[0].ItemArray[4].ToString());
                        tVoucherDetails.SignCode = 1;
                        tVoucherDetails.LedgerNo = Convert.ToInt64(dtPayLedger.Select("PayTypeNo=" + Convert.ToInt64(dgPayType.Rows[row].Cells[1].Value) + " AND CompanyNo=" + DBGetVal.CompanyNo)[0][2].ToString());// Convert.ToInt64(dgPayType.Rows[row].Cells[3].Value);//Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString())
                        Newval = 0;
                        for (int k = 0; k < dgBill.Rows.Count - 1; k++)
                        {
                            if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.StockCompanyNo].Value) == DBGetVal.CompanyNo)
                            {
                                if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.PkStockTrnNo].Value) == 0)
                                {
                                    Newval = Newval + Convert.ToDouble(dgBill.Rows[k].Cells[ColIndex.Amount].Value);
                                }
                            }
                        }
                        totamt = totamt - Newval;
                        amt = 0; if (CancelFlag == true) totamt = -totamt;
                        if (ID != 0 && PartyNo == ObjFunction.GetComboValue(cmbPartyName) && PayType == ObjFunction.GetComboValue(cmbPaymentType) && CancelFlag == false) amt = Convert.ToDouble(dtVchPrev.Select("CompanyNo=" + DBGetVal.CompanyNo + "")[0].ItemArray[1].ToString());//ObjQry.ReturnDouble("Select Sum(Debit) From TVoucherDetails Where LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " AND  CompanyNo=" + Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString()) + " ", CommonFunctions.ConStr);//Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString())

                        if (ID == 0)
                            tVoucherDetails.Debit = ((dtVoucherDetails.Rows.Count > 0) ? Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[2].ToString()) : 0) + Convert.ToDouble(dgPayType.Rows[row].Cells[4 + (1)].Value);
                        else
                        {
                            if (dtVoucherDetails.Rows.Count > 0)
                                tVoucherDetails.Debit = Newval + ((Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[2].ToString())) + totamt);
                            else
                                tVoucherDetails.Debit = totamt;
                        }

                        tVoucherDetails.Credit = 0;
                        if (tVoucherDetails.PkVoucherTrnNo == 0) VoucherSrNo += 1;
                        tVoucherDetails.Narration = "";
                        dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);

                        tVchPayTypeDetails = new TVoucherPayTypeDetails();
                        tVchPayTypeDetails.PKVoucherPayTypeNo = (dtPayType.Rows.Count > cntPayType - 1) ? Convert.ToInt64(dtPayType.Rows[cntPayType - 1].ItemArray[0].ToString()) : 0; cntPayType += 1;
                        tVchPayTypeDetails.FKSalesVoucherNo = SalesID;
                        tVchPayTypeDetails.FKPayTypeNo = Convert.ToInt64(dgPayType.Rows[row].Cells[1].Value);
                        tVchPayTypeDetails.Amount = Convert.ToDouble(dgPayType.Rows[row].Cells[4 + (1)].Value);
                        dbTVoucherEntry.AddTVoucherPayTypeDetails(tVchPayTypeDetails);

                        if ((PartyNo != ObjFunction.GetComboValue(cmbPartyName) || PayType != ObjFunction.GetComboValue(cmbPaymentType)) && ID != 0)
                        {
                            DataTable dtDelPayType = ObjFunction.GetDataView("Select PKVoucherPayTypeNo," + DBGetVal.CompanyNo + " As CompanyNo,FKReceiptVoucherNo,Amount From TVoucherPayTypeDetails,TVoucherDetails Where TVoucherDetails.PkVoucherTrnNo=TVoucherPayTypeDetails.FKVoucherTrnNo AND FKSalesVoucherNo=" + SalesID + "  order by PKVoucherPayTypeNo").Table;
                            for (int k = 0; k < dtDelPayType.Rows.Count; k++)
                            {
                                if (dtPayType.Rows.Count > 0)
                                {
                                    if (dtPayType.Select("PKVoucherPayTypeNo=" + dtDelPayType.Rows[k].ItemArray[0].ToString())[0][0].ToString() != dtDelPayType.Rows[k].ItemArray[0].ToString())
                                    {
                                        tVchPayTypeDetails = new TVoucherPayTypeDetails();
                                        tVchPayTypeDetails.PKVoucherPayTypeNo = Convert.ToInt64(dtDelPayType.Rows[k].ItemArray[0].ToString());
                                        dbTVoucherEntry.DeleteTVoucherPayTypeDetails(tVchPayTypeDetails);
                                    }
                                }
                                else
                                {
                                    tVchPayTypeDetails = new TVoucherPayTypeDetails();
                                    tVchPayTypeDetails.PKVoucherPayTypeNo = Convert.ToInt64(dtDelPayType.Rows[k].ItemArray[0].ToString());
                                    dbTVoucherEntry.DeleteTVoucherPayTypeDetails(tVchPayTypeDetails);
                                }
                                DataTable dtUpdateVoucher = ObjFunction.GetDataView("Select PKVoucherTrnNo,Debit,Credit From TVoucherDetails Where FKVoucherNo=" + dtDelPayType.Rows[k].ItemArray[2].ToString() + " ").Table;
                                totamt = 0;
                                bool alllowdel = false;
                                for (int m = 0; m < dtUpdateVoucher.Rows.Count; m++)
                                {
                                    double DrAmt = Convert.ToDouble(dtUpdateVoucher.Rows[m].ItemArray[1].ToString());
                                    double CrAmt = Convert.ToDouble(dtUpdateVoucher.Rows[m].ItemArray[2].ToString());
                                    if (DrAmt > 0) DrAmt = DrAmt - Convert.ToDouble(dtDelPayType.Rows[k].ItemArray[3].ToString());
                                    if (CrAmt > 0) CrAmt = CrAmt - Convert.ToDouble(dtDelPayType.Rows[k].ItemArray[3].ToString());
                                    dbTVoucherEntry.UpdateVoucherDetails(DrAmt, CrAmt, Convert.ToInt64(dtUpdateVoucher.Rows[m].ItemArray[0].ToString()));
                                    totamt = totamt + DrAmt + CrAmt;
                                    alllowdel = true;
                                }
                                if (totamt == 0 && alllowdel == true)
                                {
                                    for (int m = 0; m < dtUpdateVoucher.Rows.Count; m++)
                                    {
                                        tVoucherDetails = new TVoucherDetails();
                                        tVoucherDetails.PkVoucherTrnNo = Convert.ToInt64(dtUpdateVoucher.Rows[m].ItemArray[0].ToString());
                                        dbTVoucherEntry.DeleteTVoucherDetails(tVoucherDetails);

                                        if (m == dtUpdateVoucher.Rows.Count - 1)
                                        {
                                            if (ObjQry.ReturnLong("Select Count(*) From TVoucherDetails Where FKVoucherNo=" + Convert.ToInt64(dtDelPayType.Rows[k].ItemArray[2].ToString()) + "", CommonFunctions.ConStr) >= dtUpdateVoucher.Rows.Count)
                                            {
                                                tVoucherEntry = new TVoucherEntry();
                                                tVoucherEntry.PkVoucherNo = Convert.ToInt64(dtDelPayType.Rows[k].ItemArray[2].ToString());
                                                dbTVoucherEntry.DeleteTVoucherEntry1(tVoucherEntry);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        tempid = dbTVoucherEntry.ExecuteNonQueryStatements();

                    }
                }

            }
            CancelFlag = false;

            if (tempid != 0)
                return true;
            else
                return false;
        }

        private bool SaveReceiptOld(DateTime dt, long PartID)
        {
            //DataTable dtPayType = new DataTable();
            //int cntPayType = 1;
            //if (ObjFunction.GetComboValue(cmbPaymentType) != 1)
            //{
            //    FillPayType();
            //}
            //FillPayType();
            long tempid = -1, ReceiptID = 0, VoucherUserNo = 0;

            for (int row = 1; row <= dgPayType.Rows.Count - 1; row++) //for (int row = 1; row <= 4; row++)//for (int j = 0; j < dgPayType.Rows.Count; j++)
            {
                if (row == 1 || row == 3 || row == 4 || row > 4)
                {
                    if (Convert.ToDouble(dgPayType.Rows[row].Cells[2].Value) != 0)
                    {
                        if (row == 1 || row == 3 || row == 4 || row > 4)
                            ReceiptID = ObjQry.ReturnLong("SELECT TVoucherDetails.FkVoucherNo FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo " +
                              " WHERE (TVoucherEntry.VoucherTypeCode = " + VchType.Receipt + ") AND (TVoucherEntry.VoucherDate ='" + dt.ToString(Format.DDMMMYYYY) + "') AND (TVoucherDetails.LedgerNo = " + PartID + ") AND " +
                                " (TVoucherDetails.VoucherSrNo = 1) AND (TVoucherEntry.PayTypeNo=" + Convert.ToInt64(dgPayType.Rows[row].Cells[1].Value) + ") ", CommonFunctions.ConStr);
                        //else if (row == 3 || row == 4)
                        //  ReceiptID = ObjQry.ReturnLong("SELECT FKReceiptVoucherNo FROM TVoucherPayTypeDetails Where FKSalesVoucherNo=" + SalesID + " AND FKPayTypeNo=" + dgPayType.Rows[row].Cells[1].Value + " ", CommonFunctions.ConStr);
                        if (CancelFlag == true)
                        {
                            dtVchPrev = ObjFunction.GetDataView("Select LedgerNo,Debit,CompanyNo From TVoucherDetails Where FkVoucherNo=" + 0 + " order by VoucherSrNo").Table;
                            setCompanyRatio();
                            FillPayType();
                        }
                        DataTable dtReceipt = ObjFunction.GetDataView("SELECT PkVoucherTrnNo,LedgerNo,Debit,Credit FROM TVoucherDetails " +
                            " WHERE (FkVoucherNo = " + ReceiptID + ") order by VoucherSrNo ").Table;
                        VoucherUserNo = ObjQry.ReturnLong("Select IsNull((VoucherUserNo),0) From TVoucherEntry Where PkVoucherNo=" + ReceiptID + "", CommonFunctions.ConStr);
                        long VoucherSrNo = ObjQry.ReturnLong("select IsNull(max(VoucherSrNo),0)+1 from TVoucherDetails where (FkVoucherNo = " + ReceiptID + ")", CommonFunctions.ConStr);
                        double PrevAmt = 0;
                        for (int i = 0; i < dtVchPrev.Rows.Count; i++)
                            PrevAmt += Convert.ToDouble(dtVchPrev.Rows[i].ItemArray[1].ToString());
                        PrevAmt = (ObjQry.ReturnDouble("Select BilledAmount From TVoucherEntry Where  PkVoucherNo=" + ReceiptID + "", CommonFunctions.ConStr) - PrevAmt);// +((CancelFlag == true) ? -Convert.ToDouble(txtGrandTotal.Text) : Convert.ToDouble(txtGrandTotal.Text));
                        if (PrevAmt < 0) break;
                        //int VoucherSrNo = 1;
                        dbTVoucherEntry = new DBTVaucherEntry();
                        //Voucher Header Entry
                        tVoucherEntry = new TVoucherEntry();
                        tVoucherEntry.PkVoucherNo = ReceiptID;
                        tVoucherEntry.VoucherTypeCode = VchType.Receipt;
                        tVoucherEntry.VoucherUserNo = VoucherUserNo;
                        tVoucherEntry.VoucherDate = Convert.ToDateTime(dt.ToString("dd-MMM-yyyy"));
                        tVoucherEntry.VoucherTime = dtpBillTime.Value;
                        tVoucherEntry.Narration = "Receipt Bill";
                        tVoucherEntry.Reference = "";
                        tVoucherEntry.ChequeNo = 0;
                        tVoucherEntry.ClearingDate = Convert.ToDateTime("01-Jan-1900");
                        tVoucherEntry.CompanyNo = DBGetVal.CompanyNo;
                        tVoucherEntry.BilledAmount = PrevAmt;// Convert.ToDouble(txtGrandTotal.Text);
                        tVoucherEntry.ChallanNo = "";
                        tVoucherEntry.Remark = txtRemark.Text.Trim();
                        tVoucherEntry.MacNo = DBGetVal.MacNo;
                        tVoucherEntry.PayTypeNo = ObjFunction.GetComboValue(cmbPaymentType);
                        tVoucherEntry.RateTypeNo = 0;
                        tVoucherEntry.TaxTypeNo = 0;
                        tVoucherEntry.OrderType = 2;


                        tVoucherEntry.UserID = DBGetVal.UserID;
                        tVoucherEntry.UserDate = DBGetVal.ServerTime.Date;
                        dbTVoucherEntry.AddTVoucherEntry(tVoucherEntry);

                        DataTable dtVoucherDetails = new DataTable();
                        if (ReceiptID != 0)
                        {
                            dtVoucherDetails = ObjFunction.GetDataView("Select PkVoucherTrnNo,LedgerNo,Debit,Credit From TVoucherDetails Where FkVoucherNo=" + ReceiptID + " order by VoucherSrNo").Table;
                        }

                        dtVoucherDetails = ObjFunction.GetDataView("Select PkVoucherTrnNo,LedgerNo,Debit,Credit,VoucherSrNo From TVoucherDetails TD,TVoucherEntry TC Where TC.PKVoucherNo=TD.FKVoucherNo AND TC.PayTypeNo=" + ObjFunction.GetComboValue(cmbPaymentType) + " AND TD.LedgerNo=" + PartID + " AND  TD.FkVoucherNo=" + ReceiptID + "  order by TD.VoucherSrNo").Table;//Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString())

                        double totamt = 0, amt = 0;
                        if (Convert.ToDouble(dgPayType.Rows[row].Cells[2].Value) != 0)
                        {
                            amt = Convert.ToDouble(dgPayType.Rows[row].Cells[4 + (1)].Value);
                            totamt = totamt + amt;
                        }
                        amt = 0; if (CancelFlag == true) totamt = -totamt;
                        if (ID != 0 && PartyNo == PartID && PayType == ObjFunction.GetComboValue(cmbPaymentType) && CancelFlag == false) amt = Convert.ToDouble(dtVchPrev.Select("CompanyNo=" + DBGetVal.CompanyNo + "")[0].ItemArray[1].ToString());

                        //For Party Ledger
                        tVoucherDetails = new TVoucherDetails();
                        tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > 0) ? Convert.ToInt64(dtVoucherDetails.Rows[0].ItemArray[0].ToString()) : 0;
                        if (tVoucherDetails.PkVoucherTrnNo == 0)
                            tVoucherDetails.VoucherSrNo = VoucherSrNo;
                        else tVoucherDetails.VoucherSrNo = Convert.ToInt64(dtVoucherDetails.Rows[0].ItemArray[4].ToString());
                        tVoucherDetails.SignCode = 2;
                        tVoucherDetails.LedgerNo = PartID;
                        tVoucherDetails.Debit = 0;

                        double Newval = 0;
                        for (int k = 0; k < dgBill.Rows.Count - 1; k++)
                        {
                            if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.StockCompanyNo].Value) == DBGetVal.CompanyNo)
                            {
                                if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.PkStockTrnNo].Value) == 0)
                                {
                                    Newval = Newval + Convert.ToDouble(dgBill.Rows[k].Cells[ColIndex.Amount].Value);
                                }
                            }
                        }
                        totamt = totamt - Newval;
                        if (ID == 0)
                            tVoucherDetails.Credit = ((dtVoucherDetails.Rows.Count > 0) ? Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[3].ToString()) : 0) + Convert.ToDouble(dgPayType.Rows[row].Cells[4 + (1)].Value);
                        else
                        {
                            if (dtVoucherDetails.Rows.Count > 0)
                            {
                                if (PartID != ObjFunction.GetComboValue(cmbPartyName))
                                    tVoucherDetails.Credit = ((Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[3].ToString())));
                                else
                                    tVoucherDetails.Credit = ((Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[3].ToString()) - amt));
                            }
                            else
                                tVoucherDetails.Credit = totamt;
                        }
                        if (tVoucherDetails.PkVoucherTrnNo == 0) VoucherSrNo += 1;
                        tVoucherDetails.Narration = "";
                        tVoucherDetails.SrNo = Others.Party;
                        dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);

                        totamt = 0;
                        if (Convert.ToDouble(dgPayType.Rows[row].Cells[2].Value) != 0)
                            totamt += Convert.ToDouble(dgPayType.Rows[row].Cells[4 + (1)].Value);

                        dtVoucherDetails = ObjFunction.GetDataView("Select PkVoucherTrnNo,LedgerNo,Debit,Credit,VoucherSrNo From TVoucherDetails Where LedgerNo=" + Convert.ToInt64(dtPayLedger.Select("PayTypeNo=" + Convert.ToInt64(dgPayType.Rows[row].Cells[1].Value) + " AND CompanyNo=" + DBGetVal.CompanyNo)[0][2].ToString()) + " AND  FkVoucherNo=" + ReceiptID + "  order by VoucherSrNo").Table;

                        //For PayType Details
                        tVoucherDetails = new TVoucherDetails();
                        tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > 0) ? Convert.ToInt64(dtVoucherDetails.Rows[0].ItemArray[0].ToString()) : 0;
                        if (tVoucherDetails.PkVoucherTrnNo == 0)
                            tVoucherDetails.VoucherSrNo = VoucherSrNo;
                        else tVoucherDetails.VoucherSrNo = Convert.ToInt64(dtVoucherDetails.Rows[0].ItemArray[4].ToString());
                        tVoucherDetails.SignCode = 1;
                        tVoucherDetails.LedgerNo = Convert.ToInt64(dtPayLedger.Select("PayTypeNo=" + Convert.ToInt64(dgPayType.Rows[row].Cells[1].Value) + " AND CompanyNo=" + DBGetVal.CompanyNo)[0][2].ToString());// Convert.ToInt64(dgPayType.Rows[row].Cells[3].Value);
                        Newval = 0;
                        for (int k = 0; k < dgBill.Rows.Count - 1; k++)
                        {
                            if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.StockCompanyNo].Value) == DBGetVal.CompanyNo)//Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString())
                            {
                                if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.PkStockTrnNo].Value) == 0)
                                {
                                    Newval = Newval + Convert.ToDouble(dgBill.Rows[k].Cells[ColIndex.Amount].Value);
                                }
                            }
                        }
                        totamt = totamt - Newval;
                        amt = 0; if (CancelFlag == true) totamt = -totamt;
                        if (ID != 0 && PartyNo == PartID && PayType == ObjFunction.GetComboValue(cmbPaymentType) && CancelFlag == false) amt = Convert.ToDouble(dtVchPrev.Select("CompanyNo=" + DBGetVal.CompanyNo + "")[0].ItemArray[1].ToString());//ObjQry.ReturnDouble("Select Sum(Debit) From TVoucherDetails Where LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " AND  CompanyNo=" + Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString()) + " ", CommonFunctions.ConStr);

                        if (ID == 0)
                            tVoucherDetails.Debit = ((dtVoucherDetails.Rows.Count > 0) ? Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[2].ToString()) : 0) + Convert.ToDouble(dgPayType.Rows[row].Cells[4 + (1)].Value);
                        else
                        {
                            if (dtVoucherDetails.Rows.Count > 0)
                            {
                                if (PartID != ObjFunction.GetComboValue(cmbPartyName))
                                    tVoucherDetails.Debit = ((Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[2].ToString())));
                                else
                                    tVoucherDetails.Debit = ((Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[2].ToString()) - amt));
                            }
                            else
                                tVoucherDetails.Debit = totamt;
                        }

                        tVoucherDetails.Credit = 0;
                        if (tVoucherDetails.PkVoucherTrnNo == 0) VoucherSrNo += 1;
                        tVoucherDetails.Narration = "";
                        dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                        tempid = dbTVoucherEntry.ExecuteNonQueryStatements();

                    }
                }

            }
            CancelFlag = false;

            if (tempid != 0)
                return true;
            else
                return false;
        }


        #endregion

        #region Datagrid ItemList Methods

        private void dgItemList_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    long i = Convert.ToInt64(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[0].Value);
                    int rwindex = 0;
                    int pos = -1;
                    for (int s = 0; s < dgBill.RowCount - 1; s++)
                    {
                        if (Convert.ToInt64(dgBill.Rows[s].Cells[ColIndex.ItemNo].Value) == Convert.ToInt64(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[0].Value)
                            && Convert.ToInt64(dgBill.Rows[s].Cells[ColIndex.PkRateSettingNo].Value) == Convert.ToInt64(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[12].Value))
                        {
                            pos = s;
                            break;
                        }
                    }
                    if (pos != -1)
                    {
                        int totalRows = dgBill.Rows.Count;
                        DataGridViewRow selectedRow = dgBill.Rows[pos];
                        dgBill.Rows.Remove(selectedRow);
                        dgBill.Rows.Insert(dgBill.Rows.Count - 1, selectedRow);
                    }
                    if (ItemExist(i, Convert.ToInt64(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[12].Value), out rwindex) == true)
                    {
                        pnlItemName.Visible = false;
                        dgBill.Rows[rwindex].Cells[ColIndex.Quantity].Value = Convert.ToDouble(dgBill.Rows[rwindex].Cells[ColIndex.Quantity].Value) + 1;
                        if (rwindex != dgBill.CurrentRow.Index) dgBill.CurrentRow.Cells[ColIndex.ItemName].Value = "";
                        dgBill.CurrentCell = dgBill[ColIndex.Quantity, rwindex];
                        dgBill_CellValueChanged(dgBill, new DataGridViewCellEventArgs(ColIndex.Quantity, rwindex));
                        dgBill_KeyDown(dgBill, new KeyEventArgs(Keys.Enter));
                    }
                    else
                    {
                        dgBill.CurrentRow.Cells[ColIndex.UOMNo].Value = Convert.ToInt64(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[13].Value);
                        dgBill.CurrentRow.Cells[ColIndex.Rate].Value = Convert.ToDouble(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[3].Value).ToString("0.00");//lstRate.Text;
                        dgBill.CurrentRow.Cells[ColIndex.PkRateSettingNo].Value = Convert.ToInt64(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[12].Value);//lstRate.SelectedValue;
                        dgBill.CurrentRow.Cells[ColIndex.UOM].Value = dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[4].Value;
                        dgBill.CurrentRow.Cells[ColIndex.GodownNo].Value = (ObjFunction.GetAppSettings(AppSettings.S_DefaultStockLocation).ToString() == "0") ? dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[15].FormattedValue.ToString() : ObjFunction.GetAppSettings(AppSettings.S_DefaultStockLocation).ToString();
                        dgBill.CurrentRow.Cells[ColIndex.DiscountType].Value = dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[16].Value;
                        dgBill.CurrentRow.Cells[ColIndex.HamaliInKg].Value = dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[17].Value;

                        pnlItemName.Visible = false;
                        Desc_MoveNext(Convert.ToInt64(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[0].Value), 0);
                    }
                }
                else if (e.KeyCode == Keys.Space)
                {
                    pnlItemName.Visible = false;
                    txtBrandFilter.Text = "";
                    if (strItemQuery.Length > 1)
                    {
                        pnlGroup1.Visible = true;
                        txtBrandFilter.Focus();
                    }
                    else
                    {
                        dgBill.Focus();
                    }
                }
                else if (e.KeyCode == Keys.F6)
                {

                    pnlSalePurHistory.Visible = !pnlSalePurHistory.Visible;
                    if (pnlSalePurHistory.Visible == true)
                    {

                        pnlSalePurHistory.Location = new Point(88, 235 + 88);
                        pnlSalePurHistory.Size = new Size(692, 287);
                        DataSet ds = ObjDset.FillDset("New", "Exec GetLastSalePurchaseDetails " + ObjFunction.GetComboValue(cmbPartyName) + "," + dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[0].Value.ToString() + " ", CommonFunctions.ConStr);

                        if (ds.Tables.Count > 0)
                        {
                            dgSaleHistory.DataSource = ds.Tables[0].DefaultView;
                            dgPurHistory.DataSource = ds.Tables[1].DefaultView;

                            if (dgSaleHistory.Rows.Count > 0)
                            {
                                dgSaleHistory.Location = new Point(8, 8);
                                dgSaleHistory.Size = new Size(323, 150);
                                dgSaleHistory.Columns[0].Width = 70;
                                dgSaleHistory.Columns[1].Width = 45;
                                dgSaleHistory.Columns[2].Width = 70; dgSaleHistory.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                                dgSaleHistory.Columns[3].Width = 50; dgSaleHistory.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                                dgSaleHistory.Columns[4].Width = 85;
                            }

                            if (dgPurHistory.Rows.Count > 0)
                            {
                                dgPurHistory.Location = new Point(337, 8);
                                dgPurHistory.Size = new Size(339, 150);
                                dgPurHistory.Columns[0].Width = 70;
                                dgPurHistory.Columns[1].Width = 45;
                                dgPurHistory.Columns[2].Width = 70; dgPurHistory.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                                dgPurHistory.Columns[3].Width = 50; dgPurHistory.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                                dgPurHistory.Columns[4].Width = 85;
                            }

                        }
                        else
                        {
                            dgSaleHistory.Rows.Clear();
                            dgPurHistory.Rows.Clear();
                        }
                    }
                }
                else if (e.KeyCode == Keys.F3)
                {
                    dgItemList.Columns[14].Visible = !dgItemList.Columns[14].Visible;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgItemList_CurrentCellChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgItemList.CurrentCell != null)
                {
                    for (int i = 0; i < dgItemList.Rows.Count; i++)
                    {
                        dgItemList.Rows[i].DefaultCellStyle.BackColor = Color.White;
                    }
                    dgItemList.Rows[dgItemList.CurrentCell.RowIndex].DefaultCellStyle.BackColor = clrColorRow;
                    dgItemList.CurrentCell.Style.SelectionBackColor = Color.Yellow;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }
        #endregion

        #region Rate Type Realted Methods and Functions
        private void FillRateType()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("RateType");
            dt.Columns.Add("RateTypeName");
            DataRow dr = null;

            //dr = dt.NewRow();
            //dr[0] = "----Select----";
            //dr[1] = "0";
            //dt.Rows.Add(dr);

            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ARateIsActive)) == true)
            {
                dr = dt.NewRow();
                dr[1] = ObjFunction.GetAppSettings(AppSettings.ARateLabel);
                dr[0] = "ASaleRate";
                dt.Rows.Add(dr);
            }

            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.BRateIsActive)) == true)
            {
                dr = dt.NewRow();
                dr[1] = ObjFunction.GetAppSettings(AppSettings.BRateLabel);
                dr[0] = "BSaleRate";
                dt.Rows.Add(dr);
            }

            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.CRateIsActive)) == true)
            {
                dr = dt.NewRow();
                dr[1] = ObjFunction.GetAppSettings(AppSettings.CRateLabel);
                dr[0] = "CSaleRate";
                dt.Rows.Add(dr);
            }

            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.DRateIsActive)) == true)
            {
                dr = dt.NewRow();
                dr[1] = ObjFunction.GetAppSettings(AppSettings.DRateLabel);
                dr[0] = "DSaleRate";
                dt.Rows.Add(dr);
            }

            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ERateIsActive)) == true)
            {
                dr = dt.NewRow();
                dr[1] = ObjFunction.GetAppSettings(AppSettings.ERateLabel);
                dr[0] = "ESaleRate";
                dt.Rows.Add(dr);
            }

            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_BillWithMRP)) == true)
            {
                dr = dt.NewRow();
                dr[1] = "MRP";
                dr[0] = "MRP";
                dt.Rows.Add(dr);
            }

            cmbRateType.DataSource = dt.DefaultView;
            cmbRateType.DisplayMember = dt.Columns[1].ColumnName;
            cmbRateType.ValueMember = dt.Columns[0].ColumnName;
            SetRateType(Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_RateType).ToString()));
        }

        private void FillRateType(long RateTypeNo)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("RateType");
            dt.Columns.Add("RateTypeName");
            DataRow dr = null;

            //dr = dt.NewRow();
            //dr[0] = "----Select----";
            //dr[1] = "0";
            //dt.Rows.Add(dr);

            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ARateIsActive)) == true || RateTypeNo == 1)
            {
                dr = dt.NewRow();
                dr[1] = ObjFunction.GetAppSettings(AppSettings.ARateLabel);
                dr[0] = "ASaleRate";
                dt.Rows.Add(dr);
            }

            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.BRateIsActive)) == true || RateTypeNo == 2)
            {
                dr = dt.NewRow();
                dr[1] = ObjFunction.GetAppSettings(AppSettings.BRateLabel);
                dr[0] = "BSaleRate";
                dt.Rows.Add(dr);
            }

            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.CRateIsActive)) == true || RateTypeNo == 3)
            {
                dr = dt.NewRow();
                dr[1] = ObjFunction.GetAppSettings(AppSettings.CRateLabel);
                dr[0] = "CSaleRate";
                dt.Rows.Add(dr);
            }

            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.DRateIsActive)) == true || RateTypeNo == 4)
            {
                dr = dt.NewRow();
                dr[1] = ObjFunction.GetAppSettings(AppSettings.DRateLabel);
                dr[0] = "DSaleRate";
                dt.Rows.Add(dr);
            }

            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ERateIsActive)) == true || RateTypeNo == 5)
            {
                dr = dt.NewRow();
                dr[1] = ObjFunction.GetAppSettings(AppSettings.ERateLabel);
                dr[0] = "ESaleRate";
                dt.Rows.Add(dr);
            }

            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_BillWithMRP)) == true || RateTypeNo == 6)
            {
                dr = dt.NewRow();
                dr[1] = "MRP";
                dr[0] = "MRP";
                dt.Rows.Add(dr);
            }

            cmbRateType.DataSource = dt.DefaultView;
            cmbRateType.DisplayMember = dt.Columns[1].ColumnName;
            cmbRateType.ValueMember = dt.Columns[0].ColumnName;
            SetRateType(Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_RateType).ToString()));
        }

        private int GetRateType()
        {
            string str = ObjFunction.GetComboValueString(cmbRateType);
            int val = 0;
            if (str == "ASaleRate") val = 1;
            else if (str == "BSaleRate") val = 2;
            else if (str == "CSaleRate") val = 3;
            else if (str == "DSaleRate") val = 4;
            else if (str == "ESaleRate") val = 5;
            else if (str == "MRP") val = 6;
            return val;
        }

        private void SetRateType(long RateTypeNo)
        {
            if (RateTypeNo == 1) cmbRateType.SelectedValue = "ASaleRate";
            else if (RateTypeNo == 2) cmbRateType.SelectedValue = "BSaleRate";
            else if (RateTypeNo == 3) cmbRateType.SelectedValue = "CSaleRate";
            else if (RateTypeNo == 4) cmbRateType.SelectedValue = "DSaleRate";
            else if (RateTypeNo == 5) cmbRateType.SelectedValue = "ESaleRate";
            else if (RateTypeNo == 6) cmbRateType.SelectedValue = "MRP";
        }
        #endregion

        public string[] GetReportSession()
        {
            string AddressPrint = (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsAddressInBill)) == true) ? "0" : "1";
            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsAddressInBill)) == true)
            {
                if (OrderType == 1)
                {
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsAddressInBillCouterBill)) == true)
                        AddressPrint = "0";
                    else
                        AddressPrint = "1";
                }
                if (OrderType == 2)
                {
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsAddressInBillHomeDelivery)) == true)
                        AddressPrint = "0";
                    else
                        AddressPrint = "1";
                }
            }
            double Amt = 0;
            Amt = ObjQry.ReturnDouble("Select sum(Case When TStock.Rate <>0 Then (((TStock.Amount+TStock.DiscRupees) * CASE WHEN (MRateSetting.MRP = 0) THEN TStock.Rate ELSE MRateSetting.MRP END/TStock.Rate)-TStock.Amount) Else MRateSetting.MRP*TStock.Quantity END) FROM TStock INNER JOIN MRateSetting ON TStock.FkRateSettingNo = MRateSetting.PkSrNo " +
                  " Where TStock.FkVoucherNo=" + ID + " AND (Case When TStock.Rate <>0 Then ((TStock.Amount+TStock.DiscRupees) * CASE WHEN (MRateSetting.MRP = 0) THEN TStock.Rate ELSE MRateSetting.MRP END/TStock.Rate)-TStock.Amount Else MRateSetting.MRP*TStock.Quantity END)>0 ", CommonFunctions.ConStr);
            Amt += Convert.ToDouble(txtTotalAnotherDisc.Text);
            string[] ReportSession;

            ReportSession = new string[20];
            ReportSession[0] = ID.ToString();
            ReportSession[1] = "";//ObjQry.ReturnLong("Select Max(PkVoucherNo) FRom TVoucherEntry Where VoucherTypeCode=" + ((flagPP == true) ? VchType.Sales : VoucherType) + "", CommonFunctions.ConStr).ToString();
            ReportSession[2] = ObjFunction.GetAppSettings(AppSettings.S_SettingValue).ToString();
            ReportSession[3] = ObjFunction.GetAppSettings(AppSettings.S_FooterValue).ToString();
            ReportSession[4] = Amt.ToString("0.00");
            ReportSession[5] = ObjFunction.GetAppSettings(AppSettings.S_Footer2Value).ToString();
            ReportSession[6] = "" + ((MixModeVal == 1) ? "Mix Mode" : cmbPaymentType.Text);
            ReportSession[7] = (txtTotalAnotherDisc.Text == "") ? "0" : txtTotalAnotherDisc.Text;
            ReportSession[8] = (txtTotalChrgs.Text == "") ? "0" : txtTotalChrgs.Text;
            ReportSession[9] = (rbEnglish.Checked == true) ? "1" : "2";
            ReportSession[10] = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_ShowSavingBill)).ToString();
            ReportSession[11] = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_ShowOutStanding)).ToString();
            ReportSession[13] = (MixModeVal == 1) ? "1" : "0";
            ReportSession[14] = AddressPrint;//(Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsAddressInBill)) == true) ? "0" : "1";
            ReportSession[15] = (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsBillRoundOff)) == true) ? "1" : "2";
            ReportSession[16] = "0";
            ReportSession[17] = txtOtherDisc.Text;
            ReportSession[18] = NumberToWordsIndian.getWords(lblGrandTotal.Text);
            ReportSession[19] = ObjFunction.GetComboValue(cmbTaxType).ToString();

            #region New Code for Outstanding
            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_ShowOutStanding)) == true)
            {
                double TotalDues = 0;

                TotalDues = ObjQry.ReturnDouble("Select Min(case when MLedger.signcode = 1 then -OpeningBalance else OpeningBalance end) " +
                            " - SUM(Debit) + SUM(Credit) " +
                            " FROM TVoucherDetails INNER JOIN " +
                            " TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN " +
                            " MLedger ON TVoucherDetails.LedgerNo = MLedger.LedgerNo " +
                            " Where TVoucherDetails.LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) +
                            " AND TVoucherEntry.IsCancel = 'false'", CommonFunctions.ConStr);
                TotalDues = -1 * TotalDues;
                if (TotalDues > 0)
                {
                    ReportSession[12] = "Total Dues:" + TotalDues.ToString("0.00");
                }
                else if (TotalDues <= 0)
                    ReportSession[12] = "Total Dues: 0";
            }
            else
                ReportSession[12] = "0";

            #endregion

            return ReportSession;
        }

        public static class ColPrintIndex
        {
            public static int PrinterNo = 0;
            public static int PrinterName = 3;
            public static int MachineName = 2;
            public static int MachineIP = 1;
            public static int IsDefault = 4;
            public static int IsActive = 5;
            public static int PrintType = 6;
            public static int IsLangType = 7;

        }

        private void PrintBill()
        {
            try
            {
                string AddressPrint = (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsAddressInBill)) == true) ? "0" : "1";
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsAddressInBill)) == true)
                {
                    if (OrderType == 1)
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsAddressInBillCouterBill)) == true)
                            AddressPrint = "0";
                        else
                            AddressPrint = "1";
                    }
                    if (OrderType == 2)
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsAddressInBillHomeDelivery)) == true)
                            AddressPrint = "0";
                        else
                            AddressPrint = "1";
                    }
                }
                double Amt = 0;
                Amt = ObjQry.ReturnDouble("Select sum(Case When TStock.Rate <>0 Then (((TStock.Amount+TStock.DiscRupees) * CASE WHEN (MRateSetting.MRP = 0) THEN TStock.Rate ELSE MRateSetting.MRP END/TStock.Rate)-TStock.Amount) Else MRateSetting.MRP*TStock.Quantity END) FROM TStock INNER JOIN MRateSetting ON TStock.FkRateSettingNo = MRateSetting.PkSrNo " +
                      " Where TStock.FkVoucherNo=" + ID + " AND (Case When TStock.Rate <>0 Then ((TStock.Amount+TStock.DiscRupees) * CASE WHEN (MRateSetting.MRP = 0) THEN TStock.Rate ELSE MRateSetting.MRP END/TStock.Rate)-TStock.Amount Else MRateSetting.MRP*TStock.Quantity END)>0 ", CommonFunctions.ConStr);
                Amt += Convert.ToDouble(txtTotalAnotherDisc.Text);
                string[] ReportSession;

                ReportSession = new string[19];
                ReportSession[0] = ID.ToString();
                ReportSession[1] = "";//ObjQry.ReturnLong("Select Max(PkVoucherNo) FRom TVoucherEntry Where VoucherTypeCode=" + ((flagPP == true) ? VchType.Sales : VoucherType) + "", CommonFunctions.ConStr).ToString();
                ReportSession[2] = ObjFunction.GetAppSettings(AppSettings.S_SettingValue).ToString();
                ReportSession[3] = ObjFunction.GetAppSettings(AppSettings.S_FooterValue).ToString();
                ReportSession[4] = Amt.ToString("0.00");
                ReportSession[5] = ObjFunction.GetAppSettings(AppSettings.S_Footer2Value).ToString();
                ReportSession[6] = "Type: " + ((MixModeVal == 1) ? "Mix Mode" : cmbPaymentType.Text);
                ReportSession[7] = (txtTotalAnotherDisc.Text == "") ? "0" : txtTotalAnotherDisc.Text;
                ReportSession[8] = (txtTotalChrgs.Text == "") ? "0" : txtTotalChrgs.Text;
                ReportSession[9] = (rbEnglish.Checked == true) ? "1" : "2";
                ReportSession[10] = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_ShowSavingBill)).ToString();
                ReportSession[11] = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_ShowOutStanding)).ToString();

                ReportSession[13] = (MixModeVal == 1) ? "1" : "0";


                ReportSession[14] = AddressPrint;//(Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsAddressInBill)) == true) ? "0" : "1";
                ReportSession[15] = (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsBillRoundOff)) == true) ? "1" : "2";
                ReportSession[16] = "0";
                ReportSession[17] = "0";
                ReportSession[18] = "0";

                #region Old Code for Outstanding
                /*
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_ShowOutStanding)) == true)
                {
                    
                    //double OpBal = ObjQry.ReturnDouble("Select OpeningBalance From MLedger Where LedgerNo =" + ObjFunction.GetComboValue(cmbPartyName) + "", CommonFunctions.ConStr);
                    double OpBal = ObjQry.ReturnDouble("Select case when signCode=1 then isnull(OpeningBalance,0) else isNull(OpeningBalance,0)*-1 end  From MLedger Where LedgerNo =" + ObjFunction.GetComboValue(cmbPartyName) + "", CommonFunctions.ConStr);
                    //double AvlBal = OpBal + ObjQry.ReturnDouble("Select isNull(Sum(Amount),0) from TVoucherRefDetails where LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " and TypeOfRef=3 AND FKVoucherTrnNo not in(Select PKVoucherTrnNo From TVoucherDetails Where FKVoucherNo=" + ID + ") ", CommonFunctions.ConStr);
                    double AvlBal = OpBal + ObjQry.ReturnDouble("Select isNull(Sum(Amount),0) FROM         TVoucherDetails INNER JOIN " +
                                                                 " TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN " +
                                                               " TVoucherRefDetails ON TVoucherDetails.PkVoucherTrnNo = TVoucherRefDetails.FkVoucherTrnNo where TVoucherRefDetails.LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " and TypeOfRef=3 AND FKVoucherTrnNo not in(Select PKVoucherTrnNo From TVoucherDetails Where FKVoucherNo=" + ID + ") and  TVoucherEntry.IsCancel = 'false' AND VouchertypeCode=15", CommonFunctions.ConStr);
                    double RecBal = ObjQry.ReturnDouble("Select isNull(Sum(Amount),0) from TVoucherRefDetails where LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " and TypeOfRef in(2,5)", CommonFunctions.ConStr);
                    RecBal += ObjQry.ReturnDouble("SELECT isnull(SUM(TVoucherDetails.Credit),0.00) FROM TVoucherDetails INNER JOIN   TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo WHERE (TVoucherDetails.LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + ")  AND VoucherTypeCode=12", CommonFunctions.ConStr);
                    double TotalDues = 0;
                    long ControlUnder = ObjQry.ReturnLong("Select ControlUnder From MPayType Where PKPayTypeNo=" + ObjFunction.GetComboValue(cmbPaymentType) + "", CommonFunctions.ConStr);
                    //if (ObjFunction.GetComboValue(cmbPaymentType) == 3)
                    if (ControlUnder == 3)
                    {
                        if (BillFlag == false)
                            TotalDues = Convert.ToDouble((AvlBal - RecBal) + Convert.ToDouble(txtGrandTotal.Text));
                        else
                            TotalDues = Convert.ToDouble((AvlBal - RecBal));
                        if (TotalDues > 0)
                        {
                            ReportSession[12] = "Total Dues:" + TotalDues.ToString("0.00");
                        }
                        else if (TotalDues <= 0)
                            ReportSession[12] = "Total Dues: 0";
                    }
                    else
                    {
                        TotalDues = AvlBal - RecBal;
                        if (TotalDues > 0)
                            ReportSession[12] = "Total Dues:" + Convert.ToDouble(AvlBal - RecBal).ToString("0.00");
                        else
                            ReportSession[12] = "Total Dues: 0";
                    }
                }
                else
                    ReportSession[12] = "0";
                */
                #endregion

                #region New Code for Outstanding
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_ShowOutStanding)) == true)
                {
                    double TotalDues = 0;

                    TotalDues = ObjQry.ReturnDouble("Select Min(case when MLedger.signcode = 1 then -OpeningBalance else OpeningBalance end) " +
                                " - SUM(Debit) + SUM(Credit) " +
                                " FROM TVoucherDetails INNER JOIN " +
                                " TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN " +
                                " MLedger ON TVoucherDetails.LedgerNo = MLedger.LedgerNo " +
                                " Where TVoucherDetails.LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) +
                                " AND TVoucherEntry.IsCancel = 'false'", CommonFunctions.ConStr);
                    TotalDues = -1 * TotalDues;
                    if (TotalDues > 0)
                    {
                        ReportSession[12] = "Total Dues:" + TotalDues.ToString("0.00");
                    }
                    else if (TotalDues <= 0)
                        ReportSession[12] = "Total Dues: 0";
                }
                else
                    ReportSession[12] = "0";

                #endregion

                CrystalDecisions.CrystalReports.Engine.ReportDocument childForm;
                childForm = null;
                if (rbEnglish.Checked == true)
                {
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsBillWithMRP)) == true)
                            childForm = ObjFunction.GetReportObject("Reports.GetBillMRP");
                        else
                            childForm = ObjFunction.GetReportObject("Reports.GetBill");
                    }
                    else
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsBillWithMRP)) == true)
                            childForm = ObjFunction.LoadReportObject("GetBillMRP.rpt", CommonFunctions.ReportPath);
                        else
                            childForm = ObjFunction.LoadReportObject("GetBill.rpt", CommonFunctions.ReportPath);
                    }
                }
                else if (rbMarathi.Checked == true)
                {
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsBillWithMRP)) == true)
                            childForm = ObjFunction.GetReportObject("Reports.GetBillMarMRP");
                        else
                            childForm = ObjFunction.GetReportObject("Reports.GetBillMar");
                    }
                    else
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsBillWithMRP)) == true)
                            childForm = ObjFunction.LoadReportObject("GetBillMarMRP.rpt", CommonFunctions.ReportPath);
                        else
                            childForm = ObjFunction.LoadReportObject("GetBillMar.rpt", CommonFunctions.ReportPath);
                    }
                }
                if (childForm != null)
                {
                    DBReportGenerate objRpt = new DBReportGenerate(childForm, ReportSession);
                    if (objRpt.PrintReport() == true)
                    {
                        DisplayMessage("Bill Print Successfully!!!");

                    }
                    else
                    {
                        DisplayMessage("Bill not Print !!!");
                    }
                }
                else
                {
                    DisplayMessage("Bill Report not exist !!!");
                }


            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void PrintBill1()
        {
            try
            {
                DialogResult ds = DialogResult.No;
                string PrinterName = "";
                CrystalDecisions.CrystalReports.Engine.ReportDocument childForm;
                childForm = null;
                if (rbEnglish.Checked == true)
                {

                    DataRow[] dr = dtPrinterSetting.Select("IsDefault='true' And PrintType='true' And IsLangType='true'");
                    if (dr.Length != 0)
                        PrinterName = dr[0].ItemArray[ColPrintIndex.PrinterName].ToString();

                    DataTable dtPrinter = new DataTable();
                    dtPrinter.Columns.Add("PrinterNo");
                    dtPrinter.Columns.Add("MachineIP");
                    DataRow[] drs = dtPrinterSetting.Select("IsLangType='true'");
                    foreach (DataRow items in drs)
                    {
                        DataRow dr1 = dtPrinter.NewRow();
                        dr1[0] = items["PrinterNo"];

                        dr1[1] = items["MachineIP"];

                        dtPrinter.Rows.Add(dr1);

                    }

                    ds = OMPrintMessageBox.Show("Want to print this bill?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNoCancel, OMMessageBoxIcon.Question, OMMessageBoxDefaultButton.Button1, PrinterName, false, "Preview", dtPrinter);

                    if (ds != DialogResult.No)
                    {

                        dr = dtPrinterSetting.Select("PrinterNo=" + OMPrintMessageBox.PrinterName.ToUpper().Trim() + " And PrintType='true' And IsLangType='true'");
                        if (dr.Length != 0)
                        {
                            childForm = ObjFunction.LoadReportObject("" + dr[0]["MachineName"].ToString() + ".rpt", CommonFunctions.ReportPath);
                            PrinterName = dr[0]["PrinterName"].ToString();
                        }
                        else
                        {
                            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            {
                                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsBillWithMRP)) == true)
                                    childForm = ObjFunction.GetReportObject("Reports.GetBillMRP");
                                else
                                    childForm = ObjFunction.GetReportObject("Reports.GetBill");
                            }
                            else
                            {
                                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsBillWithMRP)) == true)
                                    childForm = ObjFunction.LoadReportObject("GetBillMRP.rpt", CommonFunctions.ReportPath);
                                else
                                    childForm = ObjFunction.LoadReportObject("GetBill.rpt", CommonFunctions.ReportPath);
                            }
                        }
                    }
                    else
                        return;
                }
                else if (rbMarathi.Checked == true)
                {
                    DataRow[] dr = dtPrinterSetting.Select("IsDefault='true' And PrintType='true' And IsLangType='false'");
                    if (dr.Length != 0)
                        PrinterName = dr[0].ItemArray[ColPrintIndex.PrinterName].ToString();

                    DataTable dtPrinter = new DataTable();
                    dtPrinter.Columns.Add("PrinterNo");
                    dtPrinter.Columns.Add("MachineIP");
                    DataRow[] drs = dtPrinterSetting.Select("IsLangType='false'");
                    foreach (DataRow items in drs)
                    {
                        DataRow dr1 = dtPrinter.NewRow();
                        dr1[0] = items["PrinterNo"];

                        dr1[1] = items["MachineIP"];

                        dtPrinter.Rows.Add(dr1);

                    }

                    ds = OMPrintMessageBox.Show("Want to print this bill?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNoCancel, OMMessageBoxIcon.Question, OMMessageBoxDefaultButton.Button1, PrinterName, false, "Preview", dtPrinter);


                    if (ds != DialogResult.No)
                    {

                        dr = dtPrinterSetting.Select("PrinterNo=" + OMPrintMessageBox.PrinterName.ToUpper().Trim() + " And PrintType='true' And IsLangType='False'");
                        if (dr.Length != 0)
                        {
                            childForm = ObjFunction.LoadReportObject("" + dr[0]["MachineName"].ToString() + ".rpt", CommonFunctions.ReportPath);
                            PrinterName = dr[0]["PrinterName"].ToString();
                        }
                        else
                        {
                            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            {
                                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsBillWithMRP)) == true)
                                    childForm = ObjFunction.GetReportObject("Reports.GetBillMarMRP");
                                else
                                    childForm = ObjFunction.GetReportObject("Reports.GetBillMar");
                            }
                            else
                            {
                                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsBillWithMRP)) == true)
                                    childForm = ObjFunction.LoadReportObject("GetBillMarMRP.rpt", CommonFunctions.ReportPath);
                                else
                                    childForm = ObjFunction.LoadReportObject("GetBillMar.rpt", CommonFunctions.ReportPath);
                            }
                        }
                    }
                    else
                        return;
                }

                if (childForm != null)
                {
                    if (ds == DialogResult.Yes)
                    {
                        DBReportGenerate objRpt = new DBReportGenerate(childForm, GetReportSession());
                        if (objRpt.PrintReport(PrinterName) == true)
                        {
                            DisplayMessage("Bill Print Successfully!!!");

                        }
                        else
                        {
                            DisplayMessage("Bill not Print !!!");
                        }
                    }
                    else if (ds == DialogResult.Cancel)
                    {
                        childForm.PrintOptions.PrinterName = PrinterName;
                        Form NewF = null;
                        NewF = new Display.ReportViewSource(childForm, GetReportSession());
                        if (NewF != null)
                            ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                    }
                }
                else
                {
                    DisplayMessage("Bill Report not exist !!!");
                }


            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void PrintGodownWise()
        {
            try
            {
                string[] ReportSession;

                ReportSession = new string[2];
                ReportSession[0] = ID.ToString();
                ReportSession[1] = (rbEnglish.Checked == true) ? "1" : "2";


                CrystalDecisions.CrystalReports.Engine.ReportDocument childForm;
                childForm = null;
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                {
                    if (rbEnglish.Checked == true)
                        childForm = ObjFunction.GetReportObject("Reports.GetPrintGodownWise");
                    else
                        childForm = ObjFunction.GetReportObject("Reports.GetPrintGodownWiseMar");
                }
                else
                {
                    if (rbEnglish.Checked == true)
                        childForm = ObjFunction.LoadReportObject("GetPrintGodownWise.rpt", CommonFunctions.ReportPath);
                    else
                        childForm = ObjFunction.LoadReportObject("GetPrintGodownWiseMar.rpt", CommonFunctions.ReportPath);
                }
                if (childForm != null)
                {
                    DBReportGenerate objRpt = new DBReportGenerate(childForm, ReportSession);
                    if (objRpt.PrintReport() == true)
                    {
                        DisplayMessage("Godown Wise Print Successfully!!!");
                    }
                    else
                    {
                        DisplayMessage("Godown Bill not Print !!!");
                    }
                }
                else
                {
                    DisplayMessage("Godown Wise Print Report not exist !!!");
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void GPrint()
        {
            try
            {
                DialogResult ds = DialogResult.No;
                string PrinterName = "";
                CrystalDecisions.CrystalReports.Engine.ReportDocument childForm;
                childForm = null;
                if (rbEnglish.Checked == true)
                {

                    DataRow[] dr = dtPrinterSetting.Select("IsDefault='true' And PrintType='False' And IsLangType='true'");
                    if (dr.Length != 0)
                        PrinterName = dr[0].ItemArray[ColPrintIndex.PrinterName].ToString();

                    DataTable dtPrinter = new DataTable();
                    dtPrinter.Columns.Add("PrinterNo");
                    dtPrinter.Columns.Add("MachineIP");
                    DataRow[] drs = dtPrinterSetting.Select("IsLangType='true'");
                    foreach (DataRow items in drs)
                    {
                        DataRow dr1 = dtPrinter.NewRow();
                        dr1[0] = items["PrinterNo"];

                        dr1[1] = items["MachineIP"];

                        dtPrinter.Rows.Add(dr1);

                    }

                    ds = OMPrintMessageBox.Show("Want to print this Godown?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNoCancel, OMMessageBoxIcon.Question, OMMessageBoxDefaultButton.Button1, PrinterName, false, "Preview", dtPrinter);

                    if (ds != DialogResult.No)
                    {

                        dr = dtPrinterSetting.Select("PrinterNo=" + OMPrintMessageBox.PrinterName.ToUpper().Trim() + " And PrintType='False' And IsLangType='true'");
                        if (dr.Length != 0)
                        {
                            childForm = ObjFunction.LoadReportObject("" + dr[0]["MachineName"].ToString() + ".rpt", CommonFunctions.ReportPath);
                            PrinterName = dr[0]["PrinterName"].ToString();
                        }

                    }
                    else
                        return;
                }
                else if (rbMarathi.Checked == true)
                {
                    DataRow[] dr = dtPrinterSetting.Select("IsDefault='true' And PrintType='False' And IsLangType='false'");
                    if (dr.Length != 0)
                        PrinterName = dr[0].ItemArray[ColPrintIndex.PrinterName].ToString();

                    DataTable dtPrinter = new DataTable();
                    dtPrinter.Columns.Add("PrinterNo");
                    dtPrinter.Columns.Add("MachineIP");
                    DataRow[] drs = dtPrinterSetting.Select("IsLangType='false'");
                    foreach (DataRow items in drs)
                    {
                        DataRow dr1 = dtPrinter.NewRow();
                        dr1[0] = items["PrinterNo"];

                        dr1[1] = items["MachineIP"];

                        dtPrinter.Rows.Add(dr1);

                    }

                    ds = OMPrintMessageBox.Show("Want to print this Godown?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNoCancel, OMMessageBoxIcon.Question, OMMessageBoxDefaultButton.Button1, PrinterName, false, "Preview", dtPrinter);


                    if (ds != DialogResult.No)
                    {

                        dr = dtPrinterSetting.Select("PrinterNo=" + OMPrintMessageBox.PrinterName.ToUpper().Trim() + " And PrintType='False' And IsLangType='False'");
                        if (dr.Length != 0)
                        {
                            childForm = ObjFunction.LoadReportObject("" + dr[0]["MachineName"].ToString() + ".rpt", CommonFunctions.ReportPath);
                            PrinterName = dr[0]["PrinterName"].ToString();
                        }
                    }
                    else
                        return;
                }

                if (childForm != null)
                {
                    string[] ReportSession;

                    ReportSession = new string[2];
                    ReportSession[0] = ID.ToString();
                    ReportSession[1] = (rbEnglish.Checked == true) ? "1" : "2";

                    if (ds == DialogResult.Yes)
                    {
                        DBReportGenerate objRpt = new DBReportGenerate(childForm, ReportSession);
                        if (objRpt.PrintReport(PrinterName) == true)
                        {
                            DisplayMessage("Godown Wise Print Successfully!!!");

                        }
                        else
                        {
                            DisplayMessage("Godown Bill not Print !!!");
                        }
                    }
                    else if (ds == DialogResult.Cancel)
                    {
                        childForm.PrintOptions.PrinterName = PrinterName;
                        Form NewF = null;
                        NewF = new Display.ReportViewSource(childForm, ReportSession);
                        if (NewF != null)
                            ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                    }
                }
                else
                {
                    DisplayMessage("Godown Report not exist !!!");
                }


            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void PrintGodownWise1()
        {
            GPrint();
            //try
            //{
            //    string[] ReportSession;
            //    string PrinterName = "";
            //    ReportSession = new string[2];
            //    ReportSession[0] = ID.ToString();
            //    ReportSession[1] = (rbEnglish.Checked == true) ? "1" : "2";


            //    CrystalDecisions.CrystalReports.Engine.ReportDocument childForm;
            //    childForm = null;

            //    DataRow[] dr = dtPrinterSetting.Select("IsDefault='true' And PrintType='false' And IsLangType='" + ((rbEnglish.Checked == true) ? "true" : "false") + "'");
            //    if (dr.Length != 0)
            //        PrinterName = dr[0].ItemArray[ColPrintIndex.PrinterName].ToString();

            //    if (OMPrintMessageBox.Show("Want to Print Godown ?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNoCancel, OMMessageBoxIcon.Question, OMMessageBoxDefaultButton.Button1, PrinterName) == DialogResult.Yes)
            //    {
            //        PrinterName = OMPrintMessageBox.PrinterName;

            //        dr = dtPrinterSetting.Select("PrinterName='" + OMPrintMessageBox.PrinterName.ToUpper().Trim() + "' And PrintType='false' And IsLangType='" + ((rbEnglish.Checked == true) ? "true" : "false") + "'");

            //        if (dr.Length != 0)
            //            childForm = ObjFunction.LoadReportObject("" + dr[0].ItemArray[ColPrintIndex.MachineName].ToString() + ".rpt", CommonFunctions.ReportPath);
            //        else
            //        {
            //            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
            //            {
            //                if (rbEnglish.Checked == true)
            //                    childForm = ObjFunction.GetReportObject("Reports.GetPrintGodownWise");
            //                else
            //                    childForm = ObjFunction.GetReportObject("Reports.GetPrintGodownWiseMar");
            //            }
            //            else
            //            {
            //                if (rbEnglish.Checked == true)
            //                    childForm = ObjFunction.LoadReportObject("GetPrintGodownWise.rpt", CommonFunctions.ReportPath);
            //                else
            //                    childForm = ObjFunction.LoadReportObject("GetPrintGodownWiseMar.rpt", CommonFunctions.ReportPath);
            //            }
            //        }
            //    }
            //    else
            //        return;
            //    if (childForm != null)
            //    {
            //        DBReportGenerate objRpt = new DBReportGenerate(childForm, ReportSession);
            //        if (objRpt.PrintReport(PrinterName) == true)
            //        {
            //            DisplayMessage("Godown Wise Print Successfully!!!");
            //        }
            //        else
            //        {
            //            DisplayMessage("Godown Bill not Print !!!");
            //        }
            //    }
            //    else
            //    {
            //        DisplayMessage("Godown Wise Print Report not exist !!!");
            //    }
            //}
            //catch (Exception exc)
            //{
            //    ObjFunction.ExceptionDisplay(exc.Message);
            //}
        }

        private void dtpBillDate_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_StopOnParty)) == true) cmbPartyName.Focus();
                    else if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_StopOnRateType)) == true) cmbRateType.Focus();
                    else if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_StopOnTaxType)) == true) cmbTaxType.Focus();
                    else dgBill.Focus();

                    e.SuppressKeyPress = true;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbPartyName_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (ObjFunction.GetComboValue(cmbPartyName) == 0)
                    {
                        DisplayMessage("Select Party Name");
                        cmbPartyName.Focus();
                        //cmbPartyName.SelectedValue = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_PartyAC));
                    }
                    else
                    {
                        if (ObjFunction.GetComboValue(cmbPartyName) == Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_PartyAC)))// && (ObjFunction.GetComboValue(cmbPaymentType) == 3))
                        {
                            //cmbPaymentType.SelectedValue = 2;
                            //cmbPaymentType.Enabled = false;
                        }
                        else
                            cmbPaymentType.Enabled = true;

                        FillCreditLimit();
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_StopOnRateType)) == true) cmbRateType.Focus();
                        else if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_StopOnTaxType)) == true) cmbTaxType.Focus();
                        else
                        {
                            long ControlUnder = ObjQry.ReturnLong("Select ControlUnder From MPayType Where PKPayTypeNo=" + ObjFunction.GetComboValue(cmbPaymentType) + "", CommonFunctions.ConStr);
                            //if (ObjFunction.GetComboValue(cmbPaymentType) != 3)
                            if (ControlUnder != 3)
                                dgBill.Focus();
                            else btnSave.Focus();
                        }
                    }
                    e.SuppressKeyPress = true;
                }
                if (e.KeyCode == Keys.F10)
                {
                    Form NewF = new Display.ViewOutStanding(ObjFunction.GetComboValue(cmbPartyName));
                    ObjFunction.OpenForm(NewF);
                    cmbPartyName.Focus();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbPartyName_Leave(object sender, EventArgs e)
        {
            try
            {
                if (ObjFunction.GetComboValue(cmbPartyName) == Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_PartyAC)))// && (ObjFunction.GetComboValue(cmbPaymentType) == 3))
                {
                    // cmbPaymentType.SelectedValue = 2;
                    //cmbPaymentType.Enabled = false;
                }
                else
                    cmbPaymentType.Enabled = true;


                if (dgBill.Rows.Count > 1 && btnSave.Visible)
                {
                    txtSchemeDisc.Text = Format.DoubleFloating; txtOtherDisc.Text = Format.DoubleFloating;
                    FooterDiscDtlsNo = 0;
                    chkItemLevelDisc.Checked = false;
                    chkFooterLevelDisc.Checked = false;
                    chkItemLevelDisc.Checked = true;
                    chkFooterLevelDisc.Checked = true;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbRateType_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_StopOnTaxType)) == true) cmbTaxType.Focus();
                    else dgBill.Focus();

                    e.SuppressKeyPress = true;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbTaxType_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    dgBill.Focus();
                    dgBill.CurrentCell = dgBill[1, 0];
                    e.SuppressKeyPress = true;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void lstGroup1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F9)
            {
                try
                {
                    pnlGroup1.Visible = false;
                    FillItemList(1);
                    lblBrandName.Text = lstGroup1.Text;
                }
                catch (Exception exc)
                {
                    ObjFunction.ExceptionDisplay(exc.Message);
                }
            }
        }

        private void cmbRateType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //RateTypeNo = cmbRateType.SelectedIndex + 1;
            ChangeBillRate();
        }

        private void ChangeBillRate()
        {
            try
            {
                for (int i = 0; i < dgBill.Rows.Count; i++)
                {
                    if (dgBill.Rows[i].Cells[ColIndex.PkRateSettingNo].Value != null)
                    {
                        dgBill.Rows[i].Cells[ColIndex.Rate].Value = ObjQry.ReturnDouble("Select " + ObjFunction.GetComboValueString(cmbRateType) + " From MRateSetting Where PkSrNo=" + dgBill.Rows[i].Cells[ColIndex.PkRateSettingNo].Value + "", CommonFunctions.ConStr);
                    }
                }
                CalculateTotal();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #region Cheque AND Credit grid Related Methods
        private void BindPayChequeDetails(long VchNo)
        {
            try
            {
                string strQuery = "SELECT  TVoucherChqCreditDetails.ChequeNo, TVoucherChqCreditDetails.ChequeDate, IsNull(MOtherBank.BankName,'') AS BankName, IsNull(MBranch.BranchName,'') AS BranchName,  " +
                    " TVoucherChqCreditDetails.Amount, TVoucherChqCreditDetails.PkSrNo, TVoucherChqCreditDetails.BankNo, TVoucherChqCreditDetails.BranchNo FROM TVoucherChqCreditDetails LEFT OUTER JOIN " +
                    " MBranch ON TVoucherChqCreditDetails.BranchNo = MBranch.BranchNo LEFT OUTER JOIN " +
                    " MOtherBank ON TVoucherChqCreditDetails.BankNo = MOtherBank.BankNo Where TVoucherChqCreditDetails.ChequeNo <>'' AND TVoucherChqCreditDetails.FKVoucherNo=" + VchNo + "";
                dgPayChqDetails.Rows.Clear();


                DataTable dt = ObjFunction.GetDataView(strQuery).Table;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dgPayChqDetails.Rows.Add();
                    for (int k = 0; k < dt.Columns.Count; k++)
                    {
                        if (k == 1)
                            dgPayChqDetails.Rows[i].Cells[k].Value = Convert.ToDateTime(dt.Rows[i].ItemArray[k]).ToString("dd-MMM-yyyy");
                        else
                            dgPayChqDetails.Rows[i].Cells[k].Value = dt.Rows[i].ItemArray[k];
                        //if (strBank == "0")
                        //    strBank = dt.Rows[i].ItemArray[6].ToString();
                        //else
                        //    strBank = strBank + "," + dt.Rows[i].ItemArray[6].ToString();
                        //if(strBranch=="0")
                        //    strBranch = dt.Rows[i].ItemArray[7].ToString();
                        //else
                        //    strBranch = strBranch + dt.Rows[i].ItemArray[7].ToString();


                    }
                }
                // ObjFunction.FillList(lstBank, "Select BankNo,BankName From MOtherBank where IsActive='true' or BankNo in("+strBank+") order by BankName");
                //ObjFunction.FillList(lstBranch, "Select BranchNo,BranchName From MBranch where IsActive='true' or BranchNo in (" + strBranch + ") order by BranchName");
                //dgPayChqDetails.Rows.Add();
                dgPayChqDetails.Columns[0].Width = 69;
                dgPayChqDetails.Columns[1].Width = 83;
                dgPayChqDetails.Columns[2].Width = 120;
                dgPayChqDetails.Columns[3].Width = 114;
                dgPayChqDetails.Columns[4].Width = 75;
                //dgPayChqDetails.Focus();
                //dgPayChqDetails.CurrentCell = dgPayChqDetails[0, dgPayChqDetails.Rows.Count - 1];
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgPayChqDetails_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    if (dgPayChqDetails.CurrentCell.ColumnIndex == 1)
                    {
                        dtpChqDate.Location = new Point(dgPayChqDetails.CurrentCell.ContentBounds.X + 72 + dgPayChqDetails.Location.X, dgPayChqDetails.CurrentCell.ContentBounds.Y + 40);
                        dtpChqDate.Visible = true;
                        dtpChqDate.BringToFront();
                        dtpChqDate.Focus();
                    }
                    else if (dgPayChqDetails.CurrentCell.ColumnIndex == 2)
                    {
                        pnlBank.Location = new Point(dgPayChqDetails.CurrentCell.ContentBounds.X + 150 + dgPayChqDetails.Location.X, dgPayChqDetails.CurrentCell.ContentBounds.Y + 40);
                        pnlBank.Visible = true;
                        pnlBank.BringToFront();
                        lstBank.Focus();
                    }
                    else if (dgPayChqDetails.CurrentCell.ColumnIndex == 3)
                    {
                        pnlBranch.Location = new Point(dgPayChqDetails.CurrentCell.ContentBounds.X + 235 + dgPayChqDetails.Location.X, dgPayChqDetails.CurrentCell.ContentBounds.Y + 40);
                        pnlBranch.Visible = true;
                        pnlBranch.BringToFront();
                        lstBranch.Focus();
                    }
                    else if (dgPayChqDetails.CurrentCell.ColumnIndex == 4)
                    {
                        double TotAmt = 0;
                        for (int i = 0; i < dgPayChqDetails.Rows.Count; i++)
                        {
                            if (dgPayChqDetails.Rows[0].Cells[4].Value != null)
                            {
                                TotAmt = TotAmt + Convert.ToDouble(dgPayChqDetails.Rows[i].Cells[4].Value);
                            }
                        }
                        txtTotalAmt.Text = TotAmt.ToString();
                        btnOk.Focus();
                    }
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    double Amt = 0; int cnt = 0;
                    if (dgPayChqDetails.Rows[0].Cells[0].Value != null)
                    {
                        if (dgPayChqDetails.Rows[0].Cells[1].FormattedValue.ToString() == "" || dgPayChqDetails.Rows[0].Cells[2].FormattedValue.ToString() == "" || dgPayChqDetails.Rows[0].Cells[3].FormattedValue.ToString() == "")
                        {
                            OMMessageBox.Show("Please Fill Cheque Details.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            return;
                        }

                        for (int i = 0; i < dgPayChqDetails.Rows.Count; i++)
                        {
                            Amt += (dgPayChqDetails.Rows[i].Cells[4].Value == null) ? 0 : Convert.ToDouble(dgPayChqDetails.Rows[i].Cells[4].Value);
                        }
                        for (int i = 0; i < dgPayType.Rows.Count; i++)
                        {
                            if (dgPayType.Rows[i].Cells[1].Value.ToString() == ObjFunction.GetComboValue(cmbPaymentType).ToString())
                            {
                                cnt = i;
                                break;
                            }
                        }
                        if (Convert.ToDouble(dgPayType.Rows[cnt].Cells[2].Value) != Amt)
                        {
                            OMMessageBox.Show("Please enter Cheque amount and Cheque Details amount are not same...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            dgPayChqDetails.Focus();
                        }
                        else
                        {
                            if (ObjFunction.GetComboValue(cmbPaymentType) == 1)
                            {
                                pnlPartial.Size = new Size(305, 214);
                                pnlPartial.Location = new Point(200, 123);
                                dgPayType.CurrentCell = dgPayType[2, 3];
                                dgPayType.Focus();
                            }
                            else
                            {
                                btnSave.Enabled = true;
                                btnSave.Focus();
                                pnlPartial.Visible = false;
                            }
                            //btnOk.Focus();
                        }
                    }
                    else
                    {
                        if (ObjFunction.GetComboValue(cmbPaymentType) == 1)
                        {
                            pnlPartial.Size = new Size(305, 214);
                            pnlPartial.Location = new Point(200, 123);
                            dgPayType.CurrentCell = dgPayType[2, 3];
                            dgPayType.Focus();
                        }
                        else
                        {
                            //btnSave.Enabled = true;
                            //btnSave.Focus();
                            cmbPaymentType.Focus();
                            pnlPartial.Visible = false;
                        }
                    }
                }
                else if (e.KeyCode == Keys.Delete)
                {
                    //if (dgPayChqDetails.CurrentCell.RowIndex != dgPayChqDetails.Rows.Count - 1)
                    //{
                    if (Convert.ToInt64(dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[5].Value) != 0)
                    {
                        DeleteDtls(5, Convert.ToInt64(dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[5].Value));
                    }
                    dgPayChqDetails.Rows.RemoveAt(dgPayChqDetails.CurrentCell.RowIndex);
                    if (dgPayChqDetails.Rows.Count == 0)
                        dgPayChqDetails.Rows.Add();
                    dgPayChqDetails.CurrentCell = dgPayChqDetails[0, dgPayChqDetails.Rows.Count - 1];
                    // }
                }
                else if (e.KeyCode == Keys.N && e.Control)
                {
                    if (dgPayChqDetails.CurrentCell.ColumnIndex == 2)
                    {
                        Master.OtherBankAE NewF = new Kirana.Master.OtherBankAE(-1);
                        ObjFunction.OpenForm(NewF);
                        if (NewF.ShortID < 0)
                        {
                            dgPayChqDetails.CurrentCell = dgPayChqDetails[2, dgPayChqDetails.CurrentCell.RowIndex];
                        }
                        else if (NewF.ShortID != 0)
                        {
                            ObjFunction.FillList(lstBank, "Select BankNo,BankName From MOtherBank order by BankName");
                            lstBank.SelectedValue = ObjQry.ReturnLong("Select Max(BankNo) From MOtherBank", CommonFunctions.ConStr);
                            dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[2].Value = lstBank.Text;
                            dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[6].Value = lstBank.SelectedValue;
                            dgPayChqDetails.CurrentCell = dgPayChqDetails[3, dgPayChqDetails.CurrentCell.RowIndex];
                        }
                    }
                    else if (dgPayChqDetails.CurrentCell.ColumnIndex == 3)
                    {
                        Master.BranchAE NewF = new Kirana.Master.BranchAE(-1);
                        ObjFunction.OpenForm(NewF);
                        if (NewF.ShortID < 0)
                        {
                            dgPayChqDetails.CurrentCell = dgPayChqDetails[2, dgPayChqDetails.CurrentCell.RowIndex];
                        }
                        else if (NewF.ShortID != 0)
                        {
                            ObjFunction.FillList(lstBranch, "Select BranchNo,BranchName From MBranch order by BranchName");
                            lstBranch.SelectedValue = ObjQry.ReturnLong("Select Max(BranchNo) From MBranch ", CommonFunctions.ConStr);
                            dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[3].Value = lstBranch.Text;
                            dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[7].Value = lstBranch.SelectedValue;
                            dgPayChqDetails.CurrentCell = dgPayChqDetails[4, dgPayChqDetails.CurrentCell.RowIndex];

                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dtpChqDate_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;

                    dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[1].Value = dtpChqDate.SelectionStart.ToString("dd-MMM-yy");
                    dtpChqDate.Visible = false;
                    if (dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[2].Value == null)
                    {
                        pnlBank.Location = new Point(dgPayChqDetails.CurrentCell.ContentBounds.X + 150 + dgPayChqDetails.Location.X, dgPayChqDetails.CurrentCell.ContentBounds.Y + 40);
                        pnlBank.Visible = true;
                        pnlBank.BringToFront();
                        lblNMsg.Text = "(Ctrl+N) Add New Bank";
                        lstBank.Focus();
                    }
                    else
                    {
                        dgPayChqDetails.Focus();
                        dgPayChqDetails.CurrentCell = dgPayChqDetails[2, dgPayChqDetails.CurrentCell.RowIndex];
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void lstBank_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    if (dgPayChqDetails.Visible == true)
                    {
                        dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[2].Value = lstBank.Text;
                        dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[6].Value = lstBank.SelectedValue;
                        pnlBank.Visible = false;
                        if (dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[3].Value == null)
                        {
                            pnlBranch.Location = new Point(dgPayChqDetails.CurrentCell.ContentBounds.X + 235 + dgPayChqDetails.Location.X, dgPayChqDetails.CurrentCell.ContentBounds.Y + 40);
                            pnlBranch.Visible = true;
                            pnlBranch.BringToFront();
                            lblNMsg.Text = "(Ctrl+N) Add New Branch";
                            lstBranch.Focus();
                        }
                        else
                        {
                            dgPayChqDetails.Focus();
                            dgPayChqDetails.CurrentCell = dgPayChqDetails[3, dgPayChqDetails.CurrentCell.RowIndex];
                        }
                    }
                    else if (dgPayCreditCardDetails.Visible == true)
                    {
                        dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[1].Value = lstBank.Text;
                        dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[5].Value = lstBank.SelectedValue;
                        pnlBank.Visible = false;
                        if (dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[2].Value == null)
                        {
                            pnlBranch.Location = new Point(dgPayCreditCardDetails.CurrentCell.ContentBounds.X + 235 + dgPayCreditCardDetails.Location.X, dgPayCreditCardDetails.CurrentCell.ContentBounds.Y + 40);
                            pnlBranch.Visible = true;
                            pnlBranch.BringToFront();
                            lblNMsg.Text = "(Ctrl+N) Add New Branch";
                            lstBranch.Focus();
                        }
                        else
                        {
                            dgPayCreditCardDetails.Focus();
                            dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[3, dgPayCreditCardDetails.CurrentCell.RowIndex];
                        }
                    }
                }
                else if (e.KeyCode == Keys.Space)
                {
                    if (dgPayChqDetails.Visible == true)
                    {
                        dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[2].Value = "";
                        dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[6].Value = 0;
                        pnlBank.Visible = false;
                        if (dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[3].Value == null)
                        {
                            dgPayChqDetails.Focus();
                            dgPayChqDetails.CurrentCell = dgPayChqDetails[2, dgPayChqDetails.CurrentCell.RowIndex];
                            //pnlBranch.Location = new Point(dgPayCreditCardDetails.CurrentCell.ContentBounds.X + 200 + dgPayCreditCardDetails.Location.X, dgPayCreditCardDetails.CurrentCell.ContentBounds.Y + 50);
                            //pnlBranch.Visible = true;
                            //pnlBranch.BringToFront();
                            //lstBranch.Focus();
                        }
                        else
                        {
                            dgPayChqDetails.Focus();
                            dgPayChqDetails.CurrentCell = dgPayChqDetails[3, dgPayChqDetails.CurrentCell.RowIndex];
                        }
                    }
                    else if (dgPayCreditCardDetails.Visible == true)
                    {
                        dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[1].Value = "";
                        dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[5].Value = 0;
                        pnlBank.Visible = false;
                        if (dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[2].Value == null)
                        {
                            pnlBranch.Location = new Point(dgPayCreditCardDetails.CurrentCell.ContentBounds.X + 200 + dgPayCreditCardDetails.Location.X, dgPayCreditCardDetails.CurrentCell.ContentBounds.Y + 50);
                            pnlBranch.Visible = true;
                            pnlBranch.BringToFront();
                            lblNMsg.Text = "(Ctrl+N) Add New Branch";
                            lstBranch.Focus();
                        }
                        else
                        {
                            dgPayCreditCardDetails.Focus();
                            dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[3, dgPayCreditCardDetails.CurrentCell.RowIndex];
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void lstBranch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    if (dgPayChqDetails.Visible == true)
                    {
                        dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[3].Value = lstBranch.Text;
                        dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[7].Value = lstBranch.SelectedValue;
                        pnlBranch.Visible = false;
                        dgPayChqDetails.Focus();
                        dgPayChqDetails.CurrentCell = dgPayChqDetails[4, dgPayChqDetails.CurrentCell.RowIndex];
                    }
                    else if (dgPayCreditCardDetails.Visible == true)
                    {
                        dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[2].Value = lstBranch.Text;
                        dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[6].Value = lstBranch.SelectedValue;
                        pnlBranch.Visible = false;
                        dgPayCreditCardDetails.Focus();
                        dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[3, dgPayCreditCardDetails.CurrentCell.RowIndex];
                    }
                }
                else if (e.KeyCode == Keys.Space)
                {
                    if (dgPayChqDetails.Visible == true)
                    {
                        if (dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[3].Value == null)
                            dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[3].Value = "";
                        dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[7].Value = 0;
                        pnlBranch.Visible = false;
                        dgPayChqDetails.Focus();
                        dgPayChqDetails.CurrentCell = dgPayChqDetails[4, dgPayChqDetails.CurrentCell.RowIndex];
                    }
                    else if (dgPayCreditCardDetails.Visible == true)
                    {
                        if (dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[3].Value == null)
                            dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[3].Value = "";
                        dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[6].Value = 0;
                        pnlBranch.Visible = false;
                        dgPayCreditCardDetails.Focus();
                        dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[3, dgPayCreditCardDetails.CurrentCell.RowIndex];
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgPayChqDetails_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 0)
                {
                    if (dgPayChqDetails.Rows[e.RowIndex].Cells[1].Value == null)
                    {
                        if (e.RowIndex == 0)
                        {
                            for (int i = 0; i < dgPayType.Rows.Count; i++)
                            {
                                if (dgPayType.Rows[i].Cells[1].Value.ToString() == ObjFunction.GetComboValue(cmbPaymentType).ToString())
                                    if (e.RowIndex == 0) dgPayChqDetails.Rows[e.RowIndex].Cells[4].Value = dgPayType.Rows[i].Cells[2].Value;
                            }
                        }
                        //if (e.RowIndex == 0) dgPayChqDetails.Rows[e.RowIndex].Cells[4].Value = dgPayType.Rows[3].Cells[2].Value;
                        dtpChqDate.Location = new Point(dgPayChqDetails.CurrentCell.ContentBounds.X + 72 + dgPayChqDetails.Location.X, dgPayChqDetails.CurrentCell.ContentBounds.Y + 40);
                        dtpChqDate.Visible = true;
                        dtpChqDate.BringToFront();
                        dtpChqDate.Focus();
                    }
                    else
                    {
                        dgPayChqDetails.Focus();
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { dgPayChqDetails.CurrentCell.RowIndex, 1, dgPayChqDetails });
                    }
                }
                if (e.ColumnIndex == 4)
                {
                    if (dgPayChqDetails.Rows[e.RowIndex].Cells[4].Value != null)
                    {
                        dgPayChqDetails.Rows[e.RowIndex].Cells[4].ErrorText = "";
                        if (ObjFunction.CheckValidAmount(dgPayChqDetails.Rows[e.RowIndex].Cells[4].Value.ToString()) == false)
                        {
                            dgPayChqDetails.Rows[e.RowIndex].Cells[4].ErrorText = "Please Enter Valid Amount";
                        }
                        else
                        {
                            double TotAmt = 0;
                            dgPayChqDetails.Rows[e.RowIndex].Cells[4].ErrorText = "";
                            if (e.RowIndex == dgPayChqDetails.Rows.Count - 1 && dgPayChqDetails.Rows[e.RowIndex].Cells[1].Value != null)
                            {
                                for (int i = 0; i < dgPayChqDetails.Rows.Count; i++)
                                {
                                    if (dgPayChqDetails.Rows[e.RowIndex].Cells[4].Value != null)
                                    {
                                        TotAmt = TotAmt + Convert.ToDouble(dgPayChqDetails.Rows[i].Cells[4].Value);
                                    }
                                }
                                txtTotalAmt.Text = TotAmt.ToString();
                                dgPayChqDetails.Rows.Add();
                                dgPayChqDetails.Focus();
                                dgPayChqDetails.CurrentCell = dgPayChqDetails[0, dgPayChqDetails.Rows.Count - 1];
                            }
                            else
                            {
                                for (int i = 0; i < dgPayChqDetails.Rows.Count; i++)
                                {
                                    if (dgPayChqDetails.Rows[e.RowIndex].Cells[4].Value != null)
                                    {
                                        TotAmt = TotAmt + Convert.ToDouble(dgPayChqDetails.Rows[i].Cells[4].Value);
                                    }
                                }
                                txtTotalAmt.Text = TotAmt.ToString();
                                btnOk.Focus();
                            }
                        }
                    }
                    else
                    {
                        dgPayChqDetails.Rows[e.RowIndex].Cells[4].ErrorText = "Please Enter  Amount";
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }



        private void BindPayCreditDetails(long VchNo)
        {
            string strQuery = "SELECT  TVoucherChqCreditDetails.CreditCardNo, IsNull(MOtherBank.BankName,'') AS BankName, IsNull(MBranch.BranchName,'') AS BranchName,  " +
                " TVoucherChqCreditDetails.Amount, TVoucherChqCreditDetails.PkSrNo, TVoucherChqCreditDetails.BankNo, TVoucherChqCreditDetails.BranchNo FROM TVoucherChqCreditDetails LEFT OUTER JOIN " +
                " MBranch ON TVoucherChqCreditDetails.BranchNo = MBranch.BranchNo LEFT OUTER JOIN " +
                " MOtherBank ON TVoucherChqCreditDetails.BankNo = MOtherBank.BankNo Where TVoucherChqCreditDetails.CreditCardNo <>'' AND TVoucherChqCreditDetails.FKVoucherNo=" + VchNo + "";
            dgPayCreditCardDetails.Rows.Clear();

            DataTable dt = ObjFunction.GetDataView(strQuery).Table;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dgPayCreditCardDetails.Rows.Add();
                for (int k = 0; k < dt.Columns.Count; k++)
                {
                    dgPayCreditCardDetails.Rows[i].Cells[k].Value = dt.Rows[i].ItemArray[k];
                    //if (strBank == "0")
                    //    strBank = dt.Rows[i].ItemArray[6].ToString();
                    //else
                    //    strBank = strBank + "," + dt.Rows[i].ItemArray[6].ToString();
                    //if (strBranch == "0")
                    //    strBranch = dt.Rows[i].ItemArray[7].ToString();
                    //else
                    //    strBranch = strBranch + dt.Rows[i].ItemArray[7].ToString();
                }
            }
            // ObjFunction.FillList(lstBank, "Select BankNo,BankName From MOtherBank where IsActive='true' or BankNo in(" + strBank + ") order by BankName");
            //ObjFunction.FillList(lstBranch, "Select BranchNo,BranchName From MBranch where IsActive='true' or BranchNo in (" + strBranch + ") order by BranchName");
            //dgPayCreditCardDetails.Rows.Add();
            dgPayCreditCardDetails.Columns[0].Width = 69;
            dgPayCreditCardDetails.Columns[1].Width = 120;
            dgPayCreditCardDetails.Columns[2].Width = 114;
            dgPayCreditCardDetails.Columns[3].Width = 75;
        }

        private void dgPayCreditCardDetails_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (dgPayCreditCardDetails.CurrentCell.ColumnIndex == 1)
                {
                    pnlBank.Location = new Point(dgPayCreditCardDetails.CurrentCell.ContentBounds.X + 72 + dgPayCreditCardDetails.Location.X, dgPayCreditCardDetails.CurrentCell.ContentBounds.Y + 50);
                    pnlBank.Visible = true;
                    pnlBank.BringToFront();
                    lstBank.Focus();
                }
                else if (dgPayCreditCardDetails.CurrentCell.ColumnIndex == 2)
                {
                    pnlBranch.Location = new Point(dgPayCreditCardDetails.CurrentCell.ContentBounds.X + 200 + dgPayCreditCardDetails.Location.X, dgPayCreditCardDetails.CurrentCell.ContentBounds.Y + 50);
                    pnlBranch.Visible = true;
                    pnlBranch.BringToFront();
                    lstBranch.Focus();
                }
                else if (dgPayCreditCardDetails.CurrentCell.ColumnIndex == 3)
                {
                    double TotAmt = 0;
                    for (int i = 0; i < dgPayCreditCardDetails.Rows.Count; i++)
                    {
                        if (dgPayCreditCardDetails.Rows[i].Cells[3].Value != null)
                        {
                            TotAmt = TotAmt + Convert.ToDouble(dgPayCreditCardDetails.Rows[i].Cells[3].Value);
                        }
                    }
                    txtTotalAmt.Text = TotAmt.ToString();
                    btnOk.Focus();
                }
            }
            else if (e.KeyCode == Keys.Escape)
            {
                double Amt = 0; int cnt = 0;
                if (dgPayCreditCardDetails.Rows[0].Cells[0].Value != null)
                {
                    if (dgPayCreditCardDetails.Rows[0].Cells[0].FormattedValue.ToString() == "" || dgPayCreditCardDetails.Rows[0].Cells[1].FormattedValue.ToString() == "" || dgPayCreditCardDetails.Rows[0].Cells[2].FormattedValue.ToString() == "")
                    {
                        OMMessageBox.Show("Please Fill Credit Card Details.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        return;
                    }
                    for (int i = 0; i < dgPayCreditCardDetails.Rows.Count; i++)
                    {
                        Amt += (dgPayCreditCardDetails.Rows[i].Cells[3].Value == null) ? 0 : Convert.ToDouble(dgPayCreditCardDetails.Rows[i].Cells[3].Value);
                    }
                    for (int i = 0; i < dgPayType.Rows.Count; i++)
                    {
                        if (dgPayType.Rows[i].Cells[1].Value.ToString() == ObjFunction.GetComboValue(cmbPaymentType).ToString())
                        {
                            cnt = i;
                            break;
                        }
                    }
                    if (Convert.ToDouble(dgPayType.Rows[cnt].Cells[2].Value) != Amt)
                    {
                        OMMessageBox.Show("Please enter CrediCard amount and CreditCard Details amount are not same...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        dgPayCreditCardDetails.Focus();
                    }
                    else
                    {
                        if (ObjFunction.GetComboValue(cmbPaymentType) == 1)
                        {
                            pnlPartial.Size = new Size(305, 214);
                            pnlPartial.Location = new Point(200, 123);
                            dgPayType.CurrentCell = dgPayType[2, 4];
                            dgPayType.Focus();
                        }
                        else
                        {
                            btnSave.Enabled = true;
                            btnSave.Focus();
                            pnlPartial.Visible = false;
                        }
                        //btnOk.Focus();
                    }
                }
                else
                {
                    if (ObjFunction.GetComboValue(cmbPaymentType) == 1)
                    {
                        pnlPartial.Size = new Size(305, 214);
                        pnlPartial.Location = new Point(200, 123);
                        dgPayType.CurrentCell = dgPayType[2, 4];
                        dgPayType.Focus();
                    }
                    else
                    {
                        //btnSave.Enabled = true;
                        //btnSave.Focus();
                        cmbPaymentType.Focus();
                        pnlPartial.Visible = false;
                    }
                }
            }
            else if (e.KeyCode == Keys.Delete)
            {
                //if (dgPayCreditCardDetails.CurrentCell.RowIndex != dgPayCreditCardDetails.Rows.Count - 1)
                //{
                if (Convert.ToInt64(dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[4].Value) != 0)
                {
                    DeleteDtls(5, Convert.ToInt64(dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[4].Value));
                }
                dgPayCreditCardDetails.Rows.RemoveAt(dgPayCreditCardDetails.CurrentCell.RowIndex);
                if (dgPayCreditCardDetails.Rows.Count == 0)
                    dgPayCreditCardDetails.Rows.Add();
                dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[0, dgPayCreditCardDetails.Rows.Count - 1];
                //}
            }
            else if (e.KeyCode == Keys.N && e.Control)
            {
                if (dgPayCreditCardDetails.CurrentCell.ColumnIndex == 1)
                {
                    Master.OtherBankAE NewF = new Kirana.Master.OtherBankAE(-1);
                    ObjFunction.OpenForm(NewF);
                    if (NewF.ShortID < 0)
                    {
                        dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[2, dgPayCreditCardDetails.CurrentCell.RowIndex];
                    }
                    else if (NewF.ShortID != 0)
                    {
                        ObjFunction.FillList(lstBank, "Select BankNo,BankName From MOtherBank order by BankName");
                        lstBank.SelectedValue = ObjQry.ReturnLong("Select Max(BankNo) From MOtherBank", CommonFunctions.ConStr);
                        dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[1].Value = lstBank.Text;
                        dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[5].Value = lstBank.SelectedValue;
                        dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[3, dgPayCreditCardDetails.CurrentCell.RowIndex];
                    }
                }
                else if (dgPayCreditCardDetails.CurrentCell.ColumnIndex == 2)
                {
                    Master.BranchAE NewF = new Kirana.Master.BranchAE(-1);
                    ObjFunction.OpenForm(NewF);
                    if (NewF.ShortID < 0)
                    {
                        dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[3, dgPayCreditCardDetails.CurrentCell.RowIndex];
                    }
                    else if (NewF.ShortID != 0)
                    {
                        ObjFunction.FillList(lstBranch, "Select BranchNo,BranchName From MBranch order by BranchName");
                        lstBranch.SelectedValue = ObjQry.ReturnLong("Select Max(BranchNo) From MBranch ", CommonFunctions.ConStr);
                        dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[2].Value = lstBranch.Text;
                        dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[6].Value = lstBranch.SelectedValue;
                        dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[3, dgPayCreditCardDetails.CurrentCell.RowIndex];
                    }
                }
            }
        }

        private void dgPayCreditCardDetails_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                if (dgPayCreditCardDetails.Rows[e.RowIndex].Cells[1].Value == null)
                {
                    //if (e.RowIndex == 0) dgPayCreditCardDetails.Rows[e.RowIndex].Cells[3].Value = dgPayType.Rows[4].Cells[2].Value;
                    if (e.RowIndex == 0)
                    {
                        for (int i = 0; i < dgPayType.Rows.Count; i++)
                        {
                            if (dgPayType.Rows[i].Cells[1].Value.ToString() == ObjFunction.GetComboValue(cmbPaymentType).ToString())
                                if (e.RowIndex == 0) dgPayCreditCardDetails.Rows[e.RowIndex].Cells[3].Value = dgPayType.Rows[i].Cells[2].Value;
                        }
                    }
                    pnlBank.Location = new Point(dgPayCreditCardDetails.CurrentCell.ContentBounds.X + 72 + dgPayCreditCardDetails.Location.X, dgPayCreditCardDetails.CurrentCell.ContentBounds.Y + 50);
                    pnlBank.Visible = true;
                    pnlBank.BringToFront();
                    lblNMsg.Text = "(Ctrl+N) Add New Bank";
                    lstBank.Focus();
                }
                else
                {
                    dgPayCreditCardDetails.Focus();
                    MovetoNext move2n = new MovetoNext(m2n);
                    BeginInvoke(move2n, new object[] { dgPayCreditCardDetails.CurrentCell.RowIndex, 1, dgPayCreditCardDetails });
                }
            }
            if (e.ColumnIndex == 3)
            {
                if (dgPayCreditCardDetails.Rows[e.RowIndex].Cells[3].Value != null)
                {
                    dgPayCreditCardDetails.Rows[e.RowIndex].Cells[3].ErrorText = "";
                    if (ObjFunction.CheckValidAmount(dgPayCreditCardDetails.Rows[e.RowIndex].Cells[3].Value.ToString()) == false)
                    {
                        dgPayCreditCardDetails.Rows[e.RowIndex].Cells[3].ErrorText = "Please Enter Valid Amount";
                    }
                    else
                    {
                        double TotAmt = 0;
                        dgPayCreditCardDetails.Rows[e.RowIndex].Cells[3].ErrorText = "";
                        if (e.RowIndex == dgPayCreditCardDetails.Rows.Count - 1 && dgPayCreditCardDetails.Rows[e.RowIndex].Cells[1].Value != null)
                        {
                            dgPayCreditCardDetails.Rows.Add();
                            dgPayCreditCardDetails.Focus();
                            dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[0, dgPayCreditCardDetails.Rows.Count - 1];
                        }

                        for (int i = 0; i < dgPayCreditCardDetails.Rows.Count; i++)
                        {
                            if (dgPayCreditCardDetails.Rows[i].Cells[3].Value != null)
                            {
                                TotAmt = TotAmt + Convert.ToDouble(dgPayCreditCardDetails.Rows[i].Cells[3].Value);
                            }
                        }
                        txtTotalAmt.Text = TotAmt.ToString();
                    }
                }
                else
                {
                    dgPayCreditCardDetails.Rows[e.RowIndex].Cells[3].ErrorText = "Please Enter  Amount";
                }
            }
        }
        #endregion

        #region Rate Type Password related Methods

        private void txtRateTypePassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                btnRateTypeOK_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Escape)
            {
                pnlRateType.Visible = false;
                btnNew.Focus();
            }
        }

        private void btnRateTypeOK_Click(object sender, EventArgs e)
        {
            try
            {
                bool flag = false;
                string[,] arr = new string[5, 2];
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ARateIsActive)) == true)
                {
                    arr[0, 0] = ObjFunction.GetAppSettings(AppSettings.ARatePassword);
                    arr[0, 1] = "ASaleRate";
                }

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.BRateIsActive)) == true)
                {
                    arr[1, 0] = ObjFunction.GetAppSettings(AppSettings.BRatePassword);
                    arr[1, 1] = "BSaleRate";
                }

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.CRateIsActive)) == true)
                {
                    arr[2, 0] = ObjFunction.GetAppSettings(AppSettings.CRatePassword);
                    arr[2, 1] = "CSaleRate";
                }

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.DRateIsActive)) == true)
                {
                    arr[3, 0] = ObjFunction.GetAppSettings(AppSettings.DRatePassword);
                    arr[3, 1] = "DSaleRate";
                }

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ERateIsActive)) == true)
                {
                    arr[4, 0] = ObjFunction.GetAppSettings(AppSettings.ERatePassword);
                    arr[4, 1] = "ESaleRate";
                }
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_BillWithMRP)) == true)
                {
                    arr[5, 0] = "MRP";
                    arr[5, 1] = "MRP";
                }
                for (int i = 0; i < 6; i++)
                {
                    if (arr[i, 0] != null)
                    {
                        if (txtRateTypePassword.Text == arr[i, 0].ToString())
                        {
                            cmbRateType.SelectedValue = arr[i, 1].ToString();
                            //cmbRateType_SelectedIndexChanged(sender, new EventArgs());
                            RateTypeNo = i + 1;
                            DBMSettings dbMSettings = new DBMSettings();
                            if (arr[i, 1].ToString() == "ASaleRate")
                            {
                                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ARateDBEffect)) == true)
                                    dbMSettings.AddAppSettings(AppSettings.S_RateType, "1");
                            }
                            else if (arr[i, 1].ToString() == "BSaleRate")
                            {
                                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.BRateDBEffect)) == true)
                                    dbMSettings.AddAppSettings(AppSettings.S_RateType, "2");
                            }
                            else if (arr[i, 1].ToString() == "CSaleRate")
                            {
                                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.CRateDBEffect)) == true)
                                    dbMSettings.AddAppSettings(AppSettings.S_RateType, "3");
                            }
                            else if (arr[i, 1].ToString() == "DSaleRate")
                            {
                                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.DRateDBEffect)) == true)
                                    dbMSettings.AddAppSettings(AppSettings.S_RateType, "4");
                            }
                            else if (arr[i, 1].ToString() == "ESaleRate")
                            {
                                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ERateDBEffect)) == true)
                                    dbMSettings.AddAppSettings(AppSettings.S_RateType, "5");
                            }
                            else if (arr[i, 1].ToString() == "MRP")
                            {
                                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_BillWithMRP)) == true)
                                    dbMSettings.AddAppSettings(AppSettings.O_BillWithMRP, "6");
                            }
                            dbMSettings.ExecuteNonQueryStatements();
                            ObjFunction.SetAppSettings();
                            FillControls();
                            flag = true;
                            break;
                        }
                    }
                }
                if (flag == false)
                {
                    OMMessageBox.Show("Please enter valid password", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    txtRateTypePassword.Focus();
                }
                else
                {
                    pnlRateType.Visible = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnRateTypeCancel_Click(object sender, EventArgs e)
        {
            txtRateTypePassword.Text = "";
            pnlRateType.Visible = false;
        }

        #endregion

        private bool IsSuperMode()
        {
            bool flag = false;
            long RTNo = RateTypeNo;// Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_RateType));
            if (RTNo == 1)
            {
                flag = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ARateSuperMode));
            }
            else if (RTNo == 2)
            {
                flag = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.BRateSuperMode));
            }
            else if (RTNo == 3)
            {
                flag = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.CRateSuperMode));
            }
            else if (RTNo == 4)
            {
                flag = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.DRateSuperMode));
            }
            else if (RTNo == 5)
            {
                flag = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ERateSuperMode));
            }
            return true;
        }

        #region Party Name Methods
        private void btnPartyOK_Click(object sender, EventArgs e)
        {
            pnlPartyName.Visible = false;
            PrintAsk = 1;
            btnSave_Click(btnSave, new EventArgs());
        }

        private void btnPartyCancel_Click(object sender, EventArgs e)
        {
            pnlPartyName.Visible = false;
            btnSave.Focus();
        }
        #endregion

        private void lblPrint_Click(object sender, EventArgs e)
        {
            if (ID != 0)
            {
                PrintBill();
            }
        }

        private void DeleteRefDetails()
        {
            try
            {
                if (PayType == 3)
                {
                    if (PayType != ObjFunction.GetComboValue(cmbPaymentType) && ID != 0)
                    {
                        DataTable dtRef = ObjFunction.GetDataView("Select PKRefTrnNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FKVoucherNo=" + ID + " ").Table;
                        for (int i = 0; i < dtRef.Rows.Count; i++)
                        {
                            tVchRefDtls = new TVoucherRefDetails();
                            tVchRefDtls.PkRefTrnNo = Convert.ToInt64(dtRef.Rows[i].ItemArray[0].ToString());
                            dbTVoucherEntry.DeleteTVoucherRefDetails(tVchRefDtls);
                        }
                    }
                }
                else if (PayType != 3)
                {
                    if (ObjFunction.GetComboValue(cmbPaymentType) == 3)
                    {
                        if ((PartyNo != ObjFunction.GetComboValue(cmbPartyName) || PayType != ObjFunction.GetComboValue(cmbPaymentType)) && ID != 0)
                        {
                            DataTable dtDelPayType = ObjFunction.GetDataView("Select PKVoucherPayTypeNo,TVoucherPayTypeDetails.CompanyNo,FKReceiptVoucherNo,Amount From TVoucherPayTypeDetails,TVoucherDetails Where TVoucherDetails.PkVoucherTrnNo=TVoucherPayTypeDetails.FKVoucherTrnNo AND FKSalesVoucherNo=" + ID + "  order by PKVoucherPayTypeNo").Table;
                            for (int k = 0; k < dtDelPayType.Rows.Count; k++)
                            {
                                tVchPayTypeDetails = new TVoucherPayTypeDetails();
                                tVchPayTypeDetails.PKVoucherPayTypeNo = Convert.ToInt64(dtDelPayType.Rows[k].ItemArray[0].ToString());
                                dbTVoucherEntry.DeleteTVoucherPayTypeDetails(tVchPayTypeDetails);

                                DataTable dtUpdateVoucher = ObjFunction.GetDataView("Select PKVoucherTrnNo,Debit,Credit From TVoucherDetails Where FKVoucherNo=" + dtDelPayType.Rows[k].ItemArray[2].ToString() + " AND CompanyNo=" + dtDelPayType.Rows[k].ItemArray[1].ToString() + "").Table;
                                double totamt = 0;
                                bool alllowdel = false;
                                for (int m = 0; m < dtUpdateVoucher.Rows.Count; m++)
                                {
                                    double DrAmt = Convert.ToDouble(dtUpdateVoucher.Rows[m].ItemArray[1].ToString());
                                    double CrAmt = Convert.ToDouble(dtUpdateVoucher.Rows[m].ItemArray[2].ToString());
                                    if (DrAmt > 0) DrAmt = DrAmt - Convert.ToDouble(dtDelPayType.Rows[k].ItemArray[3].ToString());
                                    if (CrAmt > 0) CrAmt = CrAmt - Convert.ToDouble(dtDelPayType.Rows[k].ItemArray[3].ToString());
                                    dbTVoucherEntry.UpdateVoucherDetails(DrAmt, CrAmt, Convert.ToInt64(dtUpdateVoucher.Rows[m].ItemArray[0].ToString()));
                                    totamt = totamt + DrAmt + CrAmt;
                                    alllowdel = true;
                                }
                                if (totamt == 0 && alllowdel == true)
                                {
                                    for (int m = 0; m < dtUpdateVoucher.Rows.Count; m++)
                                    {
                                        tVoucherDetails = new TVoucherDetails();
                                        tVoucherDetails.PkVoucherTrnNo = Convert.ToInt64(dtUpdateVoucher.Rows[m].ItemArray[0].ToString());
                                        dbTVoucherEntry.DeleteTVoucherDetails(tVoucherDetails);

                                        if (m == dtUpdateVoucher.Rows.Count - 1)
                                        {
                                            if (ObjQry.ReturnLong("Select Count(*) From TVoucherDetails Where FKVoucherNo=" + Convert.ToInt64(dtDelPayType.Rows[k].ItemArray[2].ToString()) + "", CommonFunctions.ConStr) >= dtUpdateVoucher.Rows.Count)
                                            {
                                                tVoucherEntry = new TVoucherEntry();
                                                tVoucherEntry.PkVoucherNo = Convert.ToInt64(dtDelPayType.Rows[k].ItemArray[2].ToString());
                                                dbTVoucherEntry.DeleteTVoucherEntry1(tVoucherEntry);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    long ControlUnder = ObjQry.ReturnLong("Select ControlUnder From MPayType Where PKPayTypeNo=" + PayType + "", CommonFunctions.ConStr);
                    //if (PayType == 5 || PayType == 4)
                    if (ControlUnder == 5 || ControlUnder == 4)
                    {
                        if (PayType != ObjFunction.GetComboValue(cmbPaymentType) && ID != 0)
                        {
                            DataTable dtCredit = ObjFunction.GetDataView("Select PKSrNo From TVoucherChqCreditDetails  Where FKVoucherNo=" + ID + " ").Table;
                            for (int i = 0; i < dtCredit.Rows.Count; i++)
                            {
                                tVchChqCredit = new TVoucherChqCreditDetails();
                                tVchChqCredit.PkSrNo = Convert.ToInt64(dtCredit.Rows[i].ItemArray[0].ToString());
                                dbTVoucherEntry.DeleteTVoucherChqCreditDetails(tVchChqCredit);
                            }
                            //if (PayType == 4)
                            if (ControlUnder == 4)
                                while (dgPayChqDetails.Rows.Count > 0)
                                    dgPayChqDetails.Rows.RemoveAt(0);
                            //if (PayType == 5)
                            if (ControlUnder == 5)
                                while (dgPayCreditCardDetails.Rows.Count > 0)
                                    dgPayCreditCardDetails.Rows.RemoveAt(0);
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void txtGrandTotal_TextChanged(object sender, EventArgs e)
        {
            lblGrandTotal.Text = txtGrandTotal.Text;
        }

        #region Parking Bill Related Methods
        private void ParkingSave()
        {
            try
            {
                DataTable dtParking = new DataTable();
                dbTVoucherEntry = new DBTVaucherEntry();
                TParkingBill_Mob tPBill = new TParkingBill_Mob();
                TParkingBillDetails tPBillDetails = new TParkingBillDetails();

                tPBill.ParkingBillNo = 0;
                tPBill.BillDate = dtpBillDate.Value;
                tPBill.BillTime = DBGetVal.ServerTime;
                tPBill.PersonName = txtPersonName.Text;
                tPBill.LedgerNo = ObjFunction.GetComboValue(cmbPartyName);
                tPBill.IsBill = false;
                tPBill.CompanyNo = DBGetVal.CompanyNo;
                tPBill.IsCancel = false;
                tPBill.UserID = DBGetVal.UserID;
                tPBill.UserDate = DBGetVal.ServerTime;
                tPBill.Discount = Convert.ToDouble(txtDiscRupees1.Text);
                tPBill.Charges = Convert.ToDouble(txtChrgRupees1.Text);
                tPBill.Remark = txtRemark.Text;
                tPBill.RateTypeNo = GetRateType();
                tPBill.TaxTypeNo = ObjFunction.GetComboValue(cmbTaxType);
                tPBill.InvNo = "";
                tPBill.VoucherTypeCode = VchType.Sales;
                dbTVoucherEntry.AddTParkingBill_Mob(tPBill);
                //if (ParkBillNo != 0)
                //    dtParking = ObjFunction.GetDataView("Select * from TParkingBillDetails where ParkingBillNo=" + ParkBillNo + "").Table;
                for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                {
                    tPBillDetails = new TParkingBillDetails();
                    //if (ParkBillNo == 0)
                    tPBillDetails.PkSrNo = 0;
                    //else
                    //{
                    //    if(i<dtParking.Rows.Count)
                    //         tPBillDetails.PkSrNo = Convert.ToInt64(dtParking.Select("PkSrNo", "Barcode=" + dgBill.Rows[i].Cells[ColIndex.Barcode].Value.ToString() + ""));
                    //    else
                    //        tPBillDetails.PkSrNo = 0;
                    //}
                    tPBillDetails.Barcode = dgBill.Rows[i].Cells[ColIndex.Barcode].Value.ToString();
                    tPBillDetails.Qty = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value.ToString());
                    tPBillDetails.Rate = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Rate].Value.ToString());
                    tPBillDetails.ItemDisc = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscRupees].Value.ToString());
                    tPBillDetails.UOMNo = Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.UOMNo].Value.ToString());
                    tPBillDetails.FkRateSettingNo = Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.PkRateSettingNo].Value.ToString());
                    tPBillDetails.ItemNo = Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.ItemNo].Value.ToString());
                    dbTVoucherEntry.AddTParkingBillDetails(tPBillDetails);
                }

                long tempID = dbTVoucherEntry.ExecuteNonQueryStatements();
                if (tempID != 0)
                {
                    OMMessageBox.Show("Parking Bill Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                    SetNavigation();
                    btnNew_Click(btnNew, new EventArgs());
                }
                else
                {
                    OMMessageBox.Show("Parking Bill Not Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void ShowParkingBill()
        {
            flagParking = true;
            Vouchers.ParkingBillAE itemSearch = new Vouchers.ParkingBillAE(VchType.Sales);
            ObjFunction.OpenForm(itemSearch);
            if (itemSearch.ParkBillNo != 0)
            {

                DataTable dtDetails = ObjFunction.GetDataView("Select LedgerNo,Discount,Charges,Remark,RateTypeNo,TaxTypeNo,InvNo,BillDate,BillTime " +
                    " From TParkingBill Where ParkingBillNo=" + itemSearch.ParkBillNo + "").Table;
                if (dtDetails.Rows.Count > 0)
                {
                    cmbPartyName.SelectedValue = dtDetails.Rows[0][0].ToString();
                    txtDiscRupees1.Text = dtDetails.Rows[0][1].ToString();
                    txtChrgRupees1.Text = dtDetails.Rows[0][2].ToString();
                    txtRemark.Text = dtDetails.Rows[0][3].ToString();
                    SetRateType(Convert.ToInt64(dtDetails.Rows[0][4].ToString()));
                    cmbTaxType.SelectedValue = dtDetails.Rows[0][5].ToString();
                    //dtpBillDate.Value = Convert.ToDateTime(dtDetails.Rows[0]["BillDate"].ToString());
                    // dtpBillTime.Value = Convert.ToDateTime(dtDetails.Rows[0]["BillTime"].ToString());
                    dtpBillDate.Value = DBGetVal.ServerTime.Date;
                    dtpBillTime.Value = DBGetVal.ServerTime;
                }
                DisplayParkingDetails(itemSearch.ParkBillNo);
                dgBill.Focus();
                dgBill.CurrentCell = dgBill[ColIndex.ItemName, dgBill.Rows.Count - 1];
                ParkBillNo = Convert.ToInt64(itemSearch.ParkBillNo);
                flagParking = false;
            }
        }

        private void dgParkingBills_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                ParkBillNo = 0;
                if (e.KeyCode == Keys.Enter)
                {
                    flagParking = true;
                    e.SuppressKeyPress = true;
                    DataTable dtDetails = ObjFunction.GetDataView("Select LedgerNo,Discount,Charges,Remark,RateTypeNo,TaxTypeNo From TParkingBill Where ParkingBillNo=" + dgParkingBills.CurrentRow.Cells[5].Value.ToString() + "").Table;
                    if (dtDetails.Rows.Count > 0)
                    {
                        cmbPartyName.SelectedValue = dtDetails.Rows[0][0].ToString();
                        txtDiscRupees1.Text = dtDetails.Rows[0][1].ToString();
                        txtChrgRupees1.Text = dtDetails.Rows[0][2].ToString();
                        txtRemark.Text = dtDetails.Rows[0][3].ToString();
                        SetRateType(Convert.ToInt64(dtDetails.Rows[0][4].ToString()));
                        cmbTaxType.SelectedValue = dtDetails.Rows[0][5].ToString();
                    }
                    DisplayParkingDetails(Convert.ToInt64(dgParkingBills.CurrentRow.Cells[5].Value.ToString()));
                    //     cmbPartyName.SelectedValue = ObjQry.ReturnString("Select LedgerNo From TParkingBill Where ParkingBillNo=" + dgParkingBills.CurrentRow.Cells[5].Value.ToString() + "", CommonFunctions.ConStr);
                    //DataTable dt = ObjFunction.GetDataView("Select BarCode,Qty,Rate,UOMNo,ItemDisc,FKRateSettingNo,ItemNo From TParkingBillDetails Where ParkingBillNo=" + dgParkingBills.CurrentRow.Cells[5].Value.ToString() + "").Table;
                    //for (int i = 0; i < dt.Rows.Count; i++)
                    //{

                    //    dgBill.Rows[i].Cells[ColIndex.ItemName].Value = dt.Rows[i].ItemArray[0].ToString();
                    //    dgBill.Rows[i].Cells[ColIndex.Quantity].Value = dt.Rows[i].ItemArray[1].ToString();
                    //    dgBill.Rows[i].Cells[ColIndex.Rate].Value = dt.Rows[i].ItemArray[2].ToString();
                    //    dgBill.Rows[i].Cells[ColIndex.UOMNo].Value = dt.Rows[i].ItemArray[3].ToString();

                    //    dgBill.CurrentCell = dgBill[ColIndex.ItemName, i];
                    //    dgBill_CellEndEdit(sender, new DataGridViewCellEventArgs(ColIndex.ItemName, i));
                    //    if (dgBill.Rows[i].Cells[ColIndex.Rate].Value.ToString() != dt.Rows[i].ItemArray[2].ToString())
                    //    {
                    //        dgBill.Rows[i].Cells[ColIndex.Rate].Value = dt.Rows[i].ItemArray[2].ToString();
                    //        dgBill_CellEndEdit(sender, new DataGridViewCellEventArgs(ColIndex.Rate, i));
                    //    }
                    //    dgBill.Rows[i].Cells[ColIndex.DiscRupees].Value = dt.Rows[i].ItemArray[4].ToString();
                    //    dgBill_CellEndEdit(sender, new DataGridViewCellEventArgs(ColIndex.DiscRupees, i));
                    //    CalculateTotal();
                    //    Application.DoEvents();
                    //}
                    dgBill.Focus();
                    dgBill.CurrentCell = dgBill[ColIndex.ItemName, dgBill.Rows.Count - 1];
                    pnlParking.Visible = false;
                    ParkBillNo = Convert.ToInt64(dgParkingBills.CurrentRow.Cells[5].Value.ToString());
                    flagParking = false;
                }
                else if (e.KeyCode == Keys.Delete)
                {
                    pnlParking.Visible = false;
                    Application.DoEvents();
                    if (OMMessageBox.Show("Are you sure want to delete Parking Bill...?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        TParkingBill tParkingBill = new TParkingBill();
                        tParkingBill.ParkingBillNo = Convert.ToInt64(dgParkingBills.CurrentRow.Cells[5].Value.ToString());
                        tParkingBill.FKVoucherNo = 0;
                        if (dbTVoucherEntry.DeleteTParkingBill(tParkingBill) == true)
                            DisplayMessage("Parking Bill Deleted Successfully..");

                        dgBill.Focus();
                        dgBill.CurrentCell = dgBill[ColIndex.ItemName, dgBill.Rows.Count - 1];
                    }
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    pnlParking.Visible = false;
                    dgBill.CurrentCell = dgBill[1, dgBill.CurrentRow.Index];
                }
                else if (e.KeyCode == Keys.C && e.Control && e.Alt)
                {
                    if (OMMessageBox.Show("Are you sure want cancel this parking bill..", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question, OMMessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        ObjTrans.Execute("Update TParkingBill set IsCancel='true' where ParkingBillNo=" + Convert.ToInt64(dgParkingBills.CurrentRow.Cells[5].Value.ToString()) + "", CommonFunctions.ConStr);
                        pnlParking.Visible = false;
                        Application.DoEvents();
                        DisplayMessage(dgParkingBills.CurrentRow.Cells[0].Value.ToString() + "Parking Bill Cancel Successfully....");

                        DataTable dtParking = ObjFunction.GetDataView("SELECT BillNo, PersonName AS Name " +
                        ",(SELECT SUM(Qty)FROM TParkingBillDetails WHERE (ParkingBillNo = TParkingBill.ParkingBillNo)) AS ItemQty,BillDate, BillTime,ParkingBillNo " +
                                                                  "FROM TParkingBill Where IsBill='false' and IsCancel='false' Order by BillNo desc").Table;
                        if (dtParking.Rows.Count > 0)
                        {
                            pnlParking.Visible = true;
                            dgParkingBills.DataSource = dtParking.DefaultView;
                            dgParkingBills.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                            dgParkingBills.Focus();
                            dgParkingBills.CurrentCell = dgParkingBills[0, dgParkingBills.CurrentRow.Index];

                        }
                        else
                        {
                            pnlParking.Visible = false;
                            dgBill.CurrentCell = dgBill[1, dgBill.CurrentRow.Index];
                        }
                    }
                    else
                    {
                        dgParkingBills.Focus();
                        dgParkingBills.CurrentCell = dgParkingBills[0, dgParkingBills.CurrentRow.Index];
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnParkingOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPersonName.Text == "")
                    txtPersonName.Text = "No Name";
                pnlMainParking.Visible = false;
                Application.DoEvents();

                bool schemeflag = false;
                for (int i = 0; i < dgBill.Rows.Count; i++)
                {
                    if (dgBill.Rows[i].Cells[ColIndex.SchemeDetailsNo].FormattedValue.ToString() != "")
                    {
                        schemeflag = true;
                        break;
                    }
                }

                if (schemeflag == false)
                {
                    ParkingSave();
                }
                else
                {
                    for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                    {
                        if ((dgBill.Rows[i].Cells[ColIndex.SchemeDetailsNo].FormattedValue.ToString() != "") ||
                            (dgBill.Rows[i].Cells[ColIndex.SchemeFromNo].FormattedValue.ToString() != "") ||
                            (dgBill.Rows[i].Cells[ColIndex.SchemeToNo].FormattedValue.ToString() != ""))
                        {
                            if (dgBill.Rows[i].Cells[ColIndex.SchemeDetailsNo].FormattedValue.ToString() != ""
                                && dgBill.Rows[i].Cells[ColIndex.SchemeFromNo].FormattedValue.ToString() != ""
                                && dgBill.Rows[i].Cells[ColIndex.SchemeToNo].FormattedValue.ToString() == "")
                            {

                                dgBill.Rows[i].Cells[ColIndex.SchemeDetailsNo].Value = "";
                                dgBill.Rows[i].Cells[ColIndex.SchemeFromNo].Value = "";
                                dgBill.Rows[i].Cells[ColIndex.RewardFromNo].Value = "";

                            }
                            else if (dgBill.Rows[i].Cells[ColIndex.SchemeDetailsNo].FormattedValue.ToString() != "" &&
                                dgBill.Rows[i].Cells[ColIndex.SchemeFromNo].FormattedValue.ToString() == "" &&
                                dgBill.Rows[i].Cells[ColIndex.SchemeToNo].FormattedValue.ToString() != "")// && dgBill.Rows[i].Cells[ColIndex.RewardFromNo].FormattedValue.ToString() != "" && dgBill.Rows[i].Cells[ColIndex.RewardToNo].FormattedValue.ToString() != "")
                            {

                                DataRow[] dr = dtTRewardDtls.Select("SchemeDetailsNo=" + dgBill.Rows[i].Cells[ColIndex.SchemeDetailsNo].FormattedValue.ToString());
                                if (dr.Length != 0)
                                {
                                    DeleteDtls(6, Convert.ToInt64(dr[0].ItemArray[Scheme.ColTRewardDtls.PkSrNo].ToString()));// 9 For RewardDetailsNo
                                    DeleteDtls(5, Convert.ToInt64(dr[0].ItemArray[Scheme.ColTRewardDtls.RewardNo].ToString()));// 11 For RewardTo
                                    dgBill.CurrentCell = dgBill[ColIndex.ItemName, i];
                                    delete_rowReward();
                                    i--;
                                }

                            }
                        }
                    }
                    ParkingSave();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnParkingCancel_Click(object sender, EventArgs e)
        {
            try
            {
                pnlMainParking.Visible = false;
                dgBill.Focus();
                dgBill.CurrentCell = dgBill[ColIndex.ItemName, dgBill.Rows.Count - 1];
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgParkingBills_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                e.Value = Convert.ToDateTime(e.Value).ToString("dd-MMM-yyyy");
            }
            else if (e.ColumnIndex == 4)
            {
                e.Value = Convert.ToDateTime(e.Value).ToShortTimeString();
            }

        }
        #endregion

        private void dgBill_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            //string str = "";
            if (e.ColumnIndex == ColIndex.Quantity)
                DBCellValue.Quanity = dgBill.Rows[e.RowIndex].Cells[ColIndex.Quantity].FormattedValue.ToString();
            if (e.ColumnIndex == ColIndex.Rate)
                DBCellValue.Rate = dgBill.Rows[e.RowIndex].Cells[ColIndex.Rate].FormattedValue.ToString();
            if (e.ColumnIndex == ColIndex.DiscRupees)
                DBCellValue.Disc = dgBill.Rows[e.RowIndex].Cells[ColIndex.DiscRupees].FormattedValue.ToString();
        }

        private void txtDiscount1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (txtDiscount1.Text == "") txtDiscount1.Text = "0.00";
                txtDiscRupees1.Text = Convert.ToDouble(((Convert.ToDouble(txtSubTotal.Text.Trim()) - Convert.ToDouble(txtTotalDisc.Text.Trim()) - Convert.ToDouble(txtSchemeDisc.Text.Trim()) + Convert.ToDouble(txtTotalTax.Text.Trim())) * Convert.ToDouble(txtDiscount1.Text)) / 100).ToString("0.00");
                Control_Leave((object)txtDiscount1, new EventArgs());
                txtDiscRupees1.Focus();
            }
        }

        private void txtChrgRupees1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                Control_Leave((object)txtChrgRupees1, new EventArgs());
            }
        }

        private void txtDiscRupees1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                txtDiscount1.Text = Convert.ToDouble((100 * Convert.ToDouble(txtDiscRupees1.Text)) / (Convert.ToDouble(txtSubTotal.Text.Trim()) - Convert.ToDouble(txtTotalDisc.Text.Trim()) + Convert.ToDouble(txtTotalTax.Text.Trim()))).ToString("0.00");
                Control_Leave((object)txtDiscRupees1, new EventArgs());
                txtChrgRupees1.Focus();
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (ID != 0)
            {
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_BillPrintWisePrinterSetting)) == true)
                {
                    PrintBill1();
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_GodownPrinting)) == true)
                        PrintGodownWise1();
                }
                else
                {
                    if (OMMessageBox.Show("Are you sure you want to Print this bill ?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question, OMMessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        PrintBill();
                    }
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_GodownPrinting)) == true)
                    {
                        if (OMMessageBox.Show("Are you sure you want to Print Godown Wise ?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question, OMMessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            PrintGodownWise();
                        }
                    }
                }

            }
        }

        private void btnAdvanceSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Master.AdvancedSearch Adsch = new Kirana.Master.AdvancedSearch(GroupType.SundryDebtors);
                ObjFunction.OpenForm(Adsch);
                if (Adsch.LedgerNo != 0)
                {
                    cmbPartyName.SelectedValue = Adsch.LedgerNo;
                    cmbPartyName.Focus();
                    Adsch.Close();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnShortcut_Click(object sender, EventArgs e)
        {
            if (pnlFooterInfo.Visible == false)
            {
                pnlFooterInfo.Dock = DockStyle.Bottom;
                //pnlFooterInfo.Height = 30;
                pnlFooterInfo.BorderStyle = BorderStyle.None;
                pnlFooterInfo.BringToFront();
                pnlFooterInfo.Visible = true;
            }
            else
            {
                pnlFooterInfo.Visible = false;
            }
        }

        private void btnShowDetails_Click(object sender, EventArgs e)
        {
            if (BillSizeFlag == false)
            {
                dgBill.Height = dgBill.Height - (pnlTotalAmt.Height + 10);
                pnlTotalAmt.Location = new Point(dgBill.Height + 10, pnlTotalAmt.Location.Y);
                pnlTotalAmt.Location = new Point(dgBill.Width - pnlTotalAmt.Width + 10, dgBill.Location.Y + dgBill.Height + 10);
                pnlTotalAmt.Visible = true;
                pnlTodaysSales.Location = new Point(dgBill.Height + 10, pnlTodaysSales.Location.Y);
                pnlTodaysSales.Location = new Point(dgBill.Location.X, dgBill.Location.Y + dgBill.Height + 10);//pnlTodaysSales.Location.Y+dgBill.Height + 10
                pnlTodaysSales.Visible = true;

                pnlCollectionDetails.Location = new Point(dgBill.Location.X + pnlTodaysSales.Width + 5, dgBill.Location.Y + dgBill.Height + 10);//pnlTodaysSales.Location.Y+dgBill.Height + 10
                pnlCollectionDetails.Visible = true;
                BillSizeFlag = true;

            }
            else
            {
                BillSizeFlag = false;
                pnlTotalAmt.Visible = false;
                pnlTodaysSales.Visible = false;
                pnlCollectionDetails.Visible = false;
                dgBill.Height = dgBill.Height + (pnlTotalAmt.Height + 10);
            }
        }

        private void btnTodaysSales_Click(object sender, EventArgs e)
        {
            try
            {
                string[] ReportSession;
                Form NewF = null;

                ReportSession = new string[7];
                ReportSession[0] = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                ReportSession[1] = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                ReportSession[2] = VchType.Sales.ToString();
                ReportSession[3] = "0";
                ReportSession[4] = DBGetVal.CompanyNo.ToString();
                ReportSession[5] = ObjQry.ReturnLong("Select Count(*) From TVoucherEntry Where VoucherTypeCode=" + VchType.Sales + " AND VoucherDate='" + DBGetVal.ServerTime.ToString(Format.DDMMMYYYY) + "' ANd IsCancel='false'", CommonFunctions.ConStr).ToString();
                ReportSession[6] = ObjQry.ReturnLong("Select Count(*) From TVoucherEntry Where VoucherTypeCode=" + VchType.RejectionIn + " AND VoucherDate='" + DBGetVal.ServerTime.ToString(Format.DDMMMYYYY) + "' ANd IsCancel='false'", CommonFunctions.ConStr).ToString();

                //if (OMMessageBox.Show("Do you want to Preview the report", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                //{
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                    NewF = new Display.ReportViewSource(new Reports.RPTSalesSummary(), ReportSession);
                else
                    NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("RPTSalesSummary.rpt", CommonFunctions.ReportPath), ReportSession);
                ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                //}
                //else
                //{
                //    CrystalDecisions.CrystalReports.Engine.ReportDocument childForm;
                //    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                //        childForm = ObjFunction.GetReportObject("Reports.RPTSalesSummary");
                //    else
                //        childForm = ObjFunction.LoadReportObject("RPTSalesSummary.rpt", CommonFunctions.ReportPath);

                //    DBReportGenerate objRpt = new DBReportGenerate(childForm, ReportSession);
                //    objRpt.PrintReport();
                //}
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnSalesRegister_Click(object sender, EventArgs e)
        {
            try
            {
                string[] ReportSession;
                Form NewF = null;

                ReportSession = new string[6];
                ReportSession[0] = VchType.Sales.ToString();
                ReportSession[1] = DBGetVal.CompanyNo.ToString();
                ReportSession[2] = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                ReportSession[3] = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
                ReportSession[4] = "1";
                ReportSession[5] = "0";

                //if (OMMessageBox.Show("Do you want to Preview the report", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                //{
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                    NewF = new Display.ReportViewSource(new Reports.RPTSalesRegister(), ReportSession);
                else
                    NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("RPTSalesRegister.rpt", CommonFunctions.ReportPath), ReportSession);
                ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                //}
                //else
                //{
                //    CrystalDecisions.CrystalReports.Engine.ReportDocument childForm;
                //    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                //        childForm = ObjFunction.GetReportObject("Reports.RPTSalesRegister");
                //    else
                //        childForm = ObjFunction.LoadReportObject("RPTSalesRegister.rpt", CommonFunctions.ReportPath);

                //    DBReportGenerate objRpt = new DBReportGenerate(childForm, ReportSession);
                //    objRpt.PrintReport();
                //}
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnNewCustomer_Click(object sender, EventArgs e)
        {
            try
            {
                long PartyNo = ObjFunction.GetComboValue(cmbPartyName);
                if (ObjFunction.CheckAllowMenu(23) == false) return;
                Form NewF = new Master.CustomerAE(-1);
                ObjFunction.OpenForm(NewF);

                if (((Master.CustomerAE)NewF).ShortID != 0)
                {
                    ObjFunction.FillCombo(cmbPartyName, "Select LedgerNo,LedgerName From MLedger Where GroupNo in (" + GroupType.SundryDebtors + ")  order by LedgerName", "New Entry");
                    if (((Master.CustomerAE)NewF).ShortID > 0)
                        cmbPartyName.SelectedValue = ((Master.CustomerAE)NewF).ShortID;
                    else
                        cmbPartyName.SelectedValue = PartyNo;
                    cmbPartyName.Focus();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private bool ItemExist(long ItNo, out int rowIndex)
        {
            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsAllowsDuplicatesItemsInSameBill)))
            {
                rowIndex = -1;
                return false;
            }
            rowIndex = -1;
            bool flag = false;
            try
            {
                for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                {
                    if (ItNo == Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.ItemNo].Value))
                    {
                        rowIndex = i;
                        flag = true;
                        break;
                    }
                }
                return flag;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return false;
            }
        }

        private bool ItemExist(long ItNo, long RateSettingNo, out int rowIndex)
        {
            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsAllowsDuplicatesItemsInSameBill)))
            {
                rowIndex = -1;
                return false;
            }
            rowIndex = -1;
            bool flag = false;
            try
            {
                for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                {
                    if (ItNo == Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.ItemNo].Value) && RateSettingNo == Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.PkRateSettingNo].Value))
                    {
                        rowIndex = i;
                        flag = true;
                        break;
                    }
                }
                return flag;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return false;
            }
        }

        private bool ItemExistScheme(long ItNo, long RateSettingNo, out int rowIndex)
        {
            rowIndex = -1;
            bool flag = false;
            try
            {
                for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                {
                    if (ItNo == Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.ItemNo].Value) && RateSettingNo == Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.PkRateSettingNo].Value))
                    {
                        if ((dgBill.Rows[i].Cells[ColIndex.SchemeDetailsNo].FormattedValue.ToString() == ""))
                        {
                            rowIndex = i;
                            flag = true;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return false;
            }
        }

        private void btnCashSave_Click(object sender, EventArgs e)
        {
            if (btnSave.Visible)
            {
                PrintAsk = 0;
                cmbPaymentType.SelectedValue = "2";
                btnSave_Click(sender, e);
            }
        }

        private void btnCreditSave_Click(object sender, EventArgs e)
        {
            if (btnSave.Visible)
            {
                PrintAsk = 0;
                cmbPaymentType.SelectedValue = "3";

                btnSave_Click(sender, e);
            }
        }

        private void cmbPartyName_SelectedValueChanged(object sender, EventArgs e)
        {
            FillCreditLimit();
            if (ID == 0) tempPartyNo = ObjFunction.GetComboValue(cmbPartyName);

            if (cmbPartyName.Items.Count > 1)
            {
                if (ObjFunction.GetComboValue(cmbPartyName) > 0)
                {
                    long PartyStateNo = ObjQry.ReturnLong("Select (Case When(ISNull(StateNo,0)=0) Then " + LocalStateNo + " Else StateNo end)  " +
                            " From MLedgerDetails WHERE LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName), CommonFunctions.ConStr);
                    if (PartyStateNo == 0)
                        PartyStateNo = LocalStateNo;

                    isLocalSale = PartyStateNo == LocalStateNo;
                }
            }
        }

        private void btnOrderType_Click(object sender, EventArgs e)
        {
            try
            {
                long temp = OrderType;
                if (OrderType == 1) OrderType = 2;
                else if (OrderType == 2) OrderType = 1;
                if (ID != 0)
                {
                    tVoucherEntry = new TVoucherEntry();
                    tVoucherEntry.PkVoucherNo = ID;
                    tVoucherEntry.OrderType = OrderType;
                    dbTVoucherEntry.UpdateTVoucherEntry(tVoucherEntry);
                    btnOrderType.Text = dtOrderType.Select("OrderTypeNo=" + OrderType)[0].ItemArray[1].ToString();
                    btnOrderType.BackColor = Color.FromName(dtOrderType.Select("OrderTypeNo=" + OrderType)[0].ItemArray[2].ToString());
                }
                else
                {
                    btnOrderType.Text = dtOrderType.Select("OrderTypeNo=" + OrderType)[0].ItemArray[1].ToString();
                    btnOrderType.BackColor = Color.FromName(dtOrderType.Select("OrderTypeNo=" + OrderType)[0].ItemArray[2].ToString());
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void ShowLastBill()
        {
            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_ShowLastBill)) == true)
                pnlLastBill.Visible = true;
            else
                pnlLastBill.Visible = false;
        }

        private void btnCCSave_Click(object sender, EventArgs e)
        {
            //if (btnSave.Visible)
            //{
            //    PrintAsk = 1;
            //    cmbPaymentType.SelectedValue = "5";
            //    btnSave_Click(sender, e);
            //}
        }

        private void pnlTodaysSales_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawLine(new Pen(Color.SteelBlue, 5), 0, 135, 214, 135);
        }

        private void txtChrgRupees1_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(txtChrgRupees1, 2, 5, JitFunctions.MaskedType.NotNegative);
        }

        private void txtDiscount1_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(txtDiscount1, 2, 2, JitFunctions.MaskedType.NotNegative);
        }

        private void txtDiscRupees1_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(txtDiscRupees1, 2, 4, JitFunctions.MaskedType.NotNegative);
        }

        private void btnCashSave_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (btnSave.Visible)
                {

                    PrintAsk = 1;
                    cmbPaymentType.SelectedValue = "2";
                    btnSave_Click(sender, e);
                }
            }
        }

        private void lstGroup1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_Bilingual)) == true)
                lstGroup1Lang.SelectedIndex = lstGroup1.SelectedIndex;

        }

        private void lstGroup1Lang_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_Bilingual)) == true)
            {
                if (lstGroup1.Items.Count > 0)
                    lstGroup1.SelectedIndex = lstGroup1Lang.SelectedIndex;
            }
        }

        private void txtRemark_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSave.Focus();
            }
        }

        private void rbType_CheckedChanged(object sender, EventArgs e)
        {
            rbType(true);

        }

        private void rbType(bool IsSetFocus)
        {
            try
            {
                rbInvNo.Enabled = true;
                rbPartyName.Enabled = true;
                if (rbInvNo.Checked == true)
                {
                    if (IsSetFocus)
                    {
                        lblLable.Visible = true;
                        lblLable.Text = "Inv No :";
                        txtSearch.Width = 162;
                        txtSearch.Location = new System.Drawing.Point(90, 39);
                        txtSearch.Visible = true;
                        cmbPartyNameSearch.Visible = false;
                        btnPartyName.Visible = false;
                        txtSearch.Focus();
                    }
                }
                else if (rbPartyName.Checked == true)
                {
                    if (IsSetFocus)
                    {
                        btnPartyName.Enabled = true;
                        cmbPartyNameSearch.Enabled = true;
                        btnPartyName.Visible = true;
                        lblLable.Visible = true;
                        lblLable.Text = "Party Name :";
                        cmbPartyNameSearch.Width = 250;
                        cmbPartyNameSearch.Location = new System.Drawing.Point(90, 39);
                        btnPartyName.Location = new System.Drawing.Point((90 + 250 + 5), 39);
                        cmbPartyNameSearch.Visible = true;
                        txtSearch.Visible = false;
                        cmbPartyNameSearch.Focus();
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbPartyNameSearch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (pnlPartySearch.Visible == true)
                    {
                        pnlPartySearch.Visible = false;
                        pnlSearch.Visible = true;
                    }
                    else
                    {

                        pnlPartySearch.Visible = true;
                        int x = dgBill.GetCellDisplayRectangle(0, 0, true).X + 200;//(Screen.PrimaryScreen.WorkingArea.Width) / 2;
                        int y = dgBill.GetCellDisplayRectangle(0, 0, true).Y + 100;
                        //pnlPartySearch.SetBounds(x, y, dgPartySearch.Width + 10, dgPartySearch.Height + 10);
                        pnlPartySearch.Location = new Point(x, y);
                        string str = "SELECT    0 as [#], TVoucherEntry.VoucherUserNo AS [Doc #], TVoucherEntry.VoucherDate AS 'Date', TVoucherEntry.BilledAmount AS 'Amount'," +
                                     "TVoucherEntry.PkVoucherNo FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo WHERE (TVoucherEntry.VoucherTypeCode IN (" + VchType.Sales + "," + VchType.SalesOrder + ")) AND (TVoucherDetails.VoucherSrNo = 1) AND (TVoucherEntry.CompanyNo = " + DBGetVal.CompanyNo + ")" +
                                     "And TVoucherDetails.LedgerNo=" + ObjFunction.GetComboValue(cmbPartyNameSearch) + " " +
                                     "Order By  TVoucherEntry.VoucherUserNo desc,TVoucherEntry.VoucherDate desc, TVoucherEntry.Reference desc";
                        dgPartySearch.DataSource = ObjFunction.GetDataView(str).Table.DefaultView;
                        dgPartySearch.Columns[0].Width = 50;
                        dgPartySearch.Columns[1].Width = 150;
                        dgPartySearch.Columns[2].Width = 80;
                        dgPartySearch.Columns[3].Width = 110;
                        dgPartySearch.Columns[3].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
                        dgPartySearch.Columns[3].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
                        dgPartySearch.Columns[4].Visible = false;
                        if (dgPartySearch.RowCount > 0)
                        {
                            pnlPartySearch.Focus();
                            SearchVisible(false);
                            pnlSearch.Visible = false;
                            e.SuppressKeyPress = true;
                            dgPartySearch.Focus();
                            dgPartySearch.CurrentCell = dgPartySearch[0, dgPartySearch.CurrentRow.Index];
                            lblSearchName.Text = "Party Name: " + cmbPartyNameSearch.Text;
                            lblSearchName.Font = new Font("Verdana", 11, FontStyle.Bold);

                            //MovetoNext move2n = new MovetoNext(m2n);
                            //BeginInvoke(move2n, new object[] { dgPartySearch.CurrentRow.Index, 0, dgPartySearch });
                        }
                        else
                        {
                            pnlPartySearch.Visible = false;
                            pnlSearch.Visible = false;
                            DisplayMessage("Bill Not Found");
                            pnlSearch.Visible = true;
                            rbPartyName.Focus();
                            rbType_CheckedChanged(rbPartyName, null);
                        }
                        txtSearch.Text = "";
                        cmbPartyNameSearch.SelectedIndex = 0;

                    }
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    e.SuppressKeyPress = true;
                    pnlSearch.Visible = false;
                    pnlPartySearch.Visible = false;
                    btnCancelSearch_Click(sender, new EventArgs());
                    btnNew.Focus();
                }
                else if (e.KeyCode == Keys.Left)
                {
                    rbInvNo.Checked = true;
                    rbType_CheckedChanged(rbInvNo, null);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgPartySearch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    long tempNo;
                    e.SuppressKeyPress = true;
                    tempNo = ObjQry.ReturnLong("Select PKVoucherNo From TVoucherEntry Where PkVoucherNo=" + Convert.ToInt64(dgPartySearch.Rows[dgPartySearch.CurrentRow.Index].Cells[4].Value) + " and VoucherTypeCode=" + VoucherType + " AND CompanyNo=" + DBGetVal.CompanyNo + "", CommonFunctions.ConStr);
                    if (tempNo > 0)
                    {
                        ID = tempNo;
                        SetNavigation();
                        FillControls();
                        btnNew.Enabled = true;
                        btnUpdate.Enabled = true;
                        pnlPartySearch.Visible = false;
                        btnNew.Focus();
                        SearchVisible(false);
                    }
                    else
                    {
                        txtSearch.Text = "";
                        cmbPartyName.SelectedIndex = 0;
                        DisplayMessage("Bill Not Found");
                        txtSearch.Focus();
                        //SearchVisible(false);
                    }

                }
                else if (e.KeyCode == Keys.Escape)
                {
                    e.SuppressKeyPress = true;
                    pnlPartySearch.Visible = false;
                    pnlSearch.Visible = true;
                    txtSearch.Focus();
                    rbType_CheckedChanged(sender, new EventArgs());
                }
                txtSearch.Text = "";
                cmbPartyNameSearch.SelectedIndex = 0;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void SearchVisible(bool flag)
        {
            txtSearch.Visible = flag;
            cmbPartyNameSearch.Visible = flag;
            btnPartyName.Visible = flag;
            lblLable.Visible = flag;
        }

        private void dgPartySearch_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                e.Value = e.RowIndex + 1;

            }
            else if (e.ColumnIndex == 2)
            {
                e.Value = Convert.ToDateTime(e.Value).ToString("dd-MMM-yy");
            }
        }

        private void btnPartyName_Click(object sender, EventArgs e)
        {
            try
            {
                Master.AdvancedSearch Adsch = new Kirana.Master.AdvancedSearch(GroupType.SundryDebtors);
                ObjFunction.OpenForm(Adsch);
                if (Adsch.LedgerNo != 0)
                {
                    cmbPartyNameSearch.SelectedValue = Adsch.LedgerNo;
                    cmbPartyNameSearch.Focus();
                    Adsch.Close();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgPayChqDetails_CellToolTipTextNeeded(object sender, DataGridViewCellToolTipTextNeededEventArgs e)
        {
            try
            {
                if (e.RowIndex == 2)
                {
                    Rectangle cellRect = dgPayChqDetails.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, false);
                    toolTip1.Show("Ctrl + N - Bank Name",
                                  this,
                                  dgPayChqDetails.Location.X + cellRect.X + cellRect.Size.Width,
                                  dgPayChqDetails.Location.Y + cellRect.Y + cellRect.Size.Height,
                                  1200);
                    toolTip1.BackColor = CommonFunctions.RowColor;
                }
                else if (e.RowIndex == 3)
                {
                    Rectangle cellRect = dgPayChqDetails.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, false);
                    toolTip1.Show("Ctrl + N - Branch Name",
                                  this,
                                  dgPayChqDetails.Location.X + cellRect.X + cellRect.Size.Width,
                                  dgPayChqDetails.Location.Y + cellRect.Y + cellRect.Size.Height,
                                  1200);
                    toolTip1.BackColor = CommonFunctions.RowColor;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void SalesAE_Activated(object sender, EventArgs e)
        {
            try
            {
                if (isDoProcess)
                {
                    dtPrinterSetting = ObjFunction.GetDataView("Select PrinterNo,PrinterName,MachineName,MachineIP,IsDefault,IsActive,PrintType,IsLangType From MPrinter").Table;
                    long Pid = ObjFunction.GetComboValue(cmbPartyName);
                    ObjFunction.FillComb(cmbPartyName, "Select LedgerNo,LedgerName From MLedger Where GroupNo in (" + GroupType.SundryDebtors + ") and (IsActive='true' or LedgerNo = " + PartyNo + ") order by LedgerName");
                    cmbPartyName.SelectedValue = Pid;
                    //if (cmbPaymentType.Enabled == true)
                    //{
                    //    long Pid1 = ObjFunction.GetComboValue(cmbPaymentType);
                    //    FillRateType();
                    //    SetRateType(Pid1);
                    //}
                    if (ID != 0)
                    {
                        if ((ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo in ( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + ")", CommonFunctions.ConStr) > 1) && ObjFunction.GetComboValue(cmbPaymentType) == 3)
                            btnMixMode.Visible = false;
                        else if (btnMixMode.Text == "Receipt\r\nMode")
                        {
                            if (btnSave.Visible == false)
                                btnMixMode.Visible = true;
                        }
                        else btnMixMode.Visible = false;
                    }
                    InitItemLeveDiscount();

                }
                isDoProcess = false;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                isDoProcess = false;
            }
        }

        private void SalesAE_Deactivate(object sender, EventArgs e)
        {
            isDoProcess = true;
        }

        private void dgPayChqDetails_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (dgPayChqDetails.CurrentCell.ColumnIndex == 2)
            {
                lblNMsg.Text = "(Ctrl+N) Add New Bank";
            }
            else if (dgPayChqDetails.CurrentCell.ColumnIndex == 3)
            {
                lblNMsg.Text = "(Ctrl+N) Add New Branch";
            }
            else
            {
                lblNMsg.Text = "";
            }

        }

        private void dgPayCreditCardDetails_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (dgPayCreditCardDetails.CurrentCell.ColumnIndex == 1)
            {
                lblNMsg.Text = "(Ctrl+N) Add New Bank";
            }
            else if (dgPayCreditCardDetails.CurrentCell.ColumnIndex == 2)
            {
                lblNMsg.Text = "(Ctrl+N) Add New Branch";
            }
            else
            {
                lblNMsg.Text = "";
            }
        }

        private void btnMixMode_Click(object sender, EventArgs e)
        {
            try
            {
                DisplayList(false);
                if (ID != 0)
                {
                    if (ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo in ( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + ")", CommonFunctions.ConStr) > 1)
                    {
                        btnUpdate.Visible = false;
                        btnBillCancel.Visible = false;
                        btnMixMode.Visible = false;
                        //BillFlag = false;
                        OMMessageBox.Show("Already this bill is amount collected", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        return;
                    }
                    else
                    {
                        if (ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo in ( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + " and TR.TypeOfRef in(6))", CommonFunctions.ConStr) > 1)
                        {
                            btnUpdate.Visible = false;
                            btnBillCancel.Visible = false;
                            btnMixMode.Visible = false;
                            //BillFlag = false;
                            OMMessageBox.Show("Already this bill is amount collected", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            return;
                        }
                    }

                    CalculateTotal();
                    partialPay = new Vouchers.PartialPayment(ID, Convert.ToDouble(txtGrandTotal.Text.Trim()), ObjFunction.GetComboValue(cmbPaymentType), ObjFunction.GetComboValue(cmbPartyName));

                    ObjFunction.OpenForm(partialPay);
                    partialPay.Left = 15;
                    if (((PartialPayment)partialPay).DS == DialogResult.Cancel)
                    {
                        if (MixModeFlag == true)
                            ObjTrans.Execute("Update TVoucherEntry set MixMode=0 Where PKVoucherNo=" + ID + "", CommonFunctions.ConStr);
                        return;
                    }
                    else if (((PartialPayment)partialPay).DS == DialogResult.OK)
                    {
                        partialPay.SaveData(ID);
                        FillControls();
                    }
                }
                ////if (btnSave.Visible)
                ////{
                //int cntCompany = 0;
                //if (ID == 0)
                //{
                //    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsAllowSingleFirmChq)) == true)
                //    {
                //        for (int i = 1; i < dgBill.Rows.Count - 1; i++)
                //            if (Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.StockCompanyNo].Value) != Convert.ToInt64(dgBill.Rows[i - 1].Cells[ColIndex.StockCompanyNo].Value))
                //                cntCompany++;

                //    }
                //    if (cntCompany != 0)
                //    {
                //        dgPayType.Rows[3].Cells[2].ReadOnly = true;
                //    }
                //    pnlPartial.Visible = true;
                //    // cmbPaymentType.SelectedValue = "1";
                //    cmbPaymentType_KeyDown(new object(), new KeyEventArgs(Keys.Enter));
                //}
                //else
                //{
                //    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsAllowSingleFirmChq)) == true)
                //    {
                //        for (int i = 1; i < dgBill.Rows.Count - 1; i++)
                //            if (Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.StockCompanyNo].Value) != Convert.ToInt64(dgBill.Rows[i - 1].Cells[ColIndex.StockCompanyNo].Value))
                //                cntCompany++;

                //    }
                //    if (cntCompany == 0)
                //    {
                //        pnlPartial.Visible = !pnlPartial.Visible;
                //        dgPayType.Focus();
                //        dgPayType.CurrentCell = dgPayType[2, 1];
                //    }
                //    //if (txtCash.Text != "")
                //    //{
                //    //    pnlPartial.Visible = true;
                //    //    txtCash.ReadOnly = true; txtCredit.ReadOnly = true;
                //    //    btnOk.Focus();
                //    //}
                //}
                ////}
                else
                {
                    //if (btnCreditSave.Visible == true && btnCreditSave.Enabled == true)
                    //if (Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_PartyAC)) != ObjFunction.GetComboValue(cmbPartyName))
                    {
                        MixModeVal = 1;
                        MixModeFlag = true;
                        PrintAsk = 0;

                        CalculateTotal();
                        partialPay = new Vouchers.PartialPayment(ID, Convert.ToDouble(txtGrandTotal.Text.Trim()), ObjFunction.GetComboValue(cmbPaymentType), ObjFunction.GetComboValue(cmbPartyName));
                        ObjFunction.OpenForm(partialPay);
                        partialPay.Left = 15;
                        if (((PartialPayment)partialPay).DS == DialogResult.Cancel)
                        {
                            MFlag = false;
                            MixModeVal = 0;
                            //if (MixModeFlag == true)
                            //    ObjTrans.Execute("Update TVoucherEntry set MixMode=0 Where PKVoucherNo=" + ID + "", CommonFunctions.ConStr);
                            //return;
                        }
                        else if (((PartialPayment)partialPay).DS == DialogResult.OK)
                        {
                            cmbPaymentType.SelectedValue = "3";
                            MFlag = true;
                            btnSave_Click(sender, e);
                        }

                        //btnCreditSave_Click(sender, new EventArgs());
                    }
                    //else
                    //{
                    //    MixModeVal = 0;
                    //    MixModeFlag = false;
                    //    btnCashSave_Click(sender, new EventArgs());
                    //}
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private double CalculateTotal_Billed()
        {
            try
            {
                double TotalFinal = 0;

                for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                {
                    //if (dgBill.Rows[i].Cells[ColIndex.SchemeToNo].FormattedValue.ToString() == "")
                    {
                        TotalFinal += Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Amount].FormattedValue.ToString());
                    }
                }
                return TotalFinal;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return 0;
            }

        }

        private void btnInsScheme_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgBill.Rows.Count > 0)
                {

                    double BilledAmt = CalculateTotal_Billed();

                    Form NewF = new Vouchers.Scheme(BilledAmt, dgBill, false, dtTRewardDtls, ObjFunction.GetComboValue(cmbPartyName), dtpBillDate.Value, MTDSchDetailsNo);
                    ObjFunction.OpenForm(NewF);
                    NewF.Left = 15;
                    if (((Scheme)NewF).DS == DialogResult.Cancel)
                    {
                        dgBill.CurrentCell = dgBill.Rows[dgBill.Rows.Count - 1].Cells[ColIndex.ItemName];
                        dgBill.Focus();
                        return;
                    }
                    else
                    {

                        Form temp = ((Scheme)NewF).FrmTemp;
                        SaveData(temp);


                        dtTRewardToFrom = ((Scheme)NewF).dtTRewardToFrom;
                        dtTRewardDtls = ((Scheme)NewF).dtTRewardDtls;

                        txtSchemeDisc.Text = ((dtTRewardDtls.Compute("Sum(DiscAmount)", "").ToString() == "") ? "0.00" : dtTRewardDtls.Compute("Sum(DiscAmount)", "").ToString());

                        DataRow[] drTo = dtTRewardToFrom.Select("TypeNo=2");

                        string strSchemDtlsNo = "";
                        for (int i = 0; i < dgBill.Rows.Count; i++)
                        {
                            if (dgBill.Rows[i].Cells[ColIndex.SchemeDetailsNo].FormattedValue != "" &&
                                (dgBill.Rows[i].Cells[ColIndex.RewardFromNo].FormattedValue != "" ||
                                dgBill.Rows[i].Cells[ColIndex.RewardToNo].FormattedValue != ""))
                            {
                                if (strSchemDtlsNo == "")
                                    strSchemDtlsNo += dgBill.Rows[i].Cells[ColIndex.SchemeDetailsNo].FormattedValue.ToString();
                                else
                                    strSchemDtlsNo += "," + dgBill.Rows[i].Cells[ColIndex.SchemeDetailsNo].FormattedValue.ToString();
                            }
                        }
                        if (strSchemDtlsNo == "") strSchemDtlsNo = "0";
                        foreach (DataRow row in drTo)
                        {
                            string sql = " SELECT (SELECT ItemName FROM dbo.MStockItems_V(NULL, MSchemeToDetails.ItemNo, NULL, NULL, NULL, NULL, NULL) AS MStockItems_V_1) AS ItemName, MSchemeToDetails.ItemNo,MSchemeToDetails.UomNo, MUOM.UOMName, MSchemeToDetails.FkRateSettingNo, MSchemeToDetails.Quantity,CAST((MSchemeToDetails.MRP * MSchemeToDetails.Quantity) * (MSchemeToDetails.DiscPercentage / 100) AS numeric(18, 2)) AS DiscAmt,MRP " +
                                       " FROM  MSchemeToDetails INNER JOIN MUOM ON MSchemeToDetails.UomNo = MUOM.UOMNo " +
                                       " WHERE     (MSchemeToDetails.PkSrNo = " + Convert.ToInt64(row[5].ToString()) + ") AND MSchemeToDetails.SchemeDetailsNo Not in (" + strSchemDtlsNo + ")";

                            DataTable dtToDetails = ObjFunction.GetDataView(sql).Table;
                            if (dtToDetails.Rows.Count > 0)
                            {
                                int RowCount = dgBill.Rows.Count - 1;
                                dgBill.Rows.Add();
                                dgBill.Rows[RowCount].Cells[ColIndex.ItemName].Value = dtToDetails.Rows[0].ItemArray[0].ToString();
                                dgBill.Rows[RowCount].Cells[ColIndex.ItemNo].Value = dtToDetails.Rows[0].ItemArray[1].ToString();
                                dgBill.Rows[RowCount].Cells[ColIndex.UOMNo].Value = dtToDetails.Rows[0].ItemArray[2].ToString();
                                dgBill.Rows[RowCount].Cells[ColIndex.UOM].Value = dtToDetails.Rows[0].ItemArray[3].ToString();
                                dgBill.Rows[RowCount].Cells[ColIndex.Rate].Value = dtToDetails.Rows[0].ItemArray[7].ToString();
                                dgBill.Rows[RowCount].Cells[ColIndex.Quantity].Value = dtToDetails.Rows[0].ItemArray[5].ToString();
                                dgBill.Rows[RowCount].Cells[ColIndex.DiscRupees].Value = dtToDetails.Rows[0].ItemArray[6].ToString();
                                dgBill.Rows[RowCount].Cells[ColIndex.PkRateSettingNo].Value = dtToDetails.Rows[0].ItemArray[4].ToString();
                                dgBill.Rows[RowCount].Cells[ColIndex.SchemeToNo].Value = row[5].ToString();
                                dgBill.Rows[RowCount].Cells[ColIndex.SchemeDetailsNo].Value = row[4].ToString();
                                dgBill.Rows[RowCount].Cells[ColIndex.StockCompanyNo].Value = DBGetVal.CompanyNo;
                                pnlItemName.Visible = false;
                                BindGrid(RowCount);
                            }

                        }
                        CalculateTotal();
                        dgBill.CurrentCell = dgBill.Rows[dgBill.Rows.Count - 1].Cells[ColIndex.ItemName];
                        dgBill.Focus();

                        for (int i = 0; i < ((Scheme)NewF).dtDeleteReward.Rows.Count; i++)
                        {
                            DeleteDtls(Convert.ToInt32(((Scheme)NewF).dtDeleteReward.Rows[i].ItemArray[0].ToString()), Convert.ToInt32(((Scheme)NewF).dtDeleteReward.Rows[i].ItemArray[1].ToString()));
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        public void SaveData(Form Temp)
        {
            try
            {
                Scheme frmsch = ((Scheme)Temp);
                while (frmsch.dtTRewardDtls.Rows.Count > 0)
                    frmsch.dtTRewardDtls.Rows.RemoveAt(0);

                #region init some datatable values
                for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                {
                    if (dgBill.Rows[i].Cells[ColIndex.SchemeDetailsNo].FormattedValue.ToString() != "" ||
                        dgBill.Rows[i].Cells[ColIndex.SchemeFromNo].FormattedValue.ToString() != "" ||
                        dgBill.Rows[i].Cells[ColIndex.SchemeToNo].FormattedValue.ToString() != "")
                    {
                        if (dgBill.Rows[i].Cells[ColIndex.SchemeDetailsNo].FormattedValue.ToString() != "" &&
                            dgBill.Rows[i].Cells[ColIndex.SchemeFromNo].FormattedValue.ToString() != "" &&
                            dgBill.Rows[i].Cells[ColIndex.SchemeToNo].FormattedValue.ToString() == "")
                        {

                            dgBill.Rows[i].Cells[ColIndex.SchemeDetailsNo].Value = "";
                            dgBill.Rows[i].Cells[ColIndex.SchemeFromNo].Value = "";
                            dgBill.Rows[i].Cells[ColIndex.SchemeToNo].Value = "";

                        }
                        else if (dgBill.Rows[i].Cells[ColIndex.SchemeDetailsNo].FormattedValue.ToString() != "" &&
                            dgBill.Rows[i].Cells[ColIndex.SchemeFromNo].FormattedValue.ToString() == "" &&
                            dgBill.Rows[i].Cells[ColIndex.SchemeToNo].FormattedValue.ToString() != "")// && dgBill.Rows[i].Cells[ColIndex.RewardFromNo].FormattedValue.ToString() != "" && dgBill.Rows[i].Cells[ColIndex.RewardToNo].FormattedValue.ToString() != "")
                        {

                            DataRow[] dr = frmsch.dtTRewardDtls.Select("SchemeDetailsNo=" + dgBill.Rows[i].Cells[ColIndex.SchemeDetailsNo].FormattedValue.ToString());
                            if (dr.Length != 0)
                            {
                                DeleteDtls(6, Convert.ToInt64(dr[0].ItemArray[Scheme.ColTRewardDtls.PkSrNo].ToString()));// 9 For RewardDetailsNo
                                DeleteDtls(5, Convert.ToInt64(dr[0].ItemArray[Scheme.ColTRewardDtls.RewardNo].ToString()));// 11 For RewardTo
                                dgBill.CurrentCell = dgBill[ColIndex.ItemName, i];
                                delete_rowReward();
                                i--;
                            }

                        }
                    }
                }


                int cntReward = 0;


                DataTable dtTempRwdtls = frmsch.dtTRewardDtls.Clone();
                for (int i = 0; i < frmsch.dtTRewardDtls.Rows.Count; i++)
                {
                    DataRow drTemp = dtTempRwdtls.NewRow();
                    for (int j = 0; j < dtTempRwdtls.Columns.Count; j++)
                        drTemp[j] = frmsch.dtTRewardDtls.Rows[i].ItemArray[j].ToString();
                    dtTempRwdtls.Rows.Add(drTemp);
                }
                DataTable dtTempRwToFromdtls = frmsch.dtTRewardToFrom.Clone();
                for (int i = 0; i < frmsch.dtTRewardToFrom.Rows.Count; i++)
                {
                    DataRow drTemp = dtTempRwToFromdtls.NewRow();
                    for (int j = 0; j < dtTempRwToFromdtls.Columns.Count; j++)
                        drTemp[j] = frmsch.dtTRewardToFrom.Rows[i].ItemArray[j].ToString();
                    dtTempRwToFromdtls.Rows.Add(drTemp);
                }

                while (frmsch.dtTRewardDtls.Rows.Count > 0)
                    frmsch.dtTRewardDtls.Rows.RemoveAt(0);

                while (frmsch.dtTRewardToFrom.Rows.Count > 0)
                    frmsch.dtTRewardToFrom.Rows.RemoveAt(0);
                #endregion

                #region for TSKUC & TSKU with Product
                for (int i = 0; i < frmsch.dgInsTSKU.Rows.Count; i++)
                {
                    if (frmsch.dgInsTSKU.Rows[i].Cells[Scheme.ColIndexGrid.Select].Style.BackColor == Scheme.ChkValue.True)
                    {
                        DataTable DtDetails = ObjFunction.GetDataView("SELECT MSchemeDetails.DiscPercentage, MSchemeDetails.DiscAmount*" + Convert.ToDouble(frmsch.dgInsTSKU.Rows[i].Cells[Scheme.ColIndexGrid.LoyaltyFactor].Value) + ", MSchemeFromDetails.BillAmount FROM MSchemeDetails INNER JOIN MSchemeFromDetails ON MSchemeDetails.PkSrNo = MSchemeFromDetails.SchemeDetailsNo WHERE (MSchemeDetails.PkSrNo = " + Convert.ToInt64(frmsch.dgInsTSKU.Rows[i].Cells[Scheme.ColIndexGrid.SchDtlsPksrNo].Value) + ")").Table;
                        DataRow dr = frmsch.dtTRewardDtls.NewRow();
                        //(dtTempRwdtls.Rows.Count <= cntReward)
                        DataRow[] drRow = dtTempRwdtls.Select("SchemeDetailsNo='" + frmsch.dgInsTSKU.Rows[i].Cells[Scheme.ColIndexGrid.SchDtlsPksrNo].Value + "'");
                        dr[Scheme.ColTRewardDtls.PkSrNo] = (drRow.Length <= 0) ? "0" : dtTempRwdtls.Select("SchemeDetailsNo='" + frmsch.dgInsTSKU.Rows[i].Cells[Scheme.ColIndexGrid.SchDtlsPksrNo].Value + "'")[0].ItemArray[0].ToString();// dtTempRwdtls.Rows[cntReward].ItemArray[0].ToString();
                        dr[Scheme.ColTRewardDtls.RewardNo] = (drRow.Length <= 0) ? "0" : dtTempRwdtls.Select("SchemeDetailsNo='" + frmsch.dgInsTSKU.Rows[i].Cells[Scheme.ColIndexGrid.SchDtlsPksrNo].Value + "'")[0].ItemArray[1].ToString();//dtTempRwdtls.Rows[cntReward].ItemArray[1].ToString();
                        dr[Scheme.ColTRewardDtls.SchemeNo] = frmsch.dgInsTSKU.Rows[i].Cells[Scheme.ColIndexGrid.SchemeNo].Value;
                        dr[Scheme.ColTRewardDtls.SchemeDetailsNo] = frmsch.dgInsTSKU.Rows[i].Cells[Scheme.ColIndexGrid.SchDtlsPksrNo].Value;
                        dr[Scheme.ColTRewardDtls.SchemeType] = frmsch.dgInsTSKU.Rows[i].Cells[Scheme.ColIndexGrid.SchemeTypeNo].Value;
                        dr[Scheme.ColTRewardDtls.DiscPer] = Convert.ToDouble(DtDetails.Rows[0].ItemArray[0].ToString());
                        dr[Scheme.ColTRewardDtls.DiscAmount] = Convert.ToDouble(DtDetails.Rows[0].ItemArray[1].ToString());
                        dr[Scheme.ColTRewardDtls.SchemeAmount] = Convert.ToDouble(DtDetails.Rows[0].ItemArray[2].ToString());
                        dr[Scheme.ColTRewardDtls.Status] = 0;
                        dr[Scheme.ColTRewardDtls.SchemeAchieverNo] = 0;
                        frmsch.dtTRewardDtls.Rows.Add(dr);
                        cntReward++;

                        DataTable SchemeFrom = ObjFunction.GetDataView("SELECT PkSrNo,ItemNo,FKRateSettingNo,MRP FROM MSchemeFromDetails WHERE (SchemeDetailsNo = " + Convert.ToInt64(frmsch.dgInsTSKU.Rows[i].Cells[Scheme.ColIndexGrid.SchDtlsPksrNo].Value) + ")").Table;
                        for (int f = 0; f < SchemeFrom.Rows.Count; f++)
                        {
                            DataRow drTo = frmsch.dtTRewardToFrom.NewRow();
                            drTo[Scheme.ColTRewardToFrom.TypeNo] = 1;
                            drTo[Scheme.ColTRewardToFrom.PkSrNo] = 0;
                            drTo[Scheme.ColTRewardToFrom.RewardNo] = 0;
                            drTo[Scheme.ColTRewardToFrom.SchemeDetailsNo] = frmsch.dgInsTSKU.Rows[i].Cells[Scheme.ColIndexGrid.SchDtlsPksrNo].Value;
                            drTo[Scheme.ColTRewardToFrom.SchemeFromNo] = SchemeFrom.Rows[f].ItemArray[0].ToString();
                            drTo[Scheme.ColTRewardToFrom.FkStockNo] = 0;
                            drTo[Scheme.ColTRewardToFrom.ItemNo] = SchemeFrom.Rows[f].ItemArray[1].ToString();
                            drTo[Scheme.ColTRewardToFrom.LoyaltyFactor] = 0;
                            frmsch.dtTRewardToFrom.Rows.Add(drTo);
                            for (int d = 0; d < dgBill.Rows.Count - 1; d++)
                            {
                                if (Convert.ToInt64(dgBill.Rows[d].Cells[ColIndex.ItemNo].FormattedValue.ToString()) ==
                                    Convert.ToInt64(SchemeFrom.Rows[f].ItemArray[1].ToString()) &&
                                    ObjQry.ReturnDouble("Select MRP From MRateSetting Where PksrNo=" + dgBill.Rows[d].Cells[ColIndex.PkRateSettingNo].FormattedValue + "", CommonFunctions.ConStr) ==
                                    Convert.ToDouble(SchemeFrom.Rows[f].ItemArray[3].ToString()))
                                {
                                    dgBill.Rows[d].Cells[ColIndex.SchemeDetailsNo].Value = frmsch.dgInsTSKU.Rows[i].Cells[Scheme.ColIndexGrid.SchDtlsPksrNo].Value;
                                    dgBill.Rows[d].Cells[ColIndex.SchemeFromNo].Value = SchemeFrom.Rows[f].ItemArray[0].ToString();
                                    break;
                                }
                            }
                        }

                        DataTable SchemeTo = ObjFunction.GetDataView("SELECT PkSrNo,ItemNo FROM MSchemeToDetails WHERE (SchemeDetailsNo =  " + Convert.ToInt64(frmsch.dgInsTSKU.Rows[i].Cells[Scheme.ColIndexGrid.SchDtlsPksrNo].Value) + " )").Table;
                        for (int t = 0; t < SchemeTo.Rows.Count; t++)
                        {
                            DataRow drTo = frmsch.dtTRewardToFrom.NewRow();
                            drTo[Scheme.ColTRewardToFrom.TypeNo] = 2;
                            drTo[Scheme.ColTRewardToFrom.PkSrNo] = 0;
                            drTo[Scheme.ColTRewardToFrom.RewardNo] = 0;
                            drTo[Scheme.ColTRewardToFrom.SchemeDetailsNo] = frmsch.dgInsTSKU.Rows[i].Cells[Scheme.ColIndexGrid.SchDtlsPksrNo].Value;
                            drTo[Scheme.ColTRewardToFrom.SchemeFromNo] = SchemeTo.Rows[t].ItemArray[0].ToString();
                            drTo[Scheme.ColTRewardToFrom.FkStockNo] = 0;
                            drTo[Scheme.ColTRewardToFrom.ItemNo] = SchemeTo.Rows[t].ItemArray[1].ToString();
                            drTo[Scheme.ColTRewardToFrom.LoyaltyFactor] = Convert.ToDouble(frmsch.dgInsTSKU.Rows[i].Cells[Scheme.ColIndexGrid.LoyaltyFactor].Value);
                            frmsch.dtTRewardToFrom.Rows.Add(drTo);
                        }
                    }
                }
                #endregion

            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void DisplayParkingDetails(long ParkingBillNo)
        {
            try
            {
                //string strQuery = "SELECT     MStockItems.ItemName, TParkingBillDetails.Qty, MUOM.UOMName, MRateSetting." + ObjFunction.GetComboValueString(cmbRateType) + ", MStockBarcode.Barcode,MStockBarcode.PkStockBarcodeNo,MStockItems.ItemNo, MUOM.UOMNo,   " +
                //    " GetItemTaxAll_1.TaxLedgerNo, GetItemTaxAll_1.SalesLedgerNo, MRateSetting.PkSrNo, GetItemTaxAll_1.PkSrNo AS Expr1,  " +
                //    " MRateSetting.StockConversion, MRateSetting.MKTQty, GetItemTaxAll_1.Percentage,TParkingBillDetails.ItemDisc,MStockItems.CompanyNo FROM dbo.MStockItems_V(NULL, NULL, NULL, NULL, NULL, NULL, NULL) AS MStockItems INNER JOIN " +
                //    " TParkingBillDetails ON MStockItems.ItemNo = TParkingBillDetails.ItemNo INNER JOIN MUOM ON TParkingBillDetails.UOMNo = MUOM.UOMNo INNER JOIN " +
                //    " MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo AND TParkingBillDetails.FkRateSettingNo = MRateSetting.PkSrNo INNER JOIN dbo.GetItemTaxAll(NULL, NULL, " + GroupType.SalesAccount + ", " + ObjFunction.GetComboValueString(cmbTaxType) + ", NULL) AS GetItemTaxAll_1 ON MStockItems.ItemNo = GetItemTaxAll_1.ItemNo INNER JOIN " +
                //    " MStockBarcode ON MStockItems.ItemNo = MStockBarcode.ItemNo WHERE     (TParkingBillDetails.ParkingBillNo = " + ParkingBillNo + ")";
                string strQuery = "SELECT    MStockItems.ShowItemName AS ItemName, TParkingBillDetails.Qty, MUOM.UOMName, TParkingBillDetails.Rate, MStockBarcode.Barcode,MStockBarcode.PkStockBarcodeNo,MStockItems.ItemNo, MUOM.UOMNo,   " +
                " GetItemTaxAll_1.TaxLedgerNo, GetItemTaxAll_1.SalesLedgerNo, MRateSetting.PkSrNo, GetItemTaxAll_1.PkSrNo AS Expr1,  " +
                " MRateSetting.StockConversion, MRateSetting.MKTQty, GetItemTaxAll_1.Percentage,TParkingBillDetails.ItemDisc,MStockItems.CompanyNo, MStockItems_1.DiscountType,MStockItems.GodownNo, MStockItems_1.HamaliInKg, " +
                " GetItemTaxAll_1.HSNNo, GetItemTaxAll_1.HSNCode, GetItemTaxAll_1.FkTaxSettingNo,MRateSetting.MRP, " +
                " GetItemTaxAll_1.IGSTPercent, GetItemTaxAll_1.CGSTPercent, GetItemTaxAll_1.SGSTPercent, " +
                " GetItemTaxAll_1.UTGSTPercent, GetItemTaxAll_1.CessPercent " +
                " FROM  MStockItems INNER JOIN " +
                " TParkingBillDetails ON MStockItems.ItemNo = TParkingBillDetails.ItemNo INNER JOIN MUOM ON TParkingBillDetails.UOMNo = MUOM.UOMNo INNER JOIN " +
                " MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo AND TParkingBillDetails.FkRateSettingNo = MRateSetting.PkSrNo INNER JOIN dbo.GetItemTaxAll(NULL, NULL, " + GroupType.SalesAccount + ", " + ObjFunction.GetComboValueString(cmbTaxType) + ", NULL) AS GetItemTaxAll_1 ON MStockItems.ItemNo = GetItemTaxAll_1.ItemNo INNER JOIN " +
                " MStockBarcode ON MStockItems.ItemNo = MStockBarcode.ItemNo    INNER JOIN MStockItems AS MStockItems_1 ON MStockItems.ItemNo = MStockItems_1.ItemNo " +
                " WHERE     (TParkingBillDetails.ParkingBillNo = " + ParkingBillNo + ") order by TParkingBillDetails.PkSrNo";

                DataTable dtP = ObjFunction.GetDataView(strQuery).Table;
                for (int i = 0; i < dtP.Rows.Count; i++)
                {
                    int index = dgBill.Rows.Count - 1;
                    dgBill.Rows[index].Cells[ColIndex.SrNo].Value = (index + 1);
                    dgBill.Rows[index].Cells[ColIndex.ItemName].Value = dtP.Rows[i].ItemArray[0].ToString();
                    dgBill.Rows[index].Cells[ColIndex.Quantity].Value = dtP.Rows[i].ItemArray[1].ToString();
                    dgBill.Rows[index].Cells[ColIndex.UOM].Value = dtP.Rows[i].ItemArray[2].ToString();
                    dgBill.Rows[index].Cells[ColIndex.Rate].Value = Convert.ToDouble(dtP.Rows[i].ItemArray[3].ToString()).ToString(Format.DoubleFloating);
                    dgBill.Rows[index].Cells[ColIndex.NetRate].Value = "0";
                    dgBill.Rows[index].Cells[ColIndex.DiscPercentage].Value = "0";
                    dgBill.Rows[index].Cells[ColIndex.DiscAmount].Value = "0";
                    dgBill.Rows[index].Cells[ColIndex.DiscRupees].Value = dtP.Rows[i].ItemArray[15].ToString();
                    dgBill.Rows[index].Cells[ColIndex.DiscPercentage2].Value = "0";
                    dgBill.Rows[index].Cells[ColIndex.DiscAmount2].Value = "0";
                    dgBill.Rows[index].Cells[ColIndex.DiscRupees2].Value = "0";
                    dgBill.Rows[index].Cells[ColIndex.NetAmt].Value = "0";
                    dgBill.Rows[index].Cells[ColIndex.Amount].Value = "0";
                    dgBill.Rows[index].Cells[ColIndex.Barcode].Value = dtP.Rows[i].ItemArray[4].ToString();
                    dgBill.Rows[index].Cells[ColIndex.PkStockTrnNo].Value = "0";
                    dgBill.Rows[index].Cells[ColIndex.PkBarCodeNo].Value = dtP.Rows[i].ItemArray[5].ToString();
                    dgBill.Rows[index].Cells[ColIndex.PkVoucherNo].Value = "0";
                    dgBill.Rows[index].Cells[ColIndex.ItemNo].Value = dtP.Rows[i].ItemArray[6].ToString();
                    dgBill.Rows[index].Cells[ColIndex.UOMNo].Value = dtP.Rows[i].ItemArray[7].ToString();
                    dgBill.Rows[index].Cells[ColIndex.TaxLedgerNo].Value = dtP.Rows[i].ItemArray[8].ToString();
                    dgBill.Rows[index].Cells[ColIndex.SalesLedgerNo].Value = dtP.Rows[i].ItemArray[9].ToString();
                    dgBill.Rows[index].Cells[ColIndex.PkRateSettingNo].Value = dtP.Rows[i].ItemArray[10].ToString();
                    dgBill.Rows[index].Cells[ColIndex.PkItemTaxInfo].Value = dtP.Rows[i].ItemArray[11].ToString();
                    dgBill.Rows[index].Cells[ColIndex.StockFactor].Value = dtP.Rows[i].ItemArray[12].ToString();
                    dgBill.Rows[index].Cells[ColIndex.ActualQty].Value = "0";
                    dgBill.Rows[index].Cells[ColIndex.MKTQuantity].Value = dtP.Rows[i].ItemArray[13].ToString();
                    dgBill.Rows[index].Cells[ColIndex.TaxPercentage].Value = dtP.Rows[i].ItemArray[14].ToString();
                    dgBill.Rows[index].Cells[ColIndex.StockCompanyNo].Value = dtP.Rows[i].ItemArray[16].ToString();
                    dgBill.Rows[index].Cells[ColIndex.DiscountType].Value = dtP.Rows[i].ItemArray[17].ToString();
                    dgBill.Rows[index].Cells[ColIndex.GodownNo].Value = (ObjFunction.GetAppSettings(AppSettings.S_DefaultStockLocation).ToString() == "0") ? dtP.Rows[i].ItemArray[18].ToString() : ObjFunction.GetAppSettings(AppSettings.S_DefaultStockLocation).ToString();
                    dgBill.Rows[index].Cells[ColIndex.HamaliInKg].Value = dtP.Rows[i].ItemArray[19].ToString();
                    dgBill.Rows[index].Cells[ColIndex.HSNCode].Value = dtP.Rows[i].ItemArray[21].ToString();
                    dgBill.Rows[index].Cells[ColIndex.MRP].Value = dtP.Rows[i]["MRP"].ToString();
                    dgBill.Rows[index].Cells[ColIndex.HSNCode].Value = dtP.Rows[i]["HSNCode"].ToString();
                    dgBill.Rows[index].Cells[ColIndex.IGSTPercent].Value = Convert.ToDouble(dtP.Rows[i]["IGSTPercent"].ToString());
                    dgBill.Rows[index].Cells[ColIndex.CGSTPercent].Value = Convert.ToDouble(dtP.Rows[i]["CGSTPercent"].ToString());
                    dgBill.Rows[index].Cells[ColIndex.SGSTPercent].Value = Convert.ToDouble(dtP.Rows[i]["SGSTPercent"].ToString());
                    dgBill.Rows[index].Cells[ColIndex.UTGSTPercent].Value = Convert.ToDouble(dtP.Rows[i]["UTGSTPercent"].ToString());
                    dgBill.Rows[index].Cells[ColIndex.CessPercent].Value = Convert.ToDouble(dtP.Rows[i]["CessPercent"].ToString());
                    dgBill.Rows[index].Cells[ColIndex.IGSTAmount].Value = 0;
                    dgBill.Rows[index].Cells[ColIndex.CGSTAmount].Value = 0;
                    dgBill.Rows[index].Cells[ColIndex.SGSTAmount].Value = 0;
                    dgBill.Rows[index].Cells[ColIndex.UTGSTAmount].Value = 0;
                    dgBill.Rows[index].Cells[ColIndex.CessAmount].Value = 0;
                    dgBill.Rows.Add();
                }
                CalculateTotal();
                //Application.DoEvents();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void DisplayCollectionDetails()
        {
            try
            {
                lblTotalCollection.Text = "0.00";
                lblOw.Text = "0.00";
                dgCollectionDetails.DataSource = null;
                dgCollectionDetails.RowsDefaultCellStyle.BackColor = Color.FromArgb(255, 255, 210);
                if (ObjFunction.GetComboValue(cmbPaymentType) == 3)
                {
                    //pnlCollectionDetails.Visible = true;
                    long RefNo = ObjQry.ReturnLong("Select RefNo From TVoucherRefDEtails Where FKVoucherTrnNo in (Select PKvoucherTrnNo From TVoucherDetails Where FKVoucherNo=" + ID + " AND VoucherSrNo=1)", CommonFunctions.ConStr);
                    DataTable dt = ObjFunction.GetDataView("SELECT MPayType.PayTypeName AS Name, SUM(TVoucherRefDetails.Amount) AS Amount FROM TVoucherEntry INNER JOIN " +
                        " TVoucherDetails ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN TVoucherRefDetails ON TVoucherDetails.PkVoucherTrnNo = TVoucherRefDetails.FkVoucherTrnNo INNER JOIN " +
                        " MPayType ON TVoucherEntry.PayTypeNo = MPayType.PKPayTypeNo INNER JOIN " +
                        " TVoucherPayTypeDetails ON TVoucherEntry.PkVoucherNo = TVoucherPayTypeDetails.FKReceiptVoucherNo WHERE (TVoucherDetails.LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + ") AND (TVoucherEntry.VoucherTypeCode = " + VchType.SalesReceipt + ") AND (TVoucherRefDetails.RefNo = " + RefNo + ") AND (TVoucherEntry.CompanyNo = " + DBGetVal.CompanyNo + ") " +
                        " GROUP BY MPayType.PayTypeName").Table;
                    double CollectTotal = 0;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        CollectTotal += Convert.ToDouble(dt.Rows[i].ItemArray[1].ToString());
                    }
                    if (Convert.ToDouble(Convert.ToDouble(txtGrandTotal.Text) - CollectTotal) > 0 && CollectTotal > 0)
                    {
                        DataRow dr = dt.NewRow();
                        dr[0] = "Pending";
                        dr[1] = Convert.ToDouble(Convert.ToDouble(txtGrandTotal.Text) - CollectTotal).ToString(Format.DoubleFloating);
                        dt.Rows.Add(dr);
                    }
                    dgCollectionDetails.DataSource = dt.DefaultView;

                    if (dt.Rows.Count > 0)
                    {
                        dgCollectionDetails.Columns[0].Width = 75;
                        dgCollectionDetails.Columns[1].Width = 100;
                        dgCollectionDetails.ColumnHeadersVisible = false;
                        dgCollectionDetails.RowsDefaultCellStyle.BackColor = Color.FromArgb(255, 255, 210);
                        dgCollectionDetails.BackgroundColor = Color.FromArgb(255, 255, 210);
                        dgCollectionDetails.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                        // dgCollectionDetails.Height = 107;
                        lblCollectionDetails.Font = new Font("Verdana", 10, FontStyle.Bold);
                        //double CollectTotal = 0;
                        //for (int i = 0; i < dgCollectionDetails.Rows.Count; i++)
                        //{
                        //    CollectTotal += Convert.ToDouble(dgCollectionDetails.Rows[i].Cells[1].Value);
                        //}

                        //CollectTotal = ObjQry.ReturnDouble("SELECT SUM(TVoucherRefDetails.Amount) AS Amount FROM TVoucherEntry INNER JOIN " +
                        // " TVoucherDetails ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN TVoucherRefDetails ON TVoucherDetails.PkVoucherTrnNo = TVoucherRefDetails.FkVoucherTrnNo INNER JOIN " +
                        // " MPayType ON TVoucherEntry.PayTypeNo = MPayType.PKPayTypeNo WHERE (TVoucherDetails.LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + ") AND (TVoucherEntry.VoucherTypeCode = " + VchType.SalesReceipt + ") AND (TVoucherRefDetails.RefNo = " + RefNo + ") AND (TVoucherEntry.CompanyNo = " + DBGetVal.CompanyNo + ") ", CommonFunctions.ConStr);

                        //lblTotalCollection.Text = CollectTotal.ToString(Format.DoubleFloating);
                        //lblOw.Text = (Convert.ToDouble(lblGrandTotal.Text) - CollectTotal).ToString(Format.DoubleFloating);
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void pnlCollectionDetails_Paint(object sender, PaintEventArgs e)
        {
            //e.Graphics.DrawLine(new Pen(Color.SteelBlue, 5), 0, 135, 214, 135);
        }

        #region Discount Structure related Methods

        private void dtpBillDate_Leave(object sender, EventArgs e)
        {
            InitItemLeveDiscount();
            if (ID == 0)
            {
                tempDate = dtpBillDate.Value.Date;
                ObjFunction.GetFinancialYear(dtpBillDate.Value, out dtFrom, out dtTo);
                txtInvNo.Text = (ObjQry.ReturnLong("Select max(VoucherUserNo) from TVoucherEntry Where VoucherTypeCode=" + VoucherType + " AND VoucherDate>='" + dtFrom.Date + "' AND VoucherDate<='" + dtTo.Date + "'", CommonFunctions.ConStr) + 1).ToString();
            }
        }

        #endregion

        private void DeleteSchemeData()
        {
            for (int i = 0; i < dgBill.Rows.Count; i++)
            {
                if (dgBill.Rows[i].Cells[ColIndex.SchemeDetailsNo].EditedFormattedValue.ToString().Trim() != "")
                {
                    if (dgBill.Rows[i].Cells[ColIndex.PkStockTrnNo].EditedFormattedValue.ToString() != "" &&
                        dgBill.Rows[i].Cells[ColIndex.PkStockTrnNo].EditedFormattedValue.ToString().Trim() != "0")
                        DeleteDtls(1, Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.PkStockTrnNo].EditedFormattedValue.ToString()));


                    if (dgBill.Rows[i].Cells[ColIndex.SchemeFromNo].EditedFormattedValue.ToString() != "")
                    {
                        dgBill.Rows[i].Cells[ColIndex.SchemeFromNo].Value = null;
                        dgBill.Rows[i].Cells[ColIndex.SchemeDetailsNo].Value = null;
                        dgBill.Rows[i].Cells[ColIndex.SchemeToNo].Value = null;
                        txtSchemeDisc.Text = "0.00";
                    }
                    else
                    {
                        dgBill.Rows.RemoveAt(i);
                        i--;
                    }

                }
            }

            CalculateTotal();

        }

        public void SetSchemeData()
        {
            try
            {
                if (dgBill.Rows.Count > 0)
                {

                    double BilledAmt = CalculateTotal_Billed();
                    //dtTRewardDtls.Rows.Clear();
                    Form NewF = new Vouchers.Scheme(BilledAmt, dgBill, false, dtTRewardDtls, ObjFunction.GetComboValue(cmbPartyName), dtpBillDate.Value, MTDSchDetailsNo);
                    ((Scheme)NewF).Scheme_Load(new object(), new EventArgs());
                    ((Scheme)NewF).CheckAllSchemes(chkItemLevelDisc.Checked, false);//chkFooterLevelDisc.Checked);

                    //((Scheme)NewF).BtnOk_Click(new object(), new EventArgs());
                    NewF.Left = 15;
                    //if (((Scheme)NewF).DS == DialogResult.Cancel)
                    //{
                    //    dgBill.CurrentCell = dgBill.Rows[dgBill.Rows.Count - 1].Cells[ColIndex.ItemName];
                    //    dgBill.Focus();
                    //    return;
                    //}
                    //else
                    {

                        ///Form temp = ((Scheme)NewF).FrmTemp;
                        SaveData(((Scheme)NewF));

                        dtTRewardToFrom = ((Scheme)NewF).dtTRewardToFrom;
                        dtTRewardDtls = ((Scheme)NewF).dtTRewardDtls;

                        //for (int i = 0; i < dtTRewardDtls.Rows.Count > 0; i++)
                        //{
                        //    if (dtTRewardDtls.Rows[i].ItemArray[Scheme.ColIndexGrid.SchemeTypeNo].ToString() != "2" && dtTRewardDtls.Rows[i].ItemArray[Scheme.ColIndexGrid.SchemeTypeNo].ToString() != "4")
                        //    {
                        //        if(CheckItemLevelDiscountDetails(RowCount, Convert.ToDouble(dtToDetails.Rows[0].ItemArray[6].ToString())
                        //    }
                        //}


                        //if (chkItemLevelDisc.Checked)
                        //{
                        DataRow[] drTo = dtTRewardToFrom.Select("TypeNo=2");

                        string strSchemDtlsNo = "";
                        for (int i = 0; i < dgBill.Rows.Count; i++)
                        {
                            if (dgBill.Rows[i].Cells[ColIndex.SchemeDetailsNo].FormattedValue.ToString() != "" &&
                                dgBill.Rows[i].Cells[ColIndex.RewardFromNo].FormattedValue.ToString() != "" &&
                                dgBill.Rows[i].Cells[ColIndex.RewardToNo].FormattedValue.ToString() != "")
                            {
                                if (strSchemDtlsNo == "")
                                    strSchemDtlsNo += dgBill.Rows[i].Cells[ColIndex.SchemeDetailsNo].FormattedValue.ToString();
                                else
                                    strSchemDtlsNo += "," + dgBill.Rows[i].Cells[ColIndex.SchemeDetailsNo].FormattedValue.ToString();
                            }
                        }
                        if (strSchemDtlsNo == "") strSchemDtlsNo = "0";
                        foreach (DataRow row in drTo)
                        {
                            string sql = " SELECT (SELECT ItemName FROM dbo.MStockItems_V(NULL, MSchemeToDetails.ItemNo, NULL, NULL, NULL, NULL, NULL) AS MStockItems_V_1) AS ItemName, MSchemeToDetails.ItemNo,MSchemeToDetails.UomNo, MUOM.UOMName, MSchemeToDetails.FkRateSettingNo, MSchemeToDetails.Quantity,CAST((MSchemeToDetails.MRP * MSchemeToDetails.Quantity) * (MSchemeToDetails.DiscPercentage / 100) AS numeric(18, 2)) AS DiscAmt,MRP " +
                                       " FROM  MSchemeToDetails INNER JOIN MUOM ON MSchemeToDetails.UomNo = MUOM.UOMNo " +
                                       " WHERE     (MSchemeToDetails.PkSrNo = " + Convert.ToInt64(row[5].ToString()) + ") AND MSchemeToDetails.SchemeDetailsNo Not in (" + strSchemDtlsNo + ")";

                            DataTable dtToDetails = ObjFunction.GetDataView(sql).Table;
                            if (dtToDetails.Rows.Count > 0)
                            {
                                int RowCount = dgBill.Rows.Count - 1;
                                dgBill.Rows.Add();
                                dgBill.Rows[RowCount].Cells[ColIndex.ItemName].Value = dtToDetails.Rows[0].ItemArray[0].ToString();
                                dgBill.Rows[RowCount].Cells[ColIndex.ItemNo].Value = dtToDetails.Rows[0].ItemArray[1].ToString();
                                dgBill.Rows[RowCount].Cells[ColIndex.UOMNo].Value = dtToDetails.Rows[0].ItemArray[2].ToString();
                                dgBill.Rows[RowCount].Cells[ColIndex.UOM].Value = dtToDetails.Rows[0].ItemArray[3].ToString();
                                dgBill.Rows[RowCount].Cells[ColIndex.Rate].Value = dtToDetails.Rows[0].ItemArray[7].ToString();
                                dgBill.Rows[RowCount].Cells[ColIndex.Quantity].Value = Convert.ToDouble(dtToDetails.Rows[0].ItemArray[5].ToString()) * (row[Scheme.ColTRewardToFrom.LoyaltyFactor].ToString() == "" ? 1 : Convert.ToDouble(row[Scheme.ColTRewardToFrom.LoyaltyFactor].ToString()));

                                dgBill.Rows[RowCount].Cells[ColIndex.PkRateSettingNo].Value = dtToDetails.Rows[0].ItemArray[4].ToString();
                                dgBill.Rows[RowCount].Cells[ColIndex.SchemeToNo].Value = row[5].ToString();
                                dgBill.Rows[RowCount].Cells[ColIndex.SchemeDetailsNo].Value = row[4].ToString();
                                dgBill.Rows[RowCount].Cells[ColIndex.StockCompanyNo].Value = DBGetVal.CompanyNo;
                                dgBill.Rows[RowCount].Cells[ColIndex.GodownNo].Value = ObjFunction.GetAppSettings(AppSettings.S_DefaultStockLocation);
                                //if (CheckItemLevelDiscountDetails(RowCount, Convert.ToDouble(dtToDetails.Rows[0].ItemArray[6].ToString())) == false)
                                //{
                                dgBill.Rows[RowCount].Cells[ColIndex.DiscRupees].Value = (row[Scheme.ColTRewardToFrom.LoyaltyFactor].ToString() == "" ? 1 : Convert.ToDouble(row[Scheme.ColTRewardToFrom.LoyaltyFactor].ToString())) * Convert.ToDouble(dtToDetails.Rows[0].ItemArray[6].ToString());
                                dgBill.Rows[RowCount].Cells[ColIndex.DiscountType].Value = "2";
                                dgBill.Rows[RowCount].DefaultCellStyle.ForeColor = Color.Blue;
                                //}
                                //else
                                //  dgBill.Rows[RowCount].Cells[ColIndex.DiscRupees].Value = "0";
                                pnlItemName.Visible = false;
                                DiscFlag = true;
                                BindGrid(RowCount);
                            }

                        }
                        //}

                        //if (chkFooterLevelDisc.Checked)
                        //{
                        txtSchemeDisc.Text = ((dtTRewardDtls.Compute("Sum(DiscAmount)", "").ToString() == "") ? "0.00" : dtTRewardDtls.Compute("Sum(DiscAmount)", "").ToString());
                        //}
                        //else txtSchemeDisc.Text = Format.DoubleFloating;

                        dgBill.CurrentCell = dgBill.Rows[dgBill.Rows.Count - 1].Cells[ColIndex.ItemName];
                        if (ComboFlag == false) dgBill.Focus();
                        DiscFlag = false;

                        for (int i = 0; i < ((Scheme)NewF).dtDeleteReward.Rows.Count; i++)
                        {
                            DeleteDtls(Convert.ToInt32(((Scheme)NewF).dtDeleteReward.Rows[i].ItemArray[0].ToString()), Convert.ToInt32(((Scheme)NewF).dtDeleteReward.Rows[i].ItemArray[1].ToString()));
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnInsSchemeInfo_Click(object sender, EventArgs e)
        {
            //     DeleteSchemeData();
            //   SetSchemeData();
        }


        private void NewItemAdd(string BarCode)
        {
            if (ObjFunction.CheckAllowMenu(10) == false) return;
            Form NewF = new Master.StockItemSAE(-1, BarCode);
            ObjFunction.OpenForm(NewF);

            if (((Master.StockItemSAE)NewF).ShortID != 0)
            {
                //System.Threading.Thread.Sleep(2000);
                string barcode = ObjQry.ReturnString("Select BarCode From MStockBarCode where ItemNo=" + ((Master.StockItemSAE)NewF).ShortID + "", CommonFunctions.ConStr);
                int rwindex = dgBill.CurrentCell.RowIndex;
                dgBill.CurrentRow.Cells[ColIndex.ItemName].Value = barcode;
                dgBill_CellEndEdit(dgBill, new DataGridViewCellEventArgs(ColIndex.ItemName, rwindex));

            }
        }

        private void dgInvSearch_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                e.Value = e.RowIndex + 1;

            }
            else if (e.ColumnIndex == 2)
            {
                e.Value = Convert.ToDateTime(e.Value).ToString("dd-MMM-yy");
            }
        }

        private void dgInvSearch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    long tempNo;
                    e.SuppressKeyPress = true;
                    tempNo = Convert.ToInt64(dgInvSearch.Rows[dgInvSearch.CurrentRow.Index].Cells[4].Value);
                    if (tempNo > 0)
                    {
                        ID = tempNo;
                        SetNavigation();
                        FillControls();
                        btnNew.Enabled = true;
                        btnUpdate.Enabled = true;
                        pnlPartySearch.Visible = false;
                        pnlInvSearch.Visible = false;
                        btnNew.Focus();
                        SearchVisible(false);
                    }
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    e.SuppressKeyPress = true;
                    pnlPartySearch.Visible = false;
                    pnlInvSearch.Visible = false;
                    pnlSearch.Visible = true;
                    txtSearch.Focus();
                    rbType_CheckedChanged(sender, new EventArgs());
                }
                txtSearch.Text = "";
                cmbPartyNameSearch.SelectedIndex = 0;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnSchemePromo_Click(object sender, EventArgs e)
        {
            Form NewF11 = new GetSchemeInfo();
            ObjFunction.OpenForm(NewF11);

            double BilledAmt = CalculateTotal_Billed();
            Form NewF = new Vouchers.SchemePromoInfo(BilledAmt, dgBill, false, dtTRewardDtls, ObjFunction.GetComboValue(cmbPartyName), dtpBillDate.Value, MTDSchDetailsNo, EnrFlag, ID);
            ObjFunction.OpenForm(NewF);
            //((Scheme)NewF).Scheme_Load(new object(), new EventArgs());
            //((Scheme)NewF).CheckAllSchemes(chkItemLevelDisc.Checked, false);//chkFooterLevelDisc.Checked);
            NewF.Left = 15;
        }

        private void txtBrandFilter_TextChanged(object sender, EventArgs e)
        {
            if (txtBrandFilter.Text != "" && BrandFilter != "")
            {
                string str = BrandFilter;
                str = str.Replace("IsActive='true'", "IsActive='true' And  StockGroupName Like '" + txtBrandFilter.Text.Trim() + "'+'%'");
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_Bilingual)) == true)
                    ObjFunction.FillList(lstGroup1Lang, str.Replace("StockGroupName from", "LanguageName from"));
                ObjFunction.FillList(lstGroup1Lang, str.Replace("StockGroupName from", "LanguageName from"));
                ObjFunction.FillList(lstGroup1, str);
            }
            else
            {
                if (BrandFilter != "")
                {
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_Bilingual)) == true)
                        ObjFunction.FillList(lstGroup1Lang, BrandFilter.Replace("StockGroupName from", "LanguageName from"));
                    ObjFunction.FillList(lstGroup1Lang, BrandFilter.Replace("StockGroupName from", "LanguageName from"));
                    ObjFunction.FillList(lstGroup1, BrandFilter);
                }
            }
        }

        private void txtBrandFilter_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                e.SuppressKeyPress = true;
                lstGroup1.Focus();
            }
            else if (e.KeyCode == Keys.Space)
            {
                dgBill.Focus();
                pnlGroup1.Visible = false;
                txtBrandFilter.Text = "";
            }
            else if (e.KeyCode == Keys.Escape)
            {
                e.SuppressKeyPress = true;
                dgBill.Focus();
                txtBrandFilter.Text = "";
                pnlGroup1.Visible = false;

            }
            else if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (lstGroup1.Text != "")
                {
                    pnlGroup1.Visible = false;
                    lblBrandName.Text = lstGroup1.Text;
                    FillItemList(strItemQuery.Length);
                }

            }
        }

        private void txtKg_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                CalculateTotal();
                txtHRs.Focus();
            }
        }

        private void txtHRs_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                CalculateTotal();
                txtChrgRupees1.Focus();
            }
        }

        private void txtHRs_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(((TextBox)sender), 2, 5, JitFunctions.MaskedType.NotNegative);
        }

        private void BTSacn_DoWork(object sender, DoWorkEventArgs e)
        {
            GetCilentData(e);
        }

        private void GetCilentData(DoWorkEventArgs e)
        {
            TcpListener list = null;
            try
            {
                while (true)
                {
                    byte[] b1;
                    Int32 port = 5051;

                    IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
                    for (int i = 0; i < ipHostInfo.AddressList.Length; i++)
                    {
                        if (ipHostInfo.AddressList[i].AddressFamily == AddressFamily.InterNetwork)
                        {
                            list = new TcpListener(ipHostInfo.AddressList[i], port);
                            break;
                        }
                    }

                    list.Start(0);
                    using (TcpClient client = list.AcceptTcpClient())
                    {
                        Stream s = client.GetStream();
                        b1 = new byte[1000];
                        s.Read(b1, 0, 1000);

                        string str = "";
                        str = System.Text.Encoding.UTF8.GetString(b1).TrimEnd('\0');
                        e.Result = str;
                        BTSacn.ReportProgress(0, str);
                        client.Close();
                    }

                    list.Stop();
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                try
                {
                    if (list != null)
                    {
                        list.Stop();
                    }
                }
                catch (Exception ex)
                {
                }
            }
        }

        private void BTSacn_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                if (e.UserState.ToString().Trim() != "" || btnSave.Visible == true)
                {
                    dgBill.Focus();
                    dgBill.CurrentRow.Cells[ColIndex.ItemName].Value = e.UserState.ToString();
                    dgBill_CellEndEdit(dgBill, new DataGridViewCellEventArgs(ColIndex.ItemName, dgBill.Rows.Count - 1));
                }
            }
            catch (Exception ex)
            {
                OMMessageBox.Show(ex.Message);
            }
        }

       

        private void bw_ItemSearch_DoWork(object sender, DoWorkEventArgs e)
        {
            GetByItemSearch();
        }

        private void SalesAE_Shown(object sender, EventArgs e)
        {
            dgBill.RowTemplate.DefaultCellStyle.Font = null;
            dgBill.ColumnHeadersDefaultCellStyle.Font = new Font("Verdana", 12, FontStyle.Regular);
            dgBill.RowHeadersDefaultCellStyle.Font = new Font("Verdana", 12, FontStyle.Regular);
            dgBill.RowTemplate.DefaultCellStyle.Font = new Font("Verdana", 12, FontStyle.Bold);
            dgBill.RowTemplate.Height = 26;
            if (dgBill.Rows.Count > 0)
            {
                for (int i = 0; i < dgBill.Rows.Count; i++)
                    dgBill.Rows[i].DefaultCellStyle.Font = new Font("Verdana", 12, FontStyle.Bold);
            }

        }

       

        private void btnShowScheme_Click(object sender, EventArgs e)
        {
            if (ShowSchemeFlag == false)
            {
                pnlScheme.Height = 150;
                dgInsTSKU.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
                pnlScheme.Width = dgBill.Width;
                dgBill.Height = dgBill.Height - (pnlScheme.Height + 10);
                pnlScheme.Location = new Point(dgBill.Height + 10, dgBill.Location.Y);
                pnlScheme.Location = new Point(dgBill.Width - pnlScheme.Width + 10, dgBill.Location.Y + dgBill.Height + 10);
                pnlScheme.Visible = true;
                ShowSchemeFlag = true;

            }
            else
            {
                ShowSchemeFlag = false;
                pnlScheme.Visible = false;
                dgBill.Height = dgBill.Height + (pnlScheme.Height + 10);
            }
        }

        private void ScheckForScheme()
        {
            LstSchemeConsume = new List<OM.Scheme>();
            foreach (OM.Scheme sc in LstScheme)
            {
                //Check Scheme Is Consumne Or Not
                int cnt = 0;
                foreach (SchemeFromDetails SF in sc.SchemeTarget)
                {
                    for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                    {
                        if (Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.ItemNo].Value) == SF.ItemNo &&
                        Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value) >= SF.Quantity)
                        {
                            cnt++;
                        }
                    }
                }

                if (cnt == sc.SchemeTarget.Count)
                {
                    OM.Scheme MM = new OM.Scheme();
                    MM = sc;
                    foreach (SchemeFromDetails SF in sc.SchemeTarget)
                    {
                        for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                        {
                            if (Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.ItemNo].Value) == SF.ItemNo &&
                            Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value) >= SF.Quantity)
                            {
                                double NoOfTime = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value) / SF.Quantity;

                                foreach (SchemeToDetails ST in sc.SchemeReward)
                                {
                                    ST.Quantity = SF.Quantity * NoOfTime;
                                }

                            }
                        }
                    }

                    LstSchemeConsume.Add(sc);
                }
            }
        }

        private void btnRemoveScheme_Click(object sender, EventArgs e)
        {
            DeleteSchemeData();
        }

        private void dgInsTSKU_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgInsTSKU.CurrentRow != null)
                {
                    if (dgInsTSKU.CurrentRow.Index >= 0)
                    {
                        if (dgInsTSKU.CurrentCell.ColumnIndex == ColIndexGrid.BtnDetails)
                        {
                            if (dgInsTSKU.Rows[dgInsTSKU.CurrentRow.Index].Cells[ColIndexGrid.SchemeUserNo].Value.ToString().Trim() != "")
                            {
                                Form NewF = new Vouchers.SchemeDetails(Convert.ToInt64(dgInsTSKU.Rows[dgInsTSKU.CurrentRow.Index].Cells[ColIndexGrid.SchemeNo].Value), Convert.ToInt64(dgInsTSKU.Rows[dgInsTSKU.CurrentRow.Index].Cells[ColIndexGrid.SchDtlsPksrNo].Value));
                                ObjFunction.OpenForm(NewF);
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void ShowSchemeInfo()
        {
            if (pnlScheme.Visible == false)
            {
                btnShowSchemeInfo.Text = "Hide Scheme";
                pnlScheme.Height = 150;
                dgInsTSKU.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
                pnlScheme.Width = dgBill.Width;
                dgBill.Height = dgBill.Height - (pnlScheme.Height + 10);
                pnlScheme.Location = new Point(dgBill.Height + 10, dgBill.Location.Y);
                pnlScheme.Location = new Point(dgBill.Width - pnlScheme.Width + 10, dgBill.Location.Y + dgBill.Height + 10);
                pnlScheme.Visible = true;
            }

        }

        private void btnShowSchemeInfo_Click(object sender, EventArgs e)
        {

            if (IsSchemeDispaly == false)
            {
                IsSchemeDispaly = true;
                btnShowSchemeInfo.Text = "Hide Scheme";
                pnlScheme.Height = 150;
                dgInsTSKU.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
                pnlScheme.Width = dgBill.Width;
                dgBill.Height = dgBill.Height - (pnlScheme.Height + 10);
                pnlScheme.Location = new Point(dgBill.Height + 10, dgBill.Location.Y);
                pnlScheme.Location = new Point(dgBill.Width - pnlScheme.Width + 10, dgBill.Location.Y + dgBill.Height + 10);
                pnlScheme.Visible = true;
            }
            else
            {
                IsSchemeDispaly = false;
                pnlScheme.Visible = false;
                dgBill.Height = dgBill.Height + (pnlScheme.Height + 10);
                btnShowSchemeInfo.Text = "Show Scheme";
            }

        }

        private void btnApplyScheme_Click(object sender, EventArgs e)
        {
            DeleteSchemeData();
            SetSchemeData();

        }

        private void txtOtherDisc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                Control_Leave((object)txtOtherDisc, new EventArgs());
                txtRemark.Focus();
            }
        }

        private void txtOtherDisc_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked((TextBox)sender, 3, 9, JitFunctions.MaskedType.NotNegative);
        }

        private void txtAddFreight_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                Control_Leave((object)txtAddFreight, new EventArgs());
            }
        }

        private void txtAddFreight_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked((TextBox)sender, 3, 9, JitFunctions.MaskedType.NotNegative);
        }

        #region Serach Item Method

        private void GetByItemSearch()
        {
            string sql = null;
            sql = " SELECT MStockItems.ShowItemName, MUOM.UOMName AS UOM, MStockBarcode.Barcode,(CONVERT(DECIMAL(10,2)," + strRateType + "))  As Rate ," +
                  " (CONVERT(DECIMAL(10,2), MRateSetting.MRP)) AS MRP, " +
                 " IsNull(MStockItemBalance.CurrentStock,0) AS CurrentStock, MStockItems.ItemNo  " +
                 " FROM MStockItems INNER JOIN " +
                 " MUOM ON MStockItems.UOMDefault = MUOM.UOMNo INNER JOIN " +
                 " MStockBarcode ON MStockItems.ItemNo = MStockBarcode.ItemNo INNER JOIN " +
                 " MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo LEFT OUTER JOIN " +
                 " MStockItemBalance ON MRateSetting.MRP = MStockItemBalance.MRP AND MRateSetting.ItemNo = MStockItemBalance.ItemNo " +
                 " WHERE MStockItems.ItemNo <> 1 " +
                 " AND MStockItems.IsActive='true' " +
                 " Order By ShowItemName";


            ds_ItemSearch = new DataSet();
            /*13-Apr-2014
            * Exception Handling Purpose.
            * */
            try
            {
                Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
                ds_ItemSearch = ObjDset.FillDset("New", sql, CommonFunctions.ConStr);
                TempCount = ObjQry.ReturnLong("SELECT Max(Itemno) FROM MStockItems ", CommonFunctions.ConStr);

            }
            catch (Exception e)
            {
                CommonFunctions.ErrorMessge = e.Message;
            }

        }

        private void ItemSerach_Method()
        {
            if (ObjQry.ReturnLong("SELECT Max(Itemno) FROM MStockItems ", CommonFunctions.ConStr) != TempCount)
            {
                bw_ItemSearch.RunWorkerAsync();
            }
            Master.ItemAdvanceSearch itemSearch = new Master.ItemAdvanceSearch(ds_ItemSearch);
            ObjFunction.OpenForm(itemSearch);
            if (itemSearch.ItemNo != 0)
            {
                dgBill.CurrentCell = dgBill[ColIndex.ItemName, dgBill.Rows.Count - 1];
                dgBill.Rows[dgBill.Rows.Count - 1].Cells[ColIndex.ItemName].Value = itemSearch.BarCode;
                dgBill_CellEndEdit(dgBill, new DataGridViewCellEventArgs(ColIndex.ItemName, dgBill.Rows.Count - 1));
                itemSearch.Close();
            }
        }

        private void UpdateSerachStock(long PkVoucherNo)
        {
            DataTable dt = ObjFunction.GetDataView("SELECT SB.ItemNo, SB.MRP, SB.GodownNo, SB.CurrentStock " +
                                     " FROM MStockItemBalance AS SB INNER JOIN TStock AS TS ON SB.ItemNo=TS.ItemNo "+ 
                                     " Where TS.FkVoucherNo="+ PkVoucherNo + ""+
                                     " Order By SB.ItemNo Asc", CommonFunctions.ConStr).Table;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ds_ItemSearch.Tables[0].DefaultView.Sort = "ItemNo Asc";
                for (int j = 0; j < ds_ItemSearch.Tables[0].Rows.Count; j++)
                {
                    if (ds_ItemSearch.Tables[0].Rows[j]["ItemNo"].ToString() == dt.Rows[i]["ItemNo"].ToString() &&
                        ds_ItemSearch.Tables[0].Rows[j]["MRP"].ToString() == dt.Rows[i]["MRP"].ToString())
                    {
                        ds_ItemSearch.Tables[0].Rows[j]["CurrentStock"] = dt.Rows[i]["CurrentStock"].ToString();
                        break;
                    }
                }
            }
            ds_ItemSearch.Tables[0].DefaultView.Sort = "ShowItemName ASC";
        }

        #endregion
    }
}
