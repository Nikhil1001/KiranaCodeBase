﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OM;
using JitControls;

namespace Kirana.Vouchers
{
    /// <summary>
    /// This class used for PurchaseAE
    /// </summary>
    public partial class PurchaseAE : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        DBTVaucherEntry dbTVoucherEntry = new DBTVaucherEntry();
        TVoucherEntry tVoucherEntry = new TVoucherEntry();
        TVoucherDetails tVoucherDetails = new TVoucherDetails();
        TVoucherRefDetails tVchRefDtls = new TVoucherRefDetails();
        TVoucherPayTypeDetails tVchPayTypeDetails = new TVoucherPayTypeDetails();
        TVoucherChqCreditDetails tVchChqCredit = new TVoucherChqCreditDetails();
        TStock tStock = new TStock();
        TStockGodown tStockGodown = new TStockGodown();
        TOtherStockDetails tOtherStkDtls = new TOtherStockDetails();

        //DataTable dtCompRatio = new DataTable();
        DataTable dtVchPrev = new DataTable();
        DataTable dtDelete = new DataTable();
        DataTable dtSearch = new DataTable();
        DataTable dtUOMTemp = new DataTable();
        DataTable dtVchMainDetails = new DataTable();
        DataTable dtPayLedger = new DataTable();
        DataTable dtGodown = null;
        //DataTable dtCompRatio = new DataTable();
        Color clrColorRow = Color.FromArgb(255, 224, 192);
        int cntRow, BillingMode, rowQtyIndex, MixModeVal = 0;//, tempindex
        long LastBillNo = 0;
        bool Spaceflag = true, BillSizeFlag = false;
        long ItemNameType = 0, RateTypeNo, PartyNo, PayType;/*bcdno,*/
        int iItemNameStartIndex = 3, ItemType = 0;
        string strUom, Param1Value = "", Param2Value = "";
        string[] strItemQuery, strItemQuery_last;
        DateTime tempDate; long tempPartyNo = 0;
        Boolean IsAllowDisc = false;
        bool isLocalPurchase = true;
        long LocalStateNo = 0;

        DataTable dt = new DataTable();

        long VoucherUserNo;
        bool isDisc1PercentChanged = false, isDisc2PercentChanged = false;
        bool StopOnQty = false, StopOnRate = false;
        bool isDoProcess = false, MixModeFlag = false, ExistFormFlag = false;
        long Pno;
        //bool MFlag = false;
        DateTime dtFrom, dtTo;
        PartialPurchasePayment partialPay = new PartialPurchasePayment();
        DataTable dtCurrentStock = new DataTable();
        public static string BrandFilter = "";
        long ID, VoucherType, ItemNumber = 0, ParkBillNo = -1;
        DataSet ds_ItemSearch;
        string strRateType = "";

        /// <summary>
        /// This is class of Constructor
        /// </summary>
        public PurchaseAE()
        {
            InitializeComponent();
        }

        /// <summary>
        /// This is class of parameterised Constructor
        /// </summary>
        public PurchaseAE(long ID)
        {
            InitializeComponent();
            this.ID = ID;
        }

        private void dgBill_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                e.Value = (e.RowIndex + 1).ToString();

            }
            if (e.ColumnIndex == ColIndex.ItemName)
            {
                if (dgBill.Rows[e.RowIndex].Cells[ColIndex.PkBarCodeNo].Value != null && dgBill.Rows[e.RowIndex].Cells[ColIndex.PkBarCodeNo].Value.ToString() != "")
                {
                    if (dgBill.Rows[e.RowIndex].Cells[ColIndex.PkBarCodeNo].Value.ToString() != "0")
                        dgBill.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly = true;
                }

                if (dgBill.Rows[e.RowIndex].Cells[ColIndex.PONo].Value != null && dgBill.Rows[e.RowIndex].Cells[ColIndex.PONo].Value.ToString() != "")
                    if (dgBill.Rows[e.RowIndex].Cells[ColIndex.PONo].Value.ToString() != "0")
                        dgBill.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.AliceBlue;

            }
            //if (e.ColumnIndex == ColIndex.FreeQty)
            //{
            //    if (e.Value == null || e.Value=="")
            //        e.Value = "0";
            //}

            //dgBill.CurrentRow.Selected = true;
        }

        private void PurchaseAE_Load(object sender, EventArgs e)
        {
            try
            {

                ObjFunction.LockButtons(true, this.Controls);
                ObjFunction.LockControls(false, this.Controls);
                LocalStateNo = ObjQry.ReturnLong("Select StateNo " +
                " FROM MCompany WHERE CompanyNo = " + DBGetVal.CompanyNo, CommonFunctions.ConStr);

                dgBill.Enabled = false;
                VoucherType = VchType.Purchase;
                RateTypeNo = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_RateType));
                BindCompany();
                lblCompName.Text = GetCompanyName(DBGetVal.CompanyNo);
                lblMixMode.Visible = false; lblMixMode.ForeColor = Color.Red;
                InitDelTable();
                intialCurrentStock();

                ItemNameType = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_ItemNameType)); //deepak
                initItemQuery();

                ObjFunction.FillComb(cmbPaymentType, "Select PKPayTypeNo,PayTypeName From MPayType where PKPayTypeNo in (2,3,4,5)order by PayTypeName");

                ObjFunction.FillList(lstBank, "Select LedgerNo,LedgerName From MLedger Where GroupNo=" + GroupType.BankAccounts + " And IsActive='true' order by LedgerName");
                ObjFunction.FillList(lstBranch, "Select BranchNo,BranchName From MBranch order by BranchName");
                ObjFunction.FillComb(cmbPartyName, "Select LedgerNo,LedgerName From MLedger Where GroupNo in " +
                    "(" + GroupType.SundryCreditors + ") and IsActive='true' order by LedgerName");//deepak
                ObjFunction.FillComb(cmbTaxType, "SELECT GroupNo, GroupName FROM MGroup WHERE (ControlGroup = " + GroupType.DutiesAndTaxes + " ) AND IsActive = 'True' ORDER BY GroupName");

                ObjFunction.FillCombo(cmbPartyNameSearch, " SELECT DISTINCT MLedger.LedgerNo, MLedger.LedgerName FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo  AND  " +
                             " TVoucherEntry.VoucherTypeCode = " + VchType.Purchase + " AND  TVoucherDetails.VoucherSrNo = 1 INNER JOIN MLedger ON MLedger.LedgerNo = TVoucherDetails.LedgerNo ORDER BY MLedger.LedgerName ");
                dtGodown = ObjFunction.GetDataView("Select GodownNo,GodownName From MGodown Where GodownNo<>1").Table;

                FillRateType();
                cmbTaxType.SelectedValue = ObjFunction.GetAppSettings(AppSettings.P_TaxType);
                cmbTaxType.Enabled = false;

                cmbPartyName.Enabled = false;//deepak
                txtInvNo.Enabled = false;

                dtpBillTime.Visible = false;
                label3.Visible = false;

                dtpBillDate.Enabled = false;
                dtpBillTime.Enabled = false;
                dtpBillTime.Format = DateTimePickerFormat.Time;
                InitControls();
                StopOnQty = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnQty));// Convert.ToBoolean(dtSalesSetting.Rows[0].ItemArray[14].ToString());
                StopOnRate = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnRate)); //Convert.ToBoolean(dtSalesSetting.Rows[0].ItemArray[13].ToString());

                //dtSearch = ObjFunction.GetDataView("Select PkVoucherNo from TVoucherEntry Where VoucherTypeCode=" + VoucherType + " AND CompanyNo=" + DBGetVal.CompanyNo + "").Table;

                dtSearch = ObjFunction.GetDataView("Select PkVoucherNo from TVoucherEntry " +
                   " Where VoucherTypeCode=" + VoucherType + " AND CompanyNo=" + DBGetVal.CompanyNo +
                   " Order by VoucherDate, VoucherUserNo ").Table;

                if (dtSearch.Rows.Count > 0 && ID == 0)
                {
                    ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    FillControls();
                    SetNavigation();
                    setDisplay(true);
                }
                else
                {
                    if (ID != 0)
                    {
                        FillControls();
                        SetNavigation();
                        btnFirst.Visible = false; btnPrev.Visible = false; btnNext.Visible = false; btnLast.Visible = false;
                        btnNew.Visible = false; btnUpdate.Visible = false; btnCancel.Visible = false; btnSave.Visible = false;
                        btnSearch.Visible = false; btnShortcut.Visible = false; btnNewCustomer.Visible = false; btnMixMode.Visible = false;
                        btnAdvanceSearch.Visible = false; btnAllBarCodePrint.Visible = false;
                        btndelete.Visible = false;
                        ExistFormFlag = true;
                        dgBill.Enabled = true;
                        for (int i = 0; i < dgBill.ColumnCount; i++)
                        {
                            dgBill.Columns[i].ReadOnly = true;
                        }
                        dgBill.KeyDown -= new KeyEventHandler(dgBill_KeyDown);
                    }
                    else setDisplay(true);
                }

                btnNew.Focus();
                KeyDownFormat(this.Controls);

                DataTable dtSettings = ObjFunction.GetDataView("Select PKSettingNo From MSettings Where SettingTypeNo=5").Table;

                for (int i = 0; i < dtSettings.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(Convert.ToInt32(dtSettings.Rows[i].ItemArray[0].ToString()))) == true)
                    {
                        if (i >= ColIndex.MRP && i < dtSettings.Rows.Count)
                            dgBill.Columns[i + 1].Visible = true;
                        else
                            dgBill.Columns[i].Visible = true;
                    }
                    else
                    {
                        if (i >= ColIndex.MRP && i < dtSettings.Rows.Count - 1)
                            dgBill.Columns[i + 1].Visible = false;
                        else
                            dgBill.Columns[i].Visible = false;
                    }

                }
                dgBill.Columns[ColIndex.BarcodePrint].Visible = false;
                //dgBill.Columns[ColIndex.LandedRate].Visible = false;
                txtGrandTotal.Font = new Font("Verdana", 18, FontStyle.Bold);
                txtGrandTotal.ForeColor = Color.Maroon;

                lblGrandTotal.Font = new Font("Verdana", 18, FontStyle.Bold);
                lblGrandTotal.ForeColor = Color.White;

                new GridSearch(dgItemList, 1);
                formatPics();
                DisplayChargANDDisc();
                DisplayRateType();
                btnAllBarCodePrint.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_AllBarCodePrint));
                //dgPurHistory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)
                //            | System.Windows.Forms.AnchorStyles.Left)));
                //for (int i = 0; i < dgBill.Columns.Count; i++) dgBill.Columns[i].Visible = true;

                //btnShowDetails.BackColor = Color.FromArgb(255, 128, 0);
                button1.BackColor = Color.FromArgb(255, 128, 0);
                //button2.BackColor = Color.FromArgb(255, 128, 0);
                //button3.BackColor = Color.FromArgb(255, 128, 0);
                btnAllBarCodePrint.BackColor = Color.FromArgb(255, 128, 0);
                // btnShortcut.BackColor = Color.FromArgb(255, 128, 0);
                // btnNewCustomer.BackColor = Color.FromArgb(255, 128, 0);
                btnAllBarCodePrint.Font = new Font("Arial", 8, FontStyle.Bold);

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_Bilingual)) == false)
                {
                    dgItemList.Columns[2].Visible = false;
                }
                strRateType = ObjFunction.GetComboValueString(cmbRateType);
                bw_ItemSearch.RunWorkerAsync();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void DisplayChargANDDisc()
        {
            try
            {
                lblChrg1.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_Charges1Display));
                txtChrgRupees1.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_Charges1Display));
                if (ObjFunction.GetAppSettings(AppSettings.P_Charges2Display) != "")
                    lblOtherTax.Text = ObjFunction.GetAppSettings(AppSettings.P_Charges2Display);

                lblDisc1.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_Discount1Display));
                //txtDiscount1.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_Discount1Display));
                txtDiscRupees1.Visible = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_Discount1Display));
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

        }

        private void DisplayRateType()
        {
            //Rate Type related.
            //if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_IsDisplayRateType)) == true)
            //{
            //    lblRateType.Visible = true;
            //    cmbRateType.Visible = true;
            //}
            //else
            //{
            lblRateType.Visible = false;
            cmbRateType.Visible = false;
            //}

            //if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_RateTypeAskPassword)) == true)
            //    cmbRateType.Enabled = false;
            //else cmbRateType.Enabled = true;
        }

        private void formatPics()
        {
            try
            {

                panel3.Width = pnlItemName.Width = dgItemList.Width = 1300;//560;
                dgItemList.Width = 1290;
                pnlItemName.Height = 500;
                pnlItemName.Top = 90;
                pnlItemName.Left = 10;

                pnlGroup1.Top = 88;

                dgItemList.RowTemplate.DefaultCellStyle.Font = null;
                dgItemList.Columns[2].DefaultCellStyle.Font = ObjFunction.GetLangFont();
                dgItemList.DefaultCellStyle.Font = ObjFunction.GetFont(FontStyle.Bold, 10);
                dgItemList.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dgItemList.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dgItemList.Columns[3].Width = 100;
                dgItemList.Columns[5].Width = 100;
                dgItemList.Columns[6].Width = 100;
                dgItemList.RowTemplate.Height = 25;

                //if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_Bilingual)) == true)
                //{
                //    pnlGroup1.Left = 50;//150;
                //    pnlGroup1.Width = 575;
                //    lstGroup1.Font = ObjFunction.GetFont(FontStyle.Regular, 11);
                //    //lstGroup1Lang.Font = ObjFunction.GetLangFont();
                //    dgItemList.RowTemplate.DefaultCellStyle.Font = null;
                //    dgItemList.Columns[2].DefaultCellStyle.Font = ObjFunction.GetLangFont();
                //}
                //else
                //{
                lstGroup1.Font = ObjFunction.GetFont(FontStyle.Regular, 11);
                pnlGroup1.Left = 150;
                pnlGroup1.Width = lstGroup1.Width + 10;
                txtBrandFilter.Width = lstGroup1.Width - 2;
                //  }
                pnlGroup1.Height = lstGroup1.Height + 5;

                pnlGroup2.Top = 88;
                pnlGroup2.Left = 100;
                pnlGroup2.Width = 300;
                pnlGroup2.Height = 220;

                pnlUOM.Top = 88;
                pnlUOM.Left = 372;
                pnlUOM.Width = 120;
                pnlUOM.Height = 80;

                pnlRate.Top = 88;
                pnlRate.Left = 430;
                pnlRate.Width = 120;
                pnlRate.Height = 80;

                pnlSalePurHistory.Width = 720;
                pnlSalePurHistory.Height = 235;
                pnlSalePurHistory.Top = pnlItemName.Height + 88;
                pnlSalePurHistory.Left = pnlItemName.Left;

                //btnMixMode.Left = btnExit.Left+btnExit.Width+5;
                //btnMixMode.Top = btnExit.Top;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void initItemQuery()
        {
            try
            {
                DataTable dtItemQuery = new DataTable();
                dtItemQuery = ObjFunction.GetDataView("SELECT * from MItemNameDisplayType WHERE ItemNameTypeNo = " + ItemNameType).Table;

                if (dtItemQuery.Rows.Count == 1)
                {
                    int qCount = 0;
                    for (int i = 1; i < 4; i++)
                    {
                        if (dtItemQuery.Rows[0]["Query" + i] != null && dtItemQuery.Rows[0]["Query" + i].ToString().Trim().Length > 0)
                        {
                            qCount++;
                        }
                    }

                    strItemQuery = new string[qCount];
                    strItemQuery_last = new string[qCount];
                    for (int i = 0; i < strItemQuery_last.Length; i++)
                    {
                        strItemQuery_last[i] = "";
                    }
                    qCount = 0;
                    for (int i = 1; i < 4; i++)
                    {
                        if (dtItemQuery.Rows[0]["Query" + i] != null && dtItemQuery.Rows[0]["Query" + i].ToString().Trim().Length > 0)
                        {
                            strItemQuery[qCount] = dtItemQuery.Rows[0]["Query" + i].ToString().Trim();
                            qCount++;
                        }
                    }

                    iItemNameStartIndex = Convert.ToInt32(dtItemQuery.Rows[0]["StartIndex"].ToString());
                    Param1Value = dtItemQuery.Rows[0]["Param1Value"].ToString();
                    Param2Value = dtItemQuery.Rows[0]["Param2Value"].ToString();
                }
                else
                {
                    OMMessageBox.Show("Please Select Valid Item Name display type in Purchse Setting Form ...");
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void InitControls()
        {
            try
            {
                VoucherUserNo = 0;
                SetRateType(RateTypeNo);
                cmbPaymentType.SelectedIndex = 0;
                dtpBillDate.Value = DBGetVal.ServerTime;
                dtpBillTime.Value = DBGetVal.ServerTime;
                MixModeFlag = false; MixModeVal = 0;
                while (dgBill.Rows.Count > 0)
                {
                    dgBill.Rows.RemoveAt(0);
                }
                //for (int i = 0; i < 20; i++)
                //{
                //    dgBill.Rows.Add();
                //}
                dgBill.Rows.Add();
                CalculateTotal();
                ObjFunction.GetFinancialYear(dtpBillDate.Value, out dtFrom, out dtTo);

                txtInvNo.Text = (ObjQry.ReturnLong("Select max(VoucherUserNo) from TVoucherEntry Where VoucherTypeCode=" + VoucherType + " AND VoucherDate>='" + dtFrom.Date + "' AND VoucherDate<='" + dtTo.Date + "'", CommonFunctions.ConStr) + 1).ToString();

                string sqlQuery = " SELECT 0 AS Sr, MStockItems.ItemName, TStock.Quantity, MUOM.UOMName, TStock.Rate, TStock.Amount,MStockBarcode.Barcode, " +
                                 " TStock.PkStockTrnNo, MStockBarcode.PkStockBarcodeNo,TVoucherEntry.PkVoucherNo, MStockItems.ItemNo, MUOM.UOMNo " +
                                 " FROM TVoucherDetails INNER JOIN TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo " +
                                 " INNER JOIN TStock INNER JOIN MStockItems ON TStock.ItemNo = MStockItems.ItemNo ON " +
                                 " TVoucherDetails.PkVoucherTrnNo = TStock.FkVoucherTrnNo INNER JOIN " +
                                 " MStockBarcode ON MStockItems.ItemNo = MStockBarcode.ItemNo INNER JOIN " +
                                 " MUOM ON MStockItems.UOMDefault = MUOM.UOMNo " +
                                 " WHERE (MStockBarcode.Barcode = '') AND (TVoucherDetails.VoucherSrNo = 1) AND " +
                                 " (TVoucherEntry.VoucherTypeCode IN (9, 21)) AND MStockItems.CompanyNo=" + DBGetVal.CompanyNo + "";
                dt = ObjFunction.GetDataView(sqlQuery).Table;

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnDate)) == true) dtpBillDate.Focus();
                else if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnParty)) == true) cmbPartyName.Focus();
                else if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnHeaderDisc)) == true) txtOtherDisc.Focus();
                else if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnTaxType)) == true) cmbTaxType.Focus();
                else txtRefNo.Focus();
                //dgBill.Focus();
                dgBill.CurrentCell = dgBill[ColIndex.ItemName, 0];
                lblExchange.Visible = false;
                //ExchangeMode = false; 
                lblExchange.Visible = false;
                cmbPartyName.SelectedValue = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_DefaultPartyAC));
                //txtDiscount1.Text = "0.00"; 
                txtDiscRupees1.Text = "0.00";
                txtFreight.Text = "0.00";
                txtChrgRupees1.Text = "0.00";
                txtOtherTax.Text = "0.00";
                txtVisibility.Text = "0.00";
                txtReturnAmt.Text = "0.00";
                btnMixMode.Text = "Mix\r\nMode\r\n(F3)";
                BindGridPayType(0);
                BindPayChequeDetails(0);
                BindPayCreditDetails(0);
                pnlBarCodePrint.Visible = false;
                btnSave.Enabled = true;
                tempDate = dtpBillDate.Value.Date;
                tempPartyNo = ObjFunction.GetComboValue(cmbPartyName);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillControls()
        {
            try
            {
                tVoucherEntry = dbTVoucherEntry.ModifyTVoucherEntryByID(ID);
                VoucherUserNo = Convert.ToInt32(tVoucherEntry.VoucherUserNo);
                txtInvNo.Text = tVoucherEntry.VoucherUserNo.ToString();
                dtpBillDate.Value = tVoucherEntry.VoucherDate;
                dtpBillTime.Value = tVoucherEntry.VoucherTime;
                cmbPaymentType.SelectedValue = tVoucherEntry.PayTypeNo.ToString();
                cmbTaxType.SelectedValue = tVoucherEntry.TaxTypeNo.ToString();
                SetRateType(tVoucherEntry.RateTypeNo);
                cmbTaxType.Enabled = false;
                cmbRateType.Enabled = false;
                MixModeFlag = false; MixModeVal = tVoucherEntry.MixMode;
                PayType = tVoucherEntry.PayTypeNo;
                // PurCompNo = tVoucherEntry.CompanyNo;
                lblCompName.Text = GetCompanyName(tVoucherEntry.CompanyNo);
                txtRefNo.Text = tVoucherEntry.Reference;

                txtSubTotal.Text = "0.00";
                txtTotalDisc.Text = "0.00"; txtTotalItemDisc.Text = "0.00";
                //txtDiscount1.Text = "0.00";
                txtDiscRupees1.Text = "0.00";
                txtFreight.Text = "0.00";
                txtTotalTax.Text = "0.00";
                txtReturnAmt.Text = "0.00";
                txtVisibility.Text = "0.00";
                //manali

                txtChrgRupees1.Text = "0.00";
                txtOtherTax.Text = "0.00";

                txtRemark.Text = tVoucherEntry.Remark;
                txtReturnAmt.Text = tVoucherEntry.ReturnAmount.ToString("0.00");
                txtVisibility.Text = tVoucherEntry.Visibility.ToString("0.00");
                //end
                tempDate = dtpBillDate.Value.Date;

                DataTable dt = ObjFunction.GetDataView("Select Case When Debit<>0 then Debit Else Credit End,LedgerNo,SrNo From TVoucherDetails Where FKVoucherNo=" + ID + " order by VoucherSrNo").Table;
                //double subTot = ObjQry.ReturnDouble("Select sum(Debit) from TVoucherDetails  Where FKVoucherNo=" + ID + " and LedgerNo!=1 ",CommonFunctions.ConStr);
                double subTot = ObjQry.ReturnDouble("Select sum(Debit) from TVoucherDetails  Where FKVoucherNo=" + ID + " ", CommonFunctions.ConStr);
                txtSubTotal.Text = subTot.ToString();
                Pno = ObjQry.ReturnLong("Select LedgerNo from TVoucherDetails  Where FKVoucherNo=" + ID + " AND VoucherSrNo=1", CommonFunctions.ConStr);
                ObjFunction.FillComb(cmbPartyName, "Select LedgerNo,LedgerName From MLedger Where GroupNo in " +
                    "(" + GroupType.SundryCreditors + ") and IsActive='true' or LedgerNo=" + Pno + " order by LedgerName");
                cmbPartyName.SelectedValue = Pno;

                PartyNo = ObjFunction.GetComboValue(cmbPartyName);
                tempPartyNo = PartyNo;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //if ((dt.Rows[i].ItemArray[1].ToString() == dtSalesSetting.Rows[0].ItemArray[1].ToString()) || (Convert.ToInt64(dt.Rows[i].ItemArray[1].ToString()) == 0))
                    //if (dt.Rows[i].ItemArray[1].ToString() == dgBill.Rows[i].Cells[13].Value)
                    //txtSubTotal.Text = Convert.ToDouble(dt.Rows[i].ItemArray[0].ToString()).ToString("0.00");
                    //txtSubTotal.Text=subTot.ToString();

                    if (dt.Rows[i].ItemArray[2].ToString() == Others.Discount1.ToString())
                    {
                        //txtDiscount1.Text = Math.Round((Convert.ToDouble(dt.Rows[i].ItemArray[0].ToString()) * 100) / subTot, MidpointRounding.AwayFromZero).ToString("0.00");
                        //Control_Leave((object)txtDiscount1, new EventArgs());
                        txtDiscRupees1.Text = Convert.ToDouble(dt.Rows[i].ItemArray[0].ToString()).ToString("0.00");
                    }
                    else if (dt.Rows[i].ItemArray[2].ToString() == Others.Charges1.ToString())
                    {
                        txtChrgRupees1.Text = Convert.ToDouble(dt.Rows[i].ItemArray[0].ToString()).ToString("0.00");
                    }
                    else if (dt.Rows[i].ItemArray[2].ToString() == Others.Freight.ToString())
                    {
                        txtFreight.Text = Convert.ToDouble(dt.Rows[i].ItemArray[0].ToString()).ToString("0.00");
                    }
                    else if (dt.Rows[i].ItemArray[2].ToString() == Others.Charges2.ToString())
                    {
                        txtOtherTax.Text = Convert.ToDouble(dt.Rows[i].ItemArray[0].ToString()).ToString("0.00");
                    }
                    else if (dt.Rows[i].ItemArray[2].ToString() == Others.ItemDisc.ToString())
                    {
                        txtTotalDisc.Text = Convert.ToDouble(dt.Rows[i].ItemArray[0].ToString()).ToString("0.00");
                    }
                    else if (dt.Rows[i].ItemArray[2].ToString() == Others.BTaxItemDisc.ToString())
                    {
                        txtTotalItemDisc.Text = Convert.ToDouble(dt.Rows[i].ItemArray[0].ToString()).ToString("0.00");
                    }
                }

                txtGrandTotal.Text = ((Convert.ToDouble(txtSubTotal.Text) - (Convert.ToDouble(txtTotalDisc.Text) + Convert.ToDouble(txtTotalItemDisc.Text))) + Convert.ToDouble(txtTotalTax.Text)).ToString("0.00");
                //cmbPaymentType.SelectedValue = ObjQry.ReturnLong("Select LedgerNo From TVoucherDetails Where VoucherSrNo=2 AND FKVoucherNo=" + ID + "", CommonFunctions.ConStr).ToString();

                FillGrid();


                DataTable dtPartial = ObjFunction.GetDataView("Select Credit From TVoucherDetails Where FKVoucherNo=" + ID + " AND VoucherSrNo in (2,3) AND LedgerNo in(1,3) ").Table;
                if (dtPartial.Rows.Count == 2)
                {
                    //txtCash.Text = Convert.ToDouble(dtPartial.Rows[0].ItemArray[0]).ToString("0.00");
                    //txtCredit.Text = Convert.ToDouble(dtPartial.Rows[1].ItemArray[0]).ToString("0.00");
                    txtTotalAmt.Text = "0.00";
                }

                long LastBillNo = ObjQry.ReturnLong("Select IsNull(Max(PkVoucherNo),0) From TVoucherEntry Where VoucherTypeCode=" + VoucherType + " AND  PkVoucherNo<" + ID + "", CommonFunctions.ConStr);
                //dt = ObjFunction.GetDataView("SELECT IsNull(SUM(TStock.Quantity),0) AS Quantity, IsNull(SUM(TStock.Amount),0) AS Amount FROM TVoucherDetails INNER JOIN TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN "+
                //    " TStock ON TVoucherDetails.PkVoucherTrnNo = TStock.FkVoucherTrnNo WHERE (TVoucherDetails.VoucherSrNo = 1) AND (TVoucherEntry.VoucherTypeCode = "+ VoucherType +") AND (TVoucherEntry.PkVoucherNo = "+ LastBillNo +")").Table;

                dt = ObjFunction.GetDataView("SELECT IsNull(SUM(TStock.Quantity),0) AS Quantity, IsNull(SUM(TStock.Amount),0) AS Amount FROM TVoucherDetails INNER JOIN TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN " +
                    " TStock ON TVoucherDetails.PkVoucherTrnNo = TStock.FkVoucherTrnNo WHERE (TVoucherEntry.VoucherTypeCode = " + VoucherType + ") AND (TVoucherEntry.PkVoucherNo = " + LastBillNo + ")").Table;

                if (dt.Rows.Count > 0)
                {
                    lblLastBillAmt.Text = "Amount : " + Convert.ToDouble(dt.Rows[0].ItemArray[1].ToString()).ToString("0.00");
                    txtLastBillAmt.Text = Convert.ToDouble(dt.Rows[0].ItemArray[1].ToString()).ToString("0.00");
                    lblLastBillQty.Text = "Qty: " + dt.Rows[0].ItemArray[0].ToString();
                    txtlastBillQty.Text = dt.Rows[0].ItemArray[0].ToString();
                    lblLastPayment.Text = "" + ObjQry.ReturnString("SELECT MPayType.PayTypeName FROM MPayType INNER JOIN TVoucherEntry ON MPayType.PKPayTypeNo = TVoucherEntry.PayTypeNo WHERE (TVoucherEntry.PkVoucherNo = " + LastBillNo + ")", CommonFunctions.ConStr);
                    txtLastPayment.Text = ObjQry.ReturnString("SELECT MPayType.PayTypeName FROM MPayType INNER JOIN TVoucherEntry ON MPayType.PKPayTypeNo = TVoucherEntry.PayTypeNo WHERE (TVoucherEntry.PkVoucherNo = " + LastBillNo + ")", CommonFunctions.ConStr);
                }
                dtVchMainDetails = ObjFunction.GetDataView("Select * From TVoucherDetails Where FKVoucherNo=" + ID + "").Table;
                if (ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo in ( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + ")", CommonFunctions.ConStr) > 1)
                    btnUpdate.Visible = false;
                else if (ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo in ( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + " and TR.TypeOfRef in(6))", CommonFunctions.ConStr) > 1)
                    btnUpdate.Visible = false;
                else
                    btnUpdate.Visible = true;

                if ((ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo in ( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + ")", CommonFunctions.ConStr) > 1) && PayType == 3)
                {
                    btnMixMode.Visible = false;
                }
                else if (PayType == 3 && tVoucherEntry.IsVoucherLock == false && tVoucherEntry.IsCancel == false)
                {
                    btnMixMode.Visible = true;
                    btnMixMode.Text = "Pay\r\nMode";
                }
                else
                    btnMixMode.Visible = false;
                if (MixModeVal == 1) lblMixMode.Visible = true; else lblMixMode.Visible = false;


            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillGrid()
        {
            try
            {
                dgBill.Rows.Clear();


                string sqlQuery = "SELECT 0 AS Sr,MStockItems.ShowItemName AS ItemName, TStock.Quantity, MUOM.UOMName, TStock.Rate, Cast(MRateSetting.MRP as numeric(18,2)) AS MRP,TStock.NetRate AS NetRate , TStock.FreeQty, MUOMFree.UOMName AS FreeUOM," +
                    " TStock.DiscPercentage AS DiscPercentage, TStock.DiscAmount AS DiscAmount,TStock.DiscRupees,TStock.DiscPercentage2, " +
                    " TStock.DiscAmount2, TStock.NetAmount AS NetAmt, TStock.TaxPercentage AS TaxPercentage, TStock.TaxAmount AS TaxAmount, " +
                    " TStock.DiscRupees2, TStock.Amount, MStockBarcode.Barcode,  " +
                    " TStock.PkStockTrnNo, MStockBarcode.PkStockBarcodeNo, TVoucherDetails.PkVoucherTrnNo, MStockItems.ItemNo, " +
                    " MUOM.UOMNo,  MItemTaxInfo.TaxLedgerNo, MItemTaxInfo.SalesLedgerNo, TStock.FkRateSettingNo, MItemTaxInfo.PkSrNo,  " +
                    " MRateSetting.StockConversion AS StockConversion, TStock.Quantity * MRateSetting.StockConversion AS ActualQty,  MRateSetting.MKTQty AS MKTQuantity, " +
                    " IsNULL((SELECT PkVoucherTrnNo FROM TVoucherDetails AS SV WHERE SV.CompanyNo=TVoucherDetails.CompanyNo AND (LedgerNo = MItemTaxInfo.SalesLedgerNo) AND (FkVoucherNo = TVoucherDetails.FkVoucherNo)),0) AS SalesVchNo, " +
                    " IsNull((SELECT PkVoucherTrnNo FROM TVoucherDetails AS TXV WHERE TXV.CompanyNo=TVoucherDetails.CompanyNo AND (LedgerNo = MItemTaxInfo.TaxLedgerNo) AND (FkVoucherNo = TVoucherDetails.FkVoucherNo)),0) AS TaxVchNo,MStockItems.CompanyNo,'Print' As BarcodePrinting,TStock.FreeUOMNo,Cast(MRateSetting.MRP as numeric(18,2)) AS TempMRP,TStock.LandedRate, " +//,TStock.LandedRate
                    " IsNull(TOtherStockDetails.FKOtherVoucherNo,0) As PONo, IsNull(TOtherStockDetails.FKOtherStockTrnNo,0) AS FKOtherStockTrnNo,IsNull(TOtherStockDetails.PkSrNo,0) As PKStockOtherDetailsNo " +
                    " , MStockItems.GodownNo,MStockItems.DiscountType ," +
                     " TStock.HSNCode, TStock.IGSTPercent, TStock.IGSTAmount, TStock.CGSTPercent, TStock.CGSTAmount, " +
                    " TStock.SGSTPercent, TStock.SGSTAmount, TStock.UTGSTPercent, TStock.UTGSTAmount," +
                    " TStock.CessPercent, TStock.CessAmount,TStock.GodownNo " +
                    " FROM MStockItems INNER JOIN TStock ON MStockItems.ItemNo = TStock.ItemNo INNER JOIN TVoucherDetails ON TStock.FkVoucherTrnNo = TVoucherDetails.PkVoucherTrnNo INNER JOIN " +
                    " MItemTaxInfo ON TStock.FkItemTaxInfo = MItemTaxInfo.PkSrNo INNER JOIN MStockBarcode ON TStock.FkStockBarCodeNo = MStockBarcode.PkStockBarcodeNo INNER JOIN MUOM ON TStock.FkUomNo = MUOM.UOMNo INNER JOIN MRateSetting ON TStock.FkRateSettingNo = MRateSetting.PkSrNo INNER JOIN " +
                    " MUOM AS MUOMFree ON TStock.FreeUOMNo = MUOMFree.UOMNo LEFT OUTER JOIN TOtherStockDetails ON TStock.PkStockTrnNo = TOtherStockDetails.FKStockTrnNo " +
                    " WHERE  (TVoucherDetails.FkVoucherNo = " + ID + ") " + /*" AND MStockItems.CompanyNo=" + DBGetVal.CompanyNo +*/ " ORDER BY TStock.PkStockTrnNo";//(TVoucherDetails.Credit <> 0 ) AND

                dt = ObjFunction.GetDataView(sqlQuery).Table;

                lblBillItem.Text = "0"; lblBilExchangeItem.Text = "0";
                string strStkNo = "";

                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    dgBill.Rows.Add();
                    for (int i = 0; i < dt.Columns.Count; i++)//LandedRate
                    {
                        dgBill.Rows[j].Cells[i].Value = dt.Rows[j].ItemArray[i].ToString();
                    }

                    if (Convert.ToDouble(dgBill.Rows[j].Cells[ColIndex.Quantity].Value) >= 0)
                        lblBillItem.Text = (Convert.ToDouble(lblBillItem.Text) + Convert.ToDouble(dgBill.Rows[j].Cells[ColIndex.Quantity].Value)).ToString();
                    else
                        lblBilExchangeItem.Text = (Convert.ToInt64(lblBilExchangeItem.Text) + Math.Abs(Convert.ToDouble(dgBill.Rows[j].Cells[ColIndex.Quantity].Value))).ToString();

                }


                dgBill.Rows.Add();
                dgBill.CurrentCell = dgBill[1, dgBill.Rows.Count - 1];
                CalculateTotal();
                BindGridPayType(ID);
                BindPayChequeDetails(ID);
                BindPayCreditDetails(ID);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void CalculateTotal()
        {
            try
            {
                double AmtGrTotal = (txtGrandTotal.Text == "") ? 0 : Convert.ToDouble(txtGrandTotal.Text);
                //double AmtOtherTax = (txtOtherTax.Text == "") ? 0 : Convert.ToDouble(txtOtherTax.Text);
                //double AmtOtherchrg = (txtChrgRupees1.Text == "") ? 0 : Convert.ToDouble(txtChrgRupees1.Text);
                txtSubTotal.Text = "0.00";
                lblBillItem.Text = "0";
                lblBilExchangeItem.Text = "0";
                txtGrandTotal.Text = "0.00";
                if (txtTotalDisc.Text == null || txtTotalDisc.Text == "")
                    txtTotalDisc.Text = "0.00";
                if (txtDistDisc.Text == null || txtDistDisc.Text == "")
                    txtDistDisc.Text = "0.00";
                txtTotalItemDisc.Text = "0.00";
                txtTotalTax.Text = "0.00";
                //txtVisibility.Text = "0.00";
                //txtReturnAmt.Text = "0.00";
                double subTotal = 0, TotalDiscBeforeTax = 0, TotalDiscAfterTax = 0, totalChrg = 0, totalTax = 0, TotFinal = 0, TotSchemeDisc = 0, TotDistDisc = 0;
                if (Validations() == true)
                {
                    for (int i = 0; i < dgBill.Rows.Count; i++)
                    {
                        if (dgBill.Rows[i].Cells[ColIndex.ItemNo].Value != null && dgBill.Rows[i].Cells[ColIndex.ItemNo].Value.ToString() != "")
                        {

                            #region check & init Default values
                            if (dgBill.Rows[i].Cells[ColIndex.Quantity].Value == null) dgBill.Rows[i].Cells[ColIndex.Quantity].Value = Convert.ToDouble(1).ToString("0.00");

                            if (dgBill.Rows[i].Cells[ColIndex.MKTQuantity].Value == null) dgBill.Rows[i].Cells[ColIndex.MKTQuantity].Value = 1;
                            if (dgBill.Rows[i].Cells[ColIndex.StockFactor].Value == null) dgBill.Rows[i].Cells[ColIndex.StockFactor].Value = 1;
                            if (dgBill.Rows[i].Cells[ColIndex.Rate].Value == null) dgBill.Rows[i].Cells[ColIndex.Rate].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.DiscRupees].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscRupees].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.DiscRupees2].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscRupees2].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.FreeQty].Value == null) dgBill.Rows[i].Cells[ColIndex.FreeQty].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.LandedRate].Value == null) dgBill.Rows[i].Cells[ColIndex.LandedRate].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.HSNCode].Value == null) dgBill.Rows[i].Cells[ColIndex.HSNCode].Value = "";
                            #endregion

                            #region fetch basic values
                            double Qty = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value);
                            double Rate = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Rate].Value);
                            double TaxPerce = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.TaxPercentage].Value);
                            double MktQty = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.MKTQuantity].Value);

                            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_IsReverseRateCalc)) == true)
                            {
                                Rate = Convert.ToDouble(((Rate * 100) / (100 + TaxPerce)).ToString("0.00")); //reverse rate
                            }

                            double Amount = Convert.ToDouble((((Qty) * (Rate)) / (MktQty)).ToString("0.0000"));
                            #endregion

                            #region Before tax discount calculation
                            //disc1 %
                            double Disc1 = 0;
                            if (isDisc1PercentChanged == true && i == dgBill.CurrentRow.Index)
                            {
                                dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value).ToString(Format.DoubleFloating);
                                dgBill.Rows[i].Cells[ColIndex.DiscAmount].Value = Convert.ToDouble(((Amount * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value)) / 100).ToString("0.00"));
                                isDisc1PercentChanged = false;
                                //isDisc2PercentChanged = true;
                                Disc1 = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscAmount].Value);
                            }
                            else if (i == dgBill.CurrentRow.Index)
                            {
                                //Disc1 = Convert.ToDouble(((Amount * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value)) / 100).ToString("0.0000"));
                                Disc1 = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscAmount].Value);
                                dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value = (Amount == 0) ? "0" : ((Disc1 * 100) / Amount).ToString("0.00");
                            }
                            Disc1 = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscAmount].Value);
                            //disc 1 rs
                            double DiscAmt1 = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscRupees].Value);
                            double DiscBeforeTax = Disc1 + DiscAmt1;// before disc 2%
                            double tAmount = Amount - DiscBeforeTax; // before disc 2%
                            //disc 2 %
                            double Disc2 = 0;
                            if (isDisc2PercentChanged == true && i == dgBill.CurrentRow.Index)
                            {
                                dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value).ToString(Format.DoubleFloating);
                                dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value = Convert.ToDouble(((tAmount * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value)) / 100).ToString("0.00"));
                                isDisc2PercentChanged = false;
                                Disc2 = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value);
                            }
                            else if (i == dgBill.CurrentRow.Index)
                            {
                                //Disc2 = Convert.ToDouble(((tAmount * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value)) / 100).ToString("0.0000"));
                                Disc2 = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value);
                                dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value = (tAmount == 0) ? "0" : ((Disc2 * 100) / tAmount).ToString("0.00");
                            }
                            Disc2 = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value);
                            // Total disc before tax
                            DiscBeforeTax = 0;
                            DiscBeforeTax += Disc2;
                            //Net Amt Before Tax - for sub total
                            tAmount -= Disc2;
                            //Net Rate (after 1st disc %, 1st Rs, 2nd % OR Before Tax)
                            double ttRate = 0, LandedRate = 0, LandedGrandTotal = 0;

                            #region Tax Calculation
                            double TaxAmt = Convert.ToDouble(((tAmount * TaxPerce) / (100)).ToString("0.00"));
                            #endregion

                            #region Charges Calculation
                            tAmount += Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscRupees2].Value);
                            #endregion
                            double DiscAfterTax = 0;
                            dgBill.Rows[i].Cells[ColIndex.Amount].Value = (tAmount + TaxAmt - DiscAfterTax).ToString("0.00");

                            for (int rw = 0; rw < dgBill.Rows.Count - 1; rw++)
                            {
                                LandedGrandTotal += Convert.ToDouble(dgBill.Rows[rw].Cells[ColIndex.Amount].Value);
                            }

                            if (tAmount != 0 || Qty != 0)
                            {
                                ttRate = (tAmount) / Qty;
                                LandedGrandTotal = Convert.ToDouble(Convert.ToDouble((Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Amount].Value) * (Convert.ToDouble(txtOtherTax.Text) + Convert.ToDouble(txtChrgRupees1.Text))) / LandedGrandTotal).ToString(Format.DoubleFloating));
                                if (LandedGrandTotal.ToString().Trim() == "NaN")
                                    LandedGrandTotal = 0;
                                LandedRate = (tAmount + TaxAmt + LandedGrandTotal) / (Qty + Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.FreeQty].Value));
                            }
                            #endregion

                            #region Tax Calculation
                            //Forward Calculation

                            if (TaxPerce > 0 && ObjFunction.GetComboValue(cmbTaxType) == GroupType.GST)// && dgBill.Rows[i].Cells[ColIndex.HSNCode].Value.ToString().Length > 0)
                            {
                                if (isLocalPurchase)
                                {
                                    double CGSTPercent = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.CGSTPercent].Value);
                                    if (CGSTPercent > 0)
                                    {
                                        double CGSTValue = Convert.ToDouble(((CGSTPercent * TaxAmt) / TaxPerce).ToString("0.000"));
                                        dgBill.Rows[i].Cells[ColIndex.CGSTAmount].Value = CGSTValue;
                                    }

                                    double SGSTPercent = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.SGSTPercent].Value);
                                    if (SGSTPercent > 0)
                                    {
                                        double SGSTValue = Convert.ToDouble(((SGSTPercent * TaxAmt) / TaxPerce).ToString("0.000"));
                                        dgBill.Rows[i].Cells[ColIndex.SGSTAmount].Value = SGSTValue;
                                    }

                                    double CessPercent = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.CessPercent].Value);
                                    if (CessPercent > 0)
                                    {
                                        double CessValue = Convert.ToDouble(((CessPercent * TaxAmt) / TaxPerce).ToString("0.000"));
                                        dgBill.Rows[i].Cells[ColIndex.CessAmount].Value = CessValue;
                                    }
                                }
                                else
                                {
                                    double IGSTPercent = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.IGSTPercent].Value);
                                    if (IGSTPercent > 0)
                                    {
                                        double IGSTValue = Convert.ToDouble(((IGSTPercent * TaxAmt) / TaxPerce).ToString("0.000"));
                                        dgBill.Rows[i].Cells[ColIndex.IGSTAmount].Value = IGSTValue;
                                    }

                                    double CessPercent = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.CessPercent].Value);
                                    if (CessPercent > 0)
                                    {
                                        double CessValue = Convert.ToDouble(((CessPercent * TaxAmt) / TaxPerce).ToString("0.000"));
                                        dgBill.Rows[i].Cells[ColIndex.CessAmount].Value = CessValue;
                                    }
                                }
                            }



                            #endregion

                            //#region After tax discount calculation
                            //double DiscAmt2 = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscRupees2].Value);
                            //double DiscAfterTax = DiscAmt2;
                            //#endregion


                            #region Put values in Grid

                            dgBill.Rows[i].Cells[ColIndex.DiscAmount].Value = Disc1.ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value = Disc2.ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.TaxAmount].Value = TaxAmt.ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.NetRate].Value = ttRate.ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.NetAmt].Value = tAmount.ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.ActualQty].Value = (((Qty) + Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.FreeQty].Value)) * (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.StockFactor].Value)));
                            dgBill.Rows[i].Cells[ColIndex.LandedRate].Value = LandedRate.ToString("0.00");
                            #endregion

                            #region Cumulative sum for footer calc usage
                            TotalDiscBeforeTax += DiscBeforeTax;
                            TotalDiscAfterTax += DiscAfterTax;
                            subTotal += tAmount;
                            totalTax += TaxAmt;
                            #endregion

                            #region Calculate total Sale & Exchange Qty
                            if (Qty >= 0)
                                lblBillItem.Text = (Convert.ToDouble(lblBillItem.Text) + Qty).ToString();
                            else
                                lblBilExchangeItem.Text = (Convert.ToDouble(lblBilExchangeItem.Text) + Math.Abs(Qty)).ToString();
                            #endregion

                            TotSchemeDisc = TotSchemeDisc + Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscAmount].Value);
                            TotDistDisc = TotDistDisc + Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value);
                        }
                    }



                    double TotalAmt = 0.0;
                    if (Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_BTaxItemDisc)) != 0)
                    {
                        //subTotal = Convert.ToDouble((subTotal + TotalDiscBeforeTax).ToString("0.00"));// Math.Round(subTotal, 00);
                        subTotal = Convert.ToDouble((subTotal).ToString("0.00"));// Math.Round(subTotal, 00);
                        //TotalAmt = Convert.ToDouble((subTotal - TotalDiscBeforeTax + totalTax - TotalDiscAfterTax).ToString("0.00"));
                        TotalAmt = Convert.ToDouble((subTotal + totalTax - TotalDiscAfterTax).ToString("0.00"));
                    }
                    else
                    {
                        subTotal = Convert.ToDouble(subTotal.ToString("0.00"));// Math.Round(subTotal, 00);
                        TotalAmt = Convert.ToDouble((subTotal + totalTax - TotalDiscAfterTax).ToString("0.00"));
                    }


                    #region footer discount & Charges calculation
                    //txtDiscRupees1.Text = Convert.ToDouble((TotalAmt * Convert.ToDouble(txtDiscount1.Text)) / 100).ToString("0.00");
                    TotalAmt -= Convert.ToDouble(txtDiscRupees1.Text);



                    double TotalAnotherDisc = Convert.ToDouble(txtDiscRupees1.Text);
                    totalChrg = Convert.ToDouble(txtChrgRupees1.Text) + Convert.ToDouble(txtOtherTax.Text);

                    #endregion

                    #region Put Values in Footer TextFields
                    txtSubTotal.Text = subTotal.ToString("0.00");

                    if (Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_BTaxItemDisc)) != 0)
                    {
                        txtTotalItemDisc.Text = TotalDiscBeforeTax.ToString("0.00");
                    }
                    else
                    {
                        txtTotalItemDisc.Text = "0.00";
                        TotalDiscBeforeTax = 0;
                    }
                    // txtTotalDisc.Text = TotalDiscAfterTax.ToString("0.00");
                    txtTotalDisc.Text = TotSchemeDisc.ToString("0.00");
                    txtDistDisc.Text = TotDistDisc.ToString("0.00");
                    txtTotalTax.Text = totalTax.ToString("0.00");

                    txtTotalAnotherDisc.Text = TotalAnotherDisc.ToString("0.00");
                    txtTotalChrgs.Text = totalChrg.ToString("0.00");

                    totalTax = Convert.ToDouble(totalTax.ToString("0.00"));
                    txtGrandTotal.Text = ((subTotal + totalTax + totalChrg) - (TotalDiscAfterTax + TotalAnotherDisc)+Convert.ToDouble(txtFreight.Text)).ToString("0.00");
                    //txtGrandTotal.Text = ((subTotal + totalTax + totalChrg) - (TotalDiscAfterTax + TotalDiscBeforeTax + TotalAnotherDisc)).ToString("0.00");
                    TotFinal = Math.Round(Convert.ToDouble(txtGrandTotal.Text), MidpointRounding.AwayFromZero);
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_IsBillRoundOff)) == true)
                    {
                        txtRoundOff.Text = (TotFinal - Convert.ToDouble(txtGrandTotal.Text)).ToString("0.00");
                        // txtGrandTotal.Text = ((subTotal + totalTax + totalChrg + Convert.ToDouble(txtRoundOff.Text)) - (TotalDiscAfterTax + TotalDiscBeforeTax + TotalAnotherDisc)).ToString("0.00");
                        txtGrandTotal.Text = ((subTotal + totalTax + totalChrg + Convert.ToDouble(txtRoundOff.Text) + Convert.ToDouble(txtFreight.Text)) - (TotalDiscAfterTax + TotalAnotherDisc)).ToString("0.00");
                    }
                    else
                        txtRoundOff.Text = "0.00";
                    #endregion

                    long ControlUnder = ObjQry.ReturnLong("Select ControlUnder From MPayType Where PKPayTypeNo=" + ObjFunction.GetComboValue(cmbPaymentType) + "", CommonFunctions.ConStr);
                    if (ControlUnder == 4)
                    {
                        if (dgPayChqDetails.Rows.Count > 0)
                        {
                            dgPayChqDetails.Rows[0].Cells[4].Value = txtGrandTotal.Text;
                        }
                    }
                    else if (ControlUnder == 5)
                    {
                        if (dgPayCreditCardDetails.Rows.Count > 0)
                        {
                            dgPayCreditCardDetails.Rows[0].Cells[3].Value = txtGrandTotal.Text;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void CalculateTotalCompanywise()
        {
            try
            {
                for (int i = 0; i < dgBill.Rows.Count; i++)
                {
                    if (dgBill.Rows[i].Cells[ColIndex.ItemNo].Value != null && dgBill.Rows[i].Cells[ColIndex.ItemNo].Value.ToString() != "")
                    {
                    }
                }

                double subTotal = 0, totalDisc = 0, totalChrg = 0, totalTax = 0, TotFinal = 0;
                if (Validations() == true)
                {
                    for (int i = 0; i < dgBill.Rows.Count; i++)
                    {
                        if (dgBill.Rows[i].Cells[ColIndex.ItemNo].Value != null && dgBill.Rows[i].Cells[ColIndex.ItemNo].Value.ToString() != "")
                        {
                            if (dgBill.Rows[i].Cells[ColIndex.Quantity].Value == null) dgBill.Rows[i].Cells[ColIndex.Quantity].Value = Convert.ToDouble(1).ToString("0.00");
                            if (dgBill.Rows[i].Cells[ColIndex.MKTQuantity].Value == null) dgBill.Rows[i].Cells[ColIndex.MKTQuantity].Value = 1;
                            if (dgBill.Rows[i].Cells[ColIndex.StockFactor].Value == null) dgBill.Rows[i].Cells[ColIndex.StockFactor].Value = 1;
                            if (dgBill.Rows[i].Cells[ColIndex.Rate].Value == null) dgBill.Rows[i].Cells[ColIndex.Rate].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.DiscRupees].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscRupees].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value = 0;
                            if (dgBill.Rows[i].Cells[ColIndex.DiscRupees2].Value == null) dgBill.Rows[i].Cells[ColIndex.DiscRupees2].Value = 0;

                            double Amount = Convert.ToDouble((((Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value)) * (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Rate].Value))) / (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.MKTQuantity].Value))).ToString("0.0000"));
                            double Disc1 = Convert.ToDouble(((Amount * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value)) / 100).ToString("0.0000"));
                            double Disc2 = Convert.ToDouble(((Amount * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value)) / 100).ToString("0.0000"));

                            double DiscAmt = (Disc1 + Disc2); //Convert.ToDouble(((Amount * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value)) / 100).ToString("0.0000"));
                            //DiscAmt += Convert.ToDouble((((Amount - DiscAmt) * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscPercentage2].Value)) / 100).ToString("0.0000"));
                            DiscAmt += Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscRupees].Value);// +Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscRupees2].Value);

                            DiscAmt = 0;//Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscRupees2].Value);
                            double tAmount = Amount - DiscAmt;
                            double TaxPerce = Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.TaxPercentage].Value);
                            double TaxAmt = Convert.ToDouble(((tAmount * TaxPerce) / (100 + TaxPerce)).ToString("0.0000"));
                            totalTax += TaxAmt;
                            double ttRate = (tAmount - TaxAmt) / Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value);


                            dgBill.Rows[i].Cells[ColIndex.Amount].Value = tAmount.ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.DiscAmount].Value = Disc1.ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.DiscAmount].Value = Disc2.ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.TaxAmount].Value = TaxAmt.ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.NetRate].Value = ttRate.ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.NetAmt].Value = (ttRate * Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value)).ToString("0.00");
                            dgBill.Rows[i].Cells[ColIndex.ActualQty].Value = (((Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value)) + (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.FreeQty].Value))) * (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.StockFactor].Value)));

                            subTotal = subTotal + Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.NetAmt].Value);
                            totalDisc = totalDisc + DiscAmt;// Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.DiscAmount].Value);

                        }
                    }
                    subTotal = Math.Round(subTotal, 00);
                    txtSubTotal.Text = (subTotal + totalDisc).ToString("0.00");
                    txtTotalDisc.Text = totalDisc.ToString("0.00");

                    double TotalAmt = Convert.ToDouble(((Convert.ToDouble(txtSubTotal.Text) + totalTax) - Convert.ToDouble(txtTotalDisc.Text) - Convert.ToDouble(txtDistDisc.Text)).ToString("0.00"));
                    // txtDiscRupees1.Text = Convert.ToDouble((TotalAmt * Convert.ToDouble(txtDiscount1.Text)) / 100).ToString("0.00");

                    TotalAmt -= Convert.ToDouble(txtDiscRupees1.Text) + Convert.ToDouble(txtFreight.Text);


                    totalDisc = Convert.ToDouble(txtDiscRupees1.Text);
                    totalChrg = Convert.ToDouble(txtChrgRupees1.Text) + Convert.ToDouble(txtOtherTax.Text);
                    txtTotalAnotherDisc.Text = totalDisc.ToString("0.00");
                    txtTotalChrgs.Text = totalChrg.ToString("0.00");
                    txtTotalTax.Text = totalTax.ToString("0.00");
                    totalTax = Math.Round(totalTax, 00);
                    txtGrandTotal.Text = ((subTotal + totalTax + totalChrg) - totalDisc).ToString("0.00");
                    TotFinal = Math.Round(Convert.ToDouble(txtGrandTotal.Text), MidpointRounding.AwayFromZero);
                    txtRoundOff.Text = (TotFinal - Convert.ToDouble(txtGrandTotal.Text)).ToString("0.00");
                    txtGrandTotal.Text = ((subTotal + totalTax + totalChrg + Convert.ToDouble(txtRoundOff.Text)) - totalDisc).ToString("0.00");
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void txtDiscount1_Leave(object sender, EventArgs e)
        {
            txtDiscRupees1.Focus();
            //txtDiscRupees.Focus();
            CalculateTotal();
        }

        private void txtChrg1_Leave(object sender, EventArgs e)
        {
            txtChrgRupees1.Focus();
            CalculateTotal();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SetValue();
        }

        private void DeleteRefDetails()
        {
            try
            {
                if (PayType == 3)
                {
                    if (PayType != ObjFunction.GetComboValue(cmbPaymentType) && ID != 0)
                    {
                        DataTable dtRef = ObjFunction.GetDataView("Select PKRefTrnNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FKVoucherNo=" + ID + " ").Table;
                        for (int i = 0; i < dtRef.Rows.Count; i++)
                        {
                            tVchRefDtls = new TVoucherRefDetails();
                            tVchRefDtls.PkRefTrnNo = Convert.ToInt64(dtRef.Rows[i].ItemArray[0].ToString());
                            dbTVoucherEntry.DeleteTVoucherRefDetails(tVchRefDtls);
                        }
                    }
                }
                else if (PayType != 3)
                {
                    if (ObjFunction.GetComboValue(cmbPaymentType) == 3)
                    {
                        if ((PartyNo != ObjFunction.GetComboValue(cmbPartyName) || PayType != ObjFunction.GetComboValue(cmbPaymentType)) && ID != 0)
                        {
                            DataTable dtDelPayType = ObjFunction.GetDataView("Select PKVoucherPayTypeNo,TVoucherPayTypeDetails.CompanyNo,FKReceiptVoucherNo,Amount From TVoucherPayTypeDetails,TVoucherDetails Where TVoucherDetails.PkVoucherTrnNo=TVoucherPayTypeDetails.FKVoucherTrnNo AND FKSalesVoucherNo=" + ID + "  order by PKVoucherPayTypeNo").Table;
                            for (int k = 0; k < dtDelPayType.Rows.Count; k++)
                            {
                                tVchPayTypeDetails = new TVoucherPayTypeDetails();
                                tVchPayTypeDetails.PKVoucherPayTypeNo = Convert.ToInt64(dtDelPayType.Rows[k].ItemArray[0].ToString());
                                dbTVoucherEntry.DeleteTVoucherPayTypeDetails(tVchPayTypeDetails);

                                DataTable dtUpdateVoucher = ObjFunction.GetDataView("Select PKVoucherTrnNo,Debit,Credit From TVoucherDetails Where FKVoucherNo=" + dtDelPayType.Rows[k].ItemArray[2].ToString() + " AND CompanyNo=" + dtDelPayType.Rows[k].ItemArray[1].ToString() + "").Table;
                                double totamt = 0;
                                bool alllowdel = false;
                                for (int m = 0; m < dtUpdateVoucher.Rows.Count; m++)
                                {
                                    double DrAmt = Convert.ToDouble(dtUpdateVoucher.Rows[m].ItemArray[1].ToString());
                                    double CrAmt = Convert.ToDouble(dtUpdateVoucher.Rows[m].ItemArray[2].ToString());
                                    if (DrAmt > 0) DrAmt = DrAmt - Convert.ToDouble(dtDelPayType.Rows[k].ItemArray[3].ToString());
                                    if (CrAmt > 0) CrAmt = CrAmt - Convert.ToDouble(dtDelPayType.Rows[k].ItemArray[3].ToString());
                                    if (m == 0)
                                    {
                                        dbTVoucherEntry.UpdateVoucher((DrAmt + CrAmt), Convert.ToInt64(dtDelPayType.Rows[k].ItemArray[2].ToString()));
                                    }
                                    dbTVoucherEntry.UpdateVoucherDetails(DrAmt, CrAmt, Convert.ToInt64(dtUpdateVoucher.Rows[m].ItemArray[0].ToString()));
                                    totamt = totamt + DrAmt + CrAmt;
                                    alllowdel = true;
                                }

                                if (totamt == 0 && alllowdel == true)
                                {
                                    for (int m = 0; m < dtUpdateVoucher.Rows.Count; m++)
                                    {
                                        tVoucherDetails = new TVoucherDetails();
                                        tVoucherDetails.PkVoucherTrnNo = Convert.ToInt64(dtUpdateVoucher.Rows[m].ItemArray[0].ToString());
                                        dbTVoucherEntry.DeleteTVoucherDetails(tVoucherDetails);

                                        if (m == dtUpdateVoucher.Rows.Count - 1)
                                        {
                                            if (ObjQry.ReturnLong("Select Count(*) From TVoucherDetails Where FKVoucherNo=" + Convert.ToInt64(dtDelPayType.Rows[k].ItemArray[2].ToString()) + "", CommonFunctions.ConStr) >= dtUpdateVoucher.Rows.Count)
                                            {
                                                tVoucherEntry = new TVoucherEntry();
                                                tVoucherEntry.PkVoucherNo = Convert.ToInt64(dtDelPayType.Rows[k].ItemArray[2].ToString());
                                                dbTVoucherEntry.DeleteTVoucherEntry1(tVoucherEntry);
                                            }
                                        }
                                    }
                                }



                            }
                        }
                    }
                    long ControlUnder = ObjQry.ReturnLong("Select ControlUnder From MPayType Where PKPayTypeNo=" + PayType + "", CommonFunctions.ConStr);
                    //if (PayType == 5 || PayType == 4)
                    if (ControlUnder == 5 || ControlUnder == 4)
                    {
                        if (PayType != ObjFunction.GetComboValue(cmbPaymentType) && ID != 0)
                        {
                            DataTable dtCredit = ObjFunction.GetDataView("Select PKSrNo From TVoucherChqCreditDetails  Where FKVoucherNo=" + ID + " ").Table;
                            for (int i = 0; i < dtCredit.Rows.Count; i++)
                            {
                                tVchChqCredit = new TVoucherChqCreditDetails();
                                tVchChqCredit.PkSrNo = Convert.ToInt64(dtCredit.Rows[i].ItemArray[0].ToString());
                                dbTVoucherEntry.DeleteTVoucherChqCreditDetails(tVchChqCredit);
                            }
                            //if (PayType == 4)
                            if (ControlUnder == 4)
                                while (dgPayChqDetails.Rows.Count > 0)
                                    dgPayChqDetails.Rows.RemoveAt(0);
                            //if (PayType == 5)
                            if (ControlUnder == 5)
                                while (dgPayCreditCardDetails.Rows.Count > 0)
                                    dgPayCreditCardDetails.Rows.RemoveAt(0);
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void SetValue()
        {
            try
            {
                double debit = 0;
                //long temp = 0;
                if (ValidationsMain() == false) return;
                if (Convert.ToDouble(txtGrandTotal.Text) < 0)
                {
                    OMMessageBox.Show("Negative Bill amount not allowed.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    return;
                }
                for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                {
                    if (dgBill.Rows.Count <= 1)
                    {
                        OMMessageBox.Show("Atleast one item required.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        dgBill.CurrentCell = dgBill[ColIndex.ItemName, dgBill.Rows.Count - 1];
                        dgBill.Focus();
                        return;
                    }
                    else if (Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.Quantity].Value) == 0 && Convert.ToDouble(dgBill.Rows[i].Cells[ColIndex.FreeQty].Value) == 0)
                    {
                        DeleteZeroQty(i);
                        i--;
                    }
                }
                if (dgBill.Rows.Count <= 1)
                {
                    OMMessageBox.Show("Atleast one item required.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    dgBill.CurrentCell = dgBill[ColIndex.ItemName, dgBill.Rows.Count - 1];
                    dgBill.Focus();
                    return;
                }
                if (ID != 0)
                {
                    if (Convert.ToDouble(txtDiscRupees1.Text) == 0) DeleteDtls(2, GetVoucherPK("SrNo=" + Others.Discount1));
                    if (Convert.ToDouble(txtFreight.Text) == 0) DeleteDtls(2, GetVoucherPK("SrNo=" + Others.Freight));
                    if (Convert.ToDouble(txtChrgRupees1.Text) == 0) DeleteDtls(2, GetVoucherPK("SrNo=" + Others.Charges1));
                    if (Convert.ToDouble(txtOtherTax.Text) == 0) DeleteDtls(2, GetVoucherPK("SrNo=" + Others.Charges2));
                    if (Convert.ToDouble(txtRoundOff.Text) == 0) DeleteDtls(2, GetVoucherPK("SrNo=" + Others.RoundOff));
                    if (Convert.ToDouble(txtTotalDisc.Text) == 0) DeleteDtls(2, GetVoucherPK("SrNo=" + Others.ItemDisc));
                    if (Convert.ToDouble(txtTotalItemDisc.Text) == 0) DeleteDtls(2, GetVoucherPK("SrNo=" + Others.BTaxItemDisc));
                }
                CalculateTotal();
                dbTVoucherEntry = new DBTVaucherEntry();
                DeleteRefDetails();
                UpdateCurrentStock();
                DeleteValues();//Delete Old Values

                if (ID != 0)
                    dbTVoucherEntry.ReverseStock(ID);
                int VoucherSrNo = 1;
                //Voucher Header Entry 
                tVoucherEntry = new TVoucherEntry();
                tVoucherEntry.PkVoucherNo = ID;
                tVoucherEntry.VoucherTypeCode = VoucherType;
                tVoucherEntry.VoucherUserNo = VoucherUserNo;
                tVoucherEntry.VoucherDate = Convert.ToDateTime(dtpBillDate.Text);
                tVoucherEntry.VoucherTime = dtpBillTime.Value;
                tVoucherEntry.Narration = "Purchase Bill";
                tVoucherEntry.Reference = txtRefNo.Text;
                tVoucherEntry.ChequeNo = 0;
                tVoucherEntry.ClearingDate = dtpBillDate.Value;
                tVoucherEntry.CompanyNo = DBGetVal.CompanyNo;
                tVoucherEntry.BilledAmount = Convert.ToDouble(txtGrandTotal.Text);
                tVoucherEntry.ChallanNo = "";
                tVoucherEntry.OrderType = 1;
                tVoucherEntry.Remark = txtRemark.Text.Trim();
                tVoucherEntry.MacNo = DBGetVal.MacNo;
                tVoucherEntry.PayTypeNo = ObjFunction.GetComboValue(cmbPaymentType);
                tVoucherEntry.RateTypeNo = GetRateType();
                tVoucherEntry.TaxTypeNo = ObjFunction.GetComboValue(cmbTaxType);
                tVoucherEntry.ReturnAmount = Convert.ToDouble(txtReturnAmt.Text.Trim());
                tVoucherEntry.Visibility = Convert.ToDouble(txtVisibility.Text.Trim());
                tVoucherEntry.MixMode = MixModeVal;
                tVoucherEntry.UserID = DBGetVal.UserID;
                tVoucherEntry.UserDate = DBGetVal.ServerTime.Date;
                dbTVoucherEntry.AddTVoucherEntry(tVoucherEntry);

                DataTable dtVoucherDetails = new DataTable();
                dtVchPrev = new DataTable();
                if (ID != 0)
                {
                    dtVoucherDetails = ObjFunction.GetDataView("Select PkVoucherTrnNo,LedgerNo,0 AS StatusNo From TVoucherDetails Where FkVoucherNo=" + ID + " order by VoucherSrNo").Table;
                    dtVchPrev = ObjFunction.GetDataView("Select LedgerNo,Credit,CompanyNo From TVoucherDetails Where FkVoucherNo=" + ID + " order by VoucherSrNo").Table;
                }
                setCompanyRatio();
                FillPayType();



                //Party Account
                tVoucherDetails = new TVoucherDetails();
                tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                tVoucherDetails.VoucherSrNo = VoucherSrNo; VoucherSrNo += 1; ObjFunction.SetVouchers(dtVoucherDetails, tVoucherDetails.PkVoucherTrnNo);
                tVoucherDetails.SignCode = 2;//1;
                tVoucherDetails.LedgerNo = ObjFunction.GetComboValue(cmbPartyName);
                tVoucherDetails.Debit = 0;//(Convert.ToDouble(dtCompRatio.Rows[j].ItemArray[1].ToString()) * Convert.ToDouble(txtGrandTotal.Text)) / 10;
                //tVoucherDetails.Credit = (Convert.ToDouble(dtCompRatio.Rows[j].ItemArray[1].ToString()) * Convert.ToDouble(txtGrandTotal.Text)) / 10;
                tVoucherDetails.Credit = Convert.ToDouble(txtGrandTotal.Text);
                tVoucherDetails.Narration = "";
                //tVoucherDetails.CompanyNo = Convert.ToInt64(dtCompRatio.Rows[j].ItemArray[0].ToString());
                tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                tVoucherDetails.SrNo = Others.Party;
                dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);

                for (int row = 0; row < dgPayChqDetails.Rows.Count; row++)
                {
                    if (dgPayChqDetails.Rows[row].Cells[0].Value != null && dgPayChqDetails.Rows[row].Cells[0].Value.ToString() != "")
                    {
                        tVchChqCredit.PkSrNo = Convert.ToInt64(dgPayChqDetails.Rows[row].Cells[5].Value);
                        tVchChqCredit.ChequeNo = dgPayChqDetails.Rows[row].Cells[0].Value.ToString();
                        tVchChqCredit.ChequeDate = (dgPayChqDetails.Rows[row].Cells[1].Value == null) ? Convert.ToDateTime("01-Jan-1900") : Convert.ToDateTime(dgPayChqDetails.Rows[row].Cells[1].Value);
                        tVchChqCredit.BankNo = Convert.ToInt64(dgPayChqDetails.Rows[row].Cells[6].Value);
                        tVchChqCredit.BranchNo = 0;
                        tVchChqCredit.CreditCardNo = "";
                        tVchChqCredit.Amount = Convert.ToDouble(txtGrandTotal.Text);//Convert.ToDouble(dgPayChqDetails.Rows[row].Cells[4].Value);
                        tVchChqCredit.PostFkVoucherNo = 0;
                        tVchChqCredit.PostFkVoucherTrnNo = 0;
                        tVchChqCredit.CompanyNo = DBGetVal.CompanyNo;
                        dbTVoucherEntry.AddTVoucherChqCreditDetails(tVchChqCredit);
                    }
                }

                for (int row = 0; row < dgPayCreditCardDetails.Rows.Count; row++)
                {
                    if (dgPayCreditCardDetails.Rows[row].Cells[0].Value != null && dgPayCreditCardDetails.Rows[row].Cells[0].Value.ToString() != "")
                    {
                        tVchChqCredit.PkSrNo = Convert.ToInt64(dgPayCreditCardDetails.Rows[row].Cells[4].Value);
                        tVchChqCredit.CreditCardNo = dgPayCreditCardDetails.Rows[row].Cells[0].Value.ToString();
                        tVchChqCredit.ChequeDate = Convert.ToDateTime("01-Jan-1900");
                        tVchChqCredit.BankNo = Convert.ToInt64(dgPayCreditCardDetails.Rows[row].Cells[5].Value);
                        tVchChqCredit.BranchNo = 0;
                        tVchChqCredit.ChequeNo = "";
                        tVchChqCredit.Amount = Convert.ToDouble(txtGrandTotal.Text);//Convert.ToDouble(dgPayCreditCardDetails.Rows[row].Cells[3].Value);
                        tVchChqCredit.PostFkVoucherNo = 0;
                        tVchChqCredit.PostFkVoucherTrnNo = 0;
                        tVchChqCredit.CompanyNo = DBGetVal.CompanyNo;
                        dbTVoucherEntry.AddTVoucherChqCreditDetails(tVchChqCredit);
                    }
                }
                if (Convert.ToDouble(dgPayType.Rows[2].Cells[2].Value) != 0)
                {
                    //Reference Entry
                    tVchRefDtls = new TVoucherRefDetails();
                    tVchRefDtls.PkRefTrnNo = ObjQry.ReturnLong("Select PKRefTrnNo From TVoucherRefDetails Where FKVoucherTrnNo=" + tVoucherDetails.PkVoucherTrnNo + " ", CommonFunctions.ConStr);
                    tVchRefDtls.FkVoucherSrNo = tVoucherDetails.VoucherSrNo;
                    tVchRefDtls.LedgerNo = tVoucherDetails.LedgerNo;
                    tVchRefDtls.TypeOfRef = 3;
                    tVchRefDtls.RefNo = 0;
                    tVchRefDtls.DueDays = 0;
                    tVchRefDtls.DueDate = DBGetVal.ServerTime;
                    tVchRefDtls.Amount = tVoucherEntry.BilledAmount;
                    tVchRefDtls.SignCode = 2;
                    tVchRefDtls.UserID = DBGetVal.UserID;
                    tVchRefDtls.UserDate = DBGetVal.ServerTime.Date;
                    tVchRefDtls.CompanyNo = DBGetVal.CompanyNo;
                    dbTVoucherEntry.AddTVoucherRefDetails(tVchRefDtls);
                }

                for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                {


                    // Add record in TStock Table
                    tStock = new TStock();
                    if (Convert.ToInt64(dgBill[ColIndex.PkStockTrnNo, i].Value) == 0)
                    {
                        tStock.PkStockTrnNo = 0;
                    }
                    else
                    {
                        tStock.PkStockTrnNo = Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.PkStockTrnNo].Value);
                    }

                    tStock.GroupNo = GroupType.CapitalAccount;
                    tStock.ItemNo = Convert.ToInt64(dgBill[ColIndex.ItemNo, i].Value.ToString());
                    tStock.FkVoucherSrNo = VoucherSrNo;
                    tStock.TrnCode = 1;
                    tStock.Quantity = Convert.ToDouble(dgBill[ColIndex.Quantity, i].Value.ToString());
                    tStock.BilledQuantity = Convert.ToDouble(dgBill[ColIndex.ActualQty, i].Value.ToString());
                    tStock.Rate = Convert.ToDouble(dgBill[ColIndex.Rate, i].Value.ToString());
                    tStock.Amount = Convert.ToDouble(dgBill[ColIndex.Amount, i].Value.ToString());
                    tStock.TaxPercentage = Convert.ToDouble(dgBill[ColIndex.TaxPercentage, i].Value.ToString());
                    tStock.TaxAmount = Convert.ToDouble(dgBill[ColIndex.TaxAmount, i].Value.ToString());
                    tStock.DiscPercentage = Convert.ToDouble(dgBill[ColIndex.DiscPercentage, i].Value.ToString());
                    tStock.DiscAmount = Convert.ToDouble(dgBill[ColIndex.DiscAmount, i].Value.ToString());
                    tStock.DiscRupees = Convert.ToDouble(dgBill[ColIndex.DiscRupees, i].Value.ToString());
                    tStock.DiscPercentage2 = Convert.ToDouble(dgBill[ColIndex.DiscPercentage2, i].Value.ToString());
                    tStock.DiscAmount2 = Convert.ToDouble(dgBill[ColIndex.DiscAmount2, i].Value.ToString());
                    tStock.DiscRupees2 = Convert.ToDouble(dgBill[ColIndex.DiscRupees2, i].Value.ToString());
                    tStock.NetRate = Convert.ToDouble(dgBill[ColIndex.NetRate, i].Value.ToString());
                    tStock.NetAmount = Convert.ToDouble(dgBill[ColIndex.NetAmt, i].Value.ToString());
                    tStock.FkStockBarCodeNo = Convert.ToInt64(dgBill[ColIndex.PkBarCodeNo, i].Value.ToString());
                    tStock.FkUomNo = Convert.ToInt64(dgBill[ColIndex.UOMNo, i].Value.ToString());
                    tStock.FkRateSettingNo = Convert.ToInt64(dgBill[ColIndex.PkRateSettingNo, i].Value.ToString());
                    tStock.FkItemTaxInfo = Convert.ToInt64(dgBill[ColIndex.PkItemTaxInfo, i].Value.ToString());
                    tStock.FreeQty = Convert.ToDouble(dgBill[ColIndex.FreeQty, i].Value.ToString());
                    tStock.FreeUOMNo = 1;//Convert.ToInt64(dgBill[ColIndex.FreeUomNo, i].Value.ToString());
                    tStock.UserID = DBGetVal.UserID;
                    tStock.UserDate = DBGetVal.ServerTime.Date;
                    tStock.CompanyNo = Convert.ToInt64(dgBill[ColIndex.StockCompanyNo, i].Value.ToString());
                    tStock.LandedRate = Convert.ToDouble(dgBill[ColIndex.LandedRate, i].Value.ToString());

                    tStock.MRP = Convert.ToDouble(dgBill[ColIndex.MRP, i].Value.ToString());
                    tStock.GodownNo = Convert.ToInt64(dgBill[ColIndex.GodownNo, i].Value.ToString());

                    #region GST Related fields
                    tStock.HSNCode = dgBill[ColIndex.HSNCode, i].EditedFormattedValue.ToString();
                    tStock.IGSTPercent = Convert.ToDouble(dgBill[ColIndex.IGSTPercent, i].EditedFormattedValue.ToString());
                    tStock.IGSTAmount = Convert.ToDouble(dgBill[ColIndex.IGSTAmount, i].EditedFormattedValue.ToString());
                    tStock.CGSTPercent = Convert.ToDouble(dgBill[ColIndex.CGSTPercent, i].EditedFormattedValue.ToString());
                    tStock.CGSTAmount = Convert.ToDouble(dgBill[ColIndex.CGSTAmount, i].EditedFormattedValue.ToString());
                    tStock.SGSTPercent = Convert.ToDouble(dgBill[ColIndex.SGSTPercent, i].EditedFormattedValue.ToString());
                    tStock.SGSTAmount = Convert.ToDouble(dgBill[ColIndex.SGSTAmount, i].EditedFormattedValue.ToString());
                    tStock.UTGSTPercent = Convert.ToDouble(dgBill[ColIndex.UTGSTPercent, i].EditedFormattedValue.ToString());
                    tStock.UTGSTAmount = Convert.ToDouble(dgBill[ColIndex.UTGSTAmount, i].EditedFormattedValue.ToString());
                    tStock.CessPercent = Convert.ToDouble(dgBill[ColIndex.CessPercent, i].EditedFormattedValue.ToString());
                    tStock.CessAmount = Convert.ToDouble(dgBill[ColIndex.CessAmount, i].EditedFormattedValue.ToString());
                    #endregion


                    dbTVoucherEntry.AddTStock(tStock);
                }


                //For Discount Ledger 1 %
                if (Convert.ToDouble(txtDiscRupees1.Text) != 0)
                {
                    tVoucherDetails = new TVoucherDetails();
                    tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                    tVoucherDetails.VoucherSrNo = VoucherSrNo; VoucherSrNo += 1; ObjFunction.SetVouchers(dtVoucherDetails, tVoucherDetails.PkVoucherTrnNo);
                    tVoucherDetails.SignCode = 2;//1;
                    tVoucherDetails.LedgerNo = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_Discount1));
                    tVoucherDetails.Debit = 0;
                    tVoucherDetails.Credit = Convert.ToDouble(txtDiscRupees1.Text);
                    tVoucherDetails.Narration = "";
                    tVoucherDetails.SrNo = Others.Discount1;
                    tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                    dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                }

                if (Convert.ToDouble(txtFreight.Text) != 0)
                {
                    tVoucherDetails = new TVoucherDetails();
                    tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                    tVoucherDetails.VoucherSrNo = VoucherSrNo; VoucherSrNo += 1; ObjFunction.SetVouchers(dtVoucherDetails, tVoucherDetails.PkVoucherTrnNo);
                    tVoucherDetails.SignCode = 2;//1;
                    tVoucherDetails.LedgerNo = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_Discount1));
                    tVoucherDetails.Debit = 0;
                    tVoucherDetails.Credit = Convert.ToDouble(txtFreight.Text);
                    tVoucherDetails.Narration = "";
                    tVoucherDetails.SrNo = Others.Freight;
                    tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                    dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                }

                //Item Purchase Ledger Details
                DataTable dtSaleLedger = new DataTable();
                bool TempFlag = false;
                dtSaleLedger.Columns.Add();
                dtSaleLedger.Columns.Add();
                DataRow dr = dtSaleLedger.NewRow();
                dr[0] = Convert.ToInt64(dgBill.Rows[0].Cells[ColIndex.SalesLedgerNo].Value);
                dr[1] = Convert.ToInt64(dgBill.Rows[0].Cells[ColIndex.StockCompanyNo].Value);
                dtSaleLedger.Rows.Add(dr);
                for (int k = 1; k < dgBill.Rows.Count - 1; k++)
                {
                    for (int i = 0; i < dtSaleLedger.Rows.Count; i++)
                    {
                        if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.SalesLedgerNo].Value) != Convert.ToInt64(dtSaleLedger.Rows[i].ItemArray[0].ToString()) || Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.StockCompanyNo].Value) != Convert.ToInt64(dtSaleLedger.Rows[i].ItemArray[1].ToString()))
                        {
                            TempFlag = true;

                        }
                        else
                        {
                            TempFlag = false;
                            break;
                        }
                    }
                    if (TempFlag == true)
                    {
                        dr = dtSaleLedger.NewRow();
                        dr[0] = Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.SalesLedgerNo].Value);
                        dr[1] = Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.StockCompanyNo].Value);
                        dtSaleLedger.Rows.Add(dr);
                    }
                }

                int ledgerNo = 0; int cnt = VoucherSrNo - 1, cntLedg = -1, cntTaxLedg = -1;
                for (int k = 0; k < dtSaleLedger.Rows.Count; k++)
                {
                    cntLedg = -1;
                    for (int j = 0; j < dgBill.Rows.Count - 1; j++)
                    {
                        if (Convert.ToInt64(dgBill.Rows[j].Cells[ColIndex.SalesLedgerNo].Value) == Convert.ToInt64(dtSaleLedger.Rows[k].ItemArray[0].ToString()) && Convert.ToInt64(dgBill.Rows[j].Cells[ColIndex.StockCompanyNo].Value) == Convert.ToInt64(dtSaleLedger.Rows[k].ItemArray[1].ToString()))
                        {
                            if (cntLedg == -1) cntLedg = j;
                            if (Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_BTaxItemDisc)) != 0)
                            {
                                debit = debit + Convert.ToDouble(dgBill.Rows[j].Cells[ColIndex.NetAmt].Value)
                                    + Convert.ToDouble(dgBill.Rows[j].Cells[ColIndex.DiscAmount].Value)
                                    + Convert.ToDouble(dgBill.Rows[j].Cells[ColIndex.DiscRupees].Value)
                                    + Convert.ToDouble(dgBill.Rows[j].Cells[ColIndex.DiscAmount2].Value);
                            }
                            else
                            {
                                debit = debit + Convert.ToDouble(dgBill.Rows[j].Cells[ColIndex.NetAmt].Value);
                            }
                            //debit = debit + Convert.ToDouble(dgBill.Rows[j].Cells[ColIndex.NetAmt].Value);
                            dgBill.Rows[j].Cells[ColIndex.SalesVchNo].Value = dgBill.Rows[cntLedg].Cells[ColIndex.SalesVchNo].Value;
                            ledgerNo = j;
                        }
                    }
                    if (debit > 0)
                    {
                        tVoucherDetails = new TVoucherDetails();
                        if (dtVoucherDetails.Rows.Count > 0)
                        {
                            if (Convert.ToInt64(dgBill.Rows[cntLedg].Cells[ColIndex.SalesVchNo].Value) != 0)
                            {
                                tVoucherDetails.PkVoucherTrnNo = Convert.ToInt64(dgBill.Rows[cntLedg].Cells[ColIndex.SalesVchNo].Value);// Convert.ToInt64(dtVoucherDetails.Rows[cnt].ItemArray[0].ToString());
                                cnt++;
                            }
                            else
                            {
                                tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                            }
                            tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                        }
                        else
                        {
                            tVoucherDetails.PkVoucherTrnNo = 0;
                        }
                        tVoucherDetails.VoucherSrNo = VoucherSrNo; VoucherSrNo += 1; ObjFunction.SetVouchers(dtVoucherDetails, tVoucherDetails.PkVoucherTrnNo);
                        tVoucherDetails.SignCode = 1;//2;
                        tVoucherDetails.LedgerNo = Convert.ToInt64(dgBill.Rows[ledgerNo].Cells[ColIndex.SalesLedgerNo].Value);
                        tVoucherDetails.Debit = debit;// 0;
                        tVoucherDetails.CompanyNo = Convert.ToInt64(dgBill.Rows[ledgerNo].Cells[ColIndex.StockCompanyNo].Value);
                        tVoucherDetails.Credit = 0;
                        tVoucherDetails.Narration = "";
                        dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                        debit = 0;
                    }
                }

                //Item Tax Details
                DataTable dtTAxLedger = new DataTable();
                TempFlag = false;
                dtTAxLedger.Columns.Add();
                dtTAxLedger.Columns.Add();
                dr = dtTAxLedger.NewRow();
                dr[0] = Convert.ToInt64(dgBill.Rows[0].Cells[ColIndex.TaxLedgerNo].Value);
                dr[1] = Convert.ToInt64(dgBill.Rows[0].Cells[ColIndex.StockCompanyNo].Value);
                dtTAxLedger.Rows.Add(dr);
                for (int k = 1; k < dgBill.Rows.Count - 1; k++)
                {
                    TempFlag = false;
                    for (int i = 0; i < dtTAxLedger.Rows.Count; i++)
                    {
                        if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.TaxLedgerNo].Value) != Convert.ToInt64(dtTAxLedger.Rows[i].ItemArray[0].ToString()) || Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.StockCompanyNo].Value) != Convert.ToInt64(dtTAxLedger.Rows[i].ItemArray[1].ToString()))
                        {
                            TempFlag = true;
                        }
                        else
                        {
                            TempFlag = false;
                            break;
                        }
                    }
                    if (TempFlag == true)
                    {
                        dr = dtTAxLedger.NewRow();
                        dr[0] = Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.TaxLedgerNo].Value);
                        dr[1] = Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.StockCompanyNo].Value);
                        dtTAxLedger.Rows.Add(dr);
                    }
                }

                cnt = VoucherSrNo - 1;
                debit = 0;
                ledgerNo = 0;
                for (int k = 0; k < dtTAxLedger.Rows.Count; k++)
                {
                    cntTaxLedg = -1;
                    for (int j = 0; j < dgBill.Rows.Count - 1; j++)
                    {
                        if (Convert.ToInt64(dgBill.Rows[j].Cells[ColIndex.TaxLedgerNo].Value) == Convert.ToInt64(dtTAxLedger.Rows[k].ItemArray[0].ToString()) && Convert.ToInt64(dgBill.Rows[j].Cells[ColIndex.StockCompanyNo].Value) == Convert.ToInt64(dtTAxLedger.Rows[k].ItemArray[1].ToString()))
                        {
                            if (cntTaxLedg == -1) cntTaxLedg = j;
                            debit = debit + Convert.ToDouble(dgBill.Rows[j].Cells[ColIndex.TaxAmount].Value);
                            dgBill.Rows[j].Cells[ColIndex.TaxVchNo].Value = dgBill.Rows[cntTaxLedg].Cells[ColIndex.TaxVchNo].Value;
                            ledgerNo = j;
                        }
                    }
                    if (debit > 0)
                    {
                        tVoucherDetails = new TVoucherDetails();
                        if (dtVoucherDetails.Rows.Count > 0)
                        {
                            if (Convert.ToInt64(dgBill.Rows[cntTaxLedg].Cells[ColIndex.TaxVchNo].Value) != 0)
                            {
                                tVoucherDetails.PkVoucherTrnNo = Convert.ToInt64(dgBill.Rows[cntTaxLedg].Cells[ColIndex.TaxVchNo].Value);// Convert.ToInt64(dtVoucherDetails.Rows[cnt].ItemArray[0].ToString());
                                cnt++;
                            }
                            else
                            {
                                tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                            }
                            tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                        }
                        else
                        {
                            tVoucherDetails.PkVoucherTrnNo = 0;
                        }
                        tVoucherDetails.VoucherSrNo = VoucherSrNo; VoucherSrNo += 1; ObjFunction.SetVouchers(dtVoucherDetails, tVoucherDetails.PkVoucherTrnNo);
                        tVoucherDetails.SignCode = 1;//2;
                        tVoucherDetails.LedgerNo = Convert.ToInt64(dgBill.Rows[ledgerNo].Cells[ColIndex.TaxLedgerNo].Value);
                        tVoucherDetails.Debit = debit; //0;
                        tVoucherDetails.CompanyNo = Convert.ToInt64(dgBill.Rows[ledgerNo].Cells[ColIndex.StockCompanyNo].Value);
                        tVoucherDetails.Credit = 0; //debit;// 
                        tVoucherDetails.Narration = "";
                        dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                        debit = 0;
                    }
                }

                //Item Discount Before Tax Entry
                if (Convert.ToDouble(txtTotalItemDisc.Text) != 0)
                {
                    tVoucherDetails = new TVoucherDetails();
                    tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                    tVoucherDetails.VoucherSrNo = VoucherSrNo; VoucherSrNo += 1; ObjFunction.SetVouchers(dtVoucherDetails, tVoucherDetails.PkVoucherTrnNo);
                    tVoucherDetails.SignCode = 2;
                    tVoucherDetails.LedgerNo = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_BTaxItemDisc));
                    tVoucherDetails.Debit = 0;
                    tVoucherDetails.Credit = Convert.ToDouble(txtTotalItemDisc.Text);
                    tVoucherDetails.Narration = "";
                    tVoucherDetails.SrNo = Others.BTaxItemDisc;
                    tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                    dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                }

                //Item Discount After Tax Entry
                if (Convert.ToDouble(txtTotalDisc.Text) != 0)
                {
                    tVoucherDetails = new TVoucherDetails();
                    tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                    tVoucherDetails.VoucherSrNo = VoucherSrNo; VoucherSrNo += 1; ObjFunction.SetVouchers(dtVoucherDetails, tVoucherDetails.PkVoucherTrnNo);
                    tVoucherDetails.SignCode = 2;
                    tVoucherDetails.LedgerNo = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_ATaxItemDisc));
                    tVoucherDetails.Debit = 0;
                    tVoucherDetails.Credit = Convert.ToDouble(txtTotalDisc.Text);
                    tVoucherDetails.Narration = "";
                    tVoucherDetails.SrNo = Others.ItemDisc;
                    tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                    dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                }

                //=========Debit Entrys=========================

                //For Charges Rupees 1
                if (Convert.ToDouble(txtChrgRupees1.Text) != 0)
                {
                    tVoucherDetails = new TVoucherDetails();
                    tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                    tVoucherDetails.VoucherSrNo = VoucherSrNo; VoucherSrNo += 1; ObjFunction.SetVouchers(dtVoucherDetails, tVoucherDetails.PkVoucherTrnNo);
                    tVoucherDetails.SignCode = 1;//2;
                    tVoucherDetails.LedgerNo = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_Charges1));
                    tVoucherDetails.Debit = Convert.ToDouble(txtChrgRupees1.Text);
                    tVoucherDetails.Credit = 0;
                    tVoucherDetails.Narration = "";
                    tVoucherDetails.SrNo = Others.Charges1;
                    tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                    dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                }

                //For Charges Rupees 1
                if (Convert.ToDouble(txtOtherTax.Text) != 0)
                {
                    tVoucherDetails = new TVoucherDetails();
                    tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                    tVoucherDetails.VoucherSrNo = VoucherSrNo; VoucherSrNo += 1; ObjFunction.SetVouchers(dtVoucherDetails, tVoucherDetails.PkVoucherTrnNo);
                    tVoucherDetails.SignCode = 1;//2;
                    tVoucherDetails.LedgerNo = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_Charges2));
                    tVoucherDetails.Debit = Convert.ToDouble(txtOtherTax.Text);
                    tVoucherDetails.Credit = 0;
                    tVoucherDetails.Narration = "";
                    tVoucherDetails.SrNo = Others.Charges2;
                    tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                    dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                }

                //For Round Off Acc Ledger
                if (Convert.ToDouble(txtRoundOff.Text) != 0)
                {
                    tVoucherDetails = new TVoucherDetails();
                    tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                    tVoucherDetails.VoucherSrNo = VoucherSrNo; VoucherSrNo += 1; ObjFunction.SetVouchers(dtVoucherDetails, tVoucherDetails.PkVoucherTrnNo);
                    tVoucherDetails.LedgerNo = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_RoundOfAcc));
                    if (Convert.ToDouble(txtRoundOff.Text) >= 0)
                    {
                        tVoucherDetails.SignCode = 1;
                        tVoucherDetails.Debit = Convert.ToDouble(txtRoundOff.Text);
                        tVoucherDetails.Credit = 0;
                    }
                    else
                    {
                        tVoucherDetails.SignCode = 2;//1;
                        tVoucherDetails.Debit = 0;
                        tVoucherDetails.Credit = Math.Abs(Convert.ToDouble(txtRoundOff.Text));
                    }
                    tVoucherDetails.Narration = "";
                    tVoucherDetails.SrNo = Others.RoundOff;
                    tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;
                    dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                }
                if (ID != 0)
                {
                    for (int i = 0; i < dtVoucherDetails.Rows.Count; i++)
                    {
                        if (dtVoucherDetails.Rows[i].ItemArray[2].ToString() == "0")
                            DeleteDtls(2, Convert.ToInt64(dtVoucherDetails.Rows[i].ItemArray[0].ToString()));
                    }
                    DeleteValues();
                }
                dbTVoucherEntry.EffectStock();
                long tempID = dbTVoucherEntry.ExecuteNonQueryStatements();
                if (tempID != 0)
                {

                    if (ParkBillNo != -1)
                    {
                        DBTVaucherEntry dbTVoucherEntry_P = new DBTVaucherEntry();
                        TParkingBill tParkingBill = new TParkingBill();
                        tParkingBill.ParkingBillNo = ParkBillNo;
                        tParkingBill.FKVoucherNo = tempID;
                        dbTVoucherEntry_P.DeleteTParkingBill(tParkingBill);
                        ParkBillNo = -1;

                    }

                    //if (Convert.ToDouble(dgPayType.Rows[1].Cells[2].Value) > 0 || Convert.ToDouble(dgPayType.Rows[3].Cells[2].Value) > 0 || Convert.ToDouble(dgPayType.Rows[4].Cells[2].Value) > 0)
                    if (Convert.ToDouble(dgPayType.Rows[2].Cells[2].Value) == 0)
                    {
                        if (tempDate.Date == dtpBillDate.Value.Date && tempPartyNo == ObjFunction.GetComboValue(cmbPartyName))
                            SavePayment(tempID);
                        else if (tempDate.Date != dtpBillDate.Value.Date || tempPartyNo != ObjFunction.GetComboValue(cmbPartyName))
                        {
                            SavePaymentNew(tempID);
                            SavePaymentOld(tempDate, tempPartyNo);
                        }
                    }

                    string strVChNo = ObjQry.ReturnLong("Select VoucherUserNo From TVoucherEntry Where PKVoucherNo=" + tempID + "", CommonFunctions.ConStr).ToString();
                    OMMessageBox.Show("Bill No " + strVChNo + " Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                    if (ID == 0)
                    {
                        //dtSearch = ObjFunction.GetDataView("Select PkVoucherNo from TVoucherEntry Where VoucherTypeCode=" + VoucherType + " AND CompanyNo=" + DBGetVal.CompanyNo + "").Table;
                        DataRow drSearch = dtSearch.NewRow();
                        drSearch[0] = tempID;
                        dtSearch.Rows.Add(drSearch);
                        ID = tempID;
                        if (cmbPaymentType.SelectedValue.ToString() == "3" && MixModeFlag == true)
                        {
                            if (partialPay.SaveData(ID) == true)
                            {
                                partialPay = new PartialPurchasePayment();
                                btnUpdate.Visible = false;
                            }
                            else
                                OMMessageBox.Show("MixMode Data Not Saved Successfuly", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                            //btnMixMode_Click(null, null);
                        }
                        PartyNo = 0; PayType = 0;
                        SetNavigation();
                        FillControls();
                    }
                    else
                    {
                        FillControls();
                    }
                    ObjFunction.FillCombo(cmbPartyNameSearch, " SELECT DISTINCT MLedger.LedgerNo, MLedger.LedgerName FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo  AND  " +
                            " TVoucherEntry.VoucherTypeCode = " + VchType.Purchase + " AND  TVoucherDetails.VoucherSrNo = 1 INNER JOIN MLedger ON MLedger.LedgerNo = TVoucherDetails.LedgerNo ORDER BY MLedger.LedgerName ");
                    setDisplay(true);
                    ObjFunction.LockButtons(true, this.Controls);
                    ObjFunction.LockControls(false, this.Controls);
                    dgBill.Enabled = false;
                    MixModeFlag = false;
                    POSelection.dtPOMain = new DataTable();
                    btnNew.Focus();
                    MixModeVal = 0;
                }
                else
                {
                    OMMessageBox.Show("Bill Not Added Successfully", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void setCompanyRatio()
        {
            //DataTable dtTemp = new DataTable();
            //bool TempFlag = false;
            //dtTemp.Columns.Add();
            //DataRow dr = dtTemp.NewRow();
            //dr[0] = Convert.ToInt64(dgBill.Rows[0].Cells[ColIndex.StockCompanyNo].Value);
            //dtTemp.Rows.Add(dr);
            //for (int k = 1; k < dgBill.Rows.Count - 1; k++)
            //{
            //    for (int i = 0; i < dtTemp.Rows.Count; i++)
            //    {
            //        if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.StockCompanyNo].Value) != Convert.ToInt64(dtTemp.Rows[i].ItemArray[0].ToString()))
            //        {
            //            TempFlag = true;
            //        }
            //        else
            //        {
            //            TempFlag = false;
            //            break;
            //        }
            //    }
            //    if (TempFlag == true)
            //    {
            //        dr = dtTemp.NewRow();
            //        dr[0] = Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.StockCompanyNo].Value);
            //        dtTemp.Rows.Add(dr);
            //    }
            //}

            //dtCompRatio = new DataTable();
            //dtCompRatio.Columns.Add();
            //dtCompRatio.Columns.Add();
            //double debit=0;

            //for (int k = 0; k < dtTemp.Rows.Count; k++)
            //{
            //    for (int j = 0; j < dgBill.Rows.Count - 1; j++)
            //    {
            //        if (Convert.ToInt64(dgBill.Rows[j].Cells[ColIndex.StockCompanyNo].Value) == Convert.ToInt64(dtTemp.Rows[k].ItemArray[0].ToString()))
            //        {
            //            debit = debit + Convert.ToDouble(dgBill.Rows[j].Cells[ColIndex.Amount].Value);
            //        }
            //    }

            //    DataRow dr1 = dtCompRatio.NewRow();
            //    dr1[0] = Convert.ToInt64(dtTemp.Rows[k].ItemArray[0].ToString());
            //    dr1[1] = (debit * 10) / (Convert.ToDouble(txtSubTotal.Text) + Convert.ToDouble(txtTotalTax.Text) - Convert.ToDouble(txtTotalDisc.Text));
            //    dtCompRatio.Rows.Add(dr1);
            //    debit = 0;
            //}
        }

        private bool ValidationsMain()
        {
            bool flag = false;
            try
            {
                ObjFunction.GetFinancialYear(dtpBillDate.Value, out dtFrom, out dtTo);

                if (txtRefNo.Text.Trim() == "")
                {
                    OMMessageBox.Show("Enter Invoice No.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    txtRefNo.Focus();
                }
                else if (ObjFunction.GetComboValue(cmbPartyName) <= 0)
                {
                    OMMessageBox.Show("Please Select Party Name", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    cmbPartyName.Focus();
                }
                else if (ObjFunction.GetComboValueString(cmbRateType) == "")
                {
                    OMMessageBox.Show("Please Select Rate Type", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    cmbRateType.Focus();
                }
                else if (ObjFunction.GetComboValue(cmbTaxType) <= 0)
                {
                    OMMessageBox.Show("Please Select Tax Type", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    cmbTaxType.Focus();
                }
                else if (ObjFunction.GetComboValue(cmbPaymentType) <= 0)
                {
                    OMMessageBox.Show("Please Select Payment Type", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    cmbTaxType.Focus();
                }
                else if (ObjQry.ReturnLong(" Select Count(*) FROM  TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo " +
                                            " where  TVoucherEntry.Reference ='" + txtRefNo.Text.Trim() + "' and TVoucherEntry.VoucherTypeCode=" + VchType.Purchase + " " +
                                            " and TVoucherDetails.LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " and TVoucherDetails.SrNo=501" +
                                            " AND TVoucherEntry.VoucherDate>='" + dtFrom.Date + "' AND TVoucherEntry.VoucherDate<='" + dtTo.Date + "' " +
                                            " and TVoucherEntry.PkVoucherNo!=" + ID + "", CommonFunctions.ConStr) > 0)//TVoucherEntry.VoucherDate='" + Convert.ToDateTime(dtpBillDate.Text).ToString("dd-MMM-yyyy") + "' and
                {
                    OMMessageBox.Show("This Invoice is already exist ....", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    cmbPartyName.Focus();
                }
                else flag = true;

                if (flag == true)
                {
                    for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                    {
                        DataGridViewRow dr = dgBill.Rows[i];
                        if (dr.Cells[ColIndex.PkBarCodeNo].Value == null && dr.Cells[ColIndex.ItemNo].Value == null && dr.Cells[ColIndex.TaxLedgerNo].Value == null && dr.Cells[ColIndex.SalesLedgerNo].Value == null && dr.Cells[ColIndex.PkRateSettingNo].Value == null && dr.Cells[ColIndex.PkItemTaxInfo].Value == null)
                        {
                            flag = false;
                            OMMessageBox.Show("Please Fill properly Sr No. " + (i + 1) + " of item..", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            break;
                        }
                        else if (dgBill.Rows[i].Cells[ColIndex.PkBarCodeNo].Value.ToString() == "0")
                        {
                            flag = false;
                            OMMessageBox.Show("Please Fill properly Sr No. " + (i + 1) + " of item..", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            break;
                        }
                    }
                }

                return flag;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return false;
            }
        }

        private bool Validations()
        {
            bool flag = true;
            if (txtDiscRupees1.Text == "")
            {
                txtDiscRupees1.Text = "0.00";
                flag = true;
            }
            else if (ObjFunction.CheckValidAmount(txtDiscRupees1.Text) == false)
            {
                txtDiscRupees1.Text = "0.00";
                flag = true;
            }
            if (txtFreight.Text == "")
            {
                txtFreight.Text = "0.00";
                flag = true;
            }
            else if (ObjFunction.CheckValidAmount(txtFreight.Text) == false)
            {
                txtFreight.Text = "0.00";
                flag = true;
            }
            if (txtChrgRupees1.Text == "")
            {
                txtChrgRupees1.Text = "0.00";
                flag = true;
            }
            if (txtOtherTax.Text == "")
            {
                txtOtherTax.Text = "0.00";
                flag = true;
            }
            if (txtReturnAmt.Text == "")
            {
                txtReturnAmt.Text = "0.00";
                flag = true;
            }
            if (txtVisibility.Text == "")
            {
                txtVisibility.Text = "0.00";
                flag = true;
            }
            else if (ObjFunction.CheckValidAmount(txtChrgRupees1.Text) == false)
            {
                txtChrgRupees1.Text = "0.00";
                flag = true;
            }
            else if (ObjFunction.CheckValidAmount(txtOtherTax.Text) == false)
            {
                txtOtherTax.Text = "0.00";
                flag = true;
            }
            if (txtRemark.Text.Trim() == "")
            {
                txtRemark.Text = "Purchase Bill";
            }

            return flag;
        }

        private void Control_Leave(object sender, EventArgs e)
        {
            try
            {
                double TotalAmt = 0;
                TotalAmt = ((Convert.ToDouble(txtSubTotal.Text) + Convert.ToDouble(txtTotalTax.Text)) - (Convert.ToDouble(txtTotalDisc.Text) + Convert.ToDouble(txtTotalItemDisc.Text)));
                //if (((TextBox)sender).Name == "txtDiscount1")
                //{
                //    if (ObjFunction.CheckValidAmount(((TextBox)sender).Text) == false)
                //    {
                //        OMMessageBox.Show("Enter Discount Value.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                //        ((TextBox)sender).Focus();
                //    }
                //    else
                //    {
                //        txtChrgRupees1.Focus();
                //        CalculateTotal();
                //    }
                //}
                if (((TextBox)sender).Name == "txtDiscRupees1")
                {
                    if (ObjFunction.CheckValidAmount(((TextBox)sender).Text) == false)
                    {
                        OMMessageBox.Show("Enter Discount Value.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        ((TextBox)sender).Focus();
                    }
                    else
                    {
                        //txtDiscount1.Text = Convert.ToDouble((100 * Convert.ToDouble(txtDiscRupees1.Text)) / TotalAmt).ToString("0.00");
                        //txtChrgRupees1.Focus();
                        CalculateTotal();
                    }
                }
                if (((TextBox)sender).Name == "txtFreight")
                {
                    if (ObjFunction.CheckValidAmount(((TextBox)sender).Text) == false)
                    {
                        OMMessageBox.Show("Enter Freight Value.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        ((TextBox)sender).Focus();
                    }
                    else
                    {
                        //txtDiscount1.Text = Convert.ToDouble((100 * Convert.ToDouble(txtDiscRupees1.Text)) / TotalAmt).ToString("0.00");
                        //txtChrgRupees1.Focus();
                        CalculateTotal();
                    }
                }

                else if (((TextBox)sender).Name == "txtChrgRupees1")
                {
                    if (ObjFunction.CheckValidAmount(((TextBox)sender).Text) == false)
                    {
                        OMMessageBox.Show("Enter Charges Value.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        ((TextBox)sender).Focus();
                    }
                    else
                    {
                        //txtReturnAmt.Focus();
                        CalculateTotal();
                    }
                }
                else if (((TextBox)sender).Name == "txtReturnAmt")
                {
                    if (ObjFunction.CheckValidAmount(((TextBox)sender).Text) == false)
                    {
                        OMMessageBox.Show("Enter Valid Return Amount.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        ((TextBox)sender).Focus();
                    }
                    else
                    {
                        //txtVisibility.Focus();
                        //CalculateTotal();
                    }
                }
                else if (((TextBox)sender).Name == "txtVisibility")
                {
                    if (ObjFunction.CheckValidAmount(((TextBox)sender).Text) == false)
                    {
                        OMMessageBox.Show("Enter Valid Visility Amount.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        ((TextBox)sender).Focus();
                    }
                    else
                    {
                        //txtRemark.Focus();
                        //CalculateTotal();
                    }
                }
                else if (((TextBox)sender).Name == "txtOtherTax")
                {
                    if (ObjFunction.CheckValidAmount(((TextBox)sender).Text) == false)
                    {
                        OMMessageBox.Show("Enter Other Tax Value.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        ((TextBox)sender).Focus();
                    }
                    else
                    {
                        CalculateTotal();
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }


        }

        #region dgBill Methods and Events
        private delegate void MovetoNext(int RowIndex, int ColIndex, DataGridView dg);

        private void m2n(int RowIndex, int ColIndex, DataGridView dg)
        {
            dg.CurrentCell = dg.Rows[RowIndex].Cells[ColIndex];
        }

        private void Desc_Start()
        {
            try
            {
                if (dgBill.CurrentCell.Value == null || Convert.ToString(dgBill.CurrentCell.Value) == "")
                {
                    ItemType = 1;
                    FillItemList(0, ItemType); //FillItemList();
                }
                else
                {
                    ItemType = 2;
                    long[] BarcodeNo = null; long[] ItemNo = null;

                    switch (dgBill.CurrentCell.Value.ToString().Trim())
                    {
                        case "SV":
                            {
                                if (btnSave.Visible)
                                {
                                    btnSave_Click(btnSave, null);
                                    return;
                                }
                                break;
                            }
                        default:
                            {
                                SearchBarcode(dgBill.CurrentCell.Value.ToString().Trim(), out ItemNo, out BarcodeNo);
                                dgBill.CurrentRow.Cells[ColIndex.PkRateSettingNo].Value = 0;
                                break;
                            }
                    }

                    if (ItemNo.Length == 0 || BarcodeNo.Length == 0)
                    {
                        string strB = dgBill.CurrentCell.FormattedValue.ToString();
                        dgBill.CurrentCell.Value = null;
                        dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkBarCodeNo].Value = "0";
                        //DisplayMessage("Barcode Not Found");
                        if (OMMessageBox.Show("Barcode Not Found.\nPRESS ESCAPE TO CONTINUE...." + Environment.NewLine + "Press Ctrl+N New Item..", "Information", OMMessageBoxButton.EscapeButton, OMMessageBoxIcon.Information) == DialogResult.No)
                        {
                            MovetoNext move2n = new MovetoNext(m2n);
                            BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex, ColIndex.ItemName, dgBill });
                        }
                        else
                            NewItemAdd(strB);
                    }
                    else
                    {
                        if (ItemNo.Length > 1)
                        {
                            ItemType = 3;
                            FillItemList(0, ItemType);//FillItemList();
                        }
                        else
                        {
                            dgBill.CurrentRow.Cells[ColIndex.Barcode].Value = dgBill.CurrentCell.Value;
                            Desc_MoveNext(ItemNo[0], BarcodeNo[0]);
                        }
                    }
                    //BindGrid();
                    //CalculateTotal();
                }

                //from key_down
                //ItemType = 1;
                //FillItemList();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void SearchBarcode(String strBarcode, out long[] ItemNo, out long[] BarcodeNo)
        {
            string sql = "";
            DataTable dt = new DataTable();
            sql = "SELECT     MStockBarcode.PkStockBarcodeNo, MStockBarcode.ItemNo,MStockBarcode.Barcode FROM MStockBarcode INNER JOIN MStockItems ON MStockBarcode.ItemNo = MStockItems.ItemNo " +
                    " INNER JOIN MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo " +
                    " where (MStockBarcode.Barcode = '" + strBarcode + "' or MStockItems.ShortCode = '" + strBarcode + "') And MStockItems.IsActive ='true' AND MStockItems.CompanyNo=" + DBGetVal.CompanyNo + " AND (MRateSetting.IsActive='true') and (MStockItems.FkStockGroupTypeNo<>3)";

            dt = ObjFunction.GetDataView(sql).Table;
            BarcodeNo = new long[dt.Rows.Count];
            ItemNo = new long[dt.Rows.Count];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    BarcodeNo[i] = Convert.ToInt64(dt.Rows[i].ItemArray[0].ToString());
                    ItemNo[i] = Convert.ToInt64(dt.Rows[i].ItemArray[1].ToString());
                    dgBill.CurrentCell.Value = dt.Rows[i].ItemArray[2].ToString();
                }
            }
            else
            {
                //sql = "SELECT     MStockBarcode.PkStockBarcodeNo, MStockBarcode.ItemNo,MStockBarcode.Barcode FROM MStockBarcode INNER JOIN MStockItems ON MStockBarcode.ItemNo = MStockItems.ItemNo " +
                //                    " where MStockItems.ShortCode = '" + strBarcode + "' And MStockItems.IsActive ='true' AND MStockItems.CompanyNo=" + DBGetVal.CompanyNo + "";
                //dt = ObjFunction.GetDataView(sql).Table;
                //BarcodeNo = new long[dt.Rows.Count];
                //ItemNo = new long[dt.Rows.Count];
                //if (dt.Rows.Count > 0)
                //{
                //    for (int i = 0; i < dt.Rows.Count; i++)
                //    {
                //        BarcodeNo[i] = Convert.ToInt64(dt.Rows[i].ItemArray[0].ToString());
                //        ItemNo[i] = Convert.ToInt64(dt.Rows[i].ItemArray[1].ToString());
                //        dgBill.CurrentCell.Value = dt.Rows[i].ItemArray[2].ToString();
                //    }
                //}
                //ItemNo[0] = 0;
                //BarcodeNo[0] = 0;
            }
        }

        private void Desc_MoveNext(long ItemNo, long BarcodeNo)
        {
            try
            {
                dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemNo].Value = ItemNo;
                dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkBarCodeNo].Value = BarcodeNo;

                DataTable dtItem = ObjFunction.GetDataView("Select ShowItemName AS ItemName,CompanyNo,GodownNo,DiscountType,HamaliInKg from MStockItems where ItemNo = " + ItemNo + " AND IsActive='true'").Table;
                dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemName].Value = dtItem.Rows[0].ItemArray[0].ToString();
                dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.StockCompanyNo].Value = dtItem.Rows[0].ItemArray[1].ToString();
                dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.GodownNo].Value = (ObjFunction.GetAppSettings(AppSettings.S_DefaultStockLocation).ToString() == "0") ? dtItem.Rows[0]["GodownNo"].ToString() : ObjFunction.GetAppSettings(AppSettings.S_DefaultStockLocation).ToString();
                dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.DiscountType].Value = dtItem.Rows[0].ItemArray[3].ToString();

                if (ItemType == 2)
                    dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemName].Value += " - " + dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.Barcode].Value.ToString();

                if (StopOnQty == true)
                {
                    if (dgBill[2, dgBill.CurrentCell.RowIndex].Value == null)
                    {
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex, ColIndex.Quantity, dgBill });
                        //   dgBill.CurrentCell = dgBill[2, dgBill.CurrentCell.RowIndex];
                        dgBill.Focus();
                    }
                    else
                        Qty_MoveNext();
                }
                else
                {
                    dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.Quantity].Value = Convert.ToDouble(1).ToString("0.00");
                    Qty_MoveNext();
                }

                //BindGrid();
                //CalculateTotal();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void Qty_MoveNext()
        {
            try
            {
                rowQtyIndex = dgBill.CurrentCell.RowIndex;

                MovetoNext move2n = new MovetoNext(m2n);
                BeginInvoke(move2n, new object[] { rowQtyIndex, ColIndex.UOM, dgBill });

                UOM_Start();

                //CalculateTotal();//temp

                // dgBill.Rows[rowQtyIndex].Cells[2].ReadOnly = true;

                //found in the dgBill_keydown
                //if (dgBill.CurrentCell.ColumnIndex == 2)
                //{
                //    if (dgBill.Rows.Count > 1)
                //    {
                //        row = (dgBill.CurrentCell.RowIndex == 0) ? 0 : dgBill.CurrentCell.RowIndex;
                //        if (Convert.ToString(dgBill.Rows[row].Cells[2].Value) != "")
                //        {
                //            dgBill.CurrentCell = dgBill[3, row];
                //            dgBill.CurrentCell.ReadOnly = false;
                //        }
                //    }
                //}
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

        }

        private void UOM_Start()
        {
            try
            {
                int row = 0;
                if (dgBill.CurrentCell.RowIndex == 0)
                    row = dgBill.CurrentCell.RowIndex;
                else
                    row = dgBill.CurrentCell.RowIndex;
                dgBill.CurrentCell = dgBill[3, row];

                //dgBill.CurrentCell.ReadOnly = false;
                FillUOMList(row);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void UOM_MoveNext()
        {
            try
            {
                int Row = dgBill.CurrentCell.RowIndex;

                if (dgBill.CurrentRow.Cells[ColIndex.UOMNo].Value != null &&
                    dgBill.CurrentRow.Cells[ColIndex.UOMNo].Value.ToString() != lstUOM.SelectedValue.ToString())
                {
                    dgBill.CurrentRow.Cells[ColIndex.Rate].Value = "0.00";//lstRate.Text;
                    dgBill.CurrentRow.Cells[ColIndex.PkRateSettingNo].Value = 0;//lstRate.SelectedValue;
                }

                dgBill.CurrentRow.Cells[ColIndex.UOM].Value = lstUOM.Text;
                dgBill.CurrentRow.Cells[ColIndex.UOMNo].Value = Convert.ToInt64(lstUOM.SelectedValue);
                pnlUOM.Visible = false;

                Rate_Start();
                //CalculateTotal();//temp
                //CalculateGridValues(Row);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void Rate_Start()
        {
            try
            {
                string str;
                //CalculateGridValues();
                int RowIndex = dgBill.CurrentCell.RowIndex;
                long ItemNo = Convert.ToInt64(dgBill.Rows[RowIndex].Cells[ColIndex.ItemNo].Value);
                long BarcodeNo = Convert.ToInt64(dgBill.Rows[RowIndex].Cells[ColIndex.PkBarCodeNo].Value);
                long UOMNo = Convert.ToInt64(dgBill.Rows[RowIndex].Cells[ColIndex.UOMNo].Value);
                double Qty = Convert.ToDouble(dgBill.Rows[RowIndex].Cells[ColIndex.Quantity].Value);
                dgBill.Rows[RowIndex].Cells[ColIndex.BarcodePrint].Value = "Print";
                if (dgBill.Rows[RowIndex].Cells[ColIndex.PkRateSettingNo].Value == null ||
                    Convert.ToInt64(dgBill.Rows[RowIndex].Cells[ColIndex.PkRateSettingNo].Value) == 0)
                {
                    ObjFunction.FillList(lstRate, "pksrno", ObjFunction.GetComboValueString(cmbRateType));
                    if (ItemType == 2)
                    {
                        str = "select pksrno," + ObjFunction.GetComboValueString(cmbRateType) +
                            " from GetItemRateAll(" + ItemNo + "," + BarcodeNo + "," + UOMNo + ",null ,null,null)";
                    }
                    else
                    {
                        str = "select pksrno," + ObjFunction.GetComboValueString(cmbRateType) +
                            " from GetItemRateAll(" + ItemNo + ",null," + UOMNo + ",null ,null,null)";
                    }
                    str = str.Replace("PurRate", "Isnull((Select Rate FRom Tstock Where PKStockTrnNo in (Select Max(PKStockTrnNo) FRom Tstock,TVoucherEntry Where TVoucherEntry.PKVoucherNo=TStock.FKVoucherNo AND TVoucherEntry.VoucherTypeCode=" + VchType.Purchase + " AND FKRateSettingNo in(PkSrNo))),0) AS PurRate");
                    ObjFunction.FillList(lstRate, str);

                    if (lstRate.Items.Count == 1)
                    {
                        lstRate.SelectedIndex = 0;
                        dgBill.Rows[RowIndex].Cells[ColIndex.Rate].Value = lstRate.Text;
                        dgBill.Rows[RowIndex].Cells[ColIndex.PkRateSettingNo].Value = lstRate.SelectedValue;

                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { RowIndex, ColIndex.Rate, dgBill });
                        // dgBill.CurrentCell = dgBill[ColIndex.Rate, RowIndex];
                        dgBill.Focus();
                        //BindGrid(dgBill.CurrentRow.Index);

                        if (StopOnRate == false)
                        {
                            dgBill.CurrentCell = dgBill[ColIndex.Rate, RowIndex];
                            Rate_MoveNext();
                        }
                        else
                        {
                            BindGrid(dgBill.CurrentRow.Index);
                        }
                    }
                    else if (lstRate.Items.Count > 1)
                    {
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { RowIndex, ColIndex.Rate, dgBill });
                        dgBill.CurrentCell = dgBill[ColIndex.Rate, RowIndex];
                        dgBill.Focus();

                        CalculateTotal();
                        pnlRate.Visible = true;
                        lstRate.Focus();
                    }
                    else
                    {
                        //error invalid Qty or UOM
                    }
                }
                else
                {
                    MovetoNext move2n = new MovetoNext(m2n);
                    BeginInvoke(move2n, new object[] { RowIndex, ColIndex.Rate, dgBill });
                    // dgBill.CurrentCell = dgBill[ColIndex.Rate, RowIndex];
                    dgBill.Focus();
                    //BindGrid(dgBill.CurrentRow.Index);

                    if (StopOnRate == false)
                    {
                        dgBill.CurrentCell = dgBill[ColIndex.Rate, RowIndex];
                        Rate_MoveNext();
                    }
                    else
                    {
                        BindGrid(dgBill.CurrentRow.Index);
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FreeQty_MoveNext()
        {
            try
            {
                if (dgBill.CurrentCell.Value != null && dgBill.CurrentCell.Value.ToString() != "")
                {
                    if (ObjFunction.CheckValidAmount(dgBill.CurrentCell.Value.ToString()) == true)
                    {

                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex, ColIndex.DiscPercentage, dgBill });
                        //dgBill.CurrentCell = dgBill[1, dgBill.Rows.Count - 1];
                        dgBill.Focus();

                        CalculateTotal();
                    }
                    else
                    {
                        dgBill.CurrentCell.ErrorText = "Please Enter Valid Free Quantity...";
                    }
                }
                else
                {
                    dgBill.CurrentCell.Value = "0";
                }
            }

            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void Rate_MoveNext()
        {
            try
            {
                if (dgBill.CurrentCell.Value != null)
                {
                    if (ObjFunction.CheckValidAmount(dgBill.CurrentCell.Value.ToString()) == true)
                    {
                        //dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.Amount].Value = (Convert.ToDouble(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.Rate].Value) * Convert.ToDouble(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[2].Value)) / Convert.ToDouble(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.MKTQuantity].Value);
                        //dgBill.CurrentCell.ReadOnly = true;
                        BindGrid(dgBill.CurrentCell.RowIndex);

                        if (dgBill.Columns[ColIndex.FreeQty].Visible == true)
                        {
                            MovetoNext move2n = new MovetoNext(m2n);
                            BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex, ColIndex.FreeQty, dgBill });
                            dgBill.CurrentCell = dgBill[ColIndex.FreeQty, dgBill.CurrentCell.RowIndex];
                        }
                        else
                        {
                            MovetoNext move2n = new MovetoNext(m2n);
                            BeginInvoke(move2n, new object[] { dgBill.Rows.Count - 1, ColIndex.ItemName, dgBill });
                            dgBill.CurrentCell = dgBill[1, dgBill.Rows.Count - 1];
                        }
                        dgBill.Focus();

                        //CalculateTotal();
                    }
                    else
                    {
                        dgBill.CurrentCell.ErrorText = "Please Enter valid rate...";
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void Disc_MoveNext()
        {
            //Rate_MoveNext();
            CalculateTotal();
        }

        #region Update CurrentStock code
        public void intialCurrentStock()
        {
            dtCurrentStock = ObjFunction.GetDataView("Select ItemNo,MRP,GodownNo,CurrentStock,0 as Type From MStockItemBalance Where Itemno=0").Table;

        }
        private void DeleteCurrentStock(long ItemNo, double MRP, long GodownNo, double CurrentStock, long type)
        {
            DataRow dr = null;
            dr = dtCurrentStock.NewRow();
            dr[0] = ItemNo;
            dr[1] = MRP;
            dr[2] = GodownNo;
            dr[3] = CurrentStock;
            dr[4] = type;
            dtCurrentStock.Rows.Add(dr);
        }
        private void UpdateCurrentStock()
        {
            if (dtCurrentStock != null)
            {
                for (int i = 0; i < dtCurrentStock.Rows.Count; i++)
                {
                    dbTVoucherEntry.UpdateReverStock(Convert.ToInt64(dtCurrentStock.Rows[i].ItemArray[0].ToString()),
                        Convert.ToDouble(dtCurrentStock.Rows[i].ItemArray[1].ToString()), Convert.ToInt64(dtCurrentStock.Rows[i].ItemArray[2].ToString()),
                        Convert.ToDouble(dtCurrentStock.Rows[i].ItemArray[3].ToString()), Convert.ToInt64(dtCurrentStock.Rows[i].ItemArray[4].ToString()));
                }
            }
            dtCurrentStock.Rows.Clear();
        }
        #endregion


        private void delete_row()
        {
            try
            {
                bool flag;
                if (dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkStockTrnNo].Value != null)
                {
                    if (OMMessageBox.Show("Are you sure want to delete this item ?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question, OMMessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        long PKStockTrnNo = Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkStockTrnNo].Value);
                        if (PKStockTrnNo != 0)
                        {
                            DeleteDtls(1, PKStockTrnNo);


                            DeleteCurrentStock(Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemNo].Value),
                                 Convert.ToDouble(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.MRP].Value),
                                Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.GodownNo].Value),
                                Convert.ToDouble(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.Quantity].Value), 2);

                            DeleteDtls(5, Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkStockDetailsNo].Value));

                            //For Party LedgerNo
                            flag = false;
                            for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                            {
                                if (dgBill.CurrentCell.RowIndex != i)
                                {
                                    if (Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkVoucherNo].Value) == Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.PkVoucherNo].Value))
                                    { flag = true; break; }
                                }
                            }
                            if (flag == false)
                                DeleteDtls(2, Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkVoucherNo].Value));

                            //For Purchase LedgerNo
                            flag = false;
                            for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                            {
                                if (dgBill.CurrentCell.RowIndex != i)
                                {
                                    if (Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.SalesVchNo].Value) == Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.SalesVchNo].Value))
                                    { flag = true; break; }
                                }
                            }
                            if (flag == false)
                                DeleteDtls(2, Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.SalesVchNo].Value));

                            //FOr TaxLedgerNo
                            flag = false;
                            for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                            {
                                if (dgBill.CurrentCell.RowIndex != i)
                                {
                                    if (Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.TaxVchNo].Value) == Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.TaxVchNo].Value))
                                    { flag = true; break; }
                                }
                            }
                            if (flag == false)
                                DeleteDtls(2, Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.TaxVchNo].Value));

                        }

                        if (dgBill.Rows.Count - 1 == dgBill.CurrentCell.RowIndex)
                        {
                            dgBill.Rows.RemoveAt(dgBill.CurrentCell.RowIndex);
                            dgBill.Rows.Add();
                        }
                        else
                            dgBill.Rows.RemoveAt(dgBill.CurrentCell.RowIndex);

                        CalculateTotal();

                        dgBill.CurrentCell = dgBill[2, dgBill.Rows.Count - 1];
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgBill_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (Spaceflag == false) { Spaceflag = true; return; }
                if (dgBill.CurrentCell.ColumnIndex == ColIndex.ItemName)
                {
                    isDisc1PercentChanged = true;
                    isDisc2PercentChanged = true;
                    dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemNo].Value = 0;
                    dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkRateSettingNo].Value = 0;

                    Desc_Start();
                    if (dgBill.CurrentCell.Value == null)
                    {
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { e.RowIndex, ColIndex.ItemName, dgBill });
                    }
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.Quantity)
                {
                    isDisc1PercentChanged = true;
                    isDisc2PercentChanged = true;
                    Qty_MoveNext();
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.FreeQty)
                {
                    isDisc1PercentChanged = true;
                    isDisc2PercentChanged = true;
                    FreeQty_MoveNext();
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.UOM)
                {
                    isDisc1PercentChanged = true;
                    isDisc2PercentChanged = true;
                    UOM_Start();
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.Rate)
                {
                    isDisc1PercentChanged = true;
                    isDisc2PercentChanged = true;
                    Rate_MoveNext();
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscPercentage)
                {
                    isDisc1PercentChanged = true;
                    isDisc2PercentChanged = true;
                    Disc_MoveNext();
                    if (dgBill.Columns[ColIndex.DiscAmount].Visible == true)
                    {
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { e.RowIndex, ColIndex.DiscAmount, dgBill });
                    }
                    else if (dgBill.Columns[ColIndex.DiscPercentage2].Visible == true)
                    {
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { e.RowIndex, ColIndex.DiscPercentage2, dgBill });
                    }
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscAmount)
                {
                    isDisc2PercentChanged = true;
                    //double amt = Convert.ToDouble((Convert.ToDouble(dgBill.CurrentCell.Value) * 100) / (Convert.ToDouble(dgBill.Rows[e.RowIndex].Cells[ColIndex.Quantity].Value) * Convert.ToDouble(dgBill.Rows[e.RowIndex].Cells[ColIndex.Rate].Value)));
                    //dgBill.Rows[e.RowIndex].Cells[ColIndex.DiscPercentage].Value = amt;
                    ////dgBill.Rows[e.RowIndex].Cells[ColIndex.Amount].Value = Convert.ToDouble(dgBill.Rows[e.RowIndex].Cells[ColIndex.Amount].Value) - Convert.ToDouble(dgBill.Rows[e.RowIndex].Cells[ColIndex.DiscAmount].Value);
                    CalculateTotal();
                    MovetoNext move2n = new MovetoNext(m2n);
                    if (dgBill.Columns[ColIndex.DiscPercentage2].Visible == true)
                        BeginInvoke(move2n, new object[] { e.RowIndex, ColIndex.DiscPercentage2, dgBill });
                    else
                        BeginInvoke(move2n, new object[] { dgBill.Rows.Count - 1, ColIndex.ItemName, dgBill });
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscRupees)
                {
                    Disc_MoveNext();
                    if (dgBill.Columns[ColIndex.DiscPercentage2].Visible == true)
                    {
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { e.RowIndex, ColIndex.DiscPercentage2, dgBill });
                    }
                    else
                    {
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { e.RowIndex + 1, ColIndex.ItemName, dgBill });
                    }
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscPercentage2)
                {
                    isDisc1PercentChanged = true;
                    isDisc2PercentChanged = true;
                    Disc_MoveNext();
                    if (dgBill.Columns[ColIndex.DiscAmount2].Visible == true)
                    {
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { e.RowIndex, ColIndex.DiscAmount2, dgBill });
                    }
                    else
                    {

                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { e.RowIndex + 1, ColIndex.ItemName, dgBill });
                    }
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscRupees2)
                {
                    CalculateTotal();
                    MovetoNext move2n = new MovetoNext(m2n);
                    BeginInvoke(move2n, new object[] { e.RowIndex + 1, ColIndex.ItemName, dgBill });
                    //Disc_MoveNext();
                    //MovetoNext move2n = new MovetoNext(m2n);
                    //BeginInvoke(move2n, new object[] { e.RowIndex, ColIndex.Amount, dgBill });
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscAmount2)
                {
                    //isDisc1PercentChanged = true;
                    //double amt = Convert.ToDouble((Convert.ToDouble(dgBill.CurrentCell.Value) * 100) / Convert.ToDouble(dgBill.Rows[e.RowIndex].Cells[ColIndex.Amount].Value));
                    //dgBill.Rows[e.RowIndex].Cells[ColIndex.DiscPercentage2].Value = amt;
                    //dgBill.Rows[e.RowIndex].Cells[ColIndex.Amount].Value = Convert.ToDouble(dgBill.Rows[e.RowIndex].Cells[ColIndex.Amount].Value) - Convert.ToDouble(dgBill.Rows[e.RowIndex].Cells[ColIndex.DiscAmount2].Value);
                    CalculateTotal();
                    MovetoNext move2n = new MovetoNext(m2n);
                    if (dgBill.Columns[ColIndex.DiscRupees2].Visible == true)
                        BeginInvoke(move2n, new object[] { e.RowIndex, ColIndex.DiscRupees2, dgBill });
                    else
                        BeginInvoke(move2n, new object[] { e.RowIndex + 1, ColIndex.ItemName, dgBill });
                }
                else if (dgBill.CurrentCell.ColumnIndex == ColIndex.MRP)
                {
                    if (dgBill.CurrentCell.Value != null && dgBill.CurrentCell.Value.ToString() != "")
                    {
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { e.RowIndex, ColIndex.FreeQty, dgBill });

                        if (ObjFunction.CheckValidAmount(dgBill.CurrentCell.Value.ToString()) == true)
                        {

                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgBill_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    delete_row();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    dgBill.Focus();
                    if (dgBill.CurrentCell.ColumnIndex == ColIndex.SrNo)
                    {
                        e.SuppressKeyPress = true;
                        dgBill.CurrentCell = dgBill[ColIndex.ItemName, dgBill.CurrentCell.RowIndex];
                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.ItemName)
                    {
                        e.SuppressKeyPress = true;
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_AddvanceSearch)) == false)
                        {
                            dgBill.CurrentCell.Value = "";
                            Desc_Start();
                        }
                        else
                        {
                            Master.ItemAdvanceSearch itemSearch = new Master.ItemAdvanceSearch(ds_ItemSearch);
                            ObjFunction.OpenForm(itemSearch);
                            if (itemSearch.ItemNo != 0)
                            {
                                dgBill.CurrentCell = dgBill[ColIndex.ItemName, dgBill.Rows.Count - 1];
                                dgBill.Rows[dgBill.Rows.Count - 1].Cells[ColIndex.ItemName].Value = itemSearch.BarCode;
                                dgBill_CellEndEdit(dgBill, new DataGridViewCellEventArgs(ColIndex.ItemName, dgBill.Rows.Count - 1));
                                itemSearch.Close();
                            }
                        }
                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.Quantity)
                    {
                        e.SuppressKeyPress = true;
                        if (dgBill.CurrentCell.Value == null) dgBill.CurrentCell.Value = "1";
                        Qty_MoveNext();
                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.FreeQty)
                    {
                        e.SuppressKeyPress = true;
                        if (dgBill.CurrentCell.Value == null && dgBill.CurrentCell.Value.ToString() == "") dgBill.CurrentCell.Value = "1";
                        FreeQty_MoveNext();
                    }
                    if (dgBill.CurrentCell.ColumnIndex == ColIndex.MRP)
                    {
                        e.SuppressKeyPress = true;
                        dgBill.CurrentCell = dgBill[ColIndex.FreeQty, dgBill.CurrentCell.RowIndex];
                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscPercentage)
                    {
                        e.SuppressKeyPress = true;
                        if (dgBill.CurrentCell.Value == null) dgBill.CurrentCell.Value = "0";
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex, ColIndex.DiscAmount, dgBill });

                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscPercentage2)
                    {
                        e.SuppressKeyPress = true;
                        if (dgBill.CurrentCell.Value == null) dgBill.CurrentCell.Value = "0";
                        if (dgBill.CurrentCell.RowIndex < dgBill.Rows.Count)
                        {
                            if (dgBill.Rows.Count - 1 == dgBill.CurrentCell.RowIndex)
                            {
                                MovetoNext move2n = new MovetoNext(m2n);
                                BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex, ColIndex.Rate, dgBill });
                            }
                            else
                            {
                                MovetoNext move2n = new MovetoNext(m2n);
                                BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex + 1, ColIndex.ItemName, dgBill });
                            }
                        }
                        else
                        {
                            MovetoNext move2n = new MovetoNext(m2n);
                            BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex, ColIndex.ItemName, dgBill });
                        }

                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.UOM)
                    {
                        e.SuppressKeyPress = true;
                        if (dgBill.CurrentRow.Cells[ColIndex.ItemNo].Value != null && dgBill.CurrentRow.Cells[ColIndex.ItemNo].Value.ToString() != "")
                        {
                            UOM_Start();
                        }
                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.Rate)
                    {
                        e.SuppressKeyPress = true;
                        Rate_MoveNext();
                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscAmount)
                    {
                        e.SuppressKeyPress = true;
                        if (dgBill.CurrentCell.Value == null) dgBill.CurrentCell.Value = "0";
                        MovetoNext move2n = new MovetoNext(m2n);
                        if (dgBill.Rows.Count - 1 != dgBill.CurrentCell.RowIndex)
                        {
                            if (dgBill.Columns[ColIndex.DiscPercentage2].Visible == true)
                                BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex, ColIndex.DiscPercentage2, dgBill });
                            else
                            {
                                if (dgBill.CurrentCell.RowIndex < dgBill.Rows.Count)
                                {
                                    if (dgBill.Rows.Count - 1 != dgBill.CurrentCell.RowIndex)
                                        BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex + 1, ColIndex.ItemName, dgBill });
                                }
                                else
                                {
                                    if (dgBill.Rows.Count - 1 != dgBill.CurrentCell.RowIndex)
                                        BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex, ColIndex.Amount, dgBill });
                                }
                            }
                        }

                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscAmount2)
                    {
                        e.SuppressKeyPress = true;
                        if (dgBill.CurrentCell.Value == null) dgBill.CurrentCell.Value = "0";
                        if (dgBill.Columns[ColIndex.DiscRupees2].Visible == true)
                        {
                            MovetoNext move2n = new MovetoNext(m2n);
                            BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex, ColIndex.DiscRupees2, dgBill });
                        }
                        else
                        {
                            if (dgBill.CurrentCell.RowIndex < dgBill.Rows.Count)
                            {
                                MovetoNext move2n = new MovetoNext(m2n);
                                if (dgBill.Rows.Count - 1 != dgBill.CurrentCell.RowIndex)
                                    BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex + 1, ColIndex.ItemName, dgBill });
                            }
                            else
                            {
                                MovetoNext move2n = new MovetoNext(m2n);
                                if (dgBill.Rows.Count - 1 != dgBill.CurrentCell.RowIndex)
                                    BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex, ColIndex.Amount, dgBill });
                            }
                        }

                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscRupees2)
                    {
                        e.SuppressKeyPress = true;
                        if (dgBill.CurrentCell.Value == null) dgBill.CurrentCell.Value = "0";
                        if (dgBill.CurrentCell.RowIndex < dgBill.Rows.Count)
                        {
                            MovetoNext move2n = new MovetoNext(m2n);
                            if (dgBill.Rows.Count - 1 != dgBill.CurrentCell.RowIndex)
                                BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex + 1, ColIndex.ItemName, dgBill });
                        }
                        else
                        {
                            MovetoNext move2n = new MovetoNext(m2n);
                            if (dgBill.Rows.Count - 1 != dgBill.CurrentCell.RowIndex)
                                BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex, ColIndex.Amount, dgBill });
                        }

                    }
                    else if (dgBill.CurrentCell.ColumnIndex == ColIndex.TaxPercentage || dgBill.CurrentCell.ColumnIndex == ColIndex.Amount)
                    {
                        if (dgBill.Rows.Count - 1 == dgBill.CurrentCell.RowIndex)
                        {
                            MovetoNext move2n = new MovetoNext(m2n);
                            BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex, ColIndex.Rate, dgBill });
                        }
                        else
                        {
                            MovetoNext move2n = new MovetoNext(m2n);
                            BeginInvoke(move2n, new object[] { dgBill.CurrentCell.RowIndex + 1, ColIndex.ItemName, dgBill });
                        }
                    }

                }
                else if (e.KeyCode == Keys.Escape)
                {
                    //if (txtDistDisc.Visible == true) txtDiscount1.Focus();
                    //else if (txtDiscRupees1.Visible == true) 
                    txtDiscRupees1.Focus();
                }
                else if (e.KeyCode == Keys.F8)
                {
                    if (dgBill.CurrentCell.Value != null)
                        dgBill.CurrentCell = dgBill[ColIndex.Rate, dgBill.CurrentCell.RowIndex];
                    else
                    {
                        if (dgBill.CurrentCell.RowIndex == 0)
                            dgBill.CurrentCell = dgBill[ColIndex.Rate, dgBill.CurrentCell.RowIndex];
                        else
                            dgBill.CurrentCell = dgBill[ColIndex.Rate, dgBill.CurrentCell.RowIndex - 1];
                    }
                }
                else if (e.KeyCode == Keys.F7)
                {
                    //if (btnSave.Visible == true)
                    //{
                    //    DisplayStockGodown();
                    //}
                    if (btnSave.Visible == true)
                    {
                        //DisplayStockGodown();
                        if (dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.ItemNo].Value != null)
                            dgBill.CurrentCell = dgBill[ColIndex.Quantity, dgBill.CurrentCell.RowIndex];
                        else
                            dgBill.CurrentCell = dgBill[ColIndex.Quantity, dgBill.CurrentCell.RowIndex - 1];
                    }

                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgBill_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int row, col;
                if (dgBill.CurrentCell != null)
                { col = dgBill.CurrentCell.ColumnIndex; row = dgBill.CurrentCell.RowIndex; }
                else { col = e.ColumnIndex; row = e.RowIndex; }
                if (dgBill.Rows.Count > 0)
                    dgBill.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";
                if (col == ColIndex.Quantity && row >= 0)
                {
                    //if (dgBill.Rows.Count == dgBill.CurrentCell.RowIndex + 1) AddRows = true;
                    //    if (flagParking == true) return;
                    dgBill.CurrentCell.ErrorText = "";
                    if (dgBill.CurrentCell.Value != null)
                    {
                        if (dgBill.CurrentCell.Value.ToString() != "" && dgBill.CurrentCell.Value.ToString() != "0")
                        {
                            if (ObjFunction.CheckNumeric(dgBill.CurrentCell.Value.ToString()) == true)
                            {
                                int rowIndex = dgBill.CurrentCell.RowIndex;
                                if (dgBill[ColIndex.Rate, dgBill.CurrentCell.RowIndex].Value == null || Convert.ToString(dgBill[ColIndex.Rate, dgBill.CurrentCell.RowIndex].Value) == "")
                                    dgBill[ColIndex.Amount, dgBill.CurrentCell.RowIndex].Value = "0.00";
                                //else
                                //  CalculateTotal();
                                dgBill.Focus();

                                dgBill.CurrentCell = dgBill[2, row];


                            }

                        }
                    }
                }
                else if (col == ColIndex.Rate && row >= 0)
                {
                    //    if (flagParking == true) return;
                    dgBill.CurrentCell.ErrorText = "";
                    if (dgBill.CurrentCell.Value != null)
                    {
                        if (dgBill.CurrentCell.Value.ToString() != "" && dgBill.CurrentCell.Value.ToString() != "0")
                        {
                            if (ObjFunction.CheckNumeric(dgBill.CurrentCell.Value.ToString()) == true)
                            {

                                //dgBill[5, dgBill.CurrentCell.RowIndex].Value = Convert.ToDouble(dgBill[4, dgBill.CurrentCell.RowIndex].Value) * Convert.ToDouble(dgBill[2, dgBill.CurrentCell.RowIndex].Value);
                                dgBill.CurrentCell = dgBill[ColIndex.Rate, dgBill.CurrentCell.RowIndex];
                                dgBill.Rows[dgBill.CurrentCell.RowIndex].Selected = true;
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

        }

        private void dgBill_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            TextBox txt = (TextBox)e.Control;
            txt.KeyDown += new KeyEventHandler(txtSpace_KeyDown);
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.Quantity)
            {
                TextBox txt1 = (TextBox)e.Control;
                txt1.TextChanged += new EventHandler(txtQuantity_TextChanged);
                //txt1.TextChanged -= new EventHandler(txtQuantity_TextChanged);
            }
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.FreeQty)
            {
                TextBox txt1 = (TextBox)e.Control;
                txt1.TextChanged += new EventHandler(txtFreeQuantity_TextChanged);
                //txt1.TextChanged -= new EventHandler(txtQuantity_TextChanged);
            }
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.Rate)
            {
                TextBox txtrate = (TextBox)e.Control;
                txtrate.TextChanged += new EventHandler(txtRate_TextChanged);
            }
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.MRP)
            {
                TextBox txtMRP = (TextBox)e.Control;
                txtMRP.TextChanged += new EventHandler(txtMRP_TextChanged);
            }
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscPercentage)
            {
                TextBox txtSchemeDisc = (TextBox)e.Control;
                txtSchemeDisc.TextChanged += new EventHandler(txtSchemeDisc_TextChanged);
            }
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscPercentage2)
            {
                TextBox txtDistDisc = (TextBox)e.Control;
                txtDistDisc.TextChanged += new EventHandler(txtDistDisc_TextChanged);
            }
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscAmount2)
            {
                TextBox txtDistDisc = (TextBox)e.Control;
                txtDistDisc.TextChanged += new EventHandler(txtSchemeRupess_TextChanged);
            }
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscAmount)
            {
                TextBox txtDistDisc = (TextBox)e.Control;
                txtDistDisc.TextChanged += new EventHandler(txtSchemeRupess_TextChanged);
            }
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscRupees2)
            {
                TextBox txtDiscRs2 = (TextBox)e.Control;
                txtDiscRs2.TextChanged += new EventHandler(txtDiscRs2_TextChanged);
            }
        }

        private void txtDistDisc_TextChanged(object sender, EventArgs e)
        {
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscPercentage2)
            {
                ObjFunction.SetMasked((TextBox)sender, 2, 2, JitFunctions.MaskedType.NotNegative);
            }
        }

        private void txtSchemeRupess_TextChanged(object sender, EventArgs e)
        {
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscAmount2 || dgBill.CurrentCell.ColumnIndex == ColIndex.DiscAmount)
            {
                ObjFunction.SetMasked((TextBox)sender, 2, 5, JitFunctions.MaskedType.NotNegative);
            }
        }
        private void txtDiscRs2_TextChanged(object sender, EventArgs e)
        {
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscRupees2)
            {
                ObjFunction.SetMasked((TextBox)sender, 2, 5, JitFunctions.MaskedType.NotNegative);
            }
        }




        private void txtMRP_TextChanged(object sender, EventArgs e)
        {
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.MRP)
            {
                ObjFunction.SetMasked((TextBox)sender, 2, 9, JitFunctions.MaskedType.NotNegative);
            }
        }

        private void txtSchemeDisc_TextChanged(object sender, EventArgs e)
        {
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.DiscPercentage)
            {
                ObjFunction.SetMasked((TextBox)sender, 2, 2, JitFunctions.MaskedType.NotNegative);
            }
        }

        private void txtRate_TextChanged(object sender, EventArgs e)
        {
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.Rate)
            {
                ObjFunction.SetMasked((TextBox)sender, 3, 5, JitFunctions.MaskedType.NotNegative);
            }
        }

        private void txtQuantity_TextChanged(object sender, EventArgs e)
        {
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.Quantity)
            {
                //if (((TextBox)sender).Text.Length > 6)
                //    ((TextBox)sender).Text = "1";
                ObjFunction.SetMasked((TextBox)sender, 2, 6, JitFunctions.MaskedType.NotNegative);
            }
        }
        private void txtFreeQuantity_TextChanged(object sender, EventArgs e)
        {
            if (dgBill.CurrentCell.ColumnIndex == ColIndex.FreeQty)
            {
                ObjFunction.SetMasked((TextBox)sender, 2, 6, JitFunctions.MaskedType.NotNegative);
            }
        }

        private void txtSpace_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                Spaceflag = false;
                if (dgBill.CurrentCell.RowIndex == 0)
                {
                    if (dgBill.CurrentCell.ColumnIndex != 0)
                    {
                        int col = dgBill.CurrentCell.ColumnIndex;
                        while (true)
                        {
                            if (col == 0) break;
                            if (dgBill[col - 1, dgBill.CurrentCell.RowIndex].Visible == true)
                            {
                                dgBill.CurrentCell = dgBill[col - 1, dgBill.CurrentCell.RowIndex];
                                break;
                            }
                            else
                            {
                                col = col - 1;
                            }
                        }
                    }
                }
                else
                {

                    if (dgBill.CurrentCell.ColumnIndex == 1)
                        dgBill.CurrentCell = dgBill[4, dgBill.CurrentCell.RowIndex - 1];
                    else if (dgBill.CurrentCell.ColumnIndex != 0)
                        dgBill.CurrentCell = dgBill[dgBill.CurrentCell.ColumnIndex - 1, dgBill.CurrentCell.RowIndex];
                }
            }
            TextBox txt = (TextBox)sender;
            txt.KeyDown -= new KeyEventHandler(txtSpace_KeyDown);
        }

        private void BindGrid(int RowIndex)
        {
            try
            {
                long ItemNo, RateSettingNo, BarcodeNo;
                double StockConFactor;
                DataTable dtLedger = new DataTable();

                RateSettingNo = Convert.ToInt64(dgBill.Rows[RowIndex].Cells[ColIndex.PkRateSettingNo].Value);
                ItemNo = Convert.ToInt64(dgBill.Rows[RowIndex].Cells[ColIndex.ItemNo].Value);
                BarcodeNo = Convert.ToInt64(dgBill.Rows[RowIndex].Cells[ColIndex.PkBarCodeNo].Value);

                string strSQL = "SELECT r.FkBcdSrNo, " +
                  " r.MKTQty, r.StockConversion, t.TaxLedgerNo, t.SalesLedgerNo, " +
                  " t.PkSrNo,t.Percentage, r.MRP, " +
                  " t.HSNNo, t.HSNCode, t.FkTaxSettingNo " +
                  " ,t.IGSTPercent, " +
                    " t.CGSTPercent, t.SGSTPercent, t.UTGSTPercent, t.CessPercent " +
                  " FROM " +
                  " MRateSetting As r " +
                  " , GetItemTaxAll(" + ItemNo + ", " +
                                    " NULL, " +
                                    GroupType.PurchaseAccount + ", " +
                                    ObjFunction.GetComboValue(cmbTaxType) + "," +
                                    " NULL) As t " +
                  " WHERE r.PkSrNo = " + RateSettingNo + " ";
                DataTable dt = ObjFunction.GetDataView(strSQL).Table;

                if (dt.Rows.Count > 0)
                {
                    if (BarcodeNo == 0)
                    {
                        BarcodeNo = Convert.ToInt64(dt.Rows[0][0].ToString());
                        dgBill.Rows[RowIndex].Cells[ColIndex.PkBarCodeNo].Value = BarcodeNo;
                    }

                    dgBill.Rows[RowIndex].Cells[ColIndex.MKTQuantity].Value = Convert.ToInt64(dt.Rows[0][1].ToString());

                    if (txtOtherDisc.Text.Trim() == "" || ObjFunction.CheckValidAmount(txtOtherDisc.Text) == false)
                    {
                        txtOtherDisc.Text = "0";
                    }

                    //dgBill.Rows[RowIndex].Cells[ColIndex.DiscAmount].Value = Convert.ToDouble(txtTotalDisc.Text);

                    StockConFactor = Convert.ToDouble(dt.Rows[0][2].ToString());
                    dgBill.Rows[RowIndex].Cells[ColIndex.StockFactor].Value = StockConFactor;

                    dgBill.Rows[RowIndex].Cells[ColIndex.TaxLedgerNo].Value = Convert.ToInt64(dt.Rows[0][3].ToString());
                    dgBill.Rows[RowIndex].Cells[ColIndex.SalesLedgerNo].Value = Convert.ToInt64(dt.Rows[0][4].ToString());
                    dgBill.Rows[RowIndex].Cells[ColIndex.PkItemTaxInfo].Value = Convert.ToInt64(dt.Rows[0][5].ToString());
                    if (dgBill.Rows[RowIndex].Cells[ColIndex.PkStockTrnNo].Value == null) dgBill.Rows[RowIndex].Cells[ColIndex.PkStockTrnNo].Value = 0;
                    dgBill.Rows[RowIndex].Cells[ColIndex.PkVoucherNo].Value = 0;
                    dgBill.Rows[RowIndex].Cells[ColIndex.TaxPercentage].Value = Convert.ToDouble(dt.Rows[0][6].ToString());
                    dgBill.Rows[RowIndex].Cells[ColIndex.HSNCode].Value = dt.Rows[0]["HSNCode"].ToString();

                    if (dgBill.Rows[RowIndex].Cells[ColIndex.PONo].Value == null) dgBill.Rows[RowIndex].Cells[ColIndex.PONo].Value = "0";
                    if (Convert.ToDouble(dgBill.Rows[RowIndex].Cells[ColIndex.MRP].Value) == Convert.ToDouble(dgBill.Rows[RowIndex].Cells[ColIndex.TempMRP].Value))
                    {
                        dgBill.Rows[RowIndex].Cells[ColIndex.MRP].Value = Convert.ToDouble(dt.Rows[0][7].ToString()).ToString("0.00");
                        dgBill.Rows[RowIndex].Cells[ColIndex.TempMRP].Value = Convert.ToDouble(dt.Rows[0][7].ToString()).ToString("0.00");
                    }
                    if (ID != 0)
                    {
                        if (dgBill.Rows[RowIndex].Cells[ColIndex.SalesVchNo].Value == null)
                            dgBill.Rows[RowIndex].Cells[ColIndex.SalesVchNo].Value = ObjQry.ReturnLong("SELECT TVoucherDetails.PkVoucherTrnNo FROM MItemTaxInfo INNER JOIN TVoucherDetails ON MItemTaxInfo.SalesLedgerNo = TVoucherDetails.LedgerNo " +
                                " WHERE (MItemTaxInfo.ItemNo = " + dgBill.Rows[RowIndex].Cells[ColIndex.ItemNo].Value + ") AND (TVoucherDetails.FkVoucherNo = " + ID + ")", CommonFunctions.ConStr);
                        if (dgBill.Rows[RowIndex].Cells[ColIndex.TaxVchNo].Value == null)
                            dgBill.Rows[RowIndex].Cells[ColIndex.TaxVchNo].Value = ObjQry.ReturnLong("SELECT TVoucherDetails.PkVoucherTrnNo FROM MItemTaxInfo INNER JOIN TVoucherDetails ON MItemTaxInfo.TaxLedgerNo = TVoucherDetails.LedgerNo " +
                                " WHERE (MItemTaxInfo.ItemNo = " + dgBill.Rows[RowIndex].Cells[ColIndex.ItemNo].Value + ") AND (TVoucherDetails.FkVoucherNo = " + ID + ")", CommonFunctions.ConStr);
                    }

                    #region Fill TAX Details (Part - 2) Fill GST Bifurcation details
                    //if (ObjFunction.GetComboValue(cmbTaxType) == GroupType.GST)
                    //{
                    //    string strHSNCode = dgBill.Rows[RowIndex].Cells[ColIndex.HSNCode].Value.ToString();
                    //    bool isGstTaxDetailsFound = false;
                    //    DataTable dtGSTDetails = null;
                    //    string sql = "";
                    //    if (Convert.ToInt64(dt.Rows[0]["HSNNo"].ToString()) != 0)
                    //    {
                    //        sql = "SELECT * FROM MStockGSTTaxDetails " +
                    //                " WHERE HSNNo = '" + dt.Rows[0]["HSNNo"].ToString() + "' " +
                    //                    " AND FkTaxSettingNo = " + Convert.ToInt64(dt.Rows[0]["FkTaxSettingNo"].ToString()) + " " +
                    //                    " AND IsActive = 'True'";
                    //    }
                    //    else
                    //    {
                    //        sql = "SELECT TOP 1 * FROM MStockGSTTaxDetails " +
                    //                " WHERE FkTaxSettingNo = " + Convert.ToInt64(dt.Rows[0]["FkTaxSettingNo"].ToString()) + " " +
                    //                    " AND IsActive = 'True'";
                    //    }

                    //    dtGSTDetails = ObjFunction.GetDataView(sql).Table;

                    //    if (dtGSTDetails.Rows.Count > 0)
                    //    {
                    //        isGstTaxDetailsFound = true;
                    //    }

                    //    while (!isGstTaxDetailsFound)
                    //    {
                    //        OMMessageBox.Show("Taxation details not specified for the selected SKU." + Environment.NewLine +
                    //                  "Click OK to specify taxation details and continue.",
                    //                  "Information",
                    //                  OMMessageBoxButton.Escape,
                    //                  OMMessageBoxIcon.Information);

                    //        Master.StockItemSAE sItem = new Master.StockItemSAE(ItemNo);

                    //        ObjFunction.OpenDialog(sItem, this);

                    //        //if (Convert.ToInt64(dt.Rows[0]["HSNNo"].ToString()) != 0)
                    //        //{
                    //        //    sql = "SELECT * FROM MStockGSTTaxDetails " +
                    //        //            " WHERE HSNNo = '" + dt.Rows[0]["HSNNo"].ToString() + "' " +
                    //        //                " AND FkTaxSettingNo = " + Convert.ToInt64(dt.Rows[0]["FkTaxSettingNo"].ToString()) + " " +
                    //        //                " AND IsActive = 'True'";
                    //        //}
                    //        //else
                    //        {
                    //            sql = "SELECT TOP 1 * FROM MStockGSTTaxDetails " +
                    //                    " WHERE FkTaxSettingNo = " + Convert.ToInt64(dt.Rows[0]["FkTaxSettingNo"].ToString()) + " " +
                    //                        " AND IsActive = 'True'";
                    //        }
                    //        dtGSTDetails = ObjFunction.GetDataView(sql).Table;

                    //        if (dtGSTDetails.Rows.Count > 0)
                    //        {
                    //            isGstTaxDetailsFound = true;
                    //        }
                    //    }

                    //    dgBill.Rows[RowIndex].Cells[ColIndex.IGSTPercent].Value = Convert.ToDouble(dtGSTDetails.Rows[0]["IGSTPercent"].ToString());
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.IGSTAmount].Value = 0;
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.CGSTPercent].Value = Convert.ToDouble(dtGSTDetails.Rows[0]["CGSTPercent"].ToString());
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.CGSTAmount].Value = 0;
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.SGSTPercent].Value = Convert.ToDouble(dtGSTDetails.Rows[0]["SGSTPercent"].ToString());
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.SGSTAmount].Value = 0;
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.UTGSTPercent].Value = Convert.ToDouble(dtGSTDetails.Rows[0]["UTGSTPercent"].ToString());
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.UTGSTAmount].Value = 0;
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.CessPercent].Value = Convert.ToDouble(dtGSTDetails.Rows[0]["CessPercent"].ToString());
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.CessAmount].Value = 0;
                    //}
                    //else
                    //{
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.IGSTPercent].Value = 0;
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.IGSTAmount].Value = 0;
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.CGSTPercent].Value = 0;
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.CGSTAmount].Value = 0;
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.SGSTPercent].Value = 0;
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.SGSTAmount].Value = 0;
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.UTGSTPercent].Value = 0;
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.UTGSTAmount].Value = 0;
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.CessPercent].Value = 0;
                    //    dgBill.Rows[RowIndex].Cells[ColIndex.CessAmount].Value = 0;
                    //}
                    #region New Code
                    dgBill.Rows[RowIndex].Cells[ColIndex.IGSTPercent].Value = Convert.ToDouble(dt.Rows[0]["IGSTPercent"].ToString());
                    dgBill.Rows[RowIndex].Cells[ColIndex.IGSTAmount].Value = 0;
                    dgBill.Rows[RowIndex].Cells[ColIndex.CGSTPercent].Value = Convert.ToDouble(dt.Rows[0]["CGSTPercent"].ToString());
                    dgBill.Rows[RowIndex].Cells[ColIndex.CGSTAmount].Value = 0;
                    dgBill.Rows[RowIndex].Cells[ColIndex.SGSTPercent].Value = Convert.ToDouble(dt.Rows[0]["SGSTPercent"].ToString());
                    dgBill.Rows[RowIndex].Cells[ColIndex.SGSTAmount].Value = 0;
                    dgBill.Rows[RowIndex].Cells[ColIndex.UTGSTPercent].Value = Convert.ToDouble(dt.Rows[0]["UTGSTPercent"].ToString());
                    dgBill.Rows[RowIndex].Cells[ColIndex.UTGSTAmount].Value = 0;
                    dgBill.Rows[RowIndex].Cells[ColIndex.CessPercent].Value = Convert.ToDouble(dt.Rows[0]["CessPercent"].ToString());
                    dgBill.Rows[RowIndex].Cells[ColIndex.CessAmount].Value = 0;
                    #endregion
                    #endregion


                    if (dgBill.Rows.Count == dgBill.CurrentRow.Index + 1 && (dgBill.CurrentCell.ColumnIndex == 4 || dgBill.CurrentCell.ColumnIndex == 3))
                    {
                        dgBill.Rows.Add();
                    }

                    CalculateTotal();
                }
                else
                {
                    for (int i = 1; i < dgBill.Columns.Count; i++)
                    {
                        dgBill.Rows[RowIndex].Cells[i].Value = null;
                    }
                    DisplayMessage("Items Tax Details Not Found.....");
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }


        #endregion

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                //Purchase.RequestSalesNo = 0;
                //Form NewF = new Purchase();
                //this.Close();
                //ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                ObjFunction.FillCombo(cmbPartyNameSearch, " SELECT DISTINCT MLedger.LedgerNo, MLedger.LedgerName FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo  AND  " +
                             " TVoucherEntry.VoucherTypeCode = " + VchType.Purchase + " AND  TVoucherDetails.VoucherSrNo = 1 INNER JOIN MLedger ON MLedger.LedgerNo = TVoucherDetails.LedgerNo ORDER BY MLedger.LedgerName ");
                pnlPartySearch.Visible = false;
                pnlSearch.Visible = true;
                txtSearch.Text = ""; txtSearch.Enabled = true;

                btnNew.Enabled = false;
                btnUpdate.Enabled = false;
                btndelete.Enabled = false;
                txtInvNoSearch.Enabled = true;
                cmbPartyNameSearch.Enabled = true;
                txtInvNoSearch.Text = "";
                txtSearch.Text = "";
                //cmbPartyName.SelectedIndex = 0;
                rbInvNo.Checked = true;
                rbType_CheckedChanged(rbInvNo, null);
                txtSearch.Focus();
                dgInvSearch.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                dgPartySearch.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #region Navigation Methods
        private void NavigationDisplay(int type)
        {
            try
            {
                long No = 0;
                if (type == 5)
                {
                    if (dtSearch.Rows.Count > 0)
                    {
                        No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                        ID = No;
                    }
                }
                else if (type == 1)
                {
                    No = Convert.ToInt64(dtSearch.Rows[0].ItemArray[0].ToString());
                    cntRow = 0;
                    ID = No;
                }
                else if (type == 2)
                {
                    No = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    cntRow = dtSearch.Rows.Count - 1;
                    ID = No;
                }
                else
                {
                    if (type == 3)
                    {
                        cntRow = cntRow + 1;
                    }
                    else if (type == 4)
                    {
                        cntRow = cntRow - 1;
                    }

                    if (cntRow < 0)
                    {
                        OMMessageBox.Show("This is First Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow + 1;
                    }
                    else if (cntRow > dtSearch.Rows.Count - 1)
                    {
                        OMMessageBox.Show("This is Last Record", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        cntRow = cntRow - 1;
                    }
                    else
                    {
                        No = Convert.ToInt64(dtSearch.Rows[cntRow].ItemArray[0].ToString());
                        ID = No;
                    }

                }

                if (ID > 0)
                    FillControls();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

        }

        private void SetNavigation()
        {
            try
            {
                cntRow = 0;
                for (int i = 0; i < dtSearch.Rows.Count; i++)
                {
                    if (Convert.ToInt64(dtSearch.Rows[i].ItemArray[0].ToString()) == ID)
                    {
                        cntRow = i;
                        break;
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void setDisplay(bool flag)
        {
            btnFirst.Visible = flag;
            btnPrev.Visible = flag;
            btnNext.Visible = flag;
            btnLast.Visible = flag;
            //btnDelete.Visible = flag;
            //GridRange.Height = 25;
            if (dtSearch.Rows.Count == 0)
            {
                btnFirst.Visible = false;
                btnPrev.Visible = false;
                btnNext.Visible = false;
                btnLast.Visible = false;
            }
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            NavigationDisplay(1);
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            NavigationDisplay(4);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            NavigationDisplay(3);
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            NavigationDisplay(2);
        }

        #endregion

        #region KeyDown Events
        private void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is GroupBox)
                    KeyDownFormat(ctrl.Controls);
            }
        }

        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left && e.Control)
            {
                if (btnPrev.Enabled) btnPrev_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Up && e.Control)
            {
                if (btnFirst.Enabled) btnFirst_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Right && e.Control)
            {
                if (btnNext.Enabled) btnNext_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Down && e.Control)
            {
                if (btnLast.Enabled) btnLast_Click(sender, e);
            }

            else if (e.KeyCode == Keys.F12)
            {
                if (btnSave.Visible && ID == 0)
                {
                    cmbPaymentType.SelectedValue = "2";
                    btnSave_Click(sender, e);
                }
            }
            else if (e.KeyCode == Keys.F11)
            {
                if (btnSave.Visible && ID == 0)
                {
                    cmbPaymentType.SelectedValue = "3";
                    btnSave_Click(sender, e);
                }
            }
            else if (e.KeyCode == Keys.F7)
            {
                if (dgBill.Focused == true)
                {
                    if (dgBill.Rows.Count > 0)
                    {
                        if (dgBill.CurrentCell.ColumnIndex == 2)
                        {
                            dgBill.CurrentCell.ReadOnly = false;
                            //AddRows = false;
                            //FlagRate = false;
                            //defaultQty = true;
                        }
                    }
                }
            }
            else if (e.KeyCode == Keys.Escape)
            {
                //btnExit_Click(sender, e);
            }
            //else if (e.KeyCode == Keys.F2)
            //{
            //    if (txtDiscount1.Enabled) txtDiscount1.Focus();
            //}
            else if (e.KeyCode == Keys.F4)
            {
                if (ID == 0 && btnSave.Visible == true)
                    cmbPaymentType.Focus();
                //if (ID == 0)
                //{
                //    if (ExchangeMode == false)
                //    {
                //        lblExchange.Visible = true;
                //        ExchangeMode = true;
                //    }
                //    else
                //    {
                //        ExchangeMode = false;
                //        lblExchange.Visible = false;
                //    }
                //}
            }
            else
                if (e.KeyCode == Keys.F3)
                {
                    if (btnMixMode.Text == "Mix\r\nMode\r\n(F3)" && btnMixMode.Visible == true && btnMixMode.Enabled == true)
                    {
                        btnMixMode_Click(sender, e);
                    }
                    //if (dgCompany.Rows.Count > 1 && btnNew.Visible)
                    //{
                    //    pnlCompany.Visible = true;
                    //    dgCompany.Focus();
                    //    dgCompany.CurrentCell = dgCompany[1, 0];
                    //}


                }
            if (e.KeyCode == Keys.P && e.Control)
            {
                PrintBill();
            }

            else if (e.KeyCode == Keys.F1)
            {
                e.SuppressKeyPress = true;
                if (pnlFooterInfo.Visible == false)
                {
                    pnlFooterInfo.Dock = DockStyle.Bottom;
                    //pnlFooterInfo.Height = 30;
                    pnlFooterInfo.BorderStyle = BorderStyle.None;
                    pnlFooterInfo.BringToFront();
                    pnlFooterInfo.Visible = true;
                }
                else
                {
                    pnlFooterInfo.Visible = false;
                }
            }
            //else if (e.KeyCode == Keys.F5)
            //{
            //    if (ID == 0)
            //        ParkingSave();
            //}
            //else if (e.KeyCode == Keys.F5)
            //{
            //    Import_Bill_IN_Excel_File();
            //}
            else if (e.KeyCode == Keys.F6)
            {
                if (dgBill.Rows.Count == 1 && btnSave.Visible == true)
                {
                    ShowParkingBill();
                }

            }
            //else if (e.KeyCode == Keys.F4)
            //    ValidationsMain();
            else if (e.KeyCode == Keys.L && e.Control)
            {
                if (btnSave.Visible)
                {
                    dgBill.Columns[ColIndex.LandedRate].Visible = !dgBill.Columns[ColIndex.LandedRate].Visible;
                    if (dgBill.Columns[ColIndex.LandedRate].Visible == true)
                    {
                        if (dgBill.CurrentCell != null)
                            dgBill.CurrentCell = dgBill[ColIndex.LandedRate, dgBill.CurrentRow.Index];
                        else
                            dgBill.CurrentCell = dgBill[ColIndex.LandedRate, 0];
                    }
                    else
                    {
                        if (dgBill.CurrentCell != null)
                            dgBill.CurrentCell = dgBill[ColIndex.ItemName, dgBill.CurrentRow.Index];
                        else
                            dgBill.CurrentCell = dgBill[ColIndex.ItemName, 0];
                    }

                }
            }
            else if (e.KeyCode == Keys.Q && e.Control)
            {
                if (btnSave.Visible == true)
                {
                    if (ObjFunction.CheckAllowMenu(10) == false) return;
                    Form NewF = new Master.StockItemSAE(-1);
                    ObjFunction.OpenForm(NewF);

                    if (((Master.StockItemSAE)NewF).ShortID != 0)
                    {
                        string barcode = ObjQry.ReturnString("Select BarCode From MStockBarCode where ItemNo=" + ((Master.StockItemSAE)NewF).ShortID + "", CommonFunctions.ConStr);
                        int rwindex = dgBill.CurrentCell.RowIndex;
                        dgBill.CurrentRow.Cells[ColIndex.ItemName].Value = barcode;
                        dgBill_CellEndEdit(dgBill, new DataGridViewCellEventArgs(ColIndex.ItemName, rwindex));
                        bw_ItemSearch.RunWorkerAsync();
                    }
                }
            }
            else if (e.KeyCode == Keys.W && e.Control)
            {
                if (btnSave.Visible == false)
                {
                    MixModeFlag = true;
                    cmbPaymentType.SelectedValue = "3";
                    btnSave_Click(sender, e);

                }
            }
            else if (e.KeyCode == Keys.M && e.Control)
            {
                if (cmbPaymentType.SelectedValue.ToString() == "3" && btnMixMode.Visible == true)
                {
                    //btnMixMode_Click(sender, e);

                }
            }
            else if (e.Alt && e.KeyCode == Keys.F2)
            {
                if (btnNew.Visible == false)
                {
                    if (btnAdvanceSearch.Enabled) btnAdvanceSearch_Click(sender, e);
                }
            }
        }
        #endregion

        #region Delete code
        private void InitDelTable()
        {
            dtDelete.Columns.Add();
            dtDelete.Columns.Add();
        }

        private void DeleteDtls(int Type, long PkNo)
        {
            DataRow dr = null;
            dr = dtDelete.NewRow();
            dr[0] = Type;
            dr[1] = PkNo;
            dtDelete.Rows.Add(dr);
        }

        private void DeleteValues()
        {

            if (dtDelete != null)
            {

                for (int i = 0; i < dtDelete.Rows.Count; i++)
                {
                    if (Convert.ToInt32(dtDelete.Rows[i].ItemArray[0]) == 1)
                    {
                        tStock.PkStockTrnNo = Convert.ToInt64(dtDelete.Rows[i].ItemArray[1]);
                        dbTVoucherEntry.DeleteTStock(tStock);
                    }
                    else if (Convert.ToInt32(dtDelete.Rows[i].ItemArray[0]) == 2)
                    {
                        tVoucherDetails.PkVoucherTrnNo = Convert.ToInt64(dtDelete.Rows[i].ItemArray[1]);
                        dbTVoucherEntry.DeleteTVoucherDetails(tVoucherDetails);
                    }
                    else if (Convert.ToInt32(dtDelete.Rows[i].ItemArray[0]) == 3)
                    {
                        tVoucherDetails.CompanyNo = Convert.ToInt64(dtDelete.Rows[i].ItemArray[1]);
                        tVoucherDetails.FkVoucherNo = ID;
                        dbTVoucherEntry.DeleteTVoucherDetailsCompany(tVoucherDetails);
                    }
                    else if (Convert.ToInt32(dtDelete.Rows[i].ItemArray[0]) == 4)
                    {
                        tStockGodown.PKStockGodownNo = Convert.ToInt64(dtDelete.Rows[i].ItemArray[1]);
                        dbTVoucherEntry.DeleteTStockGodown(tStockGodown);
                    }
                    else if (Convert.ToInt32(dtDelete.Rows[i].ItemArray[0]) == 5)
                    {
                        tOtherStkDtls.PkSrNo = Convert.ToInt64(dtDelete.Rows[i].ItemArray[1]);
                        dbTVoucherEntry.DeleteTOtherStockDetails(tOtherStkDtls);
                    }
                    //else if (Convert.ToInt32(dtDelete.Rows[i].ItemArray[0]) == 2)
                    //{
                    //    dbTVoucherEntry.UpdateTStockBarCode(Convert.ToInt64(dtDelete.Rows[i].ItemArray[1]));
                    //}
                    //else if (Convert.ToInt32(dtDelete.Rows[i].ItemArray[0]) == 3)
                    //{
                    //    tRequire.PkRequireNo = Convert.ToInt64(dtDelete.Rows[i].ItemArray[1]);
                    //    dbTVoucherEntry.DeleteTRequiredQuantity(tRequire);
                    //}
                    //else if (Convert.ToInt32(dtDelete.Rows[i].ItemArray[0]) == 4)
                    //{
                    //    ObjTrans.Execute("Update TStockBarCode set IsSale='False' Where PkSrNo=(Select FKStockBarCode From TParkingBills Where PkSrNo=" + Convert.ToInt64(dtDelete.Rows[i].ItemArray[1]) + ")", CommonFunctions.ConStr);

                    //    TParkingBills tParking = new TParkingBills();
                    //    tParking.PkSrNo = Convert.ToInt64(dtDelete.Rows[i].ItemArray[1]);
                    //    tParking.BillNo = 0;
                    //    dbTVoucherEntry.DeleteTParkingBills(tParking);
                    //}
                    //else if (Convert.ToInt32(dtDelete.Rows[i].ItemArray[0]) == 5)
                    //{
                    //    tExchange.PkSrNo = Convert.ToInt64(dtDelete.Rows[i].ItemArray[1]);
                    //    dbTVoucherEntry.DeleteTExchangeDetails(tExchange);
                    //}
                }
                dtDelete.Rows.Clear();


            }
        }


        #endregion

        #region Parial Payment Methods

        private void Partial_Leave(object sender, EventArgs e)
        {
            if (((TextBox)sender).Text == "")
            {
                OMMessageBox.Show("Please Enter amount", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                ((TextBox)sender).Focus();
            }
            else if (ObjFunction.CheckValidAmount(((TextBox)sender).Text) == false)
            {
                OMMessageBox.Show("Please Enter valid amount", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                ((TextBox)sender).Focus();
            }
            else
            {
                //double cash, credit;
                //cash = (txtCash.Text == "") ? 0 : Convert.ToDouble(txtCash.Text); txtCash.Text = cash.ToString("0.00");
                //credit = (txtCredit.Text == "") ? 0 : Convert.ToDouble(txtCredit.Text); txtCredit.Text = credit.ToString("0.00");
                //txtBalance.Text = (Convert.ToDouble(txtGrandTotal.Text) - (cash + credit)).ToString("0.00");
            }
        }
        private void Partial_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Partial_Leave(sender, e);
            }
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                bool flag = true;
                if (Convert.ToDouble(dgPayType.Rows[3].Cells[2].Value) > 0)
                {
                    if (dgPayChqDetails.Rows[0].Cells[0].Value == null || dgPayChqDetails.Rows[0].Cells[1].FormattedValue.ToString().Trim() == "" || dgPayChqDetails.Rows[0].Cells[2].FormattedValue.ToString().Trim() == "")
                    {
                        OMMessageBox.Show("Please Fill Cheque Details.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        flag = false;
                    }
                    else flag = true;
                }
                if (flag == true)
                {
                    if (Convert.ToDouble(dgPayType.Rows[4].Cells[2].Value) > 0)
                    {
                        if (dgPayCreditCardDetails.Rows[0].Cells[0].FormattedValue.ToString().Trim() == "" || dgPayCreditCardDetails.Rows[0].Cells[1].FormattedValue.ToString().Trim() == "")
                        {
                            OMMessageBox.Show("Please Fill Credit Card Details.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            flag = false;
                        }
                        else flag = true;
                    }
                }
                if (flag == true)
                {
                    if (Convert.ToDouble(txtTotalAmt.Text) != Convert.ToDouble(txtGrandTotal.Text))
                    {
                        OMMessageBox.Show("TOTAL AMOUNT EXCEEDS TO BILL AMOUNT.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                    else
                    {
                        btnSave.Enabled = true;
                        btnSave.Focus();
                        pnlPartial.Visible = false;
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }
        #endregion

        private void DisplayMessage(string str)
        {
            lblMsg.Visible = true;
            lblMsg.Text = str;
            Application.DoEvents();
            System.Threading.Thread.Sleep(700);
            lblMsg.Visible = false;
        }

        private void lblStatus_Click(object sender, EventArgs e)
        {
            try
            {
                if (BillingMode == 0)
                {
                    BillingMode = 1;
                    //VoucherType = VchType.TempSales;
                    lblStatus.ForeColor = Color.Green;
                }
                else
                {
                    BillingMode = 0;
                    //VoucherType = VchType.Purchase;
                    lblStatus.ForeColor = Color.Red;
                }

                ObjFunction.LockButtons(true, this.Controls);
                ObjFunction.LockControls(false, this.Controls);

                InitDelTable();
                intialCurrentStock();
                txtInvNo.Enabled = false;
                InitControls();
                dtSearch = ObjFunction.GetDataView("Select PkVoucherNo from TVoucherEntry Where VoucherTypeCode=" + VoucherType + "").Table;

                if (dtSearch.Rows.Count > 0)
                {
                    ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                    FillControls();
                    SetNavigation();
                }

                setDisplay(true);
                btnNew.Focus();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                ID = 0;
                partialPay = new PartialPurchasePayment();
                ObjFunction.InitialiseControl(this.Controls);
                ObjFunction.LockButtons(false, this.Controls);
                ObjFunction.LockControls(true, this.Controls);
                dgBill.Enabled = true;
                InitControls();
                ObjFunction.FillComb(cmbPartyName, "Select LedgerNo,LedgerName From MLedger Where GroupNo in " +
                    "(" + GroupType.SundryCreditors + ") and IsActive='true' order by LedgerName");
                cmbPartyName.SelectedValue = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_DefaultPartyAC));
                LastBillNo = ObjQry.ReturnLong("Select IsNull(Max(PkVoucherNo),0) From TVoucherEntry Where VoucherTypeCode=" + VoucherType + " ", CommonFunctions.ConStr);
                dt = ObjFunction.GetDataView("SELECT IsNull(SUM(TStock.Quantity),0) AS Quantity, IsNull(SUM(TStock.Amount),0) AS Amount FROM TVoucherDetails INNER JOIN TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN " +
                    " TStock ON TVoucherDetails.PkVoucherTrnNo = TStock.FkVoucherTrnNo WHERE (TVoucherDetails.VoucherSrNo = 1) AND (TVoucherEntry.VoucherTypeCode = " + VoucherType + ") AND (TVoucherEntry.PkVoucherNo = " + LastBillNo + ")").Table;

                if (dt.Rows.Count > 0)
                {
                    txtLastBillAmt.Text = Convert.ToDouble(dt.Rows[0].ItemArray[1].ToString()).ToString("0.00");
                    txtlastBillQty.Text = dt.Rows[0].ItemArray[0].ToString();
                    txtLastPayment.Text = ObjQry.ReturnString("SELECT MPayType.PayTypeName FROM MPayType INNER JOIN TVoucherEntry ON MPayType.PKPayTypeNo = TVoucherEntry.PayTypeNo WHERE (TVoucherEntry.PkVoucherNo = " + LastBillNo + ")", CommonFunctions.ConStr);
                    txtLastBillAmt.Enabled = false;
                    txtlastBillQty.Enabled = false;
                    txtLastPayment.Enabled = false;
                }
                btnMixMode.Visible = false;
                cmbPaymentType.SelectedValue = "2";
                //cmbPaymentType.Enabled = false;
                cmbTaxType.SelectedValue = ObjFunction.GetAppSettings(AppSettings.P_TaxType);
                cmbTaxType.Enabled = false;
                tempDate = dtpBillDate.Value.Date;
                tempPartyNo = ObjFunction.GetComboValue(cmbPartyName);
                btnMixMode.Visible = true;
                POSelection.dtPOMain = new DataTable();
                lblMixMode.Visible = false;
                dtpBillDate.Focus();
                strRateType = ObjFunction.GetComboValueString(cmbRateType);
                bw_ItemSearch.RunWorkerAsync();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo in ( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + ")", CommonFunctions.ConStr) > 1)
                {
                    btnUpdate.Visible = false;
                    btnMixMode.Visible = false;
                    btndelete.Visible = false;
                    OMMessageBox.Show("Already this bill Payment is done", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    return;
                }
                else
                {
                    if (ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo in ( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + " and TR.TypeOfRef in(6))", CommonFunctions.ConStr) > 1)
                    {
                        btnUpdate.Visible = false;
                        btnMixMode.Visible = false;
                        btndelete.Visible = false;
                        OMMessageBox.Show("Already this bill Payment is done", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        return;
                    }
                }

                ObjFunction.LockButtons(false, this.Controls);
                ObjFunction.LockControls(true, this.Controls);
                dgBill.Enabled = true;
                dgBill.Focus();
                dgBill.CurrentCell = dgBill[1, dgBill.Rows.Count - 1];
                btnMixMode.Visible = false;
                if (ObjFunction.GetComboValue(cmbPartyName) == Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_PartyAC)))// && (ObjFunction.GetComboValue(cmbPaymentType) == 3))
                {
                    //cmbPaymentType.SelectedValue = 2;
                    //cmbPaymentType.Enabled = false;
                }
                else
                    cmbPaymentType.Enabled = true;
                if (ID != 0)
                {
                    // cmbPaymentType.Enabled = false;
                    //dgPayType.Enabled = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ObjFunction.LockButtons(true, this.Controls);
            ObjFunction.LockControls(false, this.Controls);
            partialPay = new PartialPurchasePayment();
            NavigationDisplay(5);
            DisplayList(false);
            btnSave.Enabled = true;
            dgBill.Enabled = false;
            MixModeFlag = false;
            MixModeVal = 0;
            POSelection.dtPOMain = new DataTable();
            btnNew.Focus();
        }

        private void DisplayList(bool flag)
        {
            pnlItemName.Visible = flag;
            pnlGroup1.Visible = flag;
            pnlGroup2.Visible = flag;
            pnlUOM.Visible = flag;
            pnlRate.Visible = flag;
            pnlCompany.Visible = flag;
        }

        private void cmbPaymentType_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                int cntCompany = 0;
                if (e.KeyCode == Keys.Enter)
                {
                    for (int i = 0; i < dgPayType.Rows.Count; i++)
                    {
                        dgPayType.Rows[i].Cells[2].Value = "0.00";
                    }
                    long ControlUnder = ObjQry.ReturnLong("Select ControlUnder From MPayType Where PKPayTypeNo=" + ObjFunction.GetComboValue(cmbPaymentType) + "", CommonFunctions.ConStr);
                    if (ObjFunction.GetComboValue(cmbPartyName) == Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_PartyAC)))
                    {
                        if (ControlUnder == 3)
                        {
                            OMMessageBox.Show("This PayType is Not Valid For Current party .. Select other PayType...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            cmbPaymentType.Focus();
                            return;
                        }
                    }
                    if (ObjFunction.GetComboValue(cmbPaymentType) == 1 || ObjFunction.GetComboValue(cmbPaymentType) == 4 || ObjFunction.GetComboValue(cmbPaymentType) == 5)
                    {
                        cmbPaymentType.TabIndex = 656;
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.S_IsAllowSingleFirmChq)) == true)
                        {
                            for (int i = 1; i < dgBill.Rows.Count - 1; i++)
                                if (Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.StockCompanyNo].Value) != Convert.ToInt64(dgBill.Rows[i - 1].Cells[ColIndex.StockCompanyNo].Value))
                                    cntCompany++;

                        }
                        if (cntCompany == 0 || ObjFunction.GetComboValue(cmbPaymentType) == 4 || ObjFunction.GetComboValue(cmbPaymentType) == 5)
                        {
                            //dgPayType.CurrentCell = dgPayType[2, 1];
                            pnlPartial.Visible = true;
                            if (ObjFunction.GetComboValue(cmbPaymentType) == 4 || ObjFunction.GetComboValue(cmbPaymentType) == 5)
                            {
                                if (ObjFunction.GetComboValue(cmbPaymentType) == 4)
                                {
                                    pnlPartial.Size = new Size(475, 214);
                                    pnlPartial.Location = new Point(75, 123);
                                    dgPayChqDetails.Location = dgPayType.Location;
                                    dgPayChqDetails.Visible = true;
                                    dgPayChqDetails.BringToFront();
                                    dgPayChqDetails.Columns[3].Visible = false;
                                    dgPayChqDetails.Focus();
                                    dgPayCreditCardDetails.Visible = false;
                                    dgPayChqDetails.Enabled = true;
                                    if (dgPayChqDetails.Rows.Count == 0)
                                    {
                                        dgPayChqDetails.Rows.Add();
                                        dgPayChqDetails.CurrentCell = dgPayChqDetails[0, 0];
                                    }
                                }
                                else if (ObjFunction.GetComboValue(cmbPaymentType) == 5)
                                {
                                    pnlPartial.Size = new Size(475, 214);
                                    pnlPartial.Location = new Point(75, 123);
                                    dgPayCreditCardDetails.Location = dgPayType.Location;
                                    dgPayCreditCardDetails.Visible = true;
                                    ((DataGridViewTextBoxColumn)dgPayCreditCardDetails.Columns[0]).MaxInputLength = Convert.ToInt32(ObjFunction.GetAppSettings(AppSettings.S_CreditCardDigitLimit));
                                    dgPayCreditCardDetails.Focus();
                                    dgPayCreditCardDetails.BringToFront();
                                    dgPayChqDetails.Visible = false;
                                    dgPayCreditCardDetails.Enabled = true;
                                    dgPayCreditCardDetails.Columns[2].Visible = false;
                                    if (dgPayCreditCardDetails.Rows.Count == 0)
                                    {
                                        dgPayCreditCardDetails.Rows.Add();
                                        dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[0, 0];
                                    }
                                }
                                dgPayType.Rows[Convert.ToInt32(ObjFunction.GetComboValue(cmbPaymentType)) - 1].Cells[2].Value = txtGrandTotal.Text;
                                // dgPayType.CurrentCell = dgPayType[2, Convert.ToInt32(ObjFunction.GetComboValue(cmbPaymentType)) - 1];
                                CaluculatePayType();
                            }
                            else
                            {
                                dgPayCreditCardDetails.Visible = false;
                                dgPayChqDetails.Visible = false;
                                pnlPartial.Location = new Point(200, 123);
                                pnlPartial.Size = new Size(305, 221);
                                dgPayType.CurrentCell = dgPayType[2, 1];
                                dgPayType.Focus();
                            }
                            btnSave.Enabled = false;
                        }
                    }
                    else if (ObjFunction.GetComboValue(cmbPaymentType) == 3)
                    {
                        e.SuppressKeyPress = true;
                        btnSave.Enabled = true;
                        btnSave.Focus();
                    }
                    else
                    {
                        pnlPartial.Visible = false;
                        //cmbPaymentType.TabIndex = 10;
                        btnSave.Enabled = true;
                        txtRemark.Focus(); ;
                    }
                    e.SuppressKeyPress = true;
                    //if (ObjFunction.GetComboValue(cmbPaymentType) == 1 || ObjFunction.GetComboValue(cmbPaymentType) == 4 || ObjFunction.GetComboValue(cmbPaymentType) == 5)
                    //{
                    //    cmbPaymentType.TabIndex = 656;
                    //    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_IsAllowSingleFirmChq)) == true)
                    //    {
                    //        for (int i = 1; i < dgBill.Rows.Count - 1; i++)
                    //            if (Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.StockCompanyNo].Value) != Convert.ToInt64(dgBill.Rows[i - 1].Cells[ColIndex.StockCompanyNo].Value))
                    //                cntCompany++;

                    //    }
                    //    if (cntCompany == 0 || ObjFunction.GetComboValue(cmbPaymentType) == 4 || ObjFunction.GetComboValue(cmbPaymentType) == 5)
                    //    {
                    //        dgPayType.CurrentCell = dgPayType[2, 1];
                    //        if (ObjFunction.GetComboValue(cmbPaymentType) == 4 || ObjFunction.GetComboValue(cmbPaymentType) == 5)
                    //        {
                    //            dgPayType.Rows[Convert.ToInt32(ObjFunction.GetComboValue(cmbPaymentType)) - 1].Cells[2].Value = txtGrandTotal.Text;
                    //            dgPayType.CurrentCell = dgPayType[2, Convert.ToInt32(ObjFunction.GetComboValue(cmbPaymentType)) - 1];
                    //            CaluculatePayType();
                    //        }
                    //        dgPayType.Focus();
                    //        btnSave.Enabled = false;
                    //        pnlPartial.Visible = true;
                    //    }
                    //}
                    //else if (ObjFunction.GetComboValue(cmbPaymentType) == 3)
                    //{
                    //    e.SuppressKeyPress = true;
                    //    cmbPartyName.Focus();
                    //}
                    //else
                    //{
                    //    pnlPartial.Visible = false;
                    //    cmbPaymentType.TabIndex = 10;
                    //    txtRemark.Focus(); ;
                    //}
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            try
            {

                if (ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo in ( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + ")", CommonFunctions.ConStr) > 1)
                {
                    btnUpdate.Visible = false;
                    btnMixMode.Visible = false;
                    btndelete.Visible = false;
                    OMMessageBox.Show("Already this bill Payment is done", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    return;
                }
                else
                {
                    if (ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo in ( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + " and TR.TypeOfRef in(6))", CommonFunctions.ConStr) > 1)
                    {
                        btnUpdate.Visible = false;
                        btnMixMode.Visible = false;
                        btndelete.Visible = false;
                        OMMessageBox.Show("Already this bill Payment is done", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        return;
                    }
                }

                if (OMMessageBox.Show("Are you sure you want to delete the record ?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                {

                    dbTVoucherEntry = new DBTVaucherEntry();
                    tVoucherEntry = new TVoucherEntry();
                    tVoucherEntry.PkVoucherNo = ID;
                    dbTVoucherEntry.DeleteAllVoucherEntry(tVoucherEntry);

                    for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                    {
                        //dbTVoucherEntry.UpdateTStockBarCode(Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.PkVoucherNo].Value.ToString()));
                    }

                    OMMessageBox.Show("Record deleted successfully.....", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);


                    dtSearch = ObjFunction.GetDataView("Select PkVoucherNo from TVoucherEntry Where VoucherTypeCode=" + VoucherType + "").Table;
                    ID = ObjQry.ReturnLong("Select Max(PkVoucherNo) FRom TVoucherEntry Where VoucherTypeCode=" + VoucherType + "", CommonFunctions.ConStr);
                    SetNavigation();
                    FillControls();

                    setDisplay(true);
                    ObjFunction.LockButtons(true, this.Controls);
                    ObjFunction.LockControls(false, this.Controls);
                    dgBill.Enabled = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnCancelSearch_Click(object sender, EventArgs e)
        {
            pnlSearch.Visible = false;
            btnNew.Enabled = true;
            btnUpdate.Enabled = true;
            btndelete.Enabled = true;
        }

        private void txtChrg2_Leave(object sender, EventArgs e)
        {
            cmbPaymentType.Focus();
            CalculateTotal();
        }

        private void txtRemark_Leave(object sender, EventArgs e)
        {
            if (txtRemark.Text.Trim() == "")
            {
                txtRemark.Text = "Purchase Bill";
                btnSave.Focus();
            }
        }

        #region Without Barcode Methods

        private void FillItemList(int qNo, int iType)
        {
            try
            {
                switch (iType)
                {
                    case 1:
                        FillItemList(qNo);
                        break;
                    case 2:
                        break;
                    case 3:
                        //string ItemList = " SELECT MStockItems.ItemNo, MStockItems.ItemName, MRateSetting." + ObjFunction.GetComboValueString(cmbRateType) + " AS SaleRate, MUOM.UOMName, MRateSetting.MRP, " +
                        //    " 0 AS Stock, '' AS stkUOM, 0 AS SaleTax, 0 AS PurTax, " + //MItemTaxInfo_Sale.Percentage AS SaleTax , MItemTaxInfo_Pur.Percentage
                        //    " MStockItems.CompanyNo, MStockBarcode.Barcode, MRateSetting.PkSrNo As RateSettingNo, MStockItems.UOMDefault " +
                        //    " FROM MStockItems_V(NULL,NULL,NULL,NULL,NULL,NULL,NULL) AS MStockItems INNER JOIN " +
                        //    " dbo.GetItemRateAll(NULL, NULL, NULL, NULL, NULL,NULL) AS MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo AND  " +
                        //    " MStockItems.UOMDefault = MRateSetting.UOMNo INNER JOIN " +
                        //    " MStockBarcode ON MRateSetting.FkBcdSrNo = MStockBarcode.PkStockBarcodeNo INNER JOIN " +
                        //    " MUOM ON MStockItems.UOMDefault = MUOM.UOMNo  Where MStockItems.ItemNo in " +
                        //    "(Select ItemNo from MStockBarcode where Barcode = '" + dgBill.CurrentCell.Value + "' And IsActive ='true') AND MStockItems.CompanyNo=" + DBGetVal.CompanyNo + " " +
                        //    " ORDER BY MStockItems.ItemName";
                        DataTable dtBarCodeItemNo = ObjFunction.GetDataView("Select ItemNo from MStockBarcode where Barcode = '" + dgBill.CurrentCell.Value + "'").Table;
                        string ItemList = "";
                        for (int i = 0; i < dtBarCodeItemNo.Rows.Count; i++)
                        {
                            if (i != 0)
                            {
                                ItemList += " Union all ";
                            }

                            //ItemList += " SELECT MStockItems.ItemNo, MStockItems.ItemName,MStockItems.ItemNameLang, MRateSetting." + ObjFunction.GetComboValueString(cmbRateType) + " AS SaleRate, MUOM.UOMName, MRateSetting.MRP, " +
                            //    " (SELECT  ISNULL(SUM(CASE WHEN trncode = 1 THEN quantity ELSE - quantity END), 0) +  ISNULL(SUM(CASE WHEN trncode = 1 THEN FreeQty ELSE - FreeQty END), 0)   FROM TStock  INNER JOIN TStockGodown ON TSTock.PKStockTrnNo=TStockGodown.FKStockTrnNo   WHERE  TStockGodown.GodownNo=" + ObjFunction.GetComboValue(cmbLocation) + " AND    (FkRateSettingNo IN  (SELECT PkSrNo  FROM MRateSetting AS MRateSetting_1  WHERE (TStock.ItemNo = MStockItems.ItemNo) AND (MRP = MRateSetting.MRP))) AND (TStock.ItemNo = MStockItems.ItemNo) And PkStockTrnNo Not In (SELECT TStock.PkStockTrnNo FROM TVoucherEntry INNER JOIN  TStock ON TVoucherEntry.PkVoucherNo = TStock.FKVoucherNo WHERE (TVoucherEntry.IsCancel = 'True') AND (TStock.ItemNo = MStockItems.ItemNo))) AS Stock, '' AS stkUOM, 0 AS SaleTax, 0 AS PurTax, " + //MItemTaxInfo_Sale.Percentage AS SaleTax , MItemTaxInfo_Pur.Percentage
                            //    " MStockItems.CompanyNo, MStockBarcode.Barcode, MRateSetting.PkSrNo As RateSettingNo, MStockItems.UOMDefault,MRateSetting.PurRate " +
                            //    " FROM MStockItems_V(NULL," + dtBarCodeItemNo.Rows[i].ItemArray[0].ToString() + ",NULL,NULL,NULL,NULL,NULL) AS MStockItems INNER JOIN " +
                            //    " dbo.GetItemRateAll(" + dtBarCodeItemNo.Rows[i].ItemArray[0].ToString() + ", NULL, NULL, NULL, '" + dtpBillDate.Value.ToString("dd-MMM-yyyy") + " " + DBGetVal.ServerTime.ToLongTimeString() + "',NULL) AS MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo AND  " +
                            //    " MStockItems.UOMDefault = MRateSetting.UOMNo INNER JOIN " +
                            //    " MStockBarcode ON MRateSetting.FkBcdSrNo = MStockBarcode.PkStockBarcodeNo INNER JOIN " +
                            //    " MUOM ON MStockItems.UOMDefault = MUOM.UOMNo  Where  " +
                            //    " MStockItems.IsActive='true' and MStockItems.FkStockGroupTypeNo<>3 ";
                            ItemList += " SELECT MStockItems.ItemNo, MStockItems.ItemName,MStockItems.ItemNameLang, MRateSetting." + ObjFunction.GetComboValueString(cmbRateType) + " AS SaleRate, MUOM.UOMName, MRateSetting.MRP, " +
                                " IsNull(MSB.CurrentStock,0) AS Stock, '' AS stkUOM, 0 AS SaleTax, 0 AS PurTax, " + //MItemTaxInfo_Sale.Percentage AS SaleTax , MItemTaxInfo_Pur.Percentage
                                " MStockItems.CompanyNo, MStockBarcode.Barcode, MRateSetting.PkSrNo As RateSettingNo, MStockItems.UOMDefault,MRateSetting.PurRate " +
                                " FROM MStockItems_V(NULL," + dtBarCodeItemNo.Rows[i].ItemArray[0].ToString() + ",NULL,NULL,NULL,NULL,NULL) AS MStockItems INNER JOIN " +
                                " dbo.GetItemRateAll(" + dtBarCodeItemNo.Rows[i].ItemArray[0].ToString() + ", NULL, NULL, NULL, '" + dtpBillDate.Value.ToString("dd-MMM-yyyy") + " " + DBGetVal.ServerTime.ToLongTimeString() + "',NULL) AS MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo AND  " +
                                " MStockItems.UOMDefault = MRateSetting.UOMNo INNER JOIN " +
                                " MStockBarcode ON MRateSetting.FkBcdSrNo = MStockBarcode.PkStockBarcodeNo INNER JOIN " +
                                " MUOM ON MStockItems.UOMDefault = MUOM.UOMNo LEFT OUTER JOIN MStockItemBalance MSB ON MSB.ItemNo = MStockItems.ItemNo AND MSB.MRP = MRateSetting.MRP AND MSB.GodownNo = MStockItems.GodownNo  Where  " +
                                " MStockItems.IsActive='true' and MStockItems.FkStockGroupTypeNo<>3 ";
                        }
                        ItemList += " ORDER BY MStockItems.ItemName";

                        DataTable dtItemList = ObjFunction.GetDataView(ItemList).Table;
                        if (dtItemList.Rows.Count > 0)
                        {
                            dgItemList.DataSource = dtItemList.DefaultView;
                            pnlItemName.Visible = true;
                            dgItemList.CurrentCell = dgItemList[1, 0];
                            dgItemList.Focus();
                        }
                        else
                        {
                            DisplayMessage("Items Not Found......");
                            dgBill.CurrentCell = dgBill[dgBill.CurrentCell.ColumnIndex, dgBill.CurrentCell.RowIndex];
                            dgBill.Focus();
                        }
                        break;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillItemList(int qNo)
        {
            try
            {
                if (qNo == 0)
                {
                    qNo = iItemNameStartIndex;
                }

                string ItemList = strItemQuery[qNo - 1];

                ItemList = ItemList.Replace("@cmbRateType@", "" + ObjFunction.GetComboValueString(cmbRateType));
                ItemList = ItemList.Replace("ORDER BY ItemName", " and MStockItems.FkStockGroupTypeNo<>3 ORDER BY ItemName");
                switch (qNo)
                {
                    case 1:
                        break;
                    case 2:
                        switch (strItemQuery.Length)
                        {
                            case 2:
                                ItemList = ItemList.Replace("@Param1@", "" + (Convert.ToInt64(lstGroup1.SelectedValue) != 0 ? lstGroup1.SelectedValue.ToString() : Param1Value));
                                ItemList = ItemList.Replace("@Param1NULL@", "" + (Convert.ToInt64(lstGroup1.SelectedValue) != 0 ? lstGroup1.SelectedValue.ToString() : "NULL"));
                                break;
                            case 3:
                                ItemList = ItemList.Replace("@Param2@", "" + (Convert.ToInt64(lstGroup2.SelectedValue) != 0 ? lstGroup2.SelectedValue.ToString() : Param2Value));
                                ItemList = ItemList.Replace("@Param2NULL@", "" + (Convert.ToInt64(lstGroup2.SelectedValue) != 0 ? lstGroup2.SelectedValue.ToString() : "NULL"));
                                break;
                        }
                        break;
                    case 3:
                        ItemList = ItemList.Replace("@Param1@", "" + (Convert.ToInt64(lstGroup1.SelectedValue) != 0 ? lstGroup1.SelectedValue.ToString() : Param1Value));
                        ItemList = ItemList.Replace("@Param2@", "" + (Convert.ToInt64(lstGroup2.SelectedValue) != 0 ? lstGroup2.SelectedValue.ToString() : Param2Value));
                        ItemList = ItemList.Replace("@Param1NULL@", "" + (Convert.ToInt64(lstGroup1.SelectedValue) != 0 ? lstGroup1.SelectedValue.ToString() : "NULL"));
                        ItemList = ItemList.Replace("@Param2NULL@", "" + (Convert.ToInt64(lstGroup2.SelectedValue) != 0 ? lstGroup2.SelectedValue.ToString() : "NULL"));
                        break;
                }
                ItemList = ItemList.Replace("@CompNo@", "" + DBGetVal.CompanyNo);
                ItemList = ItemList.Replace("MRateSetting.PurRate AS SaleRate", "Isnull((Select Rate FRom Tstock Where PKStockTrnNo in (Select Max(PKStockTrnNo) FRom Tstock,TVoucherEntry Where TVoucherEntry.PKVoucherNo=TStock.FKVoucherNo AND TVoucherEntry.VoucherTypeCode=" + VchType.Purchase + " AND FKRateSettingNo in(MRateSetting.PkSrNo))),0) AS SaleRate");
                //ItemList = ItemList.Replace("TStockGodown.GodownNo=2", "TStockGodown.GodownNo=" + ObjFunction.GetComboValue(cmbLocation) + "");
                ItemList = ItemList.Replace("@GodownNo@", "MStockItems.GodownNo");
                switch (strItemQuery.Length - qNo)
                {
                    case 0:
                        if (!ItemList.Equals(strItemQuery_last[qNo - 1], StringComparison.CurrentCultureIgnoreCase))
                        {
                            ItemList = ItemList.Replace("AS ItemName,", "AS ItemName,Case When(MStockItems.LangShortDesc<>'') then MStockItems.LangShortDesc else MStockItems.LangFullDesc end AS ItemNameLang,");
                            DataTable dtItemList = ObjFunction.GetDataView(ItemList).Table;
                            if (dtItemList.Rows.Count > 0)
                            {
                                dgItemList.DataSource = dtItemList.DefaultView;
                                pnlItemName.Visible = true;
                                dgItemList.CurrentCell = dgItemList[1, 0];
                                dgItemList.Focus();
                            }
                            else
                            {
                                DisplayMessage("Items Not Found......");
                                dgBill.CurrentCell = dgBill[dgBill.CurrentCell.ColumnIndex, dgBill.CurrentCell.RowIndex];
                                dgBill.Focus();
                            }
                        }
                        else
                        {
                            pnlItemName.Visible = true;
                            dgItemList.CurrentCell = dgItemList[1, 0];
                            dgItemList.Focus();
                        }
                        break;
                    case 1:
                        BrandFilter = ItemList;
                        ObjFunction.FillList(lstGroup1, ItemList);
                        strItemQuery_last[qNo - 1] = ItemList;
                        //ObjFunction.FillList(lstGroup1, ItemList);
                        //strItemQuery_last[qNo - 1] = ItemList;
                        //}
                        if (lstGroup1.Items.Count > 0)
                        {
                            pnlGroup1.Visible = true;
                            txtBrandFilter.Focus();
                        }
                        else
                        {
                            DisplayMessage("Brands Not Found......");
                            dgBill.CurrentCell = dgBill[dgBill.CurrentCell.ColumnIndex, dgBill.CurrentCell.RowIndex];
                            dgBill.Focus();
                        }
                        break;
                    case 2:
                        if (!ItemList.Equals(strItemQuery_last[qNo - 1], StringComparison.CurrentCultureIgnoreCase))
                        {
                            ObjFunction.FillList(lstGroup2, ItemList);
                            strItemQuery_last[qNo - 1] = ItemList;
                        }
                        pnlGroup2.Visible = true;
                        lstGroup2.Focus();
                        break;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void FillItemList()
        {
            try
            {
                string ItemList = "";
                if (ItemType == 3)
                {
                    ItemList = " SELECT MStockItems.ItemNo, MStockItems.ItemName,MStockItems.ItemNameLang, MRateSetting." + ObjFunction.GetComboValueString(cmbRateType) + " AS SaleRate, MUOM.UOMName, MRateSetting.MRP, " +
                        " (SELECT  ISNULL(SUM(CASE WHEN trncode = 1 THEN quantity ELSE - quantity END), 0) +  ISNULL(SUM(CASE WHEN trncode = 1 THEN FreeQty ELSE - FreeQty END), 0)   FROM TStock  INNER JOIN TStockGodown ON TSTock.PKStockTrnNo=TStockGodown.FKStockTrnNo   WHERE  TStockGodown.GodownNo=MStockItems.GodownNo AND    (FkRateSettingNo IN  (SELECT PkSrNo  FROM MRateSetting AS MRateSetting_1  WHERE (TStock.ItemNo = MStockItems.ItemNo) AND (MRP = MRateSetting.MRP))) AND (TStock.ItemNo = MStockItems.ItemNo) And PkStockTrnNo Not In (SELECT TStock.PkStockTrnNo FROM TVoucherEntry INNER JOIN  TStock ON TVoucherEntry.PkVoucherNo = TStock.FKVoucherNo WHERE (TVoucherEntry.IsCancel = 'True') AND (TStock.ItemNo = MStockItems.ItemNo))) AS Stock, '' AS stkUOM, 0 AS SaleTax, 0 AS PurTax, " + //MItemTaxInfo_Sale.Percentage AS SaleTax , MItemTaxInfo_Pur.Percentage
                        " MStockItems.CompanyNo, MStockBarcode.Barcode, MRateSetting.PkSrNo As RateSettingNo, MStockItems.UOMDefault,MRateSetting.PurRate " +
                        " FROM MStockItems_V(NULL,NULL,NULL,NULL,NULL,NULL,NULL) AS MStockItems INNER JOIN " +
                        " dbo.GetItemRateAll(NULL, NULL, NULL, NULL, NULL,NULL) AS MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo AND  " +
                        " MStockItems.UOMDefault = MRateSetting.UOMNo INNER JOIN " +
                        " MStockBarcode ON MRateSetting.FkBcdSrNo = MStockBarcode.PkStockBarcodeNo INNER JOIN " +
                        // " dbo.GetItemTaxAll(NULL, NULL, 10," + ObjFunction.GetComboValue(cmbTaxType) + ",NULL) AS MItemTaxInfo_Sale ON MStockItems.ItemNo = MItemTaxInfo_Sale.ItemNo INNER JOIN " +
                        // " dbo.GetItemTaxAll(NULL, NULL, 11," + ObjFunction.GetComboValue(cmbTaxType) + ",NULL) AS MItemTaxInfo_Pur ON MStockItems.ItemNo = MItemTaxInfo_Pur.ItemNo INNER JOIN " +
                        " MUOM ON MStockItems.UOMDefault = MUOM.UOMNo  Where MStockItems.ItemNo in " +
                        "(Select ItemNo from MStockBarcode where Barcode = '" + dgBill.CurrentCell.Value + "' And IsActive ='true') AND MStockItems.CompanyNo=" + DBGetVal.CompanyNo + " AND MStockItems.IsActive='true' and MStockItems.FkStockGroupTypeNo<>3" +
                        " ORDER BY MStockItems.ItemName";
                    DataTable dtItemList = ObjFunction.GetDataView(ItemList).Table;
                    if (dtItemList.Rows.Count > 0)
                    {
                        dgItemList.DataSource = dtItemList.DefaultView;
                        pnlItemName.Visible = true;
                        dgItemList.CurrentCell = dgItemList[1, 0];
                        dgItemList.Focus();
                    }
                    else
                    {
                        DisplayMessage("Items Not Found......");
                        dgBill.CurrentCell = dgBill[dgBill.CurrentCell.ColumnIndex, dgBill.CurrentCell.RowIndex];
                        dgBill.Focus();
                    }
                }
                else if (ItemNameType == 1)
                {
                    ItemList = " SELECT MStockItems.ItemNo, MStockItems.ItemName,MStockItems.ItemNameLang, MRateSetting." + ObjFunction.GetComboValueString(cmbRateType) + " AS SaleRate, MUOM.UOMName, MRateSetting.MRP, " +
                            " (SELECT  ISNULL(SUM(CASE WHEN trncode = 1 THEN quantity ELSE - quantity END), 0) +  ISNULL(SUM(CASE WHEN trncode = 1 THEN FreeQty ELSE - FreeQty END), 0)   FROM TStock  INNER JOIN TStockGodown ON TSTock.PKStockTrnNo=TStockGodown.FKStockTrnNo   WHERE  TStockGodown.GodownNo=MStockItems.GodownNo AND    (FkRateSettingNo IN  (SELECT PkSrNo  FROM MRateSetting AS MRateSetting_1  WHERE (TStock.ItemNo = MStockItems.ItemNo) AND (MRP = MRateSetting.MRP))) AND (TStock.ItemNo = MStockItems.ItemNo) And PkStockTrnNo Not In (SELECT TStock.PkStockTrnNo FROM TVoucherEntry INNER JOIN  TStock ON TVoucherEntry.PkVoucherNo = TStock.FKVoucherNo WHERE (TVoucherEntry.IsCancel = 'True') AND (TStock.ItemNo = MStockItems.ItemNo))) AS Stock, '' AS stkUOM, 0 AS SaleTax, 0 AS PurTax, " +//MItemTaxInfo_Sale.Percentage,MItemTaxInfo_Pur.Percentage
                            " MStockItems.CompanyNo, MStockBarcode.Barcode, MRateSetting.PkSrNo As RateSettingNo, MStockItems.UOMDefault,MRateSetting.PurRate " +
                            " FROM MStockItems INNER JOIN " +
                            " dbo.GetItemRateAll(NULL, NULL, NULL, NULL, NULL, NULL) AS MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo AND  " +
                            " MStockItems.UOMDefault = MRateSetting.UOMNo INNER JOIN " +
                            " MStockBarcode ON MRateSetting.FkBcdSrNo = MStockBarcode.PkStockBarcodeNo INNER JOIN " +
                        //" dbo.GetItemTaxAll(NULL, NULL, 10," + ObjFunction.GetComboValue(cmbTaxType) + ", NULL) AS MItemTaxInfo_Sale ON MStockItems.ItemNo = MItemTaxInfo_Sale.ItemNo INNER JOIN " +
                        //" dbo.GetItemTaxAll(NULL, NULL, 11," + ObjFunction.GetComboValue(cmbTaxType) + ", NULL) AS MItemTaxInfo_Pur ON MStockItems.ItemNo = MItemTaxInfo_Pur.ItemNo INNER JOIN " +
                            " MUOM ON MStockItems.UOMDefault = MUOM.UOMNo Where MStockItems.ItemNo <> 1 AND MStockItems.CompanyNo=" + DBGetVal.CompanyNo + " and MStockItems.FkStockGroupTypeNo<>3 " +
                            " ORDER BY MStockItems.ItemName";
                    DataTable dtItemList = ObjFunction.GetDataView(ItemList).Table;
                    if (dtItemList.Rows.Count > 0)
                    {
                        dgItemList.DataSource = dtItemList.DefaultView;
                        pnlItemName.Visible = true;
                        dgItemList.CurrentCell = dgItemList[1, 0];
                        dgItemList.Focus();
                    }
                    else
                    {
                        DisplayMessage("Items Not Found......");
                        dgBill.CurrentCell = dgBill[dgBill.CurrentCell.ColumnIndex, dgBill.CurrentCell.RowIndex];
                        dgBill.Focus();
                    }

                }
                else if (ItemNameType == 2)
                {
                    ItemList = "Select StockGroupNo,StockGroupName from MStockGroup where StockGroupNo in (select Distinct GroupNo from MStockItems WHERE ItemNo<>1 AND CompanyNo=" + DBGetVal.CompanyNo + ") order by StockGroupName";
                    ObjFunction.FillList(lstGroup1, ItemList);
                    pnlGroup1.Visible = true;
                    lstGroup1.Focus();
                    //lstGroup1.Location = new Point(dgBill.Left + dgBill.CurrentCell.ContentBounds.Left, dgBill.Top + dgBill.CurrentRow.Height + dgBill.CurrentCell.ContentBounds.Top + dgBill.ColumnHeadersHeight);
                }
                else if (ItemNameType == 3)
                {
                    ItemList = "Select StockGroupNo,StockGroupName from MStockGroup where StockGroupNo in (select Distinct GroupNo1 from MStockItems WHERE ItemNo<>1 AND CompanyNo=" + DBGetVal.CompanyNo + ") order by StockGroupName";
                    ObjFunction.FillList(lstGroup2, ItemList);
                    pnlGroup2.Visible = true;
                    lstGroup2.Focus();
                }
                else if (ItemNameType == 4)
                {
                    ItemList = "Select StockGroupNo,StockGroupName from MStockGroup where StockGroupNo in (select Distinct GroupNo from MStockItems WHERE ItemNo<>1 AND CompanyNo=" + DBGetVal.CompanyNo + ") order by StockGroupName";
                    ObjFunction.FillList(lstGroup1, ItemList);
                    pnlGroup1.Visible = true;
                    lstGroup1.Focus();
                }
                dgBill.Enabled = false;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void lstUOM_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                UOM_MoveNext();
            }
            else if (e.KeyChar == ' ')
            {
                dgBill.Focus();
                pnlUOM.Visible = false;
            }
        }

        private void lstGroup1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //string ItemListStr = "";
            //if (e.KeyChar == 13)
            //{
            //    pnlGroup1.Visible = false;

            //    FillItemList(strItemQuery.Length);
            //}
            //else if (e.KeyChar == ' ')
            //{
            //    dgBill.Focus();
            //    pnlGroup1.Visible = false;
            //}
        }

        private void lstGroup2_KeyPress(object sender, KeyPressEventArgs e)
        {
            //string ItemListStr = "";
            //if (e.KeyChar == 13)
            //{
            //    pnlGroup2.Visible = false;

            //    FillItemList(strItemQuery.Length - 1);
            //}
            //else if (e.KeyChar == ' ')
            //{
            //    dgBill.Focus();
            //    pnlGroup2.Visible = false;
            //}
        }

        private void lstRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                dgBill.CurrentRow.Cells[ColIndex.Rate].Value = lstRate.Text;
                dgBill.CurrentRow.Cells[ColIndex.PkRateSettingNo].Value = lstRate.SelectedValue;
                Rate_MoveNext();
                pnlRate.Visible = false;
            }
            else if (e.KeyChar == ' ')
            {
                dgBill.Focus();
                pnlRate.Visible = false;
            }
        }

        private void FillUOMList(int RowIndex)
        {
            try
            {
                ObjFunction.FillList(lstUOM, "UomNo", "UomName");

                if (ItemType == 2)
                    strUom = " Select * From GetUomList ('" + Convert.ToString(dgBill.Rows[RowIndex].Cells[ColIndex.Barcode].Value) + "',0," + Convert.ToDouble(dgBill.Rows[RowIndex].Cells[ColIndex.Quantity].Value) + ")";
                else
                    strUom = " Select * From GetUomList (''," + Convert.ToInt64(dgBill.Rows[RowIndex].Cells[ColIndex.ItemNo].Value) + "," + Convert.ToDouble(dgBill.Rows[RowIndex].Cells[ColIndex.Quantity].Value) + ")";

                ObjFunction.FillList(lstUOM, strUom);

                if (lstUOM.Items.Count <= 1)
                {
                    //dgBill.Rows[RowIndex].Cells[3].Value = lstUOM.Text;
                    //dgBill.Rows[RowIndex].Cells[ColIndex.UOMNo].Value = Convert.ToInt64(lstUOM.SelectedValue);
                    if (lstUOM.Items.Count == 1)
                    {
                        lstUOM.SelectedIndex = 0;
                        UOM_MoveNext();
                    }
                }
                else
                {
                    CalculateTotal();
                    pnlUOM.Visible = true;
                    lstUOM.Focus();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #endregion

        private void lst_VisibleChanged(object sender, EventArgs e)
        {
            if (((System.Windows.Forms.Control)sender).Visible == true)
                dgBill.Enabled = false;
            else
            {
                dgBill.Enabled = true;
                dgBill.Focus();
            }
            if (((System.Windows.Forms.Control)sender).Name == "pnlItemName")
            {
                if (((System.Windows.Forms.Control)sender).Visible == false)
                    pnlSalePurHistory.Visible = ((System.Windows.Forms.Control)sender).Visible;
            }
        }

        private void dgBill_CurrentCellChanged(object sender, EventArgs e)
        {
            if (dgBill.CurrentCell != null)
            {
                for (int i = 0; i < dgBill.Rows.Count; i++)
                {
                    dgBill.Rows[i].DefaultCellStyle.BackColor = Color.White;
                }
                dgBill.Rows[dgBill.CurrentCell.RowIndex].DefaultCellStyle.BackColor = clrColorRow;
                dgBill.CurrentCell.Style.SelectionBackColor = Color.Yellow;
            }
        }

        #region ColumnIndex
        private static class ColIndex
        {
            public static int SrNo = 0;
            public static int ItemName = 1;
            public static int Quantity = 2;
            public static int UOM = 3;
            public static int Rate = 4;
            public static int MRP = 5;
            public static int NetRate = 6;
            public static int FreeQty = 7;
            public static int FreeUOM = 8;
            public static int DiscPercentage = 9;
            public static int DiscAmount = 10;
            public static int DiscRupees = 11;
            public static int DiscPercentage2 = 12;
            public static int DiscAmount2 = 13;
            public static int NetAmt = 14;
            public static int TaxPercentage = 15;
            public static int TaxAmount = 16;
            public static int DiscRupees2 = 17;
            public static int Amount = 18;
            public static int Barcode = 19;
            public static int PkStockTrnNo = 20;
            public static int PkBarCodeNo = 21;
            public static int PkVoucherNo = 22;
            public static int ItemNo = 23;
            public static int UOMNo = 24;
            public static int TaxLedgerNo = 25;
            public static int SalesLedgerNo = 26;
            public static int PkRateSettingNo = 27;
            public static int PkItemTaxInfo = 28;
            public static int StockFactor = 29;
            public static int ActualQty = 30;
            public static int MKTQuantity = 31;
            public static int SalesVchNo = 32;
            public static int TaxVchNo = 33;
            public static int StockCompanyNo = 34;
            public static int BarcodePrint = 35;
            public static int FreeUomNo = 36;
            public static int TempMRP = 37;
            public static int LandedRate = 38;
            public static int PONo = 39;
            public static int FkOtherStockTrnNo = 40;
            public static int PkStockDetailsNo = 41;
            public static int GodownNo = 42;
            public static int DiscountType = 43;
            public static int HSNCode = 44;
            public static int IGSTPercent = 45;
            public static int IGSTAmount = 46;
            public static int CGSTPercent = 47;
            public static int CGSTAmount = 48;
            public static int SGSTPercent = 49;
            public static int SGSTAmount = 50;
            public static int UTGSTPercent = 51;
            public static int UTGSTAmount = 52;
            public static int CessPercent = 53;
            public static int CessAmount = 54;

        }
        #endregion

        private long GetVoucherPK(string expression)
        {
            long strVal = 0;
            try
            {
                if (dtVchMainDetails.Rows.Count > 0)
                {
                    DataRow[] result = dtVchMainDetails.Select(expression);
                    strVal = Convert.ToInt64(result[0].ItemArray[0].ToString());
                }
            }
            catch (Exception e)
            {
                strVal = 0;
                CommonFunctions.ErrorMessge = e.Message;
            }
            return strVal;
        }

        #region Receipt Grid Methods
        private void BindGridPayType(long ID)
        {
            DataTable dtPayType = new DataTable();
            dtPayLedger = ObjFunction.GetDataView("Select * From MPayTypeLedger Where PayTypeNo in(2,3,4,5)").Table;
            string sqlQuery = "";
            if (ID == 0)
                sqlQuery = "SELECT PayTypeName, PKPayTypeNo, Cast(0.00 as varchar) AS Amount,0 As LedgerNo, 0 AS PKVoucherPayTypeNo FROM MPayType ORDER BY PKPayTypeNo";
            else
                sqlQuery = "SELECT PayTypeName, PKPayTypeNo,Cast( IsNull((SELECT SUM(Amount) FROM TVoucherPayTypeDetails WHERE (FKSalesVoucherNo = " + ID + ") AND (FKPayTypeNo = PKPayTypeNo)),0) AS varchar) AS Amount,0 As LedgerNo, 0 AS PKVoucherPayTypeNo FROM MPayType  ORDER BY PKPayTypeNo";

            dtPayType = ObjFunction.GetDataView(sqlQuery).Table;
            while (dgPayType.Columns.Count > 0)
                dgPayType.Columns.RemoveAt(0);
            dgPayType.DataSource = dtPayType.DefaultView;
            for (int i = 0; i < dgPayType.Columns.Count; i++)
                dgPayType.Columns[i].Visible = false;
            dgPayType.Columns[0].Visible = true;
            dgPayType.Columns[2].Visible = true;
            dgPayType.Rows[0].Visible = false;
            dgPayType.Columns[0].Width = 150;
            dgPayType.Columns[2].Width = 100;
            dgPayType.Columns[0].ReadOnly = true;
            dgPayType.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgPayType.Rows[0].Visible = false;
            if (ID != 0)
            {
                if (ObjFunction.GetComboValue(cmbPaymentType) == 1 || ObjFunction.GetComboValue(cmbPaymentType) == 4 || ObjFunction.GetComboValue(cmbPaymentType) == 5)
                {
                    dgPayType.Rows[3].Cells[2].Value = ObjQry.ReturnDouble("Select Sum(Amount) From TVoucherChqCreditDetails Where FKVoucherNo=" + ID + "  AND ChequeNo <>''", CommonFunctions.ConStr).ToString("0.00");
                    dgPayType.Rows[4].Cells[2].Value = ObjQry.ReturnDouble("Select Sum(Amount) From TVoucherChqCreditDetails Where FKVoucherNo=" + ID + "  AND CreditCardNo <>''", CommonFunctions.ConStr).ToString("0.00");
                }

                double RefAmt = ObjQry.ReturnDouble("Select Sum(Amount) From TVoucherRefDetails Where FKVucherTrnNo in (Select PKVoucherTrnNo From TVoucherDetails Where FkVoucherNo=" + ID + ")", CommonFunctions.ConStr);
                if (RefAmt > 0)
                {
                    dgPayType.Rows[2].Cells[2].Value = RefAmt;
                }
            }
            CaluculatePayType();
            pnlPartial.Visible = false;

        }

        private void dgPayType_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                if (dgPayType.CurrentCell.Value == null) dgPayType.CurrentCell.Value = "0";
                if (ObjFunction.CheckValidAmount(dgPayType.CurrentCell.Value.ToString()) == false)
                {
                    dgPayType.CurrentCell.Value = "0.00";
                    OMMessageBox.Show("Please Enter valid amount..", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    MovetoNext move2n = new MovetoNext(m2n);
                    BeginInvoke(move2n, new object[] { dgPayType.CurrentCell.RowIndex, 2, dgPayType });
                }
                else
                {
                    dgPayType.CurrentCell.ErrorText = "";
                    dgPayType.CurrentCell.Value = Convert.ToDouble(dgPayType.CurrentCell.Value).ToString("0.00");
                    CaluculatePayType();

                    if (dgPayType.CurrentCell.RowIndex == 3)
                    {
                        if (Convert.ToDouble(dgPayType.Rows[3].Cells[2].Value) <= 0)
                        {
                            dgPayChqDetails.Rows.Clear();
                            dgPayChqDetails.Rows.Add();
                        }
                        else
                        {
                            pnlPartial.Size = new Size(775, 214);
                            pnlPartial.Location = new Point(20, 123);
                            dgPayChqDetails.Visible = true;
                            dgPayChqDetails.BringToFront();
                            dgPayChqDetails.Focus();
                            dgPayCreditCardDetails.Visible = false;
                            if (dgPayChqDetails.Rows.Count == 0)
                            {
                                dgPayChqDetails.Rows.Add();
                                dgPayChqDetails.CurrentCell = dgPayChqDetails[0, 0];
                            }
                        }
                    }
                    if (dgPayType.CurrentCell.RowIndex == 4)
                    {
                        if (Convert.ToDouble(dgPayType.Rows[4].Cells[2].Value) <= 0)
                        {
                            dgPayCreditCardDetails.Rows.Clear();
                            dgPayCreditCardDetails.Rows.Add();
                        }
                        else
                        {
                            pnlPartial.Size = new Size(775, 214);
                            pnlPartial.Location = new Point(20, 123);
                            dgPayCreditCardDetails.Visible = true;
                            dgPayCreditCardDetails.Focus();
                            dgPayCreditCardDetails.BringToFront();
                            dgPayChqDetails.Visible = false;
                            if (dgPayCreditCardDetails.Rows.Count == 0)
                            {
                                dgPayCreditCardDetails.Rows.Add();
                                dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[0, 0];
                            }
                        }
                    }
                }
            }
        }

        private void dgPayType_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (dgPayType.CurrentCell.ColumnIndex == 2)
            {
                TextBox txt = (TextBox)e.Control;
                txt.KeyDown += new KeyEventHandler(txtAmt_KeyDown);
            }
        }

        private void txtAmt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                if (ObjFunction.CheckValidAmount(dgPayType.CurrentCell.Value.ToString()) == false)
                {
                    OMMessageBox.Show("Please Enter valid amount..", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    dgPayType.CurrentCell = dgPayType[2, dgPayType.CurrentCell.RowIndex];
                }
            }
        }

        private void CaluculatePayType()
        {
            double TotAmt = 0;
            for (int i = 0; i < dgPayType.Rows.Count; i++)
            {
                if (dgPayType.Rows[i].Cells[2].Value == null) dgPayType.Rows[i].Cells[2].Value = "0";
                TotAmt += Convert.ToDouble(dgPayType.Rows[i].Cells[2].Value);
            }
            txtTotalAmt.Text = TotAmt.ToString("0.00");
            if (txtGrandTotal.Text != "")
                lblPayTypeBal.Text = (Convert.ToDouble(txtGrandTotal.Text) - TotAmt).ToString("0.00");
            else
                lblPayTypeBal.Text = "0.00";
        }

        private void FillPayType()
        {
            int cntflag = 0;
            for (int i = 0; i < dgPayType.Rows.Count; i++)
            {
                if (Convert.ToDouble(dgPayType.Rows[i].Cells[2].Value) > 0)
                    cntflag += 1;
                if (cntflag > 1) break;
            }
            if (cntflag > 1) cmbPaymentType.SelectedValue = "1";

            long PayType = ObjFunction.GetComboValue(cmbPaymentType);
            //for (int i = 0; i < dtCompRatio.Rows.Count; i++)
            //{
            //    dgPayType.Columns.Add(dtCompRatio.Rows[i].ItemArray[0].ToString(), dtCompRatio.Rows[i].ItemArray[0].ToString());
            //    dgPayType.Columns[dgPayType.Columns.Count - 1].Visible = false;
            //}

            if (PayType != 1)
            {
                for (int i = 0; i < dgPayType.Rows.Count; i++)
                {
                    if (PayType == Convert.ToInt64(dgPayType.Rows[i].Cells[1].Value))
                    {
                        dgPayType.Rows[i].Cells[2].Value = txtGrandTotal.Text;
                        // for (int j = 0; j < dtCompRatio.Rows.Count; j++)
                        //{
                        dgPayType.Rows[i].Cells[4].Value = Convert.ToDouble(txtGrandTotal.Text);// * Convert.ToDouble(dtCompRatio.Rows[j].ItemArray[1].ToString())) / 10;
                        //}
                    }
                }
            }
            else
            {
                for (int i = 0; i < dgPayType.Rows.Count; i++)
                {
                    // for (int j = 0; j < dtCompRatio.Rows.Count; j++)
                    //{
                    dgPayType.Rows[i].Cells[4].Value = Convert.ToDouble(dgPayType.Rows[i].Cells[2].Value);// * Convert.ToDouble(dtCompRatio.Rows[j].ItemArray[1].ToString())) / 10;
                    //}
                }
            }
        }
        //public void FillPayType()
        //{
        //    //int cntflag = 0;
        //    //for (int i = 0; i < dgPayType.Rows.Count; i++)
        //    //{
        //    //    if (Convert.ToDouble(dgPayType.Rows[i].Cells[2].Value) > 0)
        //    //        cntflag += 1;
        //    //    if (cntflag > 1) break;
        //    //}
        //    //if (cntflag > 1) cmbPaymentType.SelectedValue = "1";

        //    //long PayType = ObjFunction.GetComboValue(cmbPaymentType);
        //    //for (int i = 0; i < dtCompRatio.Rows.Count; i++)
        //    //{
        //    //    dgPayType.Columns.Add(dtCompRatio.Rows[i].ItemArray[0].ToString(), dtCompRatio.Rows[i].ItemArray[0].ToString());
        //    //    dgPayType.Columns[dgPayType.Columns.Count - 1].Visible = false;
        //    //}

        //    //if (PayType != 1)
        //    //{
        //    //    for (int i = 0; i < dgPayType.Rows.Count; i++)
        //    //    {
        //    //        if (PayType == Convert.ToInt64(dgPayType.Rows[i].Cells[1].Value))
        //    //        {
        //    //            dgPayType.Rows[i].Cells[2].Value = txtGrandTotal.Text;
        //    //            for (int j = 0; j < dtCompRatio.Rows.Count; j++)
        //    //            {
        //    //                dgPayType.Rows[i].Cells[4 + (j + 1)].Value = (Convert.ToDouble(txtGrandTotal.Text) * Convert.ToDouble(dtCompRatio.Rows[j].ItemArray[1].ToString())) / 10;
        //    //            }
        //    //        }
        //    //    }
        //    //}
        //    //else
        //    //{
        //    //    for (int i = 0; i < dgPayType.Rows.Count; i++)
        //    //    {
        //    //        for (int j = 0; j < dtCompRatio.Rows.Count; j++)
        //    //        {
        //    //            dgPayType.Rows[i].Cells[4 + (j + 1)].Value = (Convert.ToDouble(dgPayType.Rows[i].Cells[2].Value) * Convert.ToDouble(dtCompRatio.Rows[j].ItemArray[1].ToString())) / 10;
        //    //        }
        //    //    }
        //    //}
        //}

        private void dgPayType_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btnOk_Click(sender, new EventArgs());
            }
            else if (e.KeyCode == Keys.D && e.Control)
            {
                if (dgPayType.CurrentCell.RowIndex == 3)
                {
                    if (Convert.ToDouble(dgPayType.Rows[dgPayType.CurrentCell.RowIndex].Cells[2].Value) > 0)
                    {
                        pnlPartial.Size = new Size(775, 214);
                        pnlPartial.Location = new Point(20, 123);
                        dgPayChqDetails.Visible = true;
                        dgPayChqDetails.BringToFront();
                        dgPayChqDetails.Focus();
                        dgPayCreditCardDetails.Visible = false;
                        if (dgPayChqDetails.Rows.Count == 0)
                        {
                            dgPayChqDetails.Rows.Add();
                            dgPayChqDetails.CurrentCell = dgPayChqDetails[0, 0];
                        }
                    }
                }
                else if (dgPayType.CurrentCell.RowIndex == 4)
                {
                    if (Convert.ToDouble(dgPayType.Rows[dgPayType.CurrentCell.RowIndex].Cells[2].Value) > 0)
                    {
                        pnlPartial.Size = new Size(775, 214);
                        pnlPartial.Location = new Point(20, 123);
                        dgPayCreditCardDetails.Visible = true;
                        dgPayCreditCardDetails.Focus();
                        dgPayCreditCardDetails.BringToFront();
                        dgPayChqDetails.Visible = false;
                        if (dgPayCreditCardDetails.Rows.Count == 0)
                        {
                            dgPayCreditCardDetails.Rows.Add();
                            dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[0, 0];
                        }
                    }
                }
            }
        }

        #endregion

        #region Datagrid ItemList Methods

        private void dgItemList_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    long i = Convert.ToInt64(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[0].Value);

                    dgBill.CurrentRow.Cells[ColIndex.UOMNo].Value = Convert.ToInt64(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[13].Value);
                    dgBill.CurrentRow.Cells[ColIndex.Rate].Value = Convert.ToDouble(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[3].Value).ToString("0.00");//lstRate.Text;
                    dgBill.CurrentRow.Cells[ColIndex.PkRateSettingNo].Value = Convert.ToInt64(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[12].Value);//lstRate.SelectedValue;
                    dgBill.CurrentRow.Cells[ColIndex.MRP].Value = Convert.ToDouble(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[5].Value).ToString("0.00");
                    dgBill.CurrentRow.Cells[ColIndex.TempMRP].Value = Convert.ToDouble(dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[5].Value).ToString("0.00");
                    dgBill.CurrentRow.Cells[ColIndex.UOM].Value = dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[4].Value;
                    dgBill.CurrentRow.Cells[ColIndex.GodownNo].Value = dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[15].Value;
                    dgBill.CurrentRow.Cells[ColIndex.DiscountType].Value = dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[16].Value;
                    pnlItemName.Visible = false;
                    Desc_MoveNext(i, 0);
                }
                else if (e.KeyCode == Keys.Space)
                {
                    pnlItemName.Visible = false;
                    if (strItemQuery.Length > 1)
                    {
                        pnlGroup1.Visible = true;
                        lstGroup1.Focus();
                    }
                    else
                    {
                        dgBill.Focus();
                    }
                }
                else if (e.KeyCode == Keys.F6)
                {

                    pnlSalePurHistory.Visible = !pnlSalePurHistory.Visible;
                    if (pnlSalePurHistory.Visible == true)
                    {

                        pnlSalePurHistory.Location = new Point(88, 235 + 88);
                        pnlSalePurHistory.Size = new Size(692, 287);
                        DataSet ds = ObjDset.FillDset("New", "Exec GetLastSalePurchaseDetails " + ObjFunction.GetComboValue(cmbPartyName) + "," + dgItemList.Rows[dgItemList.CurrentCell.RowIndex].Cells[0].Value.ToString() + " ", CommonFunctions.ConStr);

                        if (ds.Tables.Count > 0)
                        {
                            dgSaleHistory.DataSource = ds.Tables[0].DefaultView;
                            dgPurHistory.DataSource = ds.Tables[1].DefaultView;

                            if (dgSaleHistory.Rows.Count > 0)
                            {
                                dgSaleHistory.Location = new Point(8, 8);
                                dgSaleHistory.Size = new Size(323, 150);
                                dgSaleHistory.Columns[0].Width = 70;
                                dgSaleHistory.Columns[1].Width = 45;
                                dgSaleHistory.Columns[2].Width = 70; dgSaleHistory.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                                dgSaleHistory.Columns[3].Width = 50; dgSaleHistory.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                                dgSaleHistory.Columns[4].Width = 85;
                            }

                            if (dgPurHistory.Rows.Count > 0)
                            {
                                dgPurHistory.Location = new Point(337, 8);
                                dgPurHistory.Size = new Size(339, 150);
                                dgPurHistory.Columns[0].Width = 70;
                                dgPurHistory.Columns[1].Width = 45;
                                dgPurHistory.Columns[2].Width = 70; dgPurHistory.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                                dgPurHistory.Columns[3].Width = 50; dgPurHistory.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                                dgPurHistory.Columns[4].Width = 85;
                            }

                        }
                        else
                        {
                            dgSaleHistory.Rows.Clear();
                            dgPurHistory.Rows.Clear();
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgItemList_CurrentCellChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgItemList.CurrentCell != null)
                {
                    for (int i = 0; i < dgItemList.Rows.Count; i++)
                    {
                        dgItemList.Rows[i].DefaultCellStyle.BackColor = Color.White;
                    }
                    dgItemList.Rows[dgItemList.CurrentCell.RowIndex].DefaultCellStyle.BackColor = clrColorRow;
                    dgItemList.CurrentCell.Style.SelectionBackColor = Color.Yellow;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }
        #endregion

        #region Rate Type Realted Methods and Functions
        private void FillRateType()
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("RateType");
                dt.Columns.Add("RateTypeName");
                DataRow dr = null;

                //dr = dt.NewRow();
                //dr[0] = "----Select----";
                //dr[1] = "0";
                //dt.Rows.Add(dr);

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ARateIsActive)) == true)
                {
                    dr = dt.NewRow();
                    dr[1] = ObjFunction.GetAppSettings(AppSettings.ARateLabel);
                    dr[0] = "ASaleRate";
                    dt.Rows.Add(dr);
                }

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.BRateIsActive)) == true)
                {
                    dr = dt.NewRow();
                    dr[1] = ObjFunction.GetAppSettings(AppSettings.BRateLabel);
                    dr[0] = "BSaleRate";
                    dt.Rows.Add(dr);
                }

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.CRateIsActive)) == true)
                {
                    dr = dt.NewRow();
                    dr[1] = ObjFunction.GetAppSettings(AppSettings.CRateLabel);
                    dr[0] = "CSaleRate";
                    dt.Rows.Add(dr);
                }

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.DRateIsActive)) == true)
                {
                    dr = dt.NewRow();
                    dr[1] = ObjFunction.GetAppSettings(AppSettings.DRateLabel);
                    dr[0] = "DSaleRate";
                    dt.Rows.Add(dr);
                }

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ERateIsActive)) == true)
                {
                    dr = dt.NewRow();
                    dr[1] = ObjFunction.GetAppSettings(AppSettings.ERateLabel);
                    dr[0] = "ESaleRate";
                    dt.Rows.Add(dr);
                }

                //For Purchase Rate
                dr = dt.NewRow();
                dr[1] = "Purchase Rate";
                dr[0] = "PurRate";
                dt.Rows.Add(dr);

                cmbRateType.DataSource = dt.DefaultView;
                cmbRateType.DisplayMember = dt.Columns[1].ColumnName;
                cmbRateType.ValueMember = dt.Columns[0].ColumnName;
                SetRateType(Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_RateType).ToString()));
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private int GetRateType()
        {
            string str = ObjFunction.GetComboValueString(cmbRateType);
            int val = 0;
            if (str == "ASaleRate") val = 1;
            else if (str == "BSaleRate") val = 2;
            else if (str == "CSaleRate") val = 3;
            else if (str == "DSaleRate") val = 4;
            else if (str == "ESaleRate") val = 5;
            else if (str == "PurRate") val = 6;
            return val;
        }

        private void SetRateType(long RateTypeNo)
        {
            if (RateTypeNo == 1) cmbRateType.SelectedValue = "ASaleRate";
            else if (RateTypeNo == 2) cmbRateType.SelectedValue = "BSaleRate";
            else if (RateTypeNo == 3) cmbRateType.SelectedValue = "CSaleRate";
            else if (RateTypeNo == 4) cmbRateType.SelectedValue = "DSaleRate";
            else if (RateTypeNo == 5) cmbRateType.SelectedValue = "ESaleRate";
            else if (RateTypeNo == 6) cmbRateType.SelectedValue = "PurRate";
        }
        #endregion

        private void PrintBill()
        {
            //string[] ReportSession;

            //ReportSession = new string[2];
            //ReportSession[0] = ID.ToString();
            //ReportSession[1] = "";//ObjQry.ReturnLong("Select Max(PkVoucherNo) FRom TVoucherEntry Where VoucherTypeCode=" + ((flagPP == true) ? VchType.Purchase : VoucherType) + "", CommonFunctions.ConStr).ToString();
            //CrystalDecisions.CrystalReports.Engine.ReportClass childForm = ObjFunction.GetReportObject("Reports.GetBill");
            //if (childForm != null)
            //{
            //    DBReportGenerate objRpt = new DBReportGenerate(childForm, ReportSession);
            //    if (objRpt.PrintReport() == true)
            //    {
            //        DisplayMessage("Bill Print Successfully!!!");
            //    }
            //    else
            //    {
            //        DisplayMessage("Bill not Print !!!");
            //    }
            //}
            //else
            //{
            //    DisplayMessage("Bill Report not exist !!!");
            //}
        }

        private void dtpBillDate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnParty)) == true) cmbPartyName.Focus();
                //else if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnHeaderDisc)) == true) txtOtherDisc.Focus();
                //else if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnTaxType)) == true) cmbTaxType.Focus();
                //else dgBill.Focus();
                txtRefNo.Focus();
                e.SuppressKeyPress = true;
            }
        }

        private void cmbPartyName_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (ObjFunction.GetComboValue(cmbPartyName) == 0)
                    {
                        DisplayMessage("Select Party Name");
                        cmbPartyName.Focus();
                        //cmbPartyName.SelectedValue = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.S_PartyAC));
                    }
                    else
                    {
                        //if (ObjFunction.GetComboValue(cmbPartyName) == Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_PartyAC)))
                        //{
                        //    long ControlUnder = ObjQry.ReturnLong("Select ControlUnder From MPayType Where PKPayTypeNo=" + ObjFunction.GetComboValue(cmbPaymentType) + "", CommonFunctions.ConStr);
                        //    if (ControlUnder == 3)
                        //    {
                        //        OMMessageBox.Show("This PayType is Not Valid For Current party .. Select other PayType...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        //        cmbPaymentType.Focus();
                        //        return;
                        //    }
                        //    //if (ID == 0)
                        //    //{
                        //    //    cmbPaymentType.SelectedValue = 2;
                        //    //    //cmbPaymentType.Enabled = false;
                        //    //}
                        //}
                        //else
                        //    cmbPaymentType.Enabled = true;
                        ObjFunction.GetFinancialYear(dtpBillDate.Value, out dtFrom, out dtTo);
                        if (ObjQry.ReturnLong(" Select Count(*) FROM  TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo " +
                                             " where TVoucherEntry.Reference ='" + txtRefNo.Text.Trim() + "' and TVoucherEntry.VoucherTypeCode=" + VchType.Purchase + " " +
                                             " and TVoucherDetails.LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " and TVoucherDetails.SrNo=501" +
                                             " AND TVoucherEntry.VoucherDate>='" + dtFrom.Date + "' AND TVoucherEntry.VoucherDate<='" + dtTo.Date + "' " +
                                             " and TVoucherEntry.PkVoucherNo!=" + ID + "", CommonFunctions.ConStr) > 0)//TVoucherEntry.VoucherDate='" + Convert.ToDateTime(dtpBillDate.Text).ToString("dd-MMM-yyyy") + "' and 
                        {

                            OMMessageBox.Show("This Invoice is already exist ....", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            cmbPartyName.Focus();
                        }
                        else
                        {
                            dgBill.Focus();
                            dgBill.CurrentCell = dgBill[ColIndex.ItemName, dgBill.Rows.Count - 1];
                        }

                    }
                    //if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnTaxType)) == true) cmbTaxType.Focus();
                    //else 
                    //dgBill.Focus();
                    //dgBill.CurrentCell = dgBill[1, 0];
                    e.SuppressKeyPress = true;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbRateType_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnTaxType)) == true) cmbTaxType.Focus();
                else dgBill.Focus();

                e.SuppressKeyPress = true;
            }
        }

        private void cmbTaxType_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                dgBill.Focus();
                dgBill.CurrentCell = dgBill[1, 0];
                e.SuppressKeyPress = true;
            }
        }

        private void lstGroup1_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (lstGroup1.Items.Count <= 0)
                {
                    dgBill.Focus();
                    pnlGroup1.Visible = false;
                }
                if (e.KeyCode == Keys.F9)
                {
                    pnlGroup1.Visible = false;
                    FillItemList(1);
                }
                //string ItemListStr = "";
                if (e.KeyCode == Keys.Enter)
                {

                    e.SuppressKeyPress = true;
                    pnlGroup1.Visible = false;

                    FillItemList(strItemQuery.Length);
                }
                else if (e.KeyCode == Keys.Space)
                {
                    dgBill.Focus();
                    pnlGroup1.Visible = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbRateType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeBillRate();
        }

        private void ChangeBillRate()
        {
            try
            {
                for (int i = 0; i < dgBill.Rows.Count; i++)
                {
                    if (dgBill.Rows[i].Cells[ColIndex.PkRateSettingNo].Value != null)
                    {
                        dgBill.Rows[i].Cells[ColIndex.Rate].Value = ObjQry.ReturnDouble("Select " + ObjFunction.GetComboValueString(cmbRateType) + " From MRateSetting Where PkSrNo=" + dgBill.Rows[i].Cells[ColIndex.PkRateSettingNo].Value + "", CommonFunctions.ConStr);
                    }
                }
                CalculateTotal();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #region Cheque AND Credit grid Related Methods
        private void BindPayChequeDetails(long VchNo)
        {
            try
            {
                //string strQuery = "SELECT  TVoucherChqCreditDetails.ChequeNo, TVoucherChqCreditDetails.ChequeDate, IsNull(MOtherBank.BankName,'') AS BankName, IsNull(MBranch.BranchName,'') AS BranchName,  " +
                //    " TVoucherChqCreditDetails.Amount, TVoucherChqCreditDetails.PkSrNo, TVoucherChqCreditDetails.BankNo, TVoucherChqCreditDetails.BranchNo FROM TVoucherChqCreditDetails LEFT OUTER JOIN " +
                //    " MBranch ON TVoucherChqCreditDetails.BranchNo = MBranch.BranchNo LEFT OUTER JOIN " +
                //    " MOtherBank ON TVoucherChqCreditDetails.BankNo = MOtherBank.BankNo Where TVoucherChqCreditDetails.ChequeNo <>'' AND TVoucherChqCreditDetails.FKVoucherNo=" + VchNo + "";
                string strQuery = " SELECT TVoucherChqCreditDetails.ChequeNo, TVoucherChqCreditDetails.ChequeDate, isNull(MLedger.LedgerName,'') AS BankName,''  AS BranchName, TVoucherChqCreditDetails.Amount,  " +
                                " TVoucherChqCreditDetails.PkSrNo, TVoucherChqCreditDetails.BankNo, TVoucherChqCreditDetails.BranchNo " +
                                " FROM TVoucherChqCreditDetails LEFT OUTER JOIN MLedger ON TVoucherChqCreditDetails.BankNo = MLedger.LedgerNo " +
                                " WHERE (TVoucherChqCreditDetails.ChequeNo <> '') AND (TVoucherChqCreditDetails.FKVoucherNo = " + VchNo + ")";
                dgPayChqDetails.Rows.Clear();
                DataTable dt = ObjFunction.GetDataView(strQuery).Table;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dgPayChqDetails.Rows.Add();
                    for (int k = 0; k < dt.Columns.Count; k++)
                    {
                        if (k == 1)
                            dgPayChqDetails.Rows[i].Cells[k].Value = Convert.ToDateTime(dt.Rows[i].ItemArray[k]).ToString("dd-MMM-yyyy");
                        else
                            dgPayChqDetails.Rows[i].Cells[k].Value = dt.Rows[i].ItemArray[k];
                    }
                }
                dgPayChqDetails.Rows.Add();
                dgPayChqDetails.Columns[0].Width = 69;
                dgPayChqDetails.Columns[1].Width = 83;
                dgPayChqDetails.Columns[2].Width = 240;
                //dgPayChqDetails.Columns[3].Width = 114;
                dgPayChqDetails.Columns[4].Width = 75;
                //dgPayChqDetails.Focus();
                //dgPayChqDetails.CurrentCell = dgPayChqDetails[0, dgPayChqDetails.Rows.Count - 1];
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgPayChqDetails_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    if (dgPayChqDetails.CurrentCell.ColumnIndex == 1)
                    {
                        dtpChqDate.Visible = true;
                        dtpChqDate.BringToFront();
                        dtpChqDate.Focus();
                    }
                    else if (dgPayChqDetails.CurrentCell.ColumnIndex == 2)
                    {

                        pnlBank.Visible = true;
                        pnlBank.BringToFront();
                        lstBank.Focus();
                    }
                    else if (dgPayChqDetails.CurrentCell.ColumnIndex == 3)
                    {
                        pnlBranch.Visible = true;
                        pnlBranch.BringToFront();
                        lstBranch.Focus();
                    }
                    else if (dgPayChqDetails.CurrentCell.ColumnIndex == 4)
                    {
                        double TotAmt = 0;
                        for (int i = 0; i < dgPayChqDetails.Rows.Count; i++)
                        {
                            if (dgPayChqDetails.Rows[0].Cells[4].Value != null)
                            {
                                TotAmt = TotAmt + Convert.ToDouble(dgPayChqDetails.Rows[i].Cells[4].Value);
                            }
                        }
                        txtTotalAmt.Text = TotAmt.ToString();
                        btnOk.Focus();
                    }
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    double Amt = 0;
                    if (dgPayChqDetails.Rows[0].Cells[0].EditedFormattedValue.ToString().Trim() != "")
                    {
                        if (dgPayChqDetails.Rows[0].Cells[1].FormattedValue.ToString().Trim() == "" || dgPayChqDetails.Rows[0].Cells[2].FormattedValue.ToString().Trim() == "")
                        {
                            OMMessageBox.Show("Please Fill Cheque Details.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            return;
                        }
                        for (int i = 0; i < dgPayChqDetails.Rows.Count; i++)
                        {
                            Amt += (dgPayChqDetails.Rows[i].Cells[4].Value == null) ? 0 : Convert.ToDouble(dgPayChqDetails.Rows[i].Cells[4].Value);
                        }

                        if (Convert.ToDouble(dgPayType.Rows[3].Cells[2].Value) != Amt)
                        {
                            OMMessageBox.Show("Please enter Cheque amount and Cheque Details amount are not same...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            dgPayChqDetails.Focus();
                        }
                        else
                        {
                            if (ObjFunction.GetComboValue(cmbPaymentType) == 1)
                            {
                                pnlPartial.Size = new Size(305, 214);
                                pnlPartial.Location = new Point(200, 123);
                                dgPayType.CurrentCell = dgPayType[2, 3];
                                dgPayType.Focus();
                            }
                            else
                            {
                                btnSave.Enabled = true;
                                btnSave.Focus();
                                pnlPartial.Visible = false;
                            }
                            //btnOk.Focus();
                        }
                        //double Amt = 0;
                        //if (dgPayChqDetails.Rows[0].Cells[0].Value != null)
                        //{
                        //    for (int i = 0; i < dgPayChqDetails.Rows.Count; i++)
                        //    {
                        //        Amt += (dgPayChqDetails.Rows[i].Cells[4].Value == null) ? 0 : Convert.ToDouble(dgPayChqDetails.Rows[i].Cells[4].Value);
                        //    }

                        //    if (Convert.ToDouble(dgPayType.Rows[3].Cells[2].Value) != Amt)
                        //    {
                        //        OMMessageBox.Show("Please enter Cheque amount and Cheque Details amount are not same...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        //        dgPayChqDetails.Focus();
                        //    }
                        //    else
                        //    {
                        //        pnlPartial.Size = new Size(305, 214);
                        //        pnlPartial.Location = new Point(200, 123);
                        //        dgPayType.CurrentCell = dgPayType[2, 3];
                        //        dgPayType.Focus();
                        //        //btnOk.Focus();
                        //    }
                    }
                    else
                    {
                        if (ObjFunction.GetComboValue(cmbPaymentType) == 1)
                        {
                            pnlPartial.Size = new Size(305, 214);
                            pnlPartial.Location = new Point(200, 123);
                            dgPayType.CurrentCell = dgPayType[2, 3];
                            dgPayType.Focus();
                        }
                        else
                        {
                            //btnSave.Enabled = true;
                            //btnSave.Focus();
                            cmbPaymentType.Focus();
                            pnlPartial.Visible = false;
                        }
                    }
                }
                else if (e.KeyCode == Keys.Delete)
                {
                    //if (dgPayChqDetails.CurrentCell.RowIndex != dgPayChqDetails.Rows.Count - 1)
                    //{
                    if (Convert.ToInt64(dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[5].Value) != 0)
                    {
                        DeleteDtls(5, Convert.ToInt64(dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[5].Value));
                    }
                    dgPayChqDetails.Rows.RemoveAt(dgPayChqDetails.CurrentCell.RowIndex);
                    if (dgPayChqDetails.Rows.Count == 0)
                        dgPayChqDetails.Rows.Add();
                    dgPayChqDetails.CurrentCell = dgPayChqDetails[0, dgPayChqDetails.Rows.Count - 1];
                    // }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dtpChqDate_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;

                    dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[1].Value = dtpChqDate.SelectionStart.ToString("dd-MMM-yy");
                    dtpChqDate.Visible = false;
                    if (dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[2].Value == null)
                    {

                        pnlBank.Location = new Point(dgPayChqDetails.CurrentCell.ContentBounds.X + 150 + dgPayChqDetails.Location.X, dgPayChqDetails.CurrentCell.ContentBounds.Y + 40);
                        pnlBank.Visible = true;
                        pnlBank.BringToFront();
                        lstBank.Focus();
                    }
                    else
                    {
                        dgPayChqDetails.Focus();
                        dgPayChqDetails.CurrentCell = dgPayChqDetails[2, dgPayChqDetails.CurrentCell.RowIndex];
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void lstBank_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    if (dgPayChqDetails.Visible == true)
                    {
                        dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[2].Value = lstBank.Text;
                        dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[6].Value = lstBank.SelectedValue;
                        pnlBank.Visible = false;
                        //if (dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[3].Value == null)
                        //{
                        //    pnlBranch.Visible = true;
                        //    pnlBranch.BringToFront();
                        //    lstBranch.Focus();
                        //}
                        //else
                        //{
                        dgPayChqDetails.Focus();
                        dgPayChqDetails.CurrentCell = dgPayChqDetails[4, dgPayChqDetails.CurrentCell.RowIndex];
                        //}
                    }
                    else if (dgPayCreditCardDetails.Visible == true)
                    {
                        dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[1].Value = lstBank.Text;
                        dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[5].Value = lstBank.SelectedValue;
                        pnlBank.Visible = false;
                        //if (dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[2].Value == null)
                        //{
                        //    pnlBranch.Location = new Point(dgPayCreditCardDetails.CurrentCell.ContentBounds.X + 235 + dgPayCreditCardDetails.Location.X, dgPayCreditCardDetails.CurrentCell.ContentBounds.Y + 40);
                        //    pnlBranch.Visible = true;
                        //    pnlBranch.BringToFront();
                        //    lstBranch.Focus();
                        //}
                        //else
                        //{
                        dgPayCreditCardDetails.Focus();
                        dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[3, dgPayCreditCardDetails.CurrentCell.RowIndex];
                        //}
                    }
                }
                else if (e.KeyCode == Keys.Space)
                {
                    if (dgPayChqDetails.Visible == true)
                    {
                        dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[2].Value = "";
                        dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[6].Value = 0;
                        pnlBank.Visible = false;
                        if (dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[3].Value == null)
                        {
                            pnlBranch.Visible = true;
                            pnlBranch.BringToFront();
                            lstBranch.Focus();
                        }
                        else
                        {
                            dgPayChqDetails.Focus();
                            dgPayChqDetails.CurrentCell = dgPayChqDetails[3, dgPayChqDetails.CurrentCell.RowIndex];
                        }
                    }
                    else if (dgPayCreditCardDetails.Visible == true)
                    {
                        dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[1].Value = "";
                        dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[5].Value = 0;
                        pnlBank.Visible = false;
                        if (dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[2].Value == null)
                        {
                            pnlBranch.Visible = true;
                            pnlBranch.BringToFront();
                            lstBranch.Focus();
                        }
                        else
                        {
                            dgPayCreditCardDetails.Focus();
                            dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[3, dgPayCreditCardDetails.CurrentCell.RowIndex];
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void lstBranch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    if (dgPayChqDetails.Visible == true)
                    {
                        dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[3].Value = lstBranch.Text;
                        dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[7].Value = lstBranch.SelectedValue;
                        pnlBranch.Visible = false;
                        dgPayChqDetails.Focus();
                        dgPayChqDetails.CurrentCell = dgPayChqDetails[4, dgPayChqDetails.CurrentCell.RowIndex];
                    }
                    else if (dgPayCreditCardDetails.Visible == true)
                    {
                        dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[2].Value = lstBranch.Text;
                        dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[6].Value = lstBranch.SelectedValue;
                        pnlBranch.Visible = false;
                        dgPayCreditCardDetails.Focus();
                        dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[3, dgPayCreditCardDetails.CurrentCell.RowIndex];
                    }
                }
                else if (e.KeyCode == Keys.Space)
                {
                    if (dgPayChqDetails.Visible == true)
                    {
                        dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[3].Value = "";
                        dgPayChqDetails.Rows[dgPayChqDetails.CurrentCell.RowIndex].Cells[7].Value = 0;
                        pnlBranch.Visible = false;
                        dgPayChqDetails.Focus();
                        dgPayChqDetails.CurrentCell = dgPayChqDetails[4, dgPayChqDetails.CurrentCell.RowIndex];
                    }
                    else if (dgPayCreditCardDetails.Visible == true)
                    {
                        dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[3].Value = "";
                        dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[6].Value = 0;
                        pnlBranch.Visible = false;
                        dgPayCreditCardDetails.Focus();
                        dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[3, dgPayCreditCardDetails.CurrentCell.RowIndex];
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgPayChqDetails_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 0)
                {
                    if (dgPayChqDetails.Rows[e.RowIndex].Cells[1].Value == null)
                    {
                        if (e.RowIndex == 0) dgPayChqDetails.Rows[e.RowIndex].Cells[4].Value = dgPayType.Rows[3].Cells[2].Value;
                        dtpChqDate.Location = new Point(dgPayChqDetails.CurrentCell.ContentBounds.X + 72 + dgPayChqDetails.Location.X, dgPayChqDetails.CurrentCell.ContentBounds.Y + 40);
                        dtpChqDate.Visible = true;
                        dtpChqDate.BringToFront();
                        dtpChqDate.Focus();
                    }
                    else
                    {
                        dgPayChqDetails.Focus();
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { dgPayChqDetails.CurrentCell.RowIndex, 1, dgPayChqDetails });
                    }
                }
                if (e.ColumnIndex == 4)
                {
                    if (dgPayChqDetails.Rows[e.RowIndex].Cells[4].Value != null)
                    {
                        dgPayChqDetails.Rows[e.RowIndex].Cells[4].ErrorText = "";
                        if (ObjFunction.CheckValidAmount(dgPayChqDetails.Rows[e.RowIndex].Cells[4].Value.ToString()) == false)
                        {
                            dgPayChqDetails.Rows[e.RowIndex].Cells[4].ErrorText = "Please Enter Valid Amount";
                        }
                        else
                        {
                            double TotAmt = 0;
                            dgPayChqDetails.Rows[e.RowIndex].Cells[4].ErrorText = "";
                            if (e.RowIndex == dgPayChqDetails.Rows.Count - 1 && dgPayChqDetails.Rows[e.RowIndex].Cells[1].Value != null)
                            {
                                for (int i = 0; i < dgPayChqDetails.Rows.Count; i++)
                                {
                                    if (dgPayChqDetails.Rows[e.RowIndex].Cells[4].Value != null)
                                    {
                                        TotAmt = TotAmt + Convert.ToDouble(dgPayChqDetails.Rows[i].Cells[4].Value);
                                    }
                                }
                                txtTotalAmt.Text = TotAmt.ToString();
                                dgPayChqDetails.Rows.Add();
                                dgPayChqDetails.Focus();
                                dgPayChqDetails.CurrentCell = dgPayChqDetails[0, dgPayChqDetails.Rows.Count - 1];
                            }
                            else
                            {
                                for (int i = 0; i < dgPayChqDetails.Rows.Count; i++)
                                {
                                    if (dgPayChqDetails.Rows[e.RowIndex].Cells[4].Value != null)
                                    {
                                        TotAmt = TotAmt + Convert.ToDouble(dgPayChqDetails.Rows[i].Cells[4].Value);
                                    }
                                }
                                txtTotalAmt.Text = TotAmt.ToString();
                                btnOk.Focus();
                            }
                        }
                    }
                    else
                    {
                        dgPayChqDetails.Rows[e.RowIndex].Cells[4].ErrorText = "Please Enter  Amount";
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void BindPayCreditDetails(long VchNo)
        {
            try
            {
                //string strQuery = "SELECT  TVoucherChqCreditDetails.CreditCardNo, IsNull(MOtherBank.BankName,'') AS BankName, IsNull(MBranch.BranchName,'') AS BranchName,  " +
                //    " TVoucherChqCreditDetails.Amount, TVoucherChqCreditDetails.PkSrNo, TVoucherChqCreditDetails.BankNo, TVoucherChqCreditDetails.BranchNo FROM TVoucherChqCreditDetails LEFT OUTER JOIN " +
                //    " MBranch ON TVoucherChqCreditDetails.BranchNo = MBranch.BranchNo LEFT OUTER JOIN " +
                //    " MOtherBank ON TVoucherChqCreditDetails.BankNo = MOtherBank.BankNo Where TVoucherChqCreditDetails.CreditCardNo <>'' AND TVoucherChqCreditDetails.FKVoucherNo=" + VchNo + "";
                string strQuery = " SELECT  TVoucherChqCreditDetails.CreditCardNo, isNull(MLedger.LedgerName,'') AS BankName,''  AS BranchName, TVoucherChqCreditDetails.Amount,  " +
                                " TVoucherChqCreditDetails.PkSrNo, TVoucherChqCreditDetails.BankNo, TVoucherChqCreditDetails.BranchNo " +
                                " FROM TVoucherChqCreditDetails LEFT OUTER JOIN MLedger ON TVoucherChqCreditDetails.BankNo = MLedger.LedgerNo " +
                                " WHERE     (TVoucherChqCreditDetails.CreditCardNo <> '') AND (TVoucherChqCreditDetails.FKVoucherNo = " + VchNo + ")";
                dgPayCreditCardDetails.Rows.Clear();
                DataTable dt = ObjFunction.GetDataView(strQuery).Table;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dgPayCreditCardDetails.Rows.Add();
                    for (int k = 0; k < dt.Columns.Count; k++)
                        dgPayCreditCardDetails.Rows[i].Cells[k].Value = dt.Rows[i].ItemArray[k];
                }
                dgPayCreditCardDetails.Rows.Add();
                dgPayCreditCardDetails.Columns[0].Width = 69;
                dgPayCreditCardDetails.Columns[1].Width = 240;
                //dgPayCreditCardDetails.Columns[2].Width = 114;
                dgPayCreditCardDetails.Columns[3].Width = 75;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgPayCreditCardDetails_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    if (dgPayCreditCardDetails.CurrentCell.ColumnIndex == 1)
                    {

                        pnlBank.Location = new Point(dgPayCreditCardDetails.CurrentCell.ContentBounds.X + 72 + dgPayCreditCardDetails.Location.X, dgPayCreditCardDetails.CurrentCell.ContentBounds.Y + 50);
                        pnlBank.Visible = true;
                        pnlBank.BringToFront();
                        lstBank.Focus();
                    }
                    else if (dgPayCreditCardDetails.CurrentCell.ColumnIndex == 2)
                    {
                        pnlBranch.Location = new Point(dgPayCreditCardDetails.CurrentCell.ContentBounds.X + 200 + dgPayCreditCardDetails.Location.X, dgPayCreditCardDetails.CurrentCell.ContentBounds.Y + 50);
                        pnlBranch.Visible = true;
                        pnlBranch.BringToFront();
                        lstBranch.Focus();
                    }
                    else if (dgPayCreditCardDetails.CurrentCell.ColumnIndex == 3)
                    {
                        double TotAmt = 0;
                        for (int i = 0; i < dgPayCreditCardDetails.Rows.Count; i++)
                        {
                            if (dgPayCreditCardDetails.Rows[i].Cells[3].Value != null)
                            {
                                TotAmt = TotAmt + Convert.ToDouble(dgPayCreditCardDetails.Rows[i].Cells[3].Value);
                            }
                        }
                        txtTotalAmt.Text = TotAmt.ToString();
                        btnOk.Focus();
                    }
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    double Amt = 0;
                    if (dgPayCreditCardDetails.Rows[0].Cells[0].Value != null)
                    {
                        if (dgPayCreditCardDetails.Rows[0].Cells[0].FormattedValue.ToString() == "" || dgPayCreditCardDetails.Rows[0].Cells[1].FormattedValue.ToString() == "")
                        {
                            OMMessageBox.Show("Please Fill Credit Card Details.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            return;
                        }
                        for (int i = 0; i < dgPayCreditCardDetails.Rows.Count; i++)
                        {
                            Amt += (dgPayCreditCardDetails.Rows[i].Cells[3].Value == null) ? 0 : Convert.ToDouble(dgPayCreditCardDetails.Rows[i].Cells[3].Value);
                        }
                        if (Convert.ToDouble(dgPayType.Rows[4].Cells[2].Value) != Amt)
                        {
                            OMMessageBox.Show("Please enter CrediCard amount and CreditCard Details amount are not same...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            dgPayCreditCardDetails.Focus();
                        }
                        else
                        {
                            if (ObjFunction.GetComboValue(cmbPaymentType) == 1)
                            {
                                pnlPartial.Size = new Size(305, 214);
                                pnlPartial.Location = new Point(200, 123);
                                dgPayType.CurrentCell = dgPayType[2, 4];
                                dgPayType.Focus();
                            }
                            else
                            {
                                btnSave.Enabled = true;
                                btnSave.Focus();
                                pnlPartial.Visible = false;
                            }
                        }
                    }
                    else
                    {
                        if (ObjFunction.GetComboValue(cmbPaymentType) == 1)
                        {
                            pnlPartial.Size = new Size(305, 214);
                            pnlPartial.Location = new Point(200, 123);
                            dgPayType.CurrentCell = dgPayType[2, 4];
                            dgPayType.Focus();
                        }
                        else
                        {
                            cmbPaymentType.Focus();
                            pnlPartial.Visible = false;
                        }
                    }
                }
                else if (e.KeyCode == Keys.Delete)
                {
                    //if (dgPayCreditCardDetails.CurrentCell.RowIndex != dgPayCreditCardDetails.Rows.Count - 1)
                    //{
                    if (Convert.ToInt64(dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[4].Value) != 0)
                    {
                        DeleteDtls(5, Convert.ToInt64(dgPayCreditCardDetails.Rows[dgPayCreditCardDetails.CurrentCell.RowIndex].Cells[4].Value));
                    }
                    dgPayCreditCardDetails.Rows.RemoveAt(dgPayCreditCardDetails.CurrentCell.RowIndex);
                    if (dgPayCreditCardDetails.Rows.Count == 0)
                        dgPayCreditCardDetails.Rows.Add();
                    dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[0, dgPayCreditCardDetails.Rows.Count - 1];
                    //}
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgPayCreditCardDetails_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 0)
                {
                    if (dgPayCreditCardDetails.Rows[e.RowIndex].Cells[1].Value == null)
                    {
                        if (e.RowIndex == 0) dgPayCreditCardDetails.Rows[e.RowIndex].Cells[3].Value = dgPayType.Rows[4].Cells[2].Value;
                        pnlBank.Location = new Point(dgPayCreditCardDetails.CurrentCell.ContentBounds.X + 72 + dgPayCreditCardDetails.Location.X, dgPayCreditCardDetails.CurrentCell.ContentBounds.Y + 50);
                        pnlBank.Visible = true;
                        pnlBank.BringToFront();
                        lstBank.Focus();
                    }
                    else
                    {
                        dgPayCreditCardDetails.Focus();
                        MovetoNext move2n = new MovetoNext(m2n);
                        BeginInvoke(move2n, new object[] { dgPayCreditCardDetails.CurrentCell.RowIndex, 1, dgPayCreditCardDetails });
                    }
                }
                if (e.ColumnIndex == 3)
                {
                    if (dgPayCreditCardDetails.Rows[e.RowIndex].Cells[3].Value != null)
                    {
                        dgPayCreditCardDetails.Rows[e.RowIndex].Cells[3].ErrorText = "";
                        if (ObjFunction.CheckValidAmount(dgPayCreditCardDetails.Rows[e.RowIndex].Cells[3].Value.ToString()) == false)
                        {
                            dgPayCreditCardDetails.Rows[e.RowIndex].Cells[3].ErrorText = "Please Enter Valid Amount";
                        }
                        else
                        {
                            double TotAmt = 0;
                            dgPayCreditCardDetails.Rows[e.RowIndex].Cells[3].ErrorText = "";
                            if (e.RowIndex == dgPayCreditCardDetails.Rows.Count - 1 && dgPayCreditCardDetails.Rows[e.RowIndex].Cells[1].Value != null)
                            {
                                dgPayCreditCardDetails.Rows.Add();
                                dgPayCreditCardDetails.Focus();
                                dgPayCreditCardDetails.CurrentCell = dgPayCreditCardDetails[0, dgPayCreditCardDetails.Rows.Count - 1];
                            }

                            for (int i = 0; i < dgPayCreditCardDetails.Rows.Count; i++)
                            {
                                if (dgPayCreditCardDetails.Rows[i].Cells[3].Value != null)
                                {
                                    TotAmt = TotAmt + Convert.ToDouble(dgPayCreditCardDetails.Rows[i].Cells[3].Value);
                                }
                            }
                            txtTotalAmt.Text = TotAmt.ToString();
                        }
                    }
                    else
                    {
                        dgPayCreditCardDetails.Rows[e.RowIndex].Cells[3].ErrorText = "Please Enter  Amount";
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }
        #endregion

        #region Rate Type Password related Methods

        private void txtRateTypePassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                btnRateTypeOK_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Escape)
            {
                pnlRateType.Visible = false;
                btnNew.Focus();
            }
        }

        private void btnRateTypeOK_Click(object sender, EventArgs e)
        {
            try
            {
                bool flag = false;
                string[,] arr = new string[5, 2];
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ARateIsActive)) == true)
                {
                    arr[0, 0] = ObjFunction.GetAppSettings(AppSettings.ARatePassword);
                    arr[0, 1] = "ASaleRate";
                }

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.BRateIsActive)) == true)
                {
                    arr[1, 0] = ObjFunction.GetAppSettings(AppSettings.BRatePassword);
                    arr[1, 1] = "BSaleRate";
                }

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.CRateIsActive)) == true)
                {
                    arr[2, 0] = ObjFunction.GetAppSettings(AppSettings.CRatePassword);
                    arr[2, 1] = "CSaleRate";
                }

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.DRateIsActive)) == true)
                {
                    arr[3, 0] = ObjFunction.GetAppSettings(AppSettings.DRatePassword);
                    arr[3, 1] = "DSaleRate";
                }

                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ERateIsActive)) == true)
                {
                    arr[4, 0] = ObjFunction.GetAppSettings(AppSettings.ERatePassword);
                    arr[4, 1] = "ESaleRate";
                }

                for (int i = 0; i < 5; i++)
                {
                    if (arr[i, 0] != null)
                    {
                        if (txtRateTypePassword.Text == arr[i, 0].ToString())
                        {
                            cmbRateType.SelectedValue = arr[i, 1].ToString();
                            cmbRateType_SelectedIndexChanged(sender, new EventArgs());
                            RateTypeNo = i + 1;
                            DBMSettings dbMSettings = new DBMSettings();
                            if (arr[i, 1].ToString() == "ASaleRate")
                            {
                                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ARateDBEffect)) == true)
                                    dbMSettings.AddAppSettings(AppSettings.P_RateType, "1");
                            }
                            else if (arr[i, 1].ToString() == "BSaleRate")
                            {
                                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.BRateDBEffect)) == true)
                                    dbMSettings.AddAppSettings(AppSettings.P_RateType, "2");
                            }
                            else if (arr[i, 1].ToString() == "CSaleRate")
                            {
                                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.CRateDBEffect)) == true)
                                    dbMSettings.AddAppSettings(AppSettings.P_RateType, "3");
                            }
                            else if (arr[i, 1].ToString() == "DSaleRate")
                            {
                                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.DRateDBEffect)) == true)
                                    dbMSettings.AddAppSettings(AppSettings.P_RateType, "4");
                            }
                            else if (arr[i, 1].ToString() == "ESaleRate")
                            {
                                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ERateDBEffect)) == true)
                                    dbMSettings.AddAppSettings(AppSettings.P_RateType, "5");
                            }
                            dbMSettings.ExecuteNonQueryStatements();
                            ObjFunction.SetAppSettings();
                            flag = true;
                            break;
                        }
                    }
                }
                if (flag == false)
                {
                    OMMessageBox.Show("Please enter valid password", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    txtRateTypePassword.Focus();
                }
                else
                {
                    pnlRateType.Visible = false;
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnRateTypeCancel_Click(object sender, EventArgs e)
        {
            txtRateTypePassword.Text = "";
            pnlRateType.Visible = false;
        }

        #endregion

        private bool IsSuperMode()
        {
            try
            {
                bool flag = false;
                long RTNo = Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_RateType));
                if (RTNo == 1)
                {
                    flag = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ARateSuperMode));
                }
                else if (RTNo == 2)
                {
                    flag = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.BRateSuperMode));
                }
                else if (RTNo == 3)
                {
                    flag = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.CRateSuperMode));
                }
                else if (RTNo == 4)
                {
                    flag = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.DRateSuperMode));
                }
                else if (RTNo == 5)
                {
                    flag = Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ERateSuperMode));
                }
                return flag;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return false;
            }
        }

        private void btnOKPrintBarCode_Click(object sender, EventArgs e)
        {
            try
            {
                DBTBarCodePrint dbBarCodePrint = new DBTBarCodePrint();
                TBarCodePrint tBarCodePrint = new TBarCodePrint();
                tBarCodePrint.MacNo = DBGetVal.MacNo;
                tBarCodePrint.UserID = DBGetVal.UserID;
                dbBarCodePrint.DeleteTBarCodePrint(tBarCodePrint);

                tBarCodePrint = new TBarCodePrint();
                tBarCodePrint.PkSrNo = 0;
                tBarCodePrint.ItemNo = ItemNumber;
                tBarCodePrint.Quantity = Convert.ToInt64(txtNoOfPrint.Text);
                tBarCodePrint.FKRateSettingNo = Convert.ToInt64(dgBill.Rows[dgBill.CurrentCell.RowIndex].Cells[ColIndex.PkRateSettingNo].Value);
                tBarCodePrint.MacNo = DBGetVal.MacNo;
                tBarCodePrint.UserID = DBGetVal.UserID;
                dbBarCodePrint.AddTBarCodePrint(tBarCodePrint);
                dbBarCodePrint.ExecuteNonQueryStatements();

                string[] ReportSession;
                ReportSession = new string[4];
                //ReportSession[0] = ItemNumber.ToString();
                //ReportSession[1] = txtNoOfPrint.Text;
                ReportSession[0] = "1";
                ReportSession[1] = txtStartNo.Text;
                ReportSession[2] = DBGetVal.MacNo.ToString();
                ReportSession[3] = DBGetVal.UserID.ToString();

                if (OMMessageBox.Show("Do you want Preview of barcode?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Form NewF;
                    if (rbBigMod.Checked == true)
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            NewF = new Display.ReportViewSource(new Reports.BarCodePrintBig(), ReportSession);
                        else
                            NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("BarCodePrintBig.rpt", CommonFunctions.ReportPath), ReportSession);
                        ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                    }
                    else if (rbSmallMode.Checked == true)
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            NewF = new Display.ReportViewSource(new Reports.BarCodePrint(), ReportSession);
                        else
                            NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("BarCodePrint.rpt", CommonFunctions.ReportPath), ReportSession);
                        ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                    }
                }
                else
                {
                    CrystalDecisions.CrystalReports.Engine.ReportDocument childForm = null;
                    if (rbBigMod.Checked == true)
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            childForm = new Reports.BarCodePrintBig();
                        else
                            childForm = ObjFunction.LoadReportObject("BarCodePrintBig.rpt", CommonFunctions.ReportPath);
                    }
                    else if (rbSmallMode.Checked == true)
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            childForm = new Reports.BarCodePrint();
                        else

                            childForm = ObjFunction.LoadReportObject("BarCodePrint.rpt", CommonFunctions.ReportPath);
                    }

                    if (childForm != null)
                    {
                        DBReportGenerate objRpt = new DBReportGenerate(childForm, ReportSession);
                        if (objRpt.PrintReport() == true)
                        {
                            OMMessageBox.Show("Printing barCode sucessfully...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        }
                        else
                        {
                            OMMessageBox.Show("Barcode not Print...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        OMMessageBox.Show("Barcode Print report not exist...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                }

                pnlBarCodePrint.Visible = false;
                btnNew.Focus();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnCancelPrintBarcode_Click(object sender, EventArgs e)
        {
            pnlBarCodePrint.Visible = false;
            btnNew.Focus();
        }

        private void dgBill_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == ColIndex.BarcodePrint)
            {
                if (dgBill.Rows[e.RowIndex].Cells[ColIndex.ItemNo].Value != null)
                {
                    txtNoOfPrint.Text = "";
                    ItemNumber = Convert.ToInt64(dgBill.Rows[e.RowIndex].Cells[ColIndex.ItemNo].Value);
                    txtNoOfPrint.Text = dgBill.Rows[e.RowIndex].Cells[ColIndex.ActualQty].Value.ToString();
                    pnlBarCodePrint.Visible = true;
                    txtStartNo.Text = "0";
                    txtNoOfPrint.Enabled = true;
                    txtStartNo.Enabled = true;
                    txtNoOfPrint.Focus();
                }
            }
        }

        #region Company Panel related Methods

        private void BindCompany()
        {
            try
            {
                DataTable dtPayType = new DataTable();
                string sqlQuery = "";
                sqlQuery = "SELECT CompanyNo, CompanyName From MCompany Order by CompanyName";

                DataTable dt = ObjFunction.GetDataView(sqlQuery).Table;

                for (int row = 0; row < dt.Rows.Count; row++)
                {
                    dgCompany.Rows.Add();
                    for (int col = 0; col < dt.Columns.Count; col++)
                    {
                        dgCompany[col, row].Value = dt.Rows[row].ItemArray[col].ToString();
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgCompany_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    //PurCompNo = Convert.ToInt64(dgCompany.CurrentRow.Cells[0].Value);
                    lblCompName.Text = dgCompany.CurrentRow.Cells[1].Value.ToString();
                    pnlCompany.Visible = false;

                    ID = 0;
                    ObjFunction.InitialiseControl(this.Controls);
                    ObjFunction.LockButtons(true, this.Controls);
                    ObjFunction.LockControls(false, this.Controls);
                    dgBill.Enabled = false;
                    DisplayList(false);
                    InitControls();

                    dtSearch = ObjFunction.GetDataView("Select PkVoucherNo from TVoucherEntry Where VoucherTypeCode=" + VoucherType + " AND CompanyNo=" + DBGetVal.CompanyNo + "").Table;

                    if (dtSearch.Rows.Count > 0)
                    {
                        ID = Convert.ToInt64(dtSearch.Rows[dtSearch.Rows.Count - 1].ItemArray[0].ToString());
                        FillControls();
                        SetNavigation();
                    }

                    setDisplay(true);
                    btnNew.Focus();
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    pnlCompany.Visible = false;
                    btnNew.Focus();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private string GetCompanyName(long CNO)
        {
            string strName = "";
            for (int i = 0; i < dgCompany.Rows.Count; i++)
            {
                if (CNO == Convert.ToInt64(dgCompany.Rows[i].Cells[0].Value))
                {
                    strName = dgCompany.Rows[i].Cells[1].Value.ToString();
                    break;
                }
            }
            return strName;
        }
        #endregion

        #region All Print BarCode
        private void btnAllBarCodePrint_Click(object sender, EventArgs e)
        {
            try
            {
                pnlAllPrintBarCode.Visible = true;
                pnlAllPrintBarCode.Location = new Point(110, 98);
                pnlAllPrintBarCode.Size = new Size(540, 291);
                dgPrintBarCode.Height = 267;
                while (dgPrintBarCode.Rows.Count > 0)
                    dgPrintBarCode.Rows.RemoveAt(0);
                for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                {
                    dgPrintBarCode.Rows.Add();
                    int index = dgPrintBarCode.Rows.Count - 1;
                    dgPrintBarCode.Rows[index].Cells[0].Value = dgBill.Rows[index].Cells[ColIndex.ItemNo].Value;
                    dgPrintBarCode.Rows[index].Cells[1].Value = dgBill.Rows[index].Cells[ColIndex.ItemName].Value;
                    dgPrintBarCode.Rows[index].Cells[2].Value = dgBill.Rows[index].Cells[ColIndex.Quantity].Value;
                    dgPrintBarCode.Rows[index].Cells[3].Value = true;
                    dgPrintBarCode.Rows[index].Cells[4].Value = dgBill.Rows[index].Cells[ColIndex.PkRateSettingNo].Value;
                }
                txtAllStartNo.Enabled = true;
                txtAllStartNo.Text = "0";
                txtAllStartNo.Focus();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgPrintBarCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btnAllBarcodeOK.Focus();
            }
        }

        private void btnAllBarcodeOK_Click(object sender, EventArgs e)
        {
            try
            {
                DBTBarCodePrint dbBarCodePrint = new DBTBarCodePrint();
                TBarCodePrint tBarCodePrint = new TBarCodePrint();
                tBarCodePrint.MacNo = DBGetVal.MacNo;
                tBarCodePrint.UserID = DBGetVal.UserID;
                dbBarCodePrint.DeleteTBarCodePrint(tBarCodePrint);

                for (int i = 0; i < dgPrintBarCode.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(dgPrintBarCode.Rows[i].Cells[3].Value) == true)
                    {
                        tBarCodePrint = new TBarCodePrint();
                        tBarCodePrint.PkSrNo = 0;
                        tBarCodePrint.ItemNo = Convert.ToInt64(dgPrintBarCode.Rows[i].Cells[0].Value.ToString());
                        tBarCodePrint.Quantity = Convert.ToInt64(dgPrintBarCode.Rows[i].Cells[2].Value.ToString());
                        tBarCodePrint.FKRateSettingNo = Convert.ToInt64(dgPrintBarCode.Rows[i].Cells[4].Value.ToString());
                        tBarCodePrint.MacNo = DBGetVal.MacNo;
                        tBarCodePrint.UserID = DBGetVal.UserID;
                        dbBarCodePrint.AddTBarCodePrint(tBarCodePrint);
                    }
                }
                dbBarCodePrint.ExecuteNonQueryStatements();

                string[] ReportSession;
                ReportSession = new string[4];
                //ReportSession[0] = ItemNumber.ToString();
                //ReportSession[1] = txtNoOfPrint.Text;
                ReportSession[0] = "1";
                ReportSession[1] = txtAllStartNo.Text;
                ReportSession[2] = DBGetVal.MacNo.ToString();
                ReportSession[3] = DBGetVal.UserID.ToString();

                if (OMMessageBox.Show("Do you want Preview of barcode?", CommonFunctions.ErrorTitle, OMMessageBoxButton.YesNo, OMMessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Form NewF;
                    if (rdAllBigMode.Checked == true)
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            NewF = new Display.ReportViewSource(new Reports.BarCodePrintBig(), ReportSession);
                        else
                            NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("BarCodePrintBig.rpt", CommonFunctions.ReportPath), ReportSession);
                        ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                    }
                    else if (rdAllSmallMode.Checked == true)
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            NewF = new Display.ReportViewSource(new Reports.BarCodePrint(), ReportSession);
                        else
                            NewF = new Display.ReportViewSource(ObjFunction.LoadReportObject("BarCodePrint.rpt", CommonFunctions.ReportPath), ReportSession);
                        ObjFunction.OpenForm(NewF, DBGetVal.MainForm);
                    }
                }
                else
                {
                    CrystalDecisions.CrystalReports.Engine.ReportDocument childForm = null;
                    if (rbBigMod.Checked == true)
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            childForm = new Reports.BarCodePrintBig();
                        else
                            childForm = ObjFunction.LoadReportObject("BarCodePrintBig.rpt", CommonFunctions.ReportPath);
                    }
                    else if (rbSmallMode.Checked == true)
                    {
                        if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.ReportDisplay)) == true)
                            childForm = new Reports.BarCodePrint();
                        else

                            childForm = ObjFunction.LoadReportObject("BarCodePrint.rpt", CommonFunctions.ReportPath);
                    }

                    if (childForm != null)
                    {
                        DBReportGenerate objRpt = new DBReportGenerate(childForm, ReportSession);
                        if (objRpt.PrintReport() == true)
                        {
                            OMMessageBox.Show("Printing barCode sucessfully...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Information);
                        }
                        else
                        {
                            OMMessageBox.Show("Barcode not Print...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        OMMessageBox.Show("Barcode Print report not exist...", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                }
                //dbBarCodePrint.DeleteTBarCodePrint(tBarCodePrint);
                pnlAllPrintBarCode.Visible = false;
                btnNew.Focus();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnAllBarcodeCancel_Click(object sender, EventArgs e)
        {
            pnlAllPrintBarCode.Visible = false;
            if (btnNew.Visible)
                btnNew.Focus();
            else
                btnSave.Focus();
        }
        #endregion

        private void txtOtherDisc_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Escape)
                {
                    if (txtOtherDisc.Text.Trim() != "")
                    {
                        if (ObjFunction.CheckValidAmount(txtOtherDisc.Text) == false)
                        {
                            OMMessageBox.Show("Enter Discount Value.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            txtOtherDisc.Focus();
                        }
                        else
                        {
                            for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                            {
                                dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value = txtOtherDisc.Text;
                                CalculateTotal();
                            }
                            if (e.KeyCode == Keys.Escape)

                                btnSave.Focus();
                            else if (e.KeyCode == Keys.Enter)
                            {
                                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnParty)) == true) cmbPartyName.Focus();
                                else if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnTaxType)) == true) cmbTaxType.Focus();
                                else dgBill.Focus();
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                        {
                            dgBill.Rows[i].Cells[ColIndex.DiscPercentage].Value = 0;
                            CalculateTotal();
                        }
                        if (e.KeyCode == Keys.Escape)

                            btnSave.Focus();
                        else if (e.KeyCode == Keys.Enter)
                        {
                            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnParty)) == true) cmbPartyName.Focus();
                            else if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnTaxType)) == true) cmbTaxType.Focus();
                            else dgBill.Focus();
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void txtRefNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtRefNo_Leave(txtRefNo, new EventArgs());
                e.SuppressKeyPress = true;
            }
        }

        private void txtRefNo_Leave(object sender, EventArgs e)
        {
            if (txtRefNo.Text == "")
            {
                OMMessageBox.Show("Enter Invoice No.", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                txtRefNo.Focus();
            }
            else
            {
                if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnHeaderDisc)) == true) txtOtherDisc.Focus();
                else if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnParty)) == true) cmbPartyName.Focus();
                else if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_StopOnTaxType)) == true) cmbTaxType.Focus();
                else dgBill.Focus();
            }
        }

        #region All Search Code

        private void txtInvNoSearch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                long tempNo;
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    if (ObjQry.ReturnLong("Select Count(*) From TVoucherEntry Where Reference='" + txtInvNoSearch.Text + "' and VoucherTypeCode=" + VoucherType + "", CommonFunctions.ConStr) > 1)
                    {
                        pnlInvSearch.Visible = true;

                        int x = dgBill.GetCellDisplayRectangle(0, 0, true).X + 200;//(Screen.PrimaryScreen.WorkingArea.Width) / 2;
                        int y = dgBill.GetCellDisplayRectangle(0, 0, true).Y + 100;
                        //pnlPartySearch.SetBounds(x, y, dgPartySearch.Width + 10, dgPartySearch.Height + 10);
                        pnlInvSearch.Location = new Point(x, y);
                        string str = "SELECT    0 as [#], TVoucherEntry.VoucherUserNo AS [Doc #], TVoucherEntry.Reference AS 'Inv No', TVoucherEntry.VoucherDate AS 'Date', TVoucherEntry.BilledAmount AS 'Amount'," +
                                     "TVoucherEntry.PkVoucherNo, MLedger.LedgerName as 'Party' FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo INNER JOIN MLedger ON TVoucherDetails.LedgerNo = MLedger.LedgerNo  WHERE (TVoucherEntry.VoucherTypeCode IN (" + VchType.Purchase + "," + VchType.PurchaseOrder + ")) AND (TVoucherEntry.CompanyNo = " + DBGetVal.CompanyNo + ") And TVoucherDetails.VoucherSrNo=1 And TVoucherDetails.SrNo=501  " +
                                     "And TVoucherEntry.Reference='" + txtInvNoSearch.Text + "' " +
                                     "Order By  TVoucherEntry.VoucherUserNo desc,TVoucherEntry.VoucherDate desc, TVoucherEntry.Reference desc";
                        dgInvSearch.DataSource = ObjFunction.GetDataView(str).Table.DefaultView;

                        dgInvSearch.Columns[0].Width = 40;
                        dgInvSearch.Columns[1].Width = 50;
                        dgInvSearch.Columns[2].Width = 60;
                        dgInvSearch.Columns[3].Width = 70;
                        dgInvSearch.Columns[4].Width = 80;
                        dgInvSearch.Columns[5].Visible = false;
                        dgInvSearch.Columns[4].HeaderCell.Style.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
                        dgInvSearch.Columns[4].DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
                        dgInvSearch.Columns[6].Width = 150;
                        if (dgInvSearch.RowCount > 0)
                        {
                            pnlInvSearch.Focus();
                            SearchVisible(false);
                            pnlSearch.Visible = false;
                            e.SuppressKeyPress = true;
                            dgInvSearch.Focus();
                            dgInvSearch.CurrentCell = dgInvSearch[0, dgInvSearch.CurrentRow.Index];

                        }
                        txtInvNoSearch.Text = "";
                        cmbPartyNameSearch.SelectedIndex = 0;
                        return;
                    }

                    tempNo = ObjQry.ReturnLong("Select PKVoucherNo From TVoucherEntry Where Reference='" + txtInvNoSearch.Text + "' and VoucherTypeCode=" + VoucherType + " AND CompanyNo=" + DBGetVal.CompanyNo + "", CommonFunctions.ConStr);
                    if (tempNo > 0)
                    {
                        ID = tempNo;
                        SetNavigation();
                        FillControls();

                        pnlSearch.Visible = false;
                        btnNew.Enabled = true;
                        btnUpdate.Enabled = true;
                        btndelete.Enabled = true;
                        SearchVisible(false);
                    }
                    else
                    {
                        pnlSearch.Visible = false;
                        DisplayMessage("Bill Not Found");
                        pnlSearch.Visible = true;
                        rbInvNo.Focus();
                        rbType(true);
                        txtInvNoSearch.Text = "";
                        txtSearch.Text = "";
                        cmbPartyName.SelectedIndex = 0;
                        //SearchVisible(false);
                    }
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    e.SuppressKeyPress = true;
                    pnlSearch.Visible = false;
                    pnlPartySearch.Visible = false;
                    btnCancelSearch_Click(sender, new EventArgs());
                    btnNew.Focus();
                }
                else if (e.KeyCode == Keys.Right)
                {
                    e.SuppressKeyPress = true;
                    rbDocNo.Checked = true;
                    rbType_CheckedChanged(rbPartyName, null);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbPartyNameSearch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (pnlPartySearch.Visible == true)
                    {
                        pnlPartySearch.Visible = false;
                        pnlSearch.Visible = true;
                    }
                    else
                    {

                        pnlPartySearch.Visible = true;
                        int x = dgBill.GetCellDisplayRectangle(0, 0, true).X + 200;//(Screen.PrimaryScreen.WorkingArea.Width) / 2;
                        int y = dgBill.GetCellDisplayRectangle(0, 0, true).Y + 100;
                        pnlPartySearch.SetBounds(x, y, dgPartySearch.Width + 10, dgPartySearch.Height + 10);
                        string str = "SELECT    0 as [#], TVoucherEntry.VoucherUserNo AS [Doc #], TVoucherEntry.Reference AS 'Invoice No', TVoucherEntry.VoucherDate AS 'Date', TVoucherEntry.BilledAmount AS 'Amount'," +
                                     "TVoucherEntry.PkVoucherNo FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo WHERE (TVoucherEntry.VoucherTypeCode IN (" + VchType.Purchase + "," + VchType.PurchaseOrder + ")) AND (TVoucherDetails.VoucherSrNo = 1) AND (TVoucherEntry.CompanyNo = " + DBGetVal.CompanyNo + ")" +
                                     "And TVoucherDetails.LedgerNo=" + ObjFunction.GetComboValue(cmbPartyNameSearch) + " " +
                                     "Order By  TVoucherEntry.VoucherUserNo desc,TVoucherEntry.VoucherDate desc, TVoucherEntry.Reference desc";
                        dgPartySearch.DataSource = ObjFunction.GetDataView(str).Table.DefaultView;
                        dgPartySearch.Columns[0].Width = 50;
                        dgPartySearch.Columns[1].Width = 50;
                        dgPartySearch.Columns[2].Width = 150;
                        dgPartySearch.Columns[3].Width = 80;
                        dgPartySearch.Columns[4].Width = 110;
                        dgPartySearch.Columns[5].Visible = false;
                        if (dgPartySearch.RowCount > 0)
                        {

                            pnlSearch.Visible = false;
                            e.SuppressKeyPress = true;
                            pnlPartySearch.Focus();
                            dgPartySearch.Focus();
                            dgPartySearch.CurrentCell = dgPartySearch[0, dgPartySearch.CurrentRow.Index];
                            SearchVisible(false);
                        }
                        else
                        {
                            pnlPartySearch.Visible = false;
                            pnlSearch.Visible = false;
                            DisplayMessage("Bill Not Found");
                            pnlSearch.Visible = true;
                            rbPartyName.Focus();
                            //SearchVisible(false);
                        }
                        txtInvNoSearch.Text = "";
                        txtSearch.Text = "";
                        cmbPartyNameSearch.SelectedIndex = 0;

                    }
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    e.SuppressKeyPress = true;
                    pnlSearch.Visible = false;
                    pnlPartySearch.Visible = false;
                    btnCancelSearch_Click(sender, new EventArgs());
                    btnNew.Focus();
                }
                else if (e.KeyCode == Keys.Left)
                {
                    e.SuppressKeyPress = true;
                    rbDocNo.Checked = true;
                    rbType_CheckedChanged(rbPartyName, null);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgPartySearch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    long tempNo;
                    e.SuppressKeyPress = true;
                    tempNo = ObjQry.ReturnLong("Select PKVoucherNo From TVoucherEntry Where PkVoucherNo=" + Convert.ToInt64(dgPartySearch.Rows[dgPartySearch.CurrentRow.Index].Cells[5].Value) + " and VoucherTypeCode=" + VoucherType + " AND CompanyNo=" + DBGetVal.CompanyNo + "", CommonFunctions.ConStr);
                    if (tempNo > 0)
                    {
                        ID = tempNo;
                        SetNavigation();
                        FillControls();
                        btnNew.Enabled = true;
                        btnUpdate.Enabled = true;
                        btndelete.Enabled = true;
                        pnlPartySearch.Visible = false;
                        btnNew.Focus();
                        SearchVisible(false);
                    }
                    else
                    {
                        txtInvNoSearch.Text = "";
                        txtSearch.Text = "";
                        cmbPartyName.SelectedIndex = 0;
                        DisplayMessage("Bill Not Found");
                        txtSearch.Focus();
                        //SearchVisible(false);
                    }

                }
                else if (e.KeyCode == Keys.Escape)
                {
                    e.SuppressKeyPress = true;
                    pnlPartySearch.Visible = false;
                    pnlSearch.Visible = true;
                    txtSearch.Focus();
                    rbType_CheckedChanged(sender, new EventArgs());
                }
                txtInvNoSearch.Text = "";
                txtSearch.Text = "";
                cmbPartyNameSearch.SelectedIndex = 0;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgPartySearch_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                e.Value = e.RowIndex + 1;

            }
            else if (e.ColumnIndex == 3)
            {
                e.Value = Convert.ToDateTime(e.Value).ToString("dd-MMM-yy");
            }
        }

        private void rbType_CheckedChanged(object sender, EventArgs e)
        {
            rbType(true);

        }

        private void rbType(bool IsSetFocus)
        {
            try
            {
                rbInvNo.Enabled = true;
                rbDocNo.Enabled = true;
                rbPartyName.Enabled = true;
                if (rbInvNo.Checked == true)
                {
                    if (IsSetFocus)
                    {
                        lblLable.Visible = true;
                        lblLable.Text = "Inv No :";
                        txtInvNoSearch.Width = 162;
                        txtInvNoSearch.Location = new System.Drawing.Point(90, 39);
                        txtInvNoSearch.Visible = true;
                        txtSearch.Visible = false;
                        cmbPartyNameSearch.Visible = false;
                        txtInvNoSearch.Focus();
                    }
                }
                else if (rbDocNo.Checked == true)
                {
                    if (IsSetFocus)
                    {
                        lblLable.Visible = true;
                        lblLable.Text = "Doc No :";
                        txtSearch.Width = 162;
                        txtSearch.Location = new System.Drawing.Point(90, 39);
                        txtSearch.Visible = true;
                        txtInvNoSearch.Visible = false;
                        cmbPartyNameSearch.Visible = false;
                        txtSearch.Focus();
                    }
                }
                else if (rbPartyName.Checked == true)
                {
                    if (IsSetFocus)
                    {
                        lblLable.Visible = true;
                        lblLable.Text = "Party Name :";
                        cmbPartyNameSearch.Width = 330;
                        cmbPartyNameSearch.Location = new System.Drawing.Point(90, 39);
                        cmbPartyNameSearch.Visible = true;
                        txtSearch.Visible = false;
                        txtInvNoSearch.Visible = false;
                        cmbPartyNameSearch.Focus();
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void rbType_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                rbType(true);
            }
            else if (e.KeyCode == Keys.Escape)
            {
                e.SuppressKeyPress = true;
                pnlSearch.Visible = false;
                pnlPartySearch.Visible = false;
                btnSave.Focus();
                SearchVisible(false);
            }
        }

        private void SearchVisible(bool flag)
        {
            txtSearch.Visible = flag;
            cmbPartyNameSearch.Visible = flag;
            txtInvNoSearch.Visible = flag;
            lblLable.Visible = flag;
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                long tempNo;
                if (e.KeyCode == Keys.Enter)
                {


                    tempNo = ObjQry.ReturnLong("Select PKVoucherNo From TVoucherEntry Where VoucherUserNo=" + txtSearch.Text + " and VoucherTypeCode=" + VoucherType + " AND CompanyNo=" + DBGetVal.CompanyNo + "", CommonFunctions.ConStr);
                    if (tempNo > 0)
                    {
                        ID = tempNo;
                        SetNavigation();
                        FillControls();

                        pnlSearch.Visible = false;
                        btnNew.Enabled = true;
                        btnUpdate.Enabled = true;
                        btndelete.Enabled = true;
                        SearchVisible(false);
                    }
                    else
                    {
                        pnlSearch.Visible = false;
                        DisplayMessage("Bill Not Found");
                        pnlSearch.Visible = true;
                        rbDocNo.Focus();
                        txtSearch.Focus();
                        txtSearch.Text = "";
                        //SearchVisible(false);
                    }
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    e.SuppressKeyPress = true;
                    pnlSearch.Visible = false;
                    pnlPartySearch.Visible = false;
                    btnCancelSearch_Click(sender, new EventArgs());
                    btnNew.Focus();
                }
                else if (e.KeyCode == Keys.Right)
                {
                    e.SuppressKeyPress = true;
                    rbPartyName.Checked = true;
                    rbType_CheckedChanged(rbPartyName, null);
                }
                else if (e.KeyCode == Keys.Left)
                {
                    e.SuppressKeyPress = true;
                    rbInvNo.Checked = true;
                    rbType_CheckedChanged(rbPartyName, null);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        #endregion

        private void txtDiscount1_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            //    e.SuppressKeyPress = true;
            //    Control_Leave((object)txtDiscount1, new EventArgs());
            //}
        }

        private void txtDiscRupees1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                Control_Leave((object)txtDiscRupees1, new EventArgs());
                txtChrgRupees1.Focus();
            }
        }

        private void txtChrgRupees1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                Control_Leave((object)txtChrgRupees1, new EventArgs());
                txtReturnAmt.Focus();
            }
        }

        private void btnAdvanceSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Master.AdvancedSearch Adsch = new Kirana.Master.AdvancedSearch(GroupType.SundryCreditors);
                ObjFunction.OpenForm(Adsch);
                if (Adsch.LedgerNo != 0)
                {
                    cmbPartyName.SelectedValue = Adsch.LedgerNo;
                    cmbPartyName.Focus();
                    Adsch.Close();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void txtGrandTotal_TextChanged(object sender, EventArgs e)
        {
            lblGrandTotal.Text = txtGrandTotal.Text;
        }

        private void btnShowDetails_Click(object sender, EventArgs e)
        {
            try
            {
                if (BillSizeFlag == false)
                {
                    dgBill.Height = dgBill.Height - (pnlTotalAmt.Height + 10);
                    pnlTotalAmt.Location = new Point(dgBill.Height + 10, pnlTotalAmt.Location.Y);
                    pnlTotalAmt.Location = new Point(dgBill.Width - pnlTotalAmt.Width + 10, dgBill.Location.Y + dgBill.Height + 10);
                    pnlTotalAmt.Visible = true;
                    BillSizeFlag = true;
                }
                else
                {
                    BillSizeFlag = false;
                    pnlTotalAmt.Visible = false;
                    dgBill.Height = dgBill.Height + (pnlTotalAmt.Height + 10);
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void btnShortcut_Click(object sender, EventArgs e)
        {
            if (pnlFooterInfo.Visible == false)
            {
                pnlFooterInfo.Dock = DockStyle.Bottom;
                //pnlFooterInfo.Height = 30;
                pnlFooterInfo.BorderStyle = BorderStyle.None;
                pnlFooterInfo.BringToFront();
                pnlFooterInfo.Visible = true;
            }
            else
            {
                pnlFooterInfo.Visible = false;
            }
        }

        private void btnNewCustomer_Click(object sender, EventArgs e)
        {
            try
            {
                long PartyNo = ObjFunction.GetComboValue(cmbPartyName);
                if (ObjFunction.CheckAllowMenu(24) == false) return;
                Form NewF = new Master.SupplierAE(-1);
                ObjFunction.OpenForm(NewF);
                if (((Master.SupplierAE)NewF).ShortID != 0)
                {
                    ObjFunction.FillCombo(cmbPartyName, "Select LedgerNo,LedgerName From MLedger Where GroupNo in (" + GroupType.SundryCreditors + ")  order by LedgerName", "New Entry");
                    if (((Master.SupplierAE)NewF).ShortID > 0)
                        cmbPartyName.SelectedValue = ((Master.SupplierAE)NewF).ShortID;
                    else
                        cmbPartyName.SelectedValue = PartyNo;
                    cmbPartyName.Focus();
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgPartySearch_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtDiscRupees1_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(txtDiscRupees1, 2, 5, JitFunctions.MaskedType.NotNegative);
        }

        private void txtChrgRupees1_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(txtChrgRupees1, 2, 5, JitFunctions.MaskedType.NotNegative);
        }

        private void txtReturnAmt_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(txtReturnAmt, 2, 5, JitFunctions.MaskedType.NotNegative);
        }

        private void txtVisibility_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(txtVisibility, 2, 5, JitFunctions.MaskedType.NotNegative);
        }

        private void txtReturnAmt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                Control_Leave((object)txtReturnAmt, new EventArgs());
                txtVisibility.Focus();
            }
        }

        private void txtVisibility_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                Control_Leave((object)txtVisibility, new EventArgs());
                if (cmbPaymentType.Enabled == true)
                    cmbPaymentType.Focus();
                else
                    txtRemark.Focus();
            }
        }

        private void lstGroup2_KeyDown(object sender, KeyEventArgs e)
        {
            //string ItemListStr = "";
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                pnlGroup2.Visible = false;

                FillItemList(strItemQuery.Length - 1);
            }
            else if (e.KeyCode == Keys.Space)
            {
                dgBill.Focus();
                pnlGroup2.Visible = false;
            }
        }

        private void lstGroup1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //      if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_Bilingual)) == true)
            //        lstGroup1Lang.SelectedIndex = lstGroup1.SelectedIndex;
        }

        private void lstGroup1Lang_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.O_Bilingual)) == true)
            {
                // if (lstGroup1.Items.Count > 0)
                //   lstGroup1.SelectedIndex = lstGroup1Lang.SelectedIndex;
            }
        }

        private void PurchaseAE_Activated(object sender, EventArgs e)
        {
            try
            {
                if (ExistFormFlag == true) return;
                if (isDoProcess)
                {
                    long Pid = ObjFunction.GetComboValue(cmbPartyName);
                    ObjFunction.FillComb(cmbPartyName, "Select LedgerNo,LedgerName From MLedger Where GroupNo in " +
                    "(" + GroupType.SundryCreditors + ") and IsActive='true' or LedgerNo=" + Pno + " order by LedgerName");
                    cmbPartyName.SelectedValue = Pid;
                }
                isDoProcess = false;

                if (ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo=( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + ")", CommonFunctions.ConStr) > 1)
                    btnUpdate.Visible = false;
                else if (ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo in ( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + " and TR.TypeOfRef in(6))", CommonFunctions.ConStr) > 1)
                    btnUpdate.Visible = false;
                else
                    btnUpdate.Visible = true;

                if ((ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo=( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + ")", CommonFunctions.ConStr) > 1) && PayType == 3)
                {
                    btnMixMode.Visible = false;
                }
                else if (PayType == 3 && tVoucherEntry.IsVoucherLock == false && tVoucherEntry.IsCancel == false)
                {
                    btnMixMode.Visible = true;
                }
                else
                    btnMixMode.Visible = false;

                if (ID != 0)
                {
                    if ((ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo in ( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + ")", CommonFunctions.ConStr) > 1) && ObjFunction.GetComboValue(cmbPaymentType) == 3)
                        btnMixMode.Visible = false;
                    else if (btnMixMode.Text == "Receipt\r\nMode")
                        btnMixMode.Visible = true;
                    else btnMixMode.Visible = false;
                }

            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void PurchaseAE_Deactivate(object sender, EventArgs e)
        {
            isDoProcess = true;
        }

        private void DeleteZeroQty(int rowindex)
        {
            bool flag;
            try
            {
                long PKStockTrnNo = Convert.ToInt64(dgBill.Rows[rowindex].Cells[ColIndex.PkStockTrnNo].Value);
                if (PKStockTrnNo != 0)
                {
                    DeleteDtls(1, PKStockTrnNo);
                    DeleteDtls(5, Convert.ToInt64(dgBill.Rows[rowindex].Cells[ColIndex.PkStockDetailsNo].Value));
                    flag = false;
                    for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                    {
                        if (rowindex != i)
                        {
                            if (Convert.ToInt64(dgBill.Rows[rowindex].Cells[ColIndex.PkVoucherNo].Value) == Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.PkVoucherNo].Value))
                            { flag = true; break; }
                        }
                    }
                    if (flag == false)
                        DeleteDtls(2, Convert.ToInt64(dgBill.Rows[rowindex].Cells[ColIndex.PkVoucherNo].Value));

                    //For Purchase LedgerNo
                    flag = false;
                    for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                    {
                        if (rowindex != i)
                        {
                            if (Convert.ToInt64(dgBill.Rows[rowindex].Cells[ColIndex.SalesVchNo].Value) == Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.SalesVchNo].Value))
                            { flag = true; break; }
                        }
                    }
                    if (flag == false)
                        DeleteDtls(2, Convert.ToInt64(dgBill.Rows[rowindex].Cells[ColIndex.SalesVchNo].Value));

                    //FOr TaxLedgerNo
                    flag = false;
                    for (int i = 0; i < dgBill.Rows.Count - 1; i++)
                    {
                        if (rowindex != i)
                        {
                            if (Convert.ToInt64(dgBill.Rows[rowindex].Cells[ColIndex.TaxVchNo].Value) == Convert.ToInt64(dgBill.Rows[i].Cells[ColIndex.TaxVchNo].Value))
                            { flag = true; break; }
                        }
                    }
                    if (flag == false)
                        DeleteDtls(2, Convert.ToInt64(dgBill.Rows[rowindex].Cells[ColIndex.TaxVchNo].Value));


                }

                if (dgBill.Rows.Count - 1 == rowindex)
                {
                    dgBill.Rows.RemoveAt(rowindex);
                    dgBill.Rows.Add();
                }
                else
                    dgBill.Rows.RemoveAt(rowindex);

                CalculateTotal();

                dgBill.CurrentCell = dgBill[2, dgBill.Rows.Count - 1];
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private bool SavePayment(long SalesID)
        {
            try
            {
                DataTable dtPayType = new DataTable();
                int cntPayType = 1;

                long tempid = -1, ReceiptID = 0, VoucherUserNo = 0;

                for (int row = 1; row <= 4; row++) //for (int j = 0; j < dgPayType.Rows.Count; j++)
                {
                    if (row == 1 || row == 3 || row == 4)
                    {
                        if (Convert.ToDouble(dgPayType.Rows[row].Cells[2].Value) != 0)
                        {
                            if (row == 1 || row == 3 || row == 4)
                                ReceiptID = ObjQry.ReturnLong("SELECT TVoucherDetails.FkVoucherNo FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo " +
                                  " WHERE (TVoucherEntry.VoucherTypeCode = " + VchType.Payment + ") AND (TVoucherEntry.VoucherDate ='" + dtpBillDate.Text + "') AND (TVoucherDetails.LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + ") AND " +
                                    " (TVoucherDetails.VoucherSrNo = 1) AND (TVoucherEntry.PayTypeNo=" + Convert.ToInt64(dgPayType.Rows[row].Cells[1].Value) + ") ", CommonFunctions.ConStr);

                            DataTable dtReceipt = ObjFunction.GetDataView("SELECT PkVoucherTrnNo,LedgerNo,Debit,Credit FROM TVoucherDetails " +
                                " WHERE (FkVoucherNo = " + ReceiptID + ") order by VoucherSrNo ").Table;
                            VoucherUserNo = ObjQry.ReturnLong("Select IsNull((VoucherUserNo),0) From TVoucherEntry Where PkVoucherNo=" + ReceiptID + "", CommonFunctions.ConStr);
                            long VoucherSrNo = ObjQry.ReturnLong("select IsNull(max(VoucherSrNo),0)+1 from TVoucherDetails where (FkVoucherNo = " + ReceiptID + ")", CommonFunctions.ConStr);

                            //int VoucherSrNo = 1;
                            dbTVoucherEntry = new DBTVaucherEntry();
                            //Voucher Header Entry
                            tVoucherEntry = new TVoucherEntry();
                            tVoucherEntry.PkVoucherNo = ReceiptID;
                            tVoucherEntry.VoucherTypeCode = VchType.Payment;
                            tVoucherEntry.VoucherUserNo = VoucherUserNo;
                            tVoucherEntry.VoucherDate = Convert.ToDateTime(dtpBillDate.Text);
                            tVoucherEntry.VoucherTime = dtpBillTime.Value;
                            tVoucherEntry.Narration = "Payment Bill";
                            tVoucherEntry.Reference = "";
                            tVoucherEntry.ChequeNo = 0;
                            tVoucherEntry.ClearingDate = Convert.ToDateTime("01-Jan-1900");
                            tVoucherEntry.CompanyNo = DBGetVal.CompanyNo;
                            tVoucherEntry.BilledAmount = Convert.ToDouble(txtGrandTotal.Text);
                            tVoucherEntry.ChallanNo = "";
                            tVoucherEntry.Remark = txtRemark.Text.Trim();
                            tVoucherEntry.MacNo = DBGetVal.MacNo;
                            tVoucherEntry.PayTypeNo = ObjFunction.GetComboValue(cmbPaymentType);
                            tVoucherEntry.RateTypeNo = 0;
                            tVoucherEntry.TaxTypeNo = 0;
                            tVoucherEntry.OrderType = 2;


                            tVoucherEntry.UserID = DBGetVal.UserID;
                            tVoucherEntry.UserDate = DBGetVal.ServerTime.Date;
                            dbTVoucherEntry.AddTVoucherEntry(tVoucherEntry);

                            DataTable dtVoucherDetails = new DataTable();
                            if (ReceiptID != 0)
                            {
                                dtVoucherDetails = ObjFunction.GetDataView("Select PkVoucherTrnNo,LedgerNo,Debit,Credit From TVoucherDetails Where FkVoucherNo=" + ReceiptID + " order by VoucherSrNo").Table;
                                dtPayType = ObjFunction.GetDataView("Select PKVoucherPayTypeNo From TVoucherPayTypeDetails Where FKSalesVoucherNo=" + SalesID + " AND FKPayTypeNo=" + dgPayType.Rows[row].Cells[1].Value + "  order by PKVoucherPayTypeNo").Table;
                            }

                            //for (int i = 0; i < dtCompRatio.Rows.Count; i++)
                            //{
                            //dtVoucherDetails = ObjFunction.GetDataView("Select PkVoucherTrnNo,LedgerNo,Debit,Credit,VoucherSrNo From TVoucherDetails TD,TVoucherEntry TC Where TC.PKVoucherNo=TD.FKVoucherNo AND TC.PayTypeNo=" + ObjFunction.GetComboValue(cmbPaymentType) + " AND TD.LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " AND  TD.FkVoucherNo=" + ReceiptID + " AND TD.CompanyNo=" + Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString()) + " order by TD.VoucherSrNo").Table;
                            dtVoucherDetails = ObjFunction.GetDataView("Select PkVoucherTrnNo,LedgerNo,Debit,Credit,VoucherSrNo From TVoucherDetails TD,TVoucherEntry TC Where TC.PKVoucherNo=TD.FKVoucherNo AND TC.PayTypeNo=" + ObjFunction.GetComboValue(cmbPaymentType) + " AND TD.LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " AND  TD.FkVoucherNo=" + ReceiptID + "  order by TD.VoucherSrNo").Table;

                            double totamt = 0, amt = 0;

                            if (Convert.ToDouble(dgPayType.Rows[row].Cells[2].Value) != 0)
                            {

                                //amt = Convert.ToDouble(dgPayType.Rows[row].Cells[4 + (i + 1)].Value);
                                amt = Convert.ToDouble(dgPayType.Rows[row].Cells[4].Value);


                                totamt = totamt + amt;
                            }
                            amt = 0;
                            if (ID != 0 && PartyNo == ObjFunction.GetComboValue(cmbPartyName) && PayType == ObjFunction.GetComboValue(cmbPaymentType)) amt = Convert.ToDouble(dtVchPrev.Select("CompanyNo=" + DBGetVal.CompanyNo + "")[0].ItemArray[1].ToString());//ObjQry.ReturnDouble("Select Sum(Debit) From TVoucherDetails Where LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " AND  CompanyNo=" + Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString()) + " ", CommonFunctions.ConStr);


                            //For Party Ledger
                            tVoucherDetails = new TVoucherDetails();
                            //tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                            tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > 0) ? Convert.ToInt64(dtVoucherDetails.Rows[0].ItemArray[0].ToString()) : 0;
                            if (tVoucherDetails.PkVoucherTrnNo == 0)
                                tVoucherDetails.VoucherSrNo = VoucherSrNo;
                            else tVoucherDetails.VoucherSrNo = Convert.ToInt64(dtVoucherDetails.Rows[0].ItemArray[4].ToString());
                            tVoucherDetails.SignCode = 1;//2;
                            tVoucherDetails.LedgerNo = ObjFunction.GetComboValue(cmbPartyName);
                            //tVoucherDetails.Debit = 0;
                            tVoucherDetails.Credit = 0;

                            double Newval = 0;
                            for (int k = 0; k < dgBill.Rows.Count - 1; k++)
                            {
                                //if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.StockCompanyNo].Value) == Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString()))
                                //{
                                if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.PkStockTrnNo].Value) == 0)
                                {
                                    Newval = Newval + Convert.ToDouble(dgBill.Rows[k].Cells[ColIndex.Amount].Value);
                                }
                                //}
                            }
                            totamt = totamt - Newval;
                            if (ID == 0)
                                //                                tVoucherDetails.Credit = ((dtVoucherDetails.Rows.Count > 0) ? Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[3].ToString()) : 0) + Convert.ToDouble(dgPayType.Rows[row].Cells[4 + (i + 1)].Value);
                                tVoucherDetails.Debit = ((dtVoucherDetails.Rows.Count > 0) ? Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[2].ToString()) : 0) + Convert.ToDouble(dgPayType.Rows[row].Cells[4].Value);
                            else
                            {
                                if (dtVoucherDetails.Rows.Count > 0)
                                    // tVoucherDetails.Credit = Newval + ((Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[3].ToString()) - amt) + totamt);
                                    tVoucherDetails.Debit = Newval + ((Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[2].ToString()) - amt) + totamt);
                                else
                                    //tVoucherDetails.Credit = totamt;
                                    tVoucherDetails.Debit = totamt;
                            }
                            if (tVoucherDetails.PkVoucherTrnNo == 0) VoucherSrNo += 1;
                            tVoucherDetails.Narration = "";
                            tVoucherDetails.SrNo = Others.Party;
                            tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;//Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString());
                            if (ReceiptID != 0)
                                dbTVoucherEntry.UpdateBillAmount(ReceiptID, tVoucherDetails.Debit);
                            dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                            //}



                            //for (int i = 0; i < dtCompRatio.Rows.Count; i++)
                            //{
                            totamt = 0;
                            if (Convert.ToDouble(dgPayType.Rows[row].Cells[2].Value) != 0)
                                //totamt += Convert.ToDouble(dgPayType.Rows[row].Cells[4 + (i + 1)].Value);
                                totamt += Convert.ToDouble(dgPayType.Rows[row].Cells[4].Value);
                            dtVoucherDetails = ObjFunction.GetDataView("Select PkVoucherTrnNo,LedgerNo,Debit,Credit,VoucherSrNo From TVoucherDetails Where LedgerNo=" + Convert.ToInt64(dtPayLedger.Select("PayTypeNo=" + Convert.ToInt64(dgPayType.Rows[row].Cells[1].Value) + " AND CompanyNo=" + DBGetVal.CompanyNo + "")[0][2].ToString()) + " AND  FkVoucherNo=" + ReceiptID + " AND CompanyNo=" + DBGetVal.CompanyNo + " order by VoucherSrNo").Table;


                            //For PayType Details
                            tVoucherDetails = new TVoucherDetails();
                            //tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                            tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > 0) ? Convert.ToInt64(dtVoucherDetails.Rows[0].ItemArray[0].ToString()) : 0;
                            if (tVoucherDetails.PkVoucherTrnNo == 0)
                                tVoucherDetails.VoucherSrNo = VoucherSrNo;
                            else tVoucherDetails.VoucherSrNo = Convert.ToInt64(dtVoucherDetails.Rows[0].ItemArray[4].ToString());
                            tVoucherDetails.SignCode = 2;//1;
                            tVoucherDetails.LedgerNo = Convert.ToInt64(dtPayLedger.Select("PayTypeNo=" + Convert.ToInt64(dgPayType.Rows[row].Cells[1].Value) + " AND CompanyNo=" + DBGetVal.CompanyNo + "")[0][2].ToString());// Convert.ToInt64(dgPayType.Rows[row].Cells[3].Value);
                            Newval = 0;
                            for (int k = 0; k < dgBill.Rows.Count - 1; k++)
                            {
                                //if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.StockCompanyNo].Value) == Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString()))
                                //{
                                if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.PkStockTrnNo].Value) == 0)
                                {
                                    Newval = Newval + Convert.ToDouble(dgBill.Rows[k].Cells[ColIndex.Amount].Value);
                                }
                                //}
                            }
                            totamt = totamt - Newval;
                            amt = 0;
                            if (ID != 0 && PartyNo == ObjFunction.GetComboValue(cmbPartyName) && PayType == ObjFunction.GetComboValue(cmbPaymentType)) amt = Convert.ToDouble(dtVchPrev.Select("CompanyNo=" + DBGetVal.CompanyNo + "")[0].ItemArray[1].ToString());//ObjQry.ReturnDouble("Select Sum(Debit) From TVoucherDetails Where LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " AND  CompanyNo=" + Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString()) + " ", CommonFunctions.ConStr);

                            if (ID == 0)
                                //tVoucherDetails.Debit = ((dtVoucherDetails.Rows.Count > 0) ? Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[2].ToString()) : 0) + Convert.ToDouble(dgPayType.Rows[row].Cells[4 + (i + 1)].Value);
                                tVoucherDetails.Credit = ((dtVoucherDetails.Rows.Count > 0) ? Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[3].ToString()) : 0) + Convert.ToDouble(dgPayType.Rows[row].Cells[4].Value);
                            else
                            {
                                if (dtVoucherDetails.Rows.Count > 0)
                                    //tVoucherDetails.Debit = Newval + ((Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[2].ToString()) - amt) + totamt);
                                    tVoucherDetails.Credit = Newval + ((Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[3].ToString()) - amt) + totamt);
                                else
                                    //tVoucherDetails.Debit = totamt;
                                    tVoucherDetails.Credit = totamt;
                            }

                            //tVoucherDetails.Credit = 0;
                            tVoucherDetails.Debit = 0;
                            if (tVoucherDetails.PkVoucherTrnNo == 0) VoucherSrNo += 1;
                            tVoucherDetails.Narration = "";
                            tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;//Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString());
                            dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);




                            tVchPayTypeDetails = new TVoucherPayTypeDetails();
                            tVchPayTypeDetails.PKVoucherPayTypeNo = (dtPayType.Rows.Count > cntPayType - 1) ? Convert.ToInt64(dtPayType.Rows[cntPayType - 1].ItemArray[0].ToString()) : 0; cntPayType += 1;
                            tVchPayTypeDetails.FKSalesVoucherNo = SalesID;
                            tVchPayTypeDetails.FKPayTypeNo = Convert.ToInt64(dgPayType.Rows[row].Cells[1].Value);
                            tVchPayTypeDetails.Amount = Convert.ToDouble(dgPayType.Rows[row].Cells[4].Value);
                            tVchPayTypeDetails.CompanyNo = DBGetVal.CompanyNo;//Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString());
                            dbTVoucherEntry.AddTVoucherPayTypeDetails(tVchPayTypeDetails);
                            //}

                            if ((PartyNo != ObjFunction.GetComboValue(cmbPartyName) || PayType != ObjFunction.GetComboValue(cmbPaymentType)) && ID != 0)
                            {
                                DataTable dtDelPayType = ObjFunction.GetDataView("Select PKVoucherPayTypeNo,TVoucherPayTypeDetails.CompanyNo,FKReceiptVoucherNo,Amount From TVoucherPayTypeDetails,TVoucherDetails Where TVoucherDetails.PkVoucherTrnNo=TVoucherPayTypeDetails.FKVoucherTrnNo AND FKSalesVoucherNo=" + SalesID + "  order by PKVoucherPayTypeNo").Table;
                                for (int k = 0; k < dtDelPayType.Rows.Count; k++)
                                {
                                    if (dtPayType.Rows.Count > 0)
                                    {
                                        if (dtPayType.Select("PKVoucherPayTypeNo=" + dtDelPayType.Rows[k].ItemArray[0].ToString())[0][0].ToString() != dtDelPayType.Rows[k].ItemArray[0].ToString())
                                        {
                                            tVchPayTypeDetails = new TVoucherPayTypeDetails();
                                            tVchPayTypeDetails.PKVoucherPayTypeNo = Convert.ToInt64(dtDelPayType.Rows[k].ItemArray[0].ToString());
                                            dbTVoucherEntry.DeleteTVoucherPayTypeDetails(tVchPayTypeDetails);
                                        }
                                    }
                                    else
                                    {
                                        tVchPayTypeDetails = new TVoucherPayTypeDetails();
                                        tVchPayTypeDetails.PKVoucherPayTypeNo = Convert.ToInt64(dtDelPayType.Rows[k].ItemArray[0].ToString());
                                        dbTVoucherEntry.DeleteTVoucherPayTypeDetails(tVchPayTypeDetails);
                                    }
                                    DataTable dtUpdateVoucher = ObjFunction.GetDataView("Select PKVoucherTrnNo,Debit,Credit From TVoucherDetails Where FKVoucherNo=" + dtDelPayType.Rows[k].ItemArray[2].ToString() + " AND CompanyNo=" + dtDelPayType.Rows[k].ItemArray[1].ToString() + "").Table;
                                    totamt = 0;
                                    bool alllowdel = false;
                                    for (int m = 0; m < dtUpdateVoucher.Rows.Count; m++)
                                    {
                                        double DrAmt = Convert.ToDouble(dtUpdateVoucher.Rows[m].ItemArray[1].ToString());
                                        double CrAmt = Convert.ToDouble(dtUpdateVoucher.Rows[m].ItemArray[2].ToString());
                                        if (DrAmt > 0) DrAmt = DrAmt - Convert.ToDouble(dtDelPayType.Rows[k].ItemArray[3].ToString());
                                        if (CrAmt > 0) CrAmt = CrAmt - Convert.ToDouble(dtDelPayType.Rows[k].ItemArray[3].ToString());
                                        dbTVoucherEntry.UpdateVoucherDetails(DrAmt, CrAmt, Convert.ToInt64(dtUpdateVoucher.Rows[m].ItemArray[0].ToString()));
                                        totamt = totamt + DrAmt + CrAmt;
                                        alllowdel = true;
                                    }
                                    if (totamt == 0 && alllowdel == true)
                                    {
                                        for (int m = 0; m < dtUpdateVoucher.Rows.Count; m++)
                                        {
                                            tVoucherDetails = new TVoucherDetails();
                                            tVoucherDetails.PkVoucherTrnNo = Convert.ToInt64(dtUpdateVoucher.Rows[m].ItemArray[0].ToString());
                                            dbTVoucherEntry.DeleteTVoucherDetails(tVoucherDetails);

                                            if (m == dtUpdateVoucher.Rows.Count - 1)
                                            {
                                                if (ObjQry.ReturnLong("Select Count(*) From TVoucherDetails Where FKVoucherNo=" + Convert.ToInt64(dtDelPayType.Rows[k].ItemArray[2].ToString()) + "", CommonFunctions.ConStr) >= dtUpdateVoucher.Rows.Count)
                                                {
                                                    tVoucherEntry = new TVoucherEntry();
                                                    tVoucherEntry.PkVoucherNo = Convert.ToInt64(dtDelPayType.Rows[k].ItemArray[2].ToString());
                                                    dbTVoucherEntry.DeleteTVoucherEntry1(tVoucherEntry);
                                                }
                                            }
                                        }
                                    }
                                }


                            }
                            tempid = dbTVoucherEntry.ExecuteNonQueryStatements();
                        }
                    }

                }

                if (tempid != 0)
                    return true;
                else
                    return false;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return false;
            }
        }

        private bool SavePaymentNew(long SalesID)
        {
            try
            {
                DataTable dtPayType = new DataTable();
                int cntPayType = 1;

                long tempid = -1, ReceiptID = 0, VoucherUserNo = 0;

                for (int row = 1; row <= 4; row++) //for (int j = 0; j < dgPayType.Rows.Count; j++)
                {
                    if (row == 1 || row == 3 || row == 4)
                    {
                        if (Convert.ToDouble(dgPayType.Rows[row].Cells[2].Value) != 0)
                        {
                            if (row == 1 || row == 3 || row == 4)
                                ReceiptID = ObjQry.ReturnLong("SELECT TVoucherDetails.FkVoucherNo FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo " +
                                  " WHERE (TVoucherEntry.VoucherTypeCode = " + VchType.Payment + ") AND (TVoucherEntry.VoucherDate ='" + dtpBillDate.Text + "') AND (TVoucherDetails.LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + ") AND " +
                                    " (TVoucherDetails.VoucherSrNo = 1) AND (TVoucherEntry.PayTypeNo=" + Convert.ToInt64(dgPayType.Rows[row].Cells[1].Value) + ") ", CommonFunctions.ConStr);

                            DataTable dtReceipt = ObjFunction.GetDataView("SELECT PkVoucherTrnNo,LedgerNo,Debit,Credit FROM TVoucherDetails " +
                                " WHERE (FkVoucherNo = " + ReceiptID + ") order by VoucherSrNo ").Table;
                            VoucherUserNo = ObjQry.ReturnLong("Select IsNull((VoucherUserNo),0) From TVoucherEntry Where PkVoucherNo=" + ReceiptID + "", CommonFunctions.ConStr);
                            long VoucherSrNo = ObjQry.ReturnLong("select IsNull(max(VoucherSrNo),0)+1 from TVoucherDetails where (FkVoucherNo = " + ReceiptID + ")", CommonFunctions.ConStr);
                            double PrevAmt = 0;
                            for (int i = 0; i < dtVchPrev.Rows.Count; i++)
                                PrevAmt += Convert.ToDouble(dtVchPrev.Rows[i].ItemArray[1].ToString());
                            PrevAmt = (ObjQry.ReturnDouble("Select BilledAmount From TVoucherEntry Where  PkVoucherNo=" + ReceiptID + "", CommonFunctions.ConStr)) + Convert.ToDouble(txtGrandTotal.Text);
                            //int VoucherSrNo = 1;
                            dbTVoucherEntry = new DBTVaucherEntry();
                            //Voucher Header Entry
                            tVoucherEntry = new TVoucherEntry();
                            tVoucherEntry.PkVoucherNo = ReceiptID;
                            tVoucherEntry.VoucherTypeCode = VchType.Payment;
                            tVoucherEntry.VoucherUserNo = VoucherUserNo;
                            tVoucherEntry.VoucherDate = Convert.ToDateTime(dtpBillDate.Text);
                            tVoucherEntry.VoucherTime = dtpBillTime.Value;
                            tVoucherEntry.Narration = "Payment Bill";
                            tVoucherEntry.Reference = "";
                            tVoucherEntry.ChequeNo = 0;
                            tVoucherEntry.ClearingDate = Convert.ToDateTime("01-Jan-1900");
                            tVoucherEntry.CompanyNo = DBGetVal.CompanyNo;
                            tVoucherEntry.BilledAmount = PrevAmt;// Convert.ToDouble(txtGrandTotal.Text);
                            tVoucherEntry.ChallanNo = "";
                            tVoucherEntry.Remark = txtRemark.Text.Trim();
                            tVoucherEntry.MacNo = DBGetVal.MacNo;
                            tVoucherEntry.PayTypeNo = ObjFunction.GetComboValue(cmbPaymentType);
                            tVoucherEntry.RateTypeNo = 0;
                            tVoucherEntry.TaxTypeNo = 0;
                            tVoucherEntry.OrderType = 2;


                            tVoucherEntry.UserID = DBGetVal.UserID;
                            tVoucherEntry.UserDate = DBGetVal.ServerTime.Date;
                            dbTVoucherEntry.AddTVoucherEntry(tVoucherEntry);

                            DataTable dtVoucherDetails = new DataTable();
                            if (ReceiptID != 0)
                            {
                                dtVoucherDetails = ObjFunction.GetDataView("Select PkVoucherTrnNo,LedgerNo,Debit,Credit From TVoucherDetails Where FkVoucherNo=" + ReceiptID + " order by VoucherSrNo").Table;
                                dtPayType = ObjFunction.GetDataView("Select PKVoucherPayTypeNo From TVoucherPayTypeDetails Where FKSalesVoucherNo=" + SalesID + " AND FKPayTypeNo=" + dgPayType.Rows[row].Cells[1].Value + "  order by PKVoucherPayTypeNo").Table;
                            }

                            //for (int i = 0; i < dtCompRatio.Rows.Count; i++)
                            //{
                            //dtVoucherDetails = ObjFunction.GetDataView("Select PkVoucherTrnNo,LedgerNo,Debit,Credit,VoucherSrNo From TVoucherDetails TD,TVoucherEntry TC Where TC.PKVoucherNo=TD.FKVoucherNo AND TC.PayTypeNo=" + ObjFunction.GetComboValue(cmbPaymentType) + " AND TD.LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " AND  TD.FkVoucherNo=" + ReceiptID + " AND TD.CompanyNo=" + Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString()) + " order by TD.VoucherSrNo").Table;
                            dtVoucherDetails = ObjFunction.GetDataView("Select PkVoucherTrnNo,LedgerNo,Debit,Credit,VoucherSrNo From TVoucherDetails TD,TVoucherEntry TC Where TC.PKVoucherNo=TD.FKVoucherNo AND TC.PayTypeNo=" + ObjFunction.GetComboValue(cmbPaymentType) + " AND TD.LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " AND  TD.FkVoucherNo=" + ReceiptID + "  order by TD.VoucherSrNo").Table;

                            double totamt = 0, amt = 0;

                            if (Convert.ToDouble(dgPayType.Rows[row].Cells[2].Value) != 0)
                            {

                                //amt = Convert.ToDouble(dgPayType.Rows[row].Cells[4 + (i + 1)].Value);
                                amt = Convert.ToDouble(dgPayType.Rows[row].Cells[4].Value);


                                totamt = totamt + amt;
                            }
                            amt = 0;
                            if (ID != 0 && PartyNo == ObjFunction.GetComboValue(cmbPartyName) && PayType == ObjFunction.GetComboValue(cmbPaymentType)) amt = Convert.ToDouble(dtVchPrev.Select("CompanyNo=" + DBGetVal.CompanyNo + "")[0].ItemArray[1].ToString());//ObjQry.ReturnDouble("Select Sum(Debit) From TVoucherDetails Where LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " AND  CompanyNo=" + Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString()) + " ", CommonFunctions.ConStr);


                            //For Party Ledger
                            tVoucherDetails = new TVoucherDetails();
                            //tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                            tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > 0) ? Convert.ToInt64(dtVoucherDetails.Rows[0].ItemArray[0].ToString()) : 0;
                            if (tVoucherDetails.PkVoucherTrnNo == 0)
                                tVoucherDetails.VoucherSrNo = VoucherSrNo;
                            else tVoucherDetails.VoucherSrNo = Convert.ToInt64(dtVoucherDetails.Rows[0].ItemArray[4].ToString());
                            tVoucherDetails.SignCode = 1;//2;
                            tVoucherDetails.LedgerNo = ObjFunction.GetComboValue(cmbPartyName);
                            //tVoucherDetails.Debit = 0;
                            tVoucherDetails.Credit = 0;

                            double Newval = 0;
                            for (int k = 0; k < dgBill.Rows.Count - 1; k++)
                            {
                                //if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.StockCompanyNo].Value) == Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString()))
                                //{
                                if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.PkStockTrnNo].Value) == 0)
                                {
                                    Newval = Newval + Convert.ToDouble(dgBill.Rows[k].Cells[ColIndex.Amount].Value);
                                }
                                //}
                            }
                            totamt = totamt - Newval;
                            if (ID == 0)
                                tVoucherDetails.Debit = ((dtVoucherDetails.Rows.Count > 0) ? Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[2].ToString()) : 0) + Convert.ToDouble(dgPayType.Rows[row].Cells[4].Value);
                            else
                            {
                                if (dtVoucherDetails.Rows.Count > 0)
                                    tVoucherDetails.Debit = Newval + ((Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[2].ToString())) + totamt);
                                else
                                    tVoucherDetails.Debit = totamt;
                            }
                            if (tVoucherDetails.PkVoucherTrnNo == 0) VoucherSrNo += 1;
                            tVoucherDetails.Narration = "";
                            tVoucherDetails.SrNo = Others.Party;
                            tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;//Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString());
                            dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                            //}



                            //for (int i = 0; i < dtCompRatio.Rows.Count; i++)
                            //{
                            totamt = 0;
                            if (Convert.ToDouble(dgPayType.Rows[row].Cells[2].Value) != 0)
                                //totamt += Convert.ToDouble(dgPayType.Rows[row].Cells[4 + (i + 1)].Value);
                                totamt += Convert.ToDouble(dgPayType.Rows[row].Cells[4].Value);
                            dtVoucherDetails = ObjFunction.GetDataView("Select PkVoucherTrnNo,LedgerNo,Debit,Credit,VoucherSrNo From TVoucherDetails Where LedgerNo=" + Convert.ToInt64(dtPayLedger.Select("PayTypeNo=" + Convert.ToInt64(dgPayType.Rows[row].Cells[1].Value) + " AND CompanyNo=" + DBGetVal.CompanyNo + "")[0][2].ToString()) + " AND  FkVoucherNo=" + ReceiptID + " AND CompanyNo=" + DBGetVal.CompanyNo + " order by VoucherSrNo").Table;


                            //For PayType Details
                            tVoucherDetails = new TVoucherDetails();
                            //tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                            tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > 0) ? Convert.ToInt64(dtVoucherDetails.Rows[0].ItemArray[0].ToString()) : 0;
                            if (tVoucherDetails.PkVoucherTrnNo == 0)
                                tVoucherDetails.VoucherSrNo = VoucherSrNo;
                            else tVoucherDetails.VoucherSrNo = Convert.ToInt64(dtVoucherDetails.Rows[0].ItemArray[4].ToString());
                            tVoucherDetails.SignCode = 2;//1;
                            tVoucherDetails.LedgerNo = Convert.ToInt64(dtPayLedger.Select("PayTypeNo=" + Convert.ToInt64(dgPayType.Rows[row].Cells[1].Value) + " AND CompanyNo=" + DBGetVal.CompanyNo + "")[0][2].ToString());// Convert.ToInt64(dgPayType.Rows[row].Cells[3].Value);
                            Newval = 0;
                            for (int k = 0; k < dgBill.Rows.Count - 1; k++)
                            {
                                //if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.StockCompanyNo].Value) == Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString()))
                                //{
                                if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.PkStockTrnNo].Value) == 0)
                                {
                                    Newval = Newval + Convert.ToDouble(dgBill.Rows[k].Cells[ColIndex.Amount].Value);
                                }
                                //}
                            }
                            totamt = totamt - Newval;
                            amt = 0;
                            if (ID != 0 && PartyNo == ObjFunction.GetComboValue(cmbPartyName) && PayType == ObjFunction.GetComboValue(cmbPaymentType)) amt = Convert.ToDouble(dtVchPrev.Select("CompanyNo=" + DBGetVal.CompanyNo + "")[0].ItemArray[1].ToString());//ObjQry.ReturnDouble("Select Sum(Debit) From TVoucherDetails Where LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " AND  CompanyNo=" + Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString()) + " ", CommonFunctions.ConStr);

                            if (ID == 0)
                                tVoucherDetails.Credit = ((dtVoucherDetails.Rows.Count > 0) ? Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[3].ToString()) : 0) + Convert.ToDouble(dgPayType.Rows[row].Cells[4].Value);
                            else
                            {
                                if (dtVoucherDetails.Rows.Count > 0)
                                    tVoucherDetails.Credit = Newval + ((Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[3].ToString())) + totamt);
                                else
                                    tVoucherDetails.Credit = totamt;
                            }

                            //tVoucherDetails.Credit = 0;
                            tVoucherDetails.Debit = 0;
                            if (tVoucherDetails.PkVoucherTrnNo == 0) VoucherSrNo += 1;
                            tVoucherDetails.Narration = "";
                            tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;//Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString());
                            dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);




                            tVchPayTypeDetails = new TVoucherPayTypeDetails();
                            tVchPayTypeDetails.PKVoucherPayTypeNo = (dtPayType.Rows.Count > cntPayType - 1) ? Convert.ToInt64(dtPayType.Rows[cntPayType - 1].ItemArray[0].ToString()) : 0; cntPayType += 1;
                            tVchPayTypeDetails.FKSalesVoucherNo = SalesID;
                            tVchPayTypeDetails.FKPayTypeNo = Convert.ToInt64(dgPayType.Rows[row].Cells[1].Value);
                            tVchPayTypeDetails.Amount = Convert.ToDouble(dgPayType.Rows[row].Cells[4].Value);
                            tVchPayTypeDetails.CompanyNo = DBGetVal.CompanyNo;//Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString());
                            dbTVoucherEntry.AddTVoucherPayTypeDetails(tVchPayTypeDetails);
                            //}

                            if ((PartyNo != ObjFunction.GetComboValue(cmbPartyName) || PayType != ObjFunction.GetComboValue(cmbPaymentType)) && ID != 0)
                            {
                                DataTable dtDelPayType = ObjFunction.GetDataView("Select PKVoucherPayTypeNo,TVoucherPayTypeDetails.CompanyNo,FKReceiptVoucherNo,Amount From TVoucherPayTypeDetails,TVoucherDetails Where TVoucherDetails.PkVoucherTrnNo=TVoucherPayTypeDetails.FKVoucherTrnNo AND FKSalesVoucherNo=" + SalesID + "  order by PKVoucherPayTypeNo").Table;
                                for (int k = 0; k < dtDelPayType.Rows.Count; k++)
                                {
                                    if (dtPayType.Rows.Count > 0)
                                    {
                                        if (dtPayType.Select("PKVoucherPayTypeNo=" + dtDelPayType.Rows[k].ItemArray[0].ToString())[0][0].ToString() != dtDelPayType.Rows[k].ItemArray[0].ToString())
                                        {
                                            tVchPayTypeDetails = new TVoucherPayTypeDetails();
                                            tVchPayTypeDetails.PKVoucherPayTypeNo = Convert.ToInt64(dtDelPayType.Rows[k].ItemArray[0].ToString());
                                            dbTVoucherEntry.DeleteTVoucherPayTypeDetails(tVchPayTypeDetails);
                                        }
                                    }
                                    else
                                    {
                                        tVchPayTypeDetails = new TVoucherPayTypeDetails();
                                        tVchPayTypeDetails.PKVoucherPayTypeNo = Convert.ToInt64(dtDelPayType.Rows[k].ItemArray[0].ToString());
                                        dbTVoucherEntry.DeleteTVoucherPayTypeDetails(tVchPayTypeDetails);
                                    }
                                    DataTable dtUpdateVoucher = ObjFunction.GetDataView("Select PKVoucherTrnNo,Debit,Credit From TVoucherDetails Where FKVoucherNo=" + dtDelPayType.Rows[k].ItemArray[2].ToString() + " AND CompanyNo=" + dtDelPayType.Rows[k].ItemArray[1].ToString() + "").Table;
                                    totamt = 0;
                                    bool alllowdel = false;
                                    for (int m = 0; m < dtUpdateVoucher.Rows.Count; m++)
                                    {
                                        double DrAmt = Convert.ToDouble(dtUpdateVoucher.Rows[m].ItemArray[1].ToString());
                                        double CrAmt = Convert.ToDouble(dtUpdateVoucher.Rows[m].ItemArray[2].ToString());
                                        if (DrAmt > 0) DrAmt = DrAmt - Convert.ToDouble(dtDelPayType.Rows[k].ItemArray[3].ToString());
                                        if (CrAmt > 0) CrAmt = CrAmt - Convert.ToDouble(dtDelPayType.Rows[k].ItemArray[3].ToString());
                                        dbTVoucherEntry.UpdateVoucherDetails(DrAmt, CrAmt, Convert.ToInt64(dtUpdateVoucher.Rows[m].ItemArray[0].ToString()));
                                        totamt = totamt + DrAmt + CrAmt;
                                        alllowdel = true;
                                    }
                                    if (totamt == 0 && alllowdel == true)
                                    {
                                        for (int m = 0; m < dtUpdateVoucher.Rows.Count; m++)
                                        {
                                            tVoucherDetails = new TVoucherDetails();
                                            tVoucherDetails.PkVoucherTrnNo = Convert.ToInt64(dtUpdateVoucher.Rows[m].ItemArray[0].ToString());
                                            dbTVoucherEntry.DeleteTVoucherDetails(tVoucherDetails);

                                            if (m == dtUpdateVoucher.Rows.Count - 1)
                                            {
                                                if (ObjQry.ReturnLong("Select Count(*) From TVoucherDetails Where FKVoucherNo=" + Convert.ToInt64(dtDelPayType.Rows[k].ItemArray[2].ToString()) + "", CommonFunctions.ConStr) >= dtUpdateVoucher.Rows.Count)
                                                {
                                                    tVoucherEntry = new TVoucherEntry();
                                                    tVoucherEntry.PkVoucherNo = Convert.ToInt64(dtDelPayType.Rows[k].ItemArray[2].ToString());
                                                    dbTVoucherEntry.DeleteTVoucherEntry1(tVoucherEntry);
                                                }
                                            }
                                        }
                                    }
                                }


                            }
                            tempid = dbTVoucherEntry.ExecuteNonQueryStatements();
                        }
                    }

                }

                if (tempid != 0)
                    return true;
                else
                    return false;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return false;
            }
        }

        private bool SavePaymentOld(DateTime dt, long PartID)
        {
            try
            {
                DataTable dtPayType = new DataTable();
                //int cntPayType = 1;

                long tempid = -1, ReceiptID = 0, VoucherUserNo = 0;

                for (int row = 1; row <= 4; row++) //for (int j = 0; j < dgPayType.Rows.Count; j++)
                {
                    if (row == 1 || row == 3 || row == 4)
                    {
                        if (Convert.ToDouble(dgPayType.Rows[row].Cells[2].Value) != 0)
                        {
                            if (row == 1 || row == 3 || row == 4)
                                ReceiptID = ObjQry.ReturnLong("SELECT TVoucherDetails.FkVoucherNo FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo " +
                                  " WHERE (TVoucherEntry.VoucherTypeCode = " + VchType.Payment + ") AND (TVoucherEntry.VoucherDate ='" + dt.ToString(Format.DDMMMYYYY) + "') AND (TVoucherDetails.LedgerNo = " + PartID + ") AND " +
                                    " (TVoucherDetails.VoucherSrNo = 1) AND (TVoucherEntry.PayTypeNo=" + Convert.ToInt64(dgPayType.Rows[row].Cells[1].Value) + ") ", CommonFunctions.ConStr);

                            DataTable dtReceipt = ObjFunction.GetDataView("SELECT PkVoucherTrnNo,LedgerNo,Debit,Credit FROM TVoucherDetails " +
                                " WHERE (FkVoucherNo = " + ReceiptID + ") order by VoucherSrNo ").Table;
                            VoucherUserNo = ObjQry.ReturnLong("Select IsNull((VoucherUserNo),0) From TVoucherEntry Where PkVoucherNo=" + ReceiptID + "", CommonFunctions.ConStr);
                            long VoucherSrNo = ObjQry.ReturnLong("select IsNull(max(VoucherSrNo),0)+1 from TVoucherDetails where (FkVoucherNo = " + ReceiptID + ")", CommonFunctions.ConStr);
                            double PrevAmt = 0;
                            for (int i = 0; i < dtVchPrev.Rows.Count; i++)
                                PrevAmt += Convert.ToDouble(dtVchPrev.Rows[i].ItemArray[1].ToString());
                            PrevAmt = (ObjQry.ReturnDouble("Select BilledAmount From TVoucherEntry Where  PkVoucherNo=" + ReceiptID + "", CommonFunctions.ConStr) - PrevAmt);// +Convert.ToDouble(txtGrandTotal.Text);
                            //int VoucherSrNo = 1;
                            dbTVoucherEntry = new DBTVaucherEntry();
                            //Voucher Header Entry
                            tVoucherEntry = new TVoucherEntry();
                            tVoucherEntry.PkVoucherNo = ReceiptID;
                            tVoucherEntry.VoucherTypeCode = VchType.Payment;
                            tVoucherEntry.VoucherUserNo = VoucherUserNo;
                            tVoucherEntry.VoucherDate = Convert.ToDateTime(dt.ToString("dd-MMM-yyyy"));
                            tVoucherEntry.VoucherTime = dtpBillTime.Value;
                            tVoucherEntry.Narration = "Payment Bill";
                            tVoucherEntry.Reference = "";
                            tVoucherEntry.ChequeNo = 0;
                            tVoucherEntry.ClearingDate = Convert.ToDateTime("01-Jan-1900");
                            tVoucherEntry.CompanyNo = DBGetVal.CompanyNo;
                            tVoucherEntry.BilledAmount = PrevAmt;// Convert.ToDouble(txtGrandTotal.Text);
                            tVoucherEntry.ChallanNo = "";
                            tVoucherEntry.Remark = txtRemark.Text.Trim();
                            tVoucherEntry.MacNo = DBGetVal.MacNo;
                            tVoucherEntry.PayTypeNo = ObjFunction.GetComboValue(cmbPaymentType);
                            tVoucherEntry.RateTypeNo = 0;
                            tVoucherEntry.TaxTypeNo = 0;
                            tVoucherEntry.OrderType = 2;


                            tVoucherEntry.UserID = DBGetVal.UserID;
                            tVoucherEntry.UserDate = DBGetVal.ServerTime.Date;
                            dbTVoucherEntry.AddTVoucherEntry(tVoucherEntry);

                            DataTable dtVoucherDetails = new DataTable();
                            if (ReceiptID != 0)
                            {
                                dtVoucherDetails = ObjFunction.GetDataView("Select PkVoucherTrnNo,LedgerNo,Debit,Credit From TVoucherDetails Where FkVoucherNo=" + ReceiptID + " order by VoucherSrNo").Table;
                                //dtPayType = ObjFunction.GetDataView("Select PKVoucherPayTypeNo From TVoucherPayTypeDetails Where FKSalesVoucherNo=" + SalesID + " AND FKPayTypeNo=" + dgPayType.Rows[row].Cells[1].Value + "  order by PKVoucherPayTypeNo").Table;
                            }

                            //for (int i = 0; i < dtCompRatio.Rows.Count; i++)
                            //{
                            //dtVoucherDetails = ObjFunction.GetDataView("Select PkVoucherTrnNo,LedgerNo,Debit,Credit,VoucherSrNo From TVoucherDetails TD,TVoucherEntry TC Where TC.PKVoucherNo=TD.FKVoucherNo AND TC.PayTypeNo=" + ObjFunction.GetComboValue(cmbPaymentType) + " AND TD.LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " AND  TD.FkVoucherNo=" + ReceiptID + " AND TD.CompanyNo=" + Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString()) + " order by TD.VoucherSrNo").Table;
                            dtVoucherDetails = ObjFunction.GetDataView("Select PkVoucherTrnNo,LedgerNo,Debit,Credit,VoucherSrNo From TVoucherDetails TD,TVoucherEntry TC Where TC.PKVoucherNo=TD.FKVoucherNo AND TC.PayTypeNo=" + ObjFunction.GetComboValue(cmbPaymentType) + " AND TD.LedgerNo=" + PartID + " AND  TD.FkVoucherNo=" + ReceiptID + "  order by TD.VoucherSrNo").Table;

                            double totamt = 0, amt = 0;

                            if (Convert.ToDouble(dgPayType.Rows[row].Cells[2].Value) != 0)
                            {

                                //amt = Convert.ToDouble(dgPayType.Rows[row].Cells[4 + (i + 1)].Value);
                                amt = Convert.ToDouble(dgPayType.Rows[row].Cells[4].Value);


                                totamt = totamt + amt;
                            }
                            amt = 0;
                            if (ID != 0 && PartyNo == PartID && PayType == ObjFunction.GetComboValue(cmbPaymentType)) amt = Convert.ToDouble(dtVchPrev.Select("CompanyNo=" + DBGetVal.CompanyNo + "")[0].ItemArray[1].ToString());//ObjQry.ReturnDouble("Select Sum(Debit) From TVoucherDetails Where LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " AND  CompanyNo=" + Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString()) + " ", CommonFunctions.ConStr);


                            //For Party Ledger
                            tVoucherDetails = new TVoucherDetails();
                            //tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                            tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > 0) ? Convert.ToInt64(dtVoucherDetails.Rows[0].ItemArray[0].ToString()) : 0;
                            if (tVoucherDetails.PkVoucherTrnNo == 0)
                                tVoucherDetails.VoucherSrNo = VoucherSrNo;
                            else tVoucherDetails.VoucherSrNo = Convert.ToInt64(dtVoucherDetails.Rows[0].ItemArray[4].ToString());
                            tVoucherDetails.SignCode = 1;//2;
                            tVoucherDetails.LedgerNo = PartID;
                            //tVoucherDetails.Debit = 0;
                            tVoucherDetails.Credit = 0;

                            double Newval = 0;
                            for (int k = 0; k < dgBill.Rows.Count - 1; k++)
                            {
                                //if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.StockCompanyNo].Value) == Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString()))
                                //{
                                if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.PkStockTrnNo].Value) == 0)
                                {
                                    Newval = Newval + Convert.ToDouble(dgBill.Rows[k].Cells[ColIndex.Amount].Value);
                                }
                                //}
                            }
                            totamt = totamt - Newval;
                            if (ID == 0)
                                tVoucherDetails.Debit = ((dtVoucherDetails.Rows.Count > 0) ? Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[2].ToString()) : 0) + Convert.ToDouble(dgPayType.Rows[row].Cells[4].Value);
                            else
                            {
                                if (dtVoucherDetails.Rows.Count > 0)
                                {
                                    if (PartID != ObjFunction.GetComboValue(cmbPartyName))
                                        tVoucherDetails.Debit = ((Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[2].ToString())));
                                    else
                                        tVoucherDetails.Debit = ((Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[2].ToString()) - amt));
                                }
                                else
                                    tVoucherDetails.Debit = totamt;
                            }
                            if (tVoucherDetails.PkVoucherTrnNo == 0) VoucherSrNo += 1;
                            tVoucherDetails.Narration = "";
                            tVoucherDetails.SrNo = Others.Party;
                            tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;//Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString());
                            dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);
                            //}



                            //for (int i = 0; i < dtCompRatio.Rows.Count; i++)
                            //{
                            totamt = 0;
                            if (Convert.ToDouble(dgPayType.Rows[row].Cells[2].Value) != 0)
                                //totamt += Convert.ToDouble(dgPayType.Rows[row].Cells[4 + (i + 1)].Value);
                                totamt += Convert.ToDouble(dgPayType.Rows[row].Cells[4].Value);
                            dtVoucherDetails = ObjFunction.GetDataView("Select PkVoucherTrnNo,LedgerNo,Debit,Credit,VoucherSrNo From TVoucherDetails Where LedgerNo=" + Convert.ToInt64(dtPayLedger.Select("PayTypeNo=" + Convert.ToInt64(dgPayType.Rows[row].Cells[1].Value) + " AND CompanyNo=" + DBGetVal.CompanyNo + "")[0][2].ToString()) + " AND  FkVoucherNo=" + ReceiptID + " AND CompanyNo=" + DBGetVal.CompanyNo + " order by VoucherSrNo").Table;


                            //For PayType Details
                            tVoucherDetails = new TVoucherDetails();
                            //tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > VoucherSrNo - 1) ? Convert.ToInt64(dtVoucherDetails.Rows[VoucherSrNo - 1].ItemArray[0].ToString()) : 0;
                            tVoucherDetails.PkVoucherTrnNo = (dtVoucherDetails.Rows.Count > 0) ? Convert.ToInt64(dtVoucherDetails.Rows[0].ItemArray[0].ToString()) : 0;
                            if (tVoucherDetails.PkVoucherTrnNo == 0)
                                tVoucherDetails.VoucherSrNo = VoucherSrNo;
                            else tVoucherDetails.VoucherSrNo = Convert.ToInt64(dtVoucherDetails.Rows[0].ItemArray[4].ToString());
                            tVoucherDetails.SignCode = 2;//1;
                            tVoucherDetails.LedgerNo = Convert.ToInt64(dtPayLedger.Select("PayTypeNo=" + Convert.ToInt64(dgPayType.Rows[row].Cells[1].Value) + " AND CompanyNo=" + DBGetVal.CompanyNo + "")[0][2].ToString());// Convert.ToInt64(dgPayType.Rows[row].Cells[3].Value);
                            Newval = 0;
                            for (int k = 0; k < dgBill.Rows.Count - 1; k++)
                            {
                                //if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.StockCompanyNo].Value) == Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString()))
                                //{
                                if (Convert.ToInt64(dgBill.Rows[k].Cells[ColIndex.PkStockTrnNo].Value) == 0)
                                {
                                    Newval = Newval + Convert.ToDouble(dgBill.Rows[k].Cells[ColIndex.Amount].Value);
                                }
                                //}
                            }
                            totamt = totamt - Newval;
                            amt = 0;
                            if (ID != 0 && PartyNo == PartID && PayType == ObjFunction.GetComboValue(cmbPaymentType)) amt = Convert.ToDouble(dtVchPrev.Select("CompanyNo=" + DBGetVal.CompanyNo + "")[0].ItemArray[1].ToString());//ObjQry.ReturnDouble("Select Sum(Debit) From TVoucherDetails Where LedgerNo=" + ObjFunction.GetComboValue(cmbPartyName) + " AND  CompanyNo=" + Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString()) + " ", CommonFunctions.ConStr);

                            if (ID == 0)
                                tVoucherDetails.Credit = ((dtVoucherDetails.Rows.Count > 0) ? Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[3].ToString()) : 0) + Convert.ToDouble(dgPayType.Rows[row].Cells[4].Value);
                            else
                            {
                                if (dtVoucherDetails.Rows.Count > 0)
                                {
                                    if (PartID != ObjFunction.GetComboValue(cmbPartyName))
                                        tVoucherDetails.Credit = ((Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[3].ToString())));
                                    else
                                        tVoucherDetails.Credit = ((Convert.ToDouble(dtVoucherDetails.Rows[0].ItemArray[3].ToString()) - amt));
                                }
                                else
                                    tVoucherDetails.Credit = totamt;
                            }

                            //tVoucherDetails.Credit = 0;
                            tVoucherDetails.Debit = 0;
                            if (tVoucherDetails.PkVoucherTrnNo == 0) VoucherSrNo += 1;
                            tVoucherDetails.Narration = "";
                            tVoucherDetails.CompanyNo = DBGetVal.CompanyNo;//Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString());
                            dbTVoucherEntry.AddTVoucherDetails(tVoucherDetails);


                            #region VoucherPay Type Details
                            ////tVchPayTypeDetails = new TVoucherPayTypeDetails();
                            ////tVchPayTypeDetails.PKVoucherPayTypeNo = (dtPayType.Rows.Count > cntPayType - 1) ? Convert.ToInt64(dtPayType.Rows[cntPayType - 1].ItemArray[0].ToString()) : 0; cntPayType += 1;
                            ////tVchPayTypeDetails.FKSalesVoucherNo = SalesID;
                            ////tVchPayTypeDetails.FKPayTypeNo = Convert.ToInt64(dgPayType.Rows[row].Cells[1].Value);
                            ////tVchPayTypeDetails.Amount = Convert.ToDouble(dgPayType.Rows[row].Cells[4].Value);
                            ////tVchPayTypeDetails.CompanyNo = DBGetVal.CompanyNo;//Convert.ToInt64(dtCompRatio.Rows[i].ItemArray[0].ToString());
                            ////dbTVoucherEntry.AddTVoucherPayTypeDetails(tVchPayTypeDetails);
                            //////}

                            ////if ((PartyNo != ObjFunction.GetComboValue(cmbPartyName) || PayType != ObjFunction.GetComboValue(cmbPaymentType)) && ID != 0)
                            ////{
                            ////    DataTable dtDelPayType = ObjFunction.GetDataView("Select PKVoucherPayTypeNo,TVoucherPayTypeDetails.CompanyNo,FKReceiptVoucherNo,Amount From TVoucherPayTypeDetails,TVoucherDetails Where TVoucherDetails.PkVoucherTrnNo=TVoucherPayTypeDetails.FKVoucherTrnNo AND FKSalesVoucherNo=" + SalesID + "  order by PKVoucherPayTypeNo").Table;
                            ////    for (int k = 0; k < dtDelPayType.Rows.Count; k++)
                            ////    {
                            ////        if (dtPayType.Rows.Count > 0)
                            ////        {
                            ////            if (dtPayType.Select("PKVoucherPayTypeNo=" + dtDelPayType.Rows[k].ItemArray[0].ToString())[0][0].ToString() != dtDelPayType.Rows[k].ItemArray[0].ToString())
                            ////            {
                            ////                tVchPayTypeDetails = new TVoucherPayTypeDetails();
                            ////                tVchPayTypeDetails.PKVoucherPayTypeNo = Convert.ToInt64(dtDelPayType.Rows[k].ItemArray[0].ToString());
                            ////                dbTVoucherEntry.DeleteTVoucherPayTypeDetails(tVchPayTypeDetails);
                            ////            }
                            ////        }
                            ////        else
                            ////        {
                            ////            tVchPayTypeDetails = new TVoucherPayTypeDetails();
                            ////            tVchPayTypeDetails.PKVoucherPayTypeNo = Convert.ToInt64(dtDelPayType.Rows[k].ItemArray[0].ToString());
                            ////            dbTVoucherEntry.DeleteTVoucherPayTypeDetails(tVchPayTypeDetails);
                            ////        }
                            ////        DataTable dtUpdateVoucher = ObjFunction.GetDataView("Select PKVoucherTrnNo,Debit,Credit From TVoucherDetails Where FKVoucherNo=" + dtDelPayType.Rows[k].ItemArray[2].ToString() + " AND CompanyNo=" + dtDelPayType.Rows[k].ItemArray[1].ToString() + "").Table;
                            ////        totamt = 0;
                            ////        bool alllowdel = false;
                            ////        for (int m = 0; m < dtUpdateVoucher.Rows.Count; m++)
                            ////        {
                            ////            double DrAmt = Convert.ToDouble(dtUpdateVoucher.Rows[m].ItemArray[1].ToString());
                            ////            double CrAmt = Convert.ToDouble(dtUpdateVoucher.Rows[m].ItemArray[2].ToString());
                            ////            if (DrAmt > 0) DrAmt = DrAmt - Convert.ToDouble(dtDelPayType.Rows[k].ItemArray[3].ToString());
                            ////            if (CrAmt > 0) CrAmt = CrAmt - Convert.ToDouble(dtDelPayType.Rows[k].ItemArray[3].ToString());
                            ////            dbTVoucherEntry.UpdateVoucherDetails(DrAmt, CrAmt, Convert.ToInt64(dtUpdateVoucher.Rows[m].ItemArray[0].ToString()));
                            ////            totamt = totamt + DrAmt + CrAmt;
                            ////            alllowdel = true;
                            ////        }
                            ////        if (totamt == 0 && alllowdel == true)
                            ////        {
                            ////            for (int m = 0; m < dtUpdateVoucher.Rows.Count; m++)
                            ////            {
                            ////                tVoucherDetails = new TVoucherDetails();
                            ////                tVoucherDetails.PkVoucherTrnNo = Convert.ToInt64(dtUpdateVoucher.Rows[m].ItemArray[0].ToString());
                            ////                dbTVoucherEntry.DeleteTVoucherDetails(tVoucherDetails);

                            ////                if (m == dtUpdateVoucher.Rows.Count - 1)
                            ////                {
                            ////                    if (ObjQry.ReturnLong("Select Count(*) From TVoucherDetails Where FKVoucherNo=" + Convert.ToInt64(dtDelPayType.Rows[k].ItemArray[2].ToString()) + "", CommonFunctions.ConStr) >= dtUpdateVoucher.Rows.Count)
                            ////                    {
                            ////                        tVoucherEntry = new TVoucherEntry();
                            ////                        tVoucherEntry.PkVoucherNo = Convert.ToInt64(dtDelPayType.Rows[k].ItemArray[2].ToString());
                            ////                        dbTVoucherEntry.DeleteTVoucherEntry1(tVoucherEntry);
                            ////                    }
                            ////                }
                            ////            }
                            ////        }
                            ////    }


                            ////}
                            #endregion
                            tempid = dbTVoucherEntry.ExecuteNonQueryStatements();
                        }
                    }

                }

                if (tempid != 0)
                    return true;
                else
                    return false;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
                return false;
            }
        }

        private void btnMixMode_Click(object sender, EventArgs e)
        {
            try
            {
                if (ID != 0)
                {
                    if (ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo in ( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + ")", CommonFunctions.ConStr) > 1)
                    {
                        btnUpdate.Visible = false;
                        btnMixMode.Visible = false;
                        btndelete.Visible = false;
                        OMMessageBox.Show("Already this bill Payment is done", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        return;
                    }
                    else
                    {
                        if (ObjQry.ReturnInteger("Select Count(*) From TVoucherRefDetails Where RefNo in ( Select TR.RefNo From TVoucherRefDetails TR,TVoucherDetails TD Where TD.PKVoucherTrnNo=TR.FKVoucherTrnNo AND TD.FkVoucherNo=" + ID + " and TR.TypeOfRef in(6))", CommonFunctions.ConStr) > 1)
                        {
                            btnUpdate.Visible = false;
                            btnMixMode.Visible = false;
                            btndelete.Visible = false;
                            OMMessageBox.Show("Already this bill Payment is done", CommonFunctions.ErrorTitle, OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                            return;
                        }
                    }

                    partialPay = new Vouchers.PartialPurchasePayment(ID, Convert.ToDouble(txtGrandTotal.Text.Trim()), ObjFunction.GetComboValue(cmbPaymentType), ObjFunction.GetComboValue(cmbPartyName));
                    ObjFunction.OpenForm(partialPay);
                    partialPay.Left = 15;
                    if (((PartialPurchasePayment)partialPay).DS == DialogResult.Cancel)
                    {
                        if (MixModeFlag == true)
                            ObjTrans.Execute("Update TVoucherEntry set MixMode=0 Where PKVoucherNo=" + ID + "", CommonFunctions.ConStr);
                        return;
                    }
                    else if (((PartialPurchasePayment)partialPay).DS == DialogResult.OK)
                    {
                        partialPay.SaveData(ID);
                        FillControls();
                    }
                }
                else
                {
                    MixModeVal = 1;
                    MixModeFlag = true;
                    partialPay = new Vouchers.PartialPurchasePayment(ID, Convert.ToDouble(txtGrandTotal.Text.Trim()), ObjFunction.GetComboValue(cmbPaymentType), ObjFunction.GetComboValue(cmbPartyName));
                    ObjFunction.OpenForm(partialPay);
                    partialPay.Left = 15;
                    if (((PartialPurchasePayment)partialPay).DS == DialogResult.Cancel)
                    {
                        MixModeVal = 0;
                        //if (MixModeFlag == true)
                        //    ObjTrans.Execute("Update TVoucherEntry set MixMode=0 Where PKVoucherNo=" + ID + "", CommonFunctions.ConStr);
                        //return;
                    }
                    else if (((PartialPurchasePayment)partialPay).DS == DialogResult.OK)
                    {
                        cmbPaymentType.SelectedValue = "3";
                        btnSave_Click(sender, e);
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbPartyName_Leave(object sender, EventArgs e)
        {
            try
            {
                if (ObjFunction.GetComboValue(cmbPartyName) == Convert.ToInt64(ObjFunction.GetAppSettings(AppSettings.P_PartyAC)))// && (ObjFunction.GetComboValue(cmbPaymentType) == 3))
                {
                    if (ID == 0)
                    {
                        cmbPaymentType.SelectedValue = 2;
                        //cmbPaymentType.Enabled = false;
                    }
                }
                else
                {
                    if (ID == 0)
                    {
                        cmbPaymentType.SelectedValue = 3;
                        cmbPaymentType.Enabled = true;
                    }
                    if (Convert.ToBoolean(ObjFunction.GetAppSettings(AppSettings.P_AutoMFGMapping)) == true)
                        CheckMFGCompany();
                }
                if (ID == 0) tempPartyNo = ObjFunction.GetComboValue(cmbPartyName);
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void cmbPaymentType_Leave(object sender, EventArgs e)
        {
            cmbPaymentType_KeyDown(sender, new KeyEventArgs(Keys.Enter));
        }

        private void CheckMFGCompany()
        {
            if (ObjFunction.GetComboValue(cmbPartyName) != 0)
            {
                bool flag = false;
                DBMManufacturerCompany dbMManufacturerCompany = new DBMManufacturerCompany();
                MManufacturerDetails mManufactuerDetails = new MManufacturerDetails();

                //string sql = " Select * From  (Select ManufacturerNo From MManufacturerDetails  Where LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + "  ) As Table1 " +
                //           " EXCEPT " +
                //           " Select * From (SELECT DISTINCT MManufacturerCompany.MfgCompNo FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo INNER JOIN TStock ON TVoucherEntry.PkVoucherNo = TStock.FKVoucherNo INNER JOIN " +
                //           " MStockItems ON TStock.ItemNo = MStockItems.ItemNo INNER JOIN MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo INNER JOIN MManufacturerCompany ON MStockGroup.MfgCompNo = MManufacturerCompany.MfgCompNo " +
                //           " WHERE (TVoucherEntry.VoucherTypeCode = " + VchType.Purchase + ") AND (TVoucherEntry.VoucherDate ='" + DBGetVal.ServerTime.Date + "' ) AND (TVoucherDetails.VoucherSrNo = 1) AND  (TVoucherDetails.LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + ")) AS Table2 " +
                //           " Union " +
                //           " Select * From (SELECT DISTINCT MManufacturerCompany.MfgCompNo FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo INNER JOIN TStock ON TVoucherEntry.PkVoucherNo = TStock.FKVoucherNo INNER JOIN " +
                //           " MStockItems ON TStock.ItemNo = MStockItems.ItemNo INNER JOIN MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo INNER JOIN MManufacturerCompany ON MStockGroup.MfgCompNo = MManufacturerCompany.MfgCompNo " +
                //           " WHERE (TVoucherEntry.VoucherTypeCode = " + VchType.Purchase + ") AND (TVoucherEntry.VoucherDate = '" + DBGetVal.ServerTime.Date + "') AND (TVoucherDetails.VoucherSrNo = 1) AND (TVoucherDetails.LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + ")) AS Table2 " +
                //           " EXCEPT " +
                //           " Select * From  (Select ManufacturerNo From MManufacturerDetails  Where LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + "  ) As Table1 ";

                string sql = "SELECT DISTINCT MManufacturerCompany.MfgCompNo FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo INNER JOIN TStock ON TVoucherEntry.PkVoucherNo = TStock.FKVoucherNo INNER JOIN " +
                           " MStockItems ON TStock.ItemNo = MStockItems.ItemNo INNER JOIN MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo INNER JOIN MManufacturerCompany ON MStockGroup.MfgCompNo = MManufacturerCompany.MfgCompNo " +
                           " WHERE (TVoucherEntry.VoucherTypeCode = " + VchType.Purchase + ") AND (TVoucherEntry.VoucherDate ='" + DBGetVal.ServerTime.Date + "' ) AND (TVoucherDetails.VoucherSrNo = 1) AND  (TVoucherDetails.LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + ") " +
                           " And MManufacturerCompany.MfgCompNo Not In(Select ManufacturerNo From MManufacturerDetails  Where LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName) + ") ";


                DataTable dt = new DataTable();
                dt = ObjFunction.GetDataView(sql).Table;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    mManufactuerDetails = new MManufacturerDetails();
                    mManufactuerDetails.LedgerNo = ObjFunction.GetComboValue(cmbPartyName);
                    mManufactuerDetails.PkSrNo = 0;
                    mManufactuerDetails.ManufacturerNo = Convert.ToInt64(dt.Rows[i].ItemArray[0].ToString());
                    mManufactuerDetails.UserID = DBGetVal.UserID;
                    mManufactuerDetails.CompanyNo = DBGetVal.CompanyNo;
                    mManufactuerDetails.UserID = DBGetVal.UserID;
                    mManufactuerDetails.UserDate = DBGetVal.ServerTime.Date;
                    dbMManufacturerCompany.AddMManufacturerDetails(mManufactuerDetails);
                    flag = true;
                }
                if (flag == true)
                {
                    dbMManufacturerCompany.ExecuteNonQueryStatements();
                }
            }
        }

        private void NewItemAdd(string BarCode)
        {
            if (ObjFunction.CheckAllowMenu(10) == false) return;
            Form NewF = new Master.StockItemSAE(-1, BarCode);
            ObjFunction.OpenForm(NewF);

            if (((Master.StockItemSAE)NewF).ShortID != 0)
            {
                string barcode = ObjQry.ReturnString("Select BarCode From MStockBarCode where ItemNo=" + ((Master.StockItemSAE)NewF).ShortID + "", CommonFunctions.ConStr);
                int rwindex = dgBill.CurrentCell.RowIndex;
                dgBill.CurrentRow.Cells[ColIndex.ItemName].Value = barcode;
                dgBill_CellEndEdit(dgBill, new DataGridViewCellEventArgs(ColIndex.ItemName, rwindex));
                bw_ItemSearch.RunWorkerAsync();
            }
        }

        private void dtpBillDate_Leave(object sender, EventArgs e)
        {
            if (ID == 0)
            {
                tempDate = dtpBillDate.Value.Date;
                ObjFunction.GetFinancialYear(dtpBillDate.Value, out dtFrom, out dtTo);
                txtInvNo.Text = (ObjQry.ReturnLong("Select max(VoucherUserNo) from TVoucherEntry Where VoucherTypeCode=" + VoucherType + " AND VoucherDate>='" + dtFrom.Date + "' AND VoucherDate<='" + dtTo.Date + "'", CommonFunctions.ConStr) + 1).ToString();
            }
        }

        private void dgInvSearch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    long tempNo;
                    e.SuppressKeyPress = true;
                    tempNo = Convert.ToInt64(dgInvSearch.Rows[dgInvSearch.CurrentRow.Index].Cells[5].Value);
                    if (tempNo > 0)
                    {
                        ID = tempNo;
                        SetNavigation();
                        FillControls();
                        btnNew.Enabled = true;
                        btnUpdate.Enabled = true;
                        pnlPartySearch.Visible = false;
                        pnlInvSearch.Visible = false;
                        btnNew.Focus();
                        SearchVisible(false);
                    }
                    else
                    {
                        txtInvNoSearch.Text = "";
                        txtSearch.Text = "";
                        cmbPartyName.SelectedIndex = 0;
                        DisplayMessage("Bill Not Found");
                        txtSearch.Focus();
                        //SearchVisible(false);
                    }

                }
                else if (e.KeyCode == Keys.Escape)
                {
                    e.SuppressKeyPress = true;
                    pnlInvSearch.Visible = false;
                    pnlSearch.Visible = true;
                    txtSearch.Focus();
                    rbType_CheckedChanged(sender, new EventArgs());

                }
                txtInvNoSearch.Text = "";
                txtSearch.Text = "";
                cmbPartyNameSearch.SelectedIndex = 0;
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }

        private void dgInvSearch_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                e.Value = e.RowIndex + 1;

            }
            else if (e.ColumnIndex == 3)
            {
                e.Value = Convert.ToDateTime(e.Value).ToString("dd-MMM-yy");
            }
        }

        private void txtOtherTax_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked(txtOtherTax, 2, 5, JitFunctions.MaskedType.NotNegative);
        }

        private void txtOtherTax_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                Control_Leave((object)txtOtherTax, new EventArgs());
                txtReturnAmt.Focus();
            }
        }

        private void Cal_Total()
        {
            double totdisc = 0;
            double discAmt = Convert.ToDouble(txtDistDisc.Text);
            if (discAmt != 0)
                discAmt = discAmt / (dgBill.Rows.Count - 1);

            for (int i = 0; i < dgBill.Rows.Count - 1; i++)
            {
                dgBill.Rows[i].Cells[ColIndex.DiscAmount2].Value = discAmt.ToString("0.00");
            }
            CalculateTotal();
        }

        private void txtDistDisc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {

                IsAllowDisc = !IsAllowDisc;
                txtDistDisc.ReadOnly = !IsAllowDisc;
            }
            else if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                txtChrgRupees1.Focus();
            }
        }

        private void txtDistDisc_Leave(object sender, EventArgs e)
        {
            if (IsAllowDisc == true)
            {
                Cal_Total();
                IsAllowDisc = !IsAllowDisc;
                txtDistDisc.ReadOnly = true;
            }

        }

        private void cmbPartyName_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cmbPartyName.Items.Count > 1)
            {
                if (ObjFunction.GetComboValue(cmbPartyName) > 0)
                {
                    long PartyStateNo = ObjQry.ReturnLong("Select (Case When(ISNull(StateNo,0)=0) Then " + LocalStateNo + " Else StateNo end)  " +
                            " From MLedgerDetails WHERE LedgerNo = " + ObjFunction.GetComboValue(cmbPartyName), CommonFunctions.ConStr);
                    if (PartyStateNo == 0)
                        PartyStateNo = LocalStateNo;

                    isLocalPurchase = PartyStateNo == LocalStateNo;
                }
            }
        }

        private void txtBrandFilter_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                e.SuppressKeyPress = true;
                lstGroup1.Focus();
            }
            else if (e.KeyCode == Keys.Space)
            {
                dgBill.Focus();
                pnlGroup1.Visible = false;
                txtBrandFilter.Text = "";
            }
            else if (e.KeyCode == Keys.Escape)
            {
                e.SuppressKeyPress = true;
                dgBill.Focus();
                txtBrandFilter.Text = "";
                pnlGroup1.Visible = false;

            }
            else if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (lstGroup1.Text != "")
                {
                    pnlGroup1.Visible = false;
                    FillItemList(strItemQuery.Length);
                }

            }
        }

        private void txtBrandFilter_TextChanged(object sender, EventArgs e)
        {
            if (txtBrandFilter.Text != "" && BrandFilter != "")
            {
                string str = BrandFilter;
                str = str.Replace("IsActive='true'", "IsActive='true' And  StockGroupName Like '" + txtBrandFilter.Text.Trim() + "'+'%'");
                ObjFunction.FillList(lstGroup1, str);
            }
            else
            {
                if (BrandFilter != "")
                {
                    ObjFunction.FillList(lstGroup1, BrandFilter);
                }
            }
        }

        private void GetByItemSearch()
        {
            string sql = null;
            sql = " SELECT MStockItems.ShowItemName, MUOM.UOMName AS UOM, MStockBarcode.Barcode,(CONVERT(DECIMAL(10,2)," + strRateType + "))  As Rate ," +
                  " (CONVERT(DECIMAL(10,2), MRateSetting.MRP)) AS MRP, " +
                 " IsNull(MStockItemBalance.CurrentStock,0) AS CurrentStock, MStockItems.ItemNo  " +
                 " FROM MStockItems INNER JOIN " +
                 " MUOM ON MStockItems.UOMDefault = MUOM.UOMNo INNER JOIN " +
                 " MStockBarcode ON MStockItems.ItemNo = MStockBarcode.ItemNo INNER JOIN " +
                 " MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo LEFT OUTER JOIN " +
                 " MStockItemBalance ON MRateSetting.MRP = MStockItemBalance.MRP AND MRateSetting.ItemNo = MStockItemBalance.ItemNo " +
                 " WHERE MStockItems.ItemNo <> 1 " +
                 " AND MStockItems.IsActive='true' " +
                 " ORDER BY MStockItems.ShowItemName";

            ds_ItemSearch = new DataSet();
            /*13-Apr-2014
            * Exception Handling Purpose.
            * */
            try
            {
                Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
                ds_ItemSearch = ObjDset.FillDset("New", sql, CommonFunctions.ConStr);

            }
            catch (Exception e)
            {
                CommonFunctions.ErrorMessge = e.Message;
            }

        }

        private void bw_ItemSearch_DoWork(object sender, DoWorkEventArgs e)
        {
            GetByItemSearch();
        }

        #region Import Purchase Bill

        private void ShowParkingBill()
        {
            Vouchers.ParkingBillAE itemSearch = new Vouchers.ParkingBillAE(VchType.Purchase);
            ObjFunction.OpenForm(itemSearch);
            if (itemSearch.ParkBillNo != 0)
            {

                DataTable dtDetails = ObjFunction.GetDataView("Select LedgerNo,Discount,Charges,Remark,RateTypeNo,TaxTypeNo,InvNo,BillDate " +
                    " From TParkingBill Where ParkingBillNo=" + itemSearch.ParkBillNo + "").Table;
                if (dtDetails.Rows.Count > 0)
                {
                    cmbPartyName.SelectedValue = dtDetails.Rows[0][0].ToString();
                    txtDiscRupees1.Text = dtDetails.Rows[0][1].ToString();
                    txtChrgRupees1.Text = dtDetails.Rows[0][2].ToString();
                    txtRemark.Text = dtDetails.Rows[0][3].ToString();
                    SetRateType(Convert.ToInt64(dtDetails.Rows[0][4].ToString()));
                    cmbTaxType.SelectedValue = dtDetails.Rows[0][5].ToString();
                    txtRefNo.Text = dtDetails.Rows[0]["InvNo"].ToString();
                    dtpBillDate.Value = Convert.ToDateTime(dtDetails.Rows[0]["BillDate"].ToString());
                    dtpBillTime.Value = Convert.ToDateTime(dtDetails.Rows[0]["BillDate"].ToString());

                }
                DisplayParkingDetails(itemSearch.ParkBillNo);
                dgBill.Focus();
                dgBill.CurrentCell = dgBill[ColIndex.ItemName, dgBill.Rows.Count - 1];
                ParkBillNo = Convert.ToInt64(itemSearch.ParkBillNo);
            }
        }

        private void Import_Bill_IN_Excel_File()
        {
            try
            {
                DataTable dt = copyProductDataFromExcelToDB(@"C:\Users\admin\Desktop\Ma\Book1.xls");
                var x = (from r in dt.AsEnumerable()
                         select r["Trans_no"]).Distinct().ToList();
                foreach (var id in x)
                {
                    DataRow[] dr = dt.Select("Trans_no=" + Convert.ToInt64(id) + "");

                    if (ObjQry.ReturnLong("Select Count(*) From TParkingBill Where InvNo='" + dr[0]["Bill_no"] + "'", CommonFunctions.ConStr) != 0)
                        continue;

                    var BarcodeCnt = (from r in dr.AsEnumerable()
                                      select r["Bar_code"]).Distinct().ToList();

                    string StrBarcode = "";
                    foreach (DataRow data in dr)
                    {
                        StrBarcode += "," + "'" + data["Bar_code"].ToString() + "'";
                    }
                    StrBarcode = StrBarcode.Substring(1);

                    if (ObjQry.ReturnLong("Select Count(*) From MStockBarcode Where Barcode in(" + StrBarcode + ") ", CommonFunctions.ConStr) != BarcodeCnt.Count)
                    {
                        continue;
                    }

                    DBTVaucherEntry db = new DBTVaucherEntry();
                    TParkingBill_Mob tp = new TParkingBill_Mob();
                    bool flag = false;
                    for (int i = 0; i < dr.Count(); i++)
                    {
                        if (i == 0)
                        {
                            flag = true;
                            tp = new TParkingBill_Mob();
                            tp.BillDate = Convert.ToDateTime(dr[i]["Tran_Date"].ToString());
                            tp.InvNo = dr[i]["Bill_no"].ToString();
                            tp.BillTime = Convert.ToDateTime(dr[i]["Tran_Date"].ToString());
                            tp.Discount = Convert.ToDouble(dr[i]["sub1"].ToString()) + Convert.ToDouble(dr[i]["sub2"].ToString());
                            tp.CompanyNo = 1;
                            tp.Charges = Convert.ToDouble(dr[i]["add1"].ToString()) + Convert.ToDouble(dr[i]["add2"].ToString());
                            tp.FKVoucherNo = 0;
                            tp.IsBill = false;
                            tp.IsCancel = false;
                            tp.LedgerNo = SaveLedgers(GroupType.SundryCreditors, dr[i]["Sup_name"].ToString(), dr[i]["Sup_mob"].ToString(), dr[i]["Sup_add1"].ToString(), dr[i]["Sup_pin"].ToString());
                            tp.PersonName = dr[i]["Sup_name"].ToString();
                            tp.RateTypeNo = 0;
                            tp.Remark = "Purchase Import From Old Software";
                            tp.StatusNo = 0;
                            tp.TaxTypeNo = 38;
                            tp.UserID = 1;
                            tp.UserDate = DateTime.Now;
                            tp.VoucherTypeCode = VchType.Purchase;
                            db.AddTParkingBill_Mob(tp);
                        }

                        TParkingBillDetails tpd = new TParkingBillDetails();
                        DataTable dts = ObjFunction.GetDataView(" SELECT  Top(1)   MStockBarcode.ItemNo, MRateSetting.PkSrNo, MRateSetting.UOMNo " +
                                                            " FROM MStockBarcode INNER JOIN MRateSetting ON MStockBarcode.PkStockBarcodeNo = MRateSetting.FkBcdSrNo " +
                                                            " Where MStockBarcode.Barcode='" + dr[i]["Bar_code"].ToString() + "' ").Table;
                        if (dts.Rows.Count == 0)
                        {
                            //throw new Exception("Barcode Not Found..");
                            db = new DBTVaucherEntry();

                            continue;
                        }

                        tpd.Barcode = dr[i]["Bar_code"].ToString();
                        tpd.CompanyNo = 1;
                        tpd.ItemDisc = Convert.ToDouble(dr[i]["dis"].ToString()) + Convert.ToDouble(dr[i]["dis_sch"].ToString());
                        tpd.ItemNo = Convert.ToInt64(dts.Rows[0]["ItemNo"].ToString());
                        tpd.FkRateSettingNo = Convert.ToInt64(dts.Rows[0]["PkSrNo"].ToString());
                        tpd.UOMNo = Convert.ToInt64(dts.Rows[0]["UOMNo"].ToString());
                        tpd.Qty = Convert.ToDouble(dr[i]["Qty"].ToString());
                        tpd.Rate = Convert.ToDouble(dr[i]["Pur_Rate"].ToString());
                        db.AddTParkingBillDetails(tpd);
                    }
                    if (flag == true)
                    {
                        db.ExecuteNonQueryStatements_Parking();
                    }


                }
            }
            catch (Exception ex)
            {
                OMMessageBox.Show(ex.Message);
            }
        }

        private DataTable copyProductDataFromExcelToDB(string strFileName)
        {
            System.Data.OleDb.OleDbConnection olecon = null;
            try
            {
                string connstring;
                if (strFileName.EndsWith("xls", StringComparison.CurrentCultureIgnoreCase))
                {
                    connstring = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                           "Data Source=" + strFileName + ";Extended Properties=\"Excel 8.0; HDR=Yes; IMEX=1;\";";
                }
                else if (strFileName.EndsWith("xlsx", StringComparison.CurrentCultureIgnoreCase))
                {
                    connstring = "Provider=Microsoft.ACE.OLEDB.12.0;" +
                           "Data Source=" + strFileName + ";Extended Properties=\"Excel 12.0 xml; HDR=Yes; IMEX=1;\";";
                }
                else
                {
                    throw new Exception("Invalid file format or extention. Only *.xls and *.xlsx files supported.");
                }

                olecon = new System.Data.OleDb.OleDbConnection(connstring);
                olecon.Open();

                DataTable schemaTable = olecon.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                string strTableName = "[" + schemaTable.Rows[0].ItemArray[2].ToString() + "]";

                System.Data.OleDb.OleDbDataAdapter olecomm1 = new System.Data.OleDb.OleDbDataAdapter("SELECT * FROM " + strTableName, olecon);

                DataSet ds = new DataSet();
                olecomm1.Fill(ds);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                //ObjFunction.WriteMessage("Error while copying record(s) to temp table !!! Error : " + ex.Message);
                throw ex;
            }
            finally
            {
                if (olecon != null) olecon.Close();
            }
        }

        private long SaveLedgers(long GroupNo, string Name, string MobileNo, string Address, string GSTNo)
        {
            long cnt = ObjQry.ReturnLong("Select LedgerNo From MLedger Where LedgerName='" + Name + "' and GroupNo=" + GroupNo + " ", CommonFunctions.ConStr);
            if (cnt != 0)
                return cnt;

            DBMLedger dbMLedger = new DBMLedger();
            MLedger mLedger = new MLedger();
            mLedger.LedgerNo = 0;
            mLedger.LedgerUserNo = "";
            mLedger.ContactPerson = "";
            mLedger.IsActive = true;
            mLedger.InvFlag = true;
            //  mLedger.IsEnroll = true;
            mLedger.MaintainBillByBill = false;
            //  mLedger.LangLedgerName = "";
            mLedger.GroupNo = GroupNo;
            mLedger.LedgerName = Name.Trim();
            // mLedger.CompanyNo = DBGetVal.CompanyNo;
            mLedger.UserID = DBGetVal.UserID;
            mLedger.UserDate = DateTime.Now;
            dbMLedger.AddMLedger(mLedger);

            MLedgerDetails mLedgerDetails = new MLedgerDetails();
            mLedgerDetails.StateNo = 27;
            mLedgerDetails.MobileNo1 = MobileNo;
            mLedgerDetails.Address = "";
            mLedgerDetails.CSTNo = GSTNo;
            mLedgerDetails.PinCode = "";
            mLedgerDetails.PhNo1 = "";
            mLedgerDetails.PhNo2 = "";
            mLedgerDetails.MobileNo2 = "";
            mLedgerDetails.EmailID = "";
            mLedgerDetails.DOB = Convert.ToDateTime("1-1-1900");
            mLedgerDetails.PANNo = "";
            mLedgerDetails.VATNo = "";
            // mLedgerDetails.LangAddress = mLedgerDetails.Address;
            mLedgerDetails.UserID = DBGetVal.UserID;
            mLedgerDetails.UserDate = DBGetVal.ServerTime.Date;
            dbMLedger.AddMLedgerDetails(mLedgerDetails);

            if (dbMLedger.ExecuteNonQueryStatements() == false)
            {
                throw new Exception("Ledger Not Save :" + DBMStockItems.strerrormsg);
            }
            else
            {
                return ObjQry.ReturnLong("Select LedgerNo From MLedger Where LedgerName='" + Name + "' and GroupNo=" + GroupNo + " ", CommonFunctions.ConStr);
            }

        }

        private void DisplayParkingDetails(long ParkingBillNo)
        {
            try
            {
                string strQuery = "SELECT    MStockItems.ShowItemName AS ItemName, TParkingBillDetails.Qty, MUOM.UOMName, TParkingBillDetails.Rate, MStockBarcode.Barcode,MStockBarcode.PkStockBarcodeNo,MStockItems.ItemNo, MUOM.UOMNo,   " +
                " GetItemTaxAll_1.TaxLedgerNo, GetItemTaxAll_1.SalesLedgerNo, MRateSetting.PkSrNo, GetItemTaxAll_1.PkSrNo AS Expr1,  " +
                " MRateSetting.StockConversion, MRateSetting.MKTQty, GetItemTaxAll_1.Percentage,TParkingBillDetails.ItemDisc,MStockItems.CompanyNo, MStockItems_1.DiscountType,MStockItems.GodownNo, MStockItems_1.HamaliInKg, " +
                " GetItemTaxAll_1.HSNNo, GetItemTaxAll_1.HSNCode, GetItemTaxAll_1.FkTaxSettingNo,MRateSetting.MRP, " +
                " GetItemTaxAll_1.IGSTPercent, GetItemTaxAll_1.CGSTPercent, GetItemTaxAll_1.SGSTPercent, " +
                " GetItemTaxAll_1.UTGSTPercent, GetItemTaxAll_1.CessPercent " +
                " FROM  MStockItems INNER JOIN " +
                " TParkingBillDetails ON MStockItems.ItemNo = TParkingBillDetails.ItemNo INNER JOIN MUOM ON TParkingBillDetails.UOMNo = MUOM.UOMNo INNER JOIN " +
                " MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo AND TParkingBillDetails.FkRateSettingNo = MRateSetting.PkSrNo INNER JOIN dbo.GetItemTaxAll(NULL, NULL, " + GroupType.SalesAccount + ", " + ObjFunction.GetComboValueString(cmbTaxType) + ", NULL) AS GetItemTaxAll_1 ON MStockItems.ItemNo = GetItemTaxAll_1.ItemNo INNER JOIN " +
                " MStockBarcode ON MStockItems.ItemNo = MStockBarcode.ItemNo    INNER JOIN MStockItems AS MStockItems_1 ON MStockItems.ItemNo = MStockItems_1.ItemNo " +
                " WHERE     (TParkingBillDetails.ParkingBillNo = " + ParkingBillNo + ") order by TParkingBillDetails.PkSrNo";

                DataTable dtP = ObjFunction.GetDataView(strQuery).Table;
                for (int i = 0; i < dtP.Rows.Count; i++)
                {
                    int index = dgBill.Rows.Count - 1;
                    dgBill.Rows[index].Cells[ColIndex.SrNo].Value = (index + 1);
                    dgBill.Rows[index].Cells[ColIndex.ItemName].Value = dtP.Rows[i].ItemArray[0].ToString();
                    dgBill.Rows[index].Cells[ColIndex.Quantity].Value = dtP.Rows[i].ItemArray[1].ToString();
                    dgBill.Rows[index].Cells[ColIndex.UOM].Value = dtP.Rows[i].ItemArray[2].ToString();
                    dgBill.Rows[index].Cells[ColIndex.Rate].Value = Convert.ToDouble(dtP.Rows[i].ItemArray[3].ToString()).ToString("0.000");
                    dgBill.Rows[index].Cells[ColIndex.NetRate].Value = "0";
                    dgBill.Rows[index].Cells[ColIndex.DiscPercentage].Value = "0";
                    dgBill.Rows[index].Cells[ColIndex.DiscAmount].Value = "0";
                    dgBill.Rows[index].Cells[ColIndex.DiscRupees].Value = dtP.Rows[i].ItemArray[15].ToString();
                    dgBill.Rows[index].Cells[ColIndex.DiscPercentage2].Value = "0";
                    dgBill.Rows[index].Cells[ColIndex.DiscAmount2].Value = "0";
                    dgBill.Rows[index].Cells[ColIndex.DiscRupees2].Value = "0";
                    dgBill.Rows[index].Cells[ColIndex.NetAmt].Value = "0";
                    dgBill.Rows[index].Cells[ColIndex.Amount].Value = "0";
                    dgBill.Rows[index].Cells[ColIndex.Barcode].Value = dtP.Rows[i].ItemArray[4].ToString();
                    dgBill.Rows[index].Cells[ColIndex.PkStockTrnNo].Value = "0";
                    dgBill.Rows[index].Cells[ColIndex.PkBarCodeNo].Value = dtP.Rows[i].ItemArray[5].ToString();
                    dgBill.Rows[index].Cells[ColIndex.PkVoucherNo].Value = "0";
                    dgBill.Rows[index].Cells[ColIndex.ItemNo].Value = dtP.Rows[i].ItemArray[6].ToString();
                    dgBill.Rows[index].Cells[ColIndex.UOMNo].Value = dtP.Rows[i].ItemArray[7].ToString();
                    dgBill.Rows[index].Cells[ColIndex.TaxLedgerNo].Value = dtP.Rows[i].ItemArray[8].ToString();
                    dgBill.Rows[index].Cells[ColIndex.SalesLedgerNo].Value = dtP.Rows[i].ItemArray[9].ToString();
                    dgBill.Rows[index].Cells[ColIndex.PkRateSettingNo].Value = dtP.Rows[i].ItemArray[10].ToString();
                    dgBill.Rows[index].Cells[ColIndex.PkItemTaxInfo].Value = dtP.Rows[i].ItemArray[11].ToString();
                    dgBill.Rows[index].Cells[ColIndex.StockFactor].Value = dtP.Rows[i].ItemArray[12].ToString();
                    dgBill.Rows[index].Cells[ColIndex.ActualQty].Value = "0";
                    dgBill.Rows[index].Cells[ColIndex.MKTQuantity].Value = dtP.Rows[i].ItemArray[13].ToString();
                    dgBill.Rows[index].Cells[ColIndex.TaxPercentage].Value = dtP.Rows[i].ItemArray[14].ToString();
                    dgBill.Rows[index].Cells[ColIndex.StockCompanyNo].Value = dtP.Rows[i].ItemArray[16].ToString();
                    dgBill.Rows[index].Cells[ColIndex.DiscountType].Value = dtP.Rows[i].ItemArray[17].ToString();
                    dgBill.Rows[index].Cells[ColIndex.GodownNo].Value = dtP.Rows[i].ItemArray[18].ToString();
                    dgBill.Rows[index].Cells[ColIndex.HSNCode].Value = dtP.Rows[i].ItemArray[21].ToString();
                    dgBill.Rows[index].Cells[ColIndex.MRP].Value = dtP.Rows[i]["MRP"].ToString();
                    dgBill.Rows[index].Cells[ColIndex.HSNCode].Value = dtP.Rows[i]["HSNCode"].ToString();
                    dgBill.Rows[index].Cells[ColIndex.IGSTPercent].Value = Convert.ToDouble(dtP.Rows[i]["IGSTPercent"].ToString());
                    dgBill.Rows[index].Cells[ColIndex.CGSTPercent].Value = Convert.ToDouble(dtP.Rows[i]["CGSTPercent"].ToString());
                    dgBill.Rows[index].Cells[ColIndex.SGSTPercent].Value = Convert.ToDouble(dtP.Rows[i]["SGSTPercent"].ToString());
                    dgBill.Rows[index].Cells[ColIndex.UTGSTPercent].Value = Convert.ToDouble(dtP.Rows[i]["UTGSTPercent"].ToString());
                    dgBill.Rows[index].Cells[ColIndex.CessPercent].Value = Convert.ToDouble(dtP.Rows[i]["CessPercent"].ToString());
                    dgBill.Rows[index].Cells[ColIndex.IGSTAmount].Value = 0;
                    dgBill.Rows[index].Cells[ColIndex.CGSTAmount].Value = 0;
                    dgBill.Rows[index].Cells[ColIndex.SGSTAmount].Value = 0;
                    dgBill.Rows[index].Cells[ColIndex.UTGSTAmount].Value = 0;
                    dgBill.Rows[index].Cells[ColIndex.CessAmount].Value = 0;
                    dgBill.Rows.Add();
                }
                CalculateTotal();
                //Application.DoEvents();
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }
        }



        #endregion

        private void txtFreight_TextChanged(object sender, EventArgs e)
        {
            ObjFunction.SetMasked((TextBox)sender, 2, 9, JitFunctions.MaskedType.PositiveNegative);
        }

        private void txtFreight_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                Control_Leave((object)txtFreight, new EventArgs());
                cmbPaymentType.Focus();
            }
        }

    }
}
