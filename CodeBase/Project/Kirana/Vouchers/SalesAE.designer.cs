﻿namespace Kirana.Vouchers
{
    partial class SalesAE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.txtInvNo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpBillDate = new System.Windows.Forms.DateTimePicker();
            this.dtpBillTime = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.lblGrandTotal = new System.Windows.Forms.Label();
            this.btnBillCancel = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrev = new System.Windows.Forms.Button();
            this.btnLast = new System.Windows.Forms.Button();
            this.btnFirst = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.dgBill = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MRP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NetRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscPerce = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscRupees = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscPercentage2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscAmount2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscRupees2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NetAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Barcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkStockTrnNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkSrNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkVoucherNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxLedgerNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SalesLedgerNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkRateSettingNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkItemTaxInfo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StockFactor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActualQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MKTQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxPerce = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SalesVchNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxVchNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StockCompanyNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SchemeDetailsNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SchemeFromNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SchemeToNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RewardFromNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RewardToNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemLevelDiscNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FKItemLevelDiscNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkGodownNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscountType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HamaliInKg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HSNCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IGSTPercent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IGSTAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CGSTPercent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CGSTAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SGSTPercent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SGSTAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UTGSTPercent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UTGSTAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CessPercent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CessAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsQtyRead = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSearch = new System.Windows.Forms.Button();
            this.lblMsg = new System.Windows.Forms.Label();
            this.pnlPartial = new System.Windows.Forms.Panel();
            this.lblNMsg = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.dgPayCreditCardDetails = new System.Windows.Forms.DataGridView();
            this.CreditCardNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlBranch = new System.Windows.Forms.Panel();
            this.lstBranch = new System.Windows.Forms.ListBox();
            this.pnlBank = new System.Windows.Forms.Panel();
            this.lstBank = new System.Windows.Forms.ListBox();
            this.dtpChqDate = new System.Windows.Forms.MonthCalendar();
            this.dgPayChqDetails = new System.Windows.Forms.DataGridView();
            this.ChequeNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ChequeDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BankName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BranchName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AmountChq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkSrNoChq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BankNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BranchNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblPayTypeBal = new System.Windows.Forms.Label();
            this.dgPayType = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PKVoucherPayTypeNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ControlUnder = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnOk = new System.Windows.Forms.Button();
            this.txtTotalAmt = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.pnlSearch = new System.Windows.Forms.Panel();
            this.btnPartyName = new System.Windows.Forms.Button();
            this.cmbPartyNameSearch = new System.Windows.Forms.ComboBox();
            this.rbPartyName = new System.Windows.Forms.RadioButton();
            this.rbInvNo = new System.Windows.Forms.RadioButton();
            this.btnCancelSearch = new System.Windows.Forms.Button();
            this.lblLable = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.pnlItemName = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dgItemList = new System.Windows.Forms.DataGridView();
            this.iItemNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemDescrLang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iUOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iMRP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iStock = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iUOMStk = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iSaleTax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iPurTax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iCompany = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iBarcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iRateSettingNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMDefault = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PurRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MGodownNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MDiscType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IHamaliInKg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label31 = new System.Windows.Forms.Label();
            this.lblBrandName = new System.Windows.Forms.Label();
            this.lstUOM = new System.Windows.Forms.ListBox();
            this.lstRate = new System.Windows.Forms.ListBox();
            this.lstGroup1 = new System.Windows.Forms.ListBox();
            this.lstGroup2 = new System.Windows.Forms.ListBox();
            this.label33 = new System.Windows.Forms.Label();
            this.cmbPartyName = new System.Windows.Forms.ComboBox();
            this.cmbTaxType = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblRateType = new System.Windows.Forms.Label();
            this.cmbRateType = new System.Windows.Forms.ComboBox();
            this.pnlGroup1 = new System.Windows.Forms.Panel();
            this.txtBrandFilter = new System.Windows.Forms.TextBox();
            this.lstGroup1Lang = new System.Windows.Forms.ListBox();
            this.pnlGroup2 = new System.Windows.Forms.Panel();
            this.pnlUOM = new System.Windows.Forms.Panel();
            this.pnlRate = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pnlSalePurHistory = new System.Windows.Forms.Panel();
            this.dgPurHistory = new System.Windows.Forms.DataGridView();
            this.dgSaleHistory = new System.Windows.Forms.DataGridView();
            this.pnlRateType = new System.Windows.Forms.Panel();
            this.btnRateTypeCancel = new System.Windows.Forms.Button();
            this.btnRateTypeOK = new System.Windows.Forms.Button();
            this.txtRateTypePassword = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.pnlPartyName = new System.Windows.Forms.Panel();
            this.btnPartyCancel = new System.Windows.Forms.Button();
            this.btnPartyOK = new System.Windows.Forms.Button();
            this.txtPartyName = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.pnlParking = new System.Windows.Forms.Panel();
            this.label37 = new System.Windows.Forms.Label();
            this.dgParkingBills = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BillDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BillTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParkingBillNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlMainParking = new System.Windows.Forms.Panel();
            this.btnParkingCancel = new System.Windows.Forms.Button();
            this.btnParkingOk = new System.Windows.Forms.Button();
            this.txtPersonName = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnAdvanceSearch = new System.Windows.Forms.Button();
            this.btnSalesRegister = new System.Windows.Forms.Button();
            this.btnOrderType = new System.Windows.Forms.Button();
            this.btnNewCustomer = new System.Windows.Forms.Button();
            this.btnShortcut = new System.Windows.Forms.Button();
            this.btnShowDetails = new System.Windows.Forms.Button();
            this.btnTodaysSales = new System.Windows.Forms.Button();
            this.btnCashSave = new System.Windows.Forms.Button();
            this.btnCreditSave = new System.Windows.Forms.Button();
            this.btnCCSave = new System.Windows.Forms.Button();
            this.pnlTodaysSales = new JitControls.OMBPanel();
            this.dgSalesDetails = new System.Windows.Forms.DataGridView();
            this.label48 = new System.Windows.Forms.Label();
            this.lblFV = new System.Windows.Forms.Label();
            this.lblCc = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.lblTotAmt = new System.Windows.Forms.Label();
            this.lblTodaysSales = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.lblBillCnt = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblCredit = new System.Windows.Forms.Label();
            this.lblCheque = new System.Windows.Forms.Label();
            this.lbl = new System.Windows.Forms.Label();
            this.lblCash = new System.Windows.Forms.Label();
            this.pnlLastBill = new JitControls.OMBPanel();
            this.lblLastBillAmt = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lblLastAmt = new System.Windows.Forms.Label();
            this.lblLastPayment = new System.Windows.Forms.Label();
            this.lblLastBillQty = new System.Windows.Forms.Label();
            this.pnlFooterInfo = new JitControls.OMBPanel();
            this.label49 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.panel1 = new JitControls.OMBPanel();
            this.txtAddFreight = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.txtHRs = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.txtKg = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.txtOtherDisc = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.txtRemark = new System.Windows.Forms.TextBox();
            this.cmbPaymentType = new System.Windows.Forms.ComboBox();
            this.plnPrinting = new System.Windows.Forms.Panel();
            this.rbMarathi = new System.Windows.Forms.RadioButton();
            this.rbEnglish = new System.Windows.Forms.RadioButton();
            this.txtChrgRupees1 = new System.Windows.Forms.TextBox();
            this.txtDiscRupees1 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtDiscount1 = new System.Windows.Forms.TextBox();
            this.lblChrg1 = new System.Windows.Forms.Label();
            this.lblDisc1 = new System.Windows.Forms.Label();
            this.lblBilExchangeItem = new System.Windows.Forms.Label();
            this.lblBillItem = new System.Windows.Forms.Label();
            this.lblCreditLimit = new System.Windows.Forms.Label();
            this.txtSchemeDisc = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.lblCancelBll = new System.Windows.Forms.Label();
            this.pnlTotalAmt = new JitControls.OMBPanel();
            this.txtTotalAnotherDisc = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtRoundOff = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtTotalChrgs = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtGrandTotal = new System.Windows.Forms.TextBox();
            this.txtTotalTax = new System.Windows.Forms.TextBox();
            this.txtTotalDisc = new System.Windows.Forms.TextBox();
            this.txtSubTotal = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pnlPartySearch = new JitControls.OMBPanel();
            this.dgPartySearch = new System.Windows.Forms.DataGridView();
            this.lblSearchName = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnMixMode = new System.Windows.Forms.Button();
            this.btnInsScheme = new System.Windows.Forms.Button();
            this.pnlCollectionDetails = new JitControls.OMBPanel();
            this.dgCollectionDetails = new System.Windows.Forms.DataGridView();
            this.lblTotalCollection = new System.Windows.Forms.Label();
            this.lblCollectionDetails = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.lblOw = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.pnlDiscountDetails = new JitControls.OMBPanel();
            this.btnDiscOK = new System.Windows.Forms.Button();
            this.chkFooterLevelDisc = new System.Windows.Forms.CheckBox();
            this.label51 = new System.Windows.Forms.Label();
            this.chkItemLevelDisc = new System.Windows.Forms.CheckBox();
            this.btnInsSchemeInfo = new System.Windows.Forms.Button();
            this.pnlInvSearch = new JitControls.OMBPanel();
            this.dgInvSearch = new System.Windows.Forms.DataGridView();
            this.label52 = new System.Windows.Forms.Label();
            this.btnSchemePromo = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.BTSacn = new System.ComponentModel.BackgroundWorker();
            this.pctbCalculator = new System.Windows.Forms.PictureBox();
            this.bw_ItemSearch = new System.ComponentModel.BackgroundWorker();
            this.pnlScheme = new System.Windows.Forms.Panel();
            this.dgInsTSKU = new System.Windows.Forms.DataGridView();
            this.rType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SchemeNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SchUserNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SchName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SchTypeNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SchDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SchperFrom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SchPerTo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PDiscAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SchDtlspKSrNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UomName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BillQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BUomName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BtnDtails = new System.Windows.Forms.DataGridViewButtonColumn();
            this.select = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.chk = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TSKUCLoyaltyFactor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnRemoveScheme = new System.Windows.Forms.Button();
            this.btnShowSchemeInfo = new System.Windows.Forms.Button();
            this.btnApplyScheme = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgBill)).BeginInit();
            this.pnlPartial.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPayCreditCardDetails)).BeginInit();
            this.pnlBranch.SuspendLayout();
            this.pnlBank.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPayChqDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgPayType)).BeginInit();
            this.pnlSearch.SuspendLayout();
            this.pnlItemName.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgItemList)).BeginInit();
            this.pnlGroup1.SuspendLayout();
            this.pnlGroup2.SuspendLayout();
            this.pnlUOM.SuspendLayout();
            this.pnlRate.SuspendLayout();
            this.pnlSalePurHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPurHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgSaleHistory)).BeginInit();
            this.pnlRateType.SuspendLayout();
            this.pnlPartyName.SuspendLayout();
            this.pnlParking.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgParkingBills)).BeginInit();
            this.pnlMainParking.SuspendLayout();
            this.pnlTodaysSales.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgSalesDetails)).BeginInit();
            this.pnlLastBill.SuspendLayout();
            this.pnlFooterInfo.SuspendLayout();
            this.panel1.SuspendLayout();
            this.plnPrinting.SuspendLayout();
            this.pnlTotalAmt.SuspendLayout();
            this.pnlPartySearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPartySearch)).BeginInit();
            this.pnlCollectionDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCollectionDetails)).BeginInit();
            this.pnlDiscountDetails.SuspendLayout();
            this.pnlInvSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgInvSearch)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctbCalculator)).BeginInit();
            this.pnlScheme.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgInsTSKU)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(10, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 501;
            this.label1.Text = "Inv No :";
            // 
            // txtInvNo
            // 
            this.txtInvNo.BackColor = System.Drawing.Color.White;
            this.txtInvNo.Location = new System.Drawing.Point(64, 6);
            this.txtInvNo.Name = "txtInvNo";
            this.txtInvNo.ReadOnly = true;
            this.txtInvNo.Size = new System.Drawing.Size(100, 20);
            this.txtInvNo.TabIndex = 0;
            this.txtInvNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtInvNo.Leave += new System.EventHandler(this.Control_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(169, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 502;
            this.label2.Text = "Date  :";
            // 
            // dtpBillDate
            // 
            this.dtpBillDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpBillDate.Location = new System.Drawing.Point(217, 6);
            this.dtpBillDate.Name = "dtpBillDate";
            this.dtpBillDate.Size = new System.Drawing.Size(90, 20);
            this.dtpBillDate.TabIndex = 1;
            this.dtpBillDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpBillDate_KeyDown);
            this.dtpBillDate.Leave += new System.EventHandler(this.dtpBillDate_Leave);
            // 
            // dtpBillTime
            // 
            this.dtpBillTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpBillTime.Location = new System.Drawing.Point(358, 6);
            this.dtpBillTime.Name = "dtpBillTime";
            this.dtpBillTime.ShowUpDown = true;
            this.dtpBillTime.Size = new System.Drawing.Size(104, 20);
            this.dtpBillTime.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(312, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 503;
            this.label3.Text = "Time  :";
            // 
            // lblGrandTotal
            // 
            this.lblGrandTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblGrandTotal.BackColor = System.Drawing.Color.Olive;
            this.lblGrandTotal.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrandTotal.ForeColor = System.Drawing.Color.White;
            this.lblGrandTotal.Location = new System.Drawing.Point(822, 398);
            this.lblGrandTotal.Name = "lblGrandTotal";
            this.lblGrandTotal.Size = new System.Drawing.Size(234, 55);
            this.lblGrandTotal.TabIndex = 5553;
            this.lblGrandTotal.Text = "0.00";
            this.lblGrandTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBillCancel
            // 
            this.btnBillCancel.Location = new System.Drawing.Point(1062, 134);
            this.btnBillCancel.Name = "btnBillCancel";
            this.btnBillCancel.Size = new System.Drawing.Size(69, 60);
            this.btnBillCancel.TabIndex = 17;
            this.btnBillCancel.Text = "Delete";
            this.btnBillCancel.UseVisualStyleBackColor = true;
            this.btnBillCancel.Click += new System.EventHandler(this.btnBillCancel_Click);
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnNext.Location = new System.Drawing.Point(953, 456);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(37, 27);
            this.btnNext.TabIndex = 21;
            this.btnNext.Text = ">";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPrev.Location = new System.Drawing.Point(887, 456);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(37, 27);
            this.btnPrev.TabIndex = 20;
            this.btnPrev.Text = "<";
            this.btnPrev.UseVisualStyleBackColor = true;
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // btnLast
            // 
            this.btnLast.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLast.Location = new System.Drawing.Point(1019, 456);
            this.btnLast.Name = "btnLast";
            this.btnLast.Size = new System.Drawing.Size(37, 27);
            this.btnLast.TabIndex = 22;
            this.btnLast.Text = ">|";
            this.btnLast.UseVisualStyleBackColor = true;
            this.btnLast.Click += new System.EventHandler(this.btnLast_Click);
            // 
            // btnFirst
            // 
            this.btnFirst.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnFirst.Location = new System.Drawing.Point(821, 456);
            this.btnFirst.Name = "btnFirst";
            this.btnFirst.Size = new System.Drawing.Size(37, 27);
            this.btnFirst.TabIndex = 19;
            this.btnFirst.Text = "|<";
            this.btnFirst.UseVisualStyleBackColor = true;
            this.btnFirst.Click += new System.EventHandler(this.btnFirst_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(1063, 332);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(69, 60);
            this.btnExit.TabIndex = 16;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(1063, 2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(69, 60);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // dgBill
            // 
            this.dgBill.AllowUserToAddRows = false;
            this.dgBill.AllowUserToDeleteRows = false;
            this.dgBill.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgBill.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgBill.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.ItemName,
            this.Quantity,
            this.UOM,
            this.MRP,
            this.Rate,
            this.NetRate,
            this.DiscPerce,
            this.DiscAmt,
            this.DiscRupees,
            this.DiscPercentage2,
            this.DiscAmount2,
            this.DiscRupees2,
            this.NetAmt,
            this.Amount,
            this.Barcode,
            this.PkStockTrnNo,
            this.PkSrNo,
            this.PkVoucherNo,
            this.ItemNo,
            this.UOMNo,
            this.TaxLedgerNo,
            this.SalesLedgerNo,
            this.PkRateSettingNo,
            this.PkItemTaxInfo,
            this.StockFactor,
            this.ActualQty,
            this.MKTQuantity,
            this.TaxPerce,
            this.TaxAmount,
            this.SalesVchNo,
            this.TaxVchNo,
            this.StockCompanyNo,
            this.SchemeDetailsNo,
            this.SchemeFromNo,
            this.SchemeToNo,
            this.RewardFromNo,
            this.RewardToNo,
            this.ItemLevelDiscNo,
            this.FKItemLevelDiscNo,
            this.PkGodownNo,
            this.DiscountType,
            this.HamaliInKg,
            this.HSNCode,
            this.IGSTPercent,
            this.IGSTAmount,
            this.CGSTPercent,
            this.CGSTAmount,
            this.SGSTPercent,
            this.SGSTAmount,
            this.UTGSTPercent,
            this.UTGSTAmount,
            this.CessPercent,
            this.CessAmount,
            this.IsQtyRead});
            this.dgBill.Location = new System.Drawing.Point(12, 68);
            this.dgBill.Name = "dgBill";
            this.dgBill.Size = new System.Drawing.Size(1044, 324);
            this.dgBill.TabIndex = 550;
            this.dgBill.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgBill_CellBeginEdit);
            this.dgBill.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgBill_CellEndEdit);
            this.dgBill.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgBill_CellFormatting);
            this.dgBill.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgBill_CellValueChanged);
            this.dgBill.CurrentCellChanged += new System.EventHandler(this.dgBill_CurrentCellChanged);
            this.dgBill.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgBill_EditingControlShowing);
            this.dgBill.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgBill_KeyDown);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Sr";
            this.Column1.HeaderText = "Sr";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 40;
            // 
            // ItemName
            // 
            this.ItemName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ItemName.DataPropertyName = "ItemName";
            this.ItemName.HeaderText = "Description";
            this.ItemName.Name = "ItemName";
            // 
            // Quantity
            // 
            this.Quantity.DataPropertyName = "Quantity";
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Quantity.DefaultCellStyle = dataGridViewCellStyle27;
            this.Quantity.HeaderText = "Qty";
            this.Quantity.Name = "Quantity";
            this.Quantity.Width = 80;
            // 
            // UOM
            // 
            this.UOM.DataPropertyName = "UOMName";
            this.UOM.HeaderText = "UOM";
            this.UOM.Name = "UOM";
            this.UOM.ReadOnly = true;
            this.UOM.Width = 50;
            // 
            // MRP
            // 
            this.MRP.DataPropertyName = "MRP";
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.MRP.DefaultCellStyle = dataGridViewCellStyle28;
            this.MRP.HeaderText = "MRP";
            this.MRP.Name = "MRP";
            this.MRP.ReadOnly = true;
            this.MRP.Width = 90;
            // 
            // Rate
            // 
            this.Rate.DataPropertyName = "Rate";
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Rate.DefaultCellStyle = dataGridViewCellStyle29;
            this.Rate.HeaderText = "Rate";
            this.Rate.Name = "Rate";
            // 
            // NetRate
            // 
            this.NetRate.DataPropertyName = "NetRate";
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.NetRate.DefaultCellStyle = dataGridViewCellStyle30;
            this.NetRate.HeaderText = "NetRate";
            this.NetRate.Name = "NetRate";
            this.NetRate.ReadOnly = true;
            this.NetRate.Visible = false;
            this.NetRate.Width = 65;
            // 
            // DiscPerce
            // 
            this.DiscPerce.DataPropertyName = "DiscPercentage";
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DiscPerce.DefaultCellStyle = dataGridViewCellStyle31;
            this.DiscPerce.HeaderText = "Disc%";
            this.DiscPerce.Name = "DiscPerce";
            this.DiscPerce.Width = 55;
            // 
            // DiscAmt
            // 
            this.DiscAmt.DataPropertyName = "DiscAmount";
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DiscAmt.DefaultCellStyle = dataGridViewCellStyle32;
            this.DiscAmt.HeaderText = "DiscAmt";
            this.DiscAmt.Name = "DiscAmt";
            this.DiscAmt.ReadOnly = true;
            this.DiscAmt.Visible = false;
            this.DiscAmt.Width = 65;
            // 
            // DiscRupees
            // 
            this.DiscRupees.DataPropertyName = "DiscRupees";
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DiscRupees.DefaultCellStyle = dataGridViewCellStyle33;
            this.DiscRupees.HeaderText = "DiscRs.";
            this.DiscRupees.Name = "DiscRupees";
            this.DiscRupees.Visible = false;
            this.DiscRupees.Width = 55;
            // 
            // DiscPercentage2
            // 
            this.DiscPercentage2.DataPropertyName = "DiscPercentage2";
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DiscPercentage2.DefaultCellStyle = dataGridViewCellStyle34;
            this.DiscPercentage2.HeaderText = "Disc% 2";
            this.DiscPercentage2.Name = "DiscPercentage2";
            this.DiscPercentage2.Visible = false;
            this.DiscPercentage2.Width = 65;
            // 
            // DiscAmount2
            // 
            this.DiscAmount2.DataPropertyName = "DiscAmount2";
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DiscAmount2.DefaultCellStyle = dataGridViewCellStyle35;
            this.DiscAmount2.HeaderText = "DiscAmt2";
            this.DiscAmount2.Name = "DiscAmount2";
            this.DiscAmount2.ReadOnly = true;
            this.DiscAmount2.Visible = false;
            this.DiscAmount2.Width = 65;
            // 
            // DiscRupees2
            // 
            this.DiscRupees2.DataPropertyName = "DiscRupees2";
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DiscRupees2.DefaultCellStyle = dataGridViewCellStyle36;
            this.DiscRupees2.HeaderText = "DiscRs2";
            this.DiscRupees2.Name = "DiscRupees2";
            this.DiscRupees2.Visible = false;
            this.DiscRupees2.Width = 65;
            // 
            // NetAmt
            // 
            this.NetAmt.DataPropertyName = "NetAmt";
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.NetAmt.DefaultCellStyle = dataGridViewCellStyle37;
            this.NetAmt.HeaderText = "NetAmt";
            this.NetAmt.Name = "NetAmt";
            this.NetAmt.ReadOnly = true;
            this.NetAmt.Visible = false;
            this.NetAmt.Width = 85;
            // 
            // Amount
            // 
            this.Amount.DataPropertyName = "Amount";
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Amount.DefaultCellStyle = dataGridViewCellStyle38;
            this.Amount.HeaderText = "Amount";
            this.Amount.Name = "Amount";
            this.Amount.ReadOnly = true;
            this.Amount.Width = 85;
            // 
            // Barcode
            // 
            this.Barcode.DataPropertyName = "Barcode";
            this.Barcode.HeaderText = "Barcode";
            this.Barcode.Name = "Barcode";
            this.Barcode.Visible = false;
            // 
            // PkStockTrnNo
            // 
            this.PkStockTrnNo.DataPropertyName = "PkStockTrnNo";
            this.PkStockTrnNo.HeaderText = "PkStockTrnNo";
            this.PkStockTrnNo.Name = "PkStockTrnNo";
            this.PkStockTrnNo.Visible = false;
            // 
            // PkSrNo
            // 
            this.PkSrNo.DataPropertyName = "PkStockBarcodeNo";
            this.PkSrNo.HeaderText = "PkBarcodeNo";
            this.PkSrNo.Name = "PkSrNo";
            this.PkSrNo.Visible = false;
            // 
            // PkVoucherNo
            // 
            this.PkVoucherNo.DataPropertyName = "PkVoucherNo";
            this.PkVoucherNo.HeaderText = "PkVoucherNo";
            this.PkVoucherNo.Name = "PkVoucherNo";
            this.PkVoucherNo.Visible = false;
            // 
            // ItemNo
            // 
            this.ItemNo.DataPropertyName = "ItemNo";
            this.ItemNo.HeaderText = "ItemNo";
            this.ItemNo.Name = "ItemNo";
            this.ItemNo.Visible = false;
            // 
            // UOMNo
            // 
            this.UOMNo.DataPropertyName = "UOMNo";
            this.UOMNo.HeaderText = "UOMNo";
            this.UOMNo.Name = "UOMNo";
            this.UOMNo.Visible = false;
            // 
            // TaxLedgerNo
            // 
            this.TaxLedgerNo.HeaderText = "TaxLedgerNo";
            this.TaxLedgerNo.Name = "TaxLedgerNo";
            this.TaxLedgerNo.Visible = false;
            // 
            // SalesLedgerNo
            // 
            this.SalesLedgerNo.HeaderText = "SalesLedgerNo";
            this.SalesLedgerNo.Name = "SalesLedgerNo";
            this.SalesLedgerNo.Visible = false;
            // 
            // PkRateSettingNo
            // 
            this.PkRateSettingNo.HeaderText = "RateSettingNo";
            this.PkRateSettingNo.Name = "PkRateSettingNo";
            this.PkRateSettingNo.Visible = false;
            // 
            // PkItemTaxInfo
            // 
            this.PkItemTaxInfo.HeaderText = "ItemTaxInfoNo";
            this.PkItemTaxInfo.Name = "PkItemTaxInfo";
            this.PkItemTaxInfo.Visible = false;
            // 
            // StockFactor
            // 
            this.StockFactor.DataPropertyName = "StockConversion";
            this.StockFactor.HeaderText = "StockFactor";
            this.StockFactor.Name = "StockFactor";
            this.StockFactor.Visible = false;
            // 
            // ActualQty
            // 
            this.ActualQty.HeaderText = "ActualQty";
            this.ActualQty.Name = "ActualQty";
            this.ActualQty.Visible = false;
            // 
            // MKTQuantity
            // 
            this.MKTQuantity.DataPropertyName = "MKTQty";
            this.MKTQuantity.HeaderText = "MKTQuantity";
            this.MKTQuantity.Name = "MKTQuantity";
            this.MKTQuantity.Visible = false;
            // 
            // TaxPerce
            // 
            this.TaxPerce.DataPropertyName = "TaxPercentage";
            this.TaxPerce.HeaderText = "TaxPerce";
            this.TaxPerce.Name = "TaxPerce";
            this.TaxPerce.Visible = false;
            // 
            // TaxAmount
            // 
            this.TaxAmount.DataPropertyName = "TaxAmount";
            this.TaxAmount.HeaderText = "TaxAmt";
            this.TaxAmount.Name = "TaxAmount";
            this.TaxAmount.Visible = false;
            // 
            // SalesVchNo
            // 
            this.SalesVchNo.HeaderText = "SalesVchNo";
            this.SalesVchNo.Name = "SalesVchNo";
            this.SalesVchNo.Visible = false;
            // 
            // TaxVchNo
            // 
            this.TaxVchNo.HeaderText = "TaxVchNo";
            this.TaxVchNo.Name = "TaxVchNo";
            this.TaxVchNo.Visible = false;
            // 
            // StockCompanyNo
            // 
            this.StockCompanyNo.HeaderText = "StockCompanyNo";
            this.StockCompanyNo.Name = "StockCompanyNo";
            this.StockCompanyNo.Visible = false;
            // 
            // SchemeDetailsNo
            // 
            this.SchemeDetailsNo.HeaderText = "SchemeDetailsNo";
            this.SchemeDetailsNo.Name = "SchemeDetailsNo";
            this.SchemeDetailsNo.Visible = false;
            // 
            // SchemeFromNo
            // 
            this.SchemeFromNo.HeaderText = "SchemeFromNo";
            this.SchemeFromNo.Name = "SchemeFromNo";
            this.SchemeFromNo.Visible = false;
            // 
            // SchemeToNo
            // 
            this.SchemeToNo.HeaderText = "SchemeToNo";
            this.SchemeToNo.Name = "SchemeToNo";
            this.SchemeToNo.Visible = false;
            // 
            // RewardFromNo
            // 
            this.RewardFromNo.HeaderText = "RewardFromNo";
            this.RewardFromNo.Name = "RewardFromNo";
            this.RewardFromNo.Visible = false;
            // 
            // RewardToNo
            // 
            this.RewardToNo.HeaderText = "RewardToNo";
            this.RewardToNo.Name = "RewardToNo";
            this.RewardToNo.Visible = false;
            // 
            // ItemLevelDiscNo
            // 
            this.ItemLevelDiscNo.HeaderText = "ItemLevelDiscNo";
            this.ItemLevelDiscNo.Name = "ItemLevelDiscNo";
            this.ItemLevelDiscNo.Visible = false;
            // 
            // FKItemLevelDiscNo
            // 
            this.FKItemLevelDiscNo.HeaderText = "FKItemLevelDiscNo";
            this.FKItemLevelDiscNo.Name = "FKItemLevelDiscNo";
            this.FKItemLevelDiscNo.Visible = false;
            // 
            // PkGodownNo
            // 
            this.PkGodownNo.HeaderText = "GodownNo";
            this.PkGodownNo.Name = "PkGodownNo";
            this.PkGodownNo.Visible = false;
            // 
            // DiscountType
            // 
            this.DiscountType.HeaderText = "DiscountType";
            this.DiscountType.Name = "DiscountType";
            this.DiscountType.Visible = false;
            // 
            // HamaliInKg
            // 
            this.HamaliInKg.HeaderText = "HamaliInKg";
            this.HamaliInKg.Name = "HamaliInKg";
            this.HamaliInKg.Visible = false;
            // 
            // HSNCode
            // 
            this.HSNCode.HeaderText = "HSNCode";
            this.HSNCode.Name = "HSNCode";
            this.HSNCode.Visible = false;
            // 
            // IGSTPercent
            // 
            this.IGSTPercent.HeaderText = "IGSTPercent";
            this.IGSTPercent.Name = "IGSTPercent";
            this.IGSTPercent.Visible = false;
            // 
            // IGSTAmount
            // 
            this.IGSTAmount.HeaderText = "IGSTAmount";
            this.IGSTAmount.Name = "IGSTAmount";
            this.IGSTAmount.Visible = false;
            // 
            // CGSTPercent
            // 
            this.CGSTPercent.HeaderText = "CGSTPercent";
            this.CGSTPercent.Name = "CGSTPercent";
            this.CGSTPercent.Visible = false;
            // 
            // CGSTAmount
            // 
            this.CGSTAmount.HeaderText = "CGSTAmount";
            this.CGSTAmount.Name = "CGSTAmount";
            this.CGSTAmount.Visible = false;
            // 
            // SGSTPercent
            // 
            this.SGSTPercent.HeaderText = "SGSTPercent";
            this.SGSTPercent.Name = "SGSTPercent";
            this.SGSTPercent.Visible = false;
            // 
            // SGSTAmount
            // 
            this.SGSTAmount.HeaderText = "SGSTAmount";
            this.SGSTAmount.Name = "SGSTAmount";
            this.SGSTAmount.Visible = false;
            // 
            // UTGSTPercent
            // 
            this.UTGSTPercent.HeaderText = "UTGSTPercent";
            this.UTGSTPercent.Name = "UTGSTPercent";
            this.UTGSTPercent.Visible = false;
            // 
            // UTGSTAmount
            // 
            this.UTGSTAmount.HeaderText = "UTGSTAmount";
            this.UTGSTAmount.Name = "UTGSTAmount";
            this.UTGSTAmount.Visible = false;
            // 
            // CessPercent
            // 
            this.CessPercent.HeaderText = "CessPercent";
            this.CessPercent.Name = "CessPercent";
            this.CessPercent.Visible = false;
            // 
            // CessAmount
            // 
            this.CessAmount.HeaderText = "CessAmount";
            this.CessAmount.Name = "CessAmount";
            this.CessAmount.Visible = false;
            // 
            // IsQtyRead
            // 
            this.IsQtyRead.HeaderText = "IsQtyRead";
            this.IsQtyRead.Name = "IsQtyRead";
            this.IsQtyRead.Visible = false;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(1062, 200);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(69, 60);
            this.btnSearch.TabIndex = 18;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // lblMsg
            // 
            this.lblMsg.BackColor = System.Drawing.Color.Coral;
            this.lblMsg.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMsg.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg.ForeColor = System.Drawing.Color.Maroon;
            this.lblMsg.Location = new System.Drawing.Point(368, 254);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(481, 52);
            this.lblMsg.TabIndex = 505;
            this.lblMsg.Text = "label4";
            this.lblMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMsg.Visible = false;
            // 
            // pnlPartial
            // 
            this.pnlPartial.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlPartial.Controls.Add(this.lblNMsg);
            this.pnlPartial.Controls.Add(this.label12);
            this.pnlPartial.Controls.Add(this.label38);
            this.pnlPartial.Controls.Add(this.dgPayCreditCardDetails);
            this.pnlPartial.Controls.Add(this.pnlBranch);
            this.pnlPartial.Controls.Add(this.pnlBank);
            this.pnlPartial.Controls.Add(this.dtpChqDate);
            this.pnlPartial.Controls.Add(this.dgPayChqDetails);
            this.pnlPartial.Controls.Add(this.lblPayTypeBal);
            this.pnlPartial.Controls.Add(this.dgPayType);
            this.pnlPartial.Controls.Add(this.btnOk);
            this.pnlPartial.Controls.Add(this.txtTotalAmt);
            this.pnlPartial.Controls.Add(this.label26);
            this.pnlPartial.Location = new System.Drawing.Point(169, 123);
            this.pnlPartial.Name = "pnlPartial";
            this.pnlPartial.Size = new System.Drawing.Size(304, 275);
            this.pnlPartial.TabIndex = 506;
            this.pnlPartial.Visible = false;
            // 
            // lblNMsg
            // 
            this.lblNMsg.Location = new System.Drawing.Point(6, 158);
            this.lblNMsg.Name = "lblNMsg";
            this.lblNMsg.Size = new System.Drawing.Size(288, 13);
            this.lblNMsg.TabIndex = 5550;
            this.lblNMsg.Text = "New Label";
            this.lblNMsg.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(349, 186);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(66, 13);
            this.label12.TabIndex = 5549;
            this.label12.Text = "Exit(Escape)";
            // 
            // label38
            // 
            this.label38.Location = new System.Drawing.Point(6, 121);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(288, 37);
            this.label38.TabIndex = 5548;
            this.label38.Text = "(Ctrl + D) Display Details (For Cheque and Credit Card.";
            this.label38.Visible = false;
            // 
            // dgPayCreditCardDetails
            // 
            this.dgPayCreditCardDetails.AllowUserToAddRows = false;
            this.dgPayCreditCardDetails.AllowUserToDeleteRows = false;
            this.dgPayCreditCardDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPayCreditCardDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CreditCardNo,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12});
            this.dgPayCreditCardDetails.Location = new System.Drawing.Point(304, 3);
            this.dgPayCreditCardDetails.Name = "dgPayCreditCardDetails";
            this.dgPayCreditCardDetails.Size = new System.Drawing.Size(464, 169);
            this.dgPayCreditCardDetails.TabIndex = 5547;
            this.dgPayCreditCardDetails.Visible = false;
            this.dgPayCreditCardDetails.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPayCreditCardDetails_CellEndEdit);
            this.dgPayCreditCardDetails.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPayCreditCardDetails_CellEnter);
            this.dgPayCreditCardDetails.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgPayCreditCardDetails_KeyDown);
            // 
            // CreditCardNo
            // 
            this.CreditCardNo.DataPropertyName = "CreditCardNo";
            this.CreditCardNo.HeaderText = "Cr. Card No";
            this.CreditCardNo.MaxInputLength = 20;
            this.CreditCardNo.Name = "CreditCardNo";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "BankName";
            this.dataGridViewTextBoxColumn7.HeaderText = "Bank Name";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 120;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "BranchName";
            this.dataGridViewTextBoxColumn8.HeaderText = "BranchName";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 120;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "Amount";
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle39;
            this.dataGridViewTextBoxColumn9.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "PkSrNo";
            this.dataGridViewTextBoxColumn10.HeaderText = "PkSrNo";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Visible = false;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "BankNo";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.Visible = false;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "BranchNo";
            this.dataGridViewTextBoxColumn12.HeaderText = "BranchNo";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.Visible = false;
            // 
            // pnlBranch
            // 
            this.pnlBranch.BackColor = System.Drawing.Color.Coral;
            this.pnlBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBranch.Controls.Add(this.lstBranch);
            this.pnlBranch.Location = new System.Drawing.Point(538, 39);
            this.pnlBranch.Name = "pnlBranch";
            this.pnlBranch.Size = new System.Drawing.Size(164, 117);
            this.pnlBranch.TabIndex = 5544;
            this.pnlBranch.Visible = false;
            // 
            // lstBranch
            // 
            this.lstBranch.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstBranch.FormattingEnabled = true;
            this.lstBranch.Location = new System.Drawing.Point(8, 8);
            this.lstBranch.Name = "lstBranch";
            this.lstBranch.Size = new System.Drawing.Size(147, 93);
            this.lstBranch.TabIndex = 516;
            this.lstBranch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstBranch_KeyDown);
            // 
            // pnlBank
            // 
            this.pnlBank.BackColor = System.Drawing.Color.Coral;
            this.pnlBank.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBank.Controls.Add(this.lstBank);
            this.pnlBank.Location = new System.Drawing.Point(352, 38);
            this.pnlBank.Name = "pnlBank";
            this.pnlBank.Size = new System.Drawing.Size(176, 118);
            this.pnlBank.TabIndex = 5545;
            this.pnlBank.Visible = false;
            // 
            // lstBank
            // 
            this.lstBank.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstBank.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstBank.FormattingEnabled = true;
            this.lstBank.Location = new System.Drawing.Point(8, 10);
            this.lstBank.Name = "lstBank";
            this.lstBank.Size = new System.Drawing.Size(159, 93);
            this.lstBank.TabIndex = 516;
            this.lstBank.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstBank_KeyDown);
            // 
            // dtpChqDate
            // 
            this.dtpChqDate.Location = new System.Drawing.Point(471, 45);
            this.dtpChqDate.Name = "dtpChqDate";
            this.dtpChqDate.TabIndex = 5546;
            this.dtpChqDate.Visible = false;
            this.dtpChqDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpChqDate_KeyDown);
            // 
            // dgPayChqDetails
            // 
            this.dgPayChqDetails.AllowUserToAddRows = false;
            this.dgPayChqDetails.AllowUserToDeleteRows = false;
            this.dgPayChqDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPayChqDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ChequeNo,
            this.ChequeDate,
            this.BankName,
            this.BranchName,
            this.AmountChq,
            this.PkSrNoChq,
            this.BankNo,
            this.BranchNo});
            this.dgPayChqDetails.Location = new System.Drawing.Point(304, 3);
            this.dgPayChqDetails.Name = "dgPayChqDetails";
            this.dgPayChqDetails.Size = new System.Drawing.Size(464, 169);
            this.dgPayChqDetails.TabIndex = 659;
            this.dgPayChqDetails.Visible = false;
            this.dgPayChqDetails.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPayChqDetails_CellEndEdit);
            this.dgPayChqDetails.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPayChqDetails_CellEnter);
            this.dgPayChqDetails.CellToolTipTextNeeded += new System.Windows.Forms.DataGridViewCellToolTipTextNeededEventHandler(this.dgPayChqDetails_CellToolTipTextNeeded);
            this.dgPayChqDetails.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgPayChqDetails_KeyDown);
            // 
            // ChequeNo
            // 
            this.ChequeNo.DataPropertyName = "ChequeNo";
            this.ChequeNo.HeaderText = "Chq. No";
            this.ChequeNo.MaxInputLength = 10;
            this.ChequeNo.Name = "ChequeNo";
            this.ChequeNo.Width = 75;
            // 
            // ChequeDate
            // 
            this.ChequeDate.DataPropertyName = "ChequeDate";
            this.ChequeDate.HeaderText = "Date";
            this.ChequeDate.Name = "ChequeDate";
            // 
            // BankName
            // 
            this.BankName.DataPropertyName = "BankName";
            this.BankName.HeaderText = "Bank Name";
            this.BankName.Name = "BankName";
            this.BankName.ReadOnly = true;
            this.BankName.Width = 120;
            // 
            // BranchName
            // 
            this.BranchName.DataPropertyName = "BranchName";
            this.BranchName.HeaderText = "BranchName";
            this.BranchName.Name = "BranchName";
            this.BranchName.ReadOnly = true;
            this.BranchName.Width = 120;
            // 
            // AmountChq
            // 
            this.AmountChq.DataPropertyName = "Amount";
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.AmountChq.DefaultCellStyle = dataGridViewCellStyle40;
            this.AmountChq.HeaderText = "Amount";
            this.AmountChq.Name = "AmountChq";
            // 
            // PkSrNoChq
            // 
            this.PkSrNoChq.DataPropertyName = "PkSrNo";
            this.PkSrNoChq.HeaderText = "PkSrNo";
            this.PkSrNoChq.Name = "PkSrNoChq";
            this.PkSrNoChq.Visible = false;
            // 
            // BankNo
            // 
            this.BankNo.HeaderText = "BankNo";
            this.BankNo.Name = "BankNo";
            this.BankNo.Visible = false;
            // 
            // BranchNo
            // 
            this.BranchNo.DataPropertyName = "BranchNo";
            this.BranchNo.HeaderText = "BranchNo";
            this.BranchNo.Name = "BranchNo";
            this.BranchNo.Visible = false;
            // 
            // lblPayTypeBal
            // 
            this.lblPayTypeBal.AutoSize = true;
            this.lblPayTypeBal.Location = new System.Drawing.Point(86, 185);
            this.lblPayTypeBal.Name = "lblPayTypeBal";
            this.lblPayTypeBal.Size = new System.Drawing.Size(0, 13);
            this.lblPayTypeBal.TabIndex = 658;
            // 
            // dgPayType
            // 
            this.dgPayType.AllowUserToAddRows = false;
            this.dgPayType.AllowUserToDeleteRows = false;
            this.dgPayType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPayType.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn5,
            this.PKVoucherPayTypeNo,
            this.ControlUnder});
            this.dgPayType.Location = new System.Drawing.Point(2, 3);
            this.dgPayType.Name = "dgPayType";
            this.dgPayType.Size = new System.Drawing.Size(295, 169);
            this.dgPayType.TabIndex = 657;
            this.dgPayType.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPayType_CellEndEdit);
            this.dgPayType.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgPayType_EditingControlShowing);
            this.dgPayType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgPayType_KeyDown);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "PayTypeName";
            this.dataGridViewTextBoxColumn1.HeaderText = "Pay Type";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "PKPayTypeNo";
            this.dataGridViewTextBoxColumn4.HeaderText = "PayTypeNo";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Amount";
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle41;
            this.dataGridViewTextBoxColumn2.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "LedgerNo";
            this.dataGridViewTextBoxColumn5.HeaderText = "LedgerNo";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // PKVoucherPayTypeNo
            // 
            this.PKVoucherPayTypeNo.DataPropertyName = "PKVoucherPayTypeNo";
            this.PKVoucherPayTypeNo.HeaderText = "PKVoucherPayTypeNo";
            this.PKVoucherPayTypeNo.Name = "PKVoucherPayTypeNo";
            this.PKVoucherPayTypeNo.Visible = false;
            // 
            // ControlUnder
            // 
            this.ControlUnder.DataPropertyName = "ControlUnder";
            this.ControlUnder.HeaderText = "ControlUnder";
            this.ControlUnder.Name = "ControlUnder";
            this.ControlUnder.Visible = false;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(3, 182);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // txtTotalAmt
            // 
            this.txtTotalAmt.BackColor = System.Drawing.Color.White;
            this.txtTotalAmt.Enabled = false;
            this.txtTotalAmt.Location = new System.Drawing.Point(222, 182);
            this.txtTotalAmt.Name = "txtTotalAmt";
            this.txtTotalAmt.ReadOnly = true;
            this.txtTotalAmt.Size = new System.Drawing.Size(72, 20);
            this.txtTotalAmt.TabIndex = 656;
            this.txtTotalAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(176, 187);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(37, 13);
            this.label26.TabIndex = 502;
            this.label26.Text = "Total :";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(1063, 68);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(69, 60);
            this.btnUpdate.TabIndex = 14;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(1063, 2);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(69, 60);
            this.btnNew.TabIndex = 13;
            this.btnNew.Text = "New";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.CausesValidation = false;
            this.btnCancel.Location = new System.Drawing.Point(1063, 68);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(69, 60);
            this.btnCancel.TabIndex = 15;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // pnlSearch
            // 
            this.pnlSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSearch.Controls.Add(this.btnPartyName);
            this.pnlSearch.Controls.Add(this.cmbPartyNameSearch);
            this.pnlSearch.Controls.Add(this.rbPartyName);
            this.pnlSearch.Controls.Add(this.rbInvNo);
            this.pnlSearch.Controls.Add(this.btnCancelSearch);
            this.pnlSearch.Controls.Add(this.lblLable);
            this.pnlSearch.Controls.Add(this.txtSearch);
            this.pnlSearch.Location = new System.Drawing.Point(148, 236);
            this.pnlSearch.Name = "pnlSearch";
            this.pnlSearch.Size = new System.Drawing.Size(401, 73);
            this.pnlSearch.TabIndex = 509;
            this.pnlSearch.Visible = false;
            // 
            // btnPartyName
            // 
            this.btnPartyName.Location = new System.Drawing.Point(368, 37);
            this.btnPartyName.Name = "btnPartyName";
            this.btnPartyName.Size = new System.Drawing.Size(21, 21);
            this.btnPartyName.TabIndex = 123455;
            this.btnPartyName.Text = "..";
            this.btnPartyName.UseVisualStyleBackColor = true;
            this.btnPartyName.Click += new System.EventHandler(this.btnPartyName_Click);
            // 
            // cmbPartyNameSearch
            // 
            this.cmbPartyNameSearch.FormattingEnabled = true;
            this.cmbPartyNameSearch.Location = new System.Drawing.Point(186, 37);
            this.cmbPartyNameSearch.Name = "cmbPartyNameSearch";
            this.cmbPartyNameSearch.Size = new System.Drawing.Size(176, 21);
            this.cmbPartyNameSearch.TabIndex = 123453;
            this.cmbPartyNameSearch.Visible = false;
            this.cmbPartyNameSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbPartyNameSearch_KeyDown);
            // 
            // rbPartyName
            // 
            this.rbPartyName.AutoSize = true;
            this.rbPartyName.BackColor = System.Drawing.Color.Transparent;
            this.rbPartyName.Location = new System.Drawing.Point(124, 10);
            this.rbPartyName.Name = "rbPartyName";
            this.rbPartyName.Size = new System.Drawing.Size(80, 17);
            this.rbPartyName.TabIndex = 123451;
            this.rbPartyName.TabStop = true;
            this.rbPartyName.Text = "Party Name";
            this.rbPartyName.UseVisualStyleBackColor = false;
            this.rbPartyName.CheckedChanged += new System.EventHandler(this.rbType_CheckedChanged);
            // 
            // rbInvNo
            // 
            this.rbInvNo.AutoSize = true;
            this.rbInvNo.BackColor = System.Drawing.Color.Transparent;
            this.rbInvNo.Checked = true;
            this.rbInvNo.Location = new System.Drawing.Point(17, 9);
            this.rbInvNo.Name = "rbInvNo";
            this.rbInvNo.Size = new System.Drawing.Size(57, 17);
            this.rbInvNo.TabIndex = 123450;
            this.rbInvNo.TabStop = true;
            this.rbInvNo.Text = "Inv No";
            this.rbInvNo.UseVisualStyleBackColor = false;
            this.rbInvNo.CheckedChanged += new System.EventHandler(this.rbType_CheckedChanged);
            // 
            // btnCancelSearch
            // 
            this.btnCancelSearch.CausesValidation = false;
            this.btnCancelSearch.Location = new System.Drawing.Point(290, 5);
            this.btnCancelSearch.Name = "btnCancelSearch";
            this.btnCancelSearch.Size = new System.Drawing.Size(79, 23);
            this.btnCancelSearch.TabIndex = 123454;
            this.btnCancelSearch.Text = "Cancel";
            this.btnCancelSearch.UseVisualStyleBackColor = true;
            this.btnCancelSearch.Click += new System.EventHandler(this.btnCancelSearch_Click);
            // 
            // lblLable
            // 
            this.lblLable.AutoSize = true;
            this.lblLable.Location = new System.Drawing.Point(6, 41);
            this.lblLable.Name = "lblLable";
            this.lblLable.Size = new System.Drawing.Size(45, 13);
            this.lblLable.TabIndex = 502;
            this.lblLable.Text = "Inv No :";
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(63, 37);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(114, 20);
            this.txtSearch.TabIndex = 123454;
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            // 
            // pnlItemName
            // 
            this.pnlItemName.BackColor = System.Drawing.Color.Coral;
            this.pnlItemName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlItemName.Controls.Add(this.panel3);
            this.pnlItemName.Controls.Add(this.label31);
            this.pnlItemName.Controls.Add(this.lblBrandName);
            this.pnlItemName.Location = new System.Drawing.Point(15, 88);
            this.pnlItemName.Name = "pnlItemName";
            this.pnlItemName.Size = new System.Drawing.Size(234, 65);
            this.pnlItemName.TabIndex = 514;
            this.pnlItemName.Visible = false;
            this.pnlItemName.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.dgItemList);
            this.panel3.Location = new System.Drawing.Point(1, 22);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(235, 39);
            this.panel3.TabIndex = 518;
            // 
            // dgItemList
            // 
            this.dgItemList.AllowUserToAddRows = false;
            this.dgItemList.AllowUserToDeleteRows = false;
            this.dgItemList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgItemList.BackgroundColor = System.Drawing.Color.Coral;
            this.dgItemList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgItemList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iItemNo,
            this.iItemName,
            this.ItemDescrLang,
            this.iRate,
            this.iUOM,
            this.iMRP,
            this.iStock,
            this.iUOMStk,
            this.iSaleTax,
            this.iPurTax,
            this.iCompany,
            this.iBarcode,
            this.iRateSettingNo,
            this.UOMDefault,
            this.PurRate,
            this.MGodownNo,
            this.MDiscType,
            this.IHamaliInKg});
            this.dgItemList.Location = new System.Drawing.Point(0, 0);
            this.dgItemList.Name = "dgItemList";
            this.dgItemList.Size = new System.Drawing.Size(235, 39);
            this.dgItemList.TabIndex = 515;
            this.dgItemList.CurrentCellChanged += new System.EventHandler(this.dgItemList_CurrentCellChanged);
            this.dgItemList.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            this.dgItemList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgItemList_KeyDown);
            // 
            // iItemNo
            // 
            this.iItemNo.DataPropertyName = "ItemNo";
            this.iItemNo.HeaderText = "ItemNo";
            this.iItemNo.Name = "iItemNo";
            this.iItemNo.ReadOnly = true;
            this.iItemNo.ToolTipText = "ItemNo";
            this.iItemNo.Visible = false;
            // 
            // iItemName
            // 
            this.iItemName.DataPropertyName = "ItemName";
            this.iItemName.HeaderText = "Item Description";
            this.iItemName.Name = "iItemName";
            this.iItemName.ReadOnly = true;
            this.iItemName.ToolTipText = "Product Name";
            this.iItemName.Width = 300;
            // 
            // ItemDescrLang
            // 
            this.ItemDescrLang.DataPropertyName = "ItemNameLang";
            this.ItemDescrLang.HeaderText = "Item Name";
            this.ItemDescrLang.Name = "ItemDescrLang";
            this.ItemDescrLang.ReadOnly = true;
            this.ItemDescrLang.Width = 300;
            // 
            // iRate
            // 
            this.iRate.DataPropertyName = "SaleRate";
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle42.Format = "N2";
            dataGridViewCellStyle42.NullValue = null;
            this.iRate.DefaultCellStyle = dataGridViewCellStyle42;
            this.iRate.HeaderText = "Rate";
            this.iRate.Name = "iRate";
            this.iRate.ReadOnly = true;
            this.iRate.ToolTipText = "Sales Rate";
            this.iRate.Width = 60;
            // 
            // iUOM
            // 
            this.iUOM.DataPropertyName = "UOMName";
            this.iUOM.HeaderText = "UOM";
            this.iUOM.Name = "iUOM";
            this.iUOM.ReadOnly = true;
            this.iUOM.ToolTipText = "UOM";
            this.iUOM.Width = 60;
            // 
            // iMRP
            // 
            this.iMRP.DataPropertyName = "MRP";
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle43.Format = "N2";
            this.iMRP.DefaultCellStyle = dataGridViewCellStyle43;
            this.iMRP.HeaderText = "MRP";
            this.iMRP.Name = "iMRP";
            this.iMRP.ReadOnly = true;
            this.iMRP.ToolTipText = "MRP";
            this.iMRP.Width = 60;
            // 
            // iStock
            // 
            this.iStock.DataPropertyName = "Stock";
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle44.Format = "N2";
            this.iStock.DefaultCellStyle = dataGridViewCellStyle44;
            this.iStock.HeaderText = "Stock";
            this.iStock.Name = "iStock";
            this.iStock.ReadOnly = true;
            this.iStock.ToolTipText = "Stock QTY";
            this.iStock.Width = 60;
            // 
            // iUOMStk
            // 
            this.iUOMStk.DataPropertyName = "stkUOM";
            this.iUOMStk.HeaderText = "UOM";
            this.iUOMStk.Name = "iUOMStk";
            this.iUOMStk.ReadOnly = true;
            this.iUOMStk.ToolTipText = "Stock UOM";
            this.iUOMStk.Visible = false;
            this.iUOMStk.Width = 50;
            // 
            // iSaleTax
            // 
            this.iSaleTax.DataPropertyName = "SaleTax";
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle45.Format = "N2";
            dataGridViewCellStyle45.NullValue = null;
            this.iSaleTax.DefaultCellStyle = dataGridViewCellStyle45;
            this.iSaleTax.HeaderText = "S.Tax";
            this.iSaleTax.Name = "iSaleTax";
            this.iSaleTax.ReadOnly = true;
            this.iSaleTax.ToolTipText = "Sales Tax Percent";
            this.iSaleTax.Visible = false;
            this.iSaleTax.Width = 50;
            // 
            // iPurTax
            // 
            this.iPurTax.DataPropertyName = "PurTax";
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle46.Format = "N2";
            dataGridViewCellStyle46.NullValue = null;
            this.iPurTax.DefaultCellStyle = dataGridViewCellStyle46;
            this.iPurTax.HeaderText = "P.Tax";
            this.iPurTax.Name = "iPurTax";
            this.iPurTax.ReadOnly = true;
            this.iPurTax.ToolTipText = "Purchase Tax Percent";
            this.iPurTax.Visible = false;
            this.iPurTax.Width = 50;
            // 
            // iCompany
            // 
            this.iCompany.DataPropertyName = "CompanyNo";
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle47.Format = "N0";
            dataGridViewCellStyle47.NullValue = null;
            this.iCompany.DefaultCellStyle = dataGridViewCellStyle47;
            this.iCompany.HeaderText = "CNo";
            this.iCompany.Name = "iCompany";
            this.iCompany.ReadOnly = true;
            this.iCompany.ToolTipText = "Company No";
            this.iCompany.Visible = false;
            this.iCompany.Width = 30;
            // 
            // iBarcode
            // 
            this.iBarcode.DataPropertyName = "Barcode";
            this.iBarcode.HeaderText = "Barcode";
            this.iBarcode.Name = "iBarcode";
            this.iBarcode.ReadOnly = true;
            this.iBarcode.ToolTipText = "Barcode";
            // 
            // iRateSettingNo
            // 
            this.iRateSettingNo.DataPropertyName = "RateSettingNo";
            this.iRateSettingNo.HeaderText = "RateSettingNo";
            this.iRateSettingNo.Name = "iRateSettingNo";
            this.iRateSettingNo.ReadOnly = true;
            this.iRateSettingNo.Visible = false;
            // 
            // UOMDefault
            // 
            this.UOMDefault.DataPropertyName = "UOMDefault";
            this.UOMDefault.HeaderText = "UOMDefault";
            this.UOMDefault.Name = "UOMDefault";
            this.UOMDefault.ReadOnly = true;
            this.UOMDefault.Visible = false;
            // 
            // PurRate
            // 
            this.PurRate.DataPropertyName = "PurRate";
            dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PurRate.DefaultCellStyle = dataGridViewCellStyle48;
            this.PurRate.HeaderText = "PurRate";
            this.PurRate.Name = "PurRate";
            this.PurRate.ReadOnly = true;
            this.PurRate.Visible = false;
            this.PurRate.Width = 60;
            // 
            // MGodownNo
            // 
            this.MGodownNo.DataPropertyName = "GodownNo";
            this.MGodownNo.HeaderText = "MGodownNo";
            this.MGodownNo.Name = "MGodownNo";
            this.MGodownNo.Visible = false;
            // 
            // MDiscType
            // 
            this.MDiscType.DataPropertyName = "DiscountType";
            this.MDiscType.HeaderText = "MDiscType";
            this.MDiscType.Name = "MDiscType";
            this.MDiscType.Visible = false;
            // 
            // IHamaliInKg
            // 
            this.IHamaliInKg.DataPropertyName = "HamaliInKg";
            this.IHamaliInKg.HeaderText = "HamaliInKg";
            this.IHamaliInKg.Name = "IHamaliInKg";
            this.IHamaliInKg.Visible = false;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Location = new System.Drawing.Point(1, 3);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(136, 13);
            this.label31.TabIndex = 516;
            this.label31.Text = "(F6 -Last Sale Rate Details)";
            this.label31.Visible = false;
            // 
            // lblBrandName
            // 
            this.lblBrandName.BackColor = System.Drawing.Color.Coral;
            this.lblBrandName.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblBrandName.ForeColor = System.Drawing.Color.Black;
            this.lblBrandName.Location = new System.Drawing.Point(0, 0);
            this.lblBrandName.Name = "lblBrandName";
            this.lblBrandName.Size = new System.Drawing.Size(232, 23);
            this.lblBrandName.TabIndex = 517;
            this.lblBrandName.Text = "(F6 -Last Sale Rate Details)";
            this.lblBrandName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lstUOM
            // 
            this.lstUOM.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstUOM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstUOM.FormattingEnabled = true;
            this.lstUOM.Location = new System.Drawing.Point(8, 8);
            this.lstUOM.Name = "lstUOM";
            this.lstUOM.Size = new System.Drawing.Size(101, 28);
            this.lstUOM.TabIndex = 516;
            this.lstUOM.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            this.lstUOM.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lstUOM_KeyPress);
            // 
            // lstRate
            // 
            this.lstRate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstRate.FormattingEnabled = true;
            this.lstRate.Location = new System.Drawing.Point(8, 8);
            this.lstRate.Name = "lstRate";
            this.lstRate.Size = new System.Drawing.Size(101, 41);
            this.lstRate.TabIndex = 517;
            this.lstRate.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            this.lstRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lstRate_KeyPress);
            // 
            // lstGroup1
            // 
            this.lstGroup1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstGroup1.FormattingEnabled = true;
            this.lstGroup1.Location = new System.Drawing.Point(8, 27);
            this.lstGroup1.Name = "lstGroup1";
            this.lstGroup1.Size = new System.Drawing.Size(400, 340);
            this.lstGroup1.TabIndex = 516;
            this.lstGroup1.SelectedIndexChanged += new System.EventHandler(this.lstGroup1_SelectedIndexChanged);
            this.lstGroup1.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            this.lstGroup1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstGroup1_KeyDown);
            this.lstGroup1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lstGroup1_KeyPress);
            // 
            // lstGroup2
            // 
            this.lstGroup2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstGroup2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstGroup2.FormattingEnabled = true;
            this.lstGroup2.Location = new System.Drawing.Point(8, 8);
            this.lstGroup2.Name = "lstGroup2";
            this.lstGroup2.Size = new System.Drawing.Size(101, 28);
            this.lstGroup2.TabIndex = 517;
            this.lstGroup2.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            this.lstGroup2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lstGroup2_KeyPress);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Location = new System.Drawing.Point(10, 40);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(46, 13);
            this.label33.TabIndex = 551;
            this.label33.Text = "Party   : ";
            // 
            // cmbPartyName
            // 
            this.cmbPartyName.FormattingEnabled = true;
            this.cmbPartyName.Location = new System.Drawing.Point(64, 36);
            this.cmbPartyName.Name = "cmbPartyName";
            this.cmbPartyName.Size = new System.Drawing.Size(589, 21);
            this.cmbPartyName.TabIndex = 5521;
            this.cmbPartyName.SelectedValueChanged += new System.EventHandler(this.cmbPartyName_SelectedValueChanged);
            this.cmbPartyName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbPartyName_KeyDown);
            this.cmbPartyName.Leave += new System.EventHandler(this.cmbPartyName_Leave);
            // 
            // cmbTaxType
            // 
            this.cmbTaxType.FormattingEnabled = true;
            this.cmbTaxType.Location = new System.Drawing.Point(868, 36);
            this.cmbTaxType.Name = "cmbTaxType";
            this.cmbTaxType.Size = new System.Drawing.Size(188, 21);
            this.cmbTaxType.TabIndex = 554;
            this.cmbTaxType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbTaxType_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(788, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 5542;
            this.label4.Text = "Tax Type   :";
            // 
            // lblRateType
            // 
            this.lblRateType.AutoSize = true;
            this.lblRateType.BackColor = System.Drawing.Color.Transparent;
            this.lblRateType.Location = new System.Drawing.Point(788, 10);
            this.lblRateType.Name = "lblRateType";
            this.lblRateType.Size = new System.Drawing.Size(63, 13);
            this.lblRateType.TabIndex = 556;
            this.lblRateType.Text = "Rate Type :";
            // 
            // cmbRateType
            // 
            this.cmbRateType.Enabled = false;
            this.cmbRateType.FormattingEnabled = true;
            this.cmbRateType.Location = new System.Drawing.Point(868, 6);
            this.cmbRateType.Name = "cmbRateType";
            this.cmbRateType.Size = new System.Drawing.Size(188, 21);
            this.cmbRateType.TabIndex = 553;
            this.cmbRateType.SelectedIndexChanged += new System.EventHandler(this.cmbRateType_SelectedIndexChanged);
            this.cmbRateType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbRateType_KeyDown);
            // 
            // pnlGroup1
            // 
            this.pnlGroup1.BackColor = System.Drawing.Color.Coral;
            this.pnlGroup1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlGroup1.Controls.Add(this.txtBrandFilter);
            this.pnlGroup1.Controls.Add(this.lstGroup1Lang);
            this.pnlGroup1.Controls.Add(this.lstGroup1);
            this.pnlGroup1.Location = new System.Drawing.Point(18, 136);
            this.pnlGroup1.Name = "pnlGroup1";
            this.pnlGroup1.Size = new System.Drawing.Size(121, 48);
            this.pnlGroup1.TabIndex = 5543;
            this.pnlGroup1.Visible = false;
            this.pnlGroup1.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            // 
            // txtBrandFilter
            // 
            this.txtBrandFilter.Location = new System.Drawing.Point(8, 2);
            this.txtBrandFilter.Name = "txtBrandFilter";
            this.txtBrandFilter.Size = new System.Drawing.Size(802, 20);
            this.txtBrandFilter.TabIndex = 519;
            this.txtBrandFilter.TextChanged += new System.EventHandler(this.txtBrandFilter_TextChanged);
            this.txtBrandFilter.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBrandFilter_KeyDown);
            // 
            // lstGroup1Lang
            // 
            this.lstGroup1Lang.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstGroup1Lang.FormattingEnabled = true;
            this.lstGroup1Lang.Location = new System.Drawing.Point(410, 27);
            this.lstGroup1Lang.Name = "lstGroup1Lang";
            this.lstGroup1Lang.Size = new System.Drawing.Size(400, 262);
            this.lstGroup1Lang.TabIndex = 517;
            this.lstGroup1Lang.SelectedIndexChanged += new System.EventHandler(this.lstGroup1Lang_SelectedIndexChanged);
            this.lstGroup1Lang.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lstGroup1_KeyPress);
            // 
            // pnlGroup2
            // 
            this.pnlGroup2.BackColor = System.Drawing.Color.Coral;
            this.pnlGroup2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlGroup2.Controls.Add(this.lstGroup2);
            this.pnlGroup2.Location = new System.Drawing.Point(19, 203);
            this.pnlGroup2.Name = "pnlGroup2";
            this.pnlGroup2.Size = new System.Drawing.Size(118, 47);
            this.pnlGroup2.TabIndex = 5544;
            this.pnlGroup2.Visible = false;
            this.pnlGroup2.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            // 
            // pnlUOM
            // 
            this.pnlUOM.BackColor = System.Drawing.Color.Coral;
            this.pnlUOM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlUOM.Controls.Add(this.lstUOM);
            this.pnlUOM.Location = new System.Drawing.Point(18, 256);
            this.pnlUOM.Name = "pnlUOM";
            this.pnlUOM.Size = new System.Drawing.Size(118, 50);
            this.pnlUOM.TabIndex = 5545;
            this.pnlUOM.Visible = false;
            this.pnlUOM.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            // 
            // pnlRate
            // 
            this.pnlRate.BackColor = System.Drawing.Color.Coral;
            this.pnlRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRate.Controls.Add(this.lstRate);
            this.pnlRate.Location = new System.Drawing.Point(496, 134);
            this.pnlRate.Name = "pnlRate";
            this.pnlRate.Size = new System.Drawing.Size(118, 70);
            this.pnlRate.TabIndex = 5546;
            this.pnlRate.Visible = false;
            this.pnlRate.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Coral;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Location = new System.Drawing.Point(537, 174);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(118, 47);
            this.panel4.TabIndex = 5547;
            this.panel4.Visible = false;
            // 
            // pnlSalePurHistory
            // 
            this.pnlSalePurHistory.BackColor = System.Drawing.Color.Coral;
            this.pnlSalePurHistory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSalePurHistory.Controls.Add(this.dgPurHistory);
            this.pnlSalePurHistory.Controls.Add(this.dgSaleHistory);
            this.pnlSalePurHistory.Location = new System.Drawing.Point(1243, 11);
            this.pnlSalePurHistory.Name = "pnlSalePurHistory";
            this.pnlSalePurHistory.Size = new System.Drawing.Size(23, 20);
            this.pnlSalePurHistory.TabIndex = 5548;
            this.pnlSalePurHistory.Visible = false;
            // 
            // dgPurHistory
            // 
            this.dgPurHistory.AllowUserToAddRows = false;
            this.dgPurHistory.AllowUserToDeleteRows = false;
            this.dgPurHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgPurHistory.BackgroundColor = System.Drawing.Color.Coral;
            this.dgPurHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPurHistory.Location = new System.Drawing.Point(337, 8);
            this.dgPurHistory.Name = "dgPurHistory";
            this.dgPurHistory.Size = new System.Drawing.Size(0, 3);
            this.dgPurHistory.TabIndex = 516;
            // 
            // dgSaleHistory
            // 
            this.dgSaleHistory.AllowUserToAddRows = false;
            this.dgSaleHistory.AllowUserToDeleteRows = false;
            this.dgSaleHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgSaleHistory.BackgroundColor = System.Drawing.Color.Coral;
            this.dgSaleHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSaleHistory.Location = new System.Drawing.Point(8, 8);
            this.dgSaleHistory.Name = "dgSaleHistory";
            this.dgSaleHistory.Size = new System.Drawing.Size(3, 3);
            this.dgSaleHistory.TabIndex = 515;
            // 
            // pnlRateType
            // 
            this.pnlRateType.BackColor = System.Drawing.Color.Transparent;
            this.pnlRateType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRateType.Controls.Add(this.btnRateTypeCancel);
            this.pnlRateType.Controls.Add(this.btnRateTypeOK);
            this.pnlRateType.Controls.Add(this.txtRateTypePassword);
            this.pnlRateType.Controls.Add(this.label40);
            this.pnlRateType.Controls.Add(this.label44);
            this.pnlRateType.Location = new System.Drawing.Point(205, 206);
            this.pnlRateType.Name = "pnlRateType";
            this.pnlRateType.Size = new System.Drawing.Size(409, 104);
            this.pnlRateType.TabIndex = 5551;
            this.pnlRateType.Visible = false;
            // 
            // btnRateTypeCancel
            // 
            this.btnRateTypeCancel.Location = new System.Drawing.Point(209, 64);
            this.btnRateTypeCancel.Name = "btnRateTypeCancel";
            this.btnRateTypeCancel.Size = new System.Drawing.Size(75, 23);
            this.btnRateTypeCancel.TabIndex = 709;
            this.btnRateTypeCancel.Text = "Cancel";
            this.btnRateTypeCancel.UseVisualStyleBackColor = true;
            this.btnRateTypeCancel.Click += new System.EventHandler(this.btnRateTypeCancel_Click);
            // 
            // btnRateTypeOK
            // 
            this.btnRateTypeOK.Location = new System.Drawing.Point(110, 64);
            this.btnRateTypeOK.Name = "btnRateTypeOK";
            this.btnRateTypeOK.Size = new System.Drawing.Size(75, 23);
            this.btnRateTypeOK.TabIndex = 706;
            this.btnRateTypeOK.Text = "OK";
            this.btnRateTypeOK.UseVisualStyleBackColor = true;
            this.btnRateTypeOK.Click += new System.EventHandler(this.btnRateTypeOK_Click);
            // 
            // txtRateTypePassword
            // 
            this.txtRateTypePassword.Location = new System.Drawing.Point(137, 30);
            this.txtRateTypePassword.MaxLength = 6;
            this.txtRateTypePassword.Name = "txtRateTypePassword";
            this.txtRateTypePassword.PasswordChar = '*';
            this.txtRateTypePassword.Size = new System.Drawing.Size(187, 20);
            this.txtRateTypePassword.TabIndex = 703;
            this.txtRateTypePassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRateTypePassword_KeyDown);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(34, 31);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(53, 13);
            this.label40.TabIndex = 3;
            this.label40.Text = "Password";
            // 
            // label44
            // 
            this.label44.BackColor = System.Drawing.Color.Coral;
            this.label44.Dock = System.Windows.Forms.DockStyle.Top;
            this.label44.Location = new System.Drawing.Point(0, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(407, 13);
            this.label44.TabIndex = 0;
            this.label44.Text = "Rate Type Password Details";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlPartyName
            // 
            this.pnlPartyName.BackColor = System.Drawing.Color.Transparent;
            this.pnlPartyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPartyName.Controls.Add(this.btnPartyCancel);
            this.pnlPartyName.Controls.Add(this.btnPartyOK);
            this.pnlPartyName.Controls.Add(this.txtPartyName);
            this.pnlPartyName.Controls.Add(this.label25);
            this.pnlPartyName.Controls.Add(this.label39);
            this.pnlPartyName.Location = new System.Drawing.Point(205, 262);
            this.pnlPartyName.Name = "pnlPartyName";
            this.pnlPartyName.Size = new System.Drawing.Size(409, 104);
            this.pnlPartyName.TabIndex = 5552;
            this.pnlPartyName.Visible = false;
            // 
            // btnPartyCancel
            // 
            this.btnPartyCancel.Location = new System.Drawing.Point(209, 64);
            this.btnPartyCancel.Name = "btnPartyCancel";
            this.btnPartyCancel.Size = new System.Drawing.Size(75, 23);
            this.btnPartyCancel.TabIndex = 705;
            this.btnPartyCancel.Text = "Cancel";
            this.btnPartyCancel.UseVisualStyleBackColor = true;
            this.btnPartyCancel.Click += new System.EventHandler(this.btnPartyCancel_Click);
            // 
            // btnPartyOK
            // 
            this.btnPartyOK.Location = new System.Drawing.Point(110, 64);
            this.btnPartyOK.Name = "btnPartyOK";
            this.btnPartyOK.Size = new System.Drawing.Size(75, 23);
            this.btnPartyOK.TabIndex = 704;
            this.btnPartyOK.Text = "OK";
            this.btnPartyOK.UseVisualStyleBackColor = true;
            this.btnPartyOK.Click += new System.EventHandler(this.btnPartyOK_Click);
            // 
            // txtPartyName
            // 
            this.txtPartyName.Location = new System.Drawing.Point(137, 30);
            this.txtPartyName.MaxLength = 50;
            this.txtPartyName.Name = "txtPartyName";
            this.txtPartyName.Size = new System.Drawing.Size(187, 20);
            this.txtPartyName.TabIndex = 703;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(34, 31);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(68, 13);
            this.label25.TabIndex = 3;
            this.label25.Text = "Party Name :";
            // 
            // label39
            // 
            this.label39.BackColor = System.Drawing.Color.Coral;
            this.label39.Dock = System.Windows.Forms.DockStyle.Top;
            this.label39.Location = new System.Drawing.Point(0, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(407, 13);
            this.label39.TabIndex = 0;
            this.label39.Text = "Display Party Name Details";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlParking
            // 
            this.pnlParking.Controls.Add(this.label37);
            this.pnlParking.Controls.Add(this.dgParkingBills);
            this.pnlParking.Location = new System.Drawing.Point(140, 174);
            this.pnlParking.Name = "pnlParking";
            this.pnlParking.Size = new System.Drawing.Size(398, 156);
            this.pnlParking.TabIndex = 5554;
            this.pnlParking.Visible = false;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(186, 139);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(147, 13);
            this.label37.TabIndex = 1;
            this.label37.Text = "Parking Bill Cancel(Ctrl+Alt+C)";
            // 
            // dgParkingBills
            // 
            this.dgParkingBills.AllowUserToAddRows = false;
            this.dgParkingBills.AllowUserToDeleteRows = false;
            this.dgParkingBills.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgParkingBills.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn6,
            this.ItemQty,
            this.BillDate,
            this.BillTime,
            this.ParkingBillNo});
            this.dgParkingBills.Location = new System.Drawing.Point(5, 4);
            this.dgParkingBills.Name = "dgParkingBills";
            this.dgParkingBills.ReadOnly = true;
            this.dgParkingBills.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgParkingBills.Size = new System.Drawing.Size(388, 132);
            this.dgParkingBills.TabIndex = 0;
            this.dgParkingBills.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgParkingBills_CellFormatting);
            this.dgParkingBills.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgParkingBills_KeyDown);
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "BillNo";
            this.dataGridViewTextBoxColumn3.HeaderText = "BillNo";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 50;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Name";
            dataGridViewCellStyle49.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle49;
            this.dataGridViewTextBoxColumn6.HeaderText = "Person Name";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 130;
            // 
            // ItemQty
            // 
            this.ItemQty.DataPropertyName = "ItemQty";
            this.ItemQty.HeaderText = "Qty";
            this.ItemQty.Name = "ItemQty";
            this.ItemQty.ReadOnly = true;
            this.ItemQty.Width = 50;
            // 
            // BillDate
            // 
            this.BillDate.DataPropertyName = "BillDate";
            this.BillDate.HeaderText = "BillDate";
            this.BillDate.Name = "BillDate";
            this.BillDate.ReadOnly = true;
            this.BillDate.Width = 80;
            // 
            // BillTime
            // 
            this.BillTime.DataPropertyName = "BillTime";
            this.BillTime.HeaderText = "BillTime";
            this.BillTime.Name = "BillTime";
            this.BillTime.ReadOnly = true;
            this.BillTime.Width = 60;
            // 
            // ParkingBillNo
            // 
            this.ParkingBillNo.DataPropertyName = "ParkingBillNo";
            this.ParkingBillNo.HeaderText = "ParkingBillNo";
            this.ParkingBillNo.Name = "ParkingBillNo";
            this.ParkingBillNo.ReadOnly = true;
            this.ParkingBillNo.Visible = false;
            // 
            // pnlMainParking
            // 
            this.pnlMainParking.BackColor = System.Drawing.Color.Transparent;
            this.pnlMainParking.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMainParking.Controls.Add(this.btnParkingCancel);
            this.pnlMainParking.Controls.Add(this.btnParkingOk);
            this.pnlMainParking.Controls.Add(this.txtPersonName);
            this.pnlMainParking.Controls.Add(this.label15);
            this.pnlMainParking.Controls.Add(this.label17);
            this.pnlMainParking.Location = new System.Drawing.Point(205, 269);
            this.pnlMainParking.Name = "pnlMainParking";
            this.pnlMainParking.Size = new System.Drawing.Size(409, 104);
            this.pnlMainParking.TabIndex = 5553;
            this.pnlMainParking.Visible = false;
            // 
            // btnParkingCancel
            // 
            this.btnParkingCancel.Location = new System.Drawing.Point(209, 64);
            this.btnParkingCancel.Name = "btnParkingCancel";
            this.btnParkingCancel.Size = new System.Drawing.Size(75, 23);
            this.btnParkingCancel.TabIndex = 705;
            this.btnParkingCancel.Text = "Cancel";
            this.btnParkingCancel.UseVisualStyleBackColor = true;
            this.btnParkingCancel.Click += new System.EventHandler(this.btnParkingCancel_Click);
            // 
            // btnParkingOk
            // 
            this.btnParkingOk.Location = new System.Drawing.Point(110, 64);
            this.btnParkingOk.Name = "btnParkingOk";
            this.btnParkingOk.Size = new System.Drawing.Size(75, 23);
            this.btnParkingOk.TabIndex = 704;
            this.btnParkingOk.Text = "OK";
            this.btnParkingOk.UseVisualStyleBackColor = true;
            this.btnParkingOk.Click += new System.EventHandler(this.btnParkingOk_Click);
            // 
            // txtPersonName
            // 
            this.txtPersonName.Location = new System.Drawing.Point(137, 30);
            this.txtPersonName.MaxLength = 50;
            this.txtPersonName.Name = "txtPersonName";
            this.txtPersonName.Size = new System.Drawing.Size(187, 20);
            this.txtPersonName.TabIndex = 703;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(34, 31);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(77, 13);
            this.label15.TabIndex = 3;
            this.label15.Text = "Person Name :";
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.Coral;
            this.label17.Dock = System.Windows.Forms.DockStyle.Top;
            this.label17.Location = new System.Drawing.Point(0, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(407, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Parking Bill Details";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(1062, 266);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(69, 60);
            this.btnPrint.TabIndex = 5555;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnAdvanceSearch
            // 
            this.btnAdvanceSearch.Location = new System.Drawing.Point(659, 36);
            this.btnAdvanceSearch.Name = "btnAdvanceSearch";
            this.btnAdvanceSearch.Size = new System.Drawing.Size(21, 21);
            this.btnAdvanceSearch.TabIndex = 5560;
            this.btnAdvanceSearch.Text = "..";
            this.btnAdvanceSearch.UseVisualStyleBackColor = true;
            this.btnAdvanceSearch.Click += new System.EventHandler(this.btnAdvanceSearch_Click);
            // 
            // btnSalesRegister
            // 
            this.btnSalesRegister.Location = new System.Drawing.Point(1138, 68);
            this.btnSalesRegister.Name = "btnSalesRegister";
            this.btnSalesRegister.Size = new System.Drawing.Size(69, 60);
            this.btnSalesRegister.TabIndex = 5561;
            this.btnSalesRegister.Text = "Sales Reg.";
            this.btnSalesRegister.UseVisualStyleBackColor = true;
            this.btnSalesRegister.Click += new System.EventHandler(this.btnSalesRegister_Click);
            // 
            // btnOrderType
            // 
            this.btnOrderType.Location = new System.Drawing.Point(1138, 134);
            this.btnOrderType.Name = "btnOrderType";
            this.btnOrderType.Size = new System.Drawing.Size(69, 60);
            this.btnOrderType.TabIndex = 5562;
            this.btnOrderType.Text = ".";
            this.btnOrderType.UseVisualStyleBackColor = true;
            this.btnOrderType.Click += new System.EventHandler(this.btnOrderType_Click);
            // 
            // btnNewCustomer
            // 
            this.btnNewCustomer.Location = new System.Drawing.Point(1138, 200);
            this.btnNewCustomer.Name = "btnNewCustomer";
            this.btnNewCustomer.Size = new System.Drawing.Size(69, 60);
            this.btnNewCustomer.TabIndex = 5563;
            this.btnNewCustomer.Text = "(N) Party";
            this.btnNewCustomer.UseVisualStyleBackColor = true;
            this.btnNewCustomer.Click += new System.EventHandler(this.btnNewCustomer_Click);
            // 
            // btnShortcut
            // 
            this.btnShortcut.Location = new System.Drawing.Point(1138, 332);
            this.btnShortcut.Name = "btnShortcut";
            this.btnShortcut.Size = new System.Drawing.Size(69, 60);
            this.btnShortcut.TabIndex = 5564;
            this.btnShortcut.Text = "( F1 )   Help";
            this.btnShortcut.UseVisualStyleBackColor = true;
            this.btnShortcut.Click += new System.EventHandler(this.btnShortcut_Click);
            // 
            // btnShowDetails
            // 
            this.btnShowDetails.Location = new System.Drawing.Point(1139, 266);
            this.btnShowDetails.Name = "btnShowDetails";
            this.btnShowDetails.Size = new System.Drawing.Size(69, 60);
            this.btnShowDetails.TabIndex = 5565;
            this.btnShowDetails.Text = "Show Detail";
            this.btnShowDetails.UseVisualStyleBackColor = true;
            this.btnShowDetails.Click += new System.EventHandler(this.btnShowDetails_Click);
            // 
            // btnTodaysSales
            // 
            this.btnTodaysSales.Location = new System.Drawing.Point(1138, 2);
            this.btnTodaysSales.Name = "btnTodaysSales";
            this.btnTodaysSales.Size = new System.Drawing.Size(69, 60);
            this.btnTodaysSales.TabIndex = 5566;
            this.btnTodaysSales.Text = "Todays Sales";
            this.btnTodaysSales.UseVisualStyleBackColor = true;
            this.btnTodaysSales.Click += new System.EventHandler(this.btnTodaysSales_Click);
            // 
            // btnCashSave
            // 
            this.btnCashSave.Location = new System.Drawing.Point(1062, 200);
            this.btnCashSave.Name = "btnCashSave";
            this.btnCashSave.Size = new System.Drawing.Size(69, 60);
            this.btnCashSave.TabIndex = 5568;
            this.btnCashSave.Text = "Cash\r\nSave\r\n(F12)";
            this.btnCashSave.UseVisualStyleBackColor = true;
            this.btnCashSave.Click += new System.EventHandler(this.btnCashSave_Click);
            this.btnCashSave.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnCashSave_KeyDown);
            // 
            // btnCreditSave
            // 
            this.btnCreditSave.Location = new System.Drawing.Point(1062, 266);
            this.btnCreditSave.Name = "btnCreditSave";
            this.btnCreditSave.Size = new System.Drawing.Size(69, 60);
            this.btnCreditSave.TabIndex = 5569;
            this.btnCreditSave.Text = "Credit\r\nSave\r\n(F11)";
            this.btnCreditSave.UseVisualStyleBackColor = true;
            this.btnCreditSave.Click += new System.EventHandler(this.btnCreditSave_Click);
            // 
            // btnCCSave
            // 
            this.btnCCSave.Location = new System.Drawing.Point(1063, 134);
            this.btnCCSave.Name = "btnCCSave";
            this.btnCCSave.Size = new System.Drawing.Size(69, 60);
            this.btnCCSave.TabIndex = 5570;
            this.btnCCSave.Text = "Cr.Card\r\nSave";
            this.btnCCSave.UseVisualStyleBackColor = true;
            this.btnCCSave.Visible = false;
            this.btnCCSave.Click += new System.EventHandler(this.btnCCSave_Click);
            // 
            // pnlTodaysSales
            // 
            this.pnlTodaysSales.BackColor = System.Drawing.Color.Transparent;
            this.pnlTodaysSales.BorderColor = System.Drawing.Color.Gray;
            this.pnlTodaysSales.BorderRadius = 3;
            this.pnlTodaysSales.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTodaysSales.Controls.Add(this.dgSalesDetails);
            this.pnlTodaysSales.Controls.Add(this.label48);
            this.pnlTodaysSales.Controls.Add(this.lblFV);
            this.pnlTodaysSales.Controls.Add(this.lblCc);
            this.pnlTodaysSales.Controls.Add(this.label19);
            this.pnlTodaysSales.Controls.Add(this.lblTotAmt);
            this.pnlTodaysSales.Controls.Add(this.lblTodaysSales);
            this.pnlTodaysSales.Controls.Add(this.label28);
            this.pnlTodaysSales.Controls.Add(this.label22);
            this.pnlTodaysSales.Controls.Add(this.lblBillCnt);
            this.pnlTodaysSales.Controls.Add(this.label18);
            this.pnlTodaysSales.Controls.Add(this.label16);
            this.pnlTodaysSales.Controls.Add(this.lblCredit);
            this.pnlTodaysSales.Controls.Add(this.lblCheque);
            this.pnlTodaysSales.Controls.Add(this.lbl);
            this.pnlTodaysSales.Controls.Add(this.lblCash);
            this.pnlTodaysSales.Location = new System.Drawing.Point(1271, 167);
            this.pnlTodaysSales.Name = "pnlTodaysSales";
            this.pnlTodaysSales.Size = new System.Drawing.Size(214, 188);
            this.pnlTodaysSales.TabIndex = 5571;
            this.pnlTodaysSales.Visible = false;
            this.pnlTodaysSales.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlTodaysSales_Paint);
            // 
            // dgSalesDetails
            // 
            this.dgSalesDetails.AllowUserToAddRows = false;
            this.dgSalesDetails.AllowUserToDeleteRows = false;
            this.dgSalesDetails.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgSalesDetails.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgSalesDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSalesDetails.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgSalesDetails.Location = new System.Drawing.Point(0, 23);
            this.dgSalesDetails.Name = "dgSalesDetails";
            this.dgSalesDetails.ReadOnly = true;
            this.dgSalesDetails.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgSalesDetails.Size = new System.Drawing.Size(212, 121);
            this.dgSalesDetails.TabIndex = 911;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(9, 115);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(74, 13);
            this.label48.TabIndex = 909;
            this.label48.Text = "Food Voucher";
            // 
            // lblFV
            // 
            this.lblFV.Location = new System.Drawing.Point(87, 115);
            this.lblFV.Name = "lblFV";
            this.lblFV.Size = new System.Drawing.Size(103, 13);
            this.lblFV.TabIndex = 910;
            this.lblFV.Text = "0.00";
            this.lblFV.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCc
            // 
            this.lblCc.Location = new System.Drawing.Point(87, 94);
            this.lblCc.Name = "lblCc";
            this.lblCc.Size = new System.Drawing.Size(103, 13);
            this.lblCc.TabIndex = 908;
            this.lblCc.Text = "0.00";
            this.lblCc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(9, 94);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(59, 13);
            this.label19.TabIndex = 907;
            this.label19.Text = "Credit Card";
            // 
            // lblTotAmt
            // 
            this.lblTotAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotAmt.Location = new System.Drawing.Point(104, 147);
            this.lblTotAmt.Name = "lblTotAmt";
            this.lblTotAmt.Size = new System.Drawing.Size(105, 13);
            this.lblTotAmt.TabIndex = 906;
            this.lblTotAmt.Text = "0.00";
            this.lblTotAmt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTodaysSales
            // 
            this.lblTodaysSales.BackColor = System.Drawing.Color.Coral;
            this.lblTodaysSales.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTodaysSales.ForeColor = System.Drawing.Color.White;
            this.lblTodaysSales.Location = new System.Drawing.Point(0, 0);
            this.lblTodaysSales.Name = "lblTodaysSales";
            this.lblTodaysSales.Size = new System.Drawing.Size(212, 23);
            this.lblTodaysSales.TabIndex = 905;
            this.lblTodaysSales.Text = "Todays Sales";
            this.lblTodaysSales.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(9, 27);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(31, 13);
            this.label28.TabIndex = 600;
            this.label28.Text = "Cash";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(9, 48);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(34, 13);
            this.label22.TabIndex = 601;
            this.label22.Text = "Credit";
            // 
            // lblBillCnt
            // 
            this.lblBillCnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBillCnt.Location = new System.Drawing.Point(125, 170);
            this.lblBillCnt.Name = "lblBillCnt";
            this.lblBillCnt.Size = new System.Drawing.Size(84, 13);
            this.lblBillCnt.TabIndex = 904;
            this.lblBillCnt.Text = "0";
            this.lblBillCnt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(9, 147);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(31, 13);
            this.label18.TabIndex = 603;
            this.label18.Text = "Total";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(9, 170);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(56, 13);
            this.label16.TabIndex = 604;
            this.label16.Text = "No.Of Bills";
            // 
            // lblCredit
            // 
            this.lblCredit.Location = new System.Drawing.Point(88, 46);
            this.lblCredit.Name = "lblCredit";
            this.lblCredit.Size = new System.Drawing.Size(102, 13);
            this.lblCredit.TabIndex = 901;
            this.lblCredit.Text = "0.00";
            this.lblCredit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCheque
            // 
            this.lblCheque.Location = new System.Drawing.Point(87, 69);
            this.lblCheque.Name = "lblCheque";
            this.lblCheque.Size = new System.Drawing.Size(103, 13);
            this.lblCheque.TabIndex = 902;
            this.lblCheque.Text = "0.00";
            this.lblCheque.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.Location = new System.Drawing.Point(9, 70);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(44, 13);
            this.lbl.TabIndex = 602;
            this.lbl.Text = "Cheque";
            // 
            // lblCash
            // 
            this.lblCash.Location = new System.Drawing.Point(88, 26);
            this.lblCash.Name = "lblCash";
            this.lblCash.Size = new System.Drawing.Size(102, 13);
            this.lblCash.TabIndex = 900;
            this.lblCash.Text = "0.00";
            this.lblCash.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlLastBill
            // 
            this.pnlLastBill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlLastBill.BackColor = System.Drawing.Color.Olive;
            this.pnlLastBill.BorderColor = System.Drawing.Color.Gray;
            this.pnlLastBill.BorderRadius = 3;
            this.pnlLastBill.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLastBill.Controls.Add(this.lblLastBillAmt);
            this.pnlLastBill.Controls.Add(this.label11);
            this.pnlLastBill.Controls.Add(this.label23);
            this.pnlLastBill.Controls.Add(this.label24);
            this.pnlLastBill.Controls.Add(this.lblLastAmt);
            this.pnlLastBill.Controls.Add(this.lblLastPayment);
            this.pnlLastBill.Controls.Add(this.lblLastBillQty);
            this.pnlLastBill.Location = new System.Drawing.Point(1063, 395);
            this.pnlLastBill.Name = "pnlLastBill";
            this.pnlLastBill.Size = new System.Drawing.Size(142, 85);
            this.pnlLastBill.TabIndex = 5567;
            this.pnlLastBill.Visible = false;
            // 
            // lblLastBillAmt
            // 
            this.lblLastBillAmt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLastBillAmt.AutoSize = true;
            this.lblLastBillAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastBillAmt.ForeColor = System.Drawing.Color.White;
            this.lblLastBillAmt.Location = new System.Drawing.Point(60, 25);
            this.lblLastBillAmt.Name = "lblLastBillAmt";
            this.lblLastBillAmt.Size = new System.Drawing.Size(0, 15);
            this.lblLastBillAmt.TabIndex = 531;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(2, 4);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 13);
            this.label11.TabIndex = 530;
            this.label11.Text = "Last Bill";
            // 
            // label23
            // 
            this.label23.BackColor = System.Drawing.Color.Coral;
            this.label23.Location = new System.Drawing.Point(465, 54);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(50, 13);
            this.label23.TabIndex = 506;
            this.label23.Visible = false;
            // 
            // label24
            // 
            this.label24.BackColor = System.Drawing.Color.Coral;
            this.label24.Location = new System.Drawing.Point(409, 54);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(50, 13);
            this.label24.TabIndex = 505;
            this.label24.Visible = false;
            // 
            // lblLastAmt
            // 
            this.lblLastAmt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLastAmt.AutoSize = true;
            this.lblLastAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastAmt.ForeColor = System.Drawing.Color.White;
            this.lblLastAmt.Location = new System.Drawing.Point(2, 24);
            this.lblLastAmt.Name = "lblLastAmt";
            this.lblLastAmt.Size = new System.Drawing.Size(31, 13);
            this.lblLastAmt.TabIndex = 523;
            this.lblLastAmt.Text = "Last";
            // 
            // lblLastPayment
            // 
            this.lblLastPayment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLastPayment.AutoSize = true;
            this.lblLastPayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastPayment.ForeColor = System.Drawing.Color.White;
            this.lblLastPayment.Location = new System.Drawing.Point(2, 65);
            this.lblLastPayment.Name = "lblLastPayment";
            this.lblLastPayment.Size = new System.Drawing.Size(55, 13);
            this.lblLastPayment.TabIndex = 529;
            this.lblLastPayment.Text = "Payment";
            // 
            // lblLastBillQty
            // 
            this.lblLastBillQty.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLastBillQty.AutoSize = true;
            this.lblLastBillQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastBillQty.ForeColor = System.Drawing.Color.White;
            this.lblLastBillQty.Location = new System.Drawing.Point(2, 46);
            this.lblLastBillQty.Name = "lblLastBillQty";
            this.lblLastBillQty.Size = new System.Drawing.Size(26, 13);
            this.lblLastBillQty.TabIndex = 528;
            this.lblLastBillQty.Text = "Qty";
            // 
            // pnlFooterInfo
            // 
            this.pnlFooterInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlFooterInfo.BackColor = System.Drawing.Color.Olive;
            this.pnlFooterInfo.BorderColor = System.Drawing.Color.Gray;
            this.pnlFooterInfo.BorderRadius = 3;
            this.pnlFooterInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlFooterInfo.Controls.Add(this.label49);
            this.pnlFooterInfo.Controls.Add(this.label46);
            this.pnlFooterInfo.Controls.Add(this.label45);
            this.pnlFooterInfo.Controls.Add(this.label43);
            this.pnlFooterInfo.Controls.Add(this.label27);
            this.pnlFooterInfo.Controls.Add(this.label30);
            this.pnlFooterInfo.Controls.Add(this.label29);
            this.pnlFooterInfo.Controls.Add(this.label10);
            this.pnlFooterInfo.Controls.Add(this.label9);
            this.pnlFooterInfo.Controls.Add(this.lblStatus);
            this.pnlFooterInfo.Controls.Add(this.label20);
            this.pnlFooterInfo.Controls.Add(this.label21);
            this.pnlFooterInfo.Controls.Add(this.label14);
            this.pnlFooterInfo.Location = new System.Drawing.Point(1187, 430);
            this.pnlFooterInfo.Name = "pnlFooterInfo";
            this.pnlFooterInfo.Size = new System.Drawing.Size(939, 48);
            this.pnlFooterInfo.TabIndex = 71;
            this.pnlFooterInfo.Visible = false;
            // 
            // label49
            // 
            this.label49.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.White;
            this.label49.Location = new System.Drawing.Point(797, 27);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(127, 13);
            this.label49.TabIndex = 532;
            this.label49.Text = "Ctrl +Y : No Discount";
            // 
            // label46
            // 
            this.label46.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.White;
            this.label46.Location = new System.Drawing.Point(836, 6);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(111, 13);
            this.label46.TabIndex = 531;
            this.label46.Text = "Ctrl +Q : New Item";
            // 
            // label45
            // 
            this.label45.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.White;
            this.label45.Location = new System.Drawing.Point(632, 6);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(188, 13);
            this.label45.TabIndex = 530;
            this.label45.Text = "Ctrl +W/F3 : Mixmode Credit Bill";
            // 
            // label43
            // 
            this.label43.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.White;
            this.label43.Location = new System.Drawing.Point(626, 27);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(147, 13);
            this.label43.TabIndex = 529;
            this.label43.Text = "Ctrl +M : Instant Scheme";
            // 
            // label27
            // 
            this.label27.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.White;
            this.label27.Location = new System.Drawing.Point(384, 31);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(179, 13);
            this.label27.TabIndex = 528;
            this.label27.Text = "F8 (Bill Details) : Rate Change";
            // 
            // label30
            // 
            this.label30.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.White;
            this.label30.Location = new System.Drawing.Point(384, 7);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(225, 13);
            this.label30.TabIndex = 527;
            this.label30.Text = "F10 (Party Field) : Display Outstanding";
            // 
            // label29
            // 
            this.label29.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.White;
            this.label29.Location = new System.Drawing.Point(202, 27);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(146, 13);
            this.label29.TabIndex = 526;
            this.label29.Text = "Spacebar : Esc from List";
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(2, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(132, 13);
            this.label10.TabIndex = 525;
            this.label10.Text = "F6 : Show Parking Bill";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(240, 6);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(130, 13);
            this.label9.TabIndex = 524;
            this.label9.Text = "F5 : Save Parking Bill";
            // 
            // lblStatus
            // 
            this.lblStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.ForeColor = System.Drawing.Color.Maroon;
            this.lblStatus.Location = new System.Drawing.Point(13, 85);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(11, 13);
            this.lblStatus.TabIndex = 523;
            this.lblStatus.Text = ".";
            this.lblStatus.Click += new System.EventHandler(this.lblStatus_Click);
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(72, 4);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(68, 13);
            this.label20.TabIndex = 522;
            this.label20.Text = "F12 : Cash";
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(146, 4);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(73, 13);
            this.label21.TabIndex = 521;
            this.label21.Text = "F11 : Credit";
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(3, 4);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(52, 13);
            this.label14.TabIndex = 515;
            this.label14.Text = "F7 : Qty";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BorderColor = System.Drawing.Color.Gray;
            this.panel1.BorderRadius = 3;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.txtAddFreight);
            this.panel1.Controls.Add(this.label41);
            this.panel1.Controls.Add(this.txtHRs);
            this.panel1.Controls.Add(this.label54);
            this.panel1.Controls.Add(this.txtKg);
            this.panel1.Controls.Add(this.label53);
            this.panel1.Controls.Add(this.txtOtherDisc);
            this.panel1.Controls.Add(this.label47);
            this.panel1.Controls.Add(this.txtRemark);
            this.panel1.Controls.Add(this.cmbPaymentType);
            this.panel1.Controls.Add(this.plnPrinting);
            this.panel1.Controls.Add(this.txtChrgRupees1);
            this.panel1.Controls.Add(this.txtDiscRupees1);
            this.panel1.Controls.Add(this.label32);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.txtDiscount1);
            this.panel1.Controls.Add(this.lblChrg1);
            this.panel1.Controls.Add(this.lblDisc1);
            this.panel1.Controls.Add(this.lblBilExchangeItem);
            this.panel1.Controls.Add(this.lblBillItem);
            this.panel1.Controls.Add(this.lblCreditLimit);
            this.panel1.Location = new System.Drawing.Point(13, 395);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(803, 85);
            this.panel1.TabIndex = 91;
            // 
            // txtAddFreight
            // 
            this.txtAddFreight.Location = new System.Drawing.Point(435, 5);
            this.txtAddFreight.MaxLength = 8;
            this.txtAddFreight.Name = "txtAddFreight";
            this.txtAddFreight.Size = new System.Drawing.Size(81, 20);
            this.txtAddFreight.TabIndex = 5581;
            this.txtAddFreight.Text = "0.00";
            this.txtAddFreight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAddFreight.TextChanged += new System.EventHandler(this.txtAddFreight_TextChanged);
            this.txtAddFreight.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAddFreight_KeyDown);
            this.txtAddFreight.Leave += new System.EventHandler(this.Control_Leave);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(340, 5);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(64, 13);
            this.label41.TabIndex = 5582;
            this.label41.Text = "Add Freight.";
            // 
            // txtHRs
            // 
            this.txtHRs.Location = new System.Drawing.Point(292, 5);
            this.txtHRs.Name = "txtHRs";
            this.txtHRs.Size = new System.Drawing.Size(42, 20);
            this.txtHRs.TabIndex = 5580;
            this.txtHRs.Text = "0.00";
            this.txtHRs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtHRs.TextChanged += new System.EventHandler(this.txtHRs_TextChanged);
            this.txtHRs.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHRs_KeyDown);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(257, 5);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(31, 13);
            this.label54.TabIndex = 5579;
            this.label54.Text = "HRs.";
            // 
            // txtKg
            // 
            this.txtKg.Location = new System.Drawing.Point(193, 5);
            this.txtKg.Name = "txtKg";
            this.txtKg.ReadOnly = true;
            this.txtKg.Size = new System.Drawing.Size(60, 20);
            this.txtKg.TabIndex = 5578;
            this.txtKg.Text = "0.00";
            this.txtKg.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKg.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKg_KeyDown);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(168, 5);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(23, 13);
            this.label53.TabIndex = 5577;
            this.label53.Text = "Kg.";
            // 
            // txtOtherDisc
            // 
            this.txtOtherDisc.Location = new System.Drawing.Point(435, 30);
            this.txtOtherDisc.Name = "txtOtherDisc";
            this.txtOtherDisc.Size = new System.Drawing.Size(81, 20);
            this.txtOtherDisc.TabIndex = 5575;
            this.txtOtherDisc.Text = "0.00";
            this.txtOtherDisc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOtherDisc.TextChanged += new System.EventHandler(this.txtOtherDisc_TextChanged);
            this.txtOtherDisc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtOtherDisc_KeyDown);
            this.txtOtherDisc.Leave += new System.EventHandler(this.Control_Leave);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(340, 34);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(67, 13);
            this.label47.TabIndex = 5576;
            this.label47.Text = "Less Freight.";
            // 
            // txtRemark
            // 
            this.txtRemark.Location = new System.Drawing.Point(63, 59);
            this.txtRemark.Name = "txtRemark";
            this.txtRemark.Size = new System.Drawing.Size(453, 20);
            this.txtRemark.TabIndex = 11;
            this.txtRemark.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRemark_KeyDown);
            this.txtRemark.Leave += new System.EventHandler(this.txtRemark_Leave);
            // 
            // cmbPaymentType
            // 
            this.cmbPaymentType.FormattingEnabled = true;
            this.cmbPaymentType.Items.AddRange(new object[] {
            "Cash",
            "Credit Card",
            "PA",
            "PP"});
            this.cmbPaymentType.Location = new System.Drawing.Point(63, 30);
            this.cmbPaymentType.Name = "cmbPaymentType";
            this.cmbPaymentType.Size = new System.Drawing.Size(179, 21);
            this.cmbPaymentType.TabIndex = 10;
            this.cmbPaymentType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbPaymentType_KeyDown);
            this.cmbPaymentType.Leave += new System.EventHandler(this.cmbPaymentType_Leave);
            // 
            // plnPrinting
            // 
            this.plnPrinting.BackColor = System.Drawing.Color.Transparent;
            this.plnPrinting.Controls.Add(this.rbMarathi);
            this.plnPrinting.Controls.Add(this.rbEnglish);
            this.plnPrinting.Location = new System.Drawing.Point(668, 31);
            this.plnPrinting.Name = "plnPrinting";
            this.plnPrinting.Size = new System.Drawing.Size(121, 26);
            this.plnPrinting.TabIndex = 5572;
            // 
            // rbMarathi
            // 
            this.rbMarathi.AutoSize = true;
            this.rbMarathi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbMarathi.Location = new System.Drawing.Point(52, 4);
            this.rbMarathi.Name = "rbMarathi";
            this.rbMarathi.Size = new System.Drawing.Size(74, 21);
            this.rbMarathi.TabIndex = 1;
            this.rbMarathi.TabStop = true;
            this.rbMarathi.Text = "marazI";
            this.rbMarathi.UseVisualStyleBackColor = true;
            // 
            // rbEnglish
            // 
            this.rbEnglish.AutoSize = true;
            this.rbEnglish.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbEnglish.Location = new System.Drawing.Point(3, 6);
            this.rbEnglish.Name = "rbEnglish";
            this.rbEnglish.Size = new System.Drawing.Size(51, 17);
            this.rbEnglish.TabIndex = 0;
            this.rbEnglish.TabStop = true;
            this.rbEnglish.Text = "Eng.";
            this.rbEnglish.UseVisualStyleBackColor = true;
            // 
            // txtChrgRupees1
            // 
            this.txtChrgRupees1.Location = new System.Drawing.Point(635, 5);
            this.txtChrgRupees1.MaxLength = 8;
            this.txtChrgRupees1.Name = "txtChrgRupees1";
            this.txtChrgRupees1.Size = new System.Drawing.Size(81, 20);
            this.txtChrgRupees1.TabIndex = 8;
            this.txtChrgRupees1.Text = "0.00";
            this.txtChrgRupees1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtChrgRupees1.TextChanged += new System.EventHandler(this.txtChrgRupees1_TextChanged);
            this.txtChrgRupees1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtChrgRupees1_KeyDown);
            this.txtChrgRupees1.Leave += new System.EventHandler(this.Control_Leave);
            // 
            // txtDiscRupees1
            // 
            this.txtDiscRupees1.Location = new System.Drawing.Point(111, 5);
            this.txtDiscRupees1.Name = "txtDiscRupees1";
            this.txtDiscRupees1.Size = new System.Drawing.Size(48, 20);
            this.txtDiscRupees1.TabIndex = 6;
            this.txtDiscRupees1.Text = "0.00";
            this.txtDiscRupees1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDiscRupees1.TextChanged += new System.EventHandler(this.txtDiscRupees1_TextChanged);
            this.txtDiscRupees1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDiscRupees1_KeyDown);
            this.txtDiscRupees1.Leave += new System.EventHandler(this.Control_Leave);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(5, 61);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(50, 13);
            this.label32.TabIndex = 519;
            this.label32.Text = "Remark :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 34);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 13);
            this.label13.TabIndex = 509;
            this.label13.Text = "Payment :";
            // 
            // txtDiscount1
            // 
            this.txtDiscount1.Location = new System.Drawing.Point(63, 5);
            this.txtDiscount1.Name = "txtDiscount1";
            this.txtDiscount1.Size = new System.Drawing.Size(45, 20);
            this.txtDiscount1.TabIndex = 4;
            this.txtDiscount1.Text = "0.00";
            this.txtDiscount1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDiscount1.TextChanged += new System.EventHandler(this.txtDiscount1_TextChanged);
            this.txtDiscount1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDiscount1_KeyDown);
            this.txtDiscount1.Leave += new System.EventHandler(this.Control_Leave);
            // 
            // lblChrg1
            // 
            this.lblChrg1.AutoSize = true;
            this.lblChrg1.Location = new System.Drawing.Point(535, 5);
            this.lblChrg1.Name = "lblChrg1";
            this.lblChrg1.Size = new System.Drawing.Size(44, 13);
            this.lblChrg1.TabIndex = 508;
            this.lblChrg1.Text = "Chrg 1. ";
            // 
            // lblDisc1
            // 
            this.lblDisc1.AutoSize = true;
            this.lblDisc1.Location = new System.Drawing.Point(6, 5);
            this.lblDisc1.Name = "lblDisc1";
            this.lblDisc1.Size = new System.Drawing.Size(42, 13);
            this.lblDisc1.TabIndex = 507;
            this.lblDisc1.Text = "Disc % ";
            // 
            // lblBilExchangeItem
            // 
            this.lblBilExchangeItem.BackColor = System.Drawing.Color.Coral;
            this.lblBilExchangeItem.Location = new System.Drawing.Point(721, 66);
            this.lblBilExchangeItem.Name = "lblBilExchangeItem";
            this.lblBilExchangeItem.Size = new System.Drawing.Size(50, 10);
            this.lblBilExchangeItem.TabIndex = 506;
            this.lblBilExchangeItem.Visible = false;
            // 
            // lblBillItem
            // 
            this.lblBillItem.BackColor = System.Drawing.Color.Coral;
            this.lblBillItem.Location = new System.Drawing.Point(665, 66);
            this.lblBillItem.Name = "lblBillItem";
            this.lblBillItem.Size = new System.Drawing.Size(50, 10);
            this.lblBillItem.TabIndex = 505;
            this.lblBillItem.Visible = false;
            // 
            // lblCreditLimit
            // 
            this.lblCreditLimit.AutoSize = true;
            this.lblCreditLimit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreditLimit.ForeColor = System.Drawing.Color.Maroon;
            this.lblCreditLimit.Location = new System.Drawing.Point(535, 28);
            this.lblCreditLimit.Name = "lblCreditLimit";
            this.lblCreditLimit.Size = new System.Drawing.Size(57, 15);
            this.lblCreditLimit.TabIndex = 5568;
            this.lblCreditLimit.Text = "Cr Limit";
            // 
            // txtSchemeDisc
            // 
            this.txtSchemeDisc.Location = new System.Drawing.Point(204, 4);
            this.txtSchemeDisc.Name = "txtSchemeDisc";
            this.txtSchemeDisc.Size = new System.Drawing.Size(53, 20);
            this.txtSchemeDisc.TabIndex = 5573;
            this.txtSchemeDisc.Text = "0.00";
            this.txtSchemeDisc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(145, 7);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(53, 13);
            this.label42.TabIndex = 5574;
            this.label42.Text = "Sch Disc.";
            // 
            // lblCancelBll
            // 
            this.lblCancelBll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblCancelBll.AutoSize = true;
            this.lblCancelBll.BackColor = System.Drawing.Color.Transparent;
            this.lblCancelBll.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCancelBll.ForeColor = System.Drawing.Color.Maroon;
            this.lblCancelBll.Location = new System.Drawing.Point(642, 307);
            this.lblCancelBll.Name = "lblCancelBll";
            this.lblCancelBll.Size = new System.Drawing.Size(0, 13);
            this.lblCancelBll.TabIndex = 531;
            // 
            // pnlTotalAmt
            // 
            this.pnlTotalAmt.BackColor = System.Drawing.Color.Transparent;
            this.pnlTotalAmt.BorderColor = System.Drawing.Color.Gray;
            this.pnlTotalAmt.BorderRadius = 3;
            this.pnlTotalAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTotalAmt.Controls.Add(this.txtTotalAnotherDisc);
            this.pnlTotalAmt.Controls.Add(this.label36);
            this.pnlTotalAmt.Controls.Add(this.txtRoundOff);
            this.pnlTotalAmt.Controls.Add(this.label35);
            this.pnlTotalAmt.Controls.Add(this.txtTotalChrgs);
            this.pnlTotalAmt.Controls.Add(this.label34);
            this.pnlTotalAmt.Controls.Add(this.txtGrandTotal);
            this.pnlTotalAmt.Controls.Add(this.txtTotalTax);
            this.pnlTotalAmt.Controls.Add(this.txtTotalDisc);
            this.pnlTotalAmt.Controls.Add(this.txtSubTotal);
            this.pnlTotalAmt.Controls.Add(this.label8);
            this.pnlTotalAmt.Controls.Add(this.label7);
            this.pnlTotalAmt.Controls.Add(this.label6);
            this.pnlTotalAmt.Controls.Add(this.label5);
            this.pnlTotalAmt.Location = new System.Drawing.Point(1392, 238);
            this.pnlTotalAmt.Name = "pnlTotalAmt";
            this.pnlTotalAmt.Size = new System.Drawing.Size(214, 188);
            this.pnlTotalAmt.TabIndex = 8;
            this.pnlTotalAmt.Visible = false;
            // 
            // txtTotalAnotherDisc
            // 
            this.txtTotalAnotherDisc.BackColor = System.Drawing.Color.Coral;
            this.txtTotalAnotherDisc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAnotherDisc.Location = new System.Drawing.Point(102, 99);
            this.txtTotalAnotherDisc.Name = "txtTotalAnotherDisc";
            this.txtTotalAnotherDisc.ReadOnly = true;
            this.txtTotalAnotherDisc.Size = new System.Drawing.Size(107, 20);
            this.txtTotalAnotherDisc.TabIndex = 519;
            this.txtTotalAnotherDisc.Text = "0.00";
            this.txtTotalAnotherDisc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(1, 103);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(49, 13);
            this.label36.TabIndex = 620;
            this.label36.Text = "Discount";
            // 
            // txtRoundOff
            // 
            this.txtRoundOff.BackColor = System.Drawing.Color.Coral;
            this.txtRoundOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRoundOff.Location = new System.Drawing.Point(102, 122);
            this.txtRoundOff.Name = "txtRoundOff";
            this.txtRoundOff.ReadOnly = true;
            this.txtRoundOff.Size = new System.Drawing.Size(107, 20);
            this.txtRoundOff.TabIndex = 517;
            this.txtRoundOff.Text = "0.00";
            this.txtRoundOff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(1, 126);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(56, 13);
            this.label35.TabIndex = 618;
            this.label35.Text = "Round Off";
            // 
            // txtTotalChrgs
            // 
            this.txtTotalChrgs.BackColor = System.Drawing.Color.Coral;
            this.txtTotalChrgs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalChrgs.Location = new System.Drawing.Point(102, 75);
            this.txtTotalChrgs.Name = "txtTotalChrgs";
            this.txtTotalChrgs.ReadOnly = true;
            this.txtTotalChrgs.Size = new System.Drawing.Size(107, 20);
            this.txtTotalChrgs.TabIndex = 515;
            this.txtTotalChrgs.Text = "0.00";
            this.txtTotalChrgs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(1, 79);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(73, 13);
            this.label34.TabIndex = 516;
            this.label34.Text = "Total Charges";
            // 
            // txtGrandTotal
            // 
            this.txtGrandTotal.BackColor = System.Drawing.Color.Coral;
            this.txtGrandTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGrandTotal.Location = new System.Drawing.Point(91, 146);
            this.txtGrandTotal.Name = "txtGrandTotal";
            this.txtGrandTotal.ReadOnly = true;
            this.txtGrandTotal.Size = new System.Drawing.Size(118, 20);
            this.txtGrandTotal.TabIndex = 750;
            this.txtGrandTotal.Text = "0.00";
            this.txtGrandTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGrandTotal.TextChanged += new System.EventHandler(this.txtGrandTotal_TextChanged);
            // 
            // txtTotalTax
            // 
            this.txtTotalTax.BackColor = System.Drawing.Color.Coral;
            this.txtTotalTax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalTax.Location = new System.Drawing.Point(102, 51);
            this.txtTotalTax.Name = "txtTotalTax";
            this.txtTotalTax.ReadOnly = true;
            this.txtTotalTax.Size = new System.Drawing.Size(107, 20);
            this.txtTotalTax.TabIndex = 11;
            this.txtTotalTax.Text = "0.00";
            this.txtTotalTax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotalDisc
            // 
            this.txtTotalDisc.BackColor = System.Drawing.Color.Coral;
            this.txtTotalDisc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalDisc.Location = new System.Drawing.Point(102, 26);
            this.txtTotalDisc.Name = "txtTotalDisc";
            this.txtTotalDisc.ReadOnly = true;
            this.txtTotalDisc.Size = new System.Drawing.Size(107, 20);
            this.txtTotalDisc.TabIndex = 10;
            this.txtTotalDisc.Text = "0.00";
            this.txtTotalDisc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSubTotal
            // 
            this.txtSubTotal.BackColor = System.Drawing.Color.Coral;
            this.txtSubTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSubTotal.Location = new System.Drawing.Point(102, 2);
            this.txtSubTotal.Name = "txtSubTotal";
            this.txtSubTotal.ReadOnly = true;
            this.txtSubTotal.Size = new System.Drawing.Size(107, 20);
            this.txtSubTotal.TabIndex = 92;
            this.txtSubTotal.Text = "0.00";
            this.txtSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(1, 150);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 514;
            this.label8.Text = "Grand Total";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 513;
            this.label7.Text = "Total Tax";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 512;
            this.label6.Text = "Item Disc.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 511;
            this.label5.Text = "Sub Total";
            // 
            // pnlPartySearch
            // 
            this.pnlPartySearch.BorderColor = System.Drawing.Color.Empty;
            this.pnlPartySearch.BorderRadius = 3;
            this.pnlPartySearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPartySearch.Controls.Add(this.dgPartySearch);
            this.pnlPartySearch.Controls.Add(this.lblSearchName);
            this.pnlPartySearch.Location = new System.Drawing.Point(155, 68);
            this.pnlPartySearch.Name = "pnlPartySearch";
            this.pnlPartySearch.Size = new System.Drawing.Size(389, 322);
            this.pnlPartySearch.TabIndex = 123455;
            this.pnlPartySearch.Visible = false;
            // 
            // dgPartySearch
            // 
            this.dgPartySearch.AllowUserToAddRows = false;
            this.dgPartySearch.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgPartySearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPartySearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgPartySearch.Location = new System.Drawing.Point(0, 23);
            this.dgPartySearch.Name = "dgPartySearch";
            this.dgPartySearch.ReadOnly = true;
            this.dgPartySearch.Size = new System.Drawing.Size(387, 297);
            this.dgPartySearch.TabIndex = 123455;
            this.dgPartySearch.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgPartySearch_CellFormatting);
            this.dgPartySearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgPartySearch_KeyDown);
            // 
            // lblSearchName
            // 
            this.lblSearchName.BackColor = System.Drawing.Color.Coral;
            this.lblSearchName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSearchName.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblSearchName.ForeColor = System.Drawing.Color.White;
            this.lblSearchName.Location = new System.Drawing.Point(0, 0);
            this.lblSearchName.Name = "lblSearchName";
            this.lblSearchName.Size = new System.Drawing.Size(387, 23);
            this.lblSearchName.TabIndex = 123456;
            this.lblSearchName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnMixMode
            // 
            this.btnMixMode.Location = new System.Drawing.Point(1268, -4);
            this.btnMixMode.Name = "btnMixMode";
            this.btnMixMode.Size = new System.Drawing.Size(69, 60);
            this.btnMixMode.TabIndex = 123456;
            this.btnMixMode.Text = "Mix\r\nMode (F3)";
            this.btnMixMode.UseVisualStyleBackColor = true;
            this.btnMixMode.Visible = false;
            this.btnMixMode.Click += new System.EventHandler(this.btnMixMode_Click);
            // 
            // btnInsScheme
            // 
            this.btnInsScheme.Location = new System.Drawing.Point(14, 136);
            this.btnInsScheme.Name = "btnInsScheme";
            this.btnInsScheme.Size = new System.Drawing.Size(69, 60);
            this.btnInsScheme.TabIndex = 123457;
            this.btnInsScheme.Text = "Scheme";
            this.btnInsScheme.UseVisualStyleBackColor = true;
            this.btnInsScheme.Click += new System.EventHandler(this.btnInsScheme_Click);
            // 
            // pnlCollectionDetails
            // 
            this.pnlCollectionDetails.BackColor = System.Drawing.Color.Transparent;
            this.pnlCollectionDetails.BorderColor = System.Drawing.Color.Gray;
            this.pnlCollectionDetails.BorderRadius = 3;
            this.pnlCollectionDetails.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlCollectionDetails.Controls.Add(this.dgCollectionDetails);
            this.pnlCollectionDetails.Controls.Add(this.lblTotalCollection);
            this.pnlCollectionDetails.Controls.Add(this.lblCollectionDetails);
            this.pnlCollectionDetails.Controls.Add(this.label57);
            this.pnlCollectionDetails.Controls.Add(this.lblOw);
            this.pnlCollectionDetails.Controls.Add(this.label50);
            this.pnlCollectionDetails.Location = new System.Drawing.Point(399, 148);
            this.pnlCollectionDetails.Name = "pnlCollectionDetails";
            this.pnlCollectionDetails.Size = new System.Drawing.Size(175, 188);
            this.pnlCollectionDetails.TabIndex = 123458;
            this.pnlCollectionDetails.Visible = false;
            this.pnlCollectionDetails.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlCollectionDetails_Paint);
            // 
            // dgCollectionDetails
            // 
            this.dgCollectionDetails.AllowUserToAddRows = false;
            this.dgCollectionDetails.AllowUserToDeleteRows = false;
            this.dgCollectionDetails.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgCollectionDetails.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgCollectionDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgCollectionDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgCollectionDetails.Location = new System.Drawing.Point(0, 23);
            this.dgCollectionDetails.Name = "dgCollectionDetails";
            this.dgCollectionDetails.ReadOnly = true;
            this.dgCollectionDetails.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgCollectionDetails.Size = new System.Drawing.Size(173, 163);
            this.dgCollectionDetails.TabIndex = 911;
            // 
            // lblTotalCollection
            // 
            this.lblTotalCollection.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalCollection.Location = new System.Drawing.Point(64, 148);
            this.lblTotalCollection.Name = "lblTotalCollection";
            this.lblTotalCollection.Size = new System.Drawing.Size(105, 13);
            this.lblTotalCollection.TabIndex = 906;
            this.lblTotalCollection.Text = "0.00";
            this.lblTotalCollection.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCollectionDetails
            // 
            this.lblCollectionDetails.BackColor = System.Drawing.Color.Coral;
            this.lblCollectionDetails.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblCollectionDetails.ForeColor = System.Drawing.Color.White;
            this.lblCollectionDetails.Location = new System.Drawing.Point(0, 0);
            this.lblCollectionDetails.Name = "lblCollectionDetails";
            this.lblCollectionDetails.Size = new System.Drawing.Size(173, 23);
            this.lblCollectionDetails.TabIndex = 905;
            this.lblCollectionDetails.Text = "Mix-Mode Details";
            this.lblCollectionDetails.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(9, 147);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(31, 13);
            this.label57.TabIndex = 603;
            this.label57.Text = "Total";
            // 
            // lblOw
            // 
            this.lblOw.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOw.Location = new System.Drawing.Point(65, 167);
            this.lblOw.Name = "lblOw";
            this.lblOw.Size = new System.Drawing.Size(105, 13);
            this.lblOw.TabIndex = 913;
            this.lblOw.Text = "0.00";
            this.lblOw.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(8, 167);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(37, 13);
            this.label50.TabIndex = 912;
            this.label50.Text = "O/W :";
            // 
            // pnlDiscountDetails
            // 
            this.pnlDiscountDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlDiscountDetails.BackColor = System.Drawing.Color.Transparent;
            this.pnlDiscountDetails.BorderColor = System.Drawing.Color.Gray;
            this.pnlDiscountDetails.BorderRadius = 3;
            this.pnlDiscountDetails.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDiscountDetails.Controls.Add(this.btnDiscOK);
            this.pnlDiscountDetails.Controls.Add(this.chkFooterLevelDisc);
            this.pnlDiscountDetails.Controls.Add(this.label51);
            this.pnlDiscountDetails.Controls.Add(this.chkItemLevelDisc);
            this.pnlDiscountDetails.Location = new System.Drawing.Point(167, 21);
            this.pnlDiscountDetails.Name = "pnlDiscountDetails";
            this.pnlDiscountDetails.Size = new System.Drawing.Size(141, 69);
            this.pnlDiscountDetails.TabIndex = 123459;
            // 
            // btnDiscOK
            // 
            this.btnDiscOK.Location = new System.Drawing.Point(92, 32);
            this.btnDiscOK.Name = "btnDiscOK";
            this.btnDiscOK.Size = new System.Drawing.Size(43, 23);
            this.btnDiscOK.TabIndex = 908;
            this.btnDiscOK.Text = "OK";
            this.btnDiscOK.UseVisualStyleBackColor = true;
            // 
            // chkFooterLevelDisc
            // 
            this.chkFooterLevelDisc.AutoSize = true;
            this.chkFooterLevelDisc.Location = new System.Drawing.Point(4, 47);
            this.chkFooterLevelDisc.Name = "chkFooterLevelDisc";
            this.chkFooterLevelDisc.Size = new System.Drawing.Size(85, 17);
            this.chkFooterLevelDisc.TabIndex = 907;
            this.chkFooterLevelDisc.Text = "Footer Level";
            this.chkFooterLevelDisc.UseVisualStyleBackColor = true;
            // 
            // label51
            // 
            this.label51.BackColor = System.Drawing.Color.Coral;
            this.label51.Dock = System.Windows.Forms.DockStyle.Top;
            this.label51.ForeColor = System.Drawing.Color.White;
            this.label51.Location = new System.Drawing.Point(0, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(139, 23);
            this.label51.TabIndex = 906;
            this.label51.Text = "Discount Details";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkItemLevelDisc
            // 
            this.chkItemLevelDisc.AutoSize = true;
            this.chkItemLevelDisc.Location = new System.Drawing.Point(4, 29);
            this.chkItemLevelDisc.Name = "chkItemLevelDisc";
            this.chkItemLevelDisc.Size = new System.Drawing.Size(75, 17);
            this.chkItemLevelDisc.TabIndex = 0;
            this.chkItemLevelDisc.Text = "Item Level";
            this.chkItemLevelDisc.UseVisualStyleBackColor = true;
            // 
            // btnInsSchemeInfo
            // 
            this.btnInsSchemeInfo.Location = new System.Drawing.Point(5, 38);
            this.btnInsSchemeInfo.Name = "btnInsSchemeInfo";
            this.btnInsSchemeInfo.Size = new System.Drawing.Size(69, 60);
            this.btnInsSchemeInfo.TabIndex = 123460;
            this.btnInsSchemeInfo.Text = "Apply\r\nScheme";
            this.btnInsSchemeInfo.UseVisualStyleBackColor = true;
            this.btnInsSchemeInfo.Click += new System.EventHandler(this.btnInsSchemeInfo_Click);
            // 
            // pnlInvSearch
            // 
            this.pnlInvSearch.BorderColor = System.Drawing.Color.Empty;
            this.pnlInvSearch.BorderRadius = 3;
            this.pnlInvSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlInvSearch.Controls.Add(this.dgInvSearch);
            this.pnlInvSearch.Controls.Add(this.label52);
            this.pnlInvSearch.Location = new System.Drawing.Point(1425, 124);
            this.pnlInvSearch.Name = "pnlInvSearch";
            this.pnlInvSearch.Size = new System.Drawing.Size(389, 342);
            this.pnlInvSearch.TabIndex = 123461;
            this.pnlInvSearch.Visible = false;
            // 
            // dgInvSearch
            // 
            this.dgInvSearch.AllowUserToAddRows = false;
            this.dgInvSearch.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgInvSearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgInvSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgInvSearch.Location = new System.Drawing.Point(0, 23);
            this.dgInvSearch.Name = "dgInvSearch";
            this.dgInvSearch.ReadOnly = true;
            this.dgInvSearch.Size = new System.Drawing.Size(387, 317);
            this.dgInvSearch.TabIndex = 123455;
            this.dgInvSearch.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgInvSearch_CellFormatting);
            this.dgInvSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgInvSearch_KeyDown);
            // 
            // label52
            // 
            this.label52.BackColor = System.Drawing.Color.Coral;
            this.label52.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label52.Dock = System.Windows.Forms.DockStyle.Top;
            this.label52.ForeColor = System.Drawing.Color.White;
            this.label52.Location = new System.Drawing.Point(0, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(387, 23);
            this.label52.TabIndex = 123456;
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnSchemePromo
            // 
            this.btnSchemePromo.Location = new System.Drawing.Point(0, 8);
            this.btnSchemePromo.Name = "btnSchemePromo";
            this.btnSchemePromo.Size = new System.Drawing.Size(69, 60);
            this.btnSchemePromo.TabIndex = 123462;
            this.btnSchemePromo.Text = "Scheme Promo";
            this.btnSchemePromo.UseVisualStyleBackColor = true;
            this.btnSchemePromo.Click += new System.EventHandler(this.btnSchemePromo_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnSchemePromo);
            this.panel2.Controls.Add(this.btnInsScheme);
            this.panel2.Controls.Add(this.txtSchemeDisc);
            this.panel2.Controls.Add(this.label42);
            this.panel2.Controls.Add(this.pnlDiscountDetails);
            this.panel2.Controls.Add(this.btnInsSchemeInfo);
            this.panel2.Location = new System.Drawing.Point(1324, 176);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(308, 100);
            this.panel2.TabIndex = 123463;
            this.panel2.Visible = false;
            // 
            // BTSacn
            // 
            this.BTSacn.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BTSacn_DoWork);
            this.BTSacn.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.BTSacn_ProgressChanged);
            // 
            // pctbCalculator
            // 
            this.pctbCalculator.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pctbCalculator.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pctbCalculator.Image = global::Kirana.Properties.Resources.Calc;
            this.pctbCalculator.Location = new System.Drawing.Point(689, 5);
            this.pctbCalculator.Name = "pctbCalculator";
            this.pctbCalculator.Size = new System.Drawing.Size(45, 58);
            this.pctbCalculator.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctbCalculator.TabIndex = 123464;
            this.pctbCalculator.TabStop = false;
            // 
            // bw_ItemSearch
            // 
            this.bw_ItemSearch.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bw_ItemSearch_DoWork);
            // 
            // pnlScheme
            // 
            this.pnlScheme.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlScheme.Controls.Add(this.dgInsTSKU);
            this.pnlScheme.Location = new System.Drawing.Point(659, 100);
            this.pnlScheme.Name = "pnlScheme";
            this.pnlScheme.Size = new System.Drawing.Size(361, 84);
            this.pnlScheme.TabIndex = 123465;
            this.pnlScheme.Visible = false;
            // 
            // dgInsTSKU
            // 
            this.dgInsTSKU.AllowUserToAddRows = false;
            this.dgInsTSKU.AllowUserToDeleteRows = false;
            this.dgInsTSKU.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgInsTSKU.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rType,
            this.SchemeNo,
            this.SchUserNo,
            this.SchName,
            this.dataGridViewTextBoxColumn13,
            this.SchTypeNo,
            this.SchDate,
            this.SchperFrom,
            this.SchPerTo,
            this.dataGridViewTextBoxColumn14,
            this.PDiscAmt,
            this.SchDtlspKSrNo,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.UomName,
            this.dataGridViewTextBoxColumn17,
            this.BillQty,
            this.BUomName,
            this.BtnDtails,
            this.select,
            this.chk,
            this.dataGridViewTextBoxColumn18,
            this.TSKUCLoyaltyFactor});
            this.dgInsTSKU.Location = new System.Drawing.Point(0, 6);
            this.dgInsTSKU.Name = "dgInsTSKU";
            this.dgInsTSKU.Size = new System.Drawing.Size(356, 76);
            this.dgInsTSKU.TabIndex = 1;
            this.dgInsTSKU.Click += new System.EventHandler(this.dgInsTSKU_Click);
            // 
            // rType
            // 
            this.rType.HeaderText = "rType";
            this.rType.Name = "rType";
            this.rType.Visible = false;
            // 
            // SchemeNo
            // 
            this.SchemeNo.HeaderText = "SchemeNo";
            this.SchemeNo.Name = "SchemeNo";
            this.SchemeNo.Visible = false;
            // 
            // SchUserNo
            // 
            this.SchUserNo.HeaderText = "Sch. No.";
            this.SchUserNo.Name = "SchUserNo";
            // 
            // SchName
            // 
            this.SchName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SchName.HeaderText = "SchemeName";
            this.SchName.Name = "SchName";
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn13.HeaderText = "ItemName";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            // 
            // SchTypeNo
            // 
            this.SchTypeNo.HeaderText = "SchTypeNo";
            this.SchTypeNo.Name = "SchTypeNo";
            this.SchTypeNo.Visible = false;
            // 
            // SchDate
            // 
            this.SchDate.HeaderText = "SchDate";
            this.SchDate.Name = "SchDate";
            this.SchDate.Visible = false;
            // 
            // SchperFrom
            // 
            this.SchperFrom.HeaderText = "SchperFrom";
            this.SchperFrom.Name = "SchperFrom";
            this.SchperFrom.Visible = false;
            // 
            // SchPerTo
            // 
            this.SchPerTo.HeaderText = "SchPerTo";
            this.SchPerTo.Name = "SchPerTo";
            this.SchPerTo.Visible = false;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.HeaderText = "DiscAmt";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.Visible = false;
            // 
            // PDiscAmt
            // 
            this.PDiscAmt.HeaderText = "PDiscAmt";
            this.PDiscAmt.Name = "PDiscAmt";
            this.PDiscAmt.Visible = false;
            // 
            // SchDtlspKSrNo
            // 
            this.SchDtlspKSrNo.HeaderText = "SchDtlspKSrNo";
            this.SchDtlspKSrNo.Name = "SchDtlspKSrNo";
            this.SchDtlspKSrNo.Visible = false;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.HeaderText = "ItemNo";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.Visible = false;
            // 
            // dataGridViewTextBoxColumn16
            // 
            dataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn16.DefaultCellStyle = dataGridViewCellStyle50;
            this.dataGridViewTextBoxColumn16.HeaderText = "Qty";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.Width = 70;
            // 
            // UomName
            // 
            this.UomName.HeaderText = "Uom";
            this.UomName.Name = "UomName";
            this.UomName.Width = 70;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.HeaderText = "UOmNo";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.Visible = false;
            // 
            // BillQty
            // 
            dataGridViewCellStyle51.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.BillQty.DefaultCellStyle = dataGridViewCellStyle51;
            this.BillQty.HeaderText = "BillQty";
            this.BillQty.Name = "BillQty";
            this.BillQty.ReadOnly = true;
            this.BillQty.Visible = false;
            this.BillQty.Width = 70;
            // 
            // BUomName
            // 
            this.BUomName.HeaderText = "Uom";
            this.BUomName.Name = "BUomName";
            this.BUomName.ReadOnly = true;
            this.BUomName.Visible = false;
            this.BUomName.Width = 70;
            // 
            // BtnDtails
            // 
            dataGridViewCellStyle52.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle52.BackColor = System.Drawing.Color.AliceBlue;
            dataGridViewCellStyle52.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.BtnDtails.DefaultCellStyle = dataGridViewCellStyle52;
            this.BtnDtails.HeaderText = ".";
            this.BtnDtails.Name = "BtnDtails";
            this.BtnDtails.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.BtnDtails.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.BtnDtails.Width = 50;
            // 
            // select
            // 
            this.select.HeaderText = " ";
            this.select.Name = "select";
            this.select.ReadOnly = true;
            this.select.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.select.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.select.Visible = false;
            this.select.Width = 40;
            // 
            // chk
            // 
            this.chk.HeaderText = "chk";
            this.chk.Name = "chk";
            this.chk.Visible = false;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "MRP";
            this.dataGridViewTextBoxColumn18.HeaderText = "MRP";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.Visible = false;
            // 
            // TSKUCLoyaltyFactor
            // 
            this.TSKUCLoyaltyFactor.HeaderText = "LoyaltyFactor";
            this.TSKUCLoyaltyFactor.Name = "TSKUCLoyaltyFactor";
            this.TSKUCLoyaltyFactor.Visible = false;
            // 
            // btnRemoveScheme
            // 
            this.btnRemoveScheme.Location = new System.Drawing.Point(1214, 136);
            this.btnRemoveScheme.Name = "btnRemoveScheme";
            this.btnRemoveScheme.Size = new System.Drawing.Size(69, 60);
            this.btnRemoveScheme.TabIndex = 123466;
            this.btnRemoveScheme.Text = "Remove\r\nScheme";
            this.btnRemoveScheme.UseVisualStyleBackColor = true;
            this.btnRemoveScheme.Click += new System.EventHandler(this.btnRemoveScheme_Click);
            // 
            // btnShowSchemeInfo
            // 
            this.btnShowSchemeInfo.Location = new System.Drawing.Point(1214, 2);
            this.btnShowSchemeInfo.Name = "btnShowSchemeInfo";
            this.btnShowSchemeInfo.Size = new System.Drawing.Size(69, 60);
            this.btnShowSchemeInfo.TabIndex = 123467;
            this.btnShowSchemeInfo.Text = "Show \r\nScheme";
            this.btnShowSchemeInfo.UseVisualStyleBackColor = true;
            this.btnShowSchemeInfo.Click += new System.EventHandler(this.btnShowSchemeInfo_Click);
            // 
            // btnApplyScheme
            // 
            this.btnApplyScheme.Location = new System.Drawing.Point(1214, 69);
            this.btnApplyScheme.Name = "btnApplyScheme";
            this.btnApplyScheme.Size = new System.Drawing.Size(69, 60);
            this.btnApplyScheme.TabIndex = 123468;
            this.btnApplyScheme.Text = "Apply\r\nScheme";
            this.btnApplyScheme.UseVisualStyleBackColor = true;
            this.btnApplyScheme.Click += new System.EventHandler(this.btnApplyScheme_Click);
            // 
            // SalesAE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1362, 485);
            this.Controls.Add(this.btnApplyScheme);
            this.Controls.Add(this.btnShowSchemeInfo);
            this.Controls.Add(this.btnRemoveScheme);
            this.Controls.Add(this.pnlScheme);
            this.Controls.Add(this.pctbCalculator);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pnlInvSearch);
            this.Controls.Add(this.pnlCollectionDetails);
            this.Controls.Add(this.pnlPartial);
            this.Controls.Add(this.pnlSearch);
            this.Controls.Add(this.pnlPartySearch);
            this.Controls.Add(this.pnlItemName);
            this.Controls.Add(this.pnlGroup1);
            this.Controls.Add(this.lblCancelBll);
            this.Controls.Add(this.pnlFooterInfo);
            this.Controls.Add(this.pnlTodaysSales);
            this.Controls.Add(this.pnlParking);
            this.Controls.Add(this.btnBillCancel);
            this.Controls.Add(this.pnlLastBill);
            this.Controls.Add(this.btnTodaysSales);
            this.Controls.Add(this.btnShowDetails);
            this.Controls.Add(this.btnShortcut);
            this.Controls.Add(this.btnNewCustomer);
            this.Controls.Add(this.btnOrderType);
            this.Controls.Add(this.btnSalesRegister);
            this.Controls.Add(this.lblGrandTotal);
            this.Controls.Add(this.btnAdvanceSearch);
            this.Controls.Add(this.pnlMainParking);
            this.Controls.Add(this.pnlPartyName);
            this.Controls.Add(this.pnlRateType);
            this.Controls.Add(this.pnlSalePurHistory);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.pnlRate);
            this.Controls.Add(this.pnlUOM);
            this.Controls.Add(this.pnlGroup2);
            this.Controls.Add(this.lblRateType);
            this.Controls.Add(this.cmbRateType);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cmbTaxType);
            this.Controls.Add(this.cmbPartyName);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnPrev);
            this.Controls.Add(this.btnLast);
            this.Controls.Add(this.btnFirst);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.pnlTotalAmt);
            this.Controls.Add(this.dgBill);
            this.Controls.Add(this.dtpBillTime);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtpBillDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtInvNo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnCCSave);
            this.Controls.Add(this.btnCreditSave);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.btnCashSave);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnMixMode);
            this.Name = "SalesAE";
            this.Text = "Bill";
            this.Activated += new System.EventHandler(this.SalesAE_Activated);
            this.Deactivate += new System.EventHandler(this.SalesAE_Deactivate);
            this.Load += new System.EventHandler(this.SalesAE_Load);
            this.Shown += new System.EventHandler(this.SalesAE_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dgBill)).EndInit();
            this.pnlPartial.ResumeLayout(false);
            this.pnlPartial.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPayCreditCardDetails)).EndInit();
            this.pnlBranch.ResumeLayout(false);
            this.pnlBank.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgPayChqDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgPayType)).EndInit();
            this.pnlSearch.ResumeLayout(false);
            this.pnlSearch.PerformLayout();
            this.pnlItemName.ResumeLayout(false);
            this.pnlItemName.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgItemList)).EndInit();
            this.pnlGroup1.ResumeLayout(false);
            this.pnlGroup1.PerformLayout();
            this.pnlGroup2.ResumeLayout(false);
            this.pnlUOM.ResumeLayout(false);
            this.pnlRate.ResumeLayout(false);
            this.pnlSalePurHistory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgPurHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgSaleHistory)).EndInit();
            this.pnlRateType.ResumeLayout(false);
            this.pnlRateType.PerformLayout();
            this.pnlPartyName.ResumeLayout(false);
            this.pnlPartyName.PerformLayout();
            this.pnlParking.ResumeLayout(false);
            this.pnlParking.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgParkingBills)).EndInit();
            this.pnlMainParking.ResumeLayout(false);
            this.pnlMainParking.PerformLayout();
            this.pnlTodaysSales.ResumeLayout(false);
            this.pnlTodaysSales.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgSalesDetails)).EndInit();
            this.pnlLastBill.ResumeLayout(false);
            this.pnlLastBill.PerformLayout();
            this.pnlFooterInfo.ResumeLayout(false);
            this.pnlFooterInfo.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.plnPrinting.ResumeLayout(false);
            this.plnPrinting.PerformLayout();
            this.pnlTotalAmt.ResumeLayout(false);
            this.pnlTotalAmt.PerformLayout();
            this.pnlPartySearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgPartySearch)).EndInit();
            this.pnlCollectionDetails.ResumeLayout(false);
            this.pnlCollectionDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCollectionDetails)).EndInit();
            this.pnlDiscountDetails.ResumeLayout(false);
            this.pnlDiscountDetails.PerformLayout();
            this.pnlInvSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgInvSearch)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctbCalculator)).EndInit();
            this.pnlScheme.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgInsTSKU)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtInvNo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpBillDate;
        private System.Windows.Forms.DateTimePicker dtpBillTime;
        private System.Windows.Forms.Label label3;
        private JitControls.OMBPanel pnlTotalAmt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtGrandTotal;
        private System.Windows.Forms.TextBox txtTotalTax;
        private System.Windows.Forms.TextBox txtTotalDisc;
        private System.Windows.Forms.TextBox txtSubTotal;
        private JitControls.OMBPanel panel1;
        private System.Windows.Forms.Label lblBillItem;
        private System.Windows.Forms.Label lblBilExchangeItem;
        private System.Windows.Forms.TextBox txtDiscount1;
        private System.Windows.Forms.Label lblChrg1;
        private System.Windows.Forms.Label lblDisc1;
        private System.Windows.Forms.ComboBox cmbPaymentType;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnBillCancel;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.Button btnLast;
        private System.Windows.Forms.Button btnFirst;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnSave;
        private JitControls.OMBPanel pnlFooterInfo;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblLastAmt;
        private System.Windows.Forms.DataGridView dgBill;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.Panel pnlPartial;
        private System.Windows.Forms.TextBox txtTotalAmt;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlSearch;
        private System.Windows.Forms.Button btnCancelSearch;
        private System.Windows.Forms.Label lblLable;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.TextBox txtRemark;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Panel pnlItemName;
        private System.Windows.Forms.DataGridView dgItemList;
        private System.Windows.Forms.ListBox lstUOM;
        private System.Windows.Forms.ListBox lstRate;
        private System.Windows.Forms.ListBox lstGroup1;
        private System.Windows.Forms.ListBox lstGroup2;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.ComboBox cmbPartyName;
        private System.Windows.Forms.TextBox txtDiscRupees1;
        private System.Windows.Forms.TextBox txtRoundOff;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtTotalChrgs;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtChrgRupees1;
        private System.Windows.Forms.TextBox txtTotalAnotherDisc;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label lblLastBillQty;
        private System.Windows.Forms.DataGridView dgPayType;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Label lblPayTypeBal;
        private System.Windows.Forms.ComboBox cmbTaxType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblRateType;
        private System.Windows.Forms.ComboBox cmbRateType;
        private System.Windows.Forms.Panel pnlGroup1;
        private System.Windows.Forms.Panel pnlGroup2;
        private System.Windows.Forms.Panel pnlUOM;
        private System.Windows.Forms.Panel pnlRate;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel pnlSalePurHistory;
        private System.Windows.Forms.DataGridView dgSaleHistory;
        private System.Windows.Forms.DataGridView dgPurHistory;
        private System.Windows.Forms.DataGridView dgPayChqDetails;
        private System.Windows.Forms.Panel pnlBank;
        private System.Windows.Forms.ListBox lstBank;
        private System.Windows.Forms.Panel pnlBranch;
        private System.Windows.Forms.ListBox lstBranch;
        private System.Windows.Forms.MonthCalendar dtpChqDate;
        private System.Windows.Forms.DataGridView dgPayCreditCardDetails;
        private System.Windows.Forms.DataGridViewTextBoxColumn CreditCardNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Panel pnlRateType;
        private System.Windows.Forms.Button btnRateTypeOK;
        private System.Windows.Forms.TextBox txtRateTypePassword;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Button btnRateTypeCancel;
        private System.Windows.Forms.Panel pnlPartyName;
        private System.Windows.Forms.Button btnPartyCancel;
        private System.Windows.Forms.Button btnPartyOK;
        private System.Windows.Forms.TextBox txtPartyName;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label lblGrandTotal;
        private System.Windows.Forms.Label lblCancelBll;
        private System.Windows.Forms.Panel pnlParking;
        private System.Windows.Forms.DataGridView dgParkingBills;
        private System.Windows.Forms.Panel pnlMainParking;
        private System.Windows.Forms.Button btnParkingCancel;
        private System.Windows.Forms.Button btnParkingOk;
        private System.Windows.Forms.TextBox txtPersonName;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn BillDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn BillTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParkingBillNo;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnAdvanceSearch;
        private System.Windows.Forms.Label lblLastPayment;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnSalesRegister;
        private System.Windows.Forms.Button btnOrderType;
        private System.Windows.Forms.Button btnNewCustomer;
        private System.Windows.Forms.Button btnShortcut;
        private System.Windows.Forms.Button btnShowDetails;
        private System.Windows.Forms.Button btnTodaysSales;
        private JitControls.OMBPanel pnlLastBill;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblCreditLimit;
        private System.Windows.Forms.Label lblLastBillAmt;
        private System.Windows.Forms.Button btnCashSave;
        private System.Windows.Forms.Button btnCreditSave;
        private System.Windows.Forms.Button btnCCSave;
        private System.Windows.Forms.Label label12;
        private JitControls.OMBPanel pnlTodaysSales;
        private System.Windows.Forms.Label lblCredit;
        private System.Windows.Forms.Label lblCheque;
        private System.Windows.Forms.Label lbl;
        private System.Windows.Forms.Label lblCash;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lblBillCnt;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lblTodaysSales;
        private System.Windows.Forms.Label lblTotAmt;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lblCc;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Panel plnPrinting;
        private System.Windows.Forms.RadioButton rbMarathi;
        private System.Windows.Forms.RadioButton rbEnglish;
        private System.Windows.Forms.ListBox lstGroup1Lang;
        private System.Windows.Forms.RadioButton rbPartyName;
        private System.Windows.Forms.RadioButton rbInvNo;
        private System.Windows.Forms.ComboBox cmbPartyNameSearch;
        private JitControls.OMBPanel pnlPartySearch;
        private System.Windows.Forms.DataGridView dgPartySearch;
        private System.Windows.Forms.Button btnPartyName;
        private System.Windows.Forms.Label lblSearchName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ChequeNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ChequeDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn BankName;
        private System.Windows.Forms.DataGridViewTextBoxColumn BranchName;
        private System.Windows.Forms.DataGridViewTextBoxColumn AmountChq;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkSrNoChq;
        private System.Windows.Forms.DataGridViewTextBoxColumn BankNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn BranchNo;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnMixMode;
        private System.Windows.Forms.Label lblNMsg;
        private System.Windows.Forms.Button btnInsScheme;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtSchemeDisc;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label lblFV;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.DataGridView dgSalesDetails;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn PKVoucherPayTypeNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ControlUnder;
        private JitControls.OMBPanel pnlCollectionDetails;
        private System.Windows.Forms.DataGridView dgCollectionDetails;
        private System.Windows.Forms.Label lblTotalCollection;
        private System.Windows.Forms.Label lblCollectionDetails;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox txtOtherDisc;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label lblOw;
        private System.Windows.Forms.Label label50;
        private JitControls.OMBPanel pnlDiscountDetails;
        private System.Windows.Forms.Button btnDiscOK;
        private System.Windows.Forms.CheckBox chkFooterLevelDisc;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.CheckBox chkItemLevelDisc;
        private System.Windows.Forms.Button btnInsSchemeInfo;
        private JitControls.OMBPanel pnlInvSearch;
        private System.Windows.Forms.DataGridView dgInvSearch;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Button btnSchemePromo;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtBrandFilter;
        private System.Windows.Forms.TextBox txtHRs;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox txtKg;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label lblBrandName;
        private System.ComponentModel.BackgroundWorker BTSacn;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridViewTextBoxColumn iItemNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn iItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemDescrLang;
        private System.Windows.Forms.DataGridViewTextBoxColumn iRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn iUOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn iMRP;
        private System.Windows.Forms.DataGridViewTextBoxColumn iStock;
        private System.Windows.Forms.DataGridViewTextBoxColumn iUOMStk;
        private System.Windows.Forms.DataGridViewTextBoxColumn iSaleTax;
        private System.Windows.Forms.DataGridViewTextBoxColumn iPurTax;
        private System.Windows.Forms.DataGridViewTextBoxColumn iCompany;
        private System.Windows.Forms.DataGridViewTextBoxColumn iBarcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn iRateSettingNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMDefault;
        private System.Windows.Forms.DataGridViewTextBoxColumn PurRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn MGodownNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn MDiscType;
        private System.Windows.Forms.DataGridViewTextBoxColumn IHamaliInKg;
        private System.Windows.Forms.PictureBox pctbCalculator;
        private System.ComponentModel.BackgroundWorker bw_ItemSearch;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn MRP;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rate;
        private System.Windows.Forms.DataGridViewTextBoxColumn NetRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscPerce;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscAmt;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscRupees;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscPercentage2;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscAmount2;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscRupees2;
        private System.Windows.Forms.DataGridViewTextBoxColumn NetAmt;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Barcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkStockTrnNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkSrNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkVoucherNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaxLedgerNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn SalesLedgerNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkRateSettingNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkItemTaxInfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn StockFactor;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActualQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn MKTQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaxPerce;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaxAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn SalesVchNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaxVchNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn StockCompanyNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn SchemeDetailsNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn SchemeFromNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn SchemeToNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn RewardFromNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn RewardToNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemLevelDiscNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn FKItemLevelDiscNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkGodownNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscountType;
        private System.Windows.Forms.DataGridViewTextBoxColumn HamaliInKg;
        private System.Windows.Forms.DataGridViewTextBoxColumn HSNCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn IGSTPercent;
        private System.Windows.Forms.DataGridViewTextBoxColumn IGSTAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn CGSTPercent;
        private System.Windows.Forms.DataGridViewTextBoxColumn CGSTAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn SGSTPercent;
        private System.Windows.Forms.DataGridViewTextBoxColumn SGSTAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn UTGSTPercent;
        private System.Windows.Forms.DataGridViewTextBoxColumn UTGSTAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn CessPercent;
        private System.Windows.Forms.DataGridViewTextBoxColumn CessAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsQtyRead;
        private System.Windows.Forms.Panel pnlScheme;
        private System.Windows.Forms.Button btnRemoveScheme;
        public System.Windows.Forms.DataGridView dgInsTSKU;
        private System.Windows.Forms.DataGridViewTextBoxColumn rType;
        private System.Windows.Forms.DataGridViewTextBoxColumn SchemeNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn SchUserNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn SchName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn SchTypeNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn SchDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn SchperFrom;
        private System.Windows.Forms.DataGridViewTextBoxColumn SchPerTo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn PDiscAmt;
        private System.Windows.Forms.DataGridViewTextBoxColumn SchDtlspKSrNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn UomName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn BillQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn BUomName;
        private System.Windows.Forms.DataGridViewButtonColumn BtnDtails;
        private System.Windows.Forms.DataGridViewCheckBoxColumn select;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chk;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn TSKUCLoyaltyFactor;
        private System.Windows.Forms.Button btnShowSchemeInfo;
        private System.Windows.Forms.Button btnApplyScheme;
        private System.Windows.Forms.TextBox txtAddFreight;
        private System.Windows.Forms.Label label41;
    }
}