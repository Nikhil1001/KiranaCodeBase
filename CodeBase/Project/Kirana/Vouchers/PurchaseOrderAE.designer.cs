﻿namespace Kirana.Vouchers
{
    partial class PurchaseOrderAE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.txtInvNo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpBillDate = new System.Windows.Forms.DateTimePicker();
            this.dtpBillTime = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.pnlTotalAmt = new System.Windows.Forms.Panel();
            this.txtOtherTax = new System.Windows.Forms.TextBox();
            this.lblOtherTax = new System.Windows.Forms.Label();
            this.txtVisibility = new System.Windows.Forms.TextBox();
            this.txtReturnAmt = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtChrgRupees1 = new System.Windows.Forms.TextBox();
            this.txtDistDisc = new System.Windows.Forms.TextBox();
            this.txtDiscRupees1 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.lblGrandTotal = new System.Windows.Forms.Label();
            this.txtRoundOff = new System.Windows.Forms.TextBox();
            this.lblChrg1 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.lblDisc1 = new System.Windows.Forms.Label();
            this.txtGrandTotal = new System.Windows.Forms.TextBox();
            this.txtTotalTax = new System.Windows.Forms.TextBox();
            this.txtTotalDisc = new System.Windows.Forms.TextBox();
            this.txtSubTotal = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTotalAnotherDisc = new System.Windows.Forms.TextBox();
            this.ombPanel1 = new JitControls.OMBPanel();
            this.lblLastPayment = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblLastBillAmt = new System.Windows.Forms.Label();
            this.lblLastBillQty = new System.Windows.Forms.Label();
            this.btnAllBarCodePrint = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label36 = new System.Windows.Forms.Label();
            this.txtTotalChrgs = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtTotalItemDisc = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtRemark = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.lblExchange = new System.Windows.Forms.Label();
            this.lblBilExchangeItem = new System.Windows.Forms.Label();
            this.lblBillItem = new System.Windows.Forms.Label();
            this.lblCompName = new System.Windows.Forms.Label();
            this.cmbPaymentType = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtRefNo = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrev = new System.Windows.Forms.Button();
            this.btnLast = new System.Windows.Forms.Button();
            this.btnFirst = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.pnlFooterInfo = new System.Windows.Forms.Panel();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label37 = new System.Windows.Forms.Label();
            this.txtlastBillQty = new System.Windows.Forms.TextBox();
            this.txtLastPayment = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txtLastBillAmt = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.dgBill = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MRP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NetRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FreeQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FreeUom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscPerce = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscRupees = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscPercentage2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscAmount2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NetAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxPerce = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscRupees2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Barcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkStockTrnNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkSrNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkVoucherNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxLedgerNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SalesLedgerNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkRateSettingNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkItemTaxInfo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StockFactor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActualQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MKTQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SalesVchNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxVchNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StockCompanyNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BarCodePrinting = new System.Windows.Forms.DataGridViewButtonColumn();
            this.FreeUomNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TempMRP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LandedRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BalanceQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSearch = new System.Windows.Forms.Button();
            this.pnlPartial = new System.Windows.Forms.Panel();
            this.label38 = new System.Windows.Forms.Label();
            this.dgPayCreditCardDetails = new System.Windows.Forms.DataGridView();
            this.CreditCardNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlBranch = new System.Windows.Forms.Panel();
            this.lstBranch = new System.Windows.Forms.ListBox();
            this.pnlBank = new System.Windows.Forms.Panel();
            this.lstBank = new System.Windows.Forms.ListBox();
            this.dtpChqDate = new System.Windows.Forms.MonthCalendar();
            this.dgPayChqDetails = new System.Windows.Forms.DataGridView();
            this.ChequeNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ChequeDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BankName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BranchName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AmountChq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkSrNoChq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BankNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BranchNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblPayTypeBal = new System.Windows.Forms.Label();
            this.dgPayType = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PKVoucherPayTypeNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnOk = new System.Windows.Forms.Button();
            this.txtTotalAmt = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.dgParkingBills = new System.Windows.Forms.DataGridView();
            this.BillNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BillAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlParking = new System.Windows.Forms.Panel();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.pnlItemName = new System.Windows.Forms.Panel();
            this.btnOk1 = new System.Windows.Forms.Button();
            this.dgItemList = new System.Windows.Forms.DataGridView();
            this.iItemNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemNameLang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Qty0 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iUOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iMRP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iStock = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MinLevel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaxLevel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iUOMStk = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iSaleTax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iPurTax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iCompany = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iBarcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iRateSettingNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UOMDefault = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PurRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SuggMin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SugMax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lstUOM = new System.Windows.Forms.ListBox();
            this.lstRate = new System.Windows.Forms.ListBox();
            this.lstGroup1 = new System.Windows.Forms.ListBox();
            this.lstGroup2 = new System.Windows.Forms.ListBox();
            this.label33 = new System.Windows.Forms.Label();
            this.cmbPartyName = new System.Windows.Forms.ComboBox();
            this.cmbTaxType = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblRateType = new System.Windows.Forms.Label();
            this.cmbRateType = new System.Windows.Forms.ComboBox();
            this.pnlGroup1 = new System.Windows.Forms.Panel();
            this.lstGroup1Lang = new System.Windows.Forms.ListBox();
            this.pnlGroup2 = new System.Windows.Forms.Panel();
            this.pnlUOM = new System.Windows.Forms.Panel();
            this.pnlRate = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pnlSalePurHistory = new System.Windows.Forms.Panel();
            this.dgPurHistory = new System.Windows.Forms.DataGridView();
            this.dgSaleHistory = new System.Windows.Forms.DataGridView();
            this.pnlStockGodown = new System.Windows.Forms.Panel();
            this.txtStockGodwnQty = new System.Windows.Forms.TextBox();
            this.dgStockGodown = new System.Windows.Forms.DataGridView();
            this.GodownNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GodownName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Qty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GActualQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PkStockGodownNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnStkGodownOk = new System.Windows.Forms.Button();
            this.label41 = new System.Windows.Forms.Label();
            this.pnlRateType = new System.Windows.Forms.Panel();
            this.btnRateTypeCancel = new System.Windows.Forms.Button();
            this.btnRateTypeOK = new System.Windows.Forms.Button();
            this.txtRateTypePassword = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.dgCompany = new System.Windows.Forms.DataGridView();
            this.pnlCompany = new System.Windows.Forms.Panel();
            this.txtOtherDisc = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.btnAdvanceSearch = new System.Windows.Forms.Button();
            this.btnShortcut = new System.Windows.Forms.Button();
            this.btnNewCustomer = new System.Windows.Forms.Button();
            this.pnlPartySearch = new JitControls.OMGPanel();
            this.dgPartySearch = new System.Windows.Forms.DataGridView();
            this.pnlSearch = new JitControls.OMBPanel();
            this.rbPartyName = new System.Windows.Forms.RadioButton();
            this.rbDocNo = new System.Windows.Forms.RadioButton();
            this.rbInvNo = new System.Windows.Forms.RadioButton();
            this.cmbPartyNameSearch = new System.Windows.Forms.ComboBox();
            this.txtInvNoSearch = new System.Windows.Forms.TextBox();
            this.btnCancelSearch = new System.Windows.Forms.Button();
            this.lblLable = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.pnlAllPrintBarCode = new JitControls.OMPanel();
            this.dgPrintBarCode = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BarCodeQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chkPrint = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.FKRateSettingNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rdAllSmallMode = new System.Windows.Forms.RadioButton();
            this.rdAllBigMode = new System.Windows.Forms.RadioButton();
            this.txtAllStartNo = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.btnAllBarcodeCancel = new System.Windows.Forms.Button();
            this.btnAllBarcodeOK = new System.Windows.Forms.Button();
            this.pnlBarCodePrint = new JitControls.OMPanel();
            this.rbSmallMode = new System.Windows.Forms.RadioButton();
            this.rbBigMod = new System.Windows.Forms.RadioButton();
            this.txtStartNo = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.btnCancelPrintBarcode = new System.Windows.Forms.Button();
            this.btnOKPrintBarCode = new System.Windows.Forms.Button();
            this.txtNoOfPrint = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.lblMsg = new JitControls.OMLabel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnMixMode = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.cmbLocation = new System.Windows.Forms.ComboBox();
            this.chkFilterItem = new System.Windows.Forms.CheckBox();
            this.pnlInvSearch = new JitControls.OMGPanel();
            this.dgInvSearch = new System.Windows.Forms.DataGridView();
            this.btnAdvanceItemSelection = new System.Windows.Forms.Button();
            this.rdBtnAll = new System.Windows.Forms.RadioButton();
            this.rdBtnMin = new System.Windows.Forms.RadioButton();
            this.pnlSelect = new System.Windows.Forms.Panel();
            this.pnlActive = new JitControls.OMBPanel();
            this.lblSchemeStatus = new System.Windows.Forms.Label();
            this.btnOrderClosed = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.CompanyNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POCompanyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlTotalAmt.SuspendLayout();
            this.ombPanel1.SuspendLayout();
            this.pnlFooterInfo.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgBill)).BeginInit();
            this.pnlPartial.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPayCreditCardDetails)).BeginInit();
            this.pnlBranch.SuspendLayout();
            this.pnlBank.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPayChqDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgPayType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgParkingBills)).BeginInit();
            this.pnlParking.SuspendLayout();
            this.pnlItemName.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgItemList)).BeginInit();
            this.pnlGroup1.SuspendLayout();
            this.pnlGroup2.SuspendLayout();
            this.pnlUOM.SuspendLayout();
            this.pnlRate.SuspendLayout();
            this.pnlSalePurHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPurHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgSaleHistory)).BeginInit();
            this.pnlStockGodown.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgStockGodown)).BeginInit();
            this.pnlRateType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCompany)).BeginInit();
            this.pnlCompany.SuspendLayout();
            this.pnlPartySearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPartySearch)).BeginInit();
            this.pnlSearch.SuspendLayout();
            this.pnlAllPrintBarCode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPrintBarCode)).BeginInit();
            this.pnlBarCodePrint.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlInvSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgInvSearch)).BeginInit();
            this.pnlSelect.SuspendLayout();
            this.pnlActive.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(4, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 501;
            this.label1.Text = "Doc No :";
            // 
            // txtInvNo
            // 
            this.txtInvNo.BackColor = System.Drawing.Color.White;
            this.txtInvNo.Location = new System.Drawing.Point(63, 5);
            this.txtInvNo.Name = "txtInvNo";
            this.txtInvNo.ReadOnly = true;
            this.txtInvNo.Size = new System.Drawing.Size(88, 20);
            this.txtInvNo.TabIndex = 0;
            this.txtInvNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(175, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 502;
            this.label2.Text = "Date :";
            // 
            // dtpBillDate
            // 
            this.dtpBillDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpBillDate.Location = new System.Drawing.Point(220, 5);
            this.dtpBillDate.Name = "dtpBillDate";
            this.dtpBillDate.Size = new System.Drawing.Size(90, 20);
            this.dtpBillDate.TabIndex = 1;
            this.dtpBillDate.Leave += new System.EventHandler(this.dtpBillDate_Leave);
            this.dtpBillDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpBillDate_KeyDown);
            // 
            // dtpBillTime
            // 
            this.dtpBillTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpBillTime.Location = new System.Drawing.Point(893, 195);
            this.dtpBillTime.Name = "dtpBillTime";
            this.dtpBillTime.Size = new System.Drawing.Size(94, 20);
            this.dtpBillTime.TabIndex = 552;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(847, 199);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 503;
            this.label3.Text = "Time :";
            // 
            // pnlTotalAmt
            // 
            this.pnlTotalAmt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlTotalAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTotalAmt.Controls.Add(this.txtOtherTax);
            this.pnlTotalAmt.Controls.Add(this.lblOtherTax);
            this.pnlTotalAmt.Controls.Add(this.txtVisibility);
            this.pnlTotalAmt.Controls.Add(this.txtReturnAmt);
            this.pnlTotalAmt.Controls.Add(this.label28);
            this.pnlTotalAmt.Controls.Add(this.label27);
            this.pnlTotalAmt.Controls.Add(this.txtChrgRupees1);
            this.pnlTotalAmt.Controls.Add(this.txtDistDisc);
            this.pnlTotalAmt.Controls.Add(this.txtDiscRupees1);
            this.pnlTotalAmt.Controls.Add(this.label12);
            this.pnlTotalAmt.Controls.Add(this.lblGrandTotal);
            this.pnlTotalAmt.Controls.Add(this.txtRoundOff);
            this.pnlTotalAmt.Controls.Add(this.lblChrg1);
            this.pnlTotalAmt.Controls.Add(this.label35);
            this.pnlTotalAmt.Controls.Add(this.lblDisc1);
            this.pnlTotalAmt.Controls.Add(this.txtGrandTotal);
            this.pnlTotalAmt.Controls.Add(this.txtTotalTax);
            this.pnlTotalAmt.Controls.Add(this.txtTotalDisc);
            this.pnlTotalAmt.Controls.Add(this.txtSubTotal);
            this.pnlTotalAmt.Controls.Add(this.label8);
            this.pnlTotalAmt.Controls.Add(this.label7);
            this.pnlTotalAmt.Controls.Add(this.label6);
            this.pnlTotalAmt.Controls.Add(this.label5);
            this.pnlTotalAmt.Location = new System.Drawing.Point(694, 476);
            this.pnlTotalAmt.Name = "pnlTotalAmt";
            this.pnlTotalAmt.Size = new System.Drawing.Size(324, 148);
            this.pnlTotalAmt.TabIndex = 558;
            // 
            // txtOtherTax
            // 
            this.txtOtherTax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOtherTax.Location = new System.Drawing.Point(237, 71);
            this.txtOtherTax.Name = "txtOtherTax";
            this.txtOtherTax.Size = new System.Drawing.Size(78, 20);
            this.txtOtherTax.TabIndex = 5577;
            this.txtOtherTax.Text = "0.00";
            this.txtOtherTax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOtherTax.TextChanged += new System.EventHandler(this.txtOtherTax_TextChanged);
            this.txtOtherTax.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtOtherTax_KeyDown);
            this.txtOtherTax.Leave += new System.EventHandler(this.Control_Leave);
            // 
            // lblOtherTax
            // 
            this.lblOtherTax.AutoSize = true;
            this.lblOtherTax.Location = new System.Drawing.Point(167, 75);
            this.lblOtherTax.Name = "lblOtherTax";
            this.lblOtherTax.Size = new System.Drawing.Size(54, 13);
            this.lblOtherTax.TabIndex = 5578;
            this.lblOtherTax.Text = "Other Tax";
            // 
            // txtVisibility
            // 
            this.txtVisibility.Location = new System.Drawing.Point(82, 121);
            this.txtVisibility.MaxLength = 8;
            this.txtVisibility.Name = "txtVisibility";
            this.txtVisibility.Size = new System.Drawing.Size(78, 20);
            this.txtVisibility.TabIndex = 5576;
            this.txtVisibility.Text = "0.00";
            this.txtVisibility.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVisibility.TextChanged += new System.EventHandler(this.txtVisibility_TextChanged);
            this.txtVisibility.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtVisibility_KeyDown);
            this.txtVisibility.Leave += new System.EventHandler(this.Control_Leave);
            // 
            // txtReturnAmt
            // 
            this.txtReturnAmt.Location = new System.Drawing.Point(82, 96);
            this.txtReturnAmt.MaxLength = 8;
            this.txtReturnAmt.Name = "txtReturnAmt";
            this.txtReturnAmt.Size = new System.Drawing.Size(78, 20);
            this.txtReturnAmt.TabIndex = 5575;
            this.txtReturnAmt.Text = "0.00";
            this.txtReturnAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtReturnAmt.TextChanged += new System.EventHandler(this.txtReturnAmt_TextChanged);
            this.txtReturnAmt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtReturnAmt_KeyDown);
            this.txtReturnAmt.Leave += new System.EventHandler(this.Control_Leave);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(1, 122);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(46, 13);
            this.label28.TabIndex = 5574;
            this.label28.Text = "Visibility ";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(1, 99);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(63, 13);
            this.label27.TabIndex = 5573;
            this.label27.Text = "Return Amt.";
            // 
            // txtChrgRupees1
            // 
            this.txtChrgRupees1.Location = new System.Drawing.Point(82, 72);
            this.txtChrgRupees1.MaxLength = 8;
            this.txtChrgRupees1.Name = "txtChrgRupees1";
            this.txtChrgRupees1.Size = new System.Drawing.Size(78, 20);
            this.txtChrgRupees1.TabIndex = 11;
            this.txtChrgRupees1.Text = "0.00";
            this.txtChrgRupees1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtChrgRupees1.TextChanged += new System.EventHandler(this.txtChrgRupees1_TextChanged);
            this.txtChrgRupees1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtChrgRupees1_KeyDown);
            this.txtChrgRupees1.Leave += new System.EventHandler(this.Control_Leave);
            // 
            // txtDistDisc
            // 
            this.txtDistDisc.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.txtDistDisc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDistDisc.Location = new System.Drawing.Point(82, 26);
            this.txtDistDisc.Name = "txtDistDisc";
            this.txtDistDisc.ReadOnly = true;
            this.txtDistDisc.Size = new System.Drawing.Size(78, 20);
            this.txtDistDisc.TabIndex = 754;
            this.txtDistDisc.Text = "0.00";
            this.txtDistDisc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtDiscRupees1
            // 
            this.txtDiscRupees1.Location = new System.Drawing.Point(82, 50);
            this.txtDiscRupees1.MaxLength = 8;
            this.txtDiscRupees1.Name = "txtDiscRupees1";
            this.txtDiscRupees1.Size = new System.Drawing.Size(78, 20);
            this.txtDiscRupees1.TabIndex = 520;
            this.txtDiscRupees1.Text = "0.00";
            this.txtDiscRupees1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDiscRupees1.TextChanged += new System.EventHandler(this.txtDiscRupees1_TextChanged);
            this.txtDiscRupees1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDiscRupees1_KeyDown);
            this.txtDiscRupees1.Leave += new System.EventHandler(this.Control_Leave);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(0, 30);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 13);
            this.label12.TabIndex = 753;
            this.label12.Text = "Dist Disc.";
            // 
            // lblGrandTotal
            // 
            this.lblGrandTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblGrandTotal.BackColor = System.Drawing.Color.Olive;
            this.lblGrandTotal.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrandTotal.ForeColor = System.Drawing.Color.White;
            this.lblGrandTotal.Location = new System.Drawing.Point(163, 97);
            this.lblGrandTotal.Name = "lblGrandTotal";
            this.lblGrandTotal.Size = new System.Drawing.Size(156, 43);
            this.lblGrandTotal.TabIndex = 5572;
            this.lblGrandTotal.Text = "0.00";
            this.lblGrandTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtRoundOff
            // 
            this.txtRoundOff.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.txtRoundOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRoundOff.Location = new System.Drawing.Point(237, 49);
            this.txtRoundOff.Name = "txtRoundOff";
            this.txtRoundOff.ReadOnly = true;
            this.txtRoundOff.Size = new System.Drawing.Size(78, 20);
            this.txtRoundOff.TabIndex = 517;
            this.txtRoundOff.Text = "0.00";
            this.txtRoundOff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblChrg1
            // 
            this.lblChrg1.AutoSize = true;
            this.lblChrg1.Location = new System.Drawing.Point(1, 76);
            this.lblChrg1.Name = "lblChrg1";
            this.lblChrg1.Size = new System.Drawing.Size(49, 13);
            this.lblChrg1.TabIndex = 508;
            this.lblChrg1.Text = "Charges.";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(167, 53);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(56, 13);
            this.label35.TabIndex = 618;
            this.label35.Text = "Round Off";
            // 
            // lblDisc1
            // 
            this.lblDisc1.AutoSize = true;
            this.lblDisc1.Location = new System.Drawing.Point(1, 53);
            this.lblDisc1.Name = "lblDisc1";
            this.lblDisc1.Size = new System.Drawing.Size(61, 13);
            this.lblDisc1.TabIndex = 507;
            this.lblDisc1.Text = "Cash Disc. ";
            // 
            // txtGrandTotal
            // 
            this.txtGrandTotal.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.txtGrandTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGrandTotal.Location = new System.Drawing.Point(182, 103);
            this.txtGrandTotal.Name = "txtGrandTotal";
            this.txtGrandTotal.ReadOnly = true;
            this.txtGrandTotal.Size = new System.Drawing.Size(136, 20);
            this.txtGrandTotal.TabIndex = 750;
            this.txtGrandTotal.Text = "0.00";
            this.txtGrandTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGrandTotal.TextChanged += new System.EventHandler(this.txtGrandTotal_TextChanged);
            // 
            // txtTotalTax
            // 
            this.txtTotalTax.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.txtTotalTax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalTax.Location = new System.Drawing.Point(237, 25);
            this.txtTotalTax.Name = "txtTotalTax";
            this.txtTotalTax.ReadOnly = true;
            this.txtTotalTax.Size = new System.Drawing.Size(78, 20);
            this.txtTotalTax.TabIndex = 11;
            this.txtTotalTax.Text = "0.00";
            this.txtTotalTax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotalDisc
            // 
            this.txtTotalDisc.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.txtTotalDisc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalDisc.Location = new System.Drawing.Point(82, 2);
            this.txtTotalDisc.Name = "txtTotalDisc";
            this.txtTotalDisc.ReadOnly = true;
            this.txtTotalDisc.Size = new System.Drawing.Size(78, 20);
            this.txtTotalDisc.TabIndex = 10;
            this.txtTotalDisc.Text = "0.00";
            this.txtTotalDisc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSubTotal
            // 
            this.txtSubTotal.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.txtSubTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSubTotal.Location = new System.Drawing.Point(237, 3);
            this.txtSubTotal.Name = "txtSubTotal";
            this.txtSubTotal.ReadOnly = true;
            this.txtSubTotal.Size = new System.Drawing.Size(78, 20);
            this.txtSubTotal.TabIndex = 92;
            this.txtSubTotal.Text = "0.00";
            this.txtSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(175, 118);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 514;
            this.label8.Text = "Grand Total";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(167, 29);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 513;
            this.label7.Text = "Total Tax";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(-1, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 13);
            this.label6.TabIndex = 512;
            this.label6.Text = "Scheme Disc.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(167, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 511;
            this.label5.Text = "Sub Total ";
            // 
            // txtTotalAnotherDisc
            // 
            this.txtTotalAnotherDisc.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.txtTotalAnotherDisc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAnotherDisc.Location = new System.Drawing.Point(86, 4);
            this.txtTotalAnotherDisc.Name = "txtTotalAnotherDisc";
            this.txtTotalAnotherDisc.ReadOnly = true;
            this.txtTotalAnotherDisc.Size = new System.Drawing.Size(123, 20);
            this.txtTotalAnotherDisc.TabIndex = 519;
            this.txtTotalAnotherDisc.Text = "0.00";
            this.txtTotalAnotherDisc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // ombPanel1
            // 
            this.ombPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ombPanel1.BackColor = System.Drawing.Color.Olive;
            this.ombPanel1.BorderColor = System.Drawing.Color.Gray;
            this.ombPanel1.BorderRadius = 3;
            this.ombPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ombPanel1.Controls.Add(this.lblLastPayment);
            this.ombPanel1.Controls.Add(this.label11);
            this.ombPanel1.Controls.Add(this.button1);
            this.ombPanel1.Controls.Add(this.label9);
            this.ombPanel1.Controls.Add(this.label10);
            this.ombPanel1.Controls.Add(this.lblLastBillAmt);
            this.ombPanel1.Controls.Add(this.lblLastBillQty);
            this.ombPanel1.Controls.Add(this.btnAllBarCodePrint);
            this.ombPanel1.Controls.Add(this.panel1);
            this.ombPanel1.Location = new System.Drawing.Point(7, 23);
            this.ombPanel1.Name = "ombPanel1";
            this.ombPanel1.Size = new System.Drawing.Size(213, 78);
            this.ombPanel1.TabIndex = 5571;
            // 
            // lblLastPayment
            // 
            this.lblLastPayment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLastPayment.AutoSize = true;
            this.lblLastPayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastPayment.ForeColor = System.Drawing.Color.White;
            this.lblLastPayment.Location = new System.Drawing.Point(6, 63);
            this.lblLastPayment.Name = "lblLastPayment";
            this.lblLastPayment.Size = new System.Drawing.Size(55, 13);
            this.lblLastPayment.TabIndex = 5574;
            this.lblLastPayment.Text = "Payment";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(5, 3);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 13);
            this.label11.TabIndex = 530;
            this.label11.Text = "Last Bill";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Location = new System.Drawing.Point(57, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(69, 60);
            this.button1.TabIndex = 5570;
            this.button1.Text = ".";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.label9.Location = new System.Drawing.Point(465, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 13);
            this.label9.TabIndex = 506;
            this.label9.Visible = false;
            // 
            // label10
            // 
            this.label10.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.label10.Location = new System.Drawing.Point(409, 54);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(50, 13);
            this.label10.TabIndex = 505;
            this.label10.Visible = false;
            // 
            // lblLastBillAmt
            // 
            this.lblLastBillAmt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLastBillAmt.AutoSize = true;
            this.lblLastBillAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastBillAmt.ForeColor = System.Drawing.Color.White;
            this.lblLastBillAmt.Location = new System.Drawing.Point(5, 22);
            this.lblLastBillAmt.Name = "lblLastBillAmt";
            this.lblLastBillAmt.Size = new System.Drawing.Size(98, 13);
            this.lblLastBillAmt.TabIndex = 523;
            this.lblLastBillAmt.Text = "Last Bill Amount";
            // 
            // lblLastBillQty
            // 
            this.lblLastBillQty.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLastBillQty.AutoSize = true;
            this.lblLastBillQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastBillQty.ForeColor = System.Drawing.Color.White;
            this.lblLastBillQty.Location = new System.Drawing.Point(5, 41);
            this.lblLastBillQty.Name = "lblLastBillQty";
            this.lblLastBillQty.Size = new System.Drawing.Size(26, 13);
            this.lblLastBillQty.TabIndex = 528;
            this.lblLastBillQty.Text = "Qty";
            // 
            // btnAllBarCodePrint
            // 
            this.btnAllBarCodePrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAllBarCodePrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAllBarCodePrint.Location = new System.Drawing.Point(139, 7);
            this.btnAllBarCodePrint.Name = "btnAllBarCodePrint";
            this.btnAllBarCodePrint.Size = new System.Drawing.Size(69, 60);
            this.btnAllBarCodePrint.TabIndex = 25;
            this.btnAllBarCodePrint.Text = "Print\r\nBarcode";
            this.btnAllBarCodePrint.UseVisualStyleBackColor = true;
            this.btnAllBarCodePrint.Click += new System.EventHandler(this.btnAllBarCodePrint_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(16, 22);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(234, 42);
            this.panel1.TabIndex = 91;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(3, 7);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(58, 13);
            this.label36.TabIndex = 620;
            this.label36.Text = "Cash Disc.";
            // 
            // txtTotalChrgs
            // 
            this.txtTotalChrgs.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.txtTotalChrgs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalChrgs.Location = new System.Drawing.Point(68, 113);
            this.txtTotalChrgs.Name = "txtTotalChrgs";
            this.txtTotalChrgs.ReadOnly = true;
            this.txtTotalChrgs.Size = new System.Drawing.Size(81, 20);
            this.txtTotalChrgs.TabIndex = 515;
            this.txtTotalChrgs.Text = "0.00";
            this.txtTotalChrgs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(3, 113);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(59, 13);
            this.label34.TabIndex = 516;
            this.label34.Text = "Total Chrg.";
            // 
            // txtTotalItemDisc
            // 
            this.txtTotalItemDisc.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.txtTotalItemDisc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalItemDisc.Location = new System.Drawing.Point(858, 414);
            this.txtTotalItemDisc.Name = "txtTotalItemDisc";
            this.txtTotalItemDisc.ReadOnly = true;
            this.txtTotalItemDisc.Size = new System.Drawing.Size(123, 20);
            this.txtTotalItemDisc.TabIndex = 751;
            this.txtTotalItemDisc.Text = "0.00";
            this.txtTotalItemDisc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalItemDisc.Visible = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(779, 417);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(57, 13);
            this.label18.TabIndex = 752;
            this.label18.Text = "Item Disc. ";
            this.label18.Visible = false;
            // 
            // txtRemark
            // 
            this.txtRemark.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtRemark.Location = new System.Drawing.Point(235, 542);
            this.txtRemark.Name = "txtRemark";
            this.txtRemark.Size = new System.Drawing.Size(202, 20);
            this.txtRemark.TabIndex = 13;
            this.txtRemark.Leave += new System.EventHandler(this.txtRemark_Leave);
            // 
            // label32
            // 
            this.label32.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Location = new System.Drawing.Point(179, 545);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(44, 13);
            this.label32.TabIndex = 519;
            this.label32.Text = "Remark";
            // 
            // lblExchange
            // 
            this.lblExchange.AutoSize = true;
            this.lblExchange.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExchange.ForeColor = System.Drawing.Color.Maroon;
            this.lblExchange.Location = new System.Drawing.Point(168, 14);
            this.lblExchange.Name = "lblExchange";
            this.lblExchange.Size = new System.Drawing.Size(122, 13);
            this.lblExchange.TabIndex = 510;
            this.lblExchange.Text = "-- Exchange Mode --";
            this.lblExchange.Visible = false;
            // 
            // lblBilExchangeItem
            // 
            this.lblBilExchangeItem.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.lblBilExchangeItem.Location = new System.Drawing.Point(77, 15);
            this.lblBilExchangeItem.Name = "lblBilExchangeItem";
            this.lblBilExchangeItem.Size = new System.Drawing.Size(50, 13);
            this.lblBilExchangeItem.TabIndex = 506;
            // 
            // lblBillItem
            // 
            this.lblBillItem.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.lblBillItem.Location = new System.Drawing.Point(21, 15);
            this.lblBillItem.Name = "lblBillItem";
            this.lblBillItem.Size = new System.Drawing.Size(50, 13);
            this.lblBillItem.TabIndex = 505;
            // 
            // lblCompName
            // 
            this.lblCompName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblCompName.AutoSize = true;
            this.lblCompName.Location = new System.Drawing.Point(10, 126);
            this.lblCompName.Name = "lblCompName";
            this.lblCompName.Size = new System.Drawing.Size(40, 13);
            this.lblCompName.TabIndex = 5554;
            this.lblCompName.Text = "Party : ";
            // 
            // cmbPaymentType
            // 
            this.cmbPaymentType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbPaymentType.Enabled = false;
            this.cmbPaymentType.FormattingEnabled = true;
            this.cmbPaymentType.Items.AddRange(new object[] {
            "Cash",
            "Credit Card",
            "PA",
            "PP"});
            this.cmbPaymentType.Location = new System.Drawing.Point(512, 541);
            this.cmbPaymentType.Name = "cmbPaymentType";
            this.cmbPaymentType.Size = new System.Drawing.Size(172, 21);
            this.cmbPaymentType.TabIndex = 560;
            this.cmbPaymentType.Visible = false;
            this.cmbPaymentType.Leave += new System.EventHandler(this.cmbPaymentType_Leave);
            this.cmbPaymentType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbPaymentType_KeyDown);
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Enabled = false;
            this.label13.Location = new System.Drawing.Point(452, 546);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 13);
            this.label13.TabIndex = 509;
            this.label13.Text = "Payment :";
            this.label13.Visible = false;
            // 
            // txtRefNo
            // 
            this.txtRefNo.Location = new System.Drawing.Point(390, 5);
            this.txtRefNo.MaxLength = 20;
            this.txtRefNo.Name = "txtRefNo";
            this.txtRefNo.Size = new System.Drawing.Size(68, 20);
            this.txtRefNo.TabIndex = 2;
            this.txtRefNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRefNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRefNo_KeyDown);
            this.txtRefNo.Leave += new System.EventHandler(this.txtRefNo_Leave);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Location = new System.Drawing.Point(335, 8);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(45, 13);
            this.label25.TabIndex = 5555;
            this.label25.Text = "PO. No:";
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDelete.Location = new System.Drawing.Point(516, 475);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(69, 60);
            this.btnDelete.TabIndex = 19;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btndelete_Click);
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnNext.Location = new System.Drawing.Point(92, 539);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(37, 27);
            this.btnNext.TabIndex = 23;
            this.btnNext.Text = ">";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPrev.Location = new System.Drawing.Point(49, 539);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(37, 27);
            this.btnPrev.TabIndex = 22;
            this.btnPrev.Text = "<";
            this.btnPrev.UseVisualStyleBackColor = true;
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // btnLast
            // 
            this.btnLast.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLast.Location = new System.Drawing.Point(136, 539);
            this.btnLast.Name = "btnLast";
            this.btnLast.Size = new System.Drawing.Size(37, 27);
            this.btnLast.TabIndex = 24;
            this.btnLast.Text = ">|";
            this.btnLast.UseVisualStyleBackColor = true;
            this.btnLast.Click += new System.EventHandler(this.btnLast_Click);
            // 
            // btnFirst
            // 
            this.btnFirst.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnFirst.Location = new System.Drawing.Point(7, 539);
            this.btnFirst.Name = "btnFirst";
            this.btnFirst.Size = new System.Drawing.Size(37, 27);
            this.btnFirst.TabIndex = 21;
            this.btnFirst.Text = "|<";
            this.btnFirst.UseVisualStyleBackColor = true;
            this.btnFirst.Click += new System.EventHandler(this.btnFirst_Click);
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExit.Location = new System.Drawing.Point(368, 476);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(69, 60);
            this.btnExit.TabIndex = 18;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Location = new System.Drawing.Point(6, 476);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(69, 60);
            this.btnSave.TabIndex = 14;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // pnlFooterInfo
            // 
            this.pnlFooterInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlFooterInfo.BackColor = System.Drawing.Color.Olive;
            this.pnlFooterInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlFooterInfo.Controls.Add(this.label43);
            this.pnlFooterInfo.Controls.Add(this.label42);
            this.pnlFooterInfo.Controls.Add(this.label31);
            this.pnlFooterInfo.Controls.Add(this.label30);
            this.pnlFooterInfo.Controls.Add(this.label29);
            this.pnlFooterInfo.Controls.Add(this.label17);
            this.pnlFooterInfo.Controls.Add(this.lblStatus);
            this.pnlFooterInfo.Controls.Add(this.label14);
            this.pnlFooterInfo.Location = new System.Drawing.Point(12, 656);
            this.pnlFooterInfo.Name = "pnlFooterInfo";
            this.pnlFooterInfo.Size = new System.Drawing.Size(975, 26);
            this.pnlFooterInfo.TabIndex = 71;
            this.pnlFooterInfo.Visible = false;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.White;
            this.label43.Location = new System.Drawing.Point(647, 4);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(125, 13);
            this.label43.TabIndex = 530;
            this.label43.Text = "Ctrl+L : Landed Rate";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.White;
            this.label42.Location = new System.Drawing.Point(857, 4);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(104, 13);
            this.label42.TabIndex = 529;
            this.label42.Text = "ctrl+M :Mix Mode";
            this.label42.Visible = false;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.White;
            this.label31.Location = new System.Drawing.Point(389, 4);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(102, 13);
            this.label31.TabIndex = 528;
            this.label31.Text = "F11 :Credit Save";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.White;
            this.label30.Location = new System.Drawing.Point(512, 4);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(101, 13);
            this.label30.TabIndex = 527;
            this.label30.Text = "F12 : Cash Save";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.White;
            this.label29.Location = new System.Drawing.Point(249, 4);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(125, 13);
            this.label29.TabIndex = 526;
            this.label29.Text = "F4 :Change PayType";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(123, 4);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(111, 13);
            this.label17.TabIndex = 525;
            this.label17.Text = "Ctrl + Q :New Item";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.ForeColor = System.Drawing.Color.Maroon;
            this.lblStatus.Location = new System.Drawing.Point(7, 54);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(11, 13);
            this.lblStatus.TabIndex = 523;
            this.lblStatus.Text = ".";
            this.lblStatus.Click += new System.EventHandler(this.lblStatus_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(3, 4);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(99, 13);
            this.label14.TabIndex = 515;
            this.label14.Text = "F7 : Qty Change";
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel3.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label37);
            this.panel3.Controls.Add(this.txtlastBillQty);
            this.panel3.Controls.Add(this.txtLastPayment);
            this.panel3.Controls.Add(this.label23);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Controls.Add(this.txtLastBillAmt);
            this.panel3.Controls.Add(this.label24);
            this.panel3.Location = new System.Drawing.Point(789, 448);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(801, 29);
            this.panel3.TabIndex = 72;
            this.panel3.Visible = false;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Maroon;
            this.label37.Location = new System.Drawing.Point(689, 7);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(83, 13);
            this.label37.TabIndex = 530;
            this.label37.Text = "Ctrl + P: Print";
            // 
            // txtlastBillQty
            // 
            this.txtlastBillQty.BackColor = System.Drawing.Color.White;
            this.txtlastBillQty.Enabled = false;
            this.txtlastBillQty.Location = new System.Drawing.Point(331, 3);
            this.txtlastBillQty.Name = "txtlastBillQty";
            this.txtlastBillQty.ReadOnly = true;
            this.txtlastBillQty.Size = new System.Drawing.Size(107, 20);
            this.txtlastBillQty.TabIndex = 526;
            this.txtlastBillQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtLastPayment
            // 
            this.txtLastPayment.BackColor = System.Drawing.Color.White;
            this.txtLastPayment.Enabled = false;
            this.txtLastPayment.Location = new System.Drawing.Point(569, 3);
            this.txtLastPayment.Name = "txtLastPayment";
            this.txtLastPayment.ReadOnly = true;
            this.txtLastPayment.Size = new System.Drawing.Size(107, 20);
            this.txtLastPayment.TabIndex = 527;
            this.txtLastPayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Maroon;
            this.label23.Location = new System.Drawing.Point(227, 7);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(75, 13);
            this.label23.TabIndex = 528;
            this.label23.Text = "Last Bill Qty";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Maroon;
            this.label22.Location = new System.Drawing.Point(463, 7);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(55, 13);
            this.label22.TabIndex = 529;
            this.label22.Text = "Payment";
            // 
            // txtLastBillAmt
            // 
            this.txtLastBillAmt.BackColor = System.Drawing.Color.White;
            this.txtLastBillAmt.Enabled = false;
            this.txtLastBillAmt.Location = new System.Drawing.Point(107, 3);
            this.txtLastBillAmt.Name = "txtLastBillAmt";
            this.txtLastBillAmt.ReadOnly = true;
            this.txtLastBillAmt.Size = new System.Drawing.Size(107, 20);
            this.txtLastBillAmt.TabIndex = 13;
            this.txtLastBillAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Maroon;
            this.label24.Location = new System.Drawing.Point(4, 7);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(98, 13);
            this.label24.TabIndex = 523;
            this.label24.Text = "Last Bill Amount";
            // 
            // dgBill
            // 
            this.dgBill.AllowUserToAddRows = false;
            this.dgBill.AllowUserToDeleteRows = false;
            this.dgBill.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgBill.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgBill.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.ItemName,
            this.Quantity,
            this.UOM,
            this.Rate,
            this.MRP,
            this.NetRate,
            this.FreeQty,
            this.FreeUom,
            this.DiscPerce,
            this.DiscAmt,
            this.DiscRupees,
            this.DiscPercentage2,
            this.DiscAmount2,
            this.NetAmt,
            this.TaxPerce,
            this.TaxAmount,
            this.DiscRupees2,
            this.Amount,
            this.Barcode,
            this.PkStockTrnNo,
            this.PkSrNo,
            this.PkVoucherNo,
            this.ItemNo,
            this.UOMNo,
            this.TaxLedgerNo,
            this.SalesLedgerNo,
            this.PkRateSettingNo,
            this.PkItemTaxInfo,
            this.StockFactor,
            this.ActualQty,
            this.MKTQuantity,
            this.SalesVchNo,
            this.TaxVchNo,
            this.StockCompanyNo,
            this.BarCodePrinting,
            this.FreeUomNo,
            this.TempMRP,
            this.LandedRate,
            this.BalanceQty});
            this.dgBill.Location = new System.Drawing.Point(7, 67);
            this.dgBill.Name = "dgBill";
            this.dgBill.Size = new System.Drawing.Size(1010, 402);
            this.dgBill.TabIndex = 6;
            this.dgBill.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgBill_CellValueChanged);
            this.dgBill.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgBill_CellFormatting);
            this.dgBill.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgBill_CellEndEdit);
            this.dgBill.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgBill_CellClick);
            this.dgBill.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgBill_EditingControlShowing);
            this.dgBill.CurrentCellChanged += new System.EventHandler(this.dgBill_CurrentCellChanged);
            this.dgBill.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgBill_KeyDown);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Sr";
            this.Column1.HeaderText = "Sr";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 30;
            // 
            // ItemName
            // 
            this.ItemName.DataPropertyName = "ItemName";
            this.ItemName.HeaderText = "Description";
            this.ItemName.Name = "ItemName";
            this.ItemName.Width = 170;
            // 
            // Quantity
            // 
            this.Quantity.DataPropertyName = "Quantity";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Quantity.DefaultCellStyle = dataGridViewCellStyle1;
            this.Quantity.HeaderText = "Qty";
            this.Quantity.Name = "Quantity";
            this.Quantity.Width = 75;
            // 
            // UOM
            // 
            this.UOM.DataPropertyName = "UOMName";
            this.UOM.HeaderText = "UOM";
            this.UOM.Name = "UOM";
            this.UOM.ReadOnly = true;
            this.UOM.Width = 40;
            // 
            // Rate
            // 
            this.Rate.DataPropertyName = "Rate";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Rate.DefaultCellStyle = dataGridViewCellStyle2;
            this.Rate.HeaderText = "Rate";
            this.Rate.Name = "Rate";
            this.Rate.Width = 60;
            // 
            // MRP
            // 
            this.MRP.DataPropertyName = "MRP";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.MRP.DefaultCellStyle = dataGridViewCellStyle3;
            this.MRP.HeaderText = "MRP";
            this.MRP.Name = "MRP";
            this.MRP.Width = 60;
            // 
            // NetRate
            // 
            this.NetRate.DataPropertyName = "NetRate";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.NetRate.DefaultCellStyle = dataGridViewCellStyle4;
            this.NetRate.HeaderText = "NetRate";
            this.NetRate.Name = "NetRate";
            this.NetRate.ReadOnly = true;
            this.NetRate.Visible = false;
            this.NetRate.Width = 65;
            // 
            // FreeQty
            // 
            this.FreeQty.DataPropertyName = "FreeQty";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.FreeQty.DefaultCellStyle = dataGridViewCellStyle5;
            this.FreeQty.HeaderText = "FreeQty";
            this.FreeQty.Name = "FreeQty";
            this.FreeQty.Width = 60;
            // 
            // FreeUom
            // 
            this.FreeUom.DataPropertyName = "FreeUOM";
            this.FreeUom.HeaderText = "FreeUOM";
            this.FreeUom.Name = "FreeUom";
            this.FreeUom.ReadOnly = true;
            // 
            // DiscPerce
            // 
            this.DiscPerce.DataPropertyName = "DiscPercentage";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DiscPerce.DefaultCellStyle = dataGridViewCellStyle6;
            this.DiscPerce.HeaderText = "Scheme%";
            this.DiscPerce.Name = "DiscPerce";
            this.DiscPerce.Width = 70;
            // 
            // DiscAmt
            // 
            this.DiscAmt.DataPropertyName = "DiscAmount";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DiscAmt.DefaultCellStyle = dataGridViewCellStyle7;
            this.DiscAmt.HeaderText = "SchAmt";
            this.DiscAmt.Name = "DiscAmt";
            this.DiscAmt.Width = 65;
            // 
            // DiscRupees
            // 
            this.DiscRupees.DataPropertyName = "DiscRupees";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DiscRupees.DefaultCellStyle = dataGridViewCellStyle8;
            this.DiscRupees.HeaderText = "DiscRs.";
            this.DiscRupees.Name = "DiscRupees";
            this.DiscRupees.Visible = false;
            this.DiscRupees.Width = 55;
            // 
            // DiscPercentage2
            // 
            this.DiscPercentage2.DataPropertyName = "DiscPercentage2";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DiscPercentage2.DefaultCellStyle = dataGridViewCellStyle9;
            this.DiscPercentage2.HeaderText = "DistDisc%";
            this.DiscPercentage2.Name = "DiscPercentage2";
            this.DiscPercentage2.Visible = false;
            this.DiscPercentage2.Width = 70;
            // 
            // DiscAmount2
            // 
            this.DiscAmount2.DataPropertyName = "DiscAmount2";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DiscAmount2.DefaultCellStyle = dataGridViewCellStyle10;
            this.DiscAmount2.HeaderText = "DistDAmt";
            this.DiscAmount2.Name = "DiscAmount2";
            this.DiscAmount2.Width = 65;
            // 
            // NetAmt
            // 
            this.NetAmt.DataPropertyName = "NetAmt";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.NetAmt.DefaultCellStyle = dataGridViewCellStyle11;
            this.NetAmt.HeaderText = "NetAmt";
            this.NetAmt.Name = "NetAmt";
            this.NetAmt.ReadOnly = true;
            this.NetAmt.Visible = false;
            this.NetAmt.Width = 70;
            // 
            // TaxPerce
            // 
            this.TaxPerce.DataPropertyName = "TaxPercentage";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.TaxPerce.DefaultCellStyle = dataGridViewCellStyle12;
            this.TaxPerce.HeaderText = "Tax%";
            this.TaxPerce.Name = "TaxPerce";
            this.TaxPerce.ReadOnly = true;
            this.TaxPerce.Width = 45;
            // 
            // TaxAmount
            // 
            this.TaxAmount.DataPropertyName = "TaxAmount";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.TaxAmount.DefaultCellStyle = dataGridViewCellStyle13;
            this.TaxAmount.HeaderText = "VATAmt";
            this.TaxAmount.Name = "TaxAmount";
            this.TaxAmount.ReadOnly = true;
            this.TaxAmount.Width = 55;
            // 
            // DiscRupees2
            // 
            this.DiscRupees2.DataPropertyName = "DiscRupees2";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DiscRupees2.DefaultCellStyle = dataGridViewCellStyle14;
            this.DiscRupees2.HeaderText = "Chrg";
            this.DiscRupees2.Name = "DiscRupees2";
            this.DiscRupees2.Visible = false;
            this.DiscRupees2.Width = 50;
            // 
            // Amount
            // 
            this.Amount.DataPropertyName = "Amount";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Amount.DefaultCellStyle = dataGridViewCellStyle15;
            this.Amount.HeaderText = "Amount";
            this.Amount.Name = "Amount";
            this.Amount.ReadOnly = true;
            this.Amount.Width = 70;
            // 
            // Barcode
            // 
            this.Barcode.DataPropertyName = "Barcode";
            this.Barcode.HeaderText = "Barcode";
            this.Barcode.Name = "Barcode";
            this.Barcode.Visible = false;
            // 
            // PkStockTrnNo
            // 
            this.PkStockTrnNo.DataPropertyName = "PkStockTrnNo";
            this.PkStockTrnNo.HeaderText = "PkStockTrnNo";
            this.PkStockTrnNo.Name = "PkStockTrnNo";
            this.PkStockTrnNo.Visible = false;
            // 
            // PkSrNo
            // 
            this.PkSrNo.DataPropertyName = "PkStockBarcodeNo";
            this.PkSrNo.HeaderText = "PkBarcodeNo";
            this.PkSrNo.Name = "PkSrNo";
            this.PkSrNo.Visible = false;
            // 
            // PkVoucherNo
            // 
            this.PkVoucherNo.DataPropertyName = "PkVoucherNo";
            this.PkVoucherNo.HeaderText = "PkVoucherNo";
            this.PkVoucherNo.Name = "PkVoucherNo";
            this.PkVoucherNo.Visible = false;
            // 
            // ItemNo
            // 
            this.ItemNo.DataPropertyName = "ItemNo";
            this.ItemNo.HeaderText = "ItemNo";
            this.ItemNo.Name = "ItemNo";
            this.ItemNo.Visible = false;
            // 
            // UOMNo
            // 
            this.UOMNo.DataPropertyName = "UOMNo";
            this.UOMNo.HeaderText = "UOMNo";
            this.UOMNo.Name = "UOMNo";
            this.UOMNo.Visible = false;
            // 
            // TaxLedgerNo
            // 
            this.TaxLedgerNo.HeaderText = "TaxLedgerNo";
            this.TaxLedgerNo.Name = "TaxLedgerNo";
            this.TaxLedgerNo.Visible = false;
            // 
            // SalesLedgerNo
            // 
            this.SalesLedgerNo.HeaderText = "SalesLedgerNo";
            this.SalesLedgerNo.Name = "SalesLedgerNo";
            this.SalesLedgerNo.Visible = false;
            // 
            // PkRateSettingNo
            // 
            this.PkRateSettingNo.HeaderText = "RateSettingNo";
            this.PkRateSettingNo.Name = "PkRateSettingNo";
            this.PkRateSettingNo.Visible = false;
            // 
            // PkItemTaxInfo
            // 
            this.PkItemTaxInfo.HeaderText = "ItemTaxInfoNo";
            this.PkItemTaxInfo.Name = "PkItemTaxInfo";
            this.PkItemTaxInfo.Visible = false;
            // 
            // StockFactor
            // 
            this.StockFactor.DataPropertyName = "StockConversion";
            this.StockFactor.HeaderText = "StockFactor";
            this.StockFactor.Name = "StockFactor";
            this.StockFactor.Visible = false;
            // 
            // ActualQty
            // 
            this.ActualQty.HeaderText = "ActualQty";
            this.ActualQty.Name = "ActualQty";
            this.ActualQty.Visible = false;
            // 
            // MKTQuantity
            // 
            this.MKTQuantity.DataPropertyName = "MKTQty";
            this.MKTQuantity.HeaderText = "MKTQuantity";
            this.MKTQuantity.Name = "MKTQuantity";
            this.MKTQuantity.Visible = false;
            // 
            // SalesVchNo
            // 
            this.SalesVchNo.HeaderText = "SalesVchNo";
            this.SalesVchNo.Name = "SalesVchNo";
            this.SalesVchNo.Visible = false;
            // 
            // TaxVchNo
            // 
            this.TaxVchNo.HeaderText = "TaxVchNo";
            this.TaxVchNo.Name = "TaxVchNo";
            this.TaxVchNo.Visible = false;
            // 
            // StockCompanyNo
            // 
            this.StockCompanyNo.HeaderText = "StockCompanyNo";
            this.StockCompanyNo.Name = "StockCompanyNo";
            this.StockCompanyNo.Visible = false;
            // 
            // BarCodePrinting
            // 
            this.BarCodePrinting.HeaderText = "Print";
            this.BarCodePrinting.Name = "BarCodePrinting";
            this.BarCodePrinting.Visible = false;
            this.BarCodePrinting.Width = 60;
            // 
            // FreeUomNo
            // 
            this.FreeUomNo.DataPropertyName = "FreeUOMNo";
            this.FreeUomNo.HeaderText = "FreeUomNo";
            this.FreeUomNo.Name = "FreeUomNo";
            // 
            // TempMRP
            // 
            this.TempMRP.DataPropertyName = "TempMRP";
            this.TempMRP.HeaderText = "TempMRP";
            this.TempMRP.Name = "TempMRP";
            this.TempMRP.Visible = false;
            // 
            // LandedRate
            // 
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.LandedRate.DefaultCellStyle = dataGridViewCellStyle16;
            this.LandedRate.HeaderText = "LandedRate";
            this.LandedRate.Name = "LandedRate";
            this.LandedRate.ReadOnly = true;
            this.LandedRate.Width = 65;
            // 
            // BalanceQty
            // 
            this.BalanceQty.HeaderText = "BalanceQty";
            this.BalanceQty.Name = "BalanceQty";
            this.BalanceQty.Visible = false;
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSearch.Location = new System.Drawing.Point(151, 476);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(69, 60);
            this.btnSearch.TabIndex = 20;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // pnlPartial
            // 
            this.pnlPartial.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlPartial.Controls.Add(this.label38);
            this.pnlPartial.Controls.Add(this.dgPayCreditCardDetails);
            this.pnlPartial.Controls.Add(this.pnlBranch);
            this.pnlPartial.Controls.Add(this.pnlBank);
            this.pnlPartial.Controls.Add(this.dtpChqDate);
            this.pnlPartial.Controls.Add(this.dgPayChqDetails);
            this.pnlPartial.Controls.Add(this.lblPayTypeBal);
            this.pnlPartial.Controls.Add(this.dgPayType);
            this.pnlPartial.Controls.Add(this.btnOk);
            this.pnlPartial.Controls.Add(this.txtTotalAmt);
            this.pnlPartial.Controls.Add(this.label26);
            this.pnlPartial.Location = new System.Drawing.Point(200, 143);
            this.pnlPartial.Name = "pnlPartial";
            this.pnlPartial.Size = new System.Drawing.Size(305, 221);
            this.pnlPartial.TabIndex = 506;
            this.pnlPartial.Visible = false;
            // 
            // label38
            // 
            this.label38.Location = new System.Drawing.Point(6, 131);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(288, 37);
            this.label38.TabIndex = 5548;
            this.label38.Text = "(Ctrl + D) Display Details (For Cheque and Credit Card.";
            // 
            // dgPayCreditCardDetails
            // 
            this.dgPayCreditCardDetails.AllowUserToAddRows = false;
            this.dgPayCreditCardDetails.AllowUserToDeleteRows = false;
            this.dgPayCreditCardDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPayCreditCardDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CreditCardNo,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12});
            this.dgPayCreditCardDetails.Location = new System.Drawing.Point(304, 3);
            this.dgPayCreditCardDetails.Name = "dgPayCreditCardDetails";
            this.dgPayCreditCardDetails.Size = new System.Drawing.Size(464, 169);
            this.dgPayCreditCardDetails.TabIndex = 5547;
            this.dgPayCreditCardDetails.Visible = false;
            this.dgPayCreditCardDetails.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPayCreditCardDetails_CellEndEdit);
            this.dgPayCreditCardDetails.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgPayCreditCardDetails_KeyDown);
            // 
            // CreditCardNo
            // 
            this.CreditCardNo.DataPropertyName = "CreditCardNo";
            this.CreditCardNo.HeaderText = "Cr. Card No";
            this.CreditCardNo.MaxInputLength = 20;
            this.CreditCardNo.Name = "CreditCardNo";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "BankName";
            this.dataGridViewTextBoxColumn7.HeaderText = "Bank Name";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 240;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "BranchName";
            this.dataGridViewTextBoxColumn8.HeaderText = "BranchName";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 120;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "Amount";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridViewTextBoxColumn9.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "PkSrNo";
            this.dataGridViewTextBoxColumn10.HeaderText = "PkSrNo";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Visible = false;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "BankNo";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.Visible = false;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "BranchNo";
            this.dataGridViewTextBoxColumn12.HeaderText = "BranchNo";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.Visible = false;
            // 
            // pnlBranch
            // 
            this.pnlBranch.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.pnlBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBranch.Controls.Add(this.lstBranch);
            this.pnlBranch.Location = new System.Drawing.Point(538, 39);
            this.pnlBranch.Name = "pnlBranch";
            this.pnlBranch.Size = new System.Drawing.Size(164, 117);
            this.pnlBranch.TabIndex = 5544;
            this.pnlBranch.Visible = false;
            // 
            // lstBranch
            // 
            this.lstBranch.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstBranch.FormattingEnabled = true;
            this.lstBranch.Location = new System.Drawing.Point(8, 8);
            this.lstBranch.Name = "lstBranch";
            this.lstBranch.Size = new System.Drawing.Size(147, 93);
            this.lstBranch.TabIndex = 516;
            this.lstBranch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstBranch_KeyDown);
            // 
            // pnlBank
            // 
            this.pnlBank.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.pnlBank.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBank.Controls.Add(this.lstBank);
            this.pnlBank.Location = new System.Drawing.Point(352, 38);
            this.pnlBank.Name = "pnlBank";
            this.pnlBank.Size = new System.Drawing.Size(176, 118);
            this.pnlBank.TabIndex = 5545;
            this.pnlBank.Visible = false;
            // 
            // lstBank
            // 
            this.lstBank.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstBank.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstBank.FormattingEnabled = true;
            this.lstBank.Location = new System.Drawing.Point(8, 10);
            this.lstBank.Name = "lstBank";
            this.lstBank.Size = new System.Drawing.Size(159, 93);
            this.lstBank.TabIndex = 516;
            this.lstBank.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstBank_KeyDown);
            // 
            // dtpChqDate
            // 
            this.dtpChqDate.Location = new System.Drawing.Point(471, 45);
            this.dtpChqDate.Name = "dtpChqDate";
            this.dtpChqDate.TabIndex = 5546;
            this.dtpChqDate.Visible = false;
            this.dtpChqDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpChqDate_KeyDown);
            // 
            // dgPayChqDetails
            // 
            this.dgPayChqDetails.AllowUserToAddRows = false;
            this.dgPayChqDetails.AllowUserToDeleteRows = false;
            this.dgPayChqDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPayChqDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ChequeNo,
            this.ChequeDate,
            this.BankName,
            this.BranchName,
            this.AmountChq,
            this.PkSrNoChq,
            this.BankNo,
            this.BranchNo});
            this.dgPayChqDetails.Location = new System.Drawing.Point(304, 3);
            this.dgPayChqDetails.Name = "dgPayChqDetails";
            this.dgPayChqDetails.Size = new System.Drawing.Size(464, 169);
            this.dgPayChqDetails.TabIndex = 659;
            this.dgPayChqDetails.Visible = false;
            this.dgPayChqDetails.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPayChqDetails_CellEndEdit);
            this.dgPayChqDetails.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgPayChqDetails_KeyDown);
            // 
            // ChequeNo
            // 
            this.ChequeNo.DataPropertyName = "ChequeNo";
            this.ChequeNo.HeaderText = "Chq. No";
            this.ChequeNo.MaxInputLength = 10;
            this.ChequeNo.Name = "ChequeNo";
            this.ChequeNo.Width = 75;
            // 
            // ChequeDate
            // 
            this.ChequeDate.DataPropertyName = "ChequeDate";
            this.ChequeDate.HeaderText = "Date";
            this.ChequeDate.Name = "ChequeDate";
            // 
            // BankName
            // 
            this.BankName.DataPropertyName = "BankName";
            this.BankName.HeaderText = "Bank Name";
            this.BankName.Name = "BankName";
            this.BankName.ReadOnly = true;
            this.BankName.Width = 240;
            // 
            // BranchName
            // 
            this.BranchName.DataPropertyName = "BranchName";
            this.BranchName.HeaderText = "BranchName";
            this.BranchName.Name = "BranchName";
            this.BranchName.ReadOnly = true;
            this.BranchName.Width = 120;
            // 
            // AmountChq
            // 
            this.AmountChq.DataPropertyName = "Amount";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.AmountChq.DefaultCellStyle = dataGridViewCellStyle18;
            this.AmountChq.HeaderText = "Amount";
            this.AmountChq.Name = "AmountChq";
            // 
            // PkSrNoChq
            // 
            this.PkSrNoChq.DataPropertyName = "PkSrNo";
            this.PkSrNoChq.HeaderText = "PkSrNo";
            this.PkSrNoChq.Name = "PkSrNoChq";
            this.PkSrNoChq.Visible = false;
            // 
            // BankNo
            // 
            this.BankNo.HeaderText = "BankNo";
            this.BankNo.Name = "BankNo";
            this.BankNo.Visible = false;
            // 
            // BranchNo
            // 
            this.BranchNo.DataPropertyName = "BranchNo";
            this.BranchNo.HeaderText = "BranchNo";
            this.BranchNo.Name = "BranchNo";
            this.BranchNo.Visible = false;
            // 
            // lblPayTypeBal
            // 
            this.lblPayTypeBal.AutoSize = true;
            this.lblPayTypeBal.Location = new System.Drawing.Point(86, 185);
            this.lblPayTypeBal.Name = "lblPayTypeBal";
            this.lblPayTypeBal.Size = new System.Drawing.Size(0, 13);
            this.lblPayTypeBal.TabIndex = 658;
            // 
            // dgPayType
            // 
            this.dgPayType.AllowUserToAddRows = false;
            this.dgPayType.AllowUserToDeleteRows = false;
            this.dgPayType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPayType.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn5,
            this.PKVoucherPayTypeNo});
            this.dgPayType.Location = new System.Drawing.Point(2, 3);
            this.dgPayType.Name = "dgPayType";
            this.dgPayType.Size = new System.Drawing.Size(295, 169);
            this.dgPayType.TabIndex = 657;
            this.dgPayType.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPayType_CellEndEdit);
            this.dgPayType.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgPayType_EditingControlShowing);
            this.dgPayType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgPayType_KeyDown);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "PayTypeName";
            this.dataGridViewTextBoxColumn1.HeaderText = "Pay Type";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "PKPayTypeNo";
            this.dataGridViewTextBoxColumn4.HeaderText = "PayTypeNo";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Amount";
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle19;
            this.dataGridViewTextBoxColumn2.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "LedgerNo";
            this.dataGridViewTextBoxColumn5.HeaderText = "LedgerNo";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // PKVoucherPayTypeNo
            // 
            this.PKVoucherPayTypeNo.DataPropertyName = "PKVoucherPayTypeNo";
            this.PKVoucherPayTypeNo.HeaderText = "PKVoucherPayTypeNo";
            this.PKVoucherPayTypeNo.Name = "PKVoucherPayTypeNo";
            this.PKVoucherPayTypeNo.Visible = false;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(3, 178);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // txtTotalAmt
            // 
            this.txtTotalAmt.BackColor = System.Drawing.Color.White;
            this.txtTotalAmt.Enabled = false;
            this.txtTotalAmt.Location = new System.Drawing.Point(222, 178);
            this.txtTotalAmt.Name = "txtTotalAmt";
            this.txtTotalAmt.ReadOnly = true;
            this.txtTotalAmt.Size = new System.Drawing.Size(72, 20);
            this.txtTotalAmt.TabIndex = 656;
            this.txtTotalAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(171, 183);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(37, 13);
            this.label26.TabIndex = 502;
            this.label26.Text = "Total :";
            // 
            // dgParkingBills
            // 
            this.dgParkingBills.AllowUserToAddRows = false;
            this.dgParkingBills.AllowUserToDeleteRows = false;
            this.dgParkingBills.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgParkingBills.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BillNo,
            this.BillAmount});
            this.dgParkingBills.Location = new System.Drawing.Point(5, 4);
            this.dgParkingBills.Name = "dgParkingBills";
            this.dgParkingBills.ReadOnly = true;
            this.dgParkingBills.Size = new System.Drawing.Size(193, 132);
            this.dgParkingBills.TabIndex = 0;
            // 
            // BillNo
            // 
            this.BillNo.DataPropertyName = "BillNo";
            this.BillNo.HeaderText = "BillNo";
            this.BillNo.Name = "BillNo";
            this.BillNo.ReadOnly = true;
            this.BillNo.Width = 50;
            // 
            // BillAmount
            // 
            this.BillAmount.DataPropertyName = "Amount";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.BillAmount.DefaultCellStyle = dataGridViewCellStyle20;
            this.BillAmount.HeaderText = "Amount";
            this.BillAmount.Name = "BillAmount";
            this.BillAmount.ReadOnly = true;
            // 
            // pnlParking
            // 
            this.pnlParking.Controls.Add(this.dgParkingBills);
            this.pnlParking.Location = new System.Drawing.Point(281, 174);
            this.pnlParking.Name = "pnlParking";
            this.pnlParking.Size = new System.Drawing.Size(203, 141);
            this.pnlParking.TabIndex = 508;
            this.pnlParking.Visible = false;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnUpdate.Location = new System.Drawing.Point(78, 476);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(69, 60);
            this.btnUpdate.TabIndex = 16;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnNew
            // 
            this.btnNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnNew.Location = new System.Drawing.Point(6, 476);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(69, 60);
            this.btnNew.TabIndex = 15;
            this.btnNew.Text = "New";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.Location = new System.Drawing.Point(78, 476);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(69, 60);
            this.btnCancel.TabIndex = 17;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // pnlItemName
            // 
            this.pnlItemName.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.pnlItemName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlItemName.Controls.Add(this.btnOk1);
            this.pnlItemName.Controls.Add(this.dgItemList);
            this.pnlItemName.Location = new System.Drawing.Point(15, 88);
            this.pnlItemName.Name = "pnlItemName";
            this.pnlItemName.Size = new System.Drawing.Size(348, 233);
            this.pnlItemName.TabIndex = 514;
            this.pnlItemName.Visible = false;
            this.pnlItemName.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            // 
            // btnOk1
            // 
            this.btnOk1.Location = new System.Drawing.Point(15, 275);
            this.btnOk1.Name = "btnOk1";
            this.btnOk1.Size = new System.Drawing.Size(75, 23);
            this.btnOk1.TabIndex = 516;
            this.btnOk1.Text = "OK (F5)";
            this.btnOk1.UseVisualStyleBackColor = true;
            this.btnOk1.Click += new System.EventHandler(this.btnOk1_Click);
            // 
            // dgItemList
            // 
            this.dgItemList.AllowUserToAddRows = false;
            this.dgItemList.AllowUserToDeleteRows = false;
            this.dgItemList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgItemList.BackgroundColor = OM.ThemeColor.Panle_Back_Color;
            this.dgItemList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgItemList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iItemNo,
            this.iItemName,
            this.ItemNameLang,
            this.Qty0,
            this.iRate,
            this.iUOM,
            this.iMRP,
            this.iStock,
            this.MinLevel,
            this.MaxLevel,
            this.iUOMStk,
            this.iSaleTax,
            this.iPurTax,
            this.iCompany,
            this.iBarcode,
            this.iRateSettingNo,
            this.UOMDefault,
            this.PurRate,
            this.SuggMin,
            this.SugMax});
            this.dgItemList.Location = new System.Drawing.Point(8, 8);
            this.dgItemList.Name = "dgItemList";
            this.dgItemList.Size = new System.Drawing.Size(976, 200);
            this.dgItemList.TabIndex = 515;
            this.dgItemList.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgItemList_EditingControlShowing);
            this.dgItemList.CurrentCellChanged += new System.EventHandler(this.dgItemList_CurrentCellChanged);
            this.dgItemList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgItemList_KeyDown);
            this.dgItemList.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            // 
            // iItemNo
            // 
            this.iItemNo.DataPropertyName = "ItemNo";
            this.iItemNo.HeaderText = "ItemNo";
            this.iItemNo.Name = "iItemNo";
            this.iItemNo.ReadOnly = true;
            this.iItemNo.ToolTipText = "ItemNo";
            this.iItemNo.Visible = false;
            // 
            // iItemName
            // 
            this.iItemName.DataPropertyName = "ItemName";
            this.iItemName.HeaderText = "Item Description";
            this.iItemName.Name = "iItemName";
            this.iItemName.ReadOnly = true;
            this.iItemName.ToolTipText = "Product Name";
            this.iItemName.Width = 200;
            // 
            // ItemNameLang
            // 
            this.ItemNameLang.DataPropertyName = "ItemNameLang";
            this.ItemNameLang.HeaderText = "Item Name";
            this.ItemNameLang.Name = "ItemNameLang";
            this.ItemNameLang.ReadOnly = true;
            // 
            // Qty0
            // 
            this.Qty0.DataPropertyName = "Quantity";
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Qty0.DefaultCellStyle = dataGridViewCellStyle21;
            this.Qty0.HeaderText = "Qty";
            this.Qty0.Name = "Qty0";
            this.Qty0.Width = 50;
            // 
            // iRate
            // 
            this.iRate.DataPropertyName = "SaleRate";
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle22.Format = "N2";
            dataGridViewCellStyle22.NullValue = null;
            this.iRate.DefaultCellStyle = dataGridViewCellStyle22;
            this.iRate.HeaderText = "Rate";
            this.iRate.Name = "iRate";
            this.iRate.ReadOnly = true;
            this.iRate.ToolTipText = "Sales Rate";
            this.iRate.Width = 60;
            // 
            // iUOM
            // 
            this.iUOM.DataPropertyName = "UOMName";
            this.iUOM.HeaderText = "UOM";
            this.iUOM.Name = "iUOM";
            this.iUOM.ReadOnly = true;
            this.iUOM.ToolTipText = "UOM";
            this.iUOM.Width = 60;
            // 
            // iMRP
            // 
            this.iMRP.DataPropertyName = "MRP";
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle23.Format = "N2";
            this.iMRP.DefaultCellStyle = dataGridViewCellStyle23;
            this.iMRP.HeaderText = "MRP";
            this.iMRP.Name = "iMRP";
            this.iMRP.ReadOnly = true;
            this.iMRP.ToolTipText = "MRP";
            this.iMRP.Width = 60;
            // 
            // iStock
            // 
            this.iStock.DataPropertyName = "Stock";
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle24.Format = "N2";
            this.iStock.DefaultCellStyle = dataGridViewCellStyle24;
            this.iStock.HeaderText = "Stock";
            this.iStock.Name = "iStock";
            this.iStock.ReadOnly = true;
            this.iStock.ToolTipText = "Stock QTY";
            this.iStock.Width = 60;
            // 
            // MinLevel
            // 
            this.MinLevel.DataPropertyName = "MinLevel";
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.MinLevel.DefaultCellStyle = dataGridViewCellStyle25;
            this.MinLevel.HeaderText = "MinLevel";
            this.MinLevel.Name = "MinLevel";
            this.MinLevel.ReadOnly = true;
            this.MinLevel.Width = 70;
            // 
            // MaxLevel
            // 
            this.MaxLevel.DataPropertyName = "MaxLevel";
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.MaxLevel.DefaultCellStyle = dataGridViewCellStyle26;
            this.MaxLevel.HeaderText = "MaxLevel";
            this.MaxLevel.Name = "MaxLevel";
            this.MaxLevel.ReadOnly = true;
            this.MaxLevel.Width = 70;
            // 
            // iUOMStk
            // 
            this.iUOMStk.DataPropertyName = "stkUOM";
            this.iUOMStk.HeaderText = "UOM";
            this.iUOMStk.Name = "iUOMStk";
            this.iUOMStk.ReadOnly = true;
            this.iUOMStk.ToolTipText = "Stock UOM";
            this.iUOMStk.Visible = false;
            this.iUOMStk.Width = 50;
            // 
            // iSaleTax
            // 
            this.iSaleTax.DataPropertyName = "SaleTax";
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle27.Format = "N2";
            dataGridViewCellStyle27.NullValue = null;
            this.iSaleTax.DefaultCellStyle = dataGridViewCellStyle27;
            this.iSaleTax.HeaderText = "S.Tax";
            this.iSaleTax.Name = "iSaleTax";
            this.iSaleTax.ReadOnly = true;
            this.iSaleTax.ToolTipText = "Sales Tax Percent";
            this.iSaleTax.Visible = false;
            this.iSaleTax.Width = 50;
            // 
            // iPurTax
            // 
            this.iPurTax.DataPropertyName = "PurTax";
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle28.Format = "N2";
            dataGridViewCellStyle28.NullValue = null;
            this.iPurTax.DefaultCellStyle = dataGridViewCellStyle28;
            this.iPurTax.HeaderText = "P.Tax";
            this.iPurTax.Name = "iPurTax";
            this.iPurTax.ReadOnly = true;
            this.iPurTax.ToolTipText = "Purchase Tax Percent";
            this.iPurTax.Visible = false;
            this.iPurTax.Width = 50;
            // 
            // iCompany
            // 
            this.iCompany.DataPropertyName = "CompanyNo";
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle29.Format = "N0";
            dataGridViewCellStyle29.NullValue = null;
            this.iCompany.DefaultCellStyle = dataGridViewCellStyle29;
            this.iCompany.HeaderText = "CNo";
            this.iCompany.Name = "iCompany";
            this.iCompany.ReadOnly = true;
            this.iCompany.ToolTipText = "Company No";
            this.iCompany.Visible = false;
            this.iCompany.Width = 30;
            // 
            // iBarcode
            // 
            this.iBarcode.DataPropertyName = "Barcode";
            this.iBarcode.HeaderText = "Barcode";
            this.iBarcode.Name = "iBarcode";
            this.iBarcode.ReadOnly = true;
            this.iBarcode.ToolTipText = "Barcode";
            this.iBarcode.Width = 125;
            // 
            // iRateSettingNo
            // 
            this.iRateSettingNo.DataPropertyName = "RateSettingNo";
            this.iRateSettingNo.HeaderText = "RateSettingNo";
            this.iRateSettingNo.Name = "iRateSettingNo";
            this.iRateSettingNo.ReadOnly = true;
            this.iRateSettingNo.Visible = false;
            // 
            // UOMDefault
            // 
            this.UOMDefault.DataPropertyName = "UOMDefault";
            this.UOMDefault.HeaderText = "UOMDefault";
            this.UOMDefault.Name = "UOMDefault";
            this.UOMDefault.ReadOnly = true;
            this.UOMDefault.Visible = false;
            // 
            // PurRate
            // 
            this.PurRate.DataPropertyName = "PurRate";
            this.PurRate.HeaderText = "PurRate";
            this.PurRate.Name = "PurRate";
            this.PurRate.ReadOnly = true;
            this.PurRate.Visible = false;
            // 
            // SuggMin
            // 
            this.SuggMin.DataPropertyName = "SuggMin";
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.SuggMin.DefaultCellStyle = dataGridViewCellStyle30;
            this.SuggMin.HeaderText = "SuggMin";
            this.SuggMin.Name = "SuggMin";
            this.SuggMin.ReadOnly = true;
            this.SuggMin.Width = 70;
            // 
            // SugMax
            // 
            this.SugMax.DataPropertyName = "SuggMax";
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.SugMax.DefaultCellStyle = dataGridViewCellStyle31;
            this.SugMax.HeaderText = "SuggMax";
            this.SugMax.Name = "SugMax";
            this.SugMax.ReadOnly = true;
            this.SugMax.Width = 70;
            // 
            // lstUOM
            // 
            this.lstUOM.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstUOM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstUOM.FormattingEnabled = true;
            this.lstUOM.Location = new System.Drawing.Point(8, 8);
            this.lstUOM.Name = "lstUOM";
            this.lstUOM.Size = new System.Drawing.Size(101, 28);
            this.lstUOM.TabIndex = 516;
            this.lstUOM.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            this.lstUOM.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lstUOM_KeyPress);
            // 
            // lstRate
            // 
            this.lstRate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstRate.FormattingEnabled = true;
            this.lstRate.Location = new System.Drawing.Point(8, 8);
            this.lstRate.Name = "lstRate";
            this.lstRate.Size = new System.Drawing.Size(101, 41);
            this.lstRate.TabIndex = 517;
            this.lstRate.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            this.lstRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lstRate_KeyPress);
            // 
            // lstGroup1
            // 
            this.lstGroup1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstGroup1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstGroup1.FormattingEnabled = true;
            this.lstGroup1.Location = new System.Drawing.Point(8, 8);
            this.lstGroup1.Name = "lstGroup1";
            this.lstGroup1.Size = new System.Drawing.Size(90, 28);
            this.lstGroup1.TabIndex = 516;
            this.lstGroup1.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            this.lstGroup1.SelectedIndexChanged += new System.EventHandler(this.lstGroup1_SelectedIndexChanged);
            this.lstGroup1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lstGroup1_KeyPress);
            this.lstGroup1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstGroup1_KeyDown);
            // 
            // lstGroup2
            // 
            this.lstGroup2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstGroup2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstGroup2.FormattingEnabled = true;
            this.lstGroup2.Location = new System.Drawing.Point(8, 8);
            this.lstGroup2.Name = "lstGroup2";
            this.lstGroup2.Size = new System.Drawing.Size(101, 28);
            this.lstGroup2.TabIndex = 517;
            this.lstGroup2.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            this.lstGroup2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lstGroup2_KeyPress);
            this.lstGroup2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstGroup2_KeyDown);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Location = new System.Drawing.Point(4, 38);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(49, 13);
            this.label33.TabIndex = 551;
            this.label33.Text = "Party    : ";
            // 
            // cmbPartyName
            // 
            this.cmbPartyName.FormattingEnabled = true;
            this.cmbPartyName.Location = new System.Drawing.Point(63, 37);
            this.cmbPartyName.Name = "cmbPartyName";
            this.cmbPartyName.Size = new System.Drawing.Size(519, 21);
            this.cmbPartyName.TabIndex = 4;
            this.cmbPartyName.Leave += new System.EventHandler(this.cmbPartyName_Leave);
            this.cmbPartyName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbPartyName_KeyDown);
            // 
            // cmbTaxType
            // 
            this.cmbTaxType.FormattingEnabled = true;
            this.cmbTaxType.Location = new System.Drawing.Point(696, 39);
            this.cmbTaxType.Name = "cmbTaxType";
            this.cmbTaxType.Size = new System.Drawing.Size(77, 21);
            this.cmbTaxType.TabIndex = 5;
            this.cmbTaxType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbTaxType_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(622, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 5542;
            this.label4.Text = "Tax          :";
            // 
            // lblRateType
            // 
            this.lblRateType.AutoSize = true;
            this.lblRateType.Location = new System.Drawing.Point(786, 238);
            this.lblRateType.Name = "lblRateType";
            this.lblRateType.Size = new System.Drawing.Size(57, 13);
            this.lblRateType.TabIndex = 556;
            this.lblRateType.Text = "Rate Type";
            // 
            // cmbRateType
            // 
            this.cmbRateType.FormattingEnabled = true;
            this.cmbRateType.Location = new System.Drawing.Point(861, 234);
            this.cmbRateType.Name = "cmbRateType";
            this.cmbRateType.Size = new System.Drawing.Size(90, 21);
            this.cmbRateType.TabIndex = 553;
            this.cmbRateType.SelectedIndexChanged += new System.EventHandler(this.cmbRateType_SelectedIndexChanged);
            this.cmbRateType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbRateType_KeyDown);
            // 
            // pnlGroup1
            // 
            this.pnlGroup1.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.pnlGroup1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlGroup1.Controls.Add(this.lstGroup1Lang);
            this.pnlGroup1.Controls.Add(this.lstGroup1);
            this.pnlGroup1.Location = new System.Drawing.Point(18, 154);
            this.pnlGroup1.Name = "pnlGroup1";
            this.pnlGroup1.Size = new System.Drawing.Size(118, 46);
            this.pnlGroup1.TabIndex = 5543;
            this.pnlGroup1.Visible = false;
            this.pnlGroup1.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            // 
            // lstGroup1Lang
            // 
            this.lstGroup1Lang.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstGroup1Lang.FormattingEnabled = true;
            this.lstGroup1Lang.Location = new System.Drawing.Point(308, 7);
            this.lstGroup1Lang.Name = "lstGroup1Lang";
            this.lstGroup1Lang.Size = new System.Drawing.Size(257, 262);
            this.lstGroup1Lang.TabIndex = 517;
            this.lstGroup1Lang.SelectedIndexChanged += new System.EventHandler(this.lstGroup1Lang_SelectedIndexChanged);
            this.lstGroup1Lang.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lstGroup1_KeyPress);
            // 
            // pnlGroup2
            // 
            this.pnlGroup2.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.pnlGroup2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlGroup2.Controls.Add(this.lstGroup2);
            this.pnlGroup2.Location = new System.Drawing.Point(19, 203);
            this.pnlGroup2.Name = "pnlGroup2";
            this.pnlGroup2.Size = new System.Drawing.Size(118, 47);
            this.pnlGroup2.TabIndex = 5544;
            this.pnlGroup2.Visible = false;
            this.pnlGroup2.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            // 
            // pnlUOM
            // 
            this.pnlUOM.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.pnlUOM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlUOM.Controls.Add(this.lstUOM);
            this.pnlUOM.Location = new System.Drawing.Point(18, 256);
            this.pnlUOM.Name = "pnlUOM";
            this.pnlUOM.Size = new System.Drawing.Size(118, 50);
            this.pnlUOM.TabIndex = 5545;
            this.pnlUOM.Visible = false;
            this.pnlUOM.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            // 
            // pnlRate
            // 
            this.pnlRate.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.pnlRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRate.Controls.Add(this.lstRate);
            this.pnlRate.Location = new System.Drawing.Point(632, 174);
            this.pnlRate.Name = "pnlRate";
            this.pnlRate.Size = new System.Drawing.Size(118, 70);
            this.pnlRate.TabIndex = 5546;
            this.pnlRate.Visible = false;
            this.pnlRate.VisibleChanged += new System.EventHandler(this.lst_VisibleChanged);
            // 
            // panel4
            // 
            this.panel4.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Location = new System.Drawing.Point(624, 256);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(118, 47);
            this.panel4.TabIndex = 5547;
            this.panel4.Visible = false;
            // 
            // pnlSalePurHistory
            // 
            this.pnlSalePurHistory.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.pnlSalePurHistory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSalePurHistory.Controls.Add(this.dgPurHistory);
            this.pnlSalePurHistory.Controls.Add(this.dgSaleHistory);
            this.pnlSalePurHistory.Location = new System.Drawing.Point(958, 227);
            this.pnlSalePurHistory.Name = "pnlSalePurHistory";
            this.pnlSalePurHistory.Size = new System.Drawing.Size(23, 20);
            this.pnlSalePurHistory.TabIndex = 5548;
            this.pnlSalePurHistory.Visible = false;
            // 
            // dgPurHistory
            // 
            this.dgPurHistory.AllowUserToAddRows = false;
            this.dgPurHistory.AllowUserToDeleteRows = false;
            this.dgPurHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgPurHistory.BackgroundColor = OM.ThemeColor.Panle_Back_Color;
            this.dgPurHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPurHistory.Location = new System.Drawing.Point(337, 8);
            this.dgPurHistory.Name = "dgPurHistory";
            this.dgPurHistory.Size = new System.Drawing.Size(0, 3);
            this.dgPurHistory.TabIndex = 516;
            // 
            // dgSaleHistory
            // 
            this.dgSaleHistory.AllowUserToAddRows = false;
            this.dgSaleHistory.AllowUserToDeleteRows = false;
            this.dgSaleHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgSaleHistory.BackgroundColor = OM.ThemeColor.Panle_Back_Color;
            this.dgSaleHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSaleHistory.Location = new System.Drawing.Point(8, 8);
            this.dgSaleHistory.Name = "dgSaleHistory";
            this.dgSaleHistory.Size = new System.Drawing.Size(3, 3);
            this.dgSaleHistory.TabIndex = 515;
            // 
            // pnlStockGodown
            // 
            this.pnlStockGodown.BackColor = System.Drawing.Color.Transparent;
            this.pnlStockGodown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlStockGodown.Controls.Add(this.txtStockGodwnQty);
            this.pnlStockGodown.Controls.Add(this.dgStockGodown);
            this.pnlStockGodown.Controls.Add(this.btnStkGodownOk);
            this.pnlStockGodown.Controls.Add(this.label41);
            this.pnlStockGodown.Location = new System.Drawing.Point(205, 206);
            this.pnlStockGodown.Name = "pnlStockGodown";
            this.pnlStockGodown.Size = new System.Drawing.Size(306, 203);
            this.pnlStockGodown.TabIndex = 5550;
            this.pnlStockGodown.Visible = false;
            // 
            // txtStockGodwnQty
            // 
            this.txtStockGodwnQty.Enabled = false;
            this.txtStockGodwnQty.Location = new System.Drawing.Point(200, 172);
            this.txtStockGodwnQty.Name = "txtStockGodwnQty";
            this.txtStockGodwnQty.Size = new System.Drawing.Size(100, 20);
            this.txtStockGodwnQty.TabIndex = 708;
            this.txtStockGodwnQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // dgStockGodown
            // 
            this.dgStockGodown.AllowUserToAddRows = false;
            this.dgStockGodown.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgStockGodown.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GodownNo,
            this.GodownName,
            this.Qty,
            this.GActualQty,
            this.PkStockGodownNo});
            this.dgStockGodown.Location = new System.Drawing.Point(3, 16);
            this.dgStockGodown.Name = "dgStockGodown";
            this.dgStockGodown.Size = new System.Drawing.Size(297, 150);
            this.dgStockGodown.TabIndex = 706;
            this.dgStockGodown.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgStockGodown_CellEndEdit);
            this.dgStockGodown.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgStockGodown_KeyDown);
            // 
            // GodownNo
            // 
            this.GodownNo.DataPropertyName = "GodownNo";
            this.GodownNo.HeaderText = "GodownNo";
            this.GodownNo.Name = "GodownNo";
            // 
            // GodownName
            // 
            this.GodownName.DataPropertyName = "GodownName";
            this.GodownName.HeaderText = "GodownName";
            this.GodownName.Name = "GodownName";
            // 
            // Qty
            // 
            this.Qty.DataPropertyName = "Qty";
            this.Qty.HeaderText = "Qty";
            this.Qty.Name = "Qty";
            // 
            // GActualQty
            // 
            this.GActualQty.DataPropertyName = "ActualQty";
            this.GActualQty.HeaderText = "Actual Qty";
            this.GActualQty.Name = "GActualQty";
            // 
            // PkStockGodownNo
            // 
            this.PkStockGodownNo.DataPropertyName = "PkStockGodownNo";
            this.PkStockGodownNo.HeaderText = "PkStockGodownNo";
            this.PkStockGodownNo.Name = "PkStockGodownNo";
            // 
            // btnStkGodownOk
            // 
            this.btnStkGodownOk.Location = new System.Drawing.Point(3, 173);
            this.btnStkGodownOk.Name = "btnStkGodownOk";
            this.btnStkGodownOk.Size = new System.Drawing.Size(75, 23);
            this.btnStkGodownOk.TabIndex = 707;
            this.btnStkGodownOk.Text = "OK";
            this.btnStkGodownOk.UseVisualStyleBackColor = true;
            this.btnStkGodownOk.Click += new System.EventHandler(this.btnStkGodownOk_Click);
            // 
            // label41
            // 
            this.label41.Dock = System.Windows.Forms.DockStyle.Top;
            this.label41.Location = new System.Drawing.Point(0, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(304, 13);
            this.label41.TabIndex = 0;
            this.label41.Text = "Godown Details";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlRateType
            // 
            this.pnlRateType.BackColor = System.Drawing.Color.Transparent;
            this.pnlRateType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRateType.Controls.Add(this.btnRateTypeCancel);
            this.pnlRateType.Controls.Add(this.btnRateTypeOK);
            this.pnlRateType.Controls.Add(this.txtRateTypePassword);
            this.pnlRateType.Controls.Add(this.label40);
            this.pnlRateType.Controls.Add(this.label44);
            this.pnlRateType.Location = new System.Drawing.Point(194, 262);
            this.pnlRateType.Name = "pnlRateType";
            this.pnlRateType.Size = new System.Drawing.Size(409, 104);
            this.pnlRateType.TabIndex = 5552;
            this.pnlRateType.Visible = false;
            // 
            // btnRateTypeCancel
            // 
            this.btnRateTypeCancel.Location = new System.Drawing.Point(209, 64);
            this.btnRateTypeCancel.Name = "btnRateTypeCancel";
            this.btnRateTypeCancel.Size = new System.Drawing.Size(75, 23);
            this.btnRateTypeCancel.TabIndex = 709;
            this.btnRateTypeCancel.Text = "Cancel";
            this.btnRateTypeCancel.UseVisualStyleBackColor = true;
            this.btnRateTypeCancel.Click += new System.EventHandler(this.btnRateTypeCancel_Click);
            // 
            // btnRateTypeOK
            // 
            this.btnRateTypeOK.Location = new System.Drawing.Point(110, 64);
            this.btnRateTypeOK.Name = "btnRateTypeOK";
            this.btnRateTypeOK.Size = new System.Drawing.Size(75, 23);
            this.btnRateTypeOK.TabIndex = 706;
            this.btnRateTypeOK.Text = "OK";
            this.btnRateTypeOK.UseVisualStyleBackColor = true;
            this.btnRateTypeOK.Click += new System.EventHandler(this.btnRateTypeOK_Click);
            // 
            // txtRateTypePassword
            // 
            this.txtRateTypePassword.Location = new System.Drawing.Point(137, 30);
            this.txtRateTypePassword.MaxLength = 6;
            this.txtRateTypePassword.Name = "txtRateTypePassword";
            this.txtRateTypePassword.PasswordChar = '*';
            this.txtRateTypePassword.Size = new System.Drawing.Size(187, 20);
            this.txtRateTypePassword.TabIndex = 703;
            this.txtRateTypePassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRateTypePassword_KeyDown);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(34, 31);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(53, 13);
            this.label40.TabIndex = 3;
            this.label40.Text = "Password";
            // 
            // label44
            // 
            this.label44.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.label44.Dock = System.Windows.Forms.DockStyle.Top;
            this.label44.Location = new System.Drawing.Point(0, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(407, 13);
            this.label44.TabIndex = 0;
            this.label44.Text = "Rate Type Password Details";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.label19.Dock = System.Windows.Forms.DockStyle.Top;
            this.label19.Location = new System.Drawing.Point(0, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(352, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Company Name Selection";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dgCompany
            // 
            this.dgCompany.AllowUserToAddRows = false;
            this.dgCompany.AllowUserToDeleteRows = false;
            this.dgCompany.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgCompany.BackgroundColor = OM.ThemeColor.Panle_Back_Color;
            this.dgCompany.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgCompany.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CompanyNo,
            this.POCompanyName});
            this.dgCompany.Location = new System.Drawing.Point(6, 17);
            this.dgCompany.Name = "dgCompany";
            this.dgCompany.Size = new System.Drawing.Size(341, 83);
            this.dgCompany.TabIndex = 516;
            this.dgCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgCompany_KeyDown);
            // 
            // pnlCompany
            // 
            this.pnlCompany.BackColor = System.Drawing.Color.Transparent;
            this.pnlCompany.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlCompany.Controls.Add(this.dgCompany);
            this.pnlCompany.Controls.Add(this.label19);
            this.pnlCompany.Location = new System.Drawing.Point(157, 210);
            this.pnlCompany.Name = "pnlCompany";
            this.pnlCompany.Size = new System.Drawing.Size(354, 104);
            this.pnlCompany.TabIndex = 5553;
            this.pnlCompany.Visible = false;
            // 
            // txtOtherDisc
            // 
            this.txtOtherDisc.Location = new System.Drawing.Point(696, 7);
            this.txtOtherDisc.MaxLength = 5;
            this.txtOtherDisc.Name = "txtOtherDisc";
            this.txtOtherDisc.Size = new System.Drawing.Size(77, 20);
            this.txtOtherDisc.TabIndex = 3;
            this.txtOtherDisc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOtherDisc.Visible = false;
            this.txtOtherDisc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtOtherDisc_KeyDown);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.Location = new System.Drawing.Point(624, 11);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(54, 13);
            this.label39.TabIndex = 5558;
            this.label39.Text = "Disc %    :";
            this.label39.Visible = false;
            // 
            // btnAdvanceSearch
            // 
            this.btnAdvanceSearch.Location = new System.Drawing.Point(600, 36);
            this.btnAdvanceSearch.Name = "btnAdvanceSearch";
            this.btnAdvanceSearch.Size = new System.Drawing.Size(21, 21);
            this.btnAdvanceSearch.TabIndex = 5559;
            this.btnAdvanceSearch.Text = "..";
            this.btnAdvanceSearch.UseVisualStyleBackColor = true;
            this.btnAdvanceSearch.Click += new System.EventHandler(this.btnAdvanceSearch_Click);
            // 
            // btnShortcut
            // 
            this.btnShortcut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnShortcut.Location = new System.Drawing.Point(224, 476);
            this.btnShortcut.Name = "btnShortcut";
            this.btnShortcut.Size = new System.Drawing.Size(69, 60);
            this.btnShortcut.TabIndex = 5569;
            this.btnShortcut.Text = "( F1 )   Help";
            this.btnShortcut.UseVisualStyleBackColor = true;
            this.btnShortcut.Click += new System.EventHandler(this.btnShortcut_Click);
            // 
            // btnNewCustomer
            // 
            this.btnNewCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnNewCustomer.Location = new System.Drawing.Point(296, 476);
            this.btnNewCustomer.Name = "btnNewCustomer";
            this.btnNewCustomer.Size = new System.Drawing.Size(69, 60);
            this.btnNewCustomer.TabIndex = 5568;
            this.btnNewCustomer.Text = "(N) Party";
            this.btnNewCustomer.UseVisualStyleBackColor = true;
            this.btnNewCustomer.Click += new System.EventHandler(this.btnNewCustomer_Click);
            // 
            // pnlPartySearch
            // 
            this.pnlPartySearch.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.pnlPartySearch.BorderColor = System.Drawing.Color.Empty;
            this.pnlPartySearch.Controls.Add(this.dgPartySearch);
            this.pnlPartySearch.DropShadow = false;
            this.pnlPartySearch.Glossy = false;
            this.pnlPartySearch.LightingColor = System.Drawing.Color.FromArgb(((int)(((byte)(176)))), ((int)(((byte)(186)))), ((int)(((byte)(196)))));
            this.pnlPartySearch.Location = new System.Drawing.Point(131, 157);
            this.pnlPartySearch.Name = "pnlPartySearch";
            this.pnlPartySearch.Radius = 7;
            this.pnlPartySearch.Size = new System.Drawing.Size(475, 329);
            this.pnlPartySearch.TabIndex = 5001;
            this.pnlPartySearch.Visible = false;
            // 
            // dgPartySearch
            // 
            this.dgPartySearch.AllowUserToAddRows = false;
            this.dgPartySearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPartySearch.Location = new System.Drawing.Point(5, 4);
            this.dgPartySearch.Name = "dgPartySearch";
            this.dgPartySearch.ReadOnly = true;
            this.dgPartySearch.Size = new System.Drawing.Size(465, 311);
            this.dgPartySearch.TabIndex = 5002;
            this.dgPartySearch.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgPartySearch_CellFormatting);
            this.dgPartySearch.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPartySearch_CellClick);
            this.dgPartySearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgPartySearch_KeyDown);
            // 
            // pnlSearch
            // 
            this.pnlSearch.BorderColor = System.Drawing.Color.Empty;
            this.pnlSearch.BorderRadius = 3;
            this.pnlSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSearch.Controls.Add(this.rbPartyName);
            this.pnlSearch.Controls.Add(this.rbDocNo);
            this.pnlSearch.Controls.Add(this.rbInvNo);
            this.pnlSearch.Controls.Add(this.cmbPartyNameSearch);
            this.pnlSearch.Controls.Add(this.txtInvNoSearch);
            this.pnlSearch.Controls.Add(this.btnCancelSearch);
            this.pnlSearch.Controls.Add(this.lblLable);
            this.pnlSearch.Controls.Add(this.txtSearch);
            this.pnlSearch.Location = new System.Drawing.Point(76, 333);
            this.pnlSearch.Name = "pnlSearch";
            this.pnlSearch.Size = new System.Drawing.Size(424, 72);
            this.pnlSearch.TabIndex = 509;
            this.pnlSearch.Visible = false;
            // 
            // rbPartyName
            // 
            this.rbPartyName.AutoSize = true;
            this.rbPartyName.BackColor = System.Drawing.Color.Transparent;
            this.rbPartyName.Location = new System.Drawing.Point(203, 9);
            this.rbPartyName.Name = "rbPartyName";
            this.rbPartyName.Size = new System.Drawing.Size(80, 17);
            this.rbPartyName.TabIndex = 2;
            this.rbPartyName.TabStop = true;
            this.rbPartyName.Text = "Party Name";
            this.rbPartyName.UseVisualStyleBackColor = false;
            this.rbPartyName.CheckedChanged += new System.EventHandler(this.rbType_CheckedChanged);
            this.rbPartyName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rbType_KeyDown);
            // 
            // rbDocNo
            // 
            this.rbDocNo.AutoSize = true;
            this.rbDocNo.BackColor = System.Drawing.Color.Transparent;
            this.rbDocNo.Location = new System.Drawing.Point(107, 9);
            this.rbDocNo.Name = "rbDocNo";
            this.rbDocNo.Size = new System.Drawing.Size(62, 17);
            this.rbDocNo.TabIndex = 1;
            this.rbDocNo.TabStop = true;
            this.rbDocNo.Text = "Doc No";
            this.rbDocNo.UseVisualStyleBackColor = false;
            this.rbDocNo.CheckedChanged += new System.EventHandler(this.rbType_CheckedChanged);
            this.rbDocNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rbType_KeyDown);
            // 
            // rbInvNo
            // 
            this.rbInvNo.AutoSize = true;
            this.rbInvNo.BackColor = System.Drawing.Color.Transparent;
            this.rbInvNo.Checked = true;
            this.rbInvNo.Location = new System.Drawing.Point(8, 9);
            this.rbInvNo.Name = "rbInvNo";
            this.rbInvNo.Size = new System.Drawing.Size(57, 17);
            this.rbInvNo.TabIndex = 0;
            this.rbInvNo.TabStop = true;
            this.rbInvNo.Text = "Inv No";
            this.rbInvNo.UseVisualStyleBackColor = false;
            this.rbInvNo.CheckedChanged += new System.EventHandler(this.rbType_CheckedChanged);
            this.rbInvNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rbType_KeyDown);
            // 
            // cmbPartyNameSearch
            // 
            this.cmbPartyNameSearch.FormattingEnabled = true;
            this.cmbPartyNameSearch.Location = new System.Drawing.Point(236, 38);
            this.cmbPartyNameSearch.Name = "cmbPartyNameSearch";
            this.cmbPartyNameSearch.Size = new System.Drawing.Size(139, 21);
            this.cmbPartyNameSearch.TabIndex = 500000000;
            this.cmbPartyNameSearch.Visible = false;
            this.cmbPartyNameSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbPartyNameSearch_KeyDown);
            // 
            // txtInvNoSearch
            // 
            this.txtInvNoSearch.Location = new System.Drawing.Point(153, 39);
            this.txtInvNoSearch.Name = "txtInvNoSearch";
            this.txtInvNoSearch.Size = new System.Drawing.Size(77, 20);
            this.txtInvNoSearch.TabIndex = 50000000;
            this.txtInvNoSearch.Visible = false;
            this.txtInvNoSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtInvNoSearch_KeyDown);
            // 
            // btnCancelSearch
            // 
            this.btnCancelSearch.Location = new System.Drawing.Point(323, 6);
            this.btnCancelSearch.Name = "btnCancelSearch";
            this.btnCancelSearch.Size = new System.Drawing.Size(79, 23);
            this.btnCancelSearch.TabIndex = 4;
            this.btnCancelSearch.Text = "Cancel";
            this.btnCancelSearch.UseVisualStyleBackColor = true;
            this.btnCancelSearch.Click += new System.EventHandler(this.btnCancelSearch_Click);
            // 
            // lblLable
            // 
            this.lblLable.AutoSize = true;
            this.lblLable.BackColor = System.Drawing.Color.Transparent;
            this.lblLable.Location = new System.Drawing.Point(6, 42);
            this.lblLable.Name = "lblLable";
            this.lblLable.Size = new System.Drawing.Size(45, 13);
            this.lblLable.TabIndex = 502;
            this.lblLable.Text = "Inv No :";
            this.lblLable.Visible = false;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(70, 39);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(77, 20);
            this.txtSearch.TabIndex = 500000;
            this.txtSearch.Visible = false;
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            // 
            // pnlAllPrintBarCode
            // 
            this.pnlAllPrintBarCode.BorderRadius = 3;
            this.pnlAllPrintBarCode.Controls.Add(this.dgPrintBarCode);
            this.pnlAllPrintBarCode.Controls.Add(this.rdAllSmallMode);
            this.pnlAllPrintBarCode.Controls.Add(this.rdAllBigMode);
            this.pnlAllPrintBarCode.Controls.Add(this.txtAllStartNo);
            this.pnlAllPrintBarCode.Controls.Add(this.label20);
            this.pnlAllPrintBarCode.Controls.Add(this.btnAllBarcodeCancel);
            this.pnlAllPrintBarCode.Controls.Add(this.btnAllBarcodeOK);
            this.pnlAllPrintBarCode.CornerRadius = 3;
            this.pnlAllPrintBarCode.GradientBottom = System.Drawing.Color.LightGray;
            this.pnlAllPrintBarCode.GradientMiddle = System.Drawing.Color.White;
            this.pnlAllPrintBarCode.GradientShow = true;
            this.pnlAllPrintBarCode.GradientTop = System.Drawing.Color.Silver;
            this.pnlAllPrintBarCode.Location = new System.Drawing.Point(100, 128);
            this.pnlAllPrintBarCode.Name = "pnlAllPrintBarCode";
            this.pnlAllPrintBarCode.Size = new System.Drawing.Size(48, 38);
            this.pnlAllPrintBarCode.TabIndex = 5556;
            this.pnlAllPrintBarCode.Visible = false;
            // 
            // dgPrintBarCode
            // 
            this.dgPrintBarCode.AllowUserToAddRows = false;
            this.dgPrintBarCode.AllowUserToDeleteRows = false;
            this.dgPrintBarCode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgPrintBarCode.BackgroundColor = OM.ThemeColor.Panle_Back_Color;
            this.dgPrintBarCode.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPrintBarCode.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn6,
            this.BarCodeQty,
            this.chkPrint,
            this.FKRateSettingNo});
            this.dgPrintBarCode.Location = new System.Drawing.Point(13, 9);
            this.dgPrintBarCode.Name = "dgPrintBarCode";
            this.dgPrintBarCode.Size = new System.Drawing.Size(330, 267);
            this.dgPrintBarCode.TabIndex = 1000702;
            this.dgPrintBarCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgPrintBarCode_KeyDown);
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "ItemNo";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ToolTipText = "ItemNo";
            this.dataGridViewTextBoxColumn3.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Item Description";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.ToolTipText = "Product Name";
            this.dataGridViewTextBoxColumn6.Width = 200;
            // 
            // BarCodeQty
            // 
            this.BarCodeQty.HeaderText = "Quantity";
            this.BarCodeQty.Name = "BarCodeQty";
            this.BarCodeQty.Width = 50;
            // 
            // chkPrint
            // 
            this.chkPrint.HeaderText = "";
            this.chkPrint.Name = "chkPrint";
            this.chkPrint.Width = 50;
            // 
            // FKRateSettingNo
            // 
            this.FKRateSettingNo.HeaderText = "FKRateSettingNo";
            this.FKRateSettingNo.Name = "FKRateSettingNo";
            this.FKRateSettingNo.Visible = false;
            // 
            // rdAllSmallMode
            // 
            this.rdAllSmallMode.AutoSize = true;
            this.rdAllSmallMode.BackColor = System.Drawing.Color.Transparent;
            this.rdAllSmallMode.Location = new System.Drawing.Point(443, 88);
            this.rdAllSmallMode.Name = "rdAllSmallMode";
            this.rdAllSmallMode.Size = new System.Drawing.Size(80, 17);
            this.rdAllSmallMode.TabIndex = 10008;
            this.rdAllSmallMode.Text = "Small Mode";
            this.rdAllSmallMode.UseVisualStyleBackColor = false;
            // 
            // rdAllBigMode
            // 
            this.rdAllBigMode.AutoSize = true;
            this.rdAllBigMode.BackColor = System.Drawing.Color.Transparent;
            this.rdAllBigMode.Checked = true;
            this.rdAllBigMode.Location = new System.Drawing.Point(352, 89);
            this.rdAllBigMode.Name = "rdAllBigMode";
            this.rdAllBigMode.Size = new System.Drawing.Size(70, 17);
            this.rdAllBigMode.TabIndex = 10007;
            this.rdAllBigMode.TabStop = true;
            this.rdAllBigMode.Text = "Big Mode";
            this.rdAllBigMode.UseVisualStyleBackColor = false;
            // 
            // txtAllStartNo
            // 
            this.txtAllStartNo.Location = new System.Drawing.Point(411, 9);
            this.txtAllStartNo.MaxLength = 5;
            this.txtAllStartNo.Name = "txtAllStartNo";
            this.txtAllStartNo.Size = new System.Drawing.Size(100, 20);
            this.txtAllStartNo.TabIndex = 10004;
            this.txtAllStartNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Location = new System.Drawing.Point(353, 12);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(52, 13);
            this.label20.TabIndex = 1000701;
            this.label20.Text = "Start No :";
            // 
            // btnAllBarcodeCancel
            // 
            this.btnAllBarcodeCancel.Location = new System.Drawing.Point(438, 53);
            this.btnAllBarcodeCancel.Name = "btnAllBarcodeCancel";
            this.btnAllBarcodeCancel.Size = new System.Drawing.Size(73, 24);
            this.btnAllBarcodeCancel.TabIndex = 10006;
            this.btnAllBarcodeCancel.Text = "Cancel";
            this.btnAllBarcodeCancel.UseVisualStyleBackColor = true;
            this.btnAllBarcodeCancel.Click += new System.EventHandler(this.btnAllBarcodeCancel_Click);
            // 
            // btnAllBarcodeOK
            // 
            this.btnAllBarcodeOK.Location = new System.Drawing.Point(360, 54);
            this.btnAllBarcodeOK.Name = "btnAllBarcodeOK";
            this.btnAllBarcodeOK.Size = new System.Drawing.Size(62, 24);
            this.btnAllBarcodeOK.TabIndex = 10005;
            this.btnAllBarcodeOK.Text = "OK";
            this.btnAllBarcodeOK.UseVisualStyleBackColor = true;
            this.btnAllBarcodeOK.Click += new System.EventHandler(this.btnAllBarcodeOK_Click);
            // 
            // pnlBarCodePrint
            // 
            this.pnlBarCodePrint.BorderRadius = 3;
            this.pnlBarCodePrint.Controls.Add(this.rbSmallMode);
            this.pnlBarCodePrint.Controls.Add(this.rbBigMod);
            this.pnlBarCodePrint.Controls.Add(this.txtStartNo);
            this.pnlBarCodePrint.Controls.Add(this.label16);
            this.pnlBarCodePrint.Controls.Add(this.btnCancelPrintBarcode);
            this.pnlBarCodePrint.Controls.Add(this.btnOKPrintBarCode);
            this.pnlBarCodePrint.Controls.Add(this.txtNoOfPrint);
            this.pnlBarCodePrint.Controls.Add(this.label21);
            this.pnlBarCodePrint.CornerRadius = 3;
            this.pnlBarCodePrint.GradientBottom = System.Drawing.Color.LightGray;
            this.pnlBarCodePrint.GradientMiddle = System.Drawing.Color.White;
            this.pnlBarCodePrint.GradientShow = true;
            this.pnlBarCodePrint.GradientTop = System.Drawing.Color.Silver;
            this.pnlBarCodePrint.Location = new System.Drawing.Point(290, 167);
            this.pnlBarCodePrint.Name = "pnlBarCodePrint";
            this.pnlBarCodePrint.Size = new System.Drawing.Size(242, 126);
            this.pnlBarCodePrint.TabIndex = 5553;
            this.pnlBarCodePrint.Visible = false;
            // 
            // rbSmallMode
            // 
            this.rbSmallMode.AutoSize = true;
            this.rbSmallMode.BackColor = System.Drawing.Color.Transparent;
            this.rbSmallMode.Location = new System.Drawing.Point(141, 71);
            this.rbSmallMode.Name = "rbSmallMode";
            this.rbSmallMode.Size = new System.Drawing.Size(80, 17);
            this.rbSmallMode.TabIndex = 10006;
            this.rbSmallMode.Text = "Small Mode";
            this.rbSmallMode.UseVisualStyleBackColor = false;
            // 
            // rbBigMod
            // 
            this.rbBigMod.AutoSize = true;
            this.rbBigMod.BackColor = System.Drawing.Color.Transparent;
            this.rbBigMod.Checked = true;
            this.rbBigMod.Location = new System.Drawing.Point(50, 72);
            this.rbBigMod.Name = "rbBigMod";
            this.rbBigMod.Size = new System.Drawing.Size(70, 17);
            this.rbBigMod.TabIndex = 10005;
            this.rbBigMod.TabStop = true;
            this.rbBigMod.Text = "Big Mode";
            this.rbBigMod.UseVisualStyleBackColor = false;
            // 
            // txtStartNo
            // 
            this.txtStartNo.Location = new System.Drawing.Point(114, 43);
            this.txtStartNo.MaxLength = 5;
            this.txtStartNo.Name = "txtStartNo";
            this.txtStartNo.Size = new System.Drawing.Size(100, 20);
            this.txtStartNo.TabIndex = 10004;
            this.txtStartNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Location = new System.Drawing.Point(7, 46);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(52, 13);
            this.label16.TabIndex = 1000701;
            this.label16.Text = "Start No :";
            // 
            // btnCancelPrintBarcode
            // 
            this.btnCancelPrintBarcode.Location = new System.Drawing.Point(115, 94);
            this.btnCancelPrintBarcode.Name = "btnCancelPrintBarcode";
            this.btnCancelPrintBarcode.Size = new System.Drawing.Size(73, 24);
            this.btnCancelPrintBarcode.TabIndex = 10008;
            this.btnCancelPrintBarcode.Text = "Cancel";
            this.btnCancelPrintBarcode.UseVisualStyleBackColor = true;
            this.btnCancelPrintBarcode.Click += new System.EventHandler(this.btnCancelPrintBarcode_Click);
            // 
            // btnOKPrintBarCode
            // 
            this.btnOKPrintBarCode.Location = new System.Drawing.Point(37, 95);
            this.btnOKPrintBarCode.Name = "btnOKPrintBarCode";
            this.btnOKPrintBarCode.Size = new System.Drawing.Size(62, 24);
            this.btnOKPrintBarCode.TabIndex = 10007;
            this.btnOKPrintBarCode.Text = "OK";
            this.btnOKPrintBarCode.UseVisualStyleBackColor = true;
            this.btnOKPrintBarCode.Click += new System.EventHandler(this.btnOKPrintBarCode_Click);
            // 
            // txtNoOfPrint
            // 
            this.txtNoOfPrint.Location = new System.Drawing.Point(115, 16);
            this.txtNoOfPrint.MaxLength = 5;
            this.txtNoOfPrint.Name = "txtNoOfPrint";
            this.txtNoOfPrint.Size = new System.Drawing.Size(100, 20);
            this.txtNoOfPrint.TabIndex = 10003;
            this.txtNoOfPrint.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Location = new System.Drawing.Point(8, 19);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(65, 13);
            this.label21.TabIndex = 1000201;
            this.label21.Text = "No Of Print :";
            // 
            // lblMsg
            // 
            this.lblMsg.BackColor = OM.ThemeColor.Panle_Back_Color;
            this.lblMsg.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMsg.CornerRadius = 3;
            this.lblMsg.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg.ForeColor = System.Drawing.Color.Maroon;
            this.lblMsg.GradientBottom = OM.ThemeColor.Panle_Back_Color;
            this.lblMsg.GradientMiddle = System.Drawing.Color.White;
            this.lblMsg.GradientShow = true;
            this.lblMsg.GradientTop = OM.ThemeColor.Panle_Back_Color;
            this.lblMsg.Location = new System.Drawing.Point(121, 254);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(481, 52);
            this.lblMsg.TabIndex = 505;
            this.lblMsg.Text = "label4";
            this.lblMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMsg.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label36);
            this.panel2.Controls.Add(this.txtTotalAnotherDisc);
            this.panel2.Controls.Add(this.lblExchange);
            this.panel2.Controls.Add(this.lblCompName);
            this.panel2.Controls.Add(this.lblBilExchangeItem);
            this.panel2.Controls.Add(this.ombPanel1);
            this.panel2.Controls.Add(this.lblBillItem);
            this.panel2.Controls.Add(this.label34);
            this.panel2.Controls.Add(this.txtTotalChrgs);
            this.panel2.Location = new System.Drawing.Point(505, 577);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(252, 147);
            this.panel2.TabIndex = 5571;
            this.panel2.Visible = false;
            // 
            // btnMixMode
            // 
            this.btnMixMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnMixMode.Location = new System.Drawing.Point(442, 476);
            this.btnMixMode.Name = "btnMixMode";
            this.btnMixMode.Size = new System.Drawing.Size(69, 60);
            this.btnMixMode.TabIndex = 123457;
            this.btnMixMode.Text = "Mix\r\nMode";
            this.btnMixMode.UseVisualStyleBackColor = true;
            this.btnMixMode.Visible = false;
            this.btnMixMode.Click += new System.EventHandler(this.btnMixMode_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Location = new System.Drawing.Point(784, 42);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 13);
            this.label15.TabIndex = 123459;
            this.label15.Text = "Location :";
            this.label15.Visible = false;
            // 
            // cmbLocation
            // 
            this.cmbLocation.FormattingEnabled = true;
            this.cmbLocation.Location = new System.Drawing.Point(858, 38);
            this.cmbLocation.Name = "cmbLocation";
            this.cmbLocation.Size = new System.Drawing.Size(112, 21);
            this.cmbLocation.TabIndex = 123458;
            this.cmbLocation.Visible = false;
            this.cmbLocation.Leave += new System.EventHandler(this.cmbLocation_Leave);
            this.cmbLocation.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbLocation_KeyDown);
            // 
            // chkFilterItem
            // 
            this.chkFilterItem.AutoSize = true;
            this.chkFilterItem.BackColor = System.Drawing.Color.Transparent;
            this.chkFilterItem.Location = new System.Drawing.Point(470, 7);
            this.chkFilterItem.Name = "chkFilterItem";
            this.chkFilterItem.Size = new System.Drawing.Size(142, 17);
            this.chkFilterItem.TabIndex = 123460;
            this.chkFilterItem.Text = "All Mfg. Company(Ctrl+E)";
            this.chkFilterItem.UseVisualStyleBackColor = false;
            this.chkFilterItem.CheckedChanged += new System.EventHandler(this.chkFilterItem_CheckedChanged);
            // 
            // pnlInvSearch
            // 
            this.pnlInvSearch.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.pnlInvSearch.BorderColor = System.Drawing.Color.Empty;
            this.pnlInvSearch.Controls.Add(this.dgInvSearch);
            this.pnlInvSearch.DropShadow = false;
            this.pnlInvSearch.Glossy = false;
            this.pnlInvSearch.LightingColor = System.Drawing.Color.FromArgb(((int)(((byte)(176)))), ((int)(((byte)(186)))), ((int)(((byte)(196)))));
            this.pnlInvSearch.Location = new System.Drawing.Point(525, 148);
            this.pnlInvSearch.Name = "pnlInvSearch";
            this.pnlInvSearch.Radius = 7;
            this.pnlInvSearch.Size = new System.Drawing.Size(475, 329);
            this.pnlInvSearch.TabIndex = 5003;
            this.pnlInvSearch.Visible = false;
            // 
            // dgInvSearch
            // 
            this.dgInvSearch.AllowUserToAddRows = false;
            this.dgInvSearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgInvSearch.Location = new System.Drawing.Point(5, 4);
            this.dgInvSearch.Name = "dgInvSearch";
            this.dgInvSearch.ReadOnly = true;
            this.dgInvSearch.Size = new System.Drawing.Size(465, 311);
            this.dgInvSearch.TabIndex = 5002;
            this.dgInvSearch.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgInvSearch_CellFormatting);
            this.dgInvSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgInvSearch_KeyDown);
            // 
            // btnAdvanceItemSelection
            // 
            this.btnAdvanceItemSelection.Location = new System.Drawing.Point(794, 5);
            this.btnAdvanceItemSelection.Name = "btnAdvanceItemSelection";
            this.btnAdvanceItemSelection.Size = new System.Drawing.Size(214, 23);
            this.btnAdvanceItemSelection.TabIndex = 123461;
            this.btnAdvanceItemSelection.Text = "Advance Item Selection";
            this.btnAdvanceItemSelection.UseVisualStyleBackColor = true;
            this.btnAdvanceItemSelection.Visible = false;
            this.btnAdvanceItemSelection.Click += new System.EventHandler(this.btnAdvanceItemSelection_Click);
            // 
            // rdBtnAll
            // 
            this.rdBtnAll.AutoSize = true;
            this.rdBtnAll.Location = new System.Drawing.Point(6, 4);
            this.rdBtnAll.Name = "rdBtnAll";
            this.rdBtnAll.Size = new System.Drawing.Size(36, 17);
            this.rdBtnAll.TabIndex = 123462;
            this.rdBtnAll.TabStop = true;
            this.rdBtnAll.Text = "All";
            this.rdBtnAll.UseVisualStyleBackColor = true;
            // 
            // rdBtnMin
            // 
            this.rdBtnMin.AutoSize = true;
            this.rdBtnMin.Location = new System.Drawing.Point(52, 4);
            this.rdBtnMin.Name = "rdBtnMin";
            this.rdBtnMin.Size = new System.Drawing.Size(128, 17);
            this.rdBtnMin.TabIndex = 123463;
            this.rdBtnMin.TabStop = true;
            this.rdBtnMin.Text = "Stock less then 0/min";
            this.rdBtnMin.UseVisualStyleBackColor = true;
            // 
            // pnlSelect
            // 
            this.pnlSelect.BackColor = System.Drawing.Color.Transparent;
            this.pnlSelect.Controls.Add(this.rdBtnAll);
            this.pnlSelect.Controls.Add(this.rdBtnMin);
            this.pnlSelect.Location = new System.Drawing.Point(806, 36);
            this.pnlSelect.Name = "pnlSelect";
            this.pnlSelect.Size = new System.Drawing.Size(198, 24);
            this.pnlSelect.TabIndex = 123464;
            // 
            // pnlActive
            // 
            this.pnlActive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlActive.BorderColor = System.Drawing.Color.Gray;
            this.pnlActive.BorderRadius = 3;
            this.pnlActive.Controls.Add(this.lblSchemeStatus);
            this.pnlActive.Location = new System.Drawing.Point(6, 566);
            this.pnlActive.Name = "pnlActive";
            this.pnlActive.Size = new System.Drawing.Size(499, 31);
            this.pnlActive.TabIndex = 123465;
            // 
            // lblSchemeStatus
            // 
            this.lblSchemeStatus.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblSchemeStatus.Font = new System.Drawing.Font("Verdana", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSchemeStatus.Location = new System.Drawing.Point(3, 3);
            this.lblSchemeStatus.Name = "lblSchemeStatus";
            this.lblSchemeStatus.Size = new System.Drawing.Size(493, 22);
            this.lblSchemeStatus.TabIndex = 10031;
            this.lblSchemeStatus.Text = "Active";
            this.lblSchemeStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnOrderClosed
            // 
            this.btnOrderClosed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOrderClosed.Location = new System.Drawing.Point(591, 475);
            this.btnOrderClosed.Name = "btnOrderClosed";
            this.btnOrderClosed.Size = new System.Drawing.Size(69, 60);
            this.btnOrderClosed.TabIndex = 123466;
            this.btnOrderClosed.Text = "Order Closed";
            this.btnOrderClosed.UseVisualStyleBackColor = true;
            this.btnOrderClosed.Click += new System.EventHandler(this.btnOrderClosed_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPrint.Location = new System.Drawing.Point(441, 476);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(69, 60);
            this.btnPrint.TabIndex = 123467;
            this.btnPrint.Text = "PO. Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Visible = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // CompanyNo
            // 
            this.CompanyNo.DataPropertyName = "CompanyNo";
            this.CompanyNo.HeaderText = "CompanyNo";
            this.CompanyNo.Name = "CompanyNo";
            this.CompanyNo.Visible = false;
            // 
            // POCompanyName
            // 
            this.POCompanyName.DataPropertyName = "CompanyName";
            this.POCompanyName.HeaderText = "Name";
            this.POCompanyName.Name = "POCompanyName";
            this.POCompanyName.Width = 295;
            // 
            // PurchaseOrderAE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 694);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnOrderClosed);
            this.Controls.Add(this.pnlActive);
            this.Controls.Add(this.pnlSelect);
            this.Controls.Add(this.pnlItemName);
            this.Controls.Add(this.btnAdvanceItemSelection);
            this.Controls.Add(this.pnlSearch);
            this.Controls.Add(this.pnlInvSearch);
            this.Controls.Add(this.chkFilterItem);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.cmbLocation);
            this.Controls.Add(this.pnlPartySearch);
            this.Controls.Add(this.btnMixMode);
            this.Controls.Add(this.txtRemark);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pnlTotalAmt);
            this.Controls.Add(this.txtTotalItemDisc);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.btnShortcut);
            this.Controls.Add(this.btnNewCustomer);
            this.Controls.Add(this.cmbPaymentType);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.pnlRate);
            this.Controls.Add(this.btnAdvanceSearch);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.txtRefNo);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.txtOtherDisc);
            this.Controls.Add(this.pnlAllPrintBarCode);
            this.Controls.Add(this.pnlCompany);
            this.Controls.Add(this.pnlBarCodePrint);
            this.Controls.Add(this.pnlRateType);
            this.Controls.Add(this.pnlPartial);
            this.Controls.Add(this.pnlStockGodown);
            this.Controls.Add(this.pnlSalePurHistory);
            this.Controls.Add(this.pnlUOM);
            this.Controls.Add(this.pnlGroup2);
            this.Controls.Add(this.pnlGroup1);
            this.Controls.Add(this.lblRateType);
            this.Controls.Add(this.cmbRateType);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cmbTaxType);
            this.Controls.Add(this.cmbPartyName);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.pnlParking);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.pnlFooterInfo);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnPrev);
            this.Controls.Add(this.btnLast);
            this.Controls.Add(this.btnFirst);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.dgBill);
            this.Controls.Add(this.dtpBillTime);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtpBillDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtInvNo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnDelete);
            this.Name = "PurchaseOrderAE";
            this.Text = "Purchase Order";
            this.Deactivate += new System.EventHandler(this.PurchaseAE_Deactivate);
            this.Load += new System.EventHandler(this.PurchaseAE_Load);
            this.Activated += new System.EventHandler(this.PurchaseAE_Activated);
            this.pnlTotalAmt.ResumeLayout(false);
            this.pnlTotalAmt.PerformLayout();
            this.ombPanel1.ResumeLayout(false);
            this.ombPanel1.PerformLayout();
            this.pnlFooterInfo.ResumeLayout(false);
            this.pnlFooterInfo.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgBill)).EndInit();
            this.pnlPartial.ResumeLayout(false);
            this.pnlPartial.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPayCreditCardDetails)).EndInit();
            this.pnlBranch.ResumeLayout(false);
            this.pnlBank.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgPayChqDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgPayType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgParkingBills)).EndInit();
            this.pnlParking.ResumeLayout(false);
            this.pnlItemName.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgItemList)).EndInit();
            this.pnlGroup1.ResumeLayout(false);
            this.pnlGroup2.ResumeLayout(false);
            this.pnlUOM.ResumeLayout(false);
            this.pnlRate.ResumeLayout(false);
            this.pnlSalePurHistory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgPurHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgSaleHistory)).EndInit();
            this.pnlStockGodown.ResumeLayout(false);
            this.pnlStockGodown.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgStockGodown)).EndInit();
            this.pnlRateType.ResumeLayout(false);
            this.pnlRateType.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCompany)).EndInit();
            this.pnlCompany.ResumeLayout(false);
            this.pnlPartySearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgPartySearch)).EndInit();
            this.pnlSearch.ResumeLayout(false);
            this.pnlSearch.PerformLayout();
            this.pnlAllPrintBarCode.ResumeLayout(false);
            this.pnlAllPrintBarCode.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPrintBarCode)).EndInit();
            this.pnlBarCodePrint.ResumeLayout(false);
            this.pnlBarCodePrint.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.pnlInvSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgInvSearch)).EndInit();
            this.pnlSelect.ResumeLayout(false);
            this.pnlSelect.PerformLayout();
            this.pnlActive.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtInvNo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpBillDate;
        private System.Windows.Forms.DateTimePicker dtpBillTime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel pnlTotalAmt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtGrandTotal;
        private System.Windows.Forms.TextBox txtTotalTax;
        private System.Windows.Forms.TextBox txtTotalDisc;
        private System.Windows.Forms.TextBox txtSubTotal;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblBillItem;
        private System.Windows.Forms.Label lblBilExchangeItem;
        private System.Windows.Forms.Label lblChrg1;
        private System.Windows.Forms.Label lblDisc1;
        private System.Windows.Forms.ComboBox cmbPaymentType;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.Button btnLast;
        private System.Windows.Forms.Button btnFirst;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Panel pnlFooterInfo;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtLastBillAmt;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label lblExchange;
        private System.Windows.Forms.DataGridView dgBill;
        private System.Windows.Forms.Button btnSearch;
        private JitControls.OMLabel lblMsg;
        private System.Windows.Forms.Panel pnlPartial;
        private System.Windows.Forms.TextBox txtTotalAmt;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.DataGridView dgParkingBills;
        private System.Windows.Forms.DataGridViewTextBoxColumn BillNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn BillAmount;
        private System.Windows.Forms.Panel pnlParking;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnCancel;
        private JitControls.OMBPanel pnlSearch;
        private System.Windows.Forms.Button btnCancelSearch;
        private System.Windows.Forms.Label lblLable;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.TextBox txtRemark;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Panel pnlItemName;
        private System.Windows.Forms.DataGridView dgItemList;
        private System.Windows.Forms.ListBox lstUOM;
        private System.Windows.Forms.ListBox lstRate;
        private System.Windows.Forms.ListBox lstGroup1;
        private System.Windows.Forms.ListBox lstGroup2;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.ComboBox cmbPartyName;
        private System.Windows.Forms.TextBox txtDiscRupees1;
        private System.Windows.Forms.TextBox txtRoundOff;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtTotalChrgs;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtChrgRupees1;
        private System.Windows.Forms.TextBox txtTotalAnotherDisc;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtlastBillQty;
        private System.Windows.Forms.TextBox txtLastPayment;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.DataGridView dgPayType;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn PKVoucherPayTypeNo;
        private System.Windows.Forms.Label lblPayTypeBal;
        private System.Windows.Forms.ComboBox cmbTaxType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblRateType;
        private System.Windows.Forms.ComboBox cmbRateType;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Panel pnlGroup1;
        private System.Windows.Forms.Panel pnlGroup2;
        private System.Windows.Forms.Panel pnlUOM;
        private System.Windows.Forms.Panel pnlRate;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel pnlSalePurHistory;
        private System.Windows.Forms.DataGridView dgSaleHistory;
        private System.Windows.Forms.DataGridView dgPurHistory;
        private System.Windows.Forms.Panel pnlStockGodown;
        private System.Windows.Forms.TextBox txtStockGodwnQty;
        private System.Windows.Forms.DataGridView dgStockGodown;
        private System.Windows.Forms.Button btnStkGodownOk;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.DataGridViewTextBoxColumn GodownNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn GodownName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Qty;
        private System.Windows.Forms.DataGridViewTextBoxColumn GActualQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkStockGodownNo;
        private System.Windows.Forms.DataGridView dgPayChqDetails;
        private System.Windows.Forms.Panel pnlBank;
        private System.Windows.Forms.ListBox lstBank;
        private System.Windows.Forms.Panel pnlBranch;
        private System.Windows.Forms.ListBox lstBranch;
        private System.Windows.Forms.MonthCalendar dtpChqDate;
        private System.Windows.Forms.DataGridView dgPayCreditCardDetails;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Panel pnlRateType;
        private System.Windows.Forms.Button btnRateTypeCancel;
        private System.Windows.Forms.Button btnRateTypeOK;
        private System.Windows.Forms.TextBox txtRateTypePassword;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label44;
        private JitControls.OMPanel pnlBarCodePrint;
        private System.Windows.Forms.TextBox txtStartNo;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btnCancelPrintBarcode;
        private System.Windows.Forms.Button btnOKPrintBarCode;
        private System.Windows.Forms.TextBox txtNoOfPrint;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.RadioButton rbSmallMode;
        private System.Windows.Forms.RadioButton rbBigMod;
        private System.Windows.Forms.Label lblCompName;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.DataGridView dgCompany;
        private System.Windows.Forms.Panel pnlCompany;
        private System.Windows.Forms.TextBox txtTotalItemDisc;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnAllBarCodePrint;
        private JitControls.OMPanel pnlAllPrintBarCode;
        private System.Windows.Forms.RadioButton rdAllSmallMode;
        private System.Windows.Forms.RadioButton rdAllBigMode;
        private System.Windows.Forms.TextBox txtAllStartNo;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button btnAllBarcodeCancel;
        private System.Windows.Forms.Button btnAllBarcodeOK;
        private System.Windows.Forms.DataGridView dgPrintBarCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn BarCodeQty;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chkPrint;
        private System.Windows.Forms.DataGridViewTextBoxColumn FKRateSettingNo;
        private System.Windows.Forms.TextBox txtOtherDisc;
        private System.Windows.Forms.TextBox txtRefNo;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox cmbPartyNameSearch;
        private System.Windows.Forms.TextBox txtInvNoSearch;
        private JitControls.OMGPanel pnlPartySearch;
        private System.Windows.Forms.DataGridView dgPartySearch;
        private System.Windows.Forms.RadioButton rbPartyName;
        private System.Windows.Forms.RadioButton rbDocNo;
        private System.Windows.Forms.RadioButton rbInvNo;
        private System.Windows.Forms.Button btnAdvanceSearch;
        private System.Windows.Forms.Button btnShortcut;
        private System.Windows.Forms.Button btnNewCustomer;
        private JitControls.OMBPanel ombPanel1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblLastBillAmt;
        private System.Windows.Forms.Label lblLastBillQty;
        private System.Windows.Forms.Label lblGrandTotal;
        private System.Windows.Forms.Label lblLastPayment;
        private System.Windows.Forms.TextBox txtDistDisc;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtVisibility;
        private System.Windows.Forms.TextBox txtReturnAmt;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ListBox lstGroup1Lang;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.DataGridViewTextBoxColumn CreditCardNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn ChequeNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ChequeDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn BankName;
        private System.Windows.Forms.DataGridViewTextBoxColumn BranchName;
        private System.Windows.Forms.DataGridViewTextBoxColumn AmountChq;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkSrNoChq;
        private System.Windows.Forms.DataGridViewTextBoxColumn BankNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn BranchNo;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button btnMixMode;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cmbLocation;
        private System.Windows.Forms.CheckBox chkFilterItem;
        private System.Windows.Forms.Label label43;
        private JitControls.OMGPanel pnlInvSearch;
        private System.Windows.Forms.DataGridView dgInvSearch;
        private System.Windows.Forms.TextBox txtOtherTax;
        private System.Windows.Forms.Label lblOtherTax;
        private System.Windows.Forms.Button btnAdvanceItemSelection;
        private System.Windows.Forms.RadioButton rdBtnAll;
        private System.Windows.Forms.RadioButton rdBtnMin;
        private System.Windows.Forms.Panel pnlSelect;
        private System.Windows.Forms.Button btnOk1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rate;
        private System.Windows.Forms.DataGridViewTextBoxColumn MRP;
        private System.Windows.Forms.DataGridViewTextBoxColumn NetRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn FreeQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn FreeUom;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscPerce;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscAmt;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscRupees;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscPercentage2;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscAmount2;
        private System.Windows.Forms.DataGridViewTextBoxColumn NetAmt;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaxPerce;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaxAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiscRupees2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Barcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkStockTrnNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkSrNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkVoucherNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaxLedgerNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn SalesLedgerNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkRateSettingNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PkItemTaxInfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn StockFactor;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActualQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn MKTQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn SalesVchNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaxVchNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn StockCompanyNo;
        private System.Windows.Forms.DataGridViewButtonColumn BarCodePrinting;
        private System.Windows.Forms.DataGridViewTextBoxColumn FreeUomNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn TempMRP;
        private System.Windows.Forms.DataGridViewTextBoxColumn LandedRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn BalanceQty;
        private JitControls.OMBPanel pnlActive;
        private System.Windows.Forms.Label lblSchemeStatus;
        private System.Windows.Forms.Button btnOrderClosed;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.DataGridViewTextBoxColumn iItemNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn iItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemNameLang;
        private System.Windows.Forms.DataGridViewTextBoxColumn Qty0;
        private System.Windows.Forms.DataGridViewTextBoxColumn iRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn iUOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn iMRP;
        private System.Windows.Forms.DataGridViewTextBoxColumn iStock;
        private System.Windows.Forms.DataGridViewTextBoxColumn MinLevel;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaxLevel;
        private System.Windows.Forms.DataGridViewTextBoxColumn iUOMStk;
        private System.Windows.Forms.DataGridViewTextBoxColumn iSaleTax;
        private System.Windows.Forms.DataGridViewTextBoxColumn iPurTax;
        private System.Windows.Forms.DataGridViewTextBoxColumn iCompany;
        private System.Windows.Forms.DataGridViewTextBoxColumn iBarcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn iRateSettingNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn UOMDefault;
        private System.Windows.Forms.DataGridViewTextBoxColumn PurRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn SuggMin;
        private System.Windows.Forms.DataGridViewTextBoxColumn SugMax;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn POCompanyName;
    }
}