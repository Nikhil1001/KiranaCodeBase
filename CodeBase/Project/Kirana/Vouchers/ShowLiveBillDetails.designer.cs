﻿namespace Kirana.Vouchers
{
    partial class ShowLiveBillDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlsaleBill = new JitControls.OMBPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtInvNo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbLedgerName = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnShow = new System.Windows.Forms.Button();
            this.DTPFromDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.chkSelectAll = new System.Windows.Forms.CheckBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.dgDetailes = new System.Windows.Forms.DataGridView();
            this.SrNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BillNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BillDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LedgerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PayType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BilledAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BillPrint = new System.Windows.Forms.DataGridViewButtonColumn();
            this.ChkSelect = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.pnlDetails = new JitControls.OMBPanel();
            this.lblNetB = new System.Windows.Forms.Label();
            this.lblCrNet = new System.Windows.Forms.Label();
            this.lblDrBal = new System.Windows.Forms.Label();
            this.lblN = new System.Windows.Forms.Label();
            this.lblC = new System.Windows.Forms.Label();
            this.lbld = new System.Windows.Forms.Label();
            this.BtnSave = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.GridView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BillAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotRec = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NetBal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Remark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnShowCollection = new System.Windows.Forms.Button();
            this.cmbPartyName = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpVoucherDate = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.btnPrint = new System.Windows.Forms.Button();
            this.pnlsaleBill.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDetailes)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.pnlDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridView)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlsaleBill
            // 
            this.pnlsaleBill.BorderColor = System.Drawing.Color.Gray;
            this.pnlsaleBill.BorderRadius = 3;
            this.pnlsaleBill.Controls.Add(this.panel1);
            this.pnlsaleBill.Controls.Add(this.chkSelectAll);
            this.pnlsaleBill.Controls.Add(this.btnExit);
            this.pnlsaleBill.Controls.Add(this.btnDelete);
            this.pnlsaleBill.Controls.Add(this.dgDetailes);
            this.pnlsaleBill.Location = new System.Drawing.Point(2, 3);
            this.pnlsaleBill.Name = "pnlsaleBill";
            this.pnlsaleBill.Size = new System.Drawing.Size(788, 507);
            this.pnlsaleBill.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.txtInvNo);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.cmbLedgerName);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.btnShow);
            this.panel1.Controls.Add(this.DTPFromDate);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(6, 8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(754, 71);
            this.panel1.TabIndex = 513;
            // 
            // txtInvNo
            // 
            this.txtInvNo.Location = new System.Drawing.Point(354, 5);
            this.txtInvNo.Name = "txtInvNo";
            this.txtInvNo.Size = new System.Drawing.Size(131, 20);
            this.txtInvNo.TabIndex = 504;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(255, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 16);
            this.label3.TabIndex = 503;
            this.label3.Text = "Bill No:";
            // 
            // cmbLedgerName
            // 
            this.cmbLedgerName.FormattingEnabled = true;
            this.cmbLedgerName.Location = new System.Drawing.Point(104, 34);
            this.cmbLedgerName.Name = "cmbLedgerName";
            this.cmbLedgerName.Size = new System.Drawing.Size(381, 21);
            this.cmbLedgerName.TabIndex = 502;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 13);
            this.label4.TabIndex = 501;
            this.label4.Text = "Ledger Name :";
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(616, 6);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(123, 52);
            this.btnShow.TabIndex = 105;
            this.btnShow.Text = "Show";
            this.btnShow.UseVisualStyleBackColor = false;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // DTPFromDate
            // 
            this.DTPFromDate.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTPFromDate.Location = new System.Drawing.Point(104, 3);
            this.DTPFromDate.Name = "DTPFromDate";
            this.DTPFromDate.Size = new System.Drawing.Size(131, 23);
            this.DTPFromDate.TabIndex = 101;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 16);
            this.label1.TabIndex = 103;
            this.label1.Text = "From Date :";
            // 
            // chkSelectAll
            // 
            this.chkSelectAll.AutoSize = true;
            this.chkSelectAll.Location = new System.Drawing.Point(676, 434);
            this.chkSelectAll.Name = "chkSelectAll";
            this.chkSelectAll.Size = new System.Drawing.Size(88, 17);
            this.chkSelectAll.TabIndex = 112;
            this.chkSelectAll.Text = "SelectAll (F2)";
            this.chkSelectAll.UseVisualStyleBackColor = true;
            this.chkSelectAll.CheckedChanged += new System.EventHandler(this.chkSelectAll_CheckedChanged);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(107, 434);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(93, 46);
            this.btnExit.TabIndex = 108;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(8, 433);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(93, 46);
            this.btnDelete.TabIndex = 107;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // dgDetailes
            // 
            this.dgDetailes.AllowUserToAddRows = false;
            this.dgDetailes.AllowUserToDeleteRows = false;
            this.dgDetailes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDetailes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SrNo,
            this.BillNo,
            this.BillDate,
            this.LedgerName,
            this.PayType,
            this.BilledAmount,
            this.BillPrint,
            this.ChkSelect,
            this.ColIndex});
            this.dgDetailes.Location = new System.Drawing.Point(6, 82);
            this.dgDetailes.Name = "dgDetailes";
            this.dgDetailes.Size = new System.Drawing.Size(754, 348);
            this.dgDetailes.TabIndex = 106;
            this.dgDetailes.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgDetailes_CellFormatting);
            this.dgDetailes.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgDetailes_CellContentClick);
            // 
            // SrNo
            // 
            this.SrNo.HeaderText = "SrNo";
            this.SrNo.Name = "SrNo";
            this.SrNo.ReadOnly = true;
            this.SrNo.Width = 40;
            // 
            // BillNo
            // 
            this.BillNo.HeaderText = "BillNo";
            this.BillNo.Name = "BillNo";
            this.BillNo.ReadOnly = true;
            this.BillNo.Width = 70;
            // 
            // BillDate
            // 
            this.BillDate.HeaderText = "BillDate";
            this.BillDate.Name = "BillDate";
            this.BillDate.ReadOnly = true;
            // 
            // LedgerName
            // 
            this.LedgerName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.LedgerName.HeaderText = "LedgerName";
            this.LedgerName.Name = "LedgerName";
            this.LedgerName.ReadOnly = true;
            // 
            // PayType
            // 
            this.PayType.HeaderText = "PayType";
            this.PayType.Name = "PayType";
            this.PayType.ReadOnly = true;
            this.PayType.Width = 90;
            // 
            // BilledAmount
            // 
            this.BilledAmount.HeaderText = "BilledAmount";
            this.BilledAmount.Name = "BilledAmount";
            this.BilledAmount.ReadOnly = true;
            // 
            // BillPrint
            // 
            this.BillPrint.HeaderText = "Print";
            this.BillPrint.Name = "BillPrint";
            this.BillPrint.Width = 80;
            // 
            // ChkSelect
            // 
            this.ChkSelect.HeaderText = "Select";
            this.ChkSelect.Name = "ChkSelect";
            this.ChkSelect.Width = 60;
            // 
            // ColIndex
            // 
            this.ColIndex.HeaderText = "ColIndex";
            this.ColIndex.Name = "ColIndex";
            this.ColIndex.Visible = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(800, 533);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.pnlsaleBill);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(792, 507);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Sale Bill";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnPrint);
            this.tabPage2.Controls.Add(this.txtAmount);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.pnlDetails);
            this.tabPage2.Controls.Add(this.BtnSave);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.btnCancel);
            this.tabPage2.Controls.Add(this.GridView);
            this.tabPage2.Controls.Add(this.panel2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(792, 507);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Bill Collection";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtAmount
            // 
            this.txtAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtAmount.Location = new System.Drawing.Point(80, 401);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(165, 20);
            this.txtAmount.TabIndex = 5569;
            this.txtAmount.TextChanged += new System.EventHandler(this.txtAmount_TextChanged);
            this.txtAmount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAmount_KeyDown);
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(7, 402);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 15);
            this.label14.TabIndex = 5568;
            this.label14.Text = "Amount";
            // 
            // pnlDetails
            // 
            this.pnlDetails.BorderColor = System.Drawing.Color.Gray;
            this.pnlDetails.BorderRadius = 3;
            this.pnlDetails.Controls.Add(this.lblNetB);
            this.pnlDetails.Controls.Add(this.lblCrNet);
            this.pnlDetails.Controls.Add(this.lblDrBal);
            this.pnlDetails.Controls.Add(this.lblN);
            this.pnlDetails.Controls.Add(this.lblC);
            this.pnlDetails.Controls.Add(this.lbld);
            this.pnlDetails.Location = new System.Drawing.Point(537, 395);
            this.pnlDetails.Name = "pnlDetails";
            this.pnlDetails.Size = new System.Drawing.Size(248, 107);
            this.pnlDetails.TabIndex = 5567;
            // 
            // lblNetB
            // 
            this.lblNetB.Location = new System.Drawing.Point(108, 72);
            this.lblNetB.Name = "lblNetB";
            this.lblNetB.Size = new System.Drawing.Size(100, 22);
            this.lblNetB.TabIndex = 7;
            this.lblNetB.Text = "0.00";
            this.lblNetB.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblCrNet
            // 
            this.lblCrNet.Location = new System.Drawing.Point(108, 42);
            this.lblCrNet.Name = "lblCrNet";
            this.lblCrNet.Size = new System.Drawing.Size(100, 22);
            this.lblCrNet.TabIndex = 6;
            this.lblCrNet.Text = "0.00";
            this.lblCrNet.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblDrBal
            // 
            this.lblDrBal.Location = new System.Drawing.Point(108, 11);
            this.lblDrBal.Name = "lblDrBal";
            this.lblDrBal.Size = new System.Drawing.Size(100, 22);
            this.lblDrBal.TabIndex = 5;
            this.lblDrBal.Text = "0.00";
            this.lblDrBal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblN
            // 
            this.lblN.AutoSize = true;
            this.lblN.Location = new System.Drawing.Point(12, 71);
            this.lblN.Name = "lblN";
            this.lblN.Size = new System.Drawing.Size(51, 13);
            this.lblN.TabIndex = 3;
            this.lblN.Text = "Net Bal  :";
            // 
            // lblC
            // 
            this.lblC.AutoSize = true;
            this.lblC.Location = new System.Drawing.Point(12, 39);
            this.lblC.Name = "lblC";
            this.lblC.Size = new System.Drawing.Size(53, 13);
            this.lblC.TabIndex = 2;
            this.lblC.Text = "Receipt  :";
            // 
            // lbld
            // 
            this.lbld.AutoSize = true;
            this.lbld.Location = new System.Drawing.Point(12, 9);
            this.lbld.Name = "lbld";
            this.lbld.Size = new System.Drawing.Size(51, 13);
            this.lbld.TabIndex = 1;
            this.lbld.Text = "Sales     :";
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(6, 440);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(71, 60);
            this.BtnSave.TabIndex = 7;
            this.BtnSave.Text = "Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(154, 440);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(71, 60);
            this.button1.TabIndex = 8;
            this.button1.Text = "Exit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(80, 440);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(71, 60);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // GridView
            // 
            this.GridView.AllowUserToAddRows = false;
            this.GridView.AllowUserToDeleteRows = false;
            this.GridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.dataGridViewTextBoxColumn1,
            this.BillAmt,
            this.TotRec,
            this.NetBal,
            this.Amount,
            this.Remark});
            this.GridView.Location = new System.Drawing.Point(6, 72);
            this.GridView.Name = "GridView";
            this.GridView.Size = new System.Drawing.Size(780, 318);
            this.GridView.TabIndex = 2;
            this.GridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridView_CellEndEdit);
            this.GridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GridView_KeyDown);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "VoucherUserNo";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle6;
            this.Column1.HeaderText = "BillNo";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 90;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "VoucherDate";
            this.dataGridViewTextBoxColumn1.HeaderText = "BillDate";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // BillAmt
            // 
            this.BillAmt.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BillAmt.DataPropertyName = "Debit";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.BillAmt.DefaultCellStyle = dataGridViewCellStyle7;
            this.BillAmt.HeaderText = "BillAmt";
            this.BillAmt.Name = "BillAmt";
            this.BillAmt.ReadOnly = true;
            // 
            // TotRec
            // 
            this.TotRec.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TotRec.DataPropertyName = "TotRec";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.TotRec.DefaultCellStyle = dataGridViewCellStyle8;
            this.TotRec.HeaderText = "TotRec";
            this.TotRec.Name = "TotRec";
            this.TotRec.ReadOnly = true;
            // 
            // NetBal
            // 
            this.NetBal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NetBal.DataPropertyName = "NetBal";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.NetBal.DefaultCellStyle = dataGridViewCellStyle9;
            this.NetBal.HeaderText = "NetBal";
            this.NetBal.Name = "NetBal";
            this.NetBal.ReadOnly = true;
            // 
            // Amount
            // 
            this.Amount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Amount.DataPropertyName = "Amount";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Amount.DefaultCellStyle = dataGridViewCellStyle10;
            this.Amount.HeaderText = "Amount";
            this.Amount.Name = "Amount";
            // 
            // Remark
            // 
            this.Remark.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Remark.HeaderText = "Remark";
            this.Remark.Name = "Remark";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnShowCollection);
            this.panel2.Controls.Add(this.cmbPartyName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.dtpVoucherDate);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Location = new System.Drawing.Point(6, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(780, 60);
            this.panel2.TabIndex = 1;
            // 
            // btnShowCollection
            // 
            this.btnShowCollection.Location = new System.Drawing.Point(689, 5);
            this.btnShowCollection.Name = "btnShowCollection";
            this.btnShowCollection.Size = new System.Drawing.Size(86, 51);
            this.btnShowCollection.TabIndex = 54;
            this.btnShowCollection.Text = "Show";
            this.btnShowCollection.UseVisualStyleBackColor = true;
            this.btnShowCollection.Click += new System.EventHandler(this.btnShowCollection_Click);
            // 
            // cmbPartyName
            // 
            this.cmbPartyName.FormattingEnabled = true;
            this.cmbPartyName.Location = new System.Drawing.Point(74, 30);
            this.cmbPartyName.Name = "cmbPartyName";
            this.cmbPartyName.Size = new System.Drawing.Size(552, 21);
            this.cmbPartyName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 53;
            this.label2.Text = "Party :";
            // 
            // dtpVoucherDate
            // 
            this.dtpVoucherDate.Location = new System.Drawing.Point(74, 4);
            this.dtpVoucherDate.Name = "dtpVoucherDate";
            this.dtpVoucherDate.Size = new System.Drawing.Size(149, 20);
            this.dtpVoucherDate.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 52;
            this.label5.Text = "Date :";
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(231, 441);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(71, 60);
            this.btnPrint.TabIndex = 5570;
            this.btnPrint.Text = "Details\r\nPrint";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // ShowLiveBillDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(824, 535);
            this.Controls.Add(this.tabControl1);
            this.Name = "ShowLiveBillDetails";
            this.Text = "Show Bills";
            this.Load += new System.EventHandler(this.ShowLiveBillDetails_Load);
            this.pnlsaleBill.ResumeLayout(false);
            this.pnlsaleBill.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDetailes)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.pnlDetails.ResumeLayout(false);
            this.pnlDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridView)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private JitControls.OMBPanel pnlsaleBill;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.DateTimePicker DTPFromDate;
        private System.Windows.Forms.DataGridView dgDetailes;
        internal System.Windows.Forms.Button btnShow;
        internal System.Windows.Forms.Button btnExit;
        internal System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.CheckBox chkSelectAll;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtInvNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbLedgerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn SrNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn BillNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn BillDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn LedgerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PayType;
        private System.Windows.Forms.DataGridViewTextBoxColumn BilledAmount;
        private System.Windows.Forms.DataGridViewButtonColumn BillPrint;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ChkSelect;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColIndex;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cmbPartyName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpVoucherDate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView GridView;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnShowCollection;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn BillAmt;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotRec;
        private System.Windows.Forms.DataGridViewTextBoxColumn NetBal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remark;
        private JitControls.OMBPanel pnlDetails;
        private System.Windows.Forms.Label lblNetB;
        private System.Windows.Forms.Label lblCrNet;
        private System.Windows.Forms.Label lblDrBal;
        private System.Windows.Forms.Label lblN;
        private System.Windows.Forms.Label lblC;
        private System.Windows.Forms.Label lbld;
        private System.Windows.Forms.TextBox txtAmount;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnPrint;
    }
}