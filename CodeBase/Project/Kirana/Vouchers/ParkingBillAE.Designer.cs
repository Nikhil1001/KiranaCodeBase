﻿namespace Kirana.Vouchers
{
    partial class ParkingBillAE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlParking = new System.Windows.Forms.Panel();
            this.lblMsg = new System.Windows.Forms.Label();
            this.lblSearchName = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.dgParkingBills = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BillDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BillTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParkingBillNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Remark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlParking.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgParkingBills)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlParking
            // 
            this.pnlParking.Controls.Add(this.lblMsg);
            this.pnlParking.Controls.Add(this.lblSearchName);
            this.pnlParking.Controls.Add(this.label37);
            this.pnlParking.Controls.Add(this.dgParkingBills);
            this.pnlParking.Location = new System.Drawing.Point(2, 2);
            this.pnlParking.Name = "pnlParking";
            this.pnlParking.Size = new System.Drawing.Size(825, 421);
            this.pnlParking.TabIndex = 5555;
            // 
            // lblMsg
            // 
            this.lblMsg.BackColor = System.Drawing.Color.Coral;
            this.lblMsg.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMsg.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMsg.ForeColor = System.Drawing.Color.Maroon;
            this.lblMsg.Location = new System.Drawing.Point(162, 177);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(481, 52);
            this.lblMsg.TabIndex = 123458;
            this.lblMsg.Text = "label4";
            this.lblMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMsg.Visible = false;
            // 
            // lblSearchName
            // 
            this.lblSearchName.BackColor = System.Drawing.Color.Coral;
            this.lblSearchName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSearchName.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblSearchName.ForeColor = System.Drawing.Color.White;
            this.lblSearchName.Location = new System.Drawing.Point(0, 0);
            this.lblSearchName.Name = "lblSearchName";
            this.lblSearchName.Size = new System.Drawing.Size(825, 23);
            this.lblSearchName.TabIndex = 123457;
            this.lblSearchName.Text = "Parking Bill";
            this.lblSearchName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label37
            // 
            this.label37.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(7, 402);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(147, 13);
            this.label37.TabIndex = 1;
            this.label37.Text = "Parking Bill Cancel(Ctrl+Alt+C)";
            // 
            // dgParkingBills
            // 
            this.dgParkingBills.AllowUserToAddRows = false;
            this.dgParkingBills.AllowUserToDeleteRows = false;
            this.dgParkingBills.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgParkingBills.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgParkingBills.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn6,
            this.ItemQty,
            this.BillDate,
            this.BillTime,
            this.ParkingBillNo,
            this.Remark});
            this.dgParkingBills.Location = new System.Drawing.Point(5, 26);
            this.dgParkingBills.Name = "dgParkingBills";
            this.dgParkingBills.ReadOnly = true;
            this.dgParkingBills.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgParkingBills.Size = new System.Drawing.Size(817, 373);
            this.dgParkingBills.TabIndex = 0;
            this.dgParkingBills.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgParkingBills_CellFormatting);
            this.dgParkingBills.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgParkingBills_KeyDown);
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "BillNo";
            this.dataGridViewTextBoxColumn3.HeaderText = "BillNo";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 50;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Name";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewTextBoxColumn6.HeaderText = "Person Name";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // ItemQty
            // 
            this.ItemQty.DataPropertyName = "ItemQty";
            this.ItemQty.HeaderText = "Qty";
            this.ItemQty.Name = "ItemQty";
            this.ItemQty.ReadOnly = true;
            // 
            // BillDate
            // 
            this.BillDate.DataPropertyName = "BillDate";
            this.BillDate.HeaderText = "BillDate";
            this.BillDate.Name = "BillDate";
            this.BillDate.ReadOnly = true;
            // 
            // BillTime
            // 
            this.BillTime.DataPropertyName = "BillTime";
            this.BillTime.HeaderText = "BillTime";
            this.BillTime.Name = "BillTime";
            this.BillTime.ReadOnly = true;
            // 
            // ParkingBillNo
            // 
            this.ParkingBillNo.DataPropertyName = "ParkingBillNo";
            this.ParkingBillNo.HeaderText = "ParkingBillNo";
            this.ParkingBillNo.Name = "ParkingBillNo";
            this.ParkingBillNo.ReadOnly = true;
            this.ParkingBillNo.Visible = false;
            // 
            // Remark
            // 
            this.Remark.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Remark.DataPropertyName = "Remark";
            this.Remark.HeaderText = "Remark";
            this.Remark.Name = "Remark";
            this.Remark.ReadOnly = true;
            // 
            // ParkingBillAE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 425);
            this.Controls.Add(this.pnlParking);
            this.Name = "ParkingBillAE";
            this.Text = "Parking Bill";
            this.Shown += new System.EventHandler(this.ParkingBillAE_Shown);
            this.pnlParking.ResumeLayout(false);
            this.pnlParking.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgParkingBills)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlParking;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.DataGridView dgParkingBills;
        private System.Windows.Forms.Label lblSearchName;
        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn BillDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn BillTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParkingBillNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remark;
    }
}