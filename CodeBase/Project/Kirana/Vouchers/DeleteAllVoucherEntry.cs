﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using JitControls;
using OM;


namespace Kirana.Vouchers
{
    public partial class DeleteAllVoucherEntry : Form
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();


        DBTVaucherEntry dbTVoucherEntry = new DBTVaucherEntry();
        TVoucherPayTypeDetails tVchPayTypeDetails = new TVoucherPayTypeDetails();
        TVoucherDetails tVoucherDetails = new TVoucherDetails();
        TVoucherEntry tVoucherEntry = new TVoucherEntry();

        public long VoucherType;
        public DeleteAllVoucherEntry()
        {
            InitializeComponent();
        }
        public DeleteAllVoucherEntry(long vchType)
        {
            InitializeComponent();
            VoucherType = vchType;
        }

        private void DeleteAllVoucherEntry_Load(object sender, EventArgs e)
        {
            DateTime dtFrom = ObjQry.ReturnDate("Select IsNull(Min(VoucherDate),'01-Jan-1900') From TVoucherEntry where VouchertypeCode=" + VoucherType + "", CommonFunctions.ConStr);
            DTPFromDate.Text = dtFrom.ToString("dd-MMM-yyyy");
            DTToDate.Text = DBGetVal.ServerTime.ToString("dd-MMM-yyyy");
            DTToDate.MinDate = DTPFromDate.Value;
            KeyDownFormat(this.Controls);
        }

        #region KeyDown Events
        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                chkSelectAll.Checked = !chkSelectAll.Checked;

                for (int i = 0; i < dgDetailes.Rows.Count; i++)
                {
                    dgDetailes.Rows[i].Cells[6].Value = chkSelectAll.Checked;
                }
                btnDelete.Focus();
            }


        }

        public void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else
                    KeyDownFormat(ctrl.Controls);
            }
        }
        #endregion

        private void DTPFromDate_ValueChanged(object sender, EventArgs e)
        {
            DTToDate.MinDate = DTPFromDate.Value;
        }

        public void BindGrid()
        {
            string sql = "";
            chkSelectAll.Checked = false;
            while (dgDetailes.Rows.Count > 0)
                dgDetailes.Rows.RemoveAt(0);


            sql = "SELECT 0 AS SrNo, TVoucherEntry_1.VoucherUserNo AS BillNo, TVoucherEntry_1.VoucherDate AS BillDate, TVoucherEntry_1.BilledAmount AS Amount, MLedger.LedgerName,  MPayType.PayTypeName AS PayType, 'false' AS Chk, TVoucherEntry_1.PkVoucherNo, MLedger.LedgerNo, TVoucherEntry_1.IsCancel, " +
                " ISNULL ((SELECT     (CASE WHEN (amt = Amount) THEN 0 ELSE 1 END) AS chk FROM         (SELECT     (SELECT     ISNULL(SUM(Amount), 0) AS Expr1 FROM         TVoucherRefDetails WHERE     (RefNo = TVoucherRefDetails_1.RefNo) AND (TypeOfRef IN (2, 5))) AS amt, TVoucherRefDetails_1.Amount FROM  TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN TVoucherRefDetails AS TVoucherRefDetails_1 ON TVoucherDetails.PkVoucherTrnNo = TVoucherRefDetails_1.FkVoucherTrnNo WHERE      (TVoucherDetails.LedgerNo = MLedger.LedgerNo) AND (TVoucherEntry.VoucherTypeCode = "+VoucherType+") AND (TVoucherRefDetails_1.TypeOfRef = 3)  AND (TVoucherEntry.CompanyNo = 1) AND (TVoucherEntry.PkVoucherNo = TVoucherEntry_1.PkVoucherNo) AND  (TVoucherEntry.IsCancel = 'false')) AS tbl1), 0) AS chk " +
                " FROM  MLedger INNER JOIN TVoucherDetails AS TVoucherDetails_1 ON MLedger.LedgerNo = TVoucherDetails_1.LedgerNo INNER JOIN TVoucherEntry AS TVoucherEntry_1 ON TVoucherDetails_1.FkVoucherNo = TVoucherEntry_1.PkVoucherNo INNER JOIN " +
                " MPayType ON TVoucherEntry_1.PayTypeNo = MPayType.PKPayTypeNo " +
                
                " WHERE (TVoucherDetails_1.SrNo = 501) AND (TVoucherDetails_1.VoucherSrNo = 1) AND (TVoucherEntry_1.VoucherTypeCode = " + VoucherType + ") And " +
                "(TVoucherEntry_1.VoucherDate >= '" + Convert.ToDateTime(DTPFromDate.Text).ToString("dd-MMM-yyyy") + "')" +
                " And TVoucherEntry_1.CompanyNo=" + DBGetVal.CompanyNo + " AND (TVoucherEntry_1.VoucherDate <='" + Convert.ToDateTime(DTToDate.Text).ToString("dd-MMM-yyyy") + "') " +
                " and TVoucherEntry_1.PayTypeNo=" + ((rbAll.Checked == true) ? "TVoucherEntry_1.PayTypeNo" : ((rbCash.Checked == true) ? "2" : "3")) + "    " +
                " Order by MLedger.LedgerName,TVoucherEntry_1.VoucherDate ";


            //sql = " SELECT 0 AS SrNo, TVoucherEntry.VoucherUserNo AS BillNo, TVoucherEntry.VoucherDate AS BillDate, TVoucherEntry.BilledAmount AS Amount, MLedger.LedgerName,MPayType.PayTypeName AS PayType,'false' as Chk, TVoucherEntry.PkVoucherNo,MLedger.LedgerNo ,TVoucherEntry.IsCancel" +
            //     ", ISNull((SELECT  (case When(amt=Amount) then 0 else 1 End) as chk from (SELECT(select IsNull(sum(TVoucherRefDetails.Amount),0) from TVoucherRefDetails where RefNo=TVoucherRefDetails.RefNo and TypeOfRef in (2,5)) as amt,TVoucherRefDetails.Amount FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN TVoucherRefDetails ON TVoucherDetails.PkVoucherTrnNo = TVoucherRefDetails.FkVoucherTrnNo WHERE (TVoucherDetails.LedgerNo = MLedger.LedgerNo) AND (TVoucherEntry.VoucherTypeCode = " + VoucherType + ") AND (TVoucherRefDetails.TypeOfRef = 3) AND (TVoucherEntry.CompanyNo = 1) AND  (TVoucherEntry.IsCancel = 'false')) as tbl1),0) as chk " +

            //    " FROM MLedger INNER JOIN TVoucherDetails ON MLedger.LedgerNo = TVoucherDetails.LedgerNo INNER JOIN TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN MPayType ON TVoucherEntry.PayTypeNo = MPayType.PKPayTypeNo " +

            //    " WHERE (TVoucherDetails_1.SrNo = 501) AND (TVoucherDetails_1.VoucherSrNo = 1) AND (TVoucherEntry_1.VoucherTypeCode = " + VoucherType + ") And " +
            //    "(TVoucherEntry_1.VoucherDate >= '" + Convert.ToDateTime(DTPFromDate.Text).ToString("dd-MMM-yyyy") + "')" +
            //    " And TVoucherEntry_1.CompanyNo=" + DBGetVal.CompanyNo + " AND (TVoucherEntry_1.VoucherDate <='" + Convert.ToDateTime(DTToDate.Text).ToString("dd-MMM-yyyy") + "') " +
            //    " and TVoucherEntry_1.PayTypeNo=" + ((rbAll.Checked == true) ? "TVoucherEntry_1.PayTypeNo" : ((rbCash.Checked == true) ? "2" : "3")) + "    " +
            //    " Order by MLedger.LedgerName,TVoucherEntry_1.VoucherDate ";

            if (sql != "")
            {
                DataTable dt = ObjFunction.GetDataView(sql).Table;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dgDetailes.Rows.Add();
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        dgDetailes.Rows[i].Cells[j].Value = dt.Rows[i].ItemArray[j].ToString();

                        if (j == 10)
                        {
                            if (Convert.ToInt32(dgDetailes.Rows[i].Cells[10].Value) == 1)
                                dgDetailes.Rows[i].DefaultCellStyle.BackColor = Color.Red;
                            else
                                dgDetailes.Rows[i].DefaultCellStyle.BackColor = Color.White;
                        }
                    }
                    
                }

            }
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            //string sql = "";
            //chkSelectAll.Checked = false;
            //while (dgDetailes.Rows.Count > 0)
            //    dgDetailes.Rows.RemoveAt(0);


            //sql = " SELECT 0 AS SrNo, TVoucherEntry.VoucherUserNo AS BillNo, TVoucherEntry.VoucherDate AS BillDate, TVoucherEntry.BilledAmount AS Amount, MLedger.LedgerName,MPayType.PayTypeName AS PayType,'false' as Chk, TVoucherEntry.PkVoucherNo,MLedger.LedgerNo ,TVoucherEntry.IsCancel" +
            //    " FROM MLedger INNER JOIN TVoucherDetails ON MLedger.LedgerNo = TVoucherDetails.LedgerNo INNER JOIN TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN MPayType ON TVoucherEntry.PayTypeNo = MPayType.PKPayTypeNo " +
            //    " WHERE (TVoucherDetails.SrNo = 501) AND (TVoucherDetails.VoucherSrNo = 1) AND (TVoucherEntry.VoucherTypeCode = " + VoucherType + ") And " +
            //    "(TVoucherEntry.VoucherDate >= '" + Convert.ToDateTime(DTPFromDate.Text).ToString("dd-MMM-yyyy") + "')" +
            //    " And TVoucherEntry.CompanyNo=" + DBGetVal.CompanyNo + " AND (TVoucherEntry.VoucherDate <='" + Convert.ToDateTime(DTToDate.Text).ToString("dd-MMM-yyyy") + "') " +
            //    " Order by MLedger.LedgerName,TVoucherEntry.VoucherDate ";

            //if (sql != "")
            //{
            //    DataTable dt = ObjFunction.GetDataView(sql).Table;
            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        dgDetailes.Rows.Add();
            //        for (int j = 0; j < dt.Columns.Count; j++)
            //        {
            //            dgDetailes.Rows[i].Cells[j].Value = dt.Rows[i].ItemArray[j].ToString();
            //        }

            //    }

            //}
            BindGrid();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {

            long OpStockNo = 0;
            pnlProgress.Visible = true;
            PGB.Value = 0;
            PGB.Maximum = dgDetailes.Rows.Count;

            if (VoucherType == VchType.Sales)
                OpStockNo = ObjQry.ReturnLong("Select Max(PkVoucherNo) From TVoucherEntry Where VoucherTypeCode=" + VchType.StockOutward + "", CommonFunctions.ConStr);
            else if (VoucherType == VchType.Purchase)
                OpStockNo = ObjQry.ReturnLong("Select Max(PkVoucherNo) From TVoucherEntry Where VoucherTypeCode=" + VchType.StockInward + "", CommonFunctions.ConStr);

            if (OpStockNo == 0)
            {
                if (VoucherType == VchType.Sales)
                {
                    ObjTrans.Execute("Insert Into TVoucherEntry( VoucherTypeCode,VoucherUserNo,VoucherDate,VoucherTime,Narration,Reference,ChequeNo,ClearingDate,CompanyNo,BilledAmount,ChallanNo,Remark,InwardLocationCode,MacNo,IsCancel,PayTypeNo,RateTypeNo,TaxTypeNo,IsVoucherLock,VoucherStatus,UserID,UserDate,ModifiedBy,OrderType,ReturnAmount,Visibility,DiscPercent,DiscAmt,StatusNo,MixMode,IsItemLevelDisc,IsFooterLevelDisc,HamaliRs ) Values   ( " + VchType.StockOutward + ",1, '01-Jan-1900 12:00:00' , '07-May-2015 09:48:51' , 'Sales Bill' , NULL,0, '01-Jan-1900 12:00:00' ,1,0.00, NULL, NULL,0,1, 'False' ,0,6,0, 'False' ,1,1, '07-May-2015 12:00:00' , NULL,0,0.00,0.00,0.00,0.00,1,0, 'False' , 'False' ,0.00 )", CommonFunctions.ConStr);
                    OpStockNo = ObjQry.ReturnLong("Select Max(PkVoucherNo) From TVoucherEntry Where VoucherTypeCode=0", CommonFunctions.ConStr);
                }
                else if (VoucherType == VchType.Purchase)
                {
                    ObjTrans.Execute("Insert Into TVoucherEntry( VoucherTypeCode,VoucherUserNo,VoucherDate,VoucherTime,Narration,Reference,ChequeNo,ClearingDate,CompanyNo,BilledAmount,ChallanNo,Remark,InwardLocationCode,MacNo,IsCancel,PayTypeNo,RateTypeNo,TaxTypeNo,IsVoucherLock,VoucherStatus,UserID,UserDate,ModifiedBy,OrderType,ReturnAmount,Visibility,DiscPercent,DiscAmt,StatusNo,MixMode,IsItemLevelDisc,IsFooterLevelDisc,HamaliRs ) Values   ( " + VchType.StockInward + ",1, '01-Jan-1900 12:00:00' , '07-May-2015 09:48:51' , 'Sales Bill' , NULL,0, '01-Jan-1900 12:00:00' ,1,0.00, NULL, NULL,0,1, 'False' ,0,6,0, 'False' ,1,1, '07-May-2015 12:00:00' , NULL,0,0.00,0.00,0.00,0.00,1,0, 'False' , 'False' ,0.00 )", CommonFunctions.ConStr);
                    OpStockNo = ObjQry.ReturnLong("Select Max(PkVoucherNo) From TVoucherEntry Where VoucherTypeCode=0", CommonFunctions.ConStr);
                }

            }
            if (OpStockNo != 0)
            {
                lblPbText.Text = "Please Wait.....";
                Application.DoEvents();
                for (int i = 0; i < dgDetailes.Rows.Count; i++)
                {



                    if (Convert.ToBoolean(dgDetailes.Rows[i].Cells[6].EditedFormattedValue.ToString()) == true)
                    {
                        lblPbText.Text = "Bill Deleting Please Wait........";
                        Application.DoEvents();
                        long SalesID = Convert.ToInt64(dgDetailes.Rows[i].Cells[7].EditedFormattedValue.ToString());
                        long CompNo = DBGetVal.CompanyNo;
                        DateTime BillDate = Convert.ToDateTime(dgDetailes.Rows[i].Cells[2].EditedFormattedValue.ToString()).Date;
                        long PartyLedgerNo = Convert.ToInt64(dgDetailes.Rows[i].Cells[8].EditedFormattedValue.ToString());

                        if (Convert.ToBoolean(dgDetailes.Rows[i].Cells[9].EditedFormattedValue.ToString()) == false)
                        {
                            if (VoucherType == VchType.Sales)
                            {

                                dbTVoucherEntry = new DBTVaucherEntry();
                                DataTable dtref = ObjFunction.GetDataView("SELECT PkRefTrnNo , FkVoucherTrnNo , RefNo , Amount FROM TVoucherRefDetails WHERE     (RefNo IN (SELECT     RefNo FROM          TVoucherRefDetails AS TVoucherRefDetails_1 WHERE  (FkVoucherTrnNo IN (SELECT     PkVoucherTrnNo  FROM TVoucherDetails WHERE (FkVoucherNo = " + SalesID + "))) AND (TypeOfRef = 3))) AND (TypeOfRef <> 3) ").Table;
                                for (int s = 0; s < dtref.Rows.Count; s++)
                                {
                                    DataTable dtdetails = ObjFunction.GetDataView("Select  PkVoucherTrnNo, FkVoucherNo, Debit, Credit from  TVoucherDetails Where FkVoucherNo  in( Select FkVoucherNo from TVoucherDetails Where PkVoucherTrnNo in(" + dtref.Rows[s].ItemArray[1].ToString() + "))").Table;
                                    for (int d = 0; d < dtdetails.Rows.Count; d++)
                                    {
                                        if (Convert.ToDouble(dtref.Rows[s].ItemArray[3].ToString()) == (Convert.ToDouble(dtdetails.Rows[d].ItemArray[2].ToString()) + Convert.ToDouble(dtdetails.Rows[d].ItemArray[3].ToString())))
                                        {
                                            ObjTrans.Execute("Exec DeleteAllVoucherEntry " + Convert.ToInt64(dtdetails.Rows[d].ItemArray[1].ToString()), CommonFunctions.ConStr);
                                            break;
                                        }
                                        else
                                        {
                                            if (d == 0)
                                            {
                                                ObjTrans.Execute("update TVoucherEntry Set BilledAmount=BilledAmount-" + dtref.Rows[s].ItemArray[3].ToString() + " Where PkVoucherNo= " + dtdetails.Rows[d].ItemArray[1].ToString(), CommonFunctions.ConStr);
                                                double amt = Convert.ToDouble(dtdetails.Rows[d].ItemArray[3].ToString()) - Convert.ToDouble(dtref.Rows[s].ItemArray[3].ToString());
                                                ObjTrans.Execute("Update TVoucherDetails set Debit=0, Credit=" + amt + " where PKVoucherTrnNo=" + dtdetails.Rows[d].ItemArray[0].ToString() + "", CommonFunctions.ConStr);
                                            }
                                            else
                                            {
                                                double amt = Convert.ToDouble(dtdetails.Rows[d].ItemArray[2].ToString()) - Convert.ToDouble(dtref.Rows[s].ItemArray[3].ToString());

                                                ObjTrans.Execute("Update TVoucherDetails set Credit=0, Debit=" + amt + " where PKVoucherTrnNo=" + dtdetails.Rows[d].ItemArray[0].ToString() + "", CommonFunctions.ConStr);
                                            }
                                        }

                                    }
                                    ObjTrans.Execute("Delete From TVoucherRefDetails Where PkRefTrnNo =" + dtref.Rows[s].ItemArray[0].ToString(), CommonFunctions.ConStr);
                                }


                                dbTVoucherEntry.UpdateReceiptDetails(SalesID, CompNo, BillDate, PartyLedgerNo, VchType.Receipt, VchType.Sales, OpStockNo, 0);
                            }
                            else if (VoucherType == VchType.Purchase)
                                dbTVoucherEntry.UpdateReceiptDetails(SalesID, CompNo, BillDate, PartyLedgerNo, VchType.Payment, VchType.Purchase, OpStockNo, 0);
                        }
                        else
                        {
                            if (VoucherType == VchType.Sales)
                                dbTVoucherEntry.UpdateReceiptDetails(SalesID, CompNo, BillDate, PartyLedgerNo, VchType.Receipt, VchType.Sales, OpStockNo, 1);
                            else if (VoucherType == VchType.Purchase)
                                dbTVoucherEntry.UpdateReceiptDetails(SalesID, CompNo, BillDate, PartyLedgerNo, VchType.Payment, VchType.Purchase, OpStockNo, 1);

                        }
                    }
                    PGB.Value += 1;
                }
                pnlProgress.Visible = false;
                btnShow_Click(sender, new EventArgs());
            }
        }

        private void dgDetailes_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                e.Value = e.RowIndex + 1;
            }
            if (e.ColumnIndex == 2)
            {
                e.Value = Convert.ToDateTime(e.Value).ToString("dd-MMM-yyyy");
            }
            
        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < dgDetailes.Rows.Count; i++)
            {
                dgDetailes.Rows[i].Cells[6].Value = chkSelectAll.Checked;
            }
        }

        private void txtLedgerName_TextChanged(object sender, EventArgs e)
        {

            SearchGridValue(dgDetailes, txtLedgerName.Text.Replace("'", "''"));

        }

        private void SearchGridValue(DataGridView dg, string strSearch)
        {
            int i = 0, cnt = 0;
            try
            {
                if (strSearch != "")
                {
                    for (i = 0; i < dg.Rows.Count; i++)
                    {
                        dg.Rows[i].DefaultCellStyle.BackColor = Color.White;
                        cnt = 0;
                        for (int j = 0; j < strSearch.Trim().ToUpper().Length; j++)
                        {
                            if (dg.Rows[i].Cells[4].Value != null)
                            {
                                if (strSearch.Trim().ToUpper()[j].ToString() == dg.Rows[i].Cells[4].Value.ToString().Trim().ToUpper()[j].ToString())
                                    cnt++;
                                else break;
                            }
                        }
                        if (cnt == strSearch.Trim().ToUpper().Length)
                        {
                            dg.CurrentCell = dg.Rows[i].Cells[1];
                            dg.Rows[i].DefaultCellStyle.BackColor = CommonFunctions.RowColor;
                        }
                        cnt = 0;
                    }
                }
                else
                {
                    for (i = 0; i < dg.Rows.Count; i++)
                    {
                        dg.Rows[i].DefaultCellStyle.BackColor = Color.White;
                    }
                }
            }
            catch (Exception exc)
            {
                ObjFunction.ExceptionDisplay(exc.Message);
            }

        }

        private void rb_CheckedChanged(object sender, EventArgs e)
        {
            BindGrid();
        }


    }
}
