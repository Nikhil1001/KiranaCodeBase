﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Data;
using System.Data.SqlClient;
using JitControls;
using Newtonsoft.Json;

namespace OM
{
    public class DBMStockItems
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        public CommandCollection commandcollection = new CommandCollection();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        public static string strerrormsg;

        private List<UCMStockItems> GetStockItems()
        {
            string Sql = " SELECT     MStockItems.ItemNo, MStockItems.ItemName, MStockItems.GroupNo , MStockGroup.StockGroupName, " +
                       " MUOM.UOMName, MRateSetting.MRP, MRateSetting.BSaleRate,MRateSetting.ASaleRate,MRateSetting.CSaleRate, MItemTaxInfo.PkSrNo As TaxNo,  " +
                       " MItemTaxInfo.Percentage As TaxPer, MStockBarcode.Barcode, MStockItems.ShortCode, MStockItems.ShowItemName , " +
                       " MItemTaxInfo.HSNCode, MItemTaxInfo.IGSTPercent, MItemTaxInfo.CGSTPercent, MItemTaxInfo.SGSTPercent,MItemTaxInfo.CessPercent " +
                       " FROM         MStockItems INNER JOIN " +
                       " MStockBarcode ON MStockItems.ItemNo = MStockBarcode.ItemNo INNER JOIN " +
                       " MRateSetting ON MStockItems.ItemNo = MRateSetting.ItemNo INNER JOIN " +
                       " MItemTaxInfo ON MStockItems.ItemNo = MItemTaxInfo.ItemNo INNER JOIN " +
                       " MUOM ON MStockItems.UOMDefault = MUOM.UOMNo INNER JOIN " +
                       " MStockGroup ON MStockItems.GroupNo = MStockGroup.StockGroupNo " +
                       " WHERE   (MStockItems.StatusNo <> 3)  And  (MStockItems.IsActive = 'true') AND (MItemTaxInfo.TaxTypeNo = 38) AND (MItemTaxInfo.TransactionTypeNo = 10) AND (MRateSetting.IsActive = 'True') ";
            DataTable dt = ObjFunction.GetDataView(Sql).Table;
            List<UCMStockItems> ucItems = new List<UCMStockItems>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                UCMStockItems items = new UCMStockItems();
                items.itemno = Convert.ToInt64(dt.Rows[i]["ItemNo"].ToString());
                items.itemname = dt.Rows[i]["ItemName"].ToString();
                items.brandname = dt.Rows[i]["StockGroupName"].ToString();
                items.uom = dt.Rows[i]["UOMName"].ToString();
                items.mrp = Convert.ToDouble(dt.Rows[i]["MRP"].ToString());
                items.arate = Convert.ToDouble(dt.Rows[i]["ASaleRate"].ToString());
                items.brate = Convert.ToDouble(dt.Rows[i]["BSaleRate"].ToString());
                items.crate = Convert.ToDouble(dt.Rows[i]["CSaleRate"].ToString());
                items.taxrate = Convert.ToDouble(dt.Rows[i]["TaxPer"].ToString());
                items.barcode = dt.Rows[i]["Barcode"].ToString();
                items.itemnamedisp = dt.Rows[i]["ShowItemName"].ToString();
                items.hsncode = dt.Rows[i]["HSNCode"].ToString();
                items.igstper = Convert.ToDouble(dt.Rows[i]["IGSTPercent"].ToString());
                items.cgstper = Convert.ToDouble(dt.Rows[i]["CGSTPercent"].ToString());
                items.sgstper = Convert.ToDouble(dt.Rows[i]["SGSTPercent"].ToString());
                items.cessper = Convert.ToDouble(dt.Rows[i]["CessPercent"].ToString());
                ucItems.Add(items);
            }
            return ucItems;
        }

        private bool UpdateStockItems(long ItemNo)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Update MStockItems Set StatusNo=3 Where ItemNo=@ItemNo";


            cmd.Parameters.AddWithValue("@ItemNo", ItemNo);

            commandcollection.Add(cmd);
            return true;
        }

        private bool ExecuteNonQueryStatementsCheque()
        {

            SqlConnection cn = null;
            cn = new SqlConnection(CommonFunctions.ConStr);
            cn.Open();

            SqlTransaction myTrans;
            myTrans = cn.BeginTransaction();
            //cmd.Transaction = myTrans;
            try
            {
                for (int i = 0; (i < this.commandcollection.Count); i = (i + 1))
                {
                    if ((this.commandcollection[i] != null))
                    {
                        commandcollection[i].Connection = cn;
                        commandcollection[i].Transaction = myTrans;

                        if (commandcollection[i] != null)
                            commandcollection[i].ExecuteNonQuery();
                    }
                }

                myTrans.Commit();
                return true;
            }
            catch (Exception e)
            {
                myTrans.Rollback();

                if (e.GetBaseException().Message == "")
                {
                    strerrormsg = e.Message;
                }
                else
                {
                    strerrormsg = e.GetBaseException().Message;
                }
                return false;
            }
            finally
            {
                cn.Close();
            }
            //________________________________________________________________________________________________________________________________________________________________________________________________________________________
        }

        public void Upload_ItemMaster()
        {
            try
            {
                List<UCMStockItems> lstItems = GetStockItems();
                if (lstItems.Count == 0)
                {
                    ObjFunction.WriteMessage("Items Not Found to Upload", LogLevel.Information, null);
                    return;
                }
                string jsonStr = JsonConvert.SerializeObject(lstItems);
                ObjFunction.WriteMessage("Items:" + jsonStr, LogLevel.Debug, null);
                string result = ObjFunction.getResponseJSON("/StockItems/SaveItems", "POST", jsonStr);
                ObjFunction.WriteMessage(result, LogLevel.Information, null);
                if (result != "")
                {
                    DBMStockItems dbMstockItems = new DBMStockItems();
                    List<UCReturnStockItems> ucItems = JsonConvert.DeserializeObject<List<UCReturnStockItems>>(result);
                    foreach (UCReturnStockItems items in ucItems)
                    {
                        dbMstockItems.UpdateStockItems(items.itemno);
                    }
                    if (dbMstockItems.ExecuteNonQueryStatementsCheque() == false)
                        throw new Exception("Stock Items Not Save:" + DBMStockItems.strerrormsg);
                }
            }
            catch (Exception ex)
            {
                ObjFunction.WriteMessage(ex.Message, LogLevel.Error, ex);
                ObjFunction.WriteMessage("Items Upload Fail", LogLevel.Information, null);

            }


        }
    }
}