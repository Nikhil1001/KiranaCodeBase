﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Data;
using System.Data.SqlClient;
using OM;
using System.Data;
using JitControls;
using Newtonsoft.Json;


namespace OM
{
    class DBTVaucherEntry
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDSet = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        private CommandCollection commandcollection = new CommandCollection();

        DataTable dtId = new DataTable();

        public static string strerrormsg;

        public void Upload_Sale_Bills()
        {
            try
            {
                UCSaleBillsList SB = GetSaleBill();
                if (SB.salesBills.Count == 0)
                {
                    ObjFunction.WriteMessage("Bills Not Found to Upload", LogLevel.Information, null);
                    return;
                }
                string jsonStr = JsonConvert.SerializeObject(SB);
                ObjFunction.WriteMessage("Bills:" + jsonStr, LogLevel.Debug, null);
                string result = ObjFunction.getResponseJSON("/Bill/SaveBill", "POST", jsonStr);
                ObjFunction.WriteMessage(result, LogLevel.Information, null);
                if (result != "")
                {
                    DBTVaucherEntry dbTVaucherEntry = new DBTVaucherEntry();
                    List<TReturnBill> ucItems = JsonConvert.DeserializeObject<List<TReturnBill>>(result);
                    foreach (TReturnBill items in ucItems)
                    {
                        dbTVaucherEntry.UpdateStatus(items.PkSrNo);
                    }
                    if (dbTVaucherEntry.ExecuteNonQueryStatementsCheque() == false)
                        throw new Exception("Bill Status Not Save:" + DBMStockItems.strerrormsg);
                }
            }
            catch (Exception ex)
            {
                ObjFunction.WriteMessage(ex.Message, LogLevel.Error, ex);
                ObjFunction.WriteMessage("Items Upload Fail", LogLevel.Information, null);

            }

        }

        public void DownLoad_Sale_Bills()
        {
            try
            {
                string result = ObjFunction.getResponseJSON("/Bill/Bill", "GET", null);
                ObjFunction.WriteMessage(result, LogLevel.Debug, null);
                if (result != "")
                {
                    UCSaleBillsList ucSaleBills = JsonConvert.DeserializeObject<UCSaleBillsList>(result);

                    DBTVaucherEntry dbTVaucherEntry = new DBTVaucherEntry();
                    List<TReturnBill> lst = new List<TReturnBill>();
                    foreach (UCSaleBills items in ucSaleBills.salesBills)
                    {
                        TReturnBill tret = new TReturnBill();
                        tret.PkSrNo = items.tvoucherentry.PkVoucherNo;
                        tret.FkVoucherNo = Save_Parking(items);
                        lst.Add(tret);
                    }

                    if (lst.Count != 0)
                    {
                        string jsonStr = JsonConvert.SerializeObject(lst);
                        ObjFunction.WriteMessage("Update Bill Status:" + jsonStr, LogLevel.Debug, null);
                        string result1 = ObjFunction.getResponseJSON("/Bill/SaveBillStatus", "POST", jsonStr);
                        ObjFunction.WriteMessage(result1, LogLevel.Information, null);
                        ObjFunction.WriteMessage("Update Bill Status result Is:" + result1, LogLevel.Information, null);
                        
                    }
                }
            }
            catch (Exception ex)
            {
                ObjFunction.WriteMessage(ex.Message, LogLevel.Error, ex);
                ObjFunction.WriteMessage("Items Upload Fail", LogLevel.Information, null);

            }

        }

        private long Save_Parking(UCSaleBills items)
        {
            try
            {
                UCTVoucherEntry tvch = items.tvoucherentry;

                DBTVaucherEntry dbTVoucherEntry = new DBTVaucherEntry();
                TParkingBill_Mob tPBill = new TParkingBill_Mob();
                TParkingBillDetails tPBillDetails = new TParkingBillDetails();


                tPBill.ParkingBillNo = 0;
                tPBill.BillDate = Convert.ToDateTime(tvch.VoucherDate);
                tPBill.BillTime = Convert.ToDateTime(tvch.VoucherDate);
                tPBill.PersonName = tvch.LedgerName + " Bill No:" + tvch.MobileBillNo;
                tPBill.LedgerNo = tvch.LedgerNo;
                tPBill.IsBill = false;
                tPBill.CompanyNo = tvch.CompanyNo;
                tPBill.IsCancel = false;
                tPBill.UserID = tvch.UserID;
                tPBill.UserDate = DateTime.Now;
                tPBill.Discount = tvch.DiscAmt;
                tPBill.Charges = tvch.ChargAmount;
                tPBill.Remark = tvch.Remark;
                tPBill.RateTypeNo = tvch.RateTypeNo;
                tPBill.TaxTypeNo = tvch.TaxTypeNo;
                tPBill.InvNo = "";
                tPBill.VoucherTypeCode = tvch.VoucherTypeCode;
                dbTVoucherEntry.AddTParkingBill_Mob(tPBill);

                foreach (UCTStock st in items.tstock)
                {
                    tPBillDetails = new TParkingBillDetails();
                    tPBillDetails.PkSrNo = 0;
                    tPBillDetails.Barcode = st.Barcode;
                    tPBillDetails.Qty = st.Qty;
                    tPBillDetails.Rate = st.Rate;
                    tPBillDetails.ItemDisc = st.DiscAmount;
                    tPBillDetails.UOMNo = ObjQry.ReturnLong("SELECT UOMDefault FROM MStockItems Where ItemNo=" + st.ItemNo + "", CommonFunctions.ConStr);
                    tPBillDetails.FkRateSettingNo = ObjQry.ReturnLong("SELECT top(1) PkSrNo FROM MRateSetting Where IsActive='True' And ItemNo=" + st.ItemNo + "", CommonFunctions.ConStr);
                    tPBillDetails.ItemNo = st.ItemNo;
                    dbTVoucherEntry.AddTParkingBillDetails(tPBillDetails);
                }
                dbTVoucherEntry.ExecuteNonQueryStatements_Parking();
                return 0;
            }
            catch (Exception exc)
            {
                throw new Exception(exc.Message, exc);
            }
        }

        private UCSaleBillsList GetSaleBill()
        {
            try
            {
                string sql = "SELECT Top(20)  PkVoucherNo " +
                                   " FROM TVoucherEntry " +
                                   " WHERE VoucherTypeCode = 15" +
                                           "  AND StatusNo in (1,2) And PayTypeNo=3" +
                                   " ORDER BY PkVoucherNo ";
                DataTable dtPkSrNo = ObjFunction.GetDataView(sql).Table;
                UCSaleBillsList SB = new UCSaleBillsList();
                for (int i = 0; i < dtPkSrNo.Rows.Count; i++)
                {
                    try
                    {
                        SB.salesBills.Add(GetBill_Data(Convert.ToInt64(dtPkSrNo.Rows[i].ItemArray[0].ToString())));
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                return SB;

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }

        private bool UpdateStatus(long PkVoucherNo)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Update TVoucherEntry Set StatusNo=3 Where PkVoucherNo=@PkVoucherNo";


            cmd.Parameters.AddWithValue("@PkVoucherNo", PkVoucherNo);

            commandcollection.Add(cmd);
            return true;
        }

        private UCSaleBills GetBill_Data(long PkVoucherNo)
        {

            UCSaleBills SB = new UCSaleBills();
            try
            {
                // ObjFunction.WriteMessage("Start To Get Bill Data", LogLevel.Information, null);
                string sql = "SELECT TVoucherEntry.PkVoucherNo, TVoucherEntry.VoucherUserNo, TVoucherEntry.VoucherTime, TVoucherEntry.BilledAmount, TVoucherEntry.Remark, " +
                                " TVoucherEntry.IsCancel, MPayType.PayTypeName, TVoucherEntry.RateTypeNo," +
                                " ISNULL(TVoucherEntry.DiscAmt,0) As DiscAmt,ISNULL(TVD.Debit+ TVD.Credit, 0)  As HamaliRs,TVoucherEntry.StatusNo  " +
                            " FROM         TVoucherEntry INNER JOIN " +
                                " MPayType ON TVoucherEntry.PayTypeNo = MPayType.PKPayTypeNo  LEFT OUTER JOIN " +
                                " TVoucherDetails ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo AND TVoucherDetails.SrNo = 503 " +
                                " LEFT OUTER JOIN TVoucherDetails AS TVD ON TVD.FkVoucherNo = TVoucherEntry.PkVoucherNo AND TVD.SrNo = 506 " +
                            " WHERE     (TVoucherEntry.PkVoucherNo = " + PkVoucherNo + ") ";

                DataTable dt = ObjFunction.GetDataView(sql).Table;
                sql = "SELECT MLedger.LedgerNo,MLedger.LedgerName, MLedgerDetails.MobileNo1, MLedgerDetails.Address  " +
                          " FROM MLedger " +
                              " INNER JOIN MLedgerDetails ON MLedgerDetails.LedgerNo = MLedger.LedgerNo" +
                              " INNER JOIN TVoucherDetails ON TVoucherDetails.LedgerNo = MLedger.LedgerNo " +
                      " WHERE " +
                          " TVoucherDetails.FkVoucherNo = " + PkVoucherNo + " " +
                          " AND TVoucherDetails.SrNo = " + Others.Party + "";
                DataTable dtCustomerDetails = ObjFunction.GetDataView(sql).Table;
                //  ObjFunction.WriteMessage("Check Ledger", LogLevel.Information, null);
                if (dtCustomerDetails.Rows.Count == 0)
                {
                    throw new CustomeException.LedgerDetailsNotFound("Ledger Details Not Found BillNo=" + PkVoucherNo + "");
                }
                //  ObjFunction.WriteMessage("Check Ledger Sucessfully..", LogLevel.Information, null);

                UCTVoucherEntry tvc = new UCTVoucherEntry();
                tvc.FkVoucherNo = Convert.ToInt64(dt.Rows[0]["PkVoucherNo"].ToString());
                tvc.VoucherTypeCode = 15;
                tvc.VoucherUserNo = Convert.ToInt64(dt.Rows[0]["VoucherUserNo"].ToString());
                tvc.MobileBillNo = 0;
                tvc.LedgerNo = Convert.ToInt64(dtCustomerDetails.Rows[0]["LedgerNo"].ToString());
                tvc.VoucherDate = Convert.ToDateTime(dt.Rows[0]["VoucherTime"].ToString()).ToString("dd-MMM-yyyy");
                tvc.VoucherTime = Convert.ToDateTime(dt.Rows[0]["VoucherTime"].ToString()).ToLongTimeString();
                tvc.BilledAmount = Convert.ToDouble(dt.Rows[0]["BilledAmount"].ToString());
                tvc.Remark = dt.Rows[0]["Remark"].ToString();
                tvc.IsCancel = Convert.ToBoolean(dt.Rows[0]["IsCancel"].ToString());
                tvc.PayTypeNo = (dt.Rows[0]["PayTypeName"].ToString().ToUpper() == "CASH") ? 1 : 2;
                tvc.RateTypeNo = Convert.ToInt64(dt.Rows[0]["RateTypeNo"].ToString());
                tvc.DiscAmt = Convert.ToDouble(dt.Rows[0]["DiscAmt"].ToString());
                tvc.ChargAmount = Convert.ToDouble(dt.Rows[0]["HamaliRs"].ToString()); // Convert.ToDouble(dt.Rows[0]["DiscAmt"].ToString());
                SB.tvoucherentry = tvc;

                sql = "SELECT     MStockBarcode.Barcode, MStockItems.ItemName,MStockItems.ItemNameLang, TStock.BilledQuantity, TStock.Rate, " +
                         " TStock.Amount, TStock.NetRate, TStock.NetAmount, TStock.TaxPercentage, TStock.TaxAmount, " +
                         " TStock.DiscAmount, TStock.DiscRupees, TStock.DiscAmount2, TStock.DiscRupees2 " +
                         ", MRateSetting.MRP,MStockItems.ItemNo,MUOM.UOMName " +
                      " FROM         TStock INNER JOIN " +
                          " MStockBarcode ON TStock.FkStockBarCodeNo = MStockBarcode.PkStockBarcodeNo INNER JOIN " +
                          " MRateSetting ON TStock.FkRateSettingNo = MRateSetting.PkSrNo INNER JOIN " +
                          " dbo.MStockItems_V(NULL, NULL, NULL, NULL, NULL, NULL, NULL) AS MStockItems ON TStock.ItemNo = MStockItems.ItemNo " +
                          " INNER JOIN  MUOM ON TStock.FkUomNo = MUOM.UOMNo " +
                      " WHERE     (TStock.FkVoucherNo = " + PkVoucherNo + ") ";
                //   ObjFunction.WriteMessage(sql, LogLevel.Information, null);
                DataTable dtBillItems = ObjFunction.GetDataView(sql).Table;

                for (int i = 0; i < dtBillItems.Rows.Count; i++)
                {
                    UCTStock billItem = new UCTStock();
                    billItem.PkvouchertrnNo = 0;
                    billItem.FkvoucherNo = 0;
                    billItem.Barcode = dtBillItems.Rows[i]["Barcode"].ToString();
                    billItem.Itemname = dtBillItems.Rows[i]["ItemName"].ToString();
                    billItem.LangitemName = dtBillItems.Rows[i]["ItemNameLang"].ToString();
                    billItem.Qty = Convert.ToDouble(dtBillItems.Rows[i]["BilledQuantity"].ToString());
                    billItem.Amount = Convert.ToDouble(dtBillItems.Rows[i]["Amount"].ToString());
                    billItem.Rate = billItem.Amount / billItem.Qty;
                    billItem.Mrp = Convert.ToDouble(dtBillItems.Rows[i]["MRP"].ToString()); ;
                    billItem.NetAmount = Convert.ToDouble(dtBillItems.Rows[i]["NetAmount"].ToString());
                    billItem.NetRate = billItem.NetAmount / billItem.Qty;
                    billItem.Taxpercentage = Convert.ToDouble(dtBillItems.Rows[i]["TaxPercentage"].ToString());
                    billItem.TaxAmount = Convert.ToDouble(dtBillItems.Rows[i]["TaxAmount"].ToString());
                    billItem.DiscAmount = Convert.ToDouble(dtBillItems.Rows[i]["DiscAmount"].ToString())
                                            + Convert.ToDouble(dtBillItems.Rows[i]["DiscRupees"].ToString())
                                            + Convert.ToDouble(dtBillItems.Rows[i]["DiscAmount2"].ToString())
                                            + Convert.ToDouble(dtBillItems.Rows[i]["DiscRupees2"].ToString());
                    billItem.HSNCode = "";

                    billItem.ItemNo = Convert.ToInt64(dtBillItems.Rows[i]["ItemNo"].ToString());
                    billItem.UomName = dtBillItems.Rows[i]["UOMName"].ToString();
                    SB.tstock.Add(billItem);
                }

                if (tvc.PayTypeNo == 2)
                {
                    sql = " SELECT     TVoucherRefDetails.PkRefTrnNo, TVoucherRefDetails.FkVoucherTrnNo, TVoucherRefDetails.FkVoucherSrNo, TVoucherRefDetails.LedgerNo, TVoucherRefDetails.RefNo, " +
                        " TVoucherRefDetails.TypeOfRef, TVoucherRefDetails.DueDays, TVoucherRefDetails.DueDate, TVoucherRefDetails.Amount, TVoucherRefDetails.SignCode, TVoucherRefDetails.UserID, " +
                        " TVoucherRefDetails.UserDate, TVoucherRefDetails.Modifiedby, TVoucherRefDetails.CompanyNo, TVoucherRefDetails.StatusNo, TVoucherEntry.VoucherDate, TVoucherEntry.PayTypeNo, " +
                        " MPayType.PayTypeName " +
                        " FROM  TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo And TVoucherDetails.SrNo=501 INNER JOIN " +
                        " TVoucherRefDetails ON TVoucherDetails.PkVoucherTrnNo = TVoucherRefDetails.FkVoucherTrnNo INNER JOIN " +
                        " MPayType ON TVoucherEntry.PayTypeNo = MPayType.PKPayTypeNo " +
                        " WHERE     (TVoucherRefDetails.RefNo IN (SELECT     RefNo FROM  TVoucherRefDetails AS TVoucherRefDetails_1 " +
                        " WHERE      (FkVoucherTrnNo IN (SELECT PkVoucherTrnNo FROM TVoucherDetails AS TVoucherDetails_1 " +
                        " WHERE      (FkVoucherNo = " + PkVoucherNo + "))))) ";
                    //  ObjFunction.WriteMessage(sql, LogLevel.Information, null);
                    DataTable dtref = ObjFunction.GetDataView(sql).Table;
                    for (int i = 0; i < dtref.Rows.Count; i++)
                    {
                        UCTVoucherRefDetails tRef = new UCTVoucherRefDetails();


                        tRef.PkreftrnNo = -1;
                        tRef.Refno = 0;
                        tRef.LedgerNo = tvc.LedgerNo;
                        tRef.FkvoucherNo = tvc.PkVoucherNo;
                        tRef.TypeofRef = Convert.ToInt64(dtref.Rows[i]["TypeOfRef"].ToString());
                        tRef.Amount = Convert.ToDouble(dtref.Rows[i]["Amount"].ToString());
                        tRef.DiscAmount = 0;
                        if (dtref.Rows[i]["TypeOfRef"].ToString() == "3")
                        {
                            string stramt = dtref.Compute("Sum(Amount)", "TypeOfRef=2").ToString();
                            tRef.Balamount = Convert.ToDouble((stramt == "") ? "" + tRef.Amount : "" + (tRef.Amount - Convert.ToDouble(stramt)));
                        }
                        else
                            tRef.Balamount = tRef.Amount;

                        tRef.PayDate = dtref.Rows[i]["VoucherDate"].ToString();
                        tRef.PayType = dtref.Rows[i]["PayTypeName"].ToString();
                        tRef.CardNo = "";
                        tRef.Remark = "";
                        SB.tVoucherrefdetails.Add(tRef);
                    }
                }

            }
            catch (Exception ex)
            {
                if (ex is CustomeException.LedgerDetailsNotFound)
                {
                    throw new CustomeException.LedgerDetailsNotFound(ex.Message);
                }
                else
                    throw new Exception(ex.Message);
            }

            return SB;
        }

        private string GetRateType(long RateTypeNo)
        {
            if (RateTypeNo == 1) return "ASaleRate";
            else if (RateTypeNo == 2) return "BSaleRate";
            else if (RateTypeNo == 3) return "CSaleRate";
            else if (RateTypeNo == 4) return "DSaleRate";
            else if (RateTypeNo == 5) return "ESaleRate";
            else if (RateTypeNo == 6) return "MRP";
            else return "";
        }

        public bool AddTVoucherEntry(UCTVoucherEntry tvoucherentry)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTVoucherEntry";

            cmd.Parameters.AddWithValue("@PkVoucherNo", tvoucherentry.PkVoucherNo);

            cmd.Parameters.AddWithValue("@VoucherTypeCode", tvoucherentry.VoucherTypeCode);

            cmd.Parameters.AddWithValue("@VoucherUserNo", tvoucherentry.VoucherUserNo);

            cmd.Parameters.AddWithValue("@VoucherDate", tvoucherentry.VoucherDate);

            cmd.Parameters.AddWithValue("@VoucherTime", tvoucherentry.VoucherTime);

            cmd.Parameters.AddWithValue("@Narration", tvoucherentry.Narration);

            cmd.Parameters.AddWithValue("@Reference", tvoucherentry.Reference);

            cmd.Parameters.AddWithValue("@CompanyNo", tvoucherentry.CompanyNo);

            cmd.Parameters.AddWithValue("@BilledAmount", tvoucherentry.BilledAmount);

            cmd.Parameters.AddWithValue("@Remark", tvoucherentry.Remark);

            cmd.Parameters.AddWithValue("@IsCancel", tvoucherentry.IsCancel);

            cmd.Parameters.AddWithValue("@PayTypeNo", tvoucherentry.PayTypeNo);

            cmd.Parameters.AddWithValue("@RateTypeNo", tvoucherentry.RateTypeNo);

            cmd.Parameters.AddWithValue("@TaxTypeNo", tvoucherentry.TaxTypeNo);

            cmd.Parameters.AddWithValue("@IsVoucherLock", tvoucherentry.IsVoucherLock);

            cmd.Parameters.AddWithValue("@VoucherStatus", tvoucherentry.VoucherStatus);

            cmd.Parameters.AddWithValue("@UserID", tvoucherentry.UserID);

            cmd.Parameters.AddWithValue("@UserDate", tvoucherentry.UserDate);

            cmd.Parameters.AddWithValue("@ModifiedBy", tvoucherentry.ModifiedBy);

            cmd.Parameters.AddWithValue("@OrderType", tvoucherentry.OrderType);

            cmd.Parameters.AddWithValue("@ReturnAmount", tvoucherentry.ReturnAmount);

            cmd.Parameters.AddWithValue("@Visibility", tvoucherentry.Visibility);

            cmd.Parameters.AddWithValue("@DiscPercent", tvoucherentry.DiscPercent);

            cmd.Parameters.AddWithValue("@DiscAmt", tvoucherentry.DiscAmt);

            cmd.Parameters.AddWithValue("@ChargAmount", tvoucherentry.ChargAmount);

            cmd.Parameters.AddWithValue("@MixMode", tvoucherentry.MixMode);

            cmd.Parameters.AddWithValue("@HamaliRs", tvoucherentry.HamaliRs);

            cmd.Parameters.AddWithValue("@MobilePkSrNo", tvoucherentry.MobilePkSrNo);

            cmd.Parameters.AddWithValue("@MobileBillNo", tvoucherentry.MobileBillNo);

            cmd.Parameters.AddWithValue("@MobileUserNo", tvoucherentry.MobileUserNo);

            cmd.Parameters.AddWithValue("@LedgerName", tvoucherentry.LedgerName);

            cmd.Parameters.AddWithValue("@LedgerNo", tvoucherentry.LedgerNo);

            SqlParameter outParameter = new SqlParameter();
            outParameter.ParameterName = "@ReturnID";
            outParameter.Direction = ParameterDirection.Output;
            outParameter.DbType = DbType.Int32;
            cmd.Parameters.Add(outParameter);

            commandcollection.Add(cmd);
            return true;
        }

        public bool AddTStock(UCTStock tstock)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTStock";

            cmd.Parameters.AddWithValue("@PkVoucherTrnNo", tstock.PkvouchertrnNo);

            //  cmd.Parameters.AddWithValue("@FkVoucherNo", tstock.FkvoucherNo);

            cmd.Parameters.AddWithValue("@Barcode", tstock.Barcode);

            cmd.Parameters.AddWithValue("@ItemName", tstock.Itemname);

            cmd.Parameters.AddWithValue("@LangItemName", tstock.LangitemName);

            cmd.Parameters.AddWithValue("@ItemNo", tstock.ItemNo);

            cmd.Parameters.AddWithValue("@Qty", tstock.Qty);

            cmd.Parameters.AddWithValue("@Rate", tstock.Rate);

            cmd.Parameters.AddWithValue("@MRP", tstock.Mrp);

            cmd.Parameters.AddWithValue("@Amount", tstock.Amount);

            cmd.Parameters.AddWithValue("@NetRate", tstock.NetRate);

            cmd.Parameters.AddWithValue("@NetAmount", tstock.NetAmount);

            cmd.Parameters.AddWithValue("@TaxPercentage", tstock.Taxpercentage);

            cmd.Parameters.AddWithValue("@TaxAmount", tstock.TaxAmount);

            cmd.Parameters.AddWithValue("@DiscAmount", tstock.DiscAmount);

            cmd.Parameters.AddWithValue("@HSNCode", tstock.HSNCode);

            cmd.Parameters.AddWithValue("@IGSTPer", tstock.IGSTPer);

            cmd.Parameters.AddWithValue("@IGSTAmt", tstock.IGSTAmt);

            cmd.Parameters.AddWithValue("@CGSTPer", tstock.CGSTPer);

            cmd.Parameters.AddWithValue("@CGSTAmt", tstock.CGSTAmt);

            cmd.Parameters.AddWithValue("@SGSTPer", tstock.SGSTPer);

            cmd.Parameters.AddWithValue("@SGSTAmt", tstock.SGSTAmt);

            cmd.Parameters.AddWithValue("@CESSPer", tstock.CESSPer);

            cmd.Parameters.AddWithValue("@CESSAmt", tstock.CESSAmt);


            commandcollection.Add(cmd);
            return true;
        }

        public bool AddTVoucherRefDetails(UCTVoucherRefDetails tvoucherrefdetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTVoucherRefDetails";

            cmd.Parameters.AddWithValue("@PkRefTrnNo", tvoucherrefdetails.PkreftrnNo);

            //  cmd.Parameters.AddWithValue("@FkVoucherNo", tvoucherrefdetails.FkvoucherNo);

            cmd.Parameters.AddWithValue("@ParkingBillNo", tvoucherrefdetails.ParkingBillNo);

            cmd.Parameters.AddWithValue("@LedgerNo", tvoucherrefdetails.LedgerNo);

            cmd.Parameters.AddWithValue("@RefNo", tvoucherrefdetails.Refno);

            cmd.Parameters.AddWithValue("@TypeOfRef", tvoucherrefdetails.TypeofRef);

            cmd.Parameters.AddWithValue("@Amount", tvoucherrefdetails.Amount);

            cmd.Parameters.AddWithValue("@DiscAmount", tvoucherrefdetails.DiscAmount);

            cmd.Parameters.AddWithValue("@BalAmount", tvoucherrefdetails.Balamount);

            cmd.Parameters.AddWithValue("@PayDate", tvoucherrefdetails.PayDate);

            cmd.Parameters.AddWithValue("@PayType", tvoucherrefdetails.PayType);

            cmd.Parameters.AddWithValue("@CardNo", tvoucherrefdetails.CardNo);

            cmd.Parameters.AddWithValue("@Remark", tvoucherrefdetails.Remark);

            commandcollection.Add(cmd);
            return true;
        }

        public bool AddTParkingBill_Mob(TParkingBill_Mob tparkingbill)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTParkingBill_Mob";

            cmd.Parameters.AddWithValue("@ParkingBillNo", tparkingbill.ParkingBillNo);

            cmd.Parameters.AddWithValue("@BillNo", tparkingbill.BillNo);

            cmd.Parameters.AddWithValue("@BillDate", tparkingbill.BillDate);

            cmd.Parameters.AddWithValue("@BillTime", tparkingbill.BillTime);

            cmd.Parameters.AddWithValue("@PersonName", tparkingbill.PersonName);

            cmd.Parameters.AddWithValue("@LedgerNo", tparkingbill.LedgerNo);

            cmd.Parameters.AddWithValue("@IsBill", tparkingbill.IsBill);

            cmd.Parameters.AddWithValue("@CompanyNo", tparkingbill.CompanyNo);

            cmd.Parameters.AddWithValue("@IsCancel", tparkingbill.IsCancel);

            cmd.Parameters.AddWithValue("@UserID", tparkingbill.UserID);

            cmd.Parameters.AddWithValue("@UserDate", tparkingbill.UserDate);

            cmd.Parameters.AddWithValue("@Discount", tparkingbill.Discount);

            cmd.Parameters.AddWithValue("@Charges", tparkingbill.Charges);

            cmd.Parameters.AddWithValue("@Remark", tparkingbill.Remark);

            cmd.Parameters.AddWithValue("@RateTypeNo", tparkingbill.RateTypeNo);

            cmd.Parameters.AddWithValue("@TaxTypeNo", tparkingbill.TaxTypeNo);

            cmd.Parameters.AddWithValue("@MobilePkSrNo", 0);

            cmd.Parameters.AddWithValue("@MobileBillNo", 0);

            cmd.Parameters.AddWithValue("@InvNo", tparkingbill.InvNo);

            cmd.Parameters.AddWithValue("@VoucherTypeCode", tparkingbill.VoucherTypeCode);

            SqlParameter outParameter = new SqlParameter();
            outParameter.ParameterName = "@ReturnID";
            outParameter.Direction = ParameterDirection.Output;
            outParameter.DbType = DbType.Int32;
            cmd.Parameters.Add(outParameter);

            commandcollection.Add(cmd);
            return true;
        }

        private bool AddTParkingBill(TParkingBill tparkingbill)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTParkingBill_Mob";

            cmd.Parameters.AddWithValue("@ParkingBillNo", tparkingbill.ParkingBillNo);

            cmd.Parameters.AddWithValue("@BillNo", tparkingbill.BillNo);

            cmd.Parameters.AddWithValue("@BillDate", tparkingbill.BillDate);

            cmd.Parameters.AddWithValue("@BillTime", tparkingbill.BillTime);

            cmd.Parameters.AddWithValue("@PersonName", tparkingbill.PersonName);

            cmd.Parameters.AddWithValue("@LedgerNo", tparkingbill.LedgerNo);

            cmd.Parameters.AddWithValue("@IsBill", tparkingbill.IsBill);

            cmd.Parameters.AddWithValue("@CompanyNo", tparkingbill.CompanyNo);

            cmd.Parameters.AddWithValue("@IsCancel", tparkingbill.IsCancel);

            cmd.Parameters.AddWithValue("@UserID", tparkingbill.UserID);

            cmd.Parameters.AddWithValue("@UserDate", tparkingbill.UserDate);

            cmd.Parameters.AddWithValue("@Discount", tparkingbill.Discount);

            cmd.Parameters.AddWithValue("@Charges", tparkingbill.Charges);

            cmd.Parameters.AddWithValue("@Remark", tparkingbill.Remark);

            cmd.Parameters.AddWithValue("@RateTypeNo", tparkingbill.RateTypeNo);

            cmd.Parameters.AddWithValue("@TaxTypeNo", tparkingbill.TaxTypeNo);

            cmd.Parameters.AddWithValue("@MobilePkSrNo", tparkingbill.MobilePkSrNo);

            cmd.Parameters.AddWithValue("@MobileBillNo", tparkingbill.MobileBillNo);

            SqlParameter outParameter = new SqlParameter();
            outParameter.ParameterName = "@ReturnID";
            outParameter.Direction = ParameterDirection.Output;
            outParameter.DbType = DbType.Int32;
            cmd.Parameters.Add(outParameter);

            commandcollection.Add(cmd);
            return true;
        }

        private bool AddTParkingBillDetails(TParkingBillDetails tparkingbilldetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTParkingBillDetails";

            cmd.Parameters.AddWithValue("@PkSrNo", tparkingbilldetails.PkSrNo);

            //cmd.Parameters.AddWithValue("@ParkingBillNo", tparkingbilldetails.ParkingBillNo);

            cmd.Parameters.AddWithValue("@Barcode", tparkingbilldetails.Barcode);

            cmd.Parameters.AddWithValue("@Qty", tparkingbilldetails.Qty);

            cmd.Parameters.AddWithValue("@Rate", tparkingbilldetails.Rate);

            cmd.Parameters.AddWithValue("@ItemDisc", tparkingbilldetails.ItemDisc);

            cmd.Parameters.AddWithValue("@UOMNo", tparkingbilldetails.UOMNo);

            cmd.Parameters.AddWithValue("@FkRateSettingNo", tparkingbilldetails.FkRateSettingNo);

            cmd.Parameters.AddWithValue("@ItemNo", tparkingbilldetails.ItemNo);

            cmd.Parameters.AddWithValue("@CompanyNo", 1);

            commandcollection.Add(cmd);
            return true;
        }

        private bool DeleteTVoucherEntryAll(long PkVoucherNo)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteTVoucherEntry";

            cmd.Parameters.AddWithValue("@PkVoucherNo", PkVoucherNo);

            commandcollection.Add(cmd);
            return true;
        }

        private bool DeleteCollection(long PkRefTrnNo)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Delete From TVoucherRefDetails_Mob Where PkRefTrnNo=@PkRefTrnNo";

            cmd.Parameters.AddWithValue("@PkRefTrnNo", PkRefTrnNo);

            commandcollection.Add(cmd);
            return true;
        }

        private bool AddTVoucherRefDetails_Mob(UCTVoucherRefDetails tvoucherrefdetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTVoucherRefDetails_Mob";

            cmd.Parameters.AddWithValue("@PkRefTrnNo", tvoucherrefdetails.PkreftrnNo);

            cmd.Parameters.AddWithValue("@FkVoucherNo", tvoucherrefdetails.FkvoucherNo);

            //cmd.Parameters.AddWithValue("@ParkingBillNo", tvoucherrefdetails.ParkingBillNo);

            cmd.Parameters.AddWithValue("@LedgerNo", tvoucherrefdetails.LedgerNo);

            cmd.Parameters.AddWithValue("@RefNo", tvoucherrefdetails.Refno);

            cmd.Parameters.AddWithValue("@TypeOfRef", tvoucherrefdetails.TypeofRef);

            cmd.Parameters.AddWithValue("@Amount", tvoucherrefdetails.Amount);

            cmd.Parameters.AddWithValue("@DiscAmount", tvoucherrefdetails.DiscAmount);

            cmd.Parameters.AddWithValue("@BalAmount", tvoucherrefdetails.Balamount);

            cmd.Parameters.AddWithValue("@PayDate", tvoucherrefdetails.PayDate);

            cmd.Parameters.AddWithValue("@PayType", tvoucherrefdetails.PayType);

            cmd.Parameters.AddWithValue("@CardNo", tvoucherrefdetails.CardNo);

            cmd.Parameters.AddWithValue("@Remark", tvoucherrefdetails.Remark);


            commandcollection.Add(cmd);
            return true;
        }



        /*    private long ExecuteNonQueryStatements()
            {


                SqlConnection cn = null;
                cn = new SqlConnection(DBConnection.GetConnection_My());
                cn.Open();

                SqlTransaction myTrans;
                myTrans = cn.BeginTransaction();
                //cmd.Transaction = myTrans;
                int cntVchNo = -1;
                try
                {
                    for (int i = 0; (i < this.commandcollection.Count); i = (i + 1))
                    {
                        if ((this.commandcollection[i] != null))
                        {
                            commandcollection[i].Connection = cn;
                            commandcollection[i].Transaction = myTrans;

                            if (commandcollection[i].CommandText == "AddTParkingBill_Mob")
                            {
                                cntVchNo = i;
                            }
                            if (commandcollection[i].CommandText == "AddTParkingBillDetails")
                            {
                                commandcollection[i].Parameters.AddWithValue("@ParkingBillNo", commandcollection[cntVchNo].Parameters["@ReturnID"].Value);
                            }
                            if (commandcollection[i].CommandText == "AddTVoucherRefDetails_Mob")
                            {
                                if (cntVchNo != -1)
                                    commandcollection[i].Parameters.AddWithValue("@ParkingBillNo", commandcollection[cntVchNo].Parameters["@ReturnID"].Value);
                                else
                                    commandcollection[i].Parameters.AddWithValue("@ParkingBillNo", 0);
                            }



                            if (commandcollection[i] != null)
                                commandcollection[i].ExecuteNonQuery();
                        }
                    }

                    myTrans.Commit();
                    if (cntVchNo == -1)
                        return 0;
                    else
                        return Convert.ToInt64(commandcollection[cntVchNo].Parameters["@ReturnID"].Value);


                }
                catch (Exception e)
                {
                    myTrans.Rollback();
                    throw new Exception(e.Message);
                }
                finally
                {
                    cn.Close();
                }

            }*/

        private bool ExecuteNonQueryStatementsCheque()
        {

            SqlConnection cn = null;
            cn = new SqlConnection(CommonFunctions.ConStr);
            cn.Open();

            SqlTransaction myTrans;
            myTrans = cn.BeginTransaction();
            //cmd.Transaction = myTrans;
            try
            {
                for (int i = 0; (i < this.commandcollection.Count); i = (i + 1))
                {
                    if ((this.commandcollection[i] != null))
                    {
                        commandcollection[i].Connection = cn;
                        commandcollection[i].Transaction = myTrans;

                        if (commandcollection[i] != null)
                            commandcollection[i].ExecuteNonQuery();
                    }
                }

                myTrans.Commit();
                return true;
            }
            catch (Exception e)
            {
                myTrans.Rollback();

                if (e.GetBaseException().Message == "")
                {
                    strerrormsg = e.Message;
                }
                else
                {
                    strerrormsg = e.GetBaseException().Message;
                }
                return false;
            }
            finally
            {
                cn.Close();
            }
            //________________________________________________________________________________________________________________________________________________________________________________________________________________________
        }

        public void ExecuteNonQueryStatements_Parking()
        {


            SqlConnection cn = null;
            cn = new SqlConnection(CommonFunctions.ConStr);
            cn.Open();

            SqlTransaction myTrans;
            myTrans = cn.BeginTransaction();
            //cmd.Transaction = myTrans;
            int cntVchNo = -1;
            try
            {
                for (int i = 0; (i < this.commandcollection.Count); i = (i + 1))
                {
                    if ((this.commandcollection[i] != null))
                    {
                        commandcollection[i].Connection = cn;
                        commandcollection[i].Transaction = myTrans;

                        if (commandcollection[i].CommandText == "AddTParkingBill_Mob")
                        {
                            cntVchNo = i;
                        }
                        if (commandcollection[i].CommandText == "AddTParkingBillDetails")
                        {
                            commandcollection[i].Parameters.AddWithValue("@ParkingBillNo", commandcollection[cntVchNo].Parameters["@ReturnID"].Value);
                        }

                        if (commandcollection[i] != null)
                            commandcollection[i].ExecuteNonQuery();
                    }
                }

                myTrans.Commit();



            }
            catch (Exception e)
            {
                myTrans.Rollback();
                throw new Exception(e.Message);
            }
            finally
            {
                cn.Close();
            }

        }

        private long ExecuteNonQueryStatements()
        {


            SqlConnection cn = null;
            cn = new SqlConnection(CommonFunctions.ConStr);
            cn.Open();

            SqlTransaction myTrans;
            myTrans = cn.BeginTransaction();
            //cmd.Transaction = myTrans;
            int cntVchNo = -1;
            try
            {
                for (int i = 0; (i < this.commandcollection.Count); i = (i + 1))
                {
                    if ((this.commandcollection[i] != null))
                    {
                        commandcollection[i].Connection = cn;
                        commandcollection[i].Transaction = myTrans;

                        if (commandcollection[i].CommandText == "AddTVoucherEntry")
                        {
                            cntVchNo = i;
                        }
                        if (commandcollection[i].CommandText == "AddTStock")
                        {
                            commandcollection[i].Parameters.AddWithValue("@FkVoucherNo", commandcollection[cntVchNo].Parameters["@ReturnID"].Value);
                        }
                        if (commandcollection[i].CommandText == "AddTVoucherRefDetails")
                        {
                            if (cntVchNo != -1)
                                commandcollection[i].Parameters.AddWithValue("@FkVoucherNo", commandcollection[cntVchNo].Parameters["@ReturnID"].Value);
                            else
                                commandcollection[i].Parameters.AddWithValue("@FkVoucherNo", 0);
                        }



                        if (commandcollection[i] != null)
                            commandcollection[i].ExecuteNonQuery();
                    }
                }

                myTrans.Commit();
                if (cntVchNo == -1)
                    return 0;
                else
                    return Convert.ToInt64(commandcollection[cntVchNo].Parameters["@ReturnID"].Value);


            }
            catch (Exception e)
            {
                myTrans.Rollback();
                throw new Exception(e.Message);
            }
            finally
            {
                cn.Close();
            }

        }
    }

    public class TParkingBill
    {
        private long mParkingBillNo;
        private long mBillNo;
        private DateTime mBillDate;
        private DateTime mBillTime;
        private string mPersonName;
        private long mLedgerNo;
        private bool mIsBill;
        private long mFKVoucherNo;
        private long mCompanyNo;
        private bool mIsCancel;
        private long mUserID;
        private DateTime mUserDate;
        private int mStatusNo;
        private double mDiscount;
        private double mCharges;
        private string mRemark;
        private long mRateTypeNo;
        private long mTaxTypeNo;
        private long mMobilePkSrNo;
        private long mMobileBillNo;
        private string Mmsg;

        /// <summary>
        /// This Properties use for ParkingBillNo
        /// </summary>
        public long ParkingBillNo
        {
            get { return mParkingBillNo; }
            set { mParkingBillNo = value; }
        }
        /// <summary>
        /// This Properties use for BillNo
        /// </summary>
        public long BillNo
        {
            get { return mBillNo; }
            set { mBillNo = value; }
        }
        /// <summary>
        /// This Properties use for BillDate
        /// </summary>
        public DateTime BillDate
        {
            get { return mBillDate; }
            set { mBillDate = value; }
        }
        /// <summary>
        /// This Properties use for BillTime
        /// </summary>
        public DateTime BillTime
        {
            get { return mBillTime; }
            set { mBillTime = value; }
        }
        /// <summary>
        /// This Properties use for PersonName
        /// </summary>
        public string PersonName
        {
            get { return mPersonName; }
            set { mPersonName = value; }
        }
        /// <summary>
        /// This Properties use for LedgerNo
        /// </summary>
        public long LedgerNo
        {
            get { return mLedgerNo; }
            set { mLedgerNo = value; }
        }
        /// <summary>
        /// This Properties use for IsBill
        /// </summary>
        public bool IsBill
        {
            get { return mIsBill; }
            set { mIsBill = value; }
        }
        /// <summary>
        /// This Properties use for FKVoucherNo
        /// </summary>
        public long FKVoucherNo
        {
            get { return mFKVoucherNo; }
            set { mFKVoucherNo = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for IsCancel
        /// </summary>
        public bool IsCancel
        {
            get { return mIsCancel; }
            set { mIsCancel = value; }
        }
        /// <summary>
        /// This Properties use for UserID
        /// </summary>
        public long UserID
        {
            get { return mUserID; }
            set { mUserID = value; }
        }
        /// <summary>
        /// This Properties use for UserDate
        /// </summary>
        public DateTime UserDate
        {
            get { return mUserDate; }
            set { mUserDate = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public int StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for Discount
        /// </summary>
        public double Discount
        {
            get { return mDiscount; }
            set { mDiscount = value; }
        }
        /// <summary>
        /// This Properties use for Charges
        /// </summary>
        public double Charges
        {
            get { return mCharges; }
            set { mCharges = value; }
        }
        /// <summary>
        /// This Properties use for Remark
        /// </summary>
        public string Remark
        {
            get { return mRemark; }
            set { mRemark = value; }
        }
        /// <summary>
        /// This Properties use for RateTypeNo
        /// </summary>
        public long RateTypeNo
        {
            get { return mRateTypeNo; }
            set { mRateTypeNo = value; }
        }
        /// <summary>
        /// This Properties use for TaxTypeNo
        /// </summary>
        public long TaxTypeNo
        {
            get { return mTaxTypeNo; }
            set { mTaxTypeNo = value; }
        }

        public long MobilePkSrNo
        {
            get { return mMobilePkSrNo; }
            set { mMobilePkSrNo = value; }
        }

        public long MobileBillNo
        {
            get { return mMobileBillNo; }
            set { mMobileBillNo = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }

    public class TParkingBill_Mob
    {
        public long ParkingBillNo;
        public long BillNo;
        public DateTime BillDate;
        public DateTime BillTime;
        public string PersonName;
        public long LedgerNo;
        public bool IsBill;
        public long FKVoucherNo;
        public long CompanyNo;
        public bool IsCancel;
        public long UserID;
        public DateTime UserDate;
        public int StatusNo;
        public double Discount;
        public double Charges;
        public string Remark;
        public long RateTypeNo;
        public long TaxTypeNo;
        public string InvNo;
        public long VoucherTypeCode;

    }

    public class TParkingBillDetails
    {
        private long mPkSrNo;
        private long mParkingBillNo;
        private string mBarcode;
        private double mQty;
        private double mRate;
        private double mItemDisc;
        private long mUOMNo;
        private int mStatusNo;
        private long mCompanyNo;
        private long mFkRateSettingNo;
        private long mItemNo;
        private string Mmsg;

        /// <summary>
        /// This Properties use for PkSrNo
        /// </summary>
        public long PkSrNo
        {
            get { return mPkSrNo; }
            set { mPkSrNo = value; }
        }
        /// <summary>
        /// This Properties use for ParkingBillNo
        /// </summary>
        public long ParkingBillNo
        {
            get { return mParkingBillNo; }
            set { mParkingBillNo = value; }
        }
        /// <summary>
        /// This Properties use for Barcode
        /// </summary>
        public string Barcode
        {
            get { return mBarcode; }
            set { mBarcode = value; }
        }
        /// <summary>
        /// This Properties use for Qty
        /// </summary>
        public double Qty
        {
            get { return mQty; }
            set { mQty = value; }
        }
        /// <summary>
        /// This Properties use for Rate
        /// </summary>
        public double Rate
        {
            get { return mRate; }
            set { mRate = value; }
        }
        /// <summary>
        /// This Properties use for ItemDisc
        /// </summary>
        public double ItemDisc
        {
            get { return mItemDisc; }
            set { mItemDisc = value; }
        }
        /// <summary>
        /// This Properties use for UOMNo
        /// </summary>
        public long UOMNo
        {
            get { return mUOMNo; }
            set { mUOMNo = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public int StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for FkRateSettingNo
        /// </summary>
        public long FkRateSettingNo
        {
            get { return mFkRateSettingNo; }
            set { mFkRateSettingNo = value; }
        }
        /// <summary>
        /// This Properties use for ItemNo
        /// </summary>
        public long ItemNo
        {
            get { return mItemNo; }
            set { mItemNo = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }

}
