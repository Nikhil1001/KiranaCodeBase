﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Data;
using System.Data.SqlClient;
using JitControls;
using Newtonsoft.Json;

namespace OM
{
    class DBMCustomer
    {

        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        public CommandCollection commandcollection = new CommandCollection();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        public static string strerrormsg;

        public List<UCCustomer> GetCustomer()
        {

            string Sql =" SELECT     MLedger.LedgerNo, MLedger.LedgerName, MLedgerDetails.MobileNo1, MLedgerDetails.Address, MLedgerDetails.CityNo, MCity.CityName, MLedgerDetails.PinCode,  "+
                        " MLedgerDetails.CSTNo AS GSTNo, MState.StateName, MState.StateCodeGST, MLedger.GroupNo ,MState.StateNo" +
                        " FROM         MLedger INNER JOIN "+
                        " MLedgerDetails ON MLedger.LedgerNo = MLedgerDetails.LedgerNo INNER JOIN "+
                        " MCity ON MLedgerDetails.CityNo = MCity.CityNo INNER JOIN "+
                        " MState ON MLedgerDetails.StateNo = MState.StateNo "+
                        " WHERE     (MLedger.IsActive = 'True') And (MLedger.StatusNo<>3)";

            DataTable dt = ObjFunction.GetDataView(Sql).Table;
            List<UCCustomer> ucItems = new List<UCCustomer>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                UCCustomer items = new UCCustomer();
                items.LedgerNo = Convert.ToInt64(dt.Rows[i]["LedgerNo"].ToString());
                items.LedgerName = dt.Rows[i]["LedgerName"].ToString();
                items.MobileNo = dt.Rows[i]["MobileNo1"].ToString();
                items.Address = dt.Rows[i]["Address"].ToString();
                items.PinCode = dt.Rows[i]["PinCode"].ToString();
                items.GSTNo = dt.Rows[i]["GSTNo"].ToString();
                items.StateNo = Convert.ToInt64(dt.Rows[i]["StateNo"].ToString());
                items.StateName = dt.Rows[i]["StateName"].ToString();
                items.CityNo = Convert.ToInt64(dt.Rows[i]["CityNo"].ToString());
                items.CityName = dt.Rows[i]["CityName"].ToString();
                items.GroupNo = Convert.ToInt64(dt.Rows[i]["GroupNo"].ToString());
                items.StateCode = Convert.ToInt64(dt.Rows[i]["StateCodeGST"].ToString());
                ucItems.Add(items);
            }
            return ucItems;
        }

        private bool UpdateCustomer(long LedgerNo)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Update MLedger Set StatusNo=3 Where LedgerNo=@LedgerNo";


            cmd.Parameters.AddWithValue("@LedgerNo", LedgerNo);

            commandcollection.Add(cmd);
            return true;
        }

        private bool ExecuteNonQueryStatementsCheque()
        {

            SqlConnection cn = null;
            cn = new SqlConnection(CommonFunctions.ConStr);
            cn.Open();

            SqlTransaction myTrans;
            myTrans = cn.BeginTransaction();
            //cmd.Transaction = myTrans;
            try
            {
                for (int i = 0; (i < this.commandcollection.Count); i = (i + 1))
                {
                    if ((this.commandcollection[i] != null))
                    {
                        commandcollection[i].Connection = cn;
                        commandcollection[i].Transaction = myTrans;

                        if (commandcollection[i] != null)
                            commandcollection[i].ExecuteNonQuery();
                    }
                }

                myTrans.Commit();
                return true;
            }
            catch (Exception e)
            {
                myTrans.Rollback();

                if (e.GetBaseException().Message == "")
                {
                    strerrormsg = e.Message;
                }
                else
                {
                    strerrormsg = e.GetBaseException().Message;
                }
                return false;
            }
            finally
            {
                cn.Close();
            }
            //________________________________________________________________________________________________________________________________________________________________________________________________________________________
        }

        public void Upload_Customer()
        {
            try
            {
                List<UCCustomer> lstItems = GetCustomer();
                if (lstItems.Count == 0)
                {
                    ObjFunction.WriteMessage("Items Not Found to Upload", LogLevel.Information, null);
                    return;
                }
                string jsonStr = JsonConvert.SerializeObject(lstItems);
                ObjFunction.WriteMessage("Items:" + jsonStr, LogLevel.Debug, null);
                string result = ObjFunction.getResponseJSON("/Customer/SaveCustomer", "POST", jsonStr);
                ObjFunction.WriteMessage(result, LogLevel.Information, null);
                if (result != "")
                {
                    DBMCustomer dbMCustomer= new DBMCustomer();
                    List<UCReturnCustomer> ucItems = JsonConvert.DeserializeObject<List<UCReturnCustomer>>(result);
                    foreach (UCReturnCustomer items in ucItems)
                    {
                        dbMCustomer.UpdateCustomer(items.LedgerNo);
                    }
                    if (dbMCustomer.ExecuteNonQueryStatementsCheque() == false)
                        throw new Exception("Customer Not Save:" + DBMStockItems.strerrormsg);
                }
            }
            catch (Exception ex)
            {
                ObjFunction.WriteMessage(ex.Message, LogLevel.Error, ex);
                ObjFunction.WriteMessage("Customer Upload Fail", LogLevel.Information, null);

            }


        }
    }
}
