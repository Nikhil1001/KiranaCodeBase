﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using OM;
using JitControls;

namespace OMService
{
    public partial class OMServiceConnect : ServiceBase
    {
        OMCommonClass cc = new OMCommonClass();
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();
        Security secure = new Security();

        Timer tmrInit, tmrSaleBillUpload, tmrStockItems, tmrCustomer, tmrSaleBillDownload;

        public OMServiceConnect()
        {
            InitializeComponent();



            tmrInit = new Timer();
            tmrInit.Interval = 1000 * 60;// 1 Min
            tmrInit.Elapsed += new ElapsedEventHandler(runInit);
            tmrInit.Enabled = true;
        }

        private void runInit(object sender, EventArgs e)
        {
            Init();
        }

        private void Init()
        {
            SetConnection();
            if (tmrInit.Enabled)
            {

                tmrInit.Enabled = false;
                tmrInit.Interval = 1000 * 60 * 5;// 5 min

                //Stock Items Upload
                tmrStockItems = new Timer();
                tmrStockItems.Interval = 1000 * 60 * 1;// 1 Min
                tmrStockItems.Elapsed += new ElapsedEventHandler(Start_Synsc_Items);
                tmrStockItems.Enabled = true;

                //Stock Customer Upload
                tmrCustomer = new Timer();
                tmrCustomer.Interval = 1000 * 60 * 5;// 1 Min
                tmrCustomer.Elapsed += new ElapsedEventHandler(Start_Synsc_Customer);
                tmrCustomer.Enabled = true;

                //Sale Bill Upload
                tmrSaleBillUpload = new Timer();
                tmrSaleBillUpload.Interval = 1000 * 60 * 7;// 1 Min
                tmrSaleBillUpload.Elapsed += new ElapsedEventHandler(Start_Synsc_SaleBill);
                tmrSaleBillUpload.Enabled = true;


                //Sale Bill Download
                tmrSaleBillDownload = new Timer();
                tmrSaleBillDownload.Interval = 1000 * 60 * 1;// 1 Min
                tmrSaleBillDownload.Elapsed += new ElapsedEventHandler(Start_Synsc_Bill_Download);
                tmrSaleBillDownload.Enabled = true;
            }

        }

        private void Start_Synsc_SaleBill(object sender, EventArgs e)
        {
            if (tmrSaleBillUpload.Enabled)
            {
                tmrSaleBillUpload.Enabled = false;
                tmrSaleBillUpload.Interval = 1000 * 60 * 10;// 5 Min

                //Upload Bills
                ObjFunction.WriteMessage("Sale Bill Upload Start", LogLevel.Information, null);
                DBTVaucherEntry db = new DBTVaucherEntry();
                db.Upload_Sale_Bills();
                tmrSaleBillUpload.Enabled = true;

            }
        }

        private void Start_Synsc_Items(object sender, EventArgs e)
        {
            if (tmrStockItems.Enabled)
            {
                tmrStockItems.Enabled = false;
                tmrStockItems.Interval = 1000 * 60 * 7;// 10 Min

                //Upload Items
                ObjFunction.WriteMessage("Item Master Upload Start", LogLevel.Information, null);
                DBMStockItems db = new DBMStockItems();
                db.Upload_ItemMaster();
                tmrStockItems.Enabled = true;

            }
        }

        private void Start_Synsc_Customer(object sender, EventArgs e)
        {
            if (tmrCustomer.Enabled)
            {
                tmrCustomer.Enabled = false;
                tmrCustomer.Interval = 1000 * 60 * 10;// 10 Min

                //Upload Customer
                ObjFunction.WriteMessage("Customer Master Upload Start", LogLevel.Information, null);
                DBMCustomer db = new DBMCustomer();
                db.Upload_Customer();
                tmrCustomer.Enabled = true;

            }
        }

        private void Start_Synsc_Bill_Download(object sender, EventArgs e)
        {
            if (tmrSaleBillDownload.Enabled)
            {
                tmrSaleBillDownload.Enabled = false;
                tmrSaleBillDownload.Interval = 1000 * 60 * 1;// 10 Min

                //Download Bill
                ObjFunction.WriteMessage("Bill Download Start", LogLevel.Information, null);
                DBTVaucherEntry db = new DBTVaucherEntry();
                db.DownLoad_Sale_Bills();
                tmrSaleBillDownload.Enabled = true;
                ObjFunction.WriteMessage("Bill Download Compleated", LogLevel.Information, null);
            }
        }

        #region Start and Stop Serivce

        protected override void OnStart(string[] args)
        {
            ObjFunction.WriteMessage("Kirana Service Start", LogLevel.Information, null);



        }

        protected override void OnStop()
        {
            ObjFunction.WriteMessage("Kirana Service Stop", LogLevel.Information, null);
        }

        private void SetConnection()
        {
            try
            {
                CommonFunctions.ServerName = System.Net.Dns.GetHostName().ToUpper() + "\\SQLEXPRESS";
                CommonFunctions.DatabaseName = "Kirana0001";
                CommonFunctions.ConStr = "Data Source=" + CommonFunctions.ServerName + ";Initial Catalog=" + CommonFunctions.DatabaseName + ";User ID=OM96;Password=OM96";
                //CommonFunctions.Url = @"http://localhost/OM/API";
                // CommonFunctions.Url = @"http://localhost:34954/API";
                CommonFunctions.Url = @"http://www.RShopNow.com/API";
                CommonFunctions.StoreID = ObjQry.ReturnString("Select CompanyUserCode From MCompany", CommonFunctions.ConStr);
                ObjFunction.WriteMessage(CommonFunctions.ConStr, LogLevel.Information, null);
                ObjFunction.WriteMessage(CommonFunctions.Url, LogLevel.Information, null);
            }
            catch (Exception ex)
            {
                ObjFunction.WriteMessage(ex.Message.ToString(), LogLevel.Error, ex);
                this.Stop();
            }
        }

        #endregion
    }
}
