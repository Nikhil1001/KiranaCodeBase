﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OM
{
    
    public class UCSaleBills
    {
        public UCTVoucherEntry tvoucherentry;
        public IList<UCTStock> tstock= new List<UCTStock>();
        public IList<UCTVoucherRefDetails> tVoucherrefdetails = new List<UCTVoucherRefDetails>();
        
    }
    public class UCSaleBillsList
    {
        public List<UCSaleBills> salesBills = new List<UCSaleBills>();
    }

    
    public class UCTVoucherEntry
    {
        public long PkVoucherNo;
        public long FkVoucherNo;
        public long VoucherTypeCode;
        public long VoucherUserNo;
        public string VoucherDate;
        public string VoucherTime;
        public string Narration="";
        public string Reference="";
        public long CompanyNo;
        public double BilledAmount;
        public string Remark="";
        public bool IsCancel=false;
        public long PayTypeNo;
        public long RateTypeNo;
        public long TaxTypeNo;
        public bool IsVoucherLock=false;
        public string VoucherStatus="";
        public long UserID;
        public DateTime UserDate;
        public string ModifiedBy="";
        public long OrderType;
        public double ReturnAmount;
        public double Visibility;
        public double DiscPercent;
        public double DiscAmt;
        public double ChargAmount;
        public string StatusNo;
        public string MixMode="";
        public double HamaliRs;
        public long MobilePkSrNo;
        public long MobileBillNo;
        public long MobileUserNo;
        public string LedgerName="";
        public long LedgerNo;
    }

    public class UCTStock
    {
        public long PkvouchertrnNo;
        public long FkvoucherNo;
        public String Barcode;
        public String Itemname;
        public String LangitemName="";
        public long ItemNo;
        public double Qty;
        public double Rate;
        public double Mrp;
        public String UomName="";
        public double Amount;
        public double NetRate;
        public double NetAmount;
        public double Taxpercentage;
        public double TaxAmount;
        public double DiscAmount;
        public String HSNCode="";
        public double IGSTPer;
        public double IGSTAmt;
        public double CGSTPer;
        public double CGSTAmt;
        public double SGSTPer;
        public double SGSTAmt;
        public double CESSPer;
        public double CESSAmt;
        public long TempItemNo;
    }

    public class UCTVoucherRefDetails
    {
        public long PkSrNo;
        public long PkreftrnNo;
        public long FkvoucherNo;
        public long ParkingBillNo;
        public long LedgerNo;
        public long Refno;
        public long TypeofRef;
        public double Amount;
        public long DiscAmount;
        public double Balamount;
        public String PayDate="";
        public String PayType="";
        public String CardNo="";
        public String CheqDate="";
        public String Remark="";
    }


    public class TReturnBill
    {
        public long PkSrNo;
        public long FkVoucherNo;
    }
}
