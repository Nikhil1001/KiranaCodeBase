﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OM
{

    public class UCMStockItems
    {
        public long pksrno { get; set; }
        public long itemno { get; set; }
        public string brandname { get; set; }
        public string itemname { get; set; }
        public string langfulldesc { get; set; }
        public double mrp { get; set; }
        public double arate { get; set; }
        public double brate { get; set; }
        public double crate { get; set; }
        public string uom { get; set; }
        public string barcode { get; set; }
        public string itemnamedisp { get; set; }
        public double taxrate { get; set; }
        public double igstper { get; set; }
        public double cgstper { get; set; }
        public double sgstper { get; set; }
        public double cessper { get; set; }
        public string hsncode { get; set; }
        public string imagepath { get; set; }
    }
    public class UCReturnStockItems
    {
        public long itemno { get; set; }
    }
}