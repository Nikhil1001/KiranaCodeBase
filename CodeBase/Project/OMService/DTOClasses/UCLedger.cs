﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;

namespace OM
{
    public class UCMLedger
    {
        public long ledgerno { get; set; }
        public string ledgername { get; set; }
        public string address { get; set; }
        public string mobileno { get; set; }
        public bool isactive { get; set; }
        public long userid { get; set; }
        [Newtonsoft.Json.JsonConverter(typeof(OM.IsoDateTimeConverterWithMicroSeconds))]
        public DateTime userdate { get; set; }
    }

    public class UCCustomer
    {
        public long LedgerNo { get; set; }
        public long GroupNo { get; set; }
        public string LedgerName { get; set; }
        public string MobileNo { get; set; }
        public string Address { get; set; }
        public long CityNo { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public long StateCode { get; set; }
        public long StateNo { get; set; }
        public string PinCode { get; set; }
        public string GSTNo { get; set; }
    }
    public class UCReturnCustomer
    {
        public long LedgerNo { get; set; }
    }
}
