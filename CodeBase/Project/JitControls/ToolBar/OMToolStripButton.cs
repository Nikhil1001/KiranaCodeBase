﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.ComponentModel;

namespace JitControls
{
    public class OMToolStripButton :ToolStripButton
    {
        Color gradientTop = Color.FromArgb(255, 52, 100, 152);
        Color gradientBottom = Color.FromArgb(255, 52, 100, 152);

        Color gradientTopM = Color.FromArgb(255, 52, 100, 152);
        Color gradientBottomM = Color.FromArgb(255, 52, 100, 152);
        Color gradientBackcolor = Color.Transparent;
        Color gradientMiddlecolor = Color.White;
        Color gradientT, gradientB;
        int mCornerRadius = 3;

        public OMToolStripButton()
            : base()
        {
            //gradientT = gradientTop;// Color.FromArgb(255, 144, 185, 177);
            //gradientB = gradientBottom;// Color.FromArgb(255, 53, 98, 41);
            
        }
        public OMToolStripButton(string value)
            : base(value)
        {
            //gradientT = gradientTop;// Color.FromArgb(255, 144, 185, 177);
            //gradientB = gradientBottom;// Color.FromArgb(255, 53, 98, 41);
        }
        [Category("Appearance"), Description("The gradient fill of the component Permission.")]
        public int CornerRadius
        {
            get
            {
                return this.mCornerRadius;
            }
            set
            {
                this.mCornerRadius = value;
                this.Invalidate();
            }
        }
        [Category("Appearance"), Description("The color to use for the top portion of the gradient fill of the component.")]
        public Color GradientTop
        {
            get
            {
                return this.gradientTop;
            }
            set
            {
                this.gradientTop = value;
                this.Invalidate();
            }
        }
        
        [Category("Appearance"), Description("The color to use for the bottom portion of the gradient fill of the component.")]
        public Color GradientBottom
        {
            get
            {
                return this.gradientBottom;
            }
            set
            {
                this.gradientBottom = value;
                this.Invalidate();
            }
        }

        [Category("Appearance"), Description("The color to use for the top portion of the gradient fill of the component. on mouse")]
        public Color GradientTopOnMouse
        {
            get
            {
                return this.gradientTopM;
            }
            set
            {
                this.gradientTopM = value;
                this.Invalidate();
            }
        }

        [Category("Appearance"), Description("The color to use for the bottom portion of the gradient fill of the component. on mouse")]
        public Color GradientBottomOnMouse
        {
            get
            {
                return this.gradientBottomM;
            }
            set
            {
                this.gradientBottomM = value;
                this.Invalidate();
            }
        }

        [Category("Appearance"), Description("The color to use for the back portion of the gradient fill of the component.")]
        public Color GradientBackColor
        {
            get
            {
                return this.gradientBackcolor;
            }
            set
            {
                this.gradientBackcolor = value;
                this.Invalidate();
            }
        }

        [Category("Appearance"), Description("The color to use for the Middle portion of the gradient fill of the component.")]
        public Color GradientMiddleColor
        {
            get
            {
                return this.gradientMiddlecolor;
            }
            set
            {
                this.gradientMiddlecolor = value;
                this.Invalidate();
            }
        }
        
        protected override void OnPaint(PaintEventArgs pevent)
        {
            Graphics g = pevent.Graphics;
            // Fill the background
            using (SolidBrush backgroundBrush = new SolidBrush(gradientBackcolor))
            {
                g.FillRectangle(backgroundBrush, this.ContentRectangle);
            }
            // Paint the outer rounded rectangle
            g.SmoothingMode = SmoothingMode.AntiAlias;
            Rectangle outerRect = new Rectangle(ContentRectangle.X, ContentRectangle.Y, ContentRectangle.Width - 1, ContentRectangle.Height - 1);
            using (GraphicsPath outerPath = RoundedRectangle(outerRect, mCornerRadius, 0))
            {
                using (LinearGradientBrush outerBrush = new LinearGradientBrush(outerRect, gradientTop, gradientBottom, LinearGradientMode.Vertical))
                {
                    g.FillPath(outerBrush, outerPath);
                }
                using (Pen outlinePen = new Pen(gradientTop))
                {
                    g.DrawPath(outlinePen, outerPath);
                }
            }
            // Paint the highlight rounded rectangle
            Rectangle innerRect = new Rectangle(ContentRectangle.X, ContentRectangle.Y, ContentRectangle.Width - 1, ContentRectangle.Height / 2 - 1);
            using (GraphicsPath innerPath = RoundedRectangle(innerRect, mCornerRadius, 2))
            {
                using (LinearGradientBrush innerBrush = new LinearGradientBrush(innerRect, Color.FromArgb(255, gradientMiddlecolor), Color.FromArgb(0, gradientMiddlecolor), LinearGradientMode.Vertical))
                {
                    g.FillPath(innerBrush, innerPath);
                }
            }
            
            // Paint the text
            TextRenderer.DrawText(g, this.Text, this.Font, outerRect, this.ForeColor, Color.Transparent, TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter | TextFormatFlags.EndEllipsis);
        }
        
        protected override void OnMouseHover(EventArgs e)
        {
            base.OnMouseHover(e);
            gradientT = gradientTop;
            gradientB = gradientBottom;
            gradientTop = gradientTopM; //Color.FromArgb(144, 115, 77);
            gradientBottom = gradientBottomM;// Color.FromArgb(53, 198, 41);
            this.Invalidate();
            //OnPaint(new PaintEventArgs(this.CreateGraphics(), this.ContentRectangle));
        }
        
        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            if ((int)gradientT.A != 0)
            {
                gradientTop = gradientT;// Color.FromArgb(255, 144, 185, 177);
                gradientBottom = gradientB;// Color.FromArgb(255, 53, 98, 41);
            }
            this.Invalidate();
            //OnPaint(new PaintEventArgs(this.CreateGraphics(), this.ContentRectangle));
        }
        
        protected override void OnDragOver(DragEventArgs dragEvent)
        {
            base.OnDragOver(dragEvent);
            gradientT = gradientTopM; //Color.FromArgb(144, 115, 77);
            gradientB = gradientBottomM;// Color.FromArgb(53, 198, 41);
            this.Invalidate();
        }
        protected override void OnDragLeave(EventArgs e)
        {
            base.OnDragLeave(e);
            if ((int)gradientT.A != 0)
            {
                gradientTop = gradientT;// Color.FromArgb(255, 144, 185, 177);
                gradientBottom = gradientB;// Color.FromArgb(255, 53, 98, 41);
            }
            this.Invalidate();
        }
        
      
        private GraphicsPath RoundedRectangle(Rectangle boundingRect, int cornerRadius, int margin)
        {
            GraphicsPath roundedRect = new GraphicsPath();
            roundedRect.AddArc(boundingRect.X + margin, boundingRect.Y + margin, cornerRadius * 2, cornerRadius * 2, 180, 90);
            roundedRect.AddArc(boundingRect.X + boundingRect.Width - margin - cornerRadius * 2, boundingRect.Y + margin, cornerRadius * 2, cornerRadius * 2, 270, 90);
            roundedRect.AddArc(boundingRect.X + boundingRect.Width - margin - cornerRadius * 2, boundingRect.Y + boundingRect.Height - margin - cornerRadius * 2, cornerRadius * 2, cornerRadius * 2, 0, 90);
            roundedRect.AddArc(boundingRect.X + margin, boundingRect.Y + boundingRect.Height - margin - cornerRadius * 2, cornerRadius * 2, cornerRadius * 2, 90, 90);
            roundedRect.CloseFigure();
            return roundedRect;
        }
    }
}
