﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace JitControls
{
    public class OMBPanel :Panel
    {
        Color mBorderColor = Color.Gray;
        int mborderRadius = 3;

        public OMBPanel()
        {
            //this.DoubleBuffered = true;
        }

        [Category("Appearance"), Description("The color to use for the bottom portion of the gradient fill of the component.")]
        public Color BorderColor
        {
            get
            {
                return this.mBorderColor;
            }
            set
            {
                this.mBorderColor = value;
                this.Invalidate();
            }
        }

        [Category("Appearance"), Description("The gradient border radious fill of the component Permission.")]
        public int BorderRadius
        {
            get
            {
                return this.mborderRadius;
            }
            set
            {
                this.mborderRadius = value;
                this.Invalidate();
            }
        }

        protected override void OnPaint(PaintEventArgs pevent)
        {
            if (this.BorderStyle != BorderStyle.None)
                base.OnPaint(pevent);
            else
            {
                base.OnPaint(pevent);

                Pen pn = new Pen(mBorderColor, mborderRadius);
                pevent.Graphics.DrawRectangle(pn,
                ClientRectangle.Left,
                ClientRectangle.Top,
                ClientRectangle.Width,
                ClientRectangle.Height);
            }
            this.ResumeLayout();
        }
    }
}
