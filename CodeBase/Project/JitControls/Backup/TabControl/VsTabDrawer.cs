using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace JitControls
{
	/// <summary>
	/// Provides drawing capabilities that mimic the Visual
	/// Studio tabs.
	/// </summary>
	public class VsTabDrawer : OMTabDrawer
	{
		/// <summary>
		/// Creates an instance of the <see cref="VsTabDrawer"/> class.
		/// </summary>
        Color gradientTop = Color.Silver; 
        Color gradientBottom = Color.LightGray; 
        Color gradientMiddle = Color.White;
        Color gradientATop = Color.Silver;
        Color gradientABottom = Color.LightGray;
        Color gradientAMiddle = Color.White;
        bool mgradientShow = false, mgradientAShow = false;

        #region Object and Properties
        [Category("Appearance"), Description("The color to use  effect use or not of the component.")]
        public bool GradientShow
        {
            get
            {
                return this.mgradientShow;
            }
            set
            {
                this.mgradientShow = value;
            }
        }
        [Category("Appearance"), Description("The color to use  effect use or not of the component.")]
        public bool GradientActiveShow
        {
            get
            {
                return this.mgradientAShow;
            }
            set
            {
                this.mgradientAShow = value;
            }
        }
        [Category("Appearance"), Description("The color to use for the top portion of the gradient fill of the component.")]
        public Color GradientTop
        {
            get
            {
                return this.gradientTop;
            }
            set
            {
                this.gradientTop = value;
            }
        }

        [Category("Appearance"), Description("The color to use for the bottom portion of the gradient fill of the component.")]
        public Color GradientBottom
        {
            get
            {
                return this.gradientBottom;
            }
            set
            {
                this.gradientBottom = value;
            }
        }
        [Category("Appearance"), Description("The color to use for the Middle portion of the gradient fill of the component.")]
        public Color GradientMiddle
        {
            get
            {
                return this.gradientMiddle;
            }
            set
            {
                this.gradientMiddle = value;
            }
        }


        [Category("Appearance"), Description("The color to use for the top portion of the gradient fill of the component.")]
        public Color GradientActiveTop
        {
            get
            {
                return this.gradientATop;
            }
            set
            {
                this.gradientATop = value;
            }
        }

        [Category("Appearance"), Description("The color to use for the bottom portion of the gradient fill of the component.")]
        public Color GradientActiveBottom
        {
            get
            {
                return this.gradientABottom;
            }
            set
            {
                this.gradientABottom = value;
            }
        }
        [Category("Appearance"), Description("The color to use for the Middle portion of the gradient fill of the component.")]
        public Color GradientActiveMiddle
        {
            get
            {
                return this.gradientAMiddle;
            }
            set
            {
                this.gradientAMiddle = value;
            }
        }
        #endregion
        public VsTabDrawer()
		{
			pens = new Hashtable( 3 );
			brushes = new Hashtable( 2 );
		}

		#region OMTabDrawer Members

		/// <summary>
		/// Inherited from <see cref="OMTabDrawer"/>.
		/// </summary>
		/// <param name="foreColor">See <see cref="OMTabDrawer.DrawTab(Color,Color,Color,Color,Color,bool,DockStyle,Graphics,SizeF)"/>.</param>
		/// <param name="backColor">See <see cref="OMTabDrawer.DrawTab(Color,Color,Color,Color,Color,bool,DockStyle,Graphics,SizeF)"/>.</param>
		/// <param name="highlightColor">See <see cref="OMTabDrawer.DrawTab(Color,Color,Color,Color,Color,bool,DockStyle,Graphics,SizeF)"/>.</param>
		/// <param name="shadowColor">See <see cref="OMTabDrawer.DrawTab(Color,Color,Color,Color,Color,bool,DockStyle,Graphics,SizeF)"/>.</param>
		/// <param name="borderColor">See <see cref="OMTabDrawer.DrawTab(Color,Color,Color,Color,Color,bool,DockStyle,Graphics,SizeF)"/>.</param>
		/// <param name="active">See <see cref="OMTabDrawer.DrawTab(Color,Color,Color,Color,Color,bool,DockStyle,Graphics,SizeF)"/>.</param>
		/// <param name="dock">See <see cref="OMTabDrawer.DrawTab(Color,Color,Color,Color,Color,bool,DockStyle,Graphics,SizeF)"/>.</param>
		/// <param name="graphics">See <see cref="OMTabDrawer.DrawTab(Color,Color,Color,Color,Color,bool,DockStyle,Graphics,SizeF)"/>.</param>
		/// <param name="tabSize">See <see cref="OMTabDrawer.DrawTab(Color,Color,Color,Color,Color,bool,DockStyle,Graphics,SizeF)"/>.</param>
		public override void DrawTab( Color foreColor, Color backColor, Color highlightColor, Color shadowColor, Color borderColor, bool active, DockStyle dock, Graphics graphics, SizeF tabSize )
		{
			if( !brushes.ContainsKey( foreColor ) )
			{
				brushes[ foreColor ] = new SolidBrush( foreColor );
			}
			if( !brushes.ContainsKey( backColor ) )
			{
				brushes[ backColor ] = new SolidBrush( backColor );
			}
			if( !pens.ContainsKey( highlightColor ) )
			{
				pens[ highlightColor ] = new Pen( highlightColor );
			}
			if( !pens.ContainsKey( shadowColor ) )
			{
				pens[ shadowColor ] = new Pen( shadowColor );
			}
			if( !pens.ContainsKey( foreColor ) )
			{
				pens[ foreColor ] = new Pen( foreColor );
			}
			Brush fb = ( Brush ) brushes[ foreColor ];
			Brush bb = ( Brush ) brushes[ backColor ];
			Pen h = ( Pen ) pens[ highlightColor ];
			Pen s = ( Pen ) pens[ shadowColor ];
			Pen f = ( Pen ) pens[ foreColor ];
            if (active)
            {
                if (mgradientAShow == true)
                    DrawGradientTab(graphics, backColor, 0, 0, tabSize.Width, tabSize.Height + 1, gradientATop, gradientAMiddle, gradientABottom);
                else
                    graphics.FillRectangle(fb, 0, 0, tabSize.Width, tabSize.Height + 1);
                switch (dock)
                {
                    case DockStyle.Bottom:
                        graphics.DrawLine(h, tabSize.Width, 0, tabSize.Width, tabSize.Height);
                        graphics.DrawLine(s, 0, 0, 0, tabSize.Height);
                        graphics.DrawLine(s, 0, 0, tabSize.Width, 0);
                        break;
                    case DockStyle.Top:
                        graphics.DrawLine(h, 0, 0, tabSize.Width, 0);
                        graphics.DrawLine(h, 0, 0, 0, tabSize.Height);
                        graphics.DrawLine(s, tabSize.Width, 0, tabSize.Width, tabSize.Height);
                        break;
                    case DockStyle.Left:
                        graphics.DrawLine(h, 0, 0, tabSize.Width, 0);
                        graphics.DrawLine(s, 0, 0, 0, tabSize.Height);
                        graphics.DrawLine(h, tabSize.Width, 0, tabSize.Width, tabSize.Height);
                        break;
                    case DockStyle.Right:
                        graphics.DrawLine(s, 0, 0, tabSize.Width, 0);
                        graphics.DrawLine(h, 0, 0, 0, tabSize.Height);
                        graphics.DrawLine(s, tabSize.Width, 0, tabSize.Width, tabSize.Height);
                        break;
                }

            }
            else
            {
                if (mgradientShow == true)
                    DrawGradientTab(graphics, backColor, tabSize.Width, 1, tabSize.Width, tabSize.Height + 1, gradientTop, gradientMiddle, gradientBottom);

                graphics.DrawLine(f, tabSize.Width, 1, tabSize.Width, tabSize.Height - 1);
            }
		}

        private void DrawGradientTab(Graphics g, Color backColor, float x, float y, float w, float h, Color GrTop, Color GrMid, Color GrB)
        {
            // Fill the background
            using (SolidBrush backgroundBrush = new SolidBrush(backColor))
            {
                g.FillRectangle(backgroundBrush, 0, 0, w, h + 1);
            }
            // Paint the outer rounded rectangle
            g.SmoothingMode = SmoothingMode.AntiAlias;
            Rectangle outerRect = new Rectangle(0, 0, (int)w, (int)h + 1);
            using (GraphicsPath outerPath = RoundedRectangle(outerRect, 1, 0))
            {
                using (LinearGradientBrush outerBrush = new LinearGradientBrush(outerRect, GrTop, GrB, LinearGradientMode.Vertical))
                {
                    g.FillPath(outerBrush, outerPath);
                }
                using (Pen outlinePen = new Pen(GrTop))
                {
                    g.DrawPath(outlinePen, outerPath);
                }
            }
            // Paint the highlight rounded rectangle
            Rectangle innerRect = new Rectangle(0, 0, (int)w, (int)h / 2 - 1);
            using (GraphicsPath innerPath = RoundedRectangle(innerRect, 1, 2))
            {
                using (LinearGradientBrush innerBrush = new LinearGradientBrush(innerRect, Color.FromArgb(255, GrMid), Color.FromArgb(0, GrMid), LinearGradientMode.Vertical))
                {
                    g.FillPath(innerBrush, innerPath);
                }
            }
        }

        private GraphicsPath RoundedRectangle(Rectangle boundingRect, int cornerRadius, int margin)
        {
            GraphicsPath roundedRect = new GraphicsPath();
            roundedRect.AddArc(boundingRect.X + margin, boundingRect.Y + margin, cornerRadius * 2, cornerRadius * 2, 180, 90);
            roundedRect.AddArc(boundingRect.X + boundingRect.Width - margin - cornerRadius * 2, boundingRect.Y + margin, cornerRadius * 2, cornerRadius * 2, 270, 90);
            roundedRect.AddArc(boundingRect.X + boundingRect.Width - margin - cornerRadius * 2, boundingRect.Y + boundingRect.Height - margin - cornerRadius * 2, cornerRadius * 2, cornerRadius * 2, 0, 90);
            roundedRect.AddArc(boundingRect.X + margin, boundingRect.Y + boundingRect.Height - margin - cornerRadius * 2, cornerRadius * 2, cornerRadius * 2, 90, 90);
            roundedRect.CloseFigure();
            return roundedRect;
        }

		/// <summary>
		/// Inherited from <see cref="OMTabDrawer"/>.
		/// </summary>
		/// <returns>
		/// The <see cref="VsTabDrawer"/> uses highlights. Hence, this
		/// method always returns <b>true</b>.
		/// </returns>
		public override bool UsesHighlghts
		{
			get
			{
				return true;
			}
		}

		/// <summary>
		/// Inherited from <see cref="OMTabDrawer"/>.
		/// </summary>
		/// <returns>
		/// The <see cref="VsTabDrawer"/> supports all directional
		/// <see cref="DockStyle"/>s.
		/// </returns>
		public override DockStyle[] SupportedTabDockStyles
		{
			get
			{
				return new DockStyle[] { DockStyle.Bottom, DockStyle.Top, DockStyle.Left, DockStyle.Right };
			} 
		}

		/// <summary>
		/// Inherited from <see cref="OMTabDrawer"/>.
		/// </summary>
		/// <param name="dock">See <see cref="OMTabDrawer.SupportsTabDockStyle(DockStyle)"/>.</param>
		/// <returns>
		/// Returns <b>true</b> for all directional <see cref="DockStyle"/>s.
		/// </returns>
		public override bool SupportsTabDockStyle( DockStyle dock )
		{
			return ( dock != DockStyle.Fill && dock != DockStyle.None );
		}

		#endregion

		/// <summary>
		/// Contains pens that have been used in drawing.
		/// </summary>
		private Hashtable pens;

		/// <summary>
		/// Contains brushes that have been used in drawing.
		/// </summary>
		private Hashtable brushes;
	}
}
