using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Threading;
using System.Windows.Forms;

namespace JitControls
{
	/// <summary>
	/// Yet Another Tab Control.
	/// </summary>
	[Designer( typeof( JitControls.OMTabControlDesigner ) )]
	public class OMTabControl : Control
	{
		#region Constructor

		/// <summary>
		/// Creates a new instance of the <see cref="OMTabControl"/>
		/// class.
		/// </summary>
		public OMTabControl()
		{
			SetStyle( ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer, true );
			defaultImageIndex = -1;
			OMTabLengths = new ArrayList( 5 );
			leftArrow = new Point[ 3 ];
			rightArrow = new Point[ 3 ];
			for( int i = 0; i < 3; i++ )
			{
				leftArrow[ i ] = new Point( 0, 0 );
				rightArrow[ i ] = new Point( 0, 0 );
			}
			OMTabFont = Font;
			OMBoldTabFont = new Font( Font.FontFamily.Name, Font.Size, FontStyle.Bold );
			OMMargin = 3;
			OMTabDock = DockStyle.Top;
			OMSelectedIndex = -1;
			OMForeBrush = new SolidBrush( ForeColor );
			OMActiveBrush = ( Brush ) SystemBrushes.Control.Clone();
			OMActiveColor = SystemColors.Control;
			OMInactiveBrush = ( Brush ) SystemBrushes.Window.Clone();
			OMInactiveColor = SystemColors.Window;
			OMBorderPen = ( Pen ) Pens.DarkGray.Clone();
			OMShadowPen = ( Pen ) SystemPens.ControlDark.Clone();
			OMHighlightPen = ( Pen ) SystemPens.ControlLight.Clone();
			OMDisplayRectangle = Rectangle.Empty;
			OMTabsRectangle = Rectangle.Empty;
			OMClientRectangle = Rectangle.Empty;
			OMTransformedDisplayRectangle = Rectangle.Empty;
			Height = Width = 300;
			BackColor = SystemColors.Control;
			CalculateTabSpan();
			CalculateTabLengths();
			CalculateLastVisibleTabIndex();
			ChildTextChangeEventHandler = new EventHandler( OMTabPage_TextChanged );
		}

		#endregion

		#region Original Public Properties

		/// <summary>
		/// Gets and sets the value of the index of the image in
		/// <see cref="ImageList"/> to use to draw the default image on tabs
		/// that do not specify an image to use.
		/// </summary>
		/// <value>
		/// The zero-based index to the image in the <see cref="OMTabControl.ImageList"/>
		/// that appears on the tab. The default is -1, which signifies no image.
		/// </value>
		/// <exception cref="ArgumentException">
		/// The value of <see cref="ImageIndex"/> is less than -1.
		/// </exception>
		public virtual int ImageIndex
		{
			get
			{
				return defaultImageIndex;
			}
			set
			{
				defaultImageIndex = value;
				CalculateTabLengths();
				InU();
			}
		}

		/// <summary>
		/// Gets and sets the <see cref="ImageList"/> used by this
		/// <see cref="OMTabControl"/>.
		/// </summary>
		/// <remarks>
		/// To display an image on a tab, set the <see cref="ImageIndex"/> property
		/// of that <see cref="OMTabPage"/>. The <see cref="ImageIndex"/> acts as the
		/// index into the <see cref="ImageList"/>.
		/// </remarks>
		public virtual ImageList ImageList
		{
			get
			{
				return images;
			}
			set
			{
				images = value;
				CalculateTabLengths();
				InU();
			}
		}

		/// <summary>
		/// Gets and sets the <see cref="OMTabDrawer"/> used
		/// to draw the tabs for the <see cref="OMTabControl"/>.
		/// </summary>
		/// <remarks>
		/// <para>The default value of this property is <b>null</b>.</para>
		/// <para>When this property is <b>null</b>, no tabs get drawn.</para>
		/// </remarks>
		public virtual OMTabDrawer TabDrawer
		{
			get
			{
				return OMTabDrawer;
			}
			set
			{
				OMTabDrawer = value;
				InU();
				OnTabDrawerChanged( new EventArgs() );
			}
		}

		/// <summary>
		/// Gets and sets the docking side of the tabs.
		/// </summary>
		/// <exception cref="ArgumentException">
		/// Thrown if the property tries to get set to
		/// <see cref="DockStyle.Fill"/> of <see cref="DockStyle.None"/>.
		/// </exception>
		/// <remarks>
		/// The default value of this property is <see cref="DockStyle.Top"/>.
		/// </remarks>
		public virtual DockStyle TabDock
		{
			get
			{
				return OMTabDock;
			}
			set
			{
				if( DockStyle.Fill == value || DockStyle.None == value )
				{
					throw new ArgumentException( "Tried to set the TabDock property to an invlaid value of Fill or None." );
				}
				if( OMTabDrawer == null || OMTabDrawer.SupportsTabDockStyle( value ) )
				{
					OMTabDock = value;
				}
				else
				{
					throw new ArgumentException( "Tried to set the TabDock property to a value not supported by the current tab drawer." );
				}
				CalculateRectangles();
				PerformLayout();
				InU();
				OnTabDockChanged( new EventArgs() );
			}
		}

		/// <summary>
		/// Gets and sets the <see cref="Font"/> used to draw the strings in
		/// the tabs.
		/// </summary>
		public virtual Font TabFont
		{
			get
			{
				return OMTabFont;
			}
			set
			{
				if( value != null )
				{
					OMTabFont = value;
					OMBoldTabFont = new Font( value.FontFamily.Name, value.Size, FontStyle.Bold );
					CalculateTabSpan();
					CalculateTabLengths();
					CalculateRectangles();
					PerformLayout();
					InU();
				}
				else
				{
					OMTabFont = Font;
				}
				OnTabFontChanged( new EventArgs() );
			}
		}

		/// <summary>
		/// Gets and sets the number of pixels to use as the margin.
		/// </summary>
		/// <remarks>
		/// This property puts a margin between the edge of the control
		/// and the tabs, between the tabs and the active tab page, and
		/// the active tab page and the edges of the control. The default
		/// value is 3.
		/// </remarks>
		/// <exception cref="ArgumentException">
		/// Thrown if this property get set to a value less than 0.
		/// </exception>
		public virtual int Margin
		{
			get
			{
				return OMMargin;
			}
			set
			{
				if( value < 0 )
				{
					throw new ArgumentException( "Tried to set the property Margin to a negative number." );
				}
				OMMargin = value;
				CalculateRectangles();
				CalculateTabLengths();
				CalculateTabSpan();
				CalculateLastVisibleTabIndex();
				PerformLayout();
				InU();
				OnMarginChanged( new EventArgs() );
			}
		}

		/// <summary>
		/// The <see cref="Color"/> of the active tab's
		/// background and the margins around the visible
		/// <see cref="OMTabPage"/>.
		/// </summary>
		/// <remarks>
		/// The default value for this is
		/// <see cref="SystemColors.Control"/>.
		/// </remarks>
		public virtual Color ActiveColor
		{
			get
			{
				return OMActiveColor;
			}
			set
			{
				OMActiveColor = value;
				OMActiveBrush.Dispose();
				OMActiveBrush = new SolidBrush( value );
				OMShadowPen.Color = ControlPaint.DarkDark( value );
				OnActiveColorChanged( new EventArgs() );
				InU();
			}
		}

		/// <summary>
		/// The <see cref="Color"/> of the inactive tabs'
		/// background.
		/// </summary>
		/// <remarks>
		/// The default value for this property is <see cref="Color.GhostWhite"/>.
		/// </remarks>
		public virtual Color InactiveColor
		{
			get
			{
				return OMInactiveColor;
			}
			set
			{
				OMInactiveColor = value;
				OMInactiveBrush.Dispose();
				OMInactiveBrush = new SolidBrush( value );
				OMHighlightPen.Color = ControlPaint.LightLight( value );
				OnInactiveColorChanged( new EventArgs() );
				InU();
			}
		}

		/// <summary>
		/// The <see cref="Color"/> of the border drawn
		/// around the control.
		/// </summary>
		/// <remarks>
		/// The default value for this property is <see cref="Color.DarkGray"/>.
		/// </remarks>
		public virtual Color BorderColor
		{
			get
			{
				return OMBorderPen.Color;
			}
			set
			{
				OMBorderPen.Color = value;
				OnBorderColorChanged( new EventArgs() );
				InU();
			}
		}

		/// <summary>
		/// Gets and sets the zero-based index of the selected
		/// <see cref="OMTabPage"/>.
		/// </summary>
		/// <exception cref="ArgumentException">
		/// Thrown if this property gets set to a value less than 0 when
		/// <see cref="OMTabPage"/>s exist in the control collection.
		/// </exception>
		/// <exception cref="IndexOutOfRangeException">
		/// Thrown if this property gets set to a value greater than
		/// <see cref="Control.ControlCollection.Count"/>.
		/// </exception>
		public virtual int SelectedIndex
		{
			get
			{
				return OMSelectedIndex;
			}
			set
			{
				if( value < 0 && Controls.Count > 0 )
				{
					throw new ArgumentException( "Tried to set the property SelectedIndex to a negative number." );
				}
				else if( value >= Controls.Count )
				{
					throw new IndexOutOfRangeException( "Tried to set the property of the SelectedIndex to a value greater than the number of controls." );
				}
				TabChangingEventArgs tcea = new TabChangingEventArgs( OMSelectedIndex, value );
				OnTabChanging( tcea );
				if( tcea.Cancel )
				{
					return;
				}
				OMSelectedIndex = value;
				if( Controls.Count > 0 )
				{
					OMSelectedTab.Visible = false;
					OMSelectedTab = ( OMTabPage ) Controls[ value ];
					OMSelectedTab.Visible = true;
					PerformLayout();
					InU();
				}
				OnTabChanged( new EventArgs() );
			}
		}

		/// <summary>
		/// Gets and sets how the scroll buttons should get
		/// shown when drawing the tabs in the tab area.
		/// </summary>
		/// <remarks>
		/// The default value for this is <see cref="OMScrollButtonStyle.Always"/>.
		/// </remarks>
		public virtual OMScrollButtonStyle ScrollButtonStyle
		{
			get
			{
				return OMShowScrollButtons;
			}
			set
			{
				OMShowScrollButtons = value;
				InU();
				OnScrollButtonStyleChanged( new EventArgs() );
			}
		}

		/// <summary>
		/// Gets and sets the currently selected tab.
		/// </summary>
		/// <exception cref="ArgumentException">
		/// Thrown if this property gets set to a <see cref="OMTabPage"/>
		/// that has not been added to the <see cref="OMTabControl"/>.
		/// </exception>
		/// <exception cref="ArgumentNullException">
		/// Thrown if this property gets set to a <b>null</b> value when
		/// <see cref="OMTabPage"/>s exist in the control.
		/// </exception>
		public virtual OMTabPage SelectedTab
		{
			get
			{
				return OMSelectedTab;
			}
			set
			{
				if( value == null && Controls.Count > 0 )
				{
					throw new ArgumentNullException( "value", "Tried to set the SelectedTab property to a null value." );
				}
				else if( value != null && !Controls.Contains( value ) )
				{
					throw new ArgumentException( "Tried to set the SelectedTab property to a OMTabPage that has not been added to this OMTabControl." );
				}
				if( Controls.Count > 0 )
				{
					int newIndex;
					for( newIndex = 0; newIndex < Controls.Count; newIndex++ )
					{
						if( value == Controls[ newIndex ] )
						{
							break;
						}
					}
					TabChangingEventArgs tcea = new TabChangingEventArgs( OMSelectedIndex, newIndex );
					OnTabChanging( tcea );
					if( tcea.Cancel )
					{
						return;
					}
					OMSelectedIndex = newIndex;
					OMSelectedTab.Visible = false;
					OMSelectedTab = value;
					OMSelectedTab.Visible = true;
					PerformLayout();
					InU();
					OnTabChanged( new EventArgs() );
				}
			}
		}

		#endregion

		#region Overridden Public Properties

		/// <summary>
		/// Inherited from <see cref="Control"/>.
		/// </summary>
		/// <remarks>See <see cref="Control.ForeColor"/>.
		/// </remarks>
		public override Color ForeColor
		{
			get
			{
				return base.ForeColor;
			}
			set
			{
				base.ForeColor = value;
				OMForeBrush = new SolidBrush( value );
			}
		}

		/// <summary>
		/// Inherited from <see cref="Control"/>.
		/// </summary>
		public override Rectangle DisplayRectangle
		{
			get
			{
				return OMTransformedDisplayRectangle;
			}
		}

		#endregion

		#region Original Public Methods

		/// <summary>
		/// Returns the bounding rectangle for a specified tab in this tab control.
		/// </summary>
		/// <param name="index">The 0-based index of the tab you want.</param>
		/// <returns>A <see cref="Rectangle"/> that represents the bounds of the specified tab.</returns>
		/// <exception cref="ArgumentOutOfRangeException">
		/// The index is less than zero.<br />-or-<br />The index is greater than or equal to <see cref="Control.ControlCollection.Count" />.
		/// </exception>
		public virtual Rectangle GetTabRect( int index )
		{
			if( index < 0 || index >= Controls.Count )
			{
				throw new ArgumentOutOfRangeException( "index", index, "The value of index passed to GetTabRect fell outside the valid range." );
			}
			float l = 0.0f;
			if( OMTabDock == DockStyle.Left )
			{
				l = Height - Convert.ToSingle( OMTabLengths[ 0 ] ) + OMTabLeftDif;
				for( int i = 0; i < index; i++ )
				{
					l -= Convert.ToSingle( OMTabLengths[ i + 1 ] );
				}
			}
			else
			{
				l = 2.0f * Convert.ToSingle( OMMargin ) - OMTabLeftDif;
				for( int i = 0; i < index; i++ )
				{
					l += Convert.ToSingle( OMTabLengths[ i ] );
				}
			}
			switch( OMTabDock )
			{
				case DockStyle.Bottom:
					return new Rectangle( Convert.ToInt32( l ), 3 * OMMargin + OMClientRectangle.Height, Convert.ToInt32( OMTabLengths[ index ] ), Convert.ToInt32( OMTabSpan ) + OMMargin );
				case DockStyle.Right:
					return new Rectangle( OMClientRectangle.Height, Convert.ToInt32( l ), Convert.ToInt32( OMTabSpan ) + OMMargin, Convert.ToInt32( OMTabLengths[ index ] ) );
				case DockStyle.Left:
					return new Rectangle( OMMargin, Convert.ToInt32( l ), Convert.ToInt32( OMTabSpan ) + OMMargin, Convert.ToInt32( OMTabLengths[ index ] ) );
			}
			return new Rectangle( Convert.ToInt32( l ), 2 * OMMargin, Convert.ToInt32( OMTabLengths[ index ] ), Convert.ToInt32( OMTabSpan ) + OMMargin );
		}

		/// <summary>
		/// Gets the <see cref="Rectangle"/> that contains the left
		/// scroll button.
		/// </summary>
		/// <returns>
		/// A <see cref="Rectangle"/>.
		/// </returns>
		public virtual Rectangle GetLeftScrollButtonRect()
		{
			Rectangle r = Rectangle.Empty;
			if( OMShowScrollButtons == OMScrollButtonStyle.Always )
			{
				int tabSpan = Convert.ToInt32( OMTabSpan );
				switch( OMTabDock )
				{
					case DockStyle.Top:
						r = new Rectangle( Width - 2 * OMTabsRectangle.Height, 0, OMTabsRectangle.Height, OMTabsRectangle.Height );
						break;
					case DockStyle.Bottom:
						r = new Rectangle( Width - 2 * OMTabsRectangle.Height, OMClientRectangle.Height, OMTabsRectangle.Height, OMTabsRectangle.Height );
						break;
					case DockStyle.Left:
						r = new Rectangle( 0, OMTabsRectangle.Height, OMTabsRectangle.Height, OMTabsRectangle.Height );
						break;
					case DockStyle.Right:
						r = new Rectangle( Width - OMTabsRectangle.Height, Height - 2 * OMTabsRectangle.Height, OMTabsRectangle.Height, OMTabsRectangle.Height );
						break;
				}
			}
			return r;
		}

		/// <summary>
		/// Gets the <see cref="Rectangle"/> that contains the left
		/// scroll button.
		/// </summary>
		/// <returns>
		/// A <see cref="Rectangle"/>.
		/// </returns>
		public virtual Rectangle GetRightScrollButtonRect()
		{
			Rectangle r = Rectangle.Empty;
			if( OMShowScrollButtons == OMScrollButtonStyle.Always )
			{
				int tabSpan = Convert.ToInt32( OMTabSpan );
				switch( OMTabDock )
				{
					case DockStyle.Top:
						r = new Rectangle( Width - OMTabsRectangle.Height, 0, OMTabsRectangle.Height, OMTabsRectangle.Height );
						break;
					case DockStyle.Bottom:
						r = new Rectangle( Width - OMTabsRectangle.Height, OMClientRectangle.Height, OMTabsRectangle.Height, OMTabsRectangle.Height );
						break;
					case DockStyle.Left:
						r = new Rectangle( 0, 0, OMTabsRectangle.Height, OMTabsRectangle.Height );
						break;
					case DockStyle.Right:
						r = new Rectangle( Width - OMTabsRectangle.Height, Height - OMTabsRectangle.Height, OMTabsRectangle.Height, OMTabsRectangle.Height );
						break;
				}
			}
			return r;
		}

		/// <summary>
		/// Scrolls the tabs by the specified <i>amount</i>.
		/// </summary>
		/// <param name="amount">
		/// The number of pixels to scroll the tabs.
		/// </param>
		/// <remarks>
		/// Positive amounts will scroll the tabs to the left. Negative
		/// amounts will scroll the tabs to the right.
		/// </remarks>
		public virtual void ScrollTabs( int amount )
		{
			OMTabLeftDif = Math.Max( 0, OMTabLeftDif - amount );
			if( OMTabLeftDif <= 0 || OMTabLeftDif >= OMTotalTabSpan - Convert.ToSingle( OMTabLengths[ OMTabLengths.Count - 1 ] ) )
			{
				lock( this )
				{
					OMKeepScrolling = false;
				}
			}
			if( OMTabLeftDif >= OMTotalTabSpan - Convert.ToSingle( OMTabLengths[ OMTabLengths.Count - 1 ] ) )
			{
				canScrollLeft = false;
			}
			if( OMTabLeftDif <= 0 )
			{
				canScrollRight = false;
			}
			CalculateLastVisibleTabIndex();
			InU();
		}

		#endregion

		#region Original Public Events

		/// <summary>
		/// Occurs when the selected tab is about to change.
		/// </summary>
		public event TabChangingEventHandler TabChanging;

		/// <summary>
		/// Occurs after the selected tab has changed.
		/// </summary>
		public event EventHandler TabChanged;

		/// <summary>
		/// Occurs after the border color has changed
		/// </summary>
		public event EventHandler BorderColorChanged;

		/// <summary>
		/// Occurs after the active color has changed
		/// </summary>
		public event EventHandler ActiveColorChanged;

		/// <summary>
		/// Occurs after the inactive color has changed
		/// </summary>
		public event EventHandler InactiveColorChanged;

		/// <summary>
		/// Occurs after the margin for the control has changed.
		/// </summary>
		public event EventHandler MarginChanged;

		/// <summary>
		/// Occurs after the <see cref="TabDock"/> property
		/// has changed.
		/// </summary>
		public event EventHandler TabDockChanged;

		/// <summary>
		/// Occurs after the <see cref="TabDrawer"/> property
		/// has changed.
		/// </summary>
		public event EventHandler TabDrawerChanged;

		/// <summary>
		/// Occurs after the <see cref="TabFont"/> property
		/// has changed.
		/// </summary>
		public event EventHandler TabFontChanged;

		/// <summary>
		/// Occurs after the <see cref="ScrollButtonStyle"/>
		/// property has changed.
		/// </summary>
		public event EventHandler ScrollButtonStyleChanged;

		#endregion

		#region New Protected Methods

		/// <summary>
		/// Fires the <see cref="ScrollButtonStyleChanged"/> event.
		/// </summary>
		/// <param name="ea">
		/// Some <see cref="EventArgs"/>.
		/// </param>
		protected virtual void OnScrollButtonStyleChanged( EventArgs ea )
		{
			if( ScrollButtonStyleChanged != null )
			{
				ScrollButtonStyleChanged( this, ea );
			}
		}

		/// <summary>
		/// Fires the <see cref="TabFontChanged"/> event.
		/// </summary>
		/// <param name="ea">
		/// Some <see cref="EventArgs"/>.
		/// </param>
		protected virtual void OnTabFontChanged( EventArgs ea )
		{
			if( TabFontChanged != null )
			{
				TabFontChanged( this, ea );
			}
		}

		/// <summary>
		/// Fires the <see cref="TabDrawerChanged"/> event.
		/// </summary>
		/// <param name="ea">
		/// Some <see cref="EventArgs"/>.
		/// </param>
		protected virtual void OnTabDrawerChanged( EventArgs ea )
		{
			if( TabDrawerChanged != null )
			{
				TabDrawerChanged( this, ea );
			}
		}

		/// <summary>
		/// Fires the <see cref="TabDockChanged"/> event.
		/// </summary>
		/// <param name="ea">
		/// Some <see cref="EventArgs"/>.
		/// </param>
		protected virtual void OnTabDockChanged( EventArgs ea )
		{
			if( TabDockChanged != null )
			{
				TabDockChanged( this, ea );
			}
		}

		/// <summary>
		/// Fires the <see cref="MarginChanged"/> event.
		/// </summary>
		/// <param name="ea">
		/// Some <see cref="EventArgs"/>.
		/// </param>
		protected virtual void OnMarginChanged( EventArgs ea )
		{
			if( MarginChanged != null )
			{
				MarginChanged( this, ea );
			}
		}

		/// <summary>
		/// Fires the <see cref="InactiveColorChanged"/> event.
		/// </summary>
		/// <param name="ea">
		/// Some <see cref="EventArgs"/>.
		/// </param>
		protected virtual void OnInactiveColorChanged( EventArgs ea )
		{
			if( InactiveColorChanged != null )
			{
				InactiveColorChanged( this, ea );
			}
		}

		/// <summary>
		/// Fires the <see cref="ActiveColorChanged"/> event.
		/// </summary>
		/// <param name="ea">
		/// Some <see cref="EventArgs"/>.
		/// </param>
		protected virtual void OnActiveColorChanged( EventArgs ea )
		{
			if( ActiveColorChanged != null )
			{
				ActiveColorChanged( this, ea );
			}
		}

		/// <summary>
		/// Fires the <see cref="BorderColorChanged"/> event.
		/// </summary>
		/// <param name="ea">
		/// Some <see cref="EventArgs"/>.
		/// </param>
		protected virtual void OnBorderColorChanged( EventArgs ea )
		{
			if( BorderColorChanged != null )
			{
				BorderColorChanged( this, ea );
			}
		}

		/// <summary>
		/// Fires the <see cref="TabChanging"/> event.
		/// </summary>
		/// <param name="tcea">
		/// Some <see cref="TabChangingEventArgs"/> for the event.
		/// </param>
		protected virtual void OnTabChanging( TabChangingEventArgs tcea )
		{
			if( TabChanging != null )
			{
				TabChanging( this, tcea );
			}
		}

		/// <summary>
		/// Fires the <see cref="TabChanged"/> event.
		/// </summary>
		/// <param name="ea">
		/// Some <see cref="EventArgs"/> for the event.
		/// </param>
		protected virtual void OnTabChanged( EventArgs ea )
		{
			if( TabChanged != null )
			{
				TabChanged( this, ea );
			}
		}

		#endregion

		#region Protected Overridden Methods

		/// <summary>
		/// Overridden. Inherited from <see cref="Control"/>.
		/// </summary>
		/// <param name="mea">
		/// See <see cref="Control.OnMouseDown(MouseEventArgs)"/>.
		/// </param>
		protected override void OnMouseDown( MouseEventArgs mea )
		{
			base.OnMouseDown( mea );
			Point p = new Point( mea.X - 2 * OMMargin, mea.Y );
			switch( OMTabDock )
			{
				case DockStyle.Bottom:
					p.Y -= OMClientRectangle.Height;
					break;
				case DockStyle.Left:
					p.Y = mea.X;
					p.X = Height - mea.Y;
					break;
				case DockStyle.Right:
					p.Y = Width - mea.X;
					p.X = mea.Y;
					break;
			}
			if( p.Y > OMMargin && p.Y < Convert.ToInt32( OMTabSpan + 3.0f * OMMargin ) )
			{
				if( ( OMShowScrollButtons == OMScrollButtonStyle.Always || ( OMShowScrollButtons == OMScrollButtonStyle.Auto && OMTotalTabSpan > calcWidth ) ) && p.X >= rightArrow[ 0 ].X - 3 * OMMargin )
				{
					if( canScrollRight )
					{
						OMKeepScrolling = true;
						ScrollerThread st = new ScrollerThread( 2, this );
						Thread t = new Thread( new ThreadStart( st.ScrollIt ) );
						t.Start();
					}
				}
				else if( ( OMShowScrollButtons == OMScrollButtonStyle.Always || ( OMShowScrollButtons == OMScrollButtonStyle.Auto && OMTotalTabSpan > calcWidth ) ) && p.X >= leftArrow[ 2 ].X - 3 * OMMargin )
				{
					if( canScrollLeft )
					{
						OMKeepScrolling = true;
						ScrollerThread st = new ScrollerThread( -2, this );
						Thread t = new Thread( new ThreadStart( st.ScrollIt ) );
						t.Start();
					}
				}
				else
				{
					int t = -Convert.ToInt32( OMTabLeftDif );
					for( int i = 0; i <= OMLastVisibleTabIndex; i++ )
					{
						if( p.X >= t && p.X < t + Convert.ToInt32( OMTabLengths[ i ] ) )
						{
							SelectedIndex = i;
							break;
						}
						t += Convert.ToInt32( OMTabLengths[ i ] );
					}
				}
			}
		}

		/// <summary>
		/// Overridden. Inherited from <see cref="Control"/>.
		/// </summary>
		/// <param name="mea">
		/// Some <see cref="MouseEventArgs"/>.
		/// </param>
		protected override void OnMouseUp( MouseEventArgs mea )
		{
			lock( this )
			{
				OMKeepScrolling = false;
			}
			base.OnMouseUp( mea );
		}

		/// <summary>
		/// Overridden. Inherited from <see cref="Control"/>.
		/// </summary>
		/// <param name="cea">
		/// See <see cref="Control.OnControlAdded(ControlEventArgs)"/>.
		/// </param>
		protected override void OnControlAdded( ControlEventArgs cea )
		{
			base.OnControlAdded( cea );
			cea.Control.Visible = false;
			if( OMSelectedIndex == -1 )
			{
				OMSelectedIndex = 0;
				OMSelectedTab = ( OMTabPage ) cea.Control;
				OMSelectedTab.Visible = true;
			}
			cea.Control.TextChanged += ChildTextChangeEventHandler;
			CalculateTabLengths();
			CalculateLastVisibleTabIndex();
			InU();
		}

		/// <summary>
		/// Overridden. Inherited from <see cref="Control"/>.
		/// </summary>
		/// <param name="cea">
		/// See <see cref="Control.OnControlRemoved(ControlEventArgs)"/>.
		/// </param>
		protected override void OnControlRemoved( ControlEventArgs cea )
		{
			cea.Control.TextChanged -= ChildTextChangeEventHandler;
			base.OnControlRemoved( cea );
			if( Controls.Count > 0 )
			{
				OMSelectedIndex = 0;
				OMSelectedTab.Visible = false;
				OMSelectedTab = ( OMTabPage ) Controls[ 0 ];
				OMSelectedTab.Visible = true;
			}
			else
			{
				OMSelectedIndex = -1;
				OMSelectedTab = null;
			}
			CalculateTabLengths();
			CalculateLastVisibleTabIndex();
			InU();
		}

		/// <summary>
		/// Inherited from <see cref="Control"/>.
		/// </summary>
		/// <param name="disposing">
		/// See <see cref="Control.Dispose(bool)"/>.
		/// </param>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				base.Dispose( disposing );
				OMInactiveBrush.Dispose();
				OMActiveBrush.Dispose();
				OMForeBrush.Dispose();
				OMHighlightPen.Dispose();
				OMShadowPen.Dispose();
				OMBorderPen.Dispose();
				if( OMTabDrawer != null )
				{
					OMTabDrawer.Dispose();
				}
				foreach( Control c in Controls )
				{
					c.Dispose();
				}
			}
		}

		/// <summary>
		/// Inherited from <see cref="Control"/>.
		/// </summary>
		/// <param name="pea">
		/// See <see cref="Control.OnSizeChanged(EventArgs)"/>.
		/// </param>
		protected override void OnPaint( PaintEventArgs pea )
		{
			if( Controls.Count > 0 )
			{
				CalculateTabLengths();

				bool invert = false;
				// Create a transformation given the orientation of the tabs.
				switch( OMTabDock )
				{
					case DockStyle.Bottom:
						invert = true;
						pea.Graphics.TranslateTransform( Convert.ToSingle( calcWidth ), Convert.ToSingle( calcHeight ) );
						pea.Graphics.RotateTransform( 180.0f );
						break;
					case DockStyle.Left:
						pea.Graphics.TranslateTransform( 0, Convert.ToSingle( Height ) );
						pea.Graphics.RotateTransform( -90.0f );
						break;
					case DockStyle.Right:
						pea.Graphics.TranslateTransform( Convert.ToSingle( Width ), 0 );
						pea.Graphics.RotateTransform( 90.0f );
						break;
				}

				// Paint the areas.
				pea.Graphics.FillRectangle( OMInactiveBrush, OMTabsRectangle );
				pea.Graphics.FillRectangle( OMActiveBrush, OMClientRectangle );

				// Draws the highlight/shadow line, if applicable.
				Pen p = OMBorderPen;
				if( OMTabDrawer != null && OMTabDrawer.UsesHighlghts )
				{
					if( DockStyle.Right == OMTabDock || DockStyle.Bottom == OMTabDock )
					{
						p = OMShadowPen;
					}
					else
					{
						p = OMHighlightPen;
					}
				}
				pea.Graphics.DrawLine( p, 0, OMClientRectangle.Y, calcWidth, OMClientRectangle.Y );

				// Save the current transform so that we can go back to it
				// after printing the tabs.
				Matrix m = pea.Graphics.Transform;
				SizeF s = new SizeF( 0, OMTabSpan + 2.0f * OMMargin + 1.0f );

				// If a tab drawer exists, use it.
				if( OMTabDrawer != null )
				{
					if( !invert )
					{
						pea.Graphics.TranslateTransform( 2.0f * Convert.ToSingle( OMMargin ) - OMTabLeftDif, OMMargin + 1.0f );
					}
					else
					{
						pea.Graphics.TranslateTransform( Convert.ToSingle( Width ) - 2.0f * OMMargin - Convert.ToSingle( OMTabLengths[ 0 ] ) + OMTabLeftDif, OMMargin + 1.0f );
					}
					// The transform to the selected tab.
					Matrix selTransform = null;

					// Draw the tabs from left to right skipping over the
					// selected tab.
					for( int i = 0; i <= OMLastVisibleTabIndex; i++ )
					{
						s.Width = Convert.ToSingle( OMTabLengths[ i ] );
						if( i != OMSelectedIndex )
						{
							OMTabDrawer.DrawTab( OMActiveColor, OMInactiveColor, OMHighlightPen.Color, OMShadowPen.Color, OMBorderPen.Color, false, OMTabDock, pea.Graphics, s );
						}
						else
						{
							selTransform = pea.Graphics.Transform;
						}
						if( invert )
						{
							if( i + 1 < OMTabLengths.Count )
							{
								pea.Graphics.TranslateTransform( -Convert.ToSingle( OMTabLengths[ i + 1 ] ), 0.0f );
							}
						}
						else
						{
							pea.Graphics.TranslateTransform( s.Width, 0.0f );
						}
					}

					// Now, draw the selected tab.
					if( selTransform != null )
					{
						pea.Graphics.Transform = selTransform;
						s.Width = Convert.ToSingle( OMTabLengths[ OMSelectedIndex ] );
						OMTabDrawer.DrawTab( OMActiveColor, OMInactiveColor, OMHighlightPen.Color, OMShadowPen.Color, OMBorderPen.Color, true, OMTabDock, pea.Graphics, s );
					}
				}

				// Draw the strings. If the tabs are docked on the bottom, change
				// the tranformation to draw the string right-side up.
				if( !invert )
				{
					pea.Graphics.Transform = m;
					pea.Graphics.TranslateTransform( 2.0f * OMMargin - OMTabLeftDif, OMMargin );
				}
				else
				{
					pea.Graphics.ResetTransform();
					pea.Graphics.TranslateTransform( 2.0f * OMMargin - OMTabLeftDif, OMClientRectangle.Height );
				}
				OMTabPage ytp = null;
				for( int i = 0; i <= OMLastVisibleTabIndex; i++ )
				{
					s.Width = Convert.ToSingle( OMTabLengths[ i ] );
					ytp = Controls[ i ] as OMTabPage;
					if( ytp != null && images != null && ytp.ImageIndex > -1 && ytp.ImageIndex < images.Images.Count && images.Images[ ytp.ImageIndex ] != null )
					{
						pea.Graphics.DrawImage( images.Images[ ytp.ImageIndex ], 1.5f * OMMargin, OMMargin );
						if( i != OMSelectedIndex )
						{
							pea.Graphics.DrawString( Controls[ i ].Text, OMTabFont, OMForeBrush, 2.5f * OMMargin + images.Images[ ytp.ImageIndex ].Width, 1.5f * OMMargin );
						}
						else
						{
							pea.Graphics.DrawString( Controls[ i ].Text, OMBoldTabFont, OMForeBrush, 2.5f * OMMargin + images.Images[ ytp.ImageIndex ].Width, 1.5f * OMMargin );
						}
					}
					else if( ytp != null && images != null && defaultImageIndex > -1 && defaultImageIndex < images.Images.Count && images.Images[ defaultImageIndex ] != null )
					{
						pea.Graphics.DrawImage( images.Images[ defaultImageIndex ], 1.5f * OMMargin, OMMargin );
						if( i != OMSelectedIndex )
						{
							pea.Graphics.DrawString( Controls[ i ].Text, OMTabFont, OMForeBrush, 2.5f * OMMargin + images.Images[ defaultImageIndex ].Width, 1.5f * OMMargin );
						}
						else
						{
							pea.Graphics.DrawString( Controls[ i ].Text, OMBoldTabFont, OMForeBrush, 2.5f * OMMargin + images.Images[ defaultImageIndex ].Width, 1.5f * OMMargin );
						}
					}
					else
					{
						if( i != OMSelectedIndex )
						{
							pea.Graphics.DrawString( Controls[ i ].Text, OMTabFont, OMForeBrush, 1.5f * OMMargin, 1.5f * OMMargin );
						}
						else
						{
							pea.Graphics.DrawString( Controls[ i ].Text, OMBoldTabFont, OMForeBrush, 1.5f * OMMargin, 1.5f * OMMargin );
						}
					}
					pea.Graphics.TranslateTransform( s.Width, 0 );
				}

				// Draw the scroll buttons, if necessary
				canScrollLeft = canScrollRight = false;
				if( OMShowScrollButtons == OMScrollButtonStyle.Always || ( OMShowScrollButtons == OMScrollButtonStyle.Auto && OMTotalTabSpan > calcWidth ) )
				{
					if( invert )
					{
						pea.Graphics.ResetTransform();
						pea.Graphics.TranslateTransform( 0, OMClientRectangle.Height );
					}
					else
					{
						pea.Graphics.Transform = m;
					}
					pea.Graphics.FillRectangle( OMInactiveBrush, calcWidth - 2 * OMTabsRectangle.Height, 0, 2 * OMTabsRectangle.Height, OMTabsRectangle.Height );
					pea.Graphics.DrawRectangle( OMBorderPen, calcWidth - 2 * OMTabsRectangle.Height, 0, 2 * OMTabsRectangle.Height, OMTabsRectangle.Height );
					if( ( ( OMShowScrollButtons == OMScrollButtonStyle.Always && OMTotalTabSpan > calcWidth - 2 * Convert.ToInt32( OMTabsRectangle.Height ) ) || ( OMShowScrollButtons == OMScrollButtonStyle.Auto && OMTotalTabSpan > calcWidth ) ) && OMTabLeftDif < OMTotalTabSpan - Convert.ToSingle( OMTabLengths[ OMTabLengths.Count - 1 ] ) )
					{
						canScrollLeft = true;
						pea.Graphics.FillPolygon( OMBorderPen.Brush, leftArrow );
					}
					if( OMTabLeftDif > 0 )
					{
						canScrollRight = true;
						pea.Graphics.FillPolygon( OMBorderPen.Brush, rightArrow );
					}
					pea.Graphics.DrawPolygon( OMBorderPen, leftArrow );
					pea.Graphics.DrawPolygon( OMBorderPen, rightArrow );
				}
			}

			// Reset the transform and draw the border.
			pea.Graphics.ResetTransform();
			pea.Graphics.DrawRectangle( OMBorderPen, 0, 0, ClientRectangle.Width - 1, ClientRectangle.Height - 1 );
		}

		/// <summary>
		/// Inherited from <see cref="Control"/>.
		/// </summary>
		/// <param name="e">
		/// See <see cref="Control.OnSizeChanged(EventArgs)"/>.
		/// </param>
		protected override void OnSizeChanged( EventArgs e )
		{
			base.OnSizeChanged( e );
			CalculateRectangles();
			CalculateLastVisibleTabIndex();
			if( OMTabLengths.Count > 0 && OMTabLeftDif >= OMTotalTabSpan - Convert.ToSingle( OMTabLengths[ OMTabLengths.Count - 1 ] ) )
			{
				OMTabLeftDif = 0;
				ScrollTabs( -Convert.ToInt32( OMTotalTabSpan - Convert.ToSingle( OMTabLengths[ OMTabLengths.Count - 1 ] ) ) );
			}
			PerformLayout();
			InU();
		}

		/// <summary>
		/// Overriden from <see cref="Control"/>.
		/// </summary>
		/// <returns>
		/// A <see cref="OMTabControl.ControlCollection"/>.
		/// </returns>
		protected override System.Windows.Forms.Control.ControlCollection CreateControlsInstance()
		{
			return new OMTabControl.ControlCollection( this );
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Handles when the text changes for a control.
		/// </summary>
		/// <param name="sender">
		/// The <see cref="OMTabPage"/> whose text changed.
		/// </param>
		/// <param name="e">
		/// Some <see cref="EventArgs"/>.
		/// </param>
		private void OMTabPage_TextChanged( object sender, EventArgs e )
		{
			CalculateTabLengths();
			CalculateLastVisibleTabIndex();
		}

		/// <summary>
		/// Calculates the last visible tab shown on the control.
		/// </summary>
		private void CalculateLastVisibleTabIndex()
		{
			OMLastVisibleTabLeft = 0.0f;
			float t = 0.0f;
			for( int i = 0; i < OMTabLengths.Count; i++ )
			{
				OMLastVisibleTabIndex = i;
				t += Convert.ToSingle( OMTabLengths[ i ] ) + 2.0f;
				if( t > calcWidth + OMTabLeftDif )
				{
					break;
				}
				OMLastVisibleTabLeft = t;
			}
		}

		/// <summary>
		/// Calculates and caches the length of each tab given the value
		/// of the <see cref="Control.Text"/> property of each
		/// <see cref="OMTabPage"/>.
		/// </summary>
		private void CalculateTabLengths()
		{
			OMTotalTabSpan = 0.0f;
			OMTabLengths.Clear();
			Graphics g = CreateGraphics();
			float f = 0.0f;
			OMTabPage ytp;
			for( int i = 0; i < Controls.Count; i++ )
			{
				f = g.MeasureString( Controls[ i ].Text, OMBoldTabFont ).Width + 4.0f * Convert.ToSingle( OMMargin );
				ytp = Controls[ i ] as OMTabPage;
				if( ytp != null && images != null && ytp.ImageIndex > -1 && ytp.ImageIndex < images.Images.Count && images.Images[ ytp.ImageIndex ] != null )
				{
					f += images.Images[ ytp.ImageIndex ].Width + 2.0f * OMMargin;
				}
				else if( ytp != null && images != null && defaultImageIndex > -1 && defaultImageIndex < images.Images.Count && images.Images[ defaultImageIndex ] != null )
				{
					f += images.Images[ defaultImageIndex ].Width + 2.0f * OMMargin;
				}
				OMTabLengths.Add( f );
				OMTotalTabSpan += f;
			}
		}

		/// <summary>
		/// Calculates the span of a tab given the value of the <see cref="Font"/>
		/// property.
		/// </summary>
		private void CalculateTabSpan()
		{
			OMTabSpan = 0;
			if( images != null )
			{
				for( int i = 0; i < images.Images.Count; i++ )
				{
					OMTabSpan = Math.Max( OMTabSpan, images.Images[ i ].Height );
				}
			}
			OMTabSpan = Math.Max( OMTabSpan, CreateGraphics().MeasureString( @"ABCDEFGHIJKLMNOPQURSTUVWXYZabcdefghijklmnopqrstuvwxyz", OMTabFont ).Height + 1.0f );
		}

		/// <summary>
		/// Calculates the rectangles for the tab area, the client area,
		/// the display area, and the transformed display area.
		/// </summary>
		private void CalculateRectangles()
		{
			int spanAndMargin = Convert.ToInt32( OMTabSpan ) + 3 * OMMargin + 2;
			Size s;
			
			calcHeight = ( DockStyle.Top == OMTabDock || DockStyle.Bottom == OMTabDock )? Height : Width;
			calcWidth = ( DockStyle.Top == OMTabDock || DockStyle.Bottom == OMTabDock )? Width : Height;

			OMTabsRectangle.X = 0;
			OMTabsRectangle.Y = 0;
			s = OMTabsRectangle.Size;
			s.Width = calcWidth;
			s.Height = spanAndMargin;
			OMTabsRectangle.Size = s;

			leftArrow[ 0 ].X = s.Width - s.Height - OMMargin;
			leftArrow[ 0 ].Y = OMMargin;
			leftArrow[ 1 ].X = leftArrow[ 0 ].X;
			leftArrow[ 1 ].Y = s.Height - OMMargin;
			leftArrow[ 2 ].X = s.Width - 2 * s.Height + OMMargin;
			leftArrow[ 2 ].Y = s.Height / 2;

			rightArrow[ 0 ].X = s.Width - s.Height + OMMargin;
			rightArrow[ 0 ].Y = OMMargin;
			rightArrow[ 1 ].X = rightArrow[ 0 ].X;
			rightArrow[ 1 ].Y = s.Height - OMMargin;
			rightArrow[ 2 ].X = s.Width - OMMargin;
			rightArrow[ 2 ].Y = s.Height / 2;

			OMClientRectangle.X = 0;
			OMClientRectangle.Y = spanAndMargin;
			s = OMClientRectangle.Size;
			s.Width = calcWidth;
			s.Height = calcHeight - spanAndMargin;
			OMClientRectangle.Size = s;

			OMDisplayRectangle.X = OMMargin + 1;
			OMDisplayRectangle.Y = spanAndMargin + OMMargin + 1;
			s = OMDisplayRectangle.Size;
			s.Width = calcWidth - 2 * ( OMMargin + 1 );
			s.Height = OMClientRectangle.Size.Height - 2 * OMMargin - 2;
			OMDisplayRectangle.Size = s;

			switch( OMTabDock )
			{
				case DockStyle.Top:
					OMTransformedDisplayRectangle.Location = OMDisplayRectangle.Location;
					OMTransformedDisplayRectangle.Size = OMDisplayRectangle.Size;
					break;
				case DockStyle.Bottom:
					OMTransformedDisplayRectangle.X = OMMargin + 1;
					OMTransformedDisplayRectangle.Y = OMMargin + 1;
					OMTransformedDisplayRectangle.Size = OMDisplayRectangle.Size;
					break;
				case DockStyle.Right:
					OMTransformedDisplayRectangle.X = OMMargin + 1;
					OMTransformedDisplayRectangle.Y = OMMargin + 1;
					s.Height = OMDisplayRectangle.Size.Width;
					s.Width = OMDisplayRectangle.Size.Height;
					OMTransformedDisplayRectangle.Size = s;
					break;
				case DockStyle.Left:
					OMTransformedDisplayRectangle.X = OMDisplayRectangle.Top;
					OMTransformedDisplayRectangle.Y = calcWidth - OMDisplayRectangle.Right;
					s.Height = OMDisplayRectangle.Size.Width;
					s.Width = OMDisplayRectangle.Size.Height;
					OMTransformedDisplayRectangle.Size = s;
					break;
			}
		}

		/// <summary>
		/// Invalidates and updates the <see cref="OMTabControl"/>.
		/// </summary>
		private void InU()
		{
			Invalidate();
			Update();
		}

		/// <summary>
		/// Monitors when child <see cref="OMTabPage"/>s have their
		/// <see cref="OMTabPage.Text"/> property changed.
		/// </summary>
		/// <param name="sender">A <see cref="OMTabPage"/>.</param>
		/// <param name="ea">Some <see cref="EventArgs"/>.</param>
		private void ChildTabTextChanged( object sender, EventArgs ea )
		{
			CalculateTabLengths();
			InU();
		}

		#endregion

		#region Private Members

		/// <summary>
		/// The index to use as the default image for the tabs.
		/// </summary>
		private int defaultImageIndex;

		/// <summary>
		/// The <see cref="ImageList"/> used to draw the images in
		/// the tabs.
		/// </summary>
		private ImageList images;

		/// <summary>
		/// A flag to indicate if the tabs can scroll left.
		/// </summary>
		private bool canScrollLeft;

		/// <summary>
		/// A flag to indicate if the tabs can scroll right.
		/// </summary>
		private bool canScrollRight;

		/// <summary>
		/// A flag to indicate if scroll buttons should get drawn.
		/// </summary>
		private OMScrollButtonStyle OMShowScrollButtons;

		/// <summary>
		/// The array of floats whose each entry measures a tab's width.
		/// </summary>
		private ArrayList OMTabLengths;

		/// <summary>
		/// The sum of the lengths of all the tabs.
		/// </summary>
		private float OMTotalTabSpan;

		/// <summary>
		/// The margin around the visible <see cref="OMTabPage"/>.
		/// </summary>
		private int OMMargin;

		/// <summary>
		/// The span of the tabs. Used as the height/width of the
		/// tabs, depending on the orientation.
		/// </summary>
		private float OMTabSpan;

		/// <summary>
		/// The amount that the tabs have been scrolled to the left.
		/// </summary>
		private float OMTabLeftDif;

		/// <summary>
		/// The <see cref="Point"/>s that define the left scroll arrow.
		/// </summary>
		private Point[] leftArrow;

		/// <summary>
		/// The <see cref="Point"/>s that define the right scroll arrow.
		/// </summary>
		private Point[] rightArrow;

		/// <summary>
		/// The index of the last visible tab.
		/// </summary>
		private int OMLastVisibleTabIndex;

		/// <summary>
		/// The length from the left of the tab control
		/// to the left of the last visible tab.
		/// </summary>
		private float OMLastVisibleTabLeft;

		/// <summary>
		/// The brush used to draw the strings in the tabs.
		/// </summary>
		private Brush OMForeBrush;

		/// <summary>
		/// The color of the active tab and area.
		/// </summary>
		private Color OMActiveColor;

		/// <summary>
		/// The brush used to color the active-colored area.
		/// </summary>
		private Brush OMActiveBrush;

		/// <summary>
		/// The color of the inactive areas.
		/// </summary>
		private Color OMInactiveColor;

		/// <summary>
		/// The brush used to color the inactive-colored area.
		/// </summary>
		private Brush OMInactiveBrush;

		/// <summary>
		/// The pen used to draw the highlight lines.
		/// </summary>
		private Pen OMHighlightPen;

		/// <summary>
		/// The pen used to draw the shadow lines.
		/// </summary>
		private Pen OMShadowPen;

		/// <summary>
		/// The pen used to draw the border.
		/// </summary>
		private Pen OMBorderPen;

		/// <summary>
		/// The index of the selected tab.
		/// </summary>
		private int OMSelectedIndex;

		/// <summary>
		/// The currently selected tab.
		/// </summary>
		private OMTabPage OMSelectedTab;

		/// <summary>
		/// The side on which the tabs get docked.
		/// </summary>
		private DockStyle OMTabDock;

		/// <summary>
		/// The rectangle in which the tabs get drawn.
		/// </summary>
		private Rectangle OMTabsRectangle;

		/// <summary>
		/// The rectangle in which the client gets drawn.
		/// </summary>
		private Rectangle OMClientRectangle;

		/// <summary>
		/// The rectangle in which the currently selected
		/// <see cref="OMTabPage"/> gets drawn oriented as
		/// if the tabs were docked to the top of the control.
		/// </summary>
		private Rectangle OMDisplayRectangle;

		/// <summary>
		/// The rectangle transformed for the <see cref="DisplayRectangle"/>
		/// property to return.
		/// </summary>
		private Rectangle OMTransformedDisplayRectangle;

		/// <summary>
		/// The height used to calculate the rectangles.
		/// </summary>
		private int calcHeight;

		/// <summary>
		/// The width used to calculate the rectangles.
		/// </summary>
		private int calcWidth;

		/// <summary>
		/// The regular font used to draw the strings in the tabs.
		/// </summary>
		private Font OMTabFont;

		/// <summary>
		/// The bold font used to draw the strings in the active tab.
		/// </summary>
		private Font OMBoldTabFont;

		/// <summary>
		/// The <see cref="OMTabDrawer"/> used to draw the
		/// tabs.
		/// </summary>
		private OMTabDrawer OMTabDrawer;

		/// <summary>
		/// Used to monitor the text changing of a <see cref="OMTabPage" />.
		/// </summary>
		private EventHandler ChildTextChangeEventHandler;

		/// <summary>
		/// Used to monitor if a person has elected to scroll the tabs.
		/// </summary>
		private bool OMKeepScrolling;

		#endregion

		#region Private Inner Classes

		/// <summary>
		/// Let's the tabs scroll.
		/// </summary>
		private class ScrollerThread
		{
			/// <summary>
			/// Creates a new instance of the
			/// <see cref="OMTabControl.ScrollerThread"/> class.
			/// </summary>
			/// <param name="amount">The amount to scroll.</param>
			/// <param name="control">The control to scroll.</param>
			public ScrollerThread( int amount, OMTabControl control )
			{
				this.tabControl = control;
				this.amount = new object[] { amount };
				scroller = new ScrollTabsDelegate( tabControl.ScrollTabs );
			}

			/// <summary>
			/// Scrolls the tabs on the <see cref="OMTabControl"/>
			/// by the given amount.
			/// </summary>
			public void ScrollIt()
			{
				bool keepScrolling = false;
				lock( tabControl )
				{
					keepScrolling = tabControl.OMKeepScrolling;
				}
				while( keepScrolling )
				{
					
					tabControl.Invoke( scroller, amount );
					lock( tabControl )
					{
						keepScrolling = tabControl.OMKeepScrolling;
					}
				}
			}

			/// <summary>
			/// The control to scroll.
			/// </summary>
			private OMTabControl tabControl;

			/// <summary>
			/// The amount to scroll.
			/// </summary>
			private object[] amount;

			/// <summary>
			/// A delegate to scroll the tabs.
			/// </summary>
			private ScrollTabsDelegate scroller;

			/// <summary>
			/// A delegate to use in scrolling the tabs.
			/// </summary>
			private delegate void ScrollTabsDelegate( int amount );
		}

		#endregion

		#region Public Inner Classes

		/// <summary>
		/// A <see cref="OMTabControl"/>-specific
		/// <see cref="Control.ControlCollection"/>.
		/// </summary>
		public new class ControlCollection : Control.ControlCollection
		{
			/// <summary>
			/// Creates a new instance of the
			/// <see cref="OMTabControl.ControlCollection"/> class with 
			/// the specified <i>owner</i>.
			/// </summary>
			/// <param name="owner">
			/// The <see cref="OMTabControl"/> that owns this collection.
			/// </param>
			/// <exception cref="ArgumentNullException">
			/// Thrown if <i>owner</i> is <b>null</b>.
			/// </exception>
			/// <exception cref="ArgumentException">
			/// Thrown if <i>owner</i> is not a <see cref="OMTabControl"/>.
			/// </exception>
			public ControlCollection( Control owner ) : base( owner )
			{
				if( owner == null )
				{
					throw new ArgumentNullException( "owner", "Tried to create a OMTabControl.ControlCollection with a null owner." );
				}
				this.owner = owner as OMTabControl;
				if( this.owner == null )
				{
					throw new ArgumentException( "Tried to create a OMTabControl.ControlCollection with a non-OMTabControl owner.", "owner" );
				}
				monitor = new EventHandler( this.owner.ChildTabTextChanged );
			}

			/// <summary>
			/// Overridden. Adds a <see cref="Control"/> to the
			/// <see cref="OMTabControl"/>.
			/// </summary>
			/// <param name="value">
			/// The <see cref="Control"/> to add, which must be a
			/// <see cref="OMTabPage"/>.
			/// </param>
			/// <exception cref="ArgumentNullException">
			/// Thrown if <i>value</i> is <b>null</b>.
			/// </exception>
			/// <exception cref="ArgumentException">
			/// Thrown if <i>value</i> is not a <see cref="OMTabPage"/>.
			/// </exception>
			public override void Add( Control value )
			{
				if( value == null )
				{
					throw new ArgumentNullException( "value", "Tried to add a null value to the OMTabControl.ControlCollection." );
				}
				OMTabPage p = value as OMTabPage;
				if( p == null )
				{
					throw new ArgumentException( "Tried to add a non-OMTabPage control to the OMTabControl.ControlCollection.", "value" );
				}
				p.SendToBack();
				base.Add( p );
				p.TextChanged += monitor;
			}

			/// <summary>
			/// Overridden. Inherited from <see cref="Control.ControlCollection.Remove( Control )"/>.
			/// </summary>
			/// <param name="value"></param>
			public override void Remove( Control value )
			{
				value.TextChanged -= monitor;
				base.Remove( value );
			}

			/// <summary>
			/// Overridden. Inherited from <see cref="Control.ControlCollection.Clear()"/>.
			/// </summary>
			public override void Clear()
			{
				foreach( Control c in this )
				{
					c.TextChanged -= monitor;
				}
				base.Clear();
			}

			/// <summary>
			/// The owner of this <see cref="OMTabControl.ControlCollection"/>.
			/// </summary>
			private OMTabControl owner;

			private EventHandler monitor;
		}

		#endregion
	}
}
