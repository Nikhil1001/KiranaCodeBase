using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace JitControls
{
	/// <summary>
	/// Summary description for OMTabPage.
	/// </summary>
	[Designer( typeof( JitControls.OMTabPageDesigner ) )]
	public class OMTabPage : ContainerControl
	{
		/// <summary>
		/// Creates an instance of the <see cref="OMTabPage"/> class.
		/// </summary>
		public OMTabPage()
		{
			base.Dock = DockStyle.Fill;
			imgIndex = -1;
		}

		/// <summary>
		/// Gets or sets the index to the image displayed on this tab.
		/// </summary>
		/// <value>
		/// The zero-based index to the image in the <see cref="TabControl.ImageList"/>
		/// that appears on the tab. The default is -1, which signifies no image.
		/// </value>
		/// <exception cref="ArgumentException">
		/// The <see cref="ImageIndex"/> value is less than -1.
		/// </exception>
		public int ImageIndex
		{
			get
			{
				return imgIndex;
			}
			set
			{
				imgIndex = value;
			}
		}

		/// <summary>
		/// Overridden from <see cref="Panel"/>.
		/// </summary>
		/// <remarks>
		/// Since the <see cref="OMTabPage"/> exists only
		/// in the context of a <see cref="OMTabControl"/>,
		/// it makes sense to always have it fill the
		/// <see cref="OMTabControl"/>. Hence, this property
		/// will always return <see cref="DockStyle.Fill"/>
		/// regardless of how it is set.
		/// </remarks>
		public override DockStyle Dock
		{
			get
			{
				return base.Dock;
			}
			set
			{
				base.Dock = DockStyle.Fill;
			}
		}

		/// <summary>
		/// Only here so that it shows up in the property panel.
		/// </summary>
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
			}
		}

		/// <summary>
		/// Overriden from <see cref="Control"/>.
		/// </summary>
		/// <returns>
		/// A <see cref="OMTabPage.ControlCollection"/>.
		/// </returns>
		protected override System.Windows.Forms.Control.ControlCollection CreateControlsInstance()
		{
			return new OMTabPage.ControlCollection( this );
		}

		/// <summary>
		/// The index of the image to use for the tab that represents this
		/// <see cref="OMTabPage"/>.
		/// </summary>
		private int imgIndex;

		/// <summary>
		/// A <see cref="OMTabPage"/>-specific
		/// <see cref="Control.ControlCollection"/>.
		/// </summary>
		public new class ControlCollection : Control.ControlCollection
		{
			/// <summary>
			/// Creates a new instance of the
			/// <see cref="OMTabPage.ControlCollection"/> class with 
			/// the specified <i>owner</i>.
			/// </summary>
			/// <param name="owner">
			/// The <see cref="OMTabPage"/> that owns this collection.
			/// </param>
			/// <exception cref="ArgumentNullException">
			/// Thrown if <i>owner</i> is <b>null</b>.
			/// </exception>
			/// <exception cref="ArgumentException">
			/// Thrown if <i>owner</i> is not a <see cref="OMTabPage"/>.
			/// </exception>
			public ControlCollection( Control owner ) : base( owner )
			{
				if( owner == null )
				{
					throw new ArgumentNullException( "owner", "Tried to create a OMTabPage.ControlCollection with a null owner." );
				}
				OMTabPage c = owner as OMTabPage;
				if( c == null )
				{
					throw new ArgumentException( "Tried to create a OMTabPage.ControlCollection with a non-OMTabPage owner.", "owner" );
				}
			}

			/// <summary>
			/// Overridden. Adds a <see cref="Control"/> to the
			/// <see cref="OMTabPage"/>.
			/// </summary>
			/// <param name="value">
			/// The <see cref="Control"/> to add, which must not be a
			/// <see cref="OMTabPage"/>.
			/// </param>
			/// <exception cref="ArgumentNullException">
			/// Thrown if <i>value</i> is <b>null</b>.
			/// </exception>
			/// <exception cref="ArgumentException">
			/// Thrown if <i>value</i> is a <see cref="OMTabPage"/>.
			/// </exception>
			public override void Add( Control value )
			{
				if( value == null )
				{
					throw new ArgumentNullException( "value", "Tried to add a null value to the OMTabPage.ControlCollection." );
				}
				OMTabPage p = value as OMTabPage;
				if( p != null )
				{
					throw new ArgumentException( "Tried to add a OMTabPage control to the OMTabPage.ControlCollection.", "value" );
				}
				base.Add( value );
			}
		}
	}
}
