﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace JitControls
{
    public class DBIPPrinter
    {
        private const char ESC = (char)(27); //ESC
        private const char LF = '\n'; //Linefeed
        private const char FS = (char)(28);
        private const char GS = (char)(29);
        private const char FF = (char)(12);
        private string mSendData;
        //private string IPAddress, Port;

        public DBIPPrinter()
        {
            mSendData = "";
            InitializePrinter();
        }

        public void SetData(string strMessage)
        {
            mSendData += strMessage + LF;
        }

        public void InitializePrinter()
        {
            mSendData = ESC + "@";
        }

        public bool PrintData(string IPAddress, string Port)
        {
            IPAddress ip;
            try
            {
                mSendData += LF;
                mSendData += "--------------------------------" + LF + LF + LF + LF + LF + LF + LF + LF;
                ip = System.Net.IPAddress.Parse(IPAddress);

                Socket s = new Socket(AddressFamily.InterNetwork,
                                SocketType.Stream, ProtocolType.Tcp);

                byte[] Data = System.Text.Encoding.ASCII.GetBytes(mSendData);
                IPEndPoint ipep = new IPEndPoint(ip, int.Parse(Port));
                s.Connect(ipep);
                s.Send(Data, Data.Length, SocketFlags.None);

                s.Close();
                return true;
            }
            catch (System.Exception )
            {
                return false;
            }
        }
    }
}
