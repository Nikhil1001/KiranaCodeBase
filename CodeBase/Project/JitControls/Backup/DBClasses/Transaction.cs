using System;
using System.Data;
using System.Configuration;
using System.Web;
//using System.Web.Security;
using System.Data.SqlClient;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;
//using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Tran_Qry_Dset
/// </summary>
/// 
namespace JitControls
{
    public class Transaction
    {
   
        public class GetDataSet
        {
            private string StrErrorMsg;

            public object ErrorMessage
            {
                get
                {
                    return StrErrorMsg;
                }
            }

            public DataSet FillDset(string TbName, string str, string ConStr)
            {

                SqlConnection cn = null;
                SqlDataAdapter Adp = null;
                SqlCommand sqlCom = null;
                sqlCom = new SqlCommand();
                
                try
                {
                    cn = new SqlConnection(ConStr);
                    cn.Open();
                    sqlCom.Connection = cn;
                    sqlCom.CommandText = str;
                    sqlCom.CommandTimeout = 500;

                    if (cn.State == ConnectionState.Open)
                    {
                        Adp = new SqlDataAdapter(sqlCom);
                        DataSet DSet = new DataSet();

                        Adp.Fill(DSet, TbName);
                        return DSet;
                    }
                    else
                    {
                        StrErrorMsg = "Connection Not Successful";
                        return null;
                    }
                }
                catch (Exception e)
                {
                    StrErrorMsg = e.Message;
                    return null;
                }
                finally
                {
                    cn.Close();
                }
                //____________________________________________________________________________________________________________________________________________________________________________________________________________________________
            }

        }

        public class QueryOutPut
        {

            private string StrErrorMsg;

            public object ErrorMessage
            {
                get
                {
                    return StrErrorMsg;
                }
            }
            public int ReturnInteger(string StrQuery, string ConStr)
            {

                SqlConnection cn = null;
                SqlDataAdapter Adp = null;
                int IntReturnValue = 0;
                try
                {
                    cn = new SqlConnection(ConStr);
                    cn.Open();

                    if (cn.State == ConnectionState.Open)
                    {
                        Adp = new SqlDataAdapter(StrQuery, cn);
                        DataSet DSet = new DataSet();
                        Adp.Fill(DSet, "NewTable");
                        if (DSet.Tables[0].Rows.Count > 0)
                        {
                            if (DSet.Tables[0].Rows[0].IsNull(0) == false)
                            {
                                StrErrorMsg = "";
                                IntReturnValue = System.Convert.ToInt32(DSet.Tables[0].Rows[0][0]);
                            }
                            else
                            {
                                StrErrorMsg = "Null Value";
                                IntReturnValue = 0;
                            }
                        }
                        else
                        {
                            StrErrorMsg = "No Record Found";
                            IntReturnValue = 0;
                        }

                    }
                    else
                    {
                        StrErrorMsg = "Connection Not Successful";
                        IntReturnValue = 0;
                    }
                    return IntReturnValue;
                }
                catch (Exception e)
                {
                    StrErrorMsg = e.Message;
                    return 0;
                }
                finally
                {
                    cn.Close();
                }
                //______________________________________________________________________________________________________________________________________________________________________________________________________________________
            }
            public double ReturnDouble(string StrQuery, string ConStr)
            {
                SqlConnection cn = null;
                SqlDataAdapter Adp = null;
                double DblReturnValue = 0;

                try
                {
                    cn = new SqlConnection(ConStr);
                    cn.Open();

                    if (cn.State == ConnectionState.Open)
                    {
                        Adp = new SqlDataAdapter(StrQuery, cn);
                        DataSet DSet = new DataSet();
                        Adp.Fill(DSet, "NewTable");
                        if (DSet.Tables[0].Rows.Count > 0)
                        {
                            if (DSet.Tables[0].Rows[0].IsNull(0) == false)
                            {
                                StrErrorMsg = "";
                                DblReturnValue = System.Convert.ToDouble(DSet.Tables[0].Rows[0][0]);
                            }
                            else
                            {
                                StrErrorMsg = "Null Value";
                                DblReturnValue = 0;
                            }
                        }
                        else
                        {
                            StrErrorMsg = "No Record Found";
                            DblReturnValue = 0;
                        }
                    }
                    else
                    {
                        StrErrorMsg = "Connection Not Successful";
                        DblReturnValue = 0;
                    }
                    return DblReturnValue;
                }
                catch (Exception e)
                {
                    StrErrorMsg = e.Message;
                    return 0;
                }
                finally
                {
                    cn.Close();
                }
                //_________________________________________________________________________________________________________________________________________________________________________________________________________________
            }
            public long ReturnLong(string StrQuery, string ConStr)
            {
                SqlConnection cn = null;
                SqlDataAdapter Adp = null;
                long LngReturnValue = 0;

                try
                {
                    cn = new SqlConnection(ConStr);
                    cn.Open();

                    if (cn.State == ConnectionState.Open)
                    {
                        Adp = new SqlDataAdapter(StrQuery, cn);
                        DataSet DSet = new DataSet();
                        Adp.Fill(DSet, "NewTable");
                        if (DSet.Tables[0].Rows.Count > 0)
                        {
                            if (DSet.Tables[0].Rows[0].IsNull(0) == false)
                            {
                                StrErrorMsg = "";
                                LngReturnValue = System.Convert.ToInt32(DSet.Tables[0].Rows[0][0]);
                            }
                            else
                            {
                                StrErrorMsg = "Null Value";
                                LngReturnValue = 0;
                            }
                        }
                        else
                        {
                            StrErrorMsg = "No Record Found";
                            LngReturnValue = 0;
                        }
                    }
                    else
                    {
                        StrErrorMsg = "Connection Not Successful";
                        LngReturnValue = 0;
                    }
                    return LngReturnValue;
                }
                catch (Exception e)
                {
                    StrErrorMsg = e.Message;
                    return 0;
                }
                finally
                {
                    cn.Close();
                }
                //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
            }
            public string ReturnString(string StrQuery, string ConStr)
            {

                SqlConnection cn = null;
                SqlDataAdapter Adp = null;
                string StrReturnValue = null;

                try
                {
                    cn = new SqlConnection(ConStr);
                    cn.Open();

                    if (cn.State == ConnectionState.Open)
                    {
                        Adp = new SqlDataAdapter(StrQuery, cn);
                        DataSet DSet = new DataSet();
                        Adp.Fill(DSet, "NewTable");
                        if (DSet.Tables[0].Rows.Count > 0)
                        {
                            if (DSet.Tables[0].Rows[0].IsNull(0) == false)
                            {
                                StrErrorMsg = "";
                                StrReturnValue = System.Convert.ToString(DSet.Tables[0].Rows[0][0]);
                            }
                            else
                            {
                                StrErrorMsg = "Null Value";
                                StrReturnValue = "";
                            }
                        }
                        else
                        {
                            StrErrorMsg = "No Record Found";
                            StrReturnValue = "";
                        }
                    }
                    else
                    {
                        StrErrorMsg = "Connection Not Successful";
                        StrReturnValue = "";
                    }
                    return StrReturnValue;
                }
                catch (Exception e)
                {
                    StrErrorMsg = e.Message;

                    return "";
                }
                finally
                {
                    cn.Close();
                }
                //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
            }
            public System.DateTime ReturnDate(string StrQuery, string ConStr)
            {
                SqlConnection cn = null;
                SqlDataAdapter Adp = null;
                System.DateTime DtReturnValue = DateTime.MinValue;

                try
                {
                    cn = new SqlConnection(ConStr);
                    cn.Open();

                    if (cn.State == ConnectionState.Open)
                    {
                        Adp = new SqlDataAdapter(StrQuery, cn);
                        DataSet DSet = new DataSet();
                        Adp.Fill(DSet, "NewTable");
                        if (DSet.Tables[0].Rows.Count > 0)
                        {
                            if (DSet.Tables[0].Rows[0].IsNull(0) == false)
                            {
                                StrErrorMsg = "";
                                DtReturnValue = System.Convert.ToDateTime(DSet.Tables[0].Rows[0][0]);
                            }
                            else
                            {
                                StrErrorMsg = "Null Value";
                                DtReturnValue = System.Convert.ToDateTime("1111-11-11");
                            }
                        }
                        else
                        {
                            StrErrorMsg = "No Record Found";
                            DtReturnValue = System.Convert.ToDateTime("1111-11-11");
                        }
                    }
                    else
                    {
                        StrErrorMsg = "Connection Not Successful";
                        DtReturnValue = System.Convert.ToDateTime("1111-11-11");
                    }
                    return DtReturnValue;
                }
                catch (Exception e)
                {
                    StrErrorMsg = e.Message;
                    return System.Convert.ToDateTime("1111-11-11");
                }
                finally
                {
                    cn.Close();
                }
                //__________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
            }
            public bool ReturnBoolean(string StrQuery, string ConStr)
            {
                SqlConnection cn = null;
                SqlDataAdapter Adp = null;
                bool LngReturnValue = false;

                try
                {
                    cn = new SqlConnection(ConStr);
                    cn.Open();

                    if (cn.State == ConnectionState.Open)
                    {
                        Adp = new SqlDataAdapter(StrQuery, cn);
                        DataSet DSet = new DataSet();
                        Adp.Fill(DSet, "NewTable");
                        if (DSet.Tables[0].Rows.Count > 0)
                        {
                            if (DSet.Tables[0].Rows[0].IsNull(0) == false)
                            {
                                StrErrorMsg = "";
                                LngReturnValue = System.Convert.ToBoolean(DSet.Tables[0].Rows[0][0]);
                            }
                            else
                            {
                                StrErrorMsg = "Null Value";
                                LngReturnValue = false;
                            }
                        }
                        else
                        {
                            StrErrorMsg = "No Record Found";
                            LngReturnValue = false;
                        }
                    }
                    else
                    {
                        StrErrorMsg = "Connection Not Successful";
                        LngReturnValue = false;
                    }
                    return LngReturnValue;
                }
                catch (Exception e)
                {
                    StrErrorMsg = e.Message;
                    return false;
                }
                finally
                {
                    cn.Close();
                }
                //____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
            }
            public bool checkIsNull(string FieldName, string TableName, string ConStr)
            {
                return checkIsNull(FieldName, TableName, "");
            }

            //INSTANT C# NOTE: C# does not support optional parameters. Overloaded method(s) are created above.
            //ORIGINAL LINE: Public Function checkIsNull(ByVal FieldName As String, ByVal TableName As String, Optional ByVal WhereClause As String = "") As Boolean
            public bool checkIsNull(string FieldName, string TableName, string WhereClause, string ConStr)
            {
                SqlConnection cn = null;
                SqlDataAdapter Adp = null;
                bool BlnReturnValue = false;
                string StrQuery = null;

                try
                {
                    cn = new SqlConnection(ConStr);
                    cn.Open();
                    StrQuery = "select " + FieldName + " from " + TableName + " " + WhereClause + "";
                    if (cn.State == ConnectionState.Open)
                    {
                        Adp = new SqlDataAdapter(StrQuery, cn);
                        DataSet DSet = new DataSet();
                        Adp.Fill(DSet, "NewTable");
                        if (DSet.Tables[0].Rows.Count > 0)
                        {
                            if (DSet.Tables[0].Rows[0].IsNull(0) == false)
                            {
                                BlnReturnValue = false;
                            }
                            else
                            {
                                BlnReturnValue = true;
                            }
                        }
                        else
                        {
                            StrErrorMsg = "No Record Found";
                            BlnReturnValue = true;
                        }
                    }
                    else
                    {
                        StrErrorMsg = "Connection Not Successful";
                        BlnReturnValue = true;
                    }
                    return BlnReturnValue;
                }
                catch (Exception e)
                {
                    StrErrorMsg = e.Message;
                    return true;
                }
                finally
                {
                    cn.Close();
                }
                //____________________________________________________________________________________________________________________________________________________________________________________________________________
            }
          
        }

        public class GenerateCode
        {
            private string StrErrorMsg;

            public object ErrorMessage
            {
                get
                {
                    return StrErrorMsg;
                }
            }
            public int GetCode(string TBName, string Str, string FLDName, string ConStr)
            {
                string str1 = null;
                SqlConnection cn = null;

                SqlDataAdapter Adp1 = null;
                int LngMaxCode = 0;
                str1 = "select Max(" + FLDName + ")+1 from " + TBName + " " + Str;

                cn = new SqlConnection(ConStr);
                cn.Open();

                if (cn.State == ConnectionState.Open)
                {


                    Adp1 = new SqlDataAdapter(str1, cn);
                    DataSet DSet = new DataSet();
                    DataSet DSet1 = new DataSet();
                    Adp1.Fill(DSet1, TBName);
                    if (DSet1.Tables[0].Rows.Count != -1)
                    {
                        if (DSet1.Tables[0].Rows[0].IsNull(0) == false)
                        {
                            LngMaxCode = System.Convert.ToInt32(DSet1.Tables[0].Rows[0][0]);

                            //LngMaxCode = Microsoft.VisualBasic.Conversion.Val(DSet1.Tables[0].Rows[0][0]);
                        }
                        else
                        {
                            LngMaxCode = 1;
                        }
                    }
                    else
                    {
                        LngMaxCode = 1;
                    }

                    return LngMaxCode;
                }
                else
                {
                    StrErrorMsg = "Connection Not Successful";
                    return 0;

                }
                //________________________________________________________________________________________________________________________________________________________________________________________________________
            }
            public int GetMaxCode(string TBName, string Str, string FLDName, string ConStr)
            {
                string str1 = null;
                SqlConnection cn = null;

                SqlDataAdapter Adp1 = null;
                int LngMaxCode = 0;
                str1 = "select Max(" + FLDName + ") from " + TBName + " " + Str;

                cn = new SqlConnection(ConStr);
                cn.Open();

                if (cn.State == ConnectionState.Open)
                {


                    Adp1 = new SqlDataAdapter(str1, cn);
                    DataSet DSet = new DataSet();
                    DataSet DSet1 = new DataSet();
                    Adp1.Fill(DSet1, TBName);
                    if (DSet1.Tables[0].Rows.Count != -1)
                    {
                        if (DSet1.Tables[0].Rows[0].IsNull(0) == false)
                        {
                            LngMaxCode = System.Convert.ToInt32(DSet1.Tables[0].Rows[0][0]);

                            //LngMaxCode = Microsoft.VisualBasic.Conversion.Val(DSet1.Tables[0].Rows[0][0]);
                        }
                        else
                        {
                            LngMaxCode = 1;
                        }
                    }
                    else
                    {
                        LngMaxCode = 1;
                    }

                    return LngMaxCode;
                }
                else
                {
                    StrErrorMsg = "Connection Not Successful";
                    return 0;

                }
                //________________________________________________________________________________________________________________________________________________________________________________________________________
            }
        }
        
        public class Transactions
        {
            private string StrErrorMsg;
            //private string ConStr = ConfigurationManager.ConnectionStrings["Banking"].ConnectionString;
            public string ErrorMessage
            {
                get
                {
                    return StrErrorMsg;
                }
            }
            public bool Execute(string StrInsert, string ConStr)
            {
                //_________________________________________________________________________________________________________________________________________________
                //Function Execute Will Executes the Query passed as an argument to the function and Returns Boolean Value
                //Function Will Return 'True' On successful Execution Of Query
                //Function is used to execute only Update,Insert or Delete Strings and it is not for select queries

                SqlConnection cn = null;
                SqlDataAdapter AdpInsert = null;

                cn = new SqlConnection(ConStr);
                cn.Open();
                try
                {
                    if (cn.State == ConnectionState.Open)
                    {
                        AdpInsert = new SqlDataAdapter(StrInsert, cn);
                        DataSet DSet = new DataSet();
                        AdpInsert.Fill(DSet, "NewTable");

                    }
                    else
                    {
                        StrErrorMsg = "Connection Not Successful";
                        return false;
                    }
                    return true;
                }
                catch (Exception e)
                {
                    if (e.GetBaseException().Message == "")
                    {
                        StrErrorMsg = e.Message;
                    }
                    else
                    {
                        StrErrorMsg = e.GetBaseException().Message;
                    }

                    return false;

                }
                finally
                {
                    cn.Close();
                }
                //________________________________________________________________________________________________________________________________________________________________________________________________________________________
            }
            public int ExecuteQuery(string StrInsert, string ConStr)
            {
                //_________________________________________________________________________________________________________________________________________________
                //Function Execute Will Executes the Query passed as an argument to the function and Returns Boolean Value
                //Function Will Return 'True' On successful Execution Of Query
                //Function is used to execute only Update,Insert or Delete Strings and it is not for select queries

                SqlConnection cn = null;
                SqlCommand cmd = null;

                cn = new SqlConnection(ConStr);
                cn.Open();
                try
                {
                    if (cn.State == ConnectionState.Open)
                    {
                        cmd = new SqlCommand(StrInsert, cn);
                        return cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        StrErrorMsg = "Connection Not Successful";
                        return 0;
                    }
                }
                catch (Exception e)
                {
                    if (e.GetBaseException().Message == "")
                    {
                        StrErrorMsg = e.Message;
                    }
                    else
                    {
                        StrErrorMsg = e.GetBaseException().Message;
                    }

                    return 0;
                }
                finally
                {
                    cn.Close();
                }
                //________________________________________________________________________________________________________________________________________________________________________________________________________________________
            }
            public bool ExecuteNonQuery(SqlCommand cmd, string ConStr)
            {
                //_________________________________________________________________________________________________________________________________________________
                //Function Execute Will Executes the Query passed as an argument to the function and Returns Boolean Value
                //Function Will Return 'True' On successful Execution Of Query
                //Function is used to execute only Update,Insert or Delete Strings and it is not for select queries

                SqlConnection cn = null;
                // SqlDataAdapter AdpInsert = null;

                cn = new SqlConnection(ConStr);
                cn.Open();
                cmd.Connection = cn;
                SqlTransaction myTrans;
                myTrans = cn.BeginTransaction();

                try
                {
                    if (cn.State == ConnectionState.Open)
                    {
                        cmd.Transaction = myTrans;
                        cmd.CommandType = CommandType.StoredProcedure;
                        Convert.ToBoolean(cmd.ExecuteNonQuery());
                        myTrans.Commit();
                    }
                    else
                    {
                        StrErrorMsg = "Connection Not Successful";
                        return false;
                    }
                    return true;
                }
                catch (Exception e)
                {
                    myTrans.Rollback();          
                    if (e.GetBaseException().Message == "")
                    {
                        StrErrorMsg = e.Message;
                    }
                    else
                    {
                        StrErrorMsg = e.GetBaseException().Message;
                    }

                    return false;

                }
                finally
                {
                    cn.Close();
                }
                //________________________________________________________________________________________________________________________________________________________________________________________________________________________
            }
        }                   
    }
}
