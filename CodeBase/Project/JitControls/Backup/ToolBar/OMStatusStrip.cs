﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace JitControls
{
    public class OMStatusStrip : StatusStrip
    {
        Color gradientTop = Color.Silver;
        Color gradientBottom = Color.LightGray;
        Color gradientMiddle = Color.White;
        int mCornerRadius = 1;
        bool mgradientShow = true;

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect, // x-coordinate of upper-left corner
            int nTopRect, // y-coordinate of upper-left corner
            int nRightRect, // x-coordinate of lower-right corner
            int nBottomRect, // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
         );

        public OMStatusStrip()
        {
            //gradientT = gradientTop;// Color.FromArgb(255, 144, 185, 177);
            //gradientB = gradientBottom;// Color.FromArgb(255, 53, 98, 41);
        }

        [Category("Appearance"), Description("The color to use  effect use or not of the component.")]
        public bool GradientShow
        {
            get
            {
                return this.mgradientShow;
            }
            set
            {
                this.mgradientShow = value;
                this.Invalidate();
            }
        }

        [Category("Appearance"), Description("The color to use for the top portion of the gradient fill of the component.")]
        public Color GradientTop
        {
            get
            {
                return this.gradientTop;
            }
            set
            {
                this.gradientTop = value;
                this.Invalidate();
            }
        }

        [Category("Appearance"), Description("The color to use for the bottom portion of the gradient fill of the component.")]
        public Color GradientBottom
        {
            get
            {
                return this.gradientBottom;
            }
            set
            {
                this.gradientBottom = value;
                this.Invalidate();
            }
        }

        [Category("Appearance"), Description("The color to use for the bottom portion of the gradient fill of the component.")]
        public Color GradientMiddle
        {
            get
            {
                return this.gradientMiddle;
            }
            set
            {
                this.gradientMiddle = value;
                this.Invalidate();
            }
        }

        protected override void OnPaint(PaintEventArgs pevent)
        {
            if (mgradientShow == false)
                base.OnPaint(pevent);
            else
            {

                Graphics g = pevent.Graphics;
                // Fill the background
                using (SolidBrush backgroundBrush = new SolidBrush(this.BackColor))
                {
                    g.FillRectangle(backgroundBrush, this.ClientRectangle);
                }
                // Paint the outer rounded rectangle
                g.SmoothingMode = SmoothingMode.AntiAlias;
                Rectangle outerRect = new Rectangle(ClientRectangle.X, ClientRectangle.Y, ClientRectangle.Width - 1, ClientRectangle.Height - 1);
                using (GraphicsPath outerPath = RoundedRectangle(outerRect, mCornerRadius, 0))
                {
                    using (LinearGradientBrush outerBrush = new LinearGradientBrush(outerRect, gradientTop, gradientBottom, LinearGradientMode.Vertical))
                    {
                        g.FillPath(outerBrush, outerPath);
                    }
                    using (Pen outlinePen = new Pen(gradientTop))
                    {
                        g.DrawPath(outlinePen, outerPath);
                    }
                }
                // Paint the highlight rounded rectangle
                Rectangle innerRect = new Rectangle(ClientRectangle.X, ClientRectangle.Y, ClientRectangle.Width - 1, ClientRectangle.Height);
                using (GraphicsPath innerPath = RoundedRectangle(innerRect, mCornerRadius, 2))
                {
                    using (LinearGradientBrush innerBrush = new LinearGradientBrush(innerRect, Color.FromArgb(255, gradientMiddle), Color.FromArgb(0, gradientMiddle), LinearGradientMode.Vertical))
                    {
                        g.FillPath(innerBrush, innerPath);
                    }
                }
                base.OnPaint(pevent);
                // Paint the text
                ///TextRenderer.DrawText(g, this.Text, this.Font, outerRect, this.ForeColor, Color.Transparent, TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter | TextFormatFlags.EndEllipsis);
            }
        }

        private GraphicsPath RoundedRectangle(Rectangle boundingRect, int cornerRadius, int margin)
        {
            GraphicsPath roundedRect = new GraphicsPath();
            roundedRect.AddArc(boundingRect.X + margin, boundingRect.Y + margin, cornerRadius * 1, cornerRadius * 1, 180, 90);
            roundedRect.AddArc(boundingRect.X + boundingRect.Width - margin - cornerRadius * 1, boundingRect.Y + margin, cornerRadius * 1, cornerRadius * 1, 270, 90);
            roundedRect.AddArc(boundingRect.X + boundingRect.Width - margin - cornerRadius * 1, boundingRect.Y + boundingRect.Height - margin - cornerRadius * 2, cornerRadius * 2, cornerRadius * 2, 0, 90);
            roundedRect.AddArc(boundingRect.X + margin, boundingRect.Y + boundingRect.Height - margin - cornerRadius * 1, cornerRadius * 1, cornerRadius * 1, 90, 90);
            roundedRect.CloseFigure();
            return roundedRect;
        }

    }

}

