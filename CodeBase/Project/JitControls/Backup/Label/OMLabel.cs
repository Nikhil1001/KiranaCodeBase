﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace JitControls
{
    public class OMLabel : Label
    {
        Color gradientTop = Color.Silver;
        Color gradientBottom = Color.LightGray;
        Color gradientMiddle = Color.White;
        int mCornerRadius = 3;
        int borderRadius = 10;
        bool mgradientShow = true;

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect, // x-coordinate of upper-left corner
            int nTopRect, // y-coordinate of upper-left corner
            int nRightRect, // x-coordinate of lower-right corner
            int nBottomRect, // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
         );

        public OMLabel()
        {
            //gradientT = gradientTop;// Color.FromArgb(255, 144, 185, 177);
            //gradientB = gradientBottom;// Color.FromArgb(255, 53, 98, 41);
        }

        [Category("Appearance"), Description("The color to use  effect use or not of the component.")]
        public bool GradientShow
        {
            get
            {
                return this.mgradientShow;
            }
            set
            {
                this.mgradientShow = value;
                this.Invalidate();
            }
        }
        [Category("Appearance"), Description("The gradient fill of the component Permission.")]
        public int CornerRadius
        {
            get
            {
                return this.mCornerRadius;
            }
            set
            {
                this.mCornerRadius = value;
                this.Invalidate();
            }
        }

        [Category("Appearance"), Description("The color to use for the top portion of the gradient fill of the component.")]
        public Color GradientTop
        {
            get
            {
                return this.gradientTop;
            }
            set
            {
                this.gradientTop = value;
                this.Invalidate();
            }
        }
        
        [Category("Appearance"), Description("The color to use for the bottom portion of the gradient fill of the component.")]
        public Color GradientBottom
        {
            get
            {
                return this.gradientBottom;
            }
            set
            {
                this.gradientBottom = value;
                this.Invalidate();
            }
        }

        [Category("Appearance"), Description("The color to use for the bottom portion of the gradient fill of the component.")]
        public Color GradientMiddle
        {
            get
            {
                return this.gradientMiddle;
            }
            set
            {
                this.gradientMiddle = value;
                this.Invalidate();
            }
        }

        protected override void OnPaint(PaintEventArgs pevent)
        {
            if (mgradientShow == false)
                base.OnPaint(pevent);
            else
            {
                Graphics g = pevent.Graphics;
                // Fill the background
                using (SolidBrush backgroundBrush = new SolidBrush(this.BackColor))
                {
                    g.FillRectangle(backgroundBrush, this.ClientRectangle);
                }
                // Paint the outer rounded rectangle
                g.SmoothingMode = SmoothingMode.AntiAlias;
                Rectangle outerRect = new Rectangle(ClientRectangle.X, ClientRectangle.Y, ClientRectangle.Width - 1, ClientRectangle.Height - 1);
                using (GraphicsPath outerPath = RoundedRectangle(outerRect, mCornerRadius, 0))
                {
                    using (LinearGradientBrush outerBrush = new LinearGradientBrush(outerRect, gradientTop, gradientBottom, LinearGradientMode.Vertical))
                    {
                        g.FillPath(outerBrush, outerPath);
                    }
                    using (Pen outlinePen = new Pen(gradientTop))
                    {
                        g.DrawPath(outlinePen, outerPath);
                    }
                }

                // Paint the highlight rounded rectangle
                Rectangle innerRect = new Rectangle(ClientRectangle.X, ClientRectangle.Y, ClientRectangle.Width - 1, ClientRectangle.Height / 1 - 1);
                using (GraphicsPath innerPath = RoundedRectangle(innerRect, mCornerRadius, 0))
                {
                    using (LinearGradientBrush innerBrush = new LinearGradientBrush(innerRect, Color.FromArgb(255, gradientMiddle), Color.FromArgb(0, gradientMiddle), LinearGradientMode.Vertical))
                    {
                        g.FillPath(innerBrush, innerPath);
                    }
                }
                
                // Paint the text
                TextRenderer.DrawText(g, this.Text, this.Font, outerRect, this.ForeColor, Color.Transparent, TextFormatFlags.Left | TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter | TextFormatFlags.EndEllipsis);
               // RoundedBorders();
            }
        }
        
        private GraphicsPath RoundedRectangle(Rectangle boundingRect, int cornerRadius, int margin)
        {
            GraphicsPath roundedRect = new GraphicsPath();
            roundedRect.AddArc(boundingRect.X + margin, boundingRect.Y + margin, cornerRadius * 1, cornerRadius * 1, 180, 90);
            roundedRect.AddArc(boundingRect.X + boundingRect.Width - margin - cornerRadius * 1, boundingRect.Y + margin, cornerRadius * 1, cornerRadius * 1, 170, 90);
            roundedRect.AddArc(boundingRect.X + boundingRect.Width - margin - cornerRadius * 1, boundingRect.Y + boundingRect.Height - margin - cornerRadius * 1, cornerRadius * 1, cornerRadius * 1, 0, 90);
            roundedRect.AddArc(boundingRect.X + margin, boundingRect.Y + boundingRect.Height - margin - cornerRadius * 1, cornerRadius * 1, cornerRadius * 1, 90, 90);
            roundedRect.CloseFigure();
            return roundedRect;
        }

        private void RoundedBorders()
        {
            //Ronded Form Borders
            Pen p = new Pen(Color.Black);
            int height = Height;
            int width = Width;
            borderRadius = CornerRadius;
            // 4 Border Lines
            // x1,y1 -----> x2 y2 Left Border Line
            // x3y3  -----> x4,y4 Bottom Border Line
            // x5,y5 -----> x6,y6 Right Border Line
            // x7,y7 -----> x8,y8 Top Border Line

            // x1,y1 ( Left Top), x2,y2 ( Left Bottom of Left Border), x3,y3 (Left Bottom of Bottom Border),  x4,y4 (Right Bottom of Bottom Border)
            int x1 = 0, y1 = 0, x2 = 0, y2 = Height, x3 = 0, y3 = Height, x4 = Width, y4 = Height;
            // x5,y5 ( Bottom Right) x6,y6 (Top Right) x7,y7 Right Top
            int x5 = Width, y5 = Height, x6 = Width, y6 = 0, x7 = Width, y7 = 0, x8 = 0, y8 = 0;
            System.Drawing.Drawing2D.GraphicsPath gp = new System.Drawing.Drawing2D.GraphicsPath();
            y1 = borderRadius / 2;
            x8 = borderRadius / 2;
            y2 = height - (borderRadius / 2);
            x3 = borderRadius / 2;
            x4 = Width - (borderRadius / 2);
            y5 = Height - (borderRadius / 2);
            y6 = borderRadius / 2;
            x7 = Width - (borderRadius / 2);
            // Top Left Arc
            gp.AddArc(new Rectangle(0, 0, borderRadius, borderRadius), 180, 90);
            // Left Border
            gp.AddLine(new Point(x1, y1), new Point(x2, y2));
            // Bottom Left Arc
            gp.AddArc(new Rectangle(0, height - borderRadius, borderRadius, borderRadius), 90, 90);
            // Bottom Line
            gp.AddLine(new Point(x3, y3), new Point(x4, y4));
            // Bottom Right Arc
            gp.AddArc(new Rectangle(width - borderRadius, height - borderRadius, borderRadius, borderRadius), 0, 90);
            // Right Border
            gp.AddLine(new Point(x5, y5), new Point(x6, y6));
            // Top Right Border
            gp.AddArc(width - borderRadius, 0, borderRadius, borderRadius, 270, 90);
            // Top Border
            gp.AddLine(new Point(x7, y7), new Point(x8, y8));
            gp.CloseFigure();
            this.Region = new Region(gp);
        }
    }

}

