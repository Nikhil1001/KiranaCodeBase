﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
namespace JitControls
{
    public partial class OMMessageBox : Form
    {
        static OMMessageBoxDefaultButton omdefault = OMMessageBoxDefaultButton.Button1;
        static Color GradientTop = Color.White;
        static Color GradientBottom = Color.White;
        static Color PenColor = Color.DodgerBlue;
        static Color PenForColor = Color.White;

        Button btnFocus;

        public static void SetColor(Color GTop, Color GBottom, Color PColor)
        {
            GradientTop = GTop;
            GradientBottom = GBottom;
            PenColor = PColor;
        }

        public OMMessageBox()
        {
            InitializeComponent();
            lblTitle.BackColor = JitFunctions.MessageTitleColor;
            PenColor = JitFunctions.MessageTitleColor;
            
        }

        protected override void OnPaint(PaintEventArgs e)
        {

            Rectangle rect = this.ClientRectangle;

            LinearGradientBrush brush = new LinearGradientBrush(rect, GradientTop, GradientBottom, 66);

            e.Graphics.FillRectangle(brush, rect);

            Pen pen = new Pen(PenColor, 3);
            pen.Alignment = PenAlignment.Inset;
            e.Graphics.DrawRectangle(pen, rect);

            base.OnPaint(e);

        }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);
            btnFocus.Focus();
            KeyDownFormat(this.Controls);
        }

        #region KeyDown Events
        public void KeyDownFormat(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                ctrl.KeyDown += new KeyEventHandler(CotrolKeyDown);
                if (ctrl is Panel)
                    KeyDownFormat(ctrl.Controls);
                else if (ctrl is GroupBox)
                    KeyDownFormat(ctrl.Controls);
            }
        }

        private void CotrolKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                foreach (Control ctrl in pnlShowMessage.Controls)
                {
                    if (ctrl is Button)
                    {
                        if (ctrl.Text == "Escape")
                            this.Close();
                    }
                }
            }
        }
        #endregion

        private void setMessage(string messageText)
        {

            int number = Math.Abs(messageText.Length / 30);

            if (number != 0)

                this.lblMessageText.Height = number * 25;

            this.lblMessageText.Text = messageText;

        }



        /// <summary>

        /// This method is used to add button on message box.

        /// </summary>

        /// <param name="MessageButton">MessageButton is type of OMMessageBoxButton

        /// through which I get type of button which needs to be displayed.</param>

        private void addButton(OMMessageBoxButton MessageButton)
        {

            switch (MessageButton)
            {

                case OMMessageBoxButton.OK:
                    {

                        //If type of enumButton is OK then we add OK button only.

                        Button btnOk = new Button();  //Create object of Button.

                        btnOk.Text = "OK";  //Here we set text of Button.

                        btnOk.DialogResult = DialogResult.OK;  //Set DialogResult property of button.

                        btnOk.FlatStyle = FlatStyle.Popup;  //Set flat appearence of button.

                        btnOk.FlatAppearance.BorderSize = 0;

                        btnOk.BackColor = PenColor;
                        btnOk.ForeColor = PenForColor;
                        btnOk.GotFocus += new EventHandler(ButtonMouseHover);
                        btnOk.LostFocus += new EventHandler(ButtonMouseLeave);


                        btnOk.SetBounds(pnlShowMessage.ClientSize.Width - 80, 5, 75, 25);  // Set bounds of button.

                        btnFocus = btnOk;

                        pnlShowMessage.Controls.Add(btnOk);  //Finally Add button control on panel.

                    }

                    break;

                case OMMessageBoxButton.OKCancel:
                    {

                        Button btnCancel = new Button();

                        btnCancel.Text = "Cancel";

                        btnCancel.DialogResult = DialogResult.Cancel;

                        btnCancel.FlatStyle = FlatStyle.Popup;
                        btnCancel.BackColor = PenColor;
                        btnCancel.ForeColor = PenForColor;
                        btnCancel.FlatAppearance.BorderSize = 0;
                        btnCancel.GotFocus += new EventHandler(ButtonMouseHover);
                        btnCancel.LostFocus += new EventHandler(ButtonMouseLeave);

                        btnCancel.SetBounds((pnlShowMessage.ClientSize.Width - 70), 5, 65, 25);

                        pnlShowMessage.Controls.Add(btnCancel);



                        Button btnOk = new Button();

                        btnOk.Text = "OK";

                        btnOk.DialogResult = DialogResult.OK;

                        btnOk.FlatStyle = FlatStyle.Popup;

                        btnOk.FlatAppearance.BorderSize = 0;
                        btnOk.BackColor = PenColor;
                        btnOk.ForeColor = PenForColor;
                        btnOk.GotFocus += new EventHandler(ButtonMouseHover);
                        btnOk.LostFocus += new EventHandler(ButtonMouseLeave);

                        btnOk.SetBounds((pnlShowMessage.ClientSize.Width - (btnCancel.ClientSize.Width + 5 + 80)), 5, 75, 25);

                        pnlShowMessage.Controls.Add(btnOk);

                        btnFocus = btnOk;
                        if (omdefault == OMMessageBoxDefaultButton.Button1)
                            btnFocus = btnOk;//  btnOk.Focus();
                        else if (omdefault == OMMessageBoxDefaultButton.Button2)
                            btnFocus = btnCancel; //btnCancel.Focus();

                    }

                    break;

                case OMMessageBoxButton.YesNo:
                    {

                        Button btnNo = new Button();

                        btnNo.Text = "No";

                        btnNo.DialogResult = DialogResult.No;

                        btnNo.FlatStyle = FlatStyle.Popup;
                        btnNo.BackColor = PenColor;
                        btnNo.ForeColor = PenForColor;
                        btnNo.FlatAppearance.BorderSize = 0;
                        btnNo.GotFocus += new EventHandler(ButtonMouseHover);
                        btnNo.LostFocus += new EventHandler(ButtonMouseLeave);

                        btnNo.SetBounds((pnlShowMessage.ClientSize.Width - 70), 5, 65, 25);


                        pnlShowMessage.Controls.Add(btnNo);



                        Button btnYes = new Button();

                        btnYes.Text = "Yes";

                        btnYes.DialogResult = DialogResult.Yes;

                        btnYes.FlatStyle = FlatStyle.Popup;

                        btnYes.FlatAppearance.BorderSize = 0;
                        btnYes.BackColor = PenColor;
                        btnYes.ForeColor = PenForColor;
                        btnYes.SetBounds((pnlShowMessage.ClientSize.Width - (btnNo.ClientSize.Width + 5 + 80)), 5, 75, 25);
                        btnYes.GotFocus += new EventHandler(ButtonMouseHover);
                        btnYes.LostFocus += new EventHandler(ButtonMouseLeave);

                        pnlShowMessage.Controls.Add(btnYes);

                        btnFocus = btnYes;
                        if (omdefault == OMMessageBoxDefaultButton.Button1)
                            btnFocus = btnYes;// btnNo.Focus();
                        else if (omdefault == OMMessageBoxDefaultButton.Button2)
                            btnFocus = btnNo; //btnYes.Focus();
                    }

                    break;

                case OMMessageBoxButton.YesNoCancel:
                    {

                        Button btnCancel = new Button();

                        btnCancel.Text = "Cancel";

                        btnCancel.DialogResult = DialogResult.Cancel;

                        btnCancel.FlatStyle = FlatStyle.Popup;

                        btnCancel.FlatAppearance.BorderSize = 0;
                        btnCancel.BackColor = PenColor;
                        btnCancel.ForeColor = PenForColor;
                        btnCancel.SetBounds((pnlShowMessage.ClientSize.Width - 70), 5, 65, 25);
                        btnCancel.GotFocus += new EventHandler(ButtonMouseHover);
                        btnCancel.LostFocus += new EventHandler(ButtonMouseLeave);

                        pnlShowMessage.Controls.Add(btnCancel);



                        Button btnNo = new Button();

                        btnNo.Text = "No";

                        btnNo.DialogResult = DialogResult.No;

                        btnNo.FlatStyle = FlatStyle.Popup;

                        btnNo.FlatAppearance.BorderSize = 0;
                        btnNo.BackColor = PenColor;
                        btnNo.ForeColor = PenForColor;
                        btnNo.SetBounds((pnlShowMessage.ClientSize.Width - (btnCancel.ClientSize.Width + 5 + 80)), 5, 75, 25);
                        btnNo.GotFocus += new EventHandler(ButtonMouseHover);
                        btnNo.LostFocus += new EventHandler(ButtonMouseLeave);

                        pnlShowMessage.Controls.Add(btnNo);



                        Button btnYes = new Button();

                        btnYes.Text = "Yes";

                        btnYes.DialogResult = DialogResult.Yes;

                        btnYes.FlatStyle = FlatStyle.Popup;

                        btnYes.FlatAppearance.BorderSize = 0;
                        btnYes.ForeColor = PenForColor;
                        btnYes.BackColor = PenColor;
                        btnYes.SetBounds((pnlShowMessage.ClientSize.Width - (btnCancel.ClientSize.Width + btnNo.ClientSize.Width + 10 + 80)), 5, 75, 25);
                        btnYes.GotFocus += new EventHandler(ButtonMouseHover);
                        btnYes.LostFocus += new EventHandler(ButtonMouseLeave);

                        pnlShowMessage.Controls.Add(btnYes);

                        btnFocus = btnYes;
                        if (omdefault == OMMessageBoxDefaultButton.Button1)
                            btnFocus = btnYes;// btnCancel.Focus();
                        else if (omdefault == OMMessageBoxDefaultButton.Button2)
                            btnFocus = btnNo;//btnNo.Focus();
                        else if (omdefault == OMMessageBoxDefaultButton.Button3)
                            btnFocus = btnCancel; //btnYes.Focus();
                    }

                    break;

                case OMMessageBoxButton.Escape:
                    {

                        //If type of enumButton is OK then we add OK button only.

                        Button btnOk = new Button();  //Create object of Button.

                        btnOk.Text = "Escape";  //Here we set text of Button.

                        btnOk.DialogResult = DialogResult.OK;  //Set DialogResult property of button.

                        btnOk.FlatStyle = FlatStyle.Popup;  //Set flat appearence of button.
                        btnOk.BackColor = PenColor;
                        btnOk.FlatAppearance.BorderSize = 0;
                        btnOk.ForeColor = PenForColor;
                        btnOk.SetBounds(pnlShowMessage.ClientSize.Width - 80, 5, 75, 25);  // Set bounds of button.
                        btnOk.GotFocus += new EventHandler(ButtonMouseHover);
                        btnOk.LostFocus += new EventHandler(ButtonMouseLeave);

                        btnFocus = btnOk;

                        pnlShowMessage.Controls.Add(btnOk);  //Finally Add button control on panel.

                    }

                    break;

            }

        }

        /// <summary>

        /// We can use this method to add image on message box.

        /// I had taken all images in ImageList control so that

        /// I can eaily add images. Image is displayed in

        /// PictureBox control.

        /// </summary>

        /// <param name="MessageIcon">Type of image to be displayed.</param>

        private void addIconImage(OMMessageBoxIcon MessageIcon)
        {

            switch (MessageIcon)
            {

                case OMMessageBoxIcon.Error:

                    pictureBox1.Image = imageList1.Images["Error"];  //Error is key name in imagelist control which uniqly identified images in ImageList control.

                    break;

                case OMMessageBoxIcon.Information:

                    pictureBox1.Image = imageList1.Images["Information"];

                    break;

                case OMMessageBoxIcon.Question:

                    pictureBox1.Image = imageList1.Images["Question"];

                    break;

                case OMMessageBoxIcon.Warning:

                    pictureBox1.Image = imageList1.Images["Warning"];

                    break;

            }

        }


        public void ButtonMouseHover(object sender, EventArgs e)
        {
            ((Button)sender).FlatAppearance.BorderSize = 1;
            ((Button)sender).FlatAppearance.BorderColor = Color.Red;
        }

        public void ButtonMouseLeave(object sender, EventArgs e)
        {
            if (((Button)sender).Focused == false)
            {
                ((Button)sender).FlatAppearance.BorderSize = 1;
                ((Button)sender).FlatAppearance.BorderColor = Color.White;
            }
        }


        #region Overloaded Show message to display message box.



        /// <summary>

        /// Show method is overloaded which is used to display message

        /// and this is static method so that we don't need to create

        /// object of this class to call this method.

        /// </summary>

        /// <param name="messageText"></param>

        public static void Show(string messageText)
        {

            OMMessageBox OMMessage = new OMMessageBox();

            OMMessage.setMessage(messageText);

            OMMessage.lblTitle.Text = "Application";



            OMMessage.addIconImage(OMMessageBoxIcon.Information);

            OMMessage.addButton(OMMessageBoxButton.OK);
            OMMessage.Top = 0;
            OMMessage.ShowDialog();

        }



        public static void Show(string messageText, string messageTitle)
        {

            OMMessageBox OMMessage = new OMMessageBox();

            OMMessage.Text = messageTitle;

            OMMessage.setMessage(messageText);

            OMMessage.lblTitle.Text = messageTitle;

            OMMessage.addIconImage(OMMessageBoxIcon.Information);

            OMMessage.addButton(OMMessageBoxButton.OK);

            OMMessage.ShowDialog();

        }



        public static DialogResult Show(string messageText, string messageTitle, OMMessageBoxIcon messageIcon, OMMessageBoxButton messageButton)
        {

            OMMessageBox OMMessage = new OMMessageBox();

            OMMessage.setMessage(messageText);

            OMMessage.lblTitle.Text = messageTitle;

            OMMessage.addIconImage(messageIcon);

            OMMessage.addButton(messageButton);

            return OMMessage.ShowDialog();

        }

        public static DialogResult Show(string messageText, string messageTitle, OMMessageBoxButton messageButton, OMMessageBoxIcon messageIcon)
        {

            OMMessageBox OMMessage = new OMMessageBox();

            OMMessage.setMessage(messageText);

            OMMessage.lblTitle.Text = messageTitle;

            OMMessage.addIconImage(messageIcon);

            OMMessage.addButton(messageButton);

            return OMMessage.ShowDialog();

        }

        public static DialogResult Show(string messageText, string messageTitle, OMMessageBoxButton messageButton, OMMessageBoxIcon messageIcon, OMMessageBoxDefaultButton messageDefaultButton)
        {

            OMMessageBox OMMessage = new OMMessageBox();

            OMMessage.setMessage(messageText);

            OMMessage.lblTitle.Text = messageTitle;

            OMMessage.addIconImage(messageIcon);

            omdefault = messageDefaultButton;

            OMMessage.addButton(messageButton);

            return OMMessage.ShowDialog();

        }

        #endregion

    }

    public enum OMMessageBoxIcon
    {

        Error,

        Warning,

        Information,

        Question,

    }



    public enum OMMessageBoxButton
    {

        OK,

        YesNo,

        YesNoCancel,

        OKCancel,

        Escape

    }

    public enum OMMessageBoxDefaultButton
    {

        Button1,

        Button2,

        Button3

    }
}
