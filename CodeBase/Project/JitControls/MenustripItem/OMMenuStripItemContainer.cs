﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.ComponentModel;

namespace JitControls
{
    public class OMMenuStripItemContainer : ToolStripContainer
    {

        Color gradientTop = Color.FromArgb(255, 52, 100, 152);
        Color gradientBottom = Color.FromArgb(255, 52, 100, 152);

        Color gradientTopM = Color.FromArgb(255, 52, 100, 152);
        Color gradientBottomM = Color.FromArgb(255, 52, 100, 152);
        Color gradientBackcolor = Color.Transparent;
        Color gradientMiddlecolor = Color.White;
        Color gradientT, gradientB;
        int mCornerRadius = 3;

        public OMMenuStripItemContainer()
        {
            //this.TopToolStripPanel.Paint += new PaintEventHandler(TopToolStripPanel_Paint);
            //this.TopToolStripPanel.SizeChanged += new EventHandler(TopToolStripPanel_SizeChanged);
        }

        
        protected override void OnPaint(PaintEventArgs pevent)
        {
            Graphics g = pevent.Graphics;
            var rect = new Rectangle(0, 0, this.Width, this.FindForm().Height);
            using (LinearGradientBrush b = new LinearGradientBrush(
                rect, Color.FromArgb(255, 233, 236, 250), Color.FromArgb(255, 244, 247, 252), LinearGradientMode.Horizontal))
            {
                g.FillRectangle(b, rect);
            }
        }
    }
}
