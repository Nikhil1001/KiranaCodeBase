﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.ComponentModel;

namespace JitControls
{
    public class OMToolStripItemRenderer : ToolStripProfessionalRenderer
    {

        // Render custom background gradient
        protected override void OnRenderToolStripBackground(ToolStripRenderEventArgs e)
        {
            base.OnRenderToolStripBackground(e);

            using (var b = new LinearGradientBrush(e.AffectedBounds,
                Color.FromArgb(255, 250, 250, 253), Color.FromArgb(255, 233, 236, 250), LinearGradientMode.Vertical))
            {
                using (var shadow = new SolidBrush(Color.FromArgb(255, 181, 190, 206)))
                {
                    var rect = new Rectangle(0, e.ToolStrip.Height - 2, e.ToolStrip.Width, 1);
                    e.Graphics.FillRectangle(b, e.AffectedBounds);
                    e.Graphics.FillRectangle(shadow, rect);
                }
            }
        }

        // Render button selected and pressed state
        protected override void OnRenderButtonBackground(ToolStripItemRenderEventArgs e)
        {
            base.OnRenderButtonBackground(e);
            var rectBorder = new Rectangle(0, 0, e.Item.Width - 1, e.Item.Height - 1);
            var rect = new Rectangle(1, 1, e.Item.Width - 2, e.Item.Height - 2);

            if (e.Item.Selected == true || (e.Item as ToolStripButton).Checked)
            {
                using (var b = new LinearGradientBrush(rect, Color.FromArgb(255, 237, 248, 253),
                    Color.FromArgb(255, 129, 192, 224), LinearGradientMode.Vertical))
                {
                    using (var b2 = new SolidBrush(Color.FromArgb(255, 41, 153, 255)))
                    {
                        e.Graphics.FillRectangle(b2, rectBorder);
                        e.Graphics.FillRectangle(b, rect);
                    }
                }
            }
            if (e.Item.Pressed)
            {
                using (var b = new LinearGradientBrush(rect, Color.FromArgb(255, 228, 245, 252),
                     Color.FromArgb(255, 124, 177, 204), LinearGradientMode.Vertical))
                {
                    using (var b2 = new SolidBrush(Color.FromArgb(255, 41, 153, 255)))
                    {
                        e.Graphics.FillRectangle(b2, rectBorder);
                        e.Graphics.FillRectangle(b, rect);
                    }
                }
            }
        }
    }
}
