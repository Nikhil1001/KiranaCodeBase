﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace JitControls
{
    public class OMPanel :Panel
    {
        Color gradientTop =Color.Silver; //Color.FromArgb(255, 52, 100, 152);
        Color gradientBottom = Color.LightGray; //Color.FromArgb(255, 52, 100, 152);
        Color gradientMiddle = Color.White;
        int mCornerRadius = 3;
        int mborderRadius = 3;
        bool mgradientShow = true;

        public OMPanel()
        {
            this.DoubleBuffered = true;
        }
        [Category("Appearance"), Description("The color to use  effect use or not of the component.")]
        public bool GradientShow
        {
            get
            {
                return this.mgradientShow;
            }
            set
            {
                this.mgradientShow = value;
                this.Invalidate();
            }
        }
        [Category("Appearance"), Description("The gradient fill of the component Permission.")]
        public int CornerRadius
        {
            get
            {
                return this.mCornerRadius;
            }
            set
            {
                this.mCornerRadius = value;
                this.Invalidate();
            }
        }

        [Category("Appearance"), Description("The gradient border radious fill of the component Permission.")]
        public int BorderRadius
        {
            get
            {
                return this.mborderRadius;
            }
            set
            {
                this.mborderRadius = value;
                this.Invalidate();
            }
        }

        [Category("Appearance"), Description("The color to use for the top portion of the gradient fill of the component.")]
        public Color GradientTop
        {
            get
            {
                return this.gradientTop;
            }
            set
            {
                this.gradientTop = value;
                this.Invalidate();
            }
        }
        
        [Category("Appearance"), Description("The color to use for the bottom portion of the gradient fill of the component.")]
        public Color GradientBottom
        {
            get
            {
                return this.gradientBottom;
            }
            set
            {
                this.gradientBottom = value;
                this.Invalidate();
            }
        }
        [Category("Appearance"), Description("The color to use for the Middle portion of the gradient fill of the component.")]
        public Color GradientMiddle
        {
            get
            {
                return this.gradientMiddle;
            }
            set
            {
                this.gradientMiddle = value;
                this.Invalidate();
            }
        }
        

        protected override void OnPaint(PaintEventArgs pevent)
        {
            if (mgradientShow == false)
                base.OnPaint(pevent);
            else
            {
                base.OnPaint(pevent);
                this.SuspendLayout();
                Graphics g = pevent.Graphics;
                //// Fill the background
                //using (SolidBrush backgroundBrush = new SolidBrush(this.BackColor))
                //{
                //    g.FillRectangle(backgroundBrush, this.ClientRectangle);
                //}
                // Paint the outer rounded rectangle
                g.SmoothingMode = SmoothingMode.HighQuality;
                Rectangle outerRect = new Rectangle(ClientRectangle.X, ClientRectangle.Y, ClientRectangle.Width - 1, ClientRectangle.Height - 1);
                using (GraphicsPath outerPath = RoundedRectangle(outerRect, 5, 0))
                {
                    using (LinearGradientBrush outerBrush = new LinearGradientBrush(outerRect, gradientTop, gradientBottom, LinearGradientMode.Vertical))
                    {
                        g.FillPath(outerBrush, outerPath);
                    }
                    using (Pen outlinePen = new Pen(gradientTop))
                    {
                        g.DrawPath(outlinePen, outerPath);
                    }
                }
                // Paint the highlight rounded rectangle
                Rectangle innerRect = new Rectangle(ClientRectangle.X, ClientRectangle.Y, ClientRectangle.Width - 1, ClientRectangle.Height / 2 - 1);
                using (GraphicsPath innerPath = RoundedRectangle(innerRect, 5, 2))
                {
                    using (LinearGradientBrush innerBrush = new LinearGradientBrush(innerRect, Color.FromArgb(255, gradientMiddle), Color.FromArgb(0, gradientMiddle), LinearGradientMode.Vertical))
                    {
                        g.FillPath(innerBrush, innerPath);
                    }
                }
                RoundedBorders();
                // Paint the text
                TextRenderer.DrawText(g, this.Text, this.Font, outerRect, this.ForeColor, Color.Transparent, TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter | TextFormatFlags.EndEllipsis);
            }
            this.ResumeLayout();
        }

        protected override void OnResize(EventArgs eventargs)
        {
            //base.OnResize(eventargs);
            this.Invalidate();
        }
        private GraphicsPath RoundedRectangle(Rectangle boundingRect, int cornerRadius, int margin)
        {
            GraphicsPath roundedRect = new GraphicsPath();
            roundedRect.AddArc(boundingRect.X + margin, boundingRect.Y + margin, cornerRadius * 2, cornerRadius * 2, 180, 90);
            roundedRect.AddArc(boundingRect.X + boundingRect.Width - margin - cornerRadius * 2, boundingRect.Y + margin, cornerRadius * 2, cornerRadius * 2, 270, 90);
            roundedRect.AddArc(boundingRect.X + boundingRect.Width - margin - cornerRadius * 2, boundingRect.Y + boundingRect.Height - margin - cornerRadius * 2, cornerRadius * 2, cornerRadius * 2, 0, 90);
            roundedRect.AddArc(boundingRect.X + margin, boundingRect.Y + boundingRect.Height - margin - cornerRadius * 2, cornerRadius * 2, cornerRadius * 2, 90, 90);
            roundedRect.CloseFigure();
            return roundedRect;
        }

        private void RoundedBorders()
        {
            //Ronded Form Borders
            Pen p = new Pen(Color.Black);
            int height = Height;
            int width = Width;
            //  borderRadius = CornerRadius+40;
            // 4 Border Lines
            // x1,y1 -----> x2 y2 Left Border Line
            // x3y3  -----> x4,y4 Bottom Border Line
            // x5,y5 -----> x6,y6 Right Border Line
            // x7,y7 -----> x8,y8 Top Border Line

            // x1,y1 ( Left Top), x2,y2 ( Left Bottom of Left Border), x3,y3 (Left Bottom of Bottom Border),  x4,y4 (Right Bottom of Bottom Border)
            int x1 = 0, y1 = 0, x2 = 0, y2 = Height, x3 = 0, y3 = Height, x4 = Width, y4 = Height;
            // x5,y5 ( Bottom Right) x6,y6 (Top Right) x7,y7 Right Top
            int x5 = Width, y5 = Height, x6 = Width, y6 = 0, x7 = Width, y7 = 0, x8 = 0, y8 = 0;
            System.Drawing.Drawing2D.GraphicsPath gp = new System.Drawing.Drawing2D.GraphicsPath();
            y1 = mborderRadius / 2;
            x8 = mborderRadius / 2;
            y2 = height - (mborderRadius / 2);
            x3 = mborderRadius / 2;
            x4 = Width - (mborderRadius / 2);
            y5 = Height - (mborderRadius / 2);
            y6 = mborderRadius / 2;
            x7 = Width - (mborderRadius / 2);
            // Top Left Arc
            gp.AddArc(new Rectangle(0, 0, mborderRadius, mborderRadius), 180, 90);
            // Left Border
            gp.AddLine(new Point(x1, y1), new Point(x2, y2));
            // Bottom Left Arc
            gp.AddArc(new Rectangle(0, height - mborderRadius, mborderRadius, mborderRadius), 90, 90);
            // Bottom Line
            gp.AddLine(new Point(x3, y3), new Point(x4, y4));
            // Bottom Right Arc
            gp.AddArc(new Rectangle(width - mborderRadius, height - mborderRadius, mborderRadius, mborderRadius), 0, 90);
            // Right Border
            gp.AddLine(new Point(x5, y5), new Point(x6, y6));
            // Top Right Border
            gp.AddArc(width - mborderRadius, 0, mborderRadius, mborderRadius, 270, 90);
            // Top Border
            gp.AddLine(new Point(x7, y7), new Point(x8, y8));
            gp.CloseFigure();
            this.Region = new Region(gp);
        }
    }
}
