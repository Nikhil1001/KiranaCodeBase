﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace JitControls
{
    public class ServerDateTime
    {
        DateTime ServerTime;
        Process process;
        Regex reg;
        string ErrorMessage = "";

        public DateTime GetTime(string strComp, string UserName, string Password)
        {
            try
            {
                reg = new Regex(@"\d{2}.*:\d{2}.*");
                process = new Process();
                process.StartInfo.FileName = "cmd.exe";
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.CreateNoWindow = true;
                process.Start();
                process.StandardInput.WriteLine(@"net use \\" + strComp + " /user:" + UserName + " " + Password + " /persistent:yes");  //Change this to your server name or IP
                process.StandardInput.WriteLine(@"net time \\" + strComp + "");  //Change this to your server name or IP
                process.StandardInput.WriteLine("exit");
                string str = process.StandardOutput.ReadToEnd();
                str = str.Substring(str.IndexOf("is"), str.Length - str.IndexOf("is"));
                ServerTime = DateTime.Parse(reg.Match(str).Value);
                return ServerTime;
            }
            catch (Exception e)
            {
                ErrorMessage = "Error:" + e.Message;
                return Convert.ToDateTime("01-Jan-1900");

            }
        }
    }
}
