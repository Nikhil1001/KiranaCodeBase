using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;


namespace JitControls
{
    public class OMFTP
    {
        public string Username;
        public string Password;
        public string Host;
        public int Port;

        public OMFTP()
        {
            Username = "anonymous";
            Password = "anonymous@internet.com";
            Port = 21;
            Host = "";
        }

        public OMFTP(string theUser, string thePassword, string theHost,int thePort)
        {
            Username = theUser;
            Password = thePassword;
            Host = theHost;
            Port = thePort;
        }

        private Uri BuildServerUri(string Path)
        {
            return new Uri(String.Format("ftp://{0}:{1}/{2}", Host, Port, Path));
        }

        public byte[] DownloadData(string path)
        {
            // Get the object used to communicate with the server.
            WebClient request = new WebClient();
            
            // Logon to the server using username + password
            request.Credentials = new NetworkCredential(Username, Password);
            request.Proxy = null;
            return request.DownloadData(BuildServerUri(path));
        }
       
        public void DownloadFile(string ftppath, string destfile)
        {
            // Download the data
            byte[] Data = DownloadData(ftppath);

            // Save the data to disk
            FileStream fs = new FileStream(destfile, FileMode.Create);
            fs.Write(Data, 0, Data.Length);
            fs.Close();
        }
       

        public byte[] UploadData(string path, byte[] Data)
        {
            // Get the object used to communicate with the server.
            WebClient request = new WebClient();

            // Logon to the server using username + password
            request.Credentials = new NetworkCredential(Username, Password);
            request.Proxy = null;
            return request.UploadData(BuildServerUri(path), Data);
        }

        public byte[] UploadFile(string ftppath, string srcfile)
        {
            // Read the data from disk
            FileStream fs = new FileStream(srcfile, FileMode.Open);
            byte[] FileData = new byte[fs.Length];

            int numBytesToRead = (int)fs.Length;
            int numBytesRead = 0;
            while (numBytesToRead > 0)
            {
                // Read may return anything from 0 to numBytesToRead.
                int n = fs.Read(FileData, numBytesRead, numBytesToRead);

                // Break when the end of the file is reached.
                if (n == 0) break;

                numBytesRead += n;
                numBytesToRead -= n;
            }
            numBytesToRead = FileData.Length;
            fs.Close();

            // Upload the data from the buffer
            return UploadData(ftppath, FileData);
        }

        public string[] GetFileList(string FolderPath)
        {
            string[] downloadFiles;
            StringBuilder result = new StringBuilder();
            WebResponse response = null;
            StreamReader reader = null;
            try
            {
                FtpWebRequest reqFTP;
                if (FolderPath != "")
                    reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + Host + "/" + FolderPath + "/"));
                else
                    reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + Host + "/"));
                reqFTP.UseBinary = true;
                reqFTP.Credentials = new NetworkCredential(Username, Password);
                reqFTP.Method = WebRequestMethods.Ftp.ListDirectory;
                reqFTP.Proxy = null;
                reqFTP.KeepAlive = false;
                reqFTP.UsePassive = false;
                response = reqFTP.GetResponse();
                reader = new StreamReader(response.GetResponseStream());
                string line = reader.ReadLine();
                while (line != null)
                {
                    result.Append(line);
                    result.Append("\n");
                    line = reader.ReadLine();
                }
                // to remove the trailing '\n'
                result.Remove(result.ToString().LastIndexOf('\n'), 1);
                return result.ToString().Split('\n');
            }
            catch (Exception ex)
            {
                if (reader != null)
                {
                    reader.Close();
                }
                if (response != null)
                {
                    response.Close();
                }
                downloadFiles = null;
                return downloadFiles;
            }
        }

    }

}
