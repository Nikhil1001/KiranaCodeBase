using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;


namespace JitControls
{
    public class NumberToWordsIndian 
    {
        public NumberToWordsIndian()
        {
        }
        public static string getWords(string myNumber)
        {
            return SpellNumber(myNumber);
        }

        //Main Function
        private static  string SpellNumber(string MyNumber)
        {
            string Dollars = null;
            string Cents = null;
            string Temp = null;
            int DecimalPlace;
            int Count;
            string[] Place = new string[10];
            Place[2] = " Thousand ";
            Place[3] = " Lacs ";
            Place[4] = " Crore ";
            Place[5] = " Abj ";
            // String representation of amount
            MyNumber = Convert.ToString(MyNumber);
            // Position of decimal place 0 if none
            DecimalPlace = (MyNumber.IndexOf(".", 0) + 1);
            //Convert cents and set MyNumber to dollar amount
            if (DecimalPlace > 0)
            {
                Cents = GetTens(((string)(MyNumber.Substring(DecimalPlace) + "00")).Substring(0, 2));
                MyNumber = System.Convert.ToString(MyNumber.Substring(0, DecimalPlace - 1)).Trim(' ');
            }
            Count = 1;
            if (MyNumber == "0")
            {
                MyNumber = "";
                Dollars = "";
                Cents = "";
            }
            while (MyNumber != "")
            {
                if (Count <= 1)
                {
                    //Temp = GetHundreds(MyNumber.Substring(MyNumber.Length - 3));
                    if (MyNumber.Length == 1)
                    {
                        //MyNumber = "";
                        Temp = GetDigit(MyNumber.Substring(MyNumber.Length - 1));
                        MyNumber = MyNumber.Substring(0, MyNumber.Length - 1);
                    }
                    else if (MyNumber.Length == 2)
                    {
                        //MyNumber = "";
                        Temp = GetTens(MyNumber.Substring(MyNumber.Length - 2));
                        MyNumber = MyNumber.Substring(0, MyNumber.Length - 2);
                    }
                    else
                    {
                        Temp = GetHundreds(MyNumber.Substring(MyNumber.Length - 3));
                        MyNumber = MyNumber.Substring(0, MyNumber.Length - 3);
                    }
                }
                else
                {
                    if (MyNumber.Length >= 2)
                    {
                        Temp = GetTens(MyNumber.Substring(MyNumber.Length - 2));
                        MyNumber = MyNumber.Substring(0, MyNumber.Length - 2);
                    }
                    else
                    {
                        Temp = GetDigit(MyNumber.Substring(MyNumber.Length - 1));
                        MyNumber = "";
                    }

                }
                if (Temp != "")
                {
                    Dollars = Temp + Place[Count] + Dollars;
                }

                Count = Count + 1;
            }
            switch (Dollars)
            {
                case "":
                    Dollars = "zero Rupees";
                    break;
                case "One":
                    Dollars = "One Rupee";
                    break;
                default:
                    Dollars = Dollars.ToString() + " Rupees";
                    break;
            }
            switch (Cents)
            {
                case "":
                    Cents = " and zero Paisa";
                    break;
                case "One":
                    Cents = " and One Paisa";
                    break;
                default:
                    if (Cents != null)
                        Cents = " and " + Cents.ToString() + " Paisa";
                    else
                        Cents = " and Zero Paisa";
                    break;
            }
            return Dollars + Cents;
        }

        //Converts a number from 100-999 into text
        private static string GetHundreds(string MyNumber)
        {
            string Result = null;
            if (Convert.ToInt64(MyNumber) == 0)
            {
                return null;
            }
            MyNumber = ((string)("000" + MyNumber)).Substring(((string)("000" + MyNumber)).Length - 3);
            //Convert the hundreds place
            if (MyNumber.Substring(0, 1) != "0")
            {
                Result = GetDigit(MyNumber.Substring(0, 1)) + " Hundred ";
            }
            //Convert the tens and ones place
            if (MyNumber.Substring(1, 1) != "0")
            {
                Result = Result + GetTens(MyNumber.Substring(1));
            }
            else
            {
                Result = Result + GetDigit(MyNumber.Substring(2));
            }
            return Result;
        }

        //Converts a number from 10 to 99 into text
        private static string GetTens(string TensText)
        {
            string Result = null;
            Result = ""; //null out the temporary function value
            if (Convert.ToInt64(TensText.Substring(0, 1)) == 1) // If value between 10-19
            {
                switch (Convert.ToInt64(TensText))
                {
                    case 10:
                        Result = "Ten";
                        break;
                    case 11:
                        Result = "Eleven";
                        break;
                    case 12:
                        Result = "Twelve";
                        break;
                    case 13:
                        Result = "Thirteen";
                        break;
                    case 14:
                        Result = "Fourteen";
                        break;
                    case 15:
                        Result = "Fifteen";
                        break;
                    case 16:
                        Result = "Sixteen";
                        break;
                    case 17:
                        Result = "Seventeen";
                        break;
                    case 18:
                        Result = "Eighteen";
                        break;
                    case 19:
                        Result = "Nineteen";
                        break;
                    default:
                        break;
                }
            }
            else // If value between 20-99
            {
                switch (Convert.ToInt64(TensText.Substring(0, 1)))
                {
                    case 2:
                        Result = "Twenty ";
                        break;
                    case 3:
                        Result = "Thirty ";
                        break;
                    case 4:
                        Result = "Forty ";
                        break;
                    case 5:
                        Result = "Fifty ";
                        break;
                    case 6:
                        Result = "Sixty ";
                        break;
                    case 7:
                        Result = "Seventy ";
                        break;
                    case 8:
                        Result = "Eighty ";
                        break;
                    case 9:
                        Result = "Ninety ";
                        break;
                    default:
                        break;
                }
                Result = Result + GetDigit(TensText.Substring(TensText.Length - 1)); //Retrieve ones place
            }
            return Result;
        }

        //Converts a number from 1 to 9 into text
        private static string GetDigit(string Digit)
        {
            string tempGetDigit = null;
            switch (Convert.ToInt64(Digit))
            {
                case 1:
                    tempGetDigit = "One";
                    break;
                case 2:
                    tempGetDigit = "Two";
                    break;
                case 3:
                    tempGetDigit = "Three";
                    break;
                case 4:
                    tempGetDigit = "Four";
                    break;
                case 5:
                    tempGetDigit = "Five";
                    break;
                case 6:
                    tempGetDigit = "Six";
                    break;
                case 7:
                    tempGetDigit = "Seven";
                    break;
                case 8:
                    tempGetDigit = "Eight";
                    break;
                case 9:
                    tempGetDigit = "Nine";
                    break;
                default:
                    tempGetDigit = "";
                    break;
            }
            return tempGetDigit;
        }
    }
}