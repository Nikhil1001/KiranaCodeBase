﻿namespace JitControls
{
    using System;
    using System.IO;
    using System.IO.Packaging;

    public class OMWinZip
    {
        private string strErrorMsg = "";

        public void AddToArchive(Package zip, string fileToAdd)
        {
            try
            {
                string path = fileToAdd.Replace(" ", "_");
                Uri partUri = new Uri("/" + Path.GetFileName(path), UriKind.Relative);
                string contentType = "application/zip";
                PackagePart part = zip.CreatePart(partUri, contentType, CompressionOption.Normal);
                byte[] buffer = File.ReadAllBytes(fileToAdd);
                part.GetStream().Write(buffer, 0, buffer.Length);
            }
            catch (Exception exception)
            {
                this.strErrorMsg = exception.Message;
            }
        }

        public void ConvertFileToZip(string strZipPath, string strArchiveFilePath)
        {
            try
            {
                string path = strZipPath;
                Package zip = Package.Open(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                this.AddToArchive(zip, strArchiveFilePath);
                zip.Close();
                File.Delete(strArchiveFilePath);
            }
            catch (Exception exception)
            {
                this.strErrorMsg = exception.Message;
            }
        }

        public void ConvertFileToZip(string strZipPath, string[] strArchiveFilePath)
        {
            try
            {
                string path = strZipPath;
                Package zip = Package.Open(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                for (int i = 0; i < strArchiveFilePath.Length; i++)
                {
                    this.AddToArchive(zip, strArchiveFilePath[i]);
                }
                zip.Close();
            }
            catch (Exception exception)
            {
                this.strErrorMsg = exception.Message;
            }
        }
    }
}

