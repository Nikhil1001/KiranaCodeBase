﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Management;
using System.Data.SqlClient;
using System.Net.NetworkInformation;
using System.Security.AccessControl;
using System.Net;
using System.Threading;


namespace JitControls
{
    public class JitFunctions
    {
        string strErrorMsg;
        public static Color RowColor = Color.FromArgb(255, 224, 192);
        public static Color MessageTitleColor;
        DataSet dset = new DataSet();
        Transaction.GetDataSet objTrans = new Transaction.GetDataSet();


        public bool CheckValidAmount(string strAmt)
        {
            int cnt = 0;
            if (CheckNumeric(strAmt.Replace(".", "")) == true)
            {
                for (int i = 0; i <= strAmt.Length - 1; i++)
                {
                    if (strAmt[i] == '.')
                    {
                        cnt = cnt + 1;
                    }
                    if (cnt > 1)
                    {
                        return false;
                    }
                }
            }
            else
                return false;

            return true;
        }

        public bool CheckValidDate(string strDate)
        {
            System.DateTime dt = DateTime.MinValue;
            try
            {
                dt = Convert.ToDateTime(strDate);
                return true;
            }
            catch (Exception e)
            {
                strErrorMsg = e.Message;
                return false;
            }
        }
        public bool CheckNumeric(string strAmt)
        {
            bool flag = false;
            for (int i = 0; i <= strAmt.Length - 1; i++)
            {
                int ch = (int)strAmt[i];
                if ((ch >= 48 && ch <= 57))
                {
                    flag = true;
                }
                else if (ch == '-')
                {
                    flag = true;
                }
                else
                {
                    flag = false;
                    break;
                }
            }
            return flag;
        }
        public bool CheckValidEmailID(string strEmailID)
        {
            try
            {
                bool flag = false;
                string str = strEmailID, strTemp = "", strTemp1 = "";
                if (str != "")
                {
                    int index = str.IndexOf("@"), tempindex = 0;
                    if (index != -1)
                    {
                        int cnt = 0;
                        for (int i = 0; i < str.Length; i++)
                        {
                            if (str[i].ToString() == "@")
                                cnt++;
                        }
                        if (cnt > 1)
                        {
                            flag = false;

                        }
                        else
                        {
                            strTemp = str; cnt = 0;
                            while (true)
                            {
                                strTemp = strTemp.Substring(index + 1, strTemp.Length - index - 1);
                                index = strTemp.IndexOf(".");
                                if (index == -1)
                                {
                                    if (cnt == 0)
                                    {
                                        flag = false;
                                    }
                                    else
                                        flag = true;
                                    break;
                                }
                                else
                                {
                                    cnt = cnt + 1;
                                    strTemp1 = strTemp.Substring(index + 1, strTemp.Length - index - 1);
                                    if (strTemp1.Length == 0)
                                    {
                                        flag = false;
                                        break;
                                    }
                                    else
                                    {
                                        if (strTemp1[0].ToString() == ".")
                                        {
                                            flag = false;
                                            break;
                                        }
                                        else
                                            flag = true;
                                    }
                                }
                            }
                        }

                    }
                    else
                    {
                        flag = false;
                    }
                }
                else
                {
                    flag = true;
                }
                return flag;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public long GetComboValue(ComboBox cmb)
        {
            if (cmb.Items.Count > 0)
            {
                if (cmb.Text != "")
                {
                    DataTable dt = ((DataRowView)cmb.SelectedItem).Row.Table;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i].ItemArray[1].ToString() == cmb.Text)
                        {
                            if (CheckNumeric(dt.Rows[i].ItemArray[0].ToString()) == true)
                                return Convert.ToInt64(dt.Rows[i].ItemArray[0]);
                            else
                                return 0;
                        }
                    }
                    return 0;

                    //if (CheckNumeric(((DataRowView)cmb.SelectedItem).Row.Table.Rows[cmb.FindString(cmb.Text)].ItemArray[0].ToString()) == true)
                    //    return Convert.ToInt64(((DataRowView)cmb.SelectedItem).Row.Table.Rows[cmb.FindString(cmb.Text)].ItemArray[0]);
                    //else return 0;
                }
                else
                    return 0;
            }
            else return 0;
        }

        public Font GetFont()
        {
            return new Font("Verdana", 9, FontStyle.Regular);
        }
        public Font GetFont(FontStyle FT)
        {
            return new Font("Verdana", 8, FT);
        }
        public Font GetFont(FontStyle FT, int FontSize)
        {
            return new Font("Verdana", FontSize, FT);
        }
        public Font GetSimpleFont(FontStyle FT)
        {
            return new Font("Verdana", 8, FT);
        }

        public string GetFormatString(int index, string strValue)
        {
            string strTemp = strValue.Substring(1, strValue.Length - 1).ToLower();
            strValue = strValue[0].ToString().ToUpper() + strTemp;
            return strValue;
        }

        public string GetComboValueString(ComboBox cmb)
        {
            if (cmb.Items.Count > 0)
            {
                if (cmb.Text != "")
                    return Convert.ToString(((DataRowView)cmb.SelectedItem).Row.Table.Rows[cmb.FindString(cmb.Text)].ItemArray[0]);
                else
                    return "";
            }
            else return "";
        }

        public void LockControls(bool flag, System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                if (ctrl is TextBox)
                    ctrl.Enabled = flag;
                else if (ctrl is CheckBox)
                    ctrl.Enabled = flag;
                else if (ctrl is ComboBox)
                    ctrl.Enabled = flag;
                else if (ctrl is DateTimePicker)
                    ctrl.Enabled = flag;
                else if (ctrl is ListBox)
                    ctrl.Enabled = flag;
                else if (ctrl is GroupBox)
                    LockControls(flag, ctrl.Controls);
                else if (ctrl is Panel)
                    LockControls(flag, ctrl.Controls);
                else if (ctrl is MaskedTextBox)
                    ctrl.Enabled = flag;
                else if (ctrl is RadioButton)
                    ctrl.Enabled = flag;
            }
        }

        public void LockButtons(bool flag, System.Windows.Forms.Control.ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                if (ctrl is Button)
                {
                    if (ctrl.Name.ToUpper() == "BTNNEW")
                        ctrl.Visible = flag;
                    else if (ctrl.Name.ToUpper() == "BTNDELETE")
                        ctrl.Enabled = flag;
                    else if (ctrl.Name.ToUpper() == "BTNUPDATE")
                        ctrl.Visible = flag;
                    else if (ctrl.Name.ToUpper() == "BTNSEARCH")
                        ctrl.Enabled = flag;
                    else if (ctrl.Name.ToUpper() == "BTNSAVE")
                        ctrl.Visible = !flag;
                    else if (ctrl.Name.ToUpper() == "BTNCANCEL")
                        ctrl.Visible = !flag;
                    else if (ctrl.Name.ToUpper() == "BTNFIRST")
                        ctrl.Enabled = flag;
                    else if (ctrl.Name.ToUpper() == "BTNPREV")
                        ctrl.Enabled = flag;
                    else if (ctrl.Name.ToUpper() == "BTNNEXT")
                        ctrl.Enabled = flag;
                    else if (ctrl.Name.ToUpper() == "BTNLAST")
                        ctrl.Enabled = flag;
                    else if (ctrl.Name.ToUpper() == "BTNADVANCESEARCH")
                        ctrl.Enabled = !flag;
                    else if (ctrl.Name.ToUpper() == "BTNPRINT")
                        ctrl.Enabled = flag;
                    //else if (ctrl.Name.ToUpper() == "BTNORDERTYPE")
                    //    ctrl.Enabled = flag;
                    //else if (ctrl.Name.ToUpper() == "BTNPRINTBARCODE")
                    //    ctrl.Enabled = flag;

                }
                else if (ctrl is GroupBox)
                    LockButtons(flag, ctrl.Controls);
                else if (ctrl is Panel)
                    LockButtons(flag, ctrl.Controls);
            }

        }

        internal void LockButtons()
        {
            throw new NotImplementedException();
        }

        internal void LockButtons(bool p, Button button)
        {
            throw new NotImplementedException();
        }

        public void SetGridStatus(DataGridViewCellFormattingEventArgs ex)
        {

            if (Convert.ToBoolean(ex.Value.ToString()) == true)
            {
                ex.Value = "Active";
                ex.CellStyle.ForeColor = Color.Green;
            }
            else
            {
                ex.Value = "DeActive";
                ex.CellStyle.ForeColor = Color.Red;
            }
        }

        public string GetGroupQuery(int No, string ClauseName, string value, string SelectVal, int cnt)
        {
            string str = "";
            No -= 1;
            if (No >= 0)
            {
                str = " or " + ClauseName + " in (Select " + SelectVal + " From MGroup G Where " + ClauseName + "  in(" + value + ")" + GetGroupQuery(No, ClauseName, value, SelectVal, cnt) + "";
                if (No == 0)
                {
                    for (int i = 0; i <= cnt; i++)
                        str += ")";
                }
            }

            return str;
        }

        public Form GetFormObject(string frmName, DataRow[] dr)
        {
            System.Reflection.Assembly a = System.Reflection.Assembly.GetEntryAssembly();
            Type frmType = a.GetType(a.GetName().Name + "." + frmName);
            if (frmType == null)
                return null;
            else
            {
                Form frm;
                if (Convert.IsDBNull(dr[0][8]) == true)
                    frm = (Form)Activator.CreateInstance(frmType);
                else
                    frm = (Form)Activator.CreateInstance(frmType, Convert.ToInt32(dr[0][8].ToString()));
                return frm;
            }
        }

        public CrystalDecisions.CrystalReports.Engine.ReportClass GetReportObject(string frmName)
        {
            System.Reflection.Assembly a = System.Reflection.Assembly.GetEntryAssembly();
            Type frmType = a.GetType(a.GetName().Name + "." + frmName);
            if (frmType == null)
                return null;
            else
            {
                CrystalDecisions.CrystalReports.Engine.ReportClass frm = (CrystalDecisions.CrystalReports.Engine.ReportClass)Activator.CreateInstance(frmType);
                return frm;
            }
        }

        public CrystalDecisions.CrystalReports.Engine.ReportDocument LoadReportObject(string frmName, string ReportPath)
        {
            CrystalDecisions.CrystalReports.Engine.ReportDocument rpt = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
            rpt.Load(ReportPath + frmName);
            return rpt;
        }

        public void GetFinancialYear(DateTime dt, out DateTime dtFrom, out DateTime dtTo)
        {
            if (dt.Month <= 3)
            {
                dtFrom = Convert.ToDateTime("01-Apr-" + dt.AddYears(-1).Year);
                dtTo = Convert.ToDateTime("31-Mar-" + dt.Year);
            }
            else
            {
                dtFrom = Convert.ToDateTime("01-Apr-" + dt.Year);
                dtTo = Convert.ToDateTime("31-Mar-" + dt.AddYears(1).Year);
            }
        }

        public void SetMasked(TextBox txt, int NoOfDecimal)
        {
            int index = txt.Text.IndexOf(".");
            if ((index != -1) && (txt.Text.Substring(index + 1, (txt.Text.Length - index) - 1).Length > NoOfDecimal))
            {
                txt.Text = txt.Text.Remove(txt.Text.Length - 1);
                txt.Select(txt.Text.Length, 0);
            }
            if ((txt.Text.Length > 0) && !this.CheckValidAmount(txt.Text))
            {
                txt.Text = txt.Text.Remove(txt.Text.Length - 1);
                txt.Select(txt.Text.Length, 0);
            }
        }

        public void SetMasked(TextBox txt, string strRemove)
        {
            if (txt.Text.Length > 0)
            {
                for (int i = 0; i < strRemove.Length; i++)
                {
                    char ch = strRemove[i];
                    if (ch.ToString() == txt.Text.Substring(txt.Text.Length - 1, 1))
                    {
                        txt.Text = txt.Text.Remove(txt.Text.Length - 1);
                        txt.Select(txt.Text.Length, 0);
                        break;
                    }
                }
            }
        }

        public void SetMasked(Label txt, int NoOfDecimal, int NoOfDigit)
        {
            int index = txt.Text.IndexOf(".");
            if (index != -1)
            {
                if (txt.Text.Substring(0, index).Length > NoOfDigit)
                {
                    txt.Text = txt.Text.Remove(txt.Text.Length - 1);
                }
                else if (txt.Text.Substring(index + 1, (txt.Text.Length - index) - 1).Length > NoOfDecimal)
                {
                    txt.Text = txt.Text.Remove(txt.Text.Length - 1);
                }
                if (NoOfDecimal <= 0)
                {
                    txt.Text = txt.Text.Replace(".", "");
                }
            }
            else if (txt.Text.Length > NoOfDigit)
            {
                txt.Text = txt.Text.Remove(txt.Text.Length - 1);
            }
            if ((txt.Text.Length > 0) && !this.CheckValidAmount(txt.Text))
            {
                txt.Text = txt.Text.Remove(txt.Text.Length - 1);
            }
        }

        public void SetMasked(TextBox txt, int NoOfDecimal, int NoOfDigit)
        {
            int index = txt.Text.IndexOf(".");
            if (index != -1)
            {
                if (txt.Text.Substring(0, index).Length > NoOfDigit)
                {
                    txt.Text = txt.Text.Remove(txt.Text.Length - 1);
                    txt.Select(txt.Text.Length, 0);
                }
                else if (txt.Text.Substring(index + 1, (txt.Text.Length - index) - 1).Length > NoOfDecimal)
                {
                    txt.Text = txt.Text.Remove(txt.Text.Length - 1);
                    txt.Select(txt.Text.Length, 0);
                }
            }
            else if (txt.Text.Length > NoOfDigit)
            {
                txt.Text = txt.Text.Remove(txt.Text.Length - 1);
                txt.Select(txt.Text.Length, 0);
            }
            if ((txt.Text.Length > 0) && !this.CheckValidAmount(txt.Text))
            {
                txt.Text = txt.Text.Remove(txt.Text.Length - 1);
                txt.Select(txt.Text.Length, 0);
            }
        }

        public void SetMasked(TextBox txt, int NoOfDecimal, int NoOfDigit, MaskedType MSK)
        {
            int index = txt.Text.IndexOf(".");
            if (index != -1)
            {
                if (txt.Text.Substring(0, index).Length > NoOfDigit)
                {
                    txt.Text = txt.Text.Remove(txt.Text.Length - 1);
                    txt.Select(txt.Text.Length, 0);
                }
                else if (txt.Text.Substring(index + 1, (txt.Text.Length - index) - 1).Length > NoOfDecimal)
                {
                    txt.Text = txt.Text.Remove(txt.Text.Length - 1);
                    txt.Select(txt.Text.Length, 0);
                }
            }
            else if (txt.Text.Length > NoOfDigit)
            {
                txt.Text = txt.Text.Remove(txt.Text.Length - 1);
                txt.Select(txt.Text.Length, 0);
            }
            if (txt.Text.Length > 0)
            {
                if (MSK == MaskedType.NotNegative)
                {
                    index = txt.Text.IndexOf("-");
                    if (index != -1)
                    {
                        txt.Text = txt.Text.Remove(index, 1);
                        txt.Select(txt.Text.Length, 0);
                    }
                }
                if ((txt.Text.Length > 0) && !this.CheckValidAmount(txt.Text))
                {
                    txt.Text = txt.Text.Remove(txt.Text.Length - 1);
                    txt.Select(txt.Text.Length, 0);
                }
            }
        }

        public void SetMaskedNumeric(TextBox txt)
        {
            if ((txt.Text != "") && !this.CheckNumeric(txt.Text))
            {
                txt.Text = txt.Text.Remove(txt.Text.Length - 1);
                txt.Select(txt.Text.Length, 0);
            }
            if ((txt.Text.Length > 0) && !this.CheckValidAmount(txt.Text))
            {
                txt.Text = txt.Text.Remove(txt.Text.Length - 1);
                txt.Select(txt.Text.Length, 0);
            }
        }

        public enum MaskedType
        {
            PositiveNegative = 1,
            NotNegative = 2
        }


        public DateTime GetStartWeekDays(DateTime dt)
        {
            int Days = 0;
            if (dt.DayOfWeek == DayOfWeek.Sunday)
                Days = 0;
            else if (dt.DayOfWeek == DayOfWeek.Monday)
                Days = 1;
            else if (dt.DayOfWeek == DayOfWeek.Tuesday)
                Days = 2;
            else if (dt.DayOfWeek == DayOfWeek.Wednesday)
                Days = 3;
            else if (dt.DayOfWeek == DayOfWeek.Thursday)
                Days = 4;
            else if (dt.DayOfWeek == DayOfWeek.Friday)
                Days = 5;
            else if (dt.DayOfWeek == DayOfWeek.Saturday)
                Days = 6;
            return dt.AddDays(-Days);
        }

        public void AutoComplete(ref ComboBox cb, System.Windows.Forms.KeyPressEventArgs e, bool blnLimitToList)
        {

            string strFindStr = null;

            if (e.KeyChar == '\b') //Backspace
            {

                if (cb.SelectionStart <= 1)
                {
                    cb.Text = "";
                    return;
                }
                if (cb.SelectionLength == 0)
                {
                    strFindStr = cb.Text.Substring(0, cb.Text.Length - 1);
                }
                else
                {
                    strFindStr = cb.Text.Substring(0, cb.SelectionStart - 1);
                }
            }
            else
            {
                if (cb.SelectionLength == 0)
                {
                    strFindStr = cb.Text + e.KeyChar;
                }
                else
                {
                    strFindStr = cb.Text.Substring(0, cb.SelectionStart) + e.KeyChar;
                }
            }

            int intIdx = -1;

            // Search the string in the Combo Box List.

            intIdx = cb.FindString(strFindStr);

            if (intIdx != -1) // String found in the List.
            {

                cb.SelectedText = "";
                cb.SelectedIndex = intIdx;
                cb.SelectionStart = strFindStr.Length;
                cb.SelectionLength = cb.Text.Length;
                e.Handled = true;
            }
            else
            {
                if (blnLimitToList == true)
                {
                    e.Handled = true;
                }
                else
                {
                    e.Handled = false;
                }
            }
        }

        public void InitialiseControl(System.Windows.Forms.Control.ControlCollection ctrls)
        {
            //INSTANT C# NOTE: Commented this declaration since looping variables in 'foreach' loops are declared in the 'foreach' header in C#
            //		Control ctrl = null;
            foreach (Control ctrl in ctrls)
            {
                if (ctrl is TextBox)
                {
                    ctrl.Text = "";
                }
                if (ctrl is ComboBox)
                {
                    if (((ComboBox)ctrl).Items.Count > 0) ((ComboBox)ctrl).SelectedIndex = 0;
                }
                if (ctrl is System.Windows.Forms.Panel)
                {
                    InitialiseControl(ctrl.Controls);
                }
                if (ctrl is System.Windows.Forms.GroupBox)
                {
                    InitialiseControl(ctrl.Controls);
                }
                if (ctrl is System.Windows.Forms.TabControl)
                {
                    InitialiseControl(ctrl.Controls);
                }
                if (ctrl is System.Windows.Forms.TabPage)
                {
                    InitialiseControl(ctrl.Controls);
                }
                if (ctrl is MaskedTextBox)
                {
                    ctrl.Text = "";
                }
            }
        }

        public string GetDatabaseName(string connString)
        {
            object lcConnString = connString.ToLower();

            // if this is a Jet database, find the index of the "data source" setting
            int startIndex = 0;
            if (lcConnString.ToString().IndexOf("jet.oledb") > -1)
            {
                startIndex = lcConnString.ToString().IndexOf("data source=");
                if (startIndex > -1)
                {
                    startIndex += 12;
                }
            }
            else
            {
                // if this is a SQL Server database, find the index of the "initial 
                // catalog" or "database" setting
                startIndex = lcConnString.ToString().IndexOf("initial catalog=");
                if (startIndex > -1)
                {
                    startIndex += 16;
                }
                else // if the "Initial Catalog" setting is not found,
                {
                    //  try with "Database"
                    startIndex = lcConnString.ToString().IndexOf("database=");
                    if (startIndex > -1)
                    {
                        startIndex += 9;
                    }
                }
            }

            // if the "database", "data source" or "initial catalog" values are not 
            // found, return an empty string
            if (startIndex == -1)
            {
                return "";
            }

            // find where the database name/path ends
            int endIndex = lcConnString.ToString().IndexOf(";", startIndex);
            if (endIndex == -1)
            {
                endIndex = lcConnString.ToString().Length;
            }

            // return the substring with the database name/path
            return connString.Substring(startIndex, endIndex - startIndex);
        }

        public string GetServerName(string connString)
        {
            object lcConnString = connString.ToLower();

            // if this is a Jet database, find the index of the "data source" setting
            int startIndex = 0;

            // if this is a SQL Server database, find the index of the "initial 
            // catalog" or "database" setting
            startIndex = lcConnString.ToString().IndexOf("data source=");
            if (startIndex > -1)
            {
                startIndex += 12;
            }

            // if the "database", "data source" or "initial catalog" values are not 
            // found, return an empty string
            if (startIndex == -1)
            {
                return "";
            }

            // find where the database name/path ends
            int endIndex = lcConnString.ToString().IndexOf(";", startIndex);
            if (endIndex == -1)
            {
                endIndex = lcConnString.ToString().Length;
            }

            // return the substring with the database name/path
            return connString.Substring(startIndex, endIndex - startIndex);
        }

        public string GetServerUserID(string connString)
        {
            object lcConnString = connString.ToLower();

            // if this is a Jet database, find the index of the "data source" setting
            int startIndex = 0;

            // if this is a SQL Server database, find the index of the "initial 
            // catalog" or "database" setting
            startIndex = lcConnString.ToString().IndexOf("user id=");
            if (startIndex > -1)
            {
                startIndex += 8;
            }

            // if the "database", "data source" or "initial catalog" values are not 
            // found, return an empty string
            if (startIndex == -1)
            {
                return "";
            }

            // find where the database name/path ends
            int endIndex = lcConnString.ToString().IndexOf(";", startIndex);
            if (endIndex == -1)
            {
                endIndex = lcConnString.ToString().Length;
            }

            // return the substring with the database name/path
            return connString.Substring(startIndex, endIndex - startIndex);
        }

        public string GetServerPassword(string connString)
        {
            object lcConnString = connString.ToLower();

            // if this is a Jet database, find the index of the "data source" setting
            int startIndex = 0;

            // if this is a SQL Server database, find the index of the "initial 
            // catalog" or "database" setting
            startIndex = lcConnString.ToString().IndexOf("password=");
            if (startIndex > -1)
            {
                startIndex += 9;
            }

            // if the "database", "data source" or "initial catalog" values are not 
            // found, return an empty string
            if (startIndex == -1)
            {
                return "";
            }

            // find where the database name/path ends
            int endIndex = lcConnString.ToString().IndexOf(";", startIndex);
            if (endIndex == -1)
            {
                endIndex = lcConnString.ToString().Length;
            }

            // return the substring with the database name/path
            return connString.Substring(startIndex, endIndex - startIndex);
        }

        public static void AddDirectorySecurity(string FileName, FileSystemRights Rights, AccessControlType ControlType)
        {
            // Create a new DirectoryInfo object.
            string[] strUser = GetUsers();
            string Account;
            for (int i = 0; i < strUser.Length; i++)
            {
                Account = strUser[i];
                DirectoryInfo dInfo = new DirectoryInfo(FileName);

                // Get a DirectorySecurity object that represents the 
                // current security settings.
                DirectorySecurity dSecurity = dInfo.GetAccessControl();
                // FileSystemAccessRule rule = new FileSystemAccessRule(Account, FileSystemRights.FullControl, InheritanceFlags.None | InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow);

                // Add the FileSystemAccessRule to the security settings. 
                dSecurity.AddAccessRule(new FileSystemAccessRule(Account,
                                                              Rights,
                                                            ControlType));

                // Set the new access settings.

                dInfo.SetAccessControl(dSecurity);
            }
        }

        public static string[] GetUsers()
        {
            // This query will query for all user account names in our current Domain
            string[] str = new string[0];
            int cnt = 0;
            SelectQuery sQuery = new SelectQuery("Win32_UserAccount", "Domain='"
                                      + System.Environment.UserDomainName.ToString() + "'");

            try
            {
                // Searching for available Users
                ManagementObjectSearcher mSearcher = new ManagementObjectSearcher(sQuery);
                str = new string[mSearcher.Get().Count];
                foreach (ManagementObject mObject in mSearcher.Get())
                {
                    // Adding all user names in our combobox
                    //comboBox1.Items.Add(mObject["Name"]);
                    str[cnt] = System.Environment.UserDomainName.ToString() + "\\" + mObject["Name"].ToString(); cnt++;
                }
            }
            catch (Exception ex)
            {
                OMMessageBox.Show(ex.ToString());
            }
            return str;
        }

        public string GetFormatDate(string strDate)
        {
            string dd, mm, yy;
            //dd = strDate.Substring(0, 2);
            //mm = strDate.Substring(3, 2);
            //yy = strDate.Substring(6, 4);

            if (strDate.Length.Equals(10) || strDate.Length.Equals(22))
            {
                dd = strDate.Substring(0, 2);
                mm = strDate.Substring(3, 2);
                yy = strDate.Substring(6, 4);
            }
            else if (strDate.Length.Equals(21))
            {
                dd = strDate.Substring(0, 2);
                mm = strDate.Substring(3, 1);
                yy = strDate.Substring(5, 4);
            }
            else
            {
                dd = strDate.Substring(0, 1);
                mm = strDate.Substring(2, 1);
                yy = strDate.Substring(4, 4);
            }
            return mm + "/" + dd + "/" + yy;
        }

        public DataTable TransferData(DataTable dtSource)
        {
            DataTable dt = dtSource.Clone();
            for (int i = 0; i < dtSource.Rows.Count; i++)
            {
                DataRow dr = dt.NewRow();
                for (int j = 0; j < dtSource.Columns.Count; j++)
                {
                    dr[j] = dtSource.Rows[i].ItemArray[j];
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public void SetToolTip(Control ctrl, string Message, ToolTipIcon icon)
        {
            new ToolTip { IsBalloon = true, ToolTipIcon = icon, ToolTipTitle = Message, UseFading = true, UseAnimation = true }.SetToolTip(ctrl, " ");
        }

        public string ReturnFullFillVal(string strVal, int length, FillType FT, string strChar)
        {
            int lastindex = strVal.Length;
            for (int i = lastindex; i < length; i++)
            {
                if (FT == FillType.After)
                    strVal = strVal + strChar;
                else if (FT == FillType.Before)
                    strVal = strChar + strVal;
            }
            return strVal;
        }

        public enum FillType
        {
            After = 1,
            Before = 2
        }

        public string GetColumns(string tableName, string ConStr)
        {
            //         strConnect = "Server=.\\SQLEXPRESS;Initial Catalog=Retailer0002;Integrated Security=SSPI;Pooling=False";

            SqlConnection con = new SqlConnection(ConStr);
            con.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "sp_Columns " + tableName;
            cmd.CommandType = CommandType.Text;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.SelectCommand.Connection = con;

            DataSet dsDatabase = new DataSet();
            da.Fill(dsDatabase, "Columns");
            string str = "";
            for (int i = 0; i < dsDatabase.Tables["Columns"].Rows.Count; i++)
            {
                if (i == 0)
                    str = Convert.ToString(dsDatabase.Tables[0].Rows[i].ItemArray[3]);
                else
                    str = str + "," + Convert.ToString(dsDatabase.Tables[0].Rows[i].ItemArray[3]);


            }
            return str;
        }

        private string GetColumnsName(DataTable dt)
        {
            string str = "";
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                if (str == "")
                    str = "[" + dt.Columns[i].ColumnName.ToString() + "]";
                else
                    str += "," + "[" + dt.Columns[i].ColumnName.ToString() + "]";
            }
            return str;
        }

        public string GetSqlScript(DataTable dt, string TbleName)
        {
            string strMain = "", strData = "", colName = "";

            strMain = "\n\rGO \n\r SET IDENTITY_INSERT [dbo].[" + TbleName + "] ON ";
            colName = GetColumnsName(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                strData = "";
                strData = "\n INSERT [dbo].[" + TbleName + "] (" + colName + ") Values (";
                string data = "";
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    // if(dt.Columns[j].GetType().Name.ToString()==
                    string str = "";
                    if (dt.Columns[j].DataType == typeof(System.Decimal) || dt.Columns[j].DataType == typeof(System.Int32))
                    {
                        str = dt.Rows[i].ItemArray[j].ToString();
                    }
                    else if (dt.Columns[j].DataType == typeof(System.DateTime))
                    {
                        str = "'" + dt.Rows[i].ItemArray[j].ToString() + "'";
                    }
                    else if (dt.Columns[j].DataType == typeof(System.String))
                    {
                        str = "N'" + dt.Rows[i].ItemArray[j].ToString() + "'";
                    }
                    else if (dt.Columns[j].DataType == typeof(System.Boolean))
                    {
                        str = ((true == Convert.ToBoolean(dt.Rows[i].ItemArray[j].ToString())) ? "1" : "0");
                    }
                    else
                        MessageBox.Show("Data Type Not Found");

                    if (data == "")
                        data = str;
                    else
                        data += "," + str;
                }
                strData += data + ")";

                strMain = strMain + strData;
            }
            if (strData == "")
                strMain = "";
            else
                strMain = strMain + "\n\rSET IDENTITY_INSERT [dbo].[" + TbleName + "] OFF";


            return strMain;
        }
    }

    public class OMCommonClass
    {
        public static string ConStr = "";
        public static string ServerName, CompanyName;
        public int CompanyNo;
        public static long VoucherLock;
        Security secure = new Security();
        private string path, path1;
        Transaction.QueryOutPut objQry = new Transaction.QueryOutPut();
        public string ClientName;
        InstallationType MainINSType = InstallationType.None;


        public OMCommonClass() { }
        public OMCommonClass(bool fg)
        {
        }

        public void setPath(string StartName)
        {
            path1 = "C:\\Windows\\System32\\" + StartName + " Registered.dat";
            path = "C:\\Windows\\System32\\" + StartName + " RegisteredSerial.dat";
        }

        public bool IsDouble(object Expression)
        {
            // Variable to collect the Return value of the TryParse method.
            bool isNum;

            // Define variable to collect out parameter of the TryParse method. If the conversion fails, the out parameter is zero.
            double retNum;

            // The TryParse method converts a string in a specified style and culture-specific format to its double-precision floating point number equivalent.
            // The TryParse method does not generate an exception if the conversion fails. If the conversion passes, True is returned. If it does not, False is returned.
            isNum = double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        public bool IsInteger(TextBox txt)
        {
            // Variable to collect the Return value of the TryParse method.
            bool isNum = false;
            string str = txt.Text;
            for (int i = 0; i < str.Length; i++)
            {
                if (Convert.ToChar(str[i]) >= 48 && Convert.ToChar(str[i]) <= 57)
                    isNum = true;
                else
                {
                    isNum = false;
                    break;
                }
            }
            return isNum;
        }

        public bool IsVarchar(TextBox txt)
        {
            // Variable to collect the Return value of the TryParse method.
            bool isNum = false;
            string str = txt.Text;
            for (int i = 0; i < str.Length; i++)
            {
                if ((Convert.ToChar(str[i]) >= 65 && Convert.ToChar(str[i]) <= 90) || (Convert.ToChar(str[i]) >= 97 && Convert.ToChar(str[i]) <= 122) || Convert.ToChar(str[i]) == 32)
                    isNum = true;
                else
                {
                    isNum = false;
                    break;
                }
            }
            return isNum;
        }

        public bool IsAlphaNumeric(TextBox txt)
        {
            bool isNum = false;
            string str = txt.Text;
            for (int i = 0; i < str.Length; i++)
            {
                if ((Convert.ToChar(str[i]) >= 65 && Convert.ToChar(str[i]) <= 90) || (Convert.ToChar(str[i]) >= 97 && Convert.ToChar(str[i]) <= 122) || Convert.ToChar(str[i]) == 32 || Convert.ToChar(str[i]) >= 48 && Convert.ToChar(str[i]) <= 57)
                    isNum = true;
                else
                {
                    isNum = false;
                    break;
                }
            }
            return isNum;
        }
        public bool IsPhoneNo(TextBox txt)
        {
            bool isNum = false;
            string str = txt.Text;
            for (int i = 0; i < str.Length; i++)
            {
                if (Convert.ToChar(str[i]) >= 48 && Convert.ToChar(str[i]) <= 57 || Convert.ToChar(str[i]) == 45)
                    isNum = true;
                else
                {
                    isNum = false;
                    break;
                }
            }
            return isNum;
        }
        public void ReadServerName(string StartName)
        {
            string fname = "C:\\Windows\\System32\\" + StartName + " RegisteredSerial.dat";
            StreamReader objreader = new StreamReader(fname);

            string str = objreader.ReadLine();
            str = objreader.ReadLine();
            if (str != null)
                ServerName = secure.psDecrypt(str);

        }

        public bool ReadPasswords(string strPwd, string StartName)
        {
            bool flag = false;


            string fname = "C:\\Windows\\System32\\" + StartName + " RegisteredSerial.dat";
            StreamReader objreader = new StreamReader(fname);

            string str = objreader.ReadLine();
            str = objreader.ReadLine();
            str = objreader.ReadLine();
            while (str != null)
            {
                if (secure.psDecrypt(str) == strPwd)
                {
                    flag = true;
                    break;
                }
                str = objreader.ReadLine();
            }
            objreader.Close();
            objreader = null;
            return flag;
        }

        public string GetDate(DateTime dt)
        {
            string strDate;
            strDate = dt.ToString("dd");
            int mno = dt.Month;
            switch (mno)
            {
                case 1: strDate = strDate + "-" + "OmZodmar"; break;
                case 2: strDate = strDate + "-" + "\\o$~«wdmar"; break;
                case 3: strDate = strDate + "-" + "_mM©"; break;
                case 4: strDate = strDate + "-" + "E{àb"; break;
                case 5: strDate = strDate + "-" + "_o"; break;
                case 6: strDate = strDate + "-" + "OwZ"; break;
                case 7: strDate = strDate + "-" + "Owbo"; break;
                case 8: strDate = strDate + "-" + "A°mJñQ>"; break;
                case 9: strDate = strDate + "-" + "gßQ>|~a"; break;
                case 10: strDate = strDate + "-" + "A°mŠQ>m|~a"; break;
                case 11: strDate = strDate + "-" + "Zmoìh|~a"; break;
                case 12: strDate = strDate + "-" + "{S>g|~a"; break;
            }
            strDate = strDate + "-" + dt.ToString("yyyy");
            return strDate;
        }

        public string DateFormatMDY(string passdate)
        {
            string s = "";
            string sp = "";
            if (passdate != "")
            {
                if (passdate.Contains("-") | passdate.Contains("/") | passdate.Contains("."))
                {
                    s = passdate;
                    if (s.Contains("-"))
                    {
                        sp = "-";
                    }
                    else if (s.Contains("/"))
                    {
                        sp = "/";
                    }
                    else if (s.Contains("."))
                    {
                        sp = ".";
                    }
                    // Split string based on spaces
                    string[] words = s.Split(new string[] { sp }, StringSplitOptions.None);
                    // Use For Each loop over words and display them
                    int day = 0;
                    int Mon = 0;
                    int Year = 0;
                    day = Convert.ToInt32(words[0]);
                    Mon = Convert.ToInt32(words[1]);
                    Year = Convert.ToInt32(words[2]);
                    s = Mon + "/" + day + "/" + Year;
                    //Return s
                }
                else
                {
                    s = "";
                }
            }
            else
            {
                s = "";
            }
            return s;
        }

        public string GetMacName()
        {

            ManagementClass mc = new ManagementClass("Win32_NetworkAdapterConfiguration");

            ManagementObjectCollection moc = mc.GetInstances();
            string MACAddress = string.Empty;
            //INSTANT C# NOTE: Commented this declaration since looping variables in 'foreach' loops are declared in the 'foreach' header in C#
            //		ManagementObject mo = null;
            string Str = "";
            try
            {
                string strMac = null;
                string strMacNew = null;
                int len = 0;
                int noOfMac = 0;
                noOfMac = NetworkInterface.GetAllNetworkInterfaces().Length;
                //MacIDs = new string[noOfMac + 1];
                foreach (NetworkInterface NIC in NetworkInterface.GetAllNetworkInterfaces())
                {
                    if (NIC.GetPhysicalAddress().ToString() != "")
                    {
                        if (NIC.NetworkInterfaceType.ToString() == "Ethernet")
                        {
                            strMacNew = NIC.GetPhysicalAddress().ToString();
                            len = strMacNew.Length;
                            int k = 0;
                            while (k < len - 1)
                            {
                                MACAddress = (MACAddress + strMacNew.Substring(k, 2)) + ":";
                                k = k + 2;
                            }
                            MACAddress = MACAddress.Substring(0, MACAddress.Length - 1);
                            break;
                        }
                    }
                }


                if (MACAddress == "")
                {
                    Str = "In this system Local Area Network(LAN) is not configured either its Disabled. Please Configure the LAN either Enable the LAN. Thank You ....";
                    OMMessageBox.Show(Str, "Error System.", OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    Application.Exit();
                }
                return MACAddress;
            }
            catch (Exception ex)
            {
                Str = "There is One of the serious problem. Please Contact to software team. Thank You ....";
                OMMessageBox.Show(Str, "Error System.", OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                Application.Exit();
            }

            //INSTANT C# NOTE: Inserted the following 'return' since all code paths must return a value in C#:
            return null;
        }

        public string[] GetName()
        {
            string Str;
            try
            {
                //INSTANT C# NOTE: Commented this declaration since looping variables in 'foreach' loops are declared in the 'foreach' header in C#
                //			NetworkInterface NIC = null;
                string[] MacIDs = null;
                string strMac = null;
                string strMacNew = null;

                int i = 0;
                int len = 0;
                int noOfMac = 0;
                noOfMac = NetworkInterface.GetAllNetworkInterfaces().Length;
                MacIDs = new string[noOfMac + 1];
                foreach (NetworkInterface NIC in NetworkInterface.GetAllNetworkInterfaces())
                {
                    strMac = NIC.GetPhysicalAddress().ToString();
                    if (strMac != "")
                    {
                        strMacNew = "";
                        len = strMac.Length;
                        int k = 0;
                        while (k < len - 1)
                        {
                            strMacNew = (strMacNew + strMac.Substring(k, 2)) + ":";
                            k = k + 2;
                        }
                        MacIDs[i] = strMacNew.Substring(0, strMacNew.Length - 1);
                        i = i + 1;
                    }
                }
                return MacIDs;
            }
            catch (Exception ex)
            {
                Str = "There is One of the serious problem. Please Contact to software team. Thank You ....";
                OMMessageBox.Show(Str, "Error System.", OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                Application.Exit();
                return null;
            }

            //ManagementClass mc = new ManagementClass("Win32_NetworkAdapterConfiguration");
            //String[] MacIDs;
            //MacIDs = new string[2];
            //ManagementObjectCollection moc = mc.GetInstances();
            //string MACAddress = string.Empty;
            ////INSTANT C# NOTE: Commented this declaration since looping variables in 'foreach' loops are declared in the 'foreach' header in C#
            ////		ManagementObject mo = null;
            //string Str = "";
            //int i = 0;
            //try
            //{
            //    foreach (ManagementObject mo in moc)
            //    {

            //            if (Convert.ToBoolean(mo["IPEnabled"]) == true)
            //            {
            //                MACAddress = mo["MacAddress"].ToString();
            //                MacIDs[i] = MACAddress;
            //                i = i + 1;
            //            }

            //    }
            //    if ( MacIDs.Length == 0)
            //    {
            //        Str = "In this system Local Area Network(LAN) is not configured either its Disabled. Please Configure the LAN either Enable the LAN. Thank You ....";
            //        OMMessageBox.Show(Str, "Error System.", OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
            //        Application.Exit();
            //    }
            //    return  MacIDs;
            //}
            //catch (Exception ex)
            //{
            //    Str = "There is One of the serious problem. Please Contact to software team. Thank You ....";
            //    OMMessageBox.Show(Str, "Error System.", OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
            //    Application.Exit();
            //}

            ////INSTANT C# NOTE: Inserted the following 'return' since all code paths must return a value in C#:
            //return null;
        }

        public int CheckRegistration(string StrReg, int Type, string AssemblyTitle, string AssemblyProduct, string AssemblyVersion, string AssemblyCopyright, string AssemblyGUID, string DatabaseName)
        {
            int flag = 0;
            string str = null;
            try
            {
                if (System.IO.File.Exists(path) == true)
                {
                    string[] MacIDs = GetName();
                    for (int i = 0; i < MacIDs.Length; i++)
                    {
                        if (MacIDs[i] != null)
                        {
                            if (MacIDs[i].ToString() != DecryptMacID())
                            {
                                flag = 0;
                            }
                            else
                            {
                                flag = 1;
                                break;
                            }
                        }
                        else flag = 0;
                    }
                    if (flag == 0)
                    {
                        str = "There is One of the serious problem. Not Allowed to another Server is install this System. Please Contact to software team. Thank You ....";
                        OMMessageBox.Show(str, "Error System.", OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                        Application.Exit();
                    }
                }
                else if ((System.IO.File.Exists(path) == false) & (Type == 1))
                {
                    flag = -1;
                }
                else if (Type == 2)
                {
                    if (StrReg != EncryptMacID())
                    {
                        str = "Registered Key is not Correct Please Try Again. Thank You ....";
                        OMMessageBox.Show(str, "Error System.", OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                    }
                    else
                    {
                        WriteFile(AssemblyTitle, AssemblyProduct, AssemblyVersion, AssemblyCopyright, AssemblyGUID, DatabaseName);
                        flag = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                str = "There is One of the serious problem. Please Contact to software team. Thank You ....";
                OMMessageBox.Show(str, "Error System.", OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                Application.Exit();
            }
            return flag;
        }

        public void WriteFile(string AssemblyTitle, string AssemblyProduct, string AssemblyVersion, string AssemblyCopyright, string AssemblyGUID, string DatabaseName)
        {
            if (System.IO.File.Exists(path) == false)
            {
                // Create a file to write to.
                System.IO.StreamWriter sw = System.IO.File.CreateText(path);
                sw.WriteLine(secure.psEncrypt(EncryptMacID()));
                sw.WriteLine(secure.psEncrypt("36"));
                if (ClientName == "")
                    sw.WriteLine(secure.psEncrypt(System.Net.Dns.GetHostName() + "\\SQLEXPRESS"));
                else
                    sw.WriteLine(secure.psEncrypt(ClientName + "\\SQLEXPRESS"));
                sw.WriteLine(secure.psEncrypt(DatabaseName));
                sw.Flush();
                sw.Close();

                System.IO.StreamWriter sw1 = System.IO.File.CreateText(path1);
                sw1.WriteLine("===========================================================");
                sw1.WriteLine("                    System Information                     ");
                sw1.WriteLine("===========================================================");
                sw1.WriteLine("Retailer System: " + Application.ProductVersion);
                sw1.WriteLine("Company Name:" + Application.CompanyName);
                sw1.WriteLine("");
                sw1.WriteLine("Installation Date: \t\t" + DateTime.Now);
                sw1.WriteLine("Assembly Title: \t\t" + String.Format("About " + AssemblyTitle));
                sw1.WriteLine("Assembly Product: \t\t" + AssemblyProduct);
                sw1.WriteLine("Assembly Version: \t\t" + String.Format("Version {0}", AssemblyVersion));
                sw1.WriteLine("Assembly Copyright:\t" + AssemblyCopyright);
                sw1.WriteLine("Assembly GUID:\t\t" + AssemblyGUID);
                sw1.WriteLine("");
                sw1.WriteLine("Developed By:\tKamlesh Diddi(Sr. Software Developer)");
                sw1.WriteLine("===========================================================");
                sw1.WriteLine("Thank you for using our software.");
                sw1.Flush();
                sw1.Close();
            }
        }

        public void WriteUpdateVersionFile(DateTime dt, string AssemblyTitle, string AssemblyProduct, string AssemblyVersion, string AssemblyCopyright, string AssemblyGUID)
        {

            // Create a file to write to.

            System.IO.StreamWriter sw1 = System.IO.File.CreateText(path1);
            sw1.WriteLine("===========================================================");
            sw1.WriteLine("                    System Information                     ");
            sw1.WriteLine("===========================================================");
            sw1.WriteLine("Retailer System: " + Application.ProductVersion);
            sw1.WriteLine("Company Name:" + Application.CompanyName);
            sw1.WriteLine("");
            sw1.WriteLine("Installation Date: \t\t" + dt);
            sw1.WriteLine("Modified Date: \t\t" + DateTime.Now);
            sw1.WriteLine("Assembly Title: \t\t" + String.Format("About " + AssemblyTitle));
            sw1.WriteLine("Assembly Product: \t\t" + AssemblyProduct);
            sw1.WriteLine("Assembly Version: \t\t" + String.Format("Version {0}", AssemblyVersion));
            sw1.WriteLine("Assembly Copyright:\t" + AssemblyCopyright);
            sw1.WriteLine("Assembly GUID:\t\t" + AssemblyGUID);
            sw1.WriteLine("");
            sw1.WriteLine("Developed By:\tKamlesh Diddi(Sr. Software Developer)");
            sw1.WriteLine("Developed By:\tDeepak Patel(Sr. Software Developer)");
            sw1.WriteLine("===========================================================");
            sw1.WriteLine("Thank you for using our software.");
            sw1.Flush();
            sw1.Close();

        }

        public string EncryptGenMacID()
        {

            string strMac = GetMacName();
            string strTemp = "";
            int i = 0;
            for (i = 0; i < strMac.Length; i++)
            {
                if (strMac[i].ToString() != ":")
                {
                    strTemp = strTemp + (System.Convert.ToInt32(strMac[i]) + 10);

                }
                else
                {
                    strTemp = strTemp + strMac[i].ToString();
                }

            }
            string str = strTemp;
            strTemp = "";
            for (i = str.Length - 1; i >= 0; i--)
            {
                strTemp = strTemp + str[i].ToString();
            }

            return strTemp;
        }

        public string EncryptIDs(int sec)
        {
            string strMac = GetMacName();
            string strTemp = "";
            int i = 0;
            if (sec > 20 && sec <= 40) sec = sec - 20;
            else if (sec > 40) sec = sec - 40;
            for (i = 0; i < strMac.Length; i++)
            {
                if (strMac[i].ToString() != ":")
                {
                    strTemp = strTemp + (System.Convert.ToInt32(strMac[i]) + sec);

                }
                else
                {
                    strTemp = strTemp + strMac[i].ToString();
                }

            }
            string str = strTemp;
            strTemp = "";
            for (i = str.Length - 1; i >= 0; i--)
            {
                strTemp = strTemp + str[i].ToString();
            }

            return strTemp;
        }

        public string EncryptMacID()
        {

            string strMac = GetMacName();
            string strTemp = "";
            int i = 0;
            for (i = 0; i < strMac.Length; i++)
            {
                if (strMac[i].ToString() != ":")
                {
                    strTemp = strTemp + (System.Convert.ToInt32(strMac[i]) + 3);
                }
                else
                {
                    strTemp = strTemp + strMac[i];
                }

            }

            return strTemp;
        }

        public string DecryptMacID()
        {

            string strMac = null;

            if (System.IO.File.Exists(path) == true)
            {
                // Create a file to write to.
                System.IO.StreamReader sw = System.IO.File.OpenText(path);
                strMac = sw.ReadLine();
                sw.Close();
                strMac = secure.psDecrypt(strMac);
            }

            string strTemp = "";
            int i = 0;
            for (i = 0; i < strMac.Length; i++)
            {
                if (strMac[i].ToString() != ":")
                {
                    strTemp = strTemp + (char)(System.Convert.ToInt32(strMac.Substring(i, 2)) - 3);
                    i = i + 1;
                }
                else
                {
                    strTemp = strTemp + strMac[i];
                }
            }
            return strTemp;
        }

        public string ConvertMacID(string strMac)
        {

            string strTemp = "";
            string str = "";
            int i = 0;

            for (i = strMac.Length - 1; i >= 0; i--)
            {
                str = str + strMac[i].ToString();
            }
            strMac = str;

            for (i = 0; i < strMac.Length; i++)
            {
                if (strMac[i].ToString() != ":")
                {
                    strTemp = strTemp + (System.Convert.ToInt32((char)((Convert.ToInt32(strMac.Substring(i, 2)) - 10))) + 3);
                    i = i + 1;
                }
                else
                {
                    strTemp = strTemp + strMac[i].ToString();
                }

            }
            return strTemp;
        }

        public string ConvertID(string strMac)
        {

            string strTemp = "";
            string str = "";
            int i = 0;

            for (i = strMac.Length - 4; i >= 0; i--)
            {
                str = str + strMac[i].ToString();
            }
            int sec = Convert.ToInt32(strMac.Substring(strMac.Length - 2, 2));
            strMac = str;

            for (i = 0; i < strMac.Length; i++)
            {
                if (strMac[i].ToString() != ":")
                {
                    strTemp = strTemp + (System.Convert.ToInt32((char)(Convert.ToInt32((Convert.ToInt32(strMac.Substring(i, 2)) - sec).ToString().Replace("-", "")))) + 3);
                    i = i + 1;
                }
                else
                {
                    strTemp = strTemp + strMac[i].ToString();
                }

            }
            return strTemp;
        }

        public void DisplaySerialKey(MaskedTextBox txtKey, MaskedTextBox txtRegID)
        {

            string strKey = ConvertMacID(txtKey.Text);

            try
            {
                txtRegID.Text = strKey;

            }
            catch (Exception ex)
            {
                OMMessageBox.Show(ex.Message, "Error System", OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                Application.Exit();
            }

        }

        public void DisplayKey(MaskedTextBox txtKey, MaskedTextBox txtRegID)
        {

            string strKey = ConvertID(txtKey.Text);

            try
            {
                txtRegID.Text = strKey;

            }
            catch (Exception ex)
            {
                OMMessageBox.Show(ex.Message, "Error System", OMMessageBoxButton.OK, OMMessageBoxIcon.Error);
                Application.Exit();
            }

        }

        public bool CheckKeyPassword(string strPwd, int type)
        {
            bool flag = false;
            if (type == 1)
            {
                if (strPwd == "kamsh")
                    flag = true;
            }
            else if (type == 2)
            {
                if (strPwd == "shmak")
                    flag = true;
            }
            return flag;
        }

        public void WriteSecurityFile(DateTime dtNow, string StartName, InstallationType INSType, int NoOfDays, int NoOfMonth, int NoOfYear, bool Append)
        {
            try
            {
                MainINSType = INSType;
                if (InstallationType.None == INSType) return;
                string strpath = "C:\\Windows\\System32\\" + StartName + " Security.dat";
                if (System.IO.File.Exists(strpath) == false && Append == false)
                {

                    // Create a file to write to.
                    System.IO.StreamWriter sw = System.IO.File.CreateText(strpath);

                    if (InstallationType.Demo == INSType)
                    {
                        sw.WriteLine(secure.psEncrypt(DateTime.Now.AddDays(30).ToString("dd-MMM-yyyy")));
                    }
                    else if (InstallationType.Regular == INSType)
                    {
                        sw.WriteLine(secure.psEncrypt(DateTime.Now.AddDays(90).ToString("dd-MMM-yyyy")));
                    }
                    else if (InstallationType.Custom == INSType)
                    {
                        DateTime dt = dtNow;
                        if (NoOfDays > 0) dt = dt.AddDays(NoOfDays);
                        if (NoOfMonth > 0) dt = dt.AddMonths(NoOfMonth);
                        if (NoOfYear > 0) dt = dt.AddYears(NoOfYear);
                        sw.WriteLine(secure.psEncrypt(dt.ToString("dd-MMM-yyyy")));
                    }

                    sw.Flush();
                    sw.Close();
                }
                else if (Append == true)
                {
                    // Create a file to write to.
                    System.IO.StreamWriter sw = System.IO.File.CreateText(strpath);

                    if (InstallationType.Demo == INSType)
                    {
                        sw.WriteLine(secure.psEncrypt(DateTime.Now.AddDays(30).ToString("dd-MMM-yyyy")));
                    }
                    else if (InstallationType.Regular == INSType)
                    {
                        sw.WriteLine(secure.psEncrypt(DateTime.Now.AddDays(90).ToString("dd-MMM-yyyy")));
                    }
                    else if (InstallationType.Custom == INSType)
                    {
                        DateTime dt = DateTime.Now;
                        if (NoOfDays > 0) dt = dt.AddDays(NoOfDays);
                        if (NoOfMonth > 0) dt = dt.AddMonths(NoOfMonth);
                        if (NoOfYear > 0) dt = dt.AddYears(NoOfYear);
                        sw.WriteLine(secure.psEncrypt(dt.ToString("dd-MMM-yyyy")));
                    }

                    sw.Flush();
                    sw.Close();
                }
            }
            catch (Exception e)
            { }
        }

        public void WriteLockFile(string StartName)
        {
            try
            {
                string strpath = "C:\\Windows\\System32\\" + StartName + " Security.dat";
                if (System.IO.File.Exists(strpath) == true)
                {

                    // Create a file to write to.
                    System.IO.StreamWriter sw = System.IO.File.CreateText(strpath);

                    sw.WriteLine(secure.psEncrypt("01-Jan-1900"));


                    sw.Flush();
                    sw.Close();
                }
            }
            catch (Exception e)
            { }
        }

        public bool ReadSecurity(DateTime dt, string StartName)
        {
            bool flag = false;
            if (MainINSType == InstallationType.None) return true;
            string strfname = "C:\\Windows\\System32\\" + StartName + " Security.dat";
            StreamReader objreader = new StreamReader(strfname);
            try
            {
                string str = objreader.ReadLine();
                objreader.Close();
                objreader = null;

                DateTime dtTemp = Convert.ToDateTime(secure.psDecrypt(str));

                if (dt > dtTemp)
                    flag = false;
                else
                    flag = true;

            }
            catch (Exception e)
            {
                flag = false;
            }
            return flag;
        }

        public enum InstallationType
        {
            Demo = 1,
            Regular = 2,
            Custom = 3,
            None = 4
        }
        public string GetAbountInfo(string StartName)
        {
            string AboutInfo = "";
            try
            {
                string strfname = "C:\\Windows\\System32\\" + StartName + " Registered.dat";
                StreamReader objreader = new StreamReader(strfname);
                AboutInfo = objreader.ReadToEnd();
                objreader.Close();
                objreader = null;
            }
            catch (Exception e)
            {
                AboutInfo = "";
            }
            return AboutInfo;
        }
    }

    public class CommandCollection : System.Collections.CollectionBase
    {

        public SqlCommand this[int index]
        {
            get
            {
                return ((SqlCommand)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }

        public int Add(SqlCommand value)
        {
            return (List.Add(value));
        }

        public int IndexOf(SqlCommand value)
        {
            return (List.IndexOf(value));
        }

        public void Insert(int index, SqlCommand value)
        {
            List.Insert(index, value);
        }

        public void Remove(SqlCommand value)
        {
            List.Remove(value);
        }

        public bool Contains(SqlCommand value)
        {
            // If value is not of type Int16, this will return false.
            return (List.Contains(value));
        }

        protected override void OnValidate(Object value)
        {
            if (value.GetType() != typeof(SqlCommand))
                throw new ArgumentException("value must be of type SqlCommand.", "value");
        }

    }

    public class DataTablesCollection : System.Collections.CollectionBase
    {

        public DataTable this[int index]
        {
            get
            {
                return ((DataTable)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }

        public int Add(DataTable value)
        {
            return (List.Add(value));
        }

        public int IndexOf(DataTable value)
        {
            return (List.IndexOf(value));
        }

        public void Insert(int index, DataTable value)
        {
            List.Insert(index, value);
        }

        public void Remove(DataTable value)
        {
            List.Remove(value);
        }

        public bool Contains(DataTable value)
        {
            // If value is not of type Int16, this will return false.
            return (List.Contains(value));
        }

        protected override void OnValidate(Object value)
        {
            if (value.GetType() != typeof(DataTable))
                throw new ArgumentException("value must be of type DataTable.", "value");
        }

    }

    public class StringCollection : System.Collections.CollectionBase
    {

        public String this[int index]
        {
            get
            {
                return ((String)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }

        public int Add(String value)
        {
            return (List.Add(value));
        }

        public int IndexOf(String value)
        {
            return (List.IndexOf(value));
        }

        public void Insert(int index, String value)
        {
            List.Insert(index, value);
        }

        public void Remove(String value)
        {
            List.Remove(value);
        }

        public bool Contains(String value)
        {
            // If value is not of type Int16, this will return false.
            return (List.Contains(value));
        }

        protected override void OnValidate(Object value)
        {
            if (value.GetType() != typeof(String))
                throw new ArgumentException("value must be of type String.", "value");
        }

    }

    public class GridSearch
    {
        private Color GridBackColor = Color.White;
        public string str = "";
        public int hitCount = 0;
        public int ColIndex = -1;
        public int[] ColIndexArr;
        public long SearchType = 1;

        public GridSearch(DataGridView dg)
        {
            dg.KeyPress += new KeyPressEventHandler(dg_KeyPress);
            dg.KeyDown += new KeyEventHandler(dg_KeyDown);
        }

        public GridSearch(DataGridView dg, int SearchColumnIndex)
        {
            ColIndex = SearchColumnIndex;
            dg.KeyPress += new KeyPressEventHandler(dg_KeyPress);
            dg.KeyDown += new KeyEventHandler(dg_KeyDown);
        }

        public GridSearch(DataGridView dg, int SearchColumnIndex, Color BackColor)
        {
            ColIndex = SearchColumnIndex;
            dg.KeyPress += new KeyPressEventHandler(dg_KeyPress);
            dg.KeyDown += new KeyEventHandler(dg_KeyDown);
            GridBackColor = BackColor;
        }

        public GridSearch(DataGridView dg, int[] SearchColumnIndex)
        {
            ColIndexArr = SearchColumnIndex;
            dg.KeyPress += new KeyPressEventHandler(dg_KeyPress);
            dg.KeyDown += new KeyEventHandler(dg_KeyDown);
        }

        public GridSearch(DataGridView dg, long SearchType)
        {
            this.SearchType = SearchType;
            dg.KeyPress += new KeyPressEventHandler(dg_KeyPress);
            dg.KeyDown += new KeyEventHandler(dg_KeyDown);
        }

        public GridSearch(DataGridView dg, int SearchColumnIndex, long SearchType)
        {
            this.SearchType = SearchType;
            ColIndex = SearchColumnIndex;
            dg.KeyPress += new KeyPressEventHandler(dg_KeyPress);
            dg.KeyDown += new KeyEventHandler(dg_KeyDown);
        }
        public GridSearch(DataGridView dg, int[] SearchColumnIndex, long SearchType)
        {
            this.SearchType = SearchType;
            ColIndexArr = SearchColumnIndex;
            dg.KeyPress += new KeyPressEventHandler(dg_KeyPress);
            dg.KeyDown += new KeyEventHandler(dg_KeyDown);
        }

        public void NullText()
        {
            while (true)
            {
                hitCount++;
                try
                {
                    Thread.Sleep(100);
                }
                catch (Exception)
                {
                }
                if (hitCount >= 20)
                {
                    str = "";
                    break;
                }
            }
        }

        private void dg_KeyPress(object sender, KeyPressEventArgs e)
        {
            hitCount = 0;
            if (str == "")
            {
                Thread th = new Thread(new ThreadStart(NullText));
                th.Start();
            }

            str = str + e.KeyChar;
            if (ColIndexArr == null)
            {
                if (ColIndex == -1)
                    SearchGridValue(((DataGridView)sender), str);
                else
                    SearchGridValue(((DataGridView)sender), str, ColIndex);
            }
            else
            {
                int index = GetIndex(((DataGridView)sender), ColIndexArr);
                if (index != -1)
                    SearchGridValue(((DataGridView)sender), str, index);
            }
            if ((int)e.KeyChar != 13 || (int)e.KeyChar != 32)
            {
                e.Handled = true;

            }
        }

        public void SearchGridValue(DataGridView dg, string strSearch)
        {
            int i = 0, cnt = 0, cntRow = -1;

            if (strSearch != "")
            {
                for (i = 0; i < dg.Rows.Count; i++)
                {
                    dg.Rows[i].DefaultCellStyle.BackColor = GridBackColor;
                    cnt = 0;
                    if (SearchType == 1)
                    {
                        if (dg.Rows[i].Cells[dg.CurrentCell.ColumnIndex].Value.ToString().Trim().ToUpper().IndexOf(strSearch.Trim().ToUpper()) >= 0)
                        {
                            if (cntRow == -1)
                                cntRow = i;
                            dg.Rows[i].DefaultCellStyle.BackColor = JitFunctions.RowColor;
                        }
                    }
                    else if (SearchType == 2)
                    {
                        for (int j = 0; j < strSearch.Trim().ToUpper().Length && dg.Rows[i].Cells[dg.CurrentCell.ColumnIndex].Value != null &&
                            j < dg.Rows[i].Cells[dg.CurrentCell.ColumnIndex].Value.ToString().Length; j++)
                        {
                            if (dg.Rows[i].Cells[dg.CurrentCell.ColumnIndex].Value != null && dg.Rows[i].Cells[dg.CurrentCell.ColumnIndex].Value.ToString() != "")
                            {
                                if (strSearch.Trim().ToUpper()[j].ToString() == dg.Rows[i].Cells[dg.CurrentCell.ColumnIndex].Value.ToString().Trim().ToUpper()[j].ToString())
                                    cnt++;
                                else break;
                            }
                        }
                        if (cnt == strSearch.Trim().ToUpper().Length)
                        {
                            if (cntRow == -1)
                                cntRow = i;
                            //dg.CurrentCell = dg.Rows[i].Cells[1];
                            dg.Rows[i].DefaultCellStyle.BackColor = JitFunctions.RowColor;
                        }
                    }
                    cnt = 0;
                }
                if (cntRow != -1)
                    dg.CurrentCell = dg.Rows[cntRow].Cells[dg.CurrentCell.ColumnIndex];

            }
        }

        public void SearchGridValue(DataGridView dg, string strSearch, int ColIndex)
        {
            int i = 0, cnt = 0, cntRow = -1;

            if (strSearch.Trim() != "")
            {
                for (i = 0; i < dg.Rows.Count; i++)
                {
                    dg.Rows[i].DefaultCellStyle.BackColor = GridBackColor;
                    cnt = 0;
                    if (SearchType == 1)
                    {
                        if (dg.Rows[i].Cells[ColIndex].Value.ToString().Trim().ToUpper().IndexOf(strSearch.Trim().ToUpper()) >= 0)
                        {
                            if (cntRow == -1)
                                cntRow = i;
                            dg.Rows[i].DefaultCellStyle.BackColor = JitFunctions.RowColor;
                        }
                    }
                    else if (SearchType == 2)
                    {
                        for (int j = 0; j < strSearch.Trim().ToUpper().Length && dg.Rows[i].Cells[ColIndex].Value != null &&
                            j < dg.Rows[i].Cells[ColIndex].Value.ToString().Length; j++)
                        {
                            if (dg.Rows[i].Cells[ColIndex].Value != null && dg.Rows[i].Cells[ColIndex].Value.ToString() != "")
                            {
                                if (strSearch.Trim().ToUpper()[j].ToString() == dg.Rows[i].Cells[ColIndex].Value.ToString().Trim().ToUpper()[j].ToString())
                                    cnt++;
                                else break;
                            }
                        }
                        if (cnt == strSearch.Trim().ToUpper().Length)
                        {
                            if (cntRow == -1)
                                cntRow = i;
                            //dg.CurrentCell = dg.Rows[i].Cells[1];
                            dg.Rows[i].DefaultCellStyle.BackColor = JitFunctions.RowColor;
                        }
                    }
                    cnt = 0;
                }
                if (cntRow != -1)
                    dg.CurrentCell = dg.Rows[cntRow].Cells[ColIndex];

            }
            if (strSearch.Trim() == "") strSearch = "";
        }

        public int GetIndex(DataGridView dg, int[] ColIndexArr)
        {
            int index = -1;
            for (int i = 0; i < ColIndexArr.Length; i++)
            {
                if (ColIndexArr[i] == dg.CurrentCell.ColumnIndex)
                {
                    index = ColIndexArr[i];
                    break;
                }
            }
            return index;
        }


        private void dg_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                e.SuppressKeyPress = true;
                str = "";
            }
        }
    }

    public class Item
    {
        public string Name;
        public string Value;
        public Item(string name, string value)
        {
            Name = name; Value = value;
        }
        public override string ToString()
        {
            return Name;
        }
    }

    public class ListSearch
    {
        public string str = "";
        public int hitCount = 0;

        public ListSearch(ListBox lst)
        {
            lst.KeyPress += new KeyPressEventHandler(lst_KeyPress);
            lst.KeyDown += new KeyEventHandler(lst_KeyDown);
        }

        public void NullText()
        {
            while (true)
            {
                hitCount++;
                try
                {
                    Thread.Sleep(100);
                }
                catch (Exception)
                {
                }
                if (hitCount >= 20)
                {
                    str = "";
                    break;
                }
            }
        }

        private void lst_KeyPress(object sender, KeyPressEventArgs e)
        {
            hitCount = 0;
            if (str == "")
            {
                Thread th = new Thread(new ThreadStart(NullText));
                th.Start();
            }

            str = str + e.KeyChar;
            SearchListValue(((ListBox)sender), str);
            if ((int)e.KeyChar != 13 || (int)e.KeyChar != 32)
            {
                e.Handled = true;

            }
        }

        public void SearchListValue(ListBox lst, string strSearch)
        {
            int i = 0, cnt = 0, cntRow = -1;

            if (strSearch.Trim() != "")
            {
                for (i = 0; i < lst.Items.Count; i++)
                {
                    cnt = 0;
                    for (int j = 0;
                        j < strSearch.Trim().ToUpper().Length && j < ((DataRowView)lst.Items[i]).Row[1].ToString().Trim().ToUpper().Length;
                        j++)
                    {
                        if (strSearch.Trim().ToUpper()[j].ToString() == ((DataRowView)lst.Items[i]).Row[1].ToString().Trim().ToUpper()[j].ToString())
                            cnt++;
                        else break;
                    }
                    if (cnt == strSearch.Trim().ToUpper().Length)
                    {
                        if (cntRow == -1)
                            cntRow = i;
                    }
                    cnt = 0;
                }
                if (cntRow != -1)
                    lst.SelectedIndex = cntRow;
            }
        }

        private void lst_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                e.SuppressKeyPress = true;
                str = "";
            }
        }
    }

    public static class Format
    {
        /// <summary>Gets only value without floating points.
        /// </summary> 
        /// /// <para>"0"</para>
        public static string NoFloating
        {
            get { return "0"; }
        }
        /// <summary>Gets value as Single floating points.
        /// <para>"0.0"</para>
        /// </summary> 
        public static string SingleFloating
        {
            get { return "0.0"; }
        }
        /// <summary>Gets value as Double floating points.
        /// <para>"0.00"</para>
        /// </summary> 
        public static string DoubleFloating
        {
            get { return "0.00"; }
        }
        /// <summary>Gets value as Three floating points.
        /// <para>"0.000"</para>
        /// </summary> 
        public static string ThreeFloating
        {
            get { return "0.000"; }
        }
        /// <summary>Gets value as Single floating points.
        /// <para>"0.0000"</para>
        /// </summary> 
        public static string FourFloating
        {
            get { return "0.0000"; }
        }
        /// <summary>Gets Date formatting value.
        /// <para>"dd-MM-YY"</para>
        /// </summary> 
        public static string DDMMYY
        {
            get { return "dd-MM-YY"; }
        }
        /// <summary>Gets Date formatting value.
        /// <para>"dd-MM-YYYY"</para>
        /// </summary> 
        public static string DDMMYYYY
        {
            get { return "dd-MM-YYYY"; }
        }
        /// <summary>Gets Date formatting value.
        /// <para>"dd-MMM-YY"</para>
        /// </summary> 
        public static string DDMMMYY
        {
            get { return "dd-MMM-YY"; }
        }
        /// <summary>Gets Date formatting value.
        /// <para>"dd-MMM-YYYY"</para>
        /// </summary> 
        public static string DDMMMYYYY
        {
            get { return "dd-MMM-yyyy"; }
        }
        /// <summary>Gets Date formatting value.
        /// <para>"hh:mm:ss AM/PM"</para>
        /// </summary> 
        public static string HHMMSS
        {
            get { return "hh:mm:ss AM/PM"; }
        }
        /// <summary>Gets Date formatting value.
        /// <para>"hh:mm AM/PM"</para>
        /// </summary> 
        public static string HHMM
        {
            get { return "hh:mm AM/PM"; }
        }
    }

    public class ListViewCollection : System.Collections.CollectionBase
    {

        public ListView this[int index]
        {
            get
            {
                return ((ListView)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }

        public int Add(ListView value)
        {
            return (List.Add(value));
        }

        public int IndexOf(ListView value)
        {
            return (List.IndexOf(value));
        }

        public void Insert(int index, ListView value)
        {
            List.Insert(index, value);
        }

        public void Remove(ListView value)
        {
            List.Remove(value);
        }

        public bool Contains(ListView value)
        {
            // If value is not of type Int16, this will return false.
            return (List.Contains(value));
        }

        protected override void OnValidate(Object value)
        {
            if (value.GetType() != typeof(ListView))
                throw new ArgumentException("value must be of type ListView.", "value");
        }

    }

}
