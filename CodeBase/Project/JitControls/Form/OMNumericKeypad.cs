﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace JitControls
{
    public partial class OMNumericKeypad : Form
    {
        public int NoOfDecimal = 0, NoOfDigit = 9;
        public static string ReturnValue = "";
        JitFunctions objFunction = new JitFunctions();
        TextBox txtReturn = null;

        public OMNumericKeypad()
        {
            InitializeComponent();
        }

        public static void Show()
        {
            OMNumericKeypad OMnumeric = new OMNumericKeypad();
            OMnumeric.ShowDialog();
        }

        public static void Show(int NoOfDecimal, int NoOfDigit)
        {
            OMNumericKeypad OMnumeric = new OMNumericKeypad();
            OMnumeric.NoOfDecimal = NoOfDecimal;
            OMnumeric.NoOfDigit = NoOfDigit;
            OMnumeric.ShowDialog();
        }
        public static void Show(TextBox txt)
        {
            OMNumericKeypad OMnumeric = new OMNumericKeypad();
            OMnumeric.txtReturn = txt;
            OMnumeric.ShowDialog();
        }
        public static void Show(int NoOfDecimal, int NoOfDigit,TextBox txt)
        {
            OMNumericKeypad OMnumeric = new OMNumericKeypad();
            OMnumeric.NoOfDecimal = NoOfDecimal;
            OMnumeric.NoOfDigit = NoOfDigit;
            OMnumeric.txtReturn = txt;
            OMnumeric.ShowDialog();
        }

        private void OMNumericKeypad_Load(object sender, EventArgs e)
        {
            ReturnValue = "";
        }

        private void Button_Click(object sender, EventArgs e)
        {
            lblResult.Text += ((Button)sender).Text;
            objFunction.SetMasked(lblResult, NoOfDecimal, NoOfDigit);
            ReturnValue = lblResult.Text;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (txtReturn != null) { txtReturn.Text = lblResult.Text; txtReturn.Select(txtReturn.Text.Length, 0); }
            this.Close();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            lblResult.Text = (lblResult.Text != "") ? lblResult.Text.Remove(lblResult.Text.Length - 1) : "";
            ReturnValue = lblResult.Text;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            lblResult.Text = "";
            ReturnValue = lblResult.Text;
        }
    }
}
