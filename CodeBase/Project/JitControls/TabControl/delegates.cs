using System;

namespace JitControls
{
	/// <summary>
	/// The delegate to use for the <see cref="YaTabControl.TabChanging"/> event.
	/// </summary>
	public delegate void TabChangingEventHandler( object sender, TabChangingEventArgs tcea );
}
