﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace JitControls
{
    public class OMTextBox : TextBox
    {
        private System.Drawing.Color m_BorderColor = System.Drawing.Color.Red;
        public System.Drawing.Color GetBorderColor()
        {

            return this.m_BorderColor;

        }

        public void SetBorderColor(System.Drawing.Color clrBorder)
        {

            this.m_BorderColor = clrBorder;

            this.Invalidate();

        }


        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            Pen penBorder = new Pen(this.GetBorderColor(), 1);

            Rectangle rectBorder = new Rectangle(e.ClipRectangle.X, e.ClipRectangle.Y, e.ClipRectangle.Width - 1, e.ClipRectangle.Height - 1);

            e.Graphics.DrawRectangle(penBorder, rectBorder);

        }
       
    }

}

