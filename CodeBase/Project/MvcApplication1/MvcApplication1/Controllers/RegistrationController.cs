﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MvcApplication1.Controllers
{
    public class RegistrationController : ApiController
    {

        
        [HttpGet]
        public HttpResponseMessage GetRegistration()
        {
            try
            {
                Validation();
                OM.DBMRegistration dbreg = new OM.DBMRegistration();
                return Request.CreateResponse(HttpStatusCode.OK, dbreg.GetRegistration(OM.CommonFunctions.RegNo));
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex.Message.ToString());
            }

        }

        [HttpPost]
        public HttpResponseMessage Post([FromBody]DTOClasses.UCRegistration value)
        {
            try
            {
                if (value == null && value.DeviceID == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No Data IN Boddy");
                OM.DBMRegistration dbreg = new OM.DBMRegistration();
                return Request.CreateResponse(HttpStatusCode.OK, dbreg.SaveRegistration(value));
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message.ToString());
            }
        }



        private void Validation()
        {
            var re = Request;
            var headers = re.Headers;

            if (headers.Contains("Pos_Type") == false)
                throw new Exception("Bad Request");
            else
            {
                if (headers.GetValues("Pos_Type").First() == "WPOS")
                    OM.CommonFunctions.OrderType = 1;
                else
                    if (headers.GetValues("Pos_Type").First() == "MPOS")
                        OM.CommonFunctions.OrderType = 2;
            }
            if (headers.Contains("RegNo") == false && headers.Contains("StoreID") == false)
                throw new Exception("This agent Not Register,Please Register the Devices");


            if (headers.Contains("RegNo"))
            {
                OM.DBMRegistration dbreg = new OM.DBMRegistration();
                dbreg.CheckMobileID(headers.GetValues("RegNo").First());
            }
            if (headers.Contains("StoreID"))
            {
                OM.DBMRegistration dbreg = new OM.DBMRegistration();
                dbreg.CheckStroreID(headers.GetValues("StoreID").First());
            }

        }
    }
}
