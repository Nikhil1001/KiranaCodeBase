﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MvcApplication1.Controllers
{
    public class BillController : ApiController
    {


        [AttributeRouting.Web.Mvc.Route("api/Bill")]
        [HttpGet]
        public HttpResponseMessage GetBill()
        {
            try
            {
                Validation();
                OM.DBTVaucherEntry dbTVaucherEntry = new OM.DBTVaucherEntry();
                MvcApplication1.DTOClasses.UCSaleBillsList lst_bill= dbTVaucherEntry.GetSaleBill();
                return Request.CreateResponse(HttpStatusCode.OK, lst_bill);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex.Message.ToString());
            }

        }

        [AttributeRouting.Web.Mvc.Route("api/Bill/SaveBill")]
        [HttpPost]
        public HttpResponseMessage Post([FromBody]DTOClasses.UCSaleBillsList value)
        {
            try
            {
                Validation();
                if (value == null || value.salesBills.Count==0)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No Data IN Boddy");
                
                OM.DBTVaucherEntry  dbreg = new OM.DBTVaucherEntry();
                return Request.CreateResponse(HttpStatusCode.OK, dbreg.SaveBill(value));
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message.ToString());
            }
        }

        [AttributeRouting.Web.Mvc.Route("api/Bill/SaveBillStatus")]
        [HttpPost]
        public HttpResponseMessage Post_BillStatus([FromBody]List<OM.TReturnBill> value)
        {
            try
            {
                Validation();
                if (value == null || value.Count == 0)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No Data IN Boddy");

                OM.DBTVaucherEntry dbreg = new OM.DBTVaucherEntry();
                dbreg.SaveBillStatus(value);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message.ToString());
            }
        }

        [AttributeRouting.Web.Mvc.Route("api/Bill/Collection")]
        [HttpPost]
        public HttpResponseMessage Post_Collection([FromBody]List<DTOClasses.UCTVoucherRefDetails> value)
        {
            try
            {
                Validation();
                if (value == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No Data IN Boddy");
                OM.DBTVaucherEntry dbreg = new OM.DBTVaucherEntry();
                return Request.CreateResponse(HttpStatusCode.OK, dbreg.SaveCollectionBill(value));
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message.ToString());
            }
        }


        private void Validation()
        {
            var re = Request;
            var headers = re.Headers;

            if (headers.Contains("Pos_Type") == false)
                throw new Exception("Bad Request");
            else
            {
                if (headers.GetValues("Pos_Type").First() == "WPOS")
                    OM.CommonFunctions.OrderType = 1;
                else
                    if (headers.GetValues("Pos_Type").First() == "MPOS")
                        OM.CommonFunctions.OrderType = 2;
            }
            if (headers.Contains("RegNo") == false && headers.Contains("StoreID") == false)
                throw new Exception("This agent Not Register,Please Register the Devices");


            if (headers.Contains("RegNo"))
            {
                OM.DBMRegistration dbreg = new OM.DBMRegistration();
                dbreg.CheckMobileID(headers.GetValues("RegNo").First());
            }
            if (headers.Contains("StoreID"))
            {
                OM.DBMRegistration dbreg = new OM.DBMRegistration();
                dbreg.CheckStroreID(headers.GetValues("StoreID").First());
            }

        }
    }
}
