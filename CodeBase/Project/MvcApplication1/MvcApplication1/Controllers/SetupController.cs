﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Net.Http.Headers;
using System.IO;
using OM;

namespace MvcApplication1.Controllers
{
    public class SetupController : ApiController
    {
        [AttributeRouting.Web.Mvc.Route("api/Setup")]
        [HttpGet]
        public HttpResponseMessage DownloadFile()
        {
            try
            {
                var re = Request;
                var headers = re.Headers;
                if (headers.Contains("VersionNumber") == false)
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);

                string VersionNumber = headers.GetValues("VersionNumber").First();
                if (!string.IsNullOrEmpty(VersionNumber))
                {
                    string root = AppDomain.CurrentDomain.BaseDirectory;
                    string filePath = root + "UpdateSetupFile";
                    string FileName = "Kirana_V_" + VersionNumber + ".exe";
                    string fullPath = filePath + "\\" + FileName;
                    //return Request.CreateErrorResponse(HttpStatusCode.NotFound, fullPath);
                    if (File.Exists(fullPath))
                    {

                        HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                        var fileStream = new FileStream(fullPath, FileMode.Open);
                        response.Content = new StreamContent(fileStream);
                        response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                        response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                        response.Content.Headers.ContentDisposition.FileName = FileName;
                        return response;
                    }
                    else
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, fullPath);
                    }
                }

                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message + ":" + ex.StackTrace);
            }
        }



        [AttributeRouting.Web.Mvc.Route("api/Setup/LatestVersion")]
        [HttpGet]
        public UCCheckVersion GetLatestVersion()
        {
            DBMRegistration dbreg = new DBMRegistration();
            UCCheckVersion ucchk = new UCCheckVersion();
            ucchk.appVersion=dbreg.GetLatestVersion();
            return ucchk;
        }

    }
}
