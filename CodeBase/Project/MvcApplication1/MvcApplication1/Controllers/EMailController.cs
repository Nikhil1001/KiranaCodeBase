﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MvcApplication1.Controllers
{
    public class EMailController : ApiController
    {

        [AttributeRouting.Web.Mvc.Route("api/Email")]
        [HttpPost]
        public HttpResponseMessage Post([FromBody]DTOClasses.UCEMail value)
            {
            try
            {
                if (value == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No Data IN Boddy");

                OM.DBMEMail dbEmail = new OM.DBMEMail();
                dbEmail.SendMail(value);
                return Request.CreateResponse(HttpStatusCode.OK,"Email Is Send");
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message.ToString());
            }
        }

    }
}
