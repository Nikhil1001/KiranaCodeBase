﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using OM;

namespace MvcApplication1.Controllers
{
    public class CustomerController : ApiController
    {


        public HttpResponseMessage GetAllCustomer()
        {
            try
            {
                Validation();
                DBMLedger dbMLedger = new DBMLedger();
                List<UCCustomer> ucitems = dbMLedger.GetCustomer();
                return Request.CreateResponse(HttpStatusCode.OK, ucitems);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex.Message.ToString());
            }

        }



        [AttributeRouting.Web.Mvc.Route("api/Customer/SaveCustomer")]
        [HttpPost]
        public HttpResponseMessage Post_Save([FromBody]List<UCCustomer> value)
        {
            try
            {
                Validation();

                if (value == null || value.Count == 0)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No Data IN Boddy");

                DBMLedger dbMLedger = new DBMLedger();
                List<UCReturnCustomer> lst = dbMLedger.SaveCustomers(value);
                if (lst.Count != 0)
                    return Request.CreateResponse(HttpStatusCode.OK, lst);
                else
                    return Request.CreateResponse(HttpStatusCode.RequestTimeout);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message.ToString());
            }
        }

        private void Validation()
        {
            var re = Request;
            var headers = re.Headers;

            if (headers.Contains("Pos_Type") == false)
                throw new Exception("Bad Request");
            else
            {
                if (headers.GetValues("Pos_Type").First() == "WPOS")
                    OM.CommonFunctions.OrderType = 1;
                else
                    if (headers.GetValues("Pos_Type").First() == "MPOS")
                        OM.CommonFunctions.OrderType = 2;
            }
            if (headers.Contains("RegNo") == false && headers.Contains("StoreID") == false)
                throw new Exception("This agent Not Register,Please Register the Devices");


            if (headers.Contains("RegNo"))
            {
                OM.DBMRegistration dbreg = new OM.DBMRegistration();
                dbreg.CheckMobileID(headers.GetValues("RegNo").First());
            }
            if (headers.Contains("StoreID"))
            {
                OM.DBMRegistration dbreg = new OM.DBMRegistration();
                dbreg.CheckStroreID(headers.GetValues("StoreID").First());
            }

        }
    }
}
