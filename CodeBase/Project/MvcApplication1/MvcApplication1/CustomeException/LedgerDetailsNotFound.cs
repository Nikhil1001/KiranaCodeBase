﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomeException
{
    class LedgerDetailsNotFound:Exception
    {
        public LedgerDetailsNotFound()
        {
 
        }

        public LedgerDetailsNotFound(string message)
        : base(message)
    {
    }

        public LedgerDetailsNotFound(string message, Exception inner)
        : base(message, inner)
    {
    }
    }
}
