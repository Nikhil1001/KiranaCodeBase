﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication1.DTOClasses
{
    public class UCStockItems
    {
        public long ItemNo { get; set; }
        public string ItemName { get; set; }
        public long GroupNo { get; set; }
        public string BrandName { get; set; }
        public string Uom { get; set; }
        public double MRP { get; set; }
        public double Rate { get; set; }
        public double TaxNo { get; set; }
        public double TaxRate { get; set; }
        public string Barcode { get; set; }
        public string ShortCode { get; set; }
        public string ItemNameDisp { get; set; }
        public string HSNCode { get; set; }
        public double IGSTPer { get; set; }
        public double CGSTPer { get; set; }
        public double SGSTPer { get; set; }
        public double CessPer { get; set; }
                
    }

    public class UCMStockItems
    {
        public long pksrno { get; set; }
        public long itemno { get; set; }
        public string brandname = "";
        public string itemname = "";
        public string langfulldesc = "";
        public double mrp { get; set; }
        public double arate { get; set; }
        public double brate { get; set; }
        public double crate { get; set; }
        public string uom = "";
        public string barcode = "";
        public string itemnamedisp = "";
        public double taxrate { get; set; }
        public double igstper { get; set; }
        public double cgstper { get; set; }
        public double sgstper { get; set; }
        public double cessper { get; set; }
        public string hsncode = "";
        public string imagepath = "";
        public long companyno { get; set; }
        public long userid { get; set; }
        public DateTime userdate { get; set; }
    }

    public class UCReturnStockItems
    {
        public long itemno { get; set; }
    }
}