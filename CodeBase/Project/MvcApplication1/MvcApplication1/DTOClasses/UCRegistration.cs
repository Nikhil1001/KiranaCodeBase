﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication1.DTOClasses
{
    public class UCRegistration
    {
        public string UserName { get; set; }
        public string MobileNo { get; set; }
        public string Email_ID { get; set; }
        public string DeviceID { get; set; }
        public string UserID { get; set; }
        public string Password { get; set; }
        public string StoreEmail_ID { get; set; }
        public string StorePassword { get; set; }
    }
}