﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OM
{
    public class UCCustomer
    {
        public long PkSrNo { get; set; }
        public long LedgerNo { get; set; }
        public long GroupNo { get; set; }
        public string LedgerName { get; set; }
        public string MobileNo { get; set; }
        public string Address { get; set; }
        public long CityNo { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public long StateCode { get; set; }
        public long StateNo { get; set; }
        public string PinCode { get; set; }
        public string GSTNo { get; set; }
        public long CompanyNo { get; set; }
        public DateTime UserDate { get; set; }

    }
    public class UCReturnCustomer
    {
        public long LedgerNo { get; set; }
    }
}