﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication1.DTOClasses
{
    public class UCCity
    {
        public long CityNo { get; set; }
        public string CityName { get; set; }
    }
}