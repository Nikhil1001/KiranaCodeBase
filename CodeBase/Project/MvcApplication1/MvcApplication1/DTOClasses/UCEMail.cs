﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication1.DTOClasses
{
    public class UCEMail
    {
        public string Subject { get; set; }
        public string Body{ get; set; }
        public int Type { get; set; }
    }
}