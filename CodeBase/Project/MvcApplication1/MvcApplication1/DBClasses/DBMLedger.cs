﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Data;
using System.Data.SqlClient;
using OM;
using JitClass;
using Jit;
using System.Data;


namespace OM
{
    public class DBMLedger
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDSet = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        private CommandCollection commandcollection = new CommandCollection();

        public static string strerrormsg;

        public List<UCCustomer> GetCustomer()
        {

            string Sql = "SELECT LedgerNo, LedgerName, MobileNo, Address, CityNo, CityName, PinCode, GSTNo,GroupNo,StateName,StateCode " +
                        " FROM MLedger " +
                        " WHERE (GroupNo=26) And CompanyNo=" + CommonFunctions.CompanyNo + "";

            DataTable dt = ObjFunction.GetDataView(Sql).Table;
            List<UCCustomer> ucItems = new List<UCCustomer>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                UCCustomer items = new UCCustomer();
                items.LedgerNo = Convert.ToInt64(dt.Rows[i]["LedgerNo"].ToString());
                items.GroupNo = Convert.ToInt64(dt.Rows[i]["GroupNo"].ToString());
                items.LedgerName = dt.Rows[i]["LedgerName"].ToString();
                items.MobileNo = dt.Rows[i]["MobileNo"].ToString();
                items.Address = dt.Rows[i]["Address"].ToString();
                items.PinCode = dt.Rows[i]["PinCode"].ToString();
                items.GSTNo = dt.Rows[i]["GSTNo"].ToString();
                items.StateNo = 1;
                items.CityNo = Convert.ToInt64(dt.Rows[i]["CityNo"].ToString());
                items.CityName = dt.Rows[i]["CityName"].ToString();
                items.CompanyNo = CommonFunctions.CompanyNo;
                items.StateCode =Convert.ToInt64( dt.Rows[i]["StateCode"].ToString());
                items.StateName = dt.Rows[i]["StateName"].ToString();
                ucItems.Add(items);
            }
            return ucItems;
        }


        public bool AddMLedger(UCCustomer mledger)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMLedger";

            cmd.Parameters.AddWithValue("@PkSrNo", mledger.PkSrNo);

            cmd.Parameters.AddWithValue("@LedgerNo", mledger.LedgerNo);

            cmd.Parameters.AddWithValue("@GroupNo", mledger.GroupNo);

            cmd.Parameters.AddWithValue("@LedgerName", mledger.LedgerName);

            cmd.Parameters.AddWithValue("@MobileNo", mledger.MobileNo);

            cmd.Parameters.AddWithValue("@Address", mledger.Address);

            cmd.Parameters.AddWithValue("@CityNo", mledger.CityNo);

            cmd.Parameters.AddWithValue("@CityName", mledger.CityName);

            cmd.Parameters.AddWithValue("@StateNo", mledger.StateNo);

            cmd.Parameters.AddWithValue("@StateName", mledger.StateName);

            cmd.Parameters.AddWithValue("@StateCode", mledger.StateCode);

            cmd.Parameters.AddWithValue("@PinCode", mledger.PinCode);

            cmd.Parameters.AddWithValue("@GSTNo", mledger.GSTNo);

            cmd.Parameters.AddWithValue("@CompanyNo", mledger.CompanyNo);

            cmd.Parameters.AddWithValue("@UserDate", mledger.UserDate);


            SqlParameter outParameter = new SqlParameter();
            outParameter.ParameterName = "@ReturnID";
            outParameter.Direction = ParameterDirection.Output;
            outParameter.DbType = DbType.Int32;
            cmd.Parameters.Add(outParameter);

            commandcollection.Add(cmd);
            return true;
        }

        private List<UCReturnCustomer> ExecuteNonQueryStatements()
        {


            List<UCReturnCustomer> lst = new List<UCReturnCustomer>();
            SqlConnection cn = null;
            cn = new SqlConnection(DBConnection.GetConnection_My());
            cn.Open();

            SqlTransaction myTrans;
            myTrans = cn.BeginTransaction();
            //cmd.Transaction = myTrans;
            int cntVchNo = -1;
            try
            {
                for (int i = 0; (i < this.commandcollection.Count); i = (i + 1))
                {
                    if ((this.commandcollection[i] != null))
                    {
                        UCReturnCustomer rItems = new UCReturnCustomer();
                        commandcollection[i].Connection = cn;
                        commandcollection[i].Transaction = myTrans;

                        if (commandcollection[i] != null)
                            commandcollection[i].ExecuteNonQuery();
                        rItems.LedgerNo = Convert.ToInt64(commandcollection[i].Parameters["@ReturnID"].Value);
                        lst.Add(rItems);
                    }
                }

                myTrans.Commit();
                return lst;

            }
            catch (Exception e)
            {
                myTrans.Rollback();
                throw new Exception(e.Message);
            }
            finally
            {
                cn.Close();
            }

        }

        public List<UCReturnCustomer> SaveCustomers(List<UCCustomer> value)
        {
            try
            {
                DBMLedger dbMLedger = new DBMLedger();
                foreach (UCCustomer items in value)
                {
                    items.CompanyNo = CommonFunctions.CompanyNo;
                    items.UserDate = DateTime.Now;
                    dbMLedger.AddMLedger(items);
                }
                return dbMLedger.ExecuteNonQueryStatements();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.StackTrace);
            }


        }

    }
}