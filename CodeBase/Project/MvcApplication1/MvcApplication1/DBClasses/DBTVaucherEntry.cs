﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Data;
using System.Data.SqlClient;
using OM;
using JitClass;
using Jit;
using System.Data;



namespace OM
{
    class DBTVaucherEntry
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDSet = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        private CommandCollection commandcollection = new CommandCollection();

        DataTable dtId = new DataTable();

        public static string strerrormsg;


        public MvcApplication1.DTOClasses.UCTVoucherEntry ModifyTVoucherEntryByID(long ID)
        {
            SqlConnection Con = new SqlConnection(DBConnection.GetConnection_My());
            string sql;
            SqlCommand cmd;
            sql = "Select * from TVoucherEntry where PkVoucherNo =" + ID;
            cmd = new SqlCommand(sql, Con);
            cmd.Connection = Con;
            Con.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (dr.HasRows)
            {
                MvcApplication1.DTOClasses.UCTVoucherEntry MM = new MvcApplication1.DTOClasses.UCTVoucherEntry();
                while (dr.Read())
                {
                    MM.PkVoucherNo = Convert.ToInt32(dr["PkVoucherNo"]);
                    if (!Convert.IsDBNull(dr["FkVoucherNo"])) MM.FkVoucherNo = Convert.ToInt64(dr["FkVoucherNo"]);
                    if (!Convert.IsDBNull(dr["VoucherTypeCode"])) MM.VoucherTypeCode = Convert.ToInt64(dr["VoucherTypeCode"]);
                    if (!Convert.IsDBNull(dr["VoucherUserNo"])) MM.VoucherUserNo = Convert.ToInt64(dr["VoucherUserNo"]);
                    if (!Convert.IsDBNull(dr["VoucherDate"])) MM.VoucherDate = Convert.ToString(dr["VoucherDate"]);
                    if (!Convert.IsDBNull(dr["VoucherTime"])) MM.VoucherTime = Convert.ToString(dr["VoucherTime"]);
                    if (!Convert.IsDBNull(dr["Narration"])) MM.Narration = Convert.ToString(dr["Narration"]);
                    if (!Convert.IsDBNull(dr["Reference"])) MM.Reference = Convert.ToString(dr["Reference"]);
                    if (!Convert.IsDBNull(dr["CompanyNo"])) MM.CompanyNo = Convert.ToInt64(dr["CompanyNo"]);
                    if (!Convert.IsDBNull(dr["BilledAmount"])) MM.BilledAmount = Convert.ToDouble(dr["BilledAmount"]);
                    if (!Convert.IsDBNull(dr["Remark"])) MM.Remark = Convert.ToString(dr["Remark"]);
                    if (!Convert.IsDBNull(dr["IsCancel"])) MM.IsCancel = Convert.ToBoolean(dr["IsCancel"]);
                    if (!Convert.IsDBNull(dr["PayTypeNo"])) MM.PayTypeNo = Convert.ToInt64(dr["PayTypeNo"]);
                    if (!Convert.IsDBNull(dr["RateTypeNo"])) MM.RateTypeNo = Convert.ToInt64(dr["RateTypeNo"]);
                    if (!Convert.IsDBNull(dr["TaxTypeNo"])) MM.TaxTypeNo = Convert.ToInt64(dr["TaxTypeNo"]);
                    if (!Convert.IsDBNull(dr["IsVoucherLock"])) MM.IsVoucherLock = Convert.ToBoolean(dr["IsVoucherLock"]);
                    if (!Convert.IsDBNull(dr["VoucherStatus"])) MM.VoucherStatus = Convert.ToString(dr["VoucherStatus"]);
                    if (!Convert.IsDBNull(dr["UserID"])) MM.UserID = Convert.ToInt64(dr["UserID"]);
                    if (!Convert.IsDBNull(dr["UserDate"])) MM.UserDate = Convert.ToDateTime(dr["UserDate"]);
                    if (!Convert.IsDBNull(dr["ModifiedBy"])) MM.ModifiedBy = Convert.ToString(dr["ModifiedBy"]);
                    if (!Convert.IsDBNull(dr["OrderType"])) MM.OrderType = Convert.ToInt64(dr["OrderType"]);
                    if (!Convert.IsDBNull(dr["ReturnAmount"])) MM.ReturnAmount = Convert.ToDouble(dr["ReturnAmount"]);
                    if (!Convert.IsDBNull(dr["Visibility"])) MM.Visibility = Convert.ToDouble(dr["Visibility"]);
                    if (!Convert.IsDBNull(dr["DiscPercent"])) MM.DiscPercent = Convert.ToDouble(dr["DiscPercent"]);
                    if (!Convert.IsDBNull(dr["DiscAmt"])) MM.DiscAmt = Convert.ToDouble(dr["DiscAmt"]);
                    if (!Convert.IsDBNull(dr["ChargAmount"])) MM.ChargAmount = Convert.ToDouble(dr["ChargAmount"]);
                    if (!Convert.IsDBNull(dr["StatusNo"])) MM.StatusNo = Convert.ToString(dr["StatusNo"]);
                    if (!Convert.IsDBNull(dr["MixMode"])) MM.MixMode = Convert.ToString(dr["MixMode"]);
                    if (!Convert.IsDBNull(dr["HamaliRs"])) MM.HamaliRs = Convert.ToDouble(dr["HamaliRs"]);
                    if (!Convert.IsDBNull(dr["MobilePkSrNo"])) MM.MobilePkSrNo = Convert.ToInt64(dr["MobilePkSrNo"]);
                    if (!Convert.IsDBNull(dr["MobileBillNo"])) MM.MobileBillNo = Convert.ToInt64(dr["MobileBillNo"]);
                    if (!Convert.IsDBNull(dr["MobileUserNo"])) MM.MobileUserNo = Convert.ToInt64(dr["MobileUserNo"]);
                    if (!Convert.IsDBNull(dr["LedgerName"])) MM.LedgerName = Convert.ToString(dr["LedgerName"]);
                    if (!Convert.IsDBNull(dr["LedgerNo"])) MM.LedgerNo = Convert.ToInt64(dr["LedgerNo"]);
                }
                dr.Close();
                return MM;
            }
            else
                dr.Close();
            return new MvcApplication1.DTOClasses.UCTVoucherEntry();
        }

        public MvcApplication1.DTOClasses.UCSaleBillsList GetSaleBill()
        {
            try
            {

                string sql = "SELECT  PkVoucherNo " +
                                   " FROM TVoucherEntry " +
                                   " WHERE VoucherTypeCode = 15" +
                                           "  AND StatusNo in (1,2) And  OrderType=2 And CompanyNo=" + CommonFunctions.CompanyNo + "" +
                                   " ORDER BY PkVoucherNo ";
                DataTable dtPkSrNo = ObjFunction.GetDataView(sql).Table;
                MvcApplication1.DTOClasses.UCSaleBillsList SB = new MvcApplication1.DTOClasses.UCSaleBillsList();
                for (int i = 0; i < dtPkSrNo.Rows.Count; i++)
                {
                    try
                    {
                        SB.salesBills.Add(GetBill_Data(Convert.ToInt64(dtPkSrNo.Rows[i].ItemArray[0].ToString())));
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                return SB;

            }
            catch (Exception ex)
            {
                return null;
            }

        }

        private MvcApplication1.DTOClasses.UCSaleBills GetBill_Data(long PkVoucherNo)
        {

            MvcApplication1.DTOClasses.UCSaleBills SB = new MvcApplication1.DTOClasses.UCSaleBills();
            try
            {
                // ObjFunction.WriteMessage("Start To Get Bill Data", LogLevel.Information, null);


                SB.tvoucherentry = ModifyTVoucherEntryByID(PkVoucherNo);

                string sql = "SELECT     PkVoucherTrnNo, FkVoucherNo, Barcode, ItemName, LangItemName, ItemNo, Qty, Rate, " +
                    " MRP, Amount, NetRate, NetAmount, TaxPercentage, TaxAmount, DiscAmount, HSNCode, IGSTPer, IGSTAmt,  " +
                    " CGSTPer, CGSTAmt, SGSTPer, SGSTAmt, CESSPer, CESSAmt " +
                    " FROM  TStock " +
                    " Where FkVoucherNo=" + PkVoucherNo + "";

                DataTable dtBillItems = ObjFunction.GetDataView(sql).Table;

                for (int i = 0; i < dtBillItems.Rows.Count; i++)
                {
                    MvcApplication1.DTOClasses.UCTStock billItem = new MvcApplication1.DTOClasses.UCTStock();
                    billItem.PkvouchertrnNo = 0;
                    billItem.FkvoucherNo = 0;
                    billItem.Barcode = dtBillItems.Rows[i]["Barcode"].ToString();
                    billItem.Itemname = dtBillItems.Rows[i]["ItemName"].ToString();
                    billItem.LangitemName = dtBillItems.Rows[i]["LangItemName"].ToString();
                    billItem.Qty = Convert.ToDouble(dtBillItems.Rows[i]["Qty"].ToString());
                    billItem.Amount = Convert.ToDouble(dtBillItems.Rows[i]["Amount"].ToString());
                    billItem.Rate = Convert.ToDouble(dtBillItems.Rows[i]["Rate"].ToString());
                    billItem.Mrp = Convert.ToDouble(dtBillItems.Rows[i]["MRP"].ToString()); ;
                    billItem.NetAmount = Convert.ToDouble(dtBillItems.Rows[i]["NetAmount"].ToString());
                    billItem.NetRate = Convert.ToDouble(dtBillItems.Rows[i]["NetRate"].ToString());
                    billItem.Taxpercentage = Convert.ToDouble(dtBillItems.Rows[i]["TaxPercentage"].ToString());
                    billItem.TaxAmount = Convert.ToDouble(dtBillItems.Rows[i]["TaxAmount"].ToString());
                    billItem.DiscAmount = Convert.ToDouble(dtBillItems.Rows[i]["DiscAmount"].ToString());
                    billItem.ItemNo = Convert.ToInt64(dtBillItems.Rows[i]["ItemNo"].ToString());
                    SB.tstock.Add(billItem);
                }

                /*    if (SB.tvoucherentry.PayTypeNo == 2)
                    {
                        sql = " SELECT     TVoucherRefDetails.PkRefTrnNo, TVoucherRefDetails.FkVoucherTrnNo, TVoucherRefDetails.FkVoucherSrNo, TVoucherRefDetails.LedgerNo, TVoucherRefDetails.RefNo, " +
                            " TVoucherRefDetails.TypeOfRef, TVoucherRefDetails.DueDays, TVoucherRefDetails.DueDate, TVoucherRefDetails.Amount, TVoucherRefDetails.SignCode, TVoucherRefDetails.UserID, " +
                            " TVoucherRefDetails.UserDate, TVoucherRefDetails.Modifiedby, TVoucherRefDetails.CompanyNo, TVoucherRefDetails.StatusNo, TVoucherEntry.VoucherDate, TVoucherEntry.PayTypeNo, " +
                            " MPayType.PayTypeName " +
                            " FROM  TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo And TVoucherDetails.SrNo=501 INNER JOIN " +
                            " TVoucherRefDetails ON TVoucherDetails.PkVoucherTrnNo = TVoucherRefDetails.FkVoucherTrnNo INNER JOIN " +
                            " MPayType ON TVoucherEntry.PayTypeNo = MPayType.PKPayTypeNo " +
                            " WHERE     (TVoucherRefDetails.RefNo IN (SELECT     RefNo FROM  TVoucherRefDetails AS TVoucherRefDetails_1 " +
                            " WHERE      (FkVoucherTrnNo IN (SELECT PkVoucherTrnNo FROM TVoucherDetails AS TVoucherDetails_1 " +
                            " WHERE      (FkVoucherNo = " + PkVoucherNo + "))))) ";
                        //  ObjFunction.WriteMessage(sql, LogLevel.Information, null);
                        DataTable dtref = ObjFunction.GetDataView(sql).Table;
                        for (int i = 0; i < dtref.Rows.Count; i++)
                        {
                            MvcApplication1.DTOClasses.UCTVoucherRefDetails tRef = new MvcApplication1.DTOClasses.UCTVoucherRefDetails();


                            tRef.PkreftrnNo = -1;
                            tRef.Refno = 0;
                            tRef.LedgerNo = tvc.LedgerNo;
                            tRef.FkvoucherNo = tvc.PkVoucherNo;
                            tRef.TypeofRef = Convert.ToInt64(dtref.Rows[i]["TypeOfRef"].ToString());
                            tRef.Amount = Convert.ToDouble(dtref.Rows[i]["Amount"].ToString());
                            tRef.DiscAmount = 0;
                            if (dtref.Rows[i]["TypeOfRef"].ToString() == "3")
                            {
                                string stramt = dtref.Compute("Sum(Amount)", "TypeOfRef=2").ToString();
                                tRef.Balamount = Convert.ToDouble((stramt == "") ? "" + tRef.Amount : "" + (tRef.Amount - Convert.ToDouble(stramt)));
                            }
                            else
                                tRef.Balamount = tRef.Amount;

                            tRef.PayDate = dtref.Rows[i]["VoucherDate"].ToString();
                            tRef.PayType = dtref.Rows[i]["PayTypeName"].ToString();
                            tRef.CardNo = "";
                            tRef.Remark = "";
                            SB.tVoucherrefdetails.Add(tRef);
                        }
                    }*/

            }
            catch (Exception ex)
            {
                if (ex is CustomeException.LedgerDetailsNotFound)
                {
                    throw new CustomeException.LedgerDetailsNotFound(ex.Message);
                }
                else
                    throw new Exception(ex.Message);
            }

            return SB;
        }

        private string GetRateType(long RateTypeNo)
        {
            if (RateTypeNo == 1) return "ASaleRate";
            else if (RateTypeNo == 2) return "BSaleRate";
            else if (RateTypeNo == 3) return "CSaleRate";
            else if (RateTypeNo == 4) return "DSaleRate";
            else if (RateTypeNo == 5) return "ESaleRate";
            else if (RateTypeNo == 6) return "MRP";
            else return "";
        }

        public bool AddTVoucherEntry(MvcApplication1.DTOClasses.UCTVoucherEntry tvoucherentry)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTVoucherEntry";

            cmd.Parameters.AddWithValue("@PkVoucherNo", tvoucherentry.PkVoucherNo);

            cmd.Parameters.AddWithValue("@FkVoucherNo", tvoucherentry.FkVoucherNo);

            cmd.Parameters.AddWithValue("@VoucherTypeCode", tvoucherentry.VoucherTypeCode);

            cmd.Parameters.AddWithValue("@VoucherUserNo", tvoucherentry.VoucherUserNo);

            cmd.Parameters.AddWithValue("@VoucherDate", tvoucherentry.VoucherDate);

            cmd.Parameters.AddWithValue("@VoucherTime", tvoucherentry.VoucherTime);

            cmd.Parameters.AddWithValue("@Narration", tvoucherentry.Narration);

            cmd.Parameters.AddWithValue("@Reference", tvoucherentry.Reference);

            cmd.Parameters.AddWithValue("@CompanyNo", tvoucherentry.CompanyNo);

            cmd.Parameters.AddWithValue("@BilledAmount", tvoucherentry.BilledAmount);

            cmd.Parameters.AddWithValue("@Remark", tvoucherentry.Remark);

            cmd.Parameters.AddWithValue("@IsCancel", tvoucherentry.IsCancel);

            cmd.Parameters.AddWithValue("@PayTypeNo", tvoucherentry.PayTypeNo);

            cmd.Parameters.AddWithValue("@RateTypeNo", tvoucherentry.RateTypeNo);

            cmd.Parameters.AddWithValue("@TaxTypeNo", tvoucherentry.TaxTypeNo);

            cmd.Parameters.AddWithValue("@IsVoucherLock", tvoucherentry.IsVoucherLock);

            cmd.Parameters.AddWithValue("@VoucherStatus", tvoucherentry.VoucherStatus);

            cmd.Parameters.AddWithValue("@UserID", tvoucherentry.UserID);

            cmd.Parameters.AddWithValue("@UserDate", tvoucherentry.UserDate);

            cmd.Parameters.AddWithValue("@ModifiedBy", tvoucherentry.ModifiedBy);

            cmd.Parameters.AddWithValue("@OrderType", tvoucherentry.OrderType);

            cmd.Parameters.AddWithValue("@ReturnAmount", tvoucherentry.ReturnAmount);

            cmd.Parameters.AddWithValue("@Visibility", tvoucherentry.Visibility);

            cmd.Parameters.AddWithValue("@DiscPercent", tvoucherentry.DiscPercent);

            cmd.Parameters.AddWithValue("@DiscAmt", tvoucherentry.DiscAmt);

            cmd.Parameters.AddWithValue("@ChargAmount", tvoucherentry.ChargAmount);

            cmd.Parameters.AddWithValue("@MixMode", tvoucherentry.MixMode);

            cmd.Parameters.AddWithValue("@HamaliRs", tvoucherentry.HamaliRs);

            cmd.Parameters.AddWithValue("@MobilePkSrNo", tvoucherentry.MobilePkSrNo);

            cmd.Parameters.AddWithValue("@MobileBillNo", tvoucherentry.MobileBillNo);

            cmd.Parameters.AddWithValue("@MobileUserNo", tvoucherentry.MobileUserNo);

            cmd.Parameters.AddWithValue("@LedgerName", tvoucherentry.LedgerName);

            cmd.Parameters.AddWithValue("@LedgerNo", tvoucherentry.LedgerNo);

            SqlParameter outParameter = new SqlParameter();
            outParameter.ParameterName = "@ReturnID";
            outParameter.Direction = ParameterDirection.Output;
            outParameter.DbType = DbType.Int32;
            cmd.Parameters.Add(outParameter);

            commandcollection.Add(cmd);
            return true;
        }

        public bool AddTStock(MvcApplication1.DTOClasses.UCTStock tstock)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTStock";

            cmd.Parameters.AddWithValue("@PkVoucherTrnNo", tstock.PkvouchertrnNo);

            //  cmd.Parameters.AddWithValue("@FkVoucherNo", tstock.FkvoucherNo);

            cmd.Parameters.AddWithValue("@Barcode", tstock.Barcode);

            cmd.Parameters.AddWithValue("@ItemName", tstock.Itemname);

            cmd.Parameters.AddWithValue("@LangItemName", tstock.LangitemName);

            cmd.Parameters.AddWithValue("@ItemNo", tstock.ItemNo);

            cmd.Parameters.AddWithValue("@Qty", tstock.Qty);

            cmd.Parameters.AddWithValue("@Rate", tstock.Rate);

            cmd.Parameters.AddWithValue("@MRP", tstock.Mrp);

            cmd.Parameters.AddWithValue("@Amount", tstock.Amount);

            cmd.Parameters.AddWithValue("@NetRate", tstock.NetRate);

            cmd.Parameters.AddWithValue("@NetAmount", tstock.NetAmount);

            cmd.Parameters.AddWithValue("@TaxPercentage", tstock.Taxpercentage);

            cmd.Parameters.AddWithValue("@TaxAmount", tstock.TaxAmount);

            cmd.Parameters.AddWithValue("@DiscAmount", tstock.DiscAmount);

            cmd.Parameters.AddWithValue("@HSNCode", tstock.HSNCode);

            cmd.Parameters.AddWithValue("@IGSTPer", tstock.IGSTPer);

            cmd.Parameters.AddWithValue("@IGSTAmt", tstock.IGSTAmt);

            cmd.Parameters.AddWithValue("@CGSTPer", tstock.CGSTPer);

            cmd.Parameters.AddWithValue("@CGSTAmt", tstock.CGSTAmt);

            cmd.Parameters.AddWithValue("@SGSTPer", tstock.SGSTPer);

            cmd.Parameters.AddWithValue("@SGSTAmt", tstock.SGSTAmt);

            cmd.Parameters.AddWithValue("@CESSPer", tstock.CESSPer);

            cmd.Parameters.AddWithValue("@CESSAmt", tstock.CESSAmt);


            commandcollection.Add(cmd);
            return true;
        }

        public bool AddTVoucherRefDetails(MvcApplication1.DTOClasses.UCTVoucherRefDetails tvoucherrefdetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTVoucherRefDetails";

            cmd.Parameters.AddWithValue("@PkRefTrnNo", tvoucherrefdetails.PkreftrnNo);

            //  cmd.Parameters.AddWithValue("@FkVoucherNo", tvoucherrefdetails.FkvoucherNo);

            cmd.Parameters.AddWithValue("@ParkingBillNo", tvoucherrefdetails.ParkingBillNo);

            cmd.Parameters.AddWithValue("@LedgerNo", tvoucherrefdetails.LedgerNo);

            cmd.Parameters.AddWithValue("@RefNo", tvoucherrefdetails.Refno);

            cmd.Parameters.AddWithValue("@TypeOfRef", tvoucherrefdetails.TypeofRef);

            cmd.Parameters.AddWithValue("@Amount", tvoucherrefdetails.Amount);

            cmd.Parameters.AddWithValue("@DiscAmount", tvoucherrefdetails.DiscAmount);

            cmd.Parameters.AddWithValue("@BalAmount", tvoucherrefdetails.Balamount);

            cmd.Parameters.AddWithValue("@PayDate", tvoucherrefdetails.PayDate);

            cmd.Parameters.AddWithValue("@PayType", tvoucherrefdetails.PayType);

            cmd.Parameters.AddWithValue("@CardNo", tvoucherrefdetails.CardNo);

            cmd.Parameters.AddWithValue("@Remark", tvoucherrefdetails.Remark);

            commandcollection.Add(cmd);
            return true;
        }


        private bool AddTParkingBill(TParkingBill tparkingbill)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTParkingBill_Mob";

            cmd.Parameters.AddWithValue("@ParkingBillNo", tparkingbill.ParkingBillNo);

            cmd.Parameters.AddWithValue("@BillNo", tparkingbill.BillNo);

            cmd.Parameters.AddWithValue("@BillDate", tparkingbill.BillDate);

            cmd.Parameters.AddWithValue("@BillTime", tparkingbill.BillTime);

            cmd.Parameters.AddWithValue("@PersonName", tparkingbill.PersonName);

            cmd.Parameters.AddWithValue("@LedgerNo", tparkingbill.LedgerNo);

            cmd.Parameters.AddWithValue("@IsBill", tparkingbill.IsBill);

            cmd.Parameters.AddWithValue("@CompanyNo", tparkingbill.CompanyNo);

            cmd.Parameters.AddWithValue("@IsCancel", tparkingbill.IsCancel);

            cmd.Parameters.AddWithValue("@UserID", tparkingbill.UserID);

            cmd.Parameters.AddWithValue("@UserDate", tparkingbill.UserDate);

            cmd.Parameters.AddWithValue("@Discount", tparkingbill.Discount);

            cmd.Parameters.AddWithValue("@Charges", tparkingbill.Charges);

            cmd.Parameters.AddWithValue("@Remark", tparkingbill.Remark);

            cmd.Parameters.AddWithValue("@RateTypeNo", tparkingbill.RateTypeNo);

            cmd.Parameters.AddWithValue("@TaxTypeNo", tparkingbill.TaxTypeNo);

            cmd.Parameters.AddWithValue("@MobilePkSrNo", tparkingbill.MobilePkSrNo);

            cmd.Parameters.AddWithValue("@MobileBillNo", tparkingbill.MobileBillNo);

            SqlParameter outParameter = new SqlParameter();
            outParameter.ParameterName = "@ReturnID";
            outParameter.Direction = ParameterDirection.Output;
            outParameter.DbType = DbType.Int32;
            cmd.Parameters.Add(outParameter);

            commandcollection.Add(cmd);
            return true;
        }

        private bool AddTParkingBillDetails(TParkingBillDetails tparkingbilldetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTParkingBillDetails";

            cmd.Parameters.AddWithValue("@PkSrNo", tparkingbilldetails.PkSrNo);

            //cmd.Parameters.AddWithValue("@ParkingBillNo", tparkingbilldetails.ParkingBillNo);

            cmd.Parameters.AddWithValue("@Barcode", tparkingbilldetails.Barcode);

            cmd.Parameters.AddWithValue("@Qty", tparkingbilldetails.Qty);

            cmd.Parameters.AddWithValue("@Rate", tparkingbilldetails.Rate);

            cmd.Parameters.AddWithValue("@ItemDisc", tparkingbilldetails.ItemDisc);

            cmd.Parameters.AddWithValue("@UOMNo", tparkingbilldetails.UOMNo);

            cmd.Parameters.AddWithValue("@FkRateSettingNo", tparkingbilldetails.FkRateSettingNo);

            cmd.Parameters.AddWithValue("@ItemNo", tparkingbilldetails.ItemNo);

            cmd.Parameters.AddWithValue("@CompanyNo", 1);

            commandcollection.Add(cmd);
            return true;
        }

        private bool DeleteTVoucherEntryAll(long PkVoucherNo)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteTVoucherEntry";

            cmd.Parameters.AddWithValue("@PkVoucherNo", PkVoucherNo);

            commandcollection.Add(cmd);
            return true;
        }

        private bool DeleteCollection(long PkRefTrnNo)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Delete From TVoucherRefDetails_Mob Where PkRefTrnNo=@PkRefTrnNo";

            cmd.Parameters.AddWithValue("@PkRefTrnNo", PkRefTrnNo);

            commandcollection.Add(cmd);
            return true;
        }

        private bool UpdateBillStatus(TReturnBill tb)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Update TVoucherEntry Set StatusNo=3 Where PkVoucherNo=@PkVoucherNo";

            cmd.Parameters.AddWithValue("@PkVoucherNo",tb.PkSrNo);

            commandcollection.Add(cmd);
            return true;
        }

        private bool AddTVoucherRefDetails_Mob(MvcApplication1.DTOClasses.UCTVoucherRefDetails tvoucherrefdetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTVoucherRefDetails_Mob";

            cmd.Parameters.AddWithValue("@PkRefTrnNo", tvoucherrefdetails.PkreftrnNo);

            cmd.Parameters.AddWithValue("@FkVoucherNo", tvoucherrefdetails.FkvoucherNo);

            //cmd.Parameters.AddWithValue("@ParkingBillNo", tvoucherrefdetails.ParkingBillNo);

            cmd.Parameters.AddWithValue("@LedgerNo", tvoucherrefdetails.LedgerNo);

            cmd.Parameters.AddWithValue("@RefNo", tvoucherrefdetails.Refno);

            cmd.Parameters.AddWithValue("@TypeOfRef", tvoucherrefdetails.TypeofRef);

            cmd.Parameters.AddWithValue("@Amount", tvoucherrefdetails.Amount);

            cmd.Parameters.AddWithValue("@DiscAmount", tvoucherrefdetails.DiscAmount);

            cmd.Parameters.AddWithValue("@BalAmount", tvoucherrefdetails.Balamount);

            cmd.Parameters.AddWithValue("@PayDate", tvoucherrefdetails.PayDate);

            cmd.Parameters.AddWithValue("@PayType", tvoucherrefdetails.PayType);

            cmd.Parameters.AddWithValue("@CardNo", tvoucherrefdetails.CardNo);

            cmd.Parameters.AddWithValue("@Remark", tvoucherrefdetails.Remark);


            commandcollection.Add(cmd);
            return true;
        }



        /*    private long ExecuteNonQueryStatements()
            {


                SqlConnection cn = null;
                cn = new SqlConnection(DBConnection.GetConnection_My());
                cn.Open();

                SqlTransaction myTrans;
                myTrans = cn.BeginTransaction();
                //cmd.Transaction = myTrans;
                int cntVchNo = -1;
                try
                {
                    for (int i = 0; (i < this.commandcollection.Count); i = (i + 1))
                    {
                        if ((this.commandcollection[i] != null))
                        {
                            commandcollection[i].Connection = cn;
                            commandcollection[i].Transaction = myTrans;

                            if (commandcollection[i].CommandText == "AddTParkingBill_Mob")
                            {
                                cntVchNo = i;
                            }
                            if (commandcollection[i].CommandText == "AddTParkingBillDetails")
                            {
                                commandcollection[i].Parameters.AddWithValue("@ParkingBillNo", commandcollection[cntVchNo].Parameters["@ReturnID"].Value);
                            }
                            if (commandcollection[i].CommandText == "AddTVoucherRefDetails_Mob")
                            {
                                if (cntVchNo != -1)
                                    commandcollection[i].Parameters.AddWithValue("@ParkingBillNo", commandcollection[cntVchNo].Parameters["@ReturnID"].Value);
                                else
                                    commandcollection[i].Parameters.AddWithValue("@ParkingBillNo", 0);
                            }



                            if (commandcollection[i] != null)
                                commandcollection[i].ExecuteNonQuery();
                        }
                    }

                    myTrans.Commit();
                    if (cntVchNo == -1)
                        return 0;
                    else
                        return Convert.ToInt64(commandcollection[cntVchNo].Parameters["@ReturnID"].Value);


                }
                catch (Exception e)
                {
                    myTrans.Rollback();
                    throw new Exception(e.Message);
                }
                finally
                {
                    cn.Close();
                }

            }*/

        private long ExecuteNonQueryStatements()
        {


            SqlConnection cn = null;
            cn = new SqlConnection(DBConnection.GetConnection_My());
            cn.Open();

            SqlTransaction myTrans;
            myTrans = cn.BeginTransaction();
            //cmd.Transaction = myTrans;
            int cntVchNo = -1;
            try
            {
                for (int i = 0; (i < this.commandcollection.Count); i = (i + 1))
                {
                    if ((this.commandcollection[i] != null))
                    {
                        commandcollection[i].Connection = cn;
                        commandcollection[i].Transaction = myTrans;

                        if (commandcollection[i].CommandText == "AddTVoucherEntry")
                        {
                            cntVchNo = i;
                        }
                        if (commandcollection[i].CommandText == "AddTStock")
                        {
                            commandcollection[i].Parameters.AddWithValue("@FkVoucherNo", commandcollection[cntVchNo].Parameters["@ReturnID"].Value);
                        }
                        if (commandcollection[i].CommandText == "AddTVoucherRefDetails")
                        {
                            if (cntVchNo != -1)
                                commandcollection[i].Parameters.AddWithValue("@FkVoucherNo", commandcollection[cntVchNo].Parameters["@ReturnID"].Value);
                            else
                                commandcollection[i].Parameters.AddWithValue("@FkVoucherNo", 0);
                        }



                        if (commandcollection[i] != null)
                            commandcollection[i].ExecuteNonQuery();
                    }
                }

                myTrans.Commit();
                if (cntVchNo == -1)
                    return 0;
                else
                    return Convert.ToInt64(commandcollection[cntVchNo].Parameters["@ReturnID"].Value);


            }
            catch (Exception e)
            {
                myTrans.Rollback();
                throw new Exception(e.Message);
            }
            finally
            {
                cn.Close();
            }

        }

        public bool ExecuteNonQueryStatementsCheque()
        {

            SqlConnection cn = null;
            cn = new SqlConnection(DBConnection.GetConnection_My());
            cn.Open();

            SqlTransaction myTrans;
            myTrans = cn.BeginTransaction();
            //cmd.Transaction = myTrans;
            try
            {
                for (int i = 0; (i < this.commandcollection.Count); i = (i + 1))
                {
                    if ((this.commandcollection[i] != null))
                    {
                        commandcollection[i].Connection = cn;
                        commandcollection[i].Transaction = myTrans;

                        if (commandcollection[i] != null)
                            commandcollection[i].ExecuteNonQuery();
                    }
                }

                myTrans.Commit();
                return true;
            }
            catch (Exception e)
            {
                myTrans.Rollback();

                if (e.GetBaseException().Message == "")
                {
                    strerrormsg = e.Message;
                }
                else
                {
                    strerrormsg = e.GetBaseException().Message;
                }
                return false;
            }
            finally
            {
                cn.Close();
            }
            //________________________________________________________________________________________________________________________________________________________________________________________________________________________
        }

        private long SaveCollection_Data(MvcApplication1.DTOClasses.UCTVoucherRefDetails SB)
        {

            DBTVaucherEntry dbTVaucherEntry = new DBTVaucherEntry();

            if (SB.PkreftrnNo != 0)
                dbTVaucherEntry.DeleteCollection(SB.PkreftrnNo);
            dbTVaucherEntry.AddTVoucherRefDetails_Mob(SB);
            return dbTVaucherEntry.ExecuteNonQueryStatements();
        }

        private long SaveBill_Data(MvcApplication1.DTOClasses.UCSaleBills SB)
        {

            try
            {

                MvcApplication1.DTOClasses.UCTVoucherEntry tvch = SB.tvoucherentry;
                DBTVaucherEntry dbTVaucherEntry = new DBTVaucherEntry();
                long ID;

                if (CommonFunctions.OrderType == (long)2)
                {
                    tvch.Remark = ObjQry.ReturnString("Select UserName From MMobileUser Where PkSrNo=" + CommonFunctions.RegNo + "");
                    ID = ObjQry.ReturnLong("Select PkVoucherNo From TVoucherEntry Where  FkVoucherNo=" + tvch.MobilePkSrNo + " And MobileBillNo=" + tvch.MobileBillNo + " And OrderType=" + CommonFunctions.OrderType + "  And MobileUserNo=" + CommonFunctions.RegNo + " ");
                    tvch.FkVoucherNo = tvch.MobilePkSrNo;
                }
                else
                    ID = ObjQry.ReturnLong("Select PkVoucherNo From TVoucherEntry Where FkVoucherNo=" + tvch.FkVoucherNo + " And OrderType=" + CommonFunctions.OrderType + "  And CompanyNo =" + CommonFunctions.CompanyNo + " ");

                

                if (ID != 0)
                {
                    if (ObjQry.ReturnLong("Select StatusNo From TVoucherEntry Where PkVoucherNo=" + ID + "") == 3)
                        return ID;

                    dbTVaucherEntry.DeleteTVoucherEntryAll(ID);
                }

                tvch.PkVoucherNo = ID;
                tvch.OrderType = CommonFunctions.OrderType;
                tvch.VoucherTypeCode = 15;
                tvch.TaxTypeNo = 38;
                tvch.CompanyNo = CommonFunctions.CompanyNo;
                tvch.MobileUserNo = CommonFunctions.RegNo;
                tvch.UserDate = DateTime.Now;
                tvch.UserID = CommonFunctions.RegNo;
                
                dbTVaucherEntry.AddTVoucherEntry(tvch);

                foreach (MvcApplication1.DTOClasses.UCTStock st in SB.tstock)
                {
                    /*  TParkingBillDetails tbd = new TParkingBillDetails();
                      tbd.PkSrNo = 0;
                      tbd.ItemNo = st.ItemNo;
                      tbd.UOMNo = ObjQry.ReturnLong("Select UOMNo From MUom Where UomName='" + st.UomName + "'");
                      tbd.Rate = st.Rate;
                      tbd.Qty = st.Qty;
                      tbd.ItemDisc = st.DiscAmount;
                      tbd.Barcode = st.Barcode;
                      tbd.CompanyNo = 1;
                      tbd.FkRateSettingNo = ObjQry.ReturnLong("Select top 1 PkSrNo From MRateSetting Where ItemNo=" + st.ItemNo + " And ISActive='True' And MRP=" + st.Mrp + "");
                      dbTVaucherEntry.AddTParkingBillDetails(tbd);*/
                    dbTVaucherEntry.AddTStock(st);
                }

                foreach (MvcApplication1.DTOClasses.UCTVoucherRefDetails st in SB.tVoucherrefdetails)
                {
                    dbTVaucherEntry.AddTVoucherRefDetails(st);
                }

                return dbTVaucherEntry.ExecuteNonQueryStatements();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

        }

        private int GetRateType(string RateType)
        {
            string str = RateType;
            int val = 0;
            if (str == "ASaleRate") val = 1;
            else if (str == "BSaleRate") val = 2;
            else if (str == "CSaleRate") val = 3;
            else if (str == "DSaleRate") val = 4;
            else if (str == "ESaleRate") val = 5;
            else if (str == "MRP") val = 6;
            return val;
        }

        public List<TReturnBill> SaveBill(MvcApplication1.DTOClasses.UCSaleBillsList lst_SB)
        {
            try
            {
                List<TReturnBill> lstRe = new List<TReturnBill>();

                foreach (MvcApplication1.DTOClasses.UCSaleBills SB in lst_SB.salesBills)
                {
                    TReturnBill rbill = new TReturnBill();
                    if (CommonFunctions.OrderType == 2)
                        rbill.PkSrNo = SB.tvoucherentry.MobilePkSrNo;
                    else
                        rbill.PkSrNo = SB.tvoucherentry.FkVoucherNo;
                    rbill.FkVoucherNo = SaveBill_Data(SB);
                    lstRe.Add(rbill);
                }

                return lstRe;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        public void SaveBillStatus(List<TReturnBill> value)
        {
            DBTVaucherEntry dbTVaucherEntry = new DBTVaucherEntry();
            
            foreach (TReturnBill item in value)
            {
             dbTVaucherEntry.UpdateBillStatus(item);
            }

            dbTVaucherEntry.ExecuteNonQueryStatementsCheque();

        }

        public List<TReturnBill> SaveCollectionBill(List<MvcApplication1.DTOClasses.UCTVoucherRefDetails> lst_SB)
        {
            List<TReturnBill> lstRe = new List<TReturnBill>();
            foreach (MvcApplication1.DTOClasses.UCTVoucherRefDetails SB in lst_SB)
            {
                SaveCollection_Data(SB);
                TReturnBill rbill = new TReturnBill();
                rbill.PkSrNo = SB.PkSrNo;
                rbill.FkVoucherNo = ObjQry.ReturnLong("Select Max(PkRefTrnNo) From TVoucherRefDetails_Mob ");
                lstRe.Add(rbill);
            }
            return lstRe;
        }

    }

    public class TReturnBill
    {
        public long PkSrNo;
        public long FkVoucherNo;
    }

    public class TParkingBill
    {
        private long mParkingBillNo;
        private long mBillNo;
        private DateTime mBillDate;
        private DateTime mBillTime;
        private string mPersonName;
        private long mLedgerNo;
        private bool mIsBill;
        private long mFKVoucherNo;
        private long mCompanyNo;
        private bool mIsCancel;
        private long mUserID;
        private DateTime mUserDate;
        private int mStatusNo;
        private double mDiscount;
        private double mCharges;
        private string mRemark;
        private long mRateTypeNo;
        private long mTaxTypeNo;
        private long mMobilePkSrNo;
        private long mMobileBillNo;
        private string Mmsg;

        /// <summary>
        /// This Properties use for ParkingBillNo
        /// </summary>
        public long ParkingBillNo
        {
            get { return mParkingBillNo; }
            set { mParkingBillNo = value; }
        }
        /// <summary>
        /// This Properties use for BillNo
        /// </summary>
        public long BillNo
        {
            get { return mBillNo; }
            set { mBillNo = value; }
        }
        /// <summary>
        /// This Properties use for BillDate
        /// </summary>
        public DateTime BillDate
        {
            get { return mBillDate; }
            set { mBillDate = value; }
        }
        /// <summary>
        /// This Properties use for BillTime
        /// </summary>
        public DateTime BillTime
        {
            get { return mBillTime; }
            set { mBillTime = value; }
        }
        /// <summary>
        /// This Properties use for PersonName
        /// </summary>
        public string PersonName
        {
            get { return mPersonName; }
            set { mPersonName = value; }
        }
        /// <summary>
        /// This Properties use for LedgerNo
        /// </summary>
        public long LedgerNo
        {
            get { return mLedgerNo; }
            set { mLedgerNo = value; }
        }
        /// <summary>
        /// This Properties use for IsBill
        /// </summary>
        public bool IsBill
        {
            get { return mIsBill; }
            set { mIsBill = value; }
        }
        /// <summary>
        /// This Properties use for FKVoucherNo
        /// </summary>
        public long FKVoucherNo
        {
            get { return mFKVoucherNo; }
            set { mFKVoucherNo = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for IsCancel
        /// </summary>
        public bool IsCancel
        {
            get { return mIsCancel; }
            set { mIsCancel = value; }
        }
        /// <summary>
        /// This Properties use for UserID
        /// </summary>
        public long UserID
        {
            get { return mUserID; }
            set { mUserID = value; }
        }
        /// <summary>
        /// This Properties use for UserDate
        /// </summary>
        public DateTime UserDate
        {
            get { return mUserDate; }
            set { mUserDate = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public int StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for Discount
        /// </summary>
        public double Discount
        {
            get { return mDiscount; }
            set { mDiscount = value; }
        }
        /// <summary>
        /// This Properties use for Charges
        /// </summary>
        public double Charges
        {
            get { return mCharges; }
            set { mCharges = value; }
        }
        /// <summary>
        /// This Properties use for Remark
        /// </summary>
        public string Remark
        {
            get { return mRemark; }
            set { mRemark = value; }
        }
        /// <summary>
        /// This Properties use for RateTypeNo
        /// </summary>
        public long RateTypeNo
        {
            get { return mRateTypeNo; }
            set { mRateTypeNo = value; }
        }
        /// <summary>
        /// This Properties use for TaxTypeNo
        /// </summary>
        public long TaxTypeNo
        {
            get { return mTaxTypeNo; }
            set { mTaxTypeNo = value; }
        }

        public long MobilePkSrNo
        {
            get { return mMobilePkSrNo; }
            set { mMobilePkSrNo = value; }
        }

        public long MobileBillNo
        {
            get { return mMobileBillNo; }
            set { mMobileBillNo = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }


    public class TParkingBillDetails
    {
        private long mPkSrNo;
        private long mParkingBillNo;
        private string mBarcode;
        private double mQty;
        private double mRate;
        private double mItemDisc;
        private long mUOMNo;
        private int mStatusNo;
        private long mCompanyNo;
        private long mFkRateSettingNo;
        private long mItemNo;
        private string Mmsg;

        /// <summary>
        /// This Properties use for PkSrNo
        /// </summary>
        public long PkSrNo
        {
            get { return mPkSrNo; }
            set { mPkSrNo = value; }
        }
        /// <summary>
        /// This Properties use for ParkingBillNo
        /// </summary>
        public long ParkingBillNo
        {
            get { return mParkingBillNo; }
            set { mParkingBillNo = value; }
        }
        /// <summary>
        /// This Properties use for Barcode
        /// </summary>
        public string Barcode
        {
            get { return mBarcode; }
            set { mBarcode = value; }
        }
        /// <summary>
        /// This Properties use for Qty
        /// </summary>
        public double Qty
        {
            get { return mQty; }
            set { mQty = value; }
        }
        /// <summary>
        /// This Properties use for Rate
        /// </summary>
        public double Rate
        {
            get { return mRate; }
            set { mRate = value; }
        }
        /// <summary>
        /// This Properties use for ItemDisc
        /// </summary>
        public double ItemDisc
        {
            get { return mItemDisc; }
            set { mItemDisc = value; }
        }
        /// <summary>
        /// This Properties use for UOMNo
        /// </summary>
        public long UOMNo
        {
            get { return mUOMNo; }
            set { mUOMNo = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public int StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for FkRateSettingNo
        /// </summary>
        public long FkRateSettingNo
        {
            get { return mFkRateSettingNo; }
            set { mFkRateSettingNo = value; }
        }
        /// <summary>
        /// This Properties use for ItemNo
        /// </summary>
        public long ItemNo
        {
            get { return mItemNo; }
            set { mItemNo = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }

}
