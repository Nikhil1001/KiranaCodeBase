﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Data;
using System.Data.SqlClient;
using OM;
using JitClass;
using Jit;
using System.Data;


namespace OM
{
    public class DBMStockItems
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDSet = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        private CommandCollection commandcollection = new CommandCollection();

        public static string strerrormsg;

        public bool AddMStockItems(MvcApplication1.DTOClasses.UCMStockItems mstockitems)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMStockItems";

            cmd.Parameters.AddWithValue("@PkSrNo", mstockitems.pksrno);

            cmd.Parameters.AddWithValue("@ItemNo", mstockitems.itemno);

            cmd.Parameters.AddWithValue("@BrandName", mstockitems.brandname);

            cmd.Parameters.AddWithValue("@ItemName", mstockitems.itemname);

            cmd.Parameters.AddWithValue("@LangFullDesc", mstockitems.langfulldesc);

            cmd.Parameters.AddWithValue("@MRP", mstockitems.mrp);

            cmd.Parameters.AddWithValue("@ARate", mstockitems.arate);

            cmd.Parameters.AddWithValue("@BRate", mstockitems.brate);

            cmd.Parameters.AddWithValue("@CRate", mstockitems.crate);

            cmd.Parameters.AddWithValue("@Uom", mstockitems.uom);

            cmd.Parameters.AddWithValue("@Barcode", mstockitems.barcode);

            cmd.Parameters.AddWithValue("@ItemNameDisp", mstockitems.itemnamedisp);

            cmd.Parameters.AddWithValue("@TaxRate", mstockitems.taxrate);

            cmd.Parameters.AddWithValue("@IGSTPer", mstockitems.igstper);

            cmd.Parameters.AddWithValue("@CGSTPer", mstockitems.cgstper);

            cmd.Parameters.AddWithValue("@SGSTPer", mstockitems.sgstper);

            cmd.Parameters.AddWithValue("@CessPer", mstockitems.cessper);

            cmd.Parameters.AddWithValue("@HSNCode", mstockitems.hsncode);

            cmd.Parameters.AddWithValue("@ImagePath", mstockitems.imagepath);

            cmd.Parameters.AddWithValue("@CompanyNo", mstockitems.companyno);

            cmd.Parameters.AddWithValue("@UserID", mstockitems.userid);

            cmd.Parameters.AddWithValue("@UserDate", mstockitems.userdate);


            SqlParameter outParameter = new SqlParameter();
            outParameter.ParameterName = "@ReturnID";
            outParameter.Direction = ParameterDirection.Output;
            outParameter.DbType = DbType.Int32;
            cmd.Parameters.Add(outParameter);

            commandcollection.Add(cmd);
            return true;
        }

        private List<MvcApplication1.DTOClasses.UCReturnStockItems> ExecuteNonQueryStatements()
        {


            List<MvcApplication1.DTOClasses.UCReturnStockItems> lst = new List<MvcApplication1.DTOClasses.UCReturnStockItems>();
            SqlConnection cn = null;
            cn = new SqlConnection(DBConnection.GetConnection_My());
            cn.Open();

            SqlTransaction myTrans;
            myTrans = cn.BeginTransaction();
            //cmd.Transaction = myTrans;
            int cntVchNo = -1;
            try
            {
                for (int i = 0; (i < this.commandcollection.Count); i = (i + 1))
                {
                    if ((this.commandcollection[i] != null))
                    {
                        MvcApplication1.DTOClasses.UCReturnStockItems rItems = new MvcApplication1.DTOClasses.UCReturnStockItems();
                        commandcollection[i].Connection = cn;
                        commandcollection[i].Transaction = myTrans;

                        if (commandcollection[i] != null)
                            commandcollection[i].ExecuteNonQuery();
                        rItems.itemno = Convert.ToInt64(commandcollection[i].Parameters["@ReturnID"].Value);
                        lst.Add(rItems);
                    }
                }

                myTrans.Commit();
                return lst;

            }
            catch (Exception e)
            {
                myTrans.Rollback();
                throw new Exception(e.Message);
            }
            finally
            {
                cn.Close();
            }

        }

        public List<MvcApplication1.DTOClasses.UCStockItems> GetStockItems()
        {
            string Sql = " SELECT PkSrNo, ItemNo, BrandName, ItemName, LangFullDesc, MRP, ARate, BRate, " +
                         " CRate, Uom, Barcode, ItemNameDisp, TaxRate, IGSTPer, CGSTPer, SGSTPer, CessPer, HSNCode, ImagePath,  " +
                         " CompanyNo, StatusNo, UserID, UserDate " +
                         " FROM MStockItems " +
                         " WHERE  CompanyNo=" + CommonFunctions.CompanyNo + " ";
            DataTable dt = ObjFunction.GetDataView(Sql).Table;
            List<MvcApplication1.DTOClasses.UCStockItems> ucItems = new List<MvcApplication1.DTOClasses.UCStockItems>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MvcApplication1.DTOClasses.UCStockItems items = new MvcApplication1.DTOClasses.UCStockItems();
                items.ItemNo = Convert.ToInt64(dt.Rows[i]["ItemNo"].ToString());
                items.ItemName = dt.Rows[i]["ItemName"].ToString();
                items.GroupNo = 0;
                items.BrandName = dt.Rows[i]["BrandName"].ToString();
                items.Uom = dt.Rows[i]["Uom"].ToString();
                items.MRP = Convert.ToDouble(dt.Rows[i]["MRP"].ToString());
                items.Rate = Convert.ToDouble(dt.Rows[i]["BRate"].ToString());
                items.TaxNo = 0;
                items.TaxRate = Convert.ToDouble(dt.Rows[i]["TaxRate"].ToString());
                items.Barcode = dt.Rows[i]["Barcode"].ToString();
                items.ShortCode = dt.Rows[i]["Barcode"].ToString();
                items.ItemNameDisp = dt.Rows[i]["ItemNameDisp"].ToString();
                items.HSNCode = dt.Rows[i]["HSNCode"].ToString();
                items.IGSTPer = Convert.ToDouble(dt.Rows[i]["IGSTPer"].ToString());
                items.CGSTPer = Convert.ToDouble(dt.Rows[i]["CGSTPer"].ToString());
                items.SGSTPer = Convert.ToDouble(dt.Rows[i]["SGSTPer"].ToString());
                items.CessPer = Convert.ToDouble(dt.Rows[i]["CessPer"].ToString());

                ucItems.Add(items);
            }
            return ucItems;
        }

        public List<MvcApplication1.DTOClasses.UCReturnStockItems> SaveStockItems(List<MvcApplication1.DTOClasses.UCMStockItems> value)
        {
            try
            {
                DBMStockItems dbMStockItems = new DBMStockItems();
                foreach (MvcApplication1.DTOClasses.UCMStockItems items in value)
                {
                    items.companyno = CommonFunctions.CompanyNo;
                    items.userid = 0;
                    items.userdate = DateTime.Now;
                    items.langfulldesc = "";
                    items.imagepath = "";
                    dbMStockItems.AddMStockItems(items);
                }
                return dbMStockItems.ExecuteNonQueryStatements();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.StackTrace);
            }


        }


    }
}