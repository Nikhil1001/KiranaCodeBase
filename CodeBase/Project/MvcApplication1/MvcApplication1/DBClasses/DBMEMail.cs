﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;

namespace OM
{
    public class DBMEMail
    {

        public void SendMail(MvcApplication1.DTOClasses.UCEMail ucEmail)
        {
            Attachment attach = null;
            try
            {

                MailMessage message = new MailMessage();
                message.From = new MailAddress("support@rshopnow.com");
                message.To.Add(new MailAddress("omSystem101@gmail.com"));
                message.Subject = ucEmail.Subject;
                message.Body = ucEmail.Body;
                SmtpClient client = new SmtpClient();               
                client.Send(message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                if (attach != null)
                    attach.Dispose();
            }
        }
    }
}