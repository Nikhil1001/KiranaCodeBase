﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JitClass;
using Jit;
using System.Data;

namespace OM
{
    public class DBMCity
    {
        CommonFunctions obj = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDSet = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        public List<MvcApplication1.DTOClasses.UCCity> GetCity()
        {

            string Sql = " SELECT Distinct CityNo,CityName From MLedger Where CompanyNo=" + CommonFunctions.CompanyNo + "";   

            DataTable dt = obj.GetDataView(Sql).Table;
            List<MvcApplication1.DTOClasses.UCCity> ucItems = new List<MvcApplication1.DTOClasses.UCCity>();
            
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MvcApplication1.DTOClasses.UCCity items = new MvcApplication1.DTOClasses.UCCity();
                items.CityNo = Convert.ToInt64(dt.Rows[i]["CityNo"].ToString());
                items.CityName = dt.Rows[i]["CityName"].ToString();
                ucItems.Add(items);
            }
            return ucItems;
        }
    }
}