﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JitClass;
using Jit;
using System.Data;
using System.Data.SqlClient;

namespace OM
{
    public class DBMRegistration
    {
        CommonFunctions obj = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDSet = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();
        private CommandCollection commandcollection = new CommandCollection();
        public static string strerrormsg;

        public MMobileUser SaveRegistration(MvcApplication1.DTOClasses.UCRegistration ucItems)
        {

            DBMRegistration dbMRegistration = new DBMRegistration();

            long CompanyNo = ObjQry.ReturnLong("Select IsNull(CompanyNo,0) From MCompany Where Email_ID='" + ucItems.StoreEmail_ID + "' And Password='" + ucItems.StorePassword + "' ", DBConnection.GetConnection_My());
            if (CompanyNo == 0)
            {
                throw new Exception("User Id and Password Incorrect");
            }

            long pkSrNo = ObjQry.ReturnLong("Select top 1 IsNull(PkSrNo,0) From MMobileUser Where DeviceID='"+ucItems.DeviceID+"' And IsActive='false' order by PkSrNo Desc", DBConnection.GetConnection_My());
            if(pkSrNo!=0)
                throw new Exception("licensing details not updated Please contact software team.");
            

            DataTable dtComp = obj.GetDataView("Select CompanyName,AddressCode,PhoneNo From MCompany Where CompanyNo=" + CompanyNo + "").Table;

            MMobileUser items = new MMobileUser();
            items.PkSrNo = 0;
            items.UserName = ucItems.UserName;
            items.DeviceID = ucItems.DeviceID;
            items.Email_ID = ucItems.Email_ID;
            items.MobileNo = ucItems.MobileNo;
            items.RegDate = DateTime.Now.Date;
            items.UserID = ucItems.UserID;
            items.Password = ucItems.Password;
            items.RegNo = Guid.NewGuid().ToString();
            items.CompanyNo = CompanyNo;
            items.CompanyName = dtComp.Rows[0]["CompanyName"].ToString();
            items.CompAddess = dtComp.Rows[0]["AddressCode"].ToString();
            items.CompPhoneNo = dtComp.Rows[0]["PhoneNo"].ToString();
            items.IsActive = false;
            items.UserID = ucItems.UserID;

            dbMRegistration.AddMMobileUser(items);

            long ID = dbMRegistration.ExecuteNonQueryStatements_New();
            if (ID == 0)
            {
                throw new Exception(items.msg);
            }
            else
            {
                return GetRegistration(ID);
            }
        }

        public MMobileUser GetRegistration(long ID)
        {
            DataTable dt = obj.GetDataView("Select * From MMobileUser Where PkSrNo='" + ID + "' ").Table;

            if (dt.Rows.Count == 0)
                throw new Exception("Registration Not Found,Please Register the Devices");

            MMobileUser items = new MMobileUser();
            items.UserName = dt.Rows[0]["UserName"].ToString();
            items.RegNo = dt.Rows[0]["RegNo"].ToString();
            items.MobileNo = dt.Rows[0]["MobileNo"].ToString();
            items.Email_ID = dt.Rows[0]["Email_ID"].ToString();
            items.UserID = dt.Rows[0]["UserID"].ToString();
            items.Password = dt.Rows[0]["Password"].ToString();
            items.RegDate = Convert.ToDateTime(dt.Rows[0]["RegDate"].ToString());
            items.IsActive = Convert.ToBoolean(dt.Rows[0]["IsActive"].ToString());
            items.DeviceID = dt.Rows[0]["DeviceID"].ToString();
            items.CompanyNo = Convert.ToInt64(dt.Rows[0]["CompanyNo"].ToString());
            items.CompanyName = dt.Rows[0]["CompanyName"].ToString();
            return items;
        }

        private bool AddMMobileUser(MMobileUser mmobileuser)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMMobileUser";

            cmd.Parameters.AddWithValue("@PkSrNo", mmobileuser.PkSrNo);

            cmd.Parameters.AddWithValue("@RegNo", mmobileuser.RegNo);

            cmd.Parameters.AddWithValue("@RegDate", mmobileuser.RegDate);

            cmd.Parameters.AddWithValue("@UserName", mmobileuser.UserName);

            cmd.Parameters.AddWithValue("@MobileNo", mmobileuser.MobileNo);

            cmd.Parameters.AddWithValue("@Email_ID", mmobileuser.Email_ID);

            cmd.Parameters.AddWithValue("@CompanyName", mmobileuser.CompanyName);

            cmd.Parameters.AddWithValue("@CompAddess", mmobileuser.CompAddess);

            cmd.Parameters.AddWithValue("@CompPhoneNo", mmobileuser.CompPhoneNo);

            cmd.Parameters.AddWithValue("@UserID", mmobileuser.UserID);

            cmd.Parameters.AddWithValue("@Password", mmobileuser.Password);

            cmd.Parameters.AddWithValue("@DeviceID", mmobileuser.DeviceID);

            cmd.Parameters.AddWithValue("@CompanyNo", mmobileuser.CompanyNo);

            cmd.Parameters.AddWithValue("@IsActive", mmobileuser.IsActive);



            SqlParameter outParameter = new SqlParameter();
            outParameter.ParameterName = "@ReturnID";
            outParameter.Direction = ParameterDirection.Output;
            outParameter.DbType = DbType.Int32;
            cmd.Parameters.Add(outParameter);

            commandcollection.Add(cmd);
            return true;
        }

        public long ExecuteNonQueryStatements_New()
        {

            SqlConnection cn = null;
            cn = new SqlConnection(DBConnection.GetConnection_My());
            cn.Open();

            SqlTransaction myTrans;
            myTrans = cn.BeginTransaction();

            try
            {
                for (int i = 0; (i < this.commandcollection.Count); i = (i + 1))
                {
                    if ((this.commandcollection[i] != null))
                    {
                        commandcollection[i].Connection = cn;
                        commandcollection[i].Transaction = myTrans;

                        if (commandcollection[i] != null)
                            commandcollection[i].ExecuteNonQuery();
                    }
                }

                myTrans.Commit();
                return Convert.ToInt64(commandcollection[0].Parameters["@ReturnID"].Value);
            }
            catch (Exception e)
            {
                myTrans.Rollback();

                if (e.GetBaseException().Message == "")
                {
                    strerrormsg = e.Message;
                }
                else
                {
                    strerrormsg = e.GetBaseException().Message;
                }
                return 0;
            }
            finally
            {
                cn.Close();
            }
            //________________________________________________________________________________________________________________________________________________________________________________________________________________________
        }

        public class MMobileUser
        {
            private long mPkSrNo;
            private string mRegNo;
            private DateTime mRegDate;
            private string mUserName;
            private string mMobileNo;
            private string mEmail_ID;
            private string mCompanyName;
            private string mCompAddess;
            private string mCompPhoneNo;
            private string mUserID;
            private string mPassword;
            private string mDeviceID;
            private bool mIsActive;
            private long mCompanyNo;
            private string Mmsg;
            public long PkSrNo
            {
                get { return mPkSrNo; }
                set { mPkSrNo = value; }
            }
            public string RegNo
            {
                get { return mRegNo; }
                set { mRegNo = value; }
            }
            public DateTime RegDate
            {
                get { return mRegDate; }
                set { mRegDate = value; }
            }
            public string UserName
            {
                get { return mUserName; }
                set { mUserName = value; }
            }
            public string MobileNo
            {
                get { return mMobileNo; }
                set { mMobileNo = value; }
            }
            public string Email_ID
            {
                get { return mEmail_ID; }
                set { mEmail_ID = value; }
            }
            public string CompanyName
            {
                get { return mCompanyName; }
                set { mCompanyName = value; }
            }
            public string CompAddess
            {
                get { return mCompAddess; }
                set { mCompAddess = value; }
            }
            public string CompPhoneNo
            {
                get { return mCompPhoneNo; }
                set { mCompPhoneNo = value; }
            }
            public string UserID
            {
                get { return mUserID; }
                set { mUserID = value; }
            }
            public string Password
            {
                get { return mPassword; }
                set { mPassword = value; }
            }
            public string DeviceID
            {
                get { return mDeviceID; }
                set { mDeviceID = value; }
            }
            public long CompanyNo
            {
                get { return mCompanyNo; }
                set { mCompanyNo = value; }
            }
            public bool IsActive
            {
                get { return mIsActive; }
                set { mIsActive = value; }
            }
            public string msg
            {
                get { return Mmsg; }
                set { Mmsg = value; }
            }
        }

        public void CheckStroreID(string StoreID)
        {
            CommonFunctions.CompanyNo = ObjQry.ReturnLong("Select ISNULL(CompanyNo,0) From MCompany Where StoreID='" + StoreID + "'");
            if (CommonFunctions.CompanyNo == 0)
            {
                throw new Exception("This Devices company Not Register,Please register the Company");
            }
        }

        public void CheckMobileID(string RegID)
        {

            CommonFunctions.RegNo = ObjQry.ReturnLong("Select ISNULL(PkSrNo,0) From MMobileUser Where RegNo='" + RegID + "' ");
            if (CommonFunctions.RegNo == 0)
            {
                throw new Exception("This Devices Not Register,Please register the Devices");
            }
            CommonFunctions.CompanyNo = ObjQry.ReturnLong("Select ISNULL(CompanyNo,0) From MMobileUser Where PkSrNo='" + CommonFunctions.RegNo + "'");
        }

        public string GetLatestVersion()
        {
            return ObjQry.ReturnString("Select AppVersion From MSetting Where AppType=1");
        }

    }


}