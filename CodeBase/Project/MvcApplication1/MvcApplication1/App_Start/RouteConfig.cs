﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MvcApplication1
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "StockItems",
                 url: "{controller}/{action}/{id}",
            defaults: new { controller = "StockItems", action = "Index", id = UrlParameter.Optional }
        );

            routes.MapRoute(
            name: "Customer",
                 url: "{controller}/{action}/{id}",
            defaults: new { controller = "Customer", action = "Index", id = UrlParameter.Optional }
        );

            routes.MapRoute(
          name: "City",
               url: "{controller}/{action}/{id}",
          defaults: new { controller = "City", action = "Index", id = UrlParameter.Optional }
      );

            routes.MapRoute(
        name: "Registration",
             url: "{controller}/{action}/{id}",
        defaults: new { controller = "Registration", action = "Index", id = UrlParameter.Optional }
    );


            routes.MapRoute(
        name: "Bill",
             url: "{controller}/{action}/{id}",
        defaults: new { controller = "Bill", action = "Index", id = UrlParameter.Optional }
    );


            routes.MapRoute(
        name: "Setup",
             url: "{controller}/{action}/{id}",
        defaults: new { controller = "Setup", action = "Index", id = UrlParameter.Optional }
    );

            routes.MapRoute(
      name: "Email",
           url: "{controller}/{action}/{id}",
      defaults: new { controller = "Email", action = "Index", id = UrlParameter.Optional }
    );

        }
    }
}