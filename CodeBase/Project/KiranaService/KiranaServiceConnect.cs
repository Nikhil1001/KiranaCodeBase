﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using OM;
using JitControls;

namespace KiranaService
{
    public partial class KiranaServiceConnect : ServiceBase
    {
        OMCommonClass cc = new OMCommonClass();
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();
        Security secure = new Security();

        Timer tmrInital;

        public KiranaServiceConnect()
        {
            InitializeComponent();

            tmrInital = new Timer();
            tmrInital.Interval = 1000 * 60 * 1;// 1 Min
            tmrInital.Elapsed += new ElapsedEventHandler(Start_Synsc);
            tmrInital.Enabled = true;

        }

        private void SetConnection()
        {
            try
            {
                CommonFunctions.ServerName = System.Net.Dns.GetHostName().ToUpper() + "\\SQLEXPRESS";
                CommonFunctions.DatabaseName = "Kirana0001";
                CommonFunctions.ConStr = "Data Source=" + CommonFunctions.ServerName + ";Initial Catalog=" + CommonFunctions.DatabaseName + ";User ID=OM96;Password=OM96";
                CommonFunctions.Url = @"http://rs.rshopnow.com/ServerAccess.asmx";
                //CommonFunctions.Url = @"http://localhost:49842/ServerAccess.asmx";
            
                ObjFunction.WriteMessage(CommonFunctions.ConStr, LogLevel.Information, null);
                ObjFunction.WriteMessage(CommonFunctions.Url, LogLevel.Information, null);
            }
            catch (Exception ex)
            {
                ObjFunction.WriteMessage(ex.Message.ToString(), LogLevel.Error, ex);
                this.Stop();
            }
        }

        private void Start_Synsc(object sender, EventArgs e)
        {

            SetConnection();
            if (tmrInital.Enabled)
            {
                tmrInital.Enabled = false;
                tmrInital.Interval = 1000 * 60 * 5;// 5 Min

                //Upload Bills
                ObjFunction.WriteMessage("Sale Bill Upload Start", LogLevel.Information, null);
                DBTVaucherEntry db = new DBTVaucherEntry();
                db.SaleBill_Upload();
                tmrInital.Enabled = true;

            }
        }

        protected override void OnStart(string[] args)
        {
            ObjFunction.WriteMessage("Kirana Service Start", LogLevel.Information, null);
          


        }

        protected override void OnStop()
        {
            ObjFunction.WriteMessage("Kirana Service Stop", LogLevel.Information, null);
        }
    }
}
