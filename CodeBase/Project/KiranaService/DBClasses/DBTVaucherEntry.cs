﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Data;
using System.Data.SqlClient;
using JitControls;
using Newtonsoft.Json;


namespace OM
{
    class DBTVaucherEntry
    {
        CommonFunctions ObjFunction = new CommonFunctions();
        Transaction.Transactions ObjTrans = new Transaction.Transactions();
        Transaction.GetDataSet ObjDset = new Transaction.GetDataSet();
        public CommandCollection commandcollection = new CommandCollection();
        Transaction.QueryOutPut ObjQry = new Transaction.QueryOutPut();

        DataTable dtId = new DataTable();

        public static string strerrormsg;

        public bool AddTVoucherEntry(TVoucherEntry tvoucherentry)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTVoucherEntry";

            cmd.Parameters.AddWithValue("@PkVoucherNo", tvoucherentry.PkVoucherNo);

            cmd.Parameters.AddWithValue("@VoucherTypeCode", tvoucherentry.VoucherTypeCode);

            cmd.Parameters.AddWithValue("@VoucherUserNo", tvoucherentry.VoucherUserNo);

            cmd.Parameters.AddWithValue("@VoucherDate", tvoucherentry.VoucherDate);

            cmd.Parameters.AddWithValue("@VoucherTime", tvoucherentry.VoucherTime);

            cmd.Parameters.AddWithValue("@Narration", tvoucherentry.Narration);

            cmd.Parameters.AddWithValue("@Reference", tvoucherentry.Reference);

            cmd.Parameters.AddWithValue("@ChequeNo", tvoucherentry.ChequeNo);

            cmd.Parameters.AddWithValue("@ClearingDate", tvoucherentry.ClearingDate);

            cmd.Parameters.AddWithValue("@CompanyNo", tvoucherentry.CompanyNo);

            cmd.Parameters.AddWithValue("@BilledAmount", tvoucherentry.BilledAmount);

            cmd.Parameters.AddWithValue("@ChallanNo", tvoucherentry.ChallanNo);

            cmd.Parameters.AddWithValue("@Remark", tvoucherentry.Remark);

            cmd.Parameters.AddWithValue("@InwardLocationCode", tvoucherentry.InwardLocationCode);

            cmd.Parameters.AddWithValue("@MacNo", tvoucherentry.MacNo);

            cmd.Parameters.AddWithValue("@PayTypeNo", tvoucherentry.PayTypeNo);

            cmd.Parameters.AddWithValue("@RateTypeNo", tvoucherentry.RateTypeNo);

            cmd.Parameters.AddWithValue("@TaxTypeNo", tvoucherentry.TaxTypeNo);

            cmd.Parameters.AddWithValue("@OrderType", tvoucherentry.OrderType);

            cmd.Parameters.AddWithValue("@ReturnAmount", tvoucherentry.ReturnAmount);

            cmd.Parameters.AddWithValue("@Visibility", tvoucherentry.Visibility);

            cmd.Parameters.AddWithValue("@DiscPercent", tvoucherentry.DiscPercent);

            cmd.Parameters.AddWithValue("@DiscAmt", tvoucherentry.DiscAmt);

            cmd.Parameters.AddWithValue("@MixMode", tvoucherentry.MixMode);

            cmd.Parameters.AddWithValue("@IsItemLevelDisc", tvoucherentry.IsItemLevelDisc);

            cmd.Parameters.AddWithValue("@IsFooterLevelDisc", tvoucherentry.IsFooterLevelDisc);

            cmd.Parameters.AddWithValue("@UserID", tvoucherentry.UserID);

            cmd.Parameters.AddWithValue("@HamaliRs", tvoucherentry.HamaliRs);

            cmd.Parameters.AddWithValue("@UserDate", tvoucherentry.UserDate);

            SqlParameter outParameter = new SqlParameter();
            outParameter.ParameterName = "@ReturnID";
            outParameter.Direction = ParameterDirection.Output;
            outParameter.DbType = DbType.Int32;
            cmd.Parameters.Add(outParameter);

            commandcollection.Add(cmd);
            return true;
        }

        public bool DeleteTVoucherEntry(TVoucherEntry tvoucherentry)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteTVoucherEntry";

            cmd.Parameters.AddWithValue("@PkVoucherNo", tvoucherentry.PkVoucherNo);
            commandcollection.Add(cmd);
            return true;
        }

        public bool DeleteTVoucherEntry1(TVoucherEntry tvoucherentry)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteTVoucherEntry1";

            cmd.Parameters.AddWithValue("@PkVoucherNo", tvoucherentry.PkVoucherNo);
            commandcollection.Add(cmd);
            return true;
        }

        public DataView GetAllTVoucherEntry()
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from TVoucherEntry order by PkVoucherNo";
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public DataView GetTVoucherEntryByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from TVoucherEntry where PkVoucherNo =" + ID;
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public TVoucherEntry ModifyTVoucherEntryByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql;
            SqlCommand cmd;
            sql = "Select * from TVoucherEntry where PkVoucherNo =" + ID;
            cmd = new SqlCommand(sql, Con);
            cmd.Connection = Con;
            Con.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (dr.HasRows)
            {
                TVoucherEntry MM = new TVoucherEntry();
                while (dr.Read())
                {
                    MM.PkVoucherNo = Convert.ToInt32(dr["PkVoucherNo"]);
                    if (!Convert.IsDBNull(dr["VoucherTypeCode"])) MM.VoucherTypeCode = Convert.ToInt64(dr["VoucherTypeCode"]);
                    if (!Convert.IsDBNull(dr["VoucherUserNo"])) MM.VoucherUserNo = Convert.ToInt64(dr["VoucherUserNo"]);
                    if (!Convert.IsDBNull(dr["VoucherDate"])) MM.VoucherDate = Convert.ToDateTime(dr["VoucherDate"]);
                    if (!Convert.IsDBNull(dr["VoucherTime"])) MM.VoucherTime = Convert.ToDateTime(dr["VoucherTime"]);
                    if (!Convert.IsDBNull(dr["Narration"])) MM.Narration = Convert.ToString(dr["Narration"]);
                    if (!Convert.IsDBNull(dr["Reference"])) MM.Reference = Convert.ToString(dr["Reference"]);
                    if (!Convert.IsDBNull(dr["ChequeNo"])) MM.ChequeNo = Convert.ToInt64(dr["ChequeNo"]);
                    if (!Convert.IsDBNull(dr["ClearingDate"])) MM.ClearingDate = Convert.ToDateTime(dr["ClearingDate"]);
                    if (!Convert.IsDBNull(dr["CompanyNo"])) MM.CompanyNo = Convert.ToInt64(dr["CompanyNo"]);
                    if (!Convert.IsDBNull(dr["BilledAmount"])) MM.BilledAmount = Convert.ToDouble(dr["BilledAmount"]);
                    if (!Convert.IsDBNull(dr["ChallanNo"])) MM.ChallanNo = Convert.ToString(dr["ChallanNo"]);
                    if (!Convert.IsDBNull(dr["Remark"])) MM.Remark = Convert.ToString(dr["Remark"]);
                    if (!Convert.IsDBNull(dr["InwardLocationCode"])) MM.InwardLocationCode = Convert.ToInt64(dr["InwardLocationCode"]);
                    if (!Convert.IsDBNull(dr["MacNo"])) MM.MacNo = Convert.ToInt64(dr["MacNo"]);
                    if (!Convert.IsDBNull(dr["PayTypeNo"])) MM.PayTypeNo = Convert.ToInt64(dr["PayTypeNo"]);
                    if (!Convert.IsDBNull(dr["RateTypeNo"])) MM.RateTypeNo = Convert.ToInt64(dr["RateTypeNo"]);
                    if (!Convert.IsDBNull(dr["TaxTypeNo"])) MM.TaxTypeNo = Convert.ToInt64(dr["TaxTypeNo"]);
                    if (!Convert.IsDBNull(dr["IsVoucherLock"])) MM.IsVoucherLock = Convert.ToBoolean(dr["IsVoucherLock"]);
                    if (!Convert.IsDBNull(dr["IsCancel"])) MM.IsCancel = Convert.ToBoolean(dr["IsCancel"]);
                    if (!Convert.IsDBNull(dr["OrderType"])) MM.OrderType = Convert.ToInt64(dr["OrderType"]);
                    if (!Convert.IsDBNull(dr["ReturnAmount"])) MM.ReturnAmount = Convert.ToDouble(dr["ReturnAmount"]);
                    if (!Convert.IsDBNull(dr["Visibility"])) MM.Visibility = Convert.ToDouble(dr["Visibility"]);
                    if (!Convert.IsDBNull(dr["DiscPercent"])) MM.DiscPercent = Convert.ToDouble(dr["DiscPercent"]);
                    if (!Convert.IsDBNull(dr["DiscAmt"])) MM.DiscAmt = Convert.ToDouble(dr["DiscAmt"]);
                    if (!Convert.IsDBNull(dr["UserID"])) MM.UserID = Convert.ToInt64(dr["UserID"]);
                    if (!Convert.IsDBNull(dr["UserDate"])) MM.UserDate = Convert.ToDateTime(dr["UserDate"]);
                    if (!Convert.IsDBNull(dr["MixMode"])) MM.MixMode = Convert.ToInt32(dr["MixMode"]);
                    if (!Convert.IsDBNull(dr["IsItemLevelDisc"])) MM.IsItemLevelDisc = Convert.ToBoolean(dr["IsItemLevelDisc"]);
                    if (!Convert.IsDBNull(dr["IsFooterLevelDisc"])) MM.IsFooterLevelDisc = Convert.ToBoolean(dr["IsFooterLevelDisc"]);
                    if (!Convert.IsDBNull(dr["ModifiedBy"])) MM.ModifiedBy = Convert.ToString(dr["ModifiedBy"]);
                    if (!Convert.IsDBNull(dr["HamaliRs"])) MM.HamaliRs = Convert.ToDouble(dr["HamaliRs"]);
                }
                dr.Close();
                return MM;
            }
            else
                dr.Close();
            return new TVoucherEntry();
        }

        public DataView GetBySearch(string Column, string Value, long VchCode)
        {

            string sql = " SELECT TVoucherEntry.PkVoucherNo,TVoucherEntry.VoucherUserNo, MLedger.LedgerName, Sum(Case When (TVoucherDetails.Debit>0) then  TVoucherDetails.Debit else TVoucherDetails.Credit " +
                         " END) AS 'Amount' FROM MLedger INNER JOIN TVoucherDetails ON MLedger.LedgerNo = TVoucherDetails.LedgerNo CROSS JOIN TVoucherEntry Where SrNo=" + Others.Party + "" +
                         " And TVoucherEntry.PkVoucherNo=TVoucherDetails.FkVoucherNo and TVoucherEntry.VoucherTypeCode=" + VchCode + " GROUP BY TVoucherEntry.PkVoucherNo, TVoucherEntry.VoucherUserNo, MLedger.LedgerName order by TVoucherEntry.PkVoucherNo";
            switch (Column)
            {
                case "0":
                    sql = " SELECT TVoucherEntry.PkVoucherNo,TVoucherEntry.VoucherUserNo, MLedger.LedgerName, Sum(Case When (TVoucherDetails.Debit>0) then  TVoucherDetails.Debit else TVoucherDetails.Credit " +
                         " END) AS 'Amount' FROM MLedger INNER JOIN TVoucherDetails ON MLedger.LedgerNo = TVoucherDetails.LedgerNo CROSS JOIN TVoucherEntry Where SrNo=" + Others.Party + "" +
                         " And TVoucherEntry.PkVoucherNo=TVoucherDetails.FkVoucherNo and TVoucherEntry.VoucherTypeCode=" + VchCode + " GROUP BY TVoucherEntry.PkVoucherNo, TVoucherEntry.VoucherUserNo, MLedger.LedgerName order by TVoucherEntry.PkVoucherNo";
                    break;
                case "LedgerName":
                    sql = " SELECT TVoucherEntry.PkVoucherNo,TVoucherEntry.VoucherUserNo, MLedger.LedgerName, Sum(Case When (TVoucherDetails.Debit>0) then  TVoucherDetails.Debit else TVoucherDetails.Credit " +
                         " END) AS 'Amount' FROM MLedger INNER JOIN TVoucherDetails ON MLedger.LedgerNo = TVoucherDetails.LedgerNo CROSS JOIN TVoucherEntry Where SrNo=" + Others.Party + "" +
                         " And TVoucherEntry.PkVoucherNo=TVoucherDetails.FkVoucherNo and TVoucherEntry.VoucherTypeCode=" + VchCode + " and " + Column + " like '" + Value.Trim().Replace("'", "''") + "' + '%' GROUP BY TVoucherEntry.PkVoucherNo, TVoucherEntry.VoucherUserNo, MLedger.LedgerName order by TVoucherEntry.PkVoucherNo";
                    break;

            }
            DataSet ds = new DataSet();
            try
            {
                ds = ObjDset.FillDset("New", sql, CommonFunctions.ConStr);
            }
            catch (SqlException e)
            {
                strerrormsg = e.Message;
            }
            return ds.Tables[0].DefaultView;
        }

        public DataView GetBySearchVoucher(string Column, string Value, long VchCode)
        {

            string sql = " SELECT TVoucherEntry.PkVoucherNo,TVoucherEntry.VoucherUserNo, MLedger.LedgerName, Sum(Case When (TVoucherDetails.Debit>0) then  TVoucherDetails.Debit else TVoucherDetails.Credit " +
                         " END) AS 'Amount' FROM MLedger INNER JOIN TVoucherDetails ON MLedger.LedgerNo = TVoucherDetails.LedgerNo CROSS JOIN TVoucherEntry Where SrNo=" + Others.Party + "" +
                         " And TVoucherEntry.PkVoucherNo=TVoucherDetails.FkVoucherNo and TVoucherEntry.VoucherTypeCode=" + VchCode + " and TVoucherDetails.VoucherSrNo=1 and TVoucherEntry.IsVoucherLock='false' GROUP BY TVoucherEntry.PkVoucherNo, TVoucherEntry.VoucherUserNo, MLedger.LedgerName order by TVoucherEntry.PkVoucherNo";
            switch (Column)
            {
                case "0":
                    sql = " SELECT TVoucherEntry.PkVoucherNo,TVoucherEntry.VoucherUserNo, MLedger.LedgerName, Sum(Case When (TVoucherDetails.Debit>0) then  TVoucherDetails.Debit else TVoucherDetails.Credit " +
                         " END) AS 'Amount' FROM MLedger INNER JOIN TVoucherDetails ON MLedger.LedgerNo = TVoucherDetails.LedgerNo CROSS JOIN TVoucherEntry Where SrNo=" + Others.Party + "" +
                         " And TVoucherEntry.PkVoucherNo=TVoucherDetails.FkVoucherNo and TVoucherEntry.IsVoucherLock='false' and TVoucherEntry.VoucherTypeCode=" + VchCode + " and TVoucherDetails.VoucherSrNo=1 GROUP BY TVoucherEntry.PkVoucherNo, TVoucherEntry.VoucherUserNo, MLedger.LedgerName order by TVoucherEntry.PkVoucherNo";
                    break;
                case "LedgerName":
                    sql = " SELECT TVoucherEntry.PkVoucherNo,TVoucherEntry.VoucherUserNo, MLedger.LedgerName, Sum(Case When (TVoucherDetails.Debit>0) then  TVoucherDetails.Debit else TVoucherDetails.Credit " +
                         " END) AS 'Amount' FROM MLedger INNER JOIN TVoucherDetails ON MLedger.LedgerNo = TVoucherDetails.LedgerNo CROSS JOIN TVoucherEntry Where SrNo=" + Others.Party + "" +
                         " And TVoucherEntry.PkVoucherNo=TVoucherDetails.FkVoucherNo and TVoucherEntry.IsVoucherLock='false' and TVoucherDetails.VoucherSrNo=1 and TVoucherEntry.VoucherTypeCode=" + VchCode + " and " + Column + " like '" + Value.Trim().Replace("'", "''") + "' + '%' GROUP BY TVoucherEntry.PkVoucherNo, TVoucherEntry.VoucherUserNo, MLedger.LedgerName order by TVoucherEntry.PkVoucherNo";
                    break;

            }
            DataSet ds = new DataSet();
            try
            {
                ds = ObjDset.FillDset("New", sql, CommonFunctions.ConStr);
            }
            catch (SqlException e)
            {
                strerrormsg = e.Message;
            }
            return ds.Tables[0].DefaultView;
        }

        public bool UpdateTVoucherEntry(TVoucherEntry tvoucherentry)
        {
            string strQuery = "Update TVoucherEntry set OrderType=" + tvoucherentry.OrderType + " where PkVoucherNo=" + tvoucherentry.PkVoucherNo + "";

            if (ObjTrans.Execute(strQuery, CommonFunctions.ConStr) == true)
            {
                return true;
            }
            else
            {
                tvoucherentry.msg = ObjTrans.ErrorMessage;
                return false;
            }
        }

        public bool AddTVoucherDetails(TVoucherDetails tvoucherdetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTVoucherDetails";

            cmd.Parameters.AddWithValue("@PkVoucherTrnNo", tvoucherdetails.PkVoucherTrnNo);

            //cmd.Parameters.AddWithValue("@FkVoucherNo", tvoucherdetails.FkVoucherNo);

            cmd.Parameters.AddWithValue("@VoucherSrNo", tvoucherdetails.VoucherSrNo);

            cmd.Parameters.AddWithValue("@SignCode", tvoucherdetails.SignCode);

            cmd.Parameters.AddWithValue("@LedgerNo", tvoucherdetails.LedgerNo);

            cmd.Parameters.AddWithValue("@Debit", tvoucherdetails.Debit);

            cmd.Parameters.AddWithValue("@Credit", tvoucherdetails.Credit);

            cmd.Parameters.AddWithValue("@SrNo", tvoucherdetails.SrNo);

            cmd.Parameters.AddWithValue("@CompanyNo", tvoucherdetails.CompanyNo);

            cmd.Parameters.AddWithValue("@Narration", tvoucherdetails.Narration);

            SqlParameter outParameter = new SqlParameter();
            outParameter.ParameterName = "@ReturnID";
            outParameter.Direction = ParameterDirection.Output;
            outParameter.DbType = DbType.Int32;
            cmd.Parameters.Add(outParameter);

            commandcollection.Add(cmd);
            return true;
        }

        public bool DeleteTVoucherDetails(TVoucherDetails tvoucherdetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteTVoucherDetails";

            cmd.Parameters.AddWithValue("@PkVoucherTrnNo", tvoucherdetails.PkVoucherTrnNo);
            commandcollection.Add(cmd);
            return true;
        }

        public bool DeleteTVoucherDetailsCompany(TVoucherDetails tvoucherdetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteTVoucherDetailsCompany";

            cmd.Parameters.AddWithValue("@FkVoucherNo", tvoucherdetails.FkVoucherNo);

            cmd.Parameters.AddWithValue("@CompanyNo", tvoucherdetails.CompanyNo);
            commandcollection.Add(cmd);
            return true;
        }

        public DataView GetAllTVoucherDetails()
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from TVoucherDetails order by PkVoucherTrnNo";
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public DataView GetTVoucherDetailsByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select TVoucherDetails.SignCode,TVoucherDetails.LedgerNo,TVoucherDetails.Debit,TVoucherDetails.Credit,TVoucherDetails.PkVoucherTrnNo,TVoucherDetails.CompanyNo,MCompany.CompanyName from TVoucherDetails ,MCompany Where TVoucherDetails.CompanyNo=MCompany.CompanyNo AND FkVoucherNo =" + ID;
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public DataView GetTVoucherDetailsVoucherByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = " SELECT   0 as SrNo,  MSign.ShortName, MLedger.LedgerName, TVoucherDetails.Debit, TVoucherDetails.Credit, TVoucherDetails.PkVoucherTrnNo,  " +
                         "  MSign.SignCode, MLedger.LedgerNo, TChequeNoDetails.PkSrNo,TVoucherDetails.Narration , TVoucherChqCreditDetails.ChequeNo, " +
                         " TVoucherChqCreditDetails.ChequeDate, TVoucherChqCreditDetails.BankNo, TVoucherChqCreditDetails.BranchNo, " +
                         " TVoucherChqCreditDetails.PkSrNo FROM  MSign INNER JOIN " +
                         " TVoucherDetails ON MSign.SignCode = TVoucherDetails.SignCode INNER JOIN " +
                         " TVoucherEntry ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo INNER JOIN " +
                         " MLedger ON TVoucherDetails.LedgerNo = MLedger.LedgerNo LEFT OUTER JOIN " +
                         " TChequeNoDetails ON TVoucherDetails.PkVoucherTrnNo = TChequeNoDetails.FkVoucherTrnNo LEFT OUTER JOIN " +
                         " TVoucherChqCreditDetails ON TVoucherDetails.PkVoucherTrnNo = TVoucherChqCreditDetails.FkVoucherTrnNo " +
                         " where TVoucherEntry.IsVoucherLock='false' and TVoucherDetails.FkVoucherNo =" + ID;
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }


        public TVoucherDetails ModifyTVoucherDetailsByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql;
            SqlCommand cmd;
            sql = "Select * from TVoucherDetails where PkVoucherTrnNo =" + ID;
            cmd = new SqlCommand(sql, Con);
            cmd.Connection = Con;
            Con.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (dr.HasRows)
            {
                TVoucherDetails MM = new TVoucherDetails();
                while (dr.Read())
                {
                    MM.PkVoucherTrnNo = Convert.ToInt32(dr["PkVoucherTrnNo"]);
                    if (!Convert.IsDBNull(dr["FkVoucherNo"])) MM.FkVoucherNo = Convert.ToInt64(dr["FkVoucherNo"]);
                    if (!Convert.IsDBNull(dr["VoucherSrNo"])) MM.VoucherSrNo = Convert.ToInt64(dr["VoucherSrNo"]);
                    if (!Convert.IsDBNull(dr["SignCode"])) MM.SignCode = Convert.ToInt64(dr["SignCode"]);
                    if (!Convert.IsDBNull(dr["LedgerNo"])) MM.LedgerNo = Convert.ToInt64(dr["LedgerNo"]);
                    if (!Convert.IsDBNull(dr["Debit"])) MM.Debit = Convert.ToDouble(dr["Debit"]);
                    if (!Convert.IsDBNull(dr["Credit"])) MM.Credit = Convert.ToDouble(dr["Credit"]);
                    if (!Convert.IsDBNull(dr["SrNo"])) MM.SrNo = Convert.ToInt64(dr["SrNo"]);
                    if (!Convert.IsDBNull(dr["CompanyNo"])) MM.CompanyNo = Convert.ToInt64(dr["CompanyNo"]);
                    if (!Convert.IsDBNull(dr["Narration"])) MM.Narration = Convert.ToString(dr["Narration"]);
                }
                dr.Close();
                return MM;
            }
            else
                dr.Close();
            return new TVoucherDetails();
        }


        public bool AddTStock(TStock tstock)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTStock";

            cmd.Parameters.AddWithValue("@PkStockTrnNo", tstock.PkStockTrnNo);

            //cmd.Parameters.AddWithValue("@FKVoucherNo", tstock.FKVoucherNo);

            //cmd.Parameters.AddWithValue("@FkVoucherTrnNo", tstock.FkVoucherTrnNo);

            cmd.Parameters.AddWithValue("@FkVoucherSrNo", tstock.FkVoucherSrNo);

            cmd.Parameters.AddWithValue("@GroupNo", tstock.GroupNo);

            cmd.Parameters.AddWithValue("@ItemNo", tstock.ItemNo);

            cmd.Parameters.AddWithValue("@TrnCode", tstock.TrnCode);

            cmd.Parameters.AddWithValue("@Quantity", tstock.Quantity);

            cmd.Parameters.AddWithValue("@BilledQuantity", tstock.BilledQuantity);

            cmd.Parameters.AddWithValue("@Rate", tstock.Rate);

            cmd.Parameters.AddWithValue("@Amount", tstock.Amount);

            cmd.Parameters.AddWithValue("@NetRate", tstock.NetRate);

            cmd.Parameters.AddWithValue("@NetAmount", tstock.NetAmount);

            cmd.Parameters.AddWithValue("@TaxPercentage", tstock.TaxPercentage);

            cmd.Parameters.AddWithValue("@TaxAmount", tstock.TaxAmount);

            cmd.Parameters.AddWithValue("@DiscPercentage", tstock.DiscPercentage);

            cmd.Parameters.AddWithValue("@DiscAmount", tstock.DiscAmount);

            cmd.Parameters.AddWithValue("@DiscRupees", tstock.DiscRupees);

            cmd.Parameters.AddWithValue("@DiscPercentage2", tstock.DiscPercentage2);

            cmd.Parameters.AddWithValue("@DiscAmount2", tstock.DiscAmount2);

            cmd.Parameters.AddWithValue("@DiscRupees2", tstock.DiscRupees2);

            cmd.Parameters.AddWithValue("@FkUomNo", tstock.FkUomNo);

            cmd.Parameters.AddWithValue("@FkStockBarCodeNo", tstock.FkStockBarCodeNo);

            cmd.Parameters.AddWithValue("@FkRateSettingNo", tstock.FkRateSettingNo);

            cmd.Parameters.AddWithValue("@FkItemTaxInfo", tstock.FkItemTaxInfo);

            cmd.Parameters.AddWithValue("@FreeQty", tstock.FreeQty);

            cmd.Parameters.AddWithValue("@FreeUOMNo", tstock.FreeUOMNo);

            cmd.Parameters.AddWithValue("@UserID", tstock.UserID);

            cmd.Parameters.AddWithValue("@UserDate", tstock.UserDate);

            cmd.Parameters.AddWithValue("@CompanyNo", tstock.CompanyNo);

            cmd.Parameters.AddWithValue("@LandedRate", tstock.LandedRate);

            cmd.Parameters.AddWithValue("@DiscountType", tstock.DiscountType);

            cmd.Parameters.AddWithValue("@HamaliInKg", tstock.HamaliInKg);

            SqlParameter outParameter = new SqlParameter();
            outParameter.ParameterName = "@ReturnID";
            outParameter.Direction = ParameterDirection.Output;
            outParameter.DbType = DbType.Int32;
            cmd.Parameters.Add(outParameter);

            commandcollection.Add(cmd);
            return true;
        }

        public bool DeleteTStock(TStock tstock)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteTStock";

            cmd.Parameters.AddWithValue("@PkStockTrnNo", tstock.PkStockTrnNo);

            commandcollection.Add(cmd);
            return true;
        }

        public bool DeleteTReward_All(long FkVoucherNo)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteTReward_All";

            cmd.Parameters.AddWithValue("@FkVoucherNo", FkVoucherNo);

            commandcollection.Add(cmd);
            return true;
        }

        public DataView GetAllTStock()
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from TStock order by PkStockTrnNo";
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public DataView GetTStockByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from TStock where PkStockTrnNo =" + ID;
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public TStock ModifyTStockByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql;
            SqlCommand cmd;
            sql = "Select * from TStock where PkStockTrnNo =" + ID;
            cmd = new SqlCommand(sql, Con);
            cmd.Connection = Con;
            Con.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (dr.HasRows)
            {
                TStock MM = new TStock();
                while (dr.Read())
                {
                    MM.PkStockTrnNo = Convert.ToInt32(dr["PkStockTrnNo"]);
                    if (!Convert.IsDBNull(dr["FkVoucherTrnNo"])) MM.FkVoucherTrnNo = Convert.ToInt64(dr["FkVoucherTrnNo"]);
                    if (!Convert.IsDBNull(dr["FkVoucherSrNo"])) MM.FkVoucherSrNo = Convert.ToInt64(dr["FkVoucherSrNo"]);
                    if (!Convert.IsDBNull(dr["GroupNo"])) MM.GroupNo = Convert.ToInt64(dr["GroupNo"]);
                    if (!Convert.IsDBNull(dr["ItemNo"])) MM.ItemNo = Convert.ToInt64(dr["ItemNo"]);
                    if (!Convert.IsDBNull(dr["TrnCode"])) MM.TrnCode = Convert.ToInt64(dr["TrnCode"]);
                    if (!Convert.IsDBNull(dr["Quantity"])) MM.Quantity = Convert.ToDouble(dr["Quantity"]);
                    if (!Convert.IsDBNull(dr["BilledQuantity"])) MM.BilledQuantity = Convert.ToDouble(dr["BilledQuantity"]);
                    if (!Convert.IsDBNull(dr["Rate"])) MM.Rate = Convert.ToDouble(dr["Rate"]);
                    if (!Convert.IsDBNull(dr["Amount"])) MM.Amount = Convert.ToDouble(dr["Amount"]);
                    if (!Convert.IsDBNull(dr["NetRate"])) MM.NetRate = Convert.ToDouble(dr["NetRate"]);
                    if (!Convert.IsDBNull(dr["NetAmount"])) MM.NetAmount = Convert.ToDouble(dr["NetAmount"]);
                    if (!Convert.IsDBNull(dr["TaxPercentage"])) MM.TaxPercentage = Convert.ToDouble(dr["TaxPercentage"]);
                    if (!Convert.IsDBNull(dr["TaxAmount"])) MM.TaxAmount = Convert.ToDouble(dr["TaxAmount"]);
                    if (!Convert.IsDBNull(dr["DiscPercentage"])) MM.DiscPercentage = Convert.ToDouble(dr["DiscPercentage"]);
                    if (!Convert.IsDBNull(dr["DiscAmount"])) MM.DiscAmount = Convert.ToDouble(dr["DiscAmount"]);
                    if (!Convert.IsDBNull(dr["DiscRupees"])) MM.DiscRupees = Convert.ToDouble(dr["DiscRupees"]);
                    if (!Convert.IsDBNull(dr["DiscPercentage2"])) MM.DiscPercentage2 = Convert.ToDouble(dr["DiscPercentage2"]);
                    if (!Convert.IsDBNull(dr["DiscAmount2"])) MM.DiscAmount2 = Convert.ToDouble(dr["DiscAmount2"]);
                    if (!Convert.IsDBNull(dr["DiscRupees2"])) MM.DiscRupees2 = Convert.ToDouble(dr["DiscRupees2"]);
                    if (!Convert.IsDBNull(dr["FkUomNo"])) MM.FkUomNo = Convert.ToInt64(dr["FkUomNo"]);
                    if (!Convert.IsDBNull(dr["FkStockBarCodeNo"])) MM.FkStockBarCodeNo = Convert.ToInt64(dr["FkStockBarCodeNo"]);
                    if (!Convert.IsDBNull(dr["FkRateSettingNo"])) MM.FkRateSettingNo = Convert.ToInt64(dr["FkRateSettingNo"]);
                    if (!Convert.IsDBNull(dr["FkItemTaxInfo"])) MM.FkItemTaxInfo = Convert.ToInt64(dr["FkItemTaxInfo"]);
                    if (!Convert.IsDBNull(dr["FreeQty"])) MM.FreeQty = Convert.ToDouble(dr["FreeQty"]);
                    if (!Convert.IsDBNull(dr["FreeUOMNo"])) MM.FreeUOMNo = Convert.ToInt64(dr["FreeUOMNo"]);
                    if (!Convert.IsDBNull(dr["UserID"])) MM.UserID = Convert.ToInt64(dr["UserID"]);
                    if (!Convert.IsDBNull(dr["UserDate"])) MM.UserDate = Convert.ToDateTime(dr["UserDate"]);
                    if (!Convert.IsDBNull(dr["CompanyNo"])) MM.CompanyNo = Convert.ToInt64(dr["CompanyNo"]);
                    if (!Convert.IsDBNull(dr["LandedRate"])) MM.LandedRate = Convert.ToDouble(dr["LandedRate"]);
                    if (!Convert.IsDBNull(dr["ModifiedBy"])) MM.ModifiedBy = Convert.ToString(dr["ModifiedBy"]);
                    if (!Convert.IsDBNull(dr["DiscountType"])) MM.DiscountType = Convert.ToInt64(dr["DiscountType"]);
                    if (!Convert.IsDBNull(dr["HamaliInKg"])) MM.HamaliInKg = Convert.ToDouble(dr["HamaliInKg"]);

                }
                dr.Close();
                return MM;
            }
            else
                dr.Close();
            return new TStock();
        }


        public bool AddTStockGodown(TStockGodown tstockgodown)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTStockGodown";

            cmd.Parameters.AddWithValue("@PKStockGodownNo", tstockgodown.PKStockGodownNo);

            //cmd.Parameters.AddWithValue("@FKStockTrnNo", tstockgodown.FKStockTrnNo);

            cmd.Parameters.AddWithValue("@ItemNo", tstockgodown.ItemNo);

            cmd.Parameters.AddWithValue("@GodownNo", tstockgodown.GodownNo);

            cmd.Parameters.AddWithValue("@Qty", tstockgodown.Qty);

            cmd.Parameters.AddWithValue("@ActualQty", tstockgodown.ActualQty);

            cmd.Parameters.AddWithValue("@UserID", tstockgodown.UserID);

            cmd.Parameters.AddWithValue("@UserDate", tstockgodown.UserDate);

            cmd.Parameters.AddWithValue("@CompanyNo", tstockgodown.CompanyNo);

            commandcollection.Add(cmd);
            return true;
        }

        public bool DeleteTStockGodown(TStockGodown tstockgodown)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteTStockGodown";

            cmd.Parameters.AddWithValue("@PKStockGodownNo", tstockgodown.PKStockGodownNo);
            commandcollection.Add(cmd);
            return true;
        }

        public DataView GetAllTStockGodown()
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from TStockGodown order by PKStockGodownNo";
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public DataView GetTStockGodownByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from TStockGodown where PKStockGodownNo =" + ID;
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public TStockGodown ModifyTStockGodownByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql;
            SqlCommand cmd;
            sql = "Select * from TStockGodown where PKStockGodownNo =" + ID;
            cmd = new SqlCommand(sql, Con);
            cmd.Connection = Con;
            Con.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (dr.HasRows)
            {
                TStockGodown MM = new TStockGodown();
                while (dr.Read())
                {
                    MM.PKStockGodownNo = Convert.ToInt32(dr["PKStockGodownNo"]);
                    if (!Convert.IsDBNull(dr["FKStockTrnNo"])) MM.FKStockTrnNo = Convert.ToInt64(dr["FKStockTrnNo"]);
                    if (!Convert.IsDBNull(dr["ItemNo"])) MM.ItemNo = Convert.ToInt64(dr["ItemNo"]);
                    if (!Convert.IsDBNull(dr["GodownNo"])) MM.GodownNo = Convert.ToInt64(dr["GodownNo"]);
                    if (!Convert.IsDBNull(dr["Qty"])) MM.Qty = Convert.ToDouble(dr["Qty"]);
                    if (!Convert.IsDBNull(dr["ActualQty"])) MM.ActualQty = Convert.ToDouble(dr["ActualQty"]);
                    if (!Convert.IsDBNull(dr["UserID"])) MM.UserID = Convert.ToInt64(dr["UserID"]);
                    if (!Convert.IsDBNull(dr["UserDate"])) MM.UserDate = Convert.ToDateTime(dr["UserDate"]);
                    if (!Convert.IsDBNull(dr["CompanyNo"])) MM.CompanyNo = Convert.ToInt64(dr["CompanyNo"]);
                    if (!Convert.IsDBNull(dr["ModifiedBy"])) MM.ModifiedBy = Convert.ToString(dr["ModifiedBy"]);
                }
                dr.Close();
                return MM;
            }
            else
                dr.Close();
            return new TStockGodown();
        }


        public bool AddTVoucherRefDetails(TVoucherRefDetails tvoucherrefdetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTVoucherRefDetails";

            cmd.Parameters.AddWithValue("@PkRefTrnNo", tvoucherrefdetails.PkRefTrnNo);

            // cmd.Parameters.AddWithValue("@FkVoucherTrnNo", tvoucherrefdetails.FkVoucherTrnNo);

            cmd.Parameters.AddWithValue("@FkVoucherSrNo", tvoucherrefdetails.FkVoucherSrNo);

            cmd.Parameters.AddWithValue("@LedgerNo", tvoucherrefdetails.LedgerNo);

            cmd.Parameters.AddWithValue("@RefNo", tvoucherrefdetails.RefNo);

            cmd.Parameters.AddWithValue("@TypeOfRef", tvoucherrefdetails.TypeOfRef);

            cmd.Parameters.AddWithValue("@DueDays", tvoucherrefdetails.DueDays);

            cmd.Parameters.AddWithValue("@DueDate", tvoucherrefdetails.DueDate);

            cmd.Parameters.AddWithValue("@Amount", tvoucherrefdetails.Amount);

            cmd.Parameters.AddWithValue("@SignCode", tvoucherrefdetails.SignCode);

            cmd.Parameters.AddWithValue("@UserID", tvoucherrefdetails.UserID);

            cmd.Parameters.AddWithValue("@UserDate", tvoucherrefdetails.UserDate);

            cmd.Parameters.AddWithValue("@CompanyNo", tvoucherrefdetails.CompanyNo);

            commandcollection.Add(cmd);
            return true;
        }

        public bool AddTVoucherRefDetails1(TVoucherRefDetails tvoucherrefdetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTVoucherRefDetails1";

            cmd.Parameters.AddWithValue("@PkRefTrnNo", tvoucherrefdetails.PkRefTrnNo);

            cmd.Parameters.AddWithValue("@FkVoucherTrnNo", tvoucherrefdetails.FkVoucherTrnNo);

            cmd.Parameters.AddWithValue("@FkVoucherSrNo", tvoucherrefdetails.FkVoucherSrNo);

            cmd.Parameters.AddWithValue("@LedgerNo", tvoucherrefdetails.LedgerNo);

            cmd.Parameters.AddWithValue("@RefNo", tvoucherrefdetails.RefNo);

            cmd.Parameters.AddWithValue("@TypeOfRef", tvoucherrefdetails.TypeOfRef);

            cmd.Parameters.AddWithValue("@DueDays", tvoucherrefdetails.DueDays);

            cmd.Parameters.AddWithValue("@DueDate", tvoucherrefdetails.DueDate);

            cmd.Parameters.AddWithValue("@Amount", tvoucherrefdetails.Amount);

            cmd.Parameters.AddWithValue("@SignCode", tvoucherrefdetails.SignCode);

            cmd.Parameters.AddWithValue("@UserID", tvoucherrefdetails.UserID);

            cmd.Parameters.AddWithValue("@UserDate", tvoucherrefdetails.UserDate);

            cmd.Parameters.AddWithValue("@CompanyNo", tvoucherrefdetails.CompanyNo);

            commandcollection.Add(cmd);
            return true;
        }

        public bool DeleteTVoucherRefDetails(TVoucherRefDetails tvoucherrefdetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteTVoucherRefDetails";

            cmd.Parameters.AddWithValue("@PkRefTrnNo", tvoucherrefdetails.PkRefTrnNo);
            commandcollection.Add(cmd);
            return true;
        }

        public DataView GetAllTVoucherRefDetails()
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from TVoucherRefDetails order by PkRefTrnNo";
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public DataView GetTVoucherRefDetailsByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "SELECT     MTypeOfRef.TypeOfRef, TVoucherRefDetails.RefNo, TVoucherRefDetails.Amount,0 AftAmt, MTypeOfRef.RefTypeCode, TVoucherRefDetails.PkRefTrnNo FROM TVoucherRefDetails INNER JOIN  " +
                " MTypeOfRef ON TVoucherRefDetails.TypeOfRef = MTypeOfRef.RefTypeCode where TVoucherRefDetails.FKVoucherTrnNo =" + ID;
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public TVoucherRefDetails ModifyTVoucherRefDetailsByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql;
            SqlCommand cmd;
            sql = "Select * from TVoucherRefDetails where PkRefTrnNo =" + ID;
            cmd = new SqlCommand(sql, Con);
            cmd.Connection = Con;
            Con.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (dr.HasRows)
            {
                TVoucherRefDetails MM = new TVoucherRefDetails();
                while (dr.Read())
                {
                    MM.PkRefTrnNo = Convert.ToInt32(dr["PkRefTrnNo"]);
                    if (!Convert.IsDBNull(dr["FkVoucherTrnNo"])) MM.FkVoucherTrnNo = Convert.ToInt64(dr["FkVoucherTrnNo"]);
                    if (!Convert.IsDBNull(dr["FkVoucherSrNo"])) MM.FkVoucherSrNo = Convert.ToInt64(dr["FkVoucherSrNo"]);
                    if (!Convert.IsDBNull(dr["LedgerNo"])) MM.LedgerNo = Convert.ToInt64(dr["LedgerNo"]);
                    if (!Convert.IsDBNull(dr["RefNo"])) MM.RefNo = Convert.ToInt64(dr["RefNo"]);
                    if (!Convert.IsDBNull(dr["TypeOfRef"])) MM.TypeOfRef = Convert.ToInt32(dr["TypeOfRef"]);
                    if (!Convert.IsDBNull(dr["DueDays"])) MM.DueDays = Convert.ToInt64(dr["DueDays"]);
                    if (!Convert.IsDBNull(dr["DueDate"])) MM.DueDate = Convert.ToDateTime(dr["DueDate"]);
                    if (!Convert.IsDBNull(dr["Amount"])) MM.Amount = Convert.ToDouble(dr["Amount"]);
                    if (!Convert.IsDBNull(dr["SignCode"])) MM.SignCode = Convert.ToInt64(dr["SignCode"]);
                    if (!Convert.IsDBNull(dr["UserID"])) MM.UserID = Convert.ToInt64(dr["UserID"]);
                    if (!Convert.IsDBNull(dr["UserDate"])) MM.UserDate = Convert.ToDateTime(dr["UserDate"]);
                    if (!Convert.IsDBNull(dr["CompanyNo"])) MM.CompanyNo = Convert.ToInt64(dr["CompanyNo"]);
                    if (!Convert.IsDBNull(dr["Modifiedby"])) MM.Modifiedby = Convert.ToString(dr["Modifiedby"]);
                }
                dr.Close();
                return MM;
            }
            else
                dr.Close();
            return new TVoucherRefDetails();
        }


        public bool AddTVoucherPayTypeDetails(TVoucherPayTypeDetails tvoucherpaytypedetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTVoucherPayTypeDetails";

            cmd.Parameters.AddWithValue("@PKVoucherPayTypeNo", tvoucherpaytypedetails.PKVoucherPayTypeNo);

            cmd.Parameters.AddWithValue("@FKSalesVoucherNo", tvoucherpaytypedetails.FKSalesVoucherNo);

            //cmd.Parameters.AddWithValue("@FKReceiptVoucherNo", tvoucherpaytypedetails.FKReceiptVoucherNo);

            //cmd.Parameters.AddWithValue("@FKVoucherTrnNo", tvoucherpaytypedetails.FKVoucherTrnNo);

            cmd.Parameters.AddWithValue("@FKPayTypeNo", tvoucherpaytypedetails.FKPayTypeNo);

            cmd.Parameters.AddWithValue("@Amount", tvoucherpaytypedetails.Amount);

            cmd.Parameters.AddWithValue("@CompanyNo", tvoucherpaytypedetails.CompanyNo);

            commandcollection.Add(cmd);
            return true;
        }

        public bool DeleteTVoucherPayTypeDetails(TVoucherPayTypeDetails tvoucherpaytypedetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteTVoucherPayTypeDetails";

            cmd.Parameters.AddWithValue("@PKVoucherPayTypeNo", tvoucherpaytypedetails.PKVoucherPayTypeNo);

            commandcollection.Add(cmd);
            return true;
        }

        public DataView GetAllTVoucherPayTypeDetails()
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from TVoucherPayTypeDetails order by PKVoucherPayTypeNo";
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public DataView GetTVoucherPayTypeDetailsByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from TVoucherPayTypeDetails where PKVoucherPayTypeNo =" + ID;
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public TVoucherPayTypeDetails ModifyTVoucherPayTypeDetailsByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql;
            SqlCommand cmd;
            sql = "Select * from TVoucherPayTypeDetails where PKVoucherPayTypeNo =" + ID;
            cmd = new SqlCommand(sql, Con);
            cmd.Connection = Con;
            Con.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (dr.HasRows)
            {
                TVoucherPayTypeDetails MM = new TVoucherPayTypeDetails();
                while (dr.Read())
                {
                    MM.PKVoucherPayTypeNo = Convert.ToInt32(dr["PKVoucherPayTypeNo"]);
                    if (!Convert.IsDBNull(dr["FKSalesVoucherNo"])) MM.FKSalesVoucherNo = Convert.ToInt64(dr["FKSalesVoucherNo"]);
                    if (!Convert.IsDBNull(dr["FKReceiptVoucherNo"])) MM.FKReceiptVoucherNo = Convert.ToInt64(dr["FKReceiptVoucherNo"]);
                    if (!Convert.IsDBNull(dr["FKVoucherTrnNo"])) MM.FKVoucherTrnNo = Convert.ToInt64(dr["FKVoucherTrnNo"]);
                    if (!Convert.IsDBNull(dr["FKPayTypeNo"])) MM.FKPayTypeNo = Convert.ToInt64(dr["FKPayTypeNo"]);
                    if (!Convert.IsDBNull(dr["Amount"])) MM.Amount = Convert.ToDouble(dr["Amount"]);
                    if (!Convert.IsDBNull(dr["CompanyNo"])) MM.CompanyNo = Convert.ToInt64(dr["CompanyNo"]);
                }
                dr.Close();
                return MM;
            }
            else
                dr.Close();
            return new TVoucherPayTypeDetails();
        }

        public bool UpdateChequeDetails(long FkChequeNo)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Update TChequeNoDetails set FkVoucherTrnNo=@FkVoucherTrnNo where PkSrNo=@FkChequeNo";

            cmd.Parameters.AddWithValue("@FkChequeNo", FkChequeNo);

            commandcollection.Add(cmd);
            return true;
        }

        public bool UpdateChequeCreditDetails(long pkSrNo)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Update TVoucherChqCreditDetails set PostFkVoucherNo=@FkVoucherNo , PostFkVoucherTrnNo=@FkVoucherTrnNo, IsPost='true' where PkSrNo=@pkSrNo";

            cmd.Parameters.AddWithValue("@PkSrNo", pkSrNo);

            commandcollection.Add(cmd);
            return true;
        }

        public bool DeleteChequeDetails(long FkChequeNo)
        {

            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Update TChequeNoDetails set FkVoucherTrnNo=0 where PkSrNo=@FkChequeNo";
            cmd.Parameters.AddWithValue("@FkChequeNo", FkChequeNo);
            commandcollection.Add(cmd);
            return true;

        }

        public void UpdateReceiptDetails(long SalesID, long CompNo, DateTime BillDate, long PartyLedgerNo)
        {

            double totamt = 0;

            DBTVaucherEntry dbTVoucherEntry = new DBTVaucherEntry();
            dbTVoucherEntry.DeleteAllVoucherEntry(SalesID);
            dbTVoucherEntry.ExecuteNonQueryStatements();

            dbTVoucherEntry = new DBTVaucherEntry();
            long ReceiptID = ObjQry.ReturnLong("SELECT TVoucherDetails.FkVoucherNo FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo " +
                        " WHERE (TVoucherEntry.VoucherTypeCode = " + VchType.Receipt + ") AND (TVoucherEntry.VoucherDate ='" + Convert.ToDateTime(BillDate).ToString("dd-MMM-yyyy") + "') AND (TVoucherDetails.LedgerNo = " + PartyLedgerNo + ") AND " +
                        " (TVoucherDetails.VoucherSrNo = 1) AND (TVoucherEntry.PayTypeNo=2) AND TVoucherEntry.CompanyNo=" + CompNo + "", CommonFunctions.ConStr);
            if (ReceiptID != 0)
            {
                double Amt = ObjQry.ReturnDouble("SELECT IsNull(SUM(TVoucherDetails.Debit),0) FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo " +
                                              " WHERE (TVoucherDetails.SrNo = 501) AND (TVoucherEntry.PayTypeNo=2) AND (TVoucherDetails.LedgerNo = " + PartyLedgerNo + ") AND (TVoucherEntry.CompanyNo = " + CompNo + ") AND (TVoucherEntry.VoucherDate = '" + BillDate.ToString("dd-MMM-yyyy") + "') AND  (TVoucherEntry.VoucherTypeCode = " + VchType.Sales + ") ", CommonFunctions.ConStr);

                if (Amt == 0)
                    dbTVoucherEntry.DeleteAllVoucherEntry(ReceiptID);
                else
                {
                    DataTable dtReceipt = ObjFunction.GetDataView("SELECT PkVoucherTrnNo,LedgerNo,Debit,Credit,SrNo FROM TVoucherDetails " +
                      " WHERE (FkVoucherNo = " + ReceiptID + ") order by VoucherSrNo ").Table;
                    for (int i = 0; i < dtReceipt.Rows.Count; i++)
                    {
                        if (i == 0)
                            dbTVoucherEntry.UpdateVoucherDetails(0, Amt, Convert.ToInt64(dtReceipt.Rows[i].ItemArray[0].ToString()));
                        else
                            dbTVoucherEntry.UpdateVoucherDetails(Amt, 0, Convert.ToInt64(dtReceipt.Rows[i].ItemArray[0].ToString()));
                    }
                    dbTVoucherEntry.UpdateBillAmount(ReceiptID, Amt);
                }
                dbTVoucherEntry.ExecuteNonQueryStatements();
            }
        }

        public bool UpdateBillAmount(long PKvoucherNo, double BilledAmount)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Update TVoucherEntry Set BilledAmount=@BilledAmount Where PkVoucherNo=@PkVoucherNo";


            cmd.Parameters.AddWithValue("@BilledAmount", BilledAmount);
            cmd.Parameters.AddWithValue("@PkVoucherNo", PKvoucherNo);

            commandcollection.Add(cmd);
            return true;
        }

        public bool UpdateBillStatus(long PKvoucherNo)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Update TVoucherEntry Set StatusNo=3 Where PkVoucherNo=@PkVoucherNo";


            cmd.Parameters.AddWithValue("@PkVoucherNo", PKvoucherNo);

            commandcollection.Add(cmd);
            return true;
        }

        public void UpdateReceiptDetails(long SalesID, long CompNo, DateTime BillDate, long PartyLedgerNo, long ReciptType, long VoucherType, long OpningStockNo)
        {

            double totamt = 0;

            ObjTrans.Execute("Update TStock Set  FKVoucherNo=" + OpningStockNo + ", FkVoucherTrnNo=0 Where FkVoucherNo=" + SalesID + "", CommonFunctions.ConStr);

            DBTVaucherEntry dbTVoucherEntry = new DBTVaucherEntry();
            dbTVoucherEntry.DeleteAllVoucherEntry(SalesID);
            dbTVoucherEntry.ExecuteNonQueryStatements();


            dbTVoucherEntry = new DBTVaucherEntry();
            long ReceiptID = ObjQry.ReturnLong("SELECT TVoucherDetails.FkVoucherNo FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo " +
                        " WHERE (TVoucherEntry.VoucherTypeCode = " + ReciptType + ") AND (TVoucherEntry.VoucherDate ='" + Convert.ToDateTime(BillDate).ToString("dd-MMM-yyyy") + "') AND (TVoucherDetails.LedgerNo = " + PartyLedgerNo + ") AND " +
                        " (TVoucherDetails.VoucherSrNo = 1) AND (TVoucherEntry.PayTypeNo=2) AND TVoucherEntry.CompanyNo=" + CompNo + "", CommonFunctions.ConStr);
            if (ReceiptID != 0)
            {
                double Amt = ObjQry.ReturnDouble("SELECT IsNull(SUM(TVoucherDetails.Debit),0) FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo " +
                                              " WHERE (TVoucherDetails.SrNo = 501) AND (TVoucherEntry.PayTypeNo=2) AND (TVoucherDetails.LedgerNo = " + PartyLedgerNo + ") AND (TVoucherEntry.CompanyNo = " + CompNo + ") AND (TVoucherEntry.VoucherDate = '" + BillDate + "') AND  (TVoucherEntry.VoucherTypeCode = " + VoucherType + ") ", CommonFunctions.ConStr);

                if (Amt == 0)
                    dbTVoucherEntry.DeleteAllVoucherEntry(ReceiptID);
                else
                {
                    DataTable dtReceipt = ObjFunction.GetDataView("SELECT PkVoucherTrnNo,LedgerNo,Debit,Credit,SrNo FROM TVoucherDetails " +
                      " WHERE (FkVoucherNo = " + ReceiptID + ") order by VoucherSrNo ").Table;
                    for (int i = 0; i < dtReceipt.Rows.Count; i++)
                    {
                        if (i == 0)
                            dbTVoucherEntry.UpdateVoucherDetails(0, Amt, Convert.ToInt64(dtReceipt.Rows[i].ItemArray[0].ToString()));
                        else
                            dbTVoucherEntry.UpdateVoucherDetails(Amt, 0, Convert.ToInt64(dtReceipt.Rows[i].ItemArray[0].ToString()));
                    }
                    dbTVoucherEntry.UpdateBillAmount(ReceiptID, Amt);
                }
                dbTVoucherEntry.ExecuteNonQueryStatements();
            }
        }



        public bool AddTVoucherChequeDetails(TVoucherChequeDetails tvoucherchequedetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTVoucherChequeDetails";

            cmd.Parameters.AddWithValue("@PkSrNo", tvoucherchequedetails.PkSrNo);

            cmd.Parameters.AddWithValue("@FkVoucherTrnNo", tvoucherchequedetails.FkVoucherTrnNo);

            cmd.Parameters.AddWithValue("@ClearingDate", tvoucherchequedetails.ClearingDate);

            cmd.Parameters.AddWithValue("@UserID", tvoucherchequedetails.UserID);

            cmd.Parameters.AddWithValue("@UserDate", tvoucherchequedetails.UserDate);

            commandcollection.Add(cmd);
            return true;
        }

        public bool DeleteTVoucherChequeDetails(TVoucherChequeDetails tvoucherchequedetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteTVoucherChequeDetails";

            cmd.Parameters.AddWithValue("@PkSrNo", tvoucherchequedetails.PkSrNo);
            if (ObjTrans.ExecuteNonQuery(cmd, CommonFunctions.ConStr) == true)
            {
                return true;
            }
            else
            {
                tvoucherchequedetails.msg = ObjTrans.ErrorMessage;
                return false;
            }
        }

        public DataView GetAllTVoucherChequeDetails()
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from TVoucherChequeDetails order by PkSrNo";
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public DataView GetTVoucherChequeDetailsByID(int ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from TVoucherChequeDetails where PkSrNo =" + ID;
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch { throw; }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public TVoucherChequeDetails ModifyTVoucherChequeDetailsByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql;
            SqlCommand cmd;
            sql = "Select * from TVoucherChequeDetails where PkSrNo =" + ID;
            cmd = new SqlCommand(sql, Con);
            cmd.Connection = Con;
            Con.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (dr.HasRows)
            {
                TVoucherChequeDetails MM = new TVoucherChequeDetails();
                while (dr.Read())
                {
                    MM.PkSrNo = Convert.ToInt32(dr["PkSrNo"]);
                    if (!Convert.IsDBNull(dr["FkVoucherTrnNo"])) MM.FkVoucherTrnNo = Convert.ToInt64(dr["FkVoucherTrnNo"]);
                    if (!Convert.IsDBNull(dr["ClearingDate"])) MM.ClearingDate = Convert.ToDateTime(dr["ClearingDate"]);
                    if (!Convert.IsDBNull(dr["UserID"])) MM.UserID = Convert.ToInt64(dr["UserID"]);
                    if (!Convert.IsDBNull(dr["UserDate"])) MM.UserDate = Convert.ToDateTime(dr["UserDate"]);
                }
                dr.Close();
                return MM;
            }
            else
                dr.Close();
            return new TVoucherChequeDetails();
        }

        public bool AddTVoucherChqCreditDetails(TVoucherChqCreditDetails tvoucherchqcreditdetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTVoucherChqCreditDetails";

            cmd.Parameters.AddWithValue("@PkSrNo", tvoucherchqcreditdetails.PkSrNo);

            //cmd.Parameters.AddWithValue("@FKVoucherNo", tvoucherchqcreditdetails.FKVoucherNo);

            //cmd.Parameters.AddWithValue("@FkVoucherTrnNo", tvoucherchqcreditdetails.FkVoucherTrnNo);

            cmd.Parameters.AddWithValue("@ChequeNo", tvoucherchqcreditdetails.ChequeNo);

            cmd.Parameters.AddWithValue("@ChequeDate", tvoucherchqcreditdetails.ChequeDate);

            cmd.Parameters.AddWithValue("@BankNo", tvoucherchqcreditdetails.BankNo);

            cmd.Parameters.AddWithValue("@BranchNo", tvoucherchqcreditdetails.BranchNo);

            cmd.Parameters.AddWithValue("@CreditCardNo", tvoucherchqcreditdetails.CreditCardNo);

            cmd.Parameters.AddWithValue("@Amount", tvoucherchqcreditdetails.Amount);

            cmd.Parameters.AddWithValue("@IsPost", tvoucherchqcreditdetails.IsPost);

            cmd.Parameters.AddWithValue("@PostFkVoucherNo", tvoucherchqcreditdetails.PostFkVoucherNo);

            cmd.Parameters.AddWithValue("@PostFkVoucherTrnNo", tvoucherchqcreditdetails.PostFkVoucherTrnNo);

            cmd.Parameters.AddWithValue("@CompanyNo", tvoucherchqcreditdetails.CompanyNo);

            commandcollection.Add(cmd);
            return true;
        }

        public bool DeleteTVoucherChqCreditDetails(TVoucherChqCreditDetails tvoucherchqcreditdetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteTVoucherChqCreditDetails";

            cmd.Parameters.AddWithValue("@PkSrNo", tvoucherchqcreditdetails.PkSrNo);
            commandcollection.Add(cmd);
            return true;
        }

        public bool AddTParkingBill(TParkingBill tparkingbill)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTParkingBill";

            cmd.Parameters.AddWithValue("@ParkingBillNo", tparkingbill.ParkingBillNo);

            cmd.Parameters.AddWithValue("@BillNo", tparkingbill.BillNo);

            cmd.Parameters.AddWithValue("@BillDate", tparkingbill.BillDate);

            cmd.Parameters.AddWithValue("@BillTime", tparkingbill.BillTime);

            cmd.Parameters.AddWithValue("@PersonName", tparkingbill.PersonName);

            cmd.Parameters.AddWithValue("@LedgerNo", tparkingbill.LedgerNo);

            cmd.Parameters.AddWithValue("@IsBill", tparkingbill.IsBill);

            cmd.Parameters.AddWithValue("@CompanyNo", tparkingbill.CompanyNo);

            cmd.Parameters.AddWithValue("@IsCancel", tparkingbill.IsCancel);

            cmd.Parameters.AddWithValue("@UserID", tparkingbill.UserID);

            cmd.Parameters.AddWithValue("@UserDate", tparkingbill.UserDate);

            cmd.Parameters.AddWithValue("@Discount", tparkingbill.Discount);

            cmd.Parameters.AddWithValue("@Charges", tparkingbill.Charges);

            cmd.Parameters.AddWithValue("@Remark", tparkingbill.Remark);

            cmd.Parameters.AddWithValue("@RateTypeNo", tparkingbill.RateTypeNo);

            cmd.Parameters.AddWithValue("@TaxTypeNo", tparkingbill.TaxTypeNo);

            SqlParameter outParameter = new SqlParameter();
            outParameter.ParameterName = "@ReturnID";
            outParameter.Direction = ParameterDirection.Output;
            outParameter.DbType = DbType.Int32;
            cmd.Parameters.Add(outParameter);

            commandcollection.Add(cmd);
            return true;
        }

        public bool AddTParkingBillDetails(TParkingBillDetails tparkingbilldetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTParkingBillDetails";

            cmd.Parameters.AddWithValue("@PkSrNo", tparkingbilldetails.PkSrNo);

            //cmd.Parameters.AddWithValue("@ParkingBillNo", tparkingbilldetails.ParkingBillNo);

            cmd.Parameters.AddWithValue("@Barcode", tparkingbilldetails.Barcode);

            cmd.Parameters.AddWithValue("@Qty", tparkingbilldetails.Qty);

            cmd.Parameters.AddWithValue("@Rate", tparkingbilldetails.Rate);

            cmd.Parameters.AddWithValue("@ItemDisc", tparkingbilldetails.ItemDisc);

            cmd.Parameters.AddWithValue("@UOMNo", tparkingbilldetails.UOMNo);

            cmd.Parameters.AddWithValue("@FkRateSettingNo", tparkingbilldetails.FkRateSettingNo);

            cmd.Parameters.AddWithValue("@ItemNo", tparkingbilldetails.ItemNo);

            cmd.Parameters.AddWithValue("@CompanyNo", DBGetVal.CompanyNo);

            commandcollection.Add(cmd);
            return true;
        }

        public bool DeleteTParkingBill(TParkingBill tparkingbill)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteTParkingBill";

            cmd.Parameters.AddWithValue("@ParkingBillNo", tparkingbill.ParkingBillNo);

            cmd.Parameters.AddWithValue("@FKVoucherNo", tparkingbill.FKVoucherNo);

            if (ObjTrans.ExecuteNonQuery(cmd, CommonFunctions.ConStr) == true)
            {
                return true;
            }
            else
            {
                tparkingbill.msg = ObjTrans.ErrorMessage;
                return false;
            }
        }

        public bool DeleteTParkingBillDetails(TParkingBillDetails tparkingbilldetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteTParkingBillDetails";

            cmd.Parameters.AddWithValue("@PkSrNo", tparkingbilldetails.PkSrNo);
            if (ObjTrans.ExecuteNonQuery(cmd, CommonFunctions.ConStr) == true)
            {
                return true;
            }
            else
            {
                tparkingbilldetails.msg = ObjTrans.ErrorMessage;
                return false;
            }
        }



        public long ExecuteNonQueryStatements()
        {
            SqlConnection cn = null;
            cn = new SqlConnection(CommonFunctions.ConStr);
            cn.Open();

            SqlTransaction myTrans;
            myTrans = cn.BeginTransaction();
            //cmd.Transaction = myTrans;
            int cntVchNo = -1, cntRef = 0, cntStock = 0, cntRateSettingNo = -1, RewardNo = -1, RewardDtlsNo = -1;
            try
            {
                for (int i = 0; (i < this.commandcollection.Count); i = (i + 1))
                {
                    if ((this.commandcollection[i] != null))
                    {
                        commandcollection[i].Connection = cn;
                        commandcollection[i].Transaction = myTrans;
                        if (commandcollection[i].CommandText == "AddTVoucherEntry")
                        {
                            cntVchNo = i;
                        }
                        if (commandcollection[i].CommandText == "AddTVoucherDetails")
                        {
                            commandcollection[i].Parameters.AddWithValue("@FkVoucherNo", commandcollection[cntVchNo].Parameters["@ReturnID"].Value);
                            cntRef = i;
                        }

                        if (commandcollection[i].CommandText == "AddTVoucherRefDetails")
                        {
                            // if(cntRef!=0)
                            commandcollection[i].Parameters.AddWithValue("@FkVoucherTrnNo", commandcollection[cntRef].Parameters["@ReturnID"].Value);
                            //else
                            //  commandcollection[i].Parameters["@FkVoucherTrnNo"].Value = commandcollection[cntRateSettingNo].Parameters["@ReturnID"].Value;
                        }
                        if (commandcollection[i].CommandText == "AddTStock")
                        {
                            commandcollection[i].Parameters.AddWithValue("@FkVoucherNo", commandcollection[cntVchNo].Parameters["@ReturnID"].Value);
                            if (cntRef != 0)
                                commandcollection[i].Parameters.AddWithValue("@FkVoucherTrnNo", commandcollection[cntRef].Parameters["@ReturnID"].Value);
                            else
                                commandcollection[i].Parameters.AddWithValue("@FkVoucherTrnNo", 0);
                            cntStock = i;
                            if (cntRateSettingNo != -1)
                            {
                                commandcollection[i].Parameters["@FkRateSettingNo"].Value = commandcollection[cntRateSettingNo].Parameters["@ReturnID"].Value;

                                //commandcollection[i].CommandText.IndexOf("@FkRateSettingNo", Convert.ToInt32(commandcollection[cntRateSettingNo].Parameters["@ReturnID"].Value));

                                cntRateSettingNo = -1;
                            }
                        }
                        if (commandcollection[i].CommandText == "AddTStockGodown")
                        {
                            commandcollection[i].Parameters.AddWithValue("@FKStockTrnNo", commandcollection[cntStock].Parameters["@ReturnID"].Value);
                        }
                        if (commandcollection[i].CommandText == "AddTVoucherPayTypeDetails")
                        {
                            if (cntVchNo != -1)
                            {
                                commandcollection[i].Parameters.AddWithValue("@FKReceiptVoucherNo", commandcollection[cntVchNo].Parameters["@ReturnID"].Value);
                                if (cntRef != 0)
                                    commandcollection[i].Parameters.AddWithValue("@FkVoucherTrnNo", commandcollection[cntRef].Parameters["@ReturnID"].Value);
                            }
                            else
                            {
                                commandcollection[i].Parameters.AddWithValue("@FKReceiptVoucherNo", 0);
                                commandcollection[i].Parameters.AddWithValue("@FkVoucherTrnNo", 0);
                            }
                        }
                        if (commandcollection[i].CommandText.IndexOf("Update") >= 0)
                        {
                            if (cntRef != 0)
                                if (commandcollection[i].CommandText.IndexOf("@pkSrNo") >= 0)
                                {
                                    commandcollection[i].Parameters.AddWithValue("@FkVoucherNo", commandcollection[cntVchNo].Parameters["@ReturnID"].Value);
                                    commandcollection[i].Parameters.AddWithValue("@FkVoucherTrnNo", commandcollection[cntRef].Parameters["@ReturnID"].Value);
                                }
                                else
                                    commandcollection[i].Parameters.AddWithValue("@FkVoucherTrnNo", commandcollection[cntRef].Parameters["@ReturnID"].Value);

                        }
                        if (commandcollection[i].CommandText == "AddTVoucherChqCreditDetails")
                        {
                            commandcollection[i].Parameters.AddWithValue("@FkVoucherNo", commandcollection[cntVchNo].Parameters["@ReturnID"].Value);
                            if (cntRef != 0)
                                commandcollection[i].Parameters.AddWithValue("@FkVoucherTrnNo", commandcollection[cntRef].Parameters["@ReturnID"].Value);
                            else
                                commandcollection[i].Parameters.AddWithValue("@FkVoucherTrnNo", 0);
                        }
                        if (commandcollection[i].CommandText == "AddTParkingBill")
                        {
                            cntVchNo = i;
                        }
                        if (commandcollection[i].CommandText == "AddTParkingBillDetails")
                        {
                            commandcollection[i].Parameters.AddWithValue("@ParkingBillNo", commandcollection[0].Parameters["@ReturnID"].Value);
                        }
                        if (commandcollection[i].CommandText == "AddMRateSetting3")
                        {
                            cntRateSettingNo = i;
                        }
                        if (commandcollection[i].CommandText == "AddTVoucherRefDetails1")
                        {

                        }
                        if (commandcollection[i].CommandText == "AddTReward")
                        {
                            commandcollection[i].Parameters.AddWithValue("@FkVoucherNo", commandcollection[cntVchNo].Parameters["@ReturnID"].Value);
                            RewardNo = i;
                        }
                        if (commandcollection[i].CommandText == "AddTRewardDetails")
                        {
                            commandcollection[i].Parameters.AddWithValue("@RewardNo", commandcollection[RewardNo].Parameters["@ReturnID"].Value);
                            RewardDtlsNo = i;
                        }
                        if (commandcollection[i].CommandText == "AddTRewardFrom")
                        {
                            RewardDtlsNo = ReturnCommPos(Convert.ToInt64(commandcollection[RewardDtlsNo].Parameters["@SchemeDetailsNo"].Value));

                            commandcollection[i].Parameters.AddWithValue("@RewardNo", commandcollection[RewardNo].Parameters["@ReturnID"].Value);
                            commandcollection[i].Parameters.AddWithValue("@RewardDetailsNo", commandcollection[RewardDtlsNo].Parameters["@ReturnID"].Value);
                            commandcollection[i].Parameters.AddWithValue("@FkStockNo", commandcollection[cntStock].Parameters["@ReturnID"].Value);
                        }
                        if (commandcollection[i].CommandText == "AddTRewardTo")
                        {
                            RewardDtlsNo = ReturnCommPos(Convert.ToInt64(commandcollection[RewardDtlsNo].Parameters["@SchemeDetailsNo"].Value));
                            commandcollection[i].Parameters.AddWithValue("@RewardNo", commandcollection[RewardNo].Parameters["@ReturnID"].Value);
                            commandcollection[i].Parameters.AddWithValue("@RewardDetailsNo", commandcollection[RewardDtlsNo].Parameters["@ReturnID"].Value);
                            commandcollection[i].Parameters.AddWithValue("@FkStockNo", commandcollection[cntStock].Parameters["@ReturnID"].Value);
                        }
                        if (commandcollection[i].CommandText == "AddTFooterDiscountDetails")
                        {
                            commandcollection[i].Parameters.AddWithValue("@FkVoucherNo", commandcollection[cntVchNo].Parameters["@ReturnID"].Value);
                        }
                        if (commandcollection[i].CommandText == "AddTItemLevelDiscountDetails")
                        {
                            commandcollection[i].Parameters.AddWithValue("@FKStockTrnNo", commandcollection[cntStock].Parameters["@ReturnID"].Value);
                        }
                        if (commandcollection[i].CommandText == "AddTOtherStockDetails")
                        {
                            commandcollection[i].Parameters.AddWithValue("@FkVoucherNo", commandcollection[cntVchNo].Parameters["@ReturnID"].Value);
                            commandcollection[i].Parameters.AddWithValue("@FKStockTrnNo", commandcollection[cntStock].Parameters["@ReturnID"].Value);
                        }
                        if (commandcollection[i].CommandText == "StockEffect")
                        {
                            commandcollection[i].Parameters.AddWithValue("@FkVoucherNo", commandcollection[cntVchNo].Parameters["@ReturnID"].Value);
                        }
                        //if (commandcollection[i].CommandText == "AddTRewardFrom")
                        //{
                        //    DataRow[] dr = dtId.Select("SchemeDetailsNo=" + commandcollection[i].Parameters["@SchemeDetailsNo"].Value);
                        //    commandcollection[i].Parameters.AddWithValue("@RewardNo",Convert.ToInt64(dr[0]["RewardNo"].ToString()));
                        //    commandcollection[i].Parameters.AddWithValue("@RewardDetailsNo", Convert.ToInt64(dr[0]["RewardDetailsNo"].ToString()));
                        //    commandcollection[i].Parameters.AddWithValue("@FkStockNo", commandcollection[cntStock].Parameters["@ReturnID"].Value);
                        //}
                        //if (commandcollection[i].CommandText == "AddTRewardTo")
                        //{
                        //    DataRow[] dr = dtId.Select("SchemeDetailsNo=" + commandcollection[i].Parameters["@SchemeDetailsNo"].Value);
                        //    commandcollection[i].Parameters.AddWithValue("@RewardNo", Convert.ToInt64(dr[0]["RewardNo"].ToString()));
                        //    commandcollection[i].Parameters.AddWithValue("@RewardDetailsNo", Convert.ToInt64(dr[0]["RewardDetailsNo"].ToString()));
                        //    commandcollection[i].Parameters.AddWithValue("@FkStockNo", commandcollection[cntStock].Parameters["@ReturnID"].Value);
                        //}

                        if (commandcollection[i] != null)
                            commandcollection[i].ExecuteNonQuery();

                        //if (commandcollection[i].CommandText == "AddTRewardDetails")
                        //{
                        //    AddRows(Convert.ToInt64(commandcollection[i].Parameters["@SchemeDetailsNo"].Value), Convert.ToInt64(commandcollection[i].Parameters["@RewardNo"].Value),Convert.ToInt64(commandcollection[i].Parameters["@ReturnID"].Value));
                        //}
                    }
                }

                myTrans.Commit();
                if (cntVchNo == -1)
                    return 0;
                else
                    return Convert.ToInt64(commandcollection[cntVchNo].Parameters["@ReturnID"].Value);


            }
            catch (Exception e)
            {
                myTrans.Rollback();

                if (e.GetBaseException().Message == "")
                {
                    strerrormsg = e.Message;
                }
                else
                {
                    strerrormsg = e.GetBaseException().Message;
                }
                return 0;
            }
            finally
            {
                cn.Close();
            }

            //________________________________________________________________________________________________________________________________________________________________________________________________________________________
        }

        public bool UpdateVoucherDetails(double Debit, double Credit, long PkVoucherTrnNo)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Update TVoucherDetails set Debit=@Debit , Credit=@Credit where PKVoucherTrnNo=@PkVoucherTrnNo";

            cmd.Parameters.AddWithValue("@Debit", Debit);
            cmd.Parameters.AddWithValue("@Credit", Credit);
            cmd.Parameters.AddWithValue("@PkVoucherTrnNo", PkVoucherTrnNo);
            commandcollection.Add(cmd);
            return true;
        }

        public bool DeleteAllVoucherEntry(TVoucherEntry tvoucherentry)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteAllVoucherEntry";

            cmd.Parameters.AddWithValue("@VoucherNo", tvoucherentry.PkVoucherNo);
            if (ObjTrans.ExecuteNonQuery(cmd, CommonFunctions.ConStr) == true)
            {
                return true;
            }
            else
            {
                tvoucherentry.msg = ObjTrans.ErrorMessage;
                return false;
            }
        }

        public bool CancelTVoucherEntry(TVoucherEntry tvoucherentry)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "CancelTVoucherEntry";

            cmd.Parameters.AddWithValue("@PkVoucherNo", tvoucherentry.PkVoucherNo);
            if (ObjTrans.ExecuteNonQuery(cmd, CommonFunctions.ConStr) == true)
            {
                return true;
            }
            else
            {
                tvoucherentry.msg = ObjTrans.ErrorMessage;
                return false;
            }
        }

        public bool DeleteReceiptDetails(DateTime FromDate, long PayType)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteReceiptEntry";

            cmd.Parameters.AddWithValue("@FromDate", FromDate);
            cmd.Parameters.AddWithValue("@PayType", PayType);


            commandcollection.Add(cmd);
            return true;
        }

        public bool DeleteAllVoucherEntry(long PKvoucherNo)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteAllVoucherEntry";

            cmd.Parameters.AddWithValue("@VoucherNo", PKvoucherNo);

            commandcollection.Add(cmd);
            return true;
        }

        public bool DeleteAllVoucherEntryBillWise(long PKvoucherNo)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteAllVoucherEntryBillWise";

            cmd.Parameters.AddWithValue("@VoucherNo", PKvoucherNo);

            commandcollection.Add(cmd);
            return true;
        }

        public bool ExecuteNonQueryStatementsCheque()
        {

            SqlConnection cn = null;
            cn = new SqlConnection(CommonFunctions.ConStr);
            cn.Open();

            SqlTransaction myTrans;
            myTrans = cn.BeginTransaction();
            //cmd.Transaction = myTrans;
            try
            {
                for (int i = 0; (i < this.commandcollection.Count); i = (i + 1))
                {
                    if ((this.commandcollection[i] != null))
                    {
                        commandcollection[i].Connection = cn;
                        commandcollection[i].Transaction = myTrans;

                        if (commandcollection[i] != null)
                            commandcollection[i].ExecuteNonQuery();
                    }
                }

                myTrans.Commit();
                return true;
            }
            catch (Exception e)
            {
                myTrans.Rollback();

                if (e.GetBaseException().Message == "")
                {
                    strerrormsg = e.Message;
                }
                else
                {
                    strerrormsg = e.GetBaseException().Message;
                }
                return false;
            }
            finally
            {
                cn.Close();
            }
            //________________________________________________________________________________________________________________________________________________________________________________________________________________________
        }

        public bool AddMRateSetting3(MRateSetting3 mratesetting)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddMRateSetting3";

            cmd.Parameters.AddWithValue("@PkSrNo", mratesetting.PkSrNo);

            cmd.Parameters.AddWithValue("@FkBcdSrNo", mratesetting.FkBcdSrNo);

            cmd.Parameters.AddWithValue("@ItemNo", mratesetting.ItemNo);

            cmd.Parameters.AddWithValue("@FromDate", mratesetting.FromDate);

            cmd.Parameters.AddWithValue("@PurRate", mratesetting.PurRate);

            cmd.Parameters.AddWithValue("@MRP", mratesetting.MRP);

            cmd.Parameters.AddWithValue("@UOMNo", mratesetting.UOMNo);

            cmd.Parameters.AddWithValue("@ASaleRate", mratesetting.ASaleRate);

            cmd.Parameters.AddWithValue("@BSaleRate", mratesetting.BSaleRate);

            cmd.Parameters.AddWithValue("@CSaleRate", mratesetting.CSaleRate);

            cmd.Parameters.AddWithValue("@DSaleRate", mratesetting.DSaleRate);

            cmd.Parameters.AddWithValue("@ESaleRate", mratesetting.ESaleRate);

            cmd.Parameters.AddWithValue("@StockConversion", mratesetting.StockConversion);

            cmd.Parameters.AddWithValue("@PerOfRateVariation", mratesetting.PerOfRateVariation);

            cmd.Parameters.AddWithValue("@MKTQty", mratesetting.MKTQty);

            cmd.Parameters.AddWithValue("@IsActive", mratesetting.IsActive);

            cmd.Parameters.AddWithValue("@UserID", mratesetting.UserID);

            cmd.Parameters.AddWithValue("@UserDate", mratesetting.UserDate);

            cmd.Parameters.AddWithValue("@CompanyNo", mratesetting.CompanyNo);

            SqlParameter outParameter = new SqlParameter();
            outParameter.ParameterName = "@ReturnID";
            outParameter.Direction = ParameterDirection.Output;
            outParameter.DbType = DbType.Int32;
            cmd.Parameters.Add(outParameter);

            commandcollection.Add(cmd);
            return true;
        }


        public bool AddTReward(TReward treward)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTReward";

            cmd.Parameters.AddWithValue("@RewardNo", treward.RewardNo);

            //cmd.Parameters.AddWithValue("@FkVoucherNo", treward.FkVoucherNo);

            cmd.Parameters.AddWithValue("@VoucherUserNo", treward.VoucherUserNo);

            cmd.Parameters.AddWithValue("@TotalBillAmount", treward.TotalBillAmount);

            cmd.Parameters.AddWithValue("@LedgerNo", treward.LedgerNo);

            cmd.Parameters.AddWithValue("@ToalDiscAmount", treward.ToalDiscAmount);

            cmd.Parameters.AddWithValue("@FkVoucherTrnNo", treward.FkVoucherTrnNo);

            cmd.Parameters.AddWithValue("@RedemptionStatusNo", treward.RedemptionStatusNo);

            cmd.Parameters.AddWithValue("@CompanyNo", treward.CompanyNo);

            cmd.Parameters.AddWithValue("@UserID", treward.UserID);

            cmd.Parameters.AddWithValue("@UserDate", treward.UserDate);

            SqlParameter outParameter = new SqlParameter();
            outParameter.ParameterName = "@ReturnID";
            outParameter.Direction = ParameterDirection.Output;
            outParameter.DbType = DbType.Int32;
            cmd.Parameters.Add(outParameter);

            commandcollection.Add(cmd);
            dtId = ObjFunction.GetDataView("SELECT SchemeDetailsNo, RewardNo, PkSrNo AS 'RewardDetailsNo' FROM TRewardDetails WHERE (PkSrNo = 0)").Table;
            return true;
        }

        public void AddRows(long SchDetailsNo, long RewardNo, long RewardDetailsNo)
        {
            DataRow dr = dtId.NewRow();
            dr[0] = SchDetailsNo;
            dr[1] = RewardNo;
            dr[2] = RewardDetailsNo;
            dtId.Rows.Add(dr);
        }

        public bool DeleteTReward(TReward treward)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteTReward";

            cmd.Parameters.AddWithValue("@RewardNo", treward.RewardNo);
            commandcollection.Add(cmd);
            return true;
        }

        public TReward ModifyTRewardByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql;
            SqlCommand cmd;
            sql = "Select * from TReward where RewardNo =" + ID;
            cmd = new SqlCommand(sql, Con);
            cmd.Connection = Con;
            Con.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (dr.HasRows)
            {
                TReward MM = new TReward();
                while (dr.Read())
                {
                    MM.RewardNo = Convert.ToInt32(dr["RewardNo"]);
                    if (!Convert.IsDBNull(dr["FkVoucherNo"])) MM.FkVoucherNo = Convert.ToInt64(dr["FkVoucherNo"]);
                    if (!Convert.IsDBNull(dr["VoucherUserNo"])) MM.VoucherUserNo = Convert.ToInt64(dr["VoucherUserNo"]);
                    if (!Convert.IsDBNull(dr["TotalBillAmount"])) MM.TotalBillAmount = Convert.ToInt64(dr["TotalBillAmount"]);
                    if (!Convert.IsDBNull(dr["LedgerNo"])) MM.LedgerNo = Convert.ToInt64(dr["LedgerNo"]);
                    if (!Convert.IsDBNull(dr["ToalDiscAmount"])) MM.ToalDiscAmount = Convert.ToInt64(dr["ToalDiscAmount"]);
                    if (!Convert.IsDBNull(dr["FkVoucherTrnNo"])) MM.FkVoucherTrnNo = Convert.ToInt64(dr["FkVoucherTrnNo"]);
                    if (!Convert.IsDBNull(dr["RedemptionStatusNo"])) MM.RedemptionStatusNo = Convert.ToInt64(dr["RedemptionStatusNo"]);
                    if (!Convert.IsDBNull(dr["CompanyNo"])) MM.CompanyNo = Convert.ToInt64(dr["CompanyNo"]);
                    if (!Convert.IsDBNull(dr["UserID"])) MM.UserID = Convert.ToInt64(dr["UserID"]);
                    if (!Convert.IsDBNull(dr["UserDate"])) MM.UserDate = Convert.ToDateTime(dr["UserDate"]);
                }
                dr.Close();
                return MM;
            }
            else
                dr.Close();
            return new TReward();
        }

        public DataView GetAllTReward()
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from TReward order by RewardNo";
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch
            {
                throw;
            }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public DataView GetAllTReward(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from TReward Where RewardNo=" + ID;
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch
            {
                throw;
            }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }


        public bool AddTRewardDetails(TRewardDetails trewarddetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTRewardDetails";

            cmd.Parameters.AddWithValue("@PkSrNo", trewarddetails.PkSrNo);

            // cmd.Parameters.AddWithValue("@RewardNo", trewarddetails.RewardNo);

            cmd.Parameters.AddWithValue("@SchemeNo", trewarddetails.SchemeNo);

            cmd.Parameters.AddWithValue("@SchemeDetailsNo", trewarddetails.SchemeDetailsNo);

            cmd.Parameters.AddWithValue("@SchemeType", trewarddetails.SchemeType);

            cmd.Parameters.AddWithValue("@DiscPer", trewarddetails.DiscPer);

            cmd.Parameters.AddWithValue("@DiscAmount", trewarddetails.DiscAmount);

            cmd.Parameters.AddWithValue("@SchemeAmount", trewarddetails.SchemeAmount);

            cmd.Parameters.AddWithValue("@SchemeAcheiverNo", trewarddetails.SchemeAcheiverNo);

            cmd.Parameters.AddWithValue("@CompanyNo", trewarddetails.CompanyNo);

            cmd.Parameters.AddWithValue("@AchievedNoOfTime", trewarddetails.AchievedNoOfTime);

            cmd.Parameters.AddWithValue("@ActualNoOfTime", trewarddetails.ActualNoOfTime);

            SqlParameter outParameter = new SqlParameter();
            outParameter.ParameterName = "@ReturnID";
            outParameter.Direction = ParameterDirection.Output;
            outParameter.DbType = DbType.Int32;
            cmd.Parameters.Add(outParameter);

            commandcollection.Add(cmd);
            return true;
        }

        public bool DeleteTRewardDetails(TRewardDetails trewarddetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteTRewardDetails";

            cmd.Parameters.AddWithValue("@PkSrNo", trewarddetails.PkSrNo);
            commandcollection.Add(cmd);
            return true;
        }

        public TRewardDetails ModifyTRewardDetailsByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql;
            SqlCommand cmd;
            sql = "Select * from TRewardDetails where PkSrNo =" + ID;
            cmd = new SqlCommand(sql, Con);
            cmd.Connection = Con;
            Con.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (dr.HasRows)
            {
                TRewardDetails MM = new TRewardDetails();
                while (dr.Read())
                {
                    MM.PkSrNo = Convert.ToInt32(dr["PkSrNo"]);
                    if (!Convert.IsDBNull(dr["RewardNo"])) MM.RewardNo = Convert.ToInt64(dr["RewardNo"]);
                    if (!Convert.IsDBNull(dr["SchemeNo"])) MM.SchemeNo = Convert.ToInt64(dr["SchemeNo"]);
                    if (!Convert.IsDBNull(dr["SchemeDetailsNo"])) MM.SchemeDetailsNo = Convert.ToInt64(dr["SchemeDetailsNo"]);
                    if (!Convert.IsDBNull(dr["SchemeType"])) MM.SchemeType = Convert.ToInt64(dr["SchemeType"]);
                    if (!Convert.IsDBNull(dr["DiscPer"])) MM.DiscPer = Convert.ToInt64(dr["DiscPer"]);
                    if (!Convert.IsDBNull(dr["DiscAmount"])) MM.DiscAmount = Convert.ToInt64(dr["DiscAmount"]);
                    if (!Convert.IsDBNull(dr["SchemeAmount"])) MM.SchemeAmount = Convert.ToInt64(dr["SchemeAmount"]);
                    if (!Convert.IsDBNull(dr["CompanyNo"])) MM.CompanyNo = Convert.ToInt64(dr["CompanyNo"]);
                }
                dr.Close();
                return MM;
            }
            else
                dr.Close();
            return new TRewardDetails();
        }

        public DataView GetAllTRewardDetails()
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from TRewardDetails order by PkSrNo";
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch
            {
                throw;
            }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public DataView GetAllTRewardDetails(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from TRewardDetails Where PkSrNo=" + ID;
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch
            {
                throw;
            }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }


        public bool AddTRewardFrom(TRewardFrom trewardfrom)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTRewardFrom";

            cmd.Parameters.AddWithValue("@PkSrNo", trewardfrom.PkSrNo);

            // cmd.Parameters.AddWithValue("@RewardNo", trewardfrom.RewardNo);

            // cmd.Parameters.AddWithValue("@RewardDetailsNo", trewardfrom.RewardDetailsNo);

            cmd.Parameters.AddWithValue("@SchemeDetailsNo", trewardfrom.SchemeDetailsNo);

            cmd.Parameters.AddWithValue("@SchemeFromNo", trewardfrom.SchemeFromNo);

            //  cmd.Parameters.AddWithValue("@FkStockNo", trewardfrom.FkStockNo);

            cmd.Parameters.AddWithValue("@CompanyNo", trewardfrom.CompanyNo);

            commandcollection.Add(cmd);
            return true;
        }

        public bool DeleteTRewardFrom(TRewardFrom trewardfrom)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteTRewardFrom";

            cmd.Parameters.AddWithValue("@PkSrNo", trewardfrom.PkSrNo);
            commandcollection.Add(cmd);
            return true;
        }

        public TRewardFrom ModifyTRewardFromByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql;
            SqlCommand cmd;
            sql = "Select * from TRewardFrom where PkSrNo =" + ID;
            cmd = new SqlCommand(sql, Con);
            cmd.Connection = Con;
            Con.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (dr.HasRows)
            {
                TRewardFrom MM = new TRewardFrom();
                while (dr.Read())
                {
                    MM.PkSrNo = Convert.ToInt32(dr["PkSrNo"]);
                    if (!Convert.IsDBNull(dr["RewardNo"])) MM.RewardNo = Convert.ToInt64(dr["RewardNo"]);
                    if (!Convert.IsDBNull(dr["RewardDetailsNo"])) MM.RewardDetailsNo = Convert.ToInt64(dr["RewardDetailsNo"]);
                    if (!Convert.IsDBNull(dr["SchemeFromNo"])) MM.SchemeFromNo = Convert.ToInt64(dr["SchemeFromNo"]);
                    if (!Convert.IsDBNull(dr["FkStockNo"])) MM.FkStockNo = Convert.ToInt64(dr["FkStockNo"]);

                    if (!Convert.IsDBNull(dr["CompanyNo"])) MM.CompanyNo = Convert.ToInt64(dr["CompanyNo"]);
                }
                dr.Close();
                return MM;
            }
            else
                dr.Close();
            return new TRewardFrom();
        }

        public DataView GetAllTRewardFrom()
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from TRewardFrom order by PkSrNo";
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch
            {
                throw;
            }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public DataView GetAllTRewardFrom(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from TRewardFrom Where PkSrNo=" + ID;
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch
            {
                throw;
            }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public bool AddTRewardTo(TRewardTo trewardto)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTRewardTo";

            cmd.Parameters.AddWithValue("@PkSrNo", trewardto.PkSrNo);

            // cmd.Parameters.AddWithValue("@RewardNo", trewardto.RewardNo);

            //   cmd.Parameters.AddWithValue("@RewardDetailsNo", trewardto.RewardDetailsNo);

            cmd.Parameters.AddWithValue("@SchemeDetailsNo", trewardto.SchemeDetailsNo);

            cmd.Parameters.AddWithValue("@SchemeToNo", trewardto.SchemeToNo);

            //  cmd.Parameters.AddWithValue("@FkStockNo", trewardto.FkStockNo);

            cmd.Parameters.AddWithValue("@CompanyNo", trewardto.CompanyNo);

            commandcollection.Add(cmd);
            return true;
        }

        public bool DeleteTRewardTo(TRewardTo trewardto)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteTRewardTo";

            cmd.Parameters.AddWithValue("@PkSrNo", trewardto.PkSrNo);
            commandcollection.Add(cmd);
            return true;
        }

        public TRewardTo ModifyTRewardToByID(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql;
            SqlCommand cmd;
            sql = "Select * from TRewardTo where PkSrNo =" + ID;
            cmd = new SqlCommand(sql, Con);
            cmd.Connection = Con;
            Con.Open();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (dr.HasRows)
            {
                TRewardTo MM = new TRewardTo();
                while (dr.Read())
                {
                    MM.PkSrNo = Convert.ToInt32(dr["PkSrNo"]);
                    if (!Convert.IsDBNull(dr["RewardNo"])) MM.RewardNo = Convert.ToInt64(dr["RewardNo"]);
                    if (!Convert.IsDBNull(dr["RewardDetailsNo"])) MM.RewardDetailsNo = Convert.ToInt64(dr["RewardDetailsNo"]);
                    if (!Convert.IsDBNull(dr["SchemeToNo"])) MM.SchemeToNo = Convert.ToInt64(dr["SchemeToNo"]);
                    if (!Convert.IsDBNull(dr["FkStockNo"])) MM.FkStockNo = Convert.ToInt64(dr["FkStockNo"]);
                    if (!Convert.IsDBNull(dr["CompanyNo"])) MM.CompanyNo = Convert.ToInt64(dr["CompanyNo"]);
                }
                dr.Close();
                return MM;
            }
            else
                dr.Close();
            return new TRewardTo();
        }

        public DataView GetAllTRewardTo()
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from TRewardTo order by PkSrNo";
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch
            {
                throw;
            }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public DataView GetAllTRewardTo(long ID)
        {
            SqlConnection Con = new SqlConnection(CommonFunctions.ConStr);
            string sql = "Select * from TRewardTo Where PkSrNo=" + ID;
            SqlDataAdapter da = new SqlDataAdapter(sql, Con);
            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds);
            }
            catch
            {
                throw;
            }
            finally
            {
                Con.Close();
            }
            return ds.Tables[(0)].DefaultView;
        }

        public bool AddTFooterDiscountDetails(TFooterDiscountDetails tfooterdiscountdetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTFooterDiscountDetails";

            cmd.Parameters.AddWithValue("@PkSrNo", tfooterdiscountdetails.PkSrNo);

            cmd.Parameters.AddWithValue("@FooterDiscNo", tfooterdiscountdetails.FooterDiscNo);

            cmd.Parameters.AddWithValue("@FooterDiscDetailsNo", tfooterdiscountdetails.FooterDiscDetailsNo);

            //cmd.Parameters.AddWithValue("@FKVoucherNo", tfooterdiscountdetails.FKVoucherNo);

            cmd.Parameters.AddWithValue("@DiscAmount", tfooterdiscountdetails.DiscAmount);

            cmd.Parameters.AddWithValue("@CompanyNo", tfooterdiscountdetails.CompanyNo);

            cmd.Parameters.AddWithValue("@UserID", tfooterdiscountdetails.UserID);

            cmd.Parameters.AddWithValue("@UserDate", tfooterdiscountdetails.UserDate);

            commandcollection.Add(cmd);
            return true;
        }

        public bool DeleteTFooterDiscountDetails(TFooterDiscountDetails tfooterdiscountdetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteTFooterDiscountDetails";

            cmd.Parameters.AddWithValue("@PkSrNo", tfooterdiscountdetails.PkSrNo);
            commandcollection.Add(cmd);
            return true;
        }

        public bool AddTItemLevelDiscountDetails(TItemLevelDiscountDetails titemleveldiscountdetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTItemLevelDiscountDetails";

            cmd.Parameters.AddWithValue("@PkSrNo", titemleveldiscountdetails.PkSrNo);

            cmd.Parameters.AddWithValue("@ItemDiscNo", titemleveldiscountdetails.ItemDiscNo);

            cmd.Parameters.AddWithValue("@ItemBrandDiscNo", titemleveldiscountdetails.ItemBrandDiscNo);

            cmd.Parameters.AddWithValue("@ItemNo", titemleveldiscountdetails.ItemNo);

            cmd.Parameters.AddWithValue("@DiscAmount", titemleveldiscountdetails.DiscAmount);

            //cmd.Parameters.AddWithValue("@FKStockTrnNo", titemleveldiscountdetails.FKStockTrnNo);

            cmd.Parameters.AddWithValue("@CompanyNo", titemleveldiscountdetails.CompanyNo);

            cmd.Parameters.AddWithValue("@UserID", titemleveldiscountdetails.UserID);

            cmd.Parameters.AddWithValue("@UserDate", titemleveldiscountdetails.UserDate);

            commandcollection.Add(cmd);
            return true;
        }

        public bool DeleteTItemLevelDiscountDetails(TItemLevelDiscountDetails titemleveldiscountdetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteTItemLevelDiscountDetails";

            cmd.Parameters.AddWithValue("@PkSrNo", titemleveldiscountdetails.PkSrNo);
            commandcollection.Add(cmd);
            return true;
        }

        public int ReturnCommPos(long SchemeDtlsNo)
        {
            for (int i = 0; (i < this.commandcollection.Count); i = (i + 1))
            {
                if ((this.commandcollection[i] != null))
                {
                    if (commandcollection[i].CommandText == "AddTRewardDetails")
                    {
                        if (commandcollection[i].Parameters["@SchemeDetailsNo"].Value.ToString() == SchemeDtlsNo.ToString())
                        {
                            return i;
                        }
                    }
                }
            }
            return -1;
        }

        public bool UpdateSchemeAchievers(long pkSrNo)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Update MSchemeAchievers set IsItemDiscStatus='true',StatusNo=2 where PkSrNo=@pkSrNo";

            cmd.Parameters.AddWithValue("@PkSrNo", pkSrNo);

            commandcollection.Add(cmd);
            return true;
        }


        public bool AddTOtherStockDetails(TOtherStockDetails totherstockdetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "AddTOtherStockDetails";

            cmd.Parameters.AddWithValue("@PkSrNo", totherstockdetails.PkSrNo);

            //cmd.Parameters.AddWithValue("@FKVoucherNo", totherstockdetails.FKVoucherNo);

            //cmd.Parameters.AddWithValue("@FKStockTrnNo", totherstockdetails.FKStockTrnNo);

            cmd.Parameters.AddWithValue("@FKOtherVoucherNo", totherstockdetails.FKOtherVoucherNo);

            cmd.Parameters.AddWithValue("@FKOtherStockTrnNo", totherstockdetails.FKOtherStockTrnNo);

            cmd.Parameters.AddWithValue("@ItemNo", totherstockdetails.ItemNo);

            cmd.Parameters.AddWithValue("@Quantity", totherstockdetails.Quantity);

            cmd.Parameters.AddWithValue("@UserID", totherstockdetails.UserID);

            cmd.Parameters.AddWithValue("@UserDate", totherstockdetails.UserDate);

            cmd.Parameters.AddWithValue("@CompanyNo", totherstockdetails.CompanyNo);

            commandcollection.Add(cmd);
            return true;
        }

        public bool DeleteTOtherStockDetails(TOtherStockDetails totherstockdetails)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteTOtherStockDetails";

            cmd.Parameters.AddWithValue("@PkSrNo", totherstockdetails.PkSrNo);

            commandcollection.Add(cmd);
            return true;
        }

        public bool EffectStock()
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "StockEffect";

            commandcollection.Add(cmd);
            return true;
        }

        public bool ReverseStock(long ID)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "StockReverse";
            cmd.Parameters.AddWithValue("@FkVoucherNo", ID);
            commandcollection.Add(cmd);
            return true;
        }

        public bool UpdateReverStock(long ItemNo, double MRP, long GodownNo, double CurrentStock, long type)
        {
            SqlCommand cmd;
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Update MStockItemBalance set CurrentStock =CurrentStock+(" + ((type == 1) ? CurrentStock : -CurrentStock) + ") where ItemNo=" + ItemNo + " And MRP=" + MRP + " And GodownNo=" + GodownNo + "";

            commandcollection.Add(cmd);
            return true;
        }

        public void UpdateReceiptDetails(long SalesID, long CompNo, DateTime BillDate, long PartyLedgerNo, long ReciptType, long VoucherType, long OpningStockNo, long Type)
        {

            double totamt = 0;
            if (Type == 0)
                ObjTrans.Execute("Update TStock Set  FKVoucherNo=" + OpningStockNo + ", FkVoucherTrnNo=0 Where FkVoucherNo=" + SalesID + "", CommonFunctions.ConStr);

            DBTVaucherEntry dbTVoucherEntry = new DBTVaucherEntry();
            dbTVoucherEntry.DeleteAllVoucherEntry(SalesID);
            dbTVoucherEntry.ExecuteNonQueryStatements();


            dbTVoucherEntry = new DBTVaucherEntry();
            long ReceiptID = ObjQry.ReturnLong("SELECT TVoucherDetails.FkVoucherNo FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo " +
                        " WHERE (TVoucherEntry.VoucherTypeCode = " + ReciptType + ") AND (TVoucherEntry.VoucherDate ='" + Convert.ToDateTime(BillDate).ToString("dd-MMM-yyyy") + "') AND (TVoucherDetails.LedgerNo = " + PartyLedgerNo + ") AND " +
                        " (TVoucherDetails.VoucherSrNo = 1) AND (TVoucherEntry.PayTypeNo=2) AND TVoucherEntry.CompanyNo=" + CompNo + "", CommonFunctions.ConStr);
            if (ReceiptID != 0)
            {
                double Amt = ObjQry.ReturnDouble("SELECT IsNull(SUM(TVoucherDetails.Debit),0) FROM TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo " +
                                              " WHERE (TVoucherDetails.SrNo = 501) AND (TVoucherEntry.PayTypeNo=2) AND (TVoucherDetails.LedgerNo = " + PartyLedgerNo + ") AND (TVoucherEntry.CompanyNo = " + CompNo + ") AND (TVoucherEntry.VoucherDate = '" + BillDate.ToString("dd-MMM-yyyy") + "') AND  (TVoucherEntry.VoucherTypeCode = " + VoucherType + ") ", CommonFunctions.ConStr);

                if (Amt == 0)
                    dbTVoucherEntry.DeleteAllVoucherEntry(ReceiptID);
                else
                {
                    DataTable dtReceipt = ObjFunction.GetDataView("SELECT PkVoucherTrnNo,LedgerNo,Debit,Credit,SrNo FROM TVoucherDetails " +
                      " WHERE (FkVoucherNo = " + ReceiptID + ") order by VoucherSrNo ").Table;
                    for (int i = 0; i < dtReceipt.Rows.Count; i++)
                    {
                        if (i == 0)
                            dbTVoucherEntry.UpdateVoucherDetails(0, Amt, Convert.ToInt64(dtReceipt.Rows[i].ItemArray[0].ToString()));
                        else
                            dbTVoucherEntry.UpdateVoucherDetails(Amt, 0, Convert.ToInt64(dtReceipt.Rows[i].ItemArray[0].ToString()));
                    }
                    dbTVoucherEntry.UpdateBillAmount(ReceiptID, Amt);
                }
                dbTVoucherEntry.ExecuteNonQueryStatements();
            }
        }


        private void Upload_Sale_Bills(DTOClasses.UCSaleBillsList SB)
        {
            try
            {
                StringBuilder json = new StringBuilder();
                json.Append(JsonConvert.SerializeObject(SB.salesBills));

                ObjFunction.WriteMessage(json.ToString().Replace("&", "~"), LogLevel.Information, null);

                string result = ObjFunction.getResponseJSON("/SaveBills", "POST", string.Format("StrBills={0}", json.ToString().Replace("&", "~")));

                ObjFunction.WriteMessage(result, LogLevel.Information, null);
                UCException uex = JsonConvert.DeserializeObject<UCException>(result);
                ObjFunction.WriteMessage(uex.ToString(), LogLevel.Information, null);
                if (uex.errorCode == 0)
                {
                    DBTVaucherEntry dbTVoucherEntry = new DBTVaucherEntry();

                    foreach (DTOClasses.UCSaleBills sb in SB.salesBills)
                    {
                        dbTVoucherEntry.UpdateBillStatus(sb.tvoucherentry.pkvoucherno);
                    }
                    dbTVoucherEntry.ExecuteNonQueryStatements();
                }
                else
                {
                    throw new Exception(uex.errorCode + ":" + uex.errorMessage);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public bool SaleBill_Upload()
        {
            try
            {

                string sql = "SELECT  PkVoucherNo " +
                                   " FROM TVoucherEntry " +
                                   " WHERE VoucherTypeCode = " + VchType.Sales + " " +
                                           "  AND StatusNo in (1,2) And PayTypeNo=3" +
                                   " ORDER BY PkVoucherNo ";
                DataTable dtPkSrNo = ObjFunction.GetDataView(sql).Table;
                ObjFunction.WriteMessage("Total Upload Count :" + dtPkSrNo.Rows.Count + "", LogLevel.Information, null);
                DTOClasses.UCSaleBillsList SB = new DTOClasses.UCSaleBillsList();
                for (int i = 0; i < dtPkSrNo.Rows.Count; i++)
                {

                    try
                    {
                        SB.salesBills.Add(GetBillData(Convert.ToInt64(dtPkSrNo.Rows[i].ItemArray[0].ToString())));
                        //if (i % 2 == 0 && i > 0)
                        //{
                            ObjFunction.WriteMessage("Genrate Bill Start", LogLevel.Information, null);
                            Upload_Sale_Bills(SB);
                            SB = new DTOClasses.UCSaleBillsList();
                            ObjFunction.WriteMessage("Genrate Bill Successfully..", LogLevel.Information, null);
                       // }
                    }
                    catch (CustomeException.LedgerDetailsNotFound ex)
                    {
                        ObjFunction.WriteMessage(ex.Message, LogLevel.Error, ex);
                    }
                    catch (Exception ex)
                    {
                        SB = new DTOClasses.UCSaleBillsList();
                        throw new Exception(ex.Message);
                    }

                }
                ObjFunction.WriteMessage("Sale Bill Upload Sucessfully...", LogLevel.Information, null);
                return true;
                
            }
            catch (Exception ex)
            {
                ObjFunction.WriteMessage(ex.Message, LogLevel.Error, ex);
                ObjFunction.WriteMessage("Sale Bill Upload Fail", LogLevel.Information, null);
                return false;
            }

        }

        private DTOClasses.UCSaleBills GetBillData(long PkVoucherNo)
        {

            DTOClasses.UCSaleBills SB = new DTOClasses.UCSaleBills();
            try
            {
               // ObjFunction.WriteMessage("Start To Get Bill Data", LogLevel.Information, null);
                string sql = "SELECT TVoucherEntry.PkVoucherNo, TVoucherEntry.VoucherUserNo, TVoucherEntry.VoucherTime, TVoucherEntry.BilledAmount, TVoucherEntry.Remark, " +
                                " TVoucherEntry.IsCancel, MPayType.PayTypeName, TVoucherEntry.RateTypeNo," +
                                " ISNULL(TVoucherEntry.DiscAmt,0) As DiscAmt,ISNULL(TVD.Debit+ TVD.Credit, 0)  As HamaliRs,TVoucherEntry.StatusNo  " +
                            " FROM         TVoucherEntry INNER JOIN " +
                                " MPayType ON TVoucherEntry.PayTypeNo = MPayType.PKPayTypeNo  LEFT OUTER JOIN " +
                                " TVoucherDetails ON TVoucherDetails.FkVoucherNo = TVoucherEntry.PkVoucherNo AND TVoucherDetails.SrNo = 503 " +
                                " LEFT OUTER JOIN TVoucherDetails AS TVD ON TVD.FkVoucherNo = TVoucherEntry.PkVoucherNo AND TVD.SrNo = 506 "+
                            " WHERE     (TVoucherEntry.PkVoucherNo = " + PkVoucherNo + ") ";

                DataTable dt = ObjFunction.GetDataView(sql).Table;
                sql = "SELECT MLedger.LedgerNo,MLedger.LedgerName, MLedgerDetails.MobileNo1, MLedgerDetails.Address  " +
                          " FROM MLedger " +
                              " INNER JOIN MLedgerDetails ON MLedgerDetails.LedgerNo = MLedger.LedgerNo" +
                              " INNER JOIN TVoucherDetails ON TVoucherDetails.LedgerNo = MLedger.LedgerNo " +
                      " WHERE " +
                          " TVoucherDetails.FkVoucherNo = " + PkVoucherNo + " " +
                          " AND TVoucherDetails.SrNo = " + Others.Party + "";
                DataTable dtCustomerDetails = ObjFunction.GetDataView(sql).Table;
              //  ObjFunction.WriteMessage("Check Ledger", LogLevel.Information, null);
                if (dtCustomerDetails.Rows.Count == 0)
                {
                    throw new CustomeException.LedgerDetailsNotFound("Ledger Details Not Found BillNo=" + PkVoucherNo + "");
                }
              //  ObjFunction.WriteMessage("Check Ledger Sucessfully..", LogLevel.Information, null);

                DTOClasses.UCTVoucherEntry tvc = new DTOClasses.UCTVoucherEntry();
                tvc.pkvoucherno = Convert.ToInt64(dt.Rows[0]["PkVoucherNo"].ToString());
                tvc.vouchertypecode = VchType.Sales;
                tvc.voucheruserno = 0;
                tvc.salebillno = dt.Rows[0]["VoucherUserNo"].ToString();
                tvc.ledgerno = Convert.ToInt64(dtCustomerDetails.Rows[0]["LedgerNo"].ToString());
                tvc.voucherdate = Convert.ToDateTime(dt.Rows[0]["VoucherTime"].ToString()).Date;
                tvc.vouchertime = Convert.ToDateTime(dt.Rows[0]["VoucherTime"].ToString());
                tvc.billamount = Convert.ToDouble(dt.Rows[0]["BilledAmount"].ToString());
                tvc.remark = dt.Rows[0]["Remark"].ToString();
                tvc.iscancel = Convert.ToBoolean(dt.Rows[0]["IsCancel"].ToString());
                tvc.paymenttype = dt.Rows[0]["PayTypeName"].ToString();
                tvc.ratetype = GetRateType(Convert.ToInt64(dt.Rows[0]["RateTypeNo"].ToString()));
                tvc.billdiscamount = Convert.ToDouble(dt.Rows[0]["DiscAmt"].ToString());
                tvc.chargamount = Convert.ToDouble(dt.Rows[0]["HamaliRs"].ToString()); // Convert.ToDouble(dt.Rows[0]["DiscAmt"].ToString());
                tvc.statusno = Convert.ToInt64(dt.Rows[0]["StatusNo"].ToString());
                SB.tvoucherentry = tvc;

                DTOClasses.UCMLedger ML = new DTOClasses.UCMLedger();
                ML.ledgerno = Convert.ToInt64(dtCustomerDetails.Rows[0]["LedgerNo"].ToString());
                ML.ledgername = dtCustomerDetails.Rows[0]["LedgerName"].ToString();
                ML.mobileno = dtCustomerDetails.Rows[0]["MobileNo1"].ToString();
                ML.address = dtCustomerDetails.Rows[0]["Address"].ToString();
                ML.isactive = true;
                ML.userid = 1;
                ML.userdate = DateTime.Now;
                SB.mledger = ML;


                sql = "SELECT     MStockBarcode.Barcode, MStockItems.ItemName,MStockItems.ItemNameLang, TStock.BilledQuantity, TStock.Rate, " +
                         " TStock.Amount, TStock.NetRate, TStock.NetAmount, TStock.TaxPercentage, TStock.TaxAmount, " +
                         " TStock.DiscAmount, TStock.DiscRupees, TStock.DiscAmount2, TStock.DiscRupees2 " +
                         ", MRateSetting.MRP,MStockItems.ItemNo,MUOM.UOMName " +
                      " FROM         TStock INNER JOIN " +
                          " MStockBarcode ON TStock.FkStockBarCodeNo = MStockBarcode.PkStockBarcodeNo INNER JOIN " +
                          " MRateSetting ON TStock.FkRateSettingNo = MRateSetting.PkSrNo INNER JOIN " +
                          " dbo.MStockItems_V(NULL, NULL, NULL, NULL, NULL, NULL, NULL) AS MStockItems ON TStock.ItemNo = MStockItems.ItemNo " +
                          " INNER JOIN  MUOM ON TStock.FkUomNo = MUOM.UOMNo " +
                      " WHERE     (TStock.FkVoucherNo = " + PkVoucherNo + ") ";
             //   ObjFunction.WriteMessage(sql, LogLevel.Information, null);
                DataTable dtBillItems = ObjFunction.GetDataView(sql).Table;

                for (int i = 0; i < dtBillItems.Rows.Count; i++)
                {
                    DTOClasses.UCTStock billItem = new DTOClasses.UCTStock();
                    billItem.pkvouchertrnno = 0;
                    billItem.fkvoucherno = 0;
                    billItem.barcode = dtBillItems.Rows[i]["Barcode"].ToString();
                    billItem.itemname = dtBillItems.Rows[i]["ItemName"].ToString();
                    billItem.langitemname = dtBillItems.Rows[i]["ItemNameLang"].ToString();
                    billItem.qty = Convert.ToDouble(dtBillItems.Rows[i]["BilledQuantity"].ToString());
                    billItem.amount = Convert.ToDouble(dtBillItems.Rows[i]["Amount"].ToString());
                    billItem.rate = billItem.amount / billItem.qty;
                    billItem.mrp = Convert.ToDouble(dtBillItems.Rows[i]["MRP"].ToString()); ;
                    billItem.netamount = Convert.ToDouble(dtBillItems.Rows[i]["NetAmount"].ToString());
                    billItem.netrate = billItem.netamount / billItem.qty;
                    billItem.taxpercentage = Convert.ToDouble(dtBillItems.Rows[i]["TaxPercentage"].ToString());
                    billItem.taxamount = Convert.ToDouble(dtBillItems.Rows[i]["TaxAmount"].ToString());
                    billItem.discamount = Convert.ToDouble(dtBillItems.Rows[i]["DiscAmount"].ToString())
                                            + Convert.ToDouble(dtBillItems.Rows[i]["DiscRupees"].ToString())
                                            + Convert.ToDouble(dtBillItems.Rows[i]["DiscAmount2"].ToString())
                                            + Convert.ToDouble(dtBillItems.Rows[i]["DiscRupees2"].ToString());

                    billItem.itemno = Convert.ToInt64(dtBillItems.Rows[i]["ItemNo"].ToString());
                    billItem.uomname = dtBillItems.Rows[i]["UOMName"].ToString();
                    SB.tstock.Add(billItem);
                }

                if (tvc.paymenttype == "Credit")
                {
                    sql = " SELECT     TVoucherRefDetails.PkRefTrnNo, TVoucherRefDetails.FkVoucherTrnNo, TVoucherRefDetails.FkVoucherSrNo, TVoucherRefDetails.LedgerNo, TVoucherRefDetails.RefNo, " +
                        " TVoucherRefDetails.TypeOfRef, TVoucherRefDetails.DueDays, TVoucherRefDetails.DueDate, TVoucherRefDetails.Amount, TVoucherRefDetails.SignCode, TVoucherRefDetails.UserID, " +
                        " TVoucherRefDetails.UserDate, TVoucherRefDetails.Modifiedby, TVoucherRefDetails.CompanyNo, TVoucherRefDetails.StatusNo, TVoucherEntry.VoucherDate, TVoucherEntry.PayTypeNo, " +
                        " MPayType.PayTypeName " +
                        " FROM  TVoucherEntry INNER JOIN TVoucherDetails ON TVoucherEntry.PkVoucherNo = TVoucherDetails.FkVoucherNo And TVoucherDetails.SrNo=501 INNER JOIN " +
                        " TVoucherRefDetails ON TVoucherDetails.PkVoucherTrnNo = TVoucherRefDetails.FkVoucherTrnNo INNER JOIN " +
                        " MPayType ON TVoucherEntry.PayTypeNo = MPayType.PKPayTypeNo " +
                        " WHERE     (TVoucherRefDetails.RefNo IN (SELECT     RefNo FROM  TVoucherRefDetails AS TVoucherRefDetails_1 " +
                        " WHERE      (FkVoucherTrnNo IN (SELECT PkVoucherTrnNo FROM TVoucherDetails AS TVoucherDetails_1 " +
                        " WHERE      (FkVoucherNo = " + PkVoucherNo + "))))) ";
                  //  ObjFunction.WriteMessage(sql, LogLevel.Information, null);
                    DataTable dtref = ObjFunction.GetDataView(sql).Table;
                    for (int i = 0; i < dtref.Rows.Count; i++)
                    {
                        DTOClasses.UCTVoucherRefDetails tRef = new DTOClasses.UCTVoucherRefDetails();


                        tRef.pkreftrnno = 0;
                        tRef.refno = 0;
                        tRef.ledgerno = tvc.ledgerno;

                        tRef.typeofref = Convert.ToInt64(dtref.Rows[i]["TypeOfRef"].ToString());
                        tRef.amount = Convert.ToDouble(dtref.Rows[i]["Amount"].ToString());
                        tRef.discamount = 0;
                        if (dtref.Rows[i]["TypeOfRef"].ToString() == "3")
                        {
                            string stramt = dtref.Compute("Sum(Amount)", "TypeOfRef=2").ToString();
                            tRef.balamount = Convert.ToDouble((stramt == "") ? "0" : stramt);
                        }
                        else
                            tRef.balamount = 0;
                        tRef.paydate = Convert.ToDateTime(dtref.Rows[i]["VoucherDate"].ToString());
                        tRef.paytype = dtref.Rows[i]["PayTypeName"].ToString();
                        tRef.cardno = "";
                        tRef.remark = "";
                        SB.tVoucherrefdetails.Add(tRef);
                    }
                }
                
            }
            catch (Exception ex)
            {
                ObjFunction.WriteMessage(ex.Message, LogLevel.Error, ex);
                if (ex is CustomeException.LedgerDetailsNotFound)
                {
                    throw new CustomeException.LedgerDetailsNotFound(ex.Message);
                }
                else
                    throw new Exception(ex.Message);
            }

            return SB;
        }

        private string GetRateType(long RateTypeNo)
        {
            if (RateTypeNo == 1) return "ASaleRate";
            else if (RateTypeNo == 2) return "BSaleRate";
            else if (RateTypeNo == 3) return "CSaleRate";
            else if (RateTypeNo == 4) return "DSaleRate";
            else if (RateTypeNo == 5) return "ESaleRate";
            else if (RateTypeNo == 6) return "MRP";
            else return "";
        }

    }

    /// <summary>
    /// This Class use for TVoucherEntry
    /// </summary>
    public class TVoucherEntry
    {
        private long mPkVoucherNo;
        private long mVoucherTypeCode;
        private long mVoucherUserNo;
        private DateTime mVoucherDate;
        private DateTime mVoucherTime;
        private string mNarration;
        private string mReference;
        private long mChequeNo;
        private DateTime mClearingDate;
        private long mCompanyNo;
        private double mBilledAmount;
        private string mChallanNo;
        private string mRemark;
        private long mInwardLocationCode;
        private long mMacNo;
        private bool mIsCancel;
        private long mPayTypeNo;
        private long mRateTypeNo;
        private long mTaxTypeNo;
        private bool mIsVoucherLock;
        private int mVoucherStatus;
        private long mUserID;
        private DateTime mUserDate;
        private string mModifiedBy;
        private long mOrderType;
        private double mReturnAmount;
        private double mVisibility;
        private double mDiscPercent;
        private double mDiscAmt;
        private int mStatusNo;
        private int mMixMode;
        private bool mIsItemLevelDisc;
        private bool mIsFooterLevelDisc;
        private double mHamaliRs;
        private string Mmsg;

        /// <summary>
        /// This Properties use for PkVoucherNo
        /// </summary>
        public long PkVoucherNo
        {
            get { return mPkVoucherNo; }
            set { mPkVoucherNo = value; }
        }
        /// <summary>
        /// This Properties use for VoucherTypeCode
        /// </summary>
        public long VoucherTypeCode
        {
            get { return mVoucherTypeCode; }
            set { mVoucherTypeCode = value; }
        }
        /// <summary>
        /// This Properties use for VoucherUserNo
        /// </summary>
        public long VoucherUserNo
        {
            get { return mVoucherUserNo; }
            set { mVoucherUserNo = value; }
        }
        /// <summary>
        /// This Properties use for VoucherDate
        /// </summary>
        public DateTime VoucherDate
        {
            get { return mVoucherDate; }
            set { mVoucherDate = value; }
        }
        /// <summary>
        /// This Properties use for VoucherTime
        /// </summary>
        public DateTime VoucherTime
        {
            get { return mVoucherTime; }
            set { mVoucherTime = value; }
        }
        /// <summary>
        /// This Properties use for Narration
        /// </summary>
        public string Narration
        {
            get { return mNarration; }
            set { mNarration = value; }
        }
        /// <summary>
        /// This Properties use for Reference
        /// </summary>
        public string Reference
        {
            get { return mReference; }
            set { mReference = value; }
        }
        /// <summary>
        /// This Properties use for ChequeNo
        /// </summary>
        public long ChequeNo
        {
            get { return mChequeNo; }
            set { mChequeNo = value; }
        }
        /// <summary>
        /// This Properties use for ClearingDate
        /// </summary>
        public DateTime ClearingDate
        {
            get { return mClearingDate; }
            set { mClearingDate = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for BilledAmount
        /// </summary>
        public double BilledAmount
        {
            get { return mBilledAmount; }
            set { mBilledAmount = value; }
        }
        /// <summary>
        /// This Properties use for ChallanNo
        /// </summary>
        public string ChallanNo
        {
            get { return mChallanNo; }
            set { mChallanNo = value; }
        }
        /// <summary>
        /// This Properties use for Remark
        /// </summary>
        public string Remark
        {
            get { return mRemark; }
            set { mRemark = value; }
        }
        /// <summary>
        /// This Properties use for InwardLocationCode
        /// </summary>
        public long InwardLocationCode
        {
            get { return mInwardLocationCode; }
            set { mInwardLocationCode = value; }
        }
        /// <summary>
        /// This Properties use for MacNo
        /// </summary>
        public long MacNo
        {
            get { return mMacNo; }
            set { mMacNo = value; }
        }
        /// <summary>
        /// This Properties use for IsCancel
        /// </summary>
        public bool IsCancel
        {
            get { return mIsCancel; }
            set { mIsCancel = value; }
        }
        /// <summary>
        /// This Properties use for PayTypeNo
        /// </summary>
        public long PayTypeNo
        {
            get { return mPayTypeNo; }
            set { mPayTypeNo = value; }
        }
        /// <summary>
        /// This Properties use for RateTypeNo
        /// </summary>
        public long RateTypeNo
        {
            get { return mRateTypeNo; }
            set { mRateTypeNo = value; }
        }
        /// <summary>
        /// This Properties use for TaxTypeNo
        /// </summary>
        public long TaxTypeNo
        {
            get { return mTaxTypeNo; }
            set { mTaxTypeNo = value; }
        }
        /// <summary>
        /// This Properties use for IsVoucherLock
        /// </summary>
        public bool IsVoucherLock
        {
            get { return mIsVoucherLock; }
            set { mIsVoucherLock = value; }
        }
        /// <summary>
        /// This Properties use for VoucherStatus
        /// </summary>
        public int VoucherStatus
        {
            get { return mVoucherStatus; }
            set { mVoucherStatus = value; }
        }
        /// <summary>
        /// This Properties use for UserID
        /// </summary>
        public long UserID
        {
            get { return mUserID; }
            set { mUserID = value; }
        }
        /// <summary>
        /// This Properties use for UserDate
        /// </summary>
        public DateTime UserDate
        {
            get { return mUserDate; }
            set { mUserDate = value; }
        }
        /// <summary>
        /// This Properties use for ModifiedBy
        /// </summary>
        public string ModifiedBy
        {
            get { return mModifiedBy; }
            set { mModifiedBy = value; }
        }
        /// <summary>
        /// This Properties use for OrderType
        /// </summary>
        public long OrderType
        {
            get { return mOrderType; }
            set { mOrderType = value; }
        }
        /// <summary>
        /// This Properties use for ReturnAmount
        /// </summary>
        public double ReturnAmount
        {
            get { return mReturnAmount; }
            set { mReturnAmount = value; }
        }
        /// <summary>
        /// This Properties use for Visibility
        /// </summary>
        public double Visibility
        {
            get { return mVisibility; }
            set { mVisibility = value; }
        }
        /// <summary>
        /// This Properties use for DiscPercent
        /// </summary>
        public double DiscPercent
        {
            get { return mDiscPercent; }
            set { mDiscPercent = value; }
        }
        /// <summary>
        /// This Properties use for DiscAmt
        /// </summary>
        public double DiscAmt
        {
            get { return mDiscAmt; }
            set { mDiscAmt = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public int StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for MixMode
        /// </summary>
        public int MixMode
        {
            get { return mMixMode; }
            set { mMixMode = value; }
        }
        /// <summary>
        /// This Properties use for IsItemLevelDisc
        /// </summary>
        public bool IsItemLevelDisc
        {
            get { return mIsItemLevelDisc; }
            set { mIsItemLevelDisc = value; }
        }
        /// <summary>
        /// This Properties use for IsFooterLevelDisc
        /// </summary>
        public bool IsFooterLevelDisc
        {
            get { return mIsFooterLevelDisc; }
            set { mIsFooterLevelDisc = value; }
        }

        public double HamaliRs
        {
            get { return mHamaliRs; }
            set { mHamaliRs = value; }
        }

        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }
    /// <summary>
    /// This Class use for TVoucherDetails
    /// </summary>
    public class TVoucherDetails
    {
        private long mPkVoucherTrnNo;
        private long mFkVoucherNo;
        private long mVoucherSrNo;
        private long mSignCode;
        private long mLedgerNo;
        private double mDebit;
        private double mCredit;
        private long mSrNo;
        private long mCompanyNo;
        private string mNarration;
        private int mStatusNo;
        private string Mmsg;

        /// <summary>
        /// This Properties use for PkVoucherTrnNo
        /// </summary>
        public long PkVoucherTrnNo
        {
            get { return mPkVoucherTrnNo; }
            set { mPkVoucherTrnNo = value; }
        }
        /// <summary>
        /// This Properties use for FkVoucherNo
        /// </summary>
        public long FkVoucherNo
        {
            get { return mFkVoucherNo; }
            set { mFkVoucherNo = value; }
        }
        /// <summary>
        /// This Properties use for VoucherSrNo
        /// </summary>
        public long VoucherSrNo
        {
            get { return mVoucherSrNo; }
            set { mVoucherSrNo = value; }
        }
        /// <summary>
        /// This Properties use for SignCode
        /// </summary>
        public long SignCode
        {
            get { return mSignCode; }
            set { mSignCode = value; }
        }
        /// <summary>
        /// This Properties use for LedgerNo
        /// </summary>
        public long LedgerNo
        {
            get { return mLedgerNo; }
            set { mLedgerNo = value; }
        }
        /// <summary>
        /// This Properties use for Debit
        /// </summary>
        public double Debit
        {
            get { return mDebit; }
            set { mDebit = value; }
        }
        /// <summary>
        /// This Properties use for Credit
        /// </summary>
        public double Credit
        {
            get { return mCredit; }
            set { mCredit = value; }
        }
        /// <summary>
        /// This Properties use for SrNo
        /// </summary>
        public long SrNo
        {
            get { return mSrNo; }
            set { mSrNo = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for Narration
        /// </summary>
        public string Narration
        {
            get { return mNarration; }
            set { mNarration = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public int StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }

    /// <summary>
    /// This Class use for TStock
    /// </summary>
    public class TStock
    {
        private long mPkStockTrnNo;
        private long mFKVoucherNo;
        private long mFkVoucherTrnNo;
        private long mFkVoucherSrNo;
        private long mGroupNo;
        private long mItemNo;
        private long mTrnCode;
        private double mQuantity;
        private double mBilledQuantity;
        private double mRate;
        private double mAmount;
        private double mNetRate;
        private double mNetAmount;
        private double mTaxPercentage;
        private double mTaxAmount;
        private double mDiscPercentage;
        private double mDiscAmount;
        private double mDiscRupees;
        private double mDiscPercentage2;
        private double mDiscAmount2;
        private double mDiscRupees2;
        private long mFkUomNo;
        private long mFkStockBarCodeNo;
        private long mFkRateSettingNo;
        private long mFkItemTaxInfo;
        private bool mIsVoucherLock;
        private double mFreeQty;
        private long mFreeUOMNo;
        private long mUserID;
        private DateTime mUserDate;
        private string mModifiedBy;
        private long mCompanyNo;
        private int mStatusNo;
        private double mLandedRate;
        private long mDiscountType;
        private double mHamaliInKg;
        private string Mmsg;

        /// <summary>
        /// This Properties use for PkStockTrnNo
        /// </summary>
        public long PkStockTrnNo
        {
            get { return mPkStockTrnNo; }
            set { mPkStockTrnNo = value; }
        }
        /// <summary>
        /// This Properties use for FKVoucherNo
        /// </summary>
        public long FKVoucherNo
        {
            get { return mFKVoucherNo; }
            set { mFKVoucherNo = value; }
        }
        /// <summary>
        /// This Properties use for FkVoucherTrnNo
        /// </summary>
        public long FkVoucherTrnNo
        {
            get { return mFkVoucherTrnNo; }
            set { mFkVoucherTrnNo = value; }
        }
        /// <summary>
        /// This Properties use for FkVoucherSrNo
        /// </summary>
        public long FkVoucherSrNo
        {
            get { return mFkVoucherSrNo; }
            set { mFkVoucherSrNo = value; }
        }
        /// <summary>
        /// This Properties use for GroupNo
        /// </summary>
        public long GroupNo
        {
            get { return mGroupNo; }
            set { mGroupNo = value; }
        }
        /// <summary>
        /// This Properties use for ItemNo
        /// </summary>
        public long ItemNo
        {
            get { return mItemNo; }
            set { mItemNo = value; }
        }
        /// <summary>
        /// This Properties use for TrnCode
        /// </summary>
        public long TrnCode
        {
            get { return mTrnCode; }
            set { mTrnCode = value; }
        }
        /// <summary>
        /// This Properties use for Quantity
        /// </summary>
        public double Quantity
        {
            get { return mQuantity; }
            set { mQuantity = value; }
        }
        /// <summary>
        /// This Properties use for BilledQuantity
        /// </summary>
        public double BilledQuantity
        {
            get { return mBilledQuantity; }
            set { mBilledQuantity = value; }
        }
        /// <summary>
        /// This Properties use for Rate
        /// </summary>
        public double Rate
        {
            get { return mRate; }
            set { mRate = value; }
        }
        /// <summary>
        /// This Properties use for Amount
        /// </summary>
        public double Amount
        {
            get { return mAmount; }
            set { mAmount = value; }
        }
        /// <summary>
        /// This Properties use for NetRate
        /// </summary>
        public double NetRate
        {
            get { return mNetRate; }
            set { mNetRate = value; }
        }
        /// <summary>
        /// This Properties use for NetAmount
        /// </summary>
        public double NetAmount
        {
            get { return mNetAmount; }
            set { mNetAmount = value; }
        }
        /// <summary>
        /// This Properties use for TaxPercentage
        /// </summary>
        public double TaxPercentage
        {
            get { return mTaxPercentage; }
            set { mTaxPercentage = value; }
        }
        /// <summary>
        /// This Properties use for TaxAmount
        /// </summary>
        public double TaxAmount
        {
            get { return mTaxAmount; }
            set { mTaxAmount = value; }
        }
        /// <summary>
        /// This Properties use for DiscPercentage
        /// </summary>
        public double DiscPercentage
        {
            get { return mDiscPercentage; }
            set { mDiscPercentage = value; }
        }
        /// <summary>
        /// This Properties use for DiscAmount
        /// </summary>
        public double DiscAmount
        {
            get { return mDiscAmount; }
            set { mDiscAmount = value; }
        }
        /// <summary>
        /// This Properties use for DiscRupees
        /// </summary>
        public double DiscRupees
        {
            get { return mDiscRupees; }
            set { mDiscRupees = value; }
        }
        /// <summary>
        /// This Properties use for DiscPercentage2
        /// </summary>
        public double DiscPercentage2
        {
            get { return mDiscPercentage2; }
            set { mDiscPercentage2 = value; }
        }
        /// <summary>
        /// This Properties use for DiscAmount2
        /// </summary>
        public double DiscAmount2
        {
            get { return mDiscAmount2; }
            set { mDiscAmount2 = value; }
        }
        /// <summary>
        /// This Properties use for DiscRupees2
        /// </summary>
        public double DiscRupees2
        {
            get { return mDiscRupees2; }
            set { mDiscRupees2 = value; }
        }
        /// <summary>
        /// This Properties use for FkUomNo
        /// </summary>
        public long FkUomNo
        {
            get { return mFkUomNo; }
            set { mFkUomNo = value; }
        }
        /// <summary>
        /// This Properties use for FkStockBarCodeNo
        /// </summary>
        public long FkStockBarCodeNo
        {
            get { return mFkStockBarCodeNo; }
            set { mFkStockBarCodeNo = value; }
        }
        /// <summary>
        /// This Properties use for FkRateSettingNo
        /// </summary>
        public long FkRateSettingNo
        {
            get { return mFkRateSettingNo; }
            set { mFkRateSettingNo = value; }
        }
        /// <summary>
        /// This Properties use for FkItemTaxInfo
        /// </summary>
        public long FkItemTaxInfo
        {
            get { return mFkItemTaxInfo; }
            set { mFkItemTaxInfo = value; }
        }
        /// <summary>
        /// This Properties use for IsVoucherLock
        /// </summary>
        public bool IsVoucherLock
        {
            get { return mIsVoucherLock; }
            set { mIsVoucherLock = value; }
        }
        /// <summary>
        /// This Properties use for FreeQty
        /// </summary>
        public double FreeQty
        {
            get { return mFreeQty; }
            set { mFreeQty = value; }
        }
        /// <summary>
        /// This Properties use for FreeUOMNo
        /// </summary>
        public long FreeUOMNo
        {
            get { return mFreeUOMNo; }
            set { mFreeUOMNo = value; }
        }
        /// <summary>
        /// This Properties use for UserID
        /// </summary>
        public long UserID
        {
            get { return mUserID; }
            set { mUserID = value; }
        }
        /// <summary>
        /// This Properties use for UserDate
        /// </summary>
        public DateTime UserDate
        {
            get { return mUserDate; }
            set { mUserDate = value; }
        }
        /// <summary>
        /// This Properties use for ModifiedBy
        /// </summary>
        public string ModifiedBy
        {
            get { return mModifiedBy; }
            set { mModifiedBy = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public int StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for LandedRate
        /// </summary>
        public double LandedRate
        {
            get { return mLandedRate; }
            set { mLandedRate = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        /// 
        public long DiscountType
        {
            get { return mDiscountType; }
            set { mDiscountType = value; }
        }
        public double HamaliInKg
        {
            get { return mHamaliInKg; }
            set { mHamaliInKg = value; }
        }
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }

    /// <summary>
    /// This Class use for TStockGodown
    /// </summary>
    public class TStockGodown
    {
        private long mPKStockGodownNo;
        private long mFKStockTrnNo;
        private long mItemNo;
        private long mGodownNo;
        private double mQty;
        private double mActualQty;
        private long mUserID;
        private DateTime mUserDate;
        private string mModifiedBy;
        private long mCompanyNo;
        private string Mmsg;

        /// <summary>
        /// This Properties use for PKStockGodownNo
        /// </summary>
        public long PKStockGodownNo
        {
            get { return mPKStockGodownNo; }
            set { mPKStockGodownNo = value; }
        }
        /// <summary>
        /// This Properties use for FKStockTrnNo
        /// </summary>
        public long FKStockTrnNo
        {
            get { return mFKStockTrnNo; }
            set { mFKStockTrnNo = value; }
        }
        /// <summary>
        /// This Properties use for ItemNo
        /// </summary>
        public long ItemNo
        {
            get { return mItemNo; }
            set { mItemNo = value; }
        }
        /// <summary>
        /// This Properties use for GodownNo
        /// </summary>
        public long GodownNo
        {
            get { return mGodownNo; }
            set { mGodownNo = value; }
        }
        /// <summary>
        /// This Properties use for Qty
        /// </summary>
        public double Qty
        {
            get { return mQty; }
            set { mQty = value; }
        }
        /// <summary>
        /// This Properties use for ActualQty
        /// </summary>
        public double ActualQty
        {
            get { return mActualQty; }
            set { mActualQty = value; }
        }
        /// <summary>
        /// This Properties use for UserID
        /// </summary>
        public long UserID
        {
            get { return mUserID; }
            set { mUserID = value; }
        }
        /// <summary>
        /// This Properties use for UserDate
        /// </summary>
        public DateTime UserDate
        {
            get { return mUserDate; }
            set { mUserDate = value; }
        }
        /// <summary>
        /// This Properties use for ModifiedBy
        /// </summary>
        public string ModifiedBy
        {
            get { return mModifiedBy; }
            set { mModifiedBy = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }

    /// <summary>
    /// This Class use for TVoucherRefDetails
    /// </summary>
    public class TVoucherRefDetails
    {
        private long mPkRefTrnNo;
        private long mFkVoucherTrnNo;
        private long mFkVoucherSrNo;
        private long mLedgerNo;
        private long mRefNo;
        private int mTypeOfRef;
        private long mDueDays;
        private DateTime mDueDate;
        private double mAmount;
        private long mSignCode;
        private long mUserID;
        private DateTime mUserDate;
        private string mModifiedby;
        private long mCompanyNo;
        private int mStatusNo;
        private string Mmsg;

        /// <summary>
        /// This Properties use for PkRefTrnNo
        /// </summary>
        public long PkRefTrnNo
        {
            get { return mPkRefTrnNo; }
            set { mPkRefTrnNo = value; }
        }
        /// <summary>
        /// This Properties use for FkVoucherTrnNo
        /// </summary>
        public long FkVoucherTrnNo
        {
            get { return mFkVoucherTrnNo; }
            set { mFkVoucherTrnNo = value; }
        }
        /// <summary>
        /// This Properties use for FkVoucherSrNo
        /// </summary>
        public long FkVoucherSrNo
        {
            get { return mFkVoucherSrNo; }
            set { mFkVoucherSrNo = value; }
        }
        /// <summary>
        /// This Properties use for LedgerNo
        /// </summary>
        public long LedgerNo
        {
            get { return mLedgerNo; }
            set { mLedgerNo = value; }
        }
        /// <summary>
        /// This Properties use for RefNo
        /// </summary>
        public long RefNo
        {
            get { return mRefNo; }
            set { mRefNo = value; }
        }
        /// <summary>
        /// This Properties use for TypeOfRef
        /// </summary>
        public int TypeOfRef
        {
            get { return mTypeOfRef; }
            set { mTypeOfRef = value; }
        }
        /// <summary>
        /// This Properties use for DueDays
        /// </summary>
        public long DueDays
        {
            get { return mDueDays; }
            set { mDueDays = value; }
        }
        /// <summary>
        /// This Properties use for DueDate
        /// </summary>
        public DateTime DueDate
        {
            get { return mDueDate; }
            set { mDueDate = value; }
        }
        /// <summary>
        /// This Properties use for Amount
        /// </summary>
        public double Amount
        {
            get { return mAmount; }
            set { mAmount = value; }
        }
        /// <summary>
        /// This Properties use for SignCode
        /// </summary>
        public long SignCode
        {
            get { return mSignCode; }
            set { mSignCode = value; }
        }
        /// <summary>
        /// This Properties use for UserID
        /// </summary>
        public long UserID
        {
            get { return mUserID; }
            set { mUserID = value; }
        }
        /// <summary>
        /// This Properties use for UserDate
        /// </summary>
        public DateTime UserDate
        {
            get { return mUserDate; }
            set { mUserDate = value; }
        }
        /// <summary>
        /// This Properties use for Modifiedby
        /// </summary>
        public string Modifiedby
        {
            get { return mModifiedby; }
            set { mModifiedby = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public int StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }

    /// <summary>
    /// This Class use for TVoucherPayTypeDetails
    /// </summary>
    public class TVoucherPayTypeDetails
    {
        private long mPKVoucherPayTypeNo;
        private long mFKSalesVoucherNo;
        private long mFKReceiptVoucherNo;
        private long mFKVoucherTrnNo;
        private long mFKPayTypeNo;
        private double mAmount;
        private long mCompanyNo;
        private int mStatusNo;
        private string Mmsg;

        /// <summary>
        /// This Properties use for PKVoucherPayTypeNo
        /// </summary>
        public long PKVoucherPayTypeNo
        {
            get { return mPKVoucherPayTypeNo; }
            set { mPKVoucherPayTypeNo = value; }
        }
        /// <summary>
        /// This Properties use for FKSalesVoucherNo
        /// </summary>
        public long FKSalesVoucherNo
        {
            get { return mFKSalesVoucherNo; }
            set { mFKSalesVoucherNo = value; }
        }
        /// <summary>
        /// This Properties use for FKReceiptVoucherNo
        /// </summary>
        public long FKReceiptVoucherNo
        {
            get { return mFKReceiptVoucherNo; }
            set { mFKReceiptVoucherNo = value; }
        }
        /// <summary>
        /// This Properties use for FKVoucherTrnNo
        /// </summary>
        public long FKVoucherTrnNo
        {
            get { return mFKVoucherTrnNo; }
            set { mFKVoucherTrnNo = value; }
        }
        /// <summary>
        /// This Properties use for FKPayTypeNo
        /// </summary>
        public long FKPayTypeNo
        {
            get { return mFKPayTypeNo; }
            set { mFKPayTypeNo = value; }
        }
        /// <summary>
        /// This Properties use for Amount
        /// </summary>
        public double Amount
        {
            get { return mAmount; }
            set { mAmount = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public int StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }

    /// <summary>
    /// This Class use for TVoucherChequeDetails
    /// </summary>
    public class TVoucherChequeDetails
    {
        private long mPkSrNo;
        private long mFkVoucherTrnNo;
        private DateTime mClearingDate;
        private long mUserID;
        private DateTime mUserDate;
        private string Mmsg;

        /// <summary>
        /// This Properties use for PkSrNo
        /// </summary>
        public long PkSrNo
        {
            get { return mPkSrNo; }
            set { mPkSrNo = value; }
        }
        /// <summary>
        /// This Properties use for FkVoucherTrnNo
        /// </summary>
        public long FkVoucherTrnNo
        {
            get { return mFkVoucherTrnNo; }
            set { mFkVoucherTrnNo = value; }
        }
        /// <summary>
        /// This Properties use for ClearingDate
        /// </summary>
        public DateTime ClearingDate
        {
            get { return mClearingDate; }
            set { mClearingDate = value; }
        }
        /// <summary>
        /// This Properties use for UserID
        /// </summary>
        public long UserID
        {
            get { return mUserID; }
            set { mUserID = value; }
        }
        /// <summary>
        /// This Properties use for UserDate
        /// </summary>
        public DateTime UserDate
        {
            get { return mUserDate; }
            set { mUserDate = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }

    /// <summary>
    /// This Class use for TVoucherChqCreditDetails
    /// </summary>
    public class TVoucherChqCreditDetails
    {
        private long mPkSrNo;
        private long mFKVoucherNo;
        private long mFkVoucherTrnNo;
        private string mChequeNo;
        private DateTime mChequeDate;
        private long mBankNo;
        private long mBranchNo;
        private string mCreditCardNo;
        private double mAmount;
        private bool mIsPost;
        private long mPostFkVoucherNo;
        private long mPostFkVoucherTrnNo;
        private long mCompanyNo;
        private int mStatusNo;
        private string Mmsg;

        /// <summary>
        /// This Properties use for PkSrNo
        /// </summary>
        public long PkSrNo
        {
            get { return mPkSrNo; }
            set { mPkSrNo = value; }
        }
        /// <summary>
        /// This Properties use for FKVoucherNo
        /// </summary>
        public long FKVoucherNo
        {
            get { return mFKVoucherNo; }
            set { mFKVoucherNo = value; }
        }
        /// <summary>
        /// This Properties use for FkVoucherTrnNo
        /// </summary>
        public long FkVoucherTrnNo
        {
            get { return mFkVoucherTrnNo; }
            set { mFkVoucherTrnNo = value; }
        }
        /// <summary>
        /// This Properties use for ChequeNo
        /// </summary>
        public string ChequeNo
        {
            get { return mChequeNo; }
            set { mChequeNo = value; }
        }
        /// <summary>
        /// This Properties use for ChequeDate
        /// </summary>
        public DateTime ChequeDate
        {
            get { return mChequeDate; }
            set { mChequeDate = value; }
        }
        /// <summary>
        /// This Properties use for BankNo
        /// </summary>
        public long BankNo
        {
            get { return mBankNo; }
            set { mBankNo = value; }
        }
        /// <summary>
        /// This Properties use for BranchNo
        /// </summary>
        public long BranchNo
        {
            get { return mBranchNo; }
            set { mBranchNo = value; }
        }
        /// <summary>
        /// This Properties use for CreditCardNo
        /// </summary>
        public string CreditCardNo
        {
            get { return mCreditCardNo; }
            set { mCreditCardNo = value; }
        }
        /// <summary>
        /// This Properties use for Amount
        /// </summary>
        public double Amount
        {
            get { return mAmount; }
            set { mAmount = value; }
        }
        /// <summary>
        /// This Properties use for IsPost
        /// </summary>
        public bool IsPost
        {
            get { return mIsPost; }
            set { mIsPost = value; }
        }
        /// <summary>
        /// This Properties use for PostFkVoucherNo
        /// </summary>
        public long PostFkVoucherNo
        {
            get { return mPostFkVoucherNo; }
            set { mPostFkVoucherNo = value; }
        }
        /// <summary>
        /// This Properties use for PostFkVoucherTrnNo
        /// </summary>
        public long PostFkVoucherTrnNo
        {
            get { return mPostFkVoucherTrnNo; }
            set { mPostFkVoucherTrnNo = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public int StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }

    /// <summary>
    /// This Class use for TParkingBill
    /// </summary>
    public class TParkingBill
    {
        private long mParkingBillNo;
        private long mBillNo;
        private DateTime mBillDate;
        private DateTime mBillTime;
        private string mPersonName;
        private long mLedgerNo;
        private bool mIsBill;
        private long mFKVoucherNo;
        private long mCompanyNo;
        private bool mIsCancel;
        private long mUserID;
        private DateTime mUserDate;
        private int mStatusNo;
        private double mDiscount;
        private double mCharges;
        private string mRemark;
        private long mRateTypeNo;
        private long mTaxTypeNo;
        private string Mmsg;

        /// <summary>
        /// This Properties use for ParkingBillNo
        /// </summary>
        public long ParkingBillNo
        {
            get { return mParkingBillNo; }
            set { mParkingBillNo = value; }
        }
        /// <summary>
        /// This Properties use for BillNo
        /// </summary>
        public long BillNo
        {
            get { return mBillNo; }
            set { mBillNo = value; }
        }
        /// <summary>
        /// This Properties use for BillDate
        /// </summary>
        public DateTime BillDate
        {
            get { return mBillDate; }
            set { mBillDate = value; }
        }
        /// <summary>
        /// This Properties use for BillTime
        /// </summary>
        public DateTime BillTime
        {
            get { return mBillTime; }
            set { mBillTime = value; }
        }
        /// <summary>
        /// This Properties use for PersonName
        /// </summary>
        public string PersonName
        {
            get { return mPersonName; }
            set { mPersonName = value; }
        }
        /// <summary>
        /// This Properties use for LedgerNo
        /// </summary>
        public long LedgerNo
        {
            get { return mLedgerNo; }
            set { mLedgerNo = value; }
        }
        /// <summary>
        /// This Properties use for IsBill
        /// </summary>
        public bool IsBill
        {
            get { return mIsBill; }
            set { mIsBill = value; }
        }
        /// <summary>
        /// This Properties use for FKVoucherNo
        /// </summary>
        public long FKVoucherNo
        {
            get { return mFKVoucherNo; }
            set { mFKVoucherNo = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for IsCancel
        /// </summary>
        public bool IsCancel
        {
            get { return mIsCancel; }
            set { mIsCancel = value; }
        }
        /// <summary>
        /// This Properties use for UserID
        /// </summary>
        public long UserID
        {
            get { return mUserID; }
            set { mUserID = value; }
        }
        /// <summary>
        /// This Properties use for UserDate
        /// </summary>
        public DateTime UserDate
        {
            get { return mUserDate; }
            set { mUserDate = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public int StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for Discount
        /// </summary>
        public double Discount
        {
            get { return mDiscount; }
            set { mDiscount = value; }
        }
        /// <summary>
        /// This Properties use for Charges
        /// </summary>
        public double Charges
        {
            get { return mCharges; }
            set { mCharges = value; }
        }
        /// <summary>
        /// This Properties use for Remark
        /// </summary>
        public string Remark
        {
            get { return mRemark; }
            set { mRemark = value; }
        }
        /// <summary>
        /// This Properties use for RateTypeNo
        /// </summary>
        public long RateTypeNo
        {
            get { return mRateTypeNo; }
            set { mRateTypeNo = value; }
        }
        /// <summary>
        /// This Properties use for TaxTypeNo
        /// </summary>
        public long TaxTypeNo
        {
            get { return mTaxTypeNo; }
            set { mTaxTypeNo = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }

    /// <summary>
    /// This Class use for TParkingBillDetails
    /// </summary>
    public class TParkingBillDetails
    {
        private long mPkSrNo;
        private long mParkingBillNo;
        private string mBarcode;
        private double mQty;
        private double mRate;
        private double mItemDisc;
        private long mUOMNo;
        private int mStatusNo;
        private long mCompanyNo;
        private long mFkRateSettingNo;
        private long mItemNo;
        private string Mmsg;

        /// <summary>
        /// This Properties use for PkSrNo
        /// </summary>
        public long PkSrNo
        {
            get { return mPkSrNo; }
            set { mPkSrNo = value; }
        }
        /// <summary>
        /// This Properties use for ParkingBillNo
        /// </summary>
        public long ParkingBillNo
        {
            get { return mParkingBillNo; }
            set { mParkingBillNo = value; }
        }
        /// <summary>
        /// This Properties use for Barcode
        /// </summary>
        public string Barcode
        {
            get { return mBarcode; }
            set { mBarcode = value; }
        }
        /// <summary>
        /// This Properties use for Qty
        /// </summary>
        public double Qty
        {
            get { return mQty; }
            set { mQty = value; }
        }
        /// <summary>
        /// This Properties use for Rate
        /// </summary>
        public double Rate
        {
            get { return mRate; }
            set { mRate = value; }
        }
        /// <summary>
        /// This Properties use for ItemDisc
        /// </summary>
        public double ItemDisc
        {
            get { return mItemDisc; }
            set { mItemDisc = value; }
        }
        /// <summary>
        /// This Properties use for UOMNo
        /// </summary>
        public long UOMNo
        {
            get { return mUOMNo; }
            set { mUOMNo = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public int StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for FkRateSettingNo
        /// </summary>
        public long FkRateSettingNo
        {
            get { return mFkRateSettingNo; }
            set { mFkRateSettingNo = value; }
        }
        /// <summary>
        /// This Properties use for ItemNo
        /// </summary>
        public long ItemNo
        {
            get { return mItemNo; }
            set { mItemNo = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }

    /// <summary>
    /// This Class use for MRateSetting
    /// </summary>
    public class MRateSetting3
    {
        private long mPkSrNo;
        private long mFkBcdSrNo;
        private long mItemNo;
        private DateTime mFromDate;
        private double mPurRate;
        private double mMRP;
        private long mUOMNo;
        private double mASaleRate;
        private double mBSaleRate;
        private double mCSaleRate;
        private double mDSaleRate;
        private double mESaleRate;
        private double mStockConversion;
        private double mPerOfRateVariation;
        private long mMKTQty;
        private bool mIsActive;
        private long mUserID;
        private DateTime mUserDate;
        private string mModifiedBy;
        private long mCompanyNo;
        private int mStatusNo;
        private string Mmsg;

        /// <summary>
        /// This Properties use for PkSrNo
        /// </summary>
        public long PkSrNo
        {
            get { return mPkSrNo; }
            set { mPkSrNo = value; }
        }
        /// <summary>
        /// This Properties use for FkBcdSrNo
        /// </summary>
        public long FkBcdSrNo
        {
            get { return mFkBcdSrNo; }
            set { mFkBcdSrNo = value; }
        }
        /// <summary>
        /// This Properties use for ItemNo
        /// </summary>
        public long ItemNo
        {
            get { return mItemNo; }
            set { mItemNo = value; }
        }
        /// <summary>
        /// This Properties use for FromDate
        /// </summary>
        public DateTime FromDate
        {
            get { return mFromDate; }
            set { mFromDate = value; }
        }
        /// <summary>
        /// This Properties use for PurRate
        /// </summary>
        public double PurRate
        {
            get { return mPurRate; }
            set { mPurRate = value; }
        }
        /// <summary>
        /// This Properties use for MRP
        /// </summary>
        public double MRP
        {
            get { return mMRP; }
            set { mMRP = value; }
        }
        /// <summary>
        /// This Properties use for UOMNo
        /// </summary>
        public long UOMNo
        {
            get { return mUOMNo; }
            set { mUOMNo = value; }
        }
        /// <summary>
        /// This Properties use for ASaleRate
        /// </summary>
        public double ASaleRate
        {
            get { return mASaleRate; }
            set { mASaleRate = value; }
        }
        /// <summary>
        /// This Properties use for BSaleRate
        /// </summary>
        public double BSaleRate
        {
            get { return mBSaleRate; }
            set { mBSaleRate = value; }
        }
        /// <summary>
        /// This Properties use for CSaleRate
        /// </summary>
        public double CSaleRate
        {
            get { return mCSaleRate; }
            set { mCSaleRate = value; }
        }
        /// <summary>
        /// This Properties use for DSaleRate
        /// </summary>
        public double DSaleRate
        {
            get { return mDSaleRate; }
            set { mDSaleRate = value; }
        }
        /// <summary>
        /// This Properties use for ESaleRate
        /// </summary>
        public double ESaleRate
        {
            get { return mESaleRate; }
            set { mESaleRate = value; }
        }
        /// <summary>
        /// This Properties use for StockConversion
        /// </summary>
        public double StockConversion
        {
            get { return mStockConversion; }
            set { mStockConversion = value; }
        }
        /// <summary>
        /// This Properties use for PerOfRateVariation
        /// </summary>
        public double PerOfRateVariation
        {
            get { return mPerOfRateVariation; }
            set { mPerOfRateVariation = value; }
        }
        /// <summary>
        /// This Properties use for MKTQty
        /// </summary>
        public long MKTQty
        {
            get { return mMKTQty; }
            set { mMKTQty = value; }
        }
        /// <summary>
        /// This Properties use for IsActive
        /// </summary>
        public bool IsActive
        {
            get { return mIsActive; }
            set { mIsActive = value; }
        }
        /// <summary>
        /// This Properties use for UserID
        /// </summary>
        public long UserID
        {
            get { return mUserID; }
            set { mUserID = value; }
        }
        /// <summary>
        /// This Properties use for UserDate
        /// </summary>
        public DateTime UserDate
        {
            get { return mUserDate; }
            set { mUserDate = value; }
        }
        /// <summary>
        /// This Properties use for ModifiedBy
        /// </summary>
        public string ModifiedBy
        {
            get { return mModifiedBy; }
            set { mModifiedBy = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public int StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }

    /// <summary>
    /// This Class use for TReward
    /// </summary>
    public class TReward
    {
        private long mRewardNo;
        private long mFkVoucherNo;
        private long mVoucherUserNo;
        private double mTotalBillAmount;
        private long mLedgerNo;
        private double mToalDiscAmount;
        private long mFkVoucherTrnNo;
        private long mRedemptionStatusNo;
        private long mStatusNo;
        private long mCompanyNo;
        private long mUserID;
        private DateTime mUserDate;
        private string Mmsg;

        /// <summary>
        /// This Properties use for RewardNo
        /// </summary>
        public long RewardNo
        {
            get { return mRewardNo; }
            set { mRewardNo = value; }
        }
        /// <summary>
        /// This Properties use for FkVoucherNo
        /// </summary>
        public long FkVoucherNo
        {
            get { return mFkVoucherNo; }
            set { mFkVoucherNo = value; }
        }
        /// <summary>
        /// This Properties use for VoucherUserNo
        /// </summary>
        public long VoucherUserNo
        {
            get { return mVoucherUserNo; }
            set { mVoucherUserNo = value; }
        }
        /// <summary>
        /// This Properties use for TotalBillAmount
        /// </summary>
        public double TotalBillAmount
        {
            get { return mTotalBillAmount; }
            set { mTotalBillAmount = value; }
        }
        /// <summary>
        /// This Properties use for LedgerNo
        /// </summary>
        public long LedgerNo
        {
            get { return mLedgerNo; }
            set { mLedgerNo = value; }
        }
        /// <summary>
        /// This Properties use for ToalDiscAmount
        /// </summary>
        public double ToalDiscAmount
        {
            get { return mToalDiscAmount; }
            set { mToalDiscAmount = value; }
        }
        /// <summary>
        /// This Properties use for FkVoucherTrnNo
        /// </summary>
        public long FkVoucherTrnNo
        {
            get { return mFkVoucherTrnNo; }
            set { mFkVoucherTrnNo = value; }
        }
        /// <summary>
        /// This Properties use for RedemptionStatusNo
        /// </summary>
        public long RedemptionStatusNo
        {
            get { return mRedemptionStatusNo; }
            set { mRedemptionStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public long StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for UserID
        /// </summary>
        public long UserID
        {
            get { return mUserID; }
            set { mUserID = value; }
        }
        /// <summary>
        /// This Properties use for UserDate
        /// </summary>
        public DateTime UserDate
        {
            get { return mUserDate; }
            set { mUserDate = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }

    /// <summary>
    /// This Class use for TRewardDetails
    /// </summary>
    public class TRewardDetails
    {
        private long mPkSrNo;
        private long mRewardNo;
        private long mSchemeNo;
        private long mSchemeDetailsNo;
        private long mSchemeType;
        private double mDiscPer;
        private double mDiscAmount;
        private double mSchemeAmount;
        private long mSchemeAcheiverNo;
        private long mStatusNo;
        private long mCompanyNo;
        private long mAchievedNoOfTime;
        private long mActualNoOfTime;
        private string Mmsg;

        /// <summary>
        /// This Properties use for PkSrNo
        /// </summary>
        public long PkSrNo
        {
            get { return mPkSrNo; }
            set { mPkSrNo = value; }
        }
        /// <summary>
        /// This Properties use for RewardNo
        /// </summary>
        public long RewardNo
        {
            get { return mRewardNo; }
            set { mRewardNo = value; }
        }
        /// <summary>
        /// This Properties use for SchemeNo
        /// </summary>
        public long SchemeNo
        {
            get { return mSchemeNo; }
            set { mSchemeNo = value; }
        }
        /// <summary>
        /// This Properties use for SchemeDetailsNo
        /// </summary>
        public long SchemeDetailsNo
        {
            get { return mSchemeDetailsNo; }
            set { mSchemeDetailsNo = value; }
        }
        /// <summary>
        /// This Properties use for SchemeType
        /// </summary>
        public long SchemeType
        {
            get { return mSchemeType; }
            set { mSchemeType = value; }
        }
        /// <summary>
        /// This Properties use for DiscPer
        /// </summary>
        public double DiscPer
        {
            get { return mDiscPer; }
            set { mDiscPer = value; }
        }
        /// <summary>
        /// This Properties use for DiscAmount
        /// </summary>
        public double DiscAmount
        {
            get { return mDiscAmount; }
            set { mDiscAmount = value; }
        }
        /// <summary>
        /// This Properties use for SchemeAmount
        /// </summary>
        public double SchemeAmount
        {
            get { return mSchemeAmount; }
            set { mSchemeAmount = value; }
        }
        /// <summary>
        /// This Properties use for SchemeAcheiverNo
        /// </summary>
        public long SchemeAcheiverNo
        {
            get { return mSchemeAcheiverNo; }
            set { mSchemeAcheiverNo = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public long StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for AchievedNoOfTime
        /// </summary>
        public long AchievedNoOfTime
        {
            get { return mAchievedNoOfTime; }
            set { mAchievedNoOfTime = value; }
        }
        /// <summary>
        /// This Properties use for ActualNoOfTime
        /// </summary>
        public long ActualNoOfTime
        {
            get { return mActualNoOfTime; }
            set { mActualNoOfTime = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }

    /// <summary>
    /// This Class use for TRewardFrom
    /// </summary>
    public class TRewardFrom
    {
        private long mPkSrNo;
        private long mRewardNo;
        private long mRewardDetailsNo;
        private long mSchemeDetailsNo;
        private long mSchemeFromNo;
        private long mFkStockNo;
        private long mStatusNo;
        private long mCompanyNo;
        private string Mmsg;

        /// <summary>
        /// This Properties use for PkSrNo
        /// </summary>
        public long PkSrNo
        {
            get { return mPkSrNo; }
            set { mPkSrNo = value; }
        }
        /// <summary>
        /// This Properties use for RewardNo
        /// </summary>
        public long RewardNo
        {
            get { return mRewardNo; }
            set { mRewardNo = value; }
        }
        /// <summary>
        /// This Properties use for RewardDetailsNo
        /// </summary>
        public long RewardDetailsNo
        {
            get { return mRewardDetailsNo; }
            set { mRewardDetailsNo = value; }
        }
        /// <summary>
        /// This Properties use for SchemeDetailsNo
        /// </summary>
        public long SchemeDetailsNo
        {
            get { return mSchemeDetailsNo; }
            set { mSchemeDetailsNo = value; }
        }
        /// <summary>
        /// This Properties use for SchemeFromNo
        /// </summary>
        public long SchemeFromNo
        {
            get { return mSchemeFromNo; }
            set { mSchemeFromNo = value; }
        }
        /// <summary>
        /// This Properties use for FkStockNo
        /// </summary>
        public long FkStockNo
        {
            get { return mFkStockNo; }
            set { mFkStockNo = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public long StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }

    /// <summary>
    /// This Class use for TRewardTo
    /// </summary>
    public class TRewardTo
    {
        private long mPkSrNo;
        private long mRewardNo;
        private long mRewardDetailsNo;
        private long mSchemeDetailsNo;
        private long mSchemeToNo;
        private long mFkStockNo;
        private long mStatusNo;
        private long mCompanyNo;
        private string Mmsg;

        /// <summary>
        /// This Properties use for PkSrNo
        /// </summary>
        public long PkSrNo
        {
            get { return mPkSrNo; }
            set { mPkSrNo = value; }
        }
        /// <summary>
        /// This Properties use for RewardNo
        /// </summary>
        public long RewardNo
        {
            get { return mRewardNo; }
            set { mRewardNo = value; }
        }
        /// <summary>
        /// This Properties use for RewardDetailsNo
        /// </summary>
        public long RewardDetailsNo
        {
            get { return mRewardDetailsNo; }
            set { mRewardDetailsNo = value; }
        }
        /// <summary>
        /// This Properties use for SchemeDetailsNo
        /// </summary>
        public long SchemeDetailsNo
        {
            get { return mSchemeDetailsNo; }
            set { mSchemeDetailsNo = value; }
        }
        /// <summary>
        /// This Properties use for SchemeToNo
        /// </summary>
        public long SchemeToNo
        {
            get { return mSchemeToNo; }
            set { mSchemeToNo = value; }
        }
        /// <summary>
        /// This Properties use for FkStockNo
        /// </summary>
        public long FkStockNo
        {
            get { return mFkStockNo; }
            set { mFkStockNo = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public long StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }

    /// <summary>
    /// This Class use for TFooterDiscountDetails
    /// </summary>
    public class TFooterDiscountDetails
    {
        private long mPkSrNo;
        private long mFooterDiscNo;
        private long mFooterDiscDetailsNo;
        private long mFKVoucherNo;
        private double mDiscAmount;
        private long mCompanyNo;
        private long mStatusNo;
        private long mUserID;
        private DateTime mUserDate;
        private string Mmsg;

        /// <summary>
        /// This Properties use for PkSrNo
        /// </summary>
        public long PkSrNo
        {
            get { return mPkSrNo; }
            set { mPkSrNo = value; }
        }
        /// <summary>
        /// This Properties use for FooterDiscNo
        /// </summary>
        public long FooterDiscNo
        {
            get { return mFooterDiscNo; }
            set { mFooterDiscNo = value; }
        }
        /// <summary>
        /// This Properties use for FooterDiscDetailsNo
        /// </summary>
        public long FooterDiscDetailsNo
        {
            get { return mFooterDiscDetailsNo; }
            set { mFooterDiscDetailsNo = value; }
        }
        /// <summary>
        /// This Properties use for FKVoucherNo
        /// </summary>
        public long FKVoucherNo
        {
            get { return mFKVoucherNo; }
            set { mFKVoucherNo = value; }
        }
        /// <summary>
        /// This Properties use for DiscAmount
        /// </summary>
        public double DiscAmount
        {
            get { return mDiscAmount; }
            set { mDiscAmount = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public long StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for UserID
        /// </summary>
        public long UserID
        {
            get { return mUserID; }
            set { mUserID = value; }
        }
        /// <summary>
        /// This Properties use for UserDate
        /// </summary>
        public DateTime UserDate
        {
            get { return mUserDate; }
            set { mUserDate = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }

    /// <summary>
    /// This Class use for TItemLevelDiscountDetails
    /// </summary>
    public class TItemLevelDiscountDetails
    {
        private long mPkSrNo;
        private long mItemDiscNo;
        private long mItemBrandDiscNo;
        private long mItemNo;
        private double mDiscAmount;
        private long mFKStockTrnNo;
        private long mCompanyNo;
        private long mStatusNo;
        private long mUserID;
        private DateTime mUserDate;
        private string Mmsg;

        /// <summary>
        /// This Properties use for PkSrNo
        /// </summary>
        public long PkSrNo
        {
            get { return mPkSrNo; }
            set { mPkSrNo = value; }
        }
        /// <summary>
        /// This Properties use for ItemDiscNo
        /// </summary>
        public long ItemDiscNo
        {
            get { return mItemDiscNo; }
            set { mItemDiscNo = value; }
        }
        /// <summary>
        /// This Properties use for ItemBrandDiscNo
        /// </summary>
        public long ItemBrandDiscNo
        {
            get { return mItemBrandDiscNo; }
            set { mItemBrandDiscNo = value; }
        }
        /// <summary>
        /// This Properties use for ItemNo
        /// </summary>
        public long ItemNo
        {
            get { return mItemNo; }
            set { mItemNo = value; }
        }
        /// <summary>
        /// This Properties use for DiscAmount
        /// </summary>
        public double DiscAmount
        {
            get { return mDiscAmount; }
            set { mDiscAmount = value; }
        }
        /// <summary>
        /// This Properties use for FKStockTrnNo
        /// </summary>
        public long FKStockTrnNo
        {
            get { return mFKStockTrnNo; }
            set { mFKStockTrnNo = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public long StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for UserID
        /// </summary>
        public long UserID
        {
            get { return mUserID; }
            set { mUserID = value; }
        }
        /// <summary>
        /// This Properties use for UserDate
        /// </summary>
        public DateTime UserDate
        {
            get { return mUserDate; }
            set { mUserDate = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }

    /// <summary>
    /// This Class use for TOtherStockDetails
    /// </summary>
    public class TOtherStockDetails
    {
        private long mPkSrNo;
        private long mFKVoucherNo;
        private long mFKStockTrnNo;
        private long mFKOtherVoucherNo;
        private long mFKOtherStockTrnNo;
        private long mItemNo;
        private double mQuantity;
        private long mUserID;
        private DateTime mUserDate;
        private long mCompanyNo;
        private int mStatusNo;
        private string Mmsg;

        /// <summary>
        /// This Properties use for PkSrNo
        /// </summary>
        public long PkSrNo
        {
            get { return mPkSrNo; }
            set { mPkSrNo = value; }
        }
        /// <summary>
        /// This Properties use for FKVoucherNo
        /// </summary>
        public long FKVoucherNo
        {
            get { return mFKVoucherNo; }
            set { mFKVoucherNo = value; }
        }
        /// <summary>
        /// This Properties use for FKStockTrnNo
        /// </summary>
        public long FKStockTrnNo
        {
            get { return mFKStockTrnNo; }
            set { mFKStockTrnNo = value; }
        }
        /// <summary>
        /// This Properties use for FKOtherVoucherNo
        /// </summary>
        public long FKOtherVoucherNo
        {
            get { return mFKOtherVoucherNo; }
            set { mFKOtherVoucherNo = value; }
        }
        /// <summary>
        /// This Properties use for FKOtherStockTrnNo
        /// </summary>
        public long FKOtherStockTrnNo
        {
            get { return mFKOtherStockTrnNo; }
            set { mFKOtherStockTrnNo = value; }
        }
        /// <summary>
        /// This Properties use for ItemNo
        /// </summary>
        public long ItemNo
        {
            get { return mItemNo; }
            set { mItemNo = value; }
        }
        /// <summary>
        /// This Properties use for Quantity
        /// </summary>
        public double Quantity
        {
            get { return mQuantity; }
            set { mQuantity = value; }
        }
        /// <summary>
        /// This Properties use for UserID
        /// </summary>
        public long UserID
        {
            get { return mUserID; }
            set { mUserID = value; }
        }
        /// <summary>
        /// This Properties use for UserDate
        /// </summary>
        public DateTime UserDate
        {
            get { return mUserDate; }
            set { mUserDate = value; }
        }
        /// <summary>
        /// This Properties use for CompanyNo
        /// </summary>
        public long CompanyNo
        {
            get { return mCompanyNo; }
            set { mCompanyNo = value; }
        }
        /// <summary>
        /// This Properties use for StatusNo
        /// </summary>
        public int StatusNo
        {
            get { return mStatusNo; }
            set { mStatusNo = value; }
        }
        /// <summary>
        /// This Properties use for msg
        /// </summary>
        public string msg
        {
            get { return Mmsg; }
            set { Mmsg = value; }
        }
    }

}
