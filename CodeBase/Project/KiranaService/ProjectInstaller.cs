﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;

namespace KiranaService
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
            //this.Committed += new InstallEventHandler(ProjectInstaller_Committed);
            this.BeforeInstall += new InstallEventHandler(ProjectInstaller_BeforeInstall);
            this.BeforeUninstall += new InstallEventHandler(ProjectInstaller_BeforeUninstall);
        }

        void ProjectInstaller_BeforeUninstall(object sender, InstallEventArgs e)
        {
            var controller = new ServiceController(serviceInstaller1.ServiceName);
            try
            {
                if ((controller.Status != ServiceControllerStatus.Stopped) && (controller.Status != ServiceControllerStatus.StopPending))
                {
                    controller.Stop();
                }
            }
            catch (System.InvalidOperationException)
            {
            }
        }

        void ProjectInstaller_BeforeInstall(object sender, InstallEventArgs e)
        {
            var controller = new ServiceController(serviceInstaller1.ServiceName);
            try
            {
                if ((controller.Status != ServiceControllerStatus.Stopped) && (controller.Status != ServiceControllerStatus.StopPending))
                {
                    controller.Stop();
                }
            }
            catch (System.InvalidOperationException)
            {
            }
        }

        void ProjectInstaller_Committed(object sender, InstallEventArgs e)
        {
            var controller = new ServiceController(serviceInstaller1.ServiceName);

            if ((controller.Status == ServiceControllerStatus.Stopped) ||

            (controller.Status == ServiceControllerStatus.StopPending))
            {

                controller.Start();

            }

        }

        private void serviceProcessInstaller1_AfterInstall(object sender, InstallEventArgs e)
        {

        }
    }
}
